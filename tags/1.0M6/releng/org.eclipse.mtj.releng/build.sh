#!/bin/bash

# ****************************************************************
# Copyright (c) 2008 Motorola.
# 
# All rights reserved. This program and the accompanying 
# materials are made available under the terms of the 
# Eclipse Public License v1.0 which accompanies this 
# distribution, and is available at:
# http://www.eclipse.org/legal/epl-v10.html 
# 
# Contributors:
#     Motorola - initial version
# ****************************************************************

# *************************************************************************************************************************************************
#         										Mobile Tools for Java build bootstrap script
# *************************************************************************************************************************************************
# Usage : 
# 			setup.sh [-buildId name] [-fetchTag tag] [-publish [-outputDirectory directory] ] [-updateSite [-updateSiteDirectory directory]] 
#                    [-notify] [-sign] [-runTests] M|N|I|S|R
#
# *************************************************************************************************************************************************

# *************************************************************************************************************************************************
# 													Enable build.sh debug mode
# *************************************************************************************************************************************************

# Enable build.sh debug mode
#set -x

# *************************************************************************************************************************************************
# 													JAVA_HOME and PATH configuration
# *************************************************************************************************************************************************

# Must use the Java SE 5.0 to run the build scripts on build.eclipse.org
export JAVA_HOME=/opt/public/dsdp/JDKs/ibm-java2-ppc-50
export PATH=$JAVA_HOME/bin:$PATH

# define the current directory to be use as the 
buildDir=`pwd`

# *************************************************************************************************************************************************
# 													  Command line arguments definition
# *************************************************************************************************************************************************

#buildId - build name
buildId=""

# buildtype determines whether map file tags are used as entered or are replaced with HEAD
buildType=""

# Set the location where Eclipse Releng BaseBuilder is installed
basebuilder=$buildDir/org.eclipse.releng.basebuilder

# This reflects the svn version tag of the code to be used in the build.
revision=""
fetchTag=""

# The directory in which mtj packages will be placed after the build process if the -publish argument is set. 
outputDir="-DoutputDirectory=/home/data/httpd/download.eclipse.org/dsdp/mtj/downloads/drops/"

# Indicates if the mtj packages will be copied to the download server.
publish=""

updateSite=""

# The default directory in which mtj will post the update site files.
updateSiteDirectory="-DoutputDirectory=/home/data/httpd/download.eclipse.org/dsdp/mtj/updates/"

# The default pde build scripts
pdebuildscripts="-Dpde.build.scripts=$basebuilder/plugins/org.eclipse.pde.build/scripts"

# Flag used to indicate if the notification email must be sent after the build process.
notify=""

# Flag used to indicate if the packages must be signed.
sign=""

# Flag used to indicate that metadata soulhd be generated
metadata=""

# Flag used to indicate if the automatic test must be run.
runTests=""

# The component to be built. The supported componente are : runtime, sdk, doc, test, examples.
component=""

# Define supported command line arguments
usage="usage: $0 [-buildId name] [-revision tag] [-publish [-outputDirectory directory]] [-updateSite [-updateSiteDirectory directory]] [-notify] 
                 [-sign] [-runTests] [-component runtime|sdk|doc|test|examples] M|N|I|S|R"

# Get the build date and time
builddate=`date +%Y%m%d`
buildtime=`date +%H%M` 

# *************************************************************************************************************************************************
# 													  Command line arguments processing
# *************************************************************************************************************************************************

# Process command line arguments. At least one argument must be informed
if [ $# -lt 1 ]; then
	echo -e "$usage" 
	exit 1
fi

fail=0

while [ $# -gt 0 ]; do
	case "$1" in 
		-revision) 
			revision="$2";
			shift;;
		-buildId) 
			buildId="$2"; 
			shift;;
		-outputDirectory) 
			outputDir="-DoutputDirectory=$2"; 
			shift;;
		-component)
			component="$2";
			shift;;
		-publish)
			publish="-Dpublish=true";;
		-updateSite)
			updateSite="-DupdateSite=true";;
		-updateSiteDirectory) 
			updateSiteDirectory="-DupdateSiteDirectory=$2"; 
			shift;;	
		-notify)
			notify="-Dnotify=true";;
		-metadata)
            pdebuildscripts="-Dpde.build.scripts=$basebuilder/plugins/org.eclipse.pde.build_3.4.0.v20080522/scripts";            
            metadata="-Dmetadata=true";;
		-sign)
			sign="-Dsign=true"
		;;
		-runTests)
			runTests="-DrunTests=true";;
		-*) 
			fail=1;; 
		*) 
			break;;
	esac
	shift
done

# check for failures
if [ $fail -eq 1 ]; then
	echo -e "$usage"
	exit 1
fi

# After the above the build type is left in $1.
buildType=$1

#Check if the build type was informed
if [ "$buildType" = "" ]; then
	echo -e "The buildtype must be informed"
	echo -e "$usage"
	exit 1
else
	# Check if the build type is supported
	if [ $buildType = "M" ] || [ $buildType = "N" ] || [ $buildType = "I" ] || [ $buildType = "S" ] || [ $buildType = "R" ]; then

		#When the build is a release, the buildId must be informed
		if [ $buildType != "N" ]; then
			if [ "$buildId" = "" ]; then
				 echo "The buildId must be informed"
				 exit 1
			fi 
		else
			# The buildId is optional for nightly builds
			if [ "$buildId" = "" ]; then
				 buildId=$buildType$builddate
			fi 
		fi
		
	else
		echo -e "Build type not supported : $buildType"
		exit 1
	fi
fi

# Check if a component was informed. If so, check if it is valid.
if [ "$component" != "" ]; then
	if [ $component != "runtime" ] && [ $component != "sdk" ] && [ $component != "doc" ] && [ $component != "test" ] && [ $component != "examples" ]; then
		echo -e "Component not supported : $component"
		echo -e "$usage"
		exit 1
	else
		component="-Dcomponent="$component
	fi
fi

# Check the revision and set the fechTag
if [ "$revision" != "" ]; then
	if [ $revision = "HEAD" ];then
		fetchTag="-DfetchTag=SVN=trunk"
	else
		fetchTag="-DfetchTag=SVN=tags/$revision"
	fi
	revision="-Drevision"$revision
else
	revision="-Drevision=HEAD"
	fetchTag="-DfetchTag=SVN=trunk"
fi

# *************************************************************************************************************************************************
# 														Setup eclipse build
# *************************************************************************************************************************************************

# jvm used to run the build.  Defaults to java on system path
jvm=java

echo ...using releng basebuilder: $basebuilder

# * Identify the Eclipse Equinox launcher to use
launcher=$basebuilder/plugins/org.eclipse.equinox.launcher.jar

echo ...using Eclipse launcher: $launcher

# *************************************************************************************************************************************************
# 														 Set jvm properties
# *************************************************************************************************************************************************

# Set the jvm properties

jvmProperties=-DbuildType=$buildType
jvmProperties="$jvmProperties "-DbuildId=$buildId
jvmProperties="$jvmProperties "$component
jvmProperties="$jvmProperties "$outputDir
jvmProperties="$jvmProperties "$publish
jvmProperties="$jvmProperties "$updateSite
jvmProperties="$jvmProperties "$updateSiteDirectory
jvmProperties="$jvmProperties "$notify
jvmProperties="$jvmProperties "$sign
jvmProperties="$jvmProperties "$metadata
jvmProperties="$jvmProperties "$runTests
jvmProperties="$jvmProperties "$revision
jvmProperties="$jvmProperties "$fetchTag
jvmProperties="$jvmProperties "$pdebuildscripts
jvmProperties="$jvmProperties "-Declipse.build.tools=$basebuilder/plugins/org.eclipse.build.tools/scripts
jvmProperties="$jvmProperties "-DbaseLocation=base/eclipse
jvmProperties="$jvmProperties "-Doptimize
jvmProperties="$jvmProperties "-DforceContextQualifier=v$builddate$buildtime

# *************************************************************************************************************************************************
# 																Build
# *************************************************************************************************************************************************

# Set the build script full path
antScript=$buildDir/org.eclipse.mtj.releng/build.xml

# Full command with args
buildCommand="$jvm $jvmProperties -cp $launcher org.eclipse.core.launcher.Main -application org.eclipse.ant.core.antRunner -f $antScript"

# Capture command used to run the build
echo -e "$buildCommand" 1>buildCommand_$buildId.txt

# Try to remove old src/eclipse/ folder
rm -Rf $buildDir/org.eclipse.mtj.releng/src 2>/dev/null
# Try to remove old src/eclipse/ folder
rm -Rf $buildDir/org.eclipse.mtj.releng/*.log 2>/dev/null
$buildCommand
retCode=$?
if [ $retCode != 0 ]
then
 echo "Build failed (error code $retCode)."
fi