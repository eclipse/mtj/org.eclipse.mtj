/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Automatically query devices after directory 
 *                                selection
 *     Diego Sandin (Motorola)  - Added preprocessing entries generation from 
 *                                device properties
 *     Feng Wang (Sybase)       - Add button enable/disable logic.
 *     Hugo Raniere (Motorola)  - Add warning if devices have no preverifier
 *     Hugo Raniere (Motorola)  - Verifying if no default preverifier was set. 
 *     Feng Wang (Sybase)       - Remove preprocessing entries generation 
 *                                feature, since multi-configuration support 
 *                                covers this feature
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874] 
 */
package org.eclipse.mtj.internal.ui.devices;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceFinder;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.sdk.device.DeviceFinder;
import org.eclipse.mtj.internal.core.sdk.device.IFoundDevicesList;
import org.eclipse.mtj.internal.core.sdk.device.SimpleFoundDevicesList;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.internal.ui.viewers.TableViewerConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;

/**
 * Provides the primary functionality of the device import wizard. This page
 * strives to look and act very similar to the Eclipse Import Projects wizard.
 * 
 * @author Craig Setera
 */
public class DeviceImportWizardPage extends WizardPage {

    /**
     *
     */
    private class DirectoryFocusListener implements FocusListener,
            TraverseListener {

        /* (non-Javadoc)
         * @see org.eclipse.swt.events.FocusListener#focusGained(org.eclipse.swt.events.FocusEvent)
         */
        public void focusGained(FocusEvent e) {
        }

        /* (non-Javadoc)
         * @see org.eclipse.swt.events.FocusListener#focusLost(org.eclipse.swt.events.FocusEvent)
         */
        public void focusLost(FocusEvent e) {
            updateDevices(false);
        }

        /* (non-Javadoc)
         * @see org.eclipse.swt.events.TraverseListener#keyTraversed(org.eclipse.swt.events.TraverseEvent)
         */
        public void keyTraversed(TraverseEvent e) {
            if (e.detail == SWT.TRAVERSE_RETURN) {
                e.doit = false;
                updateDevices(false);
            }
        }
    }

    /**
     * Implementation of the table's content provider.
     */
    private static class TableContentProvider implements
            IStructuredContentProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            return (inputElement == null) ? NO_ELEMENTS
                    : ((IFoundDevicesList) inputElement).getDevices().toArray();
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    /**
     * A found devices list that will update the viewer as new devices are found
     */
    private class UpdatingFoundDevicesList extends SimpleFoundDevicesList {

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.core.sdk.device.SimpleFoundDevicesList#addDevices(java.util.List)
         */
        @Override
        public void addDevices(List<IDevice> devices) {
            super.addDevices(devices);
            refreshViewer();
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.model.device.SimpleFoundDevicesList#clear()
         */
        @Override
        public void clear() {
            super.clear();
            refreshViewer();
        }

        /**
         * Refresh the viewer contents.
         */
        private void refreshViewer() {
            final IStatus[] status = validateDevices();
            getContainer().getShell().getDisplay().asyncExec(new Runnable() {
                public void run() {
                    deviceViewer.refresh();
                    updateStatusMessage(status);
                }
            });
        }

        /**
         * Verifies if the devices found are valid.
         * 
         * @return an array of IStatus objects representing the problems found
         *         on the devices. Null if all devices are ok.
         */
        private IStatus[] validateDevices() {
            for (IDevice device : deviceList.getDevices()) {
                if ((((IMIDPDevice) device).getPreverifier() == null)
                        && (MTJCore.getDeviceRegistry().getDefaultPreferifier() == null)) {
                    return new Status[] { new Status(
                            IStatus.WARNING,
                            IMTJUIConstants.PLUGIN_ID,
                            MTJUIMessages.DeviceImportWizardPage_InvalidPreverifierMessage) };
                }
            }
            return null;
        }
    }

    public static final String NAME = "deviceImportPage"; //$NON-NLS-1$

    private static final TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
            new TableColumnInfo(
                    MTJUIMessages.DeviceImportWizardPage_import_columnInfo,
                    10f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceImportWizardPage_group_columnInfo,
                    22.5f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceImportWizardPage_name_columnInfo,
                    22.5f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceImportWizardPage_canfiguration_columnInfo,
                    22.5f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceImportWizardPage_profile_columnInfo,
                    22.5f, null), };

    // Column information structure
    private static final int DEFAULT_TABLE_WIDTH = 550;
    private static final String KEY_HEIGHT = "dialogHeight"; //$NON-NLS-1$

    private static final String KEY_WIDTH = "dialogWidth"; //$NON-NLS-1$

    private static final Object[] NO_ELEMENTS = new Object[0];

    private Button browseButton;
    private Button deselectAllButton;
    private UpdatingFoundDevicesList deviceList;
    private CheckboxTableViewer deviceViewer;
    private String lastRootText;
    private Button refreshButton;
    private Text rootText;
    private Button selectAllButton;

    private IDeviceFinder deviceFinder = DeviceFinder.getInstance();

    /**
     * Construct a new page instance.
     */
    public DeviceImportWizardPage() {

        super(NAME, MTJUIMessages.DeviceImportWizardPage_title, null);
        setDescription(MTJUIMessages.DeviceImportWizardPage_SelectDirectoryMessage);
        setImageDescriptor(MTJUIPluginImages.DESC_IMPORT_DEV);

        setPageComplete(false);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        // Set up the parent container
        parent.setLayoutData(new GridData(GridData.FILL_BOTH));

        final Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(2, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
        composite.addControlListener(new ControlAdapter() {
            @Override
            public void controlResized(ControlEvent e) {
                storeSize();
            }
        });
        setControl(composite);

        // Create the controls
        addRootDirectoryControls(composite);
        addDeviceSelectorControls(composite);

        // Set the size if previously stored away
        Point size = retrieveSize();
        if (size != null) {
            composite.setSize(size);
        }
    }

    /**
     * Return the devices that were selected by the user.
     * 
     * @return
     */
    public IDevice[] getSelectedDevices() {
        Object[] checkedElements = deviceViewer.getCheckedElements();
        IDevice[] devices = new IDevice[checkedElements.length];
        System
                .arraycopy(checkedElements, 0, devices, 0,
                        checkedElements.length);

        return devices;
    }

    /**
     * Set the roots to be searched.
     * 
     * @param searchRootsList The searchRootsList to set.
     */
    public void searchRoots(String[] searchRootsList) {
        // Disable the controls that should not be used in this case
        rootText.setEnabled(false);
        browseButton.setEnabled(false);

        ArrayList<File> files = new ArrayList<File>();
        for (String element : searchRootsList) {
            File file = new File(element);
            if (file.exists()) {
                files.add(file);
            }
        }

        File[] searchFiles = files.toArray(new File[files.size()]);
        updateDevices(searchFiles);
    }

    /**
     * Add the controls that allow the user to select the device from the
     * devices found during the import.
     * 
     * @param parent
     */
    private void addDeviceSelectorControls(Composite parent) {
        Label devicesLabel = new Label(parent, SWT.NONE);
        devicesLabel
                .setText(MTJUIMessages.DeviceImportWizardPage_deviceSelector_label_text);
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 2;
        devicesLabel.setLayoutData(gd);

        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.minimumWidth = DEFAULT_TABLE_WIDTH;
        deviceViewer = createTableViewer(parent);
        deviceViewer.getTable().setLayoutData(gridData);

        addDeviceTableButtons(parent);
    }

    /**
     * Add the device buttons.
     * 
     * @param parent
     */
    private void addDeviceTableButtons(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout(1, true);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        composite.setLayout(layout);

        selectAllButton = new Button(composite, SWT.PUSH);
        selectAllButton
                .setText(MTJUIMessages.DeviceImportWizardPage_selectAllButton_label_text);
        selectAllButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        selectAllButton.setEnabled(false);
        selectAllButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                deviceViewer.setAllChecked(true);
                // update the finish button
                updateFinishButton();
            }
        });

        deselectAllButton = new Button(composite, SWT.PUSH);
        deselectAllButton
                .setText(MTJUIMessages.DeviceImportWizardPage_deselectAllButton_label_text);
        deselectAllButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        deselectAllButton.setEnabled(false);
        deselectAllButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                deviceViewer.setAllChecked(false);
                // update the finish button
                updateFinishButton();
            }
        });

        refreshButton = new Button(composite, SWT.PUSH);
        refreshButton
                .setText(MTJUIMessages.DeviceImportWizardPage_refreshButton_label_text);
        refreshButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        refreshButton.setEnabled(false);
        refreshButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                updateDevices(true);
            }
        });
    }

    /**
     * Add the controls that allow the user to select the root directory from
     * which the import will occur.
     * 
     * @param parent
     */
    private void addRootDirectoryControls(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout(2, false);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        composite.setLayout(layout);
        composite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Label label = new Label(composite, SWT.NONE);
        label
                .setText(MTJUIMessages.DeviceImportWizardPage_search_directory_label_text);

        rootText = new Text(composite, SWT.BORDER);
        rootText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        DirectoryFocusListener listener = new DirectoryFocusListener();
        rootText.addFocusListener(listener);
        rootText.addTraverseListener(listener);
        rootText.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                updateRefreshButton();
            }

        });

        browseButton = new Button(parent, SWT.PUSH);
        browseButton
                .setText(MTJUIMessages.DeviceImportWizardPage_browseButton_label_text);
        browseButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleBrowseButton();
            }
        });
    }

    /**
     * Create the devices table viewer.
     * 
     * @param parent
     */
    private CheckboxTableViewer createTableViewer(Composite composite) {
        // Create the table
        int styles = SWT.SINGLE | SWT.V_SCROLL | SWT.BORDER
                | SWT.FULL_SELECTION | SWT.CHECK;
        Table table = new Table(composite, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));

        // Wire up the viewer
        final CheckboxTableViewer viewer = new CheckboxTableViewer(table);
        viewer.setContentProvider(new TableContentProvider());
        viewer.setLabelProvider(new DeviceTableLabelProvider());

        deviceList = new UpdatingFoundDevicesList();
        viewer.setInput(deviceList);
        // update the finish button
        viewer.addCheckStateListener(new ICheckStateListener() {
            public void checkStateChanged(CheckStateChangedEvent event) {
                updateFinishButton();
            }
        });

        IDialogSettings viewerSettings = MTJUIPlugin.getDialogSettings(
                getDialogSettings(), "viewerSettings"); //$NON-NLS-1$
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 1);
        viewerConfiguration.configure(viewer);

        return viewer;
    }

    /**
     * Return a runnable that can be used to run and update the devices in the
     * device viewer.
     * 
     * @param searchDirectories
     * @return
     */
    private IRunnableWithProgress getDeviceSearchRunnable(
            final File[] searchDirectories) {
        // The runnable to do the device search
        return new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor)
                    throws InvocationTargetException, InterruptedException {
                try {
                    getShell().getDisplay().syncExec(new Runnable() {
                        public void run() {
                            deviceList.clear();
                        }
                    });

                    for (File element : searchDirectories) {
                        deviceList.addDevices(deviceFinder.findDevices(
                                element, monitor));
                        
                    }

                    getShell().getDisplay().syncExec(new Runnable() {
                        public void run() {
                            deviceViewer.refresh();
                            deviceViewer.setAllChecked(true);
                        }
                    });
                } catch (CoreException e) {
                    throw new InvocationTargetException(e);
                }
            }
        };
    }

    /**
     * Handle the selection of the root directory browse button.
     */
    private void handleBrowseButton() {
        DirectoryDialog dialog = new DirectoryDialog(getShell());
        dialog
                .setMessage(MTJUIMessages.DeviceImportWizardPage_handleBrowseButton_message);

        String directory = dialog.open();
        if (directory != null) {
            rootText.setText(directory);
            updateDevices(true);
        }
    }

    /**
     * Handle an error during the device search.
     * 
     * @param t
     */
    private void handleDeviceSearchException(Throwable t) {
        MTJUIPlugin
                .displayError(
                        getShell(),
                        IStatus.ERROR,
                        -999,
                        MTJUIMessages.DeviceImportWizardPage_handleDeviceSearchException_title,
                        MTJUIMessages.DeviceImportWizardPage_handleDeviceSearchException_message,
                        t);

        MTJCore.log(IStatus.ERROR, t.getMessage(), t);
    }

    /**
     * Retrieve the previously stored size or <code>null</code> if not found.
     * 
     * @return
     */
    private Point retrieveSize() {
        Point size = null;

        IDialogSettings settings = getDialogSettings();
        if (settings.get(KEY_WIDTH) != null) {
            size = new Point(settings.getInt(KEY_WIDTH), settings
                    .getInt(KEY_HEIGHT));
        }

        return size;
    }

    /**
     * Store off the size of the control in the dialog settings.
     */
    private void storeSize() {
        IDialogSettings settings = getDialogSettings();
        Point size = getControl().getSize();
        settings.put(KEY_WIDTH, size.x);
        settings.put(KEY_HEIGHT, size.y);
    }

    /**
     * Update the devices table.
     */
    private void updateDevices(boolean force) {
        String newText = rootText.getText().trim();
        if (force || !newText.equals(lastRootText)) {
            lastRootText = newText;

            final File searchDirectory = new File(newText);
            if (searchDirectory.exists()) {
                updateDevices(searchDirectory);
            }
        }
        updateSelectButtons();
        updateFinishButton();
    }

    /**
     * Update the devices given the specified search directory.
     * 
     * @param searchDirectory
     */
    private void updateDevices(final File searchDirectory) {
        updateDevices(new File[] { searchDirectory });
    }

    /**
     * Update the devices given the specified search directories.
     * 
     * @param searchDirectories
     */
    private void updateDevices(final File[] searchDirectories) {
        // The runnable to do the device search
        IRunnableWithProgress runnable = getDeviceSearchRunnable(searchDirectories);

        try {
            getContainer().run(true, true, runnable);
        } catch (InvocationTargetException e) {
            handleDeviceSearchException(e.getCause());
        } catch (InterruptedException e) {
            // The user chose to bail out...
        }
    }

    /**
     * Update finish button on wizard dialog
     */
    private void updateFinishButton() {
        setPageComplete(deviceViewer.getCheckedElements().length > 0);
    }

    /**
     * Update the refreshButton
     */
    private void updateRefreshButton() {
        // update refreshButton
        String rootDir = rootText.getText().trim();
        final File searchDirectory = new File(rootDir);
        if (searchDirectory.exists()) {
            refreshButton.setEnabled(true);
        } else {
            refreshButton.setEnabled(false);
        }
    }

    /**
     * Update the selectAllButton & deselectAllButton
     */
    private void updateSelectButtons() {
        // update selectAllButton and deselectAllButton
        UpdatingFoundDevicesList deviceList = (UpdatingFoundDevicesList) deviceViewer
                .getInput();
        int deviceAmount = deviceList.getDevices().size();
        if (deviceAmount > 0) {
            selectAllButton.setEnabled(true);
            deselectAllButton.setEnabled(true);
        } else {
            selectAllButton.setEnabled(false);
            deselectAllButton.setEnabled(false);
        }
    }

    /**
     * Updates the device message of this screen with any error/warning message
     * necessary or the original description if there is no error/warning to
     * show.
     * 
     * @param status an array containing error/warning messages to show. Null if
     *            there is no error/warning
     */
    private void updateStatusMessage(IStatus[] status) {
        if ((status == null) || (status.length < 1)) {
            this.setMessage(this.getDescription());
        } else {
            this.setMessage(status[0].getMessage(), status[0].getSeverity());
        }
    }
}
