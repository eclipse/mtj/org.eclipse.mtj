/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Externalizing strings and some fixes.
 *     David Marques (Motorola) - Reseting password when keystore changes.
 *     David Marques (Motorola) - Implementing password changing and fixing
 *                                some bugs.
 *     David Marques (Motorola) - Fixing performDefaults and password button.
 */
package org.eclipse.mtj.internal.ui.preferences;

import java.io.File;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.build.sign.PreferencesSignatureProperties;
import org.eclipse.mtj.internal.core.build.sign.SignatureProperties;
import org.eclipse.mtj.internal.core.sign.DefaultKeyStoreManager;
import org.eclipse.mtj.internal.core.sign.IKeyStoreManager;
import org.eclipse.mtj.internal.core.sign.KeyStoreManagerException;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.dialog.SigningPasswordDialog;
import org.eclipse.mtj.internal.ui.forms.blocks.SigningBlock;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * SigningPreferencePage adds a new preference page for default
 * signing setup.
 * 
 * @author David Marques
 * @see 1.0
 */
public class SigningPreferencePage extends PreferencePage implements IWorkbenchPreferencePage {

	private PreferencesSignatureProperties sigProps;
	private SigningBlock signingBlock;
	private IKeyStoreManager keyStoreManager;
	private Text keystoreTxt;
	private Button externalBtn;
	private Button passwordBtn;
	private Button ksPasswordBtn1;
	private Text keystorePasswordTxt;
	private Button ksPasswordBtn2;
	
	/**
	 * Creates a new instance of SigningPreferencePage. 
	 */
	public SigningPreferencePage() {
		try {			
			this.sigProps = new PreferencesSignatureProperties();
		} catch (CoreException e) {
			setErrorMessage(e.getMessage());
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
	 */
	protected Control createContents(Composite parent) {
		parent.setLayout(new GridLayout(0x01, true));
		
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(MTJUIPlugin.buildGridData(SWT.LEFT, SWT.CENTER,
				true, true));
		GridLayout layout = new GridLayout(0x01, false);
		layout.marginWidth  = 0x00;
		layout.marginHeight = 0x00;
		composite.setLayout(layout);
		
		createKeyStoreSection(composite);
		signingBlock = new SigningBlock();
		signingBlock.createBlock(composite);
		signingBlock.setEnbaled(false);
		
		this.loadSigningData();
		return composite;
	}
	
	/**
	 * Loads the signing data into the Ui.
	 */
	private void loadSigningData() {
		this.keystoreTxt.setText(convertNullToEmpty(sigProps
				.getKeyStoreDisplayPath()));

		this.keystorePasswordTxt.setText(convertNullToEmpty(sigProps
				.getKeyStorePassword()));
		
		int passwdType = this.sigProps.getPasswordStorageMethod();
		switch (passwdType) {
			case SignatureProperties.PASSMETHOD_IN_KEYRING:
				this.ksPasswordBtn1.setSelection(false);
				this.ksPasswordBtn2.setSelection(true);
				this.keystorePasswordTxt.setEnabled(true);
			break;
			case SignatureProperties.PASSMETHOD_PROMPT:
				this.ksPasswordBtn2.setSelection(false);
				this.ksPasswordBtn1.setSelection(true);
				this.keystorePasswordTxt.setEnabled(false);
			break;
		}
		
		this.signingBlock.setProvider(sigProps.getKeyStoreProvider());
		this.signingBlock.setKeystoreType(sigProps.getKeyStoreType());
		this.loadAliases();
	}
	
	/**
	 * Utility routine to convert a null value to an empty string when loading
	 * Text widgets.
	 * 
	 * @param s
	 * @return
	 */
	private String convertNullToEmpty(String s) {
		if (s == null) {
			return (""); //$NON-NLS-1$
		}

		return (s);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.PreferencePage#performOk()
	 */
	public boolean performOk() {
		if (sigProps.getPasswordStorageMethod() != SignatureProperties.PASSMETHOD_PROMPT) {
			sigProps.setKeyStorePassword(this.keystorePasswordTxt.getText());
		}
		sigProps.setKeyStoreProvider(this.signingBlock.getProvider());
		sigProps.setKeyStoreType(this.signingBlock.getKeystoreType());
		return this.sigProps.save(); // Saves properties on preferences
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
	 */
	protected void performDefaults() {
		super.performDefaults();
		signingBlock.setEnbaled(false);
		signingBlock.setInput(null);
		passwordBtn.setEnabled(false);
		sigProps.clear();
		sigProps.save();
		try {			
			this.sigProps = new PreferencesSignatureProperties();
			this.loadSigningData();
		} catch (CoreException e) {
			setErrorMessage(e.getMessage());
		}
	}
	
	/**
	 * Creates a section for keystore setup.
	 * 
	 * @param parent parent composite.
	 */
	private void createKeyStoreSection(Composite parent) {
		Group ksGroup = new Group(parent, SWT.NONE);
		ksGroup.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL, SWT.FILL,
				true, false));
		GridLayout layout = new GridLayout(0x04, false);
		layout.verticalSpacing = 0x00;
		ksGroup.setLayout(layout);
		ksGroup.setText(MTJUIMessages.SigningPreferencePage_keyStore);

		Label location = new Label(ksGroup, SWT.NONE);
		location.setText(MTJUIMessages.SigningPreferencePage_location);
		GridData gridData = MTJUIPlugin.buildGridData(SWT.FILL, SWT.CENTER,
				true, false);
		gridData.horizontalSpan = 4;
		location.setLayoutData(gridData);

		keystoreTxt = new Text(ksGroup, SWT.BORDER);
		keystoreTxt.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL,
				SWT.CENTER, true, false));

		externalBtn = new Button(ksGroup, SWT.NONE);
		externalBtn.setText(MTJUIMessages.SigningPreferencePage_external);
		externalBtn.setLayoutData(MTJUIPlugin.buildGridData(false, false));
		externalBtn.addMouseListener(new MouseListener() {

			public void mouseDoubleClick(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
			}

			public void mouseUp(MouseEvent e) {
				browseExternalForKeystore();
			}

		});

		passwordBtn = new Button(ksGroup, SWT.NONE);
		passwordBtn.setText(MTJUIMessages.SigningPreferencePage_changePassword);
		passwordBtn.setLayoutData(MTJUIPlugin.buildGridData(false, false));
		passwordBtn.setEnabled(false);
		passwordBtn.addMouseListener(new MouseListener(){

			public void mouseUp(MouseEvent e) {
				if (keyStoreManager == null) {
					return;
				}
				
				SigningPasswordDialog dialog = new SigningPasswordDialog(getShell(), false);
				dialog.setTitle("Change Keystore Password");
				dialog.setDescription("Enter the new keystore password");
				if (dialog.open() != Dialog.OK) {
					return;
				}
				
				String kstPassword = dialog.getPassword();
				try {
					keyStoreManager.changeKeystorePassword(kstPassword);
					sigProps.setKeyStorePassword(kstPassword);
					sigProps.save();
					if (sigProps.getPasswordStorageMethod() != SignatureProperties.PASSMETHOD_PROMPT) {
						keystorePasswordTxt.setText(sigProps.getKeyStorePassword());
					}
				} catch (KeyStoreManagerException exc) {
					MessageDialog.openError(getShell(), "Keystore Manager Error", exc.getMessage());
					MTJCore.log(IStatus.ERROR, exc.getMessage());
				}
			}
			
			public void mouseDoubleClick(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
			}
		});
		
		Composite passwordGroup = new Composite(ksGroup, SWT.NONE);
		gridData = MTJUIPlugin.buildGridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 4;
		passwordGroup.setLayoutData(gridData);
		passwordGroup.setLayout(new GridLayout(3, true));

		ksPasswordBtn1 = new Button(passwordGroup, SWT.RADIO);
		ksPasswordBtn1.setText(MTJUIMessages.SigningPreferencePage_promptPassword);
		ksPasswordBtn1.setLayoutData(MTJUIPlugin.buildGridData(false, false));
		ksPasswordBtn1.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent e) {
				
			}

			public void widgetSelected(SelectionEvent e) {
				keystorePasswordTxt.setEnabled(!ksPasswordBtn1.getSelection());
				keystorePasswordTxt.setText(Utils.EMPTY_STRING);
				sigProps.setPasswordStorageMethod(SignatureProperties.PASSMETHOD_PROMPT);
				sigProps.setKeyStorePassword(Utils.EMPTY_STRING);
			}
		});
		
		ksPasswordBtn2 = new Button(passwordGroup, SWT.RADIO);
		ksPasswordBtn2.setText(MTJUIMessages.SigningPreferencePage_savePasswordInWorkspace);
		ksPasswordBtn2.setLayoutData(MTJUIPlugin.buildGridData(false, false));
		ksPasswordBtn2.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent e) {
				
			}

			public void widgetSelected(SelectionEvent e) {
				keystorePasswordTxt.setEnabled(ksPasswordBtn2.getSelection());
				sigProps.setPasswordStorageMethod(SignatureProperties.PASSMETHOD_IN_KEYRING);
			}
		});
		
		Composite ksPasswordGroup = new Composite(ksGroup, SWT.NONE);
		gridData = MTJUIPlugin.buildGridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 4;
		ksPasswordGroup.setLayoutData(gridData);
		ksPasswordGroup.setLayout(new GridLayout(2, false));

		Label kstPasswordLabel = new Label(ksPasswordGroup, SWT.NONE);
		kstPasswordLabel.setText(MTJUIMessages.SigningPreferencePage_keystorePassword);
		kstPasswordLabel.setLayoutData(MTJUIPlugin.buildGridData(false, false));

		keystorePasswordTxt = new Text(ksPasswordGroup, SWT.BORDER);
		keystorePasswordTxt.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL,
				SWT.CENTER, true, false));
	}
	
	/**
	 * Loads the aliases into the Ui.
	 */
	private void loadAliases() {
		String path = this.sigProps.getKeyStoreDisplayPath();
		if (path == null) {
			return;
		}
		
		File keyStoreFile = new File(path);
		if (!keyStoreFile.exists() || !keyStoreFile.isFile()) {
			return;
		}
		
		if (this.sigProps.getPasswordStorageMethod() != SignatureProperties.PASSMETHOD_PROMPT) {
			String kstPassword = this.keystorePasswordTxt.getText();
			keyStoreManager = new DefaultKeyStoreManager(keyStoreFile, kstPassword);
			keyStoreManager.setProvider(this.signingBlock.getProvider());
			keyStoreManager.setKeystoreType(this.signingBlock.getKeystoreType());
			this.signingBlock.setInput(keyStoreManager);
			this.signingBlock.setEnbaled(true);
			this.passwordBtn.setEnabled(true);
			String alias = sigProps.getKeyAlias();
			if (alias != null) {				
				this.signingBlock.setCurrentAlias(alias);
			}
		} else {
			openKeyStorePasswordDialog(keyStoreFile);
		}
	}
	
	/**
	 * Opens the keystore password dialog.
	 * 
	 * @param keyStoreFile the source file.
	 */
	private void openKeyStorePasswordDialog(final File keyStoreFile) {
		Display.getDefault().asyncExec(new Runnable(){
			public void run() {				
				SigningPasswordDialog dialog = new SigningPasswordDialog(getShell(), true);
				dialog.setTitle(MTJUIMessages.SigningPreferencePage_enterPassword);
				dialog.setDescription(MTJUIMessages.SigningPreferencePage_dialogTitle);
				
				if (dialog.open() != Dialog.OK) {
					return;
				}
				
				String kstPassword = dialog.getPassword();
				sigProps.setPasswordStorageMethod(SignatureProperties.PASSMETHOD_PROMPT);
				if (dialog.isSavingInWorkspace()) {
					sigProps.setPasswordStorageMethod(SignatureProperties.PASSMETHOD_IN_KEYRING);
					keystorePasswordTxt.setText(kstPassword);
					keystorePasswordTxt.setEnabled(true);
					ksPasswordBtn2.setSelection(true);
					ksPasswordBtn1.setSelection(false);
				}
				
				keyStoreManager = new DefaultKeyStoreManager(keyStoreFile, kstPassword);
				keyStoreManager.setProvider(signingBlock.getProvider());
				keyStoreManager.setKeystoreType(signingBlock.getKeystoreType());
				signingBlock.setInput(keyStoreManager);
				signingBlock.setEnbaled(true);
				passwordBtn.setEnabled(true);
			}
		});
	}
	
	/**
	 * This routine is called in response to the "Browse" button to allow the
	 * user to choose a keystore file using a File Open dialog.
	 */
	private void browseExternalForKeystore() {
		FileDialog dlg = new FileDialog(getShell(), SWT.OPEN);

		dlg
				.setText(MTJUIMessages.J2MESigningPropertiesPage_browse_dialog_title);
		dlg.setFilterNames(IMTJUIConstants.BROWSE_FILTER_NAMES);
		dlg.setFilterExtensions(IMTJUIConstants.BROWSE_FILTER_EXTENSIONS);

		String path = dlg.open();
		if (path == null) {
			return;
		}

		this.resetPasswords();
		this.sigProps.setKeyStoreDisplayPath(path);
		keystoreTxt.setText(path);
		this.loadAliases();
	}

	/**
	 * Resets all Ui for passwords and the passwords properties
	 */
	private void resetPasswords() {
		keystorePasswordTxt.setEnabled(true);
		keystorePasswordTxt.setText(Utils.EMPTY_STRING);
		ksPasswordBtn1.setSelection(true);
		ksPasswordBtn2.setSelection(false);
		sigProps.setPasswordStorageMethod(SignatureProperties.PASSMETHOD_PROMPT);
		sigProps.setKeyStorePassword(Utils.EMPTY_STRING);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
		
	}
}
