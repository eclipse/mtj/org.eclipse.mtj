/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.internal.ui.launching;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.launching.ILaunchConstants;
import org.eclipse.mtj.internal.core.launching.midp.IMIDPLaunchConstants;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.devices.DeviceSelector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * Launch configuration tab for the selection of the emulated device and any
 * device parameters.
 * 
 * @author Craig Setera
 */
public class DeviceTab extends AbstractLaunchConfigurationTab {

    // Provides the security domains from a device input element
    private class SecurityDomainContentProvider implements
            IStructuredContentProvider {
        public void dispose() {
        }

        public Object[] getElements(Object inputElement) {
            IMIDPDevice device = (IMIDPDevice) inputElement;

            Object[] elements = device.getProtectionDomains();
            if (elements == null) {
                elements = NO_ELEMENTS;
            }

            return elements;
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    private static final Object[] NO_ELEMENTS = new Object[0];

    // The device selector allows the user to choose a device
    private DeviceSelector deviceSelector;

    // Widgets
    private Text extraParameters;

    // The Java project currently associated with this launch configuration
    private IJavaProject javaProject;
    private Button projectDeviceButton;
    private ComboViewer securityDomainViewer;
    private Button specificDeviceButton;

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.AbstractLaunchConfigurationTab#activated(org.eclipse.debug.core.ILaunchConfigurationWorkingCopy)
     */
    @Override
    public void activated(ILaunchConfigurationWorkingCopy workingCopy) {

        // If the java project has changed since the last time
        // the tab was activated, we want to make sure to
        // update the device information as well
        IJavaProject currentProject = getJavaProjectFrom(workingCopy);

        if ((currentProject != null) || (javaProject != null)) {
            boolean isDifferent = ((currentProject == null) && (javaProject != null))
                    || ((currentProject != null) && (javaProject == null))
                    || !currentProject.equals(javaProject);

            if (isDifferent) {
                javaProject = currentProject;
                initializeDeviceFrom(workingCopy);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.ILaunchConfigurationTab#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {

        Font font = parent.getFont();
        Composite comp = new Composite(parent, SWT.NONE);
        setControl(comp);

        GridLayout topLayout = new GridLayout();
        comp.setLayout(topLayout);

        // The device selection controls
        Group group = new Group(comp, SWT.TITLE);
        group.setText(MTJUIMessages.DeviceTab_device_group_label_text);
        group.setLayout(new GridLayout(2, false));
        group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        projectDeviceButton = new Button(group, SWT.RADIO);
        (new Label(group, SWT.NONE))
                .setText(MTJUIMessages.DeviceTab_projectDeviceButton_radio_label_text);

        specificDeviceButton = new Button(group, SWT.RADIO);
        (new Label(group, SWT.NONE))
                .setText(MTJUIMessages.DeviceTab_specificDeviceButton_radio_label_text);

        deviceSelector = new DeviceSelector();
        deviceSelector.createContents(group, false, true);

        // Security domain setup
        Label securityDomainLabel = new Label(comp, SWT.NONE);
        securityDomainLabel
                .setText(MTJUIMessages.DeviceTab_securityDomain_label_text);
        securityDomainLabel.setFont(font);

        securityDomainViewer = new ComboViewer(comp, SWT.DROP_DOWN
                | SWT.READ_ONLY);
        securityDomainViewer
                .setContentProvider(new SecurityDomainContentProvider());
        securityDomainViewer.getCombo().setFont(font);
        securityDomainViewer.getCombo().setLayoutData(
                new GridData(GridData.FILL_HORIZONTAL));
        securityDomainViewer.setLabelProvider(new LabelProvider() {
            @Override
            public String getText(Object element) {
                return (String) element;
            }
        });
        securityDomainViewer
                .addSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        updateLaunchConfigurationDialog();
                    }
                });

        Label extraParamsLabel = new Label(comp, SWT.NONE);
        extraParamsLabel
                .setText(MTJUIMessages.DeviceTab_extraParams_label_text);
        extraParamsLabel.setFont(font);

        extraParameters = new Text(comp, SWT.BORDER);
        extraParameters.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        extraParameters.setTextLimit(200);
        extraParameters.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                updateLaunchConfigurationDialog();
            }
        });

        // Set up some listeners to keep things correctly up to date.
        projectDeviceButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                deviceSelector.setEnabled(specificDeviceButton.getSelection());
                updateLaunchConfigurationDialog();
            }
        });

        specificDeviceButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                deviceSelector.setEnabled(specificDeviceButton.getSelection());
                updateLaunchConfigurationDialog();
            }
        });

        deviceSelector
                .setSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        securityDomainViewer.setInput(deviceSelector
                                .getSelectedDevice());
                        updateLaunchConfigurationDialog();
                    }
                });
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.AbstractLaunchConfigurationTab#getImage()
     */
    @Override
    public Image getImage() {
        return MTJUIPluginImages.DESC_CELLPHONE_OBJ.createImage();
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.ILaunchConfigurationTab#getName()
     */
    public String getName() {
        return MTJUIMessages.DeviceTab_name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.ILaunchConfigurationTab#initializeFrom(org.eclipse.debug.core.ILaunchConfiguration)
     */
    public void initializeFrom(ILaunchConfiguration launchConfig) {
        javaProject = getJavaProjectFrom(launchConfig);
        initializeDeviceFrom(launchConfig);

        securityDomainViewer.setInput(getSelectedDevice());

        String domain = getStringOrDefault(launchConfig,
                IMIDPLaunchConstants.SECURITY_DOMAIN,
                IMIDPLaunchConstants.NO_SECURITY_DOMAIN);
        securityDomainViewer
                .setSelection(new StructuredSelection(domain), true);

        String params = getStringOrDefault(launchConfig,
                ILaunchConstants.LAUNCH_PARAMS, ""); //$NON-NLS-1$
        extraParameters.setText(params);
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.AbstractLaunchConfigurationTab#isValid(org.eclipse.debug.core.ILaunchConfiguration)
     */
    @Override
    public boolean isValid(ILaunchConfiguration launchConfig) {
        setErrorMessage(null);
        setMessage(null);

        boolean valid = true;

        if (getSelectedDevice() == null) {
            setErrorMessage(MTJUIMessages.DeviceTab_error_no_device_selected);
            valid = false;
        }

        return valid;
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.ILaunchConfigurationTab#performApply(org.eclipse.debug.core.ILaunchConfigurationWorkingCopy)
     */
    public void performApply(ILaunchConfigurationWorkingCopy launchConfig) {
        launchConfig.setAttribute(ILaunchConstants.USE_PROJECT_DEVICE,
                projectDeviceButton.getSelection());

        if (specificDeviceButton.getSelection()) {
            IDevice device = deviceSelector.getSelectedDevice();

            String groupName = (device == null) ? null : device.getSDKName();
            String deviceName = (device == null) ? null : device.getName();

            launchConfig.setAttribute(ILaunchConstants.EMULATED_DEVICE_GROUP,
                    groupName);
            launchConfig.setAttribute(ILaunchConstants.EMULATED_DEVICE,
                    deviceName);
        }

        IStructuredSelection selection = (IStructuredSelection) securityDomainViewer
                .getSelection();
        launchConfig.setAttribute(IMIDPLaunchConstants.SECURITY_DOMAIN,
                (String) selection.getFirstElement());
        launchConfig.setAttribute(ILaunchConstants.LAUNCH_PARAMS,
                extraParameters.getText());
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.ILaunchConfigurationTab#setDefaults(org.eclipse.debug.core.ILaunchConfigurationWorkingCopy)
     */
    public void setDefaults(ILaunchConfigurationWorkingCopy launchConfig) {
        launchConfig.setAttribute(ILaunchConstants.USE_PROJECT_DEVICE, true);
        launchConfig.setAttribute(ILaunchConstants.EMULATED_DEVICE_GROUP, ""); //$NON-NLS-1$
        launchConfig.setAttribute(ILaunchConstants.EMULATED_DEVICE, ""); //$NON-NLS-1$
        launchConfig.setAttribute(IMIDPLaunchConstants.SECURITY_DOMAIN,
                IMIDPLaunchConstants.NO_SECURITY_DOMAIN);
        launchConfig.setAttribute(ILaunchConstants.LAUNCH_PARAMS, ""); //$NON-NLS-1$
    }

    /**
     * Return the java project currently specified by the launch configuration
     * or <code>null</code> if a java project could not be found.
     * 
     * @param launchConfig
     * @return
     */
    private IJavaProject getJavaProjectFrom(ILaunchConfiguration launchConfig) {
        IJavaProject configProject = null;

        String projectName = ""; //$NON-NLS-1$
        try {
            projectName = launchConfig.getAttribute(
                    IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, ""); //$NON-NLS-1$
        } catch (CoreException ce) {
            MTJCore.log(IStatus.WARNING,
                    MTJUIMessages.DeviceTab_error_updating_project_field, ce);
        }

        if (projectName.trim().length() > 1) {
            IProject project = ResourcesPlugin.getWorkspace().getRoot()
                    .getProject(projectName);

            if ((project != null) && project.exists() && project.isOpen()) {
                configProject = JavaCore.create(project);
            }
        }

        return configProject;
    }

    /**
     * Return the device attached to the specified project.
     * 
     * @return
     */
    private IDevice getProjectDevice() {
        IDevice device = null;

        if (javaProject != null) {
            IMTJProject suite = MidletSuiteFactory
                    .getMidletSuiteProject(javaProject);
            device = suite.getMTJRuntime().getActiveMTJRuntime().getDevice();
        }

        return device;
    }

    /**
     * Return the currently selected device, taking into account the location to
     * retrieve the device.
     * 
     * @return
     */
    private IMIDPDevice getSelectedDevice() {
        IMIDPDevice device = null;

        if (projectDeviceButton.getSelection()) {
            device = (IMIDPDevice) getProjectDevice();
        } else {
            device = (IMIDPDevice) deviceSelector.getSelectedDevice();
        }

        return device;
    }

    /**
     * Get a string attribute from the launch configuration or the specified
     * default value.
     * 
     * @param launchConfig
     * @param attributeName
     * @param defaultValue
     * @return
     */
    private String getStringOrDefault(ILaunchConfiguration launchConfig,
            String attributeName, String defaultValue) {
        String value = null;

        try {
            value = launchConfig.getAttribute(attributeName, defaultValue);
        } catch (CoreException e) {
            MTJCore.log(IStatus.WARNING, e);
            value = defaultValue;
        }

        return value;
    }

    /**
     * Initialize the device selection from the selected launch configuration.
     * 
     * @param launchConfig
     */
    private void initializeDeviceFrom(ILaunchConfiguration launchConfig) {
        boolean useProjectDevice = true;
        try {
            useProjectDevice = launchConfig.getAttribute(
                    ILaunchConstants.USE_PROJECT_DEVICE, true);
        } catch (CoreException e) {
            MTJCore
                    .log(
                            IStatus.WARNING,
                            MTJUIMessages.DeviceTab_error_initializePlatformDefinitionFrom,
                            e);
        }
        projectDeviceButton.setSelection(useProjectDevice);
        deviceSelector.setEnabled(!useProjectDevice);
        specificDeviceButton.setSelection(!useProjectDevice);

        IDevice device = null;

        if (useProjectDevice) {
            device = getProjectDevice();
        } else {
            String groupName = getStringOrDefault(launchConfig,
                    ILaunchConstants.EMULATED_DEVICE_GROUP, ""); //$NON-NLS-1$
            String deviceName = getStringOrDefault(launchConfig,
                    ILaunchConstants.EMULATED_DEVICE, ""); //$NON-NLS-1$

            try {
                device = MTJCore.getDeviceRegistry().getDevice(groupName,
                        deviceName);
            } catch (PersistenceException e) {
                MTJCore.log(IStatus.WARNING,
                        MTJUIMessages.DeviceTab_error_retrieving_devices, e);
            }
        }

        deviceSelector.setSelectedDevice(device);
    }
}
