/**
 * Copyright (c) 2007,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

/**
 * A field editor capable of selecting and remembering either a workspace-based
 * file or file system file.
 * 
 * @author Craig Setera
 */
public class WorkspaceAndExternalFileFieldEditor extends FieldEditor {

    private static final IStatus ERROR_STATUS = MTJCore
            .newStatus(
                    IStatus.ERROR,
                    1,
                    MTJUIMessages.WorkspaceAndExternalFileFieldEditor_error_status_message);

    private static final IStatus OK_STATUS = MTJCore.newStatus(
            IStatus.OK, 0, ""); //$NON-NLS-1$

    private Button browseExternalButton;
    private Button browseWorkspaceButton;
    private Text fileText;
    private String[] filterExtensions;
    private Group group;

    /**
     * Creates a new WorkspaceAndExternalFileFieldEditor field editor.
     * 
     * @param name the name of the preference this field editor works on
     * @param labelText the label text of the field editor
     * @param filterExtensions the file extension filter
     * @param parent the parent of the field editor's control
     */
    public WorkspaceAndExternalFileFieldEditor(String name, String labelText,
            String[] filterExtensions, Composite parent) {
        super(name, labelText, parent);
        this.filterExtensions = filterExtensions;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditor#getNumberOfControls()
     */
    @Override
    public int getNumberOfControls() {
        return 3;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditor#adjustForNumColumns(int)
     */
    @Override
    protected void adjustForNumColumns(int numColumns) {
        ((GridData) group.getLayoutData()).horizontalSpan = numColumns;
    }

    /**
     * Browse the external file system for a file.
     */
    protected void browseExternal() {
        FileDialog dialog = new FileDialog(fileText.getShell());
        dialog
                .setText(MTJUIMessages.WorkspaceAndExternalFileFieldEditor_browseExternal_dialog_text);
        dialog.setFilterExtensions(filterExtensions);

        String filename = dialog.open();
        if (filename != null) {
            fileText.setText(filename);
        }
    }

    /**
     * Browse the workspace for a file.
     */
    protected void browseWorkspace() {
        ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
                fileText.getShell(), new WorkbenchLabelProvider(),
                new WorkbenchContentProvider());

        ISelectionStatusValidator validator = new ISelectionStatusValidator() {
            public IStatus validate(Object[] selection) {
                IStatus status = ERROR_STATUS;

                if (selection.length == 1) {
                    if (selection[0] instanceof IFile) {
                        IFile file = (IFile) selection[0];

                        if (file.getFullPath().getFileExtension().equals("xml")) { //$NON-NLS-1$
                            status = OK_STATUS;
                        }
                    }
                }
                return status;
            }
        };

        dialog
                .setTitle(MTJUIMessages.WorkspaceAndExternalFileFieldEditor_browseWorkspace_dialog_text);
        dialog
                .setMessage(MTJUIMessages.WorkspaceAndExternalFileFieldEditor_browseWorkspace_dialog_message);
        dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
        dialog.setAllowMultiple(false);
        dialog
                .setEmptyListMessage(MTJUIMessages.WorkspaceAndExternalFileFieldEditor_browseWorkspace_dialog_emptyListMessage);
        dialog.setStatusLineAboveButtons(true);
        dialog.setValidator(validator);

        if (dialog.open() == Window.OK) {
            Object[] result = dialog.getResult();

            IFile theFile = (IFile) result[0];
            IPath fullPath = theFile.getFullPath();

            fileText.setText(fullPath.toString());
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditor#doFillIntoGrid(org.eclipse.swt.widgets.Composite, int)
     */
    @Override
    protected void doFillIntoGrid(Composite parent, int numColumns) {
        group = new Group(parent, SWT.NONE);
        group.setText(getLabelText());
        group.setLayout(new GridLayout(3, false));

        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = numColumns;
        group.setLayoutData(gd);

        fileText = new Text(group, SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.minimumWidth = 350;
        fileText.setLayoutData(gd);

        browseWorkspaceButton = new Button(group, SWT.PUSH);
        browseWorkspaceButton
                .setText(MTJUIMessages.WorkspaceAndExternalFileFieldEditor_browseWorkspaceButton_text);
        browseWorkspaceButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                browseWorkspace();
            }
        });

        browseExternalButton = new Button(group, SWT.PUSH);
        browseExternalButton
                .setText(MTJUIMessages.WorkspaceAndExternalFileFieldEditor_browseExternalButton_text);
        browseExternalButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                browseExternal();
            }
        });
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditor#doLoad()
     */
    @Override
    protected void doLoad() {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditor#doLoadDefault()
     */
    @Override
    protected void doLoadDefault() {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditor#doStore()
     */
    @Override
    protected void doStore() {
    }
}
