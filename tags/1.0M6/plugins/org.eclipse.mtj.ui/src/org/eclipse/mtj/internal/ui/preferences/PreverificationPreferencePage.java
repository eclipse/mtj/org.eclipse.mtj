/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Remove the built in preverifier enabling option
 *     Hugo Raniere (Motorola)  - Including field to set default preverifier
 *     Hugo Raniere (Motorola)  - Storing default preverifier in DeviceRegistry &
 *     				  correcting "Restore Defaults" behavior
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     Gustavo de Paula (Motorola)  - Preverifier api refactoring
 */
package org.eclipse.mtj.internal.ui.preferences;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.sdk.device.midp.library.ILibrarySpecification;
import org.eclipse.mtj.internal.core.sdk.device.midp.library.Configuration;
import org.eclipse.mtj.internal.ui.IEmbeddableWorkbenchPreferencePage;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

/**
 * Preference page implementation for setting preverification preferences.
 * 
 * @author Craig Setera
 */
public class PreverificationPreferencePage extends PreferencePage implements
        IEmbeddableWorkbenchPreferencePage {

    private static final String[] BUTTON_TEXTS = new String[] {
            MTJUIMessages.PreverificationPreferencePage_use_jad_settings_label_text,
            MTJUIMessages.PreverificationPreferencePage_use_project_settings_label_text,
            MTJUIMessages.PreverificationPreferencePage_use_specific_config_label_text, };

    private static final String[] CONFIG_FILE_LOCATIONS = new String[] {
            IMTJCoreConstants.PREF_PREVERIFY_CONFIG_LOCATION_JAD,
            IMTJCoreConstants.PREF_PREVERIFY_CONFIG_LOCATION_PLATFORM,
            IMTJCoreConstants.PREF_PREVERIFY_CONFIG_LOCATION_SPECIFIED, };

    private Combo configCombo;
    private ILibrarySpecification[] configSpecs;
    private FileFieldEditor defaultPreverifierField;
    private boolean embeddedInProperties;

    private Button[] preverificationRadios;

    /**
     * Default constructor.
     */
    public PreverificationPreferencePage() {
        this(false, MTJUIPlugin.getDefault().getCorePreferenceStore());
    }

    /**
     * Constructor for use when embedding the preference page within a
     * properties page.
     * 
     * @param embeddedInProperties
     * @param preferenceStore a table mapping named preferences to values
     */
    public PreverificationPreferencePage(boolean embeddedInProperties,
            IPreferenceStore preferenceStore) {

        this.embeddedInProperties = embeddedInProperties;
        setPreferenceStore(preferenceStore);
        configSpecs = Configuration.values();
    }

    /**
     * Launch the build of the MIDlet suites.
     * 
     * @param toBuild
     */
    public void doBuild(final IProject[] toBuild) {
        // The work to be done.
        IRunnableWithProgress runnable = new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor)
                    throws InvocationTargetException {
                try {
                    for (int i = 0; i < toBuild.length; i++) {
                        SubProgressMonitor subMonitor = new SubProgressMonitor(
                                monitor, IProgressMonitor.UNKNOWN);
                        toBuild[i].build(IncrementalProjectBuilder.FULL_BUILD,
                                subMonitor);
                        subMonitor.done();
                    }
                } catch (CoreException e) {
                    throw new InvocationTargetException(e);
                }
            }
        };
        try {
            PlatformUI.getWorkbench().getProgressService().busyCursorWhile(
                    runnable);
        } catch (InterruptedException e) {
        } catch (InvocationTargetException e) {
            MTJCore
                    .log(
                            IStatus.WARNING,
                            MTJUIMessages.PreverificationPreferencePage_error_build_suite,
                            e.getCause());
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performApply()
     */
    @Override
    public void performApply() {
        super.performApply();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
     */
    @Override
    public void performDefaults() {
        setControlsFromDefaults();
        super.performDefaults();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        setPreferencesFromControls();
        if (!embeddedInProperties) {
            buildSuites();
        }

        return super.performOk();
    }

    /**
     * Add the controls for choosing the preverification configuration.
     * 
     * @param composite
     */
    private void addConfigurationControls(Composite composite) {
        Group preverifyConfigGroup = new Group(composite, SWT.NONE);
        preverifyConfigGroup
                .setText(MTJUIMessages.PreverificationPreferencePage_preverifyConfigGroup_label_text);
        preverifyConfigGroup.setLayout(new GridLayout(1, false));
        preverifyConfigGroup.setLayoutData(new GridData(
                GridData.FILL_HORIZONTAL));

        preverificationRadios = new Button[BUTTON_TEXTS.length];
        for (int i = 0; i < BUTTON_TEXTS.length; i++) {
            preverificationRadios[i] = new Button(preverifyConfigGroup,
                    SWT.RADIO);
            preverificationRadios[i].setText(BUTTON_TEXTS[i]);

            if (CONFIG_FILE_LOCATIONS[i]
                    .equals(IMTJCoreConstants.PREF_PREVERIFY_CONFIG_LOCATION_SPECIFIED)) {
                addSpecificConfigurationControls(preverifyConfigGroup);
            }
        }
    }

    /**
     * Add the controls for setting the default preverifier.
     * 
     * @param composite
     */
    private void addDefaultPreverifierControls(Composite composite) {
        Group defaultPreverifierGroup = new Group(composite, SWT.NONE);
        defaultPreverifierGroup
                .setText(MTJUIMessages.PreverificationPreferencePage_defaultPreverifierGroup_label_text);
        defaultPreverifierGroup.setLayout(new GridLayout(3, false));
        defaultPreverifierGroup.setLayoutData(new GridData(
                GridData.FILL_HORIZONTAL));

        Label tip = new Label(defaultPreverifierGroup, SWT.NONE);
        tip
                .setText(MTJUIMessages.PreverificationPreferencePage_defaultPreverifierTip_label_text);
        GridData gd = new GridData();
        gd.horizontalSpan = 3;
        gd.verticalIndent = 10;
        tip.setLayoutData(gd);

        defaultPreverifierField = new FileFieldEditor(
                IMTJCoreConstants.PREF_DEFAULT_PREVERIFIER,
                MTJUIMessages.PreverificationPreferencePage_defaultPreverifierField_label_text,
                defaultPreverifierGroup);

        String[] extensions = null;
        if (Platform.getOS().equals(Platform.OS_WIN32)) {
            extensions = new String[] { "*.exe" }; //$NON-NLS-1$
        }
        defaultPreverifierField.setFileExtensions(extensions);
        defaultPreverifierField.setPreferenceStore(getPreferenceStore());
    }

    /**
     * Add the configuration specific controls.
     * 
     * @param preverifyConfigGroup
     * @throws CoreException
     */
    private void addSpecificConfigurationControls(Group preverifyConfigGroup) {
        configCombo = new Combo(preverifyConfigGroup, SWT.READ_ONLY);
        GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
        gridData.horizontalIndent = 15;
        configCombo.setLayoutData(gridData);

        String[] displayValues = new String[configSpecs.length];
        for (int i = 0; i < displayValues.length; i++) {
            displayValues[i] = configSpecs[i].getName();
        }

        configCombo.setItems(displayValues);
    }

    /**
     * Build the midlet suites in the workspace because preverification settings
     * have changed.
     */
    private void buildSuites() {
        // TODO Add some more smarts to what needs to get rebuilt.
        IWorkspaceRoot root = MTJCore.getWorkspace().getRoot();
        IProject[] projects = root.getProjects();

        ArrayList<IProject> toBuild = new ArrayList<IProject>();

        // Collect the projects to be built
        for (int i = 0; i < projects.length; i++) {
            IProject project = projects[i];

            try {
                if (project.isOpen()
                        && project.hasNature(IMTJCoreConstants.MTJ_NATURE_ID)
                        && !usesProjectSpecificPreverification(project)) {
                    toBuild.add(project);
                }
            } catch (CoreException e) {
                MTJCore
                        .log(
                                IStatus.ERROR,
                                MTJUIMessages.PreverificationPreferencePage_error_build_midlet_suite,
                                e);
            }
        }

        // Do the build of those projects
        IProject[] projectArray = toBuild.toArray(new IProject[toBuild.size()]);
        if (projectArray.length > 0) {
            doBuild(projectArray);
        }
    }

    private void setControlsFromDefaults() {
        if (preverificationRadios != null) {
            IPreferenceStore store = getPreferenceStore();
            String location = store
                    .getDefaultString(IMTJCoreConstants.PREF_PREVERIFY_CONFIG_LOCATION);
            for (int i = 0; i < preverificationRadios.length; i++) {
                String fieldValue = CONFIG_FILE_LOCATIONS[i];
                preverificationRadios[i].setSelection(fieldValue
                        .equals(location));
            }

            int specIndex = 0;
            String config = store
                    .getDefaultString(IMTJCoreConstants.PREF_PREVERIFY_CONFIG_VALUE);
            for (int i = 0; i < configSpecs.length; i++) {
                ILibrarySpecification spec = configSpecs[i];
                if (spec.getIdentifier().equals(config)) {
                    specIndex = i;
                    break;
                }
            }

            configCombo.select(specIndex);
        }

        if (defaultPreverifierField != null) {
            defaultPreverifierField.loadDefault();
        }
    }

    /**
     * Set the state of the controls based on the preferences.
     */
    private void setControlsFromPreferences() {
        if (preverificationRadios != null) {
            IPreferenceStore store = getPreferenceStore();
            String location = store
                    .getString(IMTJCoreConstants.PREF_PREVERIFY_CONFIG_LOCATION);
            for (int i = 0; i < preverificationRadios.length; i++) {
                String fieldValue = CONFIG_FILE_LOCATIONS[i];
                preverificationRadios[i].setSelection(fieldValue
                        .equals(location));
            }

            int specIndex = 0;
            String config = store
                    .getString(IMTJCoreConstants.PREF_PREVERIFY_CONFIG_VALUE);
            for (int i = 0; i < configSpecs.length; i++) {
                ILibrarySpecification spec = configSpecs[i];
                if (spec.getIdentifier().equals(config)) {
                    specIndex = i;
                    break;
                }
            }

            configCombo.select(specIndex);
        }
        if (defaultPreverifierField != null) {
            defaultPreverifierField.load();
        }
    }

    private void setDefaultPreverifier() {
        // if the default preverifier value changed
        if (!defaultPreverifierField.getStringValue().equals(
                getPreferenceStore().getString(
                        IMTJCoreConstants.PREF_DEFAULT_PREVERIFIER))) {
            File preverifierExecutable = new File(defaultPreverifierField
                    .getStringValue());
            IPreverifier preverifier = null;
            try {
                preverifier = MTJCore.createPreverifier(
                        IPreverifier.PREVERIFIER_STANDARD,
                        preverifierExecutable);
            } catch (CoreException e) {
                MTJUIPlugin
                        .displayError(
                                getShell(),
                                IStatus.ERROR,
                                -999,
                                MTJUIMessages.PreverificationPreferencePage_invalid_preverifier_dialog_title,
                                MTJUIMessages.PreverificationPreferencePage_invalid_preverifier_dialog_message,
                                e);
                MTJCore
                        .log(
                                IStatus.ERROR,
                                NLS
                                        .bind(
                                                MTJUIMessages.PreverificationPreferencePage_device_store_error,
                                                new String[] {
                                                        e.getClass().getName(),
                                                        e.getMessage() }), e);
            }
            MTJCore.getDeviceRegistry().setDefaultPreverifer(preverifier);

            try {
                MTJCore.getDeviceRegistry().store();
            } catch (Exception e) {
                MTJUIPlugin
                        .displayError(
                                getShell(),
                                IStatus.ERROR,
                                -999,
                                MTJUIMessages.PreverificationPreferencePage_error_store_preverifier_dialog_title,
                                MTJUIMessages.PreverificationPreferencePage_error_store_preverifier_dialog_message,
                                e);
                MTJCore
                        .log(
                                IStatus.ERROR,
                                NLS
                                        .bind(
                                                MTJUIMessages.PreverificationPreferencePage_device_store_error,
                                                new String[] {
                                                        e.getClass().getName(),
                                                        e.getMessage() }), e);
            }
        }

    }

    /**
     * Set the state of the preferences based on the controls.
     */
    private void setPreferencesFromControls() {
        IPreferenceStore store = getPreferenceStore();
        for (int i = 0; i < preverificationRadios.length; i++) {
            if (preverificationRadios[i].getSelection()) {
                store.setValue(
                        IMTJCoreConstants.PREF_PREVERIFY_CONFIG_LOCATION,
                        CONFIG_FILE_LOCATIONS[i]);
                break;
            }
        }

        int index = configCombo.getSelectionIndex();
        store.setValue(IMTJCoreConstants.PREF_PREVERIFY_CONFIG_VALUE,
                configSpecs[index].getIdentifier());
        if (defaultPreverifierField != null) {
            setDefaultPreverifier();
            defaultPreverifierField.store();
        }
    }

    /**
     * Return a boolean indicating if the specified project is using
     * project-specific preverification settings.
     * 
     * @param project
     * @return
     */
    private boolean usesProjectSpecificPreverification(IProject project) {
        ProjectScope projectScope = new ProjectScope(project);
        IEclipsePreferences prefNode = projectScope
                .getNode(IMTJCoreConstants.PLUGIN_ID);

        return prefNode.getBoolean(
                IMTJCoreConstants.PREF_PREVERIFY_USE_PROJECT, false);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        addConfigurationControls(composite);

        if (!embeddedInProperties) {
            addDefaultPreverifierControls(composite);
        } else {
            noDefaultAndApplyButton();
        }

        setControlsFromPreferences();

        return composite;
    }
}
