/*******************************************************************************
 * Copyright (c) 2000, 2004 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Imported from 
 *                               org.eclipse.ui.forms.examples.internal.rcp                     
 */
package org.eclipse.mtj.internal.ui.editors;

/**
 * @author dejan
 */
public interface IModelListener {

    String ADDED = "__added";
    String REMOVED = "__removed";
    String CHANGED = "__changed";

    /**
     * @param objects
     * @param type
     * @param property
     */
    void modelChanged(Object[] objects, String type, String property);
}
