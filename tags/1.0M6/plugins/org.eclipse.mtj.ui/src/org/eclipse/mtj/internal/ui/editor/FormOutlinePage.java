/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import java.util.ArrayList;

import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.mtj.internal.core.IBaseModel;
import org.eclipse.mtj.internal.core.IModelChangeProvider;
import org.eclipse.mtj.internal.core.IModelChangedEvent;
import org.eclipse.mtj.internal.core.IModelChangedListener;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.editor.elements.DefaultContentProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.forms.editor.IFormPage;

/**
 * @since 0.9.1
 */
public class FormOutlinePage extends MTJOutlinePage implements
        IModelChangedListener, ISortableContentOutlinePage {

    /**
     * @since 0.9.1
     */
    public class BasicComparator extends ViewerComparator {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ViewerSorter#category(java.lang.Object)
         */
        @Override
        public int category(Object element) {
            Object[] pages = getPages();
            for (int i = 0; i < pages.length; i++) {
                if (pages[i] == element) {
                    return i;
                }
            }
            return Integer.MAX_VALUE;
        }
    }

    /**
     * @since 0.9.1
     */
    public class BasicContentProvider extends DefaultContentProvider implements
            ITreeContentProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
         */
        public Object[] getChildren(Object obj) {
            return FormOutlinePage.this.getChildren(obj);
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object obj) {
            return getPages();
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
         */
        public Object getParent(Object obj) {
            return null;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
         */
        public boolean hasChildren(Object obj) {
            return getChildren(obj).length > 0;
        }
    }

    /**
     * @since 0.9.1
     */
    public class BasicLabelProvider extends LabelProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
         */
        @Override
        public Image getImage(Object obj) {

            if (obj instanceof IFormPage) {
                return MTJUIPlugin.getDefault().getLabelProvider().get(
                        MTJUIPluginImages.DESC_PAGE_OBJ);
            }
            return MTJUIPlugin.getDefault().getLabelProvider().getImage(obj);
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
         */
        @Override
        public String getText(Object obj) {

            if (obj instanceof IFormPage) {
                return ((IFormPage) obj).getTitle();
            }

            return MTJUIPlugin.getDefault().getLabelProvider().getText(obj);
        }
    }

    private boolean sorted;

    private boolean stale;

    private ViewerComparator viewerComparator;

    protected boolean editorSelection = false;
    protected boolean outlineSelection = false;
    protected TreeViewer treeViewer;

    /**
     * Creates a new Form content outline page.
     * 
     * @param editor
     */
    public FormOutlinePage(MTJFormEditor editor) {
        super(editor);
    }

    /**
     * @return
     */
    public ITreeContentProvider createContentProvider() {
        return new BasicContentProvider();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#createControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createControl(Composite parent) {
        Tree widget = new Tree(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
        treeViewer = new TreeViewer(widget);
        treeViewer.addSelectionChangedListener(this);
        treeViewer.setContentProvider(createContentProvider());
        treeViewer.setLabelProvider(createLabelProvider());
        viewerComparator = createOutlineSorter();
        if (sorted) {
            treeViewer.setComparator(viewerComparator);
        } else {
            treeViewer.setComparator(null);
        }
        treeViewer.setAutoExpandLevel(AbstractTreeViewer.ALL_LEVELS);
        treeViewer.setUseHashlookup(true);
        treeViewer.setInput(fEditor);
        IBaseModel model = fEditor.getAggregateModel();
        if (model instanceof IModelChangeProvider) {
            ((IModelChangeProvider) model).addModelChangedListener(this);
        }
    }

    /**
     * @return
     */
    public ILabelProvider createLabelProvider() {
        return new BasicLabelProvider();
    }

    /**
     * @return
     */
    public ViewerComparator createOutlineSorter() {
        return new BasicComparator();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.Page#dispose()
     */
    @Override
    public void dispose() {
        IBaseModel model = fEditor.getAggregateModel();
        if (model instanceof IModelChangeProvider) {
            ((IModelChangeProvider) model).removeModelChangedListener(this);
        }
        super.dispose();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#getControl()
     */
    @Override
    public Control getControl() {
        return treeViewer != null ? treeViewer.getControl() : null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#getSelection()
     */
    @Override
    public ISelection getSelection() {
        if (treeViewer == null) {
            return StructuredSelection.EMPTY;
        }
        return treeViewer.getSelection();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangedListener#modelChanged(org.eclipse.mtj.core.model.IModelChangedEvent)
     */
    public void modelChanged(IModelChangedEvent event) {
        IFormPage page = fEditor.getActivePageInstance();
        stale = true;
        if (page.isEditor() == false) {
            refresh();
        }
    }

    /**
     * 
     */
    public void refresh() {
        if (stale) {
            final Control control = getControl();
            if ((control == null) || control.isDisposed()) {
                return;
            }
            control.getDisplay().asyncExec(new Runnable() {
                public void run() {
                    if (!treeViewer.getControl().isDisposed()) {
                        treeViewer.refresh();
                        treeViewer.expandAll();
                        stale = false;
                    }
                }
            });
        }
    }

    /**
     * @param item
     */
    public void selectionChanged(Object item) {
        IFormPage page = fEditor.getActivePageInstance();
        String id = getParentPageId(item);
        IFormPage newPage = null;
        if ((id != null) && ((page == null) || !page.getId().equals(id))) {
            newPage = fEditor.setActivePage(id);
        }
        IFormPage revealPage = newPage != null ? newPage : page;
        if ((revealPage != null) && !(item instanceof IFormPage)) {
            revealPage.selectReveal(item);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
     */
    @Override
    public void selectionChanged(SelectionChangedEvent event) {
        if (editorSelection) {
            return;
        }
        outlineSelection = true;
        try {
            ISelection selection = event.getSelection();
            if ((selection.isEmpty() == false)
                    && (selection instanceof IStructuredSelection)) {
                IStructuredSelection ssel = (IStructuredSelection) selection;
                Object item = ssel.getFirstElement();
                selectionChanged(item);
            }
            fireSelectionChanged(selection);
        } finally {
            outlineSelection = false;
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#setFocus()
     */
    @Override
    public void setFocus() {
        if (treeViewer != null) {
            treeViewer.getTree().setFocus();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#setSelection(org.eclipse.jface.viewers.ISelection)
     */
    @Override
    public void setSelection(ISelection selection) {
        if (outlineSelection) {
            return;
        }
        editorSelection = true;
        try {
            if (treeViewer == null) {
                return;
            }
            if ((selection != null) && !selection.isEmpty()
                    && (selection instanceof IStructuredSelection)) {
                Object item = ((IStructuredSelection) selection)
                        .getFirstElement();
                if (item instanceof IDocumentElementNode) {
                    while (null == treeViewer.testFindItem(item)) {
                        item = ((IDocumentElementNode) item).getParentNode();
                        if (item == null) {
                            break;
                        }
                        selection = new StructuredSelection(item);
                    }
                }
            }
            treeViewer.setSelection(selection);
        } finally {
            editorSelection = false;
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.ISortableContentOutlinePage#sort(boolean)
     */
    public void sort(boolean sorting) {
        sorted = sorting;
        if (treeViewer != null) {
            if (sorting) {
                treeViewer.setComparator(viewerComparator);
            } else {
                treeViewer.setComparator(null);
            }
        }
    }

    /**
     * @return
     */
    private Object[] getPages() {
        ArrayList<IFormPage> formPages = new ArrayList<IFormPage>();
        IFormPage[] pages = fEditor.getPages();
        for (IFormPage page : pages) {
            if (page.isEditor() == false) {
                formPages.add(page);
            }
        }
        return formPages.toArray();
    }

    /**
     * @param parent
     * @return
     */
    protected Object[] getChildren(Object parent) {
        return new Object[0];
    }

    /**
     * @param item
     * @return
     */
    protected String getParentPageId(Object item) {
        if (item instanceof IFormPage) {
            return ((IFormPage) item).getId();
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#getTreeViewer()
     */
    @Override
    protected TreeViewer getTreeViewer() {
        return treeViewer;
    }
}
