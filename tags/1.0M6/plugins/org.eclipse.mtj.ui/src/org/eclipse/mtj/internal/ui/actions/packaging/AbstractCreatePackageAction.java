/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards, changed Platform#run to 
 *                                SafeRunner#run in doPackageCreation method
 *     Feng Wang (Sybase)       - Add multi-configuration support, including 
 *                                check if Configuration is dirty and ask user 
 *                                if package all configurations before packaging
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874] 
 */
package org.eclipse.mtj.internal.ui.actions.packaging;

import java.util.Iterator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.project.runtime.MTJRuntimeListUtils;
import org.eclipse.mtj.internal.core.util.LoggingSafeRunnable;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.actions.AbstractJavaProjectAction;
import org.eclipse.mtj.internal.ui.actions.ConfigurationErrorDialog;
import org.eclipse.mtj.internal.ui.preferences.ObfuscationPreferencePage;
import org.eclipse.ui.IWorkbenchPart;

/**
 * Abstract action delegate implementation for creating a packaged version of a
 * Java ME project. This action will create a deployed jar containing the
 * application code as well as updating and deploying the JAD file. Subclasses
 * define whether or not the created package will be obfuscated or not.
 * 
 * @author Craig Setera
 */
public abstract class AbstractCreatePackageAction extends
        AbstractJavaProjectAction {

    /**
     * Default constructor
     */
    public AbstractCreatePackageAction() {
        super();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
     */
    public void run(IAction action) {

        if ((selection != null) && !selection.isEmpty()) {
            boolean proguardConfigured = true;
            if (shouldObfuscate() && !selection.isEmpty()) {
                proguardConfigured = isProguardConfigurationValid();
            }

            if (!proguardConfigured) {
                warnAboutProguardConfiguration();
            } else {
                try {
                    doPackageCreation();
                } catch (CoreException e) {
                    MTJCore
                            .log(
                                    IStatus.ERROR,
                                    MTJUIMessages.AbstractCreatePackageAction_error_creating_package,
                                    e);
                }
            }
        }
    }

    /**
     * Performs this action.
     * 
     * @param project the project to be used to generate the package.
     * @param workbenchPart the active editor workbench part.
     */
    public void run(IJavaProject project, IWorkbenchPart workbenchPart) {
        boolean proguardConfigured = true;

        if (shouldObfuscate()) {
            proguardConfigured = isProguardConfigurationValid();
        }

        if (!proguardConfigured) {
            warnAboutProguardConfiguration();
        } else {
            try {
                doPackageCreation(project, workbenchPart);
            } catch (Throwable e) {
                MTJCore
                        .log(
                                IStatus.ERROR,
                                MTJUIMessages.AbstractCreatePackageAction_error_creating_package,
                                e);
            }
        }
    }

    /**
     * Create the deployed package for the specified java project.
     * 
     * @param monitor
     * @param javaProject
     */
    private void createPackageForProject(IProgressMonitor monitor,
            IJavaProject javaProject) {
        IMTJProject suite = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);

        if (configurationsIsDirty(suite)) {
            return;
        }
        boolean packageInactiveConfigs = needPackageInactiveConfigs(suite);
        try {
            suite.createPackage(monitor, shouldObfuscate(),
                    packageInactiveConfigs);
        } catch (CoreException e) {
            MTJCore
                    .log(
                            IStatus.ERROR,
                            MTJUIMessages.AbstractCreatePackageAction_error_createPackageForProject,
                            e);

            ErrorDialog
                    .openError(
                            getShell(),
                            MTJUIMessages
                                    .bind(
                                            MTJUIMessages.AbstractCreatePackageAction_createPackageForProject_dialog_message,
                                            javaProject.getElementName()), e
                                    .getMessage(), e.getStatus());

        }
    }

    /**
     * If configurations is dirty(not save to metadata file), should not do
     * package.
     * 
     * @param suite
     * @return
     */
    private boolean configurationsIsDirty(IMTJProject suite) {
        if (MTJRuntimeListUtils.isMTJRuntimeListDirty(suite)) {
            MessageDialog
                    .openWarning(
                            null,
                            MTJUIMessages.Configuration_WarningMessage_ConfigurationsDirty_Title,
                            MTJUIMessages.Configuration_WarningMessage_ConfigurationsDirty_Message);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Ask user is he/she want to package all configurations.
     * 
     * @param suite
     * @return
     */
    private boolean needPackageInactiveConfigs(IMTJProject suite) {
        boolean packageInactiveConfigs = false;
        if (suite.getMTJRuntime().size() == 1) {
            return packageInactiveConfigs;
        }
        packageInactiveConfigs = MessageDialog
                .openQuestion(
                        null,
                        MTJUIMessages.Configuration_QuestionMessage_PackageAllConfigs_Title,
                        MTJUIMessages.Configuration_QuestionMessage_PackageAllConfigs_Message);
        return packageInactiveConfigs;
    }

    /**
     * Return a boolean indicating whether the project has a valid platform
     * definition associated.
     * 
     * @param javaProject
     * @return
     * @throws CoreException
     */
    private boolean doesProjectHaveValidDevice(IJavaProject javaProject) {
        boolean hasValidDevice = false;

        IMTJProject suite = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);
        if (suite != null) {
            IMIDPDevice device = (IMIDPDevice) suite.getMTJRuntime()
                    .getActiveMTJRuntime().getDevice();
            hasValidDevice = (device != null);
        }

        return hasValidDevice;
    }

    /**
     * Do the work of packaging.
     * 
     * @throws CoreException
     */
    private void doPackageCreation() throws CoreException {
        // Setup the progress monitoring
        ProgressMonitorDialog dialog = new ProgressMonitorDialog(workbenchPart
                .getSite().getShell());
        dialog.open();

        final IProgressMonitor monitor = dialog.getProgressMonitor();
        monitor
                .beginTask(
                        MTJUIMessages.AbstractCreatePackageAction_doPackageCreation_task_name,
                        3);

        // Create the packages
        Iterator<?> iter = selection.iterator();
        while (iter.hasNext()) {
            final IJavaProject javaProject = getJavaProject(iter.next());

            if (javaProject != null) {
                if (doesProjectHaveValidDevice(javaProject)) {
                    SafeRunner.run(new LoggingSafeRunnable() {
                        public void run() throws Exception {
                            createPackageForProject(monitor, javaProject);
                        }
                    });
                } else {
                    warnAboutInvalidDevice(javaProject);
                }
            }
        }

        // All done
        monitor.done();
        dialog.close();
    }

    /**
     * Do the work of packaging.
     * 
     * @param project the project to be used to generate the package.
     * @param workbenchPart the active editor workbench part.
     * @throws CoreException
     */
    private void doPackageCreation(final IJavaProject javaProject,
            IWorkbenchPart workbenchPart) {
        // Setup the progress monitoring
        ProgressMonitorDialog dialog = new ProgressMonitorDialog(workbenchPart
                .getSite().getShell());
        dialog.open();

        final IProgressMonitor monitor = dialog.getProgressMonitor();
        monitor
                .beginTask(
                        MTJUIMessages.AbstractCreatePackageAction_doPackageCreation_task_name,
                        3);

        if (javaProject != null) {
            if (doesProjectHaveValidDevice(javaProject)) {
                SafeRunner.run(new LoggingSafeRunnable() {
                    public void run() throws Exception {
                        createPackageForProject(monitor, javaProject);
                    }
                });
            } else {
                warnAboutInvalidDevice(javaProject);
            }
        }

        // All done
        monitor.done();
        dialog.close();
    }

    /**
     * Return a boolean indicating whether the proguard configuration is valid
     * for obfuscation.
     * 
     * @return
     */
    private boolean isProguardConfigurationValid() {
        return MTJCore.getProguardJarFile().exists();
    }

    /**
     * Warn the user that the project being packaged does not have a valid
     * device and won't be packaged.
     * 
     * @param javaProject
     */
    private void warnAboutInvalidDevice(IJavaProject javaProject) {

        String message = MTJUIMessages
                .bind(
                        MTJUIMessages.AbstractCreatePackageAction_warnAboutInvalidDevice_message,
                        new String[] { javaProject.getElementName(),
                                javaProject.getElementName() });

        MessageDialog
                .openWarning(
                        getShell(),
                        MTJUIMessages.AbstractCreatePackageAction_warnAboutInvalidDevice_dialog_title,
                        message);
    }

    /**
     * Warn the user that Proguard is not correctly configured for creating
     * obfuscated packages.
     */
    private void warnAboutProguardConfiguration() {
        String message = MTJUIMessages.AbstractCreatePackageAction_warnAboutProguardConfiguration_message;

        ConfigurationErrorDialog dialog = new ConfigurationErrorDialog(
                getShell(),
                ObfuscationPreferencePage.ID,
                MTJUIMessages.AbstractCreatePackageAction_warnAboutProguardConfiguration_dialog_title,
                message,
                MTJUIMessages.AbstractCreatePackageAction_warnAboutProguardConfiguration__configure_btn);

        dialog.open();
    }

    /**
     * Return a boolean indicating whether or not the resulting package should
     * be obfuscated using Proguard.
     * 
     * @return
     */
    protected abstract boolean shouldObfuscate();
}
