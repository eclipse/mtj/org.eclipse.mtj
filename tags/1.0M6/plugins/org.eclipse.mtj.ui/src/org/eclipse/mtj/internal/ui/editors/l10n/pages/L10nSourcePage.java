/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     David Marques (Motorola) - Overriding doSave method. 
 */
package org.eclipse.mtj.internal.ui.editors.l10n.pages;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.l10n.L10nApi;
import org.eclipse.mtj.internal.core.text.IDocumentAttributeNode;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.IDocumentRange;
import org.eclipse.mtj.internal.core.text.IEditingModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.editor.ISortableContentOutlinePage;
import org.eclipse.mtj.internal.ui.editor.MTJFormEditor;
import org.eclipse.mtj.internal.ui.editor.SourceOutlinePage;
import org.eclipse.mtj.internal.ui.editor.XMLSourcePage;
import org.eclipse.mtj.internal.ui.editors.l10n.L10nSourceOutlinePage;

/**
 * 
 */
public class L10nSourcePage extends XMLSourcePage {

    /**
     * @param editor
     * @param id
     * @param title
     */
    public L10nSourcePage(MTJFormEditor editor, String id, String title) {
        super(editor, id, title);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#createOutlinePage()
     */
    protected ISortableContentOutlinePage createOutlinePage() {
        SourceOutlinePage sourceOutlinePage = new L10nSourceOutlinePage(
                (MTJFormEditor) getEditor(), (IEditingModel) getInputContext()
                        .getModel(), createOutlineLabelProvider(),
                createOutlineContentProvider(),
                createDefaultOutlineComparator(), createOutlineComparator());

        fOutlinePage = sourceOutlinePage;
        fOutlineSelectionChangedListener = new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                updateSelection(event);
            }
        };
        fOutlinePage
                .addSelectionChangedListener(fOutlineSelectionChangedListener);
        getSelectionProvider().addSelectionChangedListener(sourceOutlinePage);
        fEditorSelectionChangedListener = new MTJSourcePageChangedListener();
        fEditorSelectionChangedListener.install(getSelectionProvider());
        return fOutlinePage;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#createOutlineComparator()
     */
    @Override
    public ViewerComparator createOutlineComparator() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#createOutlineContentProvider()
     */
    @Override
    public ITreeContentProvider createOutlineContentProvider() {
        return new L10nContentProvider();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#createOutlineLabelProvider()
     */
    @Override
    public ILabelProvider createOutlineLabelProvider() {
        return MTJUIPlugin.getDefault().getLabelProvider();
    }


    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#getRangeElement(int, boolean)
     */
    @Override
    public IDocumentRange getRangeElement(int offset, boolean searchChildren) {
        L10nLocales locales = ((L10nModel) getInputContext().getModel())
                .getLocales();
        return findNode(locales, offset, searchChildren);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJProjectionSourcePage#isQuickOutlineEnabled()
     */
    @Override
    public boolean isQuickOutlineEnabled() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#updateSelection(java.lang.Object)
     */
    @Override
    public void updateSelection(Object object) {
        if ((object instanceof IDocumentElementNode)
                && !((IDocumentElementNode) object).isErrorNode()) {
            setSelectedObject(object);
            setHighlightRange((IDocumentElementNode) object, true);
            setSelectedRange((IDocumentElementNode) object, false);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#findRange()
     */
    @Override
    protected IDocumentRange findRange() {

        Object selectedObject = getSelection();

        if (selectedObject instanceof IDocumentElementNode) {
            return (IDocumentElementNode) selectedObject;
        }

        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.editors.text.TextEditor#initializeEditor()
     */
    @Override
    protected void initializeEditor() {
        super.initializeEditor();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#isSelectionListener()
     */
    @Override
    protected boolean isSelectionListener() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorPart#setPartName(java.lang.String)
     */
    @Override
    protected void setPartName(String partName) {
        super.setPartName(MTJUIMessages.L10nSourcePage_source_partName);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#synchronizeOutlinePage(int)
     */
    @Override
    protected void synchronizeOutlinePage(int offset) {
        IDocumentRange rangeElement = getRangeElement(offset, true);
        updateHighlightRange(rangeElement);
        if (rangeElement instanceof IDocumentAttributeNode) {
            rangeElement = ((IDocumentAttributeNode) rangeElement)
                    .getEnclosingElement();
        }
        updateOutlinePageSelection(rangeElement);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractTextEditor#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    public void doSave(IProgressMonitor progressMonitor) {
        super.doSave(progressMonitor);
        
        try {            
            L10nModel model = (L10nModel) getInputContext().getModel();
            model.validate();
            L10nApi.syncronizeApi(model);
        } catch (CoreException e) {
            MTJCore.log(IStatus.ERROR, e);
        }
    }
}
