/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.internal.ui.editors.device.pages;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IClasspathAttribute;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.ui.wizards.BuildPathDialogAccess;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.core.sdk.device.midp.library.ILibrary;
import org.eclipse.mtj.core.sdk.device.midp.library.ILibraryImporter;
import org.eclipse.mtj.core.sdk.device.midp.library.api.API;
import org.eclipse.mtj.internal.core.persistence.PersistableUtilities;
import org.eclipse.mtj.internal.core.sdk.device.AbstractMIDPDevice;
import org.eclipse.mtj.internal.core.sdk.device.DeviceClasspath;
import org.eclipse.mtj.internal.core.sdk.device.midp.library.Library;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.internal.ui.viewers.TableViewerConfiguration;
import org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage;
import org.eclipse.mtj.ui.editors.device.LibraryApiEditorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

/**
 * Implements an editor page for editing the libraries available for the device
 * being edited.
 * 
 * @author Craig Setera
 */
public class DeviceLibrariesEditorPage extends AbstractDeviceEditorPage {

    // Cell editor for editing library APIs
    private class APIFileSelectionDialogCellEditor extends DialogCellEditor {

        public APIFileSelectionDialogCellEditor(Composite parent) {
            super(parent);
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.DialogCellEditor#openDialogBox(org.eclipse.swt.widgets.Control)
         */
        @Override
        protected Object openDialogBox(Control cellEditorWindow) {
            LibraryApiEditorDialog dialog = new LibraryApiEditorDialog(
                    cellEditorWindow.getShell());

            API[] apis = (API[]) doGetValue();
            dialog.setAPIs(apis);

            return (dialog.open() == Window.OK) ? dialog.getAPIs() : apis;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.DialogCellEditor#updateContents(java.lang.Object)
         */
        @Override
        protected void updateContents(Object value) {
            Label defaultLabel = getDefaultLabel();
            if (defaultLabel == null) {
                return;
            }

            String text = Utils.EMPTY_STRING;
            if (value != null) {
                API[] apis = (API[]) value;
                text = getApisLabel(apis);
            }
            defaultLabel.setText(text);
        }
    }

    // Cell editor for selecting an archive file
    private class ArchiveFileSelectionDialogCellEditor extends DialogCellEditor {
        private boolean filepath;

        public ArchiveFileSelectionDialogCellEditor(Composite parent,
                boolean filepath) {
            super(parent);
            this.filepath = filepath;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.DialogCellEditor#openDialogBox(org.eclipse.swt.widgets.Control)
         */
        @Override
        protected Object openDialogBox(Control cellEditorWindow) {
            File value = (File) doGetValue();
            return promptForArchiveFile(cellEditorWindow.getShell(), value);
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.DialogCellEditor#updateContents(java.lang.Object)
         */
        @Override
        protected void updateContents(Object value) {
            Label defaultLabel = getDefaultLabel();
            if (defaultLabel == null) {
                return;
            }

            String text = Utils.EMPTY_STRING;
            if (value != null) {
                File file = (File) value;
                text = filepath ? file.getParent() : file.getName();
            }
            defaultLabel.setText(text);
        }
    }

    // A cell modifier implementation for the device libraries editor
    private class CellModifier implements ICellModifier {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object, java.lang.String)
         */
        public boolean canModify(Object element, String property) {
            return true;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object, java.lang.String)
         */
        public Object getValue(Object element, String property) {
            Object value = null;
            Library library = (Library) element;

            switch (getColumnIndex(property)) {
                case 0:
                case 1:
                    value = library.getLibraryFile();
                    break;

                case 2:
                    value = library.getAPIs();
                    break;

                case 3:
                case 4:
                    value = library.toClasspathEntry();
                    break;
            }

            return value;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object, java.lang.String, java.lang.Object)
         */
        public void modify(Object element, String property, Object value) {
            TableItem item = (TableItem) element;
            ILibrary library = (ILibrary) item.getData();

            switch (getColumnIndex(property)) {
                case 0:
                case 1:
                    library.setLibraryFile((File) value);
                    break;

                case 2:
                    library.setApis((API[]) value);
                    break;

                case 3: {
                    URL url = getJavadocURL((IClasspathEntry) value);
                    library.setJavadocURL(url);
                }
                    break;

                case 4: {
                    IClasspathEntry entry = (IClasspathEntry) value;
                    library.setSourceAttachmentPath(entry
                            .getSourceAttachmentPath());
                    library.setSourceAttachmentRootPath(entry
                            .getSourceAttachmentRootPath());
                }
                    break;
            }

            viewer.refresh(library, true);
        }

        /**
         * Return the column index for the property.
         * 
         * @param property
         * @return
         */
        private int getColumnIndex(String property) {
            int index = -1;

            for (int i = 0; i < PROPERTIES.length; i++) {
                if (PROPERTIES[i].equals(property)) {
                    index = i;
                    break;
                }
            }

            return index;
        }
    }

    // Content provider for a device's deviceClasspath entries
    private static class DeviceClasspathContentProvider implements
            IStructuredContentProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            if (inputElement instanceof IDeviceClasspath) {
                IDeviceClasspath deviceClasspath = (IDeviceClasspath) inputElement;
                elements = deviceClasspath.getEntries();
            }

            return elements;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    // A dialog cell editor for selecting the javadoc URL for a library
    private class JavadocAttachDialogCellEditor extends DialogCellEditor {

        public JavadocAttachDialogCellEditor(Composite parent) {
            super(parent);
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.DialogCellEditor#openDialogBox(org.eclipse.swt.widgets.Control)
         */
        @Override
        protected Object openDialogBox(Control cellEditorWindow) {
            Shell shell = cellEditorWindow.getShell();

            IClasspathEntry newEntry = null;
            IClasspathEntry entry = (IClasspathEntry) doGetValue();
            if (entry != null) {
                newEntry = BuildPathDialogAccess.configureJavadocLocation(
                        shell, entry);
            }

            return newEntry;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.DialogCellEditor#updateContents(java.lang.Object)
         */
        @Override
        protected void updateContents(Object value) {
            Label defaultLabel = getDefaultLabel();
            if (defaultLabel == null) {
                return;
            }

            String text = Utils.EMPTY_STRING;
            if (value != null) {
                IClasspathEntry entry = (IClasspathEntry) value;
                URL url = getJavadocURL(entry);
                if (url != null) {
                    text = url.toString();
                }
            }
            defaultLabel.setText(text);
        }
    }

    // Label provider for Library instances
    private class LibraryLabelProvider extends LabelProvider implements
            ITableLabelProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
         */
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
         */
        public String getColumnText(Object element, int columnIndex) {
            Library library = (Library) element;
            String text = Utils.EMPTY_STRING;

            if (library != null) {
                switch (columnIndex) {
                    case 0:
                        text = library.getLibraryFile().getName();
                        break;

                    case 1:
                        text = library.getLibraryFile().getParent();
                        break;

                    case 2:
                        text = getApisLabel(library.getAPIs());
                        break;

                    case 3: {
                        URL url = library.getJavadocURL();
                        if (url != null) {
                            text = url.toString();
                        }
                    }
                        break;

                    case 4: {
                        IPath path = library.getSourceAttachmentPath();
                        if (path != null) {
                            text = path.toString();
                        }
                    }
                        break;
                }
            }

            return text;
        }
    }

    // A dialog cell editor for selecting the source locations for a library
    private static class SourceAttachDialogCellEditor extends DialogCellEditor {

        public SourceAttachDialogCellEditor(Composite parent) {
            super(parent);
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.DialogCellEditor#openDialogBox(org.eclipse.swt.widgets.Control)
         */
        @Override
        protected Object openDialogBox(Control cellEditorWindow) {
            Shell shell = cellEditorWindow.getShell();

            IClasspathEntry newEntry = null;
            IClasspathEntry entry = (IClasspathEntry) doGetValue();
            if (entry != null) {
                newEntry = BuildPathDialogAccess.configureSourceAttachment(
                        shell, entry);
            }

            return newEntry;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.DialogCellEditor#updateContents(java.lang.Object)
         */
        @Override
        protected void updateContents(Object value) {
            Label defaultLabel = getDefaultLabel();
            if (defaultLabel == null) {
                return;
            }

            String text = Utils.EMPTY_STRING;
            if (value != null) {
                IClasspathEntry entry = (IClasspathEntry) value;
                IPath attachPath = entry.getSourceAttachmentPath();
                if (attachPath != null) {
                    text = attachPath.toString();
                }
            }
            defaultLabel.setText(text);
        }
    }

    private static final TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
            new TableColumnInfo(
                    MTJUIMessages.DeviceLibrariesEditorPage_file_columnInfo,
                    15f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceLibrariesEditorPage_path_columnInfo,
                    20f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceLibrariesEditorPage_apis_columnInfo,
                    15f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceLibrariesEditorPage_javadoc_columnInfo,
                    25f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceLibrariesEditorPage_source_columnInfo,
                    25f, null), };
    // Column information structure
    private static final int DEFAULT_TABLE_WIDTH = 650;

    private static final Object[] NO_ELEMENTS = new Object[0];

    private static final String PROP_APIS = "apis"; //$NON-NLS-1$

    // Column property names
    private static final String PROP_FILE = "file"; //$NON-NLS-1$

    private static final String PROP_JAVADOC = "javadoc"; //$NON-NLS-1$

    private static final String PROP_PATH = "path"; //$NON-NLS-1$

    private static final String PROP_SOURCE = "source"; //$NON-NLS-1$

    // All of the properties in order
    private static final String[] PROPERTIES = new String[] { PROP_FILE,
            PROP_PATH, PROP_APIS, PROP_JAVADOC, PROP_SOURCE };

    // Widgets
    private TableViewer viewer;

    /**
     * Construct the editor page.
     * 
     * @param parent
     * @param style
     */
    public DeviceLibrariesEditorPage(Composite parent, int style) {
        super(parent, style);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#commitDeviceChanges()
     */
    @Override
    public void commitDeviceChanges() {
        Object viewerInput = viewer.getInput();
        if (viewerInput instanceof IDeviceClasspath) {
            editDevice.setClasspath((DeviceClasspath) viewerInput);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#getDescription()
     */
    @Override
    public String getDescription() {
        return MTJUIMessages.DeviceLibrariesEditorPage_description;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#getTitle()
     */
    @Override
    public String getTitle() {
        return MTJUIMessages.DeviceLibrariesEditorPage_title;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#setDevice(org.eclipse.mtj.core.model.device.IDevice)
     */
    @Override
    public void setDevice(IMIDPDevice device) {
        super.setDevice(device);

        if (device instanceof AbstractMIDPDevice) {
            IDeviceClasspath deviceClasspath = ((AbstractMIDPDevice) device)
                    .getClasspath();
            try {
                IDeviceClasspath clone = (IDeviceClasspath) PersistableUtilities
                        .clonePersistable(deviceClasspath);
                viewer.setInput(clone);
            } catch (PersistenceException e) {
                MTJCore
                        .log(
                                IStatus.WARNING,
                                MTJUIMessages.DeviceLibrariesEditorPage_error_cloning_device_classpath,
                                e);
            }
        }
    }

    /**
     * Create the devices table viewer.
     * 
     * @param parent
     */
    private TableViewer createTableViewer(Composite composite) {
        int styles = SWT.SINGLE | SWT.V_SCROLL | SWT.BORDER
                | SWT.FULL_SELECTION;
        Table table = new Table(composite, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        // Wire up the viewer
        TableViewer viewer = new TableViewer(table);
        viewer.setContentProvider(new DeviceClasspathContentProvider());
        viewer.setLabelProvider(new LibraryLabelProvider());

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings("deviceLibrariesViewerSettings"); //$NON-NLS-1$
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 0);
        viewerConfiguration.configure(viewer);

        // Wire up the cell modification handling
        viewer.setCellModifier(new CellModifier());
        viewer.setColumnProperties(PROPERTIES);
        viewer.setCellEditors(new CellEditor[] {
                new ArchiveFileSelectionDialogCellEditor(table, false),
                new ArchiveFileSelectionDialogCellEditor(table, true),
                new APIFileSelectionDialogCellEditor(table),
                new JavadocAttachDialogCellEditor(table),
                new SourceAttachDialogCellEditor(table), });

        return viewer;
    }

    /**
     * Return the API's for the library.
     * 
     * @param library
     * @return
     */
    private String getApisLabel(API[] apis) {
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < apis.length; i++) {
            API api = apis[i];
            if (i != 0) {
                sb.append(", "); //$NON-NLS-1$
            }

            sb.append(api);
        }

        return sb.toString();
    }

    /**
     * Return the deviceClasspath being edited.
     * 
     * @return
     */
    private IDeviceClasspath getClasspath() {
        return (IDeviceClasspath) viewer.getInput();
    }

    /**
     * Return the currently selected library or <code>null</code> if nothing is
     * selected.
     * 
     * @return
     */
    private ILibrary getSelectedLibrary() {
        IStructuredSelection selection = (IStructuredSelection) viewer
                .getSelection();
        return (selection.size() > 0) ? (ILibrary) selection.getFirstElement()
                : null;
    }

    /**
     * Handle the add button being pressed.
     */
    private void handleAddButton() {
        File archiveFile = promptForArchiveFile(getShell(), null);
        if (archiveFile != null) {
            ILibraryImporter importer = MTJCore.getLibraryImporter(ILibraryImporter.LIBRARY_IMPORTER_UEI);
            ILibrary library = importer.createLibraryFor(archiveFile);
            getClasspath().addEntry(library);
            viewer.refresh();
        }
    }

    /**
     * Handle the remove button being pressed.
     */
    private void handleRemoveButton() {
        if (MessageDialog
                .openConfirm(
                        getShell(),
                        MTJUIMessages.DeviceLibrariesEditorPage_handleRemoveButton_dialog_title,
                        MTJUIMessages.DeviceLibrariesEditorPage_handleRemoveButton_dialog_message)) {
            ILibrary selectedLibrary = getSelectedLibrary();
            getClasspath().removeEntry(selectedLibrary);
            viewer.refresh();
        }
    }

    /**
     * Prompt for an archive file.
     * 
     * @param shell
     * @param currentFile
     * @return
     */
    private File promptForArchiveFile(Shell shell, File currentFile) {
        FileDialog fileDialog = new FileDialog(shell);
        fileDialog.setFilterNames(new String[] { "*.jar;*.zip" }); //$NON-NLS-1$

        if ((currentFile != null) && (currentFile.exists())) {
            fileDialog.setFileName(currentFile.toString());
        }

        String filename = fileDialog.open();
        return (filename == null) ? null : new File(filename);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#addPageControls(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void addPageControls(Composite parent) {
        parent.setLayoutData(new GridData(GridData.FILL_BOTH));
        parent.setLayout(new GridLayout(2, false));

        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.minimumWidth = DEFAULT_TABLE_WIDTH;
        gridData.heightHint = 400;
        viewer = createTableViewer(parent);
        viewer.getTable().setLayoutData(gridData);

        Composite buttonComposite = new Composite(parent, SWT.NONE);
        buttonComposite.setLayout(new GridLayout(1, true));
        buttonComposite.setLayoutData(new GridData(GridData.FILL_VERTICAL));

        Button addButton = new Button(buttonComposite, SWT.PUSH);
        addButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        addButton.setText(MTJUIMessages.DeviceLibrariesEditorPage_addButton);
        addButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleAddButton();
            }
        });

        final Button removeButton = new Button(buttonComposite, SWT.PUSH);
        removeButton.setEnabled(false);
        removeButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        removeButton
                .setText(MTJUIMessages.DeviceLibrariesEditorPage_removeButton);
        removeButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleRemoveButton();
            }
        });

        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                ILibrary selectedLibrary = getSelectedLibrary();
                removeButton.setEnabled(selectedLibrary != null);
            }
        });
    }

    /**
     * Return the javadoc url for the specified entry.
     * 
     * @param entry
     * @return
     */
    protected URL getJavadocURL(IClasspathEntry entry) {
        URL url = null;

        if ((entry != null) && (entry.getExtraAttributes() != null)) {
            IClasspathAttribute[] attributes = entry.getExtraAttributes();
            for (IClasspathAttribute attribute : attributes) {
                if (attribute.getName().equals(
                        IClasspathAttribute.JAVADOC_LOCATION_ATTRIBUTE_NAME)) {
                    try {
                        url = new URL(attribute.getValue());
                    } catch (MalformedURLException e) {
                        MTJCore
                                .log(
                                        IStatus.WARNING,
                                        MTJUIMessages.DeviceLibrariesEditorPage_error_getting_new_Javadoc,
                                        e);
                    }
                }
            }
        }

        return url;
    }
}
