/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 *     Diego Sandin (Motorola) - Added runtime section
 *     Feng Wang (Sybase)      - Modify runtime section, replace Device Selector with
 *                               Configuration Manager, for Multi-configs support.
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874] 
 */
package org.eclipse.mtj.internal.ui.editors.jad.form.pages;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.event.AddMTJRuntimeEvent;
import org.eclipse.mtj.core.project.runtime.event.IMTJRuntimeChangeListener;
import org.eclipse.mtj.core.project.runtime.event.IMTJRuntimeListChangeListener;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeDeviceChangeEvent;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeNameChangeEvent;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeWorkspaceSymbolSetsChangeEvent;
import org.eclipse.mtj.core.project.runtime.event.RemoveMTJRuntimeEvent;
import org.eclipse.mtj.core.project.runtime.event.SwitchActiveMTJRuntimeEvent;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.project.runtime.MTJRuntimeListUtils;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.actions.exporting.AntennaBuildExportAction;
import org.eclipse.mtj.internal.ui.actions.packaging.CreateObfuscatedPackageAction;
import org.eclipse.mtj.internal.ui.actions.packaging.CreatePackageAction;
import org.eclipse.mtj.internal.ui.configurations.ConfigManageComponent;
import org.eclipse.mtj.internal.ui.editors.FormLayoutFactory;
import org.eclipse.mtj.internal.ui.editors.jad.form.JADFormEditor;
import org.eclipse.mtj.internal.ui.preferences.ExtendedStringFieldEditor;
import org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;

/**
 * @author Diego Madruga Sandin
 */
public class OverviewEditorPage extends JADPropertiesEditorPage implements
        IPropertyChangeListener, IMTJRuntimeListChangeListener,
        IMTJRuntimeChangeListener {

    /**
     * The page unique identifier
     */
    private static final String OVERVIEW_PAGEID = "overview"; //$NON-NLS-1$

    private ConfigManageComponent configManager;

    /**
     * The current setting of the jar URL after a setInput call. If changed on
     * save, we will trigger a clean build to cause the jar file to be
     * regenerated.
     */
    private String loadedJarUrl;

    private IMidletSuiteProject midletProject;

    private IJavaProject project;

    /**
     * A constructor that creates the Overview EditorPage and initializes it
     * with the editor.
     * 
     * @param editor the parent editor
     */
    public OverviewEditorPage(JADFormEditor editor) {
        super(editor, OVERVIEW_PAGEID, MTJUIMessages.OverviewEditorPage_title);

        this.project = JavaCore.create(((JADFormEditor) getEditor())
                .getJadFile().getProject());

        this.midletProject = MidletSuiteFactory
                .getMidletSuiteProject(this.project);
    }

    public void activeMTJRuntimeSwitched(SwitchActiveMTJRuntimeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.configuration.IConfigurationsChangeListener#configurationAdded(org.eclipse.mtj.core.model.configuration.AddConfigEvent)
     */
    public void mtjRuntimeAdded(AddMTJRuntimeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.configuration.IConfigurationsChangeListener#configurationRemoved(org.eclipse.mtj.core.model.configuration.RemoveConfigEvent)
     */
    public void mtjRuntimeRemoved(RemoveMTJRuntimeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.configuration.IConfigurationChangeListener#deviceChanged(org.eclipse.mtj.core.model.configuration.ConfigDeviceChangeEvent)
     */
    public void deviceChanged(MTJRuntimeDeviceChangeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#dispose()
     */
    @Override
    public void dispose() {
        super.dispose();
        if (isDirty()) {
            configManager.performCancel();
        }
        configManager.dispose();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor monitor) {
        super.doSave(monitor);

        String currentJarUrl = getPreferenceStore().getString(
                IJADConstants.JAD_MIDLET_JAR_URL);
        if (!currentJarUrl.equals(loadedJarUrl)) {
            ((JADFormEditor) getEditor()).setCleanRequired(true);
        }
        IDevice device = configManager.getActiveConfiguration().getDevice();
        if ((midletProject != null) && (device != null)) {
            try {
                midletProject.refreshClasspath(monitor);
                configManager.performFinish();
                midletProject.saveMetaData();

            } catch (CoreException e) {
                e.printStackTrace();
            }
        }

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getTitle()
     */
    @Override
    public String getTitle() {
        return MTJUIMessages.OverviewEditorPage_title;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#linkActivated(org.eclipse.ui.forms.events.HyperlinkEvent)
     */
    @Override
    public void linkActivated(HyperlinkEvent e) {
        String href = (String) e.getHref();

        IWorkbenchPart activePart = PlatformUI.getWorkbench()
                .getActiveWorkbenchWindow().getActivePage().getActivePart();

        if (href.equals("package")) { //$NON-NLS-1$
            new CreatePackageAction().run(project, activePart);
        } else if (href.equals("obfuscate")) { //$NON-NLS-1$
            new CreateObfuscatedPackageAction().run(project, activePart);
        } else if (href.equals("antenna")) { //$NON-NLS-1$
            new AntennaBuildExportAction().run(project, activePart);
        } else if (href.startsWith("launchShortcut.")) { //$NON-NLS-1$
            handleLaunchShortcut(href);
        }
    }

    public void nameChanged(MTJRuntimeNameChangeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent event) {

        if (event.getProperty().equals(FieldEditor.VALUE)) {

            if (event.getSource() instanceof ExtendedStringFieldEditor) {
                String fieldEditorLabel = ((FieldEditor) event.getSource())
                        .getLabelText();

                Control c = ((ExtendedStringFieldEditor) event.getSource())
                        .getFieldEditorTextControl();

                if (event.getNewValue().equals(Utils.EMPTY_STRING)) {

                    getErrorMessageManager()
                            .addMessage(
                                    "textLength_" + fieldEditorLabel, //$NON-NLS-1$
                                    MTJUIMessages.OverviewEditorPage_empty_field_error_msg,
                                    null, IMessageProvider.ERROR, c);

                } else {
                    getErrorMessageManager().removeMessage(
                            "textLength_" + fieldEditorLabel, c); //$NON-NLS-1$
                }
            }
            setDirty(true);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.configuration.IConfigurationChangeListener#symbolSetChanged()
     */
    public void symbolSetChanged() {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.configuration.IConfigurationChangeListener#workspaceScopeSymbolSetsChanged(org.eclipse.mtj.core.model.configuration.ConfigWorkspaceSymbolSetsChangeEvent)
     */
    public void workspaceScopeSymbolSetsChanged(
            MTJRuntimeWorkspaceSymbolSetsChangeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /**
     * Create section for debugging options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createDebuginSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticSection(toolkit, parent,
                MTJUIMessages.OverviewEditorPage_debugging_section_title);
        Composite container = createStaticSectionClient(toolkit, section);

        FormText text = createClient(container,
                MTJUIMessages.overviewPage_launchsection_debuglinks, toolkit,
                this);

        text.setImage("debugMidlet", MTJUIPluginImages.DESC_DEBUG_MIDLET //$NON-NLS-1$
                .createImage());
        text.setImage("debugjad", MTJUIPluginImages.DESC_DEBUG_JAD //$NON-NLS-1$
                .createImage());
        text.setImage("debugOta", MTJUIPluginImages.DESC_DEBUG_OTA //$NON-NLS-1$
                .createImage());

        section.setClient(container);
    }

    /**
     * Create section for exporting options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createExportingSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticSection(toolkit, parent,
                MTJUIMessages.OverviewEditorPage_exporting_section_title);
        Composite container = createStaticSectionClient(toolkit, section);

        FormText text = createClient(container,
                MTJUIMessages.overviewPage_exporting, toolkit, this);

        text.setImage("antenna", MTJUIPluginImages.DESC_ANT.createImage()); //$NON-NLS-1$

        section.setClient(container);
    }

    /**
     * Create section for required information about the application.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createOverviewSection(final IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticBasicSection(toolkit, parent,
                MTJUIMessages.overviewPage_requiredsection_title,
                MTJUIMessages.overviewPage_requiredsection_description);

        Composite sectionClient = createStaticSectionClient(toolkit, section);
        sectionClient.setLayout(FormLayoutFactory
                .createSectionClientTableWrapLayout(false, 1));

        createSectionContent(managedForm, sectionClient, this);
        section.setClient(sectionClient);

    }

    /**
     * Create section for packaging options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createPackagingSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticSection(toolkit, parent,
                MTJUIMessages.OverviewEditorPage_packaging_section_title);
        Composite container = createStaticSectionClient(toolkit, section);

        FormText text = createClient(container,
                MTJUIMessages.overviewPage_deploying, toolkit, this);

        text.setImage("package", MTJUIPluginImages.DESC_PACKAGE.createImage()); //$NON-NLS-1$
        text.setImage("obfuscate", MTJUIPluginImages.DESC_PACKAGE_OBFUSCATED //$NON-NLS-1$
                .createImage());

        section.setClient(container);
    }

    /**
     * Create section for running options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createRunningSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        // Running links
        Section section = createStaticSection(toolkit, parent,
                MTJUIMessages.OverviewEditorPage_running_section_title);
        Composite container = createStaticSectionClient(toolkit, section);

        FormText text = createClient(container,
                MTJUIMessages.overviewPage_launchsection_runlinks, toolkit,
                this);

        text.setImage("runMidlet", MTJUIPluginImages.DESC_RUN_MIDLET
                .createImage()); //$NON-NLS-1$
        text.setImage("runjad", MTJUIPluginImages.DESC_RUN_JAD.createImage()); //$NON-NLS-1$
        text.setImage("runOta", MTJUIPluginImages.DESC_RUN_OTA.createImage()); //$NON-NLS-1$

        section.setClient(container);
    }

    /**
     * Create section for debugging options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createRuntimeSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticBasicSection(toolkit, parent,
                MTJUIMessages.overviewPage_runtimesection_title,
                MTJUIMessages.overviewPage_runtimesection_description);

        Composite sectionClient = createStaticSectionClient(toolkit, section);
        sectionClient.setLayout(FormLayoutFactory
                .createSectionClientGridLayout(true, 1));

        createRuntimeSectionContent(managedForm, sectionClient, this);
        section.setClient(sectionClient);
    }

    /**
     * @param managedForm
     * @param sectionClient
     * @param overviewEditorPage
     */
    private void createRuntimeSectionContent(IManagedForm managedForm,
            Composite sectionClient, OverviewEditorPage overviewEditorPage) {

        configManager = new ConfigManageComponent(midletProject);
        configManager.setIncludeGroup(false);
        configManager.createContents(sectionClient);
        configManager.setConfigurationsChangeListener(this);
        configManager.setConfigurationChangeListener(this);
    }

    /**
     * Fill the page body with all available sections.
     * 
     * @param managedForm the managed form that wraps the form widget
     */
    private void fillEditorPageBody(IManagedForm managedForm) {

        FormToolkit toolkit = managedForm.getToolkit();

        Composite body = managedForm.getForm().getBody();
        body.setLayout(FormLayoutFactory.createFormTableWrapLayout(true, 2));

        Composite left = toolkit.createComposite(body);
        left.setLayout(FormLayoutFactory
                .createFormPaneTableWrapLayout(false, 1));
        left.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB));

        createOverviewSection(managedForm, left, toolkit);
        createPackagingSection(managedForm, left, toolkit);
        createExportingSection(managedForm, left, toolkit);

        Composite right = toolkit.createComposite(body);
        right.setLayout(FormLayoutFactory.createFormPaneTableWrapLayout(false,
                1));
        right.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB));

        createRunningSection(managedForm, right, toolkit);
        createDebuginSection(managedForm, right, toolkit);
        createRuntimeSection(managedForm, right, toolkit);

    }

    /**
     * Handles a launch shortcut activation.
     * 
     * @param href the launch shortcut. The format of href should be
     *            <code>launchShortcut.&lt;mode&gt;.&lt;launchShortcutId&gt;</code>
     */
    private void handleLaunchShortcut(String href) {

        href = href.substring(15);
        int index = href.indexOf('.');
        if (index < 0) {
            return; // error. Format of href should be
        }
        // launchShortcut.<mode>.<launchShortcutId>
        String mode = href.substring(0, index);
        String id = href.substring(index + 1);

        IExtensionRegistry registry = Platform.getExtensionRegistry();
        IConfigurationElement[] elements = registry
                .getConfigurationElementsFor("org.eclipse.debug.ui.launchShortcuts"); //$NON-NLS-1$
        for (IConfigurationElement element : elements) {
            if (id.equals(element.getAttribute("id"))) { //$NON-NLS-1$
                try {
                    ILaunchShortcut shortcut = (ILaunchShortcut) element
                            .createExecutableExtension("class"); //$NON-NLS-1$
                    shortcut.launch(new StructuredSelection(project), mode);
                } catch (CoreException e1) {
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#addContextHelp(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void addContextHelp(Composite c) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(c,
                "org.eclipse.mtj.ui.help_JADRequiredPropertiesEditorPage"); //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createFormContent(IManagedForm managedForm) {

        final ScrolledForm form = managedForm.getForm();
        FormToolkit toolkit = managedForm.getToolkit();

        form.setText(getTitle());
        toolkit.decorateFormHeading(form.getForm());

        createErrorMessageHandler(managedForm);

        /*
         * launch the help system UI, displaying the documentation identified by
         * the href parameter.
         */
        final String href = getHelpResource();
        if (href != null) {
            IToolBarManager manager = form.getToolBarManager();
            Action helpAction = new Action(
                    MTJUIMessages.OverviewEditorPage_help_action) {
                @Override
                public void run() {
                    PlatformUI.getWorkbench().getHelpSystem()
                            .displayHelpResource(href);
                }
            };

            helpAction.setImageDescriptor(MTJUIPluginImages.DESC_LINKTOHELP);
            manager.add(helpAction);
        }
        form.updateToolBar();

        fillEditorPageBody(managedForm);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
     */
    @Override
    protected String getHelpResource() {
        return "/org.eclipse.mtj.doc.user/html/reference/editors/jad_editor/overview.html"; //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionDescription()
     */
    @Override
    protected String getSectionDescription() {
        return Utils.EMPTY_STRING;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionTitle()
     */
    @Override
    protected String getSectionTitle() {
        return Utils.EMPTY_STRING;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#setInput(org.eclipse.ui.IEditorInput)
     */
    @Override
    protected void setInput(IEditorInput input) {
        super.setInput(input);

        // Store the current JAR URL setting for later save comparison
        loadedJarUrl = getPreferenceStore().getString(
                IJADConstants.JAD_MIDLET_JAR_URL);
    }
}
