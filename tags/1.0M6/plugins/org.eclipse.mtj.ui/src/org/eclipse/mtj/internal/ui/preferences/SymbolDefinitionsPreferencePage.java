/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Fixed refreshing issue     
 *     Hugo Raniere (Motorola)  - Fixing "Add" and "Remove" symbol buttons behavior
 *     Hugo Raniere (Motorola)  - Removing "Restore Defaults" and "Apply" buttons
 *     Gang Ma (Sybase)	        - Fixed bug 246721: select the new added symbol 
 *     				  after a symbol is added
 *     Feng Wang (Sybase)       - 1, Add page ID.
 *                                2, Set label provider before set input for 
 *                                   definitionsComboViewer, to prevent call 
 *                                   SymbolDefinitionSet.toString() unnecessarily
 *     Ales Milan               - Add import symbol sets from J2ME Polish 
 *                                devices.xml and groups.xml files support
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *                                
 */
package org.eclipse.mtj.internal.ui.preferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ICellEditorListener;
import org.eclipse.jface.viewers.ICellEditorValidator;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.core.symbol.ISymbolSetChangeListener;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.internal.ui.viewers.TableViewerConfiguration;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * Preference page for defining and editing the available symbol definition
 * instances that are used for handling device fragmentation.
 * 
 * @author Craig Setera
 */
public class SymbolDefinitionsPreferencePage extends PreferencePage implements
        IWorkbenchPreferencePage {

    /**
     * A cell modifier implementation for the device libraries editor
     */
    private class CellModifier implements ICellModifier {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object, java.lang.String)
         */
        public boolean canModify(Object element, String property) {
            return true;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object, java.lang.String)
         */
        public Object getValue(Object element, String property) {
            String value = null;

            ISymbol def = (ISymbol) element;
            if (property.equals(PROP_SYMBOL)) {
                value = def.getName();
            } else {
                value = def.getValue();
            }
            return value;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object, java.lang.String, java.lang.Object)
         */
        public void modify(Object element, String property, Object value) {
            TableItem item = (TableItem) element;

            String symbolName = item.getText(0);
            String currentValue = item.getText(1);

            int itemIndex = findSymbolDefinition(symbolName);
            if (itemIndex != -1) {
                if (property.equals(PROP_SYMBOL)) {
                    String newSymbol = (String) value;
                    if (isValidSymbol(newSymbol)) {
                    	ISymbol s = MTJCore.getSymbolSetFactory().createSymbol(newSymbol, currentValue);
                        currentDefinitions.set(itemIndex, s);
                    }
                } else {
                    ISymbol def = currentDefinitions.get(itemIndex);
                    def.setValue((String)value);
                }

                tableViewer.refresh();
            }

            setErrorMessage(null);
        }

        /**
         * Attempt to find the symbol definition with the specified name in the
         * current definitions. Return the index or <code>-1</code> if not
         * found.
         * 
         * @param name
         * @return
         */
        private int findSymbolDefinition(String name) {
            int index = -1;

            for (int i = 0; i < currentDefinitions.size(); i++) {
                ISymbol def = currentDefinitions.get(i);
                if (def.getName().equals(name)) {
                    index = i;
                    break;
                }
            }

            return index;
        }
    }

    /**
     * Label provider for the definitions set combo viewer
     */
    private static class DefinitionSetLabelProvider extends LabelProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
         */
        @Override
        public String getText(Object element) {
            return ((ISymbolSet) element).getName();
        }

    }

    /**
     * Content provider for the definitions set combo viewer
     */
    private class DefinitionSetsContentProvider implements
            IStructuredContentProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            Object[] definitions = NO_ELEMENTS;

            if (definitionSetsinput != null) {
                definitions = definitionSetsinput;
            }

            return definitions;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    /**
     * Label provider for the strings returned by the content provider.
     */
    private static class SymbolDefinitionLabelProvider extends LabelProvider
            implements ITableLabelProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
         */
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
         */
        public String getColumnText(Object element, int columnIndex) {
            String text = ""; //$NON-NLS-1$
            ISymbol def = (ISymbol) element;

            switch (columnIndex) {
            case 0:
                text = def.getName();
                break;
            case 1:
                text = def.getValue();
                break;
            }
            return text;
        }
    }

    /**
     * Cell editor that includes validation of the Symbol name
     */
    private class SymbolNameCellEditor extends TextCellEditor {

        /**
         * @param parent
         */
        public SymbolNameCellEditor(Composite parent) {
            super(parent);

            setValidator(new SymbolNameCellEditorValidator());
            addListener(new ICellEditorListener() {

                public void applyEditorValue() {
                }

                public void cancelEditor() {
                }

                public void editorValueChanged(boolean oldValidState,
                        boolean newValidState) {
                    if (!newValidState) {
                        setErrorMessage(getErrorMessage());
                    } else {
                        setErrorMessage(null);
                    }
                }
            });
        }
    }

    /**
     * Validates that the value for the symbol is a valid value
     */
    private class SymbolNameCellEditorValidator implements ICellEditorValidator {
        public String isValid(Object value) {
            String symbol = (String) value;
            return isValidSymbol(symbol) ? null
                    : MTJUIMessages.SymbolDefinitionsPreferencePage_invalid_symbol;
        }
    }

    /**
     * Implementation of the table's content provider.
     */
    private class TableContentProvider implements IStructuredContentProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            return currentDefinitions.toArray(new Object[currentDefinitions
                    .size()]);
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
            if (oldInput != null) {
                commitSymbolDefinitions((ISymbolSet) oldInput);
            }

            // Track the current definitions for the UI...
            // These definitions don't matter until a commit occurs
            if (currentDefinitions == null) {
                currentDefinitions = new ArrayList<ISymbol>();
            } else {
                currentDefinitions.clear();
            }

            if (newInput instanceof ISymbolSet) {
                ISymbolSet set = (ISymbolSet) newInput;
                Collection<ISymbol> symbols= set.getSymbols();

                if (symbols != null) {
                    symbolsGroup
                            .setText(NLS
                                    .bind(
                                            MTJUIMessages.SymbolDefinitionsPreferencePage_symbolsGroup_label_text,
                                            set.getName()));
                    for (ISymbol s: symbols) {
                        currentDefinitions.add(s);
                    }
                }
            }
        }
    }

    /**
     * Identifier of this preference page
     */
    public static final String ID = "org.eclipse.mtj.ui.preferences.symbolDefinitionsPreferencePage"; //$NON-NLS-1$

    private static final TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
            new TableColumnInfo(
                    MTJUIMessages.SymbolDefinitionsPreferencePage_symbolColumnInfo,
                    50f, null),
            new TableColumnInfo(
                    MTJUIMessages.SymbolDefinitionsPreferencePage_valueColumnInfo,
                    50f, null) };

    // Column information structure
    private static final int DEFAULT_TABLE_WIDTH = 650;

    // Column information structure
    private static final Object[] NO_ELEMENTS = new Object[0];

    // Column property names
    private static final String PROP_SYMBOL = "symbol"; //$NON-NLS-1$

    private static final String PROP_VALUE = "value"; //$NON-NLS-1$

    // All of the properties in order
    private static final String[] PROPERTIES = new String[] { PROP_SYMBOL,
            PROP_VALUE };

    // A regular expression compiled Pattern that matches on whitespace
    // within a string
    private static final Pattern WHITESPACE_PATTERN = Pattern
            .compile(".*\\s+.*"); //$NON-NLS-1$

    private Button addSetButton;
    private Button addSymbolButton;
    // Tracks the current definitions in the table viewer until they
    // are committed back to the underlying model
    private ArrayList<ISymbol> currentDefinitions;
    private ComboViewer definitionsComboViewer;
    private ISymbolSet[] definitionSetsinput = null;
    private Button importSetButton;
    private Button removeSetButton;
    private Button removeSymbolButton;

    private Group symbolsGroup;

    private TableViewer tableViewer;

    // UI widgets
    private IWorkbench workbench;

    /**
     * Commit the contents of the current symbol definitions back to the model
     * object.
     */
    public void commitSymbolDefinitions() {
        commitSymbolDefinitions(getSelectedSymbolDefinitionSet());
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
        this.workbench = workbench;
        noDefaultAndApplyButton();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performCancel()
     */
    @Override
    public boolean performCancel() {
        // Force a reload of the symbol definitions registry
        return reloadSymbolDefinitionsRegistry() && super.performCancel();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        boolean succeeded = false;

        commitSymbolDefinitions();
        try {
        	MTJCore.getSymbolSetRegistry().store();
            succeeded = true;
        } catch (PersistenceException e) {
            handleException(
                    MTJUIMessages.SymbolDefinitionsPreferencePage_error_store_symbol,
                    e);
        } catch (TransformerException e) {
            handleException(
                    MTJUIMessages.SymbolDefinitionsPreferencePage_error_store_symbol,
                    e);
        } catch (IOException e) {
            handleException(
                    MTJUIMessages.SymbolDefinitionsPreferencePage_error_store_symbol,
                    e);
        }

        return succeeded && super.performOk();
    }

    /**
     * Commit the contents of the current symbol definitions back to the model
     * object.
     * 
     * @param set
     */
    private void commitSymbolDefinitions(ISymbolSet set) {
        // Save the current definitions
        if (set != null) {
            Map<String, String> defs = new HashMap<String, String>(
                    currentDefinitions.size());

            Iterator<ISymbol> iterator = currentDefinitions.iterator();
            while (iterator.hasNext()) {
                ISymbol def = iterator.next();
                defs.put(def.getName(), def.getValue());
            }

            set.setSymbols(defs);
        }
    }

    /**
     * Create the table viewer.
     * 
     * @param parent
     */
    private TableViewer createTableViewer(Composite composite) {
        int styles = SWT.SINGLE | SWT.V_SCROLL | SWT.BORDER
                | SWT.FULL_SELECTION | SWT.FILL;

        final Table table = new Table(composite, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        // table.setLayoutData(new GridData(GridData.FILL_BOTH));

        // Wire up the viewer
        TableViewer viewer = new TableViewer(table);
        viewer.setContentProvider(new TableContentProvider());
        viewer.setLabelProvider(new SymbolDefinitionLabelProvider());
        viewer.setSorter(new ViewerSorter());
        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                updateButtonEnablement();
            }
        });

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings("symbolDefsViewerSettings"); //$NON-NLS-1$
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 0);
        viewerConfiguration.configure(viewer);

        // Wire up the cell modification handling
        viewer.setCellModifier(new CellModifier());
        viewer.setColumnProperties(PROPERTIES);
        viewer.setCellEditors(new CellEditor[] {
                new SymbolNameCellEditor(table), new TextCellEditor(table), });

        return viewer;
    }

    /**
     * Return the currently selected symbol definition or <code>null</code> if
     * no definition is selected.
     * 
     * @return
     */
    private ISymbol getSelectedSymbolDefinition() {
        ISymbol definition = null;

        IStructuredSelection selection = (IStructuredSelection) tableViewer
                .getSelection();
        if (selection != null) {
            definition = (ISymbol) selection.getFirstElement();
        }

        return definition;
    }

    /**
     * Return the currently selected symbol definition set or <code>null</code>
     * if no set has been selected.
     * 
     * @return
     */
    private ISymbolSet getSelectedSymbolDefinitionSet() {
        ISymbolSet set = null;

        IStructuredSelection selection = (IStructuredSelection) definitionsComboViewer
                .getSelection();

        if (selection.size() == 0) {
            set = null;
        } else {
            set = (ISymbolSet) selection.getFirstElement();
        }

        return set;
    }

    /**
     * The add set button has been selected.
     */
    private void handleAddSetButton() {
        try {
            String newSetName = definitionsComboViewer.getCombo().getText();
            ISymbolSet set = MTJCore.getSymbolSetFactory().createSymbolSet(newSetName);
            
            MTJCore.getSymbolSetRegistry().addSymbolSet(set);

            definitionsComboViewer.refresh();
            definitionsComboViewer.setSelection(new StructuredSelection(set));
            tableViewer.setInput(set);

        } catch (PersistenceException e) {
            handleException(
                    MTJUIMessages.SymbolDefinitionsPreferencePage_error_add_symbol,
                    e);
        }
    }

    /**
     * The add symbol button has been selected.
     */
    private void handleAddSymbolButton() {
        // Find a new symbol name that doesn't already exist
        // in the list
        String symbolName = "NewSymbol"; //$NON-NLS-1$
        ISymbol symbolDefinition = null;
        for (int i = 1; i < 100; i++) {
            symbolName = "NewSymbol" + i; //$NON-NLS-1$
            symbolDefinition = MTJCore.getSymbolSetFactory().createSymbol(symbolName, "true");
            if (!currentDefinitions.contains(symbolDefinition)) {
                currentDefinitions.add(symbolDefinition);
                break;
            }
        }
        tableViewer.refresh();

        TableItem[] items = tableViewer.getTable().getItems();
        int pos = 0;
        for (; pos < items.length; pos++) {
            if (symbolDefinition.equals(items[pos].getData())) {
                break;
            }
        }
        tableViewer.getTable().select(pos);
        tableViewer.getTable().forceFocus();
    }

    /**
     * An exception has occurred. Handle it appropriately.
     * 
     * @param t
     */
    private void handleException(String message, Throwable t) {
        MTJCore.log(IStatus.WARNING, message, t);
        MessageDialog
                .openError(
                        workbench.getActiveWorkbenchWindow().getShell(),
                        MTJUIMessages.SymbolDefinitionsPreferencePage_handleException_dialog_title,
                        message);
    }

    /**
     * The import set button has been selected.
     */
    private void handleImportSetButton() {

        SymbolDefinitionsImportWizard wizard = new SymbolDefinitionsImportWizard();

        WizardDialog dialog = new WizardDialog(getShell(), wizard);
        if (dialog.open() == Window.OK) {
            definitionsComboViewer.refresh();
        }
    }

    /**
     * The remove set button has been selected.
     */
    private void handleRemoveSetButton() {
        ISymbolSet set = getSelectedSymbolDefinitionSet();
        if (set != null) {
            MTJCore.getSymbolSetRegistry().removeSymbolSet(set.getName());

            definitionsComboViewer.refresh();
            tableViewer.refresh();
        }
    }

    /**
     * The remove symbol button has been selected.
     */
    private void handleRemoveSymbolButton() {
        Table table = tableViewer.getTable();
        TableItem[] selected = table.getSelection();
        for (TableItem tableItem : selected) {
            currentDefinitions.remove(tableItem.getData());
        }
        tableViewer.refresh();
    }

    /**
     * Initialize the definitions combo box and selection
     */
    private void initializeDefinitionsCombo() {

        Object inputObject = new Object(); // Instance doesn't matter for this
        // provider

        definitionsComboViewer.setInput(inputObject);
        IStructuredContentProvider contentProvider = (IStructuredContentProvider) definitionsComboViewer
                .getContentProvider();
        Object[] content = contentProvider.getElements(inputObject);

        if ((content != null) && (content.length > 0)) {
            definitionsComboViewer.setSelection(new StructuredSelection(
                    content[0]), true);
        }
    }

    /**
     * Return a boolean indicating whether the specified set name is valid.
     * 
     * @param name
     * @return
     */
    private boolean isValidSetName(String name) {
        boolean isValid = (name != null) && (name.trim().length() > 0);

        for (int i = 0; isValid && (i < name.length()); i++) {
            char c = name.charAt(i);
            isValid = (c == ' ') || (Character.isLetterOrDigit(c));
        }

        return isValid;
    }

    /**
     * Return a boolean indicating whether the specified symbol is valid.
     * 
     * @param symbol
     * @return
     */
    private boolean isValidSymbol(String symbol) {
        boolean valid = false;

        if (symbol != null) {
            Matcher matcher = WHITESPACE_PATTERN.matcher(symbol);
            valid = !matcher.matches();
        }

        return valid;
    }

    /**
     * Reload the symbol definitions registry from disk.
     */
    private boolean reloadSymbolDefinitionsRegistry() {
        boolean succeeded = true;

        try {
            MTJCore.getSymbolSetRegistry().load();
        } catch (PersistenceException e) {
            succeeded = false;
            handleException(
                    MTJUIMessages.SymbolDefinitionsPreferencePage_error_reloading_symbol_definitions,
                    e);
        }

        return succeeded;
    }

    /**
     * Update the enablement of the add/remove buttons.
     */
    private void updateButtonEnablement() {
        String typedSetName = definitionsComboViewer.getCombo().getText();
        ISymbolSet typedSet = null;
        try {
            typedSet = MTJCore.getSymbolSetRegistry().getSymbolSet(typedSetName);
        } catch (PersistenceException e) {
            // This should be safe to ignore...
        }

        ISymbolSet selectedSet = getSelectedSymbolDefinitionSet();
        addSetButton.setEnabled(isValidSetName(typedSetName)
                && (selectedSet == null) && (typedSet == null));
        removeSetButton.setEnabled(selectedSet != null);

        ISymbol selectedDefinition = getSelectedSymbolDefinition();
        addSymbolButton.setEnabled(selectedSet != null);
        removeSymbolButton.setEnabled(selectedDefinition != null);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {

        try {
            definitionSetsinput = MTJCore.getSymbolSetRegistry().getAllSymbolSets();
        } catch (PersistenceException e1) {
            handleException(
                    MTJUIMessages.SymbolDefinitionsPreferencePage_error_retrieving_symbol_definitions,
                    e1);
        }

        MTJCore.getSymbolSetRegistry()
                .addSymbolSetChangeListener(new ISymbolSetChangeListener() {

                    public void symbolSetChanged() {

                        try {
                            definitionSetsinput = MTJCore.getSymbolSetRegistry().getAllSymbolSets();
                            if ((definitionsComboViewer != null)
                                    && (!definitionsComboViewer.getControl()
                                            .isDisposed())
                                    && (workbench.getActiveWorkbenchWindow() != null)) {

                                definitionsComboViewer.refresh();
                            }
                        } catch (PersistenceException e) {
                            handleException(
                                    MTJUIMessages.SymbolDefinitionsPreferencePage_error_retrieving_symbol_definitions,
                                    e);
                        }

                    }
                });

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, true));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        Composite nameComposite = new Composite(composite, SWT.NONE);
        nameComposite.setLayout(new GridLayout(3, false));
        nameComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        new Label(nameComposite, SWT.NONE)
                .setText(MTJUIMessages.SymbolDefinitionsPreferencePage_def_set_label_text);

        definitionsComboViewer = new ComboViewer(nameComposite, SWT.DROP_DOWN);
        definitionsComboViewer
                .setContentProvider(new DefinitionSetsContentProvider());

        definitionsComboViewer
                .setLabelProvider(new DefinitionSetLabelProvider());

        definitionsComboViewer.setInput(definitionSetsinput);

        definitionsComboViewer.setSorter(new ViewerSorter());

        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.verticalAlignment = SWT.CENTER;
        definitionsComboViewer.getCombo().setLayoutData(gd);
        definitionsComboViewer.getCombo().addModifyListener(
                new ModifyListener() {
                    public void modifyText(ModifyEvent e) {
                        updateButtonEnablement();
                    }
                });

        Composite nameButtonComposite = new Composite(nameComposite, SWT.NONE);
        nameButtonComposite.setLayout(new GridLayout(1, true));

        addSetButton = new Button(nameButtonComposite, SWT.PUSH);
        addSetButton
                .setText(MTJUIMessages.SymbolDefinitionsPreferencePage_addSetButton);
        addSetButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        addSetButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleAddSetButton();
            }
        });

        removeSetButton = new Button(nameButtonComposite, SWT.PUSH);
        removeSetButton
                .setText(MTJUIMessages.SymbolDefinitionsPreferencePage_removeSetButton);
        removeSetButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        removeSetButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleRemoveSetButton();
            }
        });

        importSetButton = new Button(nameButtonComposite, SWT.PUSH);
        importSetButton
                .setText(MTJUIMessages.SymbolDefinitionsPreferencePage_ImportButton);
        importSetButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        importSetButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleImportSetButton();
            }
        });

        symbolsGroup = new Group(composite, SWT.FILL);
        symbolsGroup.setLayout(new GridLayout(2, false));
        symbolsGroup.setLayoutData(new GridData(GridData.FILL_BOTH));

        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.minimumWidth = 400;
        gridData.heightHint = 300;
        tableViewer = createTableViewer(symbolsGroup);
        tableViewer.getTable().setLayoutData(gridData);

        Composite symbolsButtonComposite = new Composite(symbolsGroup, SWT.NONE);
        symbolsButtonComposite.setLayout(new GridLayout(1, true));

        addSymbolButton = new Button(symbolsButtonComposite, SWT.PUSH);
        addSymbolButton
                .setText(MTJUIMessages.SymbolDefinitionsPreferencePage_addSymbolButton);
        addSymbolButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        addSymbolButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleAddSymbolButton();
            }
        });

        removeSymbolButton = new Button(symbolsButtonComposite, SWT.PUSH);
        removeSymbolButton
                .setText(MTJUIMessages.SymbolDefinitionsPreferencePage_removeSymbolButton);
        removeSymbolButton
                .setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        removeSymbolButton.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * @see
             * org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleRemoveSymbolButton();
            }
        });

        definitionsComboViewer
                .addSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        updateButtonEnablement();

                        ISymbolSet set = getSelectedSymbolDefinitionSet();
                        tableViewer.setInput(set);
                    }
                });

        initializeDefinitionsCombo();

        return composite;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
     */
    @Override
    protected void performDefaults() {
        // Force a reload of the symbol definitions registry
        reloadSymbolDefinitionsRegistry();

        // Refresh all of the viewers
        initializeDefinitionsCombo();
    }

}
