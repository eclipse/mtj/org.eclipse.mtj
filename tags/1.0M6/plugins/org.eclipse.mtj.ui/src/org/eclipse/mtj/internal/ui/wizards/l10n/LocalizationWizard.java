/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Adding canFinish method.
 *     David Marques (Motorola) - Refactoring Localization API.
 *     David Marques (Motorola) - Refactoring to use L10nModel.                     
 */
package org.eclipse.mtj.internal.ui.wizards.l10n;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.internal.core.l10n.L10nApi;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.ui.MTJUIMessages;

/**
 * LocalizationWizard class provides a wizard for setting up localization into a
 * java me project.
 * 
 * @since 0.9.1
 */
public class LocalizationWizard extends Wizard {

    private LocalizationPage page;
    private IJavaProject jProject;

    /**
     * Creates a LocalizationWizard instance associated to the specified
     * project.
     * 
     * @param project instance.
     */
    public LocalizationWizard(IJavaProject project) {
        super();
        jProject = project;
        setWindowTitle(MTJUIMessages.LocalizationWizard_window_title);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#addPages()
     */
    @Override
    public void addPages() {
        page = new LocalizationPage(jProject, getContainer());
        addPage(page);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#canFinish()
     */
    @Override
    public boolean canFinish() {
        IResource destination = page.getDestination();
        IJavaElement targetPack = page.getPackage();
        return (destination != null) && (targetPack != null);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        IResource properties = page.getDestination();
        IJavaElement targetPack = page.getPackage();

        boolean result = false;

        if (targetPack instanceof IPackageFragment) {
            try {
                result = createLocalizationFile(properties
                        .getProjectRelativePath(), targetPack);
                jProject.getProject().refreshLocal(IResource.DEPTH_INFINITE,
                        new NullProgressMonitor());

                result &= L10nApi.createLocalizationApi(jProject.getProject(),
                        (IPackageFragment) targetPack, properties
                                .getProjectRelativePath());
                jProject.getProject().refreshLocal(IResource.DEPTH_INFINITE,
                        new NullProgressMonitor());
            } catch (CoreException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * Creates a Localization Data file into the specified destination.
     * 
     * @param _properties destination folder.
     * @param _package Class package name.
     * @return true upon success, false otherwise.
     * @throws IOException If any error occurs during class creation.
     * @throws JavaModelException If any java model error occurs.
     */
    private boolean createLocalizationFile(IPath _properties,
            IJavaElement _package) throws JavaModelException, IOException {

        L10nModel model = new L10nModel(new Document(), false);
        L10nLocales locales = model.getLocales();
        locales.setDestination(_properties.toString());
        locales.setPackage(_package.getElementName());

        IProject project = jProject.getProject();
        IPath path = project.getLocation().append("Localization Data"); //$NON-NLS-1$
        IFile file = project.getWorkspace().getRoot().getFileForLocation(path);
        if (file == null) {
            throw new IOException(
                    MTJUIMessages.LocalizationWizard_failed_createLocalizationFile);
        }
        model.setUnderlyingResource(file);
        model.save();

        return file.exists();
    }
}
