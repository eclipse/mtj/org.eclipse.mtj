/**
 * Copyright (c) 2006,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import java.util.Set;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.mtj.internal.core.text.IEditingModel;
import org.eclipse.mtj.internal.core.text.IReconcilingParticipant;

/**
 * @since 0.9.1
 */
public interface IFoldingStructureProvider extends IReconcilingParticipant {

    /**
     * Initializes the internal structures of the FoldingStructure provider
     */
    public void initialize();

    /**
     * @param currentRegions
     * @param model
     * @throws BadLocationException
     */
    public void addFoldingRegions(Set<Position> currentRegions,
            IEditingModel model) throws BadLocationException;

}
