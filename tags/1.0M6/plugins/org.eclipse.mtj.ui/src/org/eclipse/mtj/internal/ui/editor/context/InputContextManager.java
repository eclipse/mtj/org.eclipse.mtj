/**
 * Copyright (c) 2003,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.context;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.IBaseModel;
import org.eclipse.mtj.internal.core.IModelChangeProvider;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.editor.IModelUndoManager;
import org.eclipse.mtj.internal.ui.editor.MTJFormEditor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;

/**
 * @since 0.9.1
 */
public abstract class InputContextManager implements IResourceChangeListener {

    private MTJFormEditor editor;
    private Hashtable<IEditorInput, Object> inputContexts;
    private ArrayList<IInputContextListener> listeners;
    private ArrayList<IFile> monitoredFiles;
    private IModelUndoManager undoManager;

    /**
     * @param editor
     */
    public InputContextManager(MTJFormEditor editor) {
        this.editor = editor;
        inputContexts = new Hashtable<IEditorInput, Object>();
        listeners = new ArrayList<IInputContextListener>();
        MTJUIPlugin.getWorkspace().addResourceChangeListener(this,
                IResourceChangeEvent.POST_CHANGE);
    }

    /**
     * @param listener
     */
    public void addInputContextListener(IInputContextListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    /**
     * 
     */
    public void dispose() {

        MTJUIPlugin.getWorkspace().removeResourceChangeListener(this);

        // dispose input contexts
        for (Enumeration<Object> contexts = inputContexts.elements(); contexts
                .hasMoreElements();) {
            InputContext context = (InputContext) contexts.nextElement();
            unhookUndo(context);
            context.dispose();
        }

        inputContexts.clear();
        undoManager = null;
    }

    /**
     * @param resource
     * @return
     */
    public InputContext findContext(IResource resource) {

        for (Enumeration<Object> contexts = inputContexts.elements(); contexts
                .hasMoreElements();) {
            InputContext context = (InputContext) contexts.nextElement();
            if (context.matches(resource)) {
                return context;
            }
        }
        return null;
    }

    /**
     * @param id
     * @return
     */
    public InputContext findContext(String id) {
        for (Enumeration<Object> contexts = inputContexts.elements(); contexts
                .hasMoreElements();) {
            InputContext context = (InputContext) contexts.nextElement();
            if (context.getId().equals(id)) {
                return context;
            }
        }
        return null;
    }

    /**
     * @return
     */
    public abstract IBaseModel getAggregateModel();

    /**
     * @return
     */
    public IProject getCommonProject() {
        for (Enumeration<Object> contexts = inputContexts.elements(); contexts
                .hasMoreElements();) {
            InputContext context = (InputContext) contexts.nextElement();
            IEditorInput input = context.getInput();
            if (input instanceof IFileEditorInput) {
                return ((IFileEditorInput) input).getFile().getProject();
            }
        }
        return null;
    }

    /**
     * @param input
     * @return
     */
    public InputContext getContext(IEditorInput input) {
        return (InputContext) inputContexts.get(input);
    }

    /**
     * @return
     */
    public InputContext[] getInvalidContexts() {
        ArrayList<InputContext> result = new ArrayList<InputContext>();
        for (Enumeration<Object> contexts = inputContexts.elements(); contexts
                .hasMoreElements();) {
            InputContext context = (InputContext) contexts.nextElement();
            if (context.isModelCorrect() == false) {
                result.add(context);
            }
        }
        return result.toArray(new InputContext[result.size()]);
    }

    /**
     * @return
     */
    public InputContext getPrimaryContext() {
        for (Enumeration<Object> contexts = inputContexts.elements(); contexts
                .hasMoreElements();) {
            InputContext context = (InputContext) contexts.nextElement();
            if (context.isPrimary()) {
                return context;
            }
        }
        return null;
    }

    /**
     * @return Returns the undoManager.
     */
    public IModelUndoManager getUndoManager() {
        return undoManager;
    }

    /**
     * @param id
     * @return
     */
    public boolean hasContext(String id) {
        return findContext(id) != null;
    }

    /**
     * @return
     */
    public boolean isDirty() {
        for (Enumeration<Object> contexts = inputContexts.elements(); contexts
                .hasMoreElements();) {
            InputContext context = (InputContext) contexts.nextElement();
            if (context.mustSave()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param file
     */
    public void monitorFile(IFile file) {
        if (monitoredFiles == null) {
            monitoredFiles = new ArrayList<IFile>();
        }
        monitoredFiles.add(file);
    }

    /**
     * @param input
     * @param context
     */
    public void putContext(IEditorInput input, InputContext context) {
        inputContexts.put(input, context);
        fireContextChange(context, true);
    }

    /**
     * 
     */
    public void redo() {
        if ((undoManager != null) && undoManager.isRedoable()) {
            undoManager.redo();
        }
    }

    /**
     * @param listener
     */
    public void removeInputContextListener(IInputContextListener listener) {
        listeners.remove(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IResourceChangeListener#resourceChanged(org.eclipse.core.resources.IResourceChangeEvent)
     */
    public void resourceChanged(IResourceChangeEvent event) {
        IResourceDelta delta = event.getDelta();

        try {
            delta.accept(new IResourceDeltaVisitor() {
                public boolean visit(IResourceDelta delta) {
                    int kind = delta.getKind();
                    IResource resource = delta.getResource();
                    if (resource instanceof IFile) {
                        if (kind == IResourceDelta.ADDED) {
                            asyncStructureChanged((IFile) resource, true);
                        } else if (kind == IResourceDelta.REMOVED) {
                            asyncStructureChanged((IFile) resource, false);
                        }
                        return false;
                    }
                    return true;
                }
            });
        } catch (CoreException e) {
            MTJCore.log(IStatus.ERROR, e);
        }
    }

    /**
     * Saves dirty contexts.
     * 
     * @param monitor
     */
    public void save(IProgressMonitor monitor) {
        for (Enumeration<Object> contexts = inputContexts.elements(); contexts
                .hasMoreElements();) {
            InputContext context = (InputContext) contexts.nextElement();
            if (context.mustSave()) {
                context.doSave(monitor);
            }
        }
    }

    /**
     * @param monitor
     * @param contextID
     * @throws Exception
     */
    public void saveAs(IProgressMonitor monitor, String contextID)
            throws Exception {
        // Find the existing context
        InputContext inputContext = findContext(contextID);
        if (inputContext != null) {
            // Keep the old editor input
            IEditorInput oldInput = editor.getEditorInput();
            // Perform the save as operation
            inputContext.doSaveAs(monitor);
            // Get the new editor input
            IEditorInput newInput = inputContext.getInput();
            // Update the context manager accordingly
            updateInputContext(newInput, oldInput);
        } else {
            throw new Exception(MTJUIMessages.InputContextManager_saveAsInputContextNull);
        }
    }

    /**
     * @param undoManager The undoManager to set.
     */
    public void setUndoManager(IModelUndoManager undoManager) {
        this.undoManager = undoManager;
    }

    /**
     * 
     */
    public void undo() {
        if ((undoManager != null) && undoManager.isUndoable()) {
            undoManager.undo();
        }
    }

    /**
     * @param file
     * @param added
     */
    private void asyncStructureChanged(final IFile file, final boolean added) {

        if ((editor == null) || (editor.getEditorSite() == null)) {
            return;
        }
        Shell shell = editor.getEditorSite().getShell();
        Display display = shell != null ? shell.getDisplay() : Display
                .getDefault();

        display.asyncExec(new Runnable() {
            public void run() {
                structureChanged(file, added);
            }
        });

    }

    /**
     * @param context
     */
    private void hookUndo(InputContext context) {

        if (undoManager == null) {
            return;
        }
        IBaseModel model = context.getModel();
        if (model instanceof IModelChangeProvider) {
            undoManager.connect((IModelChangeProvider) model);
        }

    }

    /**
     * @param file
     */
    private void removeContext(IFile file) {

        for (Enumeration<Object> contexts = inputContexts.elements(); contexts
                .hasMoreElements();) {
            InputContext context = (InputContext) contexts.nextElement();
            IEditorInput input = context.getInput();
            if (input instanceof IFileEditorInput) {
                IFileEditorInput fileInput = (IFileEditorInput) input;
                if (file.equals(fileInput.getFile())) {
                    inputContexts.remove(input);
                    fireContextChange(context, false);
                    return;
                }
            }
        }

    }

    /**
     * @param file
     * @param added
     */
    private void structureChanged(IFile file, boolean added) {

        if (monitoredFiles == null) {
            return;
        }
        for (int i = 0; i < monitoredFiles.size(); i++) {
            IFile ifile = monitoredFiles.get(i);
            if (ifile.equals(file)) {
                if (added) {
                    fireStructureChange(file, true);
                } else {
                    fireStructureChange(file, false);
                    removeContext(file);
                }
            }
        }

    }

    /**
     * @param context
     */
    private void unhookUndo(InputContext context) {

        if (undoManager == null) {
            return;
        }
        IBaseModel model = context.getModel();
        if (model instanceof IModelChangeProvider) {
            undoManager.disconnect((IModelChangeProvider) model);
        }

    }

    /**
     * Update the key (the editor input in this case) associated with the input
     * context without firing a context change event. Used for save as
     * operations.
     * 
     * @param newInput
     * @param oldInput
     * @throws Exception
     */
    private void updateInputContext(IEditorInput newInput, IEditorInput oldInput)
            throws Exception {

        Object value = null;
        // Retrieve the input context referenced by the old editor input and
        // remove it from the context manager
        if (inputContexts.containsKey(oldInput)) {
            value = inputContexts.remove(oldInput);
        } else {
            throw new Exception(MTJUIMessages.InputContextManager_updateInputContextNull);
        }
        // Re-insert the input context back into the context manager using the
        // new editor input as its key
        inputContexts.put(newInput, value);

    }

    /**
     * @param context
     * @param added
     */
    protected void fireContextChange(InputContext context, boolean added) {

        for (int i = 0; i < listeners.size(); i++) {
            IInputContextListener listener = listeners.get(i);
            if (added) {
                listener.contextAdded(context);
            } else {
                listener.contextRemoved(context);
            }
        }
        if (added) {
            hookUndo(context);
        } else {
            unhookUndo(context);
        }

    }

    /**
     * @param file
     * @param added
     */
    protected void fireStructureChange(IFile file, boolean added) {

        for (int i = 0; i < listeners.size(); i++) {
            IInputContextListener listener = listeners.get(i);
            if (added) {
                listener.monitoredFileAdded(file);
            } else {
                listener.monitoredFileRemoved(file);
            }
        }

    }
}
