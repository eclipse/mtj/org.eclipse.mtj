/**
 * Copyright (c) 2003, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.ui.editor.context.InputContext;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.IFormPage;

/**
 * @since 0.9.1
 */
public abstract class MultiSourceEditor extends MTJFormEditor {

    /**
     * @param contextId
     */
    protected void addSourcePage(String contextId) {
        InputContext context = inputContextManager.findContext(contextId);
        if (context == null) {
            return;
        }
        MTJSourcePage sourcePage;
        // Don't duplicate
        if (findPage(contextId) != null) {
            return;
        }
        sourcePage = createSourcePage(this, contextId, context.getInput()
                .getName(), context.getId());
        sourcePage.setInputContext(context);
        try {
            addPage(sourcePage, context.getInput());
        } catch (PartInitException e) {
            MTJCore.log(IStatus.ERROR, e);
        }
    }

    /**
     * @param editor
     * @param title
     * @param name
     * @param contextId
     * @return
     */
    protected MTJSourcePage createSourcePage(MTJFormEditor editor,
            String title, String name, String contextId) {
        return new GenericSourcePage(editor, title, name);
    }

    /**
     * @param pageId
     */
    protected void removePage(String pageId) {
        IFormPage page = findPage(pageId);
        if (page == null) {
            return;
        }
        if (page.isDirty()) {
            // need to ask the user about this
        } else {
            removePage(page.getIndex());
            if (!page.isEditor()) {
                page.dispose();
            }
        }
    }
}
