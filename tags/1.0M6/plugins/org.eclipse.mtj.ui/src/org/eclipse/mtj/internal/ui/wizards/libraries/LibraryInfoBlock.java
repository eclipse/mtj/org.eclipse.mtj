/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.internal.ui.wizards.libraries;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.mtj.internal.core.externallibrary.model.IExternalLibrary;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.preferences.ScrolledPageContent;
import org.eclipse.mtj.internal.ui.util.PixelConverter;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

/**
 * Displays a set of available information about the Library selected in the
 * {@link LibrarySelectionBlock}.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class LibraryInfoBlock implements ISelectionChangedListener {

    private Device device;

    private String fontName;
    private int height;
    /**
     * The main composite were the library information is displayed.
     */
    private Composite libraryInfoComposite;

    /**
     * Creates the SWT control for this Block under the given parent control.
     * <p>
     * Clients should call this method.
     * </p>
     * 
     * @param parent the parent control
     * @return the composite were the library information is displayed.
     */
    public Control createControl(Composite parent) {

        if (parent == null) {
            return null;
        }

        libraryInfoComposite = new Composite(parent, SWT.NONE);

        GridData gridData = new GridData(GridData.FILL_BOTH);
        PixelConverter converter = new PixelConverter(parent);
        gridData.heightHint = converter.convertHeightInCharsToPixels(12);
        gridData.widthHint = converter.convertWidthInCharsToPixels(25);
        GridLayout gridLayout = new GridLayout();
        gridLayout.marginWidth = 0;
        gridLayout.marginHeight = 0;
        libraryInfoComposite.setLayout(gridLayout);
        libraryInfoComposite.setLayoutData(gridData);
        libraryInfoComposite.setData(null);

        device = libraryInfoComposite.getFont().getDevice();
        fontName = libraryInfoComposite.getFont().getFontData()[0].getName();
        height = libraryInfoComposite.getFont().getFontData()[0].getHeight();
        return libraryInfoComposite;
    }

    /**
     * Returns the SWT control for this page.
     * 
     * @return the SWT control for this page, or <code>null</code> if this page
     *         does not have a control yet.
     */
    public Control getControl() {
        return libraryInfoComposite;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
     */
    public void selectionChanged(SelectionChangedEvent event) {
        if (event.getSelection() instanceof StructuredSelection) {
            handlePostSelectionChange((StructuredSelection) event
                    .getSelection());
        } else {
            handlePostSelectionChange(StructuredSelection.EMPTY);
        }
    }

    /**
     * @param parent
     * @param topLabelText
     * @param urlTextValue
     * @param selectionListener
     */
    private void createLinkItem(Composite parent, String topLabelText,
            String urlTextValue, Listener selectionListener) {
        Label libraryLicenseURILabel = new Label(parent, SWT.BOLD);
        libraryLicenseURILabel.setText(topLabelText);
        libraryLicenseURILabel.setFont(new Font(device, fontName, height,
                SWT.BOLD));
        Link libraryLicenseURI = new Link(parent, SWT.BOLD);
        libraryLicenseURI.setText(NLS.bind(
                MTJUIMessages.LibraryInfoBlock_libraryLicenseURI_href,
                urlTextValue));

        libraryLicenseURI.addListener(SWT.Selection, selectionListener);
    }

    /**
     * @param parent
     * @param topLabelText
     * @param textValue
     */
    private void createTextItem(Composite parent, String topLabelText,
            String textValue) {

        Label topLabel = new Label(parent, SWT.BOLD);
        topLabel.setText(topLabelText);
        topLabel.setFont(new Font(device, fontName, height, SWT.BOLD));
        Text textValueLabel = new Text(parent, SWT.BOLD | SWT.WRAP
                | SWT.READ_ONLY);
        textValueLabel.setText(textValue);
    }

    /**
     * Fill the {@link #libraryInfoComposite} composite with the selected
     * library information.
     * 
     * @param selection the selection element containing the selected library.
     */
    private void handlePostSelectionChange(StructuredSelection selection) {

        if (libraryInfoComposite == null) {
            return;
        }

        // Get the child composite of the top composite
        Composite childComposite = (Composite) libraryInfoComposite.getData();

        // Dispose old composite (if necessary)
        if ((childComposite != null) && (childComposite.getParent() != null)) {
            childComposite.getParent().dispose();
        }

        PixelConverter converter = new PixelConverter(libraryInfoComposite);

        // Create new composite
        ScrolledPageContent spc = new ScrolledPageContent(libraryInfoComposite,
                SWT.V_SCROLL);
        spc.getVerticalBar().setIncrement(5);

        GridData gridData = new GridData(GridData.FILL_BOTH);

        gridData.heightHint = converter.convertHeightInCharsToPixels(12);
        gridData.widthHint = converter.convertWidthInCharsToPixels(25);
        spc.setLayoutData(gridData);

        childComposite = spc.getBody();
        TableWrapLayout tableWrapLayout = new TableWrapLayout();
        tableWrapLayout.leftMargin = 0;
        tableWrapLayout.rightMargin = 0;
        childComposite.setLayout(tableWrapLayout);
        gridData = new GridData(GridData.FILL_BOTH);
        gridData.heightHint = converter.convertHeightInCharsToPixels(12);
        gridData.widthHint = converter.convertWidthInCharsToPixels(25);
        childComposite.setLayoutData(gridData);

        libraryInfoComposite.setData(childComposite);

        IExternalLibrary library = (IExternalLibrary) selection.getFirstElement();

        if (library != null) {

            createTextItem(childComposite,
                    MTJUIMessages.LibraryInfoBlock_libraryNameLabel, library
                            .getName());

            createTextItem(childComposite,
                    MTJUIMessages.LibraryInfoBlock_libraryVersionLabel, library
                            .getVersion().toString());

            createTextItem(childComposite,
                    MTJUIMessages.LibraryInfoBlock_libraryDescriptionLabel,
                    library.getDescription());

            createLinkItem(childComposite,
                    MTJUIMessages.LibraryInfoBlock_libraryLicenseURILabel,
                    library.getLicence().getUri().toASCIIString(),
                    new Listener() {

                        /* (non-Javadoc)
                         * @see org.eclipse.swt.widgets.Listener#handleEvent(org.eclipse.swt.widgets.Event)
                         */
                        public void handleEvent(Event event) {
                            try {
                                PlatformUI.getWorkbench().getBrowserSupport()
                                        .getExternalBrowser().openURL(
                                                new URL(event.text));
                            } catch (PartInitException e) {
                                e.printStackTrace();
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                        }
                    });

            createTextItem(childComposite,
                    MTJUIMessages.LibraryInfoBlock_protection_domain_label,
                    library.getSecurity().getProtectionDomain()
                            .getProtectionDomainType().toString().toLowerCase());

            createTextItem(childComposite,
                    MTJUIMessages.LibraryInfoBlock_permissions_label, library
                            .getSecurity().getPermissionList().toString());
        }

        libraryInfoComposite.layout(true);
    }
}
