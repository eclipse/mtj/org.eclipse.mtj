/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Gang Ma (Sybase)	        - Rename the label "Group:" to "SDK:"
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874] 
 */
package org.eclipse.mtj.internal.ui.devices;

import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceRegistryListener;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.preferences.DeviceManagementPreferencePage;
import org.eclipse.mtj.internal.ui.viewers.LabelProviderViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

/**
 * A device selector provides user interface functionality for selecting a
 * device from the registry.
 * 
 * @author Craig Setera
 */
public class DeviceSelector {

    /**
     * The content providers for the device viewer.
     */
    private class DeviceContentProvider implements IStructuredContentProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            if (inputElement != null) {
                String groupName = (String) inputElement;

                try {
                    List<IDevice> deviceList = MTJCore.getDeviceRegistry()
                            .getDevices(groupName);
                    if (deviceList != null) {
                        elements = deviceList.toArray(new Object[0]);
                    }
                } catch (PersistenceException e) {
                    MTJCore
                            .log(
                                    IStatus.WARNING,
                                    MTJUIMessages.DeviceSelector_DeviceContentProvider_error_getElements,
                                    e);
                }
            }

            return elements;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    /**
     * A label provider implementation for the Device Group
     */
    private class DeviceGroupNameLabelProvider extends LabelProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
         */
        @Override
        public String getText(Object element) {
            return (String) element;
        }
    }

    /**
     * Content provider wrapped around the device groups in the registry
     * 
     * @author Craig Setera
     */
    private class DeviceGroupsContentProvider implements
            IStructuredContentProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            try {
                List<String> groups = MTJCore.getDeviceRegistry()
                        .getDeviceGroups();
                elements = groups.toArray(new Object[groups.size()]);
            } catch (PersistenceException e) {
                MTJCore
                        .log(
                                IStatus.WARNING,
                                MTJUIMessages.DeviceSelector_DeviceGroupsContentProvider_error_getElements,
                                e);
            }

            return elements;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    /**
     * A label provider implementation for the Device Label
     */
    private class DeviceLabelProvider extends LabelProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
         */
        @Override
        public String getText(Object element) {
            IDevice device = (IDevice) element;
            return device.getName();
        }
    }

    /**
     * The Device Group Label
     */
    private String deviceGroupComentLabel = null;

    /**
     * The Device Group Label
     */
    private String deviceGroupLabel = MTJUIMessages.DeviceSelector_deviceGroupLabel;

    private ComboViewer deviceNamesViewer;

    private ComboViewer deviceViewer;

    private boolean fireSelectionChangedListener = true;

    private ComboViewer groupViewer;

    private Button manageDevicesButton;

    private final Object[] NO_ELEMENTS = new Object[0];

    // Listener for changes in the registry
    private IDeviceRegistryListener registryListener;
    private ISelectionChangedListener selectionChangedListener;
    private ComboViewer viewer;

    /**
     * Fill in the user interface components into the parent. Specify whether to
     * use a device group box in the layout.
     * 
     * @param parent
     * @param includeGroup
     * @param includeManageButton TODO
     */
    public void createContents(Composite parent, boolean includeGroup,
            boolean includeManageButton) {
        Composite composite = null;

        if (includeGroup) {
            Group group = new Group(parent, SWT.NONE);
            group.setText(deviceGroupLabel);

            composite = group;
        } else {
            composite = new Composite(parent, SWT.NONE);
        }

        setCompositeLayout(parent, composite);
        if (deviceGroupComentLabel != null) {
            (new Label(composite, SWT.NONE)).setText(deviceGroupComentLabel);
            new Label(composite, SWT.NONE);
        }

        createSelectionControls(composite);
        if (includeManageButton) {
            createDeviceManagementControls(composite);
        }

        addRegistryListener();
        enableFireSelectionChanged(false);
        setInitialState();
        enableFireSelectionChanged(true);
    }

    /**
     * Dispose as necessary.
     */
    public void dispose() {
        MTJCore.getDeviceRegistry().removeRegistryListener(registryListener);
    }

    /**
     * @param fireSelectionChangedListener the fireSelectionChangedListener to
     *            set
     */
    public void enableFireSelectionChanged(boolean fireSelectionChangedListener) {
        this.fireSelectionChangedListener = fireSelectionChangedListener;
    }

    /**
     * @return the deviceGroupComentLabel
     */
    public String getDeviceGroupComentLabel() {
        return deviceGroupComentLabel;
    }

    /**
     * @return the deviceGroupLabel
     */
    public String getDeviceGroupLabel() {
        return deviceGroupLabel;
    }

    /**
     * Return the device selected by the user or <code>null</code> if the user
     * has not yet selected a device.
     * 
     * @return
     */
    public IDevice getSelectedDevice() {
        IStructuredSelection selection = (IStructuredSelection) deviceViewer
                .getSelection();
        return (IDevice) selection.getFirstElement();
    }

    /**
     * @return the fireSelectionChangedListener
     */
    public boolean isFireSelectionChangedEnabled() {
        return fireSelectionChangedListener;
    }

    /**
     * @param deviceGroupComentLabel the deviceGroupComentLabel to set
     */
    public void setDeviceGroupComentLabel(String deviceGroupComentLabel) {
        this.deviceGroupComentLabel = deviceGroupComentLabel;
    }

    /**
     * @param deviceGroupLabel the deviceGroupLabel to set
     */
    public void setDeviceGroupLabel(String deviceGroupLabel) {
        this.deviceGroupLabel = deviceGroupLabel;
    }

    /**
     * Set the controls to the specified state of enablement.
     * 
     * @param enabled
     */
    public void setEnabled(boolean enabled) {
        manageDevicesButton.setEnabled(enabled);
        groupViewer.getCombo().setEnabled(enabled);
        deviceViewer.getCombo().setEnabled(enabled);
    }

    /**
     * Select the specified device within the selector if possible. This method
     * is not guaranteed to make a valid selection depending on the state of the
     * device registry.
     * 
     * @param device
     */
    public void setSelectedDevice(IDevice device) {
        IDevice currentlySelected = getSelectedDevice();

        if (!areDevicesEqual(device, currentlySelected)) {
            if (device != null) {
                setViewerSelection(groupViewer, device.getSDKName());
                setViewerSelection(deviceViewer, device);
            } else {
                groupViewer.setSelection(null);
                deviceViewer.setSelection(null);
            }
        }
    }

    /**
     * Set the listener for changes in the device selection.
     * 
     * @param listener
     */
    public void setSelectionChangedListener(ISelectionChangedListener listener) {
        selectionChangedListener = listener;
    }

    /**
     * Add a listener for updated in the device registry.
     */
    private void addRegistryListener() {
        registryListener = new IDeviceRegistryListener() {

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.model.device.IDeviceRegistryListener#deviceAdded(org.eclipse.mtj.core.model.device.IDevice)
             */
            public void deviceAdded(IDevice device) {

                setInitialState();

                if ((viewer != null) && (!viewer.getControl().isDisposed())) {
                    viewer.refresh();
                }

                if ((deviceNamesViewer != null)
                        && (!deviceNamesViewer.getControl().isDisposed())) {
                    deviceNamesViewer.refresh();
                }

            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.model.device.IDeviceRegistryListener#deviceRemoved(org.eclipse.mtj.core.model.device.IDevice)
             */
            public void deviceRemoved(IDevice device) {
                IMIDPDevice current = (IMIDPDevice) getSelectedDevice();
                if ((current != null) && (current == device)) {
                    setInitialState();
                }

                if ((viewer != null) && (!viewer.getControl().isDisposed())) {
                    viewer.refresh();
                }

                if ((deviceNamesViewer != null)
                        && (!deviceNamesViewer.getControl().isDisposed())) {
                    deviceNamesViewer.refresh();
                }
            }
        };

        MTJCore.getDeviceRegistry().addRegistryListener(registryListener);
    }

    /**
     * Return a boolean indicating equality, while accounting for nulls.
     * 
     * @param device1
     * @param device2
     * @return
     */
    private boolean areDevicesEqual(IDevice device1, IDevice device2) {
        boolean equal = false;

        if ((device1 == null) && (device2 == null)) {
            equal = true;
        } else if ((device1 != null) && (device2 != null)) {
            equal = device1.equals(device2);
        }

        return equal;
    }

    /**
     * Return a boolean indicating whether there are any devices registered.
     * 
     * @return
     * @throws PersistenceException
     */
    private boolean areDevicesRegistered() throws PersistenceException {
        return (MTJCore.getDeviceRegistry().getDeviceCount() > 0);
    }

    /**
     * Create the device groups combo viewer.
     * 
     * @param parent
     * @param styles
     * @return
     */
    private ComboViewer createDeviceGroupsViewer(Composite parent, int styles) {
        viewer = new ComboViewer(parent, styles);
        viewer.setContentProvider(new DeviceGroupsContentProvider());
        viewer.setLabelProvider(new DeviceGroupNameLabelProvider());
        viewer.setSorter(new LabelProviderViewerSorter());
        viewer.setInput(new Object());

        return viewer;
    }

    /**
     * Create the controls to manage devices.
     * 
     * @param composite
     */
    private void createDeviceManagementControls(Composite composite) {
        Composite manageComposite = new Composite(composite, SWT.NONE);
        manageComposite.setLayout(new GridLayout(1, true));
        GridData compositeData = new GridData();
        compositeData.verticalAlignment = SWT.CENTER;
        manageComposite.setLayoutData(compositeData);

        manageDevicesButton = new Button(manageComposite, SWT.PUSH);
        manageDevicesButton
                .setText(MTJUIMessages.DeviceSelector_manageDevicesButton_label);
        manageDevicesButton.addSelectionListener(new SelectionAdapter() {

            /* (non-Javadoc)
             * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                Shell shell = e.widget.getDisplay().getActiveShell();
                PreferenceManager manager = PlatformUI.getWorkbench()
                        .getPreferenceManager();
                PreferenceDialog dialog = new PreferenceDialog(shell, manager);
                dialog.setSelectedNode(DeviceManagementPreferencePage.ID);
                dialog.open();
            }
        });
    }

    /**
     * Create the device names combo viewer.
     * 
     * @param parent
     * @param styles
     * @return
     */
    private ComboViewer createDeviceNamesViewer(Composite parent, int styles) {
        deviceNamesViewer = new ComboViewer(parent, styles);
        deviceNamesViewer.setContentProvider(new DeviceContentProvider());
        deviceNamesViewer.setLabelProvider(new DeviceLabelProvider());
        deviceNamesViewer.setSorter(new LabelProviderViewerSorter());

        return deviceNamesViewer;
    }

    /**
     * Create the device selection controls.
     * 
     * @param composite
     */
    private void createSelectionControls(Composite composite) {
        Composite comboComposite = new Composite(composite, SWT.NONE);
        comboComposite.setLayout(new GridLayout(2, false));
        comboComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

        (new Label(comboComposite, SWT.NONE))
                .setText(MTJUIMessages.DeviceSelector_4);
        groupViewer = createDeviceGroupsViewer(comboComposite, SWT.READ_ONLY);
        groupViewer.getCombo().setLayoutData(
                new GridData(GridData.FILL_HORIZONTAL));
        groupViewer
                .addSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        deviceGroupChanged();
                        if ((selectionChangedListener != null)
                                && fireSelectionChangedListener) {
                            selectionChangedListener.selectionChanged(event);
                        }
                    }
                });

        (new Label(comboComposite, SWT.NONE))
                .setText(MTJUIMessages.DeviceSelector_device_label);
        deviceViewer = createDeviceNamesViewer(comboComposite, SWT.READ_ONLY);
        deviceViewer.getCombo().setLayoutData(
                new GridData(GridData.FILL_HORIZONTAL));
        deviceViewer
                .addSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        if ((selectionChangedListener != null)
                                && fireSelectionChangedListener) {
                            selectionChangedListener.selectionChanged(event);
                        }
                    }
                });
    }

    /**
     * The device group has been changed. We need to update the available
     * devices.
     */
    private void deviceGroupChanged() {
        groupViewer.refresh();

        IStructuredSelection selection = (IStructuredSelection) groupViewer
                .getSelection();
        String groupName = (String) selection.getFirstElement();
        deviceViewer.setInput(groupName);

        try {
            List<IDevice> groupDevices = MTJCore.getDeviceRegistry()
                    .getDevices(groupName);
            if ((groupDevices != null) && (groupDevices.size() > 0)) {
                deviceViewer.getCombo().select(0);
            }
        } catch (PersistenceException e) {
            MTJCore
                    .log(
                            IStatus.WARNING,
                            MTJUIMessages.DeviceSelector_deviceGroupChanged_error_retrieving_devices
                                    + groupName, e);
        }
    }

    /**
     * Return the device to be selected by the viewers based on a number of
     * factors.
     * 
     * @return
     * @throws PersistenceException
     */
    private IDevice getDeviceToSelect() throws PersistenceException {
        IDevice device = null;

        if (areDevicesRegistered()) {
            // Attempt to find a device to use in setting up the
            // current selection.
            device = getSelectedDevice();

            if (device == null) {
                device = MTJCore.getDeviceRegistry().getDefaultDevice();
            }

            if (device == null) {
                List<IDevice> allDevices = MTJCore.getDeviceRegistry()
                        .getAllDevices();
                device = allDevices.get(0);
            }
        }

        return device;
    }

    /**
     * Match the child layout to the parent layout, filling all available
     * columns.
     * 
     * @param parent
     * @param child
     * @return
     */
    private void setCompositeLayout(Composite parent, Composite child) {
        // Force the group to take up all of the columns in the layout
        int columns = 1;
        Object layout = parent.getLayout();
        if (layout instanceof GridLayout) {
            columns = ((GridLayout) layout).numColumns;
        }

        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = columns;
        child.setLayoutData(gd);
        child.setLayout(new GridLayout(2, false));
    }

    /**
     * Set the initial state of the combo viewers.
     */
    private void setInitialState() {
        try {
            IDevice device = getDeviceToSelect();

            setSelectedDevice(device);

        } catch (PersistenceException e) {
            MTJCore
                    .log(
                            IStatus.WARNING,
                            MTJUIMessages.DeviceSelector_setInitialState_error_retrieving_devices,
                            e);
        }
    }

    /**
     * Set the selection into the specified viewer.
     * 
     * @param viewer
     * @param selectedObject
     */
    private void setViewerSelection(Viewer viewer, Object selectedObject) {
        StructuredSelection selection = new StructuredSelection(selectedObject);
        viewer.setSelection(selection, true);
    }
}
