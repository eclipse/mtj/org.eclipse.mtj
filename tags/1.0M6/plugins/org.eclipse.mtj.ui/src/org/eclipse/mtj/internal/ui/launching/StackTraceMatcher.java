/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.ui.launching;

import java.util.Map;
import java.util.Stack;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.ui.console.IConsole;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IRegion;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.launching.StackTraceEntry;
import org.eclipse.mtj.internal.core.launching.StackTraceParser;
import org.eclipse.mtj.internal.core.launching.StackTraceParserException;
import org.eclipse.ui.console.IPatternMatchListener;
import org.eclipse.ui.console.PatternMatchEvent;
import org.eclipse.ui.console.TextConsole;

/**
 * StackTraceMatcher class finds matches on consoles 
 * for stack traces. Every time it matches a stack trace
 * it parses the stack into a lined stack.
 */
public abstract class StackTraceMatcher implements IPatternMatchListener{

    protected MTJConsoleLineTracker tracker;
    protected StackTraceParser      parser;
    
    /**
     * Creates a MTJOffsetedStackTraceMatcher instance associated to
     * the specified line tracker.
     * 
     * @param _tracker MTJConsoleLineTracker instance.
     * @param stackTraceParser 
     */
    public StackTraceMatcher(MTJConsoleLineTracker _tracker, StackTraceParser _parser) {
        this.tracker = _tracker;
        this.parser  = _parser;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.ui.console.IPatternMatchListenerDelegate#matchFound(org.eclipse.ui.console.PatternMatchEvent)
     */
    public void matchFound(PatternMatchEvent event) {
        try {
            Stack<StackTraceEntry> stack = new Stack<StackTraceEntry>();

            String trace = this.tracker.getConsole().getDocument()
                    .get(event.getOffset(), event.getLength());
            String lines[] = trace.split(System.getProperty("line.separator")); //$NON-NLS-1$
            
            Map<String, StackTraceEntry> cachedTrace = this.tracker.getCachedTrace();
            for (int i = 0; i < lines.length; i++) {
                synchronized (cachedTrace) {                
                    StackTraceEntry entry = cachedTrace.get(lines[i]);
                    if (entry == null) {
                        entry = new StackTraceEntry(lines[i]);
                        cachedTrace.put(lines[i], entry);
                    }
                    stack.push(entry);
                }
            }
            Stack<StackTraceEntry> linedStack = this.parser.parseStackTrace(stack);
            IConsole console = this.tracker.getConsole();
            while (!linedStack.isEmpty()) {
                                StackTraceEntry stackEntry = linedStack.pop();
                                for (IRegion region : stackEntry.getRegions()) {
                                        MTJConsoleHyperLink link = new MTJConsoleHyperLink(stackEntry);
                        console.addLink(link, region.getOffset(), region.getLength());
                                }
                        }
            
        } catch (BadLocationException e) {
                MTJCore.log(IStatus.ERROR, e.getMessage(), e);
        } catch (StackTraceParserException e) {
                MTJCore.log(IStatus.ERROR, e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.console.IPatternMatchListener#getPattern()
     */
    public abstract String getPattern();

    /* (non-Javadoc)
     * @see org.eclipse.ui.console.IPatternMatchListenerDelegate#connect(org.eclipse.ui.console.TextConsole)
     */
    public void connect(TextConsole console) {}

    /* (non-Javadoc)
     * @see org.eclipse.ui.console.IPatternMatchListenerDelegate#disconnect()
     */
    public void disconnect() {}
    
    /* (non-Javadoc)
     * @see org.eclipse.ui.console.IPatternMatchListener#getCompilerFlags()
     */
    public int getCompilerFlags() {
        return 0;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.console.IPatternMatchListener#getLineQualifier()
     */
    public String getLineQualifier() {
        return null;
    }

}
