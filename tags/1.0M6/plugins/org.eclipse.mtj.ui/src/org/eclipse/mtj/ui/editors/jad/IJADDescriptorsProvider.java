/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma 		(Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.editors.jad;

import org.eclipse.mtj.core.project.midp.DescriptorPropertyDescription;

/**
 * Instances of IJADDescriptorsProvider are used to provide JAD Attributes
 * descriptors. The vendors who want to provide their own JAD attributes should
 * implement this interface. The implementations are provided to the system via
 * the <code>jadAttributes</code> extension point.
 * 
 * @since 1.0
 */
public interface IJADDescriptorsProvider {

    /**
     * @return array off DescriptorPropertyDescription
     */
    DescriptorPropertyDescription[] getDescriptorPropertyDescriptions();
}
