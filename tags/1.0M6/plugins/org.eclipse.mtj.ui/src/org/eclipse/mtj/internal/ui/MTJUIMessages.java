/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 *     David Marques(Motorola) - Add midlet templates     
 */
package org.eclipse.mtj.internal.ui;

import org.eclipse.osgi.util.NLS;

/**
 * MTJUIMessages contains all messages that can be localized babel.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class MTJUIMessages extends NLS {

    public static String AbstractCreatePackageAction_createPackageForProject_dialog_message;

    public static String AbstractCreatePackageAction_doPackageCreation_task_name;
    public static String AbstractCreatePackageAction_error_createPackageForProject;
    public static String AbstractCreatePackageAction_error_creating_package;
    public static String AbstractCreatePackageAction_warnAboutInvalidDevice_dialog_title;
    public static String AbstractCreatePackageAction_warnAboutInvalidDevice_message;
    public static String AbstractCreatePackageAction_warnAboutProguardConfiguration__configure_btn;
    public static String AbstractCreatePackageAction_warnAboutProguardConfiguration_dialog_title;
    public static String AbstractCreatePackageAction_warnAboutProguardConfiguration_message;
    public static String AntennaBuildExportAction_export_task_name;

    public static String AntennaBuildExportAction_LogAndDisplaySafeRunnable_action_name;
    public static String AntennaBuildExportAction_warnUserAboutConfigurationError_btn_labl_text;
    public static String AntennaBuildExportAction_warnUserAboutConfigurationError_message;
    public static String AntennaBuildExportAction_warnUserAboutConfigurationError_title;
    public static String BuilderConsole_name;

	public static String ButtonBarBlock_Scan;

    public static String Configuration_Active;

    public static String Configuration_Add;
    public static String Configuration_AddConfiguration;
    public static String Configuration_ConfigAddAndEditWizardPage_AddConfigDescription;
    public static String Configuration_ConfigAddAndEditWizardPage_EditConfigDescription;
    public static String Configuration_Configuration;
    public static String Configuration_ConfigurationAddWizardPage_NewConfigurationName;

    public static String Configuration_Configurations;
    public static String Configuration_Edit;
    public static String Configuration_EditConfiguration;
    public static String Configuration_ErrorMessage_InvalidConfigName;
    public static String Configuration_ErrorMessage_MustHaveAtLeastOneConfig;
    public static String Configuration_ErrorMessage_NoDeviceAvailable;
    public static String Configuration_ErrorMessage_NoDeviceSelected;
    public static String Configuration_ErrorMessage_ReexportAntennaBuildFilesFailed;
    public static String Configuration_ErrorMessage_Title;
    public static String Configuration_QuestionMessage_PackageAllConfigs_Message;
    public static String Configuration_QuestionMessage_PackageAllConfigs_Title;
    public static String Configuration_QuestionMessage_ReexportAntennaBuildFiles_Message;
    public static String Configuration_QuestionMessage_ReexportAntennaBuildFiles_Title;
    public static String Configuration_Remove;
    public static String Configuration_Symbols;
    public static String Configuration_WarningMessage_ConfigurationsDirty_Message;
    public static String Configuration_WarningMessage_ConfigurationsDirty_Title;
    public static String Configuration_WorkspaceSymbolSetViewer_ColumnTitle_Choose;
    public static String Configuration_WorkspaceSymbolSetViewer_ColumnTitle_SymbolSet;
    public static String Configuration_WorkspaceSymbolSetViewer_GroupText;

    public static String Configuration_WorkspaceSymbolSetViewer_ManageButton;
    public static String ConvertToMidletProjectAction_convert_taskname;
    public static String ConvertToMidletProjectAction_convert_taskname2;
    public static String ConvertToMidletProjectAction_error_no_device_message;
    public static String ConvertToMidletProjectAction_error_no_device_title;

    public static String ConvertToMidletProjectAction_handleException_title;

    public static String CouldNotFindJarToolHandler_ErrorDialog_message;

    public static String CouldNotFindJarToolHandler_ErrorDialog_title;

    public static String DebuggerSettingCheckHandler_debugWarning_reqestTimeoutWarning;
    public static String DebuggerSettingCheckHandler_debugWarning_suspendOnCompileErrWarning;
    public static String DebuggerSettingCheckHandler_debugWarning_suspendOnUnCaughtExpWarning;
    public static String DebuggerSettingCheckHandler_debugWarning_title;
    public static String DebuggerSettingCheckHandler_debugWarning_toggleMessage;
    public static String DebuggerSettingCheckHandler_debugWarning_warningMessage;
    public static String DeviceBasicEditorPage_browseForExecutable_dialog_title;
    public static String DeviceBasicEditorPage_debug_server_label;

    public static String DeviceBasicEditorPage_description;
    public static String DeviceBasicEditorPage_description_label;
    public static String DeviceBasicEditorPage_error_invalid_executable;
    public static String DeviceBasicEditorPage_error_invalid_preverifier;
    public static String DeviceBasicEditorPage_error_no_device_group;

    public static String DeviceBasicEditorPage_error_no_device_name;
    public static String DeviceBasicEditorPage_error_no_launch_command;
    public static String DeviceBasicEditorPage_executable_label;
    public static String DeviceBasicEditorPage_executableBrowseButton_label;
    public static String DeviceBasicEditorPage_group_label;
    public static String DeviceBasicEditorPage_launchCommand_label;
    public static String DeviceBasicEditorPage_name_label;
    public static String DeviceBasicEditorPage_no_label;

    public static String DeviceBasicEditorPage_preverifier_label;
    public static String DeviceBasicEditorPage_preverifierTip;
    public static String DeviceBasicEditorPage_preverifyBrowseButton_label;
    public static String DeviceBasicEditorPage_title;
    public static String DeviceBasicEditorPage_yes_label;

    public static String DeviceEditorDialog_edit_device1;
    public static String DeviceEditorDialog_edit_device2;
    public static String DeviceEditorDialog_error_saving_device_log_message;
    public static String DeviceEditorDialog_error_saving_device_message;
    public static String DeviceEditorDialog_error_saving_device_title;
    public static String DeviceImportWizard_erro_dialog_title;
    public static String DeviceImportWizard_error_adding_new_device;
    public static String DeviceImportWizard_error_dialog_message;
    public static String DeviceImportWizard_title;
    public static String DeviceImportWizardPage_browseButton_label_text;
    public static String DeviceImportWizardPage_canfiguration_columnInfo;
    public static String DeviceImportWizardPage_deselectAllButton_label_text;

    public static String DeviceImportWizardPage_deviceSelector_label_text;
    public static String DeviceImportWizardPage_group_columnInfo;
    public static String DeviceImportWizardPage_handleBrowseButton_message;
    public static String DeviceImportWizardPage_handleDeviceSearchException_message;

    public static String DeviceImportWizardPage_handleDeviceSearchException_title;
    public static String DeviceImportWizardPage_import_columnInfo;
    public static String DeviceImportWizardPage_InvalidPreverifierMessage;
    public static String DeviceImportWizardPage_name_columnInfo;
    public static String DeviceImportWizardPage_profile_columnInfo;
    public static String DeviceImportWizardPage_refreshButton_label_text;
    public static String DeviceImportWizardPage_search_directory_label_text;
    public static String DeviceImportWizardPage_selectAllButton_label_text;
    public static String DeviceImportWizardPage_SelectDirectoryMessage;

    public static String DeviceImportWizardPage_title;
    public static String DeviceLibrariesEditorPage_addButton;
    public static String DeviceLibrariesEditorPage_apis_columnInfo;
    public static String DeviceLibrariesEditorPage_description;
    public static String DeviceLibrariesEditorPage_error_cloning_device_classpath;

    public static String DeviceLibrariesEditorPage_error_getting_new_Javadoc;
    public static String DeviceLibrariesEditorPage_file_columnInfo;
    public static String DeviceLibrariesEditorPage_handleRemoveButton_dialog_message;
    public static String DeviceLibrariesEditorPage_handleRemoveButton_dialog_title;
    public static String DeviceLibrariesEditorPage_javadoc_columnInfo;
    public static String DeviceLibrariesEditorPage_path_columnInfo;
    public static String DeviceLibrariesEditorPage_removeButton;
    public static String DeviceLibrariesEditorPage_source_columnInfo;
    public static String DeviceLibrariesEditorPage_title;
    public static String DeviceListContentProvider_error_retrieving_devices;
    public static String DeviceManagementPreferencePage_all_sdks;

    public static String DeviceManagementPreferencePage_configuration_columnInfo;

    public static String DeviceManagementPreferencePage_confirm_delet_dialog_message;

    public static String DeviceManagementPreferencePage_confirm_delete_dialog_title;

    public static String DeviceManagementPreferencePage_default_columnInfo;

    public static String DeviceManagementPreferencePage_deleteButton_label_text;

    public static String DeviceManagementPreferencePage_description;

    public static String DeviceManagementPreferencePage_duplicateButton_label_text;

    public static String DeviceManagementPreferencePage_editButton_lable_text;

    public static String DeviceManagementPreferencePage_error_device_registry_error;

    public static String DeviceManagementPreferencePage_error_device_registry_exception;

    public static String DeviceManagementPreferencePage_error_duplicated_device;

    public static String DeviceManagementPreferencePage_error_find_device;

    public static String DeviceManagementPreferencePage_error_findActionDelegate;

    public static String DeviceManagementPreferencePage_error_loading_devices;

    public static String DeviceManagementPreferencePage_error_reading_device_registry;

    public static String DeviceManagementPreferencePage_error_reloding_devices_dialog_message;

    public static String DeviceManagementPreferencePage_error_reloding_devices_dialog_title;

    public static String DeviceManagementPreferencePage_error_remove_device;

    public static String DeviceManagementPreferencePage_error_retrieving_devices;

    public static String DeviceManagementPreferencePage_error_retrieving_groups;

    public static String DeviceManagementPreferencePage_error_storing_devises_dialog_message;

    public static String DeviceManagementPreferencePage_error_storing_devises_dialog_title;

    public static String DeviceManagementPreferencePage_error_storing_devises_log_msg;

    public static String DeviceManagementPreferencePage_group_columnInfo;

    public static String DeviceManagementPreferencePage_importButton_label_text;

    public static String DeviceManagementPreferencePage_instaled_sdks_label;

    public static String DeviceManagementPreferencePage_name_columnInfo;

    public static String DeviceManagementPreferencePage_profile_columnInfo;

    public static String DeviceManagementPreferencePage_title;

    public static String DevicePropertiesEditorPage_description;

    public static String DevicePropertiesEditorPage_property_columnInfo;

    public static String DevicePropertiesEditorPage_title;

    public static String DevicePropertiesEditorPage_value_columnInfo;

    public static String DeviceSelectDialog_title;
    public static String DeviceSelector_4;
    public static String DeviceSelector_device_label;
    public static String DeviceSelector_DeviceContentProvider_error_getElements;
    public static String DeviceSelector_deviceGroupChanged_error_retrieving_devices;
    public static String DeviceSelector_deviceGroupLabel;
    public static String DeviceSelector_DeviceGroupsContentProvider_error_getElements;
    public static String DeviceSelector_manageDevicesButton_label;
    public static String DeviceSelector_setInitialState_error_retrieving_devices;

    public static String DeviceTab_device_group_label_text;

    public static String DeviceTab_error_initializePlatformDefinitionFrom;

    public static String DeviceTab_error_no_device_selected;

    public static String DeviceTab_error_retrieving_devices;

    public static String DeviceTab_error_updating_project_field;

    public static String DeviceTab_extraParams_label_text;

    public static String DeviceTab_name;

    public static String DeviceTab_projectDeviceButton_radio_label_text;

    public static String DeviceTab_securityDomain_label_text;

    public static String DeviceTab_specificDeviceButton_radio_label_text;

    public static String EditorPreferencePage_description;
    public static String EditorPreferencePage_folding_label;
    public static String EditorPreferencePage_generalTextEditor_link;
    public static String EditorPreferencePage_localization_tab_title;
    public static String EnableLocalizationAction_dialog_title;
    public static String ErrorTextWithContinueDialog_continue_label;

    public static String ExceptionHandler_displayMessageDialog_message;
    public static String FormatAction_text;
    public static String HyperlinkAction_text;
    public static String ImageSelectionDialogCreator_title;

	public static String IMTJUIConstants_allFiles;

    public static String InputContext_doSaveAs_fileNotExist;

    public static String InputContext_saveAs_noLocation;
    public static String InputContextManager_saveAsInputContextNull;
    public static String InputContextManager_updateInputContextNull;

    public static String J2MEOTAPreferencePage_deploy_prior_to_launch;
    public static String J2MEOTAPreferencePage_description;
    public static String J2MEOTAPreferencePage_ota_listen_startap;

    public static String J2MEOTAPreferencePage_ota_specified_port;
    public static String J2MEOTAPreferencePage_ota_use_specified_port;
    public static String J2MEPreferencePage_0;

    public static String J2MEPreferencePage_antenna_JAR;
    public static String J2MEPreferencePage_antenna_settings;
    public static String J2MEPreferencePage_debug_server_poll_interval;
    public static String J2MEPreferencePage_debug_server_time_out;
    public static String J2MEPreferencePage_deployment_directory;
    public static String J2MEPreferencePage_description;
    public static String J2MEPreferencePage_maximum_duration_launch;

    public static String J2MEPreferencePage_WTK_root;
    public static String J2MEProjectPropertiesPage_jad_file_name;
    public static String J2MEProjectPropertiesPage_jar_file_name;
    public static String J2MEProjectPropertiesPage_not_midletproject_error;
    public static String J2MEProjectPropertiesPage_validatePage_jad_error;
    public static String J2MEProjectPropertiesPage_validatePage_jar_error;
    public static String J2MESigningPropertiesPage_advanced_group_text;

    public static String J2MESigningPropertiesPage_advanced_text;
    public static String J2MESigningPropertiesPage_alias_label_text;

    public static String J2MESigningPropertiesPage_browse_dialog_message;
    public static String J2MESigningPropertiesPage_browse_dialog_title;
    public static String J2MESigningPropertiesPage_browse_need_file;
    public static String J2MESigningPropertiesPage_error_invalid_keystore;
    public static String J2MESigningPropertiesPage_error_missing_alias;
    public static String J2MESigningPropertiesPage_error_missing_keypass;
    public static String J2MESigningPropertiesPage_error_missing_keystore;
    public static String J2MESigningPropertiesPage_error_missing_storepass;
    public static String J2MESigningPropertiesPage_error_nosuch_keystore;
    public static String J2MESigningPropertiesPage_error_not_midlet_project;
    public static String J2MESigningPropertiesPage_external_browse_button_text;
    public static String J2MESigningPropertiesPage_internal_browse_button_text;
    public static String J2MESigningPropertiesPage_key_pwd_label_text;
    public static String J2MESigningPropertiesPage_keystore_label_text;
    public static String J2MESigningPropertiesPage_keystore_pwd_label_text;

    public static String J2MESigningPropertiesPage_keystore_type_label_text;
    public static String J2MESigningPropertiesPage_prompt_for_pwd_label_text;
    public static String J2MESigningPropertiesPage_provider_label_text;
    public static String J2MESigningPropertiesPage_sign_project_check_button_text;
    public static String J2MESigningPropertiesPage_signing_group_text;
    public static String J2MESigningPropertiesPage_store_pwd_keyring_label_text;
    public static String J2MESigningPropertiesPage_store_pwd_project_label_text;
    public static String J2MESigningPropertiesPage_test_button_text;
    public static String J2MESigningPropertiesPage_test_success;
    public static String J2MESigningPropertiesPage_testSettings_dialog_title;

    public static String JADAttributesRegistry_error_getDescriptorsFromElements;

    public static String JADFormEditor_ignore_save_jad_subtask_name;

    public static String JADFormEditor_save_jad_subtask_name;

    public static String JADFormEditor_save_jad_task_name;

    public static String JADFormEditor_shouldReloadLocalFile_dialod_title;

    public static String JADFormEditor_shouldReloadLocalFile_dialog_message;

    public static String JADOptionalPropertiesEditorPage_description;
    public static String JADOptionalPropertiesEditorPage_SectionTitle;
    public static String JADOptionalPropertiesEditorPage_title;
    public static String JADOTAPropertiesEditorPage_SectionDescription;
    public static String JADOTAPropertiesEditorPage_SectionTitle;
    public static String JADOTAPropertiesEditorPage_title;

    public static String JADPushRegistryEditorPage_add_btn_label;
    public static String JADPushRegistryEditorPage_choose_MIDlet;
    public static String JADPushRegistryEditorPage_class_column;
    public static String JADPushRegistryEditorPage_connection_column;
    public static String JADPushRegistryEditorPage_new_pushReg;
    public static String JADPushRegistryEditorPage_remove_btn_label;
    public static String JADPushRegistryEditorPage_sectionDescription;
    public static String JADPushRegistryEditorPage_sectionTitle;
    public static String JADPushRegistryEditorPage_sender_column;
    public static String JADPushRegistryEditorPage_title;

    public static String JADUserDefinedPropertiesEditorPage_add_btn_label;
    public static String JADUserDefinedPropertiesEditorPage_key_column;
    public static String JADUserDefinedPropertiesEditorPage_remove_btn_label;
    public static String JADUserDefinedPropertiesEditorPage_sectionDescription;
    public static String JADUserDefinedPropertiesEditorPage_sectionTitle;
    public static String JADUserDefinedPropertiesEditorPage_title;
    public static String JADUserDefinedPropertiesEditorPage_value_column;

    public static String L10nAddLocaleAction_text;
    public static String L10nAddLocaleEntryAction_initialKey;
    public static String L10nAddLocaleEntryAction_initialValue;
    public static String L10nAddLocaleEntryAction_text;

    public static String L10nEntryDetails_detailsDescription;
    public static String L10nEntryDetails_detailsTitle;
    public static String L10nEntryDetails_keyEntry_label;

    public static String L10nEntryDetails_keyWidgetLabel;
    public static String L10nEntryDetails_valueEntry_label;
    public static String L10nEntryDetails_valueWidget_label;
    public static String L10nLocaleDetails_choose_btn_label;
    public static String L10nLocaleDetails_countrycode_label;
    public static String L10nLocaleDetails_detailsDescription;

    public static String L10nLocaleDetails_detailsTitle;
    public static String L10nLocaleDetails_languagecode_label;
    public static String L10nLocalesDetails_browse_label;
    public static String L10nLocalesDetails_detailsTitle;

    public static String L10nLocalesDetails_locationCountryBrowseDialog_message;
    public static String L10nLocalesDetails_locationCountryBrowseDialog_title;
    public static String L10nLocalesDetails_locationEntry_label;
    public static String L10nLocalesDetails_locationEntryBrowseDialog_message;
    public static String L10nLocalesDetails_locationEntryBrowseDialog_title;
    public static String L10nLocalesDetails_locationLanguageBrowseDialog_message;
    public static String L10nLocalesDetails_locationLanguageBrowseDialog_title;
    public static String L10nLocalesDetails_locationWidget_label;

    public static String L10nLocalesDetails_packageEntry_label;
    public static String L10nLocalesDetails_packageEntryBrowseDialog_message;
    public static String L10nLocalesDetails_packageEntryBrowseDialog_title;
    public static String L10nLocalesDetails_packageWidget_label;
    public static String L10nRemoveObjectAction_text;
    public static String L10nSourcePage_source_partName;

    public static String LibraryApiEditorDialog_addButton_label_text;
    public static String LibraryApiEditorDialog_identifier_columnInfo;
    public static String LibraryApiEditorDialog_name_columnInfo;
    public static String LibraryApiEditorDialog_removeButton_label_text;
    public static String LibraryApiEditorDialog_title;
    public static String LibraryApiEditorDialog_type_columnInfo;
    public static String LibraryApiEditorDialog_version_columnInfo;

    public static String LocalesTreeSection_addEntry_button_label;
    public static String LocalesTreeSection_addLocale_button_label;
    public static String LocalesTreeSection_collapseAllAction_Text;
    public static String LocalesTreeSection_description;
    public static String LocalesTreeSection_down_button_label;
    public static String LocalesTreeSection_new_submenu_text;
    public static String LocalesTreeSection_remove_button_label;
    public static String LocalesTreeSection_showInLabel;
    public static String LocalesTreeSection_title;
    public static String LocalesTreeSection_up_button_label;

    public static String LocalizationBlock_Browse;
    public static String LocalizationBlock_DestinationFolder;
    public static String LocalizationBlock_DestinationFolderMessage;
    public static String LocalizationBlock_DestinationFolderTitle;
    public static String LocalizationBlock_PackageDialogMessage;
    public static String LocalizationBlock_PackageDialogTitle;
    public static String LocalizationBlock_PackageName;

    public static String LocalizationPage_formErrorContent_message;
    public static String LocalizationPage_formErrorContent_title;
    public static String LocalizationPage_LocalizationPage;
    public static String LocalizationPage_PageDescription;
    public static String LocalizationPage_PageTitle;
    public static String LocalizationPage_text;
    public static String LocalizationPage_title;

    public static String LocalizationWizard_failed_createLocalizationFile;
    public static String LocalizationWizard_window_title;
    public static String LogAndDisplaySafeRunnable_handleException_message;
    public static String LogAndDisplaySafeRunnable_handleException_title;
    public static String LoggingSafeRunnable_handleException_message;
    public static String MidletsEditorPage_help_action;
    public static String MidletsEditorPage_title;
    public static String MidletTab_browse_btn;

    public static String MidletTab_chooseJavaProject_message;
    public static String MidletTab_chooseJavaProject_title;
    public static String MidletTab_executable_text;
    public static String MidletTab_handleSearchButtonSelected_error;
    public static String MidletTab_invalid_JAD_URL_specified_error;

    public static String MidletTab_jad_url_text;
    public static String MidletTab_MIDlet_not_specified_error;
    public static String MidletTab_midlet_text;
    public static String MidletTab_ota_text;
    public static String MidletTab_project_does_not_exist_error;
    public static String MidletTab_project_text;

    public static String MidletTab_search_btn;
    public static String MidletTab_tab_name;
    public static String MidletTab_updateProjectFromConfig_error;

    public static String MidletTemplateBuilder_UnableToBuildTemplate;

    public static String MidletTemplateBuilder_UnableToGenerateClasses;

    public static String MidletTemplateWizard_CreateTemplateErrorMessage;

    public static String MidletTemplateWizard_WizardTitle;

    public static String MidletTemplateWizardPage1_AddToJADLAbel;

    public static String MidletTemplateWizardPage1_AvailableTemplates;

    public static String MidletTemplateWizardPage1_DescriptionLabel;

    public static String MidletTemplateWizardPage1_Page1Description;

    public static String MidletTemplateWizardPage1_Page1Title;

    public static String MidletTemplateWizardPage1_PermissionsLabel;
    
    public static String MidletTemplateWizardPage1_PermissionsNone;
    
    public static String MidletTemplateWizardPage1_UnableToBuildUi;

    public static String MidletTemplateWizardPage2_Browse;

    public static String MidletTemplateWizardPage2_ClassName;

    public static String MidletTemplateWizardPage2_DialogMessage;

    public static String MidletTemplateWizardPage2_DialogTitle;

    public static String MidletTemplateWizardPage2_GroupLabel;

    public static String MidletTemplateWizardPage2_PackageName;

    public static String MidletTemplateWizardPage2_SourceFolder;

    public static String MidletTypeDetailsPage_brose_btn_label_text;

    public static String MidletTypeDetailsPage_erro_browseButtonSelected_midlet;

    public static String MidletTypeDetailsPage_error_browseButtonSelected_image;

    public static String MidletTypeDetailsPage_midlet_class_label_text;

    public static String MidletTypeDetailsPage_midlet_details_section_description;

    public static String MidletTypeDetailsPage_midlet_details_section_title;

    public static String MidletTypeDetailsPage_midlet_icon_label_text;

    public static String MidletTypeDetailsPage_midlet_name_label_text;

    public static String MissingResourcePage_editor_failed;
    public static String MissingResourcePage_message;
    public static String MissingResourcePage_resource_unavailable;
    public static String MissingResourcePage_title;
    public static String MTJFormEditor_doSaveAs_failed;
    public static String MTJFormEditorContributor_copyAction_text;
    public static String MTJFormEditorContributor_cutAction_text;

    public static String MTJFormEditorContributor_pasteAction_text;
    public static String MTJFormEditorContributor_revertAction_text;

    public static String MTJFormEditorContributor_saveAction_text;
    public static String MTJFormPage_detailsSection_title;
    public static String MTJFormPage_helpAction_toolTipText;
    public static String MTJFormPage_messageSection_title;
    public static String MTJMultiPageContentOutline_description;
    public static String MTJMultiPageContentOutline_sortingAction_text;

    public static String MTJMultiPageContentOutline_toolTipText;
    public static String MTJSourcePage_quickOutlineAction_text;

    public static String NewJ2MEProjectPreferencePage_compliance_label_text;

    public static String NewJ2MEProjectPreferencePage_description;

    public static String NewJ2MEProjectPreferencePage_resource_dir_field_lable_text;

    public static String NewJ2MEProjectPreferencePage_resource_dir_label_text;

    public static String NewMidletWizard_dialogtitle;
    public static String NewMidletWizardPage_add_to_jad_btn_text;
    public static String NewMidletWizardPage_description;
    public static String NewMidletWizardPage_super_const;
    public static String NewMidletWizardPage_title;
    public static String NewMidletWizardPage_unimplemented;

    public static String NewMidletWizardPage_warning_NotAMidletProject;
    public static String NewMidletWizardPage_warning_NotInAMidletProject;
    public static String NewMidletWizardPage_warning_super_must_be_midlet;
    public static String NewMidletWizardPage_which_methods;
    public static String ObfuscationErrorHandler_ErrorTextWithContinueDialog_title;

    public static String ObfuscationPreferencePage_description;

    public static String ObfuscationPreferencePage_proguard_keep_expressions;
    public static String ObfuscationPreferencePage_proguard_note;
    public static String ObfuscationPreferencePage_proguard_root_directory;
    public static String ObfuscationPreferencePage_specified_arguments;
    public static String OptionalJADDescriptorsProvider_midlet_data_size;
    public static String OptionalJADDescriptorsProvider_midlet_description;
    public static String OptionalJADDescriptorsProvider_midlet_icon;
    public static String OptionalJADDescriptorsProvider_midlet_info_url;
    public static String OptionalJADDescriptorsProvider_midlet_opt_permissions;
    public static String OptionalJADDescriptorsProvider_midlet_permissions;
    public static String OTAJADDescriptorsProvider_midlet_del_confirm;
    public static String OTAJADDescriptorsProvider_midlet_del_notify;
    public static String OTAJADDescriptorsProvider_midlet_install_notify;
    public static String OTANoMidletsHandler_MessageDialog_message;

    public static String OTANoMidletsHandler_MessageDialog_title;

    public static String OverviewEditorPage_debugging_section_title;
    public static String OverviewEditorPage_empty_field_error_msg;
    public static String OverviewEditorPage_exporting_section_title;
    public static String OverviewEditorPage_help_action;
    public static String OverviewEditorPage_packaging_section_title;
    public static String OverviewEditorPage_running_section_title;
    public static String OverviewEditorPage_title;
    public static String overviewPage_deploying;
    public static String overviewPage_exporting;
    public static String overviewPage_launchsection_debuglinks;
    public static String overviewPage_launchsection_runlinks;
    public static String overviewPage_requiredsection_description;
    public static String overviewPage_requiredsection_title;
    public static String overviewPage_runtimesection_description;
    public static String overviewPage_runtimesection_title;

    public static String QuickOutlinePopupDialog_infoText;
    public static String QuickOutlinePopupDialog_sortActionText;
    public static String RequiredJADDesciptorsProvider_microedition_configuration;
    public static String RequiredJADDesciptorsProvider_microedition_profile;
    public static String RequiredJADDesciptorsProvider_midlet_jar_url;

    public static String RequiredJADDesciptorsProvider_midlet_name;
    public static String RequiredJADDesciptorsProvider_midlet_vendor;
    public static String RequiredJADDesciptorsProvider_midlet_version;
    public static String ScrolledPropertiesBlock_hor_action_toolTipText;

    public static String ScrolledPropertiesBlock_midlet_list_section_description;

    public static String ScrolledPropertiesBlock_midlet_list_section_title;

    public static String ScrolledPropertiesBlock_ver_action_toolTipText;

    public static String SecurityPermissionsDialog_message;

	public static String SecurityPermissionsDialog_title;

	public static String SigningBlock_advancedSettings;

	public static String SigningBlock_createNewKeypair;

	public static String SigningBlock_cryptoProvider;

	public static String SigningBlock_deleteKeyPair;

	public static String SigningBlock_enterKeyPassword;

	public static String SigningBlock_enterKeyPasswordForAlias;

	public static String SigningBlock_generateCSR;

	public static String SigningBlock_generateKeyPair;

	public static String SigningBlock_importCertificate;

	public static String SigningBlock_importCertificateReply;

	public static String SigningBlock_importCSRResponse;

	public static String SigningBlock_infoNotAvailable;

	public static String SigningBlock_keyAliases;

	public static String SigningBlock_keyData;

	public static String SigningBlock_keystoreManagerError;

	public static String SigningBlock_ksType;

	public static String SigningBlock_selectCertificateFile;

	public static String SigningBlock_selectCSRFolder;

	public static String SigningBlock_unableToGetKsAliases;

	public static String SigningBlock_unableToReadCertInfo;

	public static String SigningBlock_useJavaSystemDefaults;

	public static String SigningPasswordDialog_savePassword;

	public static String SigningPasswordsDialog_kew_password_label;
    public static String SigningPasswordsDialog_keystore_password_label;
    public static String SigningPasswordsDialog_message1;
    public static String SigningPasswordsDialog_message2;
    public static String SigningPasswordsDialog_title;
    public static String SigningPreferencePage_changePassword;

	public static String SigningPreferencePage_dialogTitle;

	public static String SigningPreferencePage_enterPassword;

	public static String SigningPreferencePage_external;

	public static String SigningPreferencePage_keyStore;

	public static String SigningPreferencePage_keystorePassword;

	public static String SigningPreferencePage_location;

	public static String SigningPreferencePage_promptPassword;

	public static String SigningPreferencePage_savePasswordInWorkspace;

	public static String SigningPropertiesPage_changePassword;

	public static String SigningPropertiesPage_enableProjectSpecific;

	public static String SigningPropertiesPage_enterKeystorePassword;

	public static String SigningPropertiesPage_enterPassword;

	public static String SigningPropertiesPage_external;

	public static String SigningPropertiesPage_keyPassword;

	public static String SigningPropertiesPage_keystore;

	public static String SigningPropertiesPage_keyStorePassword;

	public static String SigningPropertiesPage_location;

	public static String SigningPropertiesPage_project;

	public static String SigningPropertiesPage_prompForPassword;

	public static String SigningPropertiesPage_savePasswordInProject;

	public static String SigningPropertiesPage_savePasswordInWorkspace;

	public static String SigningPropertiesPage_unableToGetKeystoreAliases;

	public static String SigningPropertiesPage_unableToSaveSuiteMetadata;

	public static String SymbolDefinitionsImportWizardPage_browse;
    public static String SymbolDefinitionsImportWizardPage_description;
    public static String SymbolDefinitionsImportWizardPage_error;
    public static String SymbolDefinitionsImportWizardPage_error_antennaLibraryIsNotSpecified;
    public static String SymbolDefinitionsImportWizardPage_error_directoryDoesNotContaintFiles;
    public static String SymbolDefinitionsImportWizardPage_error_errorDuringImportSymbolDefinitionSet;
    public static String SymbolDefinitionsImportWizardPage_from;
    public static String SymbolDefinitionsImportWizardPage_importFromAntennaJarFile;
    public static String SymbolDefinitionsImportWizardPage_importFromXMLFiles;
    public static String SymbolDefinitionsImportWizardPage_specifyDirectory;
    public static String SymbolDefinitionsImportWizardPage_title;
    public static String SymbolDefinitionsPreferencePage_addSetButton;

    public static String SymbolDefinitionsPreferencePage_addSymbolButton;

    public static String SymbolDefinitionsPreferencePage_def_set_label_text;

    public static String SymbolDefinitionsPreferencePage_error_add_symbol;

    public static String SymbolDefinitionsPreferencePage_error_reloading_symbol_definitions;

    public static String SymbolDefinitionsPreferencePage_error_retrieving_symbol_definitions;

    public static String SymbolDefinitionsPreferencePage_error_store_symbol;

    public static String SymbolDefinitionsPreferencePage_handleException_dialog_title;

    public static String SymbolDefinitionsPreferencePage_ImportButton;

    public static String SymbolDefinitionsPreferencePage_invalid_symbol;

    public static String SymbolDefinitionsPreferencePage_removeSetButton;

    public static String SymbolDefinitionsPreferencePage_removeSymbolButton;
    public static String SymbolDefinitionsPreferencePage_symbolColumnInfo;

    public static String SymbolDefinitionsPreferencePage_symbolsGroup_label_text;

    public static String SymbolDefinitionsPreferencePage_valueColumnInfo;

    public static String SyntaxColorTab_bold_label;

    public static String SyntaxColorTab_color_label;
    public static String SyntaxColorTab_elements_label;
    public static String SyntaxColorTab_italic_label;
    public static String SyntaxColorTab_preview_label;
    public static String ToggleLinkWithEditorAction_description;
    public static String ToggleLinkWithEditorAction_name;
    public static String ToggleLinkWithEditorAction_toolTipText;

    public static String XMLSourcePage_cantLeaveThePage;

    public static String XMLSyntaxColorTab_comments_label;
    public static String XMLSyntaxColorTab_constants_label;
    public static String XMLSyntaxColorTab_process_label;
    public static String XMLSyntaxColorTab_tags_label;
    public static String XMLSyntaxColorTab_text_label;

    public static String buttonBarBlock_button_add;
    public static String buttonBarBlock_button_remove;
    public static String buttonBarBlock_button_up;
    public static String buttonBarBlock_button_down;

    public static String launch_configSelection_title;
    public static String launch_configSelection_message;

    public static String MidletLaunching_EditorContainsNoMidlet;
    public static String MidletLaunching_SelectionContainsNoMidlet;
    public static String MidletLaunching_SelectionDialogTitle;

    public static String MidletSelectionDialogCreator_createMidletSelectionDialog_message;

    public static String MidletSelectionDialogCreator_createMidletSelectionDialog_title;

    public static String JavaLaunchShortcut_0;
    public static String JavaLaunchShortcut_1;
    public static String JavaLaunchShortcut_2;
    public static String JavaLaunchShortcut_3;
    public static String MainMethodLabelProvider_0;

    public static String JavaMainTab_Choose_a_main__type_to_launch__12;

    public static String MissingDeviceMarkerResolution_fix_device_definition;

    public static String PackagingPreferencePage_description;

    public static String PackagingPreferencePage_excluded_label_text;

    public static String PackagingPreferencePage_increment_label_text;

    public static String PermissionsPage_keyAlias;

	public static String PermissionsPage_optionalPermissionsMessage;

	public static String PermissionsPage_optionalPermissionsTitle;

	public static String PermissionsPage_requiredPermissionsMessage;

	public static String PermissionsPage_requiredPermissionsTitle;

	public static String PermissionsPage_signPackage;

	public static String PermissionsPage_signProperties;

	public static String PermissionsPage_title;

	public static String PreprocessPreferencePage_debugLevel_label_text;

    public static String PreprocessPreferencePage_debugLevelSettingGroup_label_text;

    public static String PreverificationPreferencePage_defaultPreverifierField_label_text;

    public static String PreverificationPreferencePage_defaultPreverifierGroup_label_text;

    public static String PreverificationPreferencePage_defaultPreverifierTip_label_text;

    public static String PreverificationPreferencePage_device_store_error;

    public static String PreverificationPreferencePage_error_build_midlet_suite;

    public static String PreverificationPreferencePage_error_build_suite;

    public static String PreverificationPreferencePage_error_store_preverifier_dialog_message;

    public static String PreverificationPreferencePage_error_store_preverifier_dialog_title;

    public static String PreverificationPreferencePage_invalid_preverifier_dialog_message;

    public static String PreverificationPreferencePage_invalid_preverifier_dialog_title;

    public static String PreverificationPreferencePage_preverifyConfigGroup_label_text;

    public static String PreverificationPreferencePage_use_jad_settings_label_text;

    public static String PreverificationPreferencePage_use_project_settings_label_text;

    public static String PreverificationPreferencePage_use_specific_config_label_text;

    public static String PropertyAndPreferencePage_projectSpecificSettings_btn_label_text;

    public static String WorkspaceAndExternalFileFieldEditor_browseExternal_dialog_text;

    public static String WorkspaceAndExternalFileFieldEditor_browseExternalButton_text;

    public static String WorkspaceAndExternalFileFieldEditor_browseWorkspace_dialog_emptyListMessage;

    public static String WorkspaceAndExternalFileFieldEditor_browseWorkspace_dialog_message;

    public static String WorkspaceAndExternalFileFieldEditor_browseWorkspace_dialog_text;

    public static String WorkspaceAndExternalFileFieldEditor_browseWorkspaceButton_text;

    public static String WorkspaceAndExternalFileFieldEditor_error_status_message;

    private static final String BUNDLE_NAME = "org.eclipse.mtj.internal.ui.messages"; //$NON-NLS-1$

    public static String LibraryPropertyPage_description;

    public static String LibraryPropertyPage_unsavedchanges_button_discard;
    public static String LibraryPropertyPage_unsavedchanges_button_ignore;
    public static String LibraryPropertyPage_unsavedchanges_button_save;
    public static String LibraryPropertyPage_unsavedchanges_message;
    public static String LibraryPropertyPage_unsavedchanges_title;

    public static String LibraryPropertyPage_error_title;
    public static String LibraryPropertyPage_error_message;

    public static String LibraryPropertyPage_restore_error_title;
    public static String LibraryPropertyPage_restore_error_message;

    public static String LibraryInfoBlock_libraryLicenseURI_href;
    public static String LibraryInfoBlock_libraryDescriptionLabel;
    public static String LibraryInfoBlock_libraryLicenceLabel;
    public static String LibraryInfoBlock_libraryLicenseURILabel;
    public static String LibraryInfoBlock_libraryNameLabel;
    public static String LibraryInfoBlock_libraryVersionLabel;

    public static String LibraryInfoBlock_permissions_label;

    public static String LibraryInfoBlock_protection_domain_label;

    
    public static String NewMidletProjectWizard_wizard_title;
    public static String NewMidletProjectWizard_error_create_project_window_message;
    public static String NewMidletProjectWizard_error_create_project_window_title;
    public static String NewMidletProjectWizard_error_open_jad_file;

    public static String NewMidletProjectWizardPageOne_description;
    public static String NewMidletProjectWizardPageOne_deviceGroup;
    public static String NewMidletProjectWizardPageOne_deviceGroup_coment;
    public static String NewMidletProjectWizardPageOne_validate_devicecount_error;
    public static String NewMidletProjectWizardPageOne_validate_jadname_error_emptyname;
    public static String NewMidletProjectWizardPageOne_validate_jadname_error_extension;
    public static String NewMidletProjectWizardPageOne_preprocessor;
    public static String NewMidletProjectWizardPageOne_preprocessorGroup;
    public static String NewMidletProjectWizardPageOne_projectNameGroup;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_alreadyExists;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_emptyName;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_failedCreateContents;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_invalidDirectory;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_missingLocation;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_workspace1;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_workspace2;
    public static String NewMidletProjectWizardPageOne_jad_groupname;
    public static String NewMidletProjectWizardPageOne_jad_label;
    public static String NewMidletProjectWizardPageOne_jadFileNameField;
    public static String NewMidletProjectWizardPageOne_jadNameGroup_projectBasedRadio;
    public static String NewMidletProjectWizardPageOne_jadNameGroup_userDefinedRadio;
    public static String NewMidletProjectWizardPageOne_locationGroup_browse_button;
    public static String NewMidletProjectWizardPageOne_locationGroup_changeControlPressed_dialogMessage;
    public static String NewMidletProjectWizardPageOne_locationGroup_contents;
    public static String NewMidletProjectWizardPageOne_locationGroup_externalLocationRadio;
    public static String NewMidletProjectWizardPageOne_locationGroup_projectLocation;
    public static String NewMidletProjectWizardPageOne_locationGroup_workspaceLocationRadio;
    public static String NewMidletProjectWizardPageOne_title;

    public static String NewMidletProjectWizardPageTwo_changeToNewProject_errordialog_message;
    public static String NewMidletProjectWizardPageTwo_changeToNewProject_errordialog_title;
    public static String NewMidletProjectWizardPageTwo_createBackup_error_1;
    public static String NewMidletProjectWizardPageTwo_createBackup_error_2;
    public static String NewMidletProjectWizardPageTwo_doRemoveProject_taskname;
    public static String NewMidletProjectWizardPageTwo_performFinish_monitor_taskname;
    public static String NewMidletProjectWizardPageTwo_rememberExisitingFolders_errordialog_message;
    public static String NewMidletProjectWizardPageTwo_rememberExisitingFolders_errordialog_title;
    public static String NewMidletProjectWizardPageTwo_restoreExistingFiles_problem_restoring_dotclasspath;
    public static String NewMidletProjectWizardPageTwo_restoreExistingFiles_problem_restoring_dotproject;
    public static String NewMidletProjectWizardPageTwo_updateProject_errordialog_message;
    public static String NewMidletProjectWizardPageTwo_updateProject_errordialog_title;
    public static String NewMidletProjectWizardPageTwo_updateProject_fail_read_metadata;
    public static String NewMidletProjectWizardPageTwo_updateProject_monitor_buildpath_name;
    public static String NewMidletProjectWizardPageTwo_updateProject_taskname;

    public static String NewMidletProjectWizardPageLibrary_title;
    public static String NewMidletProjectWizardPageLibrary_description;
    public static String NewMidletProjectWizardPageLibrary_libraryList_label;
    public static String NewMidletProjectWizardPageLibrary_libraryList_up_button;
    public static String NewMidletProjectWizardPageLibrary_libraryList_down_button;
    public static String NewMidletProjectWizardPageLibrary_libraryList_top_button;
    public static String NewMidletProjectWizardPageLibrary_libraryList_bottom_button;
    public static String NewMidletProjectWizardPageLibrary_libraryList_checkall_button;
    public static String NewMidletProjectWizardPageLibrary_libraryList_uncheckall_button;

    public static String NewMidletProjectWizardPageLibrary_HintTextGroup_title;

    public static String ConfigurationSection_Description;
    
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, MTJUIMessages.class);
    }

    private MTJUIMessages() {
    }
}
