/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.mtj.internal.core.IModelChangedEvent;
import org.eclipse.mtj.internal.core.IModelChangedListener;
import org.eclipse.mtj.internal.core.text.l10n.L10nEntry;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.ui.editor.MTJFormPage;
import org.eclipse.mtj.internal.ui.editor.MTJMasterDetailsBlock;
import org.eclipse.mtj.internal.ui.editor.MTJSection;
import org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails;
import org.eclipse.mtj.internal.ui.editors.l10n.details.L10nEntryDetails;
import org.eclipse.mtj.internal.ui.editors.l10n.details.L10nLocaleDetails;
import org.eclipse.mtj.internal.ui.editors.l10n.details.L10nLocalesDetails;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IDetailsPage;
import org.eclipse.ui.forms.IDetailsPageProvider;
import org.eclipse.ui.forms.IManagedForm;

/**
 * @since 0.9.1
 */
public class LocalesBlock extends MTJMasterDetailsBlock implements
        IModelChangedListener, IDetailsPageProvider {

    private L10nLocaleDetails flocaleDetails;

    private L10nAbstractDetails fDetails;

    private LocalesTreeSection fMasterSection;

    private L10nAbstractDetails fAbstractDetails;

    /**
     * @param page
     */
    public LocalesBlock(MTJFormPage page) {
        super(page);
    }

    /**
     * @return
     */
    public LocalesTreeSection getMasterSection() {
        return fMasterSection;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.IDetailsPageProvider#getPage(java.lang.Object)
     */
    public IDetailsPage getPage(Object key) {
        // No dynamic pages. Static pages already registered
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.IDetailsPageProvider#getPageKey(java.lang.Object)
     */
    public Object getPageKey(Object object) {
        ISelection selection = getSelection();
        if (!(selection instanceof IStructuredSelection)
                || ((IStructuredSelection) selection).size() > 1) {
            return object.getClass();
        }

        // Get static page key
        if (object instanceof L10nLocales) {
            // Static page: Locales Details
            return L10nLocalesDetails.class;
        } else if (object instanceof L10nEntry) {
            // Static page: Entry Details
            return L10nEntryDetails.class;
        } else if (object instanceof L10nLocale) {
            // Static page: Locale Details
            return L10nLocaleDetails.class;
        }

        // Should never reach here
        return object.getClass();
    }

    /**
     * @return
     */
    public ISelection getSelection() {
        if (fMasterSection != null) {
            return fMasterSection.getSelection();
        }
        return StructuredSelection.EMPTY;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangedListener#modelChanged(org.eclipse.mtj.core.model.IModelChangedEvent)
     */
    public void modelChanged(IModelChangedEvent event) {
        // Inform the master section
        if (fMasterSection != null) {
            
            fMasterSection.modelChanged(event);
            
            
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJMasterDetailsBlock#createMasterSection(org.eclipse.ui.forms.IManagedForm, org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected MTJSection createMasterSection(IManagedForm managedForm,
            Composite parent) {
        fMasterSection = new LocalesTreeSection(getPage(), parent);
        return fMasterSection;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.MasterDetailsBlock#registerPages(org.eclipse.ui.forms.DetailsPart)
     */
    @Override
    protected void registerPages(DetailsPart detailsPart) {

        // Only static pages to be defined. Do not cache pages
        detailsPart.setPageLimit(0);

        // Register static page: Locales Details
        fDetails = new L10nLocalesDetails(fMasterSection);
        detailsPart.registerPage(L10nLocalesDetails.class, fDetails);

        // Register static page: Entry Details
        fAbstractDetails = new L10nEntryDetails(fMasterSection);
        detailsPart.registerPage(L10nEntryDetails.class, fAbstractDetails);

        // Register static page: Locale Details
        flocaleDetails = new L10nLocaleDetails(fMasterSection);
        detailsPart.registerPage(L10nLocaleDetails.class, flocaleDetails);

        // Set this class as the page provider
        detailsPart.setPageProvider(this);
    }
}
