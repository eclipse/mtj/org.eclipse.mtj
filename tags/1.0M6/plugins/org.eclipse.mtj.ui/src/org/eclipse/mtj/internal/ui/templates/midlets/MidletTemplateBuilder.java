/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version.
 *     Gorkem Ercan (Nokia)     - Merge New MIDlet wizard with New MIDlet from 
 *                                template wizard  
 */
package org.eclipse.mtj.internal.ui.templates.midlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.osgi.util.NLS;

/**
 * MidletTemplateBuilder class builds code from a template object.
 * 
 * @author David Marques
 * @since 1.0
 */
public class MidletTemplateBuilder {

    private MIDletTemplateObject template;
    private Map<String, String>  dictionary;

    /**
     * Creates a MidletTemplateBuilder instance to build code from the specified
     * template using the specified provider to get the template dictionary.
     * 
     * @param template template object.
     * @param provider template provider.
     */
    public MidletTemplateBuilder(MIDletTemplateObject template,
            Map<String, String> dictionary) {
        this.template   = template;
        this.dictionary = dictionary;
    }

    /**
     * Builds the code from the template into the specified source folder into
     * the specified package. The MIDlet class name specified will be used in
     * order to create the MIDlet class file.
     * 
     * @param _source target source folder.
     * @param _package target package name.
     * @param _name MIDlet class name.
     * @param isAddingToJad if true adds a MIDlet entry into the application
     *            descriptor.
     * @throws MIDletTemplateBuilderException - In case any error occurs.
     */
    public IType build(IPackageFragmentRoot _source, String _package,
            String _name) throws MIDletTemplateBuilderException {
        try {
            Map<String, Object> templates = null;
            Object bundleObj = null;

            File file = FileLocator.getBundleFile(this.template.getBundle());
            if (!file.isDirectory()) {
                bundleObj = new ZipFile(file);
                templates = getTemplates((ZipFile) bundleObj);
            } else {
                bundleObj = file;
                templates = getTemplates((File) bundleObj);
            }
            return this.build(_source, _package, _name, bundleObj, templates);
        } catch (Exception e) {
            throw new MIDletTemplateBuilderException(
                    MTJUIMessages.MidletTemplateBuilder_UnableToBuildTemplate
                            + e.getMessage());
        }
    }

    /**
     * Builds the code from the template into the specified source folder into
     * the specified package. The MIDlet class name specified will be used in
     * order to create the MIDlet class file. <br>
     * An object instance will be specified, it is either a Folder or a JAR file
     * where the template resides. A map of file names and objects is specified
     * with either the ZipEntries or Files.
     * 
     * @param _source target source folder.
     * @param _package target package.
     * @param _name MIDlet name.
     * @param _bundleObj Bundle container (ZipFile or Folder)
     * @param _templates Template objects (ZipEntry or File)
     * @param _isAddingToJad true if addinf to JAD.
     * @throws MIDletTemplateBuilderException - In case any error occurs.
     */
    private IType build(IPackageFragmentRoot _source, String _package,
            String _name, Object _bundleObj, Map<String, Object> _templates)
            throws MIDletTemplateBuilderException {
        IProgressMonitor monitor = new NullProgressMonitor();
        IType createdType = null;
        try {
            IPackageFragment pack = _source.createPackageFragment(_package,
                    true, monitor);

            for (String name : _templates.keySet()) {
                boolean isMidlet = false;
                String code = null;
                String clazzName = NLS.bind("{0}.java", name.replace( //$NON-NLS-1$
                        ".template", Utils.EMPTY_STRING)); //$NON-NLS-1$
                if (clazzName.equalsIgnoreCase("$class_name$.java")) { //$NON-NLS-1$
                    clazzName = NLS.bind("{0}.java", _name); //$NON-NLS-1$
                    isMidlet = true;
                }

                Object template = _templates.get(name);
                if (template instanceof ZipEntry) {
                    ZipFile zipFile = (ZipFile) _bundleObj;
                    code = processTemplate(zipFile
                            .getInputStream((ZipEntry) template), _package,
                            _name);
                } else {
                    code = processTemplate(
                            new FileInputStream((File) template), _package,
                            _name);
                }

                ICompilationUnit cu = pack.createCompilationUnit(clazzName,
                        code, true, monitor);
                if (isMidlet) {
                    createdType = cu.getType(_name);
                }
            }

            IProject project = _source.getJavaProject().getProject();
            File folder = new File(project.getLocation().toOSString());
            if (_bundleObj instanceof ZipFile) {
                copyResources((ZipFile) _bundleObj, folder);
            } else {
                copyResources((File) _bundleObj, folder);
            }
            project.refreshLocal(IResource.DEPTH_INFINITE, monitor);
            return createdType;

        } catch (Exception e) {
            throw new MIDletTemplateBuilderException(
                    MTJUIMessages.MidletTemplateBuilder_UnableToGenerateClasses
                            + e.getMessage());
        }
    }

    /**
     * Copies the source file content into the target file.
     * 
     * @param source source file.
     * @param target target file.
     * @throws IOException If any IO error occurs.
     */
    private void copyFile(File source, File target) throws IOException {
        File file = null;
        if (source.isDirectory()) {
            file = new File(target, source.getName());
            if (!file.exists() && !file.mkdir()) {
                return;
            }
            File[] children = source.listFiles();
            for (File child : children) {
                copyFile(child, file);
            }
        } else {
            file = new File(target, source.getName());
            FileOutputStream out = null;
            FileInputStream in = null;
            try {
                out = new FileOutputStream(file);
                in = new FileInputStream(source);
                copyStreams(in, out);
            } finally {
                out.close();
                in.close();
            }
        }
    }

    /**
     * Copies all content inside the plug-in template folder to the specified
     * target folder.
     * 
     * @param bundleFolder source folder.
     * @param projectFolder target folder.
     * @throws IOException If any IO error occurs.
     */
    private void copyResources(File bundleFolder, File projectFolder)
            throws IOException {
        String path = NLS
                .bind("templates/{0}/resources", this.template.getId()); //$NON-NLS-1$
        File resources = new File(bundleFolder, path);
        if (resources.exists() && resources.isDirectory()) {
            File[] children = resources.listFiles();
            for (File child : children) {
                copyFile(child, projectFolder);
            }
        }
    }

    /**
     * Copies all resources within the plug-in template resources folder to the
     * specified target folder.
     * 
     * @param zipFile plug-in JAR file.
     * @param projectFolder Target folder.
     * @throws IOException If any IO error occurs.
     */
    private void copyResources(ZipFile zipFile, File projectFolder)
            throws IOException {
        String expression = NLS.bind(
                "templates/{0}/resources/.*", this.template.getId()); //$NON-NLS-1$
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = entries.nextElement();
            if (Pattern.matches(expression, zipEntry.getName())) {
                IPath path = new Path(zipEntry.getName())
                        .removeFirstSegments(0x03);
                File file = new File(projectFolder, path.toString());
                createFileFromZipFile(file, zipFile, zipEntry);
            }
        }
    }

    /**
     * Reads an InputStream and writes the content into the target OutputStream.
     * 
     * @param in InputStream instance.
     * @param out OutputStream instance.
     * @throws IOException If any IO error occurs.
     */
    private void copyStreams(InputStream in, OutputStream out)
            throws IOException {
        int _byte = -1;
        while ((_byte = in.read()) != -1) {
            out.write(_byte);
        }
    }

    /**
     * Copies the ZipEntry file content to a target file.
     * 
     * @param file target file.
     * @param zipFile zip file.
     * @param zipEntry zip entry.
     * @throws IOException If any IO error occurs.
     */
    private void createFileFromZipFile(File file, ZipFile zipFile,
            ZipEntry zipEntry) throws IOException {
        OutputStream out = new FileOutputStream(file);
        InputStream in = zipFile.getInputStream(zipEntry);
        copyStreams(in, out);
        out.close();
        in.close();
    }

    /**
     * Gets all template files within the templates folder inside the plug-in
     * folder.
     * 
     * @param folder plug-in folder.
     * @return a MAP with the template names as keys and template files as
     *         values.
     */
    private Map<String, Object> getTemplates(File folder) {
        Map<String, Object> templates = new HashMap<String, Object>();

        String path = NLS.bind("templates/{0}/java", this.template.getId()); //$NON-NLS-1$
        File javaFolder = new File(folder, path);
        if (javaFolder.exists() && javaFolder.isDirectory()) {
            File[] files = javaFolder.listFiles();
            for (File file : files) {
                if (file.isFile()
                        && Pattern.matches(".+[.template]", file.getName())) { //$NON-NLS-1$
                    templates.put(file.getName(), file);
                }
            }
        }
        return templates;
    }

    /**
     * Gets all template ZipEntries within the plug-in JAR file.
     * 
     * @param zipFile plug-in JAR file.
     * @return a MAP with the template names as keys and template ZipEntries as
     *         values.
     */
    private Map<String, Object> getTemplates(ZipFile zipFile) {
        Map<String, Object> templates = new HashMap<String, Object>();

        String expression = NLS.bind(
                "templates/{0}/java/.+[.]template", this.template.getId()); //$NON-NLS-1$
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            if (Pattern.matches(expression, entry.getName())) {
                IPath path = new Path(entry.getName());
                templates.put(path.lastSegment(), entry);
            }
        }
        return templates;
    }

    /**
     * Processes the template code replacing all template tags for the providers
     * dictionary of values.
     * 
     * @param in Template InputStream.
     * @param _package target package.
     * @param _name MIDlet name.
     * @return the processed code as String.
     * @throws IOException If any IO error occurs.
     * @throws CoreException Any core error occurs.
     */
    private String processTemplate(InputStream in, String _package, String _name)
            throws IOException, CoreException {
        BufferedReader reader = null;
        StringBuffer buffer = null;

        reader = new BufferedReader(new InputStreamReader(in));
        buffer = new StringBuffer();
        if ((_package != null) && (_package.trim().length() > 0x00)) {
            buffer.append(NLS.bind("package {0};\n", _package)); //$NON-NLS-1$
        }

        String line = null;
        while ((line = reader.readLine()) != null) {
            buffer.append(line).append("\n"); //$NON-NLS-1$
        }
        reader.close();

        String content = buffer.toString();
        content = content.replace("$class_name$", _name); //$NON-NLS-1$

        for (String key : dictionary.keySet()) {
            content = content.replace(key, dictionary.get(key));
        }
        return content;
    }
}
