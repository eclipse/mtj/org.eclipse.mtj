/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)      - Initial implementation
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874] 
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.configurations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.ProjectType;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.core.project.runtime.event.AddMTJRuntimeEvent;
import org.eclipse.mtj.core.project.runtime.event.IMTJRuntimeChangeListener;
import org.eclipse.mtj.core.project.runtime.event.IMTJRuntimeListChangeListener;
import org.eclipse.mtj.core.project.runtime.event.RemoveMTJRuntimeEvent;
import org.eclipse.mtj.core.project.runtime.event.SwitchActiveMTJRuntimeEvent;
import org.eclipse.mtj.internal.core.build.export.AntennaBuildExporter;
import org.eclipse.mtj.internal.core.project.runtime.MTJRuntimeListUtils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.internal.ui.viewers.TableViewerConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.PlatformUI;

/**
 * ConfigManageComponent provides user interface functionality for
 * Configuration management.<br>
 * ConfigManageComponent implements IMTJRuntimeListChangeListener to refresh its
 * UI, because Configuration may modified by other thread, the UI should
 * reflect these changes.
 * 
 * @author wangf
 */
public class ConfigManageComponent implements IMTJRuntimeListChangeListener {

    private class TableContentProvider implements IStructuredContentProvider {

        public void dispose() {
        }

        @SuppressWarnings("unchecked")
        public Object[] getElements(Object inputElement) {
            Collection<MTJRuntime> configurations = (Collection<MTJRuntime>) inputElement;
            return configurations.toArray(new MTJRuntime[configurations
                    .size()]);
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

    }

    private class TableLabelProvider extends LabelProvider implements
            ITableLabelProvider {

        private static final int COL_CHECKBOX = 0;
        private static final int COL_CONFIG_NAME = 1;

        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        public String getColumnText(Object element, int columnIndex) {
            MTJRuntime config = (MTJRuntime) element;

            switch (columnIndex) {
                case COL_CHECKBOX:
                    return "";
                case COL_CONFIG_NAME:
                    return config.getName();
                default:
                    return "";
            }
        }

    }

    private static final String CONFIG_MANAGE_SETTINGS = "ConfigManageComponentSettings";
    private CheckboxTableViewer configViewer;
    private MTJRuntimeList configurations;

    private IMidletSuiteProject midletSuiteProject;

    private boolean includeGroup = true;
    private boolean configsChanged;
    private ICheckStateListener checkStateListener;

    private IMTJRuntimeListChangeListener configurationsChangeListener;
    private IMTJRuntimeChangeListener configurationChangeListener;

    private String description;

    private Button editButton;

    private Button removeButton;

    /**
     * Constructor used for a situation that has no associate MIDlet project.
     * For example, in the MIDlet project create wizard.
     */
    public ConfigManageComponent() {
        this(null);
    }

    public ConfigManageComponent(IMidletSuiteProject midletSuiteProject) {
        this.midletSuiteProject = midletSuiteProject;
        // if we use ConfigManageComponent in project create process,
        // midletSuiteProject will be null
        if (midletSuiteProject != null) {
            this.configurations = midletSuiteProject.getMTJRuntime();
        } else {
            this.configurations = new MTJRuntimeList();
        }
        // ConfigManageComponent as a IMTJRuntimeListChangeListener will
        // refresh itself when Configuration changed by other thread.
        this.configurations.addMTJRuntimeListChangeListener(this);
    }

    /**
     * If Configuration modified by other thread, we should refresh
     * ConfigManageComponent UI, to reflect the change of Configuration. <br>
     * For example, we change configurations in project properties page, we
     * should refresh "runtime section" in Application Descriptor form editor.
     */
    public void activeMTJRuntimeSwitched(SwitchActiveMTJRuntimeEvent event) {
        // If widget (the table) is disposed, do not refresh.
        if (configViewer.getControl().isDisposed()) {
            return;
        }
        configViewer.setAllChecked(false);
        configViewer.setChecked(configurations.getActiveMTJRuntime(), true);
        configViewer.refresh();
    }

    /**
     * Handle add configuration.
     * 
     * @param event
     */
    private void addConfiguration(SelectionEvent event) {
        // First add configurationsChangeListener to configurations
        addConfigurationsChangeListener();

        ConfigAddAndEditWizard wizard = new ConfigAddAndEditWizard(
                configurations, null);

        Shell shell = event.widget.getDisplay().getActiveShell();
        WizardDialog dialog = new WizardDialog(shell, wizard);

        if (dialog.open() == Window.OK) {
            configViewer.refresh();
            // changeActiveConfigIfNecessary() must be called after
            // configViewer.refresh(), if not, the check box will not checked
            changeActiveConfigIfNecessary();

            configsChanged = true;
        }
        // Remove configurationsChangeListener after perform add configuration.
        // We do this to prevent the listener been notified by configurations
        // change event fired by other thread. Also to avoid memory leak.
        removeConfigurationsChangeListener();
    }

    /**
     * Add the external IMTJRuntimeChangeListener to Configuration.
     * 
     * @param configuration
     */
    private void addConfigurationChangeListener(MTJRuntime configuration) {
        if ((this.configurationChangeListener != null)
                && (configuration != null)) {
            configuration.addMTJRuntimeChangeListener(configurationChangeListener);
        }
    }

    /**
     * Add the external IMTJRuntimeListChangeListener to Configuration.
     */
    private void addConfigurationsChangeListener() {
        if (configurationsChangeListener != null) {
            this.configurations
                    .addMTJRuntimeListChangeListener(configurationsChangeListener);
        }
    }

    /**
     * Determine if both buil.xml and mtj-build.xml files exist in MIDlet
     * project root folder.
     * 
     * @return
     */
    private boolean allBuildFilesExist() {
        IProject project = midletSuiteProject.getProject();

        IFile buildScriptFile = project
                .getFile(AntennaBuildExporter.BUILD_XML_FILE_NAME);
        IFile mtjBuildScriptFile = project
                .getFile(AntennaBuildExporter.MTJ_BUILD_XML_FILE_NAME);

        if (buildScriptFile.exists() && mtjBuildScriptFile.exists()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * If the associate MIDlet project exist, Antenna build files exist in
     * project and Configuration changed, ask the user if he want to reexport
     * Antenna build files.
     */
    private void askIfReexportAntennaBuildFiles() {
        if (midletSuiteProject == null) {
            return;
        }
        if (!allBuildFilesExist()) {
            return;
        }
        if (MTJRuntimeListUtils.isOnlyActiveMTJRuntimeDirty(midletSuiteProject)) {
            return;
        }
        Display display = PlatformUI.getWorkbench().getDisplay();
        display.syncExec(new Runnable() {

            public void run() {
                boolean reexportAntennaBuildFiles = MessageDialog
                        .openQuestion(
                                null,
                                MTJUIMessages.Configuration_QuestionMessage_ReexportAntennaBuildFiles_Title,
                                MTJUIMessages.Configuration_QuestionMessage_ReexportAntennaBuildFiles_Message);
                if (reexportAntennaBuildFiles) {
                    AntennaBuildExporter antennaBuildExporter = new AntennaBuildExporter(
                            midletSuiteProject);
                    try {
                        antennaBuildExporter
                                .doExport(new NullProgressMonitor());
                    } catch (Exception e) {
                        MessageDialog
                                .openError(
                                        null,
                                        MTJUIMessages.Configuration_ErrorMessage_Title,
                                        MTJUIMessages.Configuration_ErrorMessage_ReexportAntennaBuildFilesFailed);
                    }
                }
            }

        });
    }

    /**
     * If user attempt to delete all configs of a MIDlet project, return false.
     * 
     * @param selected
     * @param event
     * @return
     */
    private boolean canPerformRemoveConfigs(TableItem[] selected,
            SelectionEvent event) {
        if ((midletSuiteProject != null)
                && (selected.length >= configurations.size())) {
            Shell shell = event.widget.getDisplay().getActiveShell();
            MessageDialog
                    .openError(
                            shell,
                            MTJUIMessages.Configuration_ErrorMessage_Title,
                            MTJUIMessages.Configuration_ErrorMessage_MustHaveAtLeastOneConfig);
            return false;
        } else {
            return true;
        }
    }

    /**
     * If there is no active configuration, set the first configuration as
     * active. There two conditions that we should do this:<br>
     * 1, If user remove active configuration, set the first configuration as
     * active.<br>
     * 2, If user add the first configuration, set it as active.
     */
    private void changeActiveConfigIfNecessary() {
        if (!configurations.isEmpty()
                && (configurations.getActiveMTJRuntime() == null)) {
            MTJRuntime activeConfig = configurations.get(0);
            configurations.switchActiveMTJRuntime(activeConfig);
            configViewer.setChecked(activeConfig, true);
            fireCheckStateChanged();
        }
    }

    /**
     * If Configuration modified by other thread, we should refresh
     * ConfigManageComponent UI, to reflect the change of Configuration. <br>
     * For example, we change configurations in project properties page, we
     * should refresh "runtime section" in Application Descriptor form editor.
     */
    public void mtjRuntimeAdded(AddMTJRuntimeEvent event) {
        // If widget (the table) is disposed, do not refresh.
        if (configViewer.getControl().isDisposed()) {
            return;
        }
        configViewer.refresh();
    }

    /**
     * If Configuration modified by other thread, we should refresh
     * ConfigManageComponent UI, to reflect the change of Configuration. <br>
     * For example, we change configurations in project properties page, we
     * should refresh "runtime section" in Application Descriptor form editor.
     */
    public void mtjRuntimeRemoved(RemoveMTJRuntimeEvent event) {
        // If widget (the table) is disposed, do not refresh..
        if (configViewer.getControl().isDisposed()) {
            return;
        }
        configViewer.refresh();
    }

    /**
     * Create Add, Edit and Remove buttons
     * 
     * @param parent
     */
    private void createButtons(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout());
        composite.setLayoutData(new GridData(GridData.FILL_VERTICAL));
        // Create Add button
        Button addButton = new Button(composite, SWT.PUSH);
        addButton.setText(MTJUIMessages.Configuration_Add);
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        addButton.setLayoutData(gd);
        addButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                addConfiguration(e);
            }
        });
        // Create Edit button
        editButton = new Button(composite, SWT.PUSH);
        editButton.setText(MTJUIMessages.Configuration_Edit);
        editButton.setLayoutData(gd);
        editButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                editConfiguration();
            }
        });
        // Create remove button
        removeButton = new Button(composite, SWT.PUSH);
        removeButton.setText(MTJUIMessages.Configuration_Remove);
        removeButton.setLayoutData(gd);
        removeButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                removeConfiguration(e);
            }
        });
    }

    /**
     * Create the table viewer to display configurations.
     * 
     * @param composite
     */
    private void createConfigTableViewer(Composite composite) {
        final int DEFAULT_TABLE_WIDTH = 300;
        final int DEFAULT_TABLE_HIGHT = 100;

        GridData gd = new GridData();

        int styles = SWT.MULTI | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION
                | SWT.CHECK;
        Table table = new Table(composite, styles);
        gd = new GridData(GridData.FILL_BOTH);
        gd.heightHint = DEFAULT_TABLE_HIGHT;
        gd.widthHint = DEFAULT_TABLE_WIDTH;
        table.setLayoutData(gd);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        configViewer = new CheckboxTableViewer(table);
        configViewer.setContentProvider(new TableContentProvider());
        configViewer.setLabelProvider(new TableLabelProvider());
        configViewer.addCheckStateListener(new ICheckStateListener() {

            public void checkStateChanged(CheckStateChangedEvent event) {
                handleCheckStateChange(event);
                if (checkStateListener != null) {
                    checkStateListener.checkStateChanged(event);
                }
            }

        });
        configViewer
                .addSelectionChangedListener(new ISelectionChangedListener() {

                    public void selectionChanged(SelectionChangedEvent event) {
                        validateControls();
                    }

                });
        configViewer.addDoubleClickListener(new IDoubleClickListener() {

            public void doubleClick(DoubleClickEvent event) {
                editConfiguration();
            }

        });

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings(CONFIG_MANAGE_SETTINGS);
        TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
                new TableColumnInfo(MTJUIMessages.Configuration_Active, 20f,
                        null),
                new TableColumnInfo(MTJUIMessages.Configuration_Configuration,
                        80f, null) };
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 1);
        viewerConfiguration.configure(configViewer);
        // onfigViewer.setInput(configurations) method should be called after
        // the view creating complete
        configViewer.setInput(configurations);
        if ((configurations != null) && (configurations.size() > 0)) {
            configViewer.setChecked(configurations.getActiveMTJRuntime(),
                    true);
        }
    }

    /**
     * Create the UI contents.
     * 
     * @param parent
     */
    public void createContents(Composite parent) {
        Composite composite;
        if (includeGroup) {
            Group group = new Group(parent, SWT.NONE);
            group.setText(MTJUIMessages.Configuration_Configurations);
            composite = group;
        } else {
            composite = new Composite(parent, SWT.NONE);
        }

        setCompositeLayout(parent, composite);
        createDescription(composite);
        createConfigTableViewer(composite);
        createButtons(composite);

        validateControls();
    }

    /**
     * Create a description for this control on the UI.
     * 
     * @param parent
     */
    private void createDescription(Composite parent) {
        if ((description == null) || (description.trim().length() == 0)) {
            return;
        }
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout());
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 2;
        composite.setLayoutData(gd);
        Label descriptionLabel = new Label(composite, SWT.NONE);
        descriptionLabel.setText(description);
    }

    /**
     * This method must be called in parent UI's(Eg. Wizard page, properties
     * page, editor page) dispose() method. Because Configuration have a long
     * life cycle across a MIDlet project, we should remove unused listeners
     * manually to avoid memory leak.
     */
    public void dispose() {
        configurations.removeMTJRuntimeListChangeListener(this);
    }

    /**
     * Handle edit configuration.
     * 
     * @param event
     */
    private void editConfiguration() {
        TableItem[] selected = configViewer.getTable().getSelection();
        if (selected.length < 1) {
            return;
        }
        MTJRuntime currentConfig = (MTJRuntime) selected[0].getData();
        // Add configurationsChangeListener to configurations
        addConfigurationChangeListener(currentConfig);
        ConfigAddAndEditWizard wizard = new ConfigAddAndEditWizard(
                configurations, currentConfig);

        Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();
        WizardDialog dialog = new WizardDialog(shell, wizard);
        dialog.create();
        // set midletSuiteProject to ConfigAddAndEditWizard for restore symbols
        // if user click Cancel button on the wizard.
        wizard.setMidletSuiteProject(midletSuiteProject);

        if (dialog.open() == Window.OK) {
            configViewer.refresh();
            configsChanged = true;
        }
        // Remove configurationsChangeListener after perform add configuration.
        // We do this to prevent the listener been notified by configurations
        // change event fired by other thread. Also to avoid memory leak.
        removeConfigurationChangeListener(currentConfig);
    }

    /**
     * If we set call configViewer.setChecked(Object boolean) in program(not by
     * user action on UI), a CheckStateChangedEvent will not auto fired . we
     * should fire a event and notify checkStateListener by ourselves.
     */
    private void fireCheckStateChanged() {
        if (checkStateListener == null) {
            return;
        }
        Object[] configs = configViewer.getCheckedElements();
        if (configs.length <= 0) {
            return;
        }
        final MTJRuntime config = (MTJRuntime) configs[0];
        SafeRunnable.run(new SafeRunnable() {
            public void run() {
                checkStateListener
                        .checkStateChanged(new CheckStateChangedEvent(
                                configViewer, config, true));
            }
        });

    }

    /**
     * Return active Configuration. If no active one, return null.
     * 
     * @return
     */
    public MTJRuntime getActiveConfiguration() {
        if (configurations == null) {
            return null;
        }
        return configurations.getActiveMTJRuntime();
    }

    public MTJRuntimeList getConfigurations() {
        return configurations;
    }

    /**
     * Handle the CheckStateChangedEvent of the configViewer.
     * 
     * @param event
     */
    private void handleCheckStateChange(CheckStateChangedEvent event) {

        addConfigurationsChangeListener();

        MTJRuntime config = (MTJRuntime) event.getElement();
        // If user click the already checked check box, set it checked again.
        if (!event.getChecked()) {
            configViewer.setChecked(config, true);
            return;
        }
        // switch active configuration and set only this config checked.
        configurations.switchActiveMTJRuntime(config);
        configViewer.setAllChecked(false);
        configViewer.setChecked(config, true);
        configsChanged = true;

        removeConfigurationsChangeListener();
    }

    /**
     * Discard the modification and restore Configuration to former state.<br>
     * This method typically be called in the performCancel() method of the
     * parent wizard, propertiesPage, etc.
     */
    public void performCancel() {
        restoreConfigurations();
    }

    /**
     * Do some work when perform finish, including ask user if reexport Antenna
     * build files and clear removedConfigs and addedConfigs (removedConfigs and
     * addedConfigs are used to restore Configuration if user discard the
     * modification).<br>
     * This method must be called in parent UI's(Eg. Wizard page, properties
     * page, editor page) performFinish() or doSave() method.<br>
     * Note: MUST be called before midletProject.saveMetaData() method.
     */
    public void performFinish() {
        askIfReexportAntennaBuildFiles();
    }

    /**
     * Handle remove configuration.
     * 
     * @param event
     */
    private void removeConfiguration(SelectionEvent event) {
        // First add configurationsChangeListener to configurations
        addConfigurationsChangeListener();

        Table table = configViewer.getTable();
        TableItem[] selected = table.getSelection();
        // if user attend to remove all configurations, we will tell him cannot
        // do that
        if (!canPerformRemoveConfigs(selected, event)) {
            return;
        }
        // we should record all configs to remove, and then remove them. If we
        // remove them. If we remove them one by one in the following for loop,
        // a (SWTException: Widget is disposed) will be thrown. Because
        // configurationsChangeListener will refresh configViewer, lead to
        // TableItem disposed.
        List<MTJRuntime> toRemoves = new ArrayList<MTJRuntime>();
        for (TableItem tableItem : selected) {
            MTJRuntime config = (MTJRuntime) tableItem.getData();
            toRemoves.add(config);
        }
        configurations.removeAll(toRemoves);

        changeActiveConfigIfNecessary();
        configViewer.refresh();

        configsChanged = true;
        // Remove configurationsChangeListener after perform add configuration.
        // We do this to prevent the listener been notified by configurations
        // change event fired by other thread. Also to avoid memory leak.
        removeConfigurationsChangeListener();
    }

    /**
     * Remove external IMTJRuntimeChangeListener from Configuration.<br>
     * Must call this method when edit Configuration action finished, to avoid
     * configurationChangeListener notified by other thread changing
     * Configuration and also avoid unused configurationChangeListener alive for
     * long time that will cause memory leak.
     * 
     * @param configuration
     */
    private void removeConfigurationChangeListener(MTJRuntime configuration) {
        if (configuration != null) {
            configuration
                    .removeMTJRuntimeChangeListener(configurationChangeListener);
        }
    }

    /**
     * Remove external IMTJRuntimeListChangeListener from Configuration.<br>
     * Must call this method when remove Configuration action finished, to avoid
     * configurationsChangeListener notified by other thread changing
     * Configuration and also avoid unused configurationsChangeListener alive
     * for long time that will cause memory leak.
     */
    private void removeConfigurationsChangeListener() {
        this.configurations
                .removeMTJRuntimeListChangeListener(this.configurationsChangeListener);
    }

    /**
     * If we discard the change we made against Configuration, we should call
     * this method to restore Configuration to former state.<br>
     * Should be called in the performCancel() method of the parent wizard,
     * propertiesPage, etc.
     */
    private void restoreConfigurations() {
        if (midletSuiteProject == null) {
            return;
        }
        if (!configsChanged) {
            return;
        }
        MTJRuntimeList configsInMetadataFile = MTJCore.getMetaData(
                midletSuiteProject.getProject(), ProjectType.MIDLET_SUITE)
                .getRuntime();

        configurations.clear();
        configurations.addAll(configsInMetadataFile);
        // To fire a SwithActiveConfigEvent,tell listeners active config
        // switched.
        configurations.switchActiveMTJRuntime(configsInMetadataFile
                .getActiveMTJRuntime());
    }

    /**
     * Set a external ICheckStateListener to listen to Configuration
     * CheckboxTableViewer's CheckStateChangedEvent.
     * 
     * @param checkStateListener
     */
    public void setCheckStateListener(ICheckStateListener checkStateListener) {
        this.checkStateListener = checkStateListener;
    }

    private void setCompositeLayout(Composite parent, Composite child) {
        int columns = 1;
        Object layout = parent.getLayout();
        if (layout instanceof GridLayout) {
            columns = ((GridLayout) layout).numColumns;
        }

        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = columns;
        child.setLayoutData(gd);

        child.setLayout(new GridLayout(2, false));
    }

    /**
     * Set a external IMTJRuntimeChangeListener to listen to Configuration
     * change event.
     * 
     * @param listener
     */
    public void setConfigurationChangeListener(
            IMTJRuntimeChangeListener listener) {
        this.configurationChangeListener = listener;
    }

    /**
     * Set a external IMTJRuntimeListChangeListener to listen to Configuration
     * change event.
     * 
     * @param listener
     */
    public void setConfigurationsChangeListener(
            IMTJRuntimeListChangeListener listener) {
        this.configurationsChangeListener = listener;
    }

    /**
     * If description is set, a description string will appear on the UI.
     * 
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Indicate that if the UI contains by a Group.
     * 
     * @param includeGroup
     */
    public void setIncludeGroup(boolean includeGroup) {
        this.includeGroup = includeGroup;
    }

    /**
     * Determine if controls should be enabled or disabled.
     */
    private void validateControls() {
        boolean isEditButtonEnable = configViewer.getTable().getSelection().length == 1;
        editButton.setEnabled(isEditButtonEnable);
        boolean isRemoveButtonEnable = configViewer.getTable().getSelection().length > 0;
        removeButton.setEnabled(isRemoveButtonEnable);
    }
}
