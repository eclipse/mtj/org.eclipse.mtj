/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma  (Sybase)        - Initial implementation
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.preprocessor.PreprocessorHelper;
import org.eclipse.mtj.internal.ui.IEmbeddableWorkbenchPreferencePage;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;

/**
 * Preference page implementation for setting preprocessor preferences.
 * 
 * @author gma
 */
public class PreprocessPreferencePage extends PreferencePage implements
        IEmbeddableWorkbenchPreferencePage {

    private static final String DEBUGLEVEL_NONE = "none"; //$NON-NLS-1$

    private Combo debugLevelCombo;

    private Label debugLevelLabel;
    private boolean embeddedInProperties;

    /**
     * Default constructor.
     */
    public PreprocessPreferencePage() {
        this(false, MTJUIPlugin.getDefault().getCorePreferenceStore());
    }

    /**
     * Constructor for use when embedding the preference page within a
     * properties page.
     * 
     * @param embeddedInProperties
     * @param preferenceStore a table mapping named preferences to values
     */
    public PreprocessPreferencePage(boolean embeddedInProperties,
            IPreferenceStore preferenceStore) {

        this.embeddedInProperties = embeddedInProperties;
        setPreferenceStore(preferenceStore);

    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performApply()
     */
    @Override
    public void performApply() {
        super.performApply();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
     */
    @Override
    public void performDefaults() {
        setControlsFromDefaults();
        super.performDefaults();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        setPreferencesFromControls();
        return super.performOk();
    }

    /**
     * Add the controls for choosing the preverification configuration.
     * 
     * @param composite
     */
    private void addDebugLevelControls(Composite composite) {
        Group debugLevelSettingGroup = new Group(composite, SWT.NONE);
        debugLevelSettingGroup
                .setText(MTJUIMessages.PreprocessPreferencePage_debugLevelSettingGroup_label_text);
        debugLevelSettingGroup.setLayout(new GridLayout(2, false));
        debugLevelSettingGroup.setLayoutData(new GridData(
                GridData.FILL_HORIZONTAL));
        debugLevelLabel = new Label(debugLevelSettingGroup, SWT.NONE);
        debugLevelLabel.setFont(composite.getFont());
        debugLevelLabel
                .setText(MTJUIMessages.PreprocessPreferencePage_debugLevel_label_text);

        debugLevelCombo = new Combo(debugLevelSettingGroup, SWT.READ_ONLY);
        GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
        gridData.horizontalIndent = 15;
        debugLevelCombo.setLayoutData(gridData);

        String[] supportDebugLevels = PreprocessorHelper
                .getSupportDebugLevels();
        String[] items = new String[supportDebugLevels.length + 1];
        items[0] = DEBUGLEVEL_NONE;
        System.arraycopy(supportDebugLevels, 0, items, 1,
                supportDebugLevels.length);
        debugLevelCombo.setItems(items);
    }

    private void selectDebuglevel(String debuglevel) {
        int debuglevelIdx = 0;

        for (String level : debugLevelCombo.getItems()) {
            if (level.equalsIgnoreCase(debuglevel)) {
                break;
            }
            debuglevelIdx++;
        }
        if (debuglevelIdx < debugLevelCombo.getItems().length) {
            debugLevelCombo.select(debuglevelIdx);
        }

    }

    private void setControlsFromDefaults() {
        IPreferenceStore store = getPreferenceStore();
        String debuglevel = store
                .getDefaultString(IMTJCoreConstants.PREF_PREPROCESS_DEBUG_LEVEL);
        selectDebuglevel(debuglevel);
    }

    /**
     * Set the state of the controls based on the preferences.
     */
    private void setControlsFromPreferences() {
        IPreferenceStore store = getPreferenceStore();
        String debuglevel = store
                .getString(IMTJCoreConstants.PREF_PREPROCESS_DEBUG_LEVEL);
        selectDebuglevel(debuglevel);
    }

    /**
     * Set the state of the preferences based on the controls.
     */
    private void setPreferencesFromControls() {
        IPreferenceStore store = getPreferenceStore();

        int index = debugLevelCombo.getSelectionIndex();
        store.setValue(IMTJCoreConstants.PREF_PREPROCESS_DEBUG_LEVEL,
                debugLevelCombo.getItem(index));
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        addDebugLevelControls(composite);

        if (!embeddedInProperties) {
        } else {
            noDefaultAndApplyButton();
        }

        setControlsFromPreferences();

        return composite;
    }

}
