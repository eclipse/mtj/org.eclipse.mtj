/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Added preprocessing entries generation from 
 *                                device properties  
 *     Feng Wang (Sybase)       - Remove preprocessing entries generation 
 *                                feature, for  Multi-configuration support 
 *                                covers this feature.
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874] 
 */
package org.eclipse.mtj.internal.ui.devices;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;

/**
 * Implements the device import wizard functionality.
 * 
 * @author Craig Setera
 */
public class DeviceImportWizard extends Wizard {
    private DeviceImportWizardPage wizardPage;

    /**
     * Construct a new wizard instance
     */
    public DeviceImportWizard() {
        super();
        setNeedsProgressMonitor(true);
        setWindowTitle(MTJUIMessages.DeviceImportWizard_title);
        setDialogSettings(MTJUIPlugin.getDialogSettings(getClass().getName()));
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#addPages()
     */
    @Override
    public void addPages() {
        wizardPage = new DeviceImportWizardPage();
        addPage(wizardPage);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        boolean finished = false;

        MTJCore.getDeviceRegistry().enableDeviceAddedEvent(true);

        IDevice[] selectedDevices = wizardPage.getSelectedDevices();
        for (int i = 0; i < selectedDevices.length; i++) {
            IDevice device = selectedDevices[i];
            try {

                MTJCore.getDeviceRegistry().addDevice(device);
                finished = true;
            } catch (Exception e) {
                MTJCore
                        .log(
                                IStatus.WARNING,
                                MTJUIMessages.DeviceImportWizard_error_adding_new_device,
                                e);
                MTJUIPlugin.displayError(getShell(), IStatus.WARNING, -999,
                        MTJUIMessages.DeviceImportWizard_erro_dialog_title,
                        MTJUIMessages.DeviceImportWizard_error_dialog_message,
                        e);
            }
        }

        return finished;
    }
}
