/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Madruga (Motorola) - Initial version
 *     David Marques (Motorola) - Implementing class.
 *     David Marques (Motorola) - Fixing tag formatting behavior.
 *     
 *     
 * @since 0.9.1
 */
package org.eclipse.mtj.internal.jmunit.ui.actions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.ui.JavaElementLabelProvider;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.jmunit.IJMUnitContants;
import org.eclipse.mtj.internal.jmunit.core.api.JMUnitTestFinder;
import org.eclipse.mtj.internal.jmunit.core.api.TestSuiteWriter;
import org.eclipse.mtj.internal.jmunit.ui.wizards.testsuite.SuiteClassesContentProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.dialogs.ListSelectionDialog;

/**
 * UpdateJMUnitTestSuiteAction class updates the selected TestSuite tests.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class UpdateJMUnitTestSuiteAction implements IObjectActionDelegate {

    private class TestsContentProvider extends SuiteClassesContentProvider {

        private IType[] exclusion;

        public TestsContentProvider(IType[] exclusion) {
            if (exclusion == null) {
                this.exclusion = new IType[0];
            } else {
                this.exclusion = exclusion;
            }
        }

        @Override
        public void dispose() {
        }

        @Override
        public Object[] getElements(Object inputElement) {
            Object[] result = super.getElements(inputElement);

            List<Object> types = new ArrayList<Object>(Arrays.asList(result));
            for (IType excluded : exclusion) {
                if (types.contains(excluded)) {
                    types.remove(excluded);
                }
            }
            return result;
        }

        @Override
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

    }

    /**
     * Finds all classes that have the JMUnit Test as super class in the
     * specified package fragments. An exclusion list may be added to avoid
     * cycles between tests.
     * 
     * @param packageFragmentRoots target roots
     * @param exclusions excluded types
     * @return the list of tests types.
     * @throws CoreException any core error occurs.
     */
    public static IType[] findAllTests(
            IPackageFragmentRoot[] packageFragmentRoots, IType[] exclusions)
            throws CoreException {
        HashSet<IType> result = new HashSet<IType>();
        JMUnitTestFinder finder = null;

        for (IPackageFragmentRoot packageFragmentRoot : packageFragmentRoots) {
            if (packageFragmentRoot.getKind() == IPackageFragmentRoot.K_SOURCE) {
                finder = new JMUnitTestFinder(
                        IJMUnitContants.JMUNIT_TESTCASE_CLDC11);
                finder.findTestsInContainer(packageFragmentRoot, result, null);
            }
        }

        for (IType excluded : exclusions) {
            if (result.contains(excluded)) {
                result.remove(excluded);
            }
        }

        return result.toArray(new IType[result.size()]);
    }

    private ICompilationUnit testSuite;

    /* (non-Javadoc)
     * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
     */
    public void run(IAction action) {
        try {
            IType clazzType = testSuite.findPrimaryType();
            ITypeHierarchy hierarchy = clazzType
                    .newSupertypeHierarchy(new NullProgressMonitor());
            IType superType = hierarchy.getSuperclass(clazzType);
            if (superType.getFullyQualifiedName().equals(
                    IJMUnitContants.JMUNIT_TESTSUITE_CLDC11)) {
                IStructuredContentProvider contentProvider = new TestsContentProvider(
                        new IType[] { clazzType });
                ILabelProvider labelProvider = new JavaElementLabelProvider(
                        JavaElementLabelProvider.SHOW_DEFAULT);
                ListSelectionDialog dialog = new ListSelectionDialog(
                        Display.getDefault().getActiveShell(),
                        testSuite.getJavaProject(),
                        contentProvider,
                        labelProvider,
                        Messages.UpdateJMUnitTestSuiteAction_UpdateTestSuiteTests);
                dialog
                        .setTitle(Messages.UpdateJMUnitTestSuiteAction_UpdateTestSuite);
                dialog.setInitialSelections(findSuiteTests(clazzType));
                int result = dialog.open();
                if (result == Window.OK) {
                    updateTestSuiteSetupMethod(clazzType, dialog.getResult());
                }
            }
        } catch (CoreException e) {
            MessageDialog
                    .openError(
                            Display.getDefault().getActiveShell(),
                            Messages.UpdateJMUnitTestSuiteAction_ErrorUpdatingTestSuite,
                            e.getMessage());
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
     */
    public void selectionChanged(IAction action, ISelection selection) {
        if (selection instanceof StructuredSelection) {
            StructuredSelection structuredSelection = (StructuredSelection) selection;
            Object element = structuredSelection.getFirstElement();
            if (element instanceof ICompilationUnit) {
                testSuite = (ICompilationUnit) element;
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction, org.eclipse.ui.IWorkbenchPart)
     */
    public void setActivePart(IAction action, IWorkbenchPart targetPart) {
    }

    /**
     * Builds a regular expression for the test suite markers.
     * 
     * @return the regular expression string.
     */
    private String buildSetupSuiteMarkerExp() {
        StringBuffer regularExp = new StringBuffer();
        regularExp.append(IJMUnitContants.COMMENT_START);
        regularExp.append("(\\s)*"); //$NON-NLS-1$
        regularExp.append(IJMUnitContants.NON_COMMENT_START_MARKER);
        regularExp.append("(.)*"); //$NON-NLS-1$
        regularExp.append(IJMUnitContants.COMMENT_START);
        regularExp.append("(\\s)*"); //$NON-NLS-1$
        regularExp.append(IJMUnitContants.NON_COMMENT_END_MARKER);
        return regularExp.toString();
    }

    /**
     * Finds all tests contained in the type setupSuite method.
     * 
     * @param clazzType target type.
     * @return the list of tests types.
     * @throws CoreException any core error occurs.
     */
    private IType[] findSuiteTests(IType clazzType) throws CoreException {
        IType[] existingTypes = new IType[0];

        IMethod method = clazzType.getMethod("setupSuite", new String[0]); //$NON-NLS-1$
        if (method != null) {
            Pattern pattern = Pattern.compile(buildSetupSuiteMarkerExp(),
                    Pattern.DOTALL | Pattern.MULTILINE);
            Matcher matcher = pattern.matcher(method.getSource());
            if (matcher.find()) {
                StringBuffer buffer = new StringBuffer(matcher.group());
                int start = 0x00;
                int end = 0x00;

                end = buffer.indexOf(IJMUnitContants.NON_COMMENT_START_MARKER)
                        + IJMUnitContants.NON_COMMENT_START_MARKER.length();
                buffer.delete(0x00, end);

                start = buffer.indexOf(IJMUnitContants.COMMENT_START);
                end = buffer.indexOf(IJMUnitContants.NON_COMMENT_END_MARKER,
                        start)
                        + IJMUnitContants.NON_COMMENT_END_MARKER.length();
                buffer.delete(start, end);

                existingTypes = parseSetupSuite(buffer, clazzType
                        .getJavaProject());
            } else {
                MTJCore.throwCoreException(IStatus.ERROR, -999,
                        Messages.UpdateJMUnitTestSuiteAction_MarkersNotFound);
            }
        }
        return existingTypes;
    }

    /**
     * Finds all tests contained in the type setupSuite method region.
     * 
     * @param buffer region buffer.
     * @param javaProject parent project.
     * @return the list of tests types.
     * @throws CoreException any core error occurs.
     */
    private IType[] parseSetupSuite(StringBuffer buffer,
            IJavaProject javaProject) throws CoreException {
        List<String> testNames = new LinkedList<String>();
        List<IType> testTypes = new LinkedList<IType>();

        String region = buffer.toString();
        String[] adds = region.split(System.getProperty("line.separator")); //$NON-NLS-1$
        for (String add2 : adds) {
            String add = add2.trim();
            if (add.length() > 0x00) {
                int start = add.indexOf("new ") + "new ".length(); //$NON-NLS-1$ //$NON-NLS-2$
                int end = add.indexOf("(", start); //$NON-NLS-1$
                String className = add.substring(start, end);
                testNames.add(className);
            }
        }

        IType[] availableTests = findAllTests(javaProject
                .getPackageFragmentRoots(), new IType[0]);
        for (String test : testNames) {
            for (IType type : availableTests) {
                if (type.getFullyQualifiedName().equals(test)) {
                    testTypes.add(type);
                    break;
                }
            }
        }

        return testTypes.toArray(new IType[testTypes.size()]);
    }

    /**
     * Updates the Suite suiteSetup method.
     * 
     * @param clazzType target class
     * @param tests test types
     * @throws JavaModelException Any java model error.
     */
    private void updateTestSuiteSetupMethod(IType clazzType, Object[] tests)
            throws JavaModelException {
        List<String> classNames = new LinkedList<String>();
        for (Object object : tests) {
            IType type = (IType) object;
            classNames.add(type.getFullyQualifiedName());
        }
        TestSuiteWriter suiteWriter = new TestSuiteWriter(clazzType, clazzType
                .getElementName());
        suiteWriter.updateSetupSuiteMethod(classNames
                .toArray(new String[classNames.size()]),
                new NullProgressMonitor());
    }
}
