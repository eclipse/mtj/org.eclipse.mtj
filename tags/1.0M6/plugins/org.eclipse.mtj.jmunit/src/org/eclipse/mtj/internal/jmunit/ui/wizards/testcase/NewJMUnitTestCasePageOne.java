/**
 * Copyright (c) 2006,2008 Nokia and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nokia                    - Initial version
 *     Diego Madruga (Motorola) - Refactored some parts of code to follow MTJ 
 *                                standards
 *     Diego Madruga (Motorola) - Recreated wizard page code.
 */
package org.eclipse.mtj.internal.jmunit.ui.wizards.testcase;

import java.util.ArrayList;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageOne;
import org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageTwo;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.externallibrary.manager.ExternalLibraryManager;
import org.eclipse.mtj.internal.jmunit.IJMUnitContants;
import org.eclipse.mtj.internal.jmunit.JMUnitMessages;
import org.eclipse.mtj.internal.jmunit.JMUnitPlugin;
import org.eclipse.mtj.internal.jmunit.core.api.JMUnitTestFinder;
import org.eclipse.mtj.internal.jmunit.core.api.TestCaseWriter;
import org.eclipse.mtj.internal.jmunit.ui.part.MethodStubsSelectionButtonGroup;
import org.eclipse.mtj.internal.jmunit.util.JMUnitStatus;
import org.eclipse.mtj.internal.jmunit.util.LayoutUtil;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;

/**
 * The class NewJMUnitTestCasePageOne contains controls and validation routines
 * for the first page of the 'New JMUnit Test Case Wizard'.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class has been added as part of a work in
 * progress. There is no guarantee that this API will work or that it will
 * remain the same. Please do not use this API without consulting with the MTJ
 * team.
 * </p>
 * 
 * @author Gorkem Ercan
 * @since 0.9.1
 */
public class NewJMUnitTestCasePageOne extends NewTestCaseWizardPageOne {

    private final static int IDX_SETUP = 0;
    private final static int IDX_TEARDOWN = 1;

    private final static String STORE_SETUP = "NewTestCaseCreationWizardPage.USE_SETUP"; //$NON-NLS-1$
    private final static String STORE_TEARDOWN = "NewTestCaseCreationWizardPage.USE_TEARDOWN"; //$NON-NLS-1$

    private Label fImage;
    private boolean fIsJMunitCLDC11;
    private IStatus fJMunitCLDC11Status;
    private Link fLink;

    private MethodStubsSelectionButtonGroup fMethodStubsButtons;
    private NewTestCaseWizardPageTwo page2;

    /**
     * @param page2
     */
    public NewJMUnitTestCasePageOne(NewTestCaseWizardPageTwo page2) {
        super(page2);

        this.page2 = page2;
        setTitle(JMUnitMessages.NewJMUnitTestCasePageOne_title);
        setDescription(JMUnitMessages.NewJMUnitTestCasePageOne_description);

        String[] buttonNames = new String[] { /* IDX_SETUP */"set&Up()", //$NON-NLS-1$
                /* IDX_TEARDOWN */"&tearDown()" }; //$NON-NLS-1$

        fMethodStubsButtons = new MethodStubsSelectionButtonGroup(SWT.CHECK,
                buttonNames, 3);
        fMethodStubsButtons
                .setLabelText(JMUnitMessages.NewJMUnitTestCasePageOne_methodStubs_label);

        enableCommentControl(true);

        fJMunitCLDC11Status = new JMUnitStatus();
        fIsJMunitCLDC11 = true;
    }


    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageOne#createControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createControl(Composite parent) {

        initializeDialogUnits(parent);

        Composite composite = new Composite(parent, SWT.NONE);

        int nColumns = 4;

        GridLayout layout = new GridLayout();
        layout.numColumns = nColumns;

        composite.setLayout(layout);
        //createJMUnitCLDC11Controls(composite, nColumns);
        createContainerControls(composite, nColumns);
        createPackageControls(composite, nColumns);
        createSeparator(composite, nColumns);
        createTypeNameControls(composite, nColumns);
        createSuperClassControls(composite, nColumns);
        createMethodStubSelectionControls(composite, nColumns);
        createCommentControls(composite, nColumns);
        createSeparator(composite, nColumns);
        createClassUnderTestControls(composite, nColumns);
        createBuildPathConfigureControls(composite, nColumns);

        setControl(composite);

        // set default and focus
        String classUnderTest = getClassUnderTestText();

        if (classUnderTest.length() > 0) {
            setTypeName(Signature.getSimpleName(classUnderTest) + "Test", true); //$NON-NLS-1$
        }

        Dialog.applyDialogFont(composite);
        // PlatformUI.getWorkbench().getHelpSystem().setHelp(composite,
        // IJMUnitHelpContextIds.NEW_TESTCASE_WIZARD_PAGE);

        setFocus();
    }


    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageOne#init(org.eclipse.jface.viewers.IStructuredSelection)
     */
    @Override
    public void init(IStructuredSelection selection) {
        super.init(selection);
        restoreWidgetValues();
        setSuperClass(IJMUnitContants.JMUNIT_TESTCASE_CLDC11, true); //$NON-NLS-1$
        handleFieldChanged(CONTAINER);
        updateStatus(getStatusList());
    }

    /**
     * Returns <code>true</code> if the test should be created as JMunit CLDC
     * 1.1 test.
     * 
     * @return returns <code>true</code> if the test should be created as JMunit
     *         CLDC 1.1 test.
     */
    public boolean isJMUnit11() {
        return fIsJMunitCLDC11;
    }




    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageOne#setVisible(boolean)
     */
    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (!visible) {
            saveWidgetValues();
        }
    }

    /**
     * @param data
     */
    private void performBuildpathConfiguration(Object data) {
        IPackageFragmentRoot root = getPackageFragmentRoot();
        if (root == null) {
            return; // should not happen. Link shouldn't be visible
        }
        IJavaProject javaProject = root.getJavaProject();

        if ("cldc10".equals(data)) { //$NON-NLS-1$
            // add and configure JMUnit CLDC 1.0
            try {
                ExternalLibraryManager.getInstance().addLibraryToMidletProject(
                        javaProject.getProject(), "JMUnit for CLDC 1.0"); //$NON-NLS-1$
            } catch (CoreException e) {
                MTJCore.log(IStatus.ERROR, e);
            }
        } else if ("cldc11".equals(data)) { //$NON-NLS-1$
            // add and configure JMUnit CLDC 1.1
            try {
                ExternalLibraryManager.getInstance().addLibraryToMidletProject(
                        javaProject.getProject(), "JMUnit for CLDC 1.1"); //$NON-NLS-1$
            } catch (CoreException e) {
                MTJCore.log(IStatus.ERROR, e);
            }
        }
        handleFieldChanged(CONTAINER);
        updateBuildPathMessage();
    }

    private IType resolveClassNameToType(IJavaProject jproject,
            IPackageFragment pack, String classToTestName)
            throws JavaModelException {

        IType type = jproject.findType(classToTestName);

        // search in current package
        if ((type == null) && (pack != null) && !pack.isDefaultPackage()) {
            type = jproject.findType(pack.getElementName(), classToTestName);
        }

        // search in java.lang
        if (type == null) {
            type = jproject.findType("java.lang", classToTestName); //$NON-NLS-1$
        }
        return type;
    }

    /**
     * Use the dialog store to restore widget values to the values that they
     * held last time this wizard was used to completion
     */
    private void restoreWidgetValues() {

        IDialogSettings settings = getDialogSettings();
        if (settings != null) {
            fMethodStubsButtons.setSelection(IDX_SETUP, settings
                    .getBoolean(STORE_SETUP));
            fMethodStubsButtons.setSelection(IDX_TEARDOWN, settings
                    .getBoolean(STORE_TEARDOWN));
        } else {
            fMethodStubsButtons.setSelection(IDX_SETUP, false); // setUp
            fMethodStubsButtons.setSelection(IDX_TEARDOWN, false); // tearDown

        }
    }

    /**
     * Since Finish was pressed, write widget values to the dialog store so that
     * they will persist into the next invocation of this wizard page
     */
    private void saveWidgetValues() {
        IDialogSettings settings = getDialogSettings();
        if (settings != null) {
            settings
                    .put(STORE_SETUP, fMethodStubsButtons.isSelected(IDX_SETUP));
            settings.put(STORE_TEARDOWN, fMethodStubsButtons
                    .isSelected(IDX_TEARDOWN));
        }
    }

    private void updateBuildPathMessage() {
        if ((fLink == null) || fLink.isDisposed()) {
            return;
        }

        String message = null;
        IPackageFragmentRoot root = getPackageFragmentRoot();
        if (root != null) {
            try {
                IJavaProject project = root.getJavaProject();
                if (project.exists()) {
                    if (isJMUnit11()) {
                        if (project
                                .findType(IJMUnitContants.JMUNIT_TESTCASE_CLDC11) == null) { //$NON-NLS-1$
                            message = NLS
                                    .bind(
                                            JMUnitMessages.NewJMUnitTestCasePageOne_jmunit_cldc11_not_in_classpath,
                                            project.getElementName());
                        }
                    } else {
                        if (project
                                .findType(IJMUnitContants.JMUNIT_TESTCASE_CLDC10) == null) { //$NON-NLS-1$
                            message = NLS
                                    .bind(
                                            JMUnitMessages.NewJMUnitTestCasePageOne_jmunit_cldc10_not_in_classpath,
                                            project.getElementName());
                        }
                    }
                }
            } catch (JavaModelException e) {
            }
        }
        fLink.setVisible(message != null);
        fImage.setVisible(message != null);

        if (message != null) {
            fLink.setText(message);
        }
    }

    /**
     * Creates the controls for the JMUnit toggle control. Expects a
     * <code>GridLayout</code> with at least 3 columns.
     * 
     * @param composite the parent composite
     * @param nColumns number of columns to span
     * @see org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageOne#createBuildPathConfigureControls(org.eclipse.swt.widgets.Composite,
     *      int)
     */
    @Override
    protected void createBuildPathConfigureControls(Composite composite,
            int nColumns) {

        Composite inner = new Composite(composite, SWT.NONE);
        inner.setLayoutData(new GridData(GridData.FILL, GridData.FILL, false,
                false, nColumns, 1));
        GridLayout layout = new GridLayout(2, false);
        layout.marginWidth = 0;
        layout.marginHeight = 0;
        inner.setLayout(layout);

        fImage = new Label(inner, SWT.NONE);
        fImage
                .setImage(JFaceResources
                        .getImage(Dialog.DLG_IMG_MESSAGE_WARNING));
        fImage.setLayoutData(new GridData(GridData.BEGINNING,
                GridData.BEGINNING, false, false, 1, 1));

        fLink = new Link(inner, SWT.WRAP);
        fLink.setText("\n\n"); //$NON-NLS-1$

        fLink.addSelectionListener(new SelectionAdapter() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                performBuildpathConfiguration(e.text);
            }
        });
        GridData gd = new GridData(GridData.FILL, GridData.BEGINNING, true,
                false, 1, 1);
        gd.widthHint = convertWidthInCharsToPixels(60);
        fLink.setLayoutData(gd);
        updateBuildPathMessage();
    }


    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageOne#createMethodStubSelectionControls(org.eclipse.swt.widgets.Composite, int)
     */
    @Override
    protected void createMethodStubSelectionControls(Composite composite,
            int nColumns) {

        LayoutUtil.setHorizontalSpan(fMethodStubsButtons
                .getLabelControl(composite), nColumns);
        LayoutUtil.createEmptySpace(composite, 1);
        LayoutUtil.setHorizontalSpan(fMethodStubsButtons
                .getSelectionButtonsGroup(composite), nColumns - 1);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageOne#createTypeMembers(org.eclipse.jdt.core.IType, org.eclipse.jdt.ui.wizards.NewTypeWizardPage.ImportsManager, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    protected void createTypeMembers(IType type, ImportsManager imports,
            IProgressMonitor monitor) throws CoreException {
        TestCaseWriter writer = new TestCaseWriter(type, type.getElementName());

        boolean comment = this.isAddComments();
        boolean teardown = this.fMethodStubsButtons.isSelected(IDX_TEARDOWN);
        boolean setup = this.fMethodStubsButtons.isSelected(IDX_SETUP);
        boolean finalise = this.page2
                .getCreateFinalMethodStubsButtonSelection();
        boolean stubs = this.page2.isCreateTasks();

        writer.writeCode(this.page2.getCheckedMethods(), setup, teardown,
                comment, stubs, finalise, monitor);
    }


    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageOne#getStatusList()
     */
    @Override
    protected IStatus[] getStatusList() {
        ArrayList<IStatus> status = new ArrayList<IStatus>();
        for (IStatus stat : super.getStatusList()) {
            status.add(stat);
        }
        status.add(fJMunitCLDC11Status);

        return status.toArray(new IStatus[0]);
    }


    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageOne#superClassChanged()
     */
    @Override
    protected IStatus superClassChanged() {
        // replaces the super class validation of of the normal type wizard

        String superClassName = getSuperClass();
        IStatus status = new JMUnitStatus();

        if ((superClassName == null) || superClassName.trim().equals("")) { //$NON-NLS-1$
            status = JMUnitStatus
                    .createError(JMUnitMessages.NewJMUnitTestCasePageOne_empty_superclass);
            return status;
        }
        if (getPackageFragmentRoot() != null) {
            try {
                IType type = resolveClassNameToType(getPackageFragmentRoot()
                        .getJavaProject(), getPackageFragment(), superClassName);
                if (type == null) {
                    status = JMUnitStatus
                            .createError(JMUnitMessages.NewJMUnitTestCasePageOne_missing_superclass);
                    return status;
                }

                if (type.isInterface()) {
                    status = JMUnitStatus
                            .createError(JMUnitMessages.NewJMUnitTestCasePageOne_interface_as_superclass);
                    return status;
                }

                if (!(new JMUnitTestFinder(isJMUnit11() ? IJMUnitContants.JMUNIT_TESTCASE_CLDC11
                        : IJMUnitContants.JMUNIT_TESTCASE_CLDC10)).isTestImplementor(type)) {
                    status = JMUnitStatus
                            .createError(JMUnitMessages.NewJMUnitTestCasePageOne_superclass_not_text_impl
                                    + (isJMUnit11() ? IJMUnitContants.JMUNIT_TESTCASE_CLDC11
                                            : IJMUnitContants.JMUNIT_TESTCASE_CLDC10));
                    return status;
                }
            } catch (JavaModelException e) {
                JMUnitPlugin.log(e.getStatus());

            }
        }
        return status;
    }


    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageOne#handleFieldChanged(java.lang.String)
     */
    @Override
    protected void handleFieldChanged(String fieldName) {
        super.handleFieldChanged(fieldName);
        updateBuildPathMessage();

    }
}
