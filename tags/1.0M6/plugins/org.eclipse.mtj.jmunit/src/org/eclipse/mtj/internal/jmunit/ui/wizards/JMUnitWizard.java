/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.jdt.junit/JUnitWizard
 */
package org.eclipse.mtj.internal.jmunit.ui.wizards;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.dialogs.DialogSettings;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.internal.jmunit.JMUnitMessages;
import org.eclipse.mtj.internal.jmunit.JMUnitPlugin;
import org.eclipse.mtj.internal.ui.util.ExceptionHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyDelegatingOperation;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.wizards.newresource.BasicNewResourceWizard;

/**
 * The wizard base class for JMUnit creation wizards.
 * 
 * @since 0.9.1
 */
public abstract class JMUnitWizard extends Wizard implements INewWizard {

    /**
     * The name of the this wizard section in the {@link DialogSettings dialog
     * settings}.
     */
    protected static final String DIALOG_SETTINGS_KEY = "JMUnitWizards"; //$NON-NLS-1$

    /**
     * The current workbench
     */
    private IWorkbench fWorkbench;

    /**
     * The current object selection
     */
    private IStructuredSelection fSelection;

    /**
     * Creates a new JMUnitWizard.
     * <p>
     * This constructor defines that a progress monitor will be required and
     * sets the default page image descriptor for this wizard.
     * </p>
     */
    public JMUnitWizard() {
        setNeedsProgressMonitor(true);
        initializeDefaultPageImageDescriptor();
    }

    /**
     * Gets the current object selection defined in the invocation of
     * {@link #init(IWorkbench, IStructuredSelection)} method.
     * 
     * @return the current object selection.
     */
    public IStructuredSelection getSelection() {
        return fSelection;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
     */
    public void init(IWorkbench workbench, IStructuredSelection currentSelection) {
        fWorkbench = workbench;
        fSelection = currentSelection;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public abstract boolean performFinish();

    /**
     * Runs the given operation in the UI thread using the wizard context
     * 
     * @param runnable the runnable to a operation to be executed
     * @return
     *         <code>true<code> if the operation run successfully, <code>false</code>
     *         otherwise.
     */
    protected boolean finishPage(IRunnableWithProgress runnable) {
        IRunnableWithProgress op = new WorkspaceModifyDelegatingOperation(
                runnable);
        try {
            PlatformUI.getWorkbench().getProgressService().runInUI(
                    getContainer(), op,
                    ResourcesPlugin.getWorkspace().getRoot());

        } catch (InvocationTargetException e) {
            Shell shell = getShell();
            String title = JMUnitMessages.NewJUnitWizard_op_error_title;
            String message = JMUnitMessages.NewJUnitWizard_op_error_message;
            ExceptionHandler.handle(e, shell, title, message);
            return false;
        } catch (InterruptedException e) {
            return false;
        }
        return true;
    }

    /**
     * Initializes the dialog settings for this wizard.
     */
    protected void initDialogSettings() {
        IDialogSettings pluginSettings = JMUnitPlugin.getDefault()
                .getDialogSettings();
        IDialogSettings wizardSettings = pluginSettings
                .getSection(DIALOG_SETTINGS_KEY);
        if (wizardSettings == null) {
            wizardSettings = new DialogSettings(DIALOG_SETTINGS_KEY);
            pluginSettings.addSection(wizardSettings);
        }
        setDialogSettings(wizardSettings);
    }

    /**
     * Sets the default page image descriptor for this wizard.
     */
    protected abstract void initializeDefaultPageImageDescriptor();

    /**
     * Opens an editor on the given file resource.
     * 
     * @param resource the resource to be opened.
     */
    protected void openResource(final IResource resource) {
        if (resource.getType() == IResource.FILE) {
            final IWorkbenchPage activePage = JMUnitPlugin.getActivePage();
            if (activePage != null) {
                final Display display = Display.getDefault();
                if (display != null) {
                    display.asyncExec(new Runnable() {
                        public void run() {
                            try {
                                IDE.openEditor(activePage, (IFile) resource,
                                        true);
                            } catch (PartInitException e) {
                                JMUnitPlugin.log(e);
                            }
                        }
                    });
                }
            }
        }
    }

    /**
     * Attempts to select and reveal the specified resource in all parts within
     * the current workbench window's active page.
     * 
     * @param newResource the resource to be selected and revealed
     */
    protected void selectAndReveal(IResource newResource) {
        BasicNewResourceWizard.selectAndReveal(newResource, fWorkbench
                .getActiveWorkbenchWindow());
    }
}
