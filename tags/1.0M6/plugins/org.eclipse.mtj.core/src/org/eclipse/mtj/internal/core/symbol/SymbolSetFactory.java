/**
 * Copyright (c) 2003,2009 Motorola and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gustavo de Paula (Motorola) - Initial implementation
 */
package org.eclipse.mtj.internal.core.symbol;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.core.symbol.ISymbolSetFactory;
import org.xml.sax.SAXException;

/**
 * Provides a a factory to create symbol sets. those symbols are not automatically stored on the registry
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 * @noinstantiate This class is not intended to be instantiated by clients.
 */
public class SymbolSetFactory implements ISymbolSetFactory {

	/**
	 * Name of the file that represents the device on j2mepolish
	 */
    public static final String J2MEPOLISH_FILENAME_XML_DEVICES = "devices.xml"; //$NON-NLS-1$

	/**
	 * Name of the file that represents the group of device on j2mepolish
	 */
    public static final String J2MEPOLISH_FILENAME_XML_GROUPS = "groups.xml"; //$NON-NLS-1$
	
    /**
     * Singleton instance of the registry
     */
    private static final SymbolSetFactory instance = new SymbolSetFactory();
	
    /**
     * Returns the singleton instance of this class
     * 
     * @return singleton instance
     */
    public static ISymbolSetFactory getInstance () {
    	return instance;
    }

	public ISymbolSet createSymbolSetFromDevice(IDevice device) {
		ISymbolSet ss = SymbolUtils.createSymbolSet(device);		
		return ss;
	}

	public ISymbol createSymbol(String name, String value) {
		Symbol s = new Symbol(name, value);
		
		return s;
	}

	public ISymbolSet createSymbolSet(String name) {
		SymbolSet ss = new SymbolSet ();
		ss.setName(name);
		return ss;
	}

	public List<ISymbolSet> createSymbolSetFromDataBase(
			String type, URI input, IProgressMonitor monitor)
			throws IOException {
		List <ISymbolSet> result = null;
		if (type.equals(ISymbolSetFactory.DEVICE_DB_J2MEPOLISH_JAR)) {
			result = handleAntennaJAR (monitor, input);
		} else if (type.equals(ISymbolSetFactory.DEVICE_DB_J2MEPOLISH_FILE)) {
			result = handleFiles(monitor, input);
		}
		return result;
	}
    
	private List<ISymbolSet> handleAntennaJAR (IProgressMonitor monitor, URI input) throws IOException {
		List<ISymbolSet> result = null;
		JarFile antennaFileJar = null;
        InputStream devicesInputStream = null;
        InputStream groupsInputStream = null;
		try {
            // search devices.xml & groups.xml from Antenna Jar file
            antennaFileJar = new JarFile(new File(input.getRawPath()));
            Enumeration<JarEntry> e = antennaFileJar.entries();
            while (e.hasMoreElements()) {
                JarEntry entry = e.nextElement();
                if (entry
                        .getName()
                        .toLowerCase()
                        .equals(
                                SymbolSetFactory.J2MEPOLISH_FILENAME_XML_DEVICES)) {
                    devicesInputStream = antennaFileJar
                            .getInputStream(entry);
                }
                if (entry
                        .getName()
                        .toLowerCase()
                        .equals(
                        		SymbolSetFactory.J2MEPOLISH_FILENAME_XML_GROUPS)) {
                    groupsInputStream = antennaFileJar
                            .getInputStream(entry);
                }
            }
            if ((devicesInputStream != null)
                    && (groupsInputStream != null)) {
                try {
                	result = SymbolUtils.importFromJ2MEPolishFormat(monitor, devicesInputStream, groupsInputStream);
                } catch (Exception ex) {
                	throw new IOException ();	
                }
            }
        } catch (IOException e) {
            throw e;
        } finally {
            try {
            	if (antennaFileJar != null) {
            		antennaFileJar.close();
            	}
    			if (devicesInputStream != null) {
    				devicesInputStream.close();
    			}
    			if (groupsInputStream != null) {
    				groupsInputStream.close();
    			}
            } catch (Exception e) {
            }
        }
		
		return result;
	}

	private List<ISymbolSet> handleFiles (IProgressMonitor monitor, URI input) throws IOException {
		List<ISymbolSet> result = null;
        InputStream devicesInputStream = null;
        InputStream groupsInputStream = null;
        try {
            // search devices.xml & groups.xml from Antenna Jar file
            devicesInputStream = new FileInputStream(
                    new File(
                    		input
                                    + File.separator
                                    + SymbolSetFactory.J2MEPOLISH_FILENAME_XML_DEVICES));
            groupsInputStream = new FileInputStream(
                    new File(
                    		input
                                    + File.separator
                                    + SymbolSetFactory.J2MEPOLISH_FILENAME_XML_GROUPS));
            result = SymbolUtils.importFromJ2MEPolishFormat(monitor, devicesInputStream, groupsInputStream);
        } catch (IOException e) {
        	throw e;
        } catch (PersistenceException e) {
			throw new IOException ();
		} catch (ParserConfigurationException e) {
			throw new IOException ();
		} catch (SAXException e) {
			throw new IOException ();
		} finally {
			if (devicesInputStream != null) {
				devicesInputStream.close();
			}
			if (groupsInputStream != null) {
				groupsInputStream.close();
			}
		}
		
		return result;
	}
	
}
