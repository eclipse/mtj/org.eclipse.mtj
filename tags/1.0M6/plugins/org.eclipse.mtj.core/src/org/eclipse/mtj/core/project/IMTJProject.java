/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Gustavo de Paula (Motorola) - Runtime refactoring     
 */
package org.eclipse.mtj.core.project;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noimplement This class is not intended to be implemented by clients.
 * @since 1.0
 */
public interface IMTJProject {

    /**
     * @param projectListener
     */
    public abstract void addMTJProjectListener(
            IMTJProjectListener projectListener);

    /**
     * Create a deployed JAR file package for this MIDlet suite project.
     * 
     * @param monitor progress monitor
     * @param obfuscate a boolean indicating whether to obfuscate the resulting
     *            packaged code.
     * @param packageInactiveConfigs - a boolean indicating whether to package
     *            all configurations (including inactive configurations) or just
     *            package active configuration.
     * @throws CoreException
     */
    public abstract void createPackage(IProgressMonitor monitor,
            boolean obfuscate, boolean packageInactiveConfigs)
            throws CoreException;

    /**
     * Refresh the classpath for this project.
     * 
     * @param monitor
     * @return
     * @throws CoreException
     */
    public abstract void refreshClasspath(IProgressMonitor monitor)
            throws CoreException;

    /**
     * Return the underlying java project.
     * 
     * @return the underlying java project
     */
    public abstract IJavaProject getJavaProject();

    /**
     * Return the list of runtimes that are associated to the project. From the
     * list it is possible to read each runtime and information such as the
     * device of each runtime
     * 
     * @return MTJRuntimeList list of MTJRuntime
     */
    public abstract MTJRuntimeList getMTJRuntime();

    /**
     * Return the underlying project instance.
     * 
     * @return the underlying project
     */
    public abstract IProject getProject();

    /**
     * Get the ISignatureProperties associated with this MIDlet suite
     * 
     * @return the currently associated ISignatureProperties
     * @throws CoreException
     */
    public abstract ISignatureProperties getSignatureProperties()
            throws CoreException;

    /**
     * Return a boolean indicating whether the project's deployed Application
     * file exists and is up to date.
     * 
     * @return whether the deployed jar file is currently up to date.
     * @throws CoreException if there is a problem retrieving the information.
     */
    public abstract boolean isDeployedAppUpToDate() throws CoreException;

    /**
     * @param projectListener
     */
    public abstract void removeMTJProjectListener(
            IMTJProjectListener projectListener);

    /**
     * Save the MIDlet suite metadata. Should be called after
     * setPlatformDefinition or setSignatureProperties are called to persist the
     * information set.
     * 
     * @throws CoreException
     */
    public abstract void saveMetaData() throws CoreException;

    /**
     * Set the flag indicating whether or not the deployed jar file for this
     * MIDlet suite project is currently up to date.
     * 
     * @param upToDate whether the deployed jar file is up to date.
     * @throws CoreException if there is a problem setting the information.
     */
    public abstract void setDeployedAppFileUpToDate(boolean upToDate)
            throws CoreException;

    /**
     * Set the ISignatureProperties to use for this MIDlet suite
     * 
     * @param props
     * @throws CoreException
     */
    public abstract void setSignatureProperties(ISignatureProperties props)
            throws CoreException;

}