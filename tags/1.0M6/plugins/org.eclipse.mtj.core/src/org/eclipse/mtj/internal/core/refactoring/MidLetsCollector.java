/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.core.refactoring;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.IMTJCoreConstants;

/**
 * This class collect Midlets from a project or package.
 * 
 * @author wangf
 */
public class MidLetsCollector {
    /**
     * Collect MIDlet(s) within given IPackageFragment but not its child
     * IPackageFragment.
     * 
     * @param packageFragment
     * @param monitor
     * @return
     */
    public static Set<IType> collectMidletsInPackage(
            IPackageFragment packageFragment, IProgressMonitor monitor) {
        Set<IType> result = new HashSet<IType>();

        // get all MIDlets in the project
        List<IType> midlets = collectMidletsInProject(monitor, packageFragment
                .getJavaProject());
        // filter within the parent element
        // only collect MIDlets that are located in the package and child
        // packages
        for (IType midlet : midlets) {
            if (midlet.getPackageFragment().equals(packageFragment)) {
                result.add(midlet);
            }
        }
        return result;
    }

    /**
     * Collect MIDlets from project.
     * 
     * @param monitor
     * @param project
     * @return
     */
    public static List<IType> collectMidletsInProject(IProgressMonitor monitor,
            IJavaProject project) {
        IType[] types;
        HashSet<IType> result = new HashSet<IType>(5);
        try {
            IType midlet = project
                    .findType(IMTJCoreConstants.MIDLET_SUPERCLASS);
            ITypeHierarchy hierarchy = midlet.newTypeHierarchy(project,
                    new SubProgressMonitor(monitor, 1));
            types = hierarchy.getAllSubtypes(midlet);
            int length = types.length;
            if (length != 0) {
                for (int i = 0; i < length; i++) {
                    if (!types[i].isBinary()) {
                        result.add(types[i]);
                    }
                }
            }
        } catch (JavaModelException jme) {
        }
        monitor.done();
        return new ArrayList<IType>(result);
    }
}
