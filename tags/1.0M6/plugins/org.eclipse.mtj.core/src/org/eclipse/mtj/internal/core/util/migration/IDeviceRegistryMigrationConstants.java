/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.util.migration;

/**
 * @author Diego Madruga Sandin
 */
public interface IDeviceRegistryMigrationConstants {

    public static final String UEI_BUNDLE_NAME = "org.eclipse.mtj.toolkit.uei";

    public static final String UEI_DEVICE_CLASS = "org.eclipse.mtj.internal.toolkit.uei.UEIDeviceInternal";

    public static final String MICROEMU_BUNDLE_NAME = "org.eclipse.mtj.toolkit.microemu";

    public static final String MICROEMU_DEVICE_CLASS = "org.eclipse.mtj.internal.toolkit.microemu.MicroEmuDevice";

    public static final String MPOWERPLAYER_BUNDLE_NAME = "org.eclipse.mtj.toolkit.mpowerplayer";

    public static final String MPOWERPLAYER_DEVICE_CLASS = "org.eclipse.mtj.internal.toolkit.mpowerplayer.MpowerplayerDevice";

    public static final String ME4SE_BUNDLE_NAME = "org.eclipse.mtj.toolkit.me4se";

    public static final String ME4SE_DEVICE_CLASS = "org.eclipse.mtj.examples.toolkits.me4se.ME4SEDevice";

    public static final String MOT_BUNDLE_NAME = "org.eclipse.mtj.toolkit.motorola";

    public static final String MOT_DEVICE_CLASS = "org.eclipse.mtj.examples.toolkits.motorola.MotorolaDevice";

    public static final String CLASSPATH_CLASS = "org.eclipse.mtj.internal.core.sdk.device.DeviceClasspath";

    public static final String API_CLASS = "org.eclipse.mtj.core.sdk.device.midp.library.api.API";

    public static final String LIBRARY_CLASS = "org.eclipse.mtj.internal.core.sdk.device.midp.library.Library";

    public static final String API_TERMINATOR = ".API";

    public static final String LIBRARY_TERMINATOR = ".Library";

    public static final String PREVERIFIER_CLASS = "org.eclipse.mtj.internal.core.build.preverifier.StandardPreverifier";

    public static final String PARAMETERS_CLASS = "org.eclipse.mtj.internal.core.build.preverifier.StandardPreverifierParameters";

}
