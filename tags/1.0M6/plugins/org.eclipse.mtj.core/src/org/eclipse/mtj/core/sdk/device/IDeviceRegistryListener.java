/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.sdk.device;


/**
 * Implementations of this interface can subscribe to changes in the device
 * registry.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface IDeviceRegistryListener {

    /**
     * The specified device has been added to the registry.
     * 
     * @param device
     */
    public void deviceAdded(IDevice device);

    /**
     * The specified device has been removed to the registry.
     * 
     * @param device
     */
    public void deviceRemoved(IDevice device);
}
