/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.text.l10n;

import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.NodeDocumentHandler;

/**
 * @since 0.9.1
 */
public class L10nDocumentHandler extends NodeDocumentHandler {

    private L10nModel fModel;

    /**
     * @param reconciling
     */
    public L10nDocumentHandler(L10nModel model, boolean reconciling) {

        super(reconciling, model.getFactory());
        fModel = model;

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.NodeDocumentHandler#getDocument()
     */
    @Override
    protected IDocument getDocument() {
        return fModel.getDocument();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.NodeDocumentHandler#getRootNode()
     */
    @Override
    protected IDocumentElementNode getRootNode() {
        return fModel.getLocales();
    }

}
