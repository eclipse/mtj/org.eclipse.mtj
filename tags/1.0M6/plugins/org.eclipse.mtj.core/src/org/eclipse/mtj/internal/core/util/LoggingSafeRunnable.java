/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID.
 */
package org.eclipse.mtj.internal.core.util;

import org.eclipse.core.runtime.ISafeRunnable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.MTJCore;

/**
 * Simple implementation of the ISafeRunnable interface that logs any exceptions
 * that occur.
 * 
 * @author Craig Setera
 */
public abstract class LoggingSafeRunnable implements ISafeRunnable {

    /* (non-Javadoc)
     * @see org.eclipse.core.runtime.ISafeRunnable#handleException(java.lang.Throwable)
     */
    public void handleException(Throwable exception) {
        MTJCore.log(IStatus.WARNING, "Exception occurred invoking runnable",
                exception);
    }
}
