/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.core.project.runtime;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.runtime.event.IMTJRuntimeChangeListener;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeDeviceChangeEvent;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeNameChangeEvent;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeWorkspaceSymbolSetsChangeEvent;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.project.runtime.MTJRuntimeListUtils;
import org.eclipse.mtj.internal.core.symbol.SymbolUtils;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class contains configuration information for multi-configuration
 * support. Now it contains "device" and "symbol set" information, and a boolean
 * "active" to indicate if this configuration is the current active one. This
 * class may contains more information if need in future.
 * 
 * @since 1.0
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 * @see ISymbolSet
 */
public class MTJRuntime {

    // The elements and attributes in the meta data xml file.
    public static final String ATTR_NAME = "name";
    public static final String ATTR_ACTIVE = "active";
    public static final String ELEM_DEVICE = "device";
    public static final String ATTR_DEVICEGROUP = "group";
    public static final String ATTR_DEVICENAME = "name";
    public static final String ELEM_SYMBOL_SET = "symbolSet";
    public static final String ELEM_WORKSPACE_SYMBOLSET = "workspaceSymbolSet";
    public static final String ELEM_SYMBOL = "symbol";
    public static final String ATTR_VALUE = "value";
    /**
     * The name of the configuration, should be unique in project scope.
     */
    private String name;
    /**
     * The boolean value to indicate if this configuration is the current active
     * one.
     */
    private boolean active;
    /**
     * The device of the configuration.
     */
    private IDevice device;
    /**
     * The symbols of the configuration. For preprocessing support.
     */
    private ISymbolSet symbolSet;
    /**
     * SymbolSets from workspace scope.
     */
    private List<ISymbolSet> workspaceScopeSymbolSets;
    /**
     * The listeners listen to this configuration properties change event. <br>
     * <b>Note:</b>Since instance of configuration have a long life cycle (as
     * long as the Midlet project), so we should remove listener manually when
     * it no longer used.
     */
    private List<IMTJRuntimeChangeListener> listeners = new ArrayList<IMTJRuntimeChangeListener>();

    /**
     * Construct Configuration from meta data XML file.
     * 
     * @param configElement - The &#60configuration&#62 xml element.
     * @throws PersistenceException
     */
    public MTJRuntime(Element configElement) throws PersistenceException {
        name = configElement.getAttribute(ATTR_NAME);
        active = Boolean.valueOf(configElement.getAttribute(ATTR_ACTIVE));
        loadDevice(configElement);
        loadSymbolSet(configElement);
        loadWorkspaceSymbolSets(configElement);
    }

    public MTJRuntime(String name) {
        this.name = name;
    }

    public void addMTJRuntimeChangeListener(IMTJRuntimeChangeListener listener) {
        listeners.add(listener);
    }

    /**
     * If name equals, then configuration equals.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MTJRuntime other = (MTJRuntime) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    public void fireSymbolSetChanged() {
        for (IMTJRuntimeChangeListener listener : listeners) {
            listener.symbolSetChanged();
        }
    }

    public IDevice getDevice() {
        return device;
    }

    public String getName() {
        return name;
    }

    /**
     * All preprocessing related code should NOT use this method to get
     * ISymbolSet, instead, getSymbolSetForPreprocessing() should be used for
     * preprocessing purpose.
     * 
     * @return Symbol set associated with this runtime
     */
    public ISymbolSet getRuntimeSymbolSet() {
        if (symbolSet == null) {
            symbolSet =  MTJCore.getSymbolSetFactory().createSymbolSet("");
        }
        return symbolSet;
    }

    /**
     * All preprocessing related function should use this method to get
     * SymbolSet. This method return a SymbolSet for preprocessing. The
     * returned SymbolSet contains one more Symbol than SymbolSet returned
     * by this.getSymbolSet().
     * (key = * this.SYMBOLKEY_CONFIG_NAME, value=this.getName())
     * 
     * @return symbolset associated to this runtime
     */
    public ISymbolSet getRuntimeSymbolSetForPreprocessing() {
        ISymbolSet symbolSetForPreprocessing = MTJCore.getSymbolSetFactory().createSymbolSet("");
        symbolSetForPreprocessing.add(symbolSet.getSymbols());
        String configName = getName().replace(' ', '_');
        configName = SymbolUtils.replaceFirstNonLetterChar(configName);
        symbolSetForPreprocessing.add(configName, "true", ISymbol.TYPE_CONFIG);
        return symbolSetForPreprocessing;
    }

    /**
     * Returns a list of symbolsets that are on the workspace level. 
     * Those symbolsets are used during the code preprocessing
     * 
     * @return List of symbol sets on the workspace level
     */
    public List<ISymbolSet> getWorkspaceScopeSymbolSets() {
        if (workspaceScopeSymbolSets == null) {
            workspaceScopeSymbolSets = new ArrayList<ISymbolSet>();
        }
        return workspaceScopeSymbolSets;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public boolean isActive() {
        return active;
    }

    /**
     * Load device from xml.
     * 
     * @param configElement
     * @throws PersistenceException
     */
    private void loadDevice(Element configElement) throws PersistenceException {
        Element deviceElement = XMLUtils.getFirstElementWithTagName(
                configElement, ELEM_DEVICE);
        if (deviceElement != null) {
            String deviceGroup = deviceElement.getAttribute(ATTR_DEVICEGROUP);
            String deviceName = deviceElement.getAttribute(ATTR_DEVICENAME);
            device = MTJCore.getDeviceRegistry().getDevice(deviceGroup,
                    deviceName);
        }
    }

    /**
     * Load symbols from xml.
     * 
     * @param configElement
     */
    private void loadSymbolSet(Element configElement) {
        symbolSet = getRuntimeSymbolSet();
        Element symbolSetElement = XMLUtils.getFirstElementWithTagName(
                configElement, ELEM_SYMBOL_SET);
        NodeList symbols = symbolSetElement.getElementsByTagName(ELEM_SYMBOL);
        for (int i = 0; i < symbols.getLength(); i++) {
            Element symbolElement = (Element) symbols.item(i);
            String symbolName = symbolElement.getAttribute(ATTR_NAME);
            String symbolValue = symbolElement.getAttribute(ATTR_VALUE);
            symbolSet.add(symbolName, symbolValue);
        }
    }

    private void loadWorkspaceSymbolSets(Element configElement)
            throws PersistenceException {
        NodeList symbolSets = configElement
                .getElementsByTagName(ELEM_WORKSPACE_SYMBOLSET);
        if ((symbolSets == null) || (symbolSets.getLength() == 0)) {
            return;
        }
        workspaceScopeSymbolSets = new ArrayList<ISymbolSet>();
        for (int i = 0; i < symbolSets.getLength(); i++) {
            Element symbolSet = (Element) symbolSets.item(i);
            String symbolSetName = symbolSet.getAttribute(ATTR_NAME);
            ISymbolSet definitionSet = MTJCore.getSymbolSetRegistry()
                    .getSymbolSet(symbolSetName);
            if (definitionSet != null) {
                workspaceScopeSymbolSets.add(definitionSet);
            }
        }
    }

    /**
     * <b>Note:</b>Since instance of configuration have a long life cycle (as
     * long as the Midlet project), so we should remove listener manually when
     * it no longer used.
     * 
     * @param listener
     */
    public void removeMTJRuntimeChangeListener(
            IMTJRuntimeChangeListener listener) {
        listeners.remove(listener);
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @param device
     */
    public void setDevice(IDevice device) {
        // If device is the same object, just return
        if (this.device == device) {
            return;
        }
        IDevice oldDevice = this.device;
        this.device = device;
        // notify listeners
        MTJRuntimeDeviceChangeEvent event = new MTJRuntimeDeviceChangeEvent(
                this, oldDevice, device);
        for (IMTJRuntimeChangeListener listener : listeners) {
            listener.deviceChanged(event);
        }

    }

    /**
     * @param name
     */
    public void setName(String name) {
        if (this.name.equals(name)) {
            return;
        }
        String oldName = this.name;
        this.name = name;
        // notify listeners
        MTJRuntimeNameChangeEvent event = new MTJRuntimeNameChangeEvent(this,
                oldName, name);
        for (IMTJRuntimeChangeListener listener : listeners) {
            listener.nameChanged(event);
        }

    }

    public void setSymbolSet(ISymbolSet symbolSet) {
        this.symbolSet = symbolSet;
    }

    public void setWorkspaceScopeSymbolSets(List<ISymbolSet> symbolSets) {
        if (MTJRuntimeListUtils.workspaceSymbolsetsEquals(
                workspaceScopeSymbolSets, symbolSets)) {
            return;
        }
        List<ISymbolSet> oldSets = this.workspaceScopeSymbolSets;
        this.workspaceScopeSymbolSets = symbolSets;
        // notify listeners
        MTJRuntimeWorkspaceSymbolSetsChangeEvent event = new MTJRuntimeWorkspaceSymbolSetsChangeEvent(
                this, oldSets, symbolSets);
        for (IMTJRuntimeChangeListener listener : listeners) {
            listener.workspaceScopeSymbolSetsChanged(event);
        }

    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer().append("name=").append(name)
                .append("|active=").append(active).append("|device=").append(
                        device).append("|symbolSet=").append(symbolSet);
        return sb.toString();
    }
}
