/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin  (Motorola) - Initial Version
 *     David Marques (Motorola) - Chaning from PBEWithMD5AndTripleDES
 *                                to PBEWithMD5AndDES.
 */
package org.eclipse.mtj.internal.core.project.midp;

/**
 * @author Diego Madruga Sandin
 */
public interface IMetaDataConstants {

    /**
     * 
     */
    public static final String ATTR_DEVICEGROUP = "group";

    /**
     * 
     */
    public static final String ATTR_DEVICENAME = "name";

    /**
     * Java Application Descriptor data
     */
    public static final String ATTR_JAD_FILE = "jad";

    /**
     * 
     */
    public static final String ATTR_SIGN_PROJECT = "signProject";

    /**
     * 
     */
    public static final String ATTR_PROJECT_SPECIFIC = "projectSpecific";
    
    /**
     * 
     */
    public static final String ATTR_STOREPASSWORDS = "storePasswords";

    /**
     * 
     */
    public static final String CRYPTO_ALGORITHM = "PBEWithMD5AndDES";

    /**
     * PBE iteration count
     */
    public static final int CRYPTO_ITERATION_COUNT = 10;

    /**
     * PBE password
     */
    public static final String CRYPTO_PASS = "MTJ";

    /**
     * PBE 8-byte salt
     */
    public static final byte[] CRYPTO_SALT = { (byte) 0xc7, (byte) 0x73,
            (byte) 0x21, (byte) 0x8c, (byte) 0x7e, (byte) 0xc8, (byte) 0xee,
            (byte) 0x99 };

    /**
     * 
     */
    public static final String ELEM_ALIAS = "alias";

    /**
     * 
     */
    public static final String ELEM_CONFIGURATIONS = "configurations";

    /**
     * 
     */
    public static final String ELEM_DEVICE = "device";

    /**
     * 
     */
    public static final String ELEM_KEYSTORE = "keystore";

    /**
     * 
     */
    public static final String ELEM_KEYSTORETYPE = "keystoreType";

    /**
     * 
     */
    public static final String ELEM_PASSWORDS = "passwords";

    /**
     * 
     */
    public static final String ELEM_PROVIDER = "provider";

    /**
     * 
     */
    public static final String ELEM_PWD_KEY = "key";

    /**
     * 
     */
    public static final String ELEM_PWD_KEYSTORE = "keystore";

    /**
     * The metadata root element
     */
    public static final String ELEM_ROOT_NAME = "mtjMetadata";
    
    /**
     * 
     */
    public static final String ELEM_SIGNING = "signing";
    
    /**
     * 
     */
    public static final String KEYRING_KEYPASS_KEY = "KeyPass";
    
    /**
     * 
     */
    public static final String KEYRING_KEYSTOREPASS_KEY = "KeystorePass";
    
    /**
     * 
     */
    public static final String KEYRING_URL_BASE = "projects.mtj/";

}
