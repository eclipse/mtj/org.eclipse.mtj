/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 */
package org.eclipse.mtj.internal.core.build.preprocessor;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.ClasspathContainerInitializer;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.MTJCore;

/**
 * A {@link ClasspathContainerInitializer} implementation for initializing the
 * classpath container for a preprocessed project to point to another project.
 * 
 * @author Craig Setera
 */
public class PreprocessedProjectClasspathInitializer extends
        ClasspathContainerInitializer {

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jdt.core.ClasspathContainerInitializer#canUpdateClasspathContainer(org.eclipse.core.runtime.IPath,
     *      org.eclipse.jdt.core.IJavaProject)
     */
    @Override
    public boolean canUpdateClasspathContainer(IPath containerPath,
            IJavaProject project) {
        boolean canUpdate = false;

        try {
            canUpdate = isPreprocessorContainerPath(containerPath)
                    && project.getProject().hasNature(
                            IMTJCoreConstants.J2ME_PREPROCESSED_NATURE_ID);
        } catch (CoreException e) {
            MTJCore.log(IStatus.WARNING, e);
        }

        return canUpdate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jdt.core.ClasspathContainerInitializer#initialize(org.eclipse.core.runtime.IPath,
     *      org.eclipse.jdt.core.IJavaProject)
     */
    @Override
    public void initialize(IPath containerPath, IJavaProject project)
            throws CoreException {
        if (isPreprocessorContainerPath(containerPath)) {
            initializeContainer(containerPath, project);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jdt.core.ClasspathContainerInitializer#requestClasspathContainerUpdate(org.eclipse.core.runtime.IPath,
     *      org.eclipse.jdt.core.IJavaProject,
     *      org.eclipse.jdt.core.IClasspathContainer)
     */
    @Override
    public void requestClasspathContainerUpdate(IPath containerPath,
            IJavaProject project, IClasspathContainer containerSuggestion)
            throws CoreException {
        initializeContainer(containerPath, project);
    }

    /**
     * This is our container path. Go ahead and initialize.
     * 
     * @param containerPath
     * @param project
     * @throws JavaModelException
     */
    private void initializeContainer(IPath containerPath, IJavaProject project)
            throws JavaModelException {
        String referencedProjectName = containerPath.segment(1);
        IProject referencedProject = ResourcesPlugin.getWorkspace().getRoot()
                .getProject(referencedProjectName);

        if ((referencedProject != null) && (referencedProject.exists())) {
            IJavaProject referencedJavaProject = JavaCore
                    .create(referencedProject);
            IClasspathContainer[] containers = new IClasspathContainer[] { new PreprocessedProjectClasspathContainer(
                    referencedJavaProject) };

            IJavaProject[] projects = new IJavaProject[] { project };

            JavaCore.setClasspathContainer(containerPath, projects, containers,
                    null);
        }
    }

    /**
     * Return a boolean if the specified path represents a preprocessed project
     * path.
     * 
     * @param path
     * @return
     */
    private boolean isPreprocessorContainerPath(IPath path) {
        boolean isPath = false;

        if (path.segmentCount() == 2) {
            isPath = path.segment(0).equals(
                    IMTJCoreConstants.J2ME_PREPROCESSED_CONTAINER);
        }

        return isPath;
    }
}
