/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.core.project.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.runtime.event.AddMTJRuntimeEvent;
import org.eclipse.mtj.core.project.runtime.event.IMTJRuntimeListChangeListener;
import org.eclipse.mtj.core.project.runtime.event.RemoveMTJRuntimeEvent;
import org.eclipse.mtj.core.project.runtime.event.SwitchActiveMTJRuntimeEvent;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class is used to maintains configurations for Midlet project. Each
 * Midlet project have one instance of this class. We retrieve the instance by
 * IMidletSuiteProject#getConfigurations().
 * 
 * @since 1.0
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 * @see MTJRuntime
 */
public class MTJRuntimeList extends ArrayList<MTJRuntime> {

    private static final long serialVersionUID = -8304504334314517586L;
    /**
     * The configuration element name the meta data xml file.
     */
    public static final String ELEM_CONFIGURATION = "configuration";
    /**
     * The listeners listen to Configuration change event.<br>
     * <b>Note:</b>Since instance of Configuration have a long life cycle (as
     * long as the Midlet project), so we should remove listener manually when
     * it no longer used.
     */
    private List<IMTJRuntimeListChangeListener> listeners = new ArrayList<IMTJRuntimeListChangeListener>();

    /**
     * The constructor used for create Configuration for a Midlet project that
     * have not yet configurations.
     */
    public MTJRuntimeList() {
    }

    /**
     * Construct Configuration from meta data XML file.
     * 
     * @param configsElement - The &#60configurations&#62 xml element.
     * @throws PersistenceException
     */
    public MTJRuntimeList(Element configsElement) throws PersistenceException {
        populateMTJRuntimeList(configsElement);
    }

    @Override
    public boolean add(MTJRuntime config) {
        boolean result = super.add(config);
        if (result == false) {
            return result;
        }
        // notify event listener
        AddMTJRuntimeEvent event = new AddMTJRuntimeEvent(this, config);
        for (IMTJRuntimeListChangeListener listener : listeners) {
            listener.mtjRuntimeAdded(event);
        }
        return result;
    }

    @Override
    public boolean addAll(Collection<? extends MTJRuntime> configs) {
        boolean modified = false;
        for (MTJRuntime c : configs) {
            modified = this.add(c);
        }
        return modified;
    }

    public void addMTJRuntimeListChangeListener(IMTJRuntimeListChangeListener listener) {
        listeners.add(listener);
    }

    /**
     * Get the active configuration. If no active one, will return null.
     * 
     * @return
     */
    public MTJRuntime getActiveMTJRuntime() {
        for (MTJRuntime c : this) {
            if (c.isActive()) {
                return c;
            }
        }
        return null;
    }

    /**
     * Read configurations from xml and put them into Configuration object.
     * 
     * @param configsElement
     * @throws PersistenceException
     */
    private void populateMTJRuntimeList(Element configsElement)
            throws PersistenceException {
        NodeList configs = configsElement
                .getElementsByTagName(ELEM_CONFIGURATION);
        for (int i = 0; i < configs.getLength(); i++) {
            MTJRuntime config = new MTJRuntime((Element) configs.item(i));
            add(config);
        }
    }

    @Override
    public boolean remove(Object o) {
        boolean result = super.remove(o);
        if (result == false) {
            return result;
        }
        if (o instanceof MTJRuntime) {
            MTJRuntime removedConfig = (MTJRuntime) o;
            RemoveMTJRuntimeEvent event = new RemoveMTJRuntimeEvent(this, removedConfig);
            for (IMTJRuntimeListChangeListener listener : listeners) {
                listener.mtjRuntimeRemoved(event);
            }
        }
        return result;
    }

    @Override
    public boolean removeAll(Collection<?> configs) {
        boolean modified = false;
        for (Object o : configs) {
            modified = this.remove(o);
        }
        return modified;
    }

    /**
     * <b>Note:</b>Since instance of Configuration have a long life cycle (as
     * long as the Midlet project), so we should remove listener manually when
     * it no longer used.
     * 
     * @param listener
     */
    public void removeMTJRuntimeListChangeListener(
            IMTJRuntimeListChangeListener listener) {
        listeners.remove(listener);
    }

    /**
     * Switch the current active configuration.
     * 
     * @param configuration - configuration to be set as active one.
     */
    public void switchActiveMTJRuntime(MTJRuntime configuration) {
        MTJRuntime oldActiveConfig = getActiveMTJRuntime();
        if (oldActiveConfig != null) {
            getActiveMTJRuntime().setActive(false);
        }
        configuration.setActive(true);
        // notify event listener
        SwitchActiveMTJRuntimeEvent event = new SwitchActiveMTJRuntimeEvent(this,
                oldActiveConfig, configuration);
        for (IMTJRuntimeListChangeListener listener : listeners) {
            listener.activeMTJRuntimeSwitched(event);
        }
    }
}
