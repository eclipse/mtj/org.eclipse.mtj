/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Hugo Raniere (Motorola)  - Removing Preprocessor code
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code
 */
package org.eclipse.mtj.internal.core.launching;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.sourcelookup.ISourceContainer;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.sourcelookup.containers.JavaProjectSourceContainer;
import org.eclipse.jdt.launching.sourcelookup.containers.JavaSourcePathComputer;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.build.preprocessor.PreprocessorBuilder;

/**
 * Overrides the standard {@link JavaSourcePathComputer} to point the source
 * folder into the preprocessed project.
 * 
 * @author Craig Setera
 */
public class EmulationJavaSourcePathComputer extends JavaSourcePathComputer {

    public static final String ID = "org.eclipse.mtj.core.launching.emulationSourcePathComputer";

    /* (non-Javadoc)
     * @see org.eclipse.jdt.launching.sourcelookup.containers.JavaSourcePathComputer#computeSourceContainers(org.eclipse.debug.core.ILaunchConfiguration, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public ISourceContainer[] computeSourceContainers(
            ILaunchConfiguration configuration, IProgressMonitor monitor)
            throws CoreException {
        ISourceContainer[] containers = super.computeSourceContainers(
                configuration, monitor);

        if (isPreprocessingProject(configuration)) {
            for (int i = 0; i < containers.length; i++) {
                if (containers[i] instanceof JavaProjectSourceContainer) {
                    IJavaProject javaProject = ((JavaProjectSourceContainer) containers[i])
                            .getJavaProject();

                    IProject srcProject = javaProject.getProject();
                    IProject tgtProject = PreprocessorBuilder
                            .getOutputProject(srcProject);
                    if (tgtProject.exists()) {
                        IJavaProject tgtJavaProject = JavaCore
                                .create(tgtProject);
                        if (tgtJavaProject.exists()) {
                            containers[i] = new JavaProjectSourceContainer(
                                    tgtJavaProject);
                        }
                    }
                }
            }
        }

        return containers;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.launching.sourcelookup.containers.JavaSourcePathComputer#getId()
     */
    @Override
    public String getId() {
        return ID;
    }

    /**
     * Return a boolean indicating whether the project being launched by the
     * configuration is a preprocessing project.
     * 
     * @param configuration
     * @return
     */
    private boolean isPreprocessingProject(ILaunchConfiguration configuration) {
        boolean preprocessing = false;

        try {
            String projectName = configuration.getAttribute(
                    IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, "");
            IProject project = ResourcesPlugin.getWorkspace().getRoot()
                    .getProject(projectName);

            if (project.exists()) {
                preprocessing = project
                        .hasNature(IMTJCoreConstants.J2ME_PREPROCESSING_NATURE_ID);
            }

        } catch (CoreException e) {
            MTJCore.log(IStatus.WARNING, e);
        }

        return preprocessing;
    }
}
