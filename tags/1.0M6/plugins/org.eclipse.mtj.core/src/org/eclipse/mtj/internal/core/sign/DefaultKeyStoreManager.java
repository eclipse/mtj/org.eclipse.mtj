/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Implementing operations based on keytool.
 */
package org.eclipse.mtj.internal.core.sign;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.osgi.util.NLS;

import com.ibm.icu.text.SimpleDateFormat;

import de.schlichtherle.io.FileInputStream;

/**
 * DefaultKeyStoreManager class is an {@link IKeyStoreManager} default
 * implementation using java.security API and keytool command line tool
 * shipped on the Java Runtime.
 * 
 * @author David Marques
 * @since 1.0
 */
public class DefaultKeyStoreManager implements IKeyStoreManager {

	private static final String FILE = "-file"; //$NON-NLS-1$
	private static final String SIGALG_VALUE = "SHA1withRSA"; //$NON-NLS-1$
	private static final String SIGALG = "-sigalg"; //$NON-NLS-1$
	private static final String KEYPASS = "-keypass"; //$NON-NLS-1$
	private static final String DNAME = "-dname"; //$NON-NLS-1$
	private static final String ALIAS = "-alias"; //$NON-NLS-1$
	private static final String KEYALG_VALUE = "RSA"; //$NON-NLS-1$
	private static final String KEYALG = "-keyalg"; //$NON-NLS-1$
	private static final String STORETYPE = "-storetype"; //$NON-NLS-1$
	private static final String COM_SUN_CRYPTO_PROVIER_NAME = "com.sun.crypto.provier.{0}"; //$NON-NLS-1$
	private static final String PROVIDER = "-provider"; //$NON-NLS-1$
	private static final String NEW = "-new"; //$NON-NLS-1$
	private static final String STOREPASS = "-storepass"; //$NON-NLS-1$
	private static final String KEYSTORE = "-keystore"; //$NON-NLS-1$
	private static final String STOREPASSWD = "-storepasswd"; //$NON-NLS-1$
	private static final String KEYTOOL = "keytool"; //$NON-NLS-1$
	
	private File   ksFile;
	private String ksPassword;
	private String ksType;
	private String ksProvider;
	
	/**
	 * Creates a DefaultKeyStoreManager instance to manage
	 * the specified keystore file using the specified keystore
	 * password.
	 * 
	 * @param _ksFile Keystore file.
	 * @param _ksPassword Keystore password.
	 * @throws IllegalArgumentException - In case either the file
	 *         or password is null or the file does not exist. 							 
	 */
	public DefaultKeyStoreManager(File _ksFile, String _ksPassword) {
		if (_ksFile == null) {
			throw new IllegalArgumentException(Messages.DefaultKeyStoreManager_keyStoreNotNull);
		}
		
		if (!_ksFile.isFile()) {
			throw new IllegalArgumentException(Messages.DefaultKeyStoreManager_invalidKeystoreFile);
		}
		
		if (_ksPassword == null) {
			throw new IllegalArgumentException(Messages.DefaultKeyStoreManager_keystorePasswordNotNull);
		}
		
		if (_ksPassword.trim().length() == 0x00) {
			throw new IllegalArgumentException(Messages.DefaultKeyStoreManager_keystorePasswordEmpty);
		}
		
		this.ksType = KeyStore.getDefaultType();
		this.ksFile = _ksFile;
		this.ksPassword = _ksPassword;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.sign.IKeyStoreManager#setProvider(java.lang.String)
	 */
	public void setProvider(String _provider) {
		if (_provider != null && _provider.length() > 0x00) {			
			this.ksProvider = _provider;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.sign.IKeyStoreManager#setType(java.lang.String)
	 */
	public void setKeystoreType(String _type) {
		if (_type != null && _type.length() > 0x00) {			
			this.ksType = _type;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.sign.IKeyStoreManager#getPrivateKeysAliases()
	 */
	public List<String> getPrivateKeysAliases() throws KeyStoreManagerException{
		List<String> privateKeysAliases = new ArrayList<String>();
		
		try {
			KeyStore keyStore = openKeyStore();
			Enumeration<String> aliases = keyStore.aliases();
			while (aliases.hasMoreElements()) {
				String alias = (String) aliases.nextElement();
				if (keyStore.entryInstanceOf(alias, PrivateKeyEntry.class)) {
					privateKeysAliases.add(alias);
				}
			}
		} catch (KeyStoreException e) {
			String message = NLS.bind(Messages.DefaultKeyStoreManager_unableToAccessKeystoreContent, e.getMessage());
			throw new KeyStoreManagerException(message);
		}
		return privateKeysAliases;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.sign.IKeyStoreManager#getCertificateInformation(java.lang.String)
	 */
	public String getCertificateInformation(String alias) throws KeyStoreManagerException {
		String data = null;
		try {
			KeyStore keyStore = openKeyStore();
			Enumeration<String> aliases = keyStore.aliases();
			while (aliases.hasMoreElements()) {
				String current = (String) aliases.nextElement();
				if (!current.equals(alias)) {
					continue;
				}
				
				Certificate certificate = keyStore.getCertificate(alias);
				SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy"); //$NON-NLS-1$
				
				String fingerprint = buildFingerPrint(certificate.getEncoded());
				data = NLS.bind(Messages.DefaultKeyStoreManager_certificateInfoFormat
						, format.format(keyStore.getCreationDate(alias)), fingerprint);
				break;
			}
		} catch (KeyStoreException e) {
			String message = NLS.bind(Messages.DefaultKeyStoreManager_unableToAccessKeystoreContent, e.getMessage());
			throw new KeyStoreManagerException(message);
		} catch (CertificateEncodingException e) {
			String message = NLS.bind(Messages.DefaultKeyStoreManager_unableToGetEncodedCertificate, e.getMessage());
			throw new KeyStoreManagerException(message);
		} catch (NoSuchAlgorithmException e) {
			String message = NLS.bind(Messages.DefaultKeyStoreManager_unableToCreateMD5Fingerprint, e.getMessage());
			throw new KeyStoreManagerException(message);
		}
		return data;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.sign.IKeyStoreManager#changeKeystorePassword(java.lang.String)
	 */
	public void changeKeystorePassword(String password) throws KeyStoreManagerException {
		List<String> command = new LinkedList<String>();
		command.add(KEYTOOL);
		command.add(STOREPASSWD);
		command.add(KEYSTORE);
		command.add(this.ksFile.getAbsolutePath());
		command.add(STOREPASS);
		command.add(this.ksPassword);
		command.add(NEW);
		command.add(password);
		
		if (this.ksProvider != null) {
			command.add(PROVIDER);
			command.add(NLS.bind(COM_SUN_CRYPTO_PROVIER_NAME, this.ksProvider));
		}
		if (this.ksType != null) {
			command.add(STORETYPE);
			command.add(this.ksType);
		} 
		keytool(command.toArray(new String[0]));
		this.ksPassword = password;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.sign.IKeyStoreManager#genertaKeyPair(org.eclipse.mtj.internal.core.sign.KeyPairInfo)
	 */
	public void generateKeyPair(KeyPairInfo keyPairInfo) throws KeyStoreManagerException {
		List<String> command = new LinkedList<String>();
		command.add(KEYTOOL);
		command.add("-genkey"); //$NON-NLS-1$
		command.add(KEYALG);
		command.add(KEYALG_VALUE);
		command.add(KEYSTORE);
		command.add(this.ksFile.getAbsolutePath());
		command.add(STOREPASS);
		command.add(this.ksPassword);
		command.add(ALIAS);
		command.add(keyPairInfo.getAlias());
		command.add(DNAME);
		command.add(keyPairInfo.toString());
		command.add(KEYPASS);
		command.add(keyPairInfo.getPassword());
		
		if (this.ksProvider != null) {
			command.add(PROVIDER);
			command.add(NLS.bind(COM_SUN_CRYPTO_PROVIER_NAME, this.ksProvider));
		}
		if (this.ksType != null) {
			command.add(STORETYPE);
			command.add(this.ksType);
		} 
		keytool(command.toArray(new String[0]));
	}
	
	
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.sign.IKeyStoreManager#deleteKeyPair(java.lang.String)
	 */
	public void deleteKeyPair(String alias) throws KeyStoreManagerException {
		List<String> command = new LinkedList<String>();
		command.add(KEYTOOL);
		command.add("-delete"); //$NON-NLS-1$
		command.add(KEYSTORE);
		command.add(this.ksFile.getAbsolutePath());
		command.add(STOREPASS);
		command.add(this.ksPassword);
		command.add(ALIAS);
		command.add(alias);
		
		if (this.ksProvider != null) {
			command.add(PROVIDER);
			command.add(NLS.bind(COM_SUN_CRYPTO_PROVIER_NAME, this.ksProvider));
		}
		if (this.ksType != null) {
			command.add(STORETYPE);
			command.add(this.ksType);
		} 
		keytool(command.toArray(new String[0]));
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.sign.IKeyStoreManager#generateCSR(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void generateCSR(String alias, String password, String destination) throws KeyStoreManagerException {
		List<String> command = new LinkedList<String>();
		command.add(KEYTOOL);
		command.add("-certreq"); //$NON-NLS-1$
		command.add(SIGALG);
		command.add(SIGALG_VALUE);
		command.add(KEYSTORE);
		command.add(this.ksFile.getAbsolutePath());
		command.add(STOREPASS);
		command.add(this.ksPassword);
		command.add(ALIAS);
		command.add(alias);
		command.add(KEYPASS);
		command.add(password);
		command.add(FILE);
		command.add(destination);
		
		if (this.ksProvider != null) {
			command.add(PROVIDER);
			command.add(NLS.bind(COM_SUN_CRYPTO_PROVIER_NAME, this.ksProvider));
		}
		if (this.ksType != null) {
			command.add(STORETYPE);
			command.add(this.ksType);
		} 
		keytool(command.toArray(new String[0]));
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.sign.IKeyStoreManager#importCertificate(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void importCertificate(String alias, String password, String certPath) throws KeyStoreManagerException {
		List<String> command = new LinkedList<String>();
		command.add(KEYTOOL);
		command.add("-import"); //$NON-NLS-1$
		command.add("-noprompt"); //$NON-NLS-1$
		command.add("-trustcacerts"); //$NON-NLS-1$
		command.add(KEYSTORE);
		command.add(this.ksFile.getAbsolutePath());
		command.add(STOREPASS);
		command.add(this.ksPassword);
		command.add(ALIAS);
		command.add(alias);
		command.add(KEYPASS);
		command.add(password);
		command.add(FILE);
		command.add(certPath);
		
		if (this.ksProvider != null) {
			command.add(PROVIDER);
			command.add(NLS.bind(COM_SUN_CRYPTO_PROVIER_NAME, this.ksProvider));
		}
		if (this.ksType != null) {
			command.add(STORETYPE);
			command.add(this.ksType);
		} 
		keytool(command.toArray(new String[0]));
	}
	
	/**
	 * Sends a command to keytool command line application.
	 * 
	 * @param command command and its arguments.
	 * @throws KeyStoreManagerException - Any error occurs.
	 */
	private void keytool(String[] command) throws KeyStoreManagerException {
		try {
			Process process = Runtime.getRuntime().exec(command);
			while(true){ //Avoids returning in case of an InterruptedException
				try {
					int exit = process.waitFor();	
					if (exit != 0x00) {
						String message = getProcessErrorMessage(process);
						throw new KeyStoreManagerException(message);
					}
					break;
				} catch (InterruptedException e) {
					/*Keep waiting for process*/
				}
			}
		} catch (IOException e) {
			throw new KeyStoreManagerException(e.getMessage());
		}
	}

	/**
	 * Gets the error message generated by the specified process.
	 * 
	 * @param process source process.
	 * @return the error String.
	 * @throws IOException Any error reading from the error stream
	 * occurs.
	 */
	private String getProcessErrorMessage(Process process) throws IOException {
		BufferedReader inStream = new BufferedReader(new InputStreamReader(process.getInputStream()));
		StringBuffer   buffer   = new StringBuffer();
		String		   result   = Messages.DefaultKeyStoreManager_22;
		
		String line = null;
		do {
			line = inStream.readLine();
			if (line == null) {
				break;
			}
			buffer.append(line.trim());
		} while (line != null);
		
		/*
		 * Extractcs keytool error message description
		 * Error format: 
		 * 
		 * keytool error: <Exception>: <Description> 
		 */
		Pattern pattern = Pattern.compile("[keytool error:].*[:]"); //$NON-NLS-1$
		String  array[] = pattern.split(buffer.toString());
		if (array.length == 0x02) {
			result = array[0x01];
		}
		return result;
	}

	/**
	 * Builds the certificate MD5 fingerprint.
	 * 
	 * @param encoded certificate's encoded data.
	 * @return String containing the fingerprint.
	 * @throws NoSuchAlgorithmException - In case MD5
	 * is not supported.
	 */
	private String buildFingerPrint(byte[] encoded) throws NoSuchAlgorithmException {
		StringBuffer buffer = new StringBuffer();
		MessageDigest md = MessageDigest.getInstance("MD5"); //$NON-NLS-1$
		byte[] digest = md.digest(encoded);
		for (byte b : digest) {
			if (buffer.length() > 0) {
				buffer.append(":"); //$NON-NLS-1$
			}
			String hex = Integer.toHexString((int)(b & 0xFF)).toUpperCase();
			buffer.append(hex.length() > 1 ? hex : NLS.bind("0{0}", hex)); //$NON-NLS-1$
		}
		return buffer.toString();
	}

	/**
	 * Opens and Loads the keystore.
	 * 
	 * @return keystore loaded instance.
	 * @throws KeyStoreManagerException Any keystore management error occurs.
	 */
	private KeyStore openKeyStore() throws KeyStoreManagerException {
		KeyStore keyStore = null;
		try {
			if (this.ksProvider != null) {			
				keyStore = KeyStore.getInstance(this.ksType, this.ksProvider);
			} else {
				keyStore = KeyStore.getInstance(this.ksType);
			}
			keyStore.load(new FileInputStream(this.ksFile), this.ksPassword.toCharArray());
		} catch (Exception e) {
			String message = NLS.bind(Messages.DefaultKeyStoreManager_unableToOpenKeystore, e.getMessage());
			throw new KeyStoreManagerException(message);
		}
		return keyStore;
	}
}
