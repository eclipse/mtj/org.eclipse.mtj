/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.sign;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

/**
 * PermissionsGroupsRegistry class provides a registry for the
 * extensions of the org.eclipse.mtj.core.securitypermission
 * extesion point.
 * 
 * @author David Marques
 * @since 1.0
 */
public class PermissionsGroupsRegistry {

    // Constants -----------------------------------------------------

    private static final String NAME = "name"; //$NON-NLS-1$

	private static final String EXT_ID = "org.eclipse.mtj.core.securitypermission"; //$NON-NLS-1$
    
    // Attributes ----------------------------------------------------

    private List<PermissionsGroup> permissions;
    
    // Static --------------------------------------------------------

    private static PermissionsGroupsRegistry instance;
    
    /**
     * Gets the single instance of this class.
     * 
     * @return the singleton.
     */
    public static synchronized PermissionsGroupsRegistry getInstance() {
        if (PermissionsGroupsRegistry.instance == null) {
            PermissionsGroupsRegistry.instance = new PermissionsGroupsRegistry();
        }
        return PermissionsGroupsRegistry.instance;
    }
    
    // Constructors --------------------------------------------------

    /**
     * Loads all permissions found in the extension registry.
     */
    public PermissionsGroupsRegistry() {
        IExtensionRegistry      registry   = null;
        IConfigurationElement[] extensions = null;
        
        permissions = new ArrayList<PermissionsGroup>();
        registry    = Platform.getExtensionRegistry();
        extensions  = registry.getConfigurationElementsFor(EXT_ID);
        for (IConfigurationElement extension : extensions) {
            String className = extension.getAttribute(NAME);
            List<String> classPermissions = getClassPermissions(extension.getChildren());
            permissions.add(new PermissionsGroup(className, classPermissions));
        }
    }

    /**
     * Gets all class permissions.
     * 
     * @param _elements permission nodes.
     * @return the permissions list.
     */
    private List<String> getClassPermissions(IConfigurationElement[] _elements) {
        List<String> permissions = new ArrayList<String>();
        for (IConfigurationElement element : _elements) {
            String permission = element.getAttribute(NAME);
            permissions.add(permission);
        }
        return permissions;
    }

    // Public --------------------------------------------------------

    /**
     * Gets all permissions found in the registry.
     * 
     * @return list of permissions.
     */
    public List<PermissionsGroup> getPermissions() {
        List<PermissionsGroup> copy = new ArrayList<PermissionsGroup>();
        copy.addAll(this.permissions);
        return copy;
    }
    
    // X implementation ----------------------------------------------

    // Y overrides ---------------------------------------------------

    // Package protected ---------------------------------------------

    // Protected -----------------------------------------------------

    // Private -------------------------------------------------------
}
