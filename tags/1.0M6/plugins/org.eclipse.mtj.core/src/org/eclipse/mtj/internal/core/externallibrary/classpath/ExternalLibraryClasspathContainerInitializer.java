/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.externallibrary.classpath;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.ClasspathContainerInitializer;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.externallibrary.manager.ExternalLibraryManager;
import org.eclipse.mtj.internal.core.externallibrary.model.IExternalLibrary;

/**
 * Implementation of the MIDletLibrary classpath container initializer.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class ExternalLibraryClasspathContainerInitializer extends
        ClasspathContainerInitializer {

    String description = null;

    /**
     * Returns <code>false</code> because this container initializer can't be
     * requested to perform updates on its own container values.
     * 
     * @return false.
     * @see org.eclipse.jdt.core.ClasspathContainerInitializer#canUpdateClasspathContainer(org.eclipse.core.runtime.IPath,
     *      org.eclipse.jdt.core.IJavaProject)
     */
    @Override
    public boolean canUpdateClasspathContainer(IPath containerPath,
            IJavaProject project) {
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.ClasspathContainerInitializer#getComparisonID(org.eclipse.core.runtime.IPath, org.eclipse.jdt.core.IJavaProject)
     */
    @Override
    public Object getComparisonID(IPath containerPath, IJavaProject project) {
        if (containerPath == null) {
            return null;
        } else {
            return containerPath.toString();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.ClasspathContainerInitializer#getDescription(org.eclipse.core.runtime.IPath, org.eclipse.jdt.core.IJavaProject)
     */
    @Override
    public String getDescription(IPath containerPath, IJavaProject project) {
        if (description != null) {
            return description;
        }
        return super.getDescription(containerPath, project);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.ClasspathContainerInitializer#initialize(org.eclipse.core.runtime.IPath, org.eclipse.jdt.core.IJavaProject)
     */
    @Override
    public void initialize(IPath containerPath, IJavaProject project)
            throws CoreException {
        if (isMidletLibraryContainer(containerPath)) {

            String midletLibName = containerPath.segment(1);
            IExternalLibrary midletLibrary = ExternalLibraryManager.getInstance()
                    .getMidletLibrary(midletLibName);
            if (midletLibrary != null) {
                ExternalLibraryClasspathContainer container = new ExternalLibraryClasspathContainer(
                        midletLibrary);
                description = midletLibName + " ["
                        + midletLibrary.getVersion().toString() + "]";
                JavaCore.setClasspathContainer(containerPath,
                        new IJavaProject[] { project },
                        new IClasspathContainer[] { container }, null);
            } else {
                MTJCore.log(IStatus.ERROR, "Could not load "
                        + containerPath.toString(), null);
            }
        }
    }

    /**
     * Check if the path belongs to a MIDlet Library Container.
     * 
     * @param path a two-segment path (ID/hint) identifying the container that
     *            needs to be resolved
     * @return <code>true</code> if the container is a MIDlet Library Container.
     */
    private boolean isMidletLibraryContainer(IPath path) {
        return (path != null)
                && (path.segmentCount() == 2)
                && ExternalLibraryClasspathContainer.EXTERNAL_LIBRARY_CONTAINER_ID
                        .equals(path.segment(0));
    }
}
