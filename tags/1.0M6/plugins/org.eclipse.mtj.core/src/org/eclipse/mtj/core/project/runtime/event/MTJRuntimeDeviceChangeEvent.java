/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.project.runtime.event;

import java.util.EventObject;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.sdk.device.IDevice;

/**
 * MTJRuntimeDeviceChangeEvent is used to notify that the device of the
 * configuration changed.
 * 
 * @since 1.0
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class MTJRuntimeDeviceChangeEvent extends EventObject {

    private static final long serialVersionUID = 1L;
    private IDevice oldDevice;
    private IDevice newDevice;

    /**
     * Constructor.
     * 
     * @param source - the configuration has been changed.
     * @param oldDevice - the device before change.
     * @param newDevice - the device after change.
     */
    public MTJRuntimeDeviceChangeEvent(MTJRuntime source, IDevice oldDevice,
            IDevice newDevice) {
        super(source);
        this.oldDevice = oldDevice;
        this.newDevice = newDevice;
    }

    /**
     * @return
     */
    public IDevice getNewDevice() {
        return newDevice;
    }

    /**
     * @return
     */
    public IDevice getOldDevice() {
        return oldDevice;
    }

    /**
     * @param newDevice
     */
    public void setNewDevice(IDevice newDevice) {
        this.newDevice = newDevice;
    }

    /**
     * @param oldDevice
     */
    public void setOldDevice(IDevice oldDevice) {
        this.oldDevice = oldDevice;
    }

}
