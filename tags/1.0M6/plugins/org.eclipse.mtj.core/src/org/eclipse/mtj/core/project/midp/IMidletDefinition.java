/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.project.midp;

/**
 * The definition of a MIDlet within the application descriptor.
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IMidletDefinition {

    /**
     * @return Returns the className.
     */
    public abstract String getClassName();

    /**
     * @return Returns the icon.
     */
    public abstract String getIcon();

    /**
     * @return Returns the name.
     */
    public abstract String getName();

    /**
     * Return the MIDlet number.
     * 
     * @return
     */
    public abstract int getNumber();

    /**
     * @param className The className to set.
     */
    public abstract void setClassName(String className);

    /**
     * @param icon The icon to set.
     */
    public abstract void setIcon(String icon);

    /**
     * @param name The name to set.
     */
    public abstract void setName(String name);

    /**
     * Returns a string representation of the MIDlet definition in the following
     * pattern: <br>
     * <code>&lt;MIDlet name&gt;,&lt;icon&gt;,&lt;MIDlet class name&gt;</code>.
     * 
     * @return the textual representation of the MIDlet definition.
     */
    public abstract String toString();

}