/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 */
package org.eclipse.mtj.internal.core.packaging.midp;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.project.midp.IApplicationDescriptor;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.build.sign.SignatureUtils;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.project.midp.IJadSignature;
import org.eclipse.mtj.internal.core.util.ColonDelimitedProperties;

/**
 * A helper class that is used to create a JAD file representing the deployed
 * JAR file.
 * 
 * @author Craig Setera
 */
public class DeployedJADWriter {
    
    private IMidletSuiteProject midletSuite;
    private IFolder deploymentFolder;
    private File deployedJarFile;

    /**
     * Construct a new JAD Writer to handle the specified deployment.
     */
    public DeployedJADWriter(IMidletSuiteProject midletSuite,
            IFolder deploymentFolder, File deployedJar) {
        super();
        this.midletSuite = midletSuite;
        this.deploymentFolder = deploymentFolder;
        this.deployedJarFile = deployedJar;
    }

    /**
     * Write out a deployed JAD file for the previously created JAR file.
     * 
     * @param incrementalBuild
     * @param monitor
     * @throws IOException
     * @throws CoreException
     */
    public void writeDeployedJAD(boolean incrementalBuild,
            IProgressMonitor monitor) throws IOException, CoreException {
        // Read the source jad file and update the jar file
        // length property.
        IApplicationDescriptor appDescriptor = midletSuite
                .getApplicationDescriptor();
        ColonDelimitedProperties jadProperties = (ColonDelimitedProperties) appDescriptor
                .getManifestProperties();

        // Update the size of the jar file
        jadProperties.setProperty(IJADConstants.JAD_MIDLET_JAR_SIZE, (Long
                .valueOf(deployedJarFile.length())).toString());

        // If requested, we overwrite the JAD URL such that it is just the
        // name of the jar file. This is for runtime handling.
        if (incrementalBuild) {
            jadProperties.setProperty(IJADConstants.JAD_MIDLET_JAR_URL,
                    deployedJarFile.getName());
        }

        // write the JAD file in case signing blows up - at least we'll have
        // something non-bogus.
        writeJad(appDescriptor);

        if (!incrementalBuild) {
            // Retrieve the signature object for this project. This will be null
            // if we're not supposed to sign this project.
            IJadSignature signatureObject = SignatureUtils
                    .getSignatureObject(midletSuite);
            if (signatureObject != null) {
                signJad(signatureObject, deployedJarFile, jadProperties);
                writeJad(appDescriptor);
            }
        }
    }

    /**
     * Sign the JAR file associated with the project and add the signature
     * properties to the JAD file.
     * 
     * @param signatureObject IJadSignature object that will do the actual
     *            signing
     * @param deployedJarFile File referencing the JAR file to be signed
     * @param jadProperties Contents of the JAD file to be added to
     * @throws CoreException
     */
    private void signJad(IJadSignature signatureObject, File deployedJarFile,
            ColonDelimitedProperties jadProperties) throws CoreException {
        signatureObject.computeSignature(deployedJarFile);

        jadProperties.setProperty(IJADConstants.JAD_MIDLET_JAR_RSA_SHA1,
                signatureObject.getJarSignatureString());

        String[] certificates = signatureObject.getCertificateStrings();
        for (int i = 1; i <= certificates.length; i++) {
            jadProperties.setProperty(IJADConstants.JAD_MIDLET_CERTIFICATE + i,
                    certificates[i - 1]);
        }
    }

    /**
     * Write the application descriptor out to the deployed location.
     * 
     * @param appDescriptor
     * @throws IOException
     */
    private void writeJad(IApplicationDescriptor appDescriptor)
            throws IOException {
        IFile deployedJadFile = deploymentFolder.getFile(midletSuite
                .getJadFileName());
        appDescriptor.store(deployedJadFile.getLocation().toFile());
    }
}
