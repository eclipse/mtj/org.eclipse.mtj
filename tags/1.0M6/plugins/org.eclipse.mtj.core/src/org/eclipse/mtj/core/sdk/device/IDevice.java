/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.sdk.device;

import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.launching.LaunchEnvironment;
import org.eclipse.mtj.core.persistence.IPersistable;

/**
 * The device interface specifies the representation of an emulated device. Each
 * instance of a SDK must provide a list of emulated devices that implement
 * this interface.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @see org.eclipse.mtj.core.sdk.device.IDevice
 * @since 1.0 
 */
public interface IDevice extends IPersistable{

    /**
     * Return the deviceClasspath provided by this device. This deviceClasspath includes all
     * libraries for the device including configuration and profile libraries.
     * 
     * @return
     */
    public IDeviceClasspath getClasspath();

    /**
     * Return the displayable description of this device. This description will
     * be displayed within the user interface. If this method returns a
     * <code>null</code> value, the device's name will be used as the
     * description instead.
     * 
     * @return the description of this device or <code>null</code> if the
     *         device's name should be used instead.
     */
    public String getDescription();

    /**
     * Return the properties associated with this device. The available
     * properties will vary from emulator to emulator.
     * 
     * @return the properties associated with this device.
     */
    public Properties getDeviceProperties();

    /**
     * Return the name of the device group to which this device belongs. This
     * method must not return a <code>null</code> value.
     * 
     * @return
     */
    public String getSDKName();

    /**
     * Return the command-line arguments for launching this device given the
     * specified launch environment.
     * 
     * @param launchEnvironment
     * @param monitor
     * @return
     * @throws CoreException
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException;

    /**
     * Return the name of this device. This name will be used when interacting
     * with the emulator. This name may or may not be displayed within the user
     * interface, dependent on whether a display name is provided by this
     * device. This method must never return <code>null</code>.
     * 
     * @return the non-null name of this device.
     */
    public String getName();

    /**
     * Return a boolean describing whether this device wants to act as a debug
     * server rather than attaching to the debugger as a client.
     * 
     * @return if the device acts as a debug server
     */
    public boolean isDebugServer();

    /**
     * Set the name of this device. This name will be used when interacting with
     * the emulator. This name may or may not be displayed within the user
     * interface, dependent on whether a display name is provided by this
     * device. This method must never return <code>null</code>.
     * 
     * @param name the non-null name of this device.
     */
    public void setName(String name);
}
