/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.project.midp;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.osgi.framework.Version;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IApplicationDescriptor {

    /**
     * Add a new MidletDefinition instance.
     * 
     * @param midletDefinition the MIDlet definition to be added
     */
    public abstract void addMidletDefinition(IMidletDefinition midletDefinition);

    /**
     * Return the configuration specification version associated with this JAD
     * file.
     * 
     * @return
     * @throws CoreException
     */
    public abstract Version getConfigurationSpecificationVersion()
            throws CoreException;

    /**
     * Return the overall manifest properties.
     * 
     * @return the manifest properties.
     */
    public abstract Properties getManifestProperties();

    /**
     * Return the current count of MidletDefinition instances within this
     * application descriptor.
     * 
     * @return the number of MidletDefinition instances
     */
    public abstract int getMidletCount();

    /**
     * Return the list of MidletDefinition instances currently managed by the
     * ApplicationDescriptor.
     * 
     * @return a list of MidletDefinition instances
     */
    public abstract List<IMidletDefinition> getMidletDefinitions();

    /**
     * Return the JAD URL specified in the application descriptor or
     * <code>null</code> if it has not been specified. This method does not
     * check the validity of the value.
     * 
     * @return
     */
    public abstract String getMidletJarURL();

    /**
     * Store the ApplicationDescriptor instance into the same File from which it
     * was originally read.
     * 
     * @throws IOException when an error occurs while storing the descriptor
     */
    public abstract void store() throws IOException;

    /**
     * Store the ApplicationDescriptor instance into the specified file.
     * 
     * @param jadFile the file into which the descriptor will be written
     * @throws IOException when an error occurs while storing the descriptor
     */
    public abstract void store(File jadFile) throws IOException;

}