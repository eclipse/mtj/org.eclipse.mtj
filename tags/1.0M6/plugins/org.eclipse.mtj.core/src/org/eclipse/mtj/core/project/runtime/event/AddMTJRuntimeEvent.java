/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)          - Initial implementation
 *     Gustavo de Paula (Motorola) - Configuration refactory [Bug 265522]
 */
package org.eclipse.mtj.core.project.runtime.event;

import java.util.EventObject;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;

/**
 * AddMTJRuntimeEvent is used to notify that a {@link MTJRuntime} is added to
 * {@link MTJRuntimeList}.
 * 
 * @since 1.0
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class AddMTJRuntimeEvent extends EventObject {

    /**
     * Default serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * The {@link MTJRuntime} that was added to the MTJ Project.
     */
    private MTJRuntime addedRuntime;

    /**
     * Constructs a AddMTJRuntimeEvent object.
     * 
     * @param source - {@link MTJRuntimeList} instance of the MTJ MIDlet
     *            project.
     * @param addedRuntime - the added {@link MTJRuntime}.
     */
    public AddMTJRuntimeEvent(MTJRuntimeList source, MTJRuntime addedRuntime) {
        super(source);
        this.addedRuntime = addedRuntime;
    }

    /**
     * Get the {@link MTJRuntime} that was added to the MTJ Project.
     * 
     * @return the added {@link MTJRuntime}.
     */
    public MTJRuntime getAddedMTJRuntime() {
        return addedRuntime;
    }

    /**
     * Set the {@link MTJRuntime} to be added.
     * 
     * @param addedRuntime the {@link MTJRuntime} to be added.
     */
    public void setMTJRuntime(MTJRuntime addedRuntime) {
        this.addedRuntime = addedRuntime;
    }

}
