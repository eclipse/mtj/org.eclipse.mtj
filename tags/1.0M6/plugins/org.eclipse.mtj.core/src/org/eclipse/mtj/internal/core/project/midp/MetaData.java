/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)  - Initial implementation
 *     Kevin Hunter (Individual) - Added signing support
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 *     Diego Sandin (Motorola) &
 *     Hugo Raniere (Motorola)   - Remove Bouncy Castle dependencies
 *     Hugo Raniere (Motorola)   - Save user passwords in project's metadata file (w/o Bouncy Castle)
 *     Feng Wang (Sybase) - 1. Add multi-configs support, keeping compatible with
 *                             old version of meta data file.
 *                          2. Make save Meta data operation do not erase other elements
 *                             (elements other than "device", "signing", "configurations")
 *                             in meta data file. This is for backward compatibility.
 *     David Marques (Motorola) - Updating methods for loading/saving signature properties
 *                                to support signing enhancements.
 */
package org.eclipse.mtj.internal.core.project.midp;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.equinox.security.storage.ISecurePreferences;
import org.eclipse.equinox.security.storage.SecurePreferencesFactory;
import org.eclipse.equinox.security.storage.StorageException;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.midp.IMIDPMetaData;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.MTJCoreErrors;
import org.eclipse.mtj.internal.core.build.sign.Base64EncDec;
import org.eclipse.mtj.internal.core.build.sign.SignatureProperties;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.osgi.framework.Version;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * This class holds the metadata for the MIDlet suite project. This information
 * is persisted to a file called ".mtj" in the project's root directory.
 */
public class MetaData implements IMIDPMetaData, IMetaDataConstants {

    private MTJRuntimeList configurations;

    private IDevice device;

    /**
     * The name that will be used for the deployed MIDlet Suite jad file
     */
    private String jadFileName;

    private IProject project;

    private Element rootXmlElement;

    private SignatureProperties signatureProps;

    private Version version;

    /**
     * Construct a new metadata object for the MIDlet suite project.
     * 
     * @param suite
     */
    public MetaData(IMidletSuiteProject suite) {
        this(suite.getJavaProject().getProject());
    }

    /**
     * Construct a new metadata object for the MIDlet suite project.
     * 
     * @param suite
     */
    public MetaData(IProject project) {
        this.project = project;

        try {
            loadMetaData();
        } catch (CoreException e) {
            // Failure to load the metadata, log and initialize to defaults
            MTJCore.log(IStatus.ERROR, "loadMetaData() failed", e); //$NON-NLS-1$
            initializeToDefaults();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getConfigurations()
     */
    public MTJRuntimeList getRuntime() {
        return configurations;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getDevice()
     */
    public IDevice getDevice() {
        return device;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IMIDPMetaData#getJadFileName()
     */
    public String getJadFileName() {
        return jadFileName;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getSignatureProperties()
     */
    public ISignatureProperties getSignatureProperties() throws CoreException {
        return signatureProps;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getVersion()
     */
    public Version getVersion() {
        return version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#saveMetaData()
     */
    public void saveMetaData() throws CoreException {
        IFile storeFile = getStoreFile();
        if (storeFile == null) {
            MTJCore.log(IStatus.WARNING,
                    "saveMetaData failed due to null store file"); //$NON-NLS-1$
        } else {
            if (storeFile.exists() && storeFile.isReadOnly()) {
                // Attempt to clear the read-only flag via the IResource
                // interface. This should invoke the team provider and
                // do necessary checkouts.
                ResourceAttributes attributes = storeFile
                        .getResourceAttributes();
                attributes.setReadOnly(false);
                storeFile.setResourceAttributes(attributes);
            }

            saveMetaDataToFile(storeFile);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#saveMetaDataToFile(org.eclipse.core.resources.IFile)
     */
    public void saveMetaDataToFile(IFile storeFile) throws CoreException {
        try {
            String pluginVersion = MTJCore.getPluginVersion();
            Version newVersion = new Version(pluginVersion);
            if (rootXmlElement == null) {
                rootXmlElement = XMLUtils.createRootElement(ELEM_ROOT_NAME,
                        newVersion);
            } else {
                rootXmlElement.setAttribute(XMLUtils.ATTR_VERSION, newVersion
                        .toString());
            }

            if (jadFileName != null) {
                rootXmlElement.setAttribute(ATTR_JAD_FILE, jadFileName);
            }
            // We keep top level <device> element in meta data file, just for
            // compatible with old version MTJ
            saveDevice(rootXmlElement, device);

            saveSignatureProps(rootXmlElement);
            saveConfigurations(rootXmlElement);
            File localFile = storeFile.getLocation().toFile();
            XMLUtils
                    .writeDocument(localFile, rootXmlElement.getOwnerDocument());
            version = newVersion;
            getStoreIFile().refreshLocal(1, new NullProgressMonitor());
        } catch (ParserConfigurationException pce) {
            MTJCore.throwCoreException(IStatus.WARNING, 99999, pce);
        } catch (TransformerException te) {
            MTJCore.throwCoreException(IStatus.WARNING, 99999, te);
        } catch (IOException ioe) {
            MTJCore.throwCoreException(IStatus.WARNING, 99999, ioe);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#setConfigurations(org.eclipse.mtj.internal.core.project.configuration.Configurations)
     */
    public void setMTJRuntimeList(MTJRuntimeList configurations) {
        this.configurations = configurations;
    }

// /* (non-Javadoc)
// * @see
    // org.eclipse.mtj.core.project.IMetaData#setDevice(org.eclipse.mtj.core.sdk.device.IDevice)
// */
// public void setDevice(IDevice device) {
// this.device = device;
// setDeviceIntoActiveConfig(device);
// }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IMIDPMetaData#setJadFileName(java.lang.String)
     */
    public void setJadFileName(String jadFileName) {
        this.jadFileName = jadFileName.replace(' ', '_');
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#setSignatureProperties(org.eclipse.mtj.core.build.sign.ISignatureProperties)
     */
    public void setSignatureProperties(ISignatureProperties p) {
        if (signatureProps == null) {
            signatureProps = new SignatureProperties();
        }
        signatureProps.copy(p);
    }

    /**
     * @param opmode
     * @return
     * @throws GeneralSecurityException
     */
    private Cipher createCipher(int opmode) throws GeneralSecurityException {
        PBEKeySpec keySpec = new PBEKeySpec(CRYPTO_PASS.toCharArray());
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(IMetaDataConstants.CRYPTO_ALGORITHM); //$NON-NLS-1$
        SecretKey secretKey = keyFactory.generateSecret(keySpec);

        AlgorithmParameterSpec paramSpec = new PBEParameterSpec(CRYPTO_SALT, CRYPTO_ITERATION_COUNT);
        Cipher cipher = Cipher.getInstance(secretKey.getAlgorithm()); //$NON-NLS-1$
        cipher.init(opmode, secretKey, paramSpec);
        return cipher;
    }

    /**
     * @param base
     * @param subElementName
     * @return
     */
    private String decodePassword(Element base, String subElementName) {
        Element subElement = XMLUtils.getFirstElementWithTagName(base,
                subElementName);
        if (subElement == null) {
            return (null);
        }

        String encoded = XMLUtils.getElementText(subElement);
        byte[] cryptBytes = Base64EncDec.decode(encoded);
        if (cryptBytes == null) {
            return (null);
        }

        try {
            byte[] passwordBytes = null;
            Cipher cipher = createCipher(Cipher.DECRYPT_MODE);
            passwordBytes = cipher.doFinal(cryptBytes);

            return (new String(passwordBytes, "UTF8")); //$NON-NLS-1$
        } catch (UnsupportedEncodingException e) {
        } catch (GeneralSecurityException e) {
        }

        return (null);
    }

    /**
     * @param password
     * @return
     */
    private String encodePassword(String password) {
        if (password == null) {
            return (""); //$NON-NLS-1$
        }

        try {
            byte[] passwordBytes = password.getBytes("UTF8"); //$NON-NLS-1$
            byte[] cryptBytes = null;
            Cipher cipher = createCipher(Cipher.ENCRYPT_MODE);
            cryptBytes = cipher.doFinal(passwordBytes);

            return (Base64EncDec.encode(cryptBytes));
        } catch (UnsupportedEncodingException e) {
        } catch (GeneralSecurityException e) {
        }
        return (""); //$NON-NLS-1$
    }

    /**
     * Return the IFile instance in which the metadata is to be stored or
     * <code>null</code> if the file is not yet available.
     * 
     * @return
     */
    private IFile getStoreFile() {
        IFile storeFile = null;

        if (project != null) {
            storeFile = project.getFile(METADATA_FILE);
        }

        return storeFile;
    }

    /**
     * Get the IFile instance in which the metadata is to be stored.
     * 
     * @return
     */
    private IFile getStoreIFile() {
        return project.getFile(METADATA_FILE);
    }

    /**
     * Initialize the metadata to default values
     */
    private void initializeToDefaults() {
        signatureProps = new SignatureProperties();
        configurations = new MTJRuntimeList();
    }

    /**
     * Besides loading configurations from meta data file, this method also
     * handle compatible issue. It's means that it can load configurations from
     * old version of meta data file, by creating a configuration from the
     * device information.
     * 
     * @param rootElement
     * @throws PersistenceException
     */
    private void loadConfigsCompatibly(Element rootElement)
            throws PersistenceException {
        // load device if there any. New version meta data file (0.9.1) have no
        // top level <device> element
        loadDevice(rootElement);
        Element configsElement = XMLUtils.getFirstElementWithTagName(
                rootElement, ELEM_CONFIGURATIONS);
        if (configsElement != null) {
            configurations = new MTJRuntimeList(configsElement);
        } else {
            // For old version meta data file(before MTJ 0.9.1), there is no
            // <configurations> element, so we create one. After
            // Metadata#savemetada() performed, this configurations will
            // be persisted in meta data file.
            configurations = new MTJRuntimeList();
        }
        // (configurations.isEmpty() && device != null) means that the meta data
        // file is of old version. So we will create a configuration from the
        // device.
        if (configurations.isEmpty() && (device != null)) {
            setDeviceIntoActiveConfig(device);
        }
    }

    /**
     * Load Configuration from meta data file.
     * 
     * @param rootElement
     * @throws PersistenceException
     */
    private void loadConfigurations(Element rootElement)
            throws PersistenceException {
        // can load form both old version and new version meta data file.
        loadConfigsCompatibly(rootElement);
        // set device
        MTJRuntime activeConfig = configurations.getActiveMTJRuntime();
        if (activeConfig != null) {
            device = configurations.getActiveMTJRuntime().getDevice();
        }
    }

    /**
     * @param rootElement
     * @throws PersistenceException
     */
    private void loadDevice(Element rootElement) throws PersistenceException {
        Element deviceElement = XMLUtils.getFirstElementWithTagName(
                rootElement, ELEM_DEVICE);
        if (deviceElement != null) {
            String deviceGroup = deviceElement.getAttribute(ATTR_DEVICEGROUP);
            String deviceName = deviceElement.getAttribute(ATTR_DEVICENAME);
            device = MTJCore.getDeviceRegistry().getDevice(deviceGroup,
                    deviceName);
        }
    }

    /**
     * @throws CoreException
     */
    private void loadMetaData() throws CoreException {
        boolean bRewrite = false;

        loadMetaDataFromFile();

        if (signatureProps == null) {
            signatureProps = new SignatureProperties();
            signatureProps.clear();
            bRewrite = true;
        }

        if (bRewrite) {
            // NOTE: Commented out for now. The point at which this is likely to
            // happen (during startup), the workspace is locked against updates
            // and the save will fail. (Actually the workspace refresh will
            // fail)
            // saveMetaData();
        }
    }

    /**
     * @throws CoreException
     */
    private void loadMetaDataFromFile() throws CoreException {
        IFile storeFile = getStoreFile();
        if ((storeFile != null) && (storeFile.exists())) {
            try {
                File localFile = storeFile.getLocation().toFile();
                Document document = XMLUtils.readDocument(localFile);
                if (document == null) {
                    return;
                }

                rootXmlElement = document.getDocumentElement();
                if (!rootXmlElement.getNodeName().equals(ELEM_ROOT_NAME)) {
                    return;
                }

                version = XMLUtils.getVersion(document);

                /*
                 * Get the name that will be used for the deployed MIDlet Suite
                 * jad file
                 */
                jadFileName = rootXmlElement.getAttribute(ATTR_JAD_FILE);
                loadDevice(rootXmlElement);
                loadConfigurations(rootXmlElement);
                loadSignatureProperties(rootXmlElement);

            } catch (ParserConfigurationException pce) {
                MTJCore.throwCoreException(IStatus.WARNING, 99999, pce);
            } catch (SAXException se) {
                MTJCore.throwCoreException(IStatus.WARNING, 99999, se);
            } catch (IOException ioe) {
                MTJCore.throwCoreException(IStatus.WARNING, 99999, ioe);
            } catch (PersistenceException e) {
                MTJCore.throwCoreException(IStatus.WARNING, 99999, e);
            }
        } else {
            initializeToDefaults();
        }
    }

    /**
     * @param rootElement
     * @throws CoreException
     */
    private void loadSignatureProperties(Element rootElement)
            throws CoreException {
        SignatureProperties sp = new SignatureProperties();

        Element signRoot = XMLUtils.getFirstElementWithTagName(rootElement,
                ELEM_SIGNING);
        if (signRoot == null) {
            return;
        }

        String attr = signRoot.getAttribute(ATTR_PROJECT_SPECIFIC);
        if (attr == null) {
        	return;
        }
        sp.setProjectSpecific(Boolean.valueOf(attr).booleanValue());
        
        attr = signRoot.getAttribute(ATTR_SIGN_PROJECT);
        if (attr == null) {
            return;
        }
        sp.setSignProject(Boolean.valueOf(attr).booleanValue());
        signatureProps = sp;
        
        Element e;

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_KEYSTORE);
        if (e != null) {
        	sp.setKeyStoreDisplayPath(XMLUtils.getElementText(e));
        }

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_ALIAS);
        if (e != null) {
        	sp.setKeyAlias(XMLUtils.getElementText(e));
        }

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_PROVIDER);
        if (e != null) {
            sp.setKeyStoreProvider(XMLUtils.getElementText(e));
        }

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_KEYSTORETYPE);
        if (e != null) {
            sp.setKeyStoreType(XMLUtils.getElementText(e));
        }

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_PASSWORDS);
        if (e != null) {
            attr = e.getAttribute(ATTR_STOREPASSWORDS);
            int nMethod = ISignatureProperties.PASSMETHOD_PROMPT;
            if (attr != null) {
                try {
                    nMethod = Integer.valueOf(attr).intValue();
                } catch (Exception ex) {
                }
            }

            switch (nMethod) {
                case ISignatureProperties.PASSMETHOD_IN_KEYRING:
                    sp.setPasswordStorageMethod(nMethod);

                    ISecurePreferences root = SecurePreferencesFactory
                            .getDefault();

                    String url = Utils.getKeyringURL(project);
                    if (root.nodeExists(url)) {
                        ISecurePreferences preferences = root.node(url);
                        try {
                            sp.setKeyStorePassword(preferences.get(
                                    KEYRING_KEYSTOREPASS_KEY, ""));
                            sp.setKeyPassword(preferences.get(
                                    KEYRING_KEYPASS_KEY, ""));
                        } catch (StorageException e1) {
                            signatureProps = sp;
                            MTJCoreErrors.throwCoreExceptionError(7676,
                                    "Could not re password into keyring,", e1);
                        }

                    }

                    break;
                case ISignatureProperties.PASSMETHOD_IN_PROJECT:
                    sp.setPasswordStorageMethod(nMethod);
                    sp
                            .setKeyStorePassword(decodePassword(e,
                                    ELEM_PWD_KEYSTORE));
                    sp.setKeyPassword(decodePassword(e, ELEM_PWD_KEY));
                    break;
                case ISignatureProperties.PASSMETHOD_PROMPT:
                default:
                    sp
                            .setPasswordStorageMethod(ISignatureProperties.PASSMETHOD_PROMPT);
                    sp.setKeyStorePassword(null);
                    sp.setKeyPassword(null);
                    break;
            }
        }
    }

    /**
     * @param parent
     * @param name
     */
    private void removeChildXmlElement(Element parent, String name) {
        Element child = XMLUtils.getFirstElementWithTagName(parent, name);
        if (child != null) {
            child.getParentNode().removeChild(child);
        }
    }

    /**
     * @param rootElement
     */
    private void saveConfigurations(Element rootElement) {
        if (configurations == null) {
            return;
        }
        // remove old one
        removeChildXmlElement(rootElement, ELEM_CONFIGURATIONS);
        // create new one
        Element configsElement = XMLUtils.createChild(rootElement,
                ELEM_CONFIGURATIONS);
        for (MTJRuntime config : configurations) {
            Element configElement = XMLUtils.createChild(configsElement,
                    MTJRuntimeList.ELEM_CONFIGURATION);
            configElement.setAttribute(MTJRuntime.ATTR_NAME, config.getName());
            configElement.setAttribute(MTJRuntime.ATTR_ACTIVE, String
                    .valueOf(config.isActive()));
            saveDevice(configElement, config.getDevice());
            saveSymbolSet(configElement, config.getRuntimeSymbolSet());
            saveWorkspaceSymbolSets(configElement, config
                    .getWorkspaceScopeSymbolSets());
        }
    }

    /**
     * @param rootElement
     * @param device
     */
    private void saveDevice(Element rootElement, IDevice device) {
        if (device == null) {
            return;
        }
        // remove old one
        removeChildXmlElement(rootElement, ELEM_DEVICE);
        // create new one
        Element newDeviceElement = XMLUtils.createChild(rootElement,
                ELEM_DEVICE);
        newDeviceElement.setAttribute(ATTR_DEVICEGROUP, device.getSDKName());
        newDeviceElement.setAttribute(ATTR_DEVICENAME, device.getName());
    }

    /**
     * @param rootElement
     * @throws CoreException
     */
    private void saveSignatureProps(Element rootElement) throws CoreException {
        // remove old one
        removeChildXmlElement(rootElement, ELEM_SIGNING);
        // create new one
        Element newSignRoot = XMLUtils.createChild(rootElement, ELEM_SIGNING);
        boolean bSign = signatureProps.getSignProject();
        newSignRoot.setAttribute(ATTR_SIGN_PROJECT, Boolean.toString(bSign));
        boolean bSpec = signatureProps.isProjectSpecific();
        newSignRoot.setAttribute(ATTR_PROJECT_SPECIFIC, Boolean.toString(bSpec));
        
    	// The alias is necessary either if the project has specific
		// signing settings or not.
		XMLUtils.createTextElement(newSignRoot, ELEM_ALIAS, signatureProps
				.getKeyAlias());
		if (!bSpec) {
			return;
		}
		XMLUtils.createTextElement(newSignRoot, ELEM_KEYSTORE,
				signatureProps.getKeyStoreDisplayPath());
		XMLUtils.createTextElement(newSignRoot, ELEM_PROVIDER,
				signatureProps.getKeyStoreProvider());
		XMLUtils.createTextElement(newSignRoot, ELEM_KEYSTORETYPE,
				signatureProps.getKeyStoreType());

		Element passRoot = XMLUtils
				.createChild(newSignRoot, ELEM_PASSWORDS);
		passRoot.setAttribute(ATTR_STOREPASSWORDS, Integer
				.toString(signatureProps.getPasswordStorageMethod()));

		ISecurePreferences root = SecurePreferencesFactory.getDefault();
		String url = Utils.getKeyringURL(project);
		if (root.nodeExists(url)) {
			ISecurePreferences preferences = root.node(url);
			preferences.clear();
		}

		switch (signatureProps.getPasswordStorageMethod()) {
		case ISignatureProperties.PASSMETHOD_IN_PROJECT:
			XMLUtils.createTextElement(passRoot, ELEM_PWD_KEYSTORE,
					encodePassword(signatureProps.getKeyStorePassword()));
			XMLUtils.createTextElement(passRoot, ELEM_PWD_KEY,
					encodePassword(signatureProps.getKeyPassword()));
			break;
		case ISignatureProperties.PASSMETHOD_IN_KEYRING:

			ISecurePreferences node = root.node(url);
			try {
				node.put(KEYRING_KEYSTOREPASS_KEY, signatureProps
						.getKeyStorePassword(), false);

				node.put(KEYRING_KEYPASS_KEY, signatureProps
						.getKeyPassword(), false);
			} catch (StorageException e) {
				MTJCoreErrors.throwCoreExceptionError(7676,
						"Could not save password into keyring,", e);
			}

			break;
		case ISignatureProperties.PASSMETHOD_PROMPT:
		default:
			break;
		}
    }

    /**
     * @param symbolSetElement
     * @param symbol
     */
    private void saveSymbol(Element symbolSetElement, ISymbol symbol) {
        Element symbolElement = XMLUtils.createChild(symbolSetElement,
                MTJRuntime.ELEM_SYMBOL);
        symbolElement.setAttribute(MTJRuntime.ATTR_NAME, symbol.getName());
        symbolElement.setAttribute(MTJRuntime.ATTR_VALUE, symbol.getValue());

    }

    /**
     * @param configElement
     * @param symbolSet
     */
    private void saveSymbolSet(Element configElement, ISymbolSet symbolSet) {
        if (symbolSet == null) {
            return;
        }
        Element symbolSetElement = XMLUtils.createChild(configElement,
                MTJRuntime.ELEM_SYMBOL_SET);
        for (ISymbol symbol : symbolSet.getSymbols()) {
            saveSymbol(symbolSetElement, symbol);
        }
    }

    /**
     * @param configElement
     * @param workspaceScopeSymbolSets
     */
    private void saveWorkspaceSymbolSets(Element configElement,
            List<ISymbolSet> workspaceScopeSymbolSets) {
        if ((workspaceScopeSymbolSets == null)
                || (workspaceScopeSymbolSets.isEmpty())) {
            return;
        }
        for (ISymbolSet s : workspaceScopeSymbolSets) {
            Element symbolSetElement = XMLUtils.createChild(configElement,
                    MTJRuntime.ELEM_WORKSPACE_SYMBOLSET);
            symbolSetElement.setAttribute(MTJRuntime.ATTR_NAME, s.getName());
        }

    }

    /**
     * Set the device into active configuration, if project has no
     * configurations, we create one from the device.
     * 
     * @param device
     */
    private void setDeviceIntoActiveConfig(IDevice device) {
        MTJRuntime activeConfig = configurations.getActiveMTJRuntime();
        if (activeConfig != null) {
            activeConfig.setDevice(device);
            // if activeConfig is not null, must return here, make sure not add
            // the acriveConfig again.
            return;
        }
        if ((activeConfig == null) && !configurations.isEmpty()) {
            activeConfig = configurations.get(0);
        }
        // if configurations is empty, we create a configuration according the
        // device, and add it into the configurations
        if (activeConfig == null) {
            activeConfig = new MTJRuntime(device.getName());
            activeConfig.setSymbolSet(MTJCore.getSymbolSetFactory()
                    .createSymbolSetFromDevice(device));
        }
        activeConfig.setActive(true);
        activeConfig.setDevice(device);
        configurations.add(activeConfig);
    }
}
