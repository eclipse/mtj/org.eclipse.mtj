/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     David Marques (Motorola) - Renaming EclipseME to MTJ                               
 */
package org.eclipse.mtj.core.project.midp;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.project.MTJNature;
import org.eclipse.mtj.internal.core.build.BuildSpecManipulator;

/**
 * Project nature for MTJ projects.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 * @see IProjectNature
 */
public class J2MENature extends MTJNature {

    /**
     * Return a boolean indicating whether the specified project has the MTJ
     * nature associated with it.
     * 
     * @param project the project to be tested for the MTJ nature
     * @return a boolean indicating whether the specified project has the MTJ
     *         nature
     * @throws CoreException if this method fails.
     */
    public static boolean hasMtjCoreNature(IProject project)
            throws CoreException {
        return project.hasNature(IMTJCoreConstants.MTJ_NATURE_ID);
    }

    /**
     * @see IProjectNature#configure
     */
    public void configure() throws CoreException {
        BuildSpecManipulator manipulator = new BuildSpecManipulator(
                getProject());
        manipulator.addBuilderAfter(JavaCore.BUILDER_ID,
                IMTJCoreConstants.J2ME_PREVERIFIER_ID, null);
        manipulator.commitChanges(new NullProgressMonitor());
    }
}
