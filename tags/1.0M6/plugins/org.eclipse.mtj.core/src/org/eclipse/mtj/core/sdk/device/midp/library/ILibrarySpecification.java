/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Extracted as interface
 */
package org.eclipse.mtj.core.sdk.device.midp.library;

import org.osgi.framework.Version;

/**
 * Tracks a specification of a particular library. Includes the name of the
 * specification, an identifier and the version of that specification.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface ILibrarySpecification {

    /**
     * @return Returns the name.
     */
    public abstract String getName();

    /**
     * @return Returns the version.
     */
    public abstract Version getVersion();

    /**
     * @return Returns the identifier.
     */
    public abstract String getIdentifier();

}