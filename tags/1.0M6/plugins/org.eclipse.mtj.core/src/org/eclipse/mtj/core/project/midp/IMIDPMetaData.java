/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.project.midp;

import org.eclipse.mtj.core.project.IMetaData;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IMIDPMetaData extends IMetaData {

    /**
     * Set the name that must be used on the project's jad file after deployment
     * 
     * @param jadFileName the jad file name
     */
    public abstract void setJadFileName(String jadFileName);

    /**
     * Return the name that must be used on the project's jad file after
     * deployment as specified in the project's metadata file.
     * 
     * @return
     */
    public abstract String getJadFileName();
    
    
}
