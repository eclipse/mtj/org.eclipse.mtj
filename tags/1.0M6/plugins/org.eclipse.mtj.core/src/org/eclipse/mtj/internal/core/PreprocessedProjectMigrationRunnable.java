/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code                           
 */
package org.eclipse.mtj.internal.core;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.BuildSpecManipulator;

/**
 * A workspace runnable that walks through the workspace projects and migrates
 * the preprocessor natures and builders such that the preprocessor works within
 * the project rather than requiring the secondary project.
 * 
 * @author Craig Setera
 */
public class PreprocessedProjectMigrationRunnable implements IWorkspaceRunnable {

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.core.resources.IWorkspaceRunnable#run(org.eclipse.core.runtime.IProgressMonitor)
     */
    public void run(IProgressMonitor monitor) throws CoreException {
        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        IProject[] projects = root.getProjects();
        for (IProject project : projects) {
            if (project.isOpen()) {
                migrateProject(project, monitor);
            }
        }
    }

    /**
     * The specified project is doing preprocessing. Update the builders and
     * natures to match the new implementation.
     * 
     * @param project
     * @param monitor
     * @throws CoreException
     */
    private void migratePreprocessingProject(IProject project,
            IProgressMonitor monitor) throws CoreException {

        BuildSpecManipulator manipulator = new BuildSpecManipulator(project);
        if (!manipulator.hasBuilder(JavaCore.BUILDER_ID)) {
            manipulator.addBuilderBefore(IMTJCoreConstants.J2ME_PREVERIFIER_ID,
                    JavaCore.BUILDER_ID, null);
        }

        if (!manipulator.hasBuilder(IMTJCoreConstants.J2ME_PREPROCESSOR_ID)) {
            manipulator.addBuilderBefore(JavaCore.BUILDER_ID,
                    IMTJCoreConstants.J2ME_PREPROCESSOR_ID, null);
        }

        if (manipulator.hasArguments(IMTJCoreConstants.J2ME_PREVERIFIER_ID)) {
            manipulator.replaceBuilder(IMTJCoreConstants.J2ME_PREVERIFIER_ID,
                    null);
        }

        manipulator.commitChanges(monitor);
    }

    /**
     * The specified project is open, so go ahead and attempt to migrate that
     * project.
     * 
     * @param project
     * @param monitor
     * @throws CoreException
     */
    private void migrateProject(IProject project, IProgressMonitor monitor)
            throws CoreException {
        if (project.hasNature(IMTJCoreConstants.J2ME_PREPROCESSING_NATURE_ID)) {
            migratePreprocessingProject(project, monitor);
        } else if (project
                .hasNature(IMTJCoreConstants.J2ME_PREPROCESSED_NATURE_ID)) {
            // We no longer require this secondary project for the new
            // implementation so go ahead and delete it
            project.delete(true, monitor);
        }
    }
}
