/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 */
package org.eclipse.mtj.core.sdk.device;

import java.io.File;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

/**
 * Instances of IDeviceImporter are presented with a set of directories for
 * which the instance may return one or more device instances. IDeviceImporters
 * are provided to the system via the <code>deviceImporter</code> extension
 * point.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * 
 * @see IDevice
 */
public interface IDeviceImporter {

    /**
     * The extension point for use in registering new device importers.
     */
    public static final String EXT_DEVICE_IMPORTERS = "deviceimporter";

    /**
     * Return the fully configured device instances found in the specified
     * directory or <code>null</code> if no devices are recognized by this
     * importer in this directory.
     * 
     * @param directory
     * @param monitor
     * @return
     * @throws CoreException
     * @throws InterruptedException if the operation detects a request to
     *             cancel, using {@link IProgressMonitor#isCanceled()}, it should exit
     *             by throwing {@link InterruptedException}.
     */
    public List<IDevice> importDevices(File directory, IProgressMonitor monitor)
            throws CoreException, InterruptedException;
}
