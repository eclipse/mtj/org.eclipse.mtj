/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Storing default preverifier
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceRegistry;
import org.eclipse.mtj.core.sdk.device.IDeviceRegistryListener;
import org.eclipse.mtj.internal.core.persistence.XMLPersistenceProvider;
import org.eclipse.mtj.internal.core.util.migration.DeviceRegistryMigration;
import org.eclipse.mtj.internal.core.util.migration.IMigration;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * The device registry provides a place to store and retrieve devices by name.
 * <p>
 * This registry is implemented as a multi-level registry in which the top-level
 * device groups reference lower-level devices.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @author Craig Setera
 */
public class DeviceRegistry implements IDeviceRegistry {

    private static IDeviceRegistry instance = null;

    /**
     * @return the instance
     */
    public static IDeviceRegistry getInstance() {
        if (instance == null) {
            instance = new DeviceRegistry();
        }

        return instance;
    }

    // The default device
    private IDevice defaultDevice;

    // The default preverifier
    private IPreverifier defaultPreverifier;

    // Map of device group names that maps to a lower-level
    // map of device instances by name
    private Map<String, Map<String, IDevice>> deviceGroupsMap;

    private boolean fireDeviceAddedEvent = false;

    // A list of registry listeners
    private ArrayList<IDeviceRegistryListener> listenerList;

    private DeviceRegistry() {
        super();
        listenerList = new ArrayList<IDeviceRegistryListener>();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#addDevice(org.eclipse.mtj.core.sdk.device.IDevice, boolean)
     */
    public void addDevice(IDevice device) throws IllegalArgumentException,
            PersistenceException {
        if ((device.getSDKName() == null) || (device.getName() == null)) {
            throw new IllegalArgumentException();
        }

        Map<String, IDevice> groupMap = getDeviceGroupMap(device.getSDKName(),
                true);
        groupMap.put(device.getName(), device);

        if (fireDeviceAddedEvent) {
            fireDeviceAddedEvent(device);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#addRegistryListener(org.eclipse.mtj.core.sdk.device.IDeviceRegistryListener)
     */
    public void addRegistryListener(IDeviceRegistryListener listener) {
        listenerList.add(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#clear()
     */
    public void clear() throws PersistenceException {
        getDeviceGroupsMap().clear();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#enableDeviceAddedEvent(boolean)
     */
    public void enableDeviceAddedEvent(boolean fire) {
        fireDeviceAddedEvent = fire;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getAllDevices()
     */
    public List<IDevice> getAllDevices() throws PersistenceException {
        ArrayList<IDevice> deviceList = new ArrayList<IDevice>();

        Iterator<Map<String, IDevice>> groups = getDeviceGroupsMap().values()
                .iterator();
        while (groups.hasNext()) {
            Map<String, IDevice> deviceMap = groups.next();
            deviceList.addAll(deviceMap.values());
        }

        return deviceList;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getDefaultDevice()
     */
    public IDevice getDefaultDevice() {
        return defaultDevice;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getDefaultPreferifier()
     */
    public IPreverifier getDefaultPreferifier() {
        return defaultPreverifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getDevice(java.lang.String, java.lang.String)
     */
    public IDevice getDevice(String groupName, String deviceName)
            throws PersistenceException {
        IDevice device = null;

        Map<String, IDevice> groupMap = getDeviceGroupMap(groupName, false);
        if (groupMap != null) {
            device = groupMap.get(deviceName);
        }

        return device;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getDeviceCount()
     */
    public int getDeviceCount() throws PersistenceException {
        int deviceCount = 0;

        Iterator<Map<String, IDevice>> groupMaps = getDeviceGroupsMap()
                .values().iterator();
        while (groupMaps.hasNext()) {
            Map<String, IDevice> groupMap = groupMaps.next();
            deviceCount += groupMap.size();
        }

        return deviceCount;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getDeviceGroups()
     */
    public List<String> getDeviceGroups() throws PersistenceException {
        return new ArrayList<String>(getDeviceGroupsMap().keySet());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getDevices(java.lang.String)
     */
    public List<IDevice> getDevices(String groupName)
            throws PersistenceException {
        List<IDevice> devices = null;

        Map<String, IDevice> devicesGroupMap = getDeviceGroupMap(groupName,
                false);
        if (devicesGroupMap != null) {
            devices = new ArrayList<IDevice>(devicesGroupMap.values());
        }

        return devices;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#isDeviceAddedEventEnabled()
     */
    public boolean isDeviceAddedEventEnabled() {
        return fireDeviceAddedEvent;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#load()
     */
    public void load() throws PersistenceException {
        File storeFile = getComponentStoreFile();
        if (storeFile.exists()) {
            try {
                Document document = XMLUtils.readDocument(storeFile);

                if (document == null) {
                    return;
                }

                Element rootXmlElement = document.getDocumentElement();
                if (!rootXmlElement.getNodeName().equals(
                        IDeviceRegistryConstants.ELEMENT_DEVICEREGISTRY)) {
                    return;
                }

                IMigration migration = new DeviceRegistryMigration();

                migration.migrate(document);

                if (migration.isMigrated()) {

                    try {
                        XMLUtils.writeDocument(storeFile, document);
                    } catch (TransformerException e) {
                        e.printStackTrace();
                    }

                    document = XMLUtils.readDocument(storeFile);
                }

                XMLPersistenceProvider pprovider = new XMLPersistenceProvider(
                        document);

                loadUsing(pprovider);
            } catch (ParserConfigurationException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (SAXException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (IOException e) {
                throw new PersistenceException(e.getMessage(), e);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        
        enableDeviceAddedEvent(false);
        getDeviceGroupsMap().clear();
        
        try {
            

            int deviceCount = persistenceProvider.loadInteger("deviceCount"); //$NON-NLS-1$

            for (int i = 0; i < deviceCount; i++) {
                IDevice device = (IDevice) persistenceProvider
                        .loadPersistable("device" + i); //$NON-NLS-1$
                addDevice(device);
            }

            defaultDevice = (IDevice) persistenceProvider
                    .loadReference("defaultDevice"); //$NON-NLS-1$

            defaultPreverifier = (IPreverifier) persistenceProvider
                    .loadPersistable("defaultPreverifier"); //$NON-NLS-1$
        } finally {
            enableDeviceAddedEvent(true);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#removeDevice(org.eclipse.mtj.core.sdk.device.IDevice)
     */
    public void removeDevice(IDevice device) throws PersistenceException {
        Map<String, IDevice> groupMap = getDeviceGroupMap(device.getSDKName(),
                false);
        if (groupMap != null) {
            groupMap.remove(device.getName());

            // Remove empty groups
            if (groupMap.isEmpty()) {
                deviceGroupsMap.remove(device.getSDKName());
            }

            // Clear the default device if it is being removed
            if (device.equals(defaultDevice)) {
                defaultDevice = null;
            }
        }

        fireDeviceRemovedEvent(device);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#removeRegistryListener(org.eclipse.mtj.core.sdk.device.IDeviceRegistryListener)
     */
    public void removeRegistryListener(IDeviceRegistryListener listener) {
        listenerList.remove(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#setDefaultDevice(org.eclipse.mtj.core.sdk.device.IDevice)
     */
    public void setDefaultDevice(IDevice device) {
        defaultDevice = device;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#setDefaultPreverifer(org.eclipse.mtj.core.build.preverifier.IPreverifier)
     */
    public void setDefaultPreverifer(IPreverifier preverifier) {
        defaultPreverifier = preverifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#store()
     */
    public void store() throws PersistenceException, TransformerException,
            IOException {
        XMLPersistenceProvider pprovider = new XMLPersistenceProvider(
                "deviceRegistry"); //$NON-NLS-1$
        storeUsing(pprovider);
        XMLUtils
                .writeDocument(getComponentStoreFile(), pprovider.getDocument());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        List<?> allDevices = getAllDevices();
        persistenceProvider.storeInteger("deviceCount", allDevices.size()); //$NON-NLS-1$

        int index = 0;
        Iterator<?> iterator = allDevices.iterator();
        while (iterator.hasNext()) {
            IDevice device = (IDevice) iterator.next();
            persistenceProvider.storePersistable("device" + index++, device); //$NON-NLS-1$
        }

        if (defaultDevice != null) {
            persistenceProvider.storeReference("defaultDevice", defaultDevice); //$NON-NLS-1$
        }

        if (defaultPreverifier != null) {
            persistenceProvider.storePersistable("defaultPreverifier", //$NON-NLS-1$
                    defaultPreverifier);
        }
    }

    /**
     * Do the initial device group maps creation and loading.
     * 
     * @throws PersistenceException
     */
    private void createAndLoadDeviceGroups() throws PersistenceException {
        deviceGroupsMap = new HashMap<String, Map<String, IDevice>>();
        load();
    }

    /**
     * Fire an event that indicates a device has been added.
     * 
     * @param device
     */
    private void fireDeviceAddedEvent(IDevice device) {
        Iterator<IDeviceRegistryListener> listeners = listenerList.iterator();
        while (listeners.hasNext()) {
            IDeviceRegistryListener listener = listeners.next();
            listener.deviceAdded(device);
        }
    }

    /**
     * Fire an event that indicates a device has been removed.
     * 
     * @param device
     */
    private void fireDeviceRemovedEvent(IDevice device) {
        Iterator<IDeviceRegistryListener> listeners = listenerList.iterator();
        while (listeners.hasNext()) {
            IDeviceRegistryListener listener = listeners.next();
            listener.deviceRemoved(device);
        }
    }

    /**
     * Get the file for the device store file.
     * 
     * @return
     */
    private File getComponentStoreFile() {
        IPath pluginStatePath = MTJCore.getDefault().getStateLocation();
        IPath storePath = pluginStatePath
                .append(IDeviceRegistryConstants.DEVICES_FILENAME);

        return storePath.toFile();
    }

    /**
     * Return the map of devices for the specified group. If the map is null and
     * createIfNull is specified, a new map will be created.
     * 
     * @param groupName
     * @param createIfNull
     * @return
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    private Map<String, IDevice> getDeviceGroupMap(String groupName,
            boolean createIfNull) throws PersistenceException {
        Map<String, Map<String, IDevice>> groupsMap = getDeviceGroupsMap();
        Map<String, IDevice> deviceGroupMap = groupsMap.get(groupName);

        if ((deviceGroupMap == null) && createIfNull) {
            deviceGroupMap = new HashMap<String, IDevice>();
            groupsMap.put(groupName, deviceGroupMap);
        }

        return deviceGroupMap;
    }

    /**
     * Return the device groups map.
     * 
     * @return
     * @throws PersistenceException
     */
    private synchronized Map<String, Map<String, IDevice>> getDeviceGroupsMap()
            throws PersistenceException {
        if (deviceGroupsMap == null) {
            createAndLoadDeviceGroups();
        }

        return deviceGroupsMap;
    }

}
