/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Added application descriptor constant value                           
 *     Hugo Raniere (Motorola)  - Handling the case that there is no valid preverifier
 *     Feng Wang (Sybase) - 1, Add getConfigurations() method for multi-configs support
 *                          2, Remove getEnabledSymbolDefinitionSet() method, since 
 *                             SymbolDefinitionSets are contained by configuration.
 */
package org.eclipse.mtj.core.project.midp;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.build.preverifier.IPreverificationError;
import org.eclipse.mtj.core.build.preverifier.PreverifierNotFoundException;
import org.eclipse.mtj.core.project.IMTJProject;

/**
 * This interface represents access to MIDlet suite project specific
 * information. It acts as a wrapper around an IJavaProject.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IMidletSuiteProject extends IMTJProject {

    /**
     * The project's application descriptor.
     */
    public static final String APPLICATION_DESCRIPTOR_NAME = "Application Descriptor"; //$NON-NLS-1$

    /**
     * Return an ApplicationDescriptor instance wrapped around the Application
     * Descriptor (JAD) file for this MIDlet suite.
     * 
     * @return the suite's application descriptor
     */
    public IApplicationDescriptor getApplicationDescriptor();

    /**
     * @return
     */
    public IFile getApplicationDescriptorFile();

    /**
     * Return the file holding the JAD file. There is a chance that this file
     * may not actually exist if the user moved the file without using the
     * refactoring functionality.
     * 
     * @return the java application descriptor file
     */
    public String getJadFileName();

    /**
     * Return the name to use for the JAR file.
     * 
     * @return the jar file name
     */
    public String getJarFilename();

    /**
     * @return
     */
    public abstract String getTempKeyPassword();

    /**
     * @return
     */
    public abstract String getTempKeystorePassword();

    /**
     * Get the IFolder into which verified classes should be written.
     * 
     * @param monitor progress monitor
     * @return the verified classes output folder
     * @throws CoreException
     */
    public abstract IFolder getVerifiedClassesOutputFolder(
            IProgressMonitor monitor) throws CoreException;

    /**
     * Get the IFolder into which verified libraries should be written.
     * 
     * @param monitor progress monitor
     * @return the verified libraries output folder
     * @throws CoreException
     */
    public abstract IFolder getVerifiedLibrariesOutputFolder(
            IProgressMonitor monitor) throws CoreException;

    /**
     * Get the IFolder into which verified classes should be written.
     * 
     * @param monitor progress monitor
     * @return the verified root output folder
     * @throws CoreException
     */
    public abstract IFolder getVerifiedOutputFolder(IProgressMonitor monitor)
            throws CoreException;

    /**
     * Preverify the specified resources. Return the map of class names with
     * preverification errors mapped to the error that was caused.
     * 
     * @param toVerify the resources to be preverified
     * @param outputFolder the folder into which the output will be written
     * @param monitor progress monitor
     * @return An array of errors found during preverification.
     * @throws CoreException
     * @throws IOException
     */
    public IPreverificationError[] preverify(IResource[] toVerify,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException, PreverifierNotFoundException;

    /**
     * Launch the preverification process on the specified jar file.
     * 
     * @param jarFile The jar file to be preverified.
     * @param outputFolder The folder into which the output is to be placed.
     * @param monitor Progress monitor
     * @return An array of errors found during preverification.
     * @throws CoreException
     * @throws IOException
     * @throws PreverifierNotFoundException
     */
    public IPreverificationError[] preverifyJarFile(File jarFile,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException, PreverifierNotFoundException;

    /**
     * @return
     */
    public void setJadFileName(String jadFileName);

    /**
     * @param pass
     */
    public abstract void setTempKeyPassword(String pass);

    /**
     * @param pass
     */
    public abstract void setTempKeystorePassword(String pass);
}
