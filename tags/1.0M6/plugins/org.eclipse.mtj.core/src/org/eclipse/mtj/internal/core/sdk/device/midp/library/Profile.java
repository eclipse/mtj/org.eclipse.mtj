/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Gorkem Ercan (Nokia) - enum definition mixed with configuration     
 */
package org.eclipse.mtj.internal.core.sdk.device.midp.library;

import org.eclipse.mtj.core.sdk.device.midp.library.ILibrarySpecification;
import org.osgi.framework.Version;

/**
 * @author Diego Madruga Sandin
 */
public enum Profile implements ILibrarySpecification {

    /**
     * Mobile Information Device Profile (1.0)
     */
    MIDP_10("MIDP-1.0", "Mobile Information Device Profile (1.0)", new Version( //$NON-NLS-1$//$NON-NLS-2$
            "1.0")), //$NON-NLS-1$

    /**
     * Mobile Information Device Profile (2.0)
     */
    MIDP_20("MIDP-2.0", "Mobile Information Device Profile (2.0)", new Version( //$NON-NLS-1$//$NON-NLS-2$
            "2.0")), //$NON-NLS-1$

    /**
     * Mobile Information Device Profile (2.1)
     */
    MIDP_21("MIDP-2.1", "Mobile Information Device Profile (2.1)", new Version( //$NON-NLS-1$//$NON-NLS-2$
            "2.1")), //$NON-NLS-1$

    /**
     * Information Module Profile (1.0)
     */
    IMP_10("IMP-1.0", "Information Module Profile (1.0)", new Version("1.0")), //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$

    /**
     * Information Module Profile (NG)
     */
    IMP_NG("IMP-NG", "Information Module Profile (NG)", new Version("2.0")); //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
    


    private String identifier;
    private String name;
    private Version version;

    /**
     * @param identifier the profile identifier
     * @param name the profile name
     * @param version the profile version
     */
    Profile(String identifier, String name, Version version) {
        this.identifier = identifier;
        this.name = name;
        this.version = version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrarySpecification#getIdentifier()
     */
    public String getIdentifier() {
        return identifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrarySpecification#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrarySpecification#getVersion()
     */
    public Version getVersion() {
        return version;
    }

}
