/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk.device.midp.library;

import java.util.Set;

/**
 * The Library Importer Registry provides a place to store and retrieve
 * {@link ILibraryImporter Library Importers} using just it's ID.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface ILibraryImporterRegistry {

    /**
     * Retrieve a {@link ILibraryImporter} from registry.
     * 
     * @param importerId the ID for the {@link ILibraryImporter}. The
     *            <code>importerId</code> must not be <code>null</code>.
     * @return the {@link ILibraryImporter} associated with the passed ID or
     *         <code>null</code> if no importer associated to the specified ID
     *         was found.
     */
    public abstract ILibraryImporter getLibraryImporter(final String importerId);

    /**
     * Register a new {@link ILibraryImporter} associated to the specified ID.
     * If the registry previously contained a mapping for this ID, the old value
     * is replaced by the specified value.
     * 
     * @param importerId the unique ID for the {@link ILibraryImporter}. The
     *            <code>importerId</code> must not be <code>null</code>.
     * @param importer the {@link ILibraryImporter} to be registered. The
     *            <code>importer</code> must not be <code>null</code>.
     * 
     */
    public abstract void addLibraryImporter(final String importerId,
            ILibraryImporter importer);

    /**
     * Returns a set view of the Library Importer IDs contained in this
     * registry.
     * 
     * @return a set view of the Library Importer IDs contained in this
     *         registry.
     */
    public abstract Set<String> getLibraryImporterIds();

}
