/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Fixed sharing violation while packaging.
 *     Gang  Ma (Sybase)        - Fixed runtime jar file locked after building.   
 *     Feng Wang (Sybase)       - Ensure runtime JAD contains correct size of 
 *                                runtime JAR, keeping runtime JAR unlocked 
 *                                after building                  
 *     Hugo Raniere (Motorola)  - Handling the case that there is no valid 
 *                                preverifier
 *     Diego Sandin (Motorola)  - Fix error when running builder in project 
 *                                with name equal to previously deleted project
 *     Feng Wang (Sybase) - Modify getDeploymentFolder(IProject, IProgressMonitor)
 *                          method, make a deployment folder for current active
 *                          configuration, to support build for multi-configs.
 */
package org.eclipse.mtj.internal.core.build.preverifier.builder;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.IStreamListener;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IStreamMonitor;
import org.eclipse.debug.core.model.IStreamsProxy;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverificationError;
import org.eclipse.mtj.core.build.preverifier.PreverifierNotFoundException;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.midp.IApplicationDescriptor;
import org.eclipse.mtj.core.project.midp.IMidletDefinition;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.PreferenceAccessor;
import org.eclipse.mtj.internal.core.build.BuildConsoleProxy;
import org.eclipse.mtj.internal.core.build.BuildLoggingConfiguration;
import org.eclipse.mtj.internal.core.build.IBuildConsoleProxy;
import org.eclipse.mtj.internal.core.build.preverifier.PreverificationUtils;
import org.eclipse.mtj.internal.core.packaging.midp.DeployedJADWriter;
import org.eclipse.mtj.internal.core.packaging.midp.ObfuscatorTool;
import org.eclipse.mtj.internal.core.project.midp.ApplicationDescriptor;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.util.AbstractClasspathEntryVisitor;
import org.eclipse.mtj.internal.core.util.ColonDelimitedProperties;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Version;

import de.schlichtherle.io.ArchiveException;
import de.schlichtherle.io.File;
import de.schlichtherle.io.FileOutputStream;

/**
 * Provides an incremental project builder implementation to do a J2ME
 * preverification of classes. It is imperative that this builder follow the
 * standard Java builder. The standard Java builder will generate the standard
 * compiled class. The preverifier will then preverify that generated class.
 * 
 * @author Craig Setera
 */
public class PreverificationBuilder extends IncrementalProjectBuilder {

    /**
     * Implementation of the IClasspathEntryVisitor interface for collecting the
     * set of required projects
     */
    private static class RequiredProjectsCPEntryVisitor extends
            AbstractClasspathEntryVisitor {

        private ArrayList<IJavaProject> requiredProjects;

        /** Construct a new instance. */
        private RequiredProjectsCPEntryVisitor() {
            requiredProjects = new ArrayList<IJavaProject>();
        }

        /**
         * @return Returns the requiredProjects.
         */
        public ArrayList<IJavaProject> getRequiredProjects() {
            return requiredProjects;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.core.util.AbstractClasspathEntryVisitor#visitProject(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IProgressMonitor)
         */
        @Override
        public boolean visitProject(IClasspathEntry entry,
                IJavaProject javaProject, IJavaProject classpathProject,
                IProgressMonitor monitor) throws CoreException {
            boolean continueVisitation = entry.isExported();

            if (continueVisitation) {
                requiredProjects.add(classpathProject);
            }

            return continueVisitation;
        }
    }

    public static final String ARG_DO_OBFUSCATION = "_do_obfuscation"; //$NON-NLS-1$

    public static final String ARG_DO_PACKAGE = "_do_package"; //$NON-NLS-1$
    public static final String ARG_UPDATE_VERSION = "_update_version"; //$NON-NLS-1$
    private static BuildLoggingConfiguration buildLoggingConfig = BuildLoggingConfiguration
            .getInstance();

    private static BuildConsoleProxy consoleProxy = BuildConsoleProxy
            .getInstance();

    // Tracks the TrueZip File instances that point to the deployed jar files
    private static Map<IProject, File> runtimeJars = new HashMap<IProject, File>(
            5);

    /**
     * Clean the output of the specified project.
     * 
     * @param project
     * @param cleanDeployed
     * @param monitor
     * @throws JavaModelException
     * @throws CoreException
     */
    public static void cleanProject(IProject project, boolean cleanDeployed,
            IProgressMonitor monitor) throws JavaModelException, CoreException {

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_0, project));
        }

        IJavaProject javaProject = JavaCore.create(project);

        BuildInfo buildInfo = new BuildInfo(
                IncrementalProjectBuilder.CLEAN_BUILD,
                new HashMap<Object, Object>(), javaProject);

        // Clear and remove the old verified directory and runtime folders
        IFolder oldVerifiedFolder = project.getFolder(MTJCore
                .getVerifiedOutputDirectoryName());

        if (oldVerifiedFolder.exists()) {
            Utils.clearContainer(oldVerifiedFolder, monitor);
            oldVerifiedFolder.delete(true, monitor);
        }

        IFolder oldRuntimeFolder = project.getFolder(
                IMTJCoreConstants.TEMP_FOLDER_NAME).getFolder(
                IMTJCoreConstants.RUNTIME_FOLDER_NAME);
        if (oldRuntimeFolder.exists()) {
            Utils.clearContainer(oldRuntimeFolder, monitor);
            oldRuntimeFolder.delete(true, monitor);
        }

        // Clear the classes and libraries
        IFolder classesPreverifyFolder = buildInfo
                .getVerifiedClassesFolder(monitor);
        Utils.clearContainer(classesPreverifyFolder, monitor);

        IFolder libsPreverifyFolder = buildInfo.getVerifiedLibsFolder(monitor);
        Utils.clearContainer(libsPreverifyFolder, monitor);

        // Delete the runtime JAR file
        IFolder runtimeFolder = buildInfo.getRuntimeFolder(monitor);
        if (runtimeFolder.exists()) {
            deleteRuntimeJar(project, monitor);
            Utils.clearContainer(runtimeFolder, monitor);
        }

        // We also delete the deployed folder
        if (cleanDeployed) {
            IFolder deploymentFolder = getDeploymentFolder(project, monitor);
            if (deploymentFolder.exists()) {
                Utils.clearContainer(deploymentFolder, monitor);
            }
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_1, project));
        }
    }

    /**
     * Return the File instance for the runtime jar file in the specified
     * project.
     * 
     * @param project
     * @return
     */
    public static File getRuntimeJar(IProject project, IProgressMonitor monitor)
            throws CoreException {
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_2, project));
        }

        File runtimeJar = runtimeJars.get(project);

        if ((runtimeJar == null) || (!runtimeJar.exists())) {
            if (buildLoggingConfig.isPreverifierTraceEnabled()) {
                consoleProxy.traceln(BuilderMessages.PreverificationBuilder_3);
            }

            IFolder tempFolder = project
                    .getFolder(IMTJCoreConstants.TEMP_FOLDER_NAME);
            IFolder runtimeFolder = tempFolder
                    .getFolder(IMTJCoreConstants.EMULATION_FOLDER_NAME);
            createFolders(runtimeFolder, monitor);

            IJavaProject javaProject = JavaCore.create(project);
            IMidletSuiteProject midletSuite = MidletSuiteFactory
                    .getMidletSuiteProject(javaProject);

            runtimeJar = new File(runtimeFolder.getLocation().toFile(),
                    midletSuite.getJarFilename());
            runtimeJar.mkdir();
            runtimeFolder.refreshLocal(IResource.DEPTH_ONE, monitor);

            runtimeJars.put(project, runtimeJar);
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_4, project));
        }
        return runtimeJar;
    }

    /**
     * Delete the deployed jar file instance being cached (if found).
     * 
     * @param project
     * @param monitor
     * @throws CoreException
     */
    private static void deleteRuntimeJar(IProject project,
            IProgressMonitor monitor) throws CoreException {
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_5, project));
        }

        File runtimeJar = runtimeJars.remove(project);
        if ((runtimeJar != null) && (runtimeJar.exists())) {
            try {
                if (buildLoggingConfig.isPreverifierTraceEnabled()) {
                    consoleProxy.traceln(NLS.bind(
                            BuilderMessages.PreverificationBuilder_6,
                            runtimeJar));
                }
                // Release the initial locks on the jar file and flush the
                // current buffered information
                File.umount(runtimeJar, true, true, true, true);

                // Release the initial locks on the jar file and flush the
                // current buffered information
                File.umount(runtimeJar, true, true, true, true);

                // Delete the contents of the JAR file... this is necessary
                // before TrueZip will allow us to delete the top level jar
                // file. While it might be possible to get to a standard
                // java.io.File object to avoid this handling, this approach
                // helps to make sure that TrueZip has consistent information
                // about what is on the file system.
                runtimeJar.deleteAll();

                // Now that the contents of the jar file are gone, TrueZip will
                // allow us to completely delete the file, finally releasing the
                // locks on that file.
                runtimeJar.delete();

                IFolder tempFolder = project
                        .getFolder(IMTJCoreConstants.TEMP_FOLDER_NAME);
                IFolder runtimeFolder = tempFolder
                        .getFolder(IMTJCoreConstants.EMULATION_FOLDER_NAME);
                runtimeFolder.refreshLocal(1, monitor);

            } catch (CoreException e) {
                e.printStackTrace();
                throw e;
            } catch (ArchiveException e) {
                e.printStackTrace();
                MTJCore.throwCoreException(IStatus.ERROR, -999, e);
            }
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_7, project));
        }
    }

    /**
     * Get the folder into which the jad and jar will be written.
     * 
     * @param project
     * @param monitor
     * @return
     * @throws CoreException
     */
    private static IFolder getDeploymentFolder(IProject project,
            IProgressMonitor monitor) throws CoreException {

        IJavaProject javaProject = JavaCore.create(project);

        IMTJProject suite = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);

        String deploymentDirectoryName = MTJCore.getDeploymentDirectoryName();
        IPath deploymentPath = new Path(deploymentDirectoryName);
        IFolder deploymentFolder = project.getFolder(deploymentPath);

        // Should refreshLocal before create folder, because if import project,
        // the folder is not refresh yet before create
        deploymentFolder.refreshLocal(IResource.DEPTH_INFINITE, monitor);

        if (!deploymentFolder.exists()) {
            deploymentFolder.create(false, true, monitor);
        }

        String activeConfigName = suite.getMTJRuntime().getActiveMTJRuntime()
                .getName();
        IFolder configDeployFolder = deploymentFolder
                .getFolder(activeConfigName);
        if (!configDeployFolder.exists()) {
            configDeployFolder.create(false, true, monitor);
        }
        return configDeployFolder;
    }

    /**
     * Create the specified folder and all parent folders as necessary.
     * 
     * @param folder
     * @param monitor
     * @throws CoreException
     */
    static void createFolders(IFolder folder, IProgressMonitor monitor)
            throws CoreException {

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_8, folder));
        }

        while (!folder.exists()) {
            if (folder.getParent().getType() == IResource.FOLDER) {
                createFolders((IFolder) folder.getParent(), monitor);
            }

            folder.create(true, true, monitor);
            folder.setDerived(true);
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_9, folder));
        }
    }

    /**
     * Generate a MANIFEST.MF file into the deployed folder based on the current
     * information in the JAD file.
     * 
     * @param midletSuite
     * @param monitor
     * @throws CoreException
     */
    static void generateDeployedManifest(IMidletSuiteProject midletSuite,
            IProgressMonitor monitor) throws CoreException {

        IProject project = midletSuite.getProject();
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_10, project));
        }

        if (!midletSuite.getApplicationDescriptorFile().exists()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_11, midletSuite
                            .getApplicationDescriptorFile().getName()));
            return;
        }

        IApplicationDescriptor applicationDescriptor = midletSuite
                .getApplicationDescriptor();
        Properties manifestProperties = applicationDescriptor
                .getManifestProperties();

        // Filter out manifest attributes
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_12);
        }

        String[] excluded = PreferenceAccessor.instance
                .getExcludedManifestProperties(project);
        for (String excludedName : excluded) {
            if (manifestProperties.containsKey(excludedName)) {
                manifestProperties.remove(excludedName);
            }
        }

        Manifest jarManifest = new Manifest();
        Attributes mainAttributes = jarManifest.getMainAttributes();
        mainAttributes.putValue(Attributes.Name.MANIFEST_VERSION.toString(),
                BuilderMessages.PreverificationBuilder_13);

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_14);
        }
        Iterator<Map.Entry<Object, Object>> iterator = manifestProperties
                .entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Object, Object> entry = iterator.next();
            try {
                if (buildLoggingConfig.isPreverifierTraceEnabled()) {
                    consoleProxy.traceln(NLS.bind(
                            BuilderMessages.PreverificationBuilder_15,
                            new Object[] { entry.getKey(), entry.getValue() }));
                }
                mainAttributes.putValue((String) entry.getKey(), (String) entry
                        .getValue());
            } catch (IllegalArgumentException e) {
                if (buildLoggingConfig.isPreverifierTraceEnabled()) {
                    consoleProxy.traceln(NLS.bind(
                            BuilderMessages.PreverificationBuilder_16, e));
                }

                Status s = new Status(IStatus.ERROR,
                        IMTJCoreConstants.PLUGIN_ID, -999, NLS.bind(
                                BuilderMessages.PreverificationBuilder_17, e
                                        .getMessage()), e);

                throw new CoreException(s);
            }
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_18);
        }

        Iterator<IMidletDefinition> iter = applicationDescriptor
                .getMidletDefinitions().iterator();
        while (iter.hasNext()) {
            IMidletDefinition def = iter.next();

            String key = ApplicationDescriptor.MIDLET_PREFIX + def.getNumber();
            String value = def.toString();
            if (buildLoggingConfig.isPreverifierTraceEnabled()) {
                consoleProxy.traceln(NLS.bind(
                        BuilderMessages.PreverificationBuilder_19,
                        new Object[] { key, value }));
            }

            mainAttributes.putValue(key, value);
        }

        FileOutputStream fos = null;

        File manifestFile = new File(getRuntimeJar(project, monitor),
                IMTJCoreConstants.MANIFEST_FILE_NAME);

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_20, manifestFile));
        }
        try {
            fos = new FileOutputStream(manifestFile);
            jarManifest.write(fos);
        } catch (IOException e) {
            if (buildLoggingConfig.isPreverifierTraceEnabled()) {
                consoleProxy.traceln(NLS.bind(
                        BuilderMessages.PreverificationBuilder_21, e));
            }
            Status s = new Status(IStatus.ERROR, IMTJCoreConstants.PLUGIN_ID,
                    -999, NLS.bind(BuilderMessages.PreverificationBuilder_22, e
                            .getMessage()), e);

            throw new CoreException(s);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                }
            }
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(
                    BuilderMessages.PreverificationBuilder_23, midletSuite
                            .getProject()));
        }
    }

    /**
     * Construct a new builder instance.
     */
    public PreverificationBuilder() {
    }

    /**
     * Clear previous JavaME markers for the project.
     * 
     * @throws CoreException
     */
    private void clearPreprocessorMarkers() throws CoreException {
        getProject().deleteMarkers(IMTJCoreConstants.JAVAME_PROBLEM_MARKER,
                false, IResource.DEPTH_ZERO);
    }

    /**
     * Copy the runtime jar file to the deployed folder.
     * 
     * @param buildInfo
     * @param monitor
     * @throws CoreException
     */
    private void copyRuntimeJarToDeploymentFolder(BuildInfo buildInfo,
            IProgressMonitor monitor) throws CoreException {
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_24);
        }

        FileInputStream fis = null;

        try {
            File runtimeJar = getRuntimeJar(getProject(), monitor);
            fis = new FileInputStream(runtimeJar);

            IFolder deploymentFolder = getDeploymentFolder(buildInfo, monitor);
            IFile deployedJar = deploymentFolder.getFile(runtimeJar.getName());
            if (deployedJar.exists()) {
                deployedJar.setContents(fis, true, false, monitor);
            } else {
                deployedJar.create(fis, true, monitor);
            }

            writeJADFile(buildInfo,
                    new File(deployedJar.getLocation().toFile()),
                    deploymentFolder, false, monitor);

        } catch (IOException e) {
            MTJCore.throwCoreException(IStatus.ERROR, 999, e);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                }
            }
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_25);
        }
    }

    /**
     * Create an error marker on the project about no preverifier found.
     * 
     * @throws CoreException
     */
    private void createNoPreverifierFoundMarker() throws CoreException {
        final IProject project = getProject();
        IWorkspaceRunnable runnable = new IWorkspaceRunnable() {
            public void run(IProgressMonitor monitor) throws CoreException {
                IMarker marker = project
                        .createMarker(IMTJCoreConstants.JAVAME_PROBLEM_MARKER);
                marker
                        .setAttribute(
                                IMarker.MESSAGE,
                                BuilderMessages.PreverificationBuilder_PreverifierNotFoundErrorMessage);
                marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
            }
        };
        project.getWorkspace().run(runnable, null);
    }

    /**
     * Do the work to obfuscate the jar file.
     * 
     * @param deployedJarFile
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    private void doObfuscation(BuildInfo buildInfo, IProgressMonitor monitor)
            throws CoreException, PreverifierNotFoundException {
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_26);
        }

        final StringBuffer errorText = new StringBuffer();

        File runtimeJar = buildInfo.getRuntimeJarFile(monitor);
        consoleProxy.traceln(BuilderMessages.PreverificationBuilder_27
                + runtimeJar);

        // Calculate the name of the obfuscated jar file name...
        Path runtimeJarPath = new Path(runtimeJar.getAbsolutePath());
        String basename = runtimeJarPath.lastSegment();
        basename = basename.substring(0, basename.length() - 4);
        // Making an assumption of .jar file extension

        IFolder deploymentFolder = getDeploymentFolder(buildInfo, monitor);
        IFile obfuscatedJarFile = deploymentFolder.getFile(basename
                + "_obf.jar"); //$NON-NLS-1$
        IFile deployedJarFile = deploymentFolder.getFile(runtimeJarPath
                .lastSegment());

        consoleProxy.traceln(BuilderMessages.PreverificationBuilder_29
                + obfuscatedJarFile.getLocation().toFile());

        ObfuscatorTool obfuscator = new ObfuscatorTool(buildInfo
                .getMidletSuite(), runtimeJar, obfuscatedJarFile.getLocation()
                .toFile());
        ILaunch launch = obfuscator.launch(monitor);

        // Snag any error output that might occur
        final StringBuffer stdoutBuffer = new StringBuffer();
        IProcess[] processes = launch.getProcesses();
        if ((processes != null) && (processes.length > 0)) {
            IProcess process = processes[0];
            IStreamsProxy proxy = process.getStreamsProxy();

            // Wire up a listener to write to the console if logging is
            // enabled
            if (buildLoggingConfig.isObfuscationOutputEnabled()) {
                consoleProxy.traceln(BuilderMessages.PreverificationBuilder_30);
                consoleProxy.addConsoleStreamListener(
                        IBuildConsoleProxy.Stream.ERROR, proxy
                                .getErrorStreamMonitor());
                consoleProxy.addConsoleStreamListener(
                        IBuildConsoleProxy.Stream.OUTPUT, proxy
                                .getOutputStreamMonitor());
            }

            proxy.getErrorStreamMonitor().addListener(new IStreamListener() {
                public void streamAppended(String text, IStreamMonitor monitor) {
                    errorText.append(text);
                }
            });

            // Wait until completion
            while ((!monitor.isCanceled()) && (!process.isTerminated())) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                ;
            }

            if (buildLoggingConfig.isObfuscationOutputEnabled()) {
                consoleProxy.traceln(BuilderMessages.PreverificationBuilder_31
                        + process.getExitValue());
            }

            // Log the stdout if requested
            if (stdoutBuffer.length() > 0) {
                MTJCore.log(IStatus.INFO, stdoutBuffer.toString());
            }

            // Let the user know that something went wrong if necessary
            boolean doFinalPreverify = true;
            if (errorText.length() > 0) {
                String text = errorText.toString();
                IStatus status = new Status(IStatus.ERROR,
                        IMTJCoreConstants.PLUGIN_ID,
                        IMTJCoreConstants.ERR_OBFUSCATION_ERRORS, text, null);

                Boolean response = (Boolean) MTJCore.statusPrompt(status, this);
                doFinalPreverify = (response != null) ? response.booleanValue()
                        : false;
            }

            if (doFinalPreverify) {
                doPostObfuscationPreverification(buildInfo, obfuscatedJarFile,
                        deployedJarFile, monitor);
            }
        }

        writeJADFile(buildInfo,
                new File(deployedJarFile.getLocation().toFile()),
                deploymentFolder, false, monitor);

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_32);
        }
    }

    /**
     * Do the preverification necessary after obfuscation occurs.
     * 
     * @param obfuscatedJar
     * @param deployedJarFile
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     * @throws IOException
     */
    private void doPostObfuscationPreverification(BuildInfo buildInfo,
            IFile obfuscatedJar, IFile deployedJarFile, IProgressMonitor monitor)
            throws CoreException, PreverifierNotFoundException {
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_33);
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_34
                    + obfuscatedJar.getLocation().toFile());
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_35
                    + deployedJarFile.getLocation().toFile());
        }

        try {
            // Create a temporary directory to handle the preverification
            // output
            IFolder deployFolder = (IFolder) obfuscatedJar.getParent();
            IFolder tempFolder = deployFolder.getFolder("temp"); //$NON-NLS-1$
            if (!tempFolder.exists()) {
                tempFolder.create(true, true, monitor);
            }

            // Preverify the jar file into the temp directory
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_37
                    + tempFolder.getLocation().toFile());
            File jarFile = new File(obfuscatedJar.getLocation().toFile());
            IPreverificationError[] errors = buildInfo.getMidletSuite()
                    .preverifyJarFile(jarFile, tempFolder, monitor);
            tempFolder.refreshLocal(IResource.DEPTH_ONE, monitor);

            // Check for errors
            if (errors.length > 0) {
                consoleProxy.traceln(errors.length
                        + BuilderMessages.PreverificationBuilder_38);
                handlePreverificationErrors(errors);
            }

            // Copy the result back into the deployment directory
            IFile finalJarFile = getJarFile(buildInfo, deployFolder, false);
            IFile preverified = tempFolder.getFile(obfuscatedJar.getName());
            Utils.copyFile(preverified, finalJarFile);
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_39
                    + preverified + BuilderMessages.PreverificationBuilder_40
                    + finalJarFile.getLocation().toFile());

            // Clean up the temp directory
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_41
                    + preverified.getLocation().toFile());
            preverified.delete(true, monitor);
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_42
                    + tempFolder.getLocation().toFile());
            tempFolder.delete(true, monitor);
        } catch (IOException e) {
            MTJCore.throwCoreException(IStatus.ERROR, 999, e);
        }
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_43);
        }
    }

    /**
     * Get the folder into which the jad and jar will be written.
     * 
     * @param monitor
     * @return
     * @throws CoreException
     */
    private IFolder getDeploymentFolder(BuildInfo buildInfo,
            IProgressMonitor monitor) throws CoreException {
        return getDeploymentFolder(buildInfo.getMidletSuite().getProject(),
                monitor);
    }

    /**
     * Get the IFile instance into which the JAR will be written.
     * 
     * @return
     */
    private IFile getJarFile(BuildInfo buildInfo, IFolder deploymentFolder,
            boolean obfuscateName) {
        String jarFileName = buildInfo.getMidletSuite().getJarFilename();
        if (obfuscateName) {
            int length = jarFileName.length();
            jarFileName = jarFileName.substring(0, length - 4) + "_base.jar"; //$NON-NLS-1$
        }

        return deploymentFolder.getFile(jarFileName);
    }

    /**
     * Get the projects that are required by the project being built.
     * 
     * @param javaProject
     * @param monitor
     * @return
     * @throws CoreException
     */
    private IJavaProject[] getRequiredProjects(IJavaProject javaProject,
            IProgressMonitor monitor) throws CoreException {
        RequiredProjectsCPEntryVisitor visitor = new RequiredProjectsCPEntryVisitor();
        visitor.getRunner().run(javaProject, visitor, monitor);

        ArrayList<IJavaProject> projects = visitor.getRequiredProjects();
        return projects.toArray(new IJavaProject[projects.size()]);
    }

    /**
     * Add text based on the specified error to the string buffer.
     * 
     * @param sb
     * @param error
     */
    /**
     * Handle preverification errors that were encountered while obfuscating.
     * 
     * @param errors
     * @throws CoreException
     */
    private void handlePreverificationErrors(IPreverificationError[] errors)
            throws CoreException {
        StringBuffer sb = new StringBuffer(
                BuilderMessages.PreverificationBuilder_45);
        for (int i = 0; i < errors.length; i++) {
            if (i != 0) {
                sb.append("\n"); //$NON-NLS-1$
            }
            sb.append(PreverificationUtils.getErrorText(errors[i]));
        }

        MTJCore.throwCoreException(IStatus.ERROR, -999, sb.toString());
    }

    /**
     * Preverify the libraries associated with the current java project in the
     * build info.
     * 
     * @param buildInfo
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    private void preverifyLibraries(BuildInfo buildInfo,
            IProgressMonitor monitor) throws CoreException,
            PreverifierNotFoundException {
        IProject project = buildInfo.getCurrentJavaProject().getProject();
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_47
                    + project);
        }

        if ((project != null) && project.isAccessible()) {
            monitor.setTaskName(BuilderMessages.PreverificationBuilder_48
                    + project.getName());

            // Figure the resource delta to be used
            buildInfo.setCurrentResourceDelta(null);
            if (buildInfo.getBuildKind() != FULL_BUILD) {
                buildInfo.setCurrentResourceDelta(getDelta(project));
            }

            // Hand off to the build helper for the heavy lifting
            ResourceDeltaBuilder deltaBuilder = new ResourceDeltaBuilder(
                    buildInfo);
            deltaBuilder.preverifyLibraries(monitor);

            monitor.worked(1);
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_49
                    + project);
        }
    }

    /**
     * Preverify the project based on the specified build information.
     * 
     * @param buildInfo
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    private void preverifyProject(BuildInfo buildInfo, IProgressMonitor monitor)
            throws CoreException, PreverifierNotFoundException {
        IProject project = buildInfo.getCurrentJavaProject().getProject();

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_50
                    + project);
        }

        if ((project != null) && project.isAccessible()) {
            monitor.setTaskName(BuilderMessages.PreverificationBuilder_51
                    + project.getName());

            // Figure the resource delta to be used
            buildInfo.setCurrentResourceDelta(null);
            if (buildInfo.getBuildKind() != FULL_BUILD) {
                buildInfo.setCurrentResourceDelta(getDelta(project));
            }

            // Hand off to the build helper for the heavy lifting
            ResourceDeltaBuilder deltaBuilder = new ResourceDeltaBuilder(
                    buildInfo);
            deltaBuilder.build(monitor);
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_52
                    + project);
        }
    }

    /**
     * Set the resources in the output directory to be derived so they are left
     * alone by the team support.
     * 
     * @param verifiedFolder
     * @param monitor
     * @throws CoreException
     */
    private void setResourcesAsDerived(IFolder verifiedFolder,
            IProgressMonitor monitor) throws CoreException {
        // Refresh from the folder down so that we can
        // set derived on these...
        verifiedFolder.refreshLocal(IResource.DEPTH_INFINITE, monitor);

        // Get the starting folder
        MTJCore.setResourcesAsDerived(verifiedFolder);
    }

    /**
     * Update the JAD version in the manifest properties.
     * 
     * @throws IOException
     * @throws CoreException
     */
    private void updateJADVersion(BuildInfo buildInfo, IProgressMonitor monitor)
            throws CoreException {
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_53);
        }

        // Read the source jad file and update the jar file
        // length property.
        IApplicationDescriptor appDescriptor = buildInfo.getMidletSuite()
                .getApplicationDescriptor();
        ColonDelimitedProperties jadProperties = (ColonDelimitedProperties) appDescriptor
                .getManifestProperties();

        // Calculate the updated version string
        String versionString = jadProperties.getProperty(
                IJADConstants.JAD_MIDLET_VERSION, "0.0.0"); //$NON-NLS-1$
        Version version = new Version(versionString);

        int major = version.getMajor();
        int minor = version.getMinor();
        int secondary = version.getMicro();

        if (secondary >= 99) {
            secondary = 0;
            minor++;
        } else {
            secondary++;
        }

        StringBuffer newVersion = new StringBuffer();
        newVersion.append(major).append(".").append(minor).append(".").append( //$NON-NLS-1$ //$NON-NLS-2$
                secondary);

        // Update the JAD
        jadProperties.setProperty(IJADConstants.JAD_MIDLET_VERSION, newVersion
                .toString());

        try {
            appDescriptor.store();
        } catch (IOException e) {
            MTJCore.throwCoreException(IStatus.ERROR, 999, e);
        }

        generateDeployedManifest(buildInfo.getMidletSuite(), monitor);
        buildInfo.getMidletSuite().getApplicationDescriptorFile().refreshLocal(
                IResource.DEPTH_ONE, monitor);

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_57);
        }
    }

    /**
     * Write the JAD file for the jar file into the specified location.
     * 
     * @param buildInfo
     * @param targetFolder
     * @param monitor
     * @throws CoreException
     * @throws IOException
     */
    private void writeJADFile(BuildInfo buildInfo, File jarFile,
            IFolder targetFolder, boolean incrementalBuild,
            IProgressMonitor monitor) throws CoreException {
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_58
                    + jarFile + BuilderMessages.PreverificationBuilder_59
                    + targetFolder);
        }

        DeployedJADWriter writer = new DeployedJADWriter(buildInfo
                .getMidletSuite(), targetFolder, new java.io.File(jarFile
                .getAbsolutePath()));

        try {
            writer.writeDeployedJAD(incrementalBuild, monitor);
        } catch (IOException e) {
            MTJCore.throwCoreException(IStatus.ERROR, 999, e);
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_60);
        }
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.core.resources.IncrementalProjectBuilder#build(int,
     * java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    @SuppressWarnings("unchecked")
    protected IProject[] build(int kind, Map args, IProgressMonitor monitor)
            throws CoreException {
        clearPreprocessorMarkers();
        IProject project = getProject();
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_61
                    + project);
        }

        IJavaProject rootJavaProject = JavaCore.create(project);
        IJavaProject javaProject = rootJavaProject;
        IJavaProject[] requiredProjects = getRequiredProjects(javaProject,
                monitor);

        // TODO Fix monitoring
        monitor.beginTask(BuilderMessages.PreverificationBuilder_62,
                requiredProjects.length + 1);
        BuildInfo buildInfo = new BuildInfo(kind, args, javaProject);

        // Update the manifest version if specified
        boolean updateVersionForProject = PreferenceAccessor.instance
                .getAutoversionPackage(project)
                && buildInfo.isBuildArgumentTrue(ARG_UPDATE_VERSION);
        if (updateVersionForProject) {
            updateJADVersion(buildInfo, monitor);
        } else {
            generateDeployedManifest(buildInfo.getMidletSuite(), monitor);
        }

        // Start with our project
        try {
            preverifyProject(buildInfo, monitor);

            // Now prereq projects
            for (IJavaProject requiredProject : requiredProjects) {
                javaProject = requiredProject;
                if (javaProject != null) {
                    buildInfo.setCurrentJavaProject(javaProject);
                    preverifyProject(buildInfo, monitor);
                }
            }

            // Make sure all of the libraries in the project have been
            // preverified
            if (buildInfo.isClasspathChanged()
                    && buildInfo.areLibrariesPreverified()) {
                IFolder libsFolder = buildInfo.getVerifiedLibsFolder(monitor);
                Utils.clearContainer(libsFolder, monitor);

                buildInfo.setCurrentJavaProject(rootJavaProject);
                preverifyLibraries(buildInfo, monitor);

                for (IJavaProject requiredProject : requiredProjects) {
                    javaProject = requiredProject;
                    buildInfo.setCurrentJavaProject(javaProject);
                    preverifyLibraries(buildInfo, monitor);
                }
            }

            // Convert to IProject instances
            IProject[] interestingProjects = new IProject[requiredProjects.length];
            for (int i = 0; i < requiredProjects.length; i++) {
                interestingProjects[i] = requiredProjects[i].getProject();
            }

            // Make sure that the TrueZIP caches are forced out to the file
            // system
            // MUST unmount runtime jar before packaging
            if (buildInfo.isPackageDirty()) {
                try {
                    File runtimeJar = buildInfo.getRuntimeJarFile(monitor);
                    File.umount(runtimeJar, true, true, true, true);
                } catch (ArchiveException e) {
                    MTJCore.throwCoreException(IStatus.ERROR, -999, e);
                }
            }

            // Obfuscate as requested
            if (buildInfo.isBuildArgumentTrue(ARG_DO_PACKAGE)) {
                buildInfo.setPackageDirty(true);

                if (buildInfo.isBuildArgumentTrue(ARG_DO_OBFUSCATION)) {
                    doObfuscation(buildInfo, monitor);
                } else {
                    copyRuntimeJarToDeploymentFolder(buildInfo, monitor);
                }
            }

            // Refresh...
            if (buildInfo.isPackageDirty()) {
                generateDeployedManifest(buildInfo.getMidletSuite(), monitor);
                IFolder deployedFolder = getDeploymentFolder(project, monitor);
                deployedFolder.refreshLocal(IResource.DEPTH_INFINITE, monitor);

                // Set all of the resources in the verified path
                // to be derived resources
                setResourcesAsDerived(buildInfo
                        .getVerifiedClassesFolder(monitor), monitor);
                setResourcesAsDerived(buildInfo.getVerifiedLibsFolder(monitor),
                        monitor);
                setResourcesAsDerived(buildInfo.getRuntimeFolder(monitor),
                        monitor);
            }

            // Make sure that the TrueZIP caches are forced out to the file
            // system
            // and update the JAD file to contain the correct size.
            // This block should better be run at the end of build process,
            // ensure
            // runtime jar unmounted after building
            try {
                File runtimeJar = buildInfo.getRuntimeJarFile(monitor);
                // unmount runtime jar
                File.umount(runtimeJar, true, true, true, true);
                // write runtime JAD file to contain the correct size of runtime
                // jar
                writeJADFile(buildInfo, runtimeJar, buildInfo
                        .getRuntimeFolder(monitor), true, monitor);
                // refresh .mtj.tmp folder
                getProject().getFolder(IMTJCoreConstants.TEMP_FOLDER_NAME)
                        .refreshLocal(IResource.DEPTH_INFINITE, monitor);
            } catch (ArchiveException e) {
                MTJCore.throwCoreException(IStatus.ERROR, -999, e);
            }

            monitor.done();

            if (buildLoggingConfig.isPreverifierTraceEnabled()) {
                consoleProxy.traceln(BuilderMessages.PreverificationBuilder_63
                        + project);
            }

            return interestingProjects;
        } catch (PreverifierNotFoundException e) {
            createNoPreverifierFoundMarker();
            return null;
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IncrementalProjectBuilder#clean(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    protected void clean(IProgressMonitor monitor) throws CoreException {
        IProject project = getProject();

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_64
                    + project);
        }

        super.clean(monitor);
        cleanProject(project, false, monitor);

        // Regenerate the manifest
        IMidletSuiteProject suite = MidletSuiteFactory
                .getMidletSuiteProject(JavaCore.create(getProject()));
        generateDeployedManifest(suite, monitor);

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_65
                    + project);
        }
    }
}
