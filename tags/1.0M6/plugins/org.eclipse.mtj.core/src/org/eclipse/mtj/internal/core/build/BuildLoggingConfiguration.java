/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.build;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.osgi.util.NLS;

/**
 * A simple class that wraps the configuration for build logging.
 * <p>
 * Logging of the build process can be enabled and controlled via a Java System
 * property. Build logging information is configured using the "
 * <code>mtj.build.logging</code>" system property. The property supports a
 * comma-separated list of logging items that may be enabled. The available
 * options are:
 * <ul>
 * <li><b>obfuscationOutput</b><br>
 * Logs the standard error and output streams of the Proguard obfuscator.</li>
 * <li><b>preprocessorTrace</b><br>
 * Logs a trace of the preprocessor builder.</li>
 * <li><b>preverifierOutput</b><br>
 * Logs the standard error and output streams of the preverifier.</li>
 * <li><b>preverifierTrace</b><br>
 * Logs a trace of the preverification builder.</li>
 * <li><b>all</b><br>
 * Turns on all of the available logging options.</li>
 * </ul>
 * <p>
 * For example:
 * </p>
 * <code> -vmargs -Dmtj.build.logging=preverifierOutput,obfuscationOutput </code>
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 * @author Craig Setera
 */
public class BuildLoggingConfiguration {

    /**
     * {@link Enum} with all possible logging configurations to be enabled.
     * 
     * @author Diego Madruga Sandin
     * @since 1.0
     */
    public enum Configuration {

        /**
         * Enable all available logging options.
         */
        all,

        /**
         * Enable logging of the standard error and output streams of the
         * Proguard obfuscator.
         */
        obfuscationOutput,

        /**
         * Enable preprocessing tracing of the preprocessor builder.
         */
        preprocessorTrace,

        /**
         * Enable logging of the standard error and output streams of the
         * preverifier.
         */
        preverifierOutput,

        /**
         * Enable tracing of the preverification builder.
         */
        preverifierTrace;

        /**
         * Creates an enum set of {@link Configuration Configuration's}
         * containing the specified elements.
         * 
         * @param configurations comma-separated list of logging items that may
         *            be enabled.
         * @return enum set of {@link Configuration Configuration's}
         */
        public static Set<Configuration> getEnumSet(String configurations) {

            if ((configurations == null)
                    || (configurations.length() < all.name().length())) {
                return Collections.emptySet();
            }

            String[] values = configurations.split(","); //$NON-NLS-1$
            List<Configuration> m = new ArrayList<Configuration>();

            for (String conf : values) {
                try {
                    Configuration cfg = Configuration.valueOf(conf);
                    m.add(cfg);
                } catch (Exception e) {
                    MTJCore.log(IStatus.WARNING, NLS.bind(
                            Messages.BuildLoggingConfiguration_unknown_configuration_error, conf));
                }
            }
            Set<Configuration> set = EnumSet.copyOf(m);
            return set;
        }
    }

    /**
     * System property used for Build logging information configuration.
     */
    public static final String BUILD_LOGGING_VM_ARG = "mtj.build.logging"; //$NON-NLS-1$

    /**
     * The BuildLoggingConfiguration unique instance.
     */
    private static final BuildLoggingConfiguration instance = new BuildLoggingConfiguration();

    static {

        String configString = System.getProperty(BUILD_LOGGING_VM_ARG,
                Utils.EMPTY_STRING);

        Set<Configuration> set = Configuration.getEnumSet(configString);

        if (set.contains(Configuration.obfuscationOutput)) {
            instance.obfuscationOutputEnabled = true;
        }

        if (set.contains(Configuration.preverifierOutput)) {
            instance.preverifierOutputEnabled = true;
        }

        if (set.contains(Configuration.preprocessorTrace)) {
            instance.preprocessorTraceEnabled = true;
        }

        if (set.contains(Configuration.preverifierTrace)) {
            instance.preverifierTraceEnabled = true;
        }

        if (set.contains(Configuration.all)) {
            instance.obfuscationOutputEnabled = true;
            instance.preprocessorTraceEnabled = true;
            instance.preverifierOutputEnabled = true;
            instance.preverifierTraceEnabled = true;
        }
    }

    /**
     * Return the unique instance of BuildLoggingConfiguration.
     * 
     * @return BuildLoggingConfiguration unique instance.
     */
    public static synchronized BuildLoggingConfiguration getInstance() {
        return instance;
    }

    private boolean obfuscationOutputEnabled = false;
    private boolean preprocessorTraceEnabled = false;
    private boolean preverifierOutputEnabled = false;
    private boolean preverifierTraceEnabled = false;

    /**
     * Create the BuildLoggingConfiguration unique instance.
     */
    private BuildLoggingConfiguration() {
    }

    /**
     * Check if obfuscation outputting is enabled.
     * 
     * @return obfuscation outputting state.
     */
    public boolean isObfuscationOutputEnabled() {
        return obfuscationOutputEnabled;
    }

    /**
     * Check if preprocessing tracing is enabled.
     * 
     * @return preprocessing tracing state.
     */
    public boolean isPreprocessorTraceEnabled() {
        return preprocessorTraceEnabled;
    }

    /**
     * Check if preverification outputting is enabled.
     * 
     * @return preverification outputting state.
     */
    public boolean isPreverifierOutputEnabled() {
        return preverifierOutputEnabled;
    }

    /**
     * Check if preverification tracing is enabled.
     * 
     * @return preverification tracing state.
     */
    public boolean isPreverifierTraceEnabled() {
        return preverifierTraceEnabled;
    }
}
