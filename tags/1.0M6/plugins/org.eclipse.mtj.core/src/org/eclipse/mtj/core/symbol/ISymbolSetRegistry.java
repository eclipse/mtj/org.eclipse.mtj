/**
 * Copyright (c) 2008, 2009 Motorola Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gustavo de Paula (Motorola) - Initial implementation
 */
package org.eclipse.mtj.core.symbol;

import java.io.IOException;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface ISymbolSetRegistry extends IPersistable {
	
    /**
     * Add the specified SymbolDefinitions object to the registry of
     * definitions.
     * 
     * @param definitions
     * @throws PersistenceException
     * @throws IllegalStateException if the provided definition set has a
     *             <code>null</code> name.
     */
    public void addSymbolSet(ISymbolSet definitions)
            throws PersistenceException;
    
    /**
     * Add an array of symbol sets to the registry
     * 
     * @param ss symbol set array
     * @throws PersistenceException
     */
    public void addSymbolSet(List <ISymbolSet> ss)
            throws PersistenceException;

    /**
     * @param listener
     */
    public void addSymbolSetChangeListener(ISymbolSetChangeListener listener);
    
    /**
     * Clear all of the registered SymbolDefinitions objects.
     * 
     * @throws PersistenceException
     */
    public void clear() throws PersistenceException;

    /**
     * Return all of the definition names registered.
     * 
     * @return
     * @throws PersistenceException
     */
    public String[] getAllSymbolSetNames() throws PersistenceException;

    /**
     * Return all of the definitions registered.
     * 
     * @return
     * @throws PersistenceException
     */
    public ISymbolSet[] getAllSymbolSets()
            throws PersistenceException;

    /**
     * Return the SymbolDefinitions instance registered with the specified name
     * or <code>null</code> if the object cannot be found.
     * 
     * @param name
     * @return
     * @throws PersistenceException
     */
    public ISymbolSet getSymbolSet(String name)
            throws PersistenceException;

    /**
     * Remove the specified definition set from the registry. Does nothing if
     * the specified set cannot be found in the registry.
     * 
     * @param setName
     */
    public void removeSymbolSet(String setName);

    /**
     * @param listener
     */
    public void removeSymbolSetChangeListener(ISymbolSetChangeListener listener);
    
    /**
     * Load the contents of the symbol definitions registry from the storage
     * file in the plug-in state location.
     * 
     * @throws PersistenceException
     */
    public void load() throws PersistenceException;
    
    /**
     * Store out the contents of the registry into the standard device storage
     * file in the plug-in state location.
     * 
     * @throws PersistenceException
     * @throws TransformerException
     * @throws IOException
     */
    public void store() throws PersistenceException, TransformerException,
            IOException;    
}
