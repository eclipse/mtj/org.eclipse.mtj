/**
 * Copyright (c) 2004,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.project.midp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.project.midp.IApplicationDescriptor;
import org.eclipse.mtj.core.project.midp.IMidletDefinition;
import org.eclipse.mtj.internal.core.util.ColonDelimitedProperties;
import org.osgi.framework.Version;

/**
 * This class is a representation of a Java Application Descriptor (jad) for a
 * MIDlet Suite.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 * @author Craig Setera
 */
public class ApplicationDescriptor implements IApplicationDescriptor {

    /**
     * The definition of a MIDlet within the application descriptor.
     */
    public static class MidletDefinition implements IMidletDefinition {

        private String className;
        private String icon;
        private String name;
        private int number;

        /**
         * Construct a new MIDlet definition with the specified information.
         * 
         * @param name
         * @param icon
         * @param className
         */
        public MidletDefinition(int number, String name, String icon,
                String className) {
            super();
            this.number = number;
            this.name = name;
            this.icon = icon;
            this.className = className;
        }

        MidletDefinition(int number, String definitionString) {
            int fieldCount = 0;
            this.number = number;

            name = ""; //$NON-NLS-1$
            icon = ""; //$NON-NLS-1$
            className = ""; //$NON-NLS-1$

            StringTokenizer st = new StringTokenizer(definitionString, ",", //$NON-NLS-1$
                    true);
            while (st.hasMoreTokens()) {
                String token = st.nextToken();
                if (token.equals(",")) { //$NON-NLS-1$
                    fieldCount++;
                } else {
                    switch (fieldCount) {
                        case 0:
                            name = token;
                            break;
                        case 1:
                            icon = token;
                            break;
                        case 2:
                            className = token;
                            break;
                    }
                }
            }
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.project.midp.IMidletDefinition#getClassName()
         */
        public String getClassName() {
            return className;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.project.midp.IMidletDefinition#getIcon()
         */
        public String getIcon() {
            return icon;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.project.midp.IMidletDefinition#getName()
         */
        public String getName() {
            return name;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.project.midp.IMidletDefinition#getNumber()
         */
        public int getNumber() {
            return number;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.project.midp.IMidletDefinition#setClassName(java.lang.String)
         */
        public void setClassName(String className) {
            this.className = className;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.project.midp.IMidletDefinition#setIcon(java.lang.String)
         */
        public void setIcon(String icon) {
            this.icon = icon;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.project.midp.IMidletDefinition#setName(java.lang.String)
         */
        public void setName(String name) {
            this.name = name;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append(name).append(',');
            sb.append(icon).append(',');
            sb.append(className);

            return sb.toString();
        }
    }

    /**
     * The prefix of all MIDlet definition properties
     */
    public static final String MIDLET_PREFIX = "MIDlet-"; //$NON-NLS-1$

    private ColonDelimitedProperties manifestProperties;
    private List<IMidletDefinition> midletDefinitions;
    private File sourceFile;

    /**
     * Construct a new ApplicationDescriptor instance based on the data in the
     * specified file.
     * 
     * @param jadFile The file in which the descriptor is being held.
     * @throws IOException when an error occurs reading the file
     */
    public ApplicationDescriptor(File jadFile) throws IOException {
        sourceFile = jadFile;
        midletDefinitions = new ArrayList<IMidletDefinition>();
        parseDescriptor(jadFile);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IApplicationDescriptor#addMidletDefinition(org.eclipse.mtj.core.project.midp.IMidletDefinition)
     */
    public void addMidletDefinition(IMidletDefinition midletDefinition) {
        midletDefinitions.add(midletDefinition);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IApplicationDescriptor#getConfigurationSpecificationVersion()
     */
    public Version getConfigurationSpecificationVersion() throws CoreException {
        Version version = null;

        String configIdentifier = manifestProperties
                .getProperty(IJADConstants.JAD_MICROEDITION_CONFIG);

        // Attempt to parse the value
        if (configIdentifier != null) {
            String[] components = configIdentifier.split("-"); //$NON-NLS-1$
            if (components.length == 2) {
                version = new Version(components[1]);
            }
        }

        // Make sure we fall back to something reasonable
        if (version == null) {
            version = new Version("1.0"); //$NON-NLS-1$
        }

        return version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IApplicationDescriptor#getManifestProperties()
     */
    public ColonDelimitedProperties getManifestProperties() {
        return manifestProperties;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IApplicationDescriptor#getMidletCount()
     */
    public int getMidletCount() {
        return midletDefinitions.size();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IApplicationDescriptor#getMidletDefinitions()
     */
    public List<IMidletDefinition> getMidletDefinitions() {
        return midletDefinitions;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IApplicationDescriptor#getMidletJarURL()
     */
    public String getMidletJarURL() {
        String url = null;

        if (manifestProperties != null) {
            url = manifestProperties
                    .getProperty(IJADConstants.JAD_MIDLET_JAR_URL);
        }

        return url;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IApplicationDescriptor#store()
     */
    public void store() throws IOException {
        store(sourceFile);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IApplicationDescriptor#store(java.io.File)
     */
    public void store(File jadFile) throws IOException {
        // Copy the current properties and add the MIDlets
        ColonDelimitedProperties copy = copyProperties();
        Iterator<IMidletDefinition> iter = midletDefinitions.iterator();
        while (iter.hasNext()) {
            IMidletDefinition def = (IMidletDefinition) iter.next();
            String key = MIDLET_PREFIX + def.getNumber();
            String value = def.toString();
            copy.setProperty(key, value);
        }

        // Write out the resulting manifest properties
        FileOutputStream fos = new FileOutputStream(jadFile);
        try {
            copy.store(fos, "MIDlet Property Definitions"); //$NON-NLS-1$
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
            }
        }
    }

    /**
     * Copy the manifest properties.
     * 
     * @return the copy of the properties
     */
    private ColonDelimitedProperties copyProperties() {
        ColonDelimitedProperties copy = new ColonDelimitedProperties();

        Iterator<?> keys = manifestProperties.keySet().iterator();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            String value = manifestProperties.getProperty(key);
            copy.setProperty(key, value);
        }

        return copy;
    }

    /**
     * Parse the application descriptor file and store the information into this
     * instance.
     * 
     * @param jadFile the file to be parsed
     * @throws IOException
     */
    private void parseDescriptor(File jadFile) throws IOException {
        manifestProperties = new ColonDelimitedProperties();

        if (jadFile.exists()) {
            // Parse as a set of properties
            FileInputStream fis = new FileInputStream(jadFile);
            manifestProperties.load(fis);

            // Pull out the MIDlet definitions
            parseMidletDefinitions();
        }
    }

    /**
     * Parse out the MIDlet definitions from the manifest properties.
     */
    private void parseMidletDefinitions() {
        ArrayList<String> keysToRemove = new ArrayList<String>(
                manifestProperties.size());

        Iterator<?> iter = manifestProperties.keySet().iterator();
        while (iter.hasNext()) {
            String key = (String) iter.next();
            if (key.startsWith(MIDLET_PREFIX)) {
                int midletNumber = -1;
                try {
                    midletNumber = Integer.parseInt(key.substring(MIDLET_PREFIX
                            .length()));
                } catch (NumberFormatException e) {
                }

                if (midletNumber != -1) {
                    String value = manifestProperties.getProperty(key);
                    midletDefinitions.add(new MidletDefinition(midletNumber,
                            value));

                    // Mark for removal from the properties
                    keysToRemove.add(key);
                }
            }
        }

        // Remove the MIDlets
        Iterator<String> keyIter = keysToRemove.iterator();
        while (keyIter.hasNext()) {
            String key = (String) keyIter.next();
            manifestProperties.remove(key);
        }
    }
}
