/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 */

package org.eclipse.mtj.internal.core.launching.midp;

import java.io.File;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.IPath;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.project.IMTJProject;

/**
 * This class contains some utilities related with emulator launching.
 */
public class LaunchingUtils {

    /**
     * Get the project emulation folder. This folder is located at
     * /%PROJECT_HOME%/.mtj.tmp/emulation
     * 
     * @param suite
     * @return
     */
    public static IFolder getEmulationFolder(IMTJProject suite) {
        IFolder projectTempFolder = getProjectTempFolder(suite);
        IFolder emulationFolder = projectTempFolder
                .getFolder(IMTJCoreConstants.EMULATION_FOLDER_NAME);
        return emulationFolder;
    }

    /**
     * Return the path contains JAD and JAR for launching from JAD emulation.
     * 
     * @param suite
     * @return - IPath. The IPath stand for the folder contains JAD and JAR.
     *         This IPath contains the absolute path of the folder.
     */
    public static IPath getJadLaunchBasePath(IMTJProject suite) {
        IFolder tempFolder = getProjectTempFolder(suite);
        IFolder jadLaunchBaseFolder = tempFolder
                .getFolder(IMTJCoreConstants.LAUNCH_FROM_JAD_FOLDER);
        IPath jadLaunchBasePath = jadLaunchBaseFolder.getLocation();
        return jadLaunchBasePath;
    }

    /**
     * Get the project temporary folder. This folder is located at
     * /%PROJECT_HOME%/.mtj.tmp
     * 
     * @param suite
     * @return
     */
    public static IFolder getProjectTempFolder(IMTJProject suite) {
        IFolder projectTempFolder = suite.getProject().getFolder(
                IMTJCoreConstants.TEMP_FOLDER_NAME);
        return projectTempFolder;
    }

    /**
     * Create the folder which contains JAD and JAR for launching from JAD
     * emulation.
     * 
     * @param suite
     * @return - The folder act as emulation base folder. The folder locate at
     *         %PROJECT_HOME%/%DEPLOYED_FOLDER%/launchFromJAD. PROJECT_HOME is
     *         the MIDlet project's base path; DEPLOYED_FOLDER is the MIDlet
     *         project's deploy folder, the default deploy folder is 'deployed'
     */
    public static File makeJadLaunchBaseDir(IMTJProject suite) {
        IPath jadLaunchBasePath = getJadLaunchBasePath(suite);
        File jadLaunchBaseDir = jadLaunchBasePath.toFile();
        jadLaunchBaseDir.mkdirs();
        return jadLaunchBaseDir;
    }

}
