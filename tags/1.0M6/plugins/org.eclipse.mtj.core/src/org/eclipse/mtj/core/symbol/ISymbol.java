/**
 * Copyright (c) 2008, 2009 Motorola Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gustavo de Paula (Motorola) - Initial implementation
 */
package org.eclipse.mtj.core.symbol;

import org.eclipse.mtj.core.persistence.IPersistable;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface ISymbol extends IPersistable {

	/**
	 * 
	 */
	public static final int TYPE_ABILITY = 0;
	/**
	 * Project Configuration
	 */
	public static final int TYPE_CONFIG = 1;

	/**
	 * If name equals, then symbol equals. {@inheritDoc}
	 */
	public abstract boolean equals(Object obj);

	/**
	 * @return
	 */
	public abstract String getName();

	/**
	 * Get value in safe format for preprocessor.
	 * 
	 * @return
	 */
	public abstract String getSafeValue();

	/**
	 * @return
	 */
	public abstract int getType();

	/**
	 * @return
	 */
	public abstract String getValue();

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public abstract int hashCode();

	/**
	 * @param identifier
	 */
	public abstract void setName(String identifier);

	/**
	 * @param type
	 */
	public abstract void setType(int type);

	/**
	 * @param value
	 */
	public abstract void setValue(String value);

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public abstract String toString();

}