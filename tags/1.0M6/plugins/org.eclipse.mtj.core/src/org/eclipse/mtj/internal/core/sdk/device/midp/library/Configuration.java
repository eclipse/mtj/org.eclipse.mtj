/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Gorkem Ercan (Nokia) - enum definition mixed with profile
 */
package org.eclipse.mtj.internal.core.sdk.device.midp.library;

import org.eclipse.mtj.core.sdk.device.midp.library.ILibrarySpecification;
import org.osgi.framework.Version;

/**
 * @author Diego Madruga Sandin
 */
public enum Configuration implements ILibrarySpecification {

    /**
     * Connected Limited Device Configuration (1.0)
     */
    CLDC_10("CLDC-1.0", "Connected Limited Device Configuration (1.0)", //$NON-NLS-1$//$NON-NLS-2$
            new Version("1.0")), //$NON-NLS-1$

    /**
     * Connected Limited Device Configuration (1.1)
     */
    CLDC_11("CLDC-1.1", "Connected Limited Device Configuration (1.1)",  //$NON-NLS-1$//$NON-NLS-2$
            new Version("1.1")); //$NON-NLS-1$

    private String identifier;
    private String name;
    private Version version;

    /**
     * @param identifier the profile identifier
     * @param name the profile name
     * @param version the profile version
     */
    Configuration(String identifier, String name, Version version) {
        this.identifier = identifier;
        this.name = name;
        this.version = version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrarySpecification#getIdentifier()
     */
    public String getIdentifier() {
        return identifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrarySpecification#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrarySpecification#getVersion()
     */
    public Version getVersion() {
        return version;
    }
}
