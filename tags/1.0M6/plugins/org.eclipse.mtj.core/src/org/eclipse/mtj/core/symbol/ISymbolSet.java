/**
 * Copyright (c) 2008, 2009 Motorola Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gustavo de Paula (Motorola) - Initial implementation
 */
package org.eclipse.mtj.core.symbol;

import java.util.Collection;
import java.util.Map;

import org.eclipse.mtj.core.persistence.IPersistable;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface ISymbolSet extends IPersistable {

    /**
     * Define the specified identifier.
     * 
     * @param identifier
     */
    public ISymbol add(String identifier);

    /**
     * Define the specified identifier to be the specified value.
     * 
     * @param identifier
     * @param value
     */
    public ISymbol add(String identifier, String value);

    /**
     * Define the specified identifier to be the specified value.
     * 
     * @param identifier
     * @param value
     */
    public ISymbol add(String identifier, String value, int type);
    
    /**
     * Add a collection of symbols to this symbol set
     * 
     * @param symbols to be added
     */
    public void add(Collection<ISymbol> c);    
    
    /**
     * Add one specific symbol to this symbol set
     * 
     * @param symbol to be added
     */
    public void add(ISymbol s);
    
    /**
     * Return a boolean indicating whether the specified SymbolDefinitions
     * object is equal to this object.
     * 
     * @param definitions
     * @return
     */
    public boolean equals(ISymbolSet definitions);

    /**
     * Return the symbols that are defined as true in the list of symbol
     * definitions. The list of symbols returned by this method does not include
     * symbols from parent symbol definition sets.
     * 
     * @return
     */
    public Collection<ISymbol> getSymbols();

    /**
     * Return the name of this set of symbol definitions.
     * 
     * @return the name
     */
    public String getName();

    /**
     * Return the (possibly null) value of the specified identifier.
     * 
     * @param identifier
     * @return
     */
    public String getSymbolValue(String identifier);

    /**
     * Return a boolean indicating whether the specified identifier is defined.
     * 
     * @param identifier
     * @return
     */
    public boolean contains(String identifier);

    /**
     * Set the definitions set to contain the specified definitions.
     * 
     * @param definitions
     */
    public void setSymbols(Map<String, String> definitions);

    /**
     * Set the name of this set of symbol definitions.
     * 
     * @param name the name to set
     */
    public void setName(String name);

    /**
     * @see java.lang.Object#toString()
     */
    public String getSymbolSetString();

    /**
     * Undefine the specified identifier.
     * 
     * @param identifier
     */
    public void remove(String identifier);

    /**
     * Remove a list of symbols
     * 
     * @param symbols to be removed
     */
    public void remove(Collection<ISymbol> c);    
    
    /**
     * Returns the number of symbols on this symbol set
     * @return number of symbols on this symbol set
     */
    public int size ();
	
    /**
     * Retusn as array of all symbols on this symbol set
     * 
     * @param a array where the elements will be stores
     * @return array with all symbols
     */
    public <T> T[] toArray (T[] a);
}
