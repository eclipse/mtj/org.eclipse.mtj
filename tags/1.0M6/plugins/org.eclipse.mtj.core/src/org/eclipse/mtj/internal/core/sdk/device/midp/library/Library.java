/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.sdk.device.midp.library;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.jdt.core.IClasspathAttribute;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.midp.library.ILibrary;
import org.eclipse.mtj.core.sdk.device.midp.library.api.API;
import org.eclipse.mtj.core.sdk.device.midp.library.api.APIType;

/**
 * A library is a wrapper around a classpath item that attempts to provide
 * further metadata about that library.
 * 
 * @author Craig Setera
 */
public class Library implements ILibrary {

    private static IAccessRule[] NO_ACCESS_RULES = new IAccessRule[0];

    private API[] apis;
    private URL javadocURL;
    private File libraryFile;
    private IPath sourceAttachmentPath;
    private IPath sourceAttachmentRootPath;
    private IAccessRule[] accessRules;

    /**
     * Construct a new library.
     */
    public Library() {
        super();
        accessRules = NO_ACCESS_RULES;
    }

    /**
     * Creates a new instance of Library.
     * 
     * @param apis
     * @param javadocURL
     * @param libraryFile
     * @param sourceAttachmentPath
     * @param sourceAttachmentRootPath
     * @param accessRules
     */
    public Library(API[] apis, URL javadocURL, File libraryFile,
            IPath sourceAttachmentPath, IPath sourceAttachmentRootPath,
            IAccessRule[] accessRules) {
        super();
        this.apis = apis;
        this.javadocURL = javadocURL;
        this.libraryFile = libraryFile;
        this.sourceAttachmentPath = sourceAttachmentPath;
        this.sourceAttachmentRootPath = sourceAttachmentRootPath;
        this.accessRules = accessRules;
    }

    /**
     * Test the equality of this library as compared to the specified library.
     * 
     * @param library
     * @return
     */
    public boolean equals(Library library) {
        return libraryFile.equals(library.libraryFile);
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        boolean equals = false;

        if (obj instanceof Library) {
            equals = equals((Library) obj);
        }

        return equals;
    }

    /**
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrary#getAPI(int)
     */
    public API getAPI(APIType apiType) {
        API api = null;

        for (int i = 0; i < apis.length; i++) {
            if (apis[i].getType() == apiType) {
                api = apis[i];
                break;
            }
        }

        return api;
    }

    /**
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrary#getAPIs()
     */
    public API[] getAPIs() {
        return apis;
    }

    /**
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrary#getConfiguration()
     */
    public API getConfiguration() {
        return getAPI(APIType.CONFIGURATION);
    }

    /**
     * @return Returns the javadocURL.
     */
    public URL getJavadocURL() {
        return javadocURL;
    }

    /**
     * Return the file that makes up this library.
     * 
     * @return Returns the libraryFile.
     */
    public File getLibraryFile() {
        return libraryFile;
    }

    /**
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrary#getProfile()
     */
    public API getProfile() {
        return getAPI(APIType.PROFILE);
    }

    /**
     * @return Returns the sourceAttachmentPath.
     */
    public IPath getSourceAttachmentPath() {
        return sourceAttachmentPath;
    }

    /**
     * @return Returns the sourceAttachmentRootPath.
     */
    public IPath getSourceAttachmentRootPath() {
        return sourceAttachmentRootPath;
    }

    /**
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrary#hasConfiguration()
     */
    public boolean hasConfiguration() {
        return (getConfiguration() != null);
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return libraryFile.hashCode();
    }

    /**
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrary#hasProfile()
     */
    public boolean hasProfile() {
        return (getProfile() != null);
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        libraryFile = new File(persistenceProvider.loadString("file"));

        int apiLength = persistenceProvider.loadInteger("apiCount");
        apis = new API[apiLength];
        for (int i = 0; i < apis.length; i++) {
            apis[i] = (API) persistenceProvider.loadPersistable("entry" + i);
        }

        String javadocURLString = persistenceProvider.loadString("javadocURL");
        if (javadocURLString != null) {
            try {
                javadocURL = new URL(javadocURLString);
            } catch (MalformedURLException e) {
                MTJCore.log(IStatus.WARNING, "Error loading javadoc url "
                        + javadocURLString, e);
            }
        }

        String sourceAttachString = persistenceProvider
                .loadString("sourceAttachmentPath");
        if (sourceAttachString != null) {
            sourceAttachmentPath = new Path(sourceAttachString);
        }

        String rootPathString = persistenceProvider
                .loadString("sourceAttachmentRootPath");
        if (rootPathString != null) {
            sourceAttachmentRootPath = new Path(rootPathString);
        }
    }

    /**
     * Set the access rules for this library.
     * 
     * @param accessRules
     */
    public void setAccessRules(IAccessRule[] accessRules) {
        this.accessRules = accessRules;
    }

    /**
     * @param api The apis to set.
     */
    public void setApis(API[] apis) {
        this.apis = apis;
    }

    /**
     * @param javadocURL The javadocURL to set.
     */
    public void setJavadocURL(URL javadocURL) {
        this.javadocURL = javadocURL;
    }

    /**
     * Sets the file that makes up this library.
     * 
     * @param libraryFile The libraryFile to set.
     */
    public void setLibraryFile(File libraryFile) {
        this.libraryFile = libraryFile;
    }

    /**
     * @param sourceAttachmentPath The sourceAttachmentPath to set.
     */
    public void setSourceAttachmentPath(IPath sourceAttachmentPath) {
        this.sourceAttachmentPath = sourceAttachmentPath;
    }

    /**
     * @param sourceAttachmentRootPath The sourceAttachmentRootPath to set.
     */
    public void setSourceAttachmentRootPath(IPath sourceAttachmentRootPath) {
        this.sourceAttachmentRootPath = sourceAttachmentRootPath;
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        persistenceProvider.storeString("file", libraryFile.toString());
        persistenceProvider.storeInteger("apiCount", apis.length);
        for (int i = 0; i < apis.length; i++) {
            persistenceProvider.storePersistable("entry" + i, apis[i]);
        }

        if (javadocURL != null) {
            persistenceProvider
                    .storeString("javadocURL", javadocURL.toString());
        }

        if (sourceAttachmentPath != null) {
            persistenceProvider.storeString("sourceAttachmentPath",
                    sourceAttachmentPath.toString());
        }

        if (sourceAttachmentRootPath != null) {
            persistenceProvider.storeString("sourceAttachmentRootPath",
                    sourceAttachmentRootPath.toString());
        }
    }

    /**
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrary#toClasspathEntry()
     */
    public IClasspathEntry toClasspathEntry() {

        IPath entryPath = new Path(libraryFile.toString());
        IClasspathAttribute[] attributes = getClasspathAttributes();

        return JavaCore.newLibraryEntry(entryPath, sourceAttachmentPath,
                sourceAttachmentRootPath,
                (accessRules == null) ? NO_ACCESS_RULES : accessRules,
                attributes, false);
    }

    /**
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrary#toFile()
     */
    public File toFile() {
        return libraryFile;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return libraryFile.toString();
    }

    /**
     * @see org.eclipse.mtj.core.sdk.device.midp.library.ILibrary#toURL()
     */
    public URL toURL() {
        URL url = null;

        try {
            url = libraryFile.toURI().toURL();
        } catch (MalformedURLException e) {
            // This should not happen
            MTJCore.log(IStatus.ERROR, e);
        }

        return url;
    }

    /**
     * Return the classpath attributes to be used in constructing the
     * IClasspathEntry.
     * 
     * @return
     */
    private IClasspathAttribute[] getClasspathAttributes() {
        ArrayList<IClasspathAttribute> attributes = new ArrayList<IClasspathAttribute>();

        IClasspathAttribute javadocAttribute = getJavadocAttribute();
        if (javadocAttribute != null)
            attributes.add(javadocAttribute);

        return (IClasspathAttribute[]) attributes
                .toArray(new IClasspathAttribute[attributes.size()]);
    }

    /**
     * Return a classpath attribute specifying the javadoc attachment.
     * 
     * @return
     */
    private IClasspathAttribute getJavadocAttribute() {
        IClasspathAttribute attribute = null;

        if (javadocURL != null) {
            attribute = JavaCore.newClasspathAttribute(
                    IClasspathAttribute.JAVADOC_LOCATION_ATTRIBUTE_NAME,
                    javadocURL.toString());
        }

        return attribute;
    }
}
