/**
 * Copyright (c) 2008,2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)          - Initial implementation
 *     Gustavo de Paula (Motorola) - Configuration refactory [Bug 265522]
 */
package org.eclipse.mtj.core.project.runtime.event;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;

/**
 * The instance of this interface observe {@link MTJRuntimeList} change.
 * 
 * @since 1.0
 */
public interface IMTJRuntimeListChangeListener {

    /**
     * The currently active {@link MTJRuntimeList} has changed.
     * 
     * @param event
     */
    void activeMTJRuntimeSwitched(SwitchActiveMTJRuntimeEvent event);

    /**
     * A {@link MTJRuntime} was added to the {@link MTJRuntimeList}.
     * 
     * @param event
     */
    void mtjRuntimeAdded(AddMTJRuntimeEvent event);

    /**
     * A {@link MTJRuntime} was removed from the {@link MTJRuntimeList}.
     * 
     * @param event
     */
    void mtjRuntimeRemoved(RemoveMTJRuntimeEvent event);
}
