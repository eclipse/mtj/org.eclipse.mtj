/**
 * Copyright (c) 2008 Hugo Raniere.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola)  - Initial implementation
 */
package org.eclipse.mtj.internal.core.project.midp;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.mtj.core.sdk.device.IDevice;

/**
 * A JavaMEClasspathContainer references a set of libraries associated with an
 * specific JavaME Device.
 * 
 * @author Hugo Raniere
 * @since MTJ 0.9
 * @see org.eclipse.jdt.core.IClasspathContainer
 */
public class JavaMEClasspathContainer implements IClasspathContainer {

    /**
     * Represents the first segment of a JavaMEClasspathContainer path.
     */
    public static final String JAVAME_CONTAINER = "org.elipse.mtj.JavaMEContainer"; //$NON-NLS-1$

    private final IClasspathEntry[] classpathEntries;

    private final IPath path;

    private final String description;

    /**
     * Constructs a new JavaMEClasspathContainer instance associated with an
     * specific device.
     * 
     * @param containerPath the container path identifying this container
     * @param device the device whose libs are contained by this container
     */
    public JavaMEClasspathContainer(IPath containerPath, IDevice device) {
        this.path = containerPath;

        if (device != null) {
            this.classpathEntries = device.getClasspath().asClasspathEntries();
            this.description = "JavaME library [" + device + "]"; //$NON-NLS-1$ //$NON-NLS-2$
        } else {
            this.classpathEntries = new IClasspathEntry[0];
            this.description = "JavaME library [Unspecified Device]"; //$NON-NLS-1$
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jdt.core.IClasspathContainer#getClasspathEntries()
     */
    public IClasspathEntry[] getClasspathEntries() {
        return classpathEntries;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jdt.core.IClasspathContainer#getDescription()
     */
    public String getDescription() {
        return description;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jdt.core.IClasspathContainer#getKind()
     */
    public int getKind() {
        return K_APPLICATION;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jdt.core.IClasspathContainer#getPath()
     */
    public IPath getPath() {
        return path;
    }

}
