/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID
 */
package org.eclipse.mtj.core.persistence;

/**
 * An exception that may occur during the process of persisting platform
 * components.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public class PersistenceException extends Exception {

    /**
     * Default serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new PersistenceException with <code>null</code> as its
     * detail message.
     */
    public PersistenceException() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    public PersistenceException(String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public PersistenceException(Throwable cause) {
        super(cause);
    }

    /**
     * {@inheritDoc}
     */
    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }
}
