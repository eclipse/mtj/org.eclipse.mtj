/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Gustavo de Paula (Motorola)  - Refactor preverification interfaces
 */
package org.eclipse.mtj.internal.core.build.preverifier;

/**
 * @noimplement This interface is not intended to be implemented by clients.
 *
 */
public interface IPreverificationErrorLocation {

    /**
     * Return information about the class in which the error occurred or
     * <code>null</code> if there is not class information.
     * 
     * @return Returns the class information.
     */
    public abstract IClassErrorInformation getClassInformation();

    /**
     * @return Returns the lineNumber.
     */
    public abstract int getLineNumber();

    /**
     * Return information about the type of the location in which the error
     * occurred or <code>null</code> if there is not location type
     * information.
     * 
     * @return Returns the locationType.
     */
    public abstract PreverificationErrorLocationType getLocationType();
}