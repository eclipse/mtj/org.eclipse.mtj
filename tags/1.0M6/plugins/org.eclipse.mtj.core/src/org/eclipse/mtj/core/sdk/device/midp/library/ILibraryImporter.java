/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk.device.midp.library;

import java.io.File;

import org.eclipse.jdt.core.IAccessRule;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface ILibraryImporter {

	/**
	 * Constant that represents the UEI library importer
	 */
	public static final String LIBRARY_IMPORTER_UEI="org.eclipse.mtj.libraryimporter.uei";
	
    /**
     * Construct and return a new Library instance for the specified library
     * file.
     * 
     * @param libraryFile the file to be wrapped up as a library.
     * @return the newly created library wrapper
     */
    public abstract ILibrary createLibraryFor(File libraryFile);

    /**
     * Construct and return a new Library instance for the specified library
     * file with the provided access rules.
     * 
     * @param libraryFile
     * @param accessRules
     * @return
     */
    public abstract ILibrary createLibraryFor(File libraryFile,
            IAccessRule[] accessRules);
}