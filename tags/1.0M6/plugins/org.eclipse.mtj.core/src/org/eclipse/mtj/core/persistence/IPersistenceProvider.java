/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.persistence;

import java.util.Properties;

/**
 * Instances of this interface provide facilities for storing and retrieving
 * persistable objects.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @since 1.0
 */
public interface IPersistenceProvider {

    /**
     * Load the named boolean value.
     * 
     * @param name
     * @return
     * @throws PersistenceException
     */
    public boolean loadBoolean(String name) throws PersistenceException;

    /**
     * Load the named integer value.
     * 
     * @param name
     * @return
     * @throws PersistenceException
     */
    public int loadInteger(String name) throws PersistenceException;

    /**
     * Load the named persistable object value.
     * 
     * @param name
     * @return
     * @throws PersistenceException
     */
    public IPersistable loadPersistable(String name)
            throws PersistenceException;

    /**
     * Load the named properties object value.
     * 
     * @param name
     * @return
     * @throws PersistenceException
     */
    public Properties loadProperties(String name) throws PersistenceException;

    /**
     * Load the referenced object value or <code>null</code> if the object
     * cannot be resolved.
     * 
     * @param name
     * @return
     * @throws PersistenceException
     */
    public Object loadReference(String name) throws PersistenceException;

    /**
     * Load the named string value.
     * 
     * @param name
     * @return
     * @throws PersistenceException
     */
    public String loadString(String name) throws PersistenceException;

    /**
     * Store the boolean value using the specified name.
     * 
     * @param name
     * @param debugServer
     * @throws PersistenceException
     */
    public void storeBoolean(String name, boolean debugServer)
            throws PersistenceException;

    /**
     * Store the integer value using the specified name.
     * 
     * @param name
     * @param value
     * @throws PersistenceException
     */
    public void storeInteger(String name, int value)
            throws PersistenceException;

    /**
     * Store the persistable object value using the specified name.
     * 
     * @param name
     * @param value
     * @throws PersistenceException
     */
    public void storePersistable(String name, IPersistable value)
            throws PersistenceException;

    /**
     * Store the properties object value using the specified name.
     * 
     * @param name
     * @param deviceProperties
     * @throws PersistenceException
     */
    public void storeProperties(String name, Properties deviceProperties)
            throws PersistenceException;

    /**
     * Store a reference to the specified object using the specified name. This
     * object must have previously been stored by this persistence provider or a
     * persistence exception will be thrown.
     * 
     * @param name
     * @param referenceObject
     * @throws PersistenceException
     */
    public void storeReference(String name, Object referenceObject)
            throws PersistenceException;

    /**
     * Store the string value using the specified name.
     * 
     * @param name
     * @param string
     * @throws PersistenceException
     */
    public void storeString(String name, String string)
            throws PersistenceException;
}
