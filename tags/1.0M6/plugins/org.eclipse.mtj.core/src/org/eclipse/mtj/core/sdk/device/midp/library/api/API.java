/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.sdk.device.midp.library.api;

import java.net.URL;

import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Version;

/**
 * Instances of this class represent the API provided by an ILibrary instance.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 */
public class API implements IPersistable {

    private String identifier;
    private String name;
    private String primaryMatchableClass;
    private boolean registered;
    private URL[] skeletonReplacements;
    private APIType type;
    private Version version;

    /**
     * Construct a new API instance
     */
    public API() {
        super();
        type = APIType.UNKNOWN;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof API) && equalsAPI((API) obj);
    }

    /**
     * @return Returns the identifier.
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Return the fully-qualifed class name of the primary class for this API
     * that can be used for matching.
     * 
     * @return
     */
    public String getPrimaryMatchableClass() {
        return primaryMatchableClass;
    }

    /**
     * Return the replacement files that are useful for preverification and
     * devices such as MPowerPlayer and microemu. These are likely
     * skeleton/empty API implementation jars that can be safely run through a
     * preverifier. May return <code>null</code> if there are not replacements
     * for this particular API.
     * 
     * @return
     */
    public URL[] getSkeletonReplacements() {
        return skeletonReplacements;
    }

    /**
     * @return Returns the type.
     */
    public APIType getType() {
        return type;
    }

    /**
     * @return Returns the version.
     */
    public Version getVersion() {
        return version;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return identifier.hashCode() ^ type.hashCode() ^ version.hashCode();
    }

    /**
     * Return a boolean indicating whether this API is a configuration library.
     * 
     * @return
     */
    public boolean isConfiguration() {
        return (type == APIType.CONFIGURATION);
    }

    /**
     * Return a boolean indicating whether this API is a profile library.
     * 
     * @return
     */
    public boolean isProfile() {
        return (type == APIType.PROFILE);
    }

    /**
     * @return
     */
    public boolean isRegistered() {
        return registered;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        identifier = persistenceProvider.loadString("identifier"); //$NON-NLS-1$
        version = new Version(persistenceProvider.loadString("version")); //$NON-NLS-1$
        registered = persistenceProvider.loadBoolean("registered"); //$NON-NLS-1$
        name = persistenceProvider.loadString("name"); //$NON-NLS-1$
        int typeCode = persistenceProvider.loadInteger("type"); //$NON-NLS-1$
        type = APIType.typeForCode(typeCode);
    }

    /**
     * @param identifier The identifier to set.
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Set the fully-qualified class name of the primary class for this API that
     * can be used for matching.
     * 
     * @param primaryMatchableClass
     */
    public void setPrimaryMatchableClass(String primaryMatchableClass) {
        this.primaryMatchableClass = primaryMatchableClass;
    }

    /**
     * @param registered
     */
    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    /**
     * Set the replacement files that are useful for preverification. These are
     * likely skeleton/empty API implementation jars that can be safely run
     * through a preverifier.
     * 
     * @param verifiableReplacements
     */
    public void setSkeletonReplacements(URL[] verifiableReplacements) {
        this.skeletonReplacements = verifiableReplacements;
    }

    /**
     * @param type The type to set.
     */
    public void setType(APIType type) {
        this.type = type;
    }

    /**
     * @param version The version to set.
     */
    public void setVersion(Version version) {
        this.version = version;
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        persistenceProvider.storeString("identifier", identifier); //$NON-NLS-1$
        persistenceProvider.storeString("name", name); //$NON-NLS-1$
        persistenceProvider.storeInteger("type", type.getTypeCode()); //$NON-NLS-1$
        persistenceProvider.storeString("version", NLS.bind("{0}.{1}", //$NON-NLS-1$ //$NON-NLS-2$
                new Object[] { version.getMajor(), version.getMinor() }));
        persistenceProvider.storeBoolean("registered", registered); //$NON-NLS-1$
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(identifier);
        sb.append("-").append( //$NON-NLS-1$
                NLS.bind("{0}.{1}", new Object[] { version.getMajor(), //$NON-NLS-1$
                        version.getMinor() }));

        return sb.toString();
    }

    /**
     * Return a boolean indicating whether the specified API is the same as this
     * API.
     * 
     * @param api
     * @return
     */
    private boolean equalsAPI(API api) {
        return identifier.equals(api.identifier) && type.equals(api.type)
                && version.equals(api.version);
    }
}
