/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.sdk.device.midp.library;

import java.io.File;
import java.net.URL;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.sdk.device.midp.library.api.API;
import org.eclipse.mtj.core.sdk.device.midp.library.api.APIType;

/**
 * Represents a library in the device's classpath. A library is capable of
 * providing further metadata about that specified jar file.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 *
 * @since 1.0 
 */
public interface ILibrary extends IPersistable {

    /**
     * Return the APIs associated with this library instance.
     * 
     * @return
     */
    public API[] getAPIs();

    /**
     * Return the first API with a type matching the specified API type or
     * <code>null</code> if none can be found.
     * 
     * @param apiType
     * @return
     */
    public API getAPI(APIType apiType);

    /**
     * Return the configuration API or <code>null</code> if this library does
     * not provide a configuration.
     * 
     * @return
     */
    public API getConfiguration();

    /**
     * Return the profile API or <code>null</code> if this library does not
     * provide a configuration.
     * 
     * @return
     */
    public API getProfile();

    /**
     * Return a boolean indicating whether this library contains a
     * configuration.
     * 
     * @return
     */
    public boolean hasConfiguration();

    /**
     * Return a boolean indicating whether this library contains a profile.
     * 
     * @return
     */
    public boolean hasProfile();

    /**
     * Return the library as an instance of IClasspathEntry.
     * 
     * @return
     */
    public IClasspathEntry toClasspathEntry();

    /**
     * Return the library as an instance of java.io.File.
     * 
     * @return
     */
    public File toFile();

    /**
     * Return the library as an instance of java.net.URL.
     * 
     * @return
     */
    public URL toURL();

    /**
     * Set the access rules for this library.
     * 
     * @param accessRules
     */
    public void setAccessRules(IAccessRule[] accessRules);

    /**
     * @param api The apis to set.
     */
    public void setApis(API[] apis);

    /**
     * @param javadocURL The javadocURL to set.
     */
    public void setJavadocURL(URL javadocURL);

    /**
     * Sets the file that makes up this library.
     * 
     * @param libraryFile The libraryFile to set.
     */
    public void setLibraryFile(File libraryFile);

    /**
     * @param sourceAttachmentPath The sourceAttachmentPath to set.
     */
    public void setSourceAttachmentPath(IPath sourceAttachmentPath);

    /**
     * @param sourceAttachmentRootPath The sourceAttachmentRootPath to set.
     */
    public void setSourceAttachmentRootPath(IPath sourceAttachmentRootPath);
}
