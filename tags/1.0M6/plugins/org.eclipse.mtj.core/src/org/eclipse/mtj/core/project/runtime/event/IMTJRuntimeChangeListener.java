/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)          - Initial implementation
 *     Gustavo de Paula (Motorola) - Configuration refactory [Bug 265522]
 */
package org.eclipse.mtj.core.project.runtime.event;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;

/**
 * The instance of this interface observe {@link MTJRuntime} properties change.
 * 
 * @since 1.0
 */
public interface IMTJRuntimeChangeListener {

    /**
     * @param event
     */
    void deviceChanged(MTJRuntimeDeviceChangeEvent event);

    /**
     * @param event
     */
    void nameChanged(MTJRuntimeNameChangeEvent event);

    /**
     * 
     */
    void symbolSetChanged();

    /**
     * @param event
     */
    void workspaceScopeSymbolSetsChanged(
            MTJRuntimeWorkspaceSymbolSetsChangeEvent event);
}
