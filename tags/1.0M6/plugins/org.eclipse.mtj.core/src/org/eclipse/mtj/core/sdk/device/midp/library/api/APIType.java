/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.sdk.device.midp.library.api;

/**
 * An enumeration type that represents the type of an API.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 *
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 */
public class APIType {

    /**
     * Static constant representing a API that acts as a configuration
     */
    public static final int CONFIGURATION_CODE = 1;

    /**
     * An API that acts as a configuration
     */
    public static final APIType CONFIGURATION = new APIType(CONFIGURATION_CODE);

    /** Static constant representing an API that acts as a profile */
    public static final int PROFILE_CODE = 2;

    /**
     * An API that acts as a profile
     */
    public static final APIType PROFILE = new APIType(PROFILE_CODE);

    /**
     * Static constant representing an API that offers optional functionality
     */
    public static final int OPTIONAL_CODE = 3;

    /**
     * An API that is optional
     */
    public static final APIType OPTIONAL = new APIType(OPTIONAL_CODE);

    /**
     * Static constant representing an API that is of an unknown type
     */
    public static final int UNKNOWN_CODE = 0;

    /**
     * An API that is an unknown type
     */
    public static final APIType UNKNOWN = new APIType(UNKNOWN_CODE);

    // Strings values for the various types
    public static final String[] TYPE_STRINGS = new String[] { "Unknown", //$NON-NLS-1$
            "Configuration", "Profile", "Optional", }; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

    // The type instances in order
    private static final APIType[] types = new APIType[] { UNKNOWN,
            CONFIGURATION, PROFILE, OPTIONAL, };

    /**
     * Return the type associated with the specified code.
     * 
     * @param typeCode
     * @return
     */
    public static APIType typeForCode(int typeCode) {
        APIType type = UNKNOWN;

        if ((typeCode >= 1) && (typeCode <= 3)) {
            type = types[typeCode];
        }

        return type;
    }

    private int typeCode;

    private APIType(int typeCode) {
        this.typeCode = typeCode;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return (this == obj);
    }

    /**
     * Return the type code for this particular type.
     * 
     * @return
     */
    public int getTypeCode() {
        return typeCode;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return typeCode;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return TYPE_STRINGS[typeCode];
    }
}
