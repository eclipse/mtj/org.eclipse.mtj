/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.util.migration;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.sdk.device.IDeviceRegistryConstants;
import org.eclipse.mtj.internal.core.util.xml.DocumentAdapter;
import org.eclipse.mtj.internal.core.util.xml.DocumentVisitor;
import org.osgi.framework.Version;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Diego Madruga Sandin
 */
public class DeviceRegistryMigration extends AbstractMigration {

    /**
     * @author Diego Madruga Sandin
     */
    private final class ConversionDocumentVisitor implements DocumentVisitor {

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.core.util.xml.DocumentVisitor#visitElement(org.w3c.dom.Element)
         */
        public void visitElement(Element element) {

            String name = element.getNodeName();

            if (name.startsWith(IDeviceRegistryConstants.ELEMENT_DEVICE)
                    && !name
                            .equals(IDeviceRegistryConstants.ELEMENT_DEVICEREGISTRY)
                    && !name
                            .equals(IDeviceRegistryConstants.ELEMENT_DEVICECOUNT)
                    && !name
                            .equals(IDeviceRegistryConstants.ELEMENT_DEVICEPROPERTIES)) {

                Attr attr = element
                        .getAttributeNode(IDeviceRegistryConstants.ATT_BUNDLE);

                if (attr != null) {
                    String bundleName = attr.getValue();
                    if (bundleName
                            .equals(IDeviceRegistryMigrationConstants.UEI_BUNDLE_NAME)) {
                        element
                                .setAttribute(
                                        IDeviceRegistryConstants.ATT_CLASS,
                                        IDeviceRegistryMigrationConstants.UEI_DEVICE_CLASS);
                    } else if (bundleName
                            .equals(IDeviceRegistryMigrationConstants.MICROEMU_BUNDLE_NAME)) {
                        element
                                .setAttribute(
                                        IDeviceRegistryConstants.ATT_CLASS,
                                        IDeviceRegistryMigrationConstants.MICROEMU_DEVICE_CLASS);
                    } else if (bundleName
                            .equals(IDeviceRegistryMigrationConstants.MPOWERPLAYER_BUNDLE_NAME)) {
                        element
                                .setAttribute(
                                        IDeviceRegistryConstants.ATT_CLASS,
                                        IDeviceRegistryMigrationConstants.MPOWERPLAYER_DEVICE_CLASS);
                    } else if (bundleName
                            .equals(IDeviceRegistryMigrationConstants.ME4SE_BUNDLE_NAME)) {
                        element
                                .setAttribute(
                                        IDeviceRegistryConstants.ATT_CLASS,
                                        IDeviceRegistryMigrationConstants.ME4SE_DEVICE_CLASS);
                    } else if (bundleName
                            .equals(IDeviceRegistryMigrationConstants.MOT_BUNDLE_NAME)) {
                        element
                                .setAttribute(
                                        IDeviceRegistryConstants.ATT_CLASS,
                                        IDeviceRegistryMigrationConstants.MOT_DEVICE_CLASS);
                    }
                }
            } else if (name
                    .equalsIgnoreCase(IDeviceRegistryConstants.ELEMENT_CLASSPATH)) {
                element.setAttribute(IDeviceRegistryConstants.ATT_CLASS,
                        IDeviceRegistryMigrationConstants.CLASSPATH_CLASS);

            } else if (name.startsWith(IDeviceRegistryConstants.ELEMENT_ENTRY)) {
                String className = element
                        .getAttribute(IDeviceRegistryConstants.ATT_CLASS);

                if (className
                        .endsWith(IDeviceRegistryMigrationConstants.API_TERMINATOR)) {
                    element.setAttribute(IDeviceRegistryConstants.ATT_CLASS,
                            IDeviceRegistryMigrationConstants.API_CLASS);
                } else if (className
                        .endsWith(IDeviceRegistryMigrationConstants.LIBRARY_TERMINATOR)) {
                    element.setAttribute(IDeviceRegistryConstants.ATT_CLASS,
                            IDeviceRegistryMigrationConstants.LIBRARY_CLASS);
                }
            } else if (name
                    .equals(IDeviceRegistryConstants.ELEMENT_PREVERIFIER)
                    || name
                            .equals(IDeviceRegistryConstants.ELEMENT_DEFAULTPREVERIFIER)) {
                element.setAttribute(IDeviceRegistryConstants.ATT_CLASS,
                        IDeviceRegistryMigrationConstants.PREVERIFIER_CLASS);
            } else if (name.equals(IDeviceRegistryConstants.ELEMENT_PARAMETERS)) {
                element.setAttribute(IDeviceRegistryConstants.ATT_CLASS,
                        IDeviceRegistryMigrationConstants.PARAMETERS_CLASS);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.util.migration.AbstractMigration#migrate(org.w3c.dom.Document)
     */
    @Override
    public Document migrate(Document document) {

        migrated = false;

        if (document == null) {
            return null;
        }

        Element rootXmlElement = document.getDocumentElement();
        if (!rootXmlElement.getNodeName().equals(
                IDeviceRegistryConstants.ELEMENT_DEVICEREGISTRY)) {
            return null;
        }

        // FIXME
        // Version version = XMLUtils.getVersion(document);
        // if (version.compareTo(new Version("1.0.0")) < 0) { //$NON-NLS-1$
        DocumentAdapter documentAdapter = new DocumentAdapter(document);

        documentAdapter.accept(new ConversionDocumentVisitor());

        String pluginVersion = MTJCore.getPluginVersion();
        Version newVersion = new Version(pluginVersion);

        rootXmlElement.setAttribute(IDeviceRegistryConstants.ATT_VERSION,
                newVersion.toString());
        migrated = true;
        // }
        return document;
    }

}
