/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Using L10n Models.
 *     David Marques (Motorola) - Fixing cleaning.
 *     David Marques (Motorola) - Removing all .properties from project
 *                                upon cleaning project.    
 *     David Marques (Motorola) - Generating keys on upper case.                                                 
 */
package org.eclipse.mtj.internal.core.l10n;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.l10n.L10nEntry;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.osgi.util.NLS;

import de.schlichtherle.io.FileOutputStream;

/**
 * L10nBuilder Class builds localization properties files from a Localization
 * Data file found in the project root folder.
 * 
 * @author David Marques
 * @since 0.9.1
 */
public class L10nBuilder extends IncrementalProjectBuilder {

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IncrementalProjectBuilder#build(int, java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
     */
    @SuppressWarnings("unchecked")
    protected IProject[] build(int kind, Map args, IProgressMonitor monitor)
            throws CoreException {

        IProject project = this.getProject();
        try {
            IResourceDelta delta = this.getDelta(project);
            if (delta != null) {
                if (hasLocalizationDataChanged(project, delta)) {
                    doBuild(project, monitor);
                }
            } else {
                doBuild(project, monitor);
            }
        } catch (FileNotFoundException e) {
            this.clean(monitor);
            MTJCore.throwCoreException(IStatus.ERROR, 0x00, e);
        }
        return new IProject[] { project };
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IncrementalProjectBuilder#clean(org.eclipse.core.runtime.IProgressMonitor)
     */
    protected void clean(IProgressMonitor monitor) throws CoreException {
        monitor.subTask(Messages.L10nBuilder_clean_CleaningOldProperties);

        IJavaProject javaProject = JavaCore.create(getProject());
        IPackageFragmentRoot[] roots = javaProject.getPackageFragmentRoots();
        for (IPackageFragmentRoot root : roots) {
            if (root.getKind() != IPackageFragmentRoot.K_SOURCE) {
                continue;
            }

            root.getCorrespondingResource().accept(new IResourceVisitor() {

                /* (non-Javadoc)
                 * @see org.eclipse.core.resources.IResourceVisitor#visit(org.eclipse.core.resources.IResource)
                 */
                public boolean visit(IResource resource) throws CoreException {
                    String name = resource.getName();
                    if (name.startsWith("messages")
                            && name.endsWith(".properties")) {
                        resource.delete(true, new NullProgressMonitor());
                    }
                    return true;
                }
            });
        }
    }

    /**
     * Checks if the localization data file has changed.
     * 
     * @param project mtj project instance.
     * @param delta project delta.
     * @return true if has changed false otherwise.
     * @throws FileNotFoundException if the xml file does not exist.
     */
    private boolean hasLocalizationDataChanged(IProject project,
            IResourceDelta delta) throws FileNotFoundException {
        boolean result = false;

        IFile locationsXml = project.getFile(L10nApi.LOCALIZATION_DATA_FILE);
        if (locationsXml.exists()) {
            IResourceDelta xmlDelta = delta.findMember(locationsXml
                    .getProjectRelativePath());
            if (xmlDelta != null) {
                switch (xmlDelta.getKind()) {
                    case IResourceDelta.CHANGED:
                    case IResourceDelta.REMOVED:
                    case IResourceDelta.ADDED:
                        result = true;
                        break;
                }
            } else {
                result = true;
            }
        } else {
            throw new FileNotFoundException(
                    Messages.L10nBuilder_LocalizationDataDoesNotExist);
        }
        return result;
    }

    /**
     * Builds the specified project.
     * 
     * @param project mtj project.
     * @param monitor progress monitor.
     * @throws CoreException any core error occurs.
     */
    private void doBuild(IProject project, IProgressMonitor monitor)
            throws CoreException {
        try {
            this.clean(monitor);

            L10nModel model = L10nApi.loadL10nModel(project);
            if (model.isValid()) {
                monitor.subTask(Messages.L10nBuilder_BuildingLocalizationData);
                L10nLocales locales = model.getLocales();
                IPath path = project.getLocation().append(
                        locales.getDestination());
                File propertiesFolder = new File(path.toFile()
                        .getAbsolutePath());
                this.createProperties(project, locales, propertiesFolder,
                        monitor);

                IJavaProject javaProject = JavaCore.create(this.getProject());
                IPackageFragment[] packages = javaProject.getPackageFragments();
                for (int i = 0; i < packages.length; i++) {
                    if (packages[i].getElementName().equals(
                            locales.getPackage())) {
                        String code = L10nApi.buildL10nConstantsClass(project,
                                packages[i]);

                        packages[i].createCompilationUnit(
                                L10nApi.L10N_CONSTANTS_CLASS, code, true,
                                monitor);
                        break;
                    }
                }

                IFolder folder = project.getFolder(new Path(locales
                        .getDestination()));
                if (folder.exists()) {
                    folder.refreshLocal(IResource.DEPTH_ONE, monitor);
                }
            } else {
                MTJCore.log(IStatus.ERROR, Messages.L10nBuilder_InvalidModel);
            }
        } catch (Exception e) {
            MTJCore.throwCoreException(IStatus.ERROR, 0x00, e);
        }
    }

    /**
     * Creates the localization properties files.
     * 
     * @param project current project.
     * @param locales supported locales
     * @param propertiesFolder target folder.
     * @param monitor active monitor
     * @throws IOException if any IO error occurs.
     */
    private void createProperties(IProject project, L10nLocales locales,
            File propertiesFolder, IProgressMonitor monitor) throws IOException {

        monitor.subTask(Messages.L10nBuilder_ProcessingLocalizationData);
        List<IDocumentElementNode> nodes = locales.getChildren();
        for (IDocumentElementNode localeElement : nodes) {
            L10nLocale locale = (L10nLocale) localeElement;
            writePropertyFile(locale, propertiesFolder, monitor);
        }
    }

    /**
     * Creates a properties file for the specified locale.
     * 
     * @param locale desired locale.
     * @param propertiesFolder target folder.
     * @param monitor active monitor.
     * @throws IOException if any IO error occurs.
     */
    private void writePropertyFile(L10nLocale locale, File propertiesFolder,
            IProgressMonitor monitor) throws IOException {
        monitor
                .subTask(NLS
                        .bind(
                                "Writing Localization Data for locale {0}", locale.getLocaleName())); //$NON-NLS-1$

        if (!propertiesFolder.exists() || !propertiesFolder.isDirectory()) {
            throw new FileNotFoundException(
                    Messages.L10nBuilder_PackageFolderNotFound);
        }

        FileOutputStream stream = null;
        String name = null;
        if (locale.getLocaleName() != null) {
            name = NLS.bind("messages_{0}.properties", locale.getLocaleName()); //$NON-NLS-1$
        } else {
            name = "messages.properties"; //$NON-NLS-1$
        }

        File propFile = new File(propertiesFolder, name);
        try {
            StringBuffer buffer = new StringBuffer();

            IDocumentElementNode[] nodes = locale.getChildNodes();
            for (int i = 0; i < nodes.length; i++) {
                L10nEntry entry = (L10nEntry) nodes[i];
                buffer.append(entry.getKey().toUpperCase());
                buffer.append("="); //$NON-NLS-1$
                buffer.append(entry.getValue());
                buffer.append("\n"); //$NON-NLS-1$
            }

            stream = new FileOutputStream(propFile);
            stream.write(buffer.toString().getBytes());
        } catch (Exception e) {
            throw new IOException(NLS.bind(
                    Messages.L10nBuilder_ErrorWritingProperty, locale
                            .getLocaleName())); //$NON-NLS-2$
        } finally {
            if (stream != null)
                stream.close();
        }
    }
}
