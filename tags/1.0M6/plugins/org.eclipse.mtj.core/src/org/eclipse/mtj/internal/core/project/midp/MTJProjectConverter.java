/**
 * Copyright (c) 2008,2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version
 *     Gustavo de Paula (Motorola) - Refactor project converter
 */
package org.eclipse.mtj.internal.core.project.midp;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.IMetaData;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.midp.ProjectConvertionException;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * MTJProjectConverter class converts a java project into an MTJ project.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @since 1.0
 * @author David Marques
 */
public class MTJProjectConverter {

    private static final String DEFAULT_PREFERENCES_DIRNAME = ".settings"; //$NON-NLS-1$
    private static final String EclipseME_JAVAME_CONTAINER = "J2MELIB"; //$NON-NLS-1$
    private static final String EclipseME_METADATA_ELEM_ROOT = "eclipsemeMetadata"; //$NON-NLS-1$
    private static final String EclipseME_METADATA_FILE = ".eclipseme"; //$NON-NLS-1$
    private static final String EclipseME_NATURE_PREFIX = "eclipseme.core."; //$NON-NLS-1$
    private static final String EclipseME_PREFERENCE_STORE = "eclipseme.core.prefs"; //$NON-NLS-1$
    private static final String EcliseME_TEMP_FOLDER_NAME = ".eclipseme.tmp"; //$NON-NLS-1$

    private static final String FILENAME_PROJECT = ".project"; //$NON-NLS-1$
    private static final String MTJ_METADATA_ELEM_PASSWORDS = "passwords"; //$NON-NLS-1$
    private static final String MTJ_METADATA_ELEM_ROOT = "mtjMetadata"; //$NON-NLS-1$
    private static String oldJadFilePath;

    private static final String PREFS_FILE_EXTENSION = "prefs"; //$NON-NLS-1$

    private static MTJProjectConverter instance;
    
    static {
    	instance = new MTJProjectConverter();
    }
    
    /**
     * Returns a singleton instance of the project converter
     * 
     * @return singleton instance of the project converter
     */
    public static MTJProjectConverter getInstance () {
    	return instance;
    }


    /**
     * Private constructor
     */
    private MTJProjectConverter() {
    }

    /* (non-Javadoc)
	 * @see org.eclipse.mtj.core.project.midp.IMTJProjectConverter#convertEclipseMeProject(org.eclipse.core.resources.IProject, org.eclipse.core.runtime.IProgressMonitor)
	 */
    public synchronized void convertEclipseMeProject(
            final IProject project, IProgressMonitor monitor)
            throws ProjectConvertionException {

        File importedRootFolder = new File(project.getLocationURI());
        List<File> files = getChildrenFiles(importedRootFolder);

        monitor.beginTask(Messages.MTJProjectConverter_convert_taskname, 10);
        // Migrate EclipseME specific files and folders
        for (File file : files) {

            // Rename the ".eclipseme.tmp" folder of the imported project
            renameEclipseMETmpFolder(file, new SubProgressMonitor(monitor, 1));

            // Rename EclipseME preference store "eclipseme.core.prefs" to
            // "org.eclipse.mtj.core.prefs"
            renamePreferenceStoreFile(project, file, new SubProgressMonitor(
                    monitor, 1));

            // To rename the meta data and change it's contents
            modifyMetadata(file, new SubProgressMonitor(monitor, 1));

            // Remove EclipseME builder commands and natures
            removeBuilderAndNature(file, new SubProgressMonitor(monitor, 1));

            /*
             * Do nothing with the old deployment folder, because we don't know
             * the deployment folder's name.
             */
        }
        // Rename the project JAD file to "Application Descriptor"
        renameProjectJAD(project);
        // Convert the imported EclipseME project to MTJ project
        try {
            project.refreshLocal(IResource.DEPTH_INFINITE,
                    new SubProgressMonitor(monitor, 1));
            // perform converting
            convertProject(project, new SubProgressMonitor(monitor, 3));
            // Recover the JAD file name in meta data
            recoverJADFileNameInMetadata(project);

            project.refreshLocal(IResource.DEPTH_INFINITE,
                    new SubProgressMonitor(monitor, 1));

            // delete all .class file to invoke a clean build
            project.build(IncrementalProjectBuilder.CLEAN_BUILD,
                    new SubProgressMonitor(monitor, 1));
            monitor.done();
        } catch (CoreException e) {
            throw new ProjectConvertionException(e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
	 * @see org.eclipse.mtj.core.project.midp.IMTJProjectConverter#convertJavaProject(org.eclipse.jdt.core.IJavaProject, org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice, org.eclipse.core.runtime.IProgressMonitor)
	 */
    public void convertJavaProject(final IJavaProject javaProject,
            final IMIDPDevice device, final IProgressMonitor monitor)
            throws InvocationTargetException, InterruptedException,
            JavaModelException {
        // First, convert to a Java ME MIDlet project
        final IProject project = javaProject.getProject();
        String jadFileName = MidletSuiteProject.getDefaultJadFileName(project);
        MidletSuiteFactory.getMidletSuiteCreationRunnable(project, javaProject,
                device, jadFileName).run(monitor);

        // Now, remove the Java SE libraries
        removeJ2SELibraries(javaProject, monitor);
    }

    /**
     * Convert the specified java project to a Java ME MIDlet suite.
     * 
     * @param javaProject
     * @param device
     * @param monitor
     * @throws ProjectConvertionException - if a conversion error occurs.
     */
    private void convertProject(final IProject project,
            final IProgressMonitor monitor) throws ProjectConvertionException {
        IMIDPDevice device = getDevice(project);
        // Must delete device node from .mtj file, or J2ME library will not add
        // in classpath. @see MidletSuiteProject#setDevice(IDevice,
        // IProgressMonitor)
        deleteDeviceFromMetadata(project);
        if (device == null) {
            throw new ProjectConvertionException(
                    Messages.MTJProjectConverter_convertProject_device_unavailable);
        } else {
            try {
                // First, convert to a Java ME MIDlet Suite
                IJavaProject javaProject = JavaCore.create(project);
                // must remove the cached MIDlet suite project, or class path
                // container will not be added.
                MidletSuiteFactory.removeMidletSuiteProject(javaProject);
                String jadFileName = MidletSuiteProject
                        .getDefaultJadFileName(project);
                MidletSuiteFactory.getMidletSuiteCreationRunnable(project,
                        javaProject, device, jadFileName).run(monitor);
                // Now, remove the Java SE and EclipseME libraries
                removeJ2SEAndEclipseMELibraries(javaProject, monitor);
            } catch (Exception e) {
                throw new ProjectConvertionException(
                        Messages.MTJProjectConverter_convertProject_convertion_error);
            }
        }

    }

    /**
     * delete the device node from meta data file .mtj
     * 
     * @param project
     */
    private void deleteDeviceFromMetadata(IProject project) {
        IFile metadateFile = project.getFile(IMetaData.METADATA_FILE);
        try {
            Document document = XMLUtils.readDocument(metadateFile
                    .getLocation().toFile());
            // delete device in the meta data
            Element deviceNode = (Element) document.getElementsByTagName(
                    "device").item(0); //$NON-NLS-1$
            deviceNode.getParentNode().removeChild(deviceNode);
            XMLUtils.writeDocument(metadateFile.getLocation().toFile(),
                    document);
        } catch (Exception e) {
            MTJCore.log(IStatus.ERROR, e);
        }

    }

    private List<File> getChildrenFiles(File root) {
        List<File> result = new ArrayList<File>();
        if (root.isDirectory()) {
            String[] file = root.list();
            for (String element : file) {
                result.add(new File(root, element));
            }
        }
        return result;
    }

    /**
     * get device according meta data file; if not found, get default device; if
     * no default, return null.
     * 
     * @param project
     * @return
     */
    private IMIDPDevice getDevice(IProject project) {
        IMIDPDevice device = null;
        IFile metadateFile = project.getFile(IMetaData.METADATA_FILE);
        try {
            Document document = XMLUtils.readDocument(metadateFile
                    .getLocation().toFile());
            Element deviceElement = (Element) document.getElementsByTagName(
                    "device").item(0); //$NON-NLS-1$
            String deviceGroup = deviceElement.getAttribute("group"); //$NON-NLS-1$
            String deviceName = deviceElement.getAttribute("name"); //$NON-NLS-1$
            device = (IMIDPDevice) MTJCore.getDeviceRegistry().getDevice(
                    deviceGroup, deviceName);
        } catch (Exception e) {
            device = null;
        }
        if (device == null) {
            device = (IMIDPDevice) MTJCore.getDeviceRegistry().getDefaultDevice();
        }
        return device;
    }

    /**
     * Get the MIDlet project from imported project
     * 
     * @param project
     * @return
     */
    private IMidletSuiteProject getMidletSuiteProject(IProject project) {
        IJavaProject javaProject = JavaCore.create(project);
        IMidletSuiteProject midletProject = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);
        return midletProject;
    }

    private String getOldJadFilePath(File elcipsemeMetadataFile) {
        String oldJadFilePath = null;
        try {
            Document document = XMLUtils.readDocument(elcipsemeMetadataFile);
            Element rootElement = document.getDocumentElement();
            oldJadFilePath = rootElement.getAttribute("jad"); //$NON-NLS-1$
        } catch (Exception e) {
            MTJCore.log(IStatus.ERROR, e);
        }

        return oldJadFilePath;
    }

    /**
     * Return a boolean indicating whether or not the specified class path entry
     * represents the standard EclipseME library container.
     * 
     * @param entry
     * @return
     */
    private boolean isEclipseMELibraryEntry(IClasspathEntry entry) {
        boolean isEclipseMEEntry = false;

        if (entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER) {
            if (entry.getPath().lastSegment()
                    .equals(EclipseME_JAVAME_CONTAINER)) {
                isEclipseMEEntry = true;
            }
        }
        return isEclipseMEEntry;
    }

    /**
     * Return a boolean indicating whether or not the specified class path entry
     * represents the standard J2SE libraries.
     * 
     * @param entry
     * @return
     */
    private boolean isJ2SELibraryEntry(IClasspathEntry entry) {
        boolean isJ2SEEntry = false;

        if (entry.getEntryKind() == IClasspathEntry.CPE_LIBRARY) {
            if (entry.getPath().lastSegment().equals("JRE_LIB")) { //$NON-NLS-1$
                isJ2SEEntry = true;
            }
        } else if (entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER) {
            if (entry.getPath().lastSegment().equals(
                    "org.eclipse.jdt.launching.JRE_CONTAINER")) { //$NON-NLS-1$
                isJ2SEEntry = true;
            }
        }
        return isJ2SEEntry;
    }

    /**
     * To rename the meta data and change it's contents
     * 
     * @param elcipsemeMetadataFile
     * @param monitor
     */
    private void modifyMetadata(File elcipsemeMetadataFile,
            IProgressMonitor monitor) {
        if (elcipsemeMetadataFile.isFile()
                && elcipsemeMetadataFile.getName().equals(
                        EclipseME_METADATA_FILE)) {
            // reserve the old jad file path for use in
            // removeGeneratedJADFileIfNecessary(IProject) method
            oldJadFilePath = getOldJadFilePath(elcipsemeMetadataFile);
            monitor.beginTask(
                    Messages.MTJProjectConverter_modifyMetadata_taskname, 2);
            // rename meta data file
            IPath metadataPath = new Path(elcipsemeMetadataFile.getPath());
            File renamedMetaFile = new File(metadataPath.removeLastSegments(1)
                    + File.separator + IMetaData.METADATA_FILE);
            elcipsemeMetadataFile.renameTo(renamedMetaFile);
            monitor.worked(1);
            // change contents
            try {
                Document document = XMLUtils.readDocument(renamedMetaFile);
                Element oldRootElement = document.getDocumentElement();
                // If meta data file's root element is wrong, just delete the
                // meta data file.
                if (!oldRootElement.getNodeName().equals(
                        EclipseME_METADATA_ELEM_ROOT)) {
                    renamedMetaFile.delete();
                    return;
                }
                // change root element's name
                Element newRootElement = document
                        .createElement(MTJ_METADATA_ELEM_ROOT);
                // append all of the XML file contents(except root element) to
                // the new root element
                while (oldRootElement.hasChildNodes()) {
                    newRootElement.appendChild(oldRootElement.getFirstChild());
                }
                document.replaceChild(newRootElement, oldRootElement);
                // delete keystore's passwords in the meta data
                Element passwordNode = (Element) document.getElementsByTagName(
                        MTJ_METADATA_ELEM_PASSWORDS).item(0);
                if (passwordNode != null) {
                    // delete "passwords" element in meta data file.
                    passwordNode.getParentNode().removeChild(passwordNode);
                }
                XMLUtils.writeDocument(renamedMetaFile, document);
            } catch (Exception e) {
                MTJCore.log(IStatus.ERROR,
                        Messages.MTJProjectConverter_modifyMetadata_exception,
                        e);
                renamedMetaFile.delete();
                // continue;
            }
            monitor.done();
        }
    }

    /**
     * The convertProject(IProject, IProgressMonitor) method will change the JAD
     * file name in metadata to default name, so we should change it back.
     * 
     * @param project
     * @throws CoreException
     */
    private void recoverJADFileNameInMetadata(IProject project)
            throws CoreException {
        IMidletSuiteProject midletProject = getMidletSuiteProject(project);
        IFile oldJadFile = project.getFile(oldJadFilePath);
        String oldJadFileName = oldJadFile.getName();
        String generatedJADFileName = midletProject.getJadFileName();
        if (!oldJadFileName.equals(generatedJADFileName)) {
            midletProject.setJadFileName(oldJadFileName);
            midletProject.saveMetaData();
        }
    }

    /**
     * Remove EclipseME builder commands and natures
     * 
     * @param projectFile
     * @param monitor
     */
    private void removeBuilderAndNature(File projectFile,
            IProgressMonitor monitor) {
        if (projectFile.isFile()
                && projectFile.getName().equals(FILENAME_PROJECT)) {
            monitor
                    .beginTask(
                            Messages.MTJProjectConverter_removeBuilderAndNature_taskname,
                            2);
            try {
                Document document = XMLUtils.readDocument(projectFile);
                // Remove EclipseME builder
                NodeList nameNodeList = document.getElementsByTagName("name"); //$NON-NLS-1$
                List<Node> buildersToRemove = new ArrayList<Node>();
                for (int i = 0; i < nameNodeList.getLength(); i++) {
                    Node node = nameNodeList.item(i);
                    if (node.getTextContent().indexOf(EclipseME_NATURE_PREFIX) >= 0) {
                        buildersToRemove.add(node);
                    }
                }
                for (Node n : buildersToRemove) {
                    Node buildCommandNode = n.getParentNode();
                    buildCommandNode.getParentNode().removeChild(
                            buildCommandNode);
                }
                monitor.worked(1);
                // Remove EclipseME natures
                NodeList natureNodeList = document
                        .getElementsByTagName("nature"); //$NON-NLS-1$
                List<Node> naturesToRemove = new ArrayList<Node>();
                for (int i = 0; i < natureNodeList.getLength(); i++) {
                    Node node = natureNodeList.item(i);
                    if (node.getTextContent().indexOf(EclipseME_NATURE_PREFIX) >= 0) {
                        naturesToRemove.add(node);
                    }
                }
                for (Node n : naturesToRemove) {
                    n.getParentNode().removeChild(n);
                }

                XMLUtils.writeDocument(projectFile, document);
            } catch (Exception e) {
                MTJCore.log(IStatus.ERROR, FILENAME_PROJECT + " error", e); //$NON-NLS-1$
            }
            monitor.done();
        }
    }

    /**
     * Remove the Java SE standard libraries & EclipseME library container from
     * the specified IJavaProject instance.
     * 
     * @param javaProject
     */
    private void removeJ2SEAndEclipseMELibraries(
            IJavaProject javaProject, IProgressMonitor monitor)
            throws JavaModelException {
        IClasspathEntry[] entries = javaProject.getRawClasspath();
        ArrayList<IClasspathEntry> list = new ArrayList<IClasspathEntry>();

        for (int i = 0; i < entries.length; i++) {
            if (!isJ2SELibraryEntry(entries[i])
                    && !isEclipseMELibraryEntry(entries[i])) {
                list.add(entries[i]);
            }
        }

        entries = list.toArray(new IClasspathEntry[list.size()]);
        javaProject.setRawClasspath(entries, monitor);
    }

    /**
     * Remove the Java SE standard libraries from the specified IJavaProject
     * instance.
     * 
     * @param javaProject
     */
    private void removeJ2SELibraries(IJavaProject javaProject,
            IProgressMonitor monitor) throws JavaModelException {
        IClasspathEntry[] entries = javaProject.getRawClasspath();
        ArrayList<IClasspathEntry> list = new ArrayList<IClasspathEntry>();

        for (int i = 0; i < entries.length; i++) {
            if (!isJ2SELibraryEntry(entries[i])) {
                list.add(entries[i]);
            }
        }

        entries = list.toArray(new IClasspathEntry[list.size()]);
        javaProject.setRawClasspath(entries, monitor);
    }

    /**
     * Rename the ".eclipseme.tmp" folder of the imported project
     * 
     * @param eclipseMeTmpFolder
     * @param monitor
     */
    private void renameEclipseMETmpFolder(File eclipseMeTmpFolder,
            IProgressMonitor monitor) {
        if (eclipseMeTmpFolder.isDirectory()
                && eclipseMeTmpFolder.getName().equals(
                        EcliseME_TEMP_FOLDER_NAME)) {
            monitor
                    .beginTask(
                            Messages.MTJProjectConverter_renameEclipseMETmpFolder_taskname,
                            1);
            IPath path = new Path(eclipseMeTmpFolder.getPath());
            IPath mtjTmpFolderPath = path.removeLastSegments(1).append(
                    IMTJCoreConstants.TEMP_FOLDER_NAME);
            File mtjTmpFolder = new File(mtjTmpFolderPath.toFile().getPath());
            eclipseMeTmpFolder.renameTo(mtjTmpFolder);
            monitor.done();
        }
    }

    /**
     * Rename EclipseME preference store "eclipseme.core.prefs" to
     * "org.eclipse.mtj.core.prefs"
     * 
     * @param project
     * @param preferenceFolder
     * @param monitor
     */
    private void renamePreferenceStoreFile(final IProject project,
            File preferenceFolder, IProgressMonitor monitor) {
        if (preferenceFolder.isDirectory()
                && preferenceFolder.getName().equals(
                        DEFAULT_PREFERENCES_DIRNAME)) {
            monitor
                    .beginTask(
                            Messages.MTJProjectConverter_renamePreferenceStoreFile_taskname,
                            1);
            List<File> preferencesStoreFiles = getChildrenFiles(preferenceFolder);
            Iterator<File> iterator = preferencesStoreFiles.iterator();
            while (iterator.hasNext()) {
                File aPreferencesStoreFile = iterator.next();
                if (aPreferencesStoreFile.getName().equals(
                        EclipseME_PREFERENCE_STORE)) {
                    ProjectScope projectScope = new ProjectScope(project);
                    IEclipsePreferences prefNode = projectScope
                            .getNode(IMTJCoreConstants.PLUGIN_ID);
                    IPath path = new Path(aPreferencesStoreFile.getPath());
                    File renamedPrefFile = new File(path.removeLastSegments(1)
                            + File.separator + prefNode.name() + "." //$NON-NLS-1$
                            + PREFS_FILE_EXTENSION);
                    aPreferencesStoreFile.renameTo(renamedPrefFile);
                    break;
                }
            }
            monitor.done();
        }
    }

    /**
     * Rename the project JAD file to "Application Descriptor"
     * 
     * @param project
     */
    private void renameProjectJAD(IProject project) {
        IFile jadFile = project.getFile(oldJadFilePath);
        File localFile = jadFile.getLocation().toFile();
        IMidletSuiteProject suite = getMidletSuiteProject(project);
        File dest = suite.getApplicationDescriptorFile().getLocation().toFile();
        localFile.renameTo(dest);
    }
}
