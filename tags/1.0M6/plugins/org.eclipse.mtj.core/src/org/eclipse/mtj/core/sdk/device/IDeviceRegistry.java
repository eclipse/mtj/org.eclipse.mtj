/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk.device;

import java.io.IOException;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * <p>
 * The device registry is the main entry point that is used to manage the
 * devices that are currently supported on a MTJ installation. When a SDK is
 * imported it will return a list of devices that must be added to the registry
 * in order to have them available to MTJ.
 * </p>
 * 
 * @see IDevice
 * @see IPreverifier
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IDeviceRegistry extends IPersistable {

    /**
     * Add a new device instance to the device registry.
     * 
     * @param device the device instance to be added to the device registry.
     * @throws IllegalArgumentException if the device is not well formed.
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    public abstract void addDevice(IDevice device)
            throws IllegalArgumentException, PersistenceException;

    /**
     * Add the specified listener to the list of listeners for changes in this
     * registry.
     * 
     * @param listener
     */
    public abstract void addRegistryListener(IDeviceRegistryListener listener);

    /**
     * Clear the registry of all entries.
     * 
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    public abstract void clear() throws PersistenceException;

    /**
     * Enable the DeviceAdded Event to be fired to all
     * {@link IDeviceRegistryListener} instances registered using
     * {@link #addRegistryListener(IDeviceRegistryListener)} when the
     * {@link #addDevice(IDevice)} is invoked.
     * <p>
     * By default, this event is enabled. Use the
     * {@link #isDeviceAddedEventEnabled()} to get current event firing status.
     * </p>
     * 
     * @param fire flag indicating if the DeviceAdded Event should be fired or
     *            not.
     */
    public abstract void enableDeviceAddedEvent(boolean fire);

    /**
     * Return a list of all of the devices in the registry.
     * 
     * @return
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    public abstract List<IDevice> getAllDevices() throws PersistenceException;

    /**
     * Return the default device or <code>null</code> if one is not specified.
     * 
     * @return
     */
    public abstract IDevice getDefaultDevice();

    /**
     * Return the default preverifier or <code>null</code> if one is not
     * specified.
     * 
     * @return
     */
    public abstract IPreverifier getDefaultPreferifier();

    /**
     * Return the device with the specified key or <code>null</code> if no such
     * device is found in the registry.
     * 
     * @param groupName
     * @param deviceName
     * @return
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    public abstract IDevice getDevice(String groupName, String deviceName)
            throws PersistenceException;

    /**
     * Return the number of devices registered.
     * 
     * @return
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    public abstract int getDeviceCount() throws PersistenceException;

    /**
     * Return the registered device groups.
     * 
     * @return
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    public abstract List<String> getDeviceGroups() throws PersistenceException;

    /**
     * Return the devices found in the specified group or <code>null</code> if
     * the specified group cannot be found.
     * 
     * @param groupName
     * @return
     * @throws PersistenceException
     */
    public abstract List<IDevice> getDevices(String groupName)
            throws PersistenceException;

    /**
     * Checks if the DeviceAdded Event will be fired to all
     * {@link IDeviceRegistryListener} instances registered using
     * {@link #addRegistryListener(IDeviceRegistryListener)} or not when
     * invoking {@link #addDevice(IDevice)}.
     * 
     * @return <code>true</code> is DeviceAdded Event will be fired when
     *         {@link #addDevice(IDevice)} is invoked, <code>false</code>
     *         otherwise.
     */
    public abstract boolean isDeviceAddedEventEnabled();

    /**
     * Load the contents of the device registry from the storage file in the
     * plug-in state location.
     * 
     * @throws PersistenceException
     */
    public abstract void load() throws PersistenceException;

    /**
     * Remove the specified device from the registry if it exists.
     * 
     * @param device
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    public abstract void removeDevice(IDevice device)
            throws PersistenceException;

    /**
     * Remove the specified listener to the list of listeners for changes in
     * this registry.
     * 
     * @param listener
     */
    public abstract void removeRegistryListener(IDeviceRegistryListener listener);

    /**
     * Set the default device.
     * 
     * @param device
     */
    public abstract void setDefaultDevice(IDevice device);

    /**
     * Set the default preverifier.
     * 
     * @param device
     */
    public abstract void setDefaultPreverifer(IPreverifier preverifier);

    /**
     * Store out the contents of the registry into the standard device storage
     * file in the plug-in state location.
     * 
     * @throws PersistenceException
     * @throws TransformerException
     * @throws IOException
     */
    public abstract void store() throws PersistenceException,
            TransformerException, IOException;
}