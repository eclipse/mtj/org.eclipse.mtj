/**
 * Copyright (c) 2008,2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.project.midp;

/**
 * ProjectConvertionException Class wraps exceptions during a project conversion
 * process.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @since 1.0
 */
public class ProjectConvertionException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Creates a ProjectConvertionException instance.
     * 
     * @param message - error message.
     * @param throwable - wrapped throwable.
     */
    public ProjectConvertionException(String message, Throwable throwable) {
        super(message, throwable);
    }

    /**
     * Creates a ProjectConvertionException instance.
     * 
     * @param message - error message.
     */
    public ProjectConvertionException(String message) {
        super(message);
    }
}
