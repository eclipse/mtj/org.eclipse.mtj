/**
 * Copyright (c) 2007,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Changed the getAPIs to first try to extract 
 *                                API info from MANIFEST
 *     Gustavo de Paula (Motorola)  - Fix getTypeFromManifest NPE  
 *                                                                    
 *                              
 */
package org.eclipse.mtj.internal.core.sdk.device.midp.library;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.eclipse.mtj.core.sdk.device.midp.library.api.API;
import org.eclipse.mtj.core.sdk.device.midp.library.api.APIType;
import org.osgi.framework.Version;

/**
 * Provides access to the API definitions that have been registered via the API
 * extension point.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @author Craig Setera
 */
public class APIRegistry {

    /**
     * Return the list of API's present in specified JAR file.
     * 
     * @param jarFile
     * @return
     * @throws IOException
     */
    public static API[] getAPIs(File jarFile) {
        Map<String, API> apis = new HashMap<String, API>();
        JarFile jar = null;

        try {
            jar = new JarFile(jarFile);

            if (jar != null) {

                API api = createAPIFromManifest(jar);
                if (api != null) {
                    apis.put(api.getIdentifier(), api);
                }

            }
        } catch (IOException e) {

        } finally {
            if (jar != null) {
                try {
                    jar.close();
                } catch (IOException e) {
                }
            }
        }

        // Final fall back position
        if (apis.size() == 0) {
            API unknown = createUnknownAPI(jarFile);
            apis.put(unknown.getIdentifier(), unknown);
        }

        // Convert to the final array
        Collection<API> values = apis.values();
        return values.toArray(new API[values.size()]);
    }

    /**
     * Attempt to return an API definition for the information in the manifest.
     * 
     * @param attributes
     * @return
     * @throws IOException
     */
    private static API createAPIFromManifest(JarFile jarFile)
            throws IOException {
        API api = null;

        Manifest manifest = jarFile.getManifest();
        if (manifest != null) {
            Attributes attributes = manifest.getMainAttributes();

            String identifier = attributes.getValue(APIManifestAttributes.API
                    .toString());
            if (identifier != null) {
                api = new API();
                api.setIdentifier(identifier);
                api.setName(attributes.getValue(APIManifestAttributes.API_NAME
                        .toString()));
                api.setType(getTypeFromManifest(attributes));
                api.setVersion(getVersionFromManifest(attributes));
            }
        }

        return api;
    }

    /**
     * Return a new unknown API instance.
     * 
     * @param libraryFile
     * @return
     */
    private static API createUnknownAPI(File libraryFile) {
        API api = new API();
        api.setIdentifier(libraryFile.getName());
        api.setName("Unknown Library"); //$NON-NLS-1$
        api.setType(APIType.UNKNOWN);
        api.setVersion(new Version("1.0")); //$NON-NLS-1$

        return api;
    }

    /**
     * Return the integer type of the value specified in the manifest.
     * 
     * @param attributes
     * @return
     */
    private static APIType getTypeFromManifest(Attributes attributes) {
        APIType type = APIType.UNKNOWN;

        String typeString = attributes.getValue(APIManifestAttributes.API_TYPE
                .toString());
        if ((typeString == null)
                || typeString.equalsIgnoreCase(APIType.OPTIONAL.toString())) {
            type = APIType.OPTIONAL;
        } else if (typeString.equalsIgnoreCase(APIType.PROFILE.toString())) {
            type = APIType.PROFILE;
        } else if (typeString
                .equalsIgnoreCase(APIType.CONFIGURATION.toString())) {
            type = APIType.CONFIGURATION;
        }

        return type;
    }

    /**
     * Return the version of the API as found in the attributes.
     * 
     * @param attributes
     * @return
     */
    private static Version getVersionFromManifest(Attributes attributes) {
        Version version = null;

        String versionString = attributes
                .getValue(APIManifestAttributes.API_VERSION.toString());
        if (versionString != null) {
            version = new Version(versionString);
        } else {
            version = new Version("0.0.0"); //$NON-NLS-1$
        }

        return version;
    }

    // Private constructor for static access.
    private APIRegistry() {
    }
}
