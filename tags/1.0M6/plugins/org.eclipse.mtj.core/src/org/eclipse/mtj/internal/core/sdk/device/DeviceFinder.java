/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Refactoring after IDeviceFinder API creation
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceFinder;
import org.eclipse.mtj.core.sdk.device.IDeviceImporter;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.osgi.util.NLS;

/**
 * The device finder allows to find the list of devices available in a specified
 * directory. This finder uses the available registered device importers to find
 * these devices.
 * 
 * @author Craig Setera
 * @noextend This class is not intended to be subclassed by clients.
 */
public class DeviceFinder implements IDeviceFinder {

    /**
     * Map of registered {@link IDeviceImporter Device Importers's}.
     */
    private static Map<String, IDeviceImporter> deviceImportersMap;

    /**
     * The Unique DeviceFinder instance.
     */
    private static DeviceFinder finder = null;

    /**
     * Return the unique {@link DeviceFinder} instance.
     * 
     * @return the single {@link DeviceFinder} instance.
     */
    public static synchronized IDeviceFinder getInstance() {
        if (finder == null) {
            finder = new DeviceFinder();
        }
        return finder;
    }

    /**
     * Creates a new instance of DeviceFinder.
     */
    private DeviceFinder() {
        super();

        deviceImportersMap = Collections
                .synchronizedMap(new HashMap<String, IDeviceImporter>());

        try {
            readDeviceImporters();
        } catch (CoreException e) {
            MTJCore.log(IStatus.ERROR, Messages.DeviceFinder_0, e);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceFinder#findDevices(java.io.File, org.eclipse.core.runtime.IProgressMonitor)
     */
    public synchronized List<IDevice> findDevices(File directory,
            IProgressMonitor monitor) throws CoreException,
            InterruptedException {

        ArrayList<IDevice> foundDevices = new ArrayList<IDevice>();

        try {

            monitor
                    .beginTask(Messages.DeviceFinder_1,
                            IProgressMonitor.UNKNOWN);

            for (Iterator<Entry<String, IDeviceImporter>> iterator = deviceImportersMap
                    .entrySet().iterator(); iterator.hasNext();) {

                List<IDevice> list = searchDevicesInDirectory(directory,
                        iterator.next().getValue(), new SubProgressMonitor(
                                monitor, 1));
                if (list != null) {
                    foundDevices.addAll(list);
                }
            }
        } finally {
            monitor.done();
        }
        return foundDevices;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceFinder#findDevices(java.lang.String, java.io.File, org.eclipse.core.runtime.IProgressMonitor)
     */
    public synchronized List<IDevice> findDevices(String deviceImporterID,
            File directory, IProgressMonitor monitor) throws CoreException,
            InterruptedException {

        ArrayList<IDevice> foundDevices = new ArrayList<IDevice>();

        try {

            monitor
                    .beginTask(Messages.DeviceFinder_1,
                            IProgressMonitor.UNKNOWN);
            
            if (deviceImporterID != null) {
                IDeviceImporter deviceImporter = deviceImportersMap
                        .get(deviceImporterID);

                if (deviceImporter != null) {

                    List<IDevice> list = searchDevicesInDirectory(directory,
                            deviceImporter, new SubProgressMonitor(monitor, 1));
                    if (list != null) {
                        foundDevices.addAll(list);
                    }
                }
            }

        } finally {
            monitor.done();
        }
        return foundDevices;
    }

    /**
     * Read the device importers from the registered extensions to the
     * "deviceImporters" extension point.
     * 
     * @return
     * @throws CoreException
     */
    private void readDeviceImporters() throws CoreException {

        if (deviceImportersMap != null) {

            String pluginId = MTJCore.getDefault().getBundle()
                    .getSymbolicName();

            // check if this bundle does not have a specified symbolic name
            if (pluginId != null) {

                IExtensionRegistry registry = Platform.getExtensionRegistry();
                IConfigurationElement[] elements = registry
                        .getConfigurationElementsFor(pluginId,
                                IDeviceImporter.EXT_DEVICE_IMPORTERS);

                // Check if the extension point has at least one extension
                // configured
                if (elements.length >= 0) {

                    DeviceImporterElement[] deviceElements = new DeviceImporterElement[elements.length];

                    for (int i = 0; i < elements.length; i++) {
                        deviceElements[i] = new DeviceImporterElement(
                                elements[i]);
                    }

                    // sort the device importers according to their priority
                    Arrays.sort(deviceElements, new Comparator<Object>() {
                        public int compare(Object o1, Object o2) {
                            DeviceImporterElement element1 = (DeviceImporterElement) o1;
                            DeviceImporterElement element2 = (DeviceImporterElement) o2;
                            return element1.getPriority()
                                    - element2.getPriority();
                        }
                    });

                    for (int i = 0; i < deviceElements.length; i++) {
                        deviceImportersMap.put(deviceElements[i].getId(),
                                deviceElements[i].getDeviceImporter());
                    }
                }
            }
        }
    }

    /**
     * Search for devices in the specified directory or sub directories given
     * the specified {@link IDeviceImporter} instance.
     * 
     * @param directory
     * @param importer
     * @param monitor
     * @return
     * @throws InterruptedException if the user cancels the operation.
     * @throws CoreException
     */
    private List<IDevice> searchDevicesInDirectory(File directory,
            IDeviceImporter importer, IProgressMonitor monitor)
            throws InterruptedException, CoreException {

        // Give the user the chance to bail out during the search
        if (monitor.isCanceled()) {
            throw new InterruptedException();
        }

        ArrayList<IDevice> foundDevices = null;
        try {
            if (directory.canRead()) {
                foundDevices = new ArrayList<IDevice>();

                monitor.setTaskName(NLS
                        .bind(Messages.DeviceFinder_2, directory));

                SubProgressMonitor subMonitor = new SubProgressMonitor(monitor,
                        1);

                // First find any devices in the current directory
                List<IDevice> localFoundDevices = importer.importDevices(
                        directory, subMonitor);

                if (localFoundDevices != null) {
                    foundDevices.addAll(localFoundDevices);
                }

                // Now recurse to sub directories
                File[] subdirectories = directory.listFiles(new FileFilter() {
                    public boolean accept(File pathname) {
                        return pathname.isDirectory();
                    }
                });

                if (subdirectories != null) {
                    for (File element : subdirectories) {

                        List<IDevice> subdirectFoundDevices = searchDevicesInDirectory(
                                element, importer, monitor);

                        if (subdirectFoundDevices != null) {
                            foundDevices.addAll(subdirectFoundDevices);
                        }
                    }
                }
            }
        } catch (SecurityException e) {
            MTJCore.log(IStatus.ERROR, NLS.bind(Messages.DeviceFinder_3,
                    directory.getPath()), e);
        }
        monitor.worked(1);

        return foundDevices;
    }
}
