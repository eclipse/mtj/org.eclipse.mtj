/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.util.migration;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.symbol.ISymbolSetConstants;
import org.eclipse.mtj.internal.core.util.xml.DocumentAdapter;
import org.eclipse.mtj.internal.core.util.xml.DocumentVisitor;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.osgi.framework.Version;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * @author Diego Madruga Sandin
 */
public class SymbolDefinitionsMigration extends AbstractMigration implements
        ISymbolDefinitionsMigrationConstants {

    /**
     * @author Diego Madruga Sandin
     */
    private final class ConversionDocumentVisitor implements DocumentVisitor {

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.core.util.xml.DocumentVisitor#visitElement(org.w3c.dom.Element)
         */
        public void visitElement(Element element) {

            Attr attr = element
                    .getAttributeNode(ISymbolSetConstants.ATT_CLASS);

            if (attr != null) {
                attr.setValue(SYMBOLDEFINITIONSET_CLASS);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.util.migration.AbstractMigration#migrate(org.w3c.dom.Document)
     */
    @Override
    public Document migrate(Document document) {
        migrated = false;

        if (document == null) {
            return null;
        }

        Element rootXmlElement = document.getDocumentElement();
        if (!rootXmlElement
                .getNodeName()
                .equals(
                        ISymbolSetConstants.ELEMENT_SYMBOLDEFINITIONSREGISTRY)) {
            return null;
        }

        Version version = XMLUtils.getVersion(document);

        if (version.compareTo(new Version("1.0.0")) < 0) { //$NON-NLS-1$
            DocumentAdapter documentAdapter = new DocumentAdapter(document);

            documentAdapter.accept(new ConversionDocumentVisitor());

            String pluginVersion = MTJCore.getPluginVersion();
            Version newVersion = new Version(pluginVersion);

            rootXmlElement.setAttribute(
                    ISymbolSetConstants.ATT_VERSION, newVersion
                            .toString());
            migrated = true;
        }
        return document;
    }

}
