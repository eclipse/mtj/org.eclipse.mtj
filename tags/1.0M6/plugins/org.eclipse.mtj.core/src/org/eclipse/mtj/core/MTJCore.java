/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)    - Initial implementation
 *     Diego Sandin (Motorola)     - Refactoring package name to follow eclipse 
 *                                   standards
 *     Hugo Raniere (Motorola)     - Removing Preprocessor code
 *     Diego Sandin (Motorola)     - Re-enabling Preprocessor code
 *     David Marques (Motorola)    - Adding a workspace ResourceChangeListener.
 *     David Marques (Motorola)    - Adding getResourceAsStream method.
 *     Gustavo de Paula (Motorola) - Add preverifier factory     
 *     Gustavo de Paula (Motorola) - Add mtj project converter     
 */
package org.eclipse.mtj.core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.IMetaData;
import org.eclipse.mtj.core.project.ProjectType;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.IDeviceFinder;
import org.eclipse.mtj.core.sdk.device.IDeviceRegistry;
import org.eclipse.mtj.core.sdk.device.midp.library.ILibrary;
import org.eclipse.mtj.core.sdk.device.midp.library.ILibraryImporter;
import org.eclipse.mtj.core.sdk.device.midp.library.ILibrarySpecification;
import org.eclipse.mtj.core.symbol.ISymbolSetFactory;
import org.eclipse.mtj.core.symbol.ISymbolSetRegistry;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.PreprocessedProjectMigrationRunnable;
import org.eclipse.mtj.internal.core.build.preprocessor.PreprocessedSourceMapper;
import org.eclipse.mtj.internal.core.build.preverifier.StandardPreverifierFactory;
import org.eclipse.mtj.internal.core.hook.sourceMapper.SourceMapperAccess;
import org.eclipse.mtj.internal.core.project.UnknownMetaData;
import org.eclipse.mtj.internal.core.project.midp.MetaData;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.sdk.device.DeviceClasspath;
import org.eclipse.mtj.internal.core.sdk.device.DeviceFinder;
import org.eclipse.mtj.internal.core.sdk.device.DeviceRegistry;
import org.eclipse.mtj.internal.core.sdk.device.midp.library.Library;
import org.eclipse.mtj.internal.core.sdk.device.midp.library.LibrarySpecification;
import org.eclipse.mtj.internal.core.sdk.device.midp.library.UEILibraryImporter;
import org.eclipse.mtj.internal.core.symbol.SymbolSetFactory;
import org.eclipse.mtj.internal.core.symbol.SymbolSetRegistry;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.Version;

/**
 * The main plug-in class to be used in the workbench.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 */
public class MTJCore extends Plugin implements IMTJCoreConstants {

    /**
     * This job is responsible for walking through the workspace projects and
     * migrates the preprocessor natures and builders enabling that the
     * preprocessor works within the project rather than requiring the secondary
     * project
     */
    private class MigrationJob extends Job {

        /** Constructor */
        MigrationJob() {
            super(Messages.MTJCore_MigrationJob_title);
            setPriority(SHORT);
            setSystem(true);
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.IProgressMonitor)
         */
        @Override
        protected IStatus run(IProgressMonitor monitor) {
            IStatus status = null;

            try {
                doMigration(monitor);
                status = OK_STATUS;
            } catch (CoreException e) {
                status = e.getStatus();
            }

            return status;
        }
    }

    /**
     * Status code for which a UI prompter is registered.
     */
    public static final IStatus OK_STATUS = new Status(IStatus.OK,
            IMTJCoreConstants.PLUGIN_ID, 0, Messages.MTJCore_OK_STATUS_msg,
            null);

    // The shared instance.
    private static MTJCore plugin;

    /**
     * Status code for which a UI prompter is registered.
     */
    private static final IStatus PROMPTER_STATUS = new Status(IStatus.INFO,
            "org.eclipse.debug.ui", // TODO This should probably not be done.... //$NON-NLS-1$
            200, "", null); //$NON-NLS-1$

    /**
     * @param identifier
     * @param name
     * @param version
     * @return
     */
    public static ILibrarySpecification createLibrarySpecification(
            String identifier, String name, Version version) {
        return new LibrarySpecification(identifier, name, version);
    }

    /**
     * @return
     */
    public static IDeviceClasspath createNewDeviceClasspath() {
        return new DeviceClasspath();
    }

    /**
     * Create a new {@link ILibrary} instance.
     * 
     * @return the new {@link ILibrary}.
     */
    public static ILibrary createNewLibrary() {
        return new Library();
    }

    /**
     * Create a new Preverifier instance based on the preverifier type. Returns
     * <code>null</code> if the preverifier cannot be created for some reason.
     * 
     * @param preverifierType type of the preverifier to be created
     * @param param Any parameter that is required by each preverifier type.
     *            Please check PreverifierType for information about each type
     *            available and the parameters they accept
     * @return the preverifier instance or <code>null</code> if the preverifier
     *         cannot be created for some reason.
     * @throws CoreException if failed to get the preverifier parameters.
     */
    public static IPreverifier createPreverifier(String preverifierType,
            Object param) throws CoreException {
        IPreverifier preverifier = null;

        if (preverifierType.equals(IPreverifier.PREVERIFIER_STANDARD)) {
            if (param instanceof File) {
                preverifier = StandardPreverifierFactory
                        .createPreverifier((File) param);
            }
        }

        return preverifier;
    }

    /**
     * Returns the symbol set registry that can be used to manage the symbols
     * that are saved on the workspace
     * 
     * @return symbol set registry
     */
    public static ISymbolSetRegistry getSymbolSetRegistry() {
        return SymbolSetRegistry.getInstance();
    }

    /**
     * Return the {@link IDeviceFinder} instance that can be used to find for
     * devices in a specific directory.
     * 
     * @return the {@link IDeviceFinder} instance.
     */
    public static IDeviceFinder getDeviceFinder() {
        return DeviceFinder.getInstance();
    }

    /**
     * Returns the symbol set registry that can be used to manage the symbols
     * that are saved on the workspace
     * 
     * @return symbol set registry
     */
    public static ISymbolSetFactory getSymbolSetFactory() {
        return SymbolSetFactory.getInstance();
    }

    /**
     * Returns the shared instance.
     */
    public static MTJCore getDefault() {
        return plugin;
    }

    /**
     * Get the deployment directory name the user has specified in the
     * preferences.
     * 
     * @return
     */
    public static String getDeploymentDirectoryName() {
        return getDefault().getPluginPreferences().getString(
                PREF_DEPLOYMENT_DIR);
    }

    /**
     * @return
     */
    public static IDeviceRegistry getDeviceRegistry() {
        return DeviceRegistry.getInstance();
    }

    /**
     * Returns a library importer associated to a specific type. The library
     * importer is used to importer the libraries defined by each sdk
     * 
     * @param type library importer type
     * @return library importer
     */
    public static ILibraryImporter getLibraryImporter(String type) {
        ILibraryImporter result = null;

        if (type.equals(ILibraryImporter.LIBRARY_IMPORTER_UEI)) {
            result = new UEILibraryImporter();
        }
        return result;
    }

    /**
     * @param project
     * @return
     */
    public static IMetaData getMetaData(IProject project,
            ProjectType projectType) {

        switch (projectType) {
            case MIDLET_SUITE:
                return new MetaData(project);

            default:
                return new UnknownMetaData();
        }
    }

    /**
     * Return the current version associated with this plug-in.
     * 
     * @return the plug-in version
     */
    public static String getPluginVersion() {
        Bundle bundle = MTJCore.getDefault().getBundle();
        return (String) bundle.getHeaders().get(Constants.BUNDLE_VERSION);
    }

    /**
     * Return the File instance representing the Proguard implementation jar
     * file as defined by the user preferences. This File is not guaranteed to
     * exist.
     * 
     * @return
     */
    public static File getProguardJarFile() {
        String proguardDirPref = getDefault().getPluginPreferences().getString(
                PREF_PROGUARD_DIR);
        File proguardDir = new File(proguardDirPref);
        File proguardLibDir = new File(proguardDir, "lib"); //$NON-NLS-1$
        File proguardJar = new File(proguardLibDir, PROGUARD_JAR);

        return proguardJar;
    }

    /**
     * Return a boolean preference scoped to the specified project where
     * possible, otherwise falling back to instance scope.
     * 
     * @param project
     * @param key
     * @return
     */
    public static boolean getProjectBooleanPreference(IProject project,
            String key) {
        IScopeContext[] searchContexts = new IScopeContext[] {
                new ProjectScope(project), new InstanceScope(),
                new DefaultScope(), };

        IPreferencesService service = Platform.getPreferencesService();
        return service.getBoolean(IMTJCoreConstants.PLUGIN_ID, key, false,
                searchContexts);
    }

    /**
     * Return the preferences that are specific to the project and plugin.
     * 
     * @param context
     * @return
     */
    public static IEclipsePreferences getProjectPreferences(IProject context) {
        ProjectScope projectScope = new ProjectScope(context);
        return projectScope.getNode(IMTJCoreConstants.PLUGIN_ID);
    }

    /**
     * Return a String preference scoped to the specified project where
     * possible, otherwise falling back to instance scope.
     * 
     * @param project
     * @param key
     * @return
     */
    public static String getProjectStringPreference(IProject project, String key) {
        IScopeContext[] searchContexts = new IScopeContext[] {
                new ProjectScope(project), new InstanceScope(),
                new DefaultScope(), };

        IPreferencesService service = Platform.getPreferencesService();
        return service.getString(IMTJCoreConstants.PLUGIN_ID, key, null,
                searchContexts);
    }

    /**
     * Gets the InputStream for a resource inside the plug-in.
     * 
     * @param path relative resource path.
     * @return the resource InputStream.
     */
    public static InputStream getResourceAsStream(IPath path) {
        InputStream stream = null;

        URL url = FileLocator.find(getDefault().getBundle(), path, null);
        try {
            stream = url.openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream;
    }

    /**
     * Get the resources directory name the user has specified in the
     * preferences. This directory will automatically be added to the emulator
     * classpath when running MIDlets from source.
     * 
     * @return
     */
    public static String getResourcesDirectoryName() {
        return getDefault().getPluginPreferences()
                .getString(PREF_RESOURCES_DIR);
    }

    /**
     * Get the verified output directory name the user has specified in the
     * preferences.
     * 
     * @return
     */
    public static String getVerifiedOutputDirectoryName() {
        return getDefault().getPluginPreferences().getString(PREF_VERIFIED_DIR);
    }

    /**
     * Returns the workspace instance.
     */
    public static IWorkspace getWorkspace() {
        return ResourcesPlugin.getWorkspace();
    }

    /**
     * Log the specified message.
     * 
     * @param severity
     * @param message
     */
    public static void log(int severity, String message) {
        log(severity, message, null);
    }

    /**
     * Log the specified message and exception
     * 
     * @param severity
     * @param message
     * @param throwable
     */
    public static void log(int severity, String message, Throwable throwable) {
        if (message == null) {
            message = throwable.getMessage();
        }
        if (message == null) {
            message = Messages.MTJCore_no_message;
        }

        MTJCore plugin = MTJCore.getDefault();
        String id = IMTJCoreConstants.PLUGIN_ID;
        Status status = new Status(severity, id, IStatus.OK, message, throwable);
        plugin.getLog().log(status);
    }

    /**
     * Log the specified exception.
     * 
     * @param severity
     * @param throwable
     */
    public static void log(int severity, Throwable throwable) {
        log(severity, throwable.getMessage(), throwable);
    }

    /**
     * Creates a new status object for our plug-in. The created status has no
     * children.
     * 
     * @param severity the severity; one of <code>OK</code>, <code>ERROR</code>,
     *            <code>INFO</code>, or <code>WARNING</code>
     * @param code the plug-in-specific status code, or <code>OK</code>
     * @param message a human-readable message, localized to the current locale
     */
    public static IStatus newStatus(int severity, int code, String message) {
        return newStatus(severity, code, message, null);
    }

    /**
     * Creates a new status object for our plug-in. The created status has no
     * children.
     * 
     * @param severity the severity; one of <code>OK</code>, <code>ERROR</code>,
     *            <code>INFO</code>, or <code>WARNING</code>
     * @param code the plug-in-specific status code, or <code>OK</code>
     * @param message a human-readable message, localized to the current locale
     * @param exception a low-level exception, or <code>null</code> if not
     *            applicable
     */
    public static IStatus newStatus(int severity, int code, String message,
            Throwable exception) {
        return new Status(severity, PLUGIN_ID, code, message, exception);
    }

    /**
     * Recursively set the resources in the specified container and all
     * resources within that container as derived.
     * 
     * @param container
     * @throws CoreException
     */
    public static void setResourcesAsDerived(IContainer container)
            throws CoreException {
        if (container.exists()) {
            // Mark this folder first...
            container.setDerived(true);

            // Recursively handle the members of the directory
            IResource[] resources = container.members();
            for (IResource resource : resources) {
                if (resource instanceof IContainer) {
                    setResourcesAsDerived((IContainer) resource);
                } else {
                    resource.setDerived(true);
                }
            }
        }
    }

    /**
     * Attempt to prompt on a status object. If prompting fails, a CoreException
     * will be thrown.
     * 
     * @param status
     * @param source
     * @return
     * @throws CoreException
     */
    public static Object statusPrompt(IStatus status, Object source)
            throws CoreException {
        Object result = null;

        IStatusHandler prompterStatus = DebugPlugin.getDefault()
                .getStatusHandler(PROMPTER_STATUS);

        if (prompterStatus == null) {
            // if there is no handler, throw the exception
            throw new CoreException(status);
        } else {
            result = prompterStatus.handleStatus(status, source);
        }

        return result;
    }

    /**
     * Throw a new CoreException wrapped around the specified String.
     * 
     * @param severity
     * @param code
     * @param message
     * @throws CoreException
     */
    public static void throwCoreException(int severity, int code, String message)
            throws CoreException {
        if (message == null) {
            message = Messages.MTJCore_no_message2;
        }

        IStatus status = new Status(severity, IMTJCoreConstants.PLUGIN_ID,
                code, message, null);
        throw new CoreException(status);
    }

    /**
     * Throw a new CoreException wrapped around the specified exception.
     * 
     * @param severity
     * @param code
     * @param exception
     * @throws CoreException
     */
    public static void throwCoreException(int severity, int code,
            Throwable exception) throws CoreException {
        // Make sure we create a valid status object
        String message = null;
        if (exception != null) {
            message = exception.getMessage();
        }
        if (message == null) {
            message = Messages.MTJCore_no_message2;
        }

        IStatus status = new Status(severity, IMTJCoreConstants.PLUGIN_ID,
                code, message, exception);
        throw new CoreException(status);
    }

    /**
     * The constructor.
     * 
     * @noreference This constructor is not intended to be referenced by
     *              clients.
     */
    public MTJCore() {
        super();

        if (plugin == null) {
            plugin = this;
        }

        // Setting MidletSuiteFactory as the workspace ResourceChangeListener.
        ResourcesPlugin.getWorkspace().addResourceChangeListener(
                new IResourceChangeListener() {
                    /* (non-Javadoc)
                    * @see org.eclipse.core.resources.IResourceChangeListener#resourceChanged(org.eclipse.core.resources.IResourceChangeEvent)
                    */
                    public void resourceChanged(IResourceChangeEvent event) {
                        switch (event.getType()) {
                            case IResourceChangeEvent.PRE_DELETE:
                                IResource resource = event.getResource();
                                if (resource instanceof IProject) {
                                    IProject project = (IProject) resource;
                                    /*Removes IMidletSuiteProject instances from cache upon project deletion.*/
                                    IMTJProject meProject = MidletSuiteFactory
                                            .getMidletSuiteProject(project
                                                    .getName());
                                    if (meProject != null) {
                                        MidletSuiteFactory
                                                .removeMidletSuiteProject(meProject
                                                        .getJavaProject());
                                    }
                                }
                                break;
                        }
                    }
                });
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.runtime.Plugin#start(org.osgi.framework.BundleContext)
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);

        // Install the preprocessor source mapper
        SourceMapperAccess.setSourceMapper(new PreprocessedSourceMapper());

        // Do version to version migration
        (new MigrationJob()).schedule(5000L);
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        super.stop(context);
    }

    /**
     * Execute the necessary migration steps.
     * 
     * @param monitor
     * @throws CoreException
     */
    private void doMigration(IProgressMonitor monitor) throws CoreException {
        ResourcesPlugin.getWorkspace().run(
                new PreprocessedProjectMigrationRunnable(), monitor);
    }
}
