/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.build.preverifier;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.project.IMTJProject;

/**
 * Required interface for preverification support. Each IPlatformDefinition is
 * required to provide a preverifier instance for use during builds.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IPreverifier extends IPersistable {

	/**
	 * Constant that represents the standard preverifier
	 */
	public static final String PREVERIFIER_STANDARD="org.eclipse.mtj.preverifier.standard";
	
	/**
	 * Constant that represents the unknown preverifier
	 */
	public static final String PREVERIFIER_UNKNOWN="org.eclipse.mtj.preverifier.unknown";
		
    /**
     * Launch the preverification process on the specified resources.
     * 
     * @param midletProject The project in which the resources to be
     *            pre-verified reside.
     * @param toVerify The resources to be pre-verified.
     * @param outputFolder The folder into which the output is to be placed.
     * @param monitor Progress monitor
     * @return An array of preverification error instances.
     * @throws CoreException
     * @throws IOException
     */
    public IPreverificationError[] preverify(IMTJProject midletProject,
            IResource[] toVerify, IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException;

    /**
     * Launch the preverification process on the specified jar file.
     * 
     * @param midletProject The project in which the resources to be
     *            pre-verified reside.
     * @param jarFile The jar file to be pre-verified.
     * @param outputFolder The folder into which the output is to be placed.
     * @param monitor Progress monitor
     * @return An array of preverification error instances.
     * @throws CoreException
     * @throws IOException
     */
    public IPreverificationError[] preverifyJarFile(IMTJProject midletProject,
            File jarFile, IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException;

    /**
     * @return
     */
    public File getPreverifierExecutable();

}
