/**
 * Copyright (c) 2004,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.launching;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;

/**
 * The launch environment provides the necessary information to an instance of
 * IDevice for determining the correct command-line for executing the device.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 */
public class LaunchEnvironment {

    private boolean debugLaunch;
    private int debugListenerPort;
    private IMidletSuiteProject midletSuite;
    private ILaunchConfiguration launchConfiguration;

    /**
     * Return the port number to be used for debugging.
     * 
     * @return
     */
    public int getDebugListenerPort() {
        return debugListenerPort;
    }

    /**
     * Return the launch configuration that was used to launch the emulation.
     * 
     * @return
     */
    public ILaunchConfiguration getLaunchConfiguration() {
        return launchConfiguration;
    }

    /**
     * @return Returns the midletSuite.
     */
    public IMidletSuiteProject getMidletSuite() {
        return midletSuite;
    }

    /**
     * Return a boolean indicating whether the launch should be done for debug
     * or not.
     * 
     * @return
     */
    public boolean isDebugLaunch() {
        return debugLaunch;
    }

    /**
     * @param debugLaunch The debugLaunch to set.
     */
    public void setDebugLaunch(boolean debugLaunch) {
        this.debugLaunch = debugLaunch;
    }

    /**
     * @param debugListenerPort The debugListenerPort to set.
     */
    public void setDebugListenerPort(int debugListenerPort) {
        this.debugListenerPort = debugListenerPort;
    }

    /**
     * @param launchConfiguration The launchConfiguration to set.
     */
    public void setLaunchConfiguration(ILaunchConfiguration launchConfiguration) {
        this.launchConfiguration = launchConfiguration;
    }

    /**
     * @param midletSuite The midletSuite to set.
     */
    public void setMidletSuite(IMidletSuiteProject midletSuite) {
        this.midletSuite = midletSuite;
    }
}
