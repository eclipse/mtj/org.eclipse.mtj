/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.project.runtime.event;

import java.util.EventObject;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;

/**
 * SwitchActiveMTJRuntimeEvent is used to notify that active Configuration for a
 * Midlet project switched.
 * 
 * @since 1.0
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class SwitchActiveMTJRuntimeEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    private MTJRuntime oldActiveConfig;
    private MTJRuntime newActiveConfig;

    /**
     * Constructs a SwitchActiveMTJRuntimeEvent object.
     * 
     * @param source - Configuration instance of the MTJ Midlet project.
     * @param oldActiveConfig - the old active configuration.
     * @param newActiveConfig - the new active configuration.
     */
    public SwitchActiveMTJRuntimeEvent(MTJRuntimeList source,
            MTJRuntime oldActiveConfig, MTJRuntime newActiveConfig) {
        super(source);
        this.oldActiveConfig = oldActiveConfig;
        this.newActiveConfig = newActiveConfig;
    }

    public MTJRuntime getNewActiveMTJRuntime() {
        return newActiveConfig;
    }

    public MTJRuntime getOldActiveMTJRuntime() {
        return oldActiveConfig;
    }

    public void setNewActiveMTJRuntime(MTJRuntime newActiveConfig) {
        this.newActiveConfig = newActiveConfig;
    }

    public void setOldActiveMTJRuntime(MTJRuntime oldActiveConfig) {
        this.oldActiveConfig = oldActiveConfig;
    }

}
