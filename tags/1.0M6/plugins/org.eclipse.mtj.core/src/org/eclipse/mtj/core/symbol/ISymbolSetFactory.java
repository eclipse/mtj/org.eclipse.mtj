/**
 * Copyright (c) 2008, 2009 Motorola Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gustavo de Paula (Motorola) - Initial implementation
 */
package org.eclipse.mtj.core.symbol;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.sdk.device.IDevice;

/**
 * <p>
 * This API provides a mechanism to create symbols set. Two different ways are available:
 * create from a device and create from a j2mepolish device file. The first one will be based
 * on the device properties and the device libraries. The later is based on j2mepolish device
 * format and will create an array of symbol sets for each device that is available on the database
 * 
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface ISymbolSetFactory {

	/**
	 * Constant that represents the type j2mepolish from antenna jar
	 */
    public static final String DEVICE_DB_J2MEPOLISH_JAR = "org.eclipse.mtj.davicedb.j2mepolish.antennajar"; //$NON-NLS-1$
	
	/**
	 * Constant that represents the type j2mepolish from file
	 */
    public static final String DEVICE_DB_J2MEPOLISH_FILE = "org.eclipse.mtj.davicedb.j2mepolish.file"; //$NON-NLS-1$
    
	/**
	 * Creates a list of symbol sets based on a device data base. the device database 
	 * is specified via an input URI and a type. Currently MTJ supports only J2MEPolish
	 * database
	 * @param type a string that represents the type of the database to be imported
	 * @param input input URI of where the database is
	 * @param monitor  Progress monitor, inform user about process of import.
	 *            Null is allowed.
	 * 
	 * @return List of symbol set
	 * @throws IOException
	 */
	public List<ISymbolSet> createSymbolSetFromDataBase(String type, URI input, IProgressMonitor monitor) throws IOException;
	
	/**
	 * Create SymbolSet from device. The content of the symbolset will be based on the device properties and also on the libraries that
	 * the device has
	 * 
	 * @param device
	 * @return created symbol set
	 */
	public ISymbolSet createSymbolSetFromDevice(IDevice device);

	/**
	 * Create a symbol set with a predefined name
	 * 
	 * @param name symbol set name
	 * @return created symbol set
	 */
	public ISymbolSet createSymbolSet(String name);
	
	/**
	 * Creates a new symbol based on the symbol name and value
	 * 
	 * @param name symbol name
	 * @param value symbol value
	 * @return ISymbol that was just created
	 */
	public ISymbol createSymbol(String name, String value);
	
}