/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Gustavo de Paula (Motorola) - Runtime refactoring
 */
package org.eclipse.mtj.core.project;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.osgi.framework.Version;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noimplement This class is not intended to be implemented by clients.
 * @since 1.0
 */
public interface IMetaData {

    /**
     * The metadata file name for the MIDlet project
     */
    public static final String METADATA_FILE = ".mtj"; //$NON-NLS-1$

    /**
     * Return the list of runtimes that are associated to the project. From the
     * list it is possible to read each runtime and information such as the
     * device of each runtime
     * 
     * @return MTJRuntimeList list of MTJRuntime
     */
    public abstract MTJRuntimeList getRuntime();

    /**
     * @return
     * @throws CoreException
     */
    public abstract ISignatureProperties getSignatureProperties()
            throws CoreException;

    /**
     * @return Returns the version.
     */
    public abstract Version getVersion();

    /**
     * Attempt to save the metadata. This has the potential to fail.
     * 
     * @throws CoreException
     */
    public abstract void saveMetaData() throws CoreException;

    /**
     * Save the metadata to the storage file. This file is expected to be
     * non-null.
     * 
     * @param storeFile
     * @throws CoreException
     */
    public abstract void saveMetaDataToFile(IFile storeFile)
            throws CoreException;

    /**
     * @param configurations
     */
    public abstract void setMTJRuntimeList(MTJRuntimeList configurations);


    /**
     * @param p
     */
    public abstract void setSignatureProperties(ISignatureProperties p);

}