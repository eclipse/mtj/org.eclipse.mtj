/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.core.project.runtime.event;

import java.util.EventObject;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;

/**
 * RemoveMTJRuntimeEvent is used to notify that a Configuration is removed from
 * Configuration.
 * 
 * @since 1.0
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class RemoveMTJRuntimeEvent extends EventObject {
    private static final long serialVersionUID = 1L;

    private MTJRuntime removedConfig;

    /**
     * Constructs a AddConfigEvent object.
     * 
     * @param source - Configuration instance of the MTJ Midlet project.
     * @param removedConfig - the removed configuration.
     */
    public RemoveMTJRuntimeEvent(MTJRuntimeList source, MTJRuntime removedConfig) {
        super(source);
        this.removedConfig = removedConfig;
    }

    public MTJRuntime getRemovedMTJRuntime() {
        return removedConfig;
    }

    public void setRemovedMTJRuntime(MTJRuntime removedConfig) {
        this.removedConfig = removedConfig;
    }

}
