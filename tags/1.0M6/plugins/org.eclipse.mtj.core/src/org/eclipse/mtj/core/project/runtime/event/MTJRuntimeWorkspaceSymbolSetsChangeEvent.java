/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.project.runtime.event;

import java.util.EventObject;
import java.util.List;

import org.eclipse.mtj.core.symbol.ISymbolSet;

/**
 * MTJRuntimeWorkspaceSymbolSetsChangeEvent is used to notify that the
 * workspaceScopeSymbolSets of the configuration changed.
 * 
 * @since 1.0
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class MTJRuntimeWorkspaceSymbolSetsChangeEvent extends EventObject {

    private static final long serialVersionUID = 1L;
    private List<ISymbolSet> oldSymbolDefinitionSets;
    private List<ISymbolSet> newSymbolDefinitionSets;

    public MTJRuntimeWorkspaceSymbolSetsChangeEvent(Object source,
            List<ISymbolSet> oldSets, List<ISymbolSet> newSets) {
        super(source);
        this.oldSymbolDefinitionSets = oldSets;
        this.newSymbolDefinitionSets = newSets;
    }

    public List<ISymbolSet> getNewSymbolDefinitionSets() {
        return newSymbolDefinitionSets;
    }

    public List<ISymbolSet> getOldSymbolDefinitionSets() {
        return oldSymbolDefinitionSets;
    }

    public void setNewSymbolDefinitionSets(
            List<ISymbolSet> newSymbolDefinitionSets) {
        this.newSymbolDefinitionSets = newSymbolDefinitionSets;
    }

    public void setOldSymbolDefinitionSets(
            List<ISymbolSet> oldSymbolDefinitionSets) {
        this.oldSymbolDefinitionSets = oldSymbolDefinitionSets;
    }

}
