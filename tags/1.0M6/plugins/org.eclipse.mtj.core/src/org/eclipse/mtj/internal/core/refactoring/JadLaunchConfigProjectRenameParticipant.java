/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.core.refactoring;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RenameParticipant;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.launching.ILaunchConstants;
import org.eclipse.mtj.internal.core.launching.midp.IMIDPLaunchConstants;

/**
 * JAD launch configuration participant in project rename refactoring
 * operations.
 * 
 * @author wangf
 */
public class JadLaunchConfigProjectRenameParticipant extends RenameParticipant {
    /**
     * the project to rename
     */
    private IJavaProject fJavaProject;

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#
     * initialize(java.lang.Object)
     */
    @Override
    protected boolean initialize(Object element) {
        fJavaProject = (IJavaProject) element;
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#
     * getName()
     */
    @Override
    public String getName() {
        return RefactoringMessages.JadLaunchConfigParticipant_name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#
     * checkConditions(org.eclipse.core.runtime.IProgressMonitor,
     * org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext)
     */
    @Override
    public RefactoringStatus checkConditions(IProgressMonitor pm,
            CheckConditionsContext context) throws OperationCanceledException {
        return new RefactoringStatus();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#
     * createChange(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public Change createChange(IProgressMonitor pm) throws CoreException,
            OperationCanceledException {
        List<JadLaunchConfigProjectRenameChange> changeList = new ArrayList<JadLaunchConfigProjectRenameChange>();
        ILaunchConfiguration[] configs = getRelatedJadLaunchConfigs(fJavaProject
                .getElementName());
        // create changes
        for (ILaunchConfiguration config : configs) {
            changeList.add(new JadLaunchConfigProjectRenameChange(config,
                    getArguments().getNewName()));
        }
        // return changes
        int numOfChanges = changeList.size();
        if (numOfChanges == 0) {
            return null;
        } else if (numOfChanges == 1) {
            return changeList.get(0);
        } else {
            return new CompositeChange(
                    RefactoringMessages.JadLaunchConfigCompositeChange_name,
                    changeList.toArray(new Change[changeList.size()]));
        }
    }

    /**
     * Returns a listing of JAD launch configurations that have a specific
     * project name attribute in them
     * 
     * @param projectName the project attribute to compare against
     * @return the list of "MTJ.emulatorLaunchConfigurationType" type launch
     *         configurations that have the specified project attribute
     */
    private ILaunchConfiguration[] getRelatedJadLaunchConfigs(String projectName) {
        if (projectName == null || projectName.trim().length() == 0) {
            return new ILaunchConfiguration[0];
        }
        try {
            ILaunchConfiguration[] configs = DebugPlugin.getDefault()
                    .getLaunchManager().getLaunchConfigurations(
                            getEmulatorConfigType());
            ArrayList<ILaunchConfiguration> list = new ArrayList<ILaunchConfiguration>();
            // collect relative configs
            for (int i = 0; i < configs.length; i++) {
                String projectNameAttribute = configs[i].getAttribute(
                        IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
                        (String) null);
                boolean doJadLaunch = configs[i].getAttribute(
                        IMIDPLaunchConstants.DO_JAD_LAUNCH, false);
                boolean projectNameEqual = projectName
                        .equals(projectNameAttribute);
                if (doJadLaunch && projectNameEqual) {
                    list.add(configs[i]);
                }
            }

            return list.toArray(new ILaunchConfiguration[list.size()]);
        } catch (CoreException e) {
            MTJCore.log(IStatus.ERROR, e);
        }
        return new ILaunchConfiguration[0];
    }

    /**
     * Get the launch configuration type for wireless toolkit emulator.
     * 
     * @return
     */
    private ILaunchConfigurationType getEmulatorConfigType() {
        ILaunchManager lm = DebugPlugin.getDefault().getLaunchManager();
        ILaunchConfigurationType configType = lm
                .getLaunchConfigurationType(ILaunchConstants.LAUNCH_CONFIG_TYPE);
        return configType;
    }
}
