/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk.device;

import java.io.File;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

/**
 * The device factory allows access to creation of devices based on the
 * available registered device importers.
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IDeviceFinder {

    /**
     * Return all devices that can be found in the specified directory or any
     * sub-directories. This method will consult the IDeviceImporter
     * implementation that have been registered with the informed
     * deviceImporterID.
     * 
     * @param deviceImporterID
     * @param directory
     * @param monitor
     * @return
     * @throws CoreException
     * @throws InterruptedException if the operation detects a request to
     *             cancel, using IProgressMonitor.isCanceled(), it should exit
     *             by throwing InterruptedException.
     */
    public List<IDevice> findDevices(final String deviceImporterID,
            File directory, IProgressMonitor monitor) throws CoreException,
            InterruptedException;

    /**
     * Return all devices that can be found in the specified directory or any
     * sub-directories. This method will consult all registered IDeviceImporter
     * implementations that have been registered with the system.
     * 
     * @param directory
     * @param monitor
     * @throws CoreException
     * @throws InterruptedException if the operation detects a request to
     *             cancel, using IProgressMonitor.isCanceled(), it should exit
     *             by throwing InterruptedException.
     */
    public List<IDevice> findDevices(File directory, IProgressMonitor monitor)
            throws CoreException, InterruptedException;
}