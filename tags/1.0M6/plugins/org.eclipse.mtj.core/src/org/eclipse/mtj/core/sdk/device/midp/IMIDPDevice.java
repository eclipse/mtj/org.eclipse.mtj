/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk.device.midp;

import java.io.File;

import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.midp.library.ILibrary;

/**
 * The device interface specifies the representation of an emulated MIDP device.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface IMIDPDevice extends IDevice {

    /**
     * Return the library that provides the configuration for this device or
     * <code>null</code> if no such library can be found.
     * 
     * @return
     */
    public abstract ILibrary getConfigurationLibrary();

    /**
     * Return the preverifier to be used to preverify classes for this device.
     * 
     * @return the platform's preverifier
     */
    public abstract IPreverifier getPreverifier();

    /**
     * Return the library that provides the profile for this device or
     * <code>null</code> if no such library can be found.
     * 
     * @return
     */
    public abstract ILibrary getProfileLibrary();

    /**
     * Return the list of protection domains specified by this device. Returning
     * <code>null</code> from this method will imply that this device does not
     * support protection domains.
     * 
     * @return the list of protection domains or <code>null</code> if the device
     *         does not provide any protection domains.
     */
    public abstract String[] getProtectionDomains();

    /**
     * Return the working directory to be used when launching the device
     * emulation.
     * 
     * @return
     */
    public File getWorkingDirectory();

}