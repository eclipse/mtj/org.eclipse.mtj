/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Extracted interface
 *     Gustavo de Paula (Motorola)  - Preverification refactoring     
 */
package org.eclipse.mtj.core.build.preverifier;

/**
 * The description of an error that occurred during preverification.
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IPreverificationError {

    /**
     * Returns the detailed message concerning this error or <code>null</code>
     * if no detail message has been specified.
     * 
     * @return Returns the detail.
     */
    public String getDetail();

    /**
     * Returns the location where the error occurred.
     * 
     * @return Returns the error location.
     */
    public Object getLocation();

    /**
     * Returns the type of the error.
     * 
     * @return Returns the error type.
     */
    public Object getType();

}