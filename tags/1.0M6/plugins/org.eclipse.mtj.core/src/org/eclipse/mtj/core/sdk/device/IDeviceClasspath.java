/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk.device;

import java.io.File;
import java.net.URL;

import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.sdk.device.midp.library.ILibrary;

/**
 * <p>
 * This interface represents the classpath that is associated to one specific device.
 * The classpath has a list of ILibrary and each ILibrary is associated to a set of APIs.
 * Based on that is is possible to identify all apis thats are support on each device.
 * </p>
 * @see ILibrary
 * @see API
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IDeviceClasspath extends IPersistable {

    /**
     * Add a new deviceClasspath entry.
     * 
     * @param pathEntry
     */
    public abstract void addEntry(ILibrary pathEntry);

    /**
     * Return the deviceClasspath as a set of Eclipse JDT deviceClasspath
     * entries.
     * 
     * @return
     */
    public abstract IClasspathEntry[] asClasspathEntries();

    /**
     * Return the deviceClasspath as an array of the file entries in the
     * deviceClasspath.
     * 
     * @return
     */
    public abstract File[] asFileArray();

    /**
     * Return the deviceClasspath as an array of URL's.
     * 
     * @return
     */
    public abstract URL[] asURLArray();

    /**
     * Test the equality of this class with another deviceClasspath.
     * 
     * @param deviceClasspath
     * @return
     */
    public abstract boolean equals(IDeviceClasspath deviceClasspath);

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public abstract boolean equals(Object obj);

    /**
     * Return the entries in the deviceClasspath.
     * 
     * @return
     */
    public abstract ILibrary[] getEntries();

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public abstract int hashCode();

    /**
     * Remove the specified library from the deviceClasspath, if it is in the
     * deviceClasspath.
     * 
     * @param library
     */
    public abstract void removeEntry(ILibrary library);

    /**
     * <p>
     * Converts the deviceClasspath to a deviceClasspath string with
     * platform-dependent path separator characters.
     * </p> {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    public abstract String toString();

}