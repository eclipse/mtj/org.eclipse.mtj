/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Added ChangeListener capabilities
 *     Hugo Raniere (Motorola)  - Fixing key retrieval while loading sets from xml
 *     Ales Milan - Add import symbol sets from J2ME Polish devices.xml and 
 *                  groups.xml files support.
 */
package org.eclipse.mtj.internal.core.symbol;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.core.symbol.ISymbolSetChangeListener;
import org.eclipse.mtj.core.symbol.ISymbolSetRegistry;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.persistence.XMLPersistenceProvider;
import org.eclipse.mtj.internal.core.util.migration.IMigration;
import org.eclipse.mtj.internal.core.util.migration.SymbolDefinitionsMigration;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Provides a registry of SymbolDefinitionSet instances for use by various parts
 * of the core system.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 * @noinstantiate This class is not intended to be instantiated by clients.
 */
public class SymbolSetRegistry implements ISymbolSetRegistry,
        ISymbolSetConstants {

    /**
     * Singleton instance of the registry
     */
    private static final SymbolSetRegistry instance = new SymbolSetRegistry();

    // The filename used to store and retrieve the symbol definitions registry
    private static final String FILENAME = "symbolDefinitions.xml"; //$NON-NLS-1$

    // The actual mapping of symbol definition instances
    private Map<String, IPersistable> registryMap;

    List<ISymbolSetChangeListener> listeners = new ArrayList<ISymbolSetChangeListener>();

    /**
     * Private constructor for singleton access.
     */
    private SymbolSetRegistry() {
    }

    /**
     * Retuns singleton instance
     * 
     * @return singleton instance
     */
    public static ISymbolSetRegistry getInstance () {
    	return instance;
    }
    

	public void addSymbolSet(ISymbolSet definitions)
			throws PersistenceException {
        String name = definitions.getName();
        if (name == null) {
            throw new IllegalStateException(
                    Messages.SymbolDefinitionSetRegistry_addDefinitionSet_no_name_exception);
        }

        getRegistryMap().put(definitions.getName(), definitions);
        notifyListeners();
	}

	public void addSymbolSetChangeListener(ISymbolSetChangeListener listener) {
        listeners.add(listener);
	}

	public ISymbolSet getSymbolSet(String name) throws PersistenceException {
		return (ISymbolSet)getRegistryMap().get(name);
	}

	public void removeSymbolSet(String setName) {
		registryMap.remove(setName);
        notifyListeners();
	}

	public void removeSymbolSetChangeListener(
			ISymbolSetChangeListener listener) {
		listeners.remove(listener);
		
	}

	public void clear() throws PersistenceException {
		getRegistryMap().clear();
	}

	public String[] getAllSymbolSetNames() throws PersistenceException {
        Set<String> keys = registryMap.keySet();
        return keys.toArray(new String[keys.size()]);
	}

	public ISymbolSet[] getAllSymbolSets()
			throws PersistenceException {
        Collection<?> definitions = getRegistryMap().values();
        return definitions.toArray(new SymbolSet[definitions.size()]);
	}

	public void loadUsing(IPersistenceProvider persistenceProvider)
			throws PersistenceException {
        if (registryMap != null) {
            String keysString = persistenceProvider
                    .loadString(ELEMENT_KEYS_KEY);
            if ((keysString != null) && (keysString.length() > 0)) {
                String[] keys = keysString.split(","); //$NON-NLS-1$

                for (String key : keys) {
                    ISymbolSet definitions = (ISymbolSet) persistenceProvider
                            .loadPersistable(key);
                    registryMap.put(definitions.getName(), definitions);
                }
            }
        }
	}

	public void storeUsing(IPersistenceProvider persistenceProvider)
			throws PersistenceException {
        StringBuffer keyString = new StringBuffer();

        Iterator<?> definitions = getRegistryMap().values().iterator();
        while (definitions.hasNext()) {
            SymbolSet definition = (SymbolSet) definitions
                    .next();

            String name = definition.getStorableName();
            persistenceProvider.storePersistable(name, definition);
            keyString.append(name);
            if (definitions.hasNext()) {
                keyString.append(","); //$NON-NLS-1$
            }
        }

        persistenceProvider.storeString(ELEMENT_KEYS_KEY, keyString.toString());
	}
	
    /**
     * Return the registry map. The map will be loaded from persistent storage
     * if this is the first creation.
     * 
     * @return
     * @throws PersistenceException
     */
    private synchronized Map<String, IPersistable> getRegistryMap()
            throws PersistenceException {
        if (registryMap == null) {
            registryMap = new HashMap<String, IPersistable>();
            load();
        }

        return registryMap;
    }

    /**
     * Get the file used to persist the symbol definitions.
     * 
     * @return
     */
    private File getStoreFile() {
        IPath pluginStatePath = MTJCore.getDefault().getStateLocation();
        IPath storePath = pluginStatePath.append(FILENAME);

        return storePath.toFile();
    }

    /**
     * Notify the modification listeners
     */
    private void notifyListeners() {
        for (ISymbolSetChangeListener listener : listeners) {
            listener.symbolSetChanged();
        }
    }

	public void load() throws PersistenceException {
        if (registryMap != null) {
            registryMap.clear();
        }
        File storeFile = getStoreFile();
        if (storeFile.exists()) {
            try {
                Document document = XMLUtils.readDocument(storeFile);

                IMigration migration = new SymbolDefinitionsMigration();

                migration.migrate(document);

                if (migration.isMigrated()) {

                    try {
                        XMLUtils.writeDocument(storeFile, document);
                    } catch (TransformerException e) {
                        e.printStackTrace();
                    }

                    document = XMLUtils.readDocument(storeFile);
                }

                XMLPersistenceProvider pprovider = new XMLPersistenceProvider(
                        document);
                loadUsing(pprovider);
            } catch (ParserConfigurationException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (SAXException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (IOException e) {
                throw new PersistenceException(e.getMessage(), e);
            }
        }
	}

	public void store() throws PersistenceException, TransformerException,
			IOException {
        XMLPersistenceProvider pprovider = new XMLPersistenceProvider(
                ELEMENT_SYMBOLDEFINITIONSREGISTRY);
        storeUsing(pprovider);
        XMLUtils.writeDocument(getStoreFile(), pprovider.getDocument());
	}

	public void addSymbolSet(List <ISymbolSet> ss) throws PersistenceException {
		for (ISymbolSet s: ss) {
			this.registryMap.put(s.getName(), s);			
		}
		notifyListeners();
	}

	public void addNewSymbolSet(ISymbolSet ss) throws PersistenceException {
		this.registryMap.put(ss.getName(), ss);
		notifyListeners();
		
	}
	
}
