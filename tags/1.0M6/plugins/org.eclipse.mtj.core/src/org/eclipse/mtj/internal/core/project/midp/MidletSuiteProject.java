/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Re-implementing JavaMEClasspathContainer
 *     Diego Sandin (Motorola)  - Add workaround while embedded preverifier 
 *     							  is not available:
 *                                isEmbeddedPreverifierEnabled will always 
 *                                return false
 *     Hugo Raniere (Motorola)  - Handling the case that there is no valid preverifier
 *     Hugo Raniere (Motorola)  - Using default preverifier if device does not 
 *                                specify one.
 *     Feng Wang (Sybase)       - 1, Implement getConfigurations() method for 
 *                                   multi-configs support
 *                                2, Add packaging for all configurations support
 *                                3, Remove getEnabledSymbolDefinitionSet() method, 
 *                                   since SymbolDefinitionSets are contained 
 *                                   by configuration.
 *     David Marques (Motorola) - Updating getSignatureProperties() method to support
 *                                signing enhancements.
 *     David Marques (Motorola) - Updating addMTJProjectListener to check listener types.
 */
package org.eclipse.mtj.internal.core.project.midp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverificationError;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.build.preverifier.PreverifierNotFoundException;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.project.IMTJProjectListener;
import org.eclipse.mtj.core.project.midp.IApplicationDescriptor;
import org.eclipse.mtj.core.project.midp.IMIDPMetaData;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProjectListener;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.build.preverifier.StandardPreverifier;
import org.eclipse.mtj.internal.core.build.preverifier.builder.PreverificationBuilder;
import org.eclipse.mtj.internal.core.build.sign.PreferencesSignatureProperties;
import org.eclipse.osgi.util.NLS;

/**
 * Implementation of the IMidletSuiteProject interface providing access to
 * MIDlet suite specific information.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */
public class MidletSuiteProject implements IMidletSuiteProject {

    /**
     * The verified sub-directory for classes
     */
    public static final String CLASSES_DIRECTORY = "classes"; //$NON-NLS-1$

    /**
     * The verified sub-directory for libraries
     */
    public static final String LIBS_DIRECTORY = "libs"; //$NON-NLS-1$

    private static final String APP_DESCRIPTOR_FILE_EXT = "jad"; //$NON-NLS-1$

    private static final String DEPLOYED_APP_FILE_EXT = "jar"; //$NON-NLS-1$

    // The up-to-date flag for the deployed jar file
    private static final QualifiedName DEPLOYMENT_UP_TO_DATE_PROP = new QualifiedName(
            IMTJCoreConstants.PLUGIN_ID, "deployed_up_to_date"); //$NON-NLS-1$

    /**
     * Return the default JAD file name for the specified project.
     * 
     * @param project
     * @return
     */
    public static String getDefaultJadFileName(IProject project) {
        String projectName = project.getName();
        return NLS
                .bind(
                        "{0}.{1}", new String[] { projectName.replace(' ', '_'), APP_DESCRIPTOR_FILE_EXT }); //$NON-NLS-1$

    }

    // The java project on which this MIDlet suite is based
    private IJavaProject javaProject;

    // The metadata about this project
    private IMIDPMetaData metaData;

    private String tempKeyPassword;

    private String tempKeystorePassword;

    List<IMidletSuiteProjectListener> listenerList = new ArrayList<IMidletSuiteProjectListener>();

    /**
     * Creates a new instance of MidletSuiteProject.
     * 
     * @param javaProject
     */
    public MidletSuiteProject(IJavaProject javaProject) {
        super();
        this.javaProject = javaProject;
        initializeMetadata();

        if (getMTJRuntime().getActiveMTJRuntime().getDevice() == null) {
            try {
                if (getProject().findMarkers(
                        IMTJCoreConstants.JAVAME_MISSING_DEVICE_MARKER, false,
                        IResource.DEPTH_ZERO).length == 0) {
                    IMarker marker = getProject().createMarker(
                            IMTJCoreConstants.JAVAME_MISSING_DEVICE_MARKER);
                    marker.setAttribute(IMarker.MESSAGE,
                            Messages.MidletSuiteProject_device_not_available);
                    marker.setAttribute(IMarker.SEVERITY,
                            IMarker.SEVERITY_ERROR);

                }
            } catch (CoreException e) {
                e.printStackTrace();
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#addMTJProjectListener(org.eclipse.mtj.core.project.IMTJProjectListener)
     */
    public void addMTJProjectListener(IMTJProjectListener listener) {
    	// Make sure the listener is an instance of IMidletSuiteProjectListener.
    	if (!listenerList.contains(listener) && listener instanceof IMidletSuiteProjectListener) {			
    		listenerList.add((IMidletSuiteProjectListener)listener);
		}
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#createPackage(org.eclipse.core.runtime.IProgressMonitor, boolean, boolean)
     */
    public void createPackage(IProgressMonitor monitor, boolean obfuscate,
            boolean packageInactiveConfigs) throws CoreException {
        Map<String, Boolean> args = new HashMap<String, Boolean>(2);
        args.put(PreverificationBuilder.ARG_DO_PACKAGE, Boolean.TRUE);
        args.put(PreverificationBuilder.ARG_DO_OBFUSCATION, Boolean
                .valueOf(obfuscate));
        args.put(PreverificationBuilder.ARG_UPDATE_VERSION, Boolean.TRUE);

        PreverificationBuilder.cleanProject(getProject(), true, monitor);
        getProject().build(IncrementalProjectBuilder.FULL_BUILD,
                IMTJCoreConstants.J2ME_PREVERIFIER_ID, args, monitor);
        if (packageInactiveConfigs) {
            createPackageForInactiveConfigs(args, monitor);
        }

        for (IMidletSuiteProjectListener midletSuiteProjectListener : listenerList) {
            midletSuiteProjectListener.packageCreated();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProject#getApplicationDescriptor()
     */
    public IApplicationDescriptor getApplicationDescriptor() {

        IApplicationDescriptor descriptor = null;

        IFile jadFile = getApplicationDescriptorFile();
        if (!jadFile.exists()) {
            // Create JAD file
        }

        try {
            File jFile = jadFile.getLocation().toFile();
            descriptor = new ApplicationDescriptor(jFile);
        } catch (IOException e) {
            MTJCore.log(IStatus.ERROR, "getApplicationDescriptor", e); //$NON-NLS-1$
        }

        return descriptor;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProject#getApplicationDescriptorFile()
     */
    public IFile getApplicationDescriptorFile() {
        IFile jadFile = null;
        IProject project = javaProject.getProject();
        jadFile = project.getFile(APPLICATION_DESCRIPTOR_NAME);
        return jadFile;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProject#getJadFileName()
     */
    public String getJadFileName() {
        return metaData.getJadFileName();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProject#getJarFilename()
     */
    public String getJarFilename() {
        String filename = null;

        String jarUrl = getApplicationDescriptor().getMidletJarURL();
        if (jarUrl != null) {
            // If this is really a URL, we want to just get the name
            // from the end of the URL.
            int lastSlashIndex = jarUrl.lastIndexOf('/');
            if (lastSlashIndex != -1) {
                filename = jarUrl.substring(lastSlashIndex + 1);
            } else {
                filename = jarUrl;
            }
        }

        if (filename == null) {
            filename = NLS
                    .bind(
                            "{0}.{1}", new String[] { getProjectNameWithoutSpaces(), DEPLOYED_APP_FILE_EXT }); //$NON-NLS-1$
        }

        return filename;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#getJavaProject()
     */
    public IJavaProject getJavaProject() {
        return javaProject;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#getMTJRuntime()
     */
    public MTJRuntimeList getMTJRuntime() {
        return metaData.getRuntime();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#getProject()
     */
    public IProject getProject() {
        return javaProject.getProject();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#getSignatureProperties()
     */
    public ISignatureProperties getSignatureProperties() throws CoreException {
    	ISignatureProperties result = metaData.getSignatureProperties();
    	// In case the project has no specific signing settings use
    	// properties from preferences.
    	if (result != null && !result.isProjectSpecific()) {
    		// Still uses the signing flag and alias from project
    		// signature properties.
    		boolean signingProject = result.getSignProject();
    		String  projectAlias   = result.getKeyAlias();
    		result = new PreferencesSignatureProperties();
			result.setSignProject(signingProject);
			if (projectAlias != null && projectAlias.length() > 0) {
				result.setKeyAlias(projectAlias);
			}
		}
        return result;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#getTempKeyPassword()
     */
    public String getTempKeyPassword() {
        return (tempKeyPassword);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#getTempKeystorePassword()
     */
    public String getTempKeystorePassword() {
        return (tempKeystorePassword);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#getVerifiedClassesOutputFolder(org.eclipse.core.runtime.IProgressMonitor)
     */
    public IFolder getVerifiedClassesOutputFolder(IProgressMonitor monitor)
            throws CoreException {
        return getVerifiedOutputFolder(monitor).getFolder(CLASSES_DIRECTORY);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#getVerifiedLibrariesOutputFolder(org.eclipse.core.runtime.IProgressMonitor)
     */
    public IFolder getVerifiedLibrariesOutputFolder(IProgressMonitor monitor)
            throws CoreException {
        return getVerifiedOutputFolder(monitor).getFolder(LIBS_DIRECTORY);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#getVerifiedOutputFolder(org.eclipse.core.runtime.IProgressMonitor)
     */
    public IFolder getVerifiedOutputFolder(IProgressMonitor monitor)
            throws CoreException {
        IFolder tempFolder = getProject().getFolder(
                IMTJCoreConstants.TEMP_FOLDER_NAME);
        return tempFolder.getFolder(IMTJCoreConstants.VERIFIED_FOLDER_NAME);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#isDeployedJarUpToDate()
     */
    public boolean isDeployedAppUpToDate() throws CoreException {
        String upToDateString = getProject().getPersistentProperty(
                DEPLOYMENT_UP_TO_DATE_PROP);
        return (upToDateString == null) ? false : upToDateString.equals("true"); //$NON-NLS-1$
    }

    /**
     * Return a boolean indicating whether the underlying project is a
     * preprocessed project.
     * 
     * @return
     * @throws CoreException
     */
    public boolean isPreprocessedProject() throws CoreException {
        IProject project = getProject();
        return project.exists()
                && project.isOpen()
                && project
                        .hasNature(IMTJCoreConstants.J2ME_PREPROCESSED_NATURE_ID);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProject#preverify(org.eclipse.core.resources.IResource[], org.eclipse.core.resources.IFolder, org.eclipse.core.runtime.IProgressMonitor)
     */
    public IPreverificationError[] preverify(IResource[] toVerify,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException, PreverifierNotFoundException {
        return getPreverifier()
                .preverify(this, toVerify, outputFolder, monitor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProject#preverifyJarFile(java.io.File, org.eclipse.core.resources.IFolder, org.eclipse.core.runtime.IProgressMonitor)
     */
    public IPreverificationError[] preverifyJarFile(File jarFile,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException, PreverifierNotFoundException {
        return getPreverifier().preverifyJarFile(this, jarFile, outputFolder,
                monitor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#removeMTJProjectListener(org.eclipse.mtj.core.project.IMTJProjectListener)
     */
    public void removeMTJProjectListener(IMTJProjectListener listener) {
        listenerList.remove(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#saveMetaData()
     */
    public void saveMetaData() throws CoreException {
        // Don't save the metadata if this is a preprocessed project,
        // as it belongs to the base project
        if (!isPreprocessedProject()) {
            metaData.saveMetaData();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#setDeployedJarFileUpToDate(boolean)
     */
    public void setDeployedAppFileUpToDate(boolean upToDate)
            throws CoreException {
        String value = upToDate ? Boolean.TRUE.toString() : Boolean.FALSE
                .toString();
        getProject().setPersistentProperty(DEPLOYMENT_UP_TO_DATE_PROP, value);

        for (IMTJProjectListener midletSuiteProjectListener : listenerList) {
            midletSuiteProjectListener.deployedJarFileUpToDateFlagChanged();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProject#setJadFileName(java.lang.String)
     */
    public void setJadFileName(String jadFileName) {
        metaData.setJadFileName(jadFileName);
        for (IMTJProjectListener midletSuiteProjectListener : listenerList) {
            ((IMidletSuiteProjectListener) midletSuiteProjectListener)
                    .jadFileNameChanged();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#setSignatureProperties(org.eclipse.mtj.internal.core.build.sign.ISignatureProperties)
     */
    public void setSignatureProperties(ISignatureProperties props) {
        metaData.setSignatureProperties(props);
        for (IMTJProjectListener midletSuiteProjectListener : listenerList) {
            midletSuiteProjectListener.signaturePropertiesChanged();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#setTempKeyPassword(java.lang.String)
     */
    public void setTempKeyPassword(String pass) {
        tempKeyPassword = pass;
        for (IMidletSuiteProjectListener midletSuiteProjectListener : listenerList) {
            midletSuiteProjectListener.tempKeyPasswordChanged();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#setTempKeystorePassword(java.lang.String)
     */
    public void setTempKeystorePassword(String pass) {
        tempKeystorePassword = pass;
        for (IMidletSuiteProjectListener midletSuiteProjectListener : listenerList) {
            midletSuiteProjectListener.tempKeystorePasswordChanged();
        }
    }

    /**
     * Add the JavaME classpath container to the java project we wrap if it is
     * currently missing. This step provides migration for projects created with
     * earlier releases of MTJ as well as providing a means to fix projects that
     * have lost their platform definition association.
     * 
     * @param monitor
     * @throws JavaModelException
     */
    private void addClasspathContainerIfMissing(IProgressMonitor monitor)
            throws CoreException {

        boolean hasClasspathContainer = containsJavaMEClasspathContainer(getJavaProject());
        boolean hasPreprocessingNature = isPreprocessedProject();

        if (!hasClasspathContainer && !hasPreprocessingNature) {
            // Create a new classpath entry for the classpath
            // container
            IPath entryPath = new Path(
                    JavaMEClasspathContainer.JAVAME_CONTAINER + "/" //$NON-NLS-1$
                            + getMTJRuntime().getActiveMTJRuntime().getDevice());
            IClasspathEntry newEntry = JavaCore.newContainerEntry(entryPath);

            // Get the current classpath entries
            IClasspathEntry[] rawClasspath = javaProject.getRawClasspath();
            Set<IClasspathEntry> currentClasspath = new LinkedHashSet<IClasspathEntry>(
                    rawClasspath.length);
            for (IClasspathEntry entry : rawClasspath) {
                if (entry.getEntryKind() == IClasspathEntry.CPE_VARIABLE) {
                    entry = JavaCore.getResolvedClasspathEntry(entry);
                }

                currentClasspath.add(entry);
            }

            // The classpath entries that are provided by
            // the platform definition currently
            IClasspathEntry[] platformEntries = getMTJRuntime()
                    .getActiveMTJRuntime().getDevice().getClasspath()
                    .asClasspathEntries();

            // Remove the classpath entries from the project
            // if they are provided by the platform definition
            for (IClasspathEntry entry : platformEntries) {
                if (currentClasspath.contains(entry)) {
                    currentClasspath.remove(entry);
                }
            }

            // Set the updated classpath
            currentClasspath.add(newEntry);
            IClasspathEntry[] newClasspath = currentClasspath
                    .toArray(new IClasspathEntry[currentClasspath.size()]);
            javaProject.setRawClasspath(newClasspath, monitor);
        }
    }

    /**
     * @param device
     * @param monitor
     * @throws CoreException
     * @throws JavaModelException
     */
    private void changeClasspathContainer(IDevice device,
            IProgressMonitor monitor) throws CoreException, JavaModelException {
        // Make sure that we have a place to put the
        // classpath updates
        addClasspathContainerIfMissing(monitor);

        // Changing the classpath container for the project to match the
        // selected device
        IClasspathEntry[] rawClasspath = javaProject.getRawClasspath();
        for (int i = 0; i < rawClasspath.length; i++) {
            IClasspathEntry entry = rawClasspath[i];
            if (entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER) {
                IPath path = entry.getPath();
                if (path.segment(0).equals(
                        JavaMEClasspathContainer.JAVAME_CONTAINER)) {
                    IPath entryPath = new Path(
                            JavaMEClasspathContainer.JAVAME_CONTAINER + "/" //$NON-NLS-1$
                                    + device);
                    if (!entry.equals(entryPath)) { // otherwise nothing to
                        // update
                        rawClasspath[i] = JavaCore.newContainerEntry(entryPath);
                        javaProject.setRawClasspath(rawClasspath, monitor);

                        // Force a rebuild
                        getProject().build(
                                IncrementalProjectBuilder.FULL_BUILD, monitor);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Return a boolean indicating whether the project contains the JavaME
     * classpath container.
     * 
     * @param javaProject the project to be tested
     * @return whether the project has the J2ME classpath container
     * @throws JavaModelException
     */
    private boolean containsJavaMEClasspathContainer(IJavaProject javaProject)
            throws JavaModelException {
        boolean contains = false;

        IClasspathEntry[] classpath = javaProject.getRawClasspath();
        for (IClasspathEntry entry : classpath) {
            if ((entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER)
                    && (entry.getPath().segment(0)
                            .equals(JavaMEClasspathContainer.JAVAME_CONTAINER))) {
                contains = true;
                break;
            }
        }

        return contains;
    }

    /**
     * create package for all the inactive configurations.
     * 
     * @param args
     * @param monitor
     * @throws CoreException
     */
    @SuppressWarnings("unchecked")
    private void createPackageForInactiveConfigs(Map args,
            IProgressMonitor monitor) throws CoreException {

        MTJRuntimeList configurations = getMTJRuntime();
        MTJRuntime activeConfig = configurations.getActiveMTJRuntime();
        IDevice deviceOfFormerConfig = activeConfig.getDevice();
        for (MTJRuntime config : configurations) {
            if (config != activeConfig) {
                configurations.switchActiveMTJRuntime(config);
                IDevice device = config.getDevice();
                if (deviceOfFormerConfig != device) {
                    changeClasspathContainer(device, monitor);
                    deviceOfFormerConfig = device;
                }
                // Force a rebuild
                getProject().build(IncrementalProjectBuilder.FULL_BUILD,
                        monitor);
                PreverificationBuilder
                        .cleanProject(getProject(), true, monitor);
                getProject().build(IncrementalProjectBuilder.FULL_BUILD,
                        IMTJCoreConstants.J2ME_PREVERIFIER_ID, args, monitor);
            }
        }
        // restore project settings after packaging configs
        restoreProjectSettings(configurations, activeConfig,
                deviceOfFormerConfig, monitor);
    }

    /**
     * Return the preverifier to use for resources in this project.
     * 
     * @throws PreverifierNotFoundException
     */
    private IPreverifier getPreverifier() throws CoreException,
            PreverifierNotFoundException {
        IPreverifier preverifier = null;

        IMIDPDevice device = (IMIDPDevice) getMTJRuntime()
                .getActiveMTJRuntime().getDevice();

        if (device == null) {
            throw new PreverifierNotFoundException(
                    Messages.MidletSuiteProject_preverifier_missing_device);
        }
        preverifier = device.getPreverifier() != null ? device.getPreverifier()
                : MTJCore.getDeviceRegistry().getDefaultPreferifier();
        if (preverifier == null) {
            throw new PreverifierNotFoundException(
                    Messages.MidletSuiteProject_preverifier_missing_default);
        }

        return preverifier;
    }

    /**
     * Get the project name, replacing spaces with underscores.
     * 
     * @return
     */
    private String getProjectNameWithoutSpaces() {
        String projectName = javaProject.getProject().getName();
        return projectName.replace(' ', '_');
    }

    /**
     * Initialize the project metadata.
     */
    private void initializeMetadata() {
        try {
            if (isPreprocessedProject()) {
                ICommand preverifierCommand = null;

                ICommand[] buildCommands = getProject().getDescription()
                        .getBuildSpec();
                for (ICommand command : buildCommands) {
                    if (command.getBuilderName().equals(
                            IMTJCoreConstants.J2ME_PREVERIFIER_ID)) {
                        preverifierCommand = command;
                        break;
                    }
                }

                if (preverifierCommand != null) {
                    // Pull out the target project and use its metadata
                    String targetProjectName = (String) preverifierCommand
                            .getArguments().get(
                                    StandardPreverifier.BUILD_ARG_PREVERIFY_TARGET);
                    IProject targetProject = ResourcesPlugin.getWorkspace()
                            .getRoot().getProject(targetProjectName);
                    if (targetProject != null) {
                        metaData = new MetaData(targetProject);
                    }
                }
            }
        } catch (CoreException e) {
            MTJCore.log(IStatus.ERROR, e);
        }

        if (metaData == null) {
            metaData = new MetaData(this);
        }
    }

    /**
     * After creating package for inactive configurations, should restore some
     * project settings.
     * 
     * @param configurations
     * @param activeConfig
     * @param deviceOfFormerConfig
     * @param monitor
     * @throws CoreException
     * @throws JavaModelException
     */
    private void restoreProjectSettings(MTJRuntimeList configurations,
            MTJRuntime activeConfig, IDevice deviceOfFormerConfig,
            IProgressMonitor monitor) throws CoreException, JavaModelException {
        // restore active config
        configurations.switchActiveMTJRuntime(activeConfig);
        // restore classpath container
        if (deviceOfFormerConfig != activeConfig.getDevice()) {
            changeClasspathContainer(activeConfig.getDevice(), monitor);
        }
        // must build again
        getProject().build(IncrementalProjectBuilder.FULL_BUILD, monitor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMTJProject#refreshClasspath(org.eclipse.core.runtime.IProgressMonitor)
     */
    public void refreshClasspath(IProgressMonitor monitor) throws CoreException {
        IDevice device = getMTJRuntime().getActiveMTJRuntime().getDevice();
        if (device != null) {

            getProject().deleteMarkers(
                    IMTJCoreConstants.JAVAME_MISSING_DEVICE_MARKER, false,
                    IResource.DEPTH_ZERO);

            changeClasspathContainer(device, monitor);
        }

    }

}
