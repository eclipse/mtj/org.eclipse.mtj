/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.sdk.device.midp.library;

import org.eclipse.mtj.core.sdk.device.midp.library.ILibrarySpecification;
import org.osgi.framework.Version;

/**
 * Tracks a specification of a particular library. Includes the name of the
 * specification, an identifier and the version of that specification.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @since 1.0
 * @author Craig Setera
 */
public class LibrarySpecification implements ILibrarySpecification {

    private String identifier;
    private String name;
    private Version version;

    public LibrarySpecification() {
    }

    /**
     * @param identifier
     * @param name
     * @param version
     */
    public LibrarySpecification(String identifier, String name, Version version) {
        super();
        this.identifier = identifier;
        this.name = name;
        this.version = version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.sdk.device.midp.library.ILibrarySpecification#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.sdk.device.midp.library.ILibrarySpecification#getVersion()
     */
    public Version getVersion() {
        return version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.sdk.device.midp.library.ILibrarySpecification#getIdentifier()
     */
    public String getIdentifier() {
        return identifier;
    }

}
