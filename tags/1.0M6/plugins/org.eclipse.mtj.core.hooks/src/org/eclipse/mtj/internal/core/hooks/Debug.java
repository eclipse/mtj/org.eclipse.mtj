/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.hooks;

import org.eclipse.osgi.framework.debug.FrameworkDebugOptions;

/**
 * Loads framework debugging options for use by the hook functions.
 * 
 * @author Craig Setera
 */
public class Debug {
    public static boolean DEBUG_GENERAL;

    public static final String MTJ_HOOKS = "org.eclipse.mtj.core.hooks";
    public static final String OPTION_DEBUG_GENERAL = MTJ_HOOKS
            + "/debug";

    static {
        FrameworkDebugOptions fdo = FrameworkDebugOptions.getDefault();
        if (fdo != null) {
            DEBUG_GENERAL = fdo.getBooleanOption(OPTION_DEBUG_GENERAL, false);
        }
    }
}
