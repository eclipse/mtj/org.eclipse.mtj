/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Added skin attribute for device  
 *     Feng Wang (Sybase)       - Modify to fit for AbstractMIDPDevice#copyForLaunch(...)
 *                                signature changing.
 *     Diego Sandin (Motorola)  - Use LaunchTemplateProperties and MicroEmuLaunchTemplateProperties 
 *                                enums instead of hard-coded property strings          
 */
package org.eclipse.mtj.internal.toolkit.microemu;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.launching.LaunchEnvironment;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.launching.ILaunchConstants;
import org.eclipse.mtj.internal.core.sdk.device.JavaEmulatorDevice;
import org.eclipse.mtj.internal.core.sdk.device.LaunchTemplateProperties;
import org.eclipse.mtj.internal.core.util.ReplaceableParametersProcessor;
import org.eclipse.mtj.internal.core.util.Utils;

/**
 * Device implementation for the MicroEmulator SDK devices.
 * 
 * @author Craig Setera
 */
public class MicroEmuDevice extends JavaEmulatorDevice {

    /**
     * The SDK root folder
     */
    private File root;

    /**
     * The device Skin
     */
    private MicroEmuDeviceSkin skin;

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDevice#getLaunchCommand(org.eclipse.mtj.core.launching.LaunchEnvironment, org.eclipse.core.runtime.IProgressMonitor)
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException {

        IMidletSuiteProject midletSuite = launchEnvironment.getMidletSuite();
        ILaunchConfiguration launchConfiguration = launchEnvironment
                .getLaunchConfiguration();
        boolean launchFromJAD = shouldDirectLaunchJAD(launchConfiguration);

        File tempDeployed = copyForLaunch(midletSuite, monitor, launchFromJAD);

        // Fill in our properties so we can use them for resolution
        // against the raw executable command line
        Map<String, String> executionProperties = new HashMap<String, String>();

        // Adding launcher information
        executionProperties.put(LaunchTemplateProperties.EXECUTABLE.toString(),
                getJavaExecutable().toString());

        // Adding SDK root folder information
        executionProperties.put(MicroEmuLaunchTemplateProperties.TOOLKITROOT
                .toString(), root.toString());

        // Adding skin information
        executionProperties.put(MicroEmuLaunchTemplateProperties.SKINJARFILE
                .toString(), skin.getJarFile());

        executionProperties.put(MicroEmuLaunchTemplateProperties.SKINPATHINJAR
                .toString(), skin.getPath());

        // Adding java path separator information
        executionProperties.put(MicroEmuLaunchTemplateProperties.PATHSEPARATOR
                .toString(), System.getProperty("path.separator")); //$NON-NLS-1$

        // Adding debug information
        if (launchEnvironment.isDebugLaunch()) {
            executionProperties.put(LaunchTemplateProperties.DEBUGPORT
                    .toString(), new Integer(launchEnvironment
                    .getDebugListenerPort()).toString());
        }

        // Adding launch configuration values
        executionProperties.put(LaunchTemplateProperties.USERSPECIFIEDARGUMENTS
                .toString(), launchConfiguration.getAttribute(
                ILaunchConstants.LAUNCH_PARAMS, Utils.EMPTY_STRING));

        if (launchFromJAD) {
            executionProperties.put(
                    LaunchTemplateProperties.JADFILE.toString(),
                    getSpecifiedJadURL(launchConfiguration));
        } else {
            File jadFile = getJadForLaunch(midletSuite, tempDeployed, monitor);
            if (jadFile.exists()) {
                executionProperties.put(LaunchTemplateProperties.JADFILE
                        .toString(), jadFile.toString());
            }
        }

        // Do the property resolution given the previous information
        return ReplaceableParametersProcessor.processReplaceableValues(
                launchCommandTemplate, executionProperties);
    }

    /**
     * Return the SDK root directory.
     * 
     * @return Returns the root.
     */
    public File getRoot() {
        return root;
    }

    /**
     * Return the device skin
     * 
     * @return the skin
     */
    public MicroEmuDeviceSkin getSkin() {
        return skin;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.AbstractMIDPDevice#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    @Override
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.loadUsing(persistenceProvider);

        /* Loading the SDK root directory */
        String rootString = persistenceProvider
                .loadString(MicoEmuConstants.TOOLKITROOT_PERSISTENCE_KEY);
        root = new File(rootString);

        /* Loading the skin information */
        String skinJarFile = persistenceProvider
                .loadString(MicoEmuConstants.SKIN_JAR_PERSISTENCE_KEY);
        String skinPathInFile = persistenceProvider
                .loadString(MicoEmuConstants.SKIN_PATH_PERSISTENCE_KEY);
        String skinName = persistenceProvider
                .loadString(MicoEmuConstants.SKIN_NAME_PERSISTENCE_KEY);

        skin = new MicroEmuDeviceSkin(skinJarFile, skinPathInFile, skinName);

    }

    /**
     * Set the SDK root directory
     * 
     * @param mppRoot The mppRoot to set.
     */
    public void setRoot(File mppRoot) {
        this.root = mppRoot;
    }

    /**
     * Set the device skin
     * 
     * @param skin the skin to set
     */
    public void setSkin(MicroEmuDeviceSkin skin) {
        this.skin = skin;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.AbstractMIDPDevice#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    @Override
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.storeUsing(persistenceProvider);

        /* Saving the SDK root directory */
        persistenceProvider.storeString(
                MicoEmuConstants.TOOLKITROOT_PERSISTENCE_KEY, root.toString());

        /* Saving skin information */
        persistenceProvider.storeString(
                MicoEmuConstants.SKIN_JAR_PERSISTENCE_KEY, skin.getJarFile());
        persistenceProvider.storeString(
                MicoEmuConstants.SKIN_PATH_PERSISTENCE_KEY, skin.getPath());
        persistenceProvider.storeString(
                MicoEmuConstants.SKIN_NAME_PERSISTENCE_KEY, skin.getName());

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice#getWorkingDirectory()
     */
    public File getWorkingDirectory() {
        return null;
    }
}
