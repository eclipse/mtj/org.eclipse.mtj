/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)     - Initial implementation
 *     Diego Sandin (Motorola)      - Refactoring package name to follow eclipse 
 *                                    standards
 *     Gustavo de Paula (Motorola)  - Adapt to new AbstractMIDPDevice interface
 *     Diego Sandin (Motorola)      - Use LaunchTemplateProperties enum instead of 
 *                                    hard-coded strings
 *                                
 */
package org.eclipse.mtj.examples.toolkits.me4se;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.launching.LaunchEnvironment;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.launching.ILaunchConstants;
import org.eclipse.mtj.internal.core.sdk.device.JavaEmulatorDevice;
import org.eclipse.mtj.internal.core.sdk.device.LaunchTemplateProperties;
import org.eclipse.mtj.internal.core.util.ReplaceableParametersProcessor;
import org.eclipse.mtj.internal.core.util.Utils;

/**
 * Device implementation for the ME4SE SDK device.
 * 
 * @author Craig Setera
 */
public class ME4SEDevice extends JavaEmulatorDevice {

    /**
     * The ME4SE SDK jar file.
     */
    private File jarFile;

    /**
     * @return Returns the root.
     */
    public File getJarFile() {
        return jarFile;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDevice#getLaunchCommand(org.eclipse.mtj.core.launching.LaunchEnvironment, org.eclipse.core.runtime.IProgressMonitor)
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException {

        IMidletSuiteProject midletSuite = launchEnvironment.getMidletSuite();
        ILaunchConfiguration launchConfiguration = launchEnvironment
                .getLaunchConfiguration();

        boolean launchFromJAD = shouldDirectLaunchJAD(launchConfiguration);
        File tempDeployed = copyForLaunch(midletSuite, monitor, launchFromJAD);

        // Fill in our properties so we can use them for resolution
        // against the raw executable command line
        Map<String, String> executionProperties = new HashMap<String, String>();

        executionProperties.put(LaunchTemplateProperties.EXECUTABLE.toString(),
                getJavaExecutable().toString());

        executionProperties.put("me4sejar", jarFile.toString());
        executionProperties.put(LaunchTemplateProperties.CLASSPATH.toString(),
                getProjectClasspathString(midletSuite, tempDeployed, monitor));

        // Debug information
        if (launchEnvironment.isDebugLaunch()) {
            executionProperties.put(LaunchTemplateProperties.DEBUGPORT
                    .toString(), new Integer(launchEnvironment
                    .getDebugListenerPort()).toString());
        }

        // Add launch configuration values
        String extraArguments = launchConfiguration.getAttribute(
                ILaunchConstants.LAUNCH_PARAMS, Utils.EMPTY_STRING);
        executionProperties.put(LaunchTemplateProperties.USERSPECIFIEDARGUMENTS
                .toString(), extraArguments);

        if (shouldDirectLaunchJAD(launchConfiguration)) {
            executionProperties.put(
                    LaunchTemplateProperties.JADFILE.toString(),
                    getSpecifiedJadURL(launchConfiguration));
        } else {
            File jadFile = getJadForLaunch(midletSuite, tempDeployed, monitor);
            if (jadFile.exists()) {
                executionProperties.put(LaunchTemplateProperties.JADFILE
                        .toString(), jadFile.toString());
            }
        }

        // Do the property resolution given the previous information
        return ReplaceableParametersProcessor.processReplaceableValues(
                launchCommandTemplate, executionProperties);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice#getWorkingDirectory()
     */
    public File getWorkingDirectory() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.AbstractMIDPDevice#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    @Override
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.loadUsing(persistenceProvider);

        String jarFileName = persistenceProvider.loadString("jarFile");
        jarFile = new File(jarFileName);
    }

    /**
     * @param jar The mppRoot to set.
     */
    public void setJarFile(File jar) {
        this.jarFile = jar;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.AbstractMIDPDevice#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    @Override
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.storeUsing(persistenceProvider);
        persistenceProvider.storeString("jarFile", jarFile.toString());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.AbstractMIDPDevice#getProjectClasspathString(org.eclipse.mtj.core.project.IMTJProject, java.io.File, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    protected String getProjectClasspathString(IMTJProject midletSuite,
            File temporaryDirectory, IProgressMonitor monitor)
            throws CoreException {
        StringBuffer sb = new StringBuffer(super.getProjectClasspathString(
                midletSuite, temporaryDirectory, monitor));
        sb.append(File.pathSeparatorChar);
        sb.append(jarFile);

        return sb.toString();
    }
}
