/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.example.library;

/**
 * This class is used as an example of the use of AccessRules to limit the
 * access to classes from outside the library bundle.
 * <p>
 * The access to this class from outside the library bundle is Discouraged.
 * </p>
 * 
 * @author Diego Madruga Sandin
 */
public class DiscouragedAccessLibraryClass {

    /**
     * Creates a new DiscouragedAccessLibraryClass.
     */
    public DiscouragedAccessLibraryClass() {
    }

    /**
     * An discouraged access method from outside the library bundle.
     * 
     * @return the <code>"The access to this method is discouraged."</code>
     *         string.
     */
    public String discouragedAccessMethod() {
        return "The access to this method is discouraged.";
    }
}
