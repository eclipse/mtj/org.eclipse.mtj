/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma (Sybase)         - Initial implementation
 *     Diego Madruga (Motorola) - Refactored class after API updates.  
 */
package org.eclipse.mtj.examples.jadextension;

import org.eclipse.mtj.core.project.midp.DescriptorPropertyDescription;
import org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider;
import org.eclipse.mtj.ui.editors.jad.ListDescriptorPropertyDescription;

/**
 * class provides Motorola specific JAD attributes
 * 
 * @author Gang Ma
 */
public class MotoJADDesciptorsProvider implements IJADDescriptorsProvider {

    private static final String MOTO_JAD_BACKGROUND = "Background";
    private static final String MOTO_JAD_BACKGROUND_LABLE = "Background:";
    private static final String MOTO_JAD_FLIPINSENSITIVE = "FlipInsensitive";
    private static final String MOTO_JAD_FLIPINSENSITIVE_LABLE = "FlipInsensitive:";
    private static final String MOTO_JAD_MIDLETWEBSESS = "MIDlet-Web-Session";
    private static final String MOTO_JAD_MIDLETWEBSESS_LABLE = "MIDlet-Web-Session:";
    private static final String MOTO_JAD_PROG_SPACE_REQ = "Mot-Program-Space-Requirements";
    private static final String MOTO_JAD_PROG_SPACE_REQ_LABLE = "Mot-Program-Space-Requirements:";
    private static final String MOTO_JAD_DATA_SPACE_REQ = "Mot-Data-Space-Requirements";
    private static final String MOTO_JAD_DATA_SPACE_REQ_LABLE = "Mot-Data-Space-Requirements:";
    private static final String MOTO_JAD_MID_IDLE_ICON = "Mot-MIDlet-Idle-Icon";
    private static final String MOTO_JAD_MID_IDLE_ICON_LABLE = "Mot-MIDlet-Idle-Icon:";
    private static final String MOTO_JAD_MID_NEXT_URL = "Mot-MIDlet-Next-URL";
    private static final String MOTO_JAD_MID_NEXT_URL_LABLE = "Mot-MIDlet-Next-URL:";

    /**
     * Motorola specific descriptors list
     */
    private static final DescriptorPropertyDescription[] MOTO_JAD_DESCRIPTORS = new DescriptorPropertyDescription[] {
            new ListDescriptorPropertyDescription(MOTO_JAD_BACKGROUND,
                    MOTO_JAD_BACKGROUND_LABLE, getBooleanNamesAndValues()),
            new ListDescriptorPropertyDescription(MOTO_JAD_FLIPINSENSITIVE,
                    MOTO_JAD_FLIPINSENSITIVE_LABLE, getBooleanNamesAndValues()),
            new DescriptorPropertyDescription(MOTO_JAD_MIDLETWEBSESS,
                    MOTO_JAD_MIDLETWEBSESS_LABLE,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(MOTO_JAD_PROG_SPACE_REQ,
                    MOTO_JAD_PROG_SPACE_REQ_LABLE,
                    DescriptorPropertyDescription.DATATYPE_INT),
            new DescriptorPropertyDescription(MOTO_JAD_DATA_SPACE_REQ,
                    MOTO_JAD_DATA_SPACE_REQ_LABLE,
                    DescriptorPropertyDescription.DATATYPE_INT),
            new DescriptorPropertyDescription(MOTO_JAD_MID_IDLE_ICON,
                    MOTO_JAD_MID_IDLE_ICON_LABLE,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(MOTO_JAD_MID_NEXT_URL,
                    MOTO_JAD_MID_NEXT_URL_LABLE,
                    DescriptorPropertyDescription.DATATYPE_URL)

    };

    /**
     * @return
     */
    private static String[][] getBooleanNamesAndValues() {
        String[][] namesAndValues = new String[3][2];
        namesAndValues[0][0] = "";
        namesAndValues[0][1] = "";
        namesAndValues[1][0] = "True";
        namesAndValues[1][1] = "True";
        namesAndValues[2][0] = "False";
        namesAndValues[2][1] = "False";

        return namesAndValues;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider#getDescriptorPropertyDescriptions()
     */
    public DescriptorPropertyDescription[] getDescriptorPropertyDescriptions() {
        return MOTO_JAD_DESCRIPTORS;

    }

}
