/*******************************************************************************
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nokia Corporation - initial implementation 
 *******************************************************************************/
package org.eclipse.mtj.eswt.internal.templates;

import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class ESWTTemplatesPlugin extends AbstractUIPlugin {
	ContextTypeRegistry contextTypeRegistry;
	private static ESWTTemplatesPlugin plugin;
	
	public ESWTTemplatesPlugin(){
		super();
		plugin = this;
	}
	
	public static ESWTTemplatesPlugin getDefault(){
		return plugin;
	}
	
}
