/**
 * Copyright (c) 2004, 2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID.
 *     Diego Sandin (Motorola)  - Fix method signatures and enhanced javadoc 
 *                                [bug 270532]
 */
package org.eclipse.mtj.internal.core.util.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Version;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

/**
 * A set of XML utilities for working with DOM.
 * 
 * @author Craig Setera
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @since 1.0
 */
public class XMLUtils {

    /** The attribute name for the version */
    public static final String ATTR_VERSION = "version"; //$NON-NLS-1$

    private static final String PROP_LOG_XMLUTILS = "mtj.log.xmlutils"; //$NON-NLS-1$

    private static final boolean DO_LOG = System.getProperty(PROP_LOG_XMLUTILS,
            Boolean.FALSE.toString()).equalsIgnoreCase(Boolean.TRUE.toString());

    // The XSLT transformation used when writing out the XML document
    private static final String XSLT_TEMPLATE = "<xsl:stylesheet version=\"1.0\"\r\n"
            + "      xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\r\n"
            + "   <xsl:output method=\"xml\"/>\r\n"
            + "   <xsl:param name=\"indent-increment\" select=\"\'   \'\" />\r\n"
            + "\r\n"
            + "   <xsl:template match=\"*\">\r\n"
            + "      <xsl:param name=\"indent\" select=\"\'&#xA;\'\"/>\r\n"
            + "\r\n"
            + "      <xsl:value-of select=\"$indent\"/>\r\n"
            + "      <xsl:copy>\r\n"
            + "        <xsl:copy-of select=\"@*\" />\r\n"
            + "        <xsl:apply-templates>\r\n"
            + "          <xsl:with-param name=\"indent\"\r\n"
            + "               select=\"concat($indent, $indent-increment)\"/>\r\n"
            + "        </xsl:apply-templates>\r\n"
            + "        <xsl:if test=\"*\">\r\n"
            + "          <xsl:value-of select=\"$indent\"/>\r\n"
            + "        </xsl:if>\r\n"
            + "      </xsl:copy>\r\n"
            + "   </xsl:template>\r\n"
            + "\r\n"
            + "   <xsl:template match=\"comment()|processing-instruction()\">\r\n"
            + "      <xsl:copy />\r\n"
            + "   </xsl:template>\r\n"
            + "\r\n"
            + "   <!-- WARNING: this is dangerous. Handle with care -->\r\n"
            + "   <xsl:template match=\"text()[normalize-space(.)=\'\']\"/>\r\n"
            + "\r\n" + "</xsl:stylesheet>";

    /**
     * Create a new child element, of the specified name, under this parent.
     * <p>
     * This method may return <code>null</code> if the element could not be
     * created.
     * </p>
     * 
     * @param parent the parent not on which we'll append the newly created
     *            child.
     * @param name The name of the element to be created. This is case-sensitive
     *            and must not be <code>null</code> or an empty String
     *            <code>""</code>.
     * @return the newly created child element or <code>null</code> if the
     *         element could not be created.
     * @throws DOMException If the specified name is not an valid XML name or if
     *             the child node could not be append in the given parent
     *             element.
     */
    public static Element createChild(Element parent, String name)
            throws DOMException {

        Element child = null;

        if ((parent != null) && (name != null)
                && (!name.equals(Utils.EMPTY_STRING))) {

            if (DO_LOG) {
                System.out.println(NLS.bind(
                        "createChild: parent = {0}; name = {1}", new String[] {
                                parent.getTagName(), name }));
            }

            Document ownerDocument = parent.getOwnerDocument();
            if (ownerDocument != null) {
                child = ownerDocument.createElement(name);
                parent.appendChild(child);
            }
        }
        return (child);
    }

    /**
     * Return a new instance of a root element for the specified element name
     * and version.
     * 
     * @param elementName the root element name.
     * @param version the version for this root element.
     * @return A new root Element object with the elementName and version.
     * @throws ParserConfigurationException if fails to create the
     *             DocumentBuilder responsible for the creation of the DOM
     *             Document object to build the DOM tree with for the given root
     *             element.
     */
    public static Element createRootElement(String elementName, Version version)
            throws ParserConfigurationException {
        if (DO_LOG) {
            System.out.println(NLS.bind(
                    "createRootElement: elementName = {0}; version = {1}",
                    new String[] { elementName, version.toString() }));
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.newDocument();

        Element element = document.createElement(elementName);
        document.appendChild(element);
        element.setAttribute(ATTR_VERSION, version.toString());

        return element;
    }

    /**
     * Create a new child node that contains the specified text
     * 
     * @param parent
     * @param name
     * @param text
     */
    public static void createTextElement(Element parent, String name,
            String text) {
        if (DO_LOG) {
            System.out.println("createTextElement: parent = "
                    + parent.getTagName() + "; name = " + name + "; text = "
                    + text);
        }

        Element child = createChild(parent, name);
        if (text != null) {
            Text textNode = parent.getOwnerDocument().createTextNode(text);
            child.appendChild(textNode);
        }
    }

    /**
     * Get the text that is held within the specified element.
     * 
     * @param element
     * @return
     */
    public static String getElementText(Element element) {
        StringBuffer sb = new StringBuffer();

        NodeList children = element.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node node = children.item(i);
            if (node.getNodeType() == Node.TEXT_NODE) {
                sb.append(((Text) node).getData());
            }
        }

        return sb.toString();
    }

    /**
     * Return the first child element of the specified element with the
     * specified name. Return <code>null</code> if no such element is found.
     * 
     * @param element
     * @param name
     * @return
     */
    public static Element getFirstElementWithTagName(Element element,
            String name) {
        Element firstElement = null;

        NodeList nodes = element.getElementsByTagName(name);
        if (nodes.getLength() > 0) {
            firstElement = (Element) nodes.item(0);
        }

        return firstElement;
    }

    /**
     * Return the version in the specified document.
     * 
     * @param document
     * @return
     */
    public static Version getVersion(Document document) {
        Element docElement = document.getDocumentElement();
        String versionString = docElement.getAttribute(ATTR_VERSION);
        if (versionString.length() == 0)
            versionString = Version.emptyVersion.toString();

        return new Version(versionString);
    }

    /**
     * Return an XML document read from the specified XML file or
     * <code>null</code> if the file cannot be found.
     * 
     * @param xmlFile
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public static Document readDocument(File xmlFile)
            throws ParserConfigurationException, SAXException, IOException {
        Document document = null;

        if ((xmlFile != null) && (xmlFile.exists())) {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(xmlFile);
        }

        return document;
    }

    /**
     * Return an XML document read from the specified XML file or
     * <code>null</code> if the input stream is <code>null</code>.
     * 
     * @param xmlStream
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    public static Document readDocument(InputStream xmlStream)
            throws ParserConfigurationException, SAXException, IOException {
        Document document = null;

        if (xmlStream != null) {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(xmlStream);
        }

        return document;
    }

    /**
     * Write the specified XML document to the specified XML file.
     * 
     * @param xmlFile
     * @param document
     * @throws TransformerException
     * @throws IOException
     */
    public static void writeDocument(File xmlFile, Document document)
            throws TransformerException, IOException {
        FileOutputStream fos = new FileOutputStream(xmlFile);
        OutputStreamWriter writer = new OutputStreamWriter(fos, "UTF-8");
        try {
            writeDocument(writer, document);
        } finally {
            writer.close();
        }
    }

    /**
     * Write the specified XML document to the specified XML file.
     * 
     * @param xmlFile
     * @param document
     * @throws TransformerException
     * @throws IOException
     */
    public static void writeDocument(Writer writer, Document document)
            throws TransformerException, IOException {
        // Configure the output stream
        StreamResult destination = new StreamResult(writer);

        // The source
        DOMSource source = new DOMSource(document);

        // XSLT source document
        StringReader xslReader = new StringReader(XSLT_TEMPLATE);
        StreamSource xslSource = new StreamSource(xslReader);

        // Do the transformation between the source and destination
        TransformerFactory tfactory = TransformerFactory.newInstance();
        Transformer serializer = tfactory.newTransformer(xslSource);
        serializer.transform(source, destination);
    }

    /**
     * Private constructor
     */
    private XMLUtils() {
        super();
    }
}
