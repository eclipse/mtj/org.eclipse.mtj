/**
 * Copyright (c) 2007,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)    - Initial implementation
 *     Diego Sandin (Motorola)     - Refactoring package name to follow eclipse 
 *                                   standards
 *     Diego Sandin (Motorola)     - Changed the getAPIs to first try to extract 
 *                                   API info from MANIFEST
 *     Gustavo de Paula (Motorola) - Fix getTypeFromManifest NPE
 *     Diego Sandin (Motorola)     - Refactored to use IAPI            
 */
package org.eclipse.mtj.internal.core.sdk.device.midp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType;
import org.osgi.framework.Version;

/**
 * Provides access to the API definitions that have been registered via the API
 * extension point.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @author Craig Setera
 */
public class APIRegistry {

    /**
     * Return the list of API's present in specified JAR file.
     * 
     * @param jarFile
     * @return
     * @throws IOException
     */
    public static List<IAPI> getAPIs(File jarFile) {
        Map<String, IAPI> apis = new HashMap<String, IAPI>();
        JarFile jar = null;

        try {
            jar = new JarFile(jarFile);

            if (jar != null) {

                IAPI api = createAPIFromManifest(jar);
                if (api != null) {
                    apis.put(api.toString(), api);
                }

            }
        } catch (IOException e) {

        } finally {
            if (jar != null) {
                try {
                    jar.close();
                } catch (IOException e) {
                }
            }
        }

        // Final fall back position
        if (apis.size() == 0) {
            IAPI unknown = createUnknownAPI(jarFile);
            apis.put(unknown.getIdentifier(), unknown);
        }

        ArrayList<IAPI> result = new ArrayList<IAPI>(apis.values());
        return result;
    }

    /**
     * Attempt to return an API definition for the information in the manifest.
     * 
     * @param attributes
     * @return
     * @throws IOException
     */
    private static IAPI createAPIFromManifest(JarFile jarFile)
            throws IOException {
        MIDPAPI api = null;

        Manifest manifest = jarFile.getManifest();
        if (manifest != null) {
            Attributes attributes = manifest.getMainAttributes();

            String identifier = attributes.getValue(UEIAPIManifestAttributes.API
                    .toString());
            if (identifier != null) {
                api = new MIDPAPI();
                api.setIdentifier(identifier);
                api.setName(attributes.getValue(UEIAPIManifestAttributes.API_NAME
                        .toString()));
                api.setType(getTypeFromManifest(attributes));
                api.setVersion(getVersionFromManifest(attributes));
            }
        }

        return api;
    }

    /**
     * Return a new unknown API instance.
     * 
     * @param libraryFile
     * @return
     */
    private static IAPI createUnknownAPI(File libraryFile) {
        MIDPAPI api = new MIDPAPI();
        api.setIdentifier(libraryFile.getName());
        api.setName("Unknown Library"); //$NON-NLS-1$
        api.setType(MIDPAPIType.UNKNOWN);
        api.setVersion(new Version("1.0")); //$NON-NLS-1$

        return api;
    }

    /**
     * Return the integer type of the value specified in the manifest.
     * 
     * @param attributes
     * @return
     */
    private static MIDPAPIType getTypeFromManifest(Attributes attributes) {
        MIDPAPIType type = MIDPAPIType.UNKNOWN;

        String typeString = attributes.getValue(UEIAPIManifestAttributes.API_TYPE
                .toString());
        if ((typeString == null)
                || typeString.equalsIgnoreCase(MIDPAPIType.OPTIONAL.toString())) {
            type = MIDPAPIType.OPTIONAL;
        } else if (typeString.equalsIgnoreCase(MIDPAPIType.PROFILE.toString())) {
            type = MIDPAPIType.PROFILE;
        } else if (typeString.equalsIgnoreCase(MIDPAPIType.CONFIGURATION
                .toString())) {
            type = MIDPAPIType.CONFIGURATION;
        }

        return type;
    }

    /**
     * Return the version of the API as found in the attributes.
     * 
     * @param attributes
     * @return
     */
    private static Version getVersionFromManifest(Attributes attributes) {
        Version version = null;

        String versionString = attributes
                .getValue(UEIAPIManifestAttributes.API_VERSION.toString());
        if (versionString != null) {
            version = new Version(versionString);
        } else {
            version = new Version("0.0.0"); //$NON-NLS-1$
        }

        return version;
    }

    // Private constructor for static access.
    private APIRegistry() {
    }
}
