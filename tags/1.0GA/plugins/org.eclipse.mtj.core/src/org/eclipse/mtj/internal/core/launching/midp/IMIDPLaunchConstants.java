/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.launching.midp;

import org.eclipse.mtj.internal.core.launching.ILaunchConstants;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IMIDPLaunchConstants extends ILaunchConstants {

    /**
     * Whether we are emulating a MIDlet or doing OTA
     */
    public static final String DO_OTA = "mtj.do_ota"; //$NON-NLS-1$

    /**
     * Whether to use the specified JAD URL to do the launch, bypassing JAD
     * creation and OTA use
     */
    public static final String DO_JAD_LAUNCH = "mtj.do_jad_launch"; //$NON-NLS-1$

    /**
     * The URL specified by the user for launching when DO_JAD_LAUNCH has been
     * set true
     */
    public static final String SPECIFIED_JAD_URL = "mtj.specified_jad_url"; //$NON-NLS-1$

    /**
     * The application descriptor to be executed
     */
    public static final String APP_DESCRIPTOR = "mtj.app_descriptor"; //$NON-NLS-1$

    /**
     * The heap size to be used by the emulator
     */
    public static final String HEAP_SIZE = "mtj.heap_size"; //$NON-NLS-1$

    /**
     * The security domain to execute the emulation under
     */
    public static final String SECURITY_DOMAIN = "mtj.security_domain"; //$NON-NLS-1$

    /**
     * The security domain setting that tells not to add the security domain
     */
    public static final String NO_SECURITY_DOMAIN = "None"; //$NON-NLS-1$
}
