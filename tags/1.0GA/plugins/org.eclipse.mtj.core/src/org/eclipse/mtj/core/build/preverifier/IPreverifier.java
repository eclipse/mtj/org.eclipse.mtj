/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.build.preverifier;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.project.IMTJProject;

/**
 * The <code>IPreverifier</code> interface represents a preverifier instance
 * that is required for preverification support on a {@link IMTJProject}.
 * <p>
 * The preverification process will make a series of checks on the Java
 * <code>.class</code> files, generated after the compilation of the source code
 * of a {@link IMTJProject} or available inside a <code>.jar</code> file. The
 * preverifier will annotate these files, and save them on a previously
 * specified folder for use on the final deployed Application Package.
 * </p>
 * <p>
 * All implementations of this interface are <b>internal</b>, and <b>should
 * not</b> be accessed directly by clients(compatibility will not be
 * maintained). To create a new instance of a preverifier, clients <b>must
 * use</b> {@link MTJCore#createPreverifier(String, Object)} informing the type
 * of the preverifier to be created and any parameter that is required by that
 * specific preverifier type.
 * </p>
 * <p>
 * Currently, only two types of preverifiers are supported:
 * <ul>
 * <li><b>Standard Preverifier</b>, which is the default preverifier in MTJ, is
 * basically a wrapper on a preverifier binary and is represented by the
 * {@link #PREVERIFIER_STANDARD} constant.</li>
 * <li><b>Unknown Preverifier</b>, which represents an Unknown preverifier and
 * is represented by the {@link #PREVERIFIER_UNKNOWN} constant.</li>
 * </ul>
 * </p>
 * <p>
 * For more information on the preverification process, please visit <a
 * href="http://jcp.org/aboutJava/communityprocess/final/jsr030/index.html"
 * >CLDC-1.0</a> and <a
 * href="http://jcp.org/aboutJava/communityprocess/final/jsr139/index.html"
 * >CLDC-1.1</a>
 * </p>
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @since 1.0
 * @see org.eclipse.mtj.core.MTJCore#createPreverifier(String, Object)
 */
public interface IPreverifier extends IPersistable {

    /**
     * Constant that represents the standard preverifier
     */
    public static final String PREVERIFIER_STANDARD = "org.eclipse.mtj.preverifier.standard";

    /**
     * Constant that represents an unknown preverifier
     */
    public static final String PREVERIFIER_UNKNOWN = "org.eclipse.mtj.preverifier.unknown";

    /**
     * Launch the preverification process on the specified resources from the
     * given {@link IMTJProject}.
     * 
     * @param mtjProject The project in which the resources to be preverified
     *            reside.
     * @param toVerify The resources to be preverified.
     * @param outputFolder The folder into which the output is to be placed.
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @return an array of preverification error instances indicating the errors
     *         found during execution.
     * @throws CoreException if this method fails. Reasons include:
     *             <ul>
     *             <li>The existence of outputFolder could not be guaranteed.</li>
     *             <li>The command line for triggering the preverification
     *             process could not be created.</li>
     *             <li>The preverification process could not be created
     *             correctly.</li>
     *             </ul>
     */
    public IPreverificationError[] preverify(IMTJProject mtjProject,
            IResource[] toVerify, IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException;

    /**
     * Launch the preverification process on the classes available in the
     * specified jar file from the given {@link IMTJProject}.
     * <p>
     * Rather than trying to preverify a jar file, the file must be expanded
     * first and then we'll preverify against the expanded classes.
     * </p>
     * 
     * @param mtjProject The project in which the <code>.jar</code> file to be
     *            preverified reside.
     * @param jarFile the <code>.jar</code> file to be preverified.
     * @param outputFolder The folder into which the output is to be placed.
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired
     * @return an array of preverification error instances indicating the errors
     *         found during execution.
     * @throws CoreException if this method fails. Reasons include:
     *             <ul>
     *             <li>The <code>.jar</code> file could not be expanded.</li>
     *             <li>The existence of outputFolder could not be guaranteed.</li>
     *             <li>The command line for triggering the preverification
     *             process could not be created.</li>
     *             <li>The preverification process could not be created
     *             correctly.</li>
     *             </ul>
     * @throws IOException
     */
    public IPreverificationError[] preverifyJarFile(IMTJProject mtjProject,
            File jarFile, IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException;

    /**
     * Return the file that is the preverifier binary, or <code>null</code> if
     * no preverifier was specified.
     * <p>
     * If this method return <code>null</code>, MTJ will automatically assume
     * the use of the default preverifier configured in the UI.
     * </p>
     * 
     * @return the preverifier binary file, or <code>null</code> if no
     *         preverifier was specified
     */
    public File getPreverifierExecutable();

}
