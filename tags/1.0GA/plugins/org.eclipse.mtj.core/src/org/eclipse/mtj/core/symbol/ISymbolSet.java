/**
 * Copyright (c) 2008, 2009 Motorola Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gustavo de Paula (Motorola) - Initial implementation
 *     Diego Sandin (Motorola)     - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.symbol;

import java.util.Collection;
import java.util.Map;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.IPersistable;

/**
 * Symbol definition sets provide a means to define a group of related symbols
 * with a particular name to reference that group.
 * <p>
 * Symbol definition sets are most useful for controlling definitions without
 * the need to alter source code. For example, symbol definition sets can be
 * used to remove debugging information to create a production build.
 * </p>
 * <p>
 * Clients must use {@link MTJCore#getSymbolSetFactory()} to retrieve an
 * {@link ISymbolSetFactory} instance and use the
 * {@link ISymbolSetFactory#createSymbolSet(String)} to create an ISymbolSet
 * instance.
 * </p>
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * 
 * @since 1.0
 */
public interface ISymbolSet extends IPersistable {

    /**
     * Define a new symbol in the symbolset with the given name with the a
     * default value (<code>true</code>).
     * 
     * @param name the symbol name. This is case-sensitive and must not be
     *            <code>null</code> or an empty String <code>""</code>.
     * @return the instance of the newly created symbol that was added to the
     *         symbolset.
     */
    public ISymbol add(String name);

    /**
     * Define a new symbol in the symbolset with the given name and value.
     * 
     * @param name the symbol name. This is case-sensitive and must not be
     *            <code>null</code> or an empty String <code>""</code>.
     * @param value the symbol value.
     * @return the instance of the newly created symbol that was added to the
     *         symbolset.
     */
    public ISymbol add(String name, String value);

    /**
     * Define a new symbol in the symbolset with the given name, value and type.
     * 
     * @param name the symbol name. This is case-sensitive and must not be
     *            <code>null</code> or an empty String <code>""</code>.
     * @param value the symbol value.
     * @param type the symbol value. Possible types are {@link #TYPE_ABILITY}
     *            and {@link ISymbol#TYPE_RUNTIME}.
     * @return the instance of the newly created symbol that was added to the
     *         symbolset.
     */
    public ISymbol add(String identifier, String value, int type);

    /**
     * Add a collection of symbols to this symbolset.
     * 
     * @param symbols the collection of symbols to be added.
     */
    public void add(Collection<ISymbol> symbols);

    /**
     * Add one specific symbol to this symbolset.
     * 
     * @param s the symbol to be added.
     */
    public void add(ISymbol s);

    /**
     * Return a boolean indicating whether the specified SymbolSet object is
     * equal to this SymbolSet.
     * 
     * @param definitions the reference SymbolSet object with which to compare.
     * @return <code>true</code> if this SymbolSet is the same as the the one
     *         from the definitions argument; <code>false</code> otherwise.
     */
    public boolean equals(ISymbolSet definitions);

    /**
     * Returns a collection view of the values contained in this SymbolSet.
     * 
     * @return a collection view of the values contained in this SymbolSet.
     */
    public Collection<ISymbol> getSymbols();

    /**
     * Return the name of this SymbolSet.
     * 
     * @return the name of this SymbolSet.
     */
    public String getName();

    /**
     * Return the (possibly <code>null</code>) value of the symbol identified by
     * the given name.
     * 
     * @param symbolname the name of the symbol from which the value must be
     *            retrieved.
     * @return the value of the symbol with the given name.
     */
    public String getSymbolValue(String symbolname);

    /**
     * Return a boolean indicating whether the SymbolSet contains a symbol with
     * the given name.
     * 
     * @param symbolname The symbol name whose presence in the SymbolSet is to
     *            be tested.
     * @return <tt>true</tt> if this SymbolSet contains a mapping for the
     *         specified symbol.
     */
    public boolean contains(String symbolname);

    /**
     * Set the map to be used as the base of the symbols available in the
     * SymolSet.
     * 
     * @param symbols the map to be used as the base of the symbols available in
     *            the SymolSet.
     */
    public void setSymbols(Map<String, String> symbols);

    /**
     * Set the name of this set of symbol definitions.
     * 
     * @param name the name to set.This is case-sensitive and must not be
     *            <code>null</code> or an empty String <code>""</code>.
     */
    public void setName(String name);

    /**
     * Returns a string representation of this SymbolSet. The string
     * representation consists of a list of the SymbolSet's elements in the
     * order they are returned by its iterator Adjacent elements are separated
     * by the characters <tt>","</tt> (comma).
     * 
     * This implementation creates an empty string buffer, iterates over the
     * SymbolSet appending the string representation of each element in turn.
     * After appending each element except the last, the string <tt>","</tt> is
     * appended. A string is obtained from the string buffer, and returned.
     * 
     * @return a string representation of this SymbolSet.
     */
    public String getSymbolSetString();

    /**
     * Removes the mapping for the symbol, identified by the given name, from
     * this SymbolSet if present.
     * 
     * @param symbolname the name of the symbol whose mapping is to be removed
     *            from the SymbolSet.
     */
    public void remove(String symbolname);

    /**
     * Remove a collection of symbols from the SymbolSet.
     * 
     * @param symbols a collection of symbols to be removed from the SymbolSet.
     */
    public void remove(Collection<ISymbol> c);

    /**
     * Returns the number of symbols on this SymbolSet.
     * 
     * @return number of symbols on this SymbolSet.
     */
    public int size();

    /**
     * Returns an array containing all of the elements in this SymbolSet.
     * 
     * @param a the array into which the elements of this collection are to be
     *            stored, if it is big enough; otherwise, a new array of the
     *            same runtime type is allocated for this purpose.
     * @return an array containing the elements of this SymbolSet.
     */
    public <T> T[] toArray(T[] a);
}
