/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.symbol;

/**
 * @author Diego Madruga Sandin
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface ISymbolSetConstants {

    /**
     * 
     */
    public static final String ELEMENT_SYMBOLDEFINITIONSREGISTRY = "symbolDefinitionsRegistry"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String ELEMENT_NAME_KEY = "mtjName"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String ELEMENT_KEYS_KEY = "mtjKeys"; //$NON-NLS-1$
    
    /**
     * 
     */
    public static final String ATT_CLASS = "class"; //$NON-NLS-1$
    
    /**
     * 
     */
    public static final String ATT_VERSION = "version"; //$NON-NLS-1$

}
