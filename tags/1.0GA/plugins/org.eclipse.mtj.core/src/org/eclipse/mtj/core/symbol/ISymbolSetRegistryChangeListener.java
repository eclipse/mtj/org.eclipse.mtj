/**
 * Copyright (c) 2008,2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.core.symbol;

/**
 * Classes which implement this interface provide a method that deals with the
 * events that are generated when the ISymbolSetRegistry is modified.
 * 
 * @since 1.0
 */
public interface ISymbolSetRegistryChangeListener {

    /**
     * Sent when the ISymbolSetRegistry is modified.
     */
    public void symbolSetRegistryChanged();

}
