/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.refactoring;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.text.edits.DeleteEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEdit;

/**
 * A TextFileChange for updating JAD files.
 * 
 * @author Craig Setera
 */
public class MidletJadTextFileMidletChange extends TextFileChange {
    private static final String REGEX = "MIDlet-\\d+:.*?,.*?,(";

    private Matcher matcher;
    private Pattern matchPattern;

    private int mode;
    private String oldName;
    private String newName;

    /**
     * Create a new change instance.
     * 
     * @param mode
     * @param name
     * @param file
     * @param matchPattern
     * @param newName
     */
    public MidletJadTextFileMidletChange(int mode, String name, IFile file,
            String oldName, String newName) {
        super(name, file);
        this.mode = mode;
        this.oldName = oldName;
        this.newName = newName;
    }

    /**
     * Return the matcher for handling the text file change.
     * 
     * @param pm
     * @return
     * @throws CoreException
     */
    Matcher getMatcher(IProgressMonitor pm) throws CoreException {
        if (matcher == null) {
            pm.setTaskName("Initializing Regular Expression Matcher");
            String content = getCurrentContent(pm);
            matcher = getMatchPattern().matcher(content);
            pm.worked(1);
        }

        return matcher;
    }

    /**
     * Initialize the text edit if it has not already been initialized.
     * 
     * @param pm
     * @throws CoreException
     */
    void initializeTextEdit(IProgressMonitor pm) throws CoreException {
        pm.setTaskName("Initializing Text Edit");

        TextEdit edit = null;
        Matcher matcher = getMatcher(pm);
        if (mode == MidletJadFileChangesCollector.MODE_RENAME) {
            int start = matcher.start(1);
            int end = matcher.end(1);
            edit = new ReplaceEdit(start, end - start, newName);
        } else {
            int start = matcher.start();
            int end = matcher.end();
            edit = new DeleteEdit(start, end - start);
        }

        setEdit(edit);
        pm.worked(1);
    }

    /**
     * Get the match pattern for matching against a JAD file in search of the
     * midlet name to be replaced.
     * 
     * @return
     */
    private Pattern getMatchPattern() {
        if (matchPattern == null) {
            StringBuffer sb = new StringBuffer(REGEX).append(oldName).append(
                    ")");
            matchPattern = Pattern.compile(sb.toString());
        }

        return matchPattern;
    }
}
