/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.internal.core.build.MTJBuildProperties;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 *	MTJBuildPropertiesResourceListener class implements {@link IResourceChangeListener}
 *  in order to add added and remove removed resources from the project's build.properties
 *  file.
 *  
 *  @author David Marques
 */
public class MTJBuildPropertiesResourceListener implements IResourceChangeListener {
	
	/* (non-Javadoc)
	 * @see org.eclipse.core.resources.IResourceChangeListener#resourceChanged(org.eclipse.core.resources.IResourceChangeEvent)
	 */
	public void resourceChanged(IResourceChangeEvent event) {
		IResourceDelta delta = event.getDelta();
		if (delta == null) {
			return;
		}

		try {
			AddsAndRemovesCollector collector = new AddsAndRemovesCollector();
			delta.accept(collector);
			
			List<IResourceDelta> sourceDeltas = filterResourceDeltas(collector.getResourceDeltas());
			updateBuildProperties(sourceDeltas);
		} catch (Exception e) {
			MTJLogger.log(IStatus.ERROR, e);
		}
	}

	/**
	 * Updates the build.properties files with the resources' changes.
	 * 
	 * @param sourceDeltas resources' deltas.
	 * @throws IOException Error storing the porperties file.
	 */
	private void updateBuildProperties(List<IResourceDelta> sourceDeltas) throws IOException {
		for (IResourceDelta sourceDelta : sourceDeltas) {
			IResource resource = sourceDelta.getResource();
			IProject  project  = resource.getProject();
			
			IMTJProject mtjProject = MidletSuiteFactory.
					getMidletSuiteProject(project.getName());
			MTJBuildProperties buildProperties = MTJBuildProperties
					.getBuildProperties(mtjProject);
			MTJRuntimeList runtimes = mtjProject.getRuntimeList();
			
			for (MTJRuntime runtime : runtimes) {
				List<IResource> resources = new ArrayList<IResource>();
				resources.addAll(Arrays.asList(buildProperties.getBuildProperty(runtime)));
				if (sourceDelta.getKind() == IResourceDelta.REMOVED) {
					resources.remove(resource);
				} else
				if (sourceDelta.getKind() == IResourceDelta.ADDED) {
					if (!resources.contains(resource)) {						
						resources.add(resource);
					}
				}
				buildProperties.setBuildProperty(runtime, resources.toArray(new IResource[resources.size()]));
			}
			buildProperties.store();
		}
	}

	/**
	 * Filters only resources within {@link IMTJProject} instances that
	 * are inside source folders.
	 * 
	 * @param resourceDeltas deltas to filter.
	 * @return the filtered list.
	 * @throws CoreException Any core error occurs.
	 */
	private List<IResourceDelta> filterResourceDeltas(List<IResourceDelta> resourceDeltas) throws CoreException {
		List<IResourceDelta> sourceDeltas = new ArrayList<IResourceDelta>();
		for (IResourceDelta resourceDelta : resourceDeltas) {
			IResource resource = resourceDelta.getResource();
			IProject  project  = resource.getProject();
			if (!project.exists() || !project.hasNature(JavaCore.NATURE_ID)) {
				continue;
			}
			IJavaProject javaProject = JavaCore.create(project);
			for (IResource folder : Utils.getSourceFolders(javaProject)) {
				if (folder.getProjectRelativePath()
						.isPrefixOf(resource.getProjectRelativePath())) {
					sourceDeltas.add(resourceDelta);
				}
			}
		}
		return sourceDeltas;
	}

	private class AddsAndRemovesCollector implements IResourceDeltaVisitor {

		private List<IResourceDelta> deltas;
		
		public AddsAndRemovesCollector() {
			this.deltas = new ArrayList<IResourceDelta>();
		}
		
		/* (non-Javadoc)
		 * @see org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.core.resources.IResourceDelta)
		 */
		public boolean visit(IResourceDelta delta) throws CoreException {
			IResource resource = delta.getResource();
			int       kind     = delta.getKind();
			if (kind == IResourceDelta.ADDED || kind == IResourceDelta.REMOVED) {
				if (resource.getProject() != null 
						&& resource.getType() == IResource.FILE) {
					this.deltas.add(delta);
				}
			}
			return true;
		}
		
		public List<IResourceDelta> getResourceDeltas() {
			return deltas;
		}
	}
}
