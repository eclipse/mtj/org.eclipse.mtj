/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Using default preverifier instead of embedded one.
 *     Hugo Raniere (Motorola)  - Returning a null preverifier in order to correctly use the default one.
 *     Diego Sandin (Motorola)  - Use JavaEmulatorDeviceProperties enum instead of 
 *                                hard-coded strings
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.sdk.device.IDeviceImporter;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * Provides common support operations for use in emulators that are launched as
 * Java classes.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @author Craig Setera
 */
public abstract class JavaEmulatorDeviceImporter implements IDeviceImporter {

    private static final String ATTR_MAIN_CLASS = "Main-Class"; //$NON-NLS-1$

    protected Properties deviceProperties;

    /**
     * Read in the device properties
     * 
     * @return
     */
    private Properties readDeviceProperties() {
        // Read the properties the define the device data
        Properties deviceProperties = new Properties();

        InputStream propertiesStream = null;
        try {
            URL propsFileURL = getDevicePropertiesURL();

            propertiesStream = propsFileURL.openStream();
            if (propertiesStream != null) {
                deviceProperties.load(propertiesStream);
            }

        } catch (IOException e) {
            MTJLogger
                    .log(IStatus.ERROR, Messages.JavaEmulatorDeviceImporter_0,
                            e);
        } finally {
            if (propertiesStream != null) {
                try {
                    propertiesStream.close();
                } catch (IOException e) {
                }
            }
        }

        return deviceProperties;
    }

    /**
     * Return the device definition properties.
     * 
     * @return
     */
    protected Properties getDeviceProperties() {
        if (deviceProperties == null) {
            deviceProperties = readDeviceProperties();
        }

        return deviceProperties;
    }

    /**
     * Return the URL to access the device properties file.
     * 
     * @return
     */
    protected abstract URL getDevicePropertiesURL();

    /**
     * Return the launch command template.
     * 
     * @return
     */
    protected String getLaunchCommand() {
        return getDeviceProperties().getProperty(
                JavaEmulatorDeviceProperties.LAUNCH_COMMAND.toString(),
                Utils.EMPTY_STRING);
    }

    /**
     * Return the preverifier to use.
     * 
     * @param jarFile
     * @return
     */
    protected IPreverifier getPreverifier(File jarFile) {
        return null;
    }

    /**
     * Return a boolean indicating whether the specified JAR file has a main
     * class attribute of the specified class name.
     * 
     * @param file
     * @param classname
     * @return
     * @throws IOException
     */
    protected boolean hasMainClassAttribute(File file, String classname)
            throws IOException {
        JarFile jarFile = new JarFile(file);
        Manifest manifest = jarFile.getManifest();
        Attributes attributes = manifest.getMainAttributes();
        String mainClass = attributes.getValue(ATTR_MAIN_CLASS);

        return ((mainClass != null) && mainClass.trim().equals(classname));
    }

    /**
     * Return a boolean indicating whether this device acts as a debug server.
     * 
     * @return
     */
    protected boolean isDebugServer() {
        return getDeviceProperties().getProperty(
                JavaEmulatorDeviceProperties.DEBUG_SERVER.toString(),
                Boolean.FALSE.toString()).equalsIgnoreCase(
                Boolean.TRUE.toString());
    }
}
