package org.eclipse.mtj.internal.core.sdk;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.osgi.framework.Version;

/**
 * 
 */

/**
 * The very basic SDK implementation.  This SDK is not stored/retrieved.  At this
 * point, the SDK is actually managed by the devices which is backwards from the 
 * intended goal.  This was done as a shortcut to get the {@link ISDK} interface
 * linked in for the 1.0 release and not as the final goal.
 * 
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @author Craig Setera
 *
 */
public class BasicSDK implements ISDK {
	// Interim handling of SDKs
	private static final HashMap<String, ISDK> sdks = new HashMap<String, ISDK>(); 
	
	/**
	 * Return the {@link ISDK} to use for the specified device.
	 * 
	 * @param device
	 * @param version
	 * @return
	 */
	public static ISDK getSDK(String sdkName, Version version) {
		String key = sdkName + "_" + version;
		ISDK sdk = sdks.get(key);
		if (sdk == null) {
			sdk = new BasicSDK(sdkName, version);
			sdks.put(key, sdk);
		}
		
		return sdk;
	}
	
	private String name;
	private Version version;
	private List<IDevice> deviceList;
	
	/**
	 * Construct a new BasicSDK.
	 * 
	 * @param name
	 */
	public BasicSDK(String name, Version version) {
		this.name = name;
		this.version = version;
		deviceList = new ArrayList<IDevice>();
	}
	
	/**
	 * Add a new device to the SDK.
	 * 
	 * @param device
	 */
	public void addDevice(IDevice device) {
		deviceList.add(device);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.sdk.ISDK#getDescription()
	 */
	public String getDescription() {
		return name;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.sdk.ISDK#getDeviceList()
	 */
	public List<IDevice> getDeviceList() throws CoreException {
		return deviceList;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.mtj.core.sdk.ISDK#getIdentifier()
	 */
	public String getIdentifier() {
		// TODO This should be handled better...
		return name;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.sdk.ISDK#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.sdk.ISDK#getVersion()
	 */
	public Version getVersion() {
		return version;
	}
}
