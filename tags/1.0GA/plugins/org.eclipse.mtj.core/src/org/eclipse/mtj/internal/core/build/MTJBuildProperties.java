/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Refactoring to allow listeners.
 */
package org.eclipse.mtj.internal.core.build;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.osgi.util.NLS;

/**
 * MTJBuildProperties class extends the {@link Properties} class
 * in order to implement build properties for the MTJ projects.
 * 
 * @author David Marques
 */
public class MTJBuildProperties extends Properties {

	private static final long serialVersionUID = -3576616481147533654L;
	private static Map<IMTJProject, MTJBuildProperties> map; 
	
	private List<MTJBuildPropertiesChangeListener> listeners;
	private IMTJProject mtjProject;
	
	/**
	 * Gets the MTJBuildProperties instance for the specified {@link IMTJProject}
	 * instance.
	 * 
	 * @param mtjProject target {@link IMTJProject} instance.
	 * @return the build properties.
	 */
	public static synchronized MTJBuildProperties getBuildProperties(IMTJProject mtjProject) {
		if (MTJBuildProperties.map == null) {
			MTJBuildProperties.map = new HashMap<IMTJProject, MTJBuildProperties>();
		}
		
		MTJBuildProperties buildProperties = MTJBuildProperties.map.get(mtjProject);
		if (buildProperties == null) {
			buildProperties = new MTJBuildProperties(mtjProject);
			MTJBuildProperties.map.put(mtjProject, buildProperties);
		}
		return buildProperties;
	}
	
	/**
	 * Creates an instance of a MTJBuildProperties class
	 * to setup build properties for the specified MYJ
	 * project.
	 * 
	 * @param _mtjProject target project.
	 */
	private MTJBuildProperties(IMTJProject _mtjProject) {
		if (_mtjProject == null) {
			throw new IllegalArgumentException(Messages.MTJBuildProperties_invalidMTJProject);
		}
		this.listeners  = new ArrayList<MTJBuildPropertiesChangeListener>();
		this.mtjProject = _mtjProject;
		
		IResource file = getBuildPropertyFile();
		if (file.exists()) {
			try {
				this.load(new FileInputStream(file.getLocation().toOSString()));
			} catch (IOException e) {
				MTJLogger.log(IStatus.ERROR, e);
			}
		}
	}
	
	/**
	 * Adds the change listener to the {@link MTJBuildProperties}.
	 * 
	 * @param listener target listener.
	 */
	public void addPropertiesChangeListener(
			MTJBuildPropertiesChangeListener listener) {
		synchronized (this.listeners) {			
			if (!this.listeners.contains(listener)) {
				this.listeners.add(listener);
			}
		}
	}
	
	/**
	 * Removes the change listener from the {@link MTJBuildProperties}.
	 * 
	 * @param listener target listener.
	 */
	public void removePropertiesChangeListener(
			MTJBuildPropertiesChangeListener listener) {
		synchronized (this.listeners) {			
			if (this.listeners.contains(listener)) {
				this.listeners.remove(listener);
			}
		}
	}
	
	/**
	 * Sets the build property on the build.properties file for
	 * the specified {@link MTJRuntime} instance.
	 * 
	 * @param runtime target runtime.
	 * @param resources build resources.
	 */
	public void setBuildProperty(MTJRuntime runtime, IResource[] resources) {
		List<IResource> files = new ArrayList<IResource>();
		for (IResource resource : resources) {
			//Do not store folders since their files
			//already keep them on their paths.
			if (resource instanceof IFile) {
				files.add(resource);
			}
		}
		IResource[] array = files.toArray(new IResource[files.size()]);
		this.setProperty(runtime.getName(), getResourcesString(array));
	}
	
	/**
	 * Gets the resources on the build.properties file for the
	 * specified {@link MTJRuntime} instance.
	 * 
	 * @param runtime target runtime.
	 * @return an array of {@link IResource} instances or null
	 * in case the {@link MTJRuntime} instance has no entry on
	 * the properties file.
	 */
	public IResource[] getBuildProperty(MTJRuntime runtime) {
		List<IResource> resources = new ArrayList<IResource>();
		String key = runtime.getName();
		IProject project = this.mtjProject.getProject();
		String   value   = this.getProperty(key);
		String[] paths   = value.split(","); //$NON-NLS-1$
		for (String path : paths) {
			IResource resource = project.findMember(path);
			//Do not store folders since their files
			//already keep them on their paths.
			if (resource != null && resource instanceof IFile) {
				resources.add(resource);
			}
		}
		return resources.toArray(new IResource[resources.size()]);
	}
	
	/* (non-Javadoc)
	 * @see java.util.Properties#getProperty(java.lang.String)
	 */
	public String getProperty(String key) {
		String result = super.getProperty(key);
		if (result == null) {
			result = getDefaultSourceContent();
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Properties#store(java.io.Writer, java.lang.String)
	 */
	public void store(Writer writer, String comments) throws IOException {
		PrintWriter printWriter = null;
		if (!(writer instanceof PrintWriter)) {
			printWriter = new PrintWriter(writer);
		} else {
			printWriter = (PrintWriter) writer; 
		}
		
		try {			
			writeProperties(printWriter, comments);
			printWriter.flush();
			printWriter.close();
		} finally {
			notifyListeners();
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.Properties#store(java.io.OutputStream, java.lang.String)
	 */
	public void store(OutputStream out, String comments) throws IOException {
		PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));
		this.store(writer, comments);
	}

	/**
	 * Stores the properties content into the build.properties file.
	 * 
	 * @throws IOException Any IO error occured.
	 */
	public void store() throws IOException {
		IResource file  = getBuildPropertyFile();
		
		PrintWriter printWriter = new PrintWriter(new FileWriter(file.getLocation().toFile()));
		this.store(printWriter, Messages.MTJBuildProperties_mtjBuildPropertiesComment);
		
		try {			
			if (!MTJCore.getWorkspace().isTreeLocked()) {
				file.refreshLocal(IResource.DEPTH_ZERO, new NullProgressMonitor());
			}
		} catch (CoreException e) {
			MTJLogger.log(IStatus.ERROR, e);
		}
	}
	
	/**
	 * Gets the build.properties file in the specified project.
	 * 
	 * @param project target project.
	 * @return a reosurce handler.
	 */
	public IFile getBuildPropertyFile() {
		return this.mtjProject.getProject().getFile("build.properties"); //$NON-NLS-1$
	}
	
	/**
	 * Gets the {@link IMTJProject} instance containing
	 * this buid.properties.
	 * 
	 * @return mtj project.
	 */
	public IMTJProject getMTJProject() {
		return this.mtjProject;
	}
	
	/**
	 * Gets the properties content as an {@link StringBuffer}.
	 * 
	 * @return stirng buffer.
	 */
	public StringBuffer getContent() {
		StringWriter writer = new StringWriter();
		writeProperties(new PrintWriter(writer), null);
		return writer.getBuffer();
	}
	
	/**
	 * Writes the properties into the {@link PrintWriter} instance.
	 * 
	 * @param printWriter target writer.
	 * @param comments source file comments.
	 */
	private void writeProperties(PrintWriter printWriter, String comments) {
		printWriter.println((comments == null ? Messages
				.MTJBuildProperties_mtjBuildPropertiesComment : comments));
		for (MTJRuntime runtime : this.mtjProject.getRuntimeList()) {
			String rName = runtime.getName();
			String value = this.getProperty(rName);
			printWriter.println(NLS.bind("{0}={1}", new String[] {rName, format(value)})); //$NON-NLS-1$
		}
	}
	
	/**
	 * Formats the property value in order to split comma
	 * separated values each one in a line.
	 * 
	 * @param value comma separated values.
	 * @return the formatted string.
	 */
	private String format(String value) {
		String separator = System.getProperty("line.separator"); //$NON-NLS-1$
		StringBuffer buffer = new StringBuffer();
		if (value != null) {
			String[] values = value.split(","); //$NON-NLS-1$
			for (String path : values) {
				if (buffer.length() > 0) {
					buffer.append(","); //$NON-NLS-1$
				}
				buffer.append(NLS.bind("{0}\\{1}" //$NON-NLS-1$
						, new String[] {path, separator}));
			}
		}
		return buffer.toString();
	}
	
	/**
	 * Gets all content from the projects source folders and
	 * formats as a comma separated {@link String} containing
	 * the resources project relative paths.
	 * 
	 * @return formatted string.
	 */
	private String getDefaultSourceContent() {
		IJavaProject javaProject = this.mtjProject.getJavaProject();
		IProject     project     = javaProject.getProject();

		List<IResource> resources = new ArrayList<IResource>();
		try {
			IClasspathEntry[] entries = javaProject.getRawClasspath();
			for (IClasspathEntry entry : entries) {
				if (entry.getEntryKind() != IClasspathEntry.CPE_SOURCE) {
					continue;
				}
				IFolder folder = project.getFolder(entry.getPath().removeFirstSegments(0x01));
				if (folder.exists()) {						
					ResourceCollector collector = new ResourceCollector();
					folder.accept(collector);
					resources.addAll(collector.getResources());
				}
			}
		} catch (CoreException e) {
			MTJLogger.log(IStatus.ERROR, e);
		}
		
		IResource[] array = resources.toArray(new IResource[resources.size()]);
		return getResourcesString(array);
	}
	
	/**
	 * Formats as string containing a comma separated list of
	 * resources.
	 * 
	 * @param resources array of resources.
	 * @return formatted string.
	 */
	private String getResourcesString(IResource[] resources) {
		StringBuffer buffer = new StringBuffer();
		for (IResource resource : resources) {
			String path = resource.getProjectRelativePath().toString();
			if (buffer.length() > 0) {
				buffer.append(","); //$NON-NLS-1$
			}
			buffer.append(path);
		}
		return buffer.toString();
	}
	
	private class ResourceCollector implements IResourceVisitor {
		private List<IResource> resources;
		
		public ResourceCollector() {
			this.resources = new ArrayList<IResource>();
		}

		/* (non-Javadoc)
		 * @see org.eclipse.core.resources.IResourceVisitor#visit(org.eclipse.core.resources.IResource)
		 */
		public boolean visit(IResource resource) throws CoreException {
			if (resource instanceof IFile) {				
				this.resources.add(resource);
			}
			return true;
		}
		
		/**
		 * Gets all resources.
		 * 
		 * @return resources list.
		 */
		public List<IResource> getResources() {
			return this.resources;
		}
	}
	
	/**
	 * Notifies all listeners.
	 */
	private void notifyListeners() {
		for (MTJBuildPropertiesChangeListener listener : this.listeners) {
			listener.propertiesChanged(this);
		}
	}
}
