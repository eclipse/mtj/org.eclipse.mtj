/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Gustavo de Paula (Motorola)  - Refactor preverification interfaces                                
 */
package org.eclipse.mtj.internal.core.build.preverifier;

import org.eclipse.mtj.core.build.preverifier.IPreverificationError;

/**
 * Some preverification utility functions until a better place comes along to
 * place them.
 * 
 * @author Craig Setera
 */
public class PreverificationUtils {
    /**
     * Return the error text for the specified preverification error.
     * 
     * @param error
     * @return
     */
    public static String getErrorText(IPreverificationError error) {
        StringBuffer errorText = new StringBuffer();

        // If details were passed in, we just use that outright
        String detail = error.getDetail();
        if ((detail != null) && (detail.length() > 0)) {
            errorText.append(detail);
        } else {
            appendErrorInformation(errorText, error);
        }

        return errorText.toString();
    }

    /**
     * Append the error information specific to the preverification error.
     * 
     * @param errorText
     * @param error
     */
    private static void appendErrorInformation(StringBuffer errorText,
            IPreverificationError error) {
        appendErrorTypeInformation(errorText, error);

        if (error.getType() != PreverificationErrorType.FINALIZERS) {
            appendErrorLocationInformation(errorText, error);
        }
    }

    /**
     * Append the error type information to the text.
     * 
     * @param errorText
     * @param error
     */
    private static void appendErrorLocationInformation(StringBuffer errorText,
            IPreverificationError error) {
        IClassErrorInformation classInfo = (IClassErrorInformation) ((IPreverificationErrorLocation) error
                .getLocation()).getClassInformation();

        switch (((IPreverificationErrorLocation) error.getLocation()).getLocationType().getTypeCode()) {
            case PreverificationErrorLocationType.CLASS_DEFINITION_CODE:
                errorText.append(" in class ").append(classInfo.getName())
                        .append(" or superclass");
                break;

        }
    }

    /**
     * Append the error type information to the text.
     * 
     * @param errorText
     * @param error
     */
    private static void appendErrorTypeInformation(StringBuffer errorText,
            IPreverificationError error) {
        // No detailed message was provided, so we will build one
        // up
        switch (((PreverificationErrorType) error.getType()).getErrorCode()) {
            case PreverificationErrorType.FINALIZERS_CODE:
                errorText.append("Finalizers not allowed");
                break;

            case PreverificationErrorType.FLOATING_POINT_CODE:
                errorText.append("Floating point not allowed");
                break;

            case PreverificationErrorType.MISSING_TYPE_CODE:
                errorText.append("Missing type");
                break;

            case PreverificationErrorType.NATIVE_CODE:
                errorText.append("Native method implementations not allowed");
                break;

            case PreverificationErrorType.UNKNOWN_ERROR_CODE:
                errorText.append("Unknown error during preverification");
                break;
        }
    }

    private PreverificationUtils() {
        super();
    }
}
