/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.build.sign;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.equinox.security.storage.ISecurePreferences;
import org.eclipse.equinox.security.storage.SecurePreferencesFactory;
import org.eclipse.equinox.security.storage.StorageException;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.project.midp.IMetaDataConstants;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * PreferencesSignatureProperties class extends SignatureProperties class
 * storing the signing properties on the workspace secure preferences. <br>
 * To store the properties call {@link #save()} method.
 * 
 * @see SignatureProperties
 * @see ISecurePreferences
 * 
 * @author David Marques
 * @since 1.0
 */
public class PreferencesSignatureProperties extends SignatureProperties {

    private static final String SIGNING_PREFERENCE_PATH = "mtj/preferences/signing"; //$NON-NLS-1$

    private ISecurePreferences preferences;

    /**
     * Creates a PreferencesSignatureProperties instance loading the properties
     * from the secure preferences in case it exists.
     * 
     * @throws StorageException if error occurred during property decryption.
     */
    public PreferencesSignatureProperties() throws CoreException {
        try {
            this.loadPropertiesFromPeferences();
        } catch (Exception e) {
            MTJStatusHandler
                    .throwCoreException(
                            IStatus.ERROR,
                            999,
                            Messages.PreferencesSignatureProperties_unableToLoadPropertiesFromPreferences);
        }
    }

    /**
     * Loads the preferences from the secure preferences in case it already
     * exists.
     * 
     * @throws StorageException - if error occurred during property decryption.
     */
    private void loadPropertiesFromPeferences() throws StorageException {
        ISecurePreferences root = SecurePreferencesFactory.getDefault();
        if (!root.nodeExists(SIGNING_PREFERENCE_PATH)) {
            return;
        }
        preferences = root.node(SIGNING_PREFERENCE_PATH);
        this.setKeyStoreDisplayPath(preferences.get(
                IMetaDataConstants.ELEM_KEYSTORE, null));
        this.setPasswordStorageMethod(preferences.getInt(
                IMetaDataConstants.ATTR_STOREPASSWORDS,
                SignatureProperties.PASSMETHOD_PROMPT));
        this.setKeyStorePassword(preferences.get(
                IMetaDataConstants.KEYRING_KEYSTOREPASS_KEY, null));
        this.setKeyAlias(preferences.get(IMetaDataConstants.ELEM_ALIAS, null));
        this.setKeyStoreProvider(preferences.get(
                IMetaDataConstants.ELEM_PROVIDER, null));
        this.setKeyStoreType(preferences.get(
                IMetaDataConstants.ELEM_KEYSTORETYPE, null));
    }

    /**
     * Saves the cached signing properties in the preferences.
     * 
     * @return <strong>true</strong> if success <strong>false</strong>
     *         otherwise.
     */
    public boolean save() {
        boolean saved = false;
        if (this.preferences == null) {
            ISecurePreferences root = SecurePreferencesFactory.getDefault();
            preferences = root.node(SIGNING_PREFERENCE_PATH);
        }

        try {
            this.preferences.clear();
            this.preferences.put(IMetaDataConstants.ELEM_KEYSTORE,
                    getKeyStoreDisplayPath(), false);
            this.preferences.put(IMetaDataConstants.KEYRING_KEYSTOREPASS_KEY,
                    getKeyStorePassword(), true);
            this.preferences.putInt(IMetaDataConstants.ATTR_STOREPASSWORDS,
                    getPasswordStorageMethod(), false);
            this.preferences.put(IMetaDataConstants.ELEM_ALIAS, getKeyAlias(),
                    false);
            this.preferences.put(IMetaDataConstants.ELEM_PROVIDER,
                    getKeyStoreProvider(), false);
            this.preferences.put(IMetaDataConstants.ELEM_KEYSTORETYPE,
                    getKeyStoreType(), false);
            saved = true;
        } catch (StorageException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
        return saved;
    }

    /**
     * Returns false since this {@link SignatureProperties} implementation is
     * used by projects that do not have specific signing settings.
     * 
     * @return false.
     */
    public boolean isProjectSpecific() {
        return false;
    }

    /**
     * Calling this method has no effect since this {@link SignatureProperties}
     * implementation is not project specific.
     * 
     * @param value - always false.
     */
    public void setProjectSpecific(boolean value) {
        super.setProjectSpecific(false);
    }

    /**
     * Calling this method returns true since this {@link SignatureProperties}
     * implementation only supports external keystore files.
     * 
     * @return true.
     */
    public boolean isKeyStorePathExternal() {
        return true;
    }
}
