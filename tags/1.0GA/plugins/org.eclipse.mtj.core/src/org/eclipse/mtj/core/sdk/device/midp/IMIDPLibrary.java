/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk.device.midp;

import org.eclipse.mtj.core.sdk.device.ILibrary;

/**
 * Represents a library available on {@link IMIDPDevice}'s.
 * 
 * @author Diego Sandin
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IMIDPLibrary extends ILibrary {

    /**
     * Return the first {@link IMIDPAPI} instance that matches the specified API
     * type.
     * 
     * @param apiType the type of API to be returned.
     * @return the first {@link IMIDPAPI} instance that matches the specified
     *         API type or <code>null</code> if none can be found.
     */
    public IMIDPAPI getAPI(MIDPAPIType apiType);

    /**
     * Return the {@link IMIDPAPI} instance that corresponds to a configuration.
     * 
     * @return the profile {@link IMIDPAPI} instance or <code>null</code> if
     *         this library does not provide a configuration.
     */
    public IMIDPAPI getConfiguration();

    /**
     * Return the {@link IMIDPAPI} instance that corresponds to a profile.
     * 
     * @return the profile {@link IMIDPAPI} instance or <code>null</code> if
     *         this library does not provide a profile.
     */
    public IMIDPAPI getProfile();

    /**
     * Return a boolean indicating whether this library contains a
     * configuration.
     * 
     * @return <code>true</code> if this library contains a configuration or
     *         <code>false</code> otherwise.
     */
    public boolean hasConfiguration();

    /**
     * Return a boolean indicating whether this library contains a profile.
     * 
     * @return <code>true</code> if this library contains a profile or
     *         <code>false</code> otherwise.
     */
    public boolean hasProfile();
}
