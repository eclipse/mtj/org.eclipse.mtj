/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk.device.midp;

import org.eclipse.mtj.core.sdk.device.IAPI;

/**
 * This an {@link IAPI} extension to provide utility methods to deal with
 * MIDP/CLDC specific APIs and will be used on {@link IMIDPDevice}'s.
 * 
 * @author Diego Sandin
 * @noimplement This interface is not intended to be implemented by clients.
 * @since 1.0
 */
public interface IMIDPAPI extends IAPI {

    /**
     * Return the type of the {@link IMIDPAPI} instance.
     * 
     * @return the API type
     */
    public abstract MIDPAPIType getType();

    /**
     * Set the type of the {@link IMIDPAPI} instance.
     * 
     * @param type the type to set
     */
    public void setType(MIDPAPIType type);
}
