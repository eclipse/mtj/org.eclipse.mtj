/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.sdk.device;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public enum LaunchTemplateProperties {

    /**
     * 
     */
    EXECUTABLE("executable"), //$NON-NLS-1$

    /**
     * 
     */
    DEVICE("device"), //$NON-NLS-1$

    /**
     * 
     */
    DEBUGPORT("debugPort"), //$NON-NLS-1$

    /**
     * 
     */
    CLASSPATH("classpath"), //$NON-NLS-1$

    /**
     * 
     */
    VERBOSE("verbose"), //$NON-NLS-1$

    /**
     * 
     */
    HEAPSIZE("heapsize"), //$NON-NLS-1$

    /**
     * 
     */
    SECURITYDOMAIN("securityDomain"), //$NON-NLS-1$

    /**
     * 
     */
    USERSPECIFIEDARGUMENTS("userSpecifiedArguments"), //$NON-NLS-1$

    /**
     * 
     */
    JADFILE("jadfile"), //$NON-NLS-1$

    /**
     * 
     */
    OTAURL("otaurl"), //$NON-NLS-1$

    /**
     * 
     */
    TARGET("target"); //$NON-NLS-1$

    /**
     * 
     */
    private final String property;

    /**
     * @param property
     */
    LaunchTemplateProperties(final String property) {
        this.property = property;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return property;
    }
}
