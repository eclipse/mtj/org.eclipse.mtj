/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)    - Initial implementation
 *     Diego Sandin (Motorola)     - Refactoring package name to follow eclipse 
 *                                   standards
 *     Hugo Raniere (Motorola)     - Removing Preprocessor code
 *     Diego Sandin (Motorola)     - Re-enabling Preprocessor code
 *     David Marques (Motorola)    - Adding a workspace ResourceChangeListener.
 *     David Marques (Motorola)    - Adding getResourceAsStream method.
 *     Gustavo de Paula (Motorola) - Add preverifier factory     
 *     Gustavo de Paula (Motorola) - Add mtj project converter
 *     David Marques (Motorola)    - Adding build.properties resource changes
 *                                   listener.     
 */
package org.eclipse.mtj.core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.IMetaData;
import org.eclipse.mtj.core.project.ProjectType;
import org.eclipse.mtj.core.project.midp.IMIDPMetaData;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.IDeviceFinder;
import org.eclipse.mtj.core.sdk.device.IDeviceRegistry;
import org.eclipse.mtj.core.sdk.device.ILibrary;
import org.eclipse.mtj.core.sdk.device.ILibraryImporter;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary;
import org.eclipse.mtj.core.symbol.ISymbolSetFactory;
import org.eclipse.mtj.core.symbol.ISymbolSetRegistry;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.preprocessor.PreprocessedSourceMapper;
import org.eclipse.mtj.internal.core.build.preverifier.StandardPreverifierFactory;
import org.eclipse.mtj.internal.core.hook.sourceMapper.SourceMapperAccess;
import org.eclipse.mtj.internal.core.project.midp.MetaData;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.sdk.device.DeviceClasspath;
import org.eclipse.mtj.internal.core.sdk.device.DeviceFinder;
import org.eclipse.mtj.internal.core.sdk.device.DeviceRegistry;
import org.eclipse.mtj.internal.core.sdk.device.midp.MIDPAPI;
import org.eclipse.mtj.internal.core.sdk.device.midp.MIDPLibrary;
import org.eclipse.mtj.internal.core.sdk.device.midp.UEILibraryImporter;
import org.eclipse.mtj.internal.core.symbol.SymbolSetFactory;
import org.eclipse.mtj.internal.core.symbol.SymbolSetRegistry;
import org.eclipse.mtj.internal.core.util.MTJBuildPropertiesResourceListener;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;

/**
 * The main plug-in class to be used in the workbench.
 * 
 * <p>
 * The single instance of this class can be accessed from any plug-in declaring
 * the MTJ Core plug-in as a prerequisite via <code>MTJCore.getJavaCore()</code>
 * . The Java model plug-in will be activated automatically if not already
 * active.
 * </p>
 * 
 * 
 * @since 1.0
 * @noinstantiate This class is not intended to be instantiated by clients.
 */
public class MTJCore extends Plugin {

    /** The single instance of the MTJ core plug-in runtime class. */
    private static MTJCore plugin;

    /**
     * Construct a new metadata object for the given type of project.
     * <p>
     * <strong>NOTE:</strong> Each type of IMetaData is associated to a specific
     * type of project. For example, the {@link IMIDPMetaData} is associated to
     * {@link IMidletSuiteProject}.
     * </p>
     * <p>
     * This metadata will not be persisted until the
     * {@link IMetaData#saveMetaData()} method is invoked.
     * </p>
     * 
     * @param project the project to be associated to the metadata.
     * @param projectType the project type associated to this metadata.
     * @return a new metadata instance for the given type of project.
     */
    public static IMetaData createMetaData(IProject project,
            ProjectType projectType) {

        switch (projectType) {
            case MIDLET_SUITE:
                return new MetaData(project);

            default:
                return null;
        }
    }

    /**
     * Create a new {@link IAPI} instance.
     * <p>
     * <strong>NOTE:</strong> Each type of API is associated to a specific type
     * of project. For example, the {@link IMIDPAPI} is associated to
     * {@link IMidletSuiteProject}.
     * </p>
     * 
     * @param projectType the type of project.
     * @return the new {@link IAPI} instance. Possible instances are:
     *         <ul>
     *         <li>If projectType was {@link ProjectType#MIDLET_SUITE} this
     *         method will return an {@link IMIDPAPI} instance.</li>
     *         <li>If projectType was {@link ProjectType#UNKNOWN} this method
     *         will return <code>null</code>.</li>
     *         </ul>
     */
    public static IAPI createNewAPI(ProjectType projectType) {

        switch (projectType) {
            case MIDLET_SUITE:
                return new MIDPAPI();

            default:
                return null;
        }
    }

    /**
     * Construct a new deviceClasspath instance.
     * 
     * @return a new deviceClasspath instance.
     */
    public static IDeviceClasspath createNewDeviceClasspath() {
        return new DeviceClasspath();
    }

    /**
     * Create a new {@link ILibrary} instance.
     * <p>
     * <strong>NOTE:</strong> Each type of library is associated to a specific
     * type of project. For example, the {@link IMIDPLibrary} is associated to
     * {@link IMidletSuiteProject}.
     * </p>
     * 
     * @param projectType the type of project.
     * @return the new {@link ILibrary} instance. Possible instances are:
     *         <ul>
     *         <li>If projectType was {@link ProjectType#MIDLET_SUITE} this
     *         method will return an {@link IMIDPLibrary} instance.</li>
     *         <li>If projectType was {@link ProjectType#UNKNOWN} this method
     *         will return <code>null</code>.</li>
     *         </ul>
     */
    public static ILibrary createNewLibrary(ProjectType projectType) {

        switch (projectType) {
            case MIDLET_SUITE:
                return new MIDPLibrary();

            default:
                return null;
        }
    }

    /**
     * Create a new Preverifier instance based on the preverifier type. Returns
     * <code>null</code> if the preverifier cannot be created for some reason.
     * 
     * @param preverifierType type of the preverifier to be created
     * @param param Any parameter that is required by each preverifier type.
     *            Please check PreverifierType for information about each type
     *            available and the parameters they accept
     * @return the preverifier instance or <code>null</code> if the preverifier
     *         cannot be created for some reason.
     * @throws CoreException if failed to get the preverifier parameters.
     */
    public static IPreverifier createPreverifier(String preverifierType,
            Object param) throws CoreException {
        IPreverifier preverifier = null;

        if (preverifierType.equals(IPreverifier.PREVERIFIER_STANDARD)) {
            if (param instanceof File) {
                preverifier = StandardPreverifierFactory
                        .createPreverifier((File) param);
            }
        }

        return preverifier;
    }

    /**
     * Returns the active MTJ project associated with the specified IProject, or
     * <code>null</code> if no MTJ project yet exists for the IProject.
     * 
     * @param project the project for extracting the associated MTJ type of
     *            project.
     * @return the IMTJProject instance associated to the given project.
     *         <code>null</code> will be returned in case no type of MTJ project
     *         was associated to the given project.
     */
    public static IMTJProject geMTJProject(IProject project) {
        if (MidletSuiteFactory.isMidletSuiteProject(project)) {

            IJavaProject javaProject = JavaCore.create(project);
            IMidletSuiteProject suite = MidletSuiteFactory
                    .getMidletSuiteProject(javaProject);
            return suite;
        }
        return null;

    }

    /**
     * Get the deployment directory name the user has specified in the
     * preferences.
     * 
     * @return the name of the deployment folder specified by the user in the
     *         preferences.
     */
    public static String getDeploymentDirectoryName() {
        return getMTJCore().getPluginPreferences().getString(
                IMTJCoreConstants.PREF_DEPLOYMENT_DIR);
    }

    /**
     * Return the {@link IDeviceFinder} instance that can be used to find for
     * devices in a specific directory.
     * 
     * @return the {@link IDeviceFinder} instance.
     */
    public static IDeviceFinder getDeviceFinder() {
        return DeviceFinder.getInstance();
    }

    /**
     * Returns the {@link IDeviceRegistry} instance that can be used to manage
     * the devices available in MTJ.
     * 
     * @return the {@link IDeviceRegistry} instance.
     */
    public static IDeviceRegistry getDeviceRegistry() {
        return DeviceRegistry.getInstance();
    }

    /**
     * Returns a library importer associated to a specific type. The library
     * importer is used to importer the libraries defined by each SDK.
     * <p>
     * Clients should refer to {@link ILibraryImporter} documentation to find
     * the list of importers.
     * </p>
     * 
     * @param type library importer type.
     * @return the {@link ILibraryImporter} instance for the given type.
     */
    public static ILibraryImporter getLibraryImporter(String type) {
        ILibraryImporter result = null;

        if (type.equals(ILibraryImporter.LIBRARY_IMPORTER_UEI)) {
            result = new UEILibraryImporter();
        }
        return result;
    }

    /**
     * Returns the single instance of the MTJ core plug-in runtime class.
     * 
     * @return the single instance of the MTJ core plug-in runtime class.
     */
    public static MTJCore getMTJCore() {
        return plugin;
    }

    /**
     * Return MTJ Core plug-in version.
     * 
     * @return the MTJ Core plug-in version
     */
    public static String getMTJCoreVersion() {
        Bundle bundle = MTJCore.getMTJCore().getBundle();
        return (String) bundle.getHeaders().get(Constants.BUNDLE_VERSION);
    }

    /**
     * Return the File instance representing the Proguard implementation jar
     * file as defined by the user preferences. This File is not guaranteed to
     * exist.
     * 
     * @return the file instance for the Proguard jar.
     */
    public static File getProguardJarFile() {

        String proguardDirPref = getMTJCore().getPluginPreferences().getString(
                IMTJCoreConstants.PREF_PROGUARD_DIR);
        File proguardDir = new File(proguardDirPref);
        File proguardLibDir = new File(proguardDir, "lib"); //$NON-NLS-1$
        File proguardJar = new File(proguardLibDir,
                IMTJCoreConstants.PROGUARD_JAR);

        return proguardJar;
    }

    /**
     * Gets the InputStream for a resource inside the plug-in.
     * 
     * @param path relative resource path.
     * @return the resource InputStream.
     */
    public static InputStream getResourceAsStream(IPath path) {
        InputStream stream = null;

        URL url = FileLocator.find(getMTJCore().getBundle(), path, null);
        try {
            stream = url.openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream;
    }

    /**
     * Get the resources directory name the user has specified in the
     * preferences.
     * 
     * @return the name of the directory to be used for resources, as specified
     *         in the preferences.
     */
    public static String getResourcesDirectoryName() {
        return getMTJCore().getPluginPreferences().getString(
                IMTJCoreConstants.PREF_RESOURCES_DIR);
    }

    /**
     * Returns the symbol set factory that can be used create symbolsets and
     * symbols.
     * 
     * @return the {@link ISymbolSetFactory} instance.
     */
    public static ISymbolSetFactory getSymbolSetFactory() {
        return SymbolSetFactory.getInstance();
    }

    /**
     * Returns the symbol set registry that can be used to manage the symbols
     * that were saved on the workspace
     * 
     * @return the {@link ISymbolSetRegistry} instance.
     */
    public static ISymbolSetRegistry getSymbolSetRegistry() {
        return SymbolSetRegistry.getInstance();
    }

    /**
     * Get the verified output directory name the user has specified in the
     * preferences.
     * 
     * @return the name of the directory to be used for verified classes, as
     *         specified in the preferences.
     */
    public static String getVerifiedOutputDirectoryName() {
        return getMTJCore().getPluginPreferences().getString(
                IMTJCoreConstants.PREF_VERIFIED_DIR);
    }

    /**
     * Returns the workspace.
     * 
     * @return the workspace that was created by the single instance of this
     *         plug-in class.
     */
    public static IWorkspace getWorkspace() {
        return ResourcesPlugin.getWorkspace();
    }

    /**
     * Creates the MTJ core plug-in.
     * 
     * @noreference This constructor is not intended to be referenced by
     *              clients.
     */
    public MTJCore() {
        super();

        if (plugin == null) {
            plugin = this;
        }

        // Setting MidletSuiteFactory as the workspace ResourceChangeListener.
        MTJCore.getWorkspace().addResourceChangeListener(
                new IResourceChangeListener() {
                    /* (non-Javadoc)
                    * @see org.eclipse.core.resources.IResourceChangeListener#resourceChanged(org.eclipse.core.resources.IResourceChangeEvent)
                    */
                    public void resourceChanged(IResourceChangeEvent event) {
                        switch (event.getType()) {
                            case IResourceChangeEvent.PRE_DELETE:
                                IResource resource = event.getResource();
                                if (resource instanceof IProject) {
                                    IProject project = (IProject) resource;
                                    /*Removes IMidletSuiteProject instances from cache upon project deletion.*/
                                    IMTJProject meProject = MidletSuiteFactory
                                            .getMidletSuiteProject(project
                                                    .getName());
                                    if (meProject != null) {
                                        MidletSuiteFactory
                                                .removeMidletSuiteProject(meProject
                                                        .getJavaProject());
                                    }
                                }
                                break;
                        }
                    }
                });

        MTJCore.getWorkspace().addResourceChangeListener(
                new MTJBuildPropertiesResourceListener(),
                IResourceChangeEvent.POST_CHANGE);
    }

    /**
     * Returns the symbolic name of MTJUIPlugin as specified by its
     * Bundle-SymbolicName manifest header.
     * 
     * @return the symbolic name of MTJUIPlugin.
     */
    public static String getPluginId() {
        return getMTJCore().getBundle().getSymbolicName();
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.runtime.Plugin#start(org.osgi.framework.BundleContext)
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);

        // Install the preprocessor source mapper
        SourceMapperAccess.setSourceMapper(new PreprocessedSourceMapper());
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        super.stop(context);
    }
}
