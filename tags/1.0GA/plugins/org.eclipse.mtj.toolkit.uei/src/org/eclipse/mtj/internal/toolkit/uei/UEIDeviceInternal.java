/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.toolkit.uei;

import java.io.File;
import java.util.Properties;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.ILibraryImporter;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.sdk.BasicSDK;
import org.eclipse.mtj.internal.core.sdk.device.JavadocDetector;
import org.eclipse.mtj.internal.core.sdk.device.JavadocDetector.GenericLocalFSSearch;
import org.eclipse.mtj.internal.core.sdk.device.midp.UEILibraryImporter;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.toolkit.uei.properties.DeviceSpecificProperties;
import org.eclipse.mtj.internal.toolkit.uei.properties.UEIDeviceDefinition;
import org.osgi.framework.Version;

/**
 * Unified Emulator Interface (UEI) internal implementation.
 * 
 * @author Diego Madruga Sandin
 */
public final class UEIDeviceInternal extends UEIDevice {

    public UEIDeviceInternal() {
        super();
    }

    /**
     * Create a new instance of UEIDeviceInternal based on the properties for
     * that device found with the
     * <code>emulator {@link EmulatorInfoArgs#XQUERY -Xquery}</code> command.
     * 
     * @param name the device name.
     * @param groupName the device group name.
     * @param description the device description.
     * @param properties the properties for the device.
     * @param definition information on how the UEI emulator will be launched.
     * @param emulatorExecutable the emulator application.
     * @param preverifier the preverifier application.
     * @throws IllegalArgumentException if the properties or definition are
     *             <code>null</code>.
     */
    public UEIDeviceInternal(String name, String groupName, String description,
            Properties properties, UEIDeviceDefinition definition,
            File emulatorExecutable, IPreverifier preverifier)
            throws IllegalArgumentException {

        if (properties == null) {
            throw new IllegalArgumentException(
                    Messages.UEIDevice_null_properties);
        } else if (definition == null) {
            throw new IllegalArgumentException(
                    Messages.UEIDevice_null_definition);
        }

        this.setName(name);
        this.setDescription(description);
        this.setGroupName(groupName);

        this.setDeviceProperties(properties);
        this.setClasspath(getBootClasspath(properties));
        this.setProtectionDomains(getProtectionDomains(properties));

        this.setExecutable(emulatorExecutable);
        this.setPreverifier(preverifier);

        this.setDebugServer(definition.isDebugServer());
        this.setLaunchCommandTemplate(definition.getLaunchTemplate());

        this.setBundle(UeiPlugin.getDefault().getBundle().getSymbolicName());

        ISymbolSet dss = MTJCore.getSymbolSetFactory()
                .createSymbolSetFromDevice(this);
        ISymbolSet pss = MTJCore.getSymbolSetFactory()
                .createSymbolSetFromProperties(deviceProperties);
        dss.add(pss.getSymbols());
        dss.setName(this.getName());
        this.setSymbolSet(dss);

        // Associate the SDK with the device... This doesn't really belong
        // here, but for 1.0 it is done this way to make the API work until
        // ISDK can be fully built out post-1.0
        Version v = new Version(1,0,0);
        ISDK sdk = BasicSDK.getSDK(groupName, v);
        this.setSDK(sdk);
        ((BasicSDK) sdk).addDevice(this);
    }

    /**
     * Import the device's deviceClasspath and return it.
     * 
     * @param deviceProperties
     * @return
     */
    private IDeviceClasspath getBootClasspath(Properties deviceProperties) {

        IDeviceClasspath deviceClasspath = MTJCore.createNewDeviceClasspath();
        String classpathString = deviceProperties.getProperty(
                DeviceSpecificProperties.BOOTCLASSPATH.toString(),
                Utils.EMPTY_STRING);

        String[] classpathEntries = classpathString.split(","); //$NON-NLS-1$

        // create a new javadoc detector which will search the
        // javadoc of the library automatically
        JavadocDetector javadocDetector = new JavadocDetector()
                .addJavadocSearchStrategy(new GenericLocalFSSearch());
        UEILibraryImporter libraryImporter = (UEILibraryImporter) MTJCore
                .getLibraryImporter(ILibraryImporter.LIBRARY_IMPORTER_UEI);
        libraryImporter.setJavadocDetector(javadocDetector);

        for (String element : classpathEntries) {
            File libraryFile = new File(element);
            if (libraryFile.exists()) {
                deviceClasspath.addEntry(libraryImporter
                        .createLibraryFor(libraryFile));
            }
        }

        return deviceClasspath;
    }

    /**
     * Return the protection domains specified for this device.
     * 
     * @param deviceProperties
     * @return
     */
    private String[] getProtectionDomains(Properties deviceProperties) {
        String domainsString = deviceProperties.getProperty(
                DeviceSpecificProperties.SECURITY_DOMAINS.toString(),
                Utils.EMPTY_STRING);
        return domainsString.split(","); //$NON-NLS-1$
    }
}
