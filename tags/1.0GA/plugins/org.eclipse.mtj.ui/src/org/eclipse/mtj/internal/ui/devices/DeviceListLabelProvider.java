/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.internal.ui.devices;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.mtj.core.sdk.device.IDevice;

/**
 * Label provider implementation for use in list contexts such as a device combo
 * or list viewer.
 * 
 * @author Craig Setera
 */
public class DeviceListLabelProvider extends LabelProvider {

    /**
     * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
     */
    public String getText(Object element) {
        StringBuffer sb = new StringBuffer();

        IDevice device = (IDevice) element;
        sb.append(device.getSDKName()).append(" / ").append(device.getName());

        return sb.toString();
    }
}
