/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.internal.ui.devices;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Image;

/**
 * Label provider implementation that provides label information for an instance
 * of IDevice.
 * 
 * @author Craig Setera
 */
public class DeviceTableLabelProvider extends LabelProvider implements
        ITableLabelProvider {

    private static final String TEXT_UNKNOWN = "";
    private static final int COL_CHECKBOX = 0;
    private static final int COL_GROUP_NAME = 1;
    private static final int COL_NAME = 2;
    private static final int COL_CONFIGURATION = 3;
    private static final int COL_PROFILE = 4;

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
     */
    public Image getColumnImage(Object element, int columnIndex) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
     */
    public String getColumnText(Object element, int columnIndex) {
        String text = TEXT_UNKNOWN;
        IDevice device = (IDevice) element;

        switch (columnIndex) {
            case COL_CHECKBOX:
                text = "";
                break;

            case COL_NAME:
                text = device.getName();
                break;

            case COL_GROUP_NAME:
                text = device.getSDKName();
                break;

            case COL_CONFIGURATION:
                text = getConfigurationText((IMIDPDevice) device);
                break;

            case COL_PROFILE:
                text = getProfileText((IMIDPDevice) device);
                break;
        }

        return text;
    }

    /**
     * Return the text for the device's configuration library.
     * 
     * @param device
     * @return
     */
    private String getConfigurationText(IMIDPDevice device) {
        String text = TEXT_UNKNOWN;

        IAPI api = device.getCLDCAPI();
        if (api != null) {
            text = NLS.bind(IMTJCoreConstants.VERSION_NLS_BIND_TEMPLATE,
                    new Object[] { api.getVersion().getMajor(),
                            api.getVersion().getMinor() });
        }

        return text;
    }

    /**
     * Return the text for the device's profile library.
     * 
     * @param device
     * @return
     */
    private String getProfileText(IMIDPDevice device) {
        String text = TEXT_UNKNOWN;

        IAPI api = device.getMIDPAPI();
        if (api != null) {
            text = NLS.bind(IMTJCoreConstants.VERSION_NLS_BIND_TEMPLATE,
                    new Object[] { api.getVersion().getMajor(),
                            api.getVersion().getMinor() });
        }

        return text;
    }
}
