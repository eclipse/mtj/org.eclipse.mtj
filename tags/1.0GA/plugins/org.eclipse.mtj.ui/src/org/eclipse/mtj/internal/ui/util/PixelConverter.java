/*******************************************************************************
 * Copyright (c) 2000, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Adapted code available in JDT
 */
package org.eclipse.mtj.internal.ui.util;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontMetrics;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Control;

/**
 * This class will provide measurement information about fonts.
 * 
 * @since 0.9.1
 */
public class PixelConverter {

    /**
     * This instance will provide measurement information about fonts
     */
    private final FontMetrics fFontMetrics;

    /**
     * Constructs a new instance of PixelConverter.
     * 
     * @param control the Control were we get the current font witch will be used to create a
     *            FontMetrics object responsible for drawing and measuring text.
     */
    public PixelConverter(Control control) {
        this(control.getFont());
    }

    /**
     * Constructs a new instance of PixelConverter.
     * 
     * @param font the font to be used to create a FontMetrics object responsible for drawing and
     *            measuring text.
     */
    public PixelConverter(Font font) {
        GC gc = new GC(font.getDevice());
        gc.setFont(font);
        fFontMetrics = gc.getFontMetrics();
        gc.dispose();
    }

    /**
     * Returns the number of pixels corresponding to the height of the given number of characters.
     * 
     * @see org.eclipse.jface.dialogs.DialogPage#convertHeightInCharsToPixels(int)
     */
    public int convertHeightInCharsToPixels(int chars) {
        return Dialog.convertHeightInCharsToPixels(fFontMetrics, chars);
    }

    /**
     * Returns the number of pixels corresponding to the given number of horizontal dialog units.
     * 
     * @see org.eclipse.jface.dialogs.DialogPage#convertHorizontalDLUsToPixels(int)
     */
    public int convertHorizontalDLUsToPixels(int dlus) {
        return Dialog.convertHorizontalDLUsToPixels(fFontMetrics, dlus);
    }

    /**
     * Returns the number of pixels corresponding to the given number of vertical dialog units.
     * 
     * @see org.eclipse.jface.dialogs.DialogPage#convertVerticalDLUsToPixels(int)
     */
    public int convertVerticalDLUsToPixels(int dlus) {
        return Dialog.convertVerticalDLUsToPixels(fFontMetrics, dlus);
    }

    /**
     * Returns the number of pixels corresponding to the width of the given number of characters.
     * 
     * @see org.eclipse.jface.dialogs.DialogPage#convertWidthInCharsToPixels(int)
     */
    public int convertWidthInCharsToPixels(int chars) {
        return Dialog.convertWidthInCharsToPixels(fFontMetrics, chars);
    }

}
