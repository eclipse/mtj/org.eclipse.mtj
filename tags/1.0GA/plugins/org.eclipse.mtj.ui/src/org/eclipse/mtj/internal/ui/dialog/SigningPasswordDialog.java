/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.ui.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * SigningPasswordDialog class provides a dialog for user to
 * enter the keystore password.
 * 
 * @author David Marques
 * @since 1.0
 */
public class SigningPasswordDialog extends Dialog {

	private Label  label;
	private Text   text;
	private Button button;
	
	private String title;
	private String description;
	
	private String  password;
	private boolean doSave;
	private boolean enableSaving;
	
	/**
	 * Creates a SigningPasswordDialog using the specified shell.
	 * 
	 * @param parentShell parent shell instance.
	 * @param enableSavingPassword if true enables saving password in
	 * workspace keyring false otherwise.
	 */
	public SigningPasswordDialog(Shell parentShell, boolean enableSavingPassword) {
		super(parentShell);
		this.enableSaving = enableSavingPassword;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	protected Control createDialogArea(Composite parent) {
		Composite main = new Composite(parent, SWT.NONE);
		main.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		main.setLayout(new GridLayout(0x01, true));
		
		label = new Label(main, SWT.NONE);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		if (this.description != null) {
			this.label.setText(this.description);
		}
		
		text = new Text(main, SWT.PASSWORD | SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		button = new Button(main, SWT.CHECK);
		button.setText(MTJUIMessages.SigningPasswordDialog_savePassword);
		button.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		button.setEnabled(this.enableSaving);
		return main;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		if (this.title != null) {			
			newShell.setText(this.title);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	protected void okPressed() {
		this.password = this.text.getText();
		this.doSave	  = this.button.getSelection();
		super.okPressed();
	}
	
	/**
	 * Sets the dialog's description.
	 * 
	 * @param text description text.
	 */
	public void setDescription(String text) {
		this.description = text;
	}

	/**
	 * Sets the dialog's title.
	 * 
	 * @param title dialog title.
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Gets whether the user has selected
	 * to save password in workspace keyring.
	 * 
	 * @return true if save seleced false otherwise.
	 */
	public boolean isSavingInWorkspace() {
		return this.doSave;
	}
	
	/**
	 * Gets the user entered password.
	 * 
	 * @return password string.
	 */
	public String getPassword() {
		String result = "";
		if (this.password != null) {
			result = this.password;
		}
		return result;
	}
	
}
