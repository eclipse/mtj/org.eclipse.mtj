/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Add the support to group the devices by SDK
 *     Hugo Raniere (Motorola)  - Removing default and apply buttons per 
 *                                discussion on bugzilla
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.internal.ui.preferences;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.persistence.PersistableUtilities;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.devices.DeviceEditorConfigElement;
import org.eclipse.mtj.internal.ui.devices.DeviceEditorRegistry;
import org.eclipse.mtj.internal.ui.devices.DeviceImportWizard;
import org.eclipse.mtj.internal.ui.devices.DeviceTableLabelProvider;
import org.eclipse.mtj.internal.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.internal.ui.viewers.TableViewerConfiguration;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

/**
 * Implements the preference page for managing the registered devices.
 * 
 * @author Craig Setera
 */
public class DeviceManagementPreferencePage extends PreferencePage implements
        IWorkbenchPreferencePage {

    /**
     * Implementation of the table's content provider.
     */
    private class DeviceTableContentProvider implements
            IStructuredContentProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;
            String groupName = (String) inputElement;
            try {
                List<IDevice> devices;
                if (ALL_SDKS.equalsIgnoreCase(groupName)) {
                    devices = MTJCore.getDeviceRegistry().getAllDevices();
                } else {
                    devices = MTJCore.getDeviceRegistry().getDevices(groupName);
                }
                if (devices != null) {
                    elements = devices.toArray(new IDevice[devices.size()]);
                }
            } catch (PersistenceException e) {
                handleException(
                        MTJUIMessages.DeviceManagementPreferencePage_error_retrieving_devices,
                        e);
            }

            return elements;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    private class SDKContentProvider implements IStructuredContentProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            try {
                List<String> registeredSDKs = MTJCore.getDeviceRegistry()
                        .getSDKNames();
                if ((registeredSDKs != null) && (registeredSDKs.size() > 1)) {
                    registeredSDKs.add(ALL_SDKS);
                }
                elements = registeredSDKs.toArray(new String[registeredSDKs
                        .size()]);
            } catch (PersistenceException e) {
                handleException(
                        MTJUIMessages.DeviceManagementPreferencePage_error_retrieving_groups,
                        e);
            }

            return elements;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {

        }
    }

    /**
     * Identifier of this preference page
     */
    public static final String ID = "org.eclipse.mtj.ui.preferences.deviceManagementPreferencePage"; //$NON-NLS-1$

    private static final String ALL_SDKS = MTJUIMessages.DeviceManagementPreferencePage_all_sdks;

    // Holds the last selected SDK so it may be reselected.
    private static final String PREF_KEY_SELECTED_SDK = "pref_selected_SDK";

    private static final TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
            new TableColumnInfo(
                    MTJUIMessages.DeviceManagementPreferencePage_default_columnInfo,
                    10f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceManagementPreferencePage_group_columnInfo,
                    22.5f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceManagementPreferencePage_name_columnInfo,
                    22.5f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceManagementPreferencePage_configuration_columnInfo,
                    22.5f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceManagementPreferencePage_profile_columnInfo,
                    22.5f, null), };

    // Column information structure
    private static final int DEFAULT_TABLE_WIDTH = 550;
    private static final Object[] NO_ELEMENTS = new Object[0];

    // A pattern for locating previously unique-ified names.
    private static final Pattern UNIQUE_NAME_PATTERN = Pattern
            .compile("^.+\\((\\d+)\\)$"); //$NON-NLS-1$

    private Button deleteButton;
    private CheckboxTableViewer deviceViewer;
    private Button duplicateButton;
    private IAction editAction;
    private Button editButton;
    private IActionDelegate editDelegate;

    private ComboViewer sdkViewer;

    private boolean updatingCheckState;
    // Widgets
    private IWorkbench workbench;

    /**
     * Construct a new preference page instance
     */
    public DeviceManagementPreferencePage() {
        super(MTJUIMessages.DeviceManagementPreferencePage_title);
        setDescription(MTJUIMessages.DeviceManagementPreferencePage_description);
        setPreferenceStore(MTJUIPlugin.getDefault().getCorePreferenceStore());

        noDefaultAndApplyButton();

        editAction = new Action() {
        };
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
        this.workbench = workbench;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performCancel()
     */
    @Override
    public boolean performCancel() {
        performDefaults();
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        boolean wasOK = false;

        try {
            MTJCore.getDeviceRegistry().store();
            wasOK = true;
        } catch (Exception e) {
            MTJUIPlugin
                    .displayError(
                            getShell(),
                            IStatus.ERROR,
                            -999,
                            MTJUIMessages.DeviceManagementPreferencePage_error_storing_devises_dialog_title,
                            MTJUIMessages.DeviceManagementPreferencePage_error_storing_devises_dialog_message,
                            e);
            MTJLogger
                    .log(
                            IStatus.ERROR,
                            NLS
                                    .bind(
                                            MTJUIMessages.DeviceManagementPreferencePage_error_storing_devises_log_msg,
                                            new String[] {
                                                    e.getClass().getName(),
                                                    e.getMessage() }), e);
        }

        return wasOK;
    }

    /**
     * Create contents for a normal error-free case.
     * 
     * @param parent
     * @return
     */
    private Control createDeviceContents(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(2, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.minimumWidth = DEFAULT_TABLE_WIDTH;
        gridData.heightHint = 400;
        deviceViewer = createTableViewer(composite);
        deviceViewer.getTable().setLayoutData(gridData);

        Composite buttonComposite = new Composite(composite, SWT.NONE);
        buttonComposite.setLayout(new GridLayout(1, true));
        buttonComposite.setLayoutData(new GridData(GridData.FILL_VERTICAL));

        Button importButton = new Button(buttonComposite, SWT.PUSH);
        importButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        importButton
                .setText(MTJUIMessages.DeviceManagementPreferencePage_importButton_label_text);
        importButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                importDevices();
            }
        });

        editButton = new Button(buttonComposite, SWT.PUSH);
        editButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        editButton
                .setText(MTJUIMessages.DeviceManagementPreferencePage_editButton_lable_text);
        editButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleEditButton();
            }
        });

        duplicateButton = new Button(buttonComposite, SWT.PUSH);
        duplicateButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        duplicateButton
                .setText(MTJUIMessages.DeviceManagementPreferencePage_duplicateButton_label_text);
        duplicateButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleDuplicateButton();
            }
        });

        deleteButton = new Button(buttonComposite, SWT.PUSH);
        deleteButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        deleteButton
                .setText(MTJUIMessages.DeviceManagementPreferencePage_deleteButton_label_text);
        deleteButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleDeleteButton();
            }
        });

        // Wire up some enablement handling
        updateButtonEnablement(deviceViewer.getSelection());
        updateCheckboxState();
        deviceViewer
                .addSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        updateButtonEnablement(event.getSelection());
                    }
                });

        return composite;
    }

    /**
     * Create the contents for a device registry exception.
     * 
     * @param parent
     * @param e
     * @return
     */
    private Control createErrorContents(Composite parent, Exception e) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, true));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
        String text = NLS
                .bind(
                        MTJUIMessages.DeviceManagementPreferencePage_error_reading_device_registry,
                        new String[] { e.getClass().getName(), e.getMessage() });

        Text errorText = new Text(composite, SWT.BORDER | SWT.MULTI
                | SWT.READ_ONLY | SWT.WRAP);
        errorText.setLayoutData(new GridData(GridData.FILL_BOTH));
        errorText.setText(text);

        return composite;
    }

    /**
     * Create SDK contents composite
     * 
     * @param parent
     */
    private void createSDKContents(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(2, false));
        composite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Label label = new Label(composite, SWT.NONE);
        label.setFont(parent.getFont());
        label
                .setText(MTJUIMessages.DeviceManagementPreferencePage_instaled_sdks_label);
        // label.setLayoutData(new GridData(SWT.FILL, 0, true, false));

        sdkViewer = createSDKViewer(composite);
    }

    /**
     * Create SDK viewer
     * 
     * @param composite
     * @return
     */
    private ComboViewer createSDKViewer(Composite composite) {
        ComboViewer comboViewer = new ComboViewer(composite);
        comboViewer.setContentProvider(new SDKContentProvider());
        comboViewer.setInput(new Object());
        comboViewer
                .addSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        String sdkName = (String) ((IStructuredSelection) event
                                .getSelection()).getFirstElement();
                        deviceViewer.setInput(sdkName);
                        updateCheckboxState();

                        // Store the current SDK name as a preference so it
                        // can be reselected next time the dialog is opened
                        if (sdkName != null) {
                            getPreferenceStore().setValue(
                                    PREF_KEY_SELECTED_SDK, sdkName);
                        }
                    }
                });

        GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
        gridData.minimumWidth = 300;
        comboViewer.getCombo().setLayoutData(gridData);
        return comboViewer;
    }

    /**
     * Create the devices table viewer.
     * 
     * @param parent
     */
    private CheckboxTableViewer createTableViewer(Composite composite) {
        int styles = SWT.MULTI | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION
                | SWT.CHECK;
        Table table = new Table(composite, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        // Wire up the viewer
        CheckboxTableViewer viewer = new CheckboxTableViewer(table);
        viewer.setContentProvider(new DeviceTableContentProvider());
        viewer.setLabelProvider(new DeviceTableLabelProvider());
        viewer.addCheckStateListener(new ICheckStateListener() {
            public void checkStateChanged(CheckStateChangedEvent event) {
                handleCheckStateChange(event);
            }
        });
        viewer.addDoubleClickListener(new IDoubleClickListener() {
            public void doubleClick(DoubleClickEvent event) {
                IStructuredSelection selection = (IStructuredSelection) event
                        .getSelection();
                IMIDPDevice device = (IMIDPDevice) selection.getFirstElement();
                editDevice(device);
            }
        });

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings("deviceManagementViewerSettings"); //$NON-NLS-1$
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 1);
        viewerConfiguration.configure(viewer);

        return viewer;
    }

    /**
     * Edit the specified device.
     * 
     * @param device
     */
    private void editDevice(IMIDPDevice device) {
        if (editDelegate != null) {
            editDelegate.run(editAction);
            deviceViewer.refresh(getSelectedDevice(), true);
        }
    }

    /**
     * Return the action delegate for the specified device or <code>null</code>
     * if no delegate can be found.
     * 
     * @param device
     * @return
     */
    private IActionDelegate findActionDelegate(IDevice device) {
        IActionDelegate delegate = null;

        DeviceEditorConfigElement element = DeviceEditorRegistry
                .findEditorElement(device);
        if (element != null) {
            try {
                delegate = element.getActionDelegate();
            } catch (CoreException e) {
                MTJLogger
                        .log(
                                IStatus.WARNING,
                                MTJUIMessages.DeviceManagementPreferencePage_error_findActionDelegate,
                                e);
            }
        }

        return delegate;
    }

    /**
     * Find a new name that is unique using the specified name as a base.
     * 
     * @param name
     * @return
     */
    private String findUniqueName(IDevice device) {
        String uniqueName = null;

        String groupName = device.getSDKName();
        String baseName = device.getName();

        for (int i = 1; i <= 100; i++) {
            StringBuffer deviceNameBuffer = new StringBuffer(baseName);

            Matcher matcher = UNIQUE_NAME_PATTERN.matcher(deviceNameBuffer);
            if (matcher.find()) {
                int matchStart = matcher.start(1);
                int matchEnd = matcher.end(1);

                deviceNameBuffer.replace(matchStart, matchEnd, Integer
                        .toString(i));

            } else {
                deviceNameBuffer.append(" (").append(i).append(")"); //$NON-NLS-1$ //$NON-NLS-2$
            }

            uniqueName = deviceNameBuffer.toString();

            try {
                IMIDPDevice foundDevice = (IMIDPDevice) MTJCore
                        .getDeviceRegistry().getDevice(groupName, uniqueName);

                if (foundDevice == null) {
                    break;
                }
            } catch (PersistenceException e) {
                MTJLogger
                        .log(
                                IStatus.ERROR,
                                MTJUIMessages.DeviceManagementPreferencePage_error_find_device,
                                e);
            }
        }

        return uniqueName;
    }

    /**
     * Return the currently selected device.
     * 
     * @return
     */
    private IDevice getSelectedDevice() {
        return (IDevice) ((IStructuredSelection) deviceViewer.getSelection())
                .getFirstElement();
    }

    /**
     * The check state has changed, handle appropriately.
     * 
     * @param event
     */
    private void handleCheckStateChange(CheckStateChangedEvent event) {
        if (!updatingCheckState && event.getChecked()) {
            // Switching to checked... Handle appropriately
            IDevice newDefault = (IDevice) event.getElement();
            MTJCore.getDeviceRegistry().setDefaultDevice(newDefault);
            updateCheckboxState();
        }
    }

    /**
     * Handle the delete button being selected.
     */
    private void handleDeleteButton() {
        IStructuredSelection selection = (IStructuredSelection) deviceViewer
                .getSelection();
        int count = selection.size();

        String title = MTJUIMessages.DeviceManagementPreferencePage_confirm_delete_dialog_title;
        String message = NLS
                .bind(
                        MTJUIMessages.DeviceManagementPreferencePage_confirm_delet_dialog_message,
                        count);
        if (MessageDialog.openConfirm(getShell(), title, message)) {
            Iterator<?> items = selection.iterator();
            while (items.hasNext()) {
                IDevice device = (IDevice) items.next();
                try {
                    MTJCore.getDeviceRegistry().removeDevice(device);
                } catch (PersistenceException e) {
                    handleException(
                            MTJUIMessages.DeviceManagementPreferencePage_error_remove_device,
                            e);
                }
            }

            deviceViewer.refresh();
        }
    }

    /**
     * Handle the duplicate button being selected.
     */
    private void handleDuplicateButton() {
        IDevice device = getSelectedDevice();
        try {
            IDevice clonedDevice = (IDevice) PersistableUtilities
                    .clonePersistable(device);
            String uniqueName = findUniqueName(clonedDevice);
            clonedDevice.setName(uniqueName);
            MTJCore.getDeviceRegistry().enableDeviceAddedEvent(true);
            MTJCore.getDeviceRegistry().addDevice(clonedDevice);

            deviceViewer.refresh();

            deviceViewer.setSelection(new StructuredSelection(clonedDevice));

        } catch (Exception e) {
            handleException(
                    MTJUIMessages.DeviceManagementPreferencePage_error_duplicated_device,
                    e);
        }
    }

    /**
     * Handle the edit button being selected.
     */
    private void handleEditButton() {
        IMIDPDevice device = (IMIDPDevice) getSelectedDevice();
        editDevice(device);
    }

    /**
     * Handle the specified exception by displaying to the user and logging.
     * 
     * @param message
     * @param throwable
     */
    private void handleException(String message, Throwable throwable) {
        MTJLogger
                .log(
                        IStatus.WARNING,
                        MTJUIMessages.DeviceManagementPreferencePage_error_device_registry_exception,
                        throwable);
        MTJUIPlugin
                .displayError(
                        getShell(),
                        IStatus.WARNING,
                        -999,
                        MTJUIMessages.DeviceManagementPreferencePage_error_device_registry_error,
                        message, throwable);
    }

    /**
     * Prompt the user to import some devices using the import device wizard.
     */
    private void importDevices() {
        DeviceImportWizard wizard = new DeviceImportWizard();
        WizardDialog dialog = new WizardDialog(workbench
                .getActiveWorkbenchWindow().getShell(), wizard);

        if (dialog.open() == Window.OK) {
            sdkViewer.refresh();
            deviceViewer.refresh();

            // Attempt to select the "ALL" SDK's combo item so the
            // user can see what they imported.
            sdkViewer.setSelection(new StructuredSelection(ALL_SDKS), true);

            // If for some unknown reason, that failed, attempt
            // to select the first SDK in the list.
            if (sdkViewer.getSelection().isEmpty()) {
                selectFirstSDK();
            }

            IMIDPDevice defaultDevice = (IMIDPDevice) MTJCore
                    .getDeviceRegistry().getDefaultDevice();
            if (defaultDevice == null) {
                selectStartingDefaultDevice();
                updateCheckboxState();
            }
        }
    }

    /**
     * Select the first SDK in the sdkViewer
     */
    private void selectFirstSDK() {
        if (sdkViewer.getCombo().getItemCount() > 0) {
            sdkViewer.getCombo().select(0);
            deviceViewer.setInput(sdkViewer.getCombo().getText());
            updateCheckboxState();
        }
    }

    /**
     * Attempt to select the previously selected SDK on dialog open. If the
     * selection fails, go ahead and select the first SDK in the list instead.
     */
    private void selectPreviousSDK() {
        String sdkName = getPreferenceStore().getString(PREF_KEY_SELECTED_SDK);
        if (sdkName != null) {
            IStructuredSelection selection = new StructuredSelection(sdkName);
            sdkViewer.setSelection(selection, true);
        }

        if (sdkViewer.getSelection().isEmpty()) {
            selectFirstSDK();
        }
    }

    /**
     * Select a starting default device from the current available input.
     */
    private void selectStartingDefaultDevice() {
        DeviceTableContentProvider contentProvider = (DeviceTableContentProvider) deviceViewer
                .getContentProvider();
        Object[] elements = contentProvider
                .getElements(deviceViewer.getInput());

        if ((elements != null) && (elements.length > 0)) {
            MTJCore.getDeviceRegistry().setDefaultDevice((IDevice) elements[0]);
        }
    }

    /**
     * Update the enablement of the buttons in the preference page.
     * 
     * @param selection
     */
    private void updateButtonEnablement(ISelection selection) {
        IDevice device = getSelectedDevice();
        editDelegate = (device == null) ? null : findActionDelegate(device);
        if (editDelegate != null) {
            editDelegate.selectionChanged(editAction, selection);
        }

        int selectedCount = ((IStructuredSelection) deviceViewer.getSelection())
                .size();
        duplicateButton.setEnabled(selectedCount == 1);
        editButton.setEnabled((editDelegate != null) && editAction.isEnabled());
        deleteButton.setEnabled(selectedCount > 0);
    }

    /**
     * Update the state of the checkboxes based on the default value in the
     * registry.
     */
    private void updateCheckboxState() {
        updatingCheckState = true;

        deviceViewer.setAllChecked(false);

        IDevice defaultDevice = MTJCore.getDeviceRegistry().getDefaultDevice();
        if (defaultDevice != null) {
            deviceViewer.setChecked(defaultDevice, true);
        }

        updatingCheckState = false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        layout.makeColumnsEqualWidth = true;
        container.setLayout(layout);
        GridData gd = new GridData(GridData.FILL_BOTH);
        container.setLayoutData(gd);
        container.setFont(parent.getFont());

        try {
            MTJCore.getDeviceRegistry().load();
            createSDKContents(container);
            createDeviceContents(container);
            selectPreviousSDK();
        } catch (Exception e) {
            MTJLogger.log(IStatus.ERROR, e);
            createErrorContents(container, e);
        }

        PlatformUI.getWorkbench().getHelpSystem().setHelp(container,
                "org.eclipse.mtj.ui.help_DeviceManagementPage"); //$NON-NLS-1$
        return container;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
     */
    @Override
    protected void performDefaults() {
        try {
            MTJCore.getDeviceRegistry().clear();
            MTJCore.getDeviceRegistry().load();
        } catch (Exception e) {
            MTJUIPlugin
                    .displayError(
                            getShell(),
                            IStatus.ERROR,
                            -999,
                            MTJUIMessages.DeviceManagementPreferencePage_error_reloding_devices_dialog_title,
                            MTJUIMessages.DeviceManagementPreferencePage_error_reloding_devices_dialog_message,
                            e);
            MTJLogger
                    .log(
                            IStatus.ERROR,
                            MTJUIMessages
                                    .bind(
                                            MTJUIMessages.DeviceManagementPreferencePage_error_loading_devices,
                                            new String[] {
                                                    e.getClass().getName(),
                                                    e.getMessage() }), e);
        }
    }
}
