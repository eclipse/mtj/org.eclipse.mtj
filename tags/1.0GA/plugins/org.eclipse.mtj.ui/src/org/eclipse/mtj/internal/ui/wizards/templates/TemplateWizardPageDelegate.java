/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version.
 */
package org.eclipse.mtj.internal.ui.wizards.templates;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.mtj.internal.ui.preferences.ScrolledPageContent;
import org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

/**
 * TemplateWizardPageDelegate class works as a partial Delegate for MIDlet
 * Wizard template custom page. It dispatched some IWizardPage calls to
 * the template's AbstractTemplateWizardPage implementation.
 * <br>
 * It stands inside the Wizard for all template custom pages, changing
 * dynamically as the selected template changes in the template list.
 *
 * @see Delegate Pattern.
 * 
 * @author David Marques
 * @since 1.0
 */
public class TemplateWizardPageDelegate extends AbstractTemplateWizardPage {

    // Constants -----------------------------------------------------

    // Attributes ----------------------------------------------------

    private AbstractTemplateWizardPage page;
    private ScrolledPageContent scrolledComposite;
    
    // Static --------------------------------------------------------

    // Constructors --------------------------------------------------

    /**
     * Creates a TemplateWizardPageDelegate to delegate calls to
     * the specified page. The page has the specified name.
     * 
     * @param _name delegate page name.
     * @param _page delegated page object.
     */
    public TemplateWizardPageDelegate(String _name, AbstractTemplateWizardPage _page) {
        super(_name);
        
        if (this.page == null) {
            throw new IllegalArgumentException("Decorated page must not be null.");
        }
        this.page = _page;
    }
    
    /**
     * Creates a TemplateWizardPageDelegate unattached to
     * a specific page object. A latter call to setPage will
     * set the delegated page object.
     * 
     * @param _name delegate page name.
     */
    public TemplateWizardPageDelegate(String _name) {
        super(_name);
    }
    
    // Public --------------------------------------------------------

    /**
     * Sets the page for delegation.
     * 
     * @param _page target page.
     */
    public void setPage(AbstractTemplateWizardPage _page) {
        if (_page == null) {
            return;
        }
        this.page = _page;
        this.page.setWizard(this.getWizard());
        this.createTemplateControl();
    }
    
    /**
     * Checks whether the delegate is attached
     * to an object.
     * 
     * @return true if attached false otherwise.
     */
    public boolean isAtached() {
        return this.page != null;
    }

    // X implementation ----------------------------------------------

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.wizards.templates.ITemplateWizardPage#getDictionary()
     */
    public Map<String, String> getDictionary() {
        if (this.page == null) {
            return new HashMap<String, String>();
        }
        return this.page.getDictionary();
    }

    // Y overrides ---------------------------------------------------

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public final void createControl(Composite parent) {
        this.scrolledComposite = this.createScrolledPageContent(parent);
        if (this.page != null) {
            this.createTemplateControl();
        }
        this.setControl(this.scrolledComposite);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.IWizardPage#isPageComplete()
     */
    public boolean isPageComplete() {
        if (this.page == null) {
            return true;
        }
        return page.isPageComplete();
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.DialogPage#getErrorMessage()
     */
    public String getErrorMessage() {
        if (this.page == null) {
            return super.getErrorMessage();
        }
        return page.getErrorMessage();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.DialogPage#getMessage()
     */
    public String getMessage() {
        if (this.page == null) {
            return super.getMessage();
        }
        return page.getMessage();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.DialogPage#performHelp()
     */
    public void performHelp() {
        if (this.page == null) {
            super.performHelp();
        }
        page.performHelp();
    }
    
    // Package protected ---------------------------------------------

    // Protected -----------------------------------------------------

    // Private -------------------------------------------------------
    
    /**
     * Create the ScrolledPageContent that is needed for resizing on expand the
     * expandable scrolledComposite.
     * 
     * @return the ScrolledPageContent
     */
    private ScrolledPageContent createScrolledPageContent(Composite parent) {

        ScrolledPageContent scrolledContent = new ScrolledPageContent(parent);
        scrolledContent.setLayout(new GridLayout());
        scrolledContent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                true));

        return scrolledContent;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage#createTemplateControl(org.eclipse.swt.widgets.Composite)
     */
    private void createTemplateControl() {
        Composite body = scrolledComposite.getBody();
        if ((body != null) && !body.isDisposed()) {
            body.dispose();
        }

        body = new Composite(scrolledComposite, SWT.NONE);
        body.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        this.page.createControl(body);
        this.scrolledComposite.setContent(body);
        this.scrolledComposite.reflow(true);
    }
}
