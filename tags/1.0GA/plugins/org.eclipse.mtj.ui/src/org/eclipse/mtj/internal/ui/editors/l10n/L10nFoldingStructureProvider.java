/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.IEditingModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nObject;
import org.eclipse.mtj.internal.ui.editor.AbstractFoldingStructureProvider;
import org.eclipse.mtj.internal.ui.editor.MTJSourcePage;

/**
 * @since 0.9.1
 */
public class L10nFoldingStructureProvider extends
        AbstractFoldingStructureProvider {

    private Map<Position, IDocumentElementNode> fPositionToElement = new HashMap<Position, IDocumentElementNode>();

    /**
     * @param editor
     * @param model
     */
    public L10nFoldingStructureProvider(MTJSourcePage editor,
            IEditingModel model) {
        super(editor, model);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.IFoldingStructureProvider#addFoldingRegions(java.util.Set, org.eclipse.mtj.internal.core.text.IEditingModel)
     */
    public void addFoldingRegions(Set<Position> currentRegions,
            IEditingModel model) throws BadLocationException {

        L10nObject locales = ((L10nModel) model).getLocales();
        List<?> childList = locales.getChildren();
        IDocumentElementNode[] children = childList
                .toArray(new IDocumentElementNode[childList.size()]);

        addFoldingRegions(currentRegions, children, model.getDocument());
    }

    /**
     * @param regions
     * @param nodes
     * @param document
     * @throws BadLocationException
     */
    private void addFoldingRegions(Set<Position> regions,
            IDocumentElementNode[] nodes, IDocument document)
            throws BadLocationException {
        for (IDocumentElementNode element : nodes) {
            int startLine = document.getLineOfOffset(element.getOffset());
            int endLine = document.getLineOfOffset(element.getOffset()
                    + element.getLength());
            if (startLine < endLine) {
                int start = document.getLineOffset(startLine);
                int end = document.getLineOffset(endLine)
                        + document.getLineLength(endLine);
                Position position = new Position(start, end - start);
                regions.add(position);
                fPositionToElement.put(position, element);
            }
            IDocumentElementNode[] children = element.getChildNodes();
            if (children != null) {
                addFoldingRegions(regions, children, document);
            }
        }
    }

}
