/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.internal.ui.devices;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;

/**
 * Provides registry functionality for attaching editors to particular device
 * types.
 * 
 * @author Craig Setera
 */
public class DeviceEditorRegistry {

    /**
     * The extension point
     */
    public static final String ID_EXTENSION = "deviceeditor";

    private static DeviceEditorConfigElement[] editors;

    /**
     * Return the editor configuration element for editing the specified device
     * or <code>null</code> if no editor element can be found.
     * 
     * @param device
     * @return
     */
    public static DeviceEditorConfigElement findEditorElement(IDevice device) {
        DeviceEditorConfigElement element = null;

        DeviceEditorConfigElement[] elements = getEditors();
        Class<? extends IDevice> deviceClass = (Class<? extends IDevice>) device.getClass();
        for (DeviceEditorConfigElement element2 : elements) {
            String matchClassName = element2.getDeviceClass();
            if (isMatchingType(deviceClass, matchClassName)) {
                element = element2;
                break;
            }
        }

        return element;
    }

    /**
     * Return the editor extensions as an array of DeviceEditorConfigElement
     * instances.
     * 
     * @return
     */
    private static DeviceEditorConfigElement[] getEditors() {
        if (editors == null) {
            editors = readEditors();
        }

        return editors;
    }

    /**
     * Return a boolean if the specified class matches the specified class name
     * directly or via super types and interfaces.
     * 
     * @param type
     * @param matchClassName
     * @return
     */
    private static boolean isMatchingType(Class<?> type, String matchClassName) {
        boolean matches = false;

        if (type != null) {
            matches = type.getName().equals(matchClassName);

            // Test super classes
            if (!matches) {
                matches = isMatchingType(type.getSuperclass(), matchClassName);
            }

            // Test interfaces
            if (!matches) {
                Class<?>[] interfaces = type.getInterfaces();
                for (int i = 0; !matches && (i < interfaces.length); i++) {
                    matches = isMatchingType(interfaces[i], matchClassName);
                }
            }
        }

        return matches;
    }

    /**
     * Return the editor extensions as a list of DeviceEditorConfigElement
     * instances.
     * 
     * @return
     */
    private static DeviceEditorConfigElement[] readEditors() {
        String plugin = MTJUIPlugin.getDefault().getBundle().getSymbolicName();
        IConfigurationElement[] configElements = Platform
                .getExtensionRegistry().getConfigurationElementsFor(plugin,
                        ID_EXTENSION);

        DeviceEditorConfigElement[] elements = new DeviceEditorConfigElement[configElements.length];
        for (int i = 0; i < configElements.length; i++) {
            elements[i] = new DeviceEditorConfigElement(configElements[i]);
        }

        return elements;
    }

    /**
     * Static-only access.
     */
    private DeviceEditorRegistry() {
        super();
    }
}
