/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant;

/**
 * This class is used to describe the preprocess symbol's provider's
 * information, such as provider name, provide value, etc. Each symbol can have
 * several PreprocessSymbolProviderInfos
 * 
 * @author gma
 * @see <code>PreprocessSymbol</code>
 * @since 0.9.1
 */
public class PreprocessSymbolProviderInfo {
    String providerName;
    String providedSymbolValue;
    boolean isActive;

    public PreprocessSymbolProviderInfo(String providerName,
            String providedSymbolValue, boolean isActive) {
        this.providerName = providerName;
        this.providedSymbolValue = providedSymbolValue;
        this.isActive = isActive;
    }

    public String getProvidedSymbolValue() {
        return providedSymbolValue;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProvidedSymbolValue(String providedSymbolValue) {
        this.providedSymbolValue = providedSymbolValue;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

}
