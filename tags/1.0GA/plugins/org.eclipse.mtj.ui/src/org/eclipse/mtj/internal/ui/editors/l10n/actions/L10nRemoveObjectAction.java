/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.mtj.internal.core.text.l10n.L10nObject;
import org.eclipse.mtj.internal.ui.MTJUIMessages;

public class L10nRemoveObjectAction extends Action {

    // The object designated for removal
    private L10nObject[] l10nObjects;

    // The next object to be selected after the
    // selected object is removed
    private L10nObject objectToSelect;

    /**
     * 
     */
    public L10nRemoveObjectAction() {

        // Adds the 'Delete' keybinding to the action when displayed
        // in a context menu
        setActionDefinitionId("org.eclipse.ui.edit.delete"); //$NON-NLS-1$

        setText(MTJUIMessages.L10nRemoveObjectAction_text);
        l10nObjects = null;
        objectToSelect = null;
    }

    /**
     * @return the object that should be selected after the current one is
     *         removed
     */
    public L10nObject getNextSelection() {
        return objectToSelect;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {
        if (l10nObjects == null) {
            return;
        }

        for (int i = 0; i < l10nObjects.length; ++i) {
            if ((l10nObjects[i] != null) && l10nObjects[i].canBeRemoved()) {
                L10nObject parent = l10nObjects[i].getParent();
                if ((parent != null) && parent.canBeParent()) {
                    // Determine the object to select after the deletion
                    // takes place
                    determineNextSelection(parent, i);
                    // Remove the L10n object
                    (parent).removeChildNode(l10nObjects[i], true);
                }
            }
        }
    }

    /**
     * @param tocObject the object to remove
     */
    public void setToRemove(L10nObject tocObject) {
        l10nObjects = new L10nObject[] { tocObject };
    }

    /**
     * @param tocObjects the objects to remove
     */
    public void setToRemove(L10nObject[] tocObjects) {
        l10nObjects = tocObjects;
    }

    /**
     * Determine the next object that should be selected after the designated
     * object has been removed
     * 
     * @param parent The parent of the deleted object
     */
    private void determineNextSelection(L10nObject parent, int index) {
        // Select the next sibling
        objectToSelect = parent.getNextSibling(l10nObjects[index]);
        if (objectToSelect == null) {
            // No next sibling
            // Select the previous sibling
            objectToSelect = parent.getPreviousSibling(l10nObjects[index]);
            if (objectToSelect == null) {
                // No previous sibling
                // Select the parent
                objectToSelect = parent;
            }
        }
    }
}
