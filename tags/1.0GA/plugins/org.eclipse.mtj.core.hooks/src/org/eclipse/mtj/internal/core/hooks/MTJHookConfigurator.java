/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.hooks;

import org.eclipse.osgi.baseadaptor.HookConfigurator;
import org.eclipse.osgi.baseadaptor.HookRegistry;

/**
 * Hook configurator to link in MTJ class loading overrides.
 * 
 * @author Craig Setera
 */
public class MTJHookConfigurator implements HookConfigurator {

    public MTJHookConfigurator() {
        super();

        if (Debug.DEBUG_GENERAL) {
            System.out.println("Hook configurator instantiated");
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.osgi.baseadaptor.HookConfigurator#addHooks(org.eclipse.osgi.baseadaptor.HookRegistry)
     */
    public void addHooks(HookRegistry hookRegistry) {
        if (Debug.DEBUG_GENERAL) {
            System.out.println("Adding MTJ class loading hook");
        }

        hookRegistry.addClassLoadingHook(new MTJClassLoadingHook());
    }
}
