/*******************************************************************************
 * Copyright (c) 2000, 2004 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.mtj.ui.internal.forms.blocks;

import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.mtj.ui.IMTJUIConstants;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.MasterDetailsBlock;
import org.eclipse.ui.forms.SectionPart;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

/**
 *
 */
public class ScrolledPropertiesBlock extends MasterDetailsBlock {

    private TableViewer viewer;

    private List<DetailPage> detailPages;

    private ISelection currentSelectedItem;

    /**
     * 
     */
    private IStructuredContentProvider masterContentProvider;

    /**
     * 
     */
    private MasterLabelProvider masterLabelProvider;

    /**
     * 
     */
    private ButtonBarBlock buttonBarBlock = null;

    /**
     * 
     */
    private FormPage page;

    /**
     * @param page
     * @param contentProvider
     * @param labelProvider
     */
    public ScrolledPropertiesBlock(FormPage page,
            IStructuredContentProvider contentProvider,
            MasterLabelProvider labelProvider, List<DetailPage> detailPages) {
        this.page = page;
        this.masterContentProvider = contentProvider;
        this.masterLabelProvider = labelProvider;
        this.detailPages = detailPages;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.IMasterLabelProvider#getButtonBarBlock()
     */
    public ButtonBarBlock getButtonBarBlock() {
        return buttonBarBlock;
    }

    /**
     * @return the currentSelectedItem
     */
    public ISelection getCurrentSelectedItem() {
        return currentSelectedItem;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.IMasterLabelProvider#getMasterContentProvider()
     */
    public IStructuredContentProvider getMasterContentProvider() {
        return masterContentProvider;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.IMasterLabelProvider#getMasterLabelProvider()
     */
    public MasterLabelProvider getMasterLabelProvider() {
        return masterLabelProvider;
    }

    /**
     * 
     */
    public void refresh() {
        viewer.refresh();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.IMasterLabelProvider#setMasterContentProvider(org.eclipse.jface.viewers.IStructuredContentProvider)
     */
    public void setMasterContentProvider(
            IStructuredContentProvider masterContentProvider) {
        this.masterContentProvider = masterContentProvider;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.IMasterLabelProvider#setMasterLabelProvider(org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.ScrolledPropertiesBlock.MasterLabelProvider)
     */
    public void setMasterLabelProvider(MasterLabelProvider masterLabelProvider) {
        this.masterLabelProvider = masterLabelProvider;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.MasterDetailsBlock#createMasterPart(org.eclipse.ui.forms.IManagedForm,
     *      org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createMasterPart(final IManagedForm managedForm,
            Composite parent) {

        // final ScrolledForm form = managedForm.getForm();
        FormToolkit toolkit = managedForm.getToolkit();
        Section section = toolkit.createSection(parent, Section.DESCRIPTION
                | ExpandableComposite.TITLE_BAR);
        section.setText("MIDlet List");
        section.setDescription("Define the MIDlets that make up "
                + "the MIDlet Suite.");

        section.marginWidth = 10;
        section.marginHeight = 5;

        // toolkit.createCompositeSeparator(section);
        Composite client = toolkit.createComposite(section, SWT.WRAP);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.marginWidth = 2;
        layout.marginHeight = 2;
        client.setLayout(layout);

        new Label(client, SWT.NONE);
        new Label(client, SWT.NONE);

        Table t = toolkit.createTable(client, SWT.NULL);
        GridData gd = new GridData(GridData.FILL_BOTH);
        gd.heightHint = 20;
        gd.widthHint = 100;
        t.setLayoutData(gd);
        toolkit.paintBordersFor(client);

        Composite composite = toolkit.createComposite(client);
        FillLayout ButtonBarBlockLayout = new FillLayout(SWT.VERTICAL);
        ButtonBarBlockLayout.spacing = 3;
        composite.setLayout(ButtonBarBlockLayout);

        buttonBarBlock = new ButtonBarBlock(composite, toolkit,
                ButtonBarBlock.BUTTON_NONE);

        section.setClient(client);

        final SectionPart spart = new SectionPart(section);
        managedForm.addPart(spart);
        viewer = new TableViewer(t);
        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                currentSelectedItem = event.getSelection();
                managedForm.fireSelectionChanged(spart, event.getSelection());

            }
        });
        viewer.setContentProvider(masterContentProvider);
        viewer.setLabelProvider(masterLabelProvider);
        viewer.setInput(page.getEditor().getEditorInput());

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.MasterDetailsBlock#createToolBarActions(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createToolBarActions(IManagedForm managedForm) {

        final ScrolledForm form = managedForm.getForm();

        Action haction = new Action("hor", IAction.AS_RADIO_BUTTON) {
            @Override
            public void run() {
                sashForm.setOrientation(SWT.HORIZONTAL);
                form.reflow(true);
            }
        };
        haction.setChecked(true);
        haction.setToolTipText("Horizontal orientation");
        haction.setImageDescriptor(MTJUIPlugin
                .getIconImageDescriptor(IMTJUIConstants.IMG_HORIZONTAL));

        Action vaction = new Action("ver", IAction.AS_RADIO_BUTTON) {
            @Override
            public void run() {
                sashForm.setOrientation(SWT.VERTICAL);
                form.reflow(true);
            }
        };
        vaction.setChecked(false);
        vaction.setToolTipText("Vertical orientation");
        vaction.setImageDescriptor(MTJUIPlugin
                .getIconImageDescriptor(IMTJUIConstants.IMG_VERTICAL));

        form.getToolBarManager().add(haction);
        form.getToolBarManager().add(vaction);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.MasterDetailsBlock#registerPages(org.eclipse.ui.forms.DetailsPart)
     */
    @Override
    protected void registerPages(DetailsPart detailsPart) {

        for (DetailPage detailPage : detailPages) {
            DetailPage type = detailPage;

            detailsPart.registerPage(type.getObjectClass(), type
                    .getDetailsPage());
        }
    }
}
