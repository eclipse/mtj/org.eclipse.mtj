/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Change references from "J2ME projects" to 
 *                                "MIDlet projects"
 *     Diego Sandin (Motorola)  - Fixed incorrect behavior using the 
 *                                "Restore Defaults" button
 *                                
 */
package org.eclipse.mtj.ui.internal.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.utils.ValueChangeTrackingBooleanFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

/**
 * Preference page implementation for setting new J2ME project preferences.
 * 
 * @author Craig Setera
 */
public class NewJ2MEProjectPreferencePage extends FieldEditorPreferencePage
        implements IWorkbenchPreferencePage, IMTJCoreConstants {
    
    /**
     * Default constructor.
     */
    public NewJ2MEProjectPreferencePage() {
        super(GRID);
        setPreferenceStore(MTJUIPlugin.getDefault().getCorePreferenceStore());
        setDescription("Specify preferences for new MIDlet projects");
    }

    /**
     * Creates the field editors.
     */
    @Override
    public void createFieldEditors() {
        Composite parent = getFieldEditorParent();

        addField(new BooleanFieldEditor(PREF_FORCE_JAVA11,
                "Force Java 1.1 Compliance", parent));

        ValueChangeTrackingBooleanFieldEditor useResourcesDirEditor = new ValueChangeTrackingBooleanFieldEditor(
                PREF_USE_RESOURCES_DIR,
                "Automatically Use Resources Directory in New Projects", parent);
        StringFieldEditor resDirectoryEditor = new StringFieldEditor(
                PREF_RESOURCES_DIR, "Resources Directory:", parent);

        useResourcesDirEditor.setFieldEditor(resDirectoryEditor);

        addField(useResourcesDirEditor);
        addField(resDirectoryEditor);
    }

    /**
     * Initialize the preference page as necessary
     * 
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }

    /*
     * Overridden so that we get the help context where it belongs so that it
     * works when the focus is in the left pane.
     */
    @Override
    protected Control createContents(Composite parent) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_J2MEPreferencePage");
        return (super.createContents(parent));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performDefaults()
     */
    @Override
    protected void performDefaults() {
        super.performDefaults();
    }

}
