/**
 * Copyright (c) 2003-2005 Craig Setera
 * All Rights Reserved.
 * Licensed under the Eclipse Public License - v 1.0
 * For more information see http://www.eclipse.org/legal/epl-v10.html
 */
package org.eclipse.mtj.ui.internal;

import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * An extended preference page implementation for EclipseME embedding.
 * <p />
 * Copyright (c) 2003-2005 Craig Setera<br>
 * All Rights Reserved.<br>
 * Licensed under the Eclipse Public License - v 1.0<p/> <br>
 * $Revision: 1.2 $ <br>
 * $Date: 2005/08/16 00:50:23 $ <br>
 * 
 * @author Craig Setera
 */
public interface IEmbeddableWorkbenchPreferencePage extends
        IWorkbenchPreferencePage {
    /**
     * Performs special processing when this page's Defaults button has been
     * pressed.
     * <p>
     * This is a framework hook method for subclasses to do special things when
     * the Defaults button has been pressed. Subclasses may override, but should
     * call <code>super.performDefaults</code>.
     * </p>
     */
    public void performDefaults();

    /**
     * Performs special processing when this page's Apply button has been
     * pressed.
     * <p>
     * This is a framework hook method for sublcasses to do special things when
     * the Apply button has been pressed. The default implementation of this
     * framework method simply calls <code>performOk</code> to simulate the
     * pressing of the page's OK button.
     * </p>
     * 
     * @see #performOk
     */
    public void performApply();
}
