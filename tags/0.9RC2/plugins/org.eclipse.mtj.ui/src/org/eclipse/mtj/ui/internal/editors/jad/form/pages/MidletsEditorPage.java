/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Refactoring the page to add expansibilities
 *     Diego Sandin (Motorola)  - Fixed error in  new MIDlet entry name generation
 */
package org.eclipse.mtj.ui.internal.editors.jad.form.pages;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.ui.IMTJUIConstants;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage;
import org.eclipse.mtj.ui.editors.models.IModelListener;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.editors.jad.form.JADFormEditor;
import org.eclipse.mtj.ui.internal.forms.blocks.ButtonBarBlock;
import org.eclipse.mtj.ui.internal.forms.blocks.DetailPage;
import org.eclipse.mtj.ui.internal.forms.blocks.MasterLabelProvider;
import org.eclipse.mtj.ui.internal.forms.blocks.NamedObject;
import org.eclipse.mtj.ui.internal.forms.blocks.ScrolledPropertiesBlock;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;

/**
 * A property page editor for the MIDlets in the MIDlet suite.
 * 
 * @author Craig Setera
 */
public class MidletsEditorPage extends AbstractJADEditorPage implements
        IModelListener {

    /**
     * Content providers for structured viewers.
     */
    class MasterContentProvider implements IStructuredContentProvider {

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            return model.getContents();
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer,
         *      java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    /**
     * A label provider implementation.
     */
    class MidletMasterLabelProvider extends MasterLabelProvider {

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
         *      int)
         */
        @Override
        public Image getColumnImage(Object obj, int index) {

            return MTJUIPlugin
                    .getImageFromCache(IMTJUIConstants.IMG_MIDLET_ICON);

        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
         *      int)
         */
        @Override
        public String getColumnText(Object obj, int index) {
            return ((MidletType) obj).getName();
        }
    }

    /**
     * The page unique identifier
     */
    public static final String MIDLETS_PAGEID = "midlets"; //$NON-NLS-1$

    /**
     * The prefix of all MIDlet definition properties
     */
    public static final String MIDLET_PREFIX = "MIDlet-"; //$NON-NLS-1$

    /**
     * Get a string value from the resource bundle
     * 
     * @param key the key for the desired string
     * @return the string for the given key, or the key itself if no string was
     *         found
     */
    private static String getResourceString(String key) {
        return MTJUIStrings.getString(key);
    }

    /**
     * The amount of MIDlets listed.
     */
    private int midletcount = 0;

    /** The original MIDlet count as stored */
    private int storedMidletCount;

    /**
     * The Add MIDlets button
     */
    private Button add;

    /**
     * The Remove MIDlets button
     */
    private Button remove;

    /**
     * The model based on the MIDlets listed.
     */
    private MidletsModel model;

    private ScrolledPropertiesBlock block;

    List<DetailPage> list = new ArrayList<DetailPage>();

    /**
     * Creates a new page for MIDlets edition, with default description. The
     * parent editor need to be passed in the <code>initialize</code> method
     * if this constructor is used.
     */
    public MidletsEditorPage() {
        super(MIDLETS_PAGEID, getResourceString("editor.jad.tab.midlets")); //$NON-NLS-1$
        model = new MidletsModel(null);
        model.addModelListener(this);
    }

    /**
     * Creates a new page for MIDlets edition.
     * 
     * @param editor The parent editor
     * @param title the page title
     */
    public MidletsEditorPage(JADFormEditor editor, String title) {
        super(editor, MIDLETS_PAGEID, title);
        model = new MidletsModel(null);
        model.addModelListener(this);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor monitor) {

        IPreferenceStore store = getPreferenceStore();

        int i;

        Object midlets[] = model.getContents();

        int currentMidletCount = midlets.length;

        for (i = 0; i < currentMidletCount; i++) {
            store.setValue(MIDLET_PREFIX + (i + 1), ((MidletType) midlets[i])
                    .toString());
        }

        for (; i < storedMidletCount; i++) {
            store.setToDefault(MIDLET_PREFIX + (i + 1));
        }

        midletcount = currentMidletCount;
        storedMidletCount = currentMidletCount;

        setDirty(false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#editorInputChanged()
     */
    @Override
    public void editorInputChanged() {

        // if the file has been updated external to Eclipse, the model must be
        // reseted
        if (model != null) {
            model.clear();
        }

        midletcount = 0;

        updateMidletProperties();

        if (block != null) {
            block.refresh();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.WorkbenchPart#getTitle()
     */
    @Override
    public String getTitle() {

        return getResourceString("editor.jad.tab.midlets"); //$NON-NLS-1$
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#isManagingProperty(java.lang.String)
     */
    @Override
    public boolean isManagingProperty(String property) {
        boolean manages = property.startsWith(MIDLET_PREFIX);
        if (manages) {
            String value = property.substring(MIDLET_PREFIX.length());
            try {
                Integer.parseInt(value);
            } catch (NumberFormatException e) {
                manages = false;
            }
        }

        return manages;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.IModelListener#modelChanged(java.lang.Object[],
     *      java.lang.String, java.lang.String)
     */
    public void modelChanged(Object[] objects, String type, String property) {
        block.refresh();
        setDirty(true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#setFocus()
     */
    @Override
    public void setFocus() {

    }

    /**
     * Adds mouse listeners to the add and remove buttons.
     */
    private void addMouseListenersToButtons() {

        add.addMouseListener(new MouseListener() {

            public void mouseDoubleClick(MouseEvent e) {

            }

            public void mouseDown(MouseEvent e) {
                midletcount = model.getContents().length + 1;
                String midletName = MIDLET_PREFIX + String.valueOf(midletcount);

                model.add(new NamedObject[] { new MidletType(midletName,
                        midletName, "", "") }, true);
                remove.setEnabled(true);
                block.refresh();

            }

            public void mouseUp(MouseEvent e) {

            }
        });

        remove.addMouseListener(new MouseListener() {

            public void mouseDoubleClick(MouseEvent e) {

            }

            public void mouseDown(MouseEvent e) {
                IStructuredSelection ssel = (IStructuredSelection) block
                        .getCurrentSelectedItem();

                if (ssel.size() == 1) {
                    MidletType type = (MidletType) ssel.getFirstElement();
                    model.remove(new MidletType[] { type }, true);
                    midletcount = model.getContents().length;
                    if (midletcount == 0) {
                        remove.setEnabled(false);
                    }
                }
            }

            public void mouseUp(MouseEvent e) {

            }
        });
    }

    /**
     * Update the MIDlet properties from the current preference store.
     */
    private void updateMidletProperties() {
        IPreferenceStore store = getPreferenceStore();

        // This is sort of ugly, but IPreferenceStore does not
        // allow getting the complete list of preference keys
        List<MidletType> midlets = new ArrayList<MidletType>();

        for (int i = 1; i < 1000; i++) {
            String propName = MIDLET_PREFIX + i;

            if (store.contains(propName)) {
                String propValue = store.getString(propName);

                MidletType midletType = new MidletType(propName, propValue);
                midletType.setName(midletType.getMidletName(), false);

                midlets.add(midletType);
                storedMidletCount++;
            } else {
                break;
            }
        }

        midletcount = storedMidletCount;

        if (!midlets.isEmpty()) {
            model.add(midlets.toArray(new NamedObject[0]), false);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createFormContent(IManagedForm managedForm) {

        list.add(new DetailPage(MidletType.class, new MidletTypeDetailsPage(
                getJavaProject())));
        block = new ScrolledPropertiesBlock(this, new MasterContentProvider(),
                new MidletMasterLabelProvider(), list);

        final ScrolledForm form = managedForm.getForm();
        FormToolkit toolkit = managedForm.getToolkit();

        form.setText(getTitle());
        toolkit.decorateFormHeading(form.getForm());

        createErrorMessageHandler(managedForm);

        /*
         * launch the help system UI, displaying the documentation identified by
         * the href parameter.
         */
        final String href = getHelpResource();
        if (href != null) {
            IToolBarManager manager = form.getToolBarManager();
            Action helpAction = new Action("help") { //$NON-NLS-1$
                @Override
                public void run() {
                    PlatformUI.getWorkbench().getHelpSystem()
                            .displayHelpResource(href);
                }
            };

            helpAction.setImageDescriptor(MTJUIPlugin
                    .getIconImageDescriptor(IMTJUIConstants.IMG_LINKTOHELP));
            manager.add(helpAction);
        }
        form.updateToolBar();

        block.createContent(managedForm);

        add = block.getButtonBarBlock().getButton(
                ButtonBarBlock.BUTTON_ADD_INDEX);
        add.setEnabled(true);

        remove = block.getButtonBarBlock().getButton(
                ButtonBarBlock.BUTTON_REMOVE_INDEX);

        addMouseListenersToButtons();
        if (midletcount > 0) {
            remove.setEnabled(true);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
     */
    @Override
    protected String getHelpResource() {
        return "/org.eclipse.mtj.doc.user/html/reference/editors/jad_editor/midlets.html"; //$NON-NLS-1$
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
     */
    @Override
    protected void setInput(IEditorInput input) {
        super.setInput(input);
        setDirty(false);
        updateMidletProperties();
    }
}
