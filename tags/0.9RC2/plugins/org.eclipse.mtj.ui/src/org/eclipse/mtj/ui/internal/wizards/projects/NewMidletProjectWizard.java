/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.ui.internal.wizards.projects;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.utils.ExceptionHandler;
import org.eclipse.mtj.ui.internal.wizards.NewElementWizard;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;
import org.eclipse.ui.wizards.newresource.BasicNewResourceWizard;

/**
 * Wizard for creation of a new Java ME MIDlet Project.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9
 */
public class NewMidletProjectWizard extends NewElementWizard implements
        IExecutableExtension {

    private IConfigurationElement configElement;

    private NewMidletProjectWizardPageOne firstPage;
    private NewMidletProjectWizardPageTwo secondPage;

    private IWorkbench workbench;

    /**
     * Construct a new MIDLet project wizard.
     */
    public NewMidletProjectWizard() {
        this(null, null);
    }

    /**
     * Construct a new MIDLet project wizard.
     * 
     * @param pageOne the first wizard page
     * @param pageTwo the second wizard page
     */
    public NewMidletProjectWizard(NewMidletProjectWizardPageOne pageOne,
            NewMidletProjectWizardPageTwo pageTwo) {

        ImageDescriptor descriptor = MTJUIPlugin
                .getIconImageDescriptor(Messages.NewMidletProjectWizard_image_descriptor);
        setDefaultPageImageDescriptor(descriptor);
        setWindowTitle(Messages.NewMidletProjectWizard_wizard_title);

        firstPage = pageOne;
        secondPage = pageTwo;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#addPages()
     */
    @Override
    public void addPages() {
        if (firstPage == null) {
            firstPage = new NewMidletProjectWizardPageOne();
        }
        addPage(firstPage);

        if (secondPage == null) {
            secondPage = new NewMidletProjectWizardPageTwo(firstPage);
        }
        addPage(secondPage);

        firstPage.init(getSelection(), getActivePart());
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets
     * .Composite)
     */
    public void createControl(Composite parent) {
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jdt.internal.ui.wizards.NewElementWizard#getCreatedElement()
     */
    @Override
    public IJavaElement getCreatedElement() {
        return secondPage.getJavaProject();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.wizards.testing.NewElementWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
     */
    @Override
    public void init(IWorkbench workbench, IStructuredSelection currentSelection) {
        super.init(workbench, currentSelection);
        this.workbench = workbench;
    }

    /*
     * (non-Javadoc)
     * @see IWizard#performCancel()
     */
    @Override
    public boolean performCancel() {
        secondPage.performCancel();
        return super.performCancel();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.wizard.IWizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        boolean res = super.performFinish();
        if (res) {

            BasicNewProjectResourceWizard.updatePerspective(configElement);

            /* Open the Application Descriptor file */
            try {
                IMidletSuiteProject midletSuiteProject = MidletSuiteFactory
                        .getMidletSuiteProject(secondPage.getJavaProject());

                IFile jadfile = midletSuiteProject
                        .getApplicationDescriptorFile();

                if (jadfile != null) {
                    BasicNewResourceWizard.selectAndReveal(jadfile, workbench
                            .getActiveWorkbenchWindow());
                    openResource(jadfile);
                }

            } catch (Throwable e) {
                MTJCorePlugin.log(IStatus.ERROR,
                        Messages.NewMidletProjectWizard_error_open_jad_file, e
                                .getCause());
            }
        }
        return res;
    }

    /*
     * Stores the configuration element for the wizard. The config element will
     * be used in <code>performFinish</code> to set the result perspective.
     */
    public void setInitializationData(IConfigurationElement cfig,
            String propertyName, Object data) {
        configElement = cfig;
    }

    private IWorkbenchPart getActivePart() {
        IWorkbenchWindow activeWindow = getWorkbench()
                .getActiveWorkbenchWindow();
        if (activeWindow != null) {
            IWorkbenchPage activePage = activeWindow.getActivePage();
            if (activePage != null) {
                return activePage.getActivePart();
            }
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jdt.internal.ui.wizards.NewElementWizard#finishPage(org.eclipse
     * .core.runtime.IProgressMonitor)
     */
    @Override
    protected void finishPage(IProgressMonitor monitor)
            throws InterruptedException, CoreException {
        secondPage.performFinish(monitor); // use the full progress monitor
    }

    @Override
    protected void handleFinishException(Shell shell,
            InvocationTargetException e) {
        String title = Messages.NewMidletProjectWizard_error_create_project_window_title;
        String message = Messages.NewMidletProjectWizard_error_create_project_window_message;
        ExceptionHandler.handle(e, getShell(), title, message);
    }

}
