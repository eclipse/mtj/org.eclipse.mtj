/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Refactoring the page to add expansibilities
 */
package org.eclipse.mtj.ui.internal.editors.jad.form.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage;
import org.eclipse.mtj.ui.internal.editors.jad.form.JADFormEditor;
import org.eclipse.mtj.ui.internal.utils.ManifestPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * A property page editor for user-defined properties.
 * 
 * @author Craig Setera
 */
public class JADUserDefinedPropertiesEditorPage extends AbstractJADEditorPage {

	/**
	 * Implementation of the ICellModifier interface.
	 */
	private class CellModifier implements ICellModifier {

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object,
		 *      java.lang.String)
		 */
		public boolean canModify(Object element, String property) {
			// All columns are modifiable
			return true;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object,
		 *      java.lang.String)
		 */
		public Object getValue(Object element, String property) {
			Object value = null;

			if (element instanceof KeyValuePair) {
				KeyValuePair pair = (KeyValuePair) element;

				int fieldIndex = getFieldIndex(property);
				if (fieldIndex != -1) {
					value = pair.fields[fieldIndex];
				}
			}

			return value;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object,
		 *      java.lang.String, java.lang.Object)
		 */
		public void modify(Object element, String property, Object value) {
			if (element instanceof TableItem) {
				Object data = ((TableItem) element).getData();
				String newValue = (String) value;

				if (data instanceof KeyValuePair) {
					int fieldIndex = getFieldIndex(property);
					KeyValuePair pair = (KeyValuePair) data;

					if (fieldIndex != -1) {
						updateField(pair, property, fieldIndex, newValue);
					}
				}
			}
		}

		/**
		 * Return the field index to match the specified property name. Returns
		 * <code>-1</code> if the property is not recognized.
		 * 
		 * @param property
		 * @return
		 */
		private int getFieldIndex(String property) {
			return PROPERTY_LIST.indexOf(property);
		}

		/**
		 * Update the specified field as necessary.
		 * 
		 * @param pair
		 * @param property
		 * @param fieldIndex
		 * @param newValue
		 */
		private void updateField(KeyValuePair pair, String property,
				int fieldIndex, String newValue) {
			if (!pair.fields[fieldIndex].equals(newValue)) {
				pair.fields[fieldIndex] = newValue;
				setDirty(true);
				tableViewer.update(pair, new String[] { property });
			}
		}
	}

	/**
	 * A holder class for a key/value pair.
	 */
	private static class KeyValuePair {
		String[] fields;

		KeyValuePair(String key, String value) {
			fields = new String[] { key, value };
		}
	}

	/**
	 * Implementation of the table's content provider.
	 */
	private class TableContentProvider implements IStructuredContentProvider {

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		public void dispose() {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		public Object[] getElements(Object inputElement) {
			return userDefinedProperties
					.toArray(new Object[userDefinedProperties.size()]);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer,
		 *      java.lang.Object, java.lang.Object)
		 */
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}

	/**
	 * Implementation of the table's label provider.
	 */
	private static class TableLabelProvider extends LabelProvider implements
			ITableLabelProvider {

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
		 *      int)
		 */
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
		 *      int)
		 */
		public String getColumnText(Object element, int columnIndex) {
			KeyValuePair pair = (KeyValuePair) element;
			return pair.fields[columnIndex];
		}
	}

	/**
	 * The page unique identifier
	 */
	private static final String USER_DEFINED_PAGEID = "user_defined";

	/**
	 * Default new property value
	 */
	private static final String NEW_VALUE = "New Value";

	/**
	 * Default new property key value
	 */
	private static final String NEW_KEY = "New_Key";

	// Column property names
	private static final String PROP_KEY = "key";

	private static final String PROP_VALUE = "value";

	// All of the properties in order
	private static final String[] PROPERTIES = new String[] { PROP_KEY,
			PROP_VALUE };

	private static final List<String> PROPERTY_LIST = Arrays.asList(PROPERTIES);

	/**
	 * Get a string value from the resource bundle
	 * 
	 * @param key
	 * @return
	 */
	private static String getResourceString(String key) {
		return MTJUIStrings.getString(key);
	}

	private Button addButton;
	private Button removeButton;

	// Widgets
	private TableViewer tableViewer;

	// The list of user-defined properties
	private List<KeyValuePair> userDefinedProperties;

	/**
	 * A constructor that creates the JAD EditorPage for <b>User Defined
	 * Properties</b>.
	 */
	public JADUserDefinedPropertiesEditorPage() {
		super(USER_DEFINED_PAGEID,
				getResourceString("editor.jad.tab.user_defined_properties"));
		userDefinedProperties = new ArrayList<KeyValuePair>();
	}

	/**
	 * A constructor that creates the JAD EditorPage for <b>User Defined
	 * Properties</b> and initializes it with the editor.
	 * 
	 * @param editor the parent editor
	 * @param title the page title
	 */
	public JADUserDefinedPropertiesEditorPage(JADFormEditor editor, String title) {
		super(editor, USER_DEFINED_PAGEID, title);
		userDefinedProperties = new ArrayList<KeyValuePair>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		IPreferenceStore store = getPreferenceStore();
		Iterator<KeyValuePair> entries = userDefinedProperties.iterator();
		while (entries.hasNext()) {
			KeyValuePair entry = entries.next();
			store.setValue(entry.fields[0], entry.fields[1]);
		}

		setDirty(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#editorInputChanged()
	 */
	@Override
	public void editorInputChanged() {
		updateMidletProperties();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#getTitle()
	 */
	@Override
	public String getTitle() {
		return getResourceString("editor.jad.tab.user_defined_properties");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#isManagingProperty(java.lang.String)
	 */
	@Override
	public boolean isManagingProperty(String property) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormPage#setFocus()
	 */
	@Override
	public void setFocus() {
		tableViewer.getTable().setFocus();
	}

	/**
	 * Add a new key item to the table and change the dirty flag to true,
	 * letting the editor know the state has changed. .
	 */
	private void addItem() {
		KeyValuePair keyValuePair = new KeyValuePair(NEW_KEY, NEW_VALUE);
		userDefinedProperties.add(keyValuePair);
		tableViewer.refresh();
		setDirty(true);

	}

	/**
	 * Create the add and remove buttons to the composite.
	 * 
	 * @param toolkit the Eclipse Form's toolkit
	 * @param parent
	 */
	private void createButtons(FormToolkit toolkit, Composite parent) {
		Composite composite = toolkit.createComposite(parent);
		FillLayout layout = new FillLayout();
		layout.type = SWT.VERTICAL;
		composite.setLayout(layout);

		addButton = toolkit.createButton(composite,
				getResourceString("editor.button.add"), SWT.PUSH);
		addButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent se) {
				addItem();
			}
		});

		toolkit.createLabel(composite, "");

		removeButton = toolkit.createButton(composite,
				getResourceString("editor.button.remove"), SWT.PUSH);
		removeButton.setEnabled(false);
		removeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent se) {
				removeSelectedItems();
			}
		});
	}

	/**
	 * Create the table viewer for this editor.
	 * 
	 * @param toolkit The Eclipse form's toolkit
	 * @param parent
	 */
	private void createTableViewer(FormToolkit toolkit, Composite parent) {
		String[] columns = new String[] {
				getResourceString("property.jad.userdef.key"),
				getResourceString("property.jad.userdef.value"), };

		// Setup the table
		int styles = SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER
				| SWT.FULL_SELECTION;

		Table table = toolkit.createTable(parent, styles);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setLayoutData(new GridData(GridData.FILL_BOTH));
		table.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TableItem selected = (TableItem) e.item;
				removeButton.setEnabled(selected.getParent()
						.getSelectionCount() > 0);
			}
		});
		tableViewer = new TableViewer(table);

		// Set the table layout on the table
		TableLayout layout = new TableLayout();

		int width = 100 / columns.length;
		for (String element : columns) {
			TableColumn column = new TableColumn(table, SWT.NONE);
			column.setText(element);
			layout.addColumnData(new ColumnWeightData(width));
		}
		table.setLayout(layout);

		// Set the content providers
		tableViewer.setContentProvider(new TableContentProvider());
		tableViewer.setLabelProvider(new TableLabelProvider());

		// Wire up the cell modification handling
		tableViewer.setCellModifier(new CellModifier());
		tableViewer.setColumnProperties(PROPERTIES);
		tableViewer.setCellEditors(new CellEditor[] {
				new TextCellEditor(table), new TextCellEditor(table), });

		// Get some data into the viewer
		tableViewer.setInput(getEditorInput());
		tableViewer.refresh();
	}

	/**
	 * Return a boolean indicating whether the specified key is a user-defined
	 * property.
	 * 
	 * @param key
	 * @return
	 */
	private boolean isUserDefinedPropertyKey(String key) {
		JADFormEditor jadEditor = (JADFormEditor) getEditor();
		return jadEditor.isUserDefinedPropertyKey(key);
	}

	/**
	 * Remove the items currently selected within the table.
	 */
	private void removeSelectedItems() {
		int[] indices = tableViewer.getTable().getSelectionIndices();

		for (int i = indices.length; i > 0; i--) {
			int index = indices[i - 1];
			KeyValuePair keyValuePair = userDefinedProperties.remove(index);
			getPreferenceStore().setToDefault(keyValuePair.fields[0]);
		}

		setDirty(true);
		tableViewer.refresh();
	}

	/**
	 * Update the user properties from the current preference store.
	 */
	private void updateMidletProperties() {
		userDefinedProperties.clear();
		ManifestPreferenceStore store = getPreferenceStore();

		String[] names = store.preferenceNames();
		for (String propName : names) {
			if (isUserDefinedPropertyKey(propName)) {
				userDefinedProperties.add(new KeyValuePair(propName, store
						.getString(propName)));
			}
		}

		if (tableViewer != null) {
			tableViewer.refresh();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
	 */
	@Override
	protected void createFormContent(IManagedForm managedForm) {
		super.createFormContent(managedForm);

		FormToolkit toolkit = managedForm.getToolkit();

		// Set the top-level layout
		Composite parent = managedForm.getForm().getBody();
		parent.setLayoutData(new GridData(GridData.FILL_BOTH));
		parent.setLayout(new GridLayout(2, false));
		new Label(parent, SWT.NONE);
		new Label(parent, SWT.NONE);

		createTableViewer(toolkit, parent);
		createButtons(toolkit, parent);

		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
				"org.eclipse.mtj.ui.help_JADUserDefinedPropertiesEditorPage");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
	 */
	@Override
	protected String getHelpResource() {
		return "/org.eclipse.mtj.doc.user/html/reference/editors/jad_editor/user_defined.html";
	}

	/**
	 * @return
	 */
	protected String getSectionDescription() {
		return "User Defined properties may be specified on this page";
	}

	/**
	 * @return
	 */
	protected String getSectionTitle() {
		return "User Defined Properties";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
	 */
	@Override
	protected void setInput(IEditorInput input) {
		super.setInput(input);
		if (tableViewer != null) {
			tableViewer.setInput(input);
		}

		updateMidletProperties();
	}
}
