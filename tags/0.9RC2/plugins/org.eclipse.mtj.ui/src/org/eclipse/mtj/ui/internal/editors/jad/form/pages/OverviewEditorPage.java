/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version 
 *     Diego Sandin (Motorola) - Added runtime section                    
 */
package org.eclipse.mtj.ui.internal.editors.jad.form.pages;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.mtj.core.model.jad.IJADConstants;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.IMidletSuiteProjectListener;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.ui.IMTJUIConstants;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.devices.DeviceSelector;
import org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.actions.exporting.AntennaBuildExportAction;
import org.eclipse.mtj.ui.internal.actions.packaging.CreateObfuscatedPackageAction;
import org.eclipse.mtj.ui.internal.actions.packaging.CreatePackageAction;
import org.eclipse.mtj.ui.internal.editors.EditorsUIContent;
import org.eclipse.mtj.ui.internal.editors.FormLayoutFactory;
import org.eclipse.mtj.ui.internal.editors.jad.form.JADFormEditor;
import org.eclipse.mtj.ui.internal.preferences.ExtendedStringFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;

/**
 * @author Diego Madruga Sandin
 */
public class OverviewEditorPage extends JADPropertiesEditorPage implements
        IPropertyChangeListener {

    /**
     * The page unique identifier
     */
    private static final String OVERVIEW_PAGEID = "overview";

    // Widget variables
    private DeviceSelector deviceSelector;

    private Display display = null;

    private IJavaProject project;

    /**
     * The current setting of the jar URL after a setInput call. If changed on
     * save, we will trigger a clean build to cause the jar file to be
     * regenerated.
     */
    private String loadedJarUrl;

    private IMidletSuiteProject midletProject;

    /**
     * A constructor that creates the Overview EditorPage and initializes it
     * with the editor.
     * 
     * @param editor the parent editor
     */
    public OverviewEditorPage(JADFormEditor editor) {
        super(editor, OVERVIEW_PAGEID, MTJUIStrings
                .getString("editor.jad.tab.overview"));

        this.project = JavaCore.create(((JADFormEditor) getEditor())
                .getJadFile().getProject());

        this.midletProject = MidletSuiteFactory
                .getMidletSuiteProject(this.project);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor monitor) {
        super.doSave(monitor);

        String currentJarUrl = getPreferenceStore().getString(
                IJADConstants.JAD_MIDLET_JAR_URL);
        if (!currentJarUrl.equals(loadedJarUrl)) {
            ((JADFormEditor) getEditor()).setCleanRequired(true);
        }

        if ((midletProject != null)
                && (deviceSelector.getSelectedDevice() != null)) {
            try {
                midletProject.setDevice(deviceSelector.getSelectedDevice(),
                        monitor);
            } catch (CoreException e) {
                e.printStackTrace();
            }
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getTitle()
     */
    @Override
    public String getTitle() {
        return MTJUIStrings.getString("editor.jad.tab.overview");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.events.IHyperlinkListener#linkActivated(org.eclipse.ui.forms.events.HyperlinkEvent)
     */
    @Override
    public void linkActivated(HyperlinkEvent e) {
        String href = (String) e.getHref();

        IWorkbenchPart activePart = PlatformUI.getWorkbench()
                .getActiveWorkbenchWindow().getActivePage().getActivePart();

        if (href.equals("package")) { //$NON-NLS-1$
            new CreatePackageAction().run(project, activePart);
        } else if (href.equals("obfuscate")) { //$NON-NLS-1$
            new CreateObfuscatedPackageAction().run(project, activePart);
        } else if (href.equals("antenna")) {
            new AntennaBuildExportAction().run(project, activePart);
        } else if (href.startsWith("launchShortcut.")) {
            handleLaunchShortcut(href);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent event) {

        if (event.getProperty().equals(FieldEditor.VALUE)) {

            if (event.getSource() instanceof ExtendedStringFieldEditor) {
                String fieldEditorLabel = ((FieldEditor) event.getSource())
                        .getLabelText();

                Control c = ((ExtendedStringFieldEditor) event.getSource())
                        .getFieldEditorTextControl();

                if (event.getNewValue().equals("")) {

                    getErrorMessageManager().addMessage(
                            "textLength_" + fieldEditorLabel,
                            " field must not be empty.", null,
                            IMessageProvider.ERROR, c);

                } else {
                    getErrorMessageManager().removeMessage(
                            "textLength_" + fieldEditorLabel, c);
                }
            }
            setDirty(true);
        }
    }

    /**
     * 
     */
    private void addMidletProjectChangeListener() {

        midletProject
                .addMidletSuiteProjectListener(new IMidletSuiteProjectListener() {

                    public void deployedJarFileUpToDateFlagChanged() {
                    }

                    public void deviceChanded() {
                        if (display != null) {
                            display.asyncExec(new Runnable() {
                                public void run() {
                                    if (deviceSelector != null) {
                                        deviceSelector
                                                .enableFireSelectionChanged(false);
                                        deviceSelector
                                                .setSelectedDevice(midletProject
                                                        .getDevice());
                                        deviceSelector
                                                .enableFireSelectionChanged(true);
                                    }
                                }
                            });
                        }
                    }

                    public void enabledSymbolDefinitionSetChanged() {
                        // TODO Auto-generated method stub

                    }

                    public void jadFileNameChanged() {
                    }

                    public void signaturePropertiesChanged() {
                    }

                    public void tempKeyPasswordChanged() {
                    }

                    public void tempKeystorePasswordChanged() {
                    }
                });
    }

    /**
     * Create section for debugging options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *                to work in Eclipse forms.
     */
    private void createDebuginSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticSection(toolkit, parent, "Debugging");
        Composite container = createStaticSectionClient(toolkit, section);

        FormText text = createClient(container,
                EditorsUIContent.overviewPage_launchsection_debuglinks,
                toolkit, this);

        text.setImage("debugMidlet", MTJUIPlugin
                .getImageFromCache(IMTJUIConstants.IMG_DEBUG_MIDLET));
        text.setImage("debugjad", MTJUIPlugin
                .getImageFromCache(IMTJUIConstants.IMG_DEBUG_JAD));
        text.setImage("debugOta", MTJUIPlugin
                .getImageFromCache(IMTJUIConstants.IMG_DEBUG_OTA));

        section.setClient(container);
    }

    /**
     * Create section for exporting options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *                to work in Eclipse forms.
     */
    private void createExportingSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticSection(toolkit, parent, "Exporting");
        Composite container = createStaticSectionClient(toolkit, section);

        FormText text = createClient(container,
                EditorsUIContent.overviewPage_exporting, toolkit, this);

        text.setImage("antenna", MTJUIPlugin
                .getImageFromCache(IMTJUIConstants.IMG_ANT));

        section.setClient(container);
    }

    /**
     * Create section for required information about the application.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *                to work in Eclipse forms.
     */
    private void createOverviewSection(final IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticBasicSection(toolkit, parent,
                EditorsUIContent.overviewPage_requiredsection_title,
                EditorsUIContent.overviewPage_requiredsection_description);

        Composite sectionClient = createStaticSectionClient(toolkit, section);
        sectionClient.setLayout(FormLayoutFactory
                .createSectionClientTableWrapLayout(false, 1));

        createSectionContent(managedForm, sectionClient, this);
        section.setClient(sectionClient);

    }

    /**
     * Create section for packaging options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *                to work in Eclipse forms.
     */
    private void createPackagingSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticSection(toolkit, parent, "Packaging");
        Composite container = createStaticSectionClient(toolkit, section);

        FormText text = createClient(container,
                EditorsUIContent.overviewPage_deploying, toolkit, this);

        text.setImage("package", MTJUIPlugin
                .getImageFromCache(IMTJUIConstants.IMG_PACKAGE));
        text.setImage("obfuscate", MTJUIPlugin
                .getImageFromCache(IMTJUIConstants.IMG_PACKAGE_OBFUSCATED));

        section.setClient(container);
    }

    /**
     * Create section for running options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *                to work in Eclipse forms.
     */
    private void createRunningSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        // Running links
        Section section = createStaticSection(toolkit, parent, "Running");
        Composite container = createStaticSectionClient(toolkit, section);

        FormText text = createClient(container,
                EditorsUIContent.overviewPage_launchsection_runlinks, toolkit,
                this);

        text.setImage("runMidlet", MTJUIPlugin
                .getImageFromCache(IMTJUIConstants.IMG_RUN_MIDLET));
        text.setImage("runjad", MTJUIPlugin
                .getImageFromCache(IMTJUIConstants.IMG_RUN_JAD));
        text.setImage("runOta", MTJUIPlugin
                .getImageFromCache(IMTJUIConstants.IMG_RUN_OTA));

        section.setClient(container);
    }

    /**
     * Create section for debugging options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *                to work in Eclipse forms.
     */
    private void createRuntimeSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticBasicSection(toolkit, parent,
                EditorsUIContent.overviewPage_runtimesection_title,
                EditorsUIContent.overviewPage_runtimesection_description);

        Composite sectionClient = createStaticSectionClient(toolkit, section);
        sectionClient.setLayout(FormLayoutFactory
                .createSectionClientGridLayout(true, 1));

        createRuntimeSectionContent(managedForm, sectionClient, this);
        section.setClient(sectionClient);
    }

    /**
     * @param managedForm
     * @param sectionClient
     * @param overviewEditorPage
     */
    private void createRuntimeSectionContent(IManagedForm managedForm,
            Composite sectionClient, OverviewEditorPage overviewEditorPage) {

        // Create the device selection controls
        deviceSelector = new DeviceSelector();
        deviceSelector.createContents(sectionClient, false, false);
        deviceSelector.enableFireSelectionChanged(false);
        if ((midletProject != null) && (midletProject.getDevice() != null)) {
            deviceSelector.setSelectedDevice(midletProject.getDevice());
        }

        deviceSelector
                .setSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        setDirty(true);
                    }
                });
        deviceSelector.enableFireSelectionChanged(true);

        addMidletProjectChangeListener();
    }

    /**
     * Fill the page body with all available sections.
     * 
     * @param managedForm the managed form that wraps the form widget
     */
    private void fillEditorPageBody(IManagedForm managedForm) {

        display = Display.getCurrent();
        FormToolkit toolkit = managedForm.getToolkit();

        Composite body = managedForm.getForm().getBody();
        body.setLayout(FormLayoutFactory.createFormTableWrapLayout(true, 2));

        Composite left = toolkit.createComposite(body);
        left.setLayout(FormLayoutFactory
                .createFormPaneTableWrapLayout(false, 1));
        left.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB));

        createOverviewSection(managedForm, left, toolkit);
        createPackagingSection(managedForm, left, toolkit);
        createExportingSection(managedForm, left, toolkit);

        Composite right = toolkit.createComposite(body);
        right.setLayout(FormLayoutFactory.createFormPaneTableWrapLayout(false,
                1));
        right.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB));

        createRunningSection(managedForm, right, toolkit);
        createDebuginSection(managedForm, right, toolkit);
        createRuntimeSection(managedForm, right, toolkit);

    }

    /**
     * Handles a launch shortcut activation.
     * 
     * @param href the launch shortcut. The format of href should be
     *                <code>launchShortcut.&lt;mode&gt;.&lt;launchShortcutId&gt;</code>
     */
    private void handleLaunchShortcut(String href) {

        href = href.substring(15);
        int index = href.indexOf('.');
        if (index < 0) {
            return; // error. Format of href should be
        }
        // launchShortcut.<mode>.<launchShortcutId>
        String mode = href.substring(0, index);
        String id = href.substring(index + 1);

        IExtensionRegistry registry = Platform.getExtensionRegistry();
        IConfigurationElement[] elements = registry
                .getConfigurationElementsFor("org.eclipse.debug.ui.launchShortcuts"); //$NON-NLS-1$
        for (IConfigurationElement element : elements) {
            if (id.equals(element.getAttribute("id"))) {
                try {
                    ILaunchShortcut shortcut = (ILaunchShortcut) element
                            .createExecutableExtension("class"); //$NON-NLS-1$
                    shortcut.launch(new StructuredSelection(project), mode);
                } catch (CoreException e1) {
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#addContextHelp(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void addContextHelp(Composite c) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(c,
                "org.eclipse.mtj.ui.help_JADRequiredPropertiesEditorPage");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createFormContent(IManagedForm managedForm) {

        final ScrolledForm form = managedForm.getForm();
        FormToolkit toolkit = managedForm.getToolkit();

        form.setText(getTitle());
        toolkit.decorateFormHeading(form.getForm());

        createErrorMessageHandler(managedForm);

        /*
         * launch the help system UI, displaying the documentation identified by
         * the href parameter.
         */
        final String href = getHelpResource();
        if (href != null) {
            IToolBarManager manager = form.getToolBarManager();
            Action helpAction = new Action("help") { //$NON-NLS-1$
                @Override
                public void run() {
                    PlatformUI.getWorkbench().getHelpSystem()
                            .displayHelpResource(href);
                }
            };

            helpAction.setImageDescriptor(MTJUIPlugin
                    .getIconImageDescriptor(IMTJUIConstants.IMG_LINKTOHELP));
            manager.add(helpAction);
        }
        form.updateToolBar();

        fillEditorPageBody(managedForm);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
     */
    @Override
    protected String getHelpResource() {
        return "/org.eclipse.mtj.doc.user/html/reference/editors/jad_editor/overview.html";
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionDescription()
     */
    @Override
    protected String getSectionDescription() {
        return "";
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionTitle()
     */
    @Override
    protected String getSectionTitle() {
        return "";
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#setInput(org.eclipse.ui.IEditorInput)
     */
    @Override
    protected void setInput(IEditorInput input) {
        super.setInput(input);

        // Store the current JAR URL setting for later save comparison
        loadedJarUrl = getPreferenceStore().getString(
                IJADConstants.JAD_MIDLET_JAR_URL);
    }
}
