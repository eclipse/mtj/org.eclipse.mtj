<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
	<title>MTJ - Packaging</title>
	<link rel="stylesheet" href="docs.css" type="text/css">
</head>
<body>
<p class="PageTitle">Packaging</p>
<p>
In order to deploy a Java ME MIDlet Suite for use on a Java ME device,
it must first be deployed as a JAD and JAR file.  MTJ provides
built-in support for packaging a MIDlet Suite into appropriate JAD
and JAR files.
</p>
<ol>
	<li class="TOC"><a class="TOC" href="#howto">How to package your MIDlet Suite</a></li>
	<li class="TOC"><a class="TOC" href="#options">Packaging options</a></li>
	<li class="TOC"><a class="TOC" href="#outputs">Packaging outputs</a></li>
	<li class="TOC"><a class="TOC" href="#antenna">Packaging using Antenna</a></li>
	<li class="TOC"><a class="TOC" href="#signing">Signing while packaging</a></li>
</ol>
<hr>
<h1 id="howto">How to package your MIDlet Suite</h1>
<p>
MTJ packaging support is provided via the Java ME MIDlet Suite
project context menu as shown in this snapshot.
</p>
<img src="img/crtpkg.png" alt="screenshot">
<h1 id="options">Packaging Options</h1>
<p>
There are two packaging options - 
<span class="keyword">Create Obfuscated Package</span>
and
<span class="keyword">Create Package</span>.
</p>
<h2>Create Package</h2>
<p>
When the <span class="keyword">Create Package</span> action is run, it will
deploy the JAD file and JAR file into the deployment directory specified in
the <a href="refPrefJ2ME.html#basic">Preferences</a>.
The deployed JAR file will contain the verified classes and
resources.
</p>
<h2>Create Obfuscated Package</h2>
<p>
When the <span class="keyword">Create Obfuscated Package</span> action is run,
it will deploy the JAD file and JAR file into the deployment directory
specified in
the <a href="refPrefJ2ME.html#basic">Preferences</a>.
The deployed JAR file will be obfuscated by the using the Proguard Java Tool
as specified in
the <a href="refPrefJ2ME.html#basic">Preferences</a>.
</p>
<p>
Obfuscated packages are useful
in providing a small level of security for your deployed MIDlets.  More
importantly, obfuscation generally leads to smaller deployed JAR file sizes.
<p>
In order to produce obfuscated packages, the Proguard Java obfuscation tools
must be installed and the root of the Proguard installation must be configured
in the <a href="refPrefJ2ME.html#obfuscation">Obfuscation Preferences</a>.  
Proguard is a free open-source
library that is available for download from
<a href="http://proguard.sourceforge.net/">http://proguard.sourceforge.net/</a>.
</p>
<h4>Errors and Warnings During Obfuscation</h4>
<p>
It is possible that there will be warnings or errors during the obfuscation
process.  When this occurs, a prompt will be opened to allow packaging to
continue or be cancelled.
</p>
<img src="img/obf_problems.png" alt="screenshot">
<p>
If the warning or error can be safely ignored, you may select
<span class="keyword">Continue</span>
and MTJ will attempt to continue creating the obfuscated package. 
Depending on the type and severity of the problems encountered the package
may not be able to be created.
</p>
<h1 id="outputs">Packaging Outputs</h1>
<h2>JAR File Deployment</h2>
The deployed JAR file will be created in the deployment directory
using the following information:
<ul>
<li>The verified class files found in the verified output directory.</li>
<li>The resources found in the resources directory.</li>
<li>The manifest file will be based on the contents of the project's
source JAD file.</li>
</ul>
<h2>JAD File Deployment</h2>
<p>
The source JAD file found in the root directory of the MIDlet
Suite project will be copied into the deployment directory during the
packaging operation.  All changes should be made in the source JAD
file as opposed to the deployed JAD file, as the deployed JAD file
will be overwritten during the packaging operation.
</p>
<p>
During deployment of the JAD file, the
<span class="keyword">MIDlet-Jar-Size</span>
property of the JAD file will be updated to match the size of the
previously deployed JAR file.
<h2>Obfuscated Package Outputs</h2>
<p>
The JAR and JAD files are necessary for deployment to a MIDP capable
device.  In addition to those files, a number of other files are made
available during the obfuscated packaging process.  These files are placed
in the deployment directory alongside the resulting JAR and JAD files.
<ul>
<li><b>*_base.jar</b>
<br>This JAR file contains the initial packaging before the obfuscation
processing occurs.  This file acts as the source for the obfuscation processing.
<li><b>*_base_obf.jar</b>
<br>This JAR file contains the obfuscated classes.  Preverification will
be run against this jar file again to produce the final obfuscated and
preverified JAR file.
<li><b>pro_map.txt</b>
<br>This file contains information concerning the mapping applied from the
original source names to the obfuscated names.  This file may be used by
the Proguard <span class="keyword">ReTrace</span> command to rebuild the original stack trace
from an obfuscated stack trace.
<li><b>pro_seeds.txt</b>
<br>This file lists the MIDlet classes that were included and acted as
the seeds to the obfuscation process.
<li><b>proguard.cfg</b>
<br>This file holds the configuration that was specified to Proguard in
order to generate the resulting obfuscated package.
</ul>
<h1 id="antenna">Packaging using Antenna</h1>
<p>Prerequisites:
<ul>
<li>Antenna library available from <a href="http://antenna.sourceforge.net/">
http://antenna.sourceforge.net/</a> correctly configured in the
<a href="refPrefJ2ME.html#basic">Basic Java ME Preferences</a>.</li>
<li>A supported Sun Wireless Toolkit configured in the
<a href="refPrefJ2ME.html#platcomp">Platform Components</a>.</li>
</ul>
<p>
If the appropriate prerequisites are not met, an error dialog will be
displayed.
</p>
<p>Export the Antenna build files from the Mobiles Tools for Java project menu.  Choose "Export
Antenna Build Files".  The following files will be generated:
</p>
<ul>
<li>build.xml - The root build file that may be executed using standard Ant
execution.  This file is a skeleton that wraps the mtj-build.xml file.
This file may be modified and will not be overwritten during subsequent
export operations.</li>
<li>mtj-build.xml - This build file handles the calls to the Antenna
library and contains all of the classpath information generated from the
Eclipse projects involved in the export.  This file should not be altered, as
it will be overwritten during each subsequent export operation.  This file
should be regenerated whenever the project classpath is updated.</li>
<li>mtj-build.properties - This file contains a set of properties that
define the locations and preferences concerning the build.  This file should
not be altered, as it will be overwritten during each subsequent export
operation.  The properties in this file may be overridden by creating a
user-build.properties in the same directory as the generated files.  Properties
defined in the user-build.properties will override the values in the
mtj-build.properties.</li>
</ul>

<h1 id="signing">Signing while packaging</h1>
<p>
You can automatically have your
MIDlet suite signed during the packing process.  Signing a MIDlet suite
allows it to operate in the "trusted third party" protection domain instead
of the "untrusted" protection domain.  This can be significant if your
MIDlet intends to access protected features.
</p>
<p>
To have your MIDlet suite signed as part of packaging, check the 
<span class="bold">Sign project</span> box on the
<a href="refPropProject.html">Project Properties</a> page.  You will
need to provide the location of your keystore file, the alias of the
key within that keystore file, and, optionally, the password for the
keystore and key.
</p>
<p>
For more information about MIDlet suite signing,
<a href="refSigning.html">click here</a>.
</p>
</body>
</html>
