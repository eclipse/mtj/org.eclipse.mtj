/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.internal.refactoring;

import org.eclipse.osgi.util.NLS;

/**
 * @author wangf
 * 
 */
public class RefactoringMessages {
	private static final String BUNDLE_NAME = "org.eclipse.mtj.core.internal.refactoring.messages";//$NON-NLS-1$

	public static String JadLaunchConfigParticipant_name;

	public static String JadLaunchConfigCompositeChange_name;

	public static String JadLaunchConfigProjectRenameChange_name;

	public static String JadLaunchConfig_doNotExist;

	static {
		// load message values from bundle file
		NLS.initializeMessages(BUNDLE_NAME, RefactoringMessages.class);
	}
}
