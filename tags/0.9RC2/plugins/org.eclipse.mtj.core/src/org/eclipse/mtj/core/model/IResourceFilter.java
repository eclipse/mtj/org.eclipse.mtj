/**
 * Copyright (c) 2004,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;

/**
 * This interface represents a filter controlling inclusion of resources into
 * the packaged output.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */
public interface IResourceFilter {

    /**
     * Return a boolean indicating whether the specified container should be
     * traversed.
     * 
     * @param container the container to be traversed
     * @return whether the container should be traversed
     */
    public boolean shouldTraverseContainer(IContainer container);

    /**
     * Return a boolean indicating whether the specified file should be
     * included.
     * 
     * @param file the file to be tested
     * @return whether the file should be included
     */
    public boolean shouldBeIncluded(IFile file);
}
