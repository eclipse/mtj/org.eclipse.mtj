/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.library.api;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.mtj.core.model.Version;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * Instances of this class represent the API provided by an ILibrary instance.
 * 
 * @author Craig Setera
 */
public class API implements IPersistable {
    private String identifier;
    private String name;
    private Version version;
    private APIType type;
    private boolean registered;
    private String primaryMatchableClass;
    private URL[] skeletonReplacements;

    /**
     * Construct a new API instance
     */
    public API() {
        super();
        type = APIType.UNKNOWN;
    }

    /**
     * Construct a new API instance for the registry element.
     * 
     * @param configurationElement
     * @throws IOException
     */
    public API(IConfigurationElement configurationElement) throws IOException {
        registered = true;

        identifier = configurationElement.getAttribute("id");
        name = configurationElement.getAttribute("name");
        if (name == null)
            name = identifier;

        primaryMatchableClass = configurationElement
                .getAttribute("primaryClass");

        String versionString = configurationElement.getAttribute("version");
        if (versionString == null) {
            versionString = "1.0";
        }
        version = new Version(versionString);

        String typeString = configurationElement.getAttribute("type");
        if ("profile".equals(typeString)) {
            type = APIType.PROFILE;
        } else if ("configuration".equals(typeString)) {
            type = APIType.CONFIGURATION;
        } else {
            type = APIType.OPTIONAL;
        }

        IConfigurationElement[] files = configurationElement
                .getChildren("skeletonFile");
        skeletonReplacements = new URL[files.length];
        for (int i = 0; i < files.length; i++) {
            IConfigurationElement fileElement = files[i];

            IPath path = new Path(fileElement.getAttribute("path"));
            if (path.isAbsolute()) {
                skeletonReplacements[i] = path.toFile().toURI().toURL();
            } else {
                String bundleIdentifier = fileElement.getNamespaceIdentifier();
                skeletonReplacements[i] = new URL("platform:///plugin/"
                        + bundleIdentifier + "/" + path);
            }
        }
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return (obj instanceof API) && equalsAPI((API) obj);
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return identifier.hashCode() ^ type.hashCode() ^ version.hashCode();
    }

    /**
     * @return Returns the identifier.
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier The identifier to set.
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Return the fully-qualifed class name of the primary class for this API
     * that can be used for matching.
     * 
     * @return
     */
    public String getPrimaryMatchableClass() {
        return primaryMatchableClass;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Set the fully-qualified class name of the primary class for this API that
     * can be used for matching.
     * 
     * @param primaryMatchableClass
     */
    public void setPrimaryMatchableClass(String primaryMatchableClass) {
        this.primaryMatchableClass = primaryMatchableClass;
    }

    /**
     * @return Returns the type.
     */
    public APIType getType() {
        return type;
    }

    /**
     * @return
     */
    public boolean isRegistered() {
        return registered;
    }

    /**
     * @param registered
     */
    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    /**
     * Return the replacement files that are useful for preverification and
     * devices such as MPowerPlayer and microemu. These are likely
     * skeleton/empty API implementation jars that can be safely run through a
     * preverifier. May return <code>null</code> if there are not replacements
     * for this particular API.
     * 
     * @return
     */
    public URL[] getSkeletonReplacements() {
        return skeletonReplacements;
    }

    /**
     * @param type The type to set.
     */
    public void setType(APIType type) {
        this.type = type;
    }

    /**
     * @return Returns the version.
     */
    public Version getVersion() {
        return version;
    }

    /**
     * Set the replacement files that are useful for preverification. These are
     * likely skeleton/empty API implementation jars that can be safely run
     * through a preverifier.
     * 
     * @param verifiableReplacements
     */
    public void setSkeletonReplacements(URL[] verifiableReplacements) {
        this.skeletonReplacements = verifiableReplacements;
    }

    /**
     * @param version The version to set.
     */
    public void setVersion(Version version) {
        this.version = version;
    }

    /**
     * Return a boolean indicating whether this API is a configuration library.
     * 
     * @return
     */
    public boolean isConfiguration() {
        return (type == APIType.CONFIGURATION);
    }

    /**
     * Return a boolean indicating whether this API is a profile library.
     * 
     * @return
     */
    public boolean isProfile() {
        return (type == APIType.PROFILE);
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        identifier = persistenceProvider.loadString("identifier");
        version = new Version(persistenceProvider.loadString("version"));
        registered = persistenceProvider.loadBoolean("registered");

        API registeredAPI = null;
        if (registered) {
            registeredAPI = APIRegistry.getAPI(identifier, version);
        }

        if (registeredAPI != null) {
            name = registeredAPI.name;
            type = registeredAPI.type;
            primaryMatchableClass = registeredAPI.primaryMatchableClass;
            skeletonReplacements = registeredAPI.skeletonReplacements;
        } else {
            name = persistenceProvider.loadString("name");
            int typeCode = persistenceProvider.loadInteger("type");
            type = APIType.typeForCode(typeCode);
        }
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        persistenceProvider.storeString("identifier", identifier);
        persistenceProvider.storeString("name", name);
        persistenceProvider.storeInteger("type", type.getTypeCode());
        persistenceProvider.storeString("version", version.toString());
        persistenceProvider.storeBoolean("registered", registered);
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        StringBuffer sb = new StringBuffer(identifier);
        sb.append("-").append(version);

        return sb.toString();
    }

    /**
     * Return a boolean indicating whether the specified API is the same as this
     * API.
     * 
     * @param api
     * @return
     */
    private boolean equalsAPI(API api) {
        return identifier.equals(api.identifier) && type.equals(api.type)
                && version.equals(api.version);
    }
}
