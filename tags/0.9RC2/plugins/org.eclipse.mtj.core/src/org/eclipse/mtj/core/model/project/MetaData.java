/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)  - Initial implementation
 *     Kevin Hunter (Individual) - Added signing support
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 *     Diego Sandin (Motorola) &
 *     Hugo Raniere (Motorola)   - Remove Bouncy Castle dependencies
 *     Hugo Raniere (Motorola)   - Save user passwords in project's metadata file (w/o Bouncy Castle)
 */
package org.eclipse.mtj.core.model.project;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.mtj.core.MTJCoreErrors;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.signing.Base64EncDec;
import org.eclipse.mtj.core.internal.utils.XMLUtils;
import org.eclipse.mtj.core.model.Version;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.project.impl.MidletSuiteProject;
import org.eclipse.mtj.core.model.sign.ISignatureProperties;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.signing.SignatureProperties;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * This class holds the metadata for the MIDlet suite project. This information
 * is persisted to a file called ".mtj" in the project's root directory.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 */
public class MetaData {

    /**
     * The metadata file name for the MIDlet project
     */
    public static final String METADATA_FILE = ".mtj"; //$NON-NLS-1$

    /**
     * The metadata root element
     */
    private static final String ELEM_ROOT_NAME = "mtjMetadata"; //$NON-NLS-1$

    /**
     * Java Application Descriptor data
     */
    private static final String ATTR_JAD_FILE = "jad"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ELEM_SIGNING = "signing"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ATTR_SIGN_PROJECT = "signProject"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ELEM_KEYSTORE = "keystore"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ELEM_ALIAS = "alias"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ELEM_PROVIDER = "provider"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ELEM_KEYSTORETYPE = "keystoreType"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ELEM_PASSWORDS = "passwords"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ATTR_STOREPASSWORDS = "storePasswords"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ELEM_PWD_KEYSTORE = "keystore"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ELEM_PWD_KEY = "key"; //$NON-NLS-1$

    // Device related constants
    /**
     * 
     */
    private static final String ELEM_DEVICE = "device"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ATTR_DEVICEGROUP = "group"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String ATTR_DEVICENAME = "name"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String CRYPTO_ALGORITHM = "PBEWithMD5AndTripleDES"; //$NON-NLS-1$

    /**
     * PBE password
     */
    private static final String CRYPTO_PASS = "MTJ"; //$NON-NLS-1$
    /**
     * PBE 8-byte salt
     */
    private static final byte[] CRYPTO_SALT = { (byte) 0xc7, (byte) 0x73,
            (byte) 0x21, (byte) 0x8c, (byte) 0x7e, (byte) 0xc8, (byte) 0xee,
            (byte) 0x99 };

    /**
     * PBE iteration count
     */
    private static final int CRYPTO_ITERATION_COUNT = 10;

    private static final String KEYRING_URL_BASE = "http://projects.mtj/"; //$NON-NLS-1$
    private static final String KEYRING_REALM = "projects"; //$NON-NLS-1$
    private static final String KEYRING_SCHEME = "MTJ"; //$NON-NLS-1$
    private static final String KEYRING_KEYSTOREPASS_KEY = "KeystorePass"; //$NON-NLS-1$
    private static final String KEYRING_KEYPASS_KEY = "KeyPass"; //$NON-NLS-1$

    private IProject project;

    private Version version;
    private IDevice device;

    /**
     * The name that will be used for the deployed MIDlet Suite jad file
     */
    private String jadFileName;

    private SignatureProperties signatureProps;

    /**
     * Construct a new metadata object for the MIDlet suite project.
     * 
     * @param suite
     */
    public MetaData(MidletSuiteProject suite) {
        this(suite.getJavaProject().getProject());
    }

    /**
     * Construct a new metadata object for the MIDlet suite project.
     * 
     * @param suite
     */
    public MetaData(IProject project) {
        this.project = project;

        try {
            loadMetaData();
        } catch (CoreException e) {
            // Failure to load the metadata, log and initialize to defaults
            MTJCorePlugin.log(IStatus.ERROR, "loadMetaData() failed", e); //$NON-NLS-1$
            initializeToDefaults();
        }
    }

    /**
     * Return the IFile instance in which the metadata is to be stored or
     * <code>null</code> if the file is not yet available.
     * 
     * @return
     */
    private IFile getStoreFile() {
        IFile storeFile = null;

        if (project != null) {
            storeFile = project.getFile(METADATA_FILE);
        }

        return storeFile;
    }

    /**
     * Initialize the metadata to default values
     */
    private void initializeToDefaults() {
        signatureProps = new SignatureProperties();
    }

    private void loadMetaData() throws CoreException {
        boolean bRewrite = false;

        loadMetaDataFromFile();

        if (signatureProps == null) {
            signatureProps = new SignatureProperties();
            signatureProps.clear();
            bRewrite = true;
        }

        if (bRewrite) {
            // NOTE: Commented out for now. The point at which this is likely to
            // happen (during startup), the workspace is locked against updates
            // and the save will fail. (Actually the workspace refresh will
            // fail)
            // saveMetaData();
        }
    }

    private void loadMetaDataFromFile() throws CoreException {
        IFile storeFile = getStoreFile();
        if ((storeFile != null) && (storeFile.exists())) {
            try {
                File localFile = storeFile.getLocation().toFile();
                Document document = XMLUtils.readDocument(localFile);
                if (document == null) {
                    return;
                }

                Element rootElement = document.getDocumentElement();
                if (!rootElement.getNodeName().equals(ELEM_ROOT_NAME)) {
                    return;
                }

                version = XMLUtils.getVersion(document);

                /*
                 * Get the name that will be used for the deployed MIDlet Suite
                 * jad file
                 */
                jadFileName = rootElement.getAttribute(ATTR_JAD_FILE);

                loadDevice(rootElement);

                loadSignatureProperties(rootElement);
            } catch (ParserConfigurationException pce) {
                MTJCorePlugin.throwCoreException(IStatus.WARNING, 99999, pce);
            } catch (SAXException se) {
                MTJCorePlugin.throwCoreException(IStatus.WARNING, 99999, se);
            } catch (IOException ioe) {
                MTJCorePlugin.throwCoreException(IStatus.WARNING, 99999, ioe);
            } catch (PersistenceException e) {
                MTJCorePlugin.throwCoreException(IStatus.WARNING, 99999, e);
            }
        } else {
            initializeToDefaults();
        }
    }

    private void loadDevice(Element rootElement) throws PersistenceException {
        Element deviceElement = XMLUtils.getFirstElementWithTagName(
                rootElement, ELEM_DEVICE);
        if (deviceElement != null) {
            String deviceGroup = deviceElement.getAttribute(ATTR_DEVICEGROUP);
            String deviceName = deviceElement.getAttribute(ATTR_DEVICENAME);
            device = DeviceRegistry.singleton
                    .getDevice(deviceGroup, deviceName);
        }
    }

    private void loadSignatureProperties(Element rootElement)
            throws CoreException {
        SignatureProperties sp = new SignatureProperties();

        Element signRoot = XMLUtils.getFirstElementWithTagName(rootElement,
                ELEM_SIGNING);
        if (signRoot == null) {
            return;
        }

        String attr = signRoot.getAttribute(ATTR_SIGN_PROJECT);
        if (attr == null) {
            return;
        }

        if (!Boolean.valueOf(attr).booleanValue()) {
            sp.setSignProject(false);
            signatureProps = sp;
            return;
        }

        sp.setSignProject(true);

        Element e;

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_KEYSTORE);
        if (e == null) {
            return;
        }
        sp.setKeyStoreDisplayPath(XMLUtils.getElementText(e));

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_ALIAS);
        if (e == null) {
            return;
        }
        sp.setKeyAlias(XMLUtils.getElementText(e));

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_PROVIDER);
        if (e != null) {
            sp.setKeyStoreProvider(XMLUtils.getElementText(e));
        }

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_KEYSTORETYPE);
        if (e != null) {
            sp.setKeyStoreType(XMLUtils.getElementText(e));
        }

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_PASSWORDS);
        if (e != null) {
            attr = e.getAttribute(ATTR_STOREPASSWORDS);
            int nMethod = ISignatureProperties.PASSMETHOD_PROMPT;
            if (attr != null) {
                try {
                    nMethod = Integer.valueOf(attr).intValue();
                } catch (Exception ex) {
                }
            }

            Map<?, ?> keyMap;

            switch (nMethod) {
            case ISignatureProperties.PASSMETHOD_IN_KEYRING:
                sp.setPasswordStorageMethod(nMethod);
                keyMap = Platform.getAuthorizationInfo(getKeyringURL(),
                        KEYRING_REALM, KEYRING_SCHEME);
                if (keyMap != null) {
                    sp.setKeyStorePassword((String) keyMap
                            .get(KEYRING_KEYSTOREPASS_KEY));
                    sp.setKeyPassword((String) keyMap.get(KEYRING_KEYPASS_KEY));
                }
                break;
            case ISignatureProperties.PASSMETHOD_IN_PROJECT:
                sp.setPasswordStorageMethod(nMethod);
                sp.setKeyStorePassword(decodePassword(e, ELEM_PWD_KEYSTORE));
                sp.setKeyPassword(decodePassword(e, ELEM_PWD_KEY));
                break;
            case ISignatureProperties.PASSMETHOD_PROMPT:
            default:
                sp
                        .setPasswordStorageMethod(ISignatureProperties.PASSMETHOD_PROMPT);
                sp.setKeyStorePassword(null);
                sp.setKeyPassword(null);
                break;
            }
        }

        signatureProps = sp;
    }

    private String encodePassword(String password) {
        if (password == null) {
            return (""); //$NON-NLS-1$
        }

        try {
            byte[] passwordBytes = password.getBytes("UTF8"); //$NON-NLS-1$
            byte[] cryptBytes = null;
            Cipher cipher = createCipher(Cipher.ENCRYPT_MODE);
            cryptBytes = cipher.doFinal(passwordBytes);

            return (Base64EncDec.encode(cryptBytes));
        } catch (UnsupportedEncodingException e) {
        } catch (GeneralSecurityException e) {
        }
        return (""); //$NON-NLS-1$
    }

    private String decodePassword(Element base, String subElementName) {
        Element subElement = XMLUtils.getFirstElementWithTagName(base,
                subElementName);
        if (subElement == null) {
            return (null);
        }

        String encoded = XMLUtils.getElementText(subElement);
        byte[] cryptBytes = Base64EncDec.decode(encoded);
        if (cryptBytes == null) {
            return (null);
        }

        try {
            byte[] passwordBytes = null;
            Cipher cipher = createCipher(Cipher.DECRYPT_MODE);
            passwordBytes = cipher.doFinal(cryptBytes);

            return (new String(passwordBytes, "UTF8")); //$NON-NLS-1$
        } catch (UnsupportedEncodingException e) {
        } catch (GeneralSecurityException e) {
        }

        return (null);
    }

    private Cipher createCipher(int opmode) throws GeneralSecurityException {
        PBEKeySpec keySpec = new PBEKeySpec(CRYPTO_PASS.toCharArray());
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(
                CRYPTO_ALGORITHM, "SunJCE"); //$NON-NLS-1$
        SecretKey secretKey = keyFactory.generateSecret(keySpec);

        AlgorithmParameterSpec paramSpec = new PBEParameterSpec(CRYPTO_SALT,
                CRYPTO_ITERATION_COUNT);
        Cipher cipher = Cipher.getInstance(secretKey.getAlgorithm(), "SunJCE"); //$NON-NLS-1$
        cipher.init(opmode, secretKey, paramSpec);
        return cipher;
    }

    /**
     * Attempt to save the metadata. This has the potential to fail.
     * 
     * @throws CoreException
     */
    public void saveMetaData() throws CoreException {
        IFile storeFile = getStoreFile();
        if (storeFile == null) {
            MTJCorePlugin.log(IStatus.WARNING,
                    "saveMetaData failed due to null store file"); //$NON-NLS-1$
        } else {
            if (storeFile.exists() && storeFile.isReadOnly()) {
                // Attempt to clear the read-only flag via the IResource
                // interface. This should invoke the team provider and
                // do necessary checkouts.
                ResourceAttributes attributes = storeFile
                        .getResourceAttributes();
                attributes.setReadOnly(false);
                storeFile.setResourceAttributes(attributes);
            }

            saveMetaDataToFile(storeFile);
        }
    }

    /**
     * Save the metadata to the storage file. This file is expected to be
     * non-null.
     * 
     * @param storeFile
     * @throws CoreException
     */
    public void saveMetaDataToFile(IFile storeFile) throws CoreException {
        try {
            String pluginVersion = MTJCorePlugin.getPluginVersion();
            Version newVersion = new Version(pluginVersion);
            Element rootElement = XMLUtils.createRootElement(ELEM_ROOT_NAME,
                    newVersion);
            if (jadFileName != null) {
                rootElement.setAttribute(ATTR_JAD_FILE, jadFileName);
            }

            if (device != null) {
                Element deviceElement = XMLUtils.createChild(rootElement,
                        ELEM_DEVICE);
                deviceElement.setAttribute(ATTR_DEVICEGROUP, device
                        .getGroupName());
                deviceElement.setAttribute(ATTR_DEVICENAME, device.getName());
            }

            Element signRoot = XMLUtils.createChild(rootElement, ELEM_SIGNING);
            boolean bSign = signatureProps.getSignProject();
            signRoot.setAttribute(ATTR_SIGN_PROJECT, Boolean.toString(bSign));

            if (bSign) {
                XMLUtils.createTextElement(signRoot, ELEM_KEYSTORE,
                        signatureProps.getKeyStoreDisplayPath());
                XMLUtils.createTextElement(signRoot, ELEM_ALIAS, signatureProps
                        .getKeyAlias());
                XMLUtils.createTextElement(signRoot, ELEM_PROVIDER,
                        signatureProps.getKeyStoreProvider());
                XMLUtils.createTextElement(signRoot, ELEM_KEYSTORETYPE,
                        signatureProps.getKeyStoreType());

                Element passRoot = XMLUtils.createChild(signRoot,
                        ELEM_PASSWORDS);
                passRoot.setAttribute(ATTR_STOREPASSWORDS, Integer
                        .toString(signatureProps.getPasswordStorageMethod()));

                Map<String, String> keyMap;
                Platform.flushAuthorizationInfo(getKeyringURL(), KEYRING_REALM,
                        KEYRING_SCHEME);
                switch (signatureProps.getPasswordStorageMethod()) {
                case ISignatureProperties.PASSMETHOD_IN_PROJECT:
                    XMLUtils
                            .createTextElement(passRoot, ELEM_PWD_KEYSTORE,
                                    encodePassword(signatureProps
                                            .getKeyStorePassword()));
                    XMLUtils.createTextElement(passRoot, ELEM_PWD_KEY,
                            encodePassword(signatureProps.getKeyPassword()));
                    break;
                case ISignatureProperties.PASSMETHOD_IN_KEYRING:
                    keyMap = new HashMap<String, String>();
                    keyMap.put(KEYRING_KEYSTOREPASS_KEY, signatureProps
                            .getKeyStorePassword());
                    keyMap.put(KEYRING_KEYPASS_KEY, signatureProps
                            .getKeyPassword());
                    Platform.addAuthorizationInfo(getKeyringURL(),
                            KEYRING_REALM, KEYRING_SCHEME, keyMap);
                    break;
                case ISignatureProperties.PASSMETHOD_PROMPT:
                default:
                    break;
                }
            }

            File localFile = storeFile.getLocation().toFile();
            XMLUtils.writeDocument(localFile, rootElement.getOwnerDocument());
            version = newVersion;
            getStoreIFile().refreshLocal(1, new NullProgressMonitor());
        } catch (ParserConfigurationException pce) {
            MTJCorePlugin.throwCoreException(IStatus.WARNING, 99999, pce);
        } catch (TransformerException te) {
            MTJCorePlugin.throwCoreException(IStatus.WARNING, 99999, te);
        } catch (IOException ioe) {
            MTJCorePlugin.throwCoreException(IStatus.WARNING, 99999, ioe);
        }
    }

    /**
     * Return the device stored in the project metadata.
     * 
     * @return
     */
    public IDevice getDevice() {
        return device;
    }

    /**
     * Return the name that must be used on the project's jad file after
     * deployment as specified in the project's metadata file.
     * 
     * @return
     */
    public String getJadFileName() {
        return jadFileName;
    }

    /**
     * Set the device stored in the project metadata.
     * 
     * @param device
     */
    public void setDevice(IDevice device) {
        this.device = device;
    }

    /**
     * Set the name that must be used on the project's jad file after deployment
     * 
     * @param jadFileName the jad file name
     */
    public void setJadFileName(String jadFileName) {
        this.jadFileName = jadFileName;
    }

    public ISignatureProperties getSignatureProperties() throws CoreException {
        return signatureProps;
    }

    /**
     * @return Returns the version.
     */
    public Version getVersion() {
        return version;
    }

    public void setSignatureProperties(ISignatureProperties p) {
        if (signatureProps == null) {
            signatureProps = new SignatureProperties();
        }

        signatureProps.copy(p);
    }

    /**
     * Get the IFile instance in which the metadata is to be stored.
     * 
     * @return
     */
    private IFile getStoreIFile() {
        return project.getFile(METADATA_FILE);
    }

    private URL getKeyringURL() throws CoreException {
        StringBuffer buf = new StringBuffer();
        buf.append(KEYRING_URL_BASE);

        String projectName = project.getName();
        int nLength = projectName.length();
        for (int i = 0; i < nLength; i++) {
            char c = projectName.charAt(i);
            if (Character.isLetterOrDigit(c) || c == '.') {
                buf.append(c);
            } else {
                buf.append('%');
                String hexString = Integer.toHexString((int) c);
                if ((hexString.length() & 0x01) != 0) {
                    buf.append('0');
                }
                buf.append(hexString);
            }
        }

        URL retval = null;

        try {
            retval = new URL(buf.toString());
        } catch (Exception ex) {
            MTJCoreErrors.throwCoreExceptionError(
                    MTJCoreErrors.SIGNING_INTERNAL_UNABLE_TO_BUILD_KEYRING_URL,
                    ex);
        }

        return (retval);
    }
}
