/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.device;

import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.model.Classpath;
import org.eclipse.mtj.core.model.LaunchEnvironment;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.preverifier.IPreverifier;
import org.eclipse.mtj.core.persistence.IPersistable;

/**
 * The device interface specifies the representation of an emulated device. Each
 * instance of IEmulator must provide a list of emulated devices that implement
 * this interface.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @see org.eclipse.mtj.core.model.device.IDevice
 * @author Craig Setera
 */
public interface IDevice extends IPersistable {

    /**
     * Return the classpath provided by this device. This classpath includes all
     * libraries for the device including configuration and profile libraries.
     * 
     * @return
     */
    public Classpath getClasspath();

    /**
     * Return the library that provides the configuration for this device or
     * <code>null</code> if no such library can be found.
     * 
     * @return
     */
    public ILibrary getConfigurationLibrary();

    /**
     * Return the displayable description of this device. This description will
     * be displayed within the user interface. If this method returns a
     * <code>null</code> value, the device's name will be used as the
     * description instead.
     * 
     * @return the description of this device or <code>null</code> if the
     *         device's name should be used instead.
     */
    public String getDescription();

    /**
     * Return the properties associated with this device. The available
     * properties will vary from emulator to emulator.
     * 
     * @return the properties associated with this device.
     */
    public Properties getDeviceProperties();

    /**
     * Return the name of the device group to which this device belongs. This
     * method must not return a <code>null</code> value.
     * 
     * @return
     */
    public String getGroupName();

    /**
     * Return the command-line arguments for launching this device given the
     * specified launch environment.
     * 
     * @param launchEnvironment
     * @param monitor
     * @return
     * @throws CoreException
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException;

    /**
     * Return the name of this device. This name will be used when interacting
     * with the emulator. This name may or may not be displayed within the user
     * interface, dependent on whether a display name is provided by this
     * device. This method must never return <code>null</code>.
     * 
     * @return the non-null name of this device.
     */
    public String getName();

    /**
     * Return the preverifier to be used to preverify classes for this device.
     * 
     * @return the platform's preverifier
     */
    public IPreverifier getPreverifier();

    /**
     * Return the library that provides the profile for this device or
     * <code>null</code> if no such library can be found.
     * 
     * @return
     */
    public ILibrary getProfileLibrary();

    /**
     * Return the list of protection domains specified by this device. Returning
     * <code>null</code> from this method will imply that this device does not
     * support protection domains.
     * 
     * @return the list of protection domains or <code>null</code> if the
     *         device does not provide any protection domains.
     */
    public String[] getProtectionDomains();

    /**
     * Return a boolean describing whether this device wants to act as a debug
     * server rather than attaching to the debugger as a client.
     * 
     * @return if the device acts as a debug server
     */
    public boolean isDebugServer();

    /**
     * Return a boolean indicating whether this device requires a deployed jar
     * file to be created before launch can be done. If this device requires
     * pre-deployment, standard packaging (without obfuscation) will be done if
     * the currently deployed jar file is not up to date.
     * 
     * @return whether or not pre-deployment is necessary.
     * @deprecated This method is no longer called during launch
     */
    public boolean isPredeploymentRequired();

    /**
     * Set the name of this device. This name will be used when interacting with
     * the emulator. This name may or may not be displayed within the user
     * interface, dependent on whether a display name is provided by this
     * device. This method must never return <code>null</code>.
     * 
     * @param name the non-null name of this device.
     */
    public void setName(String name);
}
