/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Removing Preprocessor code
 *     Diego Sandin (Motorola)  - Fix getProfileVersion method
 *     Feng Wang (Sybase)       - Add removeMidletSuiteProject
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code 
 *     Diego Sandin (Motorola)  - Changed the way to set the project compliance 
 *                                options
 */
package org.eclipse.mtj.core.model.project;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Preferences;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.ColonDelimitedProperties;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.jad.IJADConstants;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.library.api.API;
import org.eclipse.mtj.core.model.project.impl.MidletSuiteProject;

/**
 * A factory for accessing MIDlet suite instances based on projects.
 * 
 * @author Craig Setera
 */
public class MidletSuiteFactory {

    /**
     * Workspace runnable for creating a MIDlet Suite within a project.
     */
    public static class MidletSuiteCreationRunnable {

        /**
         * 
         */
        private static final String BIN_FOLDER_NAME = "bin";

        /**
         * 
         */
        private static final String CONFIGURATION_VERSION_1_0 = "1.0";

        /**
         * 
         */
        private static final String MIDLET_DEFAULT_NAME_END = " MIDlet Suite";

        /**
         * 
         */
        private static final String MIDLET_DEFAULT_VENDOR = "MIDlet Suite Vendor";

        /**
         * 
         */
        private static final String MIDLET_INITIAL_VERSION_1_0_0 = "1.0.0";

        private IDevice device;

        private String jadFileName;

        private IJavaProject javaProject;

        private boolean preprocessingEnable;

        private IProject project;

        /**
         * Construct a new runnable for MIDlet suite creation.
         * 
         * @param project
         */
        private MidletSuiteCreationRunnable(IProject project,
                IJavaProject javaProject, IDevice device, String jadFileName) {
            this.project = project;
            this.javaProject = javaProject;
            this.device = device;
            this.jadFileName = jadFileName;
        }

        /**
         * @return the preprocessEnable
         */
        public boolean isPreprocessingEnable() {
            return preprocessingEnable;
        }

        /**
         * Run the specified runnable using the specified progress monitor.
         * 
         * @param monitor the monitor used to report progress
         * @throws InvocationTargetException
         * @throws InterruptedException
         */
        public void run(IProgressMonitor monitor)
                throws InvocationTargetException, InterruptedException {
            try {
                // Configure the project
                IMidletSuiteProject suite = getMidletSuiteProject(javaProject);

                suite.setDevice(device, monitor);
                suite.setJadFileName(jadFileName);
                addNatures(monitor);

                setJavaProjectOptions(monitor);

                // Set the platform definition for the project
                setProjectMetadata();

                // Add the default application descriptor file.
                createApplicationDescriptorInProject(monitor);

            } catch (CoreException e) {
                throw new InvocationTargetException(e);
            } catch (IOException e) {
                throw new InvocationTargetException(e);
            }
        }

        /**
         * @param preprocessEnable the preprocessEnable to set
         */
        public void setPreprocessingEnable(boolean preprocessEnable) {
            this.preprocessingEnable = preprocessEnable;
        }

        /**
         * Add the specified nature to the list of natures if it is not already
         * in the list.
         * 
         * @param natures
         * @param nature
         * @return
         */
        private boolean addNatureIfNecessary(ArrayList<String> natures,
                String nature) {
            boolean added = false;

            if (!natures.contains(nature)) {
                natures.add(nature);
                added = true;
            }

            return added;
        }

        /**
         * Add the Java ME nature to the specified project.
         * 
         * @param monitor
         * @throws CoreException
         */
        private void addNatures(IProgressMonitor monitor) throws CoreException {
            IProjectDescription desc = project.getDescription();
            ArrayList<String> natures = new ArrayList<String>(Arrays
                    .asList(desc.getNatureIds()));

            boolean updated = addNatureIfNecessary(natures,
                    IMTJCoreConstants.J2ME_NATURE_ID);
            updated = updated
                    | addNatureIfNecessary(natures, JavaCore.NATURE_ID);

            if (isPreprocessingEnable()) {
                updated = updated
                        | addNatureIfNecessary(natures,
                                IMTJCoreConstants.J2ME_PREPROCESSING_NATURE_ID);
            }

            if (updated) {
                IProgressMonitor submonitor = new SubProgressMonitor(monitor, 1);
                desc.setNatureIds(natures.toArray(new String[natures.size()]));
                project.setDescription(desc, submonitor);
            }
        }

        /**
         * Create the template application descriptor in the project.
         * 
         * @param monitor
         * @throws CoreException
         */
        private void createApplicationDescriptorInProject(
                IProgressMonitor monitor) throws CoreException, IOException {

            // Get the project references
            IMidletSuiteProject midletSuite = getMidletSuiteProject(javaProject);

            // Check the JAD file for existence
            IFile jadFile = midletSuite.getApplicationDescriptorFile();

            if (!jadFile.exists()) {
                InputStream is = getJADFileSource(midletSuite);
                jadFile.create(is, true, monitor);
            }
        }

        /**
         * Return the version of the configuration from the device or
         * <code>null</code> if the version cannot be determined.
         * 
         * @return
         */
        private String getConfigurationVersion() {
            ILibrary library = device.getConfigurationLibrary();

            API api = null;
            if (library != null) {
                api = library.getConfiguration();
            }

            return (api == null) ? CONFIGURATION_VERSION_1_0 : api.toString();
        }

        /**
         * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#getDefaultApplicationDescriptorProperties()
         */
        private ColonDelimitedProperties getDefaultApplicationDescriptorProperties(
                IMidletSuiteProject suite) {
            ColonDelimitedProperties descriptor = new ColonDelimitedProperties();

            // Do the OTA URL
            descriptor.setProperty(IJADConstants.JAD_MIDLET_JAR_URL, suite
                    .getJarFilename());

            // Couple of names...
            descriptor.setProperty(IJADConstants.JAD_MIDLET_NAME, project
                    .getName()
                    + MIDLET_DEFAULT_NAME_END);
            descriptor.setProperty(IJADConstants.JAD_MIDLET_VENDOR,
                    MIDLET_DEFAULT_VENDOR);
            descriptor.setProperty(IJADConstants.JAD_MIDLET_VERSION,
                    MIDLET_INITIAL_VERSION_1_0_0);

            // Platform information
            String configVersion = getConfigurationVersion();
            if (configVersion != null) {
                descriptor.setProperty(IJADConstants.JAD_MICROEDITION_CONFIG,
                        configVersion);
            }

            String profileVersion = getProfileVersion();
            if (profileVersion != null) {
                descriptor.setProperty(IJADConstants.JAD_MICROEDITION_PROFILE,
                        profileVersion);
            }

            return descriptor;
        }

        /**
         * Get the bytes that make up the contents of the to-be created JAD
         * file.
         * 
         * @param midletSuite
         * @return
         * @throws IOException
         * @throws CoreException
         */
        private InputStream getJADFileSource(IMidletSuiteProject midletSuite)
                throws IOException, CoreException {
            // The InputStream to be returned...
            InputStream is = null;

            // Check for a JAD source file to see if it exists in the
            // project source directory
            String jadName = midletSuite.getJadFileName();
            IFolder folder = project.getFolder(BIN_FOLDER_NAME);
            if (folder.exists()) {
                IFile jadFile = folder.getFile(jadName);
                if (jadFile.exists()) {
                    is = jadFile.getContents();
                }
            }

            // If we didn't get a previous source file, use defaults
            if (is == null) {
                // Get the application descriptor and write it to
                // a byte array
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ColonDelimitedProperties defaultProps = getDefaultApplicationDescriptorProperties(midletSuite);
                defaultProps.store(bos, ""); //$NON-NLS-1$

                // Use the byte array as the source to create the
                // new file
                is = new ByteArrayInputStream(bos.toByteArray());
            }

            return is;
        }

        /**
         * Return the version of the profile from the device or
         * <code>null</code> if the version cannot be determined.
         * 
         * @return
         */
        private String getProfileVersion() {
            ILibrary library = device.getProfileLibrary();
            API api = (library == null) ? null : library.getProfile();

            return (api == null) ? null : api.toString();
        }

        /**
         * Set the necessary java project options.
         * 
         * @param monitor
         */
        @SuppressWarnings("unchecked")
        private void setJavaProjectOptions(IProgressMonitor monitor) {
            Preferences prefs = MTJCorePlugin.getDefault()
                    .getPluginPreferences();

            boolean forceJava11 = prefs
                    .getBoolean(IMTJCoreConstants.PREF_FORCE_JAVA11);

            if (forceJava11) {
                Map<String, String> options = javaProject.getOptions(true);
                JavaCore.setComplianceOptions(JavaCore.VERSION_1_3, options);
                javaProject.setOptions(options);
            }
        }

        /**
         * Set the persistent property on the created project to track the
         * platform definition.
         */
        private void setProjectMetadata() throws CoreException {
            IMidletSuiteProject midletprj = getMidletSuiteProject(javaProject);
            midletprj.saveMetaData();
        }
    }

    // Storage of the previously created MIDlet suite projects
    private static final Map<IJavaProject, IMidletSuiteProject> midletSuiteMap = new HashMap<IJavaProject, IMidletSuiteProject>();

    /**
     * Return a runnable capable of setting up the J2ME nature on the project.
     * 
     * @return a runnable that can be used to create a new MIDlet suite
     */
    public static MidletSuiteCreationRunnable getMidletSuiteCreationRunnable(
            IProject project, IJavaProject javaProject, IDevice device,
            String jadFileName) {
        return new MidletSuiteCreationRunnable(project, javaProject, device,
                jadFileName);
    }

    /**
     * Return the MIDlet suite project instance for the specified java project.
     * 
     * @param javaProject the Java project to retrieve the MIDlet suite wrapper
     * @return the MIDlet suite wrapper
     */
    public static IMidletSuiteProject getMidletSuiteProject(
            IJavaProject javaProject) {
        IMidletSuiteProject suite = null;

        synchronized (midletSuiteMap) {
            suite = midletSuiteMap.get(javaProject);
            if (suite == null) {
                suite = new MidletSuiteProject(javaProject);
                midletSuiteMap.put(javaProject, suite);
            }
        }

        return suite;
    }

    /**
     * Remove the MIDlet suite project from the cache hash-map.
     * 
     * @param javaProject - the Java project wrapped by the MIDlet suite which
     *            will be removed.
     */
    public static void removeMidletSuiteProject(IJavaProject javaProject) {
        synchronized (midletSuiteMap) {
            midletSuiteMap.remove(javaProject);
        }
    }

    /**
     * Static-only access
     */
    private MidletSuiteFactory() {
        super();
    }
}
