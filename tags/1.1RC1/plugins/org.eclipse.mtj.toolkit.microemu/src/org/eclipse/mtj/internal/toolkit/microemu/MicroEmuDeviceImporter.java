/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)   - Initial implementation
 *     Diego Sandin (Motorola)    - Refactoring package name to follow eclipse 
 *                                  standards
 *     Daniel Murphy (Individual) - Set up the deviceClasspath correctly  
 *     Diego Sandin (Motorola)    - Changed the device discovery algorithm
 *     Diego Sandin (Motorola)    - Use JavaEmulatorDeviceProperties enum instead of 
 *                                  hard-coded strings
 *     Diego Sandin (Motorola)    - IDeviceImporter API refactoring  
 */
package org.eclipse.mtj.internal.toolkit.microemu;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.IDeviceImporter;
import org.eclipse.mtj.core.sdk.device.ILibraryImporter;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary;
import org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.sdk.BasicSDK;
import org.eclipse.mtj.internal.core.sdk.device.JavaEmulatorDeviceImporter;
import org.eclipse.mtj.internal.core.sdk.device.JavaEmulatorDeviceProperties;
import org.eclipse.mtj.internal.core.util.ReplaceableParametersProcessor;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.osgi.framework.Version;

/**
 * An {@link IDeviceImporter} implementation that recognizes and imports
 * MicroEmulator toolkit instances.
 * 
 * @author Craig Setera
 */
public class MicroEmuDeviceImporter extends JavaEmulatorDeviceImporter {

    /* Devices Skins */
    private static final MicroEmuDeviceSkin EMULATOR_DEVICE_SKINS[] = {
            new MicroEmuDeviceSkin(MicoEmuConstants.EMULATOR_STANDARDSKIN_JAR,
                    MicoEmuConstants.EMULATOR_STANDARDSKIN_PATH,
                    MicoEmuConstants.EMULATOR_STANDARDSKIN_NAME),
            new MicroEmuDeviceSkin(MicoEmuConstants.EMULATOR_LARGESKIN_JAR,
                    MicoEmuConstants.EMULATOR_LARGESKIN_PATH,
                    MicoEmuConstants.EMULATOR_LARGESKIN_NAME),
            new MicroEmuDeviceSkin(MicoEmuConstants.EMULATOR_MINIMUNSKIN_JAR,
                    MicoEmuConstants.EMULATOR_MINIMUNSKIN_PATH,
                    MicoEmuConstants.EMULATOR_MINIMUNSKIN_NAME) };

    /**
     * The MicroEmulator jar name
     */
    private static final String EMULATOR_JAR_NAME = "microemulator.jar"; //$NON-NLS-1$

    /**
     * The MicroEmulator main class name
     */
    private static final String MAIN_CLASS_NAME = "org.microemu.app.Main"; //$NON-NLS-1$

    /**
     * Properties file holding emulator/device information
     */
    private static final String PROPS_FILE = "microemu.properties"; //$NON-NLS-1$

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceImporter#importDevices(java.io.File, org.eclipse.core.runtime.IProgressMonitor)
     */
    public List<IDevice> importDevices(File directory, IProgressMonitor monitor) {

        List<IDevice> deviceList = null;

        try {
            /* Check if the MicoEmulator Jar exists */
            File jarFile = new File(directory, EMULATOR_JAR_NAME);
            if (jarFile.exists()
                    && hasMainClassAttribute(jarFile, MAIN_CLASS_NAME)) {

                deviceList = new ArrayList<IDevice>();

                /* Import all devices */
                for (MicroEmuDeviceSkin element : EMULATOR_DEVICE_SKINS) {
                    IMIDPDevice singleDevice = createDevice(jarFile, element);
                    deviceList.add(singleDevice);
                }
            }
        } catch (Exception e) {
            MTJLogger.log(IStatus.WARNING,
                    Messages.MicroEmuDeviceImporter_import_error, e);
        }

        return deviceList;
    }

    /**
     * Add the device libraries.
     * 
     * @param jarFile
     * @param deviceClasspath
     * @param libraryImporter
     */
    private void addDeviceLibraries(File jarFile,
            IDeviceClasspath deviceClasspath, ILibraryImporter libraryImporter) {
        // Now add the player libraries
        String classpathString = getDeviceProperties().getProperty(
                JavaEmulatorDeviceProperties.CLASSPATH.toString(),
                Utils.EMPTY_STRING);

        Map<String, String> replaceableParameters = new HashMap<String, String>();
        replaceableParameters.put(MicroEmuLaunchTemplateProperties.TOOLKITROOT
                .toString(), jarFile.getParent());

        classpathString = ReplaceableParametersProcessor
                .processReplaceableValues(classpathString,
                        replaceableParameters);
        String[] entries = classpathString.split(";"); //$NON-NLS-1$

        for (String entrie : entries) {
            IMIDPLibrary library = (IMIDPLibrary) libraryImporter
                    .createLibraryFor(new File(entrie));

            // Because of the structure of the libraries,
            // we need to hard-code the CLDC and MIDP libraries
            IMIDPAPI api = library.getAPI(MIDPAPIType.UNKNOWN);
            if (api != null) {
                if (api.getIdentifier().equalsIgnoreCase("cldcapi11.jar")) { //$NON-NLS-1$
                    api.setIdentifier("CLDC"); //$NON-NLS-1$
                    api.setType(MIDPAPIType.CONFIGURATION);
                    api.setName("Connected Limited Device Configuration"); //$NON-NLS-1$
                    api.setVersion(new Version("1.1")); //$NON-NLS-1$
                } else if (api.getIdentifier()
                        .equalsIgnoreCase("midpapi20.jar")) { //$NON-NLS-1$
                    api.setIdentifier("MIDP"); //$NON-NLS-1$
                    api.setType(MIDPAPIType.PROFILE);
                    api.setName("Mobile Information Device Profile"); //$NON-NLS-1$
                    api.setVersion(new Version("2.0")); //$NON-NLS-1$
                } else if (api.getIdentifier().equalsIgnoreCase(
                        "microemu-jsr-75.jar")) { //$NON-NLS-1$
                    api.setIdentifier("JSR75"); //$NON-NLS-1$
                    api.setType(MIDPAPIType.OPTIONAL);
                    api.setName("PDA Optional Packages for the J2ME Platform"); //$NON-NLS-1$
                    api.setVersion(new Version("1.0")); //$NON-NLS-1$
                } else if (api.getIdentifier().equalsIgnoreCase(
                        "microemu-jsr-82.jar")) { //$NON-NLS-1$
                    api.setIdentifier("JSR82"); //$NON-NLS-1$
                    api.setType(MIDPAPIType.OPTIONAL);
                    api.setName("Java APIs for Bluetooth"); //$NON-NLS-1$
                    api.setVersion(new Version("1.1")); //$NON-NLS-1$
                } else if (api.getIdentifier().equalsIgnoreCase(
                        "microemu-jsr-120.jar")) { //$NON-NLS-1$
                    api.setIdentifier("JSR120"); //$NON-NLS-1$
                    api.setType(MIDPAPIType.OPTIONAL);
                    api.setName("Wireless Messaging API"); //$NON-NLS-1$
                    api.setVersion(new Version("1.0")); //$NON-NLS-1$
                } else if (api.getIdentifier().equalsIgnoreCase(
                        "microemu-jsr-135.jar")) { //$NON-NLS-1$
                    api.setIdentifier("JSR135"); //$NON-NLS-1$
                    api.setType(MIDPAPIType.OPTIONAL);
                    api.setName("Mobile Media API"); //$NON-NLS-1$
                    api.setVersion(new Version("1.0")); //$NON-NLS-1$
                } else if (api.getIdentifier().equalsIgnoreCase(
                        "microemu-nokiaui.jar")) { //$NON-NLS-1$
                    api.setIdentifier("Nokia UI"); //$NON-NLS-1$
                    api.setType(MIDPAPIType.OPTIONAL);
                    api.setName("Nokia UI API"); //$NON-NLS-1$
                    api.setVersion(new Version("1.1")); //$NON-NLS-1$
                } else if (api.getIdentifier().equalsIgnoreCase(
                        "microemu-siemensapi.jar")) { //$NON-NLS-1$
                    api.setIdentifier("Siemens API"); //$NON-NLS-1$
                    api.setType(MIDPAPIType.OPTIONAL);
                    api.setName("Siemens API"); //$NON-NLS-1$
                    api.setVersion(new Version("1.0")); //$NON-NLS-1$
                }

                deviceClasspath.addEntry(library);
            }
        }
    }

    /**
     * Create the new device instance for the specified Microemulator jar file.
     * 
     * @param jarFile
     * @param skin TODO
     * @return
     */
    private IMIDPDevice createDevice(File jarFile, MicroEmuDeviceSkin skin) {

        MicroEmuDevice device = new MicroEmuDevice();

        device.setBundle(Activator.getDefault().getBundle().getSymbolicName());
        device.setClasspath(getDeviceClasspath(jarFile));
        device.setDebugServer(isDebugServer());
        device.setDescription("Microemulator Device"); //$NON-NLS-1$
        device.setDeviceProperties(new Properties());
        device.setGroupName("Microemulator"); //$NON-NLS-1$
        device.setName(skin.getName());
        device.setPreverifier(getPreverifier(jarFile));
        device.setProtectionDomains(new String[0]);
        device.setLaunchCommandTemplate(getLaunchCommand());
        device.setRoot(jarFile.getParentFile());
        device.setSkin(skin);
        ISymbolSet dss = MTJCore.getSymbolSetFactory()
                .createSymbolSetFromDevice(device);
        dss.setName(device.getName());
        device.setSymbolSet(dss);
        
        // Associate the SDK with the device... This doesn't really belong
        // here, but for 1.0 it is done this way to make the API work until
        // ISDK can be fully built out post-1.0
        Version v = new Version(1,0,0);
        ISDK sdk = BasicSDK.getSDK("Microemulator", v);
        device.setSDK(sdk);
        ((BasicSDK) sdk).addDevice(device);

        return device;
    }

    /**
     * Get the device deviceClasspath based on the specified player.jar file.
     * 
     * @param jarFile
     * @return
     */

    private IDeviceClasspath getDeviceClasspath(File jarFile) {
        IDeviceClasspath deviceClasspath = MTJCore.createNewDeviceClasspath();
        addDeviceLibraries(jarFile, deviceClasspath, MTJCore
                .getLibraryImporter(ILibraryImporter.LIBRARY_IMPORTER_UEI));

        return deviceClasspath;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.importer.impl.JavaEmulatorDeviceImporter#getDevicePropertiesURL()
     */
    @Override
    protected URL getDevicePropertiesURL() {
        return Activator.getDefault().getBundle().getEntry(PROPS_FILE);
    }
}
