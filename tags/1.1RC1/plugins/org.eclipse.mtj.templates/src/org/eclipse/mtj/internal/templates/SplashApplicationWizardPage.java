/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.templates;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * SplashApplicationWizardPage provides a template to create 
 * a splash screen application.
 * 
 * @author David Marques
 * @since 1.0
 */
public class SplashApplicationWizardPage extends AbstractTemplateWizardPage {

    private Text imageText;
    private Button size1;
    private Button size2;
    private Button size3;
    private Text colorText;
    private Text timeText;

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        GridData data = null;
        parent.setLayout(new GridLayout(0x01, false));
        
        Group group = new Group(parent, SWT.NONE);
        group.setText(Messages.SplashTemplateProvider_0);
        
        data = new GridData(SWT.FILL, SWT.FILL, true, false);
        group.setLayoutData(data);
        group.setLayout(new GridLayout(3, false));
        
        Label imageLabel = new Label(group, SWT.NONE);
        imageLabel.setText(Messages.SplashTemplateProvider_1);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        imageLabel.setLayoutData(data);
        
        imageText = new Text(group, SWT.BORDER);
        imageText.setText(Messages.SplashTemplateProvider_2);
        
        imageText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        data.horizontalSpan = 2;
        imageText.setLayoutData(data);
        
        Group sizesGroup = new Group(group, SWT.NONE);
        sizesGroup.setText(Messages.SplashTemplateProvider_3);
        
        data = new GridData(SWT.FILL, SWT.FILL, true, false);
        data.horizontalSpan = 3;
        sizesGroup.setLayoutData(data);
        sizesGroup.setLayout(new GridLayout(3, false));
        
        size1 = new Button(sizesGroup, SWT.RADIO);
        size1.setText("128 x 160"); //$NON-NLS-1$
        data = new GridData(GridData.FILL_HORIZONTAL);
        size1.setLayoutData(data);
        size1.setSelection(true);
        
        size2 = new Button(sizesGroup, SWT.RADIO);
        size2.setText("176 x 220"); //$NON-NLS-1$
        data = new GridData(GridData.FILL_HORIZONTAL);
        size2.setLayoutData(data);
        
        size3 = new Button(sizesGroup, SWT.RADIO);
        size3.setText("240 x 320"); //$NON-NLS-1$
        data = new GridData(GridData.FILL_HORIZONTAL);
        size3.setLayoutData(data);
        
        Label colorLabel = new Label(group, SWT.NONE);
        colorLabel.setText(Messages.SplashTemplateProvider_7);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        colorLabel.setLayoutData(data);
        
        colorText = new Text(group, SWT.BORDER);
        colorText.setText("0xFFFFFF"); //$NON-NLS-1$
        
        colorText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        colorText.setLayoutData(data);
        
        Label formatLabel = new Label(group, SWT.NONE);
        formatLabel.setText(Messages.SplashTemplateProvider_9);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        formatLabel.setLayoutData(data);
        
        Label timeLabel = new Label(group, SWT.NONE);
        timeLabel.setText(Messages.SplashTemplateProvider_10);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        timeLabel.setLayoutData(data);
        
        timeText = new Text(group, SWT.BORDER);
        timeText.setText("3000"); //$NON-NLS-1$
        
        timeText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        data.horizontalSpan = 2;
        timeText.setLayoutData(data);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.ITemplateProvider#getDictionary()
     */
    public Map<String, String> getDictionary() {
        Map<String, String> dictionary = new HashMap<String, String>();
        
        dictionary.put("$image-name$", this.imageText.getText()); //$NON-NLS-1$
        dictionary.put("$image-size$", getImageSize()); //$NON-NLS-1$
        dictionary.put("$splash-time$", this.timeText.getText()); //$NON-NLS-1$
        dictionary.put("$bg-color$", this.colorText.getText()); //$NON-NLS-1$
        
        return dictionary;
    }

    private String getImageSize() {
        StringBuffer buffer = new StringBuffer();
        if (this.size1.getSelection()) {
            buffer.append("_128x160"); //$NON-NLS-1$
        } else
        if (this.size2.getSelection()) {
            buffer.append("_176x220"); //$NON-NLS-1$
        } else
        if (this.size3.getSelection()) {
            buffer.append("_240x320"); //$NON-NLS-1$
        }
        return buffer.toString();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.ITemplateProvider#isPageComplete()
     */
    public boolean isPageComplete() {
        boolean result = true;
        result &= Pattern.matches("(0x)([a-fA-F0-9]){6}", this.colorText.getText()); //$NON-NLS-1$
        result &= Pattern.matches("\\d+", this.timeText.getText()); //$NON-NLS-1$
        result &= this.imageText.getText().length() > 0x00;
        return result;
    }
}
