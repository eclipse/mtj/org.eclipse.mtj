/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - IFoundDevicesList API refactory
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mtj.core.sdk.device.IDevice;

/**
 * Simple implementation of the IFoundDevicesList interface. This implementation
 * collects the devices, but does nothing else with that information.
 * 
 * @author Craig Setera
 */
public class SimpleFoundDevicesList implements IFoundDevicesList {
    private ArrayList<IDevice> devices;

    /**
     * Construct a new device list.
     */
    public SimpleFoundDevicesList() {
        super();
        devices = new ArrayList<IDevice>();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.sdk.device.IFoundDevicesList#addDevices(java.util.List)
     */
    public void addDevices(List<IDevice> devices) {
        if (devices != null) {
            this.devices.addAll(devices);
        }
    }

    /**
     * Clear the previous results from this list.
     */
    public void clear() {
        // Allow the previous results to be cleared
        devices.clear();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.sdk.device.IFoundDevicesList#getDevices()
     */
    public List<IDevice> getDevices() {
        return devices;
    }
}
