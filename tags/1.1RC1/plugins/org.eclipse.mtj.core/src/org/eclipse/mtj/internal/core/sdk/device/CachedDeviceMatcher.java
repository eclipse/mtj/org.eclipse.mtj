/**
 * Copyright (c) 2009 Sony Ericsson.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Daniel Olsson (Sony Ericsson) - Initial contribution
 *     Gustavo de Paula (Individual) - Remove dependency with MTJCore.getDeviceMacher
 */
package org.eclipse.mtj.internal.core.sdk.device;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDevice;

/**
 * This class is a non-user interactive implementation of {@link IDeviceMatcher}
 * , that uses (by the user) cached values to match a given SDK and device
 * against installed ones.
 * 
 * @author Daniel Olsson
 */
public class CachedDeviceMatcher implements IDeviceMatcher {

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.core.sdk.device.IDeviceMatcher#match(java.lang.String,
     * java.lang.String)
     */
    public IDevice match(String deviceGroup, String deviceName) {
        String[] cachedValue = getCachedValue(deviceGroup, deviceName);
        if (cachedValue != null) {
            try {
                IDevice cachedDevice = MTJCore.getDeviceRegistry().getDevice(
                        cachedValue[0], cachedValue[1]);
                return cachedDevice;
            } catch (PersistenceException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * @param deviceGroup
     * @param deviceName
     * @return
     */
    private String[] getCachedValue(String deviceGroup, String deviceName) {
        // If exists in cache, return that value
        IDeviceMatchCache cache = DeviceMatchCache.getInstance();
        String[] cachedValue = cache.getValueFromCache(cache.createKey(
                deviceGroup, deviceName));
        return cachedValue;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.core.sdk.device.IDeviceMatcher#match(java.lang
     * .String, java.lang.String, java.lang.String)
     */
    public String[] match(String configurationName, String deviceGroup,
            String deviceName) {
        String[] cachedValue = getCachedValue(deviceGroup, deviceName);
        if (cachedValue != null) {
            // configname , SDK , device
            return new String[] { cachedValue[1], cachedValue[0],
                    cachedValue[1] };
        }
        return null;
    }
}
