/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin  (Motorola) - Initial Version
 *     David Marques (Motorola) - Fixing version migration.
 */
package org.eclipse.mtj.internal.core.util.migration;

/**
 * @author Diego Madruga Sandin
 */
public interface ISymbolDefinitionsMigrationConstants {

    public static final String SYMBOLDEFINITIONSET_CLASS = "org.eclipse.mtj.internal.core.symbol.SymbolSet";

}
