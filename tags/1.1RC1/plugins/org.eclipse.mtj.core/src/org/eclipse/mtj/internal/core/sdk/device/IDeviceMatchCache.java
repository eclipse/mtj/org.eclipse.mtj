/**
 * Copyright (c) 2009 Sony Ericsson.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Daniel Olsson (Sony Ericsson) - Initial contribution
 */
package org.eclipse.mtj.internal.core.sdk.device;

import org.eclipse.mtj.core.sdk.device.IDevice;

/**
 * The only implementation of this interface is {@link DeviceMatchCache}
 * 
 * @author Daniel Olsson
 * @see {@link DeviceMatchCache}
 */
public interface IDeviceMatchCache {

    /**
     * Get a SDK, device pair from cache given the SDK, device key.
     * 
     * @param key SDK and device as a single string
     * @return String array with matching SDK and device, or <b>null</b> if not
     *         found in cache
     * @see {@link #createKey(String, String)}
     */
    public abstract String[] getValueFromCache(String key);

    /**
     * Creates a key for the cache given the SDK and device
     * 
     * @param deviceGroup SDK as a string
     * @param deviceName Device as a string
     * @return String to be used as a key for the cache
     */
    public abstract String createKey(String deviceGroup, String deviceName);

    /**
     * Saves the value with the specified key in the cache
     * 
     */
    public abstract void saveInCache(String key, String[] value);

    /**
     * Creates a value for the cache from an {@link IDevice}
     * 
     * @param device The IDevice to create a String[] value for
     * @return A String[] as { "&ltSDK&gt", "&ltdevice&gt" }
     */
    public abstract String[] createValue(IDevice device);

}