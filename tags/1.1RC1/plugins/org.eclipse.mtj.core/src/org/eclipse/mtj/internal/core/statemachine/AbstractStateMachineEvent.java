/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.statemachine;

/**
 * AbstractStateMachineEvent class defines an abstract type
 * to be extended in order to create events to be sent to
 * the {@link StateMachine#postEvent(AbstractStateMachineEvent)}.
 * <br>
 * All events sent to the {@link StateMachine} are dispatched
 * to the current state transitions 
 * {@link AbstractStateTransition#isTransitionReady(AbstractStateMachineEvent)}
 * method in order to trigger state changes.
 * 
 * @author David Marques
 */
public abstract class AbstractStateMachineEvent {

}
