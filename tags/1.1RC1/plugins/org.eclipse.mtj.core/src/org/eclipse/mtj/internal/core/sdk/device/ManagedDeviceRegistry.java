/**
 * Copyright (c) 2009 Research In Motion Limited.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial Version
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.core.sdk.ISDKProvider;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.core.sdk.SDKProviderRegistry;

/**
 * ManagedDeviceRegistry is contained by DeviceRegistry. ManagedDeviceRegistry
 * is a wrapper around a collection of SDK providers. SDK providers contain
 * managed SDKs which in turn contain managed devices. Devices are persisted by
 * SDK providers outside of the DeviceRegistry. Managed devices are not deleted
 * (unless they are a duplicate), though they may be edited and duplicated.
 * ManagedDeviceRegistry does not perform any persistence.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * 
 * @since 1.1
 * @noextend This class is not intended to be subclassed by clients.
 */
class ManagedDeviceRegistry {

    ManagedDeviceRegistry() {

    }

    List<IDevice> getAllDevices() throws PersistenceException {
        return getAllDevicesInternal(false, null);
    }

    IDevice getDevice(String groupName, String deviceName)
            throws PersistenceException {
        IDevice device = null;
        List<ISDKProvider> providers = SDKProviderRegistry.getInstance().getSDKProviders();
        // For each SDK provider... 
        for (ISDKProvider provider : providers) {
            List<ISDK> sdks = provider.getSDKs();
            List<IDevice> devices;
            // For each SDK...
            for (ISDK sdk : sdks) {
                if (sdk == null || !sdk.getName().equals(groupName))
                    continue;
                try {
                    devices = sdk.getDeviceList();
                } catch (CoreException e) {
                    e.printStackTrace();
                    continue;
                }
                // For each device...
                for (IDevice managedDevice : devices) {
                    if (managedDevice == null)
                        continue;
                    // Match?
                    String autoDeviceName = managedDevice.getName();
                    if (autoDeviceName.equals(deviceName)) {
                        return managedDevice;
                    }
                }
            }
        }
        return device;
    }

    int getDeviceCount() throws PersistenceException {
        int deviceCount = 0;
        List<ISDKProvider> providers = SDKProviderRegistry.getInstance()
        .getSDKProviders();
        // For each SDK provider...
        for (ISDKProvider provider : providers) {
            List<ISDK> sdks = provider.getSDKs();
            List<IDevice> devices;
            // For each SDK...
            for (ISDK sdk : sdks) {
                if (sdk == null)
                    continue;
                try {
                    devices = sdk.getDeviceList();
                } catch (CoreException e) {
                    e.printStackTrace();
                    continue;
                }
                // For each device...
                for (IDevice managedDevice : devices) {
                    if (managedDevice != null) {
                        deviceCount++;
                    }
                }
            }
        }
        return deviceCount;
    }

    List<IDevice> getDevices(String groupName)
            throws PersistenceException {
        List<IDevice> devices = getAllDevicesInternal(true, groupName);
        return (devices.isEmpty() ? null : devices);
    }

    List<String> getSDKNames() throws PersistenceException {
        List<String> sdksNames = new ArrayList<String>();
        List<ISDKProvider> providers = SDKProviderRegistry.getInstance().getSDKProviders();
        // For each SDK provider... 
        for (ISDKProvider provider : providers) {
            List<ISDK> sdks = provider.getSDKs();
            // For each SDK...
            for (ISDK sdk : sdks) {
                if (sdk != null) {
                    sdksNames.add(sdk.getName());
                }
            }
        }
        return sdksNames;
    }
    
    /**
     * @return An empty list if no matching devices are found.
     */
    private List<IDevice> getAllDevicesInternal(boolean filterByGroupName,
            String groupName) throws PersistenceException {
        
        // If filterByGroupName is false, a non-null groupName is likely an error
        if (!filterByGroupName && groupName != null)
            throw new IllegalArgumentException("If filterByGroupName is false, groupName should be null");
        
        if (filterByGroupName && groupName == null)
            throw new IllegalArgumentException("If filterByGroupName is true, groupName cannot be null");
     
        ArrayList<IDevice> allDevices = new ArrayList<IDevice>();
        List<ISDKProvider> providers = SDKProviderRegistry.getInstance().getSDKProviders();
        
        // For each SDK provider... 
        for (ISDKProvider provider : providers) {
            List<ISDK> sdks = provider.getSDKs();
            List<IDevice> devices;
            // For each SDK...
            for (ISDK sdk : sdks) {
                if (sdk == null)
                    continue;
                if (filterByGroupName && !sdk.getName().equals(groupName))
                    continue;
                try {
                    devices = sdk.getDeviceList();
                } catch (CoreException e) {
                    e.printStackTrace();
                    continue;
                }
                // For each device...
                for (IDevice device : devices) {
                    if (device != null) {
                        allDevices.add(device);
                    }
                }
            }
        }

        return allDevices;
    }
    
}
