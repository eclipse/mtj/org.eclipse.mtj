/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gustavo de Paula (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk.device;

import org.eclipse.mtj.core.persistence.IPersistable;
import org.osgi.framework.Version;

/**
 * This interface represent a API that is provided by one specific device.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @since 1.0
 */
public interface IAPI extends IPersistable {

    /**
     * Returns the API identifier. For example, if a device support WMA version
     * 1.1, this method should return "WMA" as identifier.
     * 
     * @return API identifier.
     */
    public String getIdentifier();

    /**
     * Returns the API name. For example, if a device support WMA version 1.1,
     * this method should return "Wireless Messaging API".
     * 
     * @return the API name
     */
    public String getName();

    /**
     * Returns the API version in OSGi format. For example, if a device support
     * WMA version 1.1, this method should return 1.1.0
     * 
     * @return the version for this API.
     */
    public Version getVersion();

    /**
     * Sets the API identifier. For example, if a device support WMA version
     * 1.1, "WMA" should be use as identifier.
     * 
     * @param identifier the API identifier.
     */
    public void setIdentifier(String identifier);

    /**
     * Sets the API Name. For example, if a device support WMA version 1.1,
     * "Wireless Messaging API" should be used as name.
     * 
     * @param name API name
     */
    public void setName(String name);

    /**
     * Sets the API version.
     * 
     * @param version API Version.
     */
    public void setVersion(Version version);

    /**
     * Returns a string representation of the API. The API will be represented
     * by both the name and version concatenated with a -. For example if the
     * device support WMA version 1.1, the method will return WMA-1.1
     * 
     * @return a string representation of the API.
     */
    public String toString();

}
