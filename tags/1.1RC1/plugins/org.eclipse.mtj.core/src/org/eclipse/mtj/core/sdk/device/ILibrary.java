/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gustavo de Paula (Motorola) - Initial Version
 *     Diego Sandin (Motorola)     - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.sdk.device;

import java.io.File;
import java.net.URL;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.mtj.core.persistence.IPersistable;

/**
 * Represents a library that is associated to a device. The library is directly
 * related to a JAR file and also to a list of {@link IAPI}'s.
 * 
 * @since 1.0
 */
public interface ILibrary extends IPersistable {

    /**
     * Return the APIs associated with this library instance.
     * 
     * @return the list of APIs available in this library or <code>null</code>
     *         if none was specified.
     */
    public List<? extends IAPI> getAPIs();

    /**
     * Set the list of access rules for this library.
     * 
     * @param accessRules the list with all access rules for this library.
     */
    public void setAccessRules(List<IAccessRule> accessRules);

    /**
     * Set the APIs associated with this library instance.
     * 
     * @param api the list of APIs to be associated with this library.
     */
    public void setApis(List<? extends IAPI> apis);

    /**
     * Set the URL containing the path to the javadoc describing this library.
     * 
     * @param javadocURL the URL containing the path to the javadoc describing
     *            this library.
     */
    public void setJavadocURL(URL javadocURL);

    /**
     * Sets the jar file represented by this library.
     * 
     * @param libraryFile the jar file represented by this library.
     */
    public void setLibraryFile(File libraryFile);

    /**
     * Set the path to the source archive or folder associated with this
     * Library.
     * 
     * @param sourceAttachmentPath the path to the source archive or folder
     */
    public void setSourceAttachmentPath(IPath sourceAttachmentPath);

    /**
     * Set the path within the source archive or folder where package fragments
     * are located. An empty path indicates that packages are located at the
     * root of the source archive or folder.
     * 
     * @param sourceAttachmentRootPath the path within the source archive or
     *            folder, or <code>null</code> if not applicable.
     */
    public void setSourceAttachmentRootPath(IPath sourceAttachmentRootPath);

    /**
     * Return the library as an instance of IClasspathEntry.
     * 
     * @return the library as an instance of IClasspathEntry.
     */
    public IClasspathEntry toClasspathEntry();

    /**
     * Return the jar file represented by this library.
     * 
     * @return the jar file represented by this library.
     */
    public File toFile();

    /**
     * Return the library as an instance of java.net.URL.
     * 
     * @return An absolute, hierarchical URL with a scheme equal to "file", a
     *         path representing this library pathname, and undefined authority,
     *         query, and fragment components.
     */
    public URL toURL();

    /**
     * Returns the path to the source archive or folder associated with this
     * library, or <code>null</code> if this classpath entry has no source
     * attachment.
     * 
     * @return the path to the source archive or folder, or <code>null</code> if
     *         none
     */
    public IPath getSourceAttachmentPath();

    /**
     * Returns the path within the source archive or folder where package
     * fragments are located. An empty path indicates that packages are located
     * at the root of the source archive or folder.
     * 
     * @return the path within the source archive or folder, or
     *         <code>null</code> if not applicable
     */
    public IPath getSourceAttachmentRootPath();

    /**
     * Returns the possibly empty list of access rules for this library.
     * 
     * @return the possibly empty list of access rules for this library.
     */
    List<IAccessRule> getAccessRules();
}
