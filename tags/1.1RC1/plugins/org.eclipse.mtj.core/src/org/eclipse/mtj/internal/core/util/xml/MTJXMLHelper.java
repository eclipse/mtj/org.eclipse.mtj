/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core/PDEXMLHelper
 */
package org.eclipse.mtj.internal.core.util.xml;

import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

/**
 * @since 0.9.1
 */
public class MTJXMLHelper {

    protected static DocumentBuilderFactory fDOMFactory;
    protected static List<SoftReference<DocumentBuilder>> fDOMParserQueue;
    protected static int fDOMPoolLimit;
    protected static final int FMAXPOOLLIMIT = 1;
    protected static MTJXMLHelper fPinstance;
    protected static SAXParserFactory fSAXFactory;
    protected static List<SoftReference<SAXParser>> fSAXParserQueue;
    protected static int fSAXPoolLimit;

    /**
     * @return
     */
    public static int getDOMPoolLimit() {
        return fDOMPoolLimit;
    }

    /**
     * @return
     */
    public static int getSAXPoolLimit() {
        return fSAXPoolLimit;
    }

    /**
     * @param source
     * @return
     */
    public static String getWritableAttributeString(String source) {
        // Ensure source is defined
        if (source == null) {
            return ""; //$NON-NLS-1$
        }
        // Trim the leading and trailing whitespace if any
        source = source.trim();
        // Translate source using a buffer
        StringBuffer buffer = new StringBuffer();
        // Translate source character by character
        for (int i = 0; i < source.length(); i++) {
            char character = source.charAt(i);
            switch (character) {
                case '&':
                    buffer.append("&amp;"); //$NON-NLS-1$
                    break;
                case '<':
                    buffer.append("&lt;"); //$NON-NLS-1$
                    break;
                case '>':
                    buffer.append("&gt;"); //$NON-NLS-1$
                    break;
                case '\'':
                    buffer.append("&apos;"); //$NON-NLS-1$
                    break;
                case '\"':
                    buffer.append("&quot;"); //$NON-NLS-1$
                    break;
                case '\r':
                    buffer.append("&#x0D;"); //$NON-NLS-1$
                    break;
                case '\n':
                    buffer.append("&#x0A;"); //$NON-NLS-1$
                    break;
                default:
                    buffer.append(character);
                    break;
            }
        }
        return buffer.toString();
    }

    public static String getWritableString(String source) {
        if (source == null) {
            return ""; //$NON-NLS-1$
        }
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < source.length(); i++) {
            char c = source.charAt(i);
            switch (c) {
                case '&':
                    buf.append("&amp;"); //$NON-NLS-1$
                    break;
                case '<':
                    buf.append("&lt;"); //$NON-NLS-1$
                    break;
                case '>':
                    buf.append("&gt;"); //$NON-NLS-1$
                    break;
                case '\'':
                    buf.append("&apos;"); //$NON-NLS-1$
                    break;
                case '\"':
                    buf.append("&quot;"); //$NON-NLS-1$
                    break;
                default:
                    buf.append(c);
                    break;
            }
        }
        return buf.toString();
    }

    /**
     * @return
     * @throws FactoryConfigurationError
     */
    public static MTJXMLHelper Instance() throws FactoryConfigurationError {
        if (fPinstance == null) {
            fPinstance = new MTJXMLHelper();
        }
        return fPinstance;
    }

    /**
     * @param poolLimit
     */
    public static void setDOMPoolLimit(int poolLimit) {
        fDOMPoolLimit = poolLimit;
    }

    /**
     * @param poolLimit
     */
    public static void setSAXPoolLimit(int poolLimit) {
        fSAXPoolLimit = poolLimit;
    }

    /**
     * Obtain new instances of SAXParserFactory and DocumentBuilderFactory.
     * 
     * @throws FactoryConfigurationError if the implementation is not available
     *             or cannot be instantiated.
     */
    protected MTJXMLHelper() throws FactoryConfigurationError {

        fSAXFactory = SAXParserFactory.newInstance();
        fDOMFactory = DocumentBuilderFactory.newInstance();

        fSAXParserQueue = Collections
                .synchronizedList(new LinkedList<SoftReference<SAXParser>>());

        fDOMParserQueue = Collections
                .synchronizedList(new LinkedList<SoftReference<DocumentBuilder>>());

        fSAXPoolLimit = FMAXPOOLLIMIT;
        fDOMPoolLimit = FMAXPOOLLIMIT;

    }

    /**
     * @return
     * @throws ParserConfigurationException
     */
    public synchronized DocumentBuilder getDefaultDOMParser()
            throws ParserConfigurationException {

        DocumentBuilder parser = null;
        if (fDOMParserQueue.isEmpty()) {
            parser = fDOMFactory.newDocumentBuilder();
        } else {
            SoftReference<DocumentBuilder> reference = fDOMParserQueue
                    .remove(0);
            if (reference.get() != null) {
                parser = reference.get();
            } else {
                parser = fDOMFactory.newDocumentBuilder();
            }
        }
        return parser;
    }

    /**
     * Gets the default instance of a {@link SAXParser}.
     * 
     * @return the {@link SAXParser} default instance.
     * @throws ParserConfigurationException if a parser cannot be created which
     *             satisfies the requested configuration.
     * @throws SAXException for SAX errors.
     */
    public synchronized SAXParser getDefaultSAXParser()
            throws ParserConfigurationException, SAXException {

        SAXParser parser = null;
        if (fSAXParserQueue.isEmpty()) {
            parser = fSAXFactory.newSAXParser();
        } else {
            SoftReference<SAXParser> reference = fSAXParserQueue.remove(0);
            if (reference.get() != null) {
                parser = reference.get();
            } else {
                parser = fSAXFactory.newSAXParser();
            }
        }
        return parser;
    }

    /**
     * @param parser
     */
    public synchronized void recycleDOMParser(DocumentBuilder parser) {
        if (fDOMParserQueue.size() < fDOMPoolLimit) {
            SoftReference<DocumentBuilder> reference = new SoftReference<DocumentBuilder>(
                    parser);
            fDOMParserQueue.add(reference);
        }
    }

    /**
     * @param parser
     */
    public synchronized void recycleSAXParser(SAXParser parser) {
        if (fSAXParserQueue.size() < fSAXPoolLimit) {
            SoftReference<SAXParser> reference = new SoftReference<SAXParser>(
                    parser);
            fSAXParserQueue.add(reference);
        }
    }
}
