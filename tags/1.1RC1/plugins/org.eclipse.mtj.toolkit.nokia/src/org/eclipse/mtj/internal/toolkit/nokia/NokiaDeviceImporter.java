/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.toolkit.nokia;

import java.io.File;
import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.toolkit.uei.UEIDeviceImporter;
import org.eclipse.mtj.internal.toolkit.uei.properties.UEIDeviceDefinition;

/**
 * @author Diego Madruga Sandin
 * @since 1.0
 */
public class NokiaDeviceImporter extends UEIDeviceImporter {

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.toolkit.uei.UEIDeviceImporter#createDevice(java.io.File, org.eclipse.mtj.internal.toolkit.uei.properties.UEIDeviceDefinition, java.util.Properties, java.lang.String)
     */
    @Override
    protected IMIDPDevice createDevice(File emulatorExecutable,
            UEIDeviceDefinition definition, Properties ueiProperties,
            String deviceName) throws CoreException {

        if (ueiProperties.getProperty("org.eclipse.mtj.toolkit.name").equals(
                "S60 3rd Edition FP1 SDK for MIDP")) {

            Properties deviceProperties = filterDeviceProperties(ueiProperties,
                    deviceName);

            NokiaS603rdFP1Device device = new NokiaS603rdFP1Device(
                    deviceName,
                    ueiProperties.getProperty(
                            UEIDeviceImporter.PROP_TOOLKIT_NAME, "Unknown"), //$NON-NLS-1$
                    getDeviceDescription(deviceProperties, deviceName),
                    deviceProperties, definition, emulatorExecutable,
                    getPreverifier(emulatorExecutable));

            return device;
        }

        return null;
    }
}
