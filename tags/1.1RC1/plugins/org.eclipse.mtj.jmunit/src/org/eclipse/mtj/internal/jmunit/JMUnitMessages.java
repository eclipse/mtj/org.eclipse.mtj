/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.jmunit;

import org.eclipse.osgi.util.NLS;

/**
 * JMUnitMessages contains all messages that can be localized.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class JMUnitMessages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.internal.jmunit.messages"; //$NON-NLS-1$

    public static String JMUnitTestFinder_findTestsInContainer_taskName;

    public static String NewJMUnitTestCasePageOne_description;
    public static String NewJMUnitTestCasePageOne_empty_superclass;
    public static String NewJMUnitTestCasePageOne_interface_as_superclass;
    public static String NewJMUnitTestCasePageOne_jmunit_cldc10_not_in_classpath;
    public static String NewJMUnitTestCasePageOne_jmunit_cldc11_not_in_classpath;
    public static String NewJMUnitTestCasePageOne_jmunit10Toggle_label;
    public static String NewJMUnitTestCasePageOne_jmunit11Toggle_label;
    public static String NewJMUnitTestCasePageOne_methodStubs_label;
    public static String NewJMUnitTestCasePageOne_missing_superclass;
    public static String NewJMUnitTestCasePageOne_superclass_not_text_impl;
    public static String NewJMUnitTestCasePageOne_title;
  
    public static String NewJMUnitTestCaseWizard_window_title;

    public static String NewJUnitWizard_op_error_title;
    public static String NewJUnitWizard_op_error_message;

    public static String NewJMUnitTestSuiteCreationWizard_title;

    public static String NewJMUnitTestSuiteWizardPageOne_class_selected;
    public static String NewJMUnitTestSuiteWizardPageOne_classes_selected;
    public static String NewJMUnitTestSuiteWizardPageOne_createClassesInSuiteControl_label;
    public static String NewJMUnitTestSuiteWizardPageOne_description;
    public static String NewJMUnitTestSuiteWizardPageOne_deselect_all;
    public static String NewJMUnitTestSuiteWizardPageOne_jmunit10Toggle_label;
    public static String NewJMUnitTestSuiteWizardPageOne_jmunit11Toggle_label;
    public static String NewJMUnitTestSuiteWizardPageOne_no_test_classes_selected;
    public static String NewJMUnitTestSuiteWizardPageOne_select_all;
    public static String NewJMUnitTestSuiteWizardPageOne_title;


    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, JMUnitMessages.class);
    }

    private JMUnitMessages() {
    }
}
