/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant;

/**
 * this interface will be used to get preprocess directive information
 * 
 * @author gma
 * @since 0.9.1
 */
public interface IPreprocessDirectiveProvider {
    /**
     * @return all preprocess directives that the preprocessor support
     */
    PreprocessDirective[] getAllDirectives();

    /**
     * @param directiveName the directive name
     * @return the preprocess directive for the specific directive name, if the
     *         directive doesn't exist, return null.
     */
    PreprocessDirective getDirective(String directiveName);
}
