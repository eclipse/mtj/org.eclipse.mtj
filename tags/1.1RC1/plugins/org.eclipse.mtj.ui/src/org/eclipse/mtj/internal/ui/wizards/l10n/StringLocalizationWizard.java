/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.ui.wizards.l10n;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IBuffer;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IImportDeclaration;
import org.eclipse.jdt.core.IType;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.internal.core.l10n.L10nApi;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.l10n.L10nEntry;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.osgi.util.NLS;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;

/**
 * StringLocalizationWizard class provides a wizard for
 * string localization.
 * 
 * @author David Marques
 */
public class StringLocalizationWizard extends Wizard {

    private StringLocalizationWizardPage1 page1;

    private ICompilationUnit              target;
    private L10nModel                     model;

    /**
     * Creates an instance of a StringLocalizationWizard to
     * externalize strings from the specified {@link ICompilationUnit}
     * writing the keys into the specified {@link L10nModel}.
     * 
     * @param _class target class.
     * @param _model target model.
     */
    public StringLocalizationWizard(ICompilationUnit _class, L10nModel _model) {
        if (_class == null) {
            throw new IllegalArgumentException(
                    MTJUIMessages.StringLocalizationWizard_classCanNotBeNull);
        }

        if (_model == null) {
            throw new IllegalArgumentException(
                    MTJUIMessages.StringLocalizationWizard_l10nModelCanNotBeNull);
        }

        this.target = _class;
        this.model = _model;
        this.setWindowTitle(MTJUIMessages.StringLocalizationWizard_dialogTitle);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    public boolean performFinish() {
        try {
            if (model.getLocales().getChildCount() == 0x00) {
                return false;
            }

            StringLocalizationData externalized[] = this.page1
                    .getExternalizedStrings();
            StringLocalizationData ignored[] = this.page1.getIgnoredStrings();
            if (externalized.length > 0x00 || ignored.length > 0x00) {
                // Update Localization Data file
                this.updateLocalizationData(model, externalized);

                // Update Class file
                this.updateClassFile(model, externalized, ignored);
                return true;
            }
        } catch (Exception e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
        return false;
    }

    /**
     * Updates the class file content.
     * 
     * @param model l10n model.
     * @param externalized externalized strings.
     * @param ignored ignored strings.
     * @throws CoreException Any core error occurs.
     */
    private void updateClassFile(L10nModel model,
            StringLocalizationData[] externalized,
            StringLocalizationData[] ignored) throws CoreException {
        IImportDeclaration iimport = null;
        String ppackage = null;

        IProgressMonitor monitor = new NullProgressMonitor();
        L10nLocales locales = model.getLocales();

        MultiTextEdit multiEdit = new MultiTextEdit();
        for (StringLocalizationData stringData : externalized) {
            IRegion region = stringData.getRegion();

            String value = NLS.bind("l10n.getString(L10nConstants.keys.{0})", //$NON-NLS-1$
                    stringData.getKey().toUpperCase());
            ReplaceEdit edit = new ReplaceEdit(region.getOffset(), region
                    .getLength(), value);
            multiEdit.addChild(edit);
        }

        for (StringLocalizationData stringData : ignored) {
            IRegion region = stringData.getRegion();

            String value = NLS.bind("\"{0}\"{1}", new String[] { //$NON-NLS-1$
                    stringData.getValue(), IStringExternalizer.NON_NLS });
            ReplaceEdit edit = new ReplaceEdit(region.getOffset(), region
                    .getLength(), value);
            multiEdit.addChild(edit);
        }
        this.target.applyTextEdit(multiEdit, monitor);

        ppackage = locales.getPackage();
        if (ppackage.length() > 0x00) {
            ppackage = ppackage + "."; //$NON-NLS-1$
        }
        iimport = this.target.getImport(NLS.bind("{0}L10nResources", ppackage)); //$NON-NLS-1$
        if (!iimport.exists()) {
            this.target.createImport(NLS.bind("{0}L10nResources", ppackage), //$NON-NLS-1$
                    null, monitor);
        }

        iimport = this.target.getImport(NLS.bind("{0}L10nConstants", ppackage)); //$NON-NLS-1$
        if (!iimport.exists()) {
            this.target.createImport(NLS.bind("{0}L10nConstants", ppackage), //$NON-NLS-1$
                    null, monitor);
        }

        IType type = this.target.findPrimaryType();
        IField field = type.getField("l10n"); //$NON-NLS-1$
        if (!field.exists()) {
            type
                    .createField(
                            "L10nResources l10n = L10nResources.getL10nResources(null);", //$NON-NLS-1$
                            null, true, monitor);
        }
        this.target.save(monitor, true);
    }

    /**
     * Update the Localization Data file content.
     * 
     * @param model l10n model.
     * @param stringsData externalized strings.
     * @throws CoreException Any core error occurs.
     */
    private void updateLocalizationData(L10nModel model,
            StringLocalizationData[] stringsData) throws CoreException {
        L10nLocales locales = model.getLocales();
        IDocumentElementNode children[] = locales.getChildNodes();
        for (IDocumentElementNode child : children) {
            L10nLocale locale = (L10nLocale) child;
            for (StringLocalizationData data : stringsData) {
                L10nEntry entry = locale.getEntry(data.getKey());
                if (entry == null) {
                    entry = new L10nEntry(model);
                    entry.setKey(data.getKey());
                    locale.addChild(entry);
                }
                entry.setValue(data.getValue());
            }
        }
        model.save();
        L10nApi.syncronizeApi(model);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#addPages()
     */
    public void addPages() {
        try {
            String name = this.target.getElementName();
            IBuffer buffer = this.target.getBuffer();
            page1 = new StringLocalizationWizardPage1(name, buffer);
            this.addPage(page1);
        } catch (CoreException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }

}
