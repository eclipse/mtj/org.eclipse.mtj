/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Fernando Rocha (Motorola) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.wizards.dialogfields;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

/**
 * Dialog field containing a label, combo control and a button control.
 * 
 * @author Fernando Rocha
 * @since 1.0
 */
public class ComboButtonDialogField extends ComboDialogField {

    protected static GridData gridDataForButton(Button button, int span) {
        GridData gd = new GridData();
        gd.horizontalAlignment = GridData.FILL;
        gd.grabExcessHorizontalSpace = false;
        gd.horizontalSpan = span;
        return gd;
    }

    private IComboButtonAdapter fComboButtonAdapter;

    private Button fSelectButton;
    private String fSelectButtonLabel;
    private boolean fButtonEnabled;

    public ComboButtonDialogField(IComboButtonAdapter adapter, int comboStyle) {
        super(comboStyle);
        fComboButtonAdapter = adapter;
        fSelectButtonLabel = "!Select...!"; //$NON-NLS-1$
        fButtonEnabled = true;
    }

    // ------ adapter communication

    /**
     * Programmatical pressing of the button
     */
    public void changeControlPressed() {
        fComboButtonAdapter.changeControlPressed(this);
    }

    // ------- layout helpers

    /*
     * @see DialogField#doFillIntoGrid
     */
    @Override
    public Control[] doFillIntoGrid(Composite parent, int nColumns) {
        assertEnoughColumns(nColumns);

        Label label = getLabelControl(parent);
        label.setLayoutData(gridDataForLabel(1));
        Combo combo = getComboControl(parent);
        combo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL)); // gridDataForCombo(nColumns
                                                                     // - 2));
        Button button = getChangeControl(parent);
        button.setLayoutData(gridDataForButton(button, 1));

        return new Control[] { label, combo, button };
    }

    /**
     * Sets the enable state of the button.
     */
    public void enableButton(boolean enable) {
        if (isOkToUse(fSelectButton)) {
            fSelectButton.setEnabled(isEnabled() && enable);
        }
        fButtonEnabled = enable;
    }

    /**
     * Creates or returns the created button widget.
     * 
     * @param parent The parent composite or <code>null</code> if the widget has
     *            already been created.
     */
    public Button getChangeControl(Composite parent) {
        if (fSelectButton == null) {
            assertCompositeNotNull(parent);

            fSelectButton = new Button(parent, SWT.PUSH);
            fSelectButton.setFont(parent.getFont());
            fSelectButton.setText(fSelectButtonLabel);
            fSelectButton.setEnabled(isEnabled() && fButtonEnabled);
            fSelectButton.addSelectionListener(new SelectionListener() {
                public void widgetDefaultSelected(SelectionEvent e) {
                    changeControlPressed();
                }

                public void widgetSelected(SelectionEvent e) {
                    changeControlPressed();
                }
            });

        }
        return fSelectButton;
    }

    /**
     * Sets the label of the button.
     */
    public void setButtonLabel(String label) {
        fSelectButtonLabel = label;
    }

    @Override
    public int getNumberOfControls() {
        return 3;
    }

    @Override
    protected void updateEnableState() {
        super.updateEnableState();
        if (isOkToUse(fSelectButton)) {
            fSelectButton.setEnabled(isEnabled() && fButtonEnabled);
        }
    }

}
