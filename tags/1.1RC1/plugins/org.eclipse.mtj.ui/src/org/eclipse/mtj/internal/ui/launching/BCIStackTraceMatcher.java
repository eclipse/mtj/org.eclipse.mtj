/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Eric Dias (Motorola) - Initial Version.
 */
package org.eclipse.mtj.internal.ui.launching;

import org.eclipse.mtj.internal.core.launching.StackTraceParser;
import org.eclipse.ui.console.IPatternMatchListener;

/**
 * BCIStackTraceMatcher class finds matches on consoles 
 * for stack traces. Every time it matches a stack trace
 * it parses the stack into a lined stack.
 * 
 * @author Eric Dias
 */
public class BCIStackTraceMatcher extends StackTraceMatcher implements IPatternMatchListener{

    /**
     * Creates a BCIStackTraceMatcher instance associated to
     * the specified line tracker.
     * 
     * @param _tracker MTJConsoleLineTracker instance.
     * @param stackTraceParser 
     */
    public BCIStackTraceMatcher(MTJConsoleLineTracker _tracker, StackTraceParser _parser) {
        super(_tracker, _parser);
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.ui.console.IPatternMatchListener#getPattern()
     */
    public String getPattern() {
        return "(\\s[-]\\s.+[,]\\s(bci=)\\d+"+System.getProperty("line.separator")+")+"; //$NON-NLS-2$
    }

}
