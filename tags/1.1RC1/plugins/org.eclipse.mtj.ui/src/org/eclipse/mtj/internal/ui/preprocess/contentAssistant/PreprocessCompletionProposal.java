/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension2;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension4;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

/**
 * @author gma
 * @since 0.9.1
 */
public class PreprocessCompletionProposal implements ICompletionProposal,
        ICompletionProposalExtension2, ICompletionProposalExtension4 {
    PreprocessContext ppContext;
    Image image;
    IPreprocessContentAssistModel model;

    private String fDisplayString;

    public PreprocessCompletionProposal(IPreprocessContentAssistModel model,
            PreprocessContext ppContext, Image image) {
        this.model = model;
        this.ppContext = ppContext;
        this.image = image;

    }

    public void apply(IDocument document) {

    }

    public void apply(ITextViewer viewer, char trigger, int stateMask,
            int offset) {
        int replaceOffset = getReplacementOffset();
        int length = offset - replaceOffset;
        String replacementString = getReplacementString();
        try {
            viewer.getDocument().replace(replaceOffset, length,
                    replacementString);
        } catch (BadLocationException e) {

        }

    }

    public String getAdditionalProposalInfo() {
        return getJavaDoc();
    }

    public IContextInformation getContextInformation() {

        return null;
    }

    public String getDisplayString() {
        if (fDisplayString == null) {
            fDisplayString = model.getName();
            if (model.getDescription() != null) {
                String[] arguments = new String[] { model.getName(),
                        model.getDescription() };
                fDisplayString = PreprocessContentAssistMessages
                        .getFormattedString(
                                PreprocessContentAssistMessages.PreprocessProposal_displayString,
                                arguments);
            }
        }
        return fDisplayString;
    }

    public Image getImage() {

        return image;
    }

    protected String getJavaDoc() {
        return model.getJavaDoc();

    }

    public int getReplacementOffset() {
        return ppContext.getCompletionOffset();
    }

    public String getReplacementString() {
        return model.getName();
    }

    public Point getSelection(IDocument document) {
        return new Point(getReplacementOffset()
                + getReplacementString().length(), 0);
    }

    public boolean isAutoInsertable() {

        return false;
    }

    public void selected(ITextViewer viewer, boolean smartToggle) {

    }

    public void unselected(ITextViewer viewer) {

    }

    public boolean validate(IDocument document, int offset, DocumentEvent event) {
        boolean valid = false;
        try {
            String content = document.get(getReplacementOffset(), offset
                    - getReplacementOffset());
            if (getReplacementString().toLowerCase().startsWith(
                    content.toLowerCase())) {
                valid = true;
            }

            // if(valid)fReplacementLength = content.length();
        } catch (BadLocationException e) {
            // ignore concurrently modified document
        }

        return valid;
    }

}
