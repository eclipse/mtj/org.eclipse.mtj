/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.internal.core.sdk.device.AbstractMIDPDevice;
import org.eclipse.mtj.ui.editors.device.DeviceEditorDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.PlatformUI;

/**
 * <p>
 * An action delegate implementation that opens the default device editor
 * implementation.
 * </p>
 * 
 * @since 1.0
 */
public class DefaultDeviceEditorActionDelegate implements IActionDelegate {

    private AbstractMIDPDevice device;

    /* (non-Javadoc)
     * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
     */
    public void run(IAction action) {
        Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
                .getShell();

        DeviceEditorDialog dialog = getDeviceEditorDialog(shell);
        dialog.setDevice(device);
        dialog.open();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
     */
    public void selectionChanged(IAction action, ISelection selection) {
        device = null;

        if (selection instanceof IStructuredSelection) {
            IStructuredSelection ss = (IStructuredSelection) selection;
            if (ss.size() == 1) {
                Object obj = ss.getFirstElement();
                if (obj instanceof AbstractMIDPDevice) {
                    device = (AbstractMIDPDevice) obj;
                }
            }
        }

        action.setEnabled(device != null);
    }

    /**
     * Return the dialog to be used for editing the dialog.
     * 
     * @param shell
     * @return
     */
    protected DeviceEditorDialog getDeviceEditorDialog(Shell shell) {
        return new DeviceEditorDialog(shell);
    }
}
