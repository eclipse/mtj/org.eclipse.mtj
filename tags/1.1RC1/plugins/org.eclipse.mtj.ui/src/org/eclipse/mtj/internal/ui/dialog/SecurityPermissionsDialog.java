/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.ui.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.internal.core.sign.PermissionsGroup;
import org.eclipse.mtj.internal.core.sign.PermissionsGroupsRegistry;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.CheckedTreeSelectionDialog;

/**
 * SecurityPermissionsDialog class wraps the building process
 * for the permissions dialog. It encapsulates both label and
 * content providers.
 * 
 * @author David Marques
 * @since 1.0
 */
public class SecurityPermissionsDialog {

	/**
	 * Creates a security permissions dialog.
	 * 
	 * @param _shell parent shell.
	 * @return the dialog's instance.
	 */
	public static CheckedTreeSelectionDialog createDialog(Shell _shell) {
		SecurityPermissionsDialog instance = new SecurityPermissionsDialog();

		CheckedTreeSelectionDialog dialog = new CheckedTreeSelectionDialog(
				_shell, instance.new SecurityPermissionsDialogLabelProvider(),
				instance.new SecurityPermissionsDialogContentProvider());
		dialog.setTitle(MTJUIMessages.SecurityPermissionsDialog_title);
		dialog.setMessage(MTJUIMessages.SecurityPermissionsDialog_message);
		dialog.setContainerMode(true);
		dialog.setInput(PermissionsGroupsRegistry.getInstance());
		return dialog;
	}

	/**
	 * Private constructor.
	 */
	private SecurityPermissionsDialog() {
	}

	private class SecurityPermissionsDialogLabelProvider extends LabelProvider {

		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		public String getText(Object element) {
			String text = null;
			if (element instanceof PermissionsGroup) {
				text = ((PermissionsGroup) element).getClassName();
			} else if (element instanceof PermissionNode) {
				text = ((PermissionNode) element).getPermission();
			}
			return text;
		}
		
		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
		 */
		public Image getImage(Object element) {
			Image image = null;
			if (element instanceof PermissionsGroup) {
				image = MTJUIPluginImages.DESC_CLASS_OBJ.createImage();
			} else if (element instanceof PermissionNode) {
				image = MTJUIPluginImages.DESC_PERMISSION_OBJ.createImage();
			}
			return image;
		}
	}

	private class SecurityPermissionsDialogContentProvider implements
			ITreeContentProvider {

		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
		 */
		public Object[] getChildren(Object parentElement) {
			if (parentElement instanceof PermissionsGroup) {
				PermissionsGroup permissionsGroup = (PermissionsGroup) parentElement;
				List<PermissionNode> children = new ArrayList<PermissionNode>();
				for (String permission : permissionsGroup.getPermissions()) {
					children.add(new PermissionNode(permission,
							permissionsGroup));
				}
				return children.toArray();
			}
			return null;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
		 */
		public Object getParent(Object element) {
			if (element instanceof PermissionNode) {
				return ((PermissionNode) element).getParent();
			}
			return null;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
		 */
		public boolean hasChildren(Object element) {
			if (element instanceof PermissionsGroup) {
				return true;
			}
			return false;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		public Object[] getElements(Object inputElement) {
			if (inputElement instanceof PermissionsGroupsRegistry) {
				PermissionsGroupsRegistry registry = (PermissionsGroupsRegistry) inputElement;
				return registry.getPermissions().toArray();
			}
			return null;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
		 */
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		public void dispose() {
		}
	}

	/**
	 * PermissionNode class wraps a permission String in
	 * order to keep track of the parent PermissionsGroup
	 * since it is required to be used by an ITreeContentProvider
	 * instance.
	 */
	public class PermissionNode {

		private PermissionsGroup parent;
		private String permission;

		public PermissionNode(String _permission, PermissionsGroup _parent) {
			this.permission = _permission;
			this.parent = _parent;
		}

		/**
		 * @return the parent
		 */
		public PermissionsGroup getParent() {
			return parent;
		}

		/**
		 * @return the permission
		 */
		public String getPermission() {
			return permission;
		}
	}

}
