/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n.details;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.internal.core.IModelChangedEvent;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.editor.MTJDetails;
import org.eclipse.mtj.internal.ui.editor.MTJFormPage;
import org.eclipse.mtj.internal.ui.editors.FormLayoutFactory;
import org.eclipse.mtj.internal.ui.editors.l10n.LocalesTreeSection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.IFormPart;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

/**
 * 
 */
public abstract class L10nAbstractDetails extends MTJDetails {

    private static final int NUM_COLUMNS = 3;

    private String fContextID;

    private Section fMainSection;

    private LocalesTreeSection fMasterSection;

    /**
     * @param masterSection
     * @param contextID
     */
    public L10nAbstractDetails(LocalesTreeSection masterSection,
            String contextID) {
        fMasterSection = masterSection;
        fContextID = contextID;
        fMainSection = null;
    }

    /**
     * @param parent
     */
    public void createContents(Composite parent) {
        configureParentLayout(parent);
        createDetails(parent);
        hookListeners();
    }

    /**
     * @param parent
     */
    public void createDetails(Composite parent) { // Create the main section
        int style = ExpandableComposite.TITLE_BAR;

        if (getDetailsDescription() != null) {
            style |= Section.DESCRIPTION;
        }

        fMainSection = getPage().createUISection(parent, getDetailsTitle(),
                getDetailsDescription(), style);
        // Align the master and details section headers (misalignment caused
        // by section toolbar icons)
        getPage().alignSectionHeaders(getMasterSection().getSection(),
                fMainSection);
        // Create the container for the main section
        Composite sectionClient = getPage().createUISectionContainer(
                fMainSection, NUM_COLUMNS);
        GridData data = new GridData(GridData.FILL_BOTH);
        fMainSection.setLayoutData(data);
        createFields(sectionClient);

        // Bind widgets
        getManagedForm().getToolkit().paintBordersFor(sectionClient);
        fMainSection.setClient(sectionClient);
        markDetailsPart(fMainSection);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.IContextPart#fireSaveNeeded()
     */
    public void fireSaveNeeded() {
        markDirty();
        getPage().getMTJEditor().fireSaveNeeded(getContextId(), false);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.IContextPart#getContextId()
     */
    public String getContextId() {
        return fContextID;
    }

    /**
     * @return
     */
    public LocalesTreeSection getMasterSection() {
        return fMasterSection;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.IContextPart#getPage()
     */
    public MTJFormPage getPage() {
        return (MTJFormPage) getManagedForm().getContainer();
    }

    /**
     * @return
     */
    public FormToolkit getToolkit() {
        return getManagedForm().getToolkit();
    }

    /**
     * 
     */
    public abstract void hookListeners();

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.IContextPart#isEditable()
     */
    public boolean isEditable() {
        return fMasterSection.isEditable();
    }

    /**
     * @return
     */
    public boolean isEditableElement() {
        return fMasterSection.isEditable();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangedListener#modelChanged(org.eclipse.mtj.core.model.IModelChangedEvent)
     */
    public void modelChanged(IModelChangedEvent event) {
        // NO-OP
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.IPartSelectionListener#selectionChanged(org.eclipse.ui.forms.IFormPart, org.eclipse.jface.viewers.ISelection)
     */
    public void selectionChanged(IFormPart part, ISelection selection) {
        // NO-OP
        // Children to override
    }

    /**
     * 
     */
    public abstract void updateFields();

    /**
     * @param parent
     */
    private void configureParentLayout(Composite parent) {
        parent.setLayout(FormLayoutFactory.createDetailsGridLayout(false, 1));
    }

    /**
     * @param parent
     */
    protected abstract void createFields(Composite parent);

    /**
     * @param client
     * @param toolkit
     * @param text
     */
    protected void createLabel(Composite client, FormToolkit toolkit,
            String text) {
        Label label = toolkit.createLabel(client, text, SWT.WRAP);
        GridData gd = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gd.horizontalSpan = NUM_COLUMNS;
        label.setLayoutData(gd);
    }

    /**
     * @param parent
     */
    protected void createSpace(Composite parent) {
        createLabel(parent, getManagedForm().getToolkit(), Utils.EMPTY_STRING);
    }

    /**
     * @return
     */
    protected abstract String getDetailsDescription();

    /**
     * @return
     */
    protected abstract String getDetailsTitle();

    /**
     * @param selection
     * @return
     */
    protected Object getFirstSelectedObject(ISelection selection) {
        // Get the structured selection (obtained from the master tree viewer)
        IStructuredSelection structuredSel = ((IStructuredSelection) selection);
        // Ensure we have a selection
        if (structuredSel == null) {
            return null;
        }
        return structuredSel.getFirstElement();
    }

}
