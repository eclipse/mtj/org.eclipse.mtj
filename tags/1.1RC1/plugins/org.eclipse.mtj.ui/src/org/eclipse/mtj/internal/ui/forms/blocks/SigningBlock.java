/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Only refresh viewer if new input is valid.
 *     David Marques (Motorola) - Implementing keystore management.
 *     David Marques (Motorola) - Clearing text field values upon disable.
 *     David Marques (Motorola) - Adding support for certificates.
 *     David Aragao  (Motorola) - Signing buttons availability.
 */
package org.eclipse.mtj.internal.ui.forms.blocks;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.internal.core.sign.IKeyStoreManager;
import org.eclipse.mtj.internal.core.sign.KeyStoreEntry;
import org.eclipse.mtj.internal.core.sign.KeyStoreManagerException;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.dialog.KeystoreEntryDialog;
import org.eclipse.mtj.internal.ui.dialog.NewKeyPairDialog;
import org.eclipse.mtj.internal.ui.dialog.SigningPasswordDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;

/**
 * SigningBlock class provides a block that is common to
 * keystore management UIs.
 * <br>
 * It provides a list for private keys aliases and allows 
 * keystore management.
 * 
 *  @author David Marques
 *  @since 1.0
 */
public class SigningBlock {
	
	public static enum PasswordType {Prompt, Project, Workspace}
	
	private TableViewer viewer;
	private Table table;
	private Button aliasGroupBtn1;
	private Button aliasGroupBtn2;
	private Button aliasGroupBtn3;
	private Button aliasGroupBtn4;
	private Button aliasGroupBtn5;
	private Text keyDataText;
	private Text providerText;
	private Text ksTypeText;
	
	/**
	 * Creates the signing block.
	 * 
	 * @param parent parent composite.
	 */
	public void createBlock(Composite parent) {
		this.createKeyAliasSection(parent);
		this.createAdvancedSection(parent);
	}
	
	/**
	 * Sets the block input IKeyStoreManager instance.
	 * 
	 * @param _keyStoreManager input object.
	 */
	public void setInput(IKeyStoreManager _keyStoreManager) {
		this.viewer.setInput(_keyStoreManager);
		this.updateButtonsAvailability();
		this.keyDataText.setText(Utils.EMPTY_STRING);
	}
	
	/**
	 * Enables/Disables all block widgets.
	 * 
	 * @param enabled true to enable false to disable.
	 */
	public void setEnabled(boolean enabled) {
		this.providerText.setEnabled(enabled);
		this.keyDataText.setEnabled(enabled);
		this.ksTypeText.setEnabled(enabled);
		this.table.setEnabled(enabled);
				
		updateButtonsAvailability();
	}

	/**
	 * Clears all block fields.
	 */
	public void clearFields() {
		this.providerText.setText(Utils.EMPTY_STRING);
		this.keyDataText.setText(Utils.EMPTY_STRING);
		this.ksTypeText.setText(Utils.EMPTY_STRING);
	}
	
	/**
	 * Enables/Disables the block keystore buttons.
	 * 
	 * @param enabled true to enable false to disable.
	 */
	private void updateButtonsAvailability() {
		if (this.viewer.getInput() != null) {
			this.aliasGroupBtn1.setEnabled(true);
			this.aliasGroupBtn4.setEnabled(true);
			if (this.viewer.getSelection().isEmpty()) {
				this.aliasGroupBtn2.setEnabled(false);
				this.aliasGroupBtn3.setEnabled(false);
				this.aliasGroupBtn5.setEnabled(false);
			} else {
				this.aliasGroupBtn2.setEnabled(true);
				this.aliasGroupBtn3.setEnabled(true);
				this.aliasGroupBtn5.setEnabled(true);
			}
		} else {
			this.aliasGroupBtn1.setEnabled(false);
			this.aliasGroupBtn2.setEnabled(false);
			this.aliasGroupBtn3.setEnabled(false);
			this.aliasGroupBtn4.setEnabled(false);
			this.aliasGroupBtn5.setEnabled(false);
		}
	}

	/**
	 * Creates the Alias list section.
	 * 
	 * @param parent parent composite.
	 */
	private void createKeyAliasSection(final Composite parent) {
		Group aliasGroup = new Group(parent, SWT.NONE);
		aliasGroup.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(0x02, false);
		layout.verticalSpacing = 0x00;
        aliasGroup.setLayout(layout);
		aliasGroup.setText(MTJUIMessages.SigningBlock_keyAliases);
		
		table = new Table(aliasGroup, SWT.SINGLE | SWT.BORDER);
		GridData gridData = MTJUIPlugin.buildGridData(SWT.FILL, SWT.FILL, true, true);
		gridData.verticalSpan = 0x05;
		table.setLayoutData(gridData);
		
		viewer = new TableViewer(table);
		viewer.setContentProvider(new SigningBlockContentProvider());
		viewer.setLabelProvider(new SigningBlockLabelProvider());
		viewer.addSelectionChangedListener(new ISelectionChangedListener(){
			public void selectionChanged(SelectionChangedEvent event) {
				ISelection selection = event.getSelection();
				updateButtonsAvailability();
				if (!(selection instanceof IStructuredSelection) || selection.isEmpty()) {
					return;
				}
				Object selectedObj = ((IStructuredSelection) selection).getFirstElement();
				if (!(selectedObj instanceof KeyStoreEntry)) {
					return;
				}
				
				KeyStoreEntry entry = (KeyStoreEntry) selectedObj;
				displayKeyData(entry);
			}
		});
		aliasGroupBtn1 = new Button(aliasGroup, SWT.NONE);
		aliasGroupBtn1.setText(MTJUIMessages.SigningBlock_createNewKeypair);
		aliasGroupBtn1.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL, SWT.CENTER, false, false));
		aliasGroupBtn1.addMouseListener(new MouseListener(){
			public void mouseUp(MouseEvent e) {
				NewKeyPairDialog dialog = new NewKeyPairDialog(parent.getShell());
				dialog.setTitle(MTJUIMessages.SigningBlock_generateKeyPair);
				if (dialog.open() != Dialog.OK) {
					return;
				}
				
				Object input = viewer.getInput();
				if (!(input instanceof IKeyStoreManager)) {
					return;
				}
				
				IKeyStoreManager keyStoreManager = (IKeyStoreManager) input;
				try {					
					keyStoreManager.generateKeyPair(dialog.getKeyPairInfo());
					viewer.refresh();
				} catch (KeyStoreManagerException exc) {
					MessageDialog.openError(parent.getShell(), MTJUIMessages.SigningBlock_keystoreManagerError, exc.getMessage());
					MTJLogger.log(IStatus.ERROR, exc.getMessage());
				}
			}
			
			public void mouseDoubleClick(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
			}
		});
		
		aliasGroupBtn2 = new Button(aliasGroup, SWT.NONE);
		aliasGroupBtn2.setText(MTJUIMessages.SigningBlock_deleteKeyPair);
		aliasGroupBtn2.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL, SWT.CENTER, false, false));
		aliasGroupBtn2.addMouseListener(new MouseListener(){
			public void mouseUp(MouseEvent e) {
				Object input = viewer.getInput();
				if (!(input instanceof IKeyStoreManager)) {
					return;
				}
				
				ISelection selection = viewer.getSelection();
				if (selection.isEmpty() || !(selection instanceof IStructuredSelection)) {
					return;
				}
				
				IKeyStoreManager keyStoreManager = (IKeyStoreManager) input;
				try {					
					keyStoreManager.deleteEntry((KeyStoreEntry) ((IStructuredSelection)selection).getFirstElement());
					keyDataText.setText(Utils.EMPTY_STRING);
					viewer.refresh();
				} catch (KeyStoreManagerException exc) {
					MessageDialog.openError(parent.getShell(), MTJUIMessages.SigningBlock_keystoreManagerError, exc.getMessage());
					MTJLogger.log(IStatus.ERROR, exc.getMessage());
				}
			}
			
			public void mouseDoubleClick(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
			}
		});
		
		aliasGroupBtn3 = new Button(aliasGroup, SWT.NONE);
		aliasGroupBtn3.setText(MTJUIMessages.SigningBlock_generateCSR);
		aliasGroupBtn3.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL, SWT.CENTER, false, false));
		aliasGroupBtn3.addMouseListener(new MouseListener(){
			public void mouseUp(MouseEvent e) {
				Object input = viewer.getInput();
				if (!(input instanceof IKeyStoreManager)) {
					return;
				}
				
				ISelection selection = viewer.getSelection();
				if (selection.isEmpty() || !(selection instanceof IStructuredSelection)) {
					return;
				}
				
				DirectoryDialog folderDialog = new DirectoryDialog(parent.getShell(), SWT.SAVE);
				folderDialog.setMessage(MTJUIMessages.SigningBlock_selectCSRFolder);

				String path = folderDialog.open();
				if (path == null) {
					return;
				}
				IKeyStoreManager keyStoreManager = (IKeyStoreManager) input;
				try {
					String alias = (String) ((IStructuredSelection)selection).getFirstElement();
					
					SigningPasswordDialog dialog = new SigningPasswordDialog(parent.getShell(), false);
					dialog.setTitle(MTJUIMessages.SigningBlock_enterKeyPassword);
					dialog.setDescription(NLS.bind(MTJUIMessages.SigningBlock_enterKeyPasswordForAlias, alias));
					if (dialog.open() != Dialog.OK) {
						return;
					}
					path = NLS.bind("{0}{1}{2}.csr", new String[]{path, File.separator, alias});  //$NON-NLS-1$
					keyStoreManager.generateCSR(alias, dialog.getPassword(), path);
				} catch (KeyStoreManagerException exc) {
					MessageDialog.openError(parent.getShell(), MTJUIMessages.SigningBlock_keystoreManagerError, exc.getMessage());
					MTJLogger.log(IStatus.ERROR, exc.getMessage());
				}
			}
			
			public void mouseDoubleClick(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
			}
		});
		
		aliasGroupBtn4 = new Button(aliasGroup, SWT.NONE);
		aliasGroupBtn4.setText(MTJUIMessages.SigningBlock_importCertificate);
		aliasGroupBtn4.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL, SWT.CENTER, false, false));
		aliasGroupBtn4.addMouseListener(new MouseListener(){
			public void mouseUp(MouseEvent e) {
				Object input = viewer.getInput();
				if (!(input instanceof IKeyStoreManager)) {
					return;
				}
				
				FileDialog fileDialog = new FileDialog(parent.getShell(), SWT.OPEN);
				fileDialog.setFilterExtensions(new String[]{"*.*"}); //$NON-NLS-1$
				fileDialog.setText(MTJUIMessages.SigningBlock_importCertificate);

				String path = fileDialog.open();
				if (path == null) {
					return;
				}
				
				IKeyStoreManager keyStoreManager = (IKeyStoreManager) input;
				try {
					KeystoreEntryDialog dialog = new KeystoreEntryDialog(parent.getShell());
					dialog.setTitle(MTJUIMessages.SigningBlock_selectCertificateFile);
					if (dialog.open() != Dialog.OK) {
						return;
					}
					
					keyStoreManager.importCertificate(dialog.getAlias(), dialog.getPassword(), path);
					viewer.refresh();
				} catch (KeyStoreManagerException exc) {
					MessageDialog.openError(parent.getShell(), MTJUIMessages.SigningBlock_keystoreManagerError, exc.getMessage());
					MTJLogger.log(IStatus.ERROR, exc.getMessage());
				}
			}
			
			public void mouseDoubleClick(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
			}
		});
		
		aliasGroupBtn5 = new Button(aliasGroup, SWT.NONE);
		aliasGroupBtn5.setText(MTJUIMessages.SigningBlock_importCSRResponse);
		aliasGroupBtn5.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL, SWT.CENTER, false, false));
		aliasGroupBtn5.addMouseListener(new MouseListener(){
			public void mouseUp(MouseEvent e) {
				Object input = viewer.getInput();
				if (!(input instanceof IKeyStoreManager)) {
					return;
				}
				
				String alias = null;
				ISelection selection = viewer.getSelection();
				if (!selection.isEmpty() && (selection instanceof IStructuredSelection)) {
					alias = (String) ((IStructuredSelection)selection).getFirstElement();
				}
				
				FileDialog fileDialog = new FileDialog(parent.getShell(), SWT.OPEN);
				fileDialog.setFilterExtensions(new String[]{"*.*"}); //$NON-NLS-1$
				fileDialog.setText(MTJUIMessages.SigningBlock_importCertificateReply);

				String path = fileDialog.open();
				if (path == null) {
					return;
				}
				
				IKeyStoreManager keyStoreManager = (IKeyStoreManager) input;
				try {
					KeystoreEntryDialog dialog = new KeystoreEntryDialog(parent.getShell(), alias);
					dialog.setTitle(MTJUIMessages.SigningBlock_selectCertificateFile);
					if (dialog.open() != Dialog.OK) {
						return;
					}
					alias = dialog.getAlias();
					keyStoreManager.importCertificate(alias, dialog.getPassword(), path);
				} catch (KeyStoreManagerException exc) {
					MessageDialog.openError(parent.getShell(), MTJUIMessages.SigningBlock_keystoreManagerError, exc.getMessage());
					MTJLogger.log(IStatus.ERROR, exc.getMessage());
				}
			}
			
			public void mouseDoubleClick(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
			}
		});
		
		Label keyDataLabel = new Label(aliasGroup, SWT.NONE);
		keyDataLabel.setText(MTJUIMessages.SigningBlock_keyData);
		gridData = MTJUIPlugin.buildGridData(SWT.FILL, SWT.CENTER, true, false);
		gridData.horizontalSpan = 2;
		keyDataLabel.setLayoutData(gridData);
		
		keyDataText = new Text(aliasGroup, SWT.READ_ONLY | SWT.MULTI | SWT.WRAP | SWT.BORDER);
		gridData = MTJUIPlugin.buildGridData(SWT.FILL, SWT.CENTER, true, false);
		gridData.horizontalSpan = 2;
		gridData.heightHint = 50;
		keyDataText.setLayoutData(gridData);
		this.updateButtonsAvailability();
	}
	
	/**
	 * Displays the certificate user information.
	 * 
	 * @param keyStoreEntry source certificate entry.
	 */
	protected void displayKeyData(KeyStoreEntry keyStoreEntry) {
		Object input = this.viewer.getInput();
		if (input == null || !(input instanceof IKeyStoreManager)) {
			return;
		}
		
		IKeyStoreManager keyStoreManager = (IKeyStoreManager) input;
		String 			 data            = null;
		try {
			data = keyStoreManager.getCertificateInformation(keyStoreEntry);
			if (data == null) {			
				data = MTJUIMessages.SigningBlock_infoNotAvailable;
			}
		} catch (KeyStoreManagerException e) {
			data = MTJUIMessages.SigningBlock_unableToReadCertInfo;
			MTJLogger.log(IStatus.ERROR, e);
		}
		this.keyDataText.setText(data);
	}

	/**
	 * Creates the Advanced keystore section.
	 * 
	 * @param parent parent composite.
	 */
	private void createAdvancedSection(Composite parent) {
		Group advancedGroup = new Group(parent, SWT.NONE);
		advancedGroup.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL, SWT.FILL, true, false));
        advancedGroup.setLayout(new GridLayout(0x01, false));
		advancedGroup.setText(MTJUIMessages.SigningBlock_advancedSettings);
		
		Label descriptionLabel = new Label(advancedGroup, SWT.NONE);
		descriptionLabel.setText(MTJUIMessages.SigningBlock_useJavaSystemDefaults);
		descriptionLabel.setLayoutData(MTJUIPlugin.buildGridData(true, false));
		
		Label providerLabel = new Label(advancedGroup, SWT.NONE);
		providerLabel.setText(MTJUIMessages.SigningBlock_cryptoProvider);
		providerLabel.setLayoutData(MTJUIPlugin.buildGridData(true, false));
		
		providerText = new Text(advancedGroup, SWT.BORDER);
		providerText.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL, SWT.CENTER, true, false));
		
		Label ksTypeLabel = new Label(advancedGroup, SWT.NONE);
		ksTypeLabel.setText(MTJUIMessages.SigningBlock_ksType);
		ksTypeLabel.setLayoutData(MTJUIPlugin.buildGridData(true, false));
		
		ksTypeText = new Text(advancedGroup, SWT.BORDER);
		ksTypeText.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL, SWT.CENTER, true, false));
	}
	
	private class SigningBlockContentProvider implements IStructuredContentProvider {

		private Viewer viewer;
		
		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		public Object[] getElements(Object inputElement) {
			List<Object> elements = new ArrayList<Object>();
			if (inputElement instanceof IKeyStoreManager) {
				try {					
					elements.addAll(((IKeyStoreManager)inputElement).getEntries());
				} catch (KeyStoreManagerException e) {
					String message = NLS.bind(MTJUIMessages.SigningBlock_unableToGetKsAliases, e.getMessage());
					MessageDialog.openError(this.viewer.getControl().getShell(), MTJUIMessages.SigningBlock_keystoreManagerError, message);
					MTJLogger.log(IStatus.ERROR, e);
				}
			}
			return elements.toArray();
		}

		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		public void dispose() {
		}

		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
		 */
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			this.viewer = viewer;
		}
	}
	
	private class SigningBlockLabelProvider extends LabelProvider {
		
		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		public String getText(Object element) {
			String text = null;
			if (element instanceof KeyStoreEntry) {
				KeyStoreEntry entry = (KeyStoreEntry) element;
				text = entry.getAlias();
			}
			return text;
		}
	}

	/**
	 * Gets the current selected alias name.
	 * 
	 * @return alias name.
	 */
	public String getCurrentAlias() {
		String alias = null;
		
		ISelection selection = this.viewer.getSelection();
		if ((selection instanceof IStructuredSelection) && !selection.isEmpty()) {			
			Object selectedObj = ((IStructuredSelection) selection).getFirstElement();
			if (selectedObj instanceof String) {
				alias = (String) selectedObj;
			}
		}
		return alias;
	}

	/**
	 * Sets the current selected alias.
	 * 
	 * @param keyAlias alias name.
	 */
	public void setCurrentAlias(String keyAlias) {
		this.viewer.setSelection(new StructuredSelection(keyAlias));
	}
	
	
	/**
	 * Gets the keystore provider.
	 * 
	 * @return provider name.
	 */
	public String getProvider() {
		String result = this.providerText.getText();
		if (result.length() == 0x00) {
			result = null;
		}
		return result;
	}
	
	/**
	 * Sets the keystore provider text field value.
	 * 
	 * @param type provider value.
	 */
	public void setProvider(String provider) {
		if (provider != null) {			
			this.providerText.setText(provider);
		} else {
			this.providerText.setText(Utils.EMPTY_STRING);
		}
	}
	
	/**
	 * Gets the keystore type.
	 * 
	 * @return keystore type name.
	 */
	public String getKeystoreType() {
		String result = this.ksTypeText.getText();
		if (result.length() == 0x00) {
			result = null;
		}
		return result;
	}
	
	/**
	 * Sets the keystore type text field value.
	 * 
	 * @param type type value.
	 */
	public void setKeystoreType(String type) {
		if (type != null) {			
			this.ksTypeText.setText(type);
		} else {
			this.ksTypeText.setText(Utils.EMPTY_STRING);
		}
	}
}
