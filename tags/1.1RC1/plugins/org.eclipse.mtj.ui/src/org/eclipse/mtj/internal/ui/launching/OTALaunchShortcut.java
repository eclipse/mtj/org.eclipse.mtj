/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques, David Arag�o (Motorola) - Initial Verison.
 */


package org.eclipse.mtj.internal.ui.launching;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.launching.ILaunchConstants;
import org.eclipse.mtj.internal.core.launching.midp.IMIDPLaunchConstants;
import org.eclipse.mtj.internal.core.launching.midp.LaunchingUtils;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;

public class OTALaunchShortcut extends NonJavaLaunchShortcut {

	private IFile jadFile;

	/**
     * Get all ILaunchConfiguration meet these conditions: 1. Project Name
     * matching. 2. doJadLaunch==true. 3. jadUrl contains the jadFile name.
     * 
     * @param jadFile
     * @return
     */
    private List<ILaunchConfiguration> getCandidateConfigs(IFile jadFile) {
        ILaunchConfigurationType configType = getEmulatorConfigType();
		List<ILaunchConfiguration> candidateConfigs = Collections.emptyList();
		try {
			ILaunchConfiguration[] configs = DebugPlugin.getDefault()
					.getLaunchManager().getLaunchConfigurations(configType);
			candidateConfigs = new ArrayList<ILaunchConfiguration>(
					configs.length);
			for (ILaunchConfiguration config : configs) {
				boolean doJadLaunch = config.getAttribute(
						IMIDPLaunchConstants.DO_OTA, false);
				String projectName = config
						.getAttribute(
								IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
								"");
				// If doJadLaunch && project name is matching && jadUrl contains
				// JAD name, then the launch config is a candidate.
				if (doJadLaunch
						&& projectName.equals(jadFile.getProject().getName())) {
					candidateConfigs.add(config);
				}
			}
		} catch (CoreException e) {
			MTJLogger.log(IStatus.WARNING, "getCandidateConfigs", e);
		}
		
		this.jadFile = jadFile;
        return candidateConfigs;
    }

	/**
     * Get the launch configuration type for wireless toolkit emulator.
     * 
     * @return
     */
    private ILaunchConfigurationType getEmulatorConfigType() {
        ILaunchManager lm = DebugPlugin.getDefault().getLaunchManager();
        ILaunchConfigurationType configType = lm
                .getLaunchConfigurationType(ILaunchConstants.LAUNCH_CONFIG_TYPE);
        return configType;
    }
	
	 /**
     * @param selectedFile
     * @return
     */
    private IJavaProject getJavaProject(IFile selectedFile) {
        IProject project = selectedFile.getProject();
        IJavaProject javaProject = JavaCore.create(project);
        return javaProject;
    }
	
    /**
     * Return the JAD file location for launching. The launching JAD file is at
     * /%DEPLOY_FOLDER%/launchFromJAD/. In which DEPLOY_FOLDER is project's
     * deployment folder.
     * 
     * @param jadFile
     * @return
     */
    private String getLaunchingJadFileLocation(IFile jadFile) {
        IMTJProject suite = getMidletSuiteProject(jadFile);
        IPath launchBasePath = LaunchingUtils.getJadLaunchBasePath(suite);
        IPath launchingJadPath = launchBasePath.append(jadFile.getName());
        String launchingJadFileLocation = launchingJadPath.toPortableString();
        return launchingJadFileLocation;
    }
    
    /**
     * Return the IMidletSuiteProject which contains the JAD file.
     * 
     * @param selectedFile
     * @return
     */
    private IMidletSuiteProject getMidletSuiteProject(IFile selectedFile) {
        IJavaProject javaProject = getJavaProject(selectedFile);
        IMidletSuiteProject midletSuiteProject = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);
        return midletSuiteProject;
    }

    /**
     * @param suite
     * @return
     */
    private IFile getRuntimeJadFile(IMidletSuiteProject suite) {
        IFolder emulationFolder = LaunchingUtils.getEmulationFolder(suite);
        IFile runtimeJadFile = emulationFolder.getFile(suite.getJadFileName());
        return runtimeJadFile;
    }
	
    /**
     * Determine if the JAD file is the project's JAD file. Because the project
     * may contains some other JAD file which is not the project's JAD file.
     * 
     * @param receiver
     * @return - If the selected JAD file name equals the project JAD file name,
     *         return true.
     */
    private boolean isProjectJad(IFile receiver) {
        IMidletSuiteProject midletProject = getMidletSuiteProject(receiver);
        String jadFileName = midletProject.getJadFileName();
        if (!jadFileName.equals(receiver.getName())) {
            return false;
        }
        return true;
    }
    
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.ui.launching.NonJavaLaunchShortcut#createConfiguration()
	 */
	@Override
	protected ILaunchConfiguration createConfiguration() {
		ILaunchConfiguration config = null;
        try {
            ILaunchConfigurationType configType = getEmulatorConfigType();

            String launchConfigName = DebugPlugin.getDefault()
                    .getLaunchManager()
                    .generateUniqueLaunchConfigurationNameFrom(
                            jadFile.getName());
            ILaunchConfigurationWorkingCopy wc = configType.newInstance(null,
                    launchConfigName);
            wc.setAttribute(IMIDPLaunchConstants.DO_JAD_LAUNCH, false);
            String jadFileURL = getLaunchingJadFileLocation(jadFile);
            wc.setAttribute(IMIDPLaunchConstants.SPECIFIED_JAD_URL, jadFileURL);
            wc.setAttribute(
                    IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
                    jadFile.getProject().getName());
            wc.setAttribute(IMIDPLaunchConstants.DO_OTA, true);

            DebugUITools.setLaunchPerspective(configType,
                    ILaunchManager.RUN_MODE,
                    IDebugUIConstants.PERSPECTIVE_DEFAULT);
            DebugUITools.setLaunchPerspective(configType,
                    ILaunchManager.DEBUG_MODE,
                    IDebugUIConstants.PERSPECTIVE_DEFAULT);

            config = wc.doSave();

        } catch (CoreException ce) {
            MTJLogger.log(IStatus.WARNING, "createConfiguration", ce);
        }

        return config;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.ui.launching.NonJavaLaunchShortcut#getLaunchConfigurations(org.eclipse.ui.IEditorPart)
	 */
	@Override
	protected List<ILaunchConfiguration> getLaunchConfigurations(
			IEditorPart editor) {
		IEditorInput input = editor.getEditorInput();
		IFile jadFile = (IFile) input.getAdapter(IFile.class);
		if (jadFile == null) {
			return null;
		}
		
		if (!isProjectJad(jadFile)) {
			IMidletSuiteProject suite = getMidletSuiteProject(jadFile);
            jadFile = getRuntimeJadFile(suite);
        } 
		return getCandidateConfigs(jadFile);
	}
	
    /* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.ui.launching.NonJavaLaunchShortcut#getLaunchConfigurations(org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	protected List<ILaunchConfiguration> getLaunchConfigurations(ISelection selection) {
		IFile jadFile = null;
		Object selected = ((IStructuredSelection) selection).getFirstElement();
        if (selected instanceof IFile) {
        	jadFile = (IFile) selected;
        	if (!isProjectJad(jadFile)) {
    			IMidletSuiteProject suite = getMidletSuiteProject(jadFile);
                jadFile = getRuntimeJadFile(suite);
            } 
        } else if (selected instanceof IProject) {
            IJavaProject javaProject = JavaCore.create((IProject) selected);
            IMidletSuiteProject suite = MidletSuiteFactory.getMidletSuiteProject(javaProject);
            jadFile = getRuntimeJadFile(suite);
        } else if (selected instanceof IJavaProject) {
            IMidletSuiteProject suite = MidletSuiteFactory.getMidletSuiteProject((IJavaProject) selected);
            jadFile = getRuntimeJadFile(suite);
        }
        
        if (jadFile == null) {
			return null;
		}
        return getCandidateConfigs(jadFile);
	}
}
