/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.toolkit.uei.internal;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.osgi.framework.BundleContext;

/**
 * The main plug-in class to be used in the desktop.
 */
public class UeiPlugin extends Plugin {
    /**
     * A debug flag used for debugging the UEI device import process. Debug is
     * enabled by specifying the System property
     * "org.eclipse.mtj.toolkit.uei.debug" as "true".
     */
    public static final boolean DEBUG = System.getProperty(
            "org.eclipse.mtj.toolkit.uei.debug", "false").equalsIgnoreCase(
            "true");

    /**
     * Log the specified message for debug, if debugging is enabled.
     * 
     * @param message
     */
    public static void debugLog(String message) {
        if (DEBUG) {
            MTJCorePlugin.log(IStatus.INFO, "[UEI] " + message);
        }
    }

    // The shared instance.
    private static UeiPlugin plugin;

    /**
     * The constructor.
     */
    public UeiPlugin() {
        plugin = this;
    }

    /**
     * This method is called upon plug-in activation
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);
        debugLog("Starting UEI plugin");
    }

    /**
     * This method is called when the plug-in is stopped
     */
    public void stop(BundleContext context) throws Exception {
        super.stop(context);
        plugin = null;
    }

    /**
     * Returns the shared instance.
     */
    public static UeiPlugin getDefault() {
        return plugin;
    }

}
