/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.toolkit.uei;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.ColonDelimitedProperties;
import org.eclipse.mtj.core.internal.utils.Utils;

/**
 * A helper class that provides access to the properties of a UEI emulator.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */
public class UEIPropertiesReader {

    /**
     * Singleton instance of this cache
     */
    public static final UEIPropertiesReader instance = new UEIPropertiesReader();

    private static final Pattern VALID_VERSION_PATTERN = Pattern.compile(
            "^Profile:\\s.*|^Configuration:\\s.*", Pattern.MULTILINE);

    // Parameter used to query UEI properties
    private static final String PARM_XQUERY = "-Xquery";

    /**
     * Private constructor for singleton access
     */
    private UEIPropertiesReader() {
        super();
    }

    /**
     * Return the UEI properties associated with the specified emulator.
     * 
     * @param emulatorExecutable the file system file that represents the
     *                emulator executable.
     * 
     * @return the properties that define this UEI emulator.
     * @throws CoreException
     */
    public Properties getUEIProperties(File emulatorExecutable)
            throws CoreException {
        Properties properties = null;

        try {
            properties = getUEIPropertiesFromEmulator(emulatorExecutable);
        } catch (IOException e) {
            MTJCorePlugin.throwCoreException(IStatus.ERROR, -9999, e);
        }

        return properties;
    }

    /**
     * Return the toolkit name from the specified executable.
     * 
     * @param executable
     * @return
     * @throws IOException
     */
    private String getToolkitName(String versionInformation) throws IOException {
        String toolkitName = null;

        // Parse the output
        BufferedReader reader = new BufferedReader(new StringReader(
                versionInformation));
        toolkitName = reader.readLine();
        if (toolkitName == null)
            toolkitName = "Generic UEI Toolkit";

        return toolkitName;
    }

    /**
     * Return the UEI emulator properties as specified by the emulator.
     * 
     * @return the UEI properties
     * @throws CoreException
     * @throws IOException
     */
    private Properties getUEIPropertiesFromEmulator(File emulatorExecutable)
            throws CoreException, IOException {
        Properties ueiProperties = null;

        // Make sure the emulator actually exists before trying to run it...
        if (Utils.executableExists(emulatorExecutable)) {
            // Siemens emulators use some funky behavior to distinguish
            // a valid UEI emulator from a non-valid one. Look to see
            // if the output uses the Profile: and/or Configuration: information
            // before assuming it is a valid UEI toolkit.
            String versionString = getUEIVersionInformation(emulatorExecutable);
            Matcher matcher = VALID_VERSION_PATTERN.matcher(versionString);
            if (matcher.find()) {
                // Grab the standard UEI properties from the emulator
                String output = getStandardOutput("UEI Emulator Query",
                        emulatorExecutable, new String[] { PARM_XQUERY });
                ueiProperties = parseUEIPropertiesString(output);

                // Add in a non-standard property to hold the name
                String toolkitName = getToolkitName(versionString);
                if ((toolkitName != null) && (toolkitName.length() > 0)) {
                    ueiProperties.setProperty(
                            UEIDeviceImporter.PROP_TOOLKIT_NAME, toolkitName);
                }
            }
        } else {
            ueiProperties = new Properties();
        }

        return ueiProperties;
    }

    /**
     * Return the output from the standard output, accounting for annoying
     * device emulators that must be run from within the console or they will
     * outright fail.
     * 
     * @param name
     * @param executable
     * @param arguments
     * @return
     * @throws CoreException
     */
    private String getStandardOutput(String name, File executable,
            String[] arguments) throws CoreException {
        boolean isWin32 = Platform.getOS().equals(Platform.OS_WIN32);

        // Capture and parse the output
        File workingDirectory = executable.getParentFile();
        String exeName = isWin32 ? executable.getName() : executable
                .getAbsolutePath();

        ArrayList list = new ArrayList(Arrays.asList(arguments));
        list.add(0, exeName);

        if (isWin32) {
            list.add(0, "/c");
            list.add(0, "cmd");
        }

        String[] commandLine = (String[]) list.toArray(new String[list.size()]);

        return Utils.getStandardOutput("UEI Emulator Version", commandLine,
                workingDirectory);
    }

    /**
     * Call the emulator with the "-version" parameter and return the results.
     * 
     * @param executable
     * @return
     * @throws CoreException
     */
    private String getUEIVersionInformation(File executable)
            throws CoreException {
        return getStandardOutput("UEI Emulator Version", executable,
                new String[] { "-version" });
    }

    /**
     * Parse the properties String containing the UEI properties.
     * 
     * @param contents the string containing the properties
     * @return the newly parsed properties object
     * 
     * @throws IOException
     */
    private Properties parseUEIPropertiesString(String contents)
            throws IOException {
        ColonDelimitedProperties props = new ColonDelimitedProperties();

        StringReader reader = new StringReader(contents);
        try {
            props.load(reader);
        } catch (Exception e) {
            MTJCorePlugin.log(IStatus.WARNING, "Error parsing UEI properties",
                    e);
            MTJCorePlugin.log(IStatus.WARNING, contents);
        }

        return props;
    }
}
