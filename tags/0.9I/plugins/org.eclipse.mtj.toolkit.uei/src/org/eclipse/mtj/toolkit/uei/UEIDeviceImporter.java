/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and removed setPredeploymentRequired
 *                                invocation
 *     Gang Ma (Sybase)         - Added javadoc detector
 */
package org.eclipse.mtj.toolkit.uei;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.importer.IDeviceImporter;
import org.eclipse.mtj.core.importer.JavadocDetector;
import org.eclipse.mtj.core.importer.LibraryImporter;
import org.eclipse.mtj.core.importer.JavadocDetector.GenericLocalFSSearch;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.Classpath;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.preverifier.StandardPreverifierFactory;
import org.eclipse.mtj.core.model.preverifier.impl.StandardPreverifier;
import org.eclipse.mtj.toolkit.uei.internal.UeiPlugin;
import org.osgi.framework.Bundle;

/**
 * A device importer implementation that imports device information from a UEI
 * device emulator.
 * 
 * @author Craig Setera
 */
public class UEIDeviceImporter implements IDeviceImporter {

	/**
	 * The property defining the UEI classpath
	 */
	public static final String PROP_CLASSPATH = "bootclasspath";

	/**
	 * UEI Description Property
	 */
	public static final String PROP_DESCRIPTION = "description";

	/**
	 * The property defining the devices provided by this emulator
	 */
	public static final String PROP_DEVICE_LIST = "device.list";

	/**
	 * The property defining the known UEI arguments
	 */
	public static final String PROP_KNOWN_ARGUMENTS = "uei.arguments";

	/**
	 * UEI Security Domains Property
	 */
	public static final String PROP_SECURITY_DOMAINS = "security.domains";

	/**
	 * A non-standard property name in the device properties to hold the
	 * emulator version
	 */
	public static final String PROP_TOOLKIT_NAME = "org.eclipse.mtj.toolkit.name";

	// Holder class for the information the defines the way
	// that a UEI emulator will be launched.
	private class UEIDeviceDefinition {
		private String name;
		private boolean debugServer;
		private boolean predeployRequired;
		private String launchTemplate;
		private Pattern matchPattern;

		/**
		 * @return the isDebugServer
		 */
		public boolean isDebugServer() {
			return debugServer;
		}

		/**
		 * @param isDebugServer the isDebugServer to set
		 */
		public void setDebugServer(boolean isDebugServer) {
			this.debugServer = isDebugServer;
		}

		/**
		 * @return the launchTemplate
		 */
		public String getLaunchTemplate() {
			return launchTemplate;
		}

		/**
		 * @param launchTemplate the launchTemplate to set
		 */
		public void setLaunchTemplate(String launchTemplate) {
			this.launchTemplate = launchTemplate;
		}

		/**
		 * @return the matchPattern
		 */
		public Pattern getMatchPattern() {
			return matchPattern;
		}

		/**
		 * @param matchPattern the matchPattern to set
		 */
		public void setMatchPattern(Pattern matchPattern) {
			this.matchPattern = matchPattern;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @return the predeployRequired
		 */
		public boolean isPredeployRequired() {
			return predeployRequired;
		}

		/**
		 * @param predeployRequired the predeployRequired to set
		 */
		public void setPredeployRequired(boolean predeployRequired) {
			this.predeployRequired = predeployRequired;
		}

	}

	// The properties file holding our implementation information
	static public final String PROPS_FILE = "uei_device.properties";

	// The list of UEI device invocations read from the properties file
	// This list holds instances of Properties
	private ArrayList deviceDefinitions;

	/**
	 * @see org.eclipse.mtj.core.importer.IDeviceImporter#getMatchingDevices(java.io.File,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	public IDevice[] getMatchingDevices(File directory, IProgressMonitor monitor) {
		IDevice[] devices = null;

		if (UeiPlugin.DEBUG) {
			UeiPlugin.debugLog("> getMatchingDevices for " + directory);
		}

		File emulator = getEmulatorInDirectory(directory);
		if (emulator != null) {
			if (UeiPlugin.DEBUG) {
				UeiPlugin.debugLog("- getMatchingDevices emulator =  "
						+ emulator);
			}

			try {
				Properties ueiProperties = getUEIEmulatorProperties(emulator);
				if (isValidUEIProperties(ueiProperties)) {
					UeiPlugin
							.debugLog("- getMatchingDevices UEI properties are valid");

					UEIDeviceDefinition definition = getDeviceDefinition(ueiProperties);
					if (UeiPlugin.DEBUG) {
						UeiPlugin
								.debugLog("- getMatchingDevices UEI Device definition = "
										+ definition);
					}

					if (definition != null) {
						ArrayList deviceList = new ArrayList();
						addUEIDevices(deviceList, emulator, definition,
								ueiProperties);
						devices = (IDevice[]) deviceList
								.toArray(new IDevice[deviceList.size()]);
					}
				}
			} catch (CoreException e) {
				MTJCorePlugin.log(IStatus.ERROR,
						"Error retrieving UEI importer extensions", e);
			} catch (IOException e) {
				MTJCorePlugin.log(IStatus.ERROR,
						"Error retrieving UEI importer extensions", e);
			}
		}

		if (UeiPlugin.DEBUG) {
			UeiPlugin.debugLog("< getMatchingDevices for " + directory);
		}

		return devices;
	}

	/**
	 * Add a new device definition based on the specified information.
	 * 
	 * @param defs
	 * @param defProps
	 * @param category
	 */
	private UEIDeviceDefinition getNewDeviceDefinition(ArrayList defs,
			Properties defProps, String category) {
		UEIDeviceDefinition def = null;

		String matchExpression = defProps.getProperty(category
				+ ".match.expression");
		if (matchExpression != null) {
			def = new UEIDeviceDefinition();

			// The calculated fields
			Pattern matchPattern = Pattern.compile(matchExpression);
			boolean debugServer = defProps.getProperty(
					category + ".debug.server", "").equalsIgnoreCase("true");
			boolean predeployRequired = defProps.getProperty(
					category + ".predeploy.required", "").equalsIgnoreCase(
					"true");

			def.setDebugServer(debugServer);
			def.setName(category);
			def.setMatchPattern(matchPattern);
			def.setLaunchTemplate(defProps.getProperty(category
					+ ".launch.template", ""));
			def.setPredeployRequired(predeployRequired);
		}

		return def;
	}

	/**
	 * Return an array of all of the devices found for the specified emulator
	 * with the specified UEI properties.
	 * 
	 * @param devices
	 * @param emulator
	 * @param definition
	 * @param ueiProperties
	 * @return
	 * @throws CoreException
	 */
	private void addUEIDevices(ArrayList devices, File emulator,
			UEIDeviceDefinition definition, Properties ueiProperties)
			throws CoreException {
		UeiPlugin.debugLog("> addUEIDevices");

		String devicesProp = ueiProperties.getProperty(PROP_DEVICE_LIST);
		if (UeiPlugin.DEBUG) {
			UeiPlugin.debugLog("- addUEIDevices devices = " + devicesProp);
		}

		if (devicesProp != null) {
			StringTokenizer st = new StringTokenizer(devicesProp, ",");
			while (st.hasMoreTokens()) {
				String deviceName = st.nextToken().trim();
				IDevice device = createDevice(emulator, definition,
						ueiProperties, deviceName);
				devices.add(device);
			}
		}

		UeiPlugin.debugLog("< addUEIDevices");
	}

	/**
	 * Return the list of device definitions to be used in attempting UEI device
	 * definition matching.
	 * 
	 * @return
	 * @throws IOException
	 */
	private ArrayList getDeviceDefinitions() throws IOException {
		if (deviceDefinitions == null) {
			deviceDefinitions = readDeviceDefinitions();
		}

		return deviceDefinitions;
	}

	/**
	 * @param deviceProperties
	 * @param deviceName
	 * @return
	 */
	private String getDeviceDescription(Properties deviceProperties,
			String deviceName) {
		return deviceProperties.getProperty(PROP_DESCRIPTION, deviceName)
				.trim();
	}

	/**
	 * @param ueiProperties
	 * @return
	 */
	private String getDeviceGroupName(Properties ueiProperties) {
		return ueiProperties.getProperty(UEIDeviceImporter.PROP_TOOLKIT_NAME,
				"Unknown");
	}

	/**
	 * Return the emulator in the specified directory or <code>null</code> if
	 * no emulator is found.
	 */
	private File getEmulatorInDirectory(File directory) {
		File[] files = directory.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				return pathname.getName().equalsIgnoreCase("emulator.exe")
						|| pathname.getName().equals("emulator");
			}
		});

		File emulator = null;

		if ((files != null) && (files.length > 0)) {
			emulator = files[0];
		}

		return emulator;
	}

	/**
	 * @param emulatorExecutable
	 * @return
	 * @throws CoreException
	 */
	private StandardPreverifier getPreverifier(File emulatorExecutable)
			throws CoreException {
		File preverifierExecutable = new File(emulatorExecutable
				.getParentFile(), "preverify");
		StandardPreverifier preverifier = StandardPreverifierFactory
				.createPreverifier(preverifierExecutable);
		return preverifier;
	}

	/**
	 * Return the protection domains specified for this device.
	 * 
	 * @param deviceProperties
	 * @return
	 */
	private String[] getProtectionDomains(Properties deviceProperties) {
		String domainsString = deviceProperties.getProperty(
				PROP_SECURITY_DOMAINS, "");
		return domainsString.split(",");
	}

	/**
	 * Return the UEI emulator properties.
	 * 
	 * @param emulator the emulator executable to query
	 * @return the UEI properties.
	 * @throws CoreException
	 */
	private Properties getUEIEmulatorProperties(File emulator)
			throws CoreException {
		return UEIPropertiesReader.instance.getUEIProperties(emulator);
	}

	/**
	 * @throws IOException
	 * @see org.eclipse.mtj.toolkit.uei.AbstractUEIDeviceImporterExtension#getUEIDevicesPropertiesStream()
	 */
	private InputStream getUEIDevicesPropertiesStream() throws IOException {
		Bundle bundle = UeiPlugin.getDefault().getBundle();
		URL propsFileURL = bundle.getEntry(PROPS_FILE);

		return propsFileURL.openStream();
	}

	/**
	 * Create and return a new IDevice to match the provided information.
	 * 
	 * @param emulatorExecutable
	 * @param definition
	 * @param ueiProperties
	 * @param deviceName
	 * 
	 * @throws CoreException
	 */
	private IDevice createDevice(File emulatorExecutable,
			UEIDeviceDefinition definition, Properties ueiProperties,
			String deviceName) throws CoreException {
		if (UeiPlugin.DEBUG) {
			UeiPlugin.debugLog("Creating device definition = " + deviceName);
		}

		Properties deviceProperties = filterDeviceProperties(ueiProperties,
				deviceName);

		UEIDevice device = new UEIDevice();
		device.setBundle(UeiPlugin.getDefault().getBundle().getSymbolicName());
		device.setClasspath(getDeviceClasspath(deviceProperties));
		device.setDebugServer(definition.isDebugServer());
		device
				.setDescription(getDeviceDescription(deviceProperties,
						deviceName));
		device.setDeviceProperties(deviceProperties);
		device.setExecutable(emulatorExecutable);
		device.setGroupName(getDeviceGroupName(ueiProperties));
		device.setName(deviceName);
		device.setPreverifier(getPreverifier(emulatorExecutable));
		device.setProtectionDomains(getProtectionDomains(deviceProperties));
		device.setLaunchCommandTemplate(definition.getLaunchTemplate());

		return device;
	}

	/**
	 * Filter out the device-specified properties from the complete list of the
	 * emulator's UEI properties.
	 * 
	 * @param ueiProperties
	 */
	private Properties filterDeviceProperties(Properties ueiProperties,
			String deviceName) {
		Properties deviceProperties = new Properties();

		String prefix = deviceName + ".";
		int substringStart = prefix.length();

		Iterator keys = ueiProperties.keySet().iterator();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			if (key.startsWith(prefix)) {
				String propName = key.substring(substringStart);
				String value = ueiProperties.getProperty(key);
				deviceProperties.setProperty(propName, value);
			}
		}

		return deviceProperties;
	}

	/**
	 * Import the device's classpath and return it.
	 * 
	 * @param deviceProperties
	 * @return
	 */
	private Classpath getDeviceClasspath(Properties deviceProperties) {
		Classpath classpath = new Classpath();
		String classpathString = deviceProperties.getProperty(PROP_CLASSPATH,
				"");
		String[] classpathEntries = classpathString.split(",");

		//create a new javadoc detector which will search the 
		//javadoc of the library automatically
		JavadocDetector javadocDetector = new JavadocDetector()
				.addJavadocSearchStrategy(new GenericLocalFSSearch());
		LibraryImporter libraryImporter = new LibraryImporter();
		libraryImporter.setJavadocDetector(javadocDetector);

		for (int i = 0; i < classpathEntries.length; i++) {
			File libraryFile = new File(classpathEntries[i]);
			if (libraryFile.exists()) {
				classpath.addEntry(libraryImporter
						.createLibraryFor(libraryFile));
			}
		}

		return classpath;
	}

	/**
	 * Return the device definition associated with the specified UEI
	 * properties.
	 * 
	 * @param ueiProperties
	 * @return
	 * @throws IOException
	 */
	private UEIDeviceDefinition getDeviceDefinition(Properties ueiProperties)
			throws IOException {
		UEIDeviceDefinition definition = null;

		String toolkitName = ueiProperties.getProperty(
				UEIDeviceImporter.PROP_TOOLKIT_NAME, "");

		Iterator defs = getDeviceDefinitions().iterator();
		while (defs.hasNext() && (definition == null)) {
			UEIDeviceDefinition def = (UEIDeviceDefinition) defs.next();
			Matcher matcher = def.getMatchPattern().matcher(toolkitName);
			if (matcher.find()) {
				definition = def;
			}
		}

		return definition;
	}

	/**
	 * Return a boolean indicating whether the specified properties appears to
	 * be valid UEI properties.
	 * 
	 * @param ueiProperties
	 * @return
	 */
	private boolean isValidUEIProperties(Properties ueiProperties) {
		return (ueiProperties != null)
				&& ueiProperties.containsKey(PROP_DEVICE_LIST);
	}

	/**
	 * Read in the device definitions from the properties file.
	 * 
	 * @return
	 * @throws IOException
	 */
	private ArrayList readDeviceDefinitions() throws IOException {
		ArrayList defs = new ArrayList();

		Properties defProps = new Properties();
		InputStream is = getUEIDevicesPropertiesStream();
		if (is != null) {
			try {
				defProps.load(is);
			} finally {
				try {
					is.close();
				} catch (IOException e) {
				}
			}

			String devCatList = defProps.getProperty("devices", "");
			String[] deviceCategories = devCatList.split(",");
			for (int i = 0; i < deviceCategories.length; i++) {
				defs.add(getNewDeviceDefinition(defs, defProps,
						deviceCategories[i]));
			}
		}

		return defs;
	}
}
