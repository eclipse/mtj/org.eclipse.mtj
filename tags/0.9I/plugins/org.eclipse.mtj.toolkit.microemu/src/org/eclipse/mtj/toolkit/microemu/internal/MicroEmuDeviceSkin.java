/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 */
package org.eclipse.mtj.toolkit.microemu.internal;

/**
 * This class represents the skins avaliable from MicroEmulator
 * 
 * @author Diego Madruga Sandin
 */
public final class MicroEmuDeviceSkin {

    /**
     * The device skin jar file
     */
    private final String jarFile;

    /**
     * The device skin.
     */
    private final String path;

    /**
     * The device skin.
     */
    private final String name;

    /**
     * Creates a new MicroEmulator Device Skin.
     * 
     * @param jarFile the jar file where the skin can be found
     * @param path the path to the skin
     * @param name the skin name
     */
    public MicroEmuDeviceSkin(String jarFile, String path, String name) {

        this.jarFile = jarFile;
        this.path = path;
        this.name = name;
    }

    /**
     * Return the path to the jar file where the skin can be found.
     * 
     * @return the path to the jar file
     */
    public String getJarFile() {
        return jarFile;
    }

    /**
     * Return the path of the device skin in the jar file
     * 
     * @return the path to the skin
     */
    public String getPath() {
        return path;
    }

    /**
     * Return the skin name
     * 
     * @return the skin name
     */
    public String getName() {
        return name;
    }

}
