/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Feng Wang (Sybase) - Modify to fit for AbstractDevice#copyForLaunch(...)
 *                          signature changing.
 */
package org.eclipse.mtj.toolkit.mpowerplayer;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.launching.ILaunchConstants;
import org.eclipse.mtj.core.model.LaunchEnvironment;
import org.eclipse.mtj.core.model.ReplaceableParametersProcessor;
import org.eclipse.mtj.core.model.device.impl.JavaEmulatorDevice;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * Device implementation for the MPowerPlayer SDK device.
 * 
 * @author Craig Setera
 */
public class MpowerplayerDevice extends JavaEmulatorDevice {
    private File mppRoot;

    /**
     * @see org.eclipse.mtj.core.model.device.IDevice#getLaunchCommand(org.eclipse.mtj.core.model.LaunchEnvironment,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException {
        IMidletSuiteProject midletSuite = launchEnvironment.getMidletSuite();
		ILaunchConfiguration launchConfiguration = launchEnvironment
				.getLaunchConfiguration();
		boolean launchFromJAD = shouldDirectLaunchJAD(launchConfiguration);

        File tempDeployed = copyForLaunch(midletSuite, monitor, launchFromJAD);

        // Fill in our properties so we can use them for resolution
        // against the raw executable command line
        Map<String, Object> executionProperties = new HashMap<String, Object>();
        executionProperties.put("executable", getJavaExecutable());
        executionProperties.put("mpproot", mppRoot);

        // Debug information
        if (launchEnvironment.isDebugLaunch()) {
            executionProperties.put("debugPort", new Integer(launchEnvironment
                    .getDebugListenerPort()));
        }

        // Add launch configuration values

        String extraArguments = launchConfiguration.getAttribute(
                ILaunchConstants.LAUNCH_PARAMS, "");
        executionProperties.put("userSpecifiedArguments", extraArguments);

        if (launchFromJAD) {
            executionProperties.put("jadfile",
                    getSpecifiedJadURL(launchConfiguration));
        } else {
            File jadFile = getJadForLaunch(midletSuite, tempDeployed, monitor);
            if (jadFile.exists()) {
                executionProperties.put("jadfile", jadFile.toString());
            }
        }

        // Do the property resolution given the previous information
        return ReplaceableParametersProcessor.processReplaceableValues(
                launchCommandTemplate, executionProperties);
    }

    /**
     * @return Returns the mppRoot.
     */
    public File getMppRoot() {
        return mppRoot;
    }

    /**
     * @see org.eclipse.mtj.core.model.device.impl.AbstractDevice#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.loadUsing(persistenceProvider);

        String rootString = persistenceProvider.loadString("mppRoot");
        mppRoot = new File(rootString);
    }

    /**
     * @param mppRoot The mppRoot to set.
     */
    public void setMppRoot(File mppRoot) {
        this.mppRoot = mppRoot;
    }

    /**
     * @see org.eclipse.mtj.core.model.device.impl.AbstractDevice#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.storeUsing(persistenceProvider);
        persistenceProvider.storeString("mppRoot", mppRoot.toString());
    }
}
