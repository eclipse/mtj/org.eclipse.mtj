<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
	<title>MTJ - JAD Editor</title>
	<link rel="stylesheet" href="docs.css" type="text/css">
</head>
<body>
<p class="PageTitle">Java Application Descriptor Editor</p>
<p>
This document describes MTJ's built-in Java Application Descriptor
(JAD) editor.
</p>
<ol>
	<li class="TOC"><a class="TOC" href="#overview">Overview</a></li>
	<li class="TOC"><a class="TOC" href="#required">Required tab</a></li>
	<li class="TOC"><a class="TOC" href="#midlets">Midlets tab</a></li>
	<li class="TOC"><a class="TOC" href="#optional">Optional tab</a></li>
	<li class="TOC"><a class="TOC" href="#overtheair">Over The Air tab</a></li>
	<li class="TOC"><a class="TOC" href="#userdefined">User Defined tab</a></li>
</ol>
<hr>
<h1 id="overview">Overview</h1>
<p>
Java ME distributions consist of a JAR file containing the software, and
a JAD file containing information for the Java ME container describing the
contents of the source file.
</p>
<p>
MTJ has a built-in JAD editor that handles the details of formatting
the JAD file for you.  Using the JAD editor, you can make all the required
entries to allow your MIDlet to be properly supported.  The information
that makes up the JAD file is spread across several tabs, along the bottom
of the editor window, for convenience.
</p>
<h1 id="required">Required Tab</h1>
<p>
The first tab on the JAD editor window is labeled
<span class="keyword">Required</span>
</p>
<img src="img/jadEditor1.png" alt="screenshot">
<p>
The items on this tab are:
</p>
<table border="1">
<tr>
<td class="bold">Item</td><td class="bold">Contents</td>
</tr>
<tr>
<td>Midlet Jar URL</td>
<td>
The URL from which the JAR file can be loaded.  By default, this field will be
set to the name of the Midlet Suite project.  Changing the name of the jar file
will automatically cause a rebuild of the deployed jar with the new name.  A
full URL may be specified in this field, in which case the final component of the
URL will be used when building the JAR name.
</td>
</tr>
<tr>
<td>Midlet Name</td>
<td>
The name of the MIDlet suite that identifies the MIDlets to the user. 
Individual MIDlets can also have names.  See the
<a href="#midlets">Midlets tab</a> below for that information.
</td>
</tr>
<tr>
<td>Midlet Vendor</td>
<td>
The organization that provides the MIDlet suite.
</td>
</tr>
<tr>
<td>Midlet Version</td>
<td>
The version number of the MIDlet suite. The format is &lt;major&gt;.&lt;minor&gt;.&lt;micro&gt;
as described in the Java Product Versioning Specification. the Java ME container
can use this for install and upgrade purposes, as well as for communication
with the user. 
</td>
</tr>
<tr>
<td>Microedition Configuration</td>
<td>
The Java ME Configuration (CLDC version) required by this MIDlet suite.  The
contents of this pull-down will depend on the CLDC versions supported by the
Wireless Toolkit that you are using for this project.
</td>
</tr>
<tr>
<td>Microedition Profile</td>
<td>
The Java ME profile required by this MIDlet suite.  As with the Microedition
Configuration, the contents of this pull-down will depend on the profile versions
supported by the Wireless Toolkit that you are using for this project. 
</td>
</tr>
</table>
<p>
Note that the MTJ plug-in automatically provides the
<span class="keyword">MIDLet-Jar-Size</span> item required by the Java ME
specification, so you are not required to enter this information.
</p>

<h1 id="midlets">Midlets tab</h1>
<p>
The second tab on the JAD editor window is labeled
<span class="keyword">Midlets</span>.
On this tab, you must have one entry for each MIDlet in your
MIDlet suite.  If you selected the
<span class="keyword">Add to Application Descriptor</span> option
when you created the MIDlet class, an entry is automatically made
for you.  If not, you must use the
<span class="keyword">Add</span>
button to add an entry for your MIDlet.
</p>
<p>
Pressing the 
<span class="keyword">Add</span> button creates a new row, and the items in the list
can be directly edited in-place.
</p>
<img src="img/jadEditor2.png" alt="screenshot">
<p>
The columns on this tab are:
</p>
<table border="1">
<tr>
<td class="bold">Item</td><td class="bold">Contents</td>
</tr>
<tr>
<td>Name</td>
<td>
Name of the MIDlet.  If there are multiple MIDlets in your suite, when
the user invokes the suite, the Java ME device will typically prompt the user
for the specific MIDlet to be executed.  This entry provides the name
that will be shown to the user.
</td>
</tr>
<tr>
<td>Icon</td>
<td>
Path to the PNG file within the JAR file that contains the icon for this
MIDlet.
</td>
</tr>
<tr>
<td>Class</td>
<td>
The MIDlet class itself.  This is the class you created derived from
<span class="code">javax.microedition.midlet.MIDlet</span>.
</td>
</tr>
</table>
<p>
To edit the name or the icon path, simply click in the cell and enter the
new value.  To change the class, click in the cell.  When you do this, a
button will appear.<br>
<img src="img/jadEditor2a.png" alt="screenshot"><br>
Pressing the button will display a dialog box that will allow you to choose
the appropriate class for the midlet.
</p>
<img src="img/jadEditor2b.png" alt="screenshot"><br>
<h1 id="optional">Optional tab</h1>
<p>
The third tab on the JAD editor window is labeled
<span class="keyword">Optional</span>.
On this tab you can make entries that are defined in the Java ME specification,
but which are not required.
</p>
<img src="img/jadEditor3.png" alt="screenshot">
<p>
The items on this tab are:
</p>
<table border="1">
<tr>
<td class="bold">Item</td><td class="bold">Contents</td>
</tr>
<tr>
<td>Midlet&nbsp;Permissions</td>
<td>
Permissions that your MIDlet must have in order to operate correctly.
Permissions are usually only provided for signed MIDlets.
</td>
</tr>
<tr>
<td>Optional&nbsp;Midlet&nbsp;Permissions</td>
<td>
Permissions that your MIDlet would like, but can work without.
</td>
</tr>
<tr>
<td>Midlet Data Size</td>
<td>
The minimum number of bytes of persistent data required by the MIDlet.
The device may provide additional storage according to its own policy.
The default is zero.
</td>
</tr>
<tr>
<td>Midlet Description</td>
<td>
The description of the MIDlet suite.
</td>
</tr>
<tr>
<td>Midlet Icon</td>
<td>
The name of a PNG file within the JAR file used to represent the MIDlet suite.
It is the icon used by the Java Application Manager to identify the suite.
This icon is for the suite as a whole, as distinct from the individual
MIDlet icons you can set up on the
<a href="#midlets">Midlets Tab</a>.
</td>
</tr>
<tr>
<td>Midlet Information URL</td>
<td>
A URL for information further describing the MIDlet suite.
</td>
</tr>
</table>
<h1 id="overtheair">Over the Air tab</h1>
<p>
The fourth tab on the JAD editor window is labeled
<span class="keyword">Over the Air</span>.
On this tab you can make entries that are related to Over-the-Air
provisioning.
</p>
<img src="img/jadEditor4.png" alt="screenshot">
<p>
The items on this tab are:
</p>
<table border="1">
<tr>
<td class="bold">Item</td><td class="bold">Contents</td>
</tr>
<tr>
<td>Midlet Delete Confirm</td>
<td>
A text message provided to the user when prompted to confirm deletion of the
MIDlet suite. 
</td>
</tr>
<tr>
<td>Midlet Delete Notify</td>
<td>
The URL to which a POST request is sent to confirm successful deletion of
this MIDlet suite.
</td>
</tr>
<tr>
<td>Midlet Install Notify</td>
<td>
The URL to which a POST request is sent to confirm successful installation of
this MIDlet suite.
</td>
</tr>
</table>

<h1 id="pushreg">Push Registry tab</h1>
<p>
The fifth tab on the JAD editor window is labeled
<span class="keyword">Push Registry</span>.
On this tab, you must have one entry for every midlet that you want
to register on device's push registry.
</p>
<p>
Pressing the <span class="keyword">Add</span> button creates a new
row, and the items in the list
can be directly edited in-place.
</p>
<img src="img/jadEditor6.png" alt="screenshot">
<p>
The columns on this tab are:
</p>
<table border="1">
<tr>
<td class="bold">Item</td><td class="bold">Contents</td>
</tr>
<tr>
<td>Connection String</td>
<td>
URL of the inbound connection that AMS will be listening to. If any information
is received in this connection, the AMS will wake up the midlet registered.
Note that only one midlet can be registered in AMS per Connection String.
</td>
</tr>
<tr>
<td>Class</td>
<td>
Fully qualified path for the Midlet class to be activated. Note that there must be
an entry for this same class in the <a href="#midlets">Midlets tab</a></li>.
</td>
</tr>
<tr>
<td>Allowed Sender</td>
<td>
Used to filter the servers that can activate this Midlet. The format for this field
is protocol-specific, but the wildcards "*" and "?" can be used, with their usual
meanings.
</td>
</tr>
</table>
<p>
To edit the Connection String and Allowed Sender columns, simply click in the cell and enter the
new value.  To change the class, click in the cell.  When you do this, a
button will appear.<br>
<img src="img/jadEditor2a.png" alt="screenshot"><br>
Pressing the button will display a dialog box that will allow you to choose
the appropriate class for the midlet.
</p>
<img src="img/jadEditor6a.png" alt="screenshot"><br>

<h1 id="userdefined">User Defined tab</h1>
<p>
The sixth tab on the JAD editor window is labeled
<span class="keyword">User Defined</span>.
On this tab you can make custom entries relating to your particular MIDlet.
</p>
<img src="img/jadEditor5.png" alt="screenshot">
<p>
The columns on this tab are:
</p>
<table border="1">
<tr>
<td class="bold">Item</td><td class="bold">Contents</td>
</tr>
<tr>
<td>Key</td>
<td>
The key string used to retrieve the value. 
</td>
</tr>
<tr>
<td>Value</td>
<td>
The value associated with the key.
</td>
</tr>
</table>
</body>
</html>
