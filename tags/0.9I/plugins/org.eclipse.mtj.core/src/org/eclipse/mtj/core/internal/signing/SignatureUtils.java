/**
 * Copyright (c) 2005,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Kevin Hunter             - Initial implementation
 *     Craig Setera (EclipseME) - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.internal.signing;

import java.io.FileInputStream;
import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.jad.IJadSignature;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.sign.ISignatureProperties;
import org.eclipse.mtj.core.signing.SignaturePasswords;

/**
 * This class is used to construct IJadSignature objects.
 * 
 * @author Kevin Hunter
 */
public class SignatureUtils {
    /**
     * Obtain a signature object for the specified IMidletSuiteProject. The
     * signature properties used will be those found in the project's persistant
     * properties.
     * 
     * @param project
     * @return
     * @throws CoreException
     */
    public static IJadSignature getSignatureObject(IMidletSuiteProject project)
            throws CoreException {
        ISignatureProperties sigProps = project.getSignatureProperties();

        return (getSignatureObject(project, sigProps));
    }

    public static IJadSignature getSignatureObject(IMidletSuiteProject project,
            ISignatureProperties sigProps) throws CoreException {
        if (!sigProps.getSignProject()) {
            return (null);
        }

        JadSignature sigObject = null;
        FileInputStream fis = null;
        String strKeyStorePass = null;
        String strKeyPass = null;

        switch (sigProps.getPasswordStorageMethod()) {
        case ISignatureProperties.PASSMETHOD_IN_KEYRING:
        case ISignatureProperties.PASSMETHOD_IN_PROJECT:
            strKeyStorePass = sigProps.getKeyStorePassword();
            strKeyPass = sigProps.getKeyPassword();
            break;

        case ISignatureProperties.PASSMETHOD_PROMPT:
        default:
            if (project != null) {
                strKeyStorePass = project.getTempKeystorePassword();
                strKeyPass = project.getTempKeyPassword();
            }
            break;
        }

        boolean bPromptedForPasswords = false;

        if (strKeyStorePass == null || strKeyPass == null) {
            IStatus status = new Status(IStatus.INFO,
                    IMTJCoreConstants.PLUGIN_ID,
                    IMTJCoreConstants.INFO_NEED_SIGNATURE_PASSWORDS,
                    "Need signing passwords", null);

            SignaturePasswords passwords = (SignaturePasswords) MTJCorePlugin
                    .statusPrompt(status, project.getProject());

            if (passwords == null) {
                return (null);
            }

            strKeyStorePass = passwords.getKeystorePassword();
            strKeyPass = passwords.getKeyPassword();
            bPromptedForPasswords = true;
        }

        try {
            fis = new FileInputStream(sigProps.getAbsoluteKeyStorePath(project
                    .getProject()));
            KeyChainSet set = KeyChainSet.getInstance(fis, sigProps
                    .getKeyStoreType(), sigProps.getKeyStoreProvider(),
                    strKeyStorePass, sigProps.getKeyAlias(), strKeyPass);
            sigObject = new JadSignature(set);
            sigObject.checkKeyChainSet();

            if (bPromptedForPasswords) {
                switch (sigProps.getPasswordStorageMethod()) {
                case ISignatureProperties.PASSMETHOD_IN_KEYRING:
                case ISignatureProperties.PASSMETHOD_IN_PROJECT:
                    sigProps.setKeyPassword(strKeyPass);
                    sigProps.setKeyStorePassword(strKeyStorePass);
                    if (project != null) {
                        project.saveMetaData();
                    }
                    break;

                case ISignatureProperties.PASSMETHOD_PROMPT:
                default:
                    if (project != null) {
                        project.setTempKeystorePassword(strKeyStorePass);
                        project.setTempKeyPassword(strKeyPass);
                    }
                    break;
                }
            }

        } catch (IOException ioe) {
            MTJCorePlugin.throwCoreException(IStatus.ERROR, 99999, ioe);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e) {
                }
            }
        }

        return (sigObject);
    }
}