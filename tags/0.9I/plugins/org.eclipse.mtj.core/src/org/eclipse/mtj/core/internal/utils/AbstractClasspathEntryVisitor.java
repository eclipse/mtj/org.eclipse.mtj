/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.internal.utils;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;

/**
 * Abstract implementation of the IClasspathEntryVisitor. Provides a default
 * implementation that will return <code>false</code> concerning all
 * resolution and traversal.
 * 
 * @author Craig Setera
 */
public abstract class AbstractClasspathEntryVisitor implements
        IClasspathEntryVisitor {

    /**
     * Return a new instance of an object that may be used to run through the
     * classpath entry tree. The runner created will always traverse the tree in
     * a depth-first manner.
     * 
     * @return
     */
    public ClasspathEntryVisitorRunner getRunner() {
        return getRunner(false);
    }

    /**
     * Return a new instance of an object that may be used to run through the
     * classpath entry tree. This method allows the specification of the breadth
     * versus depth first traversal order.
     * 
     * @param breadthFirst if <code>true</code> use a breadth-first traversal
     *                rather than a depth-first traversal.
     * @return
     */
    public ClasspathEntryVisitorRunner getRunner(boolean breadthFirst) {
        return new ClasspathEntryVisitorRunner(breadthFirst);
    }

    /**
     * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitContainerBegin(IClasspathEntry,
     *      org.eclipse.jdt.core.IJavaProject, IPath,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public boolean visitContainerBegin(IClasspathEntry entry,
            IJavaProject javaProject, IPath containerPath,
            IProgressMonitor monitor) throws CoreException {
        return false;
    }

    /**
     * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitContainerEnd(org.eclipse.jdt.core.IClasspathEntry,
     *      org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IPath,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public void visitContainerEnd(IClasspathEntry entry,
            IJavaProject javaProject, IPath containerPath,
            IProgressMonitor monitor) throws CoreException {
    }

    /**
     * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitLibraryEntry(org.eclipse.jdt.core.IClasspathEntry,
     *      org.eclipse.jdt.core.IJavaProject,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public void visitLibraryEntry(IClasspathEntry entry,
            IJavaProject javaProject, IProgressMonitor monitor)
            throws CoreException {
    }

    /**
     * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitProject(IClasspathEntry,
     *      org.eclipse.jdt.core.IJavaProject,
     *      org.eclipse.jdt.core.IJavaProject,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public boolean visitProject(IClasspathEntry entry,
            IJavaProject javaProject, IJavaProject classpathProject,
            IProgressMonitor monitor) throws CoreException {
        return false;
    }

    /**
     * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitSourceEntry(org.eclipse.jdt.core.IClasspathEntry,
     *      org.eclipse.jdt.core.IJavaProject,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public void visitSourceEntry(IClasspathEntry entry,
            IJavaProject javaProject, IProgressMonitor monitor)
            throws CoreException {
    }

    /**
     * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitVariable(IClasspathEntry,
     *      org.eclipse.jdt.core.IJavaProject, java.lang.String,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public boolean visitVariable(IClasspathEntry entry,
            IJavaProject javaProject, String variableName,
            IProgressMonitor monitor) throws CoreException {
        return false;
    }
}