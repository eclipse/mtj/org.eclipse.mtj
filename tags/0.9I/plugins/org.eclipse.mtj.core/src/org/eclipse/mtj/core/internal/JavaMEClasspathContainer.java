/**
 * Copyright (c) 2008 Hugo Raniere.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola)  - Initial implementation
 */
package org.eclipse.mtj.core.internal;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.mtj.core.model.device.IDevice;

/**
 * A JavaMEClasspathContainer references a set of libraries associated with an
 * specific JavaME Device.
 * 
 * @author Hugo Raniere
 * @since MTJ 0.10
 * @see org.eclipse.jdt.core.IClasspathContainer
 * 
 */
public class JavaMEClasspathContainer implements IClasspathContainer {

	/**
	 * Represents the first segment of a JavaMEClasspathContainer path.
	 */
	public static final String JAVAME_CONTAINER = "org.elipse.mtj.JavaMEContainer";

	private final IPath path;

	private final IDevice device;

	private final IClasspathEntry[] classpathEntries;

	/**
	 * Constructs a new JavaMEClasspathContainer instance associated with an
	 * specific device.
	 * 
	 * @param containerPath
	 *            the container path identifying this container
	 * @param device
	 *            the device whose libs are contained by this container
	 */
	public JavaMEClasspathContainer(IPath containerPath, IDevice device) {
		this.path = containerPath;
		this.device = device;
		this.classpathEntries = device.getClasspath().asClasspathEntries();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.IClasspathContainer#getClasspathEntries()
	 */
	public IClasspathEntry[] getClasspathEntries() {
		return classpathEntries;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.IClasspathContainer#getDescription()
	 */
	public String getDescription() {
		return "JavaME library [" + device + "]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.IClasspathContainer#getKind()
	 */
	public int getKind() {
		return K_APPLICATION;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jdt.core.IClasspathContainer#getPath()
	 */
	public IPath getPath() {
		return path;
	}

}
