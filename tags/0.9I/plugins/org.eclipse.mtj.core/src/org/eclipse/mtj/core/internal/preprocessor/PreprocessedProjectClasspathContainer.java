/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 */
package org.eclipse.mtj.core.internal.preprocessor;

import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.mtj.core.internal.MTJCorePlugin;

/**
 * An implementation of the {@link IClasspathContainer} that provides a
 * reference to the classpath of the project that is providing the
 * preprocessing.
 * 
 * @author Craig Setera
 */
public class PreprocessedProjectClasspathContainer implements
        IClasspathContainer {
    private IProject project;
    private IJavaProject javaProject;

    /**
     * Construct a classpath container for the specified project.
     * 
     * @param project
     */
    public PreprocessedProjectClasspathContainer(IJavaProject javaProject) {
        super();
        this.project = javaProject.getProject();
        this.javaProject = javaProject;
    }

    /**
     * @see org.eclipse.jdt.core.IClasspathContainer#getClasspathEntries()
     */
    public IClasspathEntry[] getClasspathEntries() {
        ArrayList<IClasspathEntry> entries = new ArrayList<IClasspathEntry>();

        try {
            IClasspathEntry[] srcEntries = javaProject
                    .getResolvedClasspath(true);

            // Filter out all of the source entries
            for (int i = 0; i < srcEntries.length; i++) {
                if (srcEntries[i].getEntryKind() != IClasspathEntry.CPE_SOURCE) {
                    entries.add(srcEntries[i]);
                }
            }
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.ERROR, e.getMessage(), e);
        }

        return (IClasspathEntry[]) entries.toArray(new IClasspathEntry[entries
                .size()]);
    }

    /**
     * @see org.eclipse.jdt.core.IClasspathContainer#getDescription()
     */
    public String getDescription() {
        return "Preprocessed Project [ " + project.getName() + " ]";
    }

    /**
     * @see org.eclipse.jdt.core.IClasspathContainer#getKind()
     */
    public int getKind() {
        return K_APPLICATION;
    }

    /**
     * @see org.eclipse.jdt.core.IClasspathContainer#getPath()
     */
    public IPath getPath() {
        return project.getFullPath();
    }
}
