/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Hugo Raniere (Motorola)  - Removing Preprocessor code
 */
package org.eclipse.mtj.core.internal.preprocessor;

import org.eclipse.core.resources.IFile;
import org.eclipse.mtj.core.hook.sourceMapper.SourceMapper;


/**
 * An implementation of the {@link SourceMapper} hook interface. This mapper
 * attempts to map the requested file to a preprocessed version of the file.
 * 
 * @author Craig Setera
 */
public class PreprocessedSourceMapper implements SourceMapper {
    public IFile getMappedResource(IFile sourceFile) {
        IFile mappedFile = null;

        //FIXME Preprocessor is not yet available
//        if (isPreprocessingProject(sourceFile.getProject())) {
//            mappedFile = PreprocessorBuilder.getOutputFile(sourceFile);
//            if (!mappedFile.exists()) {
//                mappedFile = null;
//            }
//        }

        return mappedFile;
    }

    //FIXME Preprocessor is not yet available
//    /**
//     * Return a boolean indicating whether the specified project has
//     * preprocessing enabled.
//     * 
//     * @param project
//     * @return
//     */
//    private boolean isPreprocessingProject(IProject project) {
//        boolean preprocessing = false;
//
//        try {
//            preprocessing = project
//                    .hasNature(IMTJCoreConstants.J2ME_PREPROCESSING_NATURE_ID);
//        } catch (CoreException e) {
//            MTJCorePlugin.log(IStatus.WARNING, e);
//        }
//
//        return preprocessing;
//    }
}
