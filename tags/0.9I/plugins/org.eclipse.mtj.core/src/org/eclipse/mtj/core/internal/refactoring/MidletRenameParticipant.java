/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.internal.refactoring;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IType;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RenameParticipant;

/**
 * MTJ participant in rename refactoring operations
 * 
 * @author Craig Setera
 */
public class MidletRenameParticipant extends RenameParticipant {
    private IType type;

    /**
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#initialize(java.lang.Object)
     */
    protected boolean initialize(Object element) {
        type = (IType) element;
        return getArguments().getUpdateReferences();
    }

    /**
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#getName()
     */
    public String getName() {
        return "Update Application Descriptors";
    }

    /**
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#checkConditions(org.eclipse.core.runtime.IProgressMonitor,
     *      org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext)
     */
    public RefactoringStatus checkConditions(IProgressMonitor pm,
            CheckConditionsContext context) throws OperationCanceledException {
        return new RefactoringStatus();
    }

    /**
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#createChange(org.eclipse.core.runtime.IProgressMonitor)
     */
    public Change createChange(IProgressMonitor pm) throws CoreException,
            OperationCanceledException {
        SubProgressMonitor subMonitor = new SubProgressMonitor(pm, 5);
        subMonitor.setTaskName("Creating Application Descriptor Changes");

        String newName = getNewName();
        CompositeChange compositeChange = MidletJadFileChangesCollector
                .collectChange(MidletJadFileChangesCollector.MODE_RENAME, type,
                        newName, subMonitor);

        subMonitor.done();
        return (compositeChange.getChildren().length > 0) ? compositeChange
                : null;
    }

    /**
     * Return the new name to be used for the updates.
     * 
     * @return
     */
    private String getNewName() {
        StringBuffer sb = new StringBuffer();

        String packageName = type.getPackageFragment().getElementName();
        if ((packageName != null) && (packageName.length() > 0)) {
            sb.append(packageName).append(".");
        }

        sb.append(getArguments().getNewName());

        return sb.toString();
    }
}
