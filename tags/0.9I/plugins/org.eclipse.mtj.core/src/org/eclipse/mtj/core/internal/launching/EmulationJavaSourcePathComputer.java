/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Hugo Raniere (Motorola)  - Removing Preprocessor code
 */
package org.eclipse.mtj.core.internal.launching;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.sourcelookup.ISourceContainer;
import org.eclipse.jdt.launching.sourcelookup.containers.JavaSourcePathComputer;

/**
 * Overrides the standard {@link JavaSourcePathComputer} to point the source
 * folder into the preprocessed project.
 * 
 * @author Craig Setera
 */
public class EmulationJavaSourcePathComputer extends JavaSourcePathComputer {
    public static final String ID = "org.eclipse.mtj.core.launching.emulationSourcePathComputer";

    /**
     * @see org.eclipse.jdt.launching.sourcelookup.containers.JavaSourcePathComputer#computeSourceContainers(org.eclipse.debug.core.ILaunchConfiguration,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public ISourceContainer[] computeSourceContainers(
            ILaunchConfiguration configuration, IProgressMonitor monitor)
            throws CoreException {
        ISourceContainer[] containers = super.computeSourceContainers(
                configuration, monitor);

        // if (isPreprocessingProject(configuration)) {
        // for (int i = 0; i < containers.length; i++) {
        // if (containers[i] instanceof JavaProjectSourceContainer) {
        // IJavaProject javaProject =
        // ((JavaProjectSourceContainer) containers[i]).getJavaProject();
        //					
        // IProject srcProject = javaProject.getProject();
        // IProject tgtProject =
        // PreprocessorBuilder.getOutputProject(srcProject);
        // if (tgtProject.exists()) {
        // IJavaProject tgtJavaProject = JavaCore.create(tgtProject);
        // if (tgtJavaProject.exists()) {
        // containers[i] = new JavaProjectSourceContainer(tgtJavaProject);
        // }
        // }
        // }
        // }
        // }

        return containers;
    }

    /**
     * @see org.eclipse.jdt.launching.sourcelookup.containers.JavaSourcePathComputer#getId()
     */
    public String getId() {
        return ID;
    }

    // FIXME Preprocessor is not yet available
    // /**
    // * Return a boolean indicating whether the project being launched by the
    // * configuration is a preprocessing project.
    // *
    // * @param configuration
    // * @return
    // */
    // private boolean isPreprocessingProject(ILaunchConfiguration
    // configuration) {
    // boolean preprocessing = false;
    //
    // try {
    // String projectName = configuration.getAttribute(
    // IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, "");
    // IProject project = ResourcesPlugin.getWorkspace().getRoot()
    // .getProject(projectName);
    //
    // if (project.exists()) {
    // preprocessing = project
    // .hasNature(IMTJCoreConstants.J2ME_PREPROCESSING_NATURE_ID);
    // }
    //
    // } catch (CoreException e) {
    // MTJCorePlugin.log(IStatus.WARNING, e);
    // }
    //
    // return preprocessing;
    // }
}
