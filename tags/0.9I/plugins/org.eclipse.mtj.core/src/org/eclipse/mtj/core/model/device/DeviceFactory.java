/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.device;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.mtj.core.importer.IDeviceImporter;
import org.eclipse.mtj.core.internal.MTJCorePlugin;

/**
 * The device factory allows access to creation of devices based on the
 * available registered device importers.
 * 
 * @author Craig Setera
 */
public class DeviceFactory {

    /**
     * The extension point for use in registering new device importers.
     */
    public static final String EXT_DEVICE_IMPORTERS = "deviceImporter";

    // Get the registered device importers
    private static IDeviceImporter[] deviceImporters;

    /**
     * Return all devices that can be found in the specified directory or any
     * subdirectories. This method will consult all registered IDeviceImporter
     * implementations that have been registered with the system.
     * 
     * @param directory
     * @param foundDevices
     * @param monitor
     * @return
     * @throws CoreException
     * @throws InterruptedException if the user cancels the operation
     */
    public static void findDevices(File directory,
            IFoundDevicesList foundDevices, IProgressMonitor monitor)
            throws CoreException, InterruptedException {
        monitor.beginTask("Device Search", IProgressMonitor.UNKNOWN);
        IDeviceImporter[] importers = getDeviceImporters();
        addDevices(foundDevices, directory, importers, monitor);
        monitor.done();
    }

    /**
     * Add any devices that can be found in the specified directory or sub
     * directories given the specified device importer instances.
     * 
     * @param foundDevices
     * @param directory
     * @param importers
     * @param monitor
     * @throws InterruptedException if the user cancels the operation
     */
    private static void addDevices(IFoundDevicesList foundDevices,
            File directory, IDeviceImporter[] importers,
            IProgressMonitor monitor) throws InterruptedException {
        // Give the user the chance to bail out during the search
        if (monitor.isCanceled()) {
            throw new InterruptedException();
        }

        // First find any devices in the current directory
        monitor.setTaskName("Searching in " + directory);
        for (int i = 0; i < importers.length; i++) {
            SubProgressMonitor subMonitor = new SubProgressMonitor(monitor, 1);
            IDevice[] localFoundDevices = importers[i].getMatchingDevices(
                    directory, subMonitor);
            if (localFoundDevices != null) {
                foundDevices.addDevices(localFoundDevices);
            }
        }

        // Now recurse to sub directories
        File[] subdirectories = directory.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });

        for (int i = 0; i < subdirectories.length; i++) {
            addDevices(foundDevices, subdirectories[i], importers, monitor);
        }

        monitor.worked(1);
    }

    /**
     * Return the registered device importer instances.
     * 
     * @return
     * @throws CoreException
     */
    private static IDeviceImporter[] getDeviceImporters() throws CoreException {
        if (deviceImporters == null) {
            deviceImporters = readDeviceImporters();
        }

        return deviceImporters;
    }

    /**
     * Read the device importers from the registered extensions to the
     * "deviceImporters" extension point.
     * 
     * @return
     * @throws CoreException
     */
    private static IDeviceImporter[] readDeviceImporters() throws CoreException {
        String pluginId = MTJCorePlugin.getDefault().getBundle()
                .getSymbolicName();
        IExtensionRegistry registry = Platform.getExtensionRegistry();

        IConfigurationElement[] elements = registry
                .getConfigurationElementsFor(pluginId, EXT_DEVICE_IMPORTERS);
        DeviceImporterElement[] deviceElements = new DeviceImporterElement[elements.length];

        for (int i = 0; i < elements.length; i++) {
            deviceElements[i] = new DeviceImporterElement(elements[i]);
        }

        Arrays.sort(deviceElements, new Comparator<Object>() {
            public int compare(Object o1, Object o2) {
                DeviceImporterElement element1 = (DeviceImporterElement) o1;
                DeviceImporterElement element2 = (DeviceImporterElement) o2;
                return element1.getPriority() - element2.getPriority();
            }
        });

        IDeviceImporter[] importers = new IDeviceImporter[elements.length];
        for (int i = 0; i < deviceElements.length; i++) {
            importers[i] = deviceElements[i].getDeviceImporter();
        }

        return importers;
    }

    /*
     * Static factory methods only
     */
    private DeviceFactory() {
        super();
    }
}
