/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 */
package org.eclipse.mtj.core.internal.preprocessor;

import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.nature.AbstractNature;

/**
 * Project nature for J2ME projects that contain preprocessed resources.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * 
 * @author Craig Setera
 * @see IProjectNature
 */
public class PreprocessedNature extends AbstractNature {
    /**
     * @see IProjectNature#configure
     */
    public void configure() throws CoreException {
        // Do nothing. This nature is no longer really necessary,
        // but it is kept around to aid in migration
    }
}
