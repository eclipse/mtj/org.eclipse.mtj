/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui;

import org.eclipse.mtj.ui.internal.device.editor.DeviceEditorDialog;
import org.eclipse.mtj.ui.internal.device.editor.JavaDeviceEditorDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * An action delegate to launch the Java Device editor dialog.
 * 
 * @author Craig Setera
 */
public class JavaDeviceEditorActionDelegate extends
        DefaultDeviceEditorActionDelegate {

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.DefaultDeviceEditorActionDelegate#getDeviceEditorDialog(org.eclipse.swt.widgets.Shell)
     */
    protected DeviceEditorDialog getDeviceEditorDialog(Shell shell) {
        return new JavaDeviceEditorDialog(shell);
    }
}
