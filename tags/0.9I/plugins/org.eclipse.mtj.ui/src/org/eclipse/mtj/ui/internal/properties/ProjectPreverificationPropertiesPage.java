/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)  - Initial implementation
 *     Kevin Hunter (Individual) - Added signature support
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse
 *                                 standards
 */
package org.eclipse.mtj.ui.internal.properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.ui.internal.preferences.PreverificationPreferencePage;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.PropertyPage;

/**
 * Property page implementation for J2ME properties associated with
 * preverification of a project.
 * 
 * @author Craig Setera
 * @see PropertyPage
 */
public class ProjectPreverificationPropertiesPage extends
        PropertyAndPreferencePage {
    /**
     * Default constructor.
     */
    public ProjectPreverificationPropertiesPage() {
    }

    /**
     * @see org.eclipse.mtj.ui.internal.properties.PropertyAndPreferencePage#performOk()
     */
    public boolean performOk() {
        boolean result = super.performOk();

        if (result) {
            IProject project = getProject();
            PreverificationPreferencePage page = (PreverificationPreferencePage) preferencePage;
            page.doBuild(new IProject[] { project });
        }

        return result;
    }

    /**
     * Embed a preference page into the parent composite, setting things up
     * correctly as we go along.
     * 
     * @param composite
     */
    protected void embedPreferencePage(Composite composite) {
        preferencePage = new PreverificationPreferencePage(true,
                getPreferenceStore());
        preferencePage.createControl(composite);
        preferencePage.getControl().setLayoutData(
                new GridData(GridData.FILL_BOTH));
    }

    /**
     * @see org.eclipse.mtj.ui.internal.properties.PropertyAndPreferencePage#getProjectSpecificSettingsKey()
     */
    protected String getProjectSpecificSettingsKey() {
        return IMTJCoreConstants.PREF_PREVERIFY_USE_PROJECT;
    }

    /**
     * @see org.eclipse.mtj.ui.internal.properties.PropertyAndPreferencePage#isReadOnly()
     */
    protected boolean isReadOnly() {
        return isPreprocessedOutputProject();
    }
}
