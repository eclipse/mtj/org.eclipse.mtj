/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.viewers;

import java.util.Comparator;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

/**
 * Instances of this class provide necessary configuration information for
 * making a fully-functional table viewer with sortable columns.
 * 
 * @author Craig Setera
 */
public class TableViewerConfiguration {

    private static final String SORT_COLUMN = "sortColumn";
    private static final String SORT_DIRECTION = "sortDirection";

    /**
     * Comparator that uses the labels from the label provider and does a
     * string-based sort.
     */
    private static class LabelProviderComparator implements Comparator {
        private TableViewer tableViewer;
        private int columnIndex;

        /**
         * @param treeViewer
         * @param columnIndex
         */
        LabelProviderComparator(TableViewer tableViewer, int columnIndex) {
            super();
            this.tableViewer = tableViewer;
            this.columnIndex = columnIndex;
        }

        public int compare(Object o1, Object o2) {
            ITableLabelProvider labelProvider = (ITableLabelProvider) tableViewer
                    .getLabelProvider();
            String label1 = getLabel(labelProvider, o1);
            String label2 = getLabel(labelProvider, o2);

            return label1.compareTo(label2);
        }

        /**
         * Return the correct label for the specified object.
         * 
         * @param labelProvider
         * @param object
         * @return
         */
        private String getLabel(ITableLabelProvider labelProvider, Object object) {
            return labelProvider.getColumnText(object, columnIndex);
        }
    }

    /*
     * A comparator class that wraps another comparator. The response of the
     * base comparator is negated to reverse the sort order.
     */
    private static class ReversingComparator implements Comparator {
        private Comparator baseComparator;

        ReversingComparator(Comparator baseComparator) {
            super();
            this.baseComparator = baseComparator;
        }

        public int compare(Object o1, Object o2) {
            return 0 - baseComparator.compare(o1, o2);
        }
    }

    /*
     * A listener implementation to handle events on table columns, including
     * resize and sorting.
     */
    private class TableColumnListener implements ControlListener,
            SelectionListener {
        private TableViewer viewer;
        private int columnIndex;

        TableColumnListener(TableViewer viewer, int columnIndex) {
            super();
            this.viewer = viewer;
            this.columnIndex = columnIndex;
        }

        public void controlMoved(ControlEvent e) {
        }

        public void widgetDefaultSelected(SelectionEvent e) {
        }

        public void controlResized(ControlEvent e) {
            TableColumn column = (TableColumn) e.widget;
            setColumnWidth(columnIndex, column.getWidth());
        }

        public void widgetSelected(SelectionEvent e) {
            // Handle the sorting
            int currentSortColumn = getSortColumn();
            if (currentSortColumn == columnIndex) {
                setSortDirectionAscending(!getSortDirectionAscending());
            } else {
                setSortColumn(columnIndex);
                setSortDirectionAscending(true);
            }

            updateSortImages(viewer);
            updateViewerSorter();
            viewer.refresh();
        }

        /**
         * Update the viewer sorter based on the current sort criteria.
         */
        private void updateViewerSorter() {
            ViewerSorter sorter = viewer.getSorter();
            if (sorter instanceof TableViewerInfoSorter) {
                TableViewerInfoSorter infoSorter = (TableViewerInfoSorter) sorter;
                infoSorter.setComparator(getComparator(viewer));
            }
        }
    }

    /*
     * A viewer sorter implementation that sorts based on the current sort
     * information and the specified comparator.
     */
    private class TableViewerInfoSorter extends ViewerSorter {
        private Comparator comparator;

        public TableViewerInfoSorter() {
            super();
        }

        public int compare(Viewer viewer, Object e1, Object e2) {
            return comparator.compare(e1, e2);
        }

        public void setComparator(Comparator comparator) {
            this.comparator = comparator;
        }
    }

    private IDialogSettings dialogSettings;
    private TableColumnInfo[] columnInfo;
    private int defaultSortColumn;
    private int defaultTableWidth;
    private String[] columnWidthKeys;

    // Hold on to some references to cached images for sorting
    private Image downArrow;
    private Image upArrow;

    /**
     * Construct new viewer information with storage in the specified location.
     * 
     * @param dialogSettings
     * @param columnInfo
     */
    public TableViewerConfiguration(IDialogSettings dialogSettings,
            int defaultTableWidth, TableColumnInfo[] columnInfo,
            int defaultSortColumn) {
        super();
        this.dialogSettings = dialogSettings;
        this.columnInfo = columnInfo;
        this.defaultSortColumn = defaultSortColumn;
        this.defaultTableWidth = defaultTableWidth;

        // Cache the keys for looking up the column widths
        columnWidthKeys = new String[columnInfo.length];
        for (int i = 0; i < columnInfo.length; i++) {
            TableColumnInfo info = columnInfo[i];
            columnWidthKeys[i] = "columnWidth_" + info.getName();
        }

        // Initialize images
        downArrow = MTJUIPlugin.getImageFromCache("downarrow16.gif");
        upArrow = MTJUIPlugin.getImageFromCache("uparrow16.gif");
    }

    /**
     * Configure the table viewer and all related listeners based on this table
     * viewer information. This method will install a sorter into the viewer.
     * Removal of this sorter will cause the sorting functionality to fail.
     * 
     * @param viewer
     */
    public void configure(TableViewer viewer) {
        // For any of this to work, we must attach our sorter to the
        // viewer
        TableViewerInfoSorter sorter = new TableViewerInfoSorter();
        sorter.setComparator(getComparator(viewer));
        viewer.setSorter(sorter);

        // Set the table layout on the table
        Table table = viewer.getTable();
        TableLayout layout = new TableLayout();
        table.setLayout(layout);

        for (int i = 0; i < columnInfo.length; i++) {
            TableColumnInfo info = columnInfo[i];

            // Set up this table column
            TableColumn column = new TableColumn(table, info.getAlignment());
            column.setText(info.getName());
            int columnWidth = getColumnWidth(i);
            layout.addColumnData(new ColumnPixelData(columnWidth));

            // Wire up listeners to the column
            TableColumnListener listener = new TableColumnListener(viewer, i);
            column.addControlListener(listener);
            column.addSelectionListener(listener);
        }

        updateSortImages(viewer);
    }

    /**
     * @return Returns the columnInfo.
     */
    public TableColumnInfo[] getColumnInfo() {
        return columnInfo;
    }

    /**
     * @param columnInfo The columnInfo to set.
     */
    public void setColumnInfo(TableColumnInfo[] columnInfo) {
        this.columnInfo = columnInfo;
    }

    /**
     * @return Returns the dialogSettings.
     */
    public IDialogSettings getDialogSettings() {
        return dialogSettings;
    }

    /**
     * @param dialogSettings The dialogSettings to set.
     */
    public void setDialogSettings(IDialogSettings dialogSettings) {
        this.dialogSettings = dialogSettings;
    }

    /**
     * Return the index of the current column to be sorted.
     * 
     * @return
     */
    public int getSortColumn() {
        return (dialogSettings.get(SORT_COLUMN) != null) ? dialogSettings
                .getInt(SORT_COLUMN) : defaultSortColumn;
    }

    /**
     * Set the index of the current column to be sorted.
     * 
     * @param columnIndex
     */
    public void setSortColumn(int columnIndex) {
        dialogSettings.put(SORT_COLUMN, columnIndex);
    }

    /**
     * Return a boolean indicating whether or not the current sort direction is
     * ascending.
     * 
     * @return
     */
    public boolean getSortDirectionAscending() {
        return (dialogSettings.get(SORT_DIRECTION) != null) ? dialogSettings
                .getBoolean(SORT_DIRECTION) : true;
    }

    /**
     * Return a boolean indicating whether or not the current sort direction is
     * ascending.
     * 
     * @param ascending
     */
    public void setSortDirectionAscending(boolean ascending) {
        dialogSettings.put(SORT_DIRECTION, ascending);
    }

    /**
     * Return the correct column width for the specified column. The column
     * width will be pulled from the dialog settings if it has been stored
     * there. If not found in the settings, it will be calculated from the
     * default in the provided column info.
     * 
     * @param columnIndex
     * @return
     */
    private int getColumnWidth(int columnIndex) {
        int width = 0;
        String key = columnWidthKeys[columnIndex];

        if (dialogSettings.get(key) != null) {
            width = dialogSettings.getInt(key);
        } else {
            float percent = columnInfo[columnIndex].getDefaultWidthPercent() / 100;
            width = (int) (defaultTableWidth * percent);
        }

        return width;
    }

    /**
     * Store the correct column width for the specified column into the dialog
     * settings.
     * 
     * @param columnIndex
     * @param width
     */
    private void setColumnWidth(int columnIndex, int width) {
        String key = columnWidthKeys[columnIndex];
        dialogSettings.put(key, width);
    }

    /**
     * Get the appropriate comparator to use given the current sort criteria.
     * 
     * @return
     */
    private Comparator getComparator(TableViewer viewer) {
        TableColumnInfo info = columnInfo[getSortColumn()];

        Comparator comparator = info.getComparator();
        if (comparator == null) {
            comparator = new LabelProviderComparator(viewer, getSortColumn());
        }

        if (!getSortDirectionAscending()) {
            comparator = new ReversingComparator(comparator);
        }

        return comparator;
    }

    /**
     * Update the sort images for the columns based on the current sort
     * criteria.
     */
    private void updateSortImages(TableViewer viewer) {
        Table table = viewer.getTable();
        TableColumn[] columns = table.getColumns();
        for (int i = 0; i < columns.length; i++) {
            Image image = null;
            if (i == getSortColumn()) {
                image = getSortDirectionAscending() ? downArrow : upArrow;
            }

            columns[i].setImage(image);
        }
    }
}
