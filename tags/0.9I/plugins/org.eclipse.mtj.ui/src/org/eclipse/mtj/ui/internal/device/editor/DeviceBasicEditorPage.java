/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Always enable the preverifier browse button                           
 */
package org.eclipse.mtj.ui.internal.device.editor;

import java.io.File;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.preverifier.IPreverifier;
import org.eclipse.mtj.core.model.preverifier.StandardPreverifierFactory;
import org.eclipse.mtj.core.model.preverifier.impl.StandardPreverifier;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * A composite implementation for the editing of the basic properties of a
 * device.
 * 
 * @author Craig Setera
 */
public class DeviceBasicEditorPage extends AbstractDeviceEditorPage {

	private static final Boolean[] BOOLEAN_VALUES = new Boolean[] {
			Boolean.TRUE, Boolean.FALSE };

	/**
	 * Content provider for boolean types
	 */
	private static class BooleanContentProvider implements
			IStructuredContentProvider {

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
		 */
		public Object[] getElements(Object inputElement) {
			return BOOLEAN_VALUES;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
		 */
		public void dispose() {
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer,
		 *      java.lang.Object, java.lang.Object)
		 */
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}
	}

	private static class BooleanLabelProvider extends LabelProvider {
		
		/* (non-Javadoc)
		 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
		 */
		public String getText(Object element) {
			Boolean bool = (Boolean) element;
			return bool.booleanValue() ? "Yes" : "No";
		}
	}

	// The widgets
	private Text nameText;
	private Text groupText;
	private Text descriptionText;
	private Text executableText;
	private Text launchCommandText;
	private ComboViewer debugServerCombo;
	private Text preverifyExecutableText;
	private Button preverifyBrowseButton;
	private boolean preverifierEnabled;
	private boolean javaExecutableDevice;
	private Button executableBrowseButton;

	/**
	 * Construct a new instance of the page.
	 * 
	 * @param parent
	 * @param style
	 */
	public DeviceBasicEditorPage(Composite parent,
			boolean javaExecutableDevice, int style) {
		super(parent, style);
		this.javaExecutableDevice = javaExecutableDevice;
		preverifierEnabled = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mtj.ui.internal.device.editor.AbstractDeviceEditorPage#commitDeviceChanges()
	 */
	public void commitDeviceChanges() throws CoreException {
		editDevice.setName(nameText.getText());
		editDevice.setGroupName(groupText.getText());
		editDevice.setDescription(descriptionText.getText());

		if (!javaExecutableDevice) {
			editDevice.setExecutable(new File(executableText.getText()));
		}

		editDevice.setDebugServer(getBooleanSelection(debugServerCombo));
		editDevice.setLaunchCommandTemplate(launchCommandText.getText());

		if (preverifierEnabled) {
			File exe = new File(preverifyExecutableText.getText());
			if (exe.exists()) {
				StandardPreverifier standard = StandardPreverifierFactory
						.createPreverifier(exe);
				editDevice.setPreverifier(standard);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mtj.ui.internal.device.editor.AbstractDeviceEditorPage#getDescription()
	 */
	public String getDescription() {
		return "Specify the basic information that defines the device";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mtj.ui.internal.device.editor.AbstractDeviceEditorPage#getTitle()
	 */
	public String getTitle() {
		return "Basic";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mtj.ui.internal.device.editor.AbstractDeviceEditorPage#setDevice(org.eclipse.mtj.core.model.device.IDevice)
	 */
	public void setDevice(IDevice device) {
		super.setDevice(device);
		initializeDeviceState();
	}

	/**
	 * Browse the file system for an executable. Use the value in the specified
	 * text field to set a starting point for the browser. Store the result in
	 * the specified text field.
	 * 
	 * @param textField
	 */
	protected void browseForExecutable(Text textField) {
		FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);
		dialog.setText("Select Device Executable");

		File currentFile = new File(textField.getText());
		while (currentFile != null) {
			if (currentFile.exists()) {
				dialog.setFileName(currentFile.toString());
				break;
			} else {
				currentFile = currentFile.getParentFile();
			}
		}

		String filename = dialog.open();
		if (filename != null) {
			textField.setText(filename);
		}
	}

	/**
	 * Create a new viewer for boolean value selection.
	 * 
	 * @param parent
	 * @return
	 */
	private ComboViewer createBooleanComboViewer(Composite parent) {
		ComboViewer viewer = new ComboViewer(parent, SWT.READ_ONLY);
		viewer.setContentProvider(new BooleanContentProvider());
		viewer.setLabelProvider(new BooleanLabelProvider());
		viewer.setInput(new Object());

		return viewer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mtj.ui.internal.device.editor.AbstractDeviceEditorPage#addPageControls(org.eclipse.swt.widgets.Composite)
	 */
	protected void addPageControls(Composite parent) {
		ModifyListener modifyListener = new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				updateState();
			}
		};

		parent.setLayoutData(new GridData(GridData.FILL_BOTH));
		parent.setLayout(new GridLayout(3, false));

		(new Label(parent, SWT.NONE)).setText("Name:");
		nameText = new Text(parent, SWT.BORDER);
		nameText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		nameText.addModifyListener(modifyListener);
		new Label(parent, SWT.NONE);

		(new Label(parent, SWT.NONE)).setText("Group:");
		groupText = new Text(parent, SWT.BORDER);
		groupText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		groupText.addModifyListener(modifyListener);
		new Label(parent, SWT.NONE);

		(new Label(parent, SWT.NONE)).setText("Description:");
		descriptionText = new Text(parent, SWT.BORDER);
		descriptionText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		new Label(parent, SWT.NONE);

		(new Label(parent, SWT.NONE)).setText("Executable:");
		executableText = new Text(parent, SWT.BORDER);
		executableText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		executableText.addModifyListener(modifyListener);

		executableBrowseButton = new Button(parent, SWT.PUSH);
		executableBrowseButton.setText("Browse...");
		executableBrowseButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				browseForExecutable(executableText);
			}
		});

		(new Label(parent, SWT.NONE)).setText("Preverifier:");
		preverifyExecutableText = new Text(parent, SWT.BORDER);
		preverifyExecutableText.setLayoutData(new GridData(
				GridData.FILL_HORIZONTAL));
		preverifyExecutableText.addModifyListener(modifyListener);

		preverifyBrowseButton = new Button(parent, SWT.PUSH);
		preverifyBrowseButton.setText("Browse...");
		preverifyBrowseButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				browseForExecutable(preverifyExecutableText);
			}
		});

		(new Label(parent, SWT.NONE)).setText("Debug Server:");
		debugServerCombo = createBooleanComboViewer(parent);
		debugServerCombo.getCombo().setLayoutData(
				new GridData(GridData.FILL_HORIZONTAL));
		new Label(parent, SWT.NONE);

		Label blankLabel = new Label(parent, SWT.NONE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 3;
		blankLabel.setLayoutData(gd);

		Label launchCommandLabel = new Label(parent, SWT.NONE);
		launchCommandLabel.setText("Launch Command Template:");
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 3;
		launchCommandLabel.setLayoutData(gd);

		launchCommandText = new Text(parent, SWT.MULTI | SWT.WRAP | SWT.BORDER);
		launchCommandText.setTextLimit(1000);
		launchCommandText.addModifyListener(modifyListener);
		gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 3;
		gd.widthHint = 500;
		gd.heightHint = 300;
		launchCommandText.setLayoutData(gd);

		initializeDeviceState();
	}

	/**
	 * Get the boolean combo viewer selection as a boolean value.
	 * 
	 * @param viewer
	 * @return
	 */
	private boolean getBooleanSelection(ComboViewer viewer) {
		IStructuredSelection selection = (IStructuredSelection) viewer
				.getSelection();
		Boolean value = (Boolean) selection.getFirstElement();
		return value.booleanValue();
	}

	/**
	 * Initialize the state of the widgets based on the state of the device
	 */
	private void initializeDeviceState() {
		if ((nameText != null) && (editDevice != null)) {
			nameText.setText(editDevice.getName());
			groupText.setText(editDevice.getGroupName());
			descriptionText.setText(editDevice.getDescription());

			String exeText = javaExecutableDevice ? "Java" : editDevice
					.getExecutable().toString();
			executableText.setText(exeText);
			executableText.setEnabled(!javaExecutableDevice);
			executableBrowseButton.setEnabled(!javaExecutableDevice);

			setBooleanSelection(debugServerCombo, editDevice.isDebugServer());
			launchCommandText.setText(editDevice.getLaunchCommandTemplate());

			preverifierEnabled = true;
			IPreverifier preverifier = editDevice.getPreverifier();
			if (preverifier instanceof StandardPreverifier) {
				preverifierEnabled = true;
				StandardPreverifier standard = (StandardPreverifier) preverifier;
				preverifyExecutableText.setText(standard
						.getPreverifierExecutable().toString());
			}

			preverifyBrowseButton.setEnabled(preverifierEnabled);
			preverifyExecutableText.setEnabled(preverifierEnabled);
		}
	}

	/**
	 * Set the boolean combo viewer selection to the specified value.
	 * 
	 * @param viewer
	 * @param value
	 */
	private void setBooleanSelection(ComboViewer viewer, boolean value) {
		StructuredSelection selection = new StructuredSelection(Boolean
				.valueOf(value));
		viewer.setSelection(selection, true);
	}

	/**
	 * Update the state of this page.
	 */
	private void updateState() {
		String errorMessage = null;

		if (nameText.getText().length() == 0) {
			errorMessage = "Device name must be specified.";
		} else if (groupText.getText().length() == 0) {
			errorMessage = "Device group must be specified";
		} else if (launchCommandText.getText().length() == 0) {
			errorMessage = "Launch command must be specified.";
		} else {
			File executable = new File(executableText.getText());
			if (!javaExecutableDevice && !executable.exists()) {
				errorMessage = "Valid executable must be specified";
			} else {
				executable = new File(preverifyExecutableText.getText());
				if (!executable.exists()) {
					executable = new File(preverifyExecutableText.getText()
							+ ".exe");
					if (!executable.exists()) {
						errorMessage = "Valid preverifier executable must be specified";
					}
				}
			}
		}

		setErrorMessage(errorMessage);
		setValid(errorMessage == null);
	}
}
