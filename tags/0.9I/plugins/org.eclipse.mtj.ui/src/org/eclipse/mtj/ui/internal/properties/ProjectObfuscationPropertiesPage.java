/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)  - Initial implementation
 *     Kevin Hunter (Individual) - Added signature support
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse
 *                                 standards
 */
package org.eclipse.mtj.ui.internal.properties;

import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.ui.internal.preferences.ObfuscationPreferencePage;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.PropertyPage;

/**
 * Property page implementation for Java ME obfuscation properties associated
 * with a specific project.
 * 
 * @author Craig Setera
 * @see PropertyPage
 */
public class ProjectObfuscationPropertiesPage extends PropertyAndPreferencePage {
    
    /**
     * Default constructor.
     */
    public ProjectObfuscationPropertiesPage() {
    }

    /**
     * Embed a preference page into the parent composite, setting things up
     * correctly as we go along.
     * 
     * @param composite
     */
    protected void embedPreferencePage(Composite composite) {
        preferencePage = new ObfuscationPreferencePage(true,
                getPreferenceStore());
        preferencePage.createControl(composite);
        preferencePage.getControl().setLayoutData(
                new GridData(GridData.FILL_BOTH));
    }

    /**
     * @see org.eclipse.mtj.ui.internal.properties.PropertyAndPreferencePage#getProjectSpecificSettingsKey()
     */
    protected String getProjectSpecificSettingsKey() {
        return IMTJCoreConstants.PREF_OBFUSCATION_USE_PROJECT;
    }

    /**
     * @see org.eclipse.mtj.ui.internal.properties.PropertyAndPreferencePage#isReadOnly()
     */
    protected boolean isReadOnly() {
        return isPreprocessedOutputProject();
    }
}
