/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.ui.internal.actions;

import org.eclipse.ui.IObjectActionDelegate;

/**
 * Action delegate implementation for creating a packaged version of a J2ME
 * project. This action will create a deployed jar containing the application
 * code as well as updating and deploying the JAD file. This subclass does
 * obfuscate the resulting package.
 * 
 * @author Craig Setera
 */
public class CreateObfuscatedPackageAction extends AbstractCreatePackageAction
        implements IObjectActionDelegate {

    /**
     * Default constructor
     */
    public CreateObfuscatedPackageAction() {
        super();
    }

    /**
     * @see org.eclipse.mtj.ui.internal.actions.AbstractCreatePackageAction#shouldObfuscate()
     */
    protected boolean shouldObfuscate() {
        return true;
    }
}
