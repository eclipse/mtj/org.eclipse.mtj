/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.preferences;

import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePreferenceInitializer;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.IEmbeddableWorkbenchPreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

/**
 * Preference page implementation for setting J2ME obfuscation preferences.
 * 
 * @author Craig Setera
 */
public class ObfuscationPreferencePage extends FieldEditorPreferencePage
        implements IEmbeddableWorkbenchPreferencePage, IMTJCoreConstants {

    public static final String ID = "org.eclipse.mtj.ui.preferences.ObfuscationPreferencePage";

    private boolean embeddedInProperties;

    // Widgets
    private Button specifiedArgumentsButton;
    private Text specifiedArgumentsText;

    /**
     * Default constructor.
     */
    public ObfuscationPreferencePage() {
        this(false, MTJUIPlugin.getDefault().getCorePreferenceStore());
    }

    /**
     * Constructor for use when embedding the preference page within a
     * properties page.
     * 
     * @param embeddedInProperties
     * @param preferenceStore
     */
    public ObfuscationPreferencePage(boolean embeddedInProperties,
            IPreferenceStore preferenceStore) {
        super(GRID);

        this.embeddedInProperties = embeddedInProperties;
        setPreferenceStore(preferenceStore);

        if (!embeddedInProperties) {
            setDescription(MTJUIStrings.getString("pref.description"));
        }
    }

    /*
     * Overridden so that we get the help context where it belongs so that it
     * works when the focus is in the left pane.
     */
    protected Control createContents(Composite parent) {
        if (embeddedInProperties) {
            noDefaultAndApplyButton();
        }

        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_ObfuscationPreferencePage");
        return (super.createContents(parent));
    }

    /**
     * Creates the field editors.
     */
    public void createFieldEditors() {
        final Composite parent = getFieldEditorParent();

        if (!embeddedInProperties) {
            addField(new DirectoryFieldEditor(PREF_PROGUARD_DIR,
                    "Proguard Root Directory", parent));
        }

        Font font = parent.getFont();
        specifiedArgumentsButton = new Button(parent, SWT.CHECK);
        specifiedArgumentsButton.setText("Use specified arguments:");
        specifiedArgumentsButton.setFont(font);

        specifiedArgumentsText = new Text(parent, SWT.SINGLE | SWT.BORDER);
        specifiedArgumentsText.setFont(font);
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 2;
        specifiedArgumentsText.setLayoutData(gd);

        specifiedArgumentsButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                specifiedArgumentsText.setEnabled(specifiedArgumentsButton
                        .getSelection());
            }
        });

        addField(new MultiValuedTableFieldEditor(PREF_PROGUARD_KEEP,
                "Proguard Keep Expressions", parent));

        addProguardNotes(parent, font);
    }

    /**
     * Initialize the preference page as necessary
     * 
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#initialize()
     */
    protected void initialize() {
        super.initialize();

        IPreferenceStore store = getPreferenceStore();
        setState(store
                .getBoolean(IMTJCoreConstants.PREF_PROGUARD_USE_SPECIFIED),
                store.getString(IMTJCoreConstants.PREF_PROGUARD_OPTIONS));
    }

    /**
     * @see org.eclipse.jface.preference.PreferencePage#performApply()
     */
    public void performApply() {
        super.performApply();
    }

    /**
     * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
     */
    public void performDefaults() {
        super.performDefaults();

        setState(MTJCorePreferenceInitializer.PREF_DEF_PROGUARD_USE_SPECIFIED,
                MTJCorePreferenceInitializer.PREF_DEF_PROGUARD_OPTIONS);
    }

    /**
     * @see org.eclipse.jface.preference.IPreferencePage#performOk()
     */
    public boolean performOk() {
        IPreferenceStore store = getPreferenceStore();

        boolean useSpecified = specifiedArgumentsButton.getSelection();
        store.setValue(IMTJCoreConstants.PREF_PROGUARD_USE_SPECIFIED,
                useSpecified);
        store.setValue(IMTJCoreConstants.PREF_PROGUARD_OPTIONS,
                specifiedArgumentsText.getText());

        return super.performOk();
    }

    /**
     * Add the notes label for Proguard.
     * 
     * @param parent
     * @param font
     */
    private void addProguardNotes(final Composite parent, Font font) {
        GridData gd;
        Label notes = new Label(parent, SWT.NONE);
        notes.setFont(font);
        notes.setText("NOTE: See Proguard documentation for more information");

        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 3;
        notes.setLayoutData(gd);
    }

    /**
     * Set the current state of the widgets.
     * 
     * @param useSpecified
     * @param specified
     */
    private void setState(boolean useSpecified, String specified) {
        specifiedArgumentsButton.setSelection(useSpecified);
        specifiedArgumentsText.setText(specified);
        specifiedArgumentsText.setEnabled(useSpecified);
    }
}
