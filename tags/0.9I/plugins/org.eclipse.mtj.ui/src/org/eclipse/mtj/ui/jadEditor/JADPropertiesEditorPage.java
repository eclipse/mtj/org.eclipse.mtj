/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Refactoring the page to support extensibility
 */
package org.eclipse.mtj.ui.jadEditor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.jad.DescriptorPropertyDescription;
import org.eclipse.mtj.ui.internal.editor.jad.form.JADAttributesRegistry;
import org.eclipse.mtj.ui.internal.editor.jad.form.JADFormEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * An editor part page that may be added to the JAD editor.
 * 
 * @author Craig Setera
 */
public abstract class JADPropertiesEditorPage extends AbstractJADEditorPage {

    // The descriptors being edited
    private DescriptorPropertyDescription[] descriptors;

    // The field editors in use
    protected FieldEditor[] fieldEditors;

    // To handle the change from the internal ComboFieldEditor used prior
    // to 3.3. This probably should have been better handled in the first
    // place, but now that it is moving to an externally available class,
    // it isn't worth replicating
    private Class<?> comboFieldEditorClass;
    private Constructor<?> comboEditorConstructor;

    /**
     * Constructor
     * 
     * @param id
     * @param title
     */
    public JADPropertiesEditorPage(String id, String title) {
        super(id, title);
    }

    /**
     * Constructor
     * 
     * @param editor
     * @param id
     * @param title
     */
    public JADPropertiesEditorPage(JADFormEditor editor, String id, String title) {
        super(editor, id, title);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    public void doSave(IProgressMonitor monitor) {
        monitor.setTaskName(getTitle());

        for (int i = 0; i < fieldEditors.length; i++) {
            fieldEditors[i].store();
        }
        setDirty(false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    protected void createFormContent(IManagedForm managedForm) {
        FormToolkit toolkit = managedForm.getToolkit();

        Composite composite = createSectionComposite(managedForm);
        composite.setLayout(new GridLayout(2, false));
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
        DescriptorPropertyDescription[] theDescriptors = getDescriptors();
        fieldEditors = new FieldEditor[theDescriptors.length];
        for (int i = 0; i < theDescriptors.length; i++) {
            switch (theDescriptors[i].getDataType()) {
            case DescriptorPropertyDescription.DATATYPE_INT:
                fieldEditors[i] = createIntegerFieldEditor(toolkit, composite,
                        theDescriptors[i]);
                break;

            case DescriptorPropertyDescription.DATATYPE_LIST:
                fieldEditors[i] = createComboFieldEditor(toolkit, composite,
                        theDescriptors[i]);
                break;

            case DescriptorPropertyDescription.DATATYPE_URL:
            case DescriptorPropertyDescription.DATATYPE_STRING:
            default:
                fieldEditors[i] = createStringFieldEditor(toolkit, composite,
                        theDescriptors[i]);
                break;
            }

            Label label = fieldEditors[i].getLabelControl(composite);
            toolkit.adapt(label, false, false);

            // Listen for property change events on the editor
            fieldEditors[i]
                    .setPropertyChangeListener(new IPropertyChangeListener() {
                        public void propertyChange(PropertyChangeEvent event) {
                            if (event.getProperty().equals(FieldEditor.VALUE)) {
                                setDirty(true);
                            }
                        }
                    });
        }

        // Adapt the Combo instances...
        Control[] children = composite.getChildren();
        for (int i = 0; i < children.length; i++) {
            Control control = children[i];
            if (control instanceof Combo) {
                toolkit.adapt(control, false, false);
            }
        }

        updateEditComponents();
        addContextHelp(composite);
    }

    protected void addContextHelp(Composite c) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#setFocus()
     */
    public void setFocus() {
        if (fieldEditors.length > 0) {
            fieldEditors[0].setFocus();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
     */
    protected void setInput(IEditorInput input) {
        super.setInput(input);
        updateEditComponents();
        setDirty(false);
    }

    /**
     * Create a new combo field editor.
     * 
     * @param toolkit
     * @param composite
     * @param description
     * @return
     */
    private FieldEditor createComboFieldEditor(FormToolkit toolkit,
            Composite composite, DescriptorPropertyDescription description) {
        if (comboFieldEditorClass == null) {
            initializeComboFieldEditorSupport();
        }

        ListDescriptorPropertyDescription listDescription = (ListDescriptorPropertyDescription) description;

        FieldEditor editor = null;
        try {
            editor = (FieldEditor) comboEditorConstructor
                    .newInstance(new Object[] {
                            listDescription.getPropertyName(),
                            listDescription.getDisplayName(),
                            listDescription.getNamesAndValues(), composite });

        } catch (IllegalArgumentException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        } catch (InstantiationException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        } catch (IllegalAccessException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        } catch (InvocationTargetException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        }

        return editor;
    }

    /**
     * Create a new integer field editor.
     * 
     * @param toolkit
     * @param parent
     * @param descriptor
     * @return
     */
    private IntegerFieldEditor createIntegerFieldEditor(FormToolkit toolkit,
            Composite parent, DescriptorPropertyDescription descriptor) {
        IntegerFieldEditor integerEditor = new IntegerFieldEditor(descriptor
                .getPropertyName(), descriptor.getDisplayName(), parent);
        toolkit.adapt(integerEditor.getTextControl(parent), true, true);

        return integerEditor;
    }

    /**
     * Create a new String field editor.
     * 
     * @param toolkit
     * @param parent
     * @param descriptor
     * @return
     */
    private StringFieldEditor createStringFieldEditor(FormToolkit toolkit,
            Composite parent, DescriptorPropertyDescription descriptor) {
        StringFieldEditor editor = new StringFieldEditor(descriptor
                .getPropertyName(), descriptor.getDisplayName(), parent);
        toolkit.adapt(editor.getTextControl(parent), true, true);

        return editor;
    }

    /**
     * Resolve the appropriate combo field editor class to be used dependent on
     * the version.
     */
    private void initializeComboFieldEditorSupport() {
        String[] names = new String[] {
                "org.eclipse.jdt.internal.debug.ui.launcher.ComboFieldEditor",
                "org.eclipse.jface.preference.ComboFieldEditor", };

        for (int i = 0; (comboFieldEditorClass == null) && (i < names.length); i++) {
            String name = names[i];
            try {
                comboFieldEditorClass = Class.forName(name);
                comboEditorConstructor = comboFieldEditorClass
                        .getConstructors()[0];
            } catch (ClassNotFoundException e) {
            } catch (SecurityException e) {
                MTJCorePlugin.log(IStatus.ERROR, e);
            }
        }
    }

    /**
     * Update the application descriptor the components are handling
     */
    private void updateEditComponents() {
        if (fieldEditors != null) {
            IPreferenceStore store = getPreferenceStore();
            for (int i = 0; i < fieldEditors.length; i++) {
                FieldEditor fieldEditor = fieldEditors[i];
                fieldEditor.setPreferenceStore(store);
                fieldEditor.load();
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#getSectionDescription()
     */
    protected String getSectionDescription() {
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#getSectionTitle()
     */
    protected String getSectionTitle() {
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#editorInputChanged()
     */
    public void editorInputChanged() {
        updateEditComponents();
    }

    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#isManagingProperty(java.lang.String)
     */
    public boolean isManagingProperty(String property) {
        boolean manages = false;
        for (int i = 0; i < getDescriptors().length; i++) {
            DescriptorPropertyDescription desc = getDescriptors()[i];
            if (property.equals(desc.getPropertyName())) {
                manages = true;
                break;
            }
        }
        return manages;
    }

    public DescriptorPropertyDescription[] getDescriptors() {
        if (descriptors == null) {
            descriptors = doGetDescriptors();
        }
        return descriptors;
    }

    /**
     * sub class may override this method to provide its own descriptor provide
     * strategy
     * 
     * @return
     */
    protected DescriptorPropertyDescription[] doGetDescriptors() {
        return JADAttributesRegistry.getJADAttrDescriptorsByPage(getId());
    }

}
