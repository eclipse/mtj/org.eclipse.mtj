/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.editor.jad.source;

import java.util.ResourceBundle;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.texteditor.ContentAssistAction;
import org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds;

/**
 * The standard text editor for Java Application Descriptors
 * 
 * @author Diego Madruga Sandin
 */
public class JADSourceEditor extends TextEditor {

    /**
     * 
     */
    private static String RES_BUNDLE_LOCATION = "org.eclipse.mtj.ui.internal.editor.jad.source.contentassist.JADSourceEditorMessages"; //$NON-NLS-1$

    /**
     * 
     */
    private static ResourceBundle fgBundleForConstructedKeys = ResourceBundle
            .getBundle(RES_BUNDLE_LOCATION);

    /**
     * @return
     */
    public static ResourceBundle getBundleForConstructedKeys() {
        return fgBundleForConstructedKeys;
    }

    /**
     * Creates a new Application Descriptor editor
     */
    public JADSourceEditor() {
        setSourceViewerConfiguration(new JADSourceViewerConfiguration());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.editors.text.TextEditor#createActions()
     */
    @Override
    protected void createActions() {
        super.createActions();
        
        /* Add action to allow the user to invoke Content Assist */
        Action action = new ContentAssistAction(getBundleForConstructedKeys(),
                "ContentAssistProposal.", this);
        String id = ITextEditorActionDefinitionIds.CONTENT_ASSIST_PROPOSALS;
        action.setActionDefinitionId(id);
        setAction("ContentAssistProposal", action);
        markAsStateDependentAction("ContentAssistProposal", true);
    }
}
