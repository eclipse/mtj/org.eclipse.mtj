/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *      Gang Ma      (Sybase)	- Rename the lable "Group:" to "SDK:"
 */
package org.eclipse.mtj.ui.devices;

import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.device.IDeviceRegistryListener;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.ui.internal.preferences.DeviceManagementPreferencePage;
import org.eclipse.mtj.ui.internal.viewers.LabelProviderViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

/**
 * A device selector provides user interface functionality for selecting a
 * device from the registry.
 * 
 * @author Craig Setera
 */
public class DeviceSelector {
    private static final Object[] NO_ELEMENTS = new Object[0];

    /**
     * Content provider wrapped around the device groups in the registry
     * 
     * @author Craig Setera
     */
    private static class DeviceGroupsContentProvider implements
            IStructuredContentProvider {
        public void dispose() {
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            try {
                List groups = DeviceRegistry.singleton.getDeviceGroups();
                elements = (Object[]) groups.toArray(new Object[groups.size()]);
            } catch (PersistenceException e) {
                MTJCorePlugin.log(IStatus.WARNING,
                        "Error retrieving device groups.", e);
            }

            return elements;
        }
    }

    private static class DeviceGroupNameLabelProvider extends LabelProvider {
        public String getText(Object element) {
            return (String) element;
        }
    }

    private static class DeviceContentProvider implements
            IStructuredContentProvider {
        public void dispose() {
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            if (inputElement != null) {
                String groupName = (String) inputElement;

                try {
                    List deviceList = DeviceRegistry.singleton
                            .getDevices(groupName);
                    if (deviceList != null) {
                        elements = (Object[]) deviceList
                                .toArray(new Object[deviceList.size()]);
                    }
                } catch (PersistenceException e) {
                    MTJCorePlugin.log(IStatus.WARNING,
                            "Error retrieving devices for group", e);
                }
            }

            return elements;
        }
    }

    private static class DeviceLabelProvider extends LabelProvider {
        public String getText(Object element) {
            IDevice device = (IDevice) element;
            return device.getName();
        }
    }

    // Listener for changes in the registry
    private IDeviceRegistryListener registryListener;

    private Button manageDevicesButton;
    private ComboViewer groupViewer;
    private ComboViewer deviceViewer;
    private ISelectionChangedListener selectionChangedListener;

    /**
     * Fill in the user interface components into the parent. Specify whether to
     * use a device group box in the layout.
     * 
     * @param parent
     * @param includeGroup
     */
    public void createContents(Composite parent, boolean includeGroup) {
        Composite composite = null;
        if (includeGroup) {
            Group group = new Group(parent, SWT.NONE);
            group.setText("Device");

            composite = group;
        } else {
            composite = new Composite(parent, SWT.NONE);
        }

        setCompositeLayout(parent, composite);
        createSelectionControls(composite);
        createDeviceManagementControls(composite);

        addRegistryListener();
        setInitialState();
    }

    /**
     * Dispose as necessary.
     */
    public void dispose() {
        DeviceRegistry.singleton.removeRegistryListener(registryListener);
    }

    /**
     * Return the device selected by the user or <code>null</code> if the user
     * has not yet selected a device.
     * 
     * @return
     */
    public IDevice getSelectedDevice() {
        IStructuredSelection selection = (IStructuredSelection) deviceViewer
                .getSelection();
        return (IDevice) selection.getFirstElement();
    }

    /**
     * Set the controls to the specified state of enablement.
     * 
     * @param enabled
     */
    public void setEnabled(boolean enabled) {
        manageDevicesButton.setEnabled(enabled);
        groupViewer.getCombo().setEnabled(enabled);
        deviceViewer.getCombo().setEnabled(enabled);
    }

    /**
     * Select the specified device within the selector if possible. This method
     * is not guaranteed to make a valid selection depending on the state of the
     * device registry.
     * 
     * @param device
     */
    public void setSelectedDevice(IDevice device) {
        IDevice currentlySelected = getSelectedDevice();

        if (!areDevicesEqual(device, currentlySelected)) {
            if (device != null) {
                setViewerSelection(groupViewer, device.getGroupName());
                setViewerSelection(deviceViewer, device);
            } else {
                groupViewer.setSelection(null);
                deviceViewer.setSelection(null);
            }
        }
    }

    /**
     * Set the listener for changes in the device selection.
     * 
     * @param listener
     */
    public void setSelectionChangedListener(ISelectionChangedListener listener) {
        selectionChangedListener = listener;
    }

    /**
     * Add a listener for updated in the device registry.
     */
    private void addRegistryListener() {
        registryListener = new IDeviceRegistryListener() {
            public void deviceAdded(IDevice device) {
                setInitialState();
            }

            public void deviceRemoved(IDevice device) {
                setInitialState();
            }
        };

        DeviceRegistry.singleton.addRegistryListener(registryListener);
    }

    /**
     * Return a boolean indicating whether there are any devices registered.
     * 
     * @return
     * @throws PersistenceException
     */
    private boolean areDevicesRegistered() throws PersistenceException {
        return (DeviceRegistry.singleton.getDeviceCount() > 0);
    }

    /**
     * Return a boolean indicating equality, while accounting for nulls.
     * 
     * @param device1
     * @param device2
     * @return
     */
    private boolean areDevicesEqual(IDevice device1, IDevice device2) {
        boolean equal = false;

        if ((device1 == null) && (device2 == null)) {
            equal = true;
        } else if ((device1 != null) && (device2 != null)) {
            equal = device1.equals(device2);
        }

        return equal;
    }

    /**
     * Create the device groups combo viewer.
     * 
     * @param parent
     * @param styles
     * @return
     */
    private ComboViewer createDeviceGroupsViewer(Composite parent, int styles) {
        ComboViewer viewer = new ComboViewer(parent, styles);
        viewer.setContentProvider(new DeviceGroupsContentProvider());
        viewer.setLabelProvider(new DeviceGroupNameLabelProvider());
        viewer.setSorter(new LabelProviderViewerSorter());
        viewer.setInput(new Object());

        return viewer;
    }

    /**
     * Create the device names combo viewer.
     * 
     * @param parent
     * @param styles
     * @return
     */
    private ComboViewer createDeviceNamesViewer(Composite parent, int styles) {
        ComboViewer viewer = new ComboViewer(parent, styles);
        viewer.setContentProvider(new DeviceContentProvider());
        viewer.setLabelProvider(new DeviceLabelProvider());
        viewer.setSorter(new LabelProviderViewerSorter());

        return viewer;
    }

    /**
     * Match the child layout to the parent layout, filling all available
     * columns.
     * 
     * @param parent
     * @param child
     * @return
     */
    private void setCompositeLayout(Composite parent, Composite child) {
        // Force the group to take up all of the columns in the layout
        int columns = 1;
        Object layout = parent.getLayout();
        if (layout instanceof GridLayout) {
            columns = ((GridLayout) layout).numColumns;
        }

        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = columns;
        child.setLayoutData(gd);
        child.setLayout(new GridLayout(2, false));
    }

    /**
     * Create the controls to manage devices.
     * 
     * @param composite
     */
    private void createDeviceManagementControls(Composite composite) {
        Composite manageComposite = new Composite(composite, SWT.NONE);
        manageComposite.setLayout(new GridLayout(1, true));
        GridData compositeData = new GridData();
        compositeData.verticalAlignment = SWT.CENTER;
        manageComposite.setLayoutData(compositeData);

        manageDevicesButton = new Button(manageComposite, SWT.PUSH);
        manageDevicesButton.setText("Manage Devices...");
        manageDevicesButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                Shell shell = e.widget.getDisplay().getActiveShell();
                PreferenceManager manager = PlatformUI.getWorkbench()
                        .getPreferenceManager();
                PreferenceDialog dialog = new PreferenceDialog(shell, manager);
                dialog.setSelectedNode(DeviceManagementPreferencePage.ID);
                dialog.open();
            }
        });
    }

    /**
     * Create the device selection controls.
     * 
     * @param composite
     */
    private void createSelectionControls(Composite composite) {
        Composite comboComposite = new Composite(composite, SWT.NONE);
        comboComposite.setLayout(new GridLayout(2, false));
        comboComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

        (new Label(comboComposite, SWT.NONE)).setText("SDK: ");
        groupViewer = createDeviceGroupsViewer(comboComposite, SWT.READ_ONLY);
        groupViewer.getCombo().setLayoutData(
                new GridData(GridData.FILL_HORIZONTAL));
        groupViewer
                .addSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        deviceGroupChanged();
                        if (selectionChangedListener != null) {
                            selectionChangedListener.selectionChanged(event);
                        }
                    }
                });

        (new Label(comboComposite, SWT.NONE)).setText("Device: ");
        deviceViewer = createDeviceNamesViewer(comboComposite, SWT.READ_ONLY);
        deviceViewer.getCombo().setLayoutData(
                new GridData(GridData.FILL_HORIZONTAL));
        deviceViewer
                .addSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        if (selectionChangedListener != null) {
                            selectionChangedListener.selectionChanged(event);
                        }
                    }
                });
    }

    /**
     * The device group has been changed. We need to update the available
     * devices.
     */
    private void deviceGroupChanged() {
        groupViewer.refresh();

        IStructuredSelection selection = (IStructuredSelection) groupViewer
                .getSelection();
        String groupName = (String) selection.getFirstElement();
        deviceViewer.setInput(groupName);

        try {
            List groupDevices = DeviceRegistry.singleton.getDevices(groupName);
            if ((groupDevices != null) && (groupDevices.size() > 0)) {
                deviceViewer.getCombo().select(0);
            }
        } catch (PersistenceException e) {
            MTJCorePlugin.log(IStatus.WARNING,
                    "Error retrieving devices for group " + groupName, e);
        }
    }

    /**
     * Return the device to be selected by the viewers based on a number of
     * factors.
     * 
     * @return
     * @throws PersistenceException
     */
    private IDevice getDeviceToSelect() throws PersistenceException {
        IDevice device = null;

        if (areDevicesRegistered()) {
            // Attempt to find a device to use in setting up the
            // current selection.
            device = getSelectedDevice();

            if (device == null) {
                device = DeviceRegistry.singleton.getDefaultDevice();
            }

            if (device == null) {
                List allDevices = DeviceRegistry.singleton.getAllDevices();
                device = (IDevice) allDevices.get(0);
            }
        }

        return device;
    }

    /**
     * Set the initial state of the combo viewers.
     */
    private void setInitialState() {
        try {
            IDevice device = getDeviceToSelect();
            setSelectedDevice(device);
        } catch (PersistenceException e) {
            MTJCorePlugin.log(IStatus.WARNING,
                    "Error retrieving device to select", e);
        }
    }

    /**
     * Set the selection into the specified viewer.
     * 
     * @param viewer
     * @param selectedObject
     */
    private void setViewerSelection(Viewer viewer, Object selectedObject) {
        StructuredSelection selection = new StructuredSelection(selectedObject);
        viewer.setSelection(selection, true);
    }
}
