/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Refactoring the page to add expansibilities
 */
package org.eclipse.mtj.ui.internal.editor.jad.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.internal.utils.MidletSelectionDialogCreator;
import org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.SelectionDialog;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * A property page editor for the MIDlets in the MIDlet suite.
 * 
 * @author Craig Setera
 */
public class JADMidletsEditorPage extends AbstractJADEditorPage {

    public static final String ID = "midlets";

    // Column property names
    private static final String PROP_NAME = "name";
    private static final String PROP_ICON = "icon";
    private static final String PROP_CLASS = "class";

    // All of the properties in order
    private static final String[] PROPERTIES = new String[] { PROP_NAME,
            PROP_ICON, PROP_CLASS };
    private static final List PROPERTY_LIST = Arrays.asList(PROPERTIES);

    /**
     * Definition of a MIDlet.
     */
    static class MidletDefinition {
        public String[] fields;

        MidletDefinition(String definitionString) {
            fields = new String[3];
            String[] tokens = tokenize(definitionString, ',');

            for (int i = 0; i < 3; i++) {
                fields[i] = (i > tokens.length) ? "" : tokens[i];
            }
        }

        MidletDefinition(String name, String icon, String className) {
            fields = new String[3];

            fields[0] = name;
            fields[1] = icon;
            fields[2] = className;
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < 3; i++) {
                if (i != 0)
                    sb.append(",");
                sb.append(fields[i]);
            }

            return sb.toString();
        }

        /**
         * Tokenize the specified string, returning the strings within that
         * string.
         * 
         * @param definition
         * @return
         */
        private String[] tokenize(String string, char delimiter) {
            ArrayList tokens = new ArrayList();

            int offset = 0;
            int delimiterIndex = 0;

            do {
                delimiterIndex = string.indexOf(delimiter, offset);
                if (delimiterIndex == -1) {
                    tokens.add(string.substring(offset));
                } else {
                    tokens.add(string.substring(offset, delimiterIndex));
                    offset = delimiterIndex + 1;
                }
            } while (delimiterIndex != -1);

            return (String[]) tokens.toArray(new String[tokens.size()]);
        }
    }

    /**
     * Implementation of the ICellModifier interface.
     */
    private class CellModifier implements ICellModifier {
        /**
         * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object,
         *      java.lang.String)
         */
        public boolean canModify(Object element, String property) {
            // All columns are modifiable
            return true;
        }

        /**
         * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object,
         *      java.lang.String)
         */
        public Object getValue(Object element, String property) {
            Object value = null;

            if (element instanceof MidletDefinition) {
                MidletDefinition midletDef = (MidletDefinition) element;

                int fieldIndex = getFieldIndex(property);
                if (fieldIndex != -1) {
                    value = midletDef.fields[fieldIndex];
                }
            }

            return value;
        }

        /**
         * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object,
         *      java.lang.String, java.lang.Object)
         */
        public void modify(Object element, String property, Object value) {
            if (element instanceof TableItem) {
                Object data = ((TableItem) element).getData();
                String newValue = (String) value;

                if (data instanceof MidletDefinition) {
                    int fieldIndex = getFieldIndex(property);
                    MidletDefinition midletDef = (MidletDefinition) data;

                    if (fieldIndex != -1) {
                        updateField(midletDef, property, fieldIndex, newValue);
                    }
                }
            }
        }

        /**
         * Return the field index to match the specified property name. Returns
         * <code>-1</code> if the property is not recognized.
         * 
         * @param property
         * @return
         */
        private int getFieldIndex(String property) {
            return PROPERTY_LIST.indexOf(property);
        }

        /**
         * Update the specified field as necessary.
         * 
         * @param midletDef
         * @param property
         * @param fieldIndex
         * @param newValue
         */
        private void updateField(MidletDefinition midletDef, String property,
                int fieldIndex, String newValue) {
            if (!midletDef.fields[fieldIndex].equals(newValue)) {
                midletDef.fields[fieldIndex] = newValue;
                setDirty(true);
                tableViewer.update(midletDef, new String[] { property });
            }
        }
    }

    /**
     * A cell editor implementation that allows for selection of a MIDlet class.
     */
    private class MidletCellEditor extends DialogCellEditor {

        /** 
         * Construct a new cell editor 
         */
        MidletCellEditor(Composite parent) {
            super(parent);
        }

        /**
         * @see org.eclipse.jface.viewers.DialogCellEditor#openDialogBox(org.eclipse.swt.widgets.Control)
         */
        protected Object openDialogBox(Control cellEditorWindow) {
            Object value = null;

            try {
                IJavaProject javaProject = getJavaProject();
                SelectionDialog dialog = MidletSelectionDialogCreator
                        .createMidletSelectionDialog(cellEditorWindow
                                .getShell(), getSite().getPage()
                                .getWorkbenchWindow(), javaProject, false);

                if (dialog.open() == SelectionDialog.OK) {
                    Object[] results = dialog.getResult();
                    if ((results != null) && (results.length > 0)) {
                        IType type = (IType) results[0];
                        if (type != null) {
                            value = type.getFullyQualifiedName();
                        }
                    }
                }
            } catch (JavaModelException e) {
                MTJCorePlugin.log(IStatus.ERROR, "openDialogBox", e);
            }

            return value;
        }
    }

    /**
     * Implementation of the table's content provider.
     */
    private class TableContentProvider implements IStructuredContentProvider {
        /**
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            return (Object[]) midlets.toArray(new Object[midlets.size()]);
        }

        /**
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /**
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer,
         *      java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    /**
     * Implementation of the table's label provider.
     */
    private static class TableLabelProvider extends LabelProvider implements
            ITableLabelProvider {
        /**
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
         *      int)
         */
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        /**
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
         *      int)
         */
        public String getColumnText(Object element, int columnIndex) {
            return ((MidletDefinition) element).fields[columnIndex];
        }
    }

    /** 
     * The prefix of all MIDlet definition properties 
     */
    public static final String MIDLET_PREFIX = "MIDlet-";

    // The property names currently defining MIDlets
    private ArrayList midlets;

    // The table viewer in use
    private TableViewer tableViewer;

    // Buttons
    private Button addButton;
    private Button removeButton;

    // The original MIDlet count as stored
    private int storedMidletCount;

    /**
     * Constructor
     */
    public JADMidletsEditorPage(JADFormEditor editor, String title) {
        super(editor, ID, title);
        midlets = new ArrayList();
    }
    public JADMidletsEditorPage() {
        super(ID, getResourceString("editor.jad.tab.midlets"));
        midlets = new ArrayList();
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    public void doSave(IProgressMonitor monitor) {
        IPreferenceStore store = getPreferenceStore();

        // Add the MIDlets to the store
        int i;
        int currentMidletCount = midlets.size();

        for (i = 0; i < currentMidletCount; i++) {
            MidletDefinition def = (MidletDefinition) midlets.get(i);
            store.setValue(MIDLET_PREFIX + (i + 1), def.toString());
        }

        for (; i < storedMidletCount; i++) {
            store.setToDefault(MIDLET_PREFIX + (i + 1));
        }

        storedMidletCount = currentMidletCount;
        setDirty(false);
    }

    /**
     * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    protected void createFormContent(IManagedForm managedForm) {
        FormToolkit toolkit = managedForm.getToolkit();

        // Set the top-level layout
        Composite parent = createSectionComposite(managedForm);
        parent.setLayoutData(new GridData(GridData.FILL_BOTH));
        parent.setLayout(new GridLayout(2, false));
        new Label(parent, SWT.NONE);
        new Label(parent, SWT.NONE);

        createTableViewer(toolkit, parent);
        createButtons(toolkit, parent);

        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_JADMidletsEditorPage");
    }

    /**
     * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
     */
    public void setFocus() {
        tableViewer.getTable().setFocus();
    }

    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#getSectionTitle()
     */
    protected String getSectionTitle() {
        return "MIDlets";
    }

    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#getSectionDescription()
     */
    protected String getSectionDescription() {
        return "Define the MIDlets that make up the MIDlet Suite";
    }

    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#editorInputChanged()
     */
    public void editorInputChanged() {
        updateMidletProperties();
    }

    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#isManagingProperty(java.lang.String)
     */
    public boolean isManagingProperty(String property) {
        boolean manages = property.startsWith(MIDLET_PREFIX);
        if (manages) {
            String value = property.substring(MIDLET_PREFIX.length());
            try {
                Integer.parseInt(value);
            } catch (NumberFormatException e) {
                manages = false;
            }
        }

        return manages;
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
     */
    protected void setInput(IEditorInput input) {
        super.setInput(input);
        if (tableViewer != null) {
            tableViewer.setInput(input);
        }

        setDirty(false);
        updateMidletProperties();
    }

    /**
     * Add a new item to the table.
     */
    private void addItem() {
        MidletDefinition midletDefinition = new MidletDefinition("New MIDlet",
                "", "");
        midlets.add(midletDefinition);
        tableViewer.refresh();
        setDirty(true);
    }

    /**
     * Create the add and remove buttons to the composite.
     * 
     * @param toolkit the Eclipse Form's toolkit
     * @param parent
     */
    private void createButtons(FormToolkit toolkit, Composite parent) {
        Composite composite = toolkit.createComposite(parent);
        FillLayout layout = new FillLayout();
        layout.type = SWT.VERTICAL;
        composite.setLayout(layout);

        addButton = toolkit.createButton(composite,
                getResourceString("editor.button.add"), SWT.PUSH);
        addButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent se) {
                addItem();
            }
        });

        toolkit.createLabel(composite, "");

        removeButton = toolkit.createButton(composite,
                getResourceString("editor.button.remove"), SWT.PUSH);
        removeButton.setEnabled(false);
        removeButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent se) {
                removeSelectedItems();
            }
        });
    }

    /**
     * Create the table viewer for this editor.
     * 
     * @param toolkit The Eclipse form's toolkit
     * @param parent
     */
    private void createTableViewer(FormToolkit toolkit, Composite parent) {

        String[] columns = new String[] {
                getResourceString("property.jad.midlet.name"),
                getResourceString("property.jad.midlet.icon"),
                getResourceString("property.jad.midlet.class"), };

        // Setup the table
        int styles = SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER
                | SWT.FULL_SELECTION;
        Table table = toolkit.createTable(parent, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        table.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                TableItem selected = (TableItem) e.item;
                removeButton.setEnabled(selected.getParent()
                        .getSelectionCount() > 0);
            }
        });
        tableViewer = new TableViewer(table);

        // Set the table layout on the table
        TableLayout layout = new TableLayout();

        int width = 100 / columns.length;
        for (int i = 0; i < columns.length; i++) {
            TableColumn column = new TableColumn(table, SWT.NONE);
            column.setText(columns[i]);
            layout.addColumnData(new ColumnWeightData(width));
        }
        table.setLayout(layout);

        // Set the content providers
        tableViewer.setContentProvider(new TableContentProvider());
        tableViewer.setLabelProvider(new TableLabelProvider());

        // Wire up the cell modification handling
        tableViewer.setCellModifier(new CellModifier());
        tableViewer.setColumnProperties(PROPERTIES);
        tableViewer.setCellEditors(new CellEditor[] {
                new TextCellEditor(table), new TextCellEditor(table),
                // TODO new IconCellEditor(table),
                new MidletCellEditor(table), });

        // Get some data into the viewer
        tableViewer.setInput(getEditorInput());
        tableViewer.refresh();
    }

    /**
     * Return the java project holding the JAD file.
     * 
     * @return
     */
    private IJavaProject getJavaProject() {
        IProject project = getProject();
        return (project == null) ? null : JavaCore.create(project);
    }

    /**
     * Return the project that holds the JAD file being edited.
     * 
     * @return
     */
    private IProject getProject() {
        IProject project = null;

        IEditorInput input = getEditorInput();
        if (input instanceof IFileEditorInput) {
            IFile file = ((IFileEditorInput) input).getFile();
            project = file.getProject();
        }

        return project;
    }

    /**
     * Remove the items currently selected within the table.
     */
    private void removeSelectedItems() {
        int[] indices = tableViewer.getTable().getSelectionIndices();

        for (int i = indices.length; i > 0; i--) {
            int index = indices[i - 1];
            midlets.remove(index);
        }

        setDirty(true);
        tableViewer.refresh();
    }

    /**
     * Update the MIDlet properties from the current preference store.
     */
    private void updateMidletProperties() {
        midlets.clear();
        IPreferenceStore store = getPreferenceStore();

        // This is sort of ugly, but IPreferenceStore does not
        // allow getting the complete list of preference keys
        for (int i = 1; i < 1000; i++) {
            String propName = MIDLET_PREFIX + i;

            if (store.contains(propName)) {
                String propValue = store.getString(propName);
                midlets.add(new MidletDefinition(propValue));
            } else {
                break;
            }
        }

        storedMidletCount = midlets.size();
        if (tableViewer != null)
            tableViewer.refresh();
    }
    /**
     * Get a string value from the resource bundle
     * 
     * @param key
     * @return
     */
    private static String getResourceString(String key) {
        return MTJUIStrings.getString(key);
    }
}
