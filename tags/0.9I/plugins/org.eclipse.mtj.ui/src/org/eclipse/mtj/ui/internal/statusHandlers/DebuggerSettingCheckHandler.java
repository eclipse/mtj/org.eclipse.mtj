/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma 		(Sybase) - Initial implementation
 *     
 */
package org.eclipse.mtj.ui.internal.statusHandlers;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.jdt.debug.core.JDIDebugModel;
import org.eclipse.jdt.debug.ui.JavaDebugUtils;
import org.eclipse.jdt.internal.debug.ui.IJDIPreferencesConstants;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.mtj.ui.IMTJUIConstants;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
/**
 * Status handler for configuring debug settings.
 * @author gma
 *
 */
public class DebuggerSettingCheckHandler implements IStatusHandler {

	public Object handleStatus(IStatus status, Object source)
			throws CoreException {
		configDebuggerSetting();
		//always continue debugging
		return Boolean.TRUE;
	}
	
	/**
	 * The preference key of debug setting warning, if user select
	 * "don't warn me again", the preference will be stored.
	 */
	public static final String PREF_DEBUGSETTING_WARNING = IMTJUIConstants.PLUGIN_ID
			+ ".debugSettingWarning";
	/**
	 * Debugger time out must be set more than 15000
	 */
	public static final int MIN_DEBUGGERTIMEOUT_SETTING = 15000;
	/**
	 * Suspend execution on uncaught exception must not be checked
	 */
	public static final boolean CORRECT_SUSPENDONUNCAUGHTEXP_SETTING = false;
	/**
	 * Suspend execution on compilation errors must not be checked
	 */
	public static final boolean CORRECT_SUSPENDONCOMPILEERR_SETTING = false;

	

	/**
	 * configure the debug setting for user
	 */
	public void configDebuggerSetting() {
		
		IStatus debugSettingStatus = debugSettingSatisfied();
		// only need to configure debugger setting while the setting is not
		// satisfied
		if (debugSettingStatus.isOK())
			return;

		String preferenceKey = PREF_DEBUGSETTING_WARNING;
		String debugWarningDesc = MTJUIPlugin.getDefault().getPreferenceStore()
				.getString(preferenceKey);
		// if the user have ever checked "Don't warn me again",don't warn that
		if (ErrorDialogWithToggle.NO_WARING.equals(debugWarningDesc)) {
			return;
		}
		// show the warning dialog
		Shell shell = getShell();
		ErrorDialogWithToggle dialog = new ErrorDialogWithToggle(shell,
				MTJUIStrings.getString("debugWarning.title"), null,
				debugSettingStatus, preferenceKey, MTJUIStrings
						.getString("debugWarning.toggleMessage"), MTJUIPlugin
						.getDefault().getPreferenceStore());
		dialog.open();

		boolean answer = (dialog.getReturnCode() == IDialogConstants.YES_ID);
		if (answer) {
			JDIDebugModel.getPreferences().setValue(
					JDIDebugModel.PREF_REQUEST_TIMEOUT,
					MIN_DEBUGGERTIMEOUT_SETTING);
			JavaDebugUtils
					.getPreferenceStore()
					.setValue(
							IJDIPreferencesConstants.PREF_SUSPEND_ON_UNCAUGHT_EXCEPTIONS,
							CORRECT_SUSPENDONUNCAUGHTEXP_SETTING);
			JavaDebugUtils
					.getPreferenceStore()
					.setValue(
							IJDIPreferencesConstants.PREF_SUSPEND_ON_COMPILATION_ERRORS,
							CORRECT_SUSPENDONCOMPILEERR_SETTING);

		}

	}

	/**
	 * check if the debug setting is satisfied if it is satisfied for debugging
	 * return status ok else return status warning.
	 * 
	 * @return IStatus
	 */
	private IStatus debugSettingSatisfied() {
		boolean suspendOnUnCaughtExp = JavaDebugUtils
				.getPreferenceStore()
				.getBoolean(
						IJDIPreferencesConstants.PREF_SUSPEND_ON_UNCAUGHT_EXCEPTIONS);
		boolean suspendOnCompileErr = JavaDebugUtils
				.getPreferenceStore()
				.getBoolean(
						IJDIPreferencesConstants.PREF_SUSPEND_ON_COMPILATION_ERRORS);
		int currReqTimeout = JDIDebugModel.getPreferences().getInt(
				JDIDebugModel.PREF_REQUEST_TIMEOUT);

		if (!suspendOnUnCaughtExp && !suspendOnCompileErr
				&& currReqTimeout >= MIN_DEBUGGERTIMEOUT_SETTING) {
			return new Status(IStatus.OK, IMTJUIConstants.PLUGIN_ID, "");
		} else {
			MultiStatus warningStatus = new MultiStatus(
					IMTJUIConstants.PLUGIN_ID, IStatus.WARNING, MTJUIStrings
							.getString("debugWarning.warningMessage"), null);

			if (suspendOnUnCaughtExp) {
				warningStatus
						.add(new Status(
								IStatus.WARNING,
								IMTJUIConstants.PLUGIN_ID,
								MTJUIStrings
										.getString("debugWarning.suspendOnUnCaughtExpWarning")));
			}
			if (suspendOnCompileErr) {
				warningStatus
						.add(new Status(
								IStatus.WARNING,
								IMTJUIConstants.PLUGIN_ID,
								MTJUIStrings
										.getString("debugWarning.suspendOnCompileErrWarning")));

			}
			if (currReqTimeout < MIN_DEBUGGERTIMEOUT_SETTING) {
				warningStatus
						.add(new Status(
								IStatus.WARNING,
								IMTJUIConstants.PLUGIN_ID,
								MTJUIStrings
										.getString(
												"debugWarning.reqestTimeoutWarning",
												new Integer[] { MIN_DEBUGGERTIMEOUT_SETTING })));
			}
			return warningStatus;
		}
	}

	/**
	 * Get the active workbench window's shell.
	 * 
	 * @return
	 */
	private Shell getShell() {
		Shell shell = null;

		IWorkbenchWindow workbenchWindow = MTJUIPlugin.getDefault()
				.getWorkbench().getActiveWorkbenchWindow();
		if (workbenchWindow != null) {
			shell = workbenchWindow.getShell();
		}

		return shell;
	}

	/**
	 * 
	 * this is a dialog with toggle message, "Yes" "No" and "Details" buttons.
	 * 
	 * @author gma
	 * 
	 */
	class ErrorDialogWithToggle extends ErrorDialog {

		public static final String NO_WARING = "neverWarning";

		/**
		 * The selected state of the toggle.
		 */
		private boolean toggleState;
		/**
		 * The preference key which is set by the toggle button.
		 * 
		 */
		private String fPreferenceKey = null;
		/**
		 * The message displayed to the user, with the toggle button
		 */
		private String fToggleMessage = null;
		private Button fToggleButton = null;
		/**
		 * The preference store which will be affected by the toggle button
		 */
		IPreferenceStore prefStore = null;

		public ErrorDialogWithToggle(Shell parentShell, String dialogTitle,
				String message, IStatus status, String preferenceKey,
				String toggleMessage, IPreferenceStore store) {
			super(parentShell, dialogTitle, message, status, IStatus.WARNING
					| IStatus.ERROR | IStatus.INFO);
			prefStore = store;
			fPreferenceKey = preferenceKey;
			fToggleMessage = toggleMessage;
		}

		protected Control createDialogArea(Composite parent) {
			Composite dialogComposite = (Composite) super
					.createDialogArea(parent);
			dialogComposite.setFont(parent.getFont());
			setToggleButton(createCheckButton(dialogComposite, fToggleMessage));
			applyDialogFont(dialogComposite);
			return dialogComposite;
		}

		/**
		 * Creates a button with the given label and sets the default
		 * configuration data.
		 */
		private Button createCheckButton(Composite parent, String label) {
			final Button button = new Button(parent, SWT.CHECK | SWT.LEFT);
			button.setText(label);

			GridData data = new GridData(SWT.NONE);
			data.horizontalSpan = 2;
			data.horizontalAlignment = GridData.CENTER;
			button.setLayoutData(data);
			button.setFont(parent.getFont());
			button.addSelectionListener(new SelectionAdapter() {

				public void widgetSelected(SelectionEvent e) {
					toggleState = button.getSelection();
				}
			});

			return button;
		}

		protected void buttonPressed(int buttonId) {

			if (buttonId != IDialogConstants.DETAILS_ID) {
				if (toggleState && prefStore != null && fPreferenceKey != null) {
					prefStore.setValue(fPreferenceKey, NO_WARING);
				}
				setReturnCode(buttonId);
				close();
			}
			super.buttonPressed(buttonId);
		}

		protected Button getToggleButton() {
			return fToggleButton;
		}

		protected void setToggleButton(Button button) {
			fToggleButton = button;
		}

		/**
		 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
		 */
		protected void createButtonsForButtonBar(Composite parent) {
			createButton(parent, IDialogConstants.YES_ID,
					IDialogConstants.YES_LABEL, true);
			createButton(parent, IDialogConstants.NO_ID,
					IDialogConstants.NO_LABEL, true);
			createDetailsButton(parent);

			getButton(IDialogConstants.YES_ID).setFocus();
		}
	}

}
