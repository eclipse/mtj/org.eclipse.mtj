/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Refactoring the page to add expansibilities                           
 */
package org.eclipse.mtj.ui.internal.editor.jad.form;

import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.jadEditor.JADPropertiesEditorPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;

/**
 * JAD editor page for handling the over the air properties.
 * 
 * @author Craig Setera
 */
public class JADOTAPropertiesEditorPage extends JADPropertiesEditorPage {
    
    public static final String ID = "ota";

   
    /**
     * Construct a new required properties editor.
     * 
     * @param editor
     */
    public JADOTAPropertiesEditorPage(JADFormEditor editor) {
        super(editor, ID, MTJUIStrings.getString("editor.jad.tab.ota"));
    }

    public JADOTAPropertiesEditorPage() {
        super(ID, MTJUIStrings.getString("editor.jad.tab.ota"));
    }
    
    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#getSectionTitle()
     */
    protected String getSectionTitle() {
        return "Over The Air Properties";
    }

    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#getSectionDescription()
     */
    protected String getSectionDescription() {
        return "Properties related to Over the Air deployment may be specified on this page";
    }

    protected void addContextHelp(Composite c) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(c,
                "org.eclipse.mtj.ui.help_JADOTAPropertiesEditorPage");
    }
}
