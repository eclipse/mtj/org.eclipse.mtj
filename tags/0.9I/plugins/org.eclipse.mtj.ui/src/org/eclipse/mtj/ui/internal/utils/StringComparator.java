/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards and added default serialVersionUID
 */
package org.eclipse.mtj.ui.internal.utils;

import java.io.Serializable;
import java.util.Comparator;

/**
 * A Comparator implementation that uses the toString method to do the
 * comparisons.
 * 
 * @author Craig Setera
 */
public class StringComparator implements Comparator, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    public int compare(Object o1, Object o2) {
        return o1.toString().compareTo(o2.toString());
    }
}
