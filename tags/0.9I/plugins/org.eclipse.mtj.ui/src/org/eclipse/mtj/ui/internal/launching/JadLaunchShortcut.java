/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.ui.internal.launching;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.IDebugModelPresentation;
import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.launching.LaunchingUtils;
import org.eclipse.mtj.core.launching.ILaunchConstants;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

public class JadLaunchShortcut implements ILaunchShortcut {
	/**
	 * @see ILaunchShortcut#launch(ISelection, String)
	 */
	public void launch(ISelection selection, String mode) {
		Object selected = ((IStructuredSelection) selection).getFirstElement();
		if (selected instanceof IFile) {
			IFile selectedFile = (IFile) selected;
			launch(selectedFile, mode);
		} else if (selected instanceof IProject) {
			IJavaProject javaProject = JavaCore.create((IProject) selected);
			IMidletSuiteProject suite = MidletSuiteFactory
					.getMidletSuiteProject(javaProject);
			IFile jadFile = getRuntimeJadFile(suite);
			launch(jadFile, mode);
		} else if (selected instanceof IJavaProject) {
			IMidletSuiteProject suite = MidletSuiteFactory
					.getMidletSuiteProject((IJavaProject) selected);
			IFile jadFile = getRuntimeJadFile(suite);
			launch(jadFile, mode);
		}
	}

	/**
	 * @see ILaunchShortcut#launch(IEditorPart, String)
	 */
	public void launch(IEditorPart editor, String mode) {
		IEditorInput input = editor.getEditorInput();
		IFile jadFile = (IFile) input.getAdapter(IFile.class);
		if (jadFile != null) {
			launch(jadFile, mode);
		}
	}

	/**
	 * Launch from a JAD file.
	 * 
	 * @param selectedFile
	 * @param mode
	 */
	private void launch(IFile selectedFile, String mode) {

		ILaunchConfiguration config = null;
		if (isProjectJad(selectedFile)) {
			config = findLaunchConfiguration(selectedFile, mode);
		} else {
			IMidletSuiteProject suite = getMidletSuiteProject(selectedFile);
			IFile jadFile = getRuntimeJadFile(suite);
			config = findLaunchConfiguration(jadFile, mode);
		}

		if (config == null) {
			return;
		}
		
		// launch
		DebugUITools.launch(config, mode);
	}

	private IFile getRuntimeJadFile(IMidletSuiteProject suite) {
		IFolder emulationFolder = LaunchingUtils.getEmulationFolder(suite);
		IFile runtimeJadFile = emulationFolder.getFile(suite.getJadFileName());
		return runtimeJadFile;
	}

	/**
	 * Determine if the JAD file is the project's JAD file. Because the project
	 * may contains some other JAD file which is not the project's JAD file.
	 * 
	 * @param receiver
	 * @return - If the selected JAD file name equals the project JAD file name,
	 *         return true.
	 */
	private boolean isProjectJad(IFile receiver) {
		IMidletSuiteProject midletProject = getMidletSuiteProject(receiver);
		String jadFileName = midletProject.getJadFileName();
		if (!jadFileName.equals(receiver.getName())) {
			return false;
		}
		return true;
	}

	/**
	 * Locate a configuration to re-launch for the given type. If one cannot be
	 * found, create one.
	 * 
	 * @return a re-usable config or <code>null</code> if none
	 */
	private ILaunchConfiguration findLaunchConfiguration(IFile jadFile,
			String mode) {
		ILaunchConfiguration configuration = null;
		List<ILaunchConfiguration> candidateConfigs = getCandidateConfigs(jadFile);

		// If there are no existing configs associated with the IType, create
		// one. If there is exactly one config associated with the IType, return
		// it. Otherwise, if there is more than one config associated with the
		// IType, prompt the user to choose one.
		int candidateCount = candidateConfigs.size();
		if (candidateCount < 1) {
			configuration = createConfiguration(jadFile);
		} else if (candidateCount == 1) {
			configuration = (ILaunchConfiguration) candidateConfigs.get(0);
		} else {
			// Prompt the user to choose a config. A null result means the user
			// canceled the dialog, in which case this method returns null,
			// since canceling the dialog should also cancel launching
			// anything.
			ILaunchConfiguration config = chooseConfiguration(candidateConfigs);
			if (config != null) {
				configuration = config;
			}
		}
		return configuration;
	}

	/**
	 * Returns a configuration from the given collection of configurations that
	 * should be launched, or <code>null</code> to cancel. Default
	 * implementation opens a selection dialog that allows the user to choose
	 * one of the specified launch configurations. Returns the chosen
	 * configuration, or <code>null</code> if the user cancels.
	 * 
	 * @param configList
	 *            list of configurations to choose from
	 * @return configuration to launch or <code>null</code> to cancel
	 */
	private ILaunchConfiguration chooseConfiguration(
			List<ILaunchConfiguration> configList) {
		IDebugModelPresentation labelProvider = DebugUITools
				.newDebugModelPresentation();
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(
				getShell(), labelProvider);
		dialog.setElements(configList.toArray());
		dialog.setTitle(LauncherMessage.launch_configSelection_title);
		dialog.setMessage(LauncherMessage.launch_configSelection_message);
		dialog.setMultipleSelection(false);
		int result = dialog.open();
		labelProvider.dispose();
		if (result == Window.OK) {
			return (ILaunchConfiguration) dialog.getFirstResult();
		}
		return null;
	}

	/**
	 * Get all ILaunchConfiguration meet these conditions: 1. Project Name
	 * matching. 2. doJadLaunch==true. 3. jadUrl contains the jadFile name.
	 * 
	 * @param jadFile
	 * @return
	 */
	private List<ILaunchConfiguration> getCandidateConfigs(IFile jadFile) {
		ILaunchConfigurationType configType = getEmulatorConfigType();
		List<ILaunchConfiguration> candidateConfigs = Collections.emptyList();
		try {
			ILaunchConfiguration[] configs = DebugPlugin.getDefault()
					.getLaunchManager().getLaunchConfigurations(configType);
			candidateConfigs = new ArrayList<ILaunchConfiguration>(
					configs.length);
			for (int i = 0; i < configs.length; i++) {
				ILaunchConfiguration config = configs[i];
				boolean doJadLaunch = config.getAttribute(
						ILaunchConstants.DO_JAD_LAUNCH, false);
				String projectName = config
						.getAttribute(
								IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
								"");
				String jadUrl = config.getAttribute(
						ILaunchConstants.SPECIFIED_JAD_URL, "");
				// If doJadLaunch && project name is matching && jadUrl contains
				// JAD name, then the launch config is a candidate.
				if (doJadLaunch
						&& projectName.equals(jadFile.getProject().getName())
						&& jadUrl.indexOf(jadFile.getName()) >= 0) {
					candidateConfigs.add(config);
				}
			}
		} catch (CoreException e) {
			MTJCorePlugin.log(IStatus.WARNING, "getCandidateConfigs", e);
		}

		return candidateConfigs;
	}

	/**
	 * Create & return a new launch configuration based on the selected JAD
	 * file.
	 */
	private ILaunchConfiguration createConfiguration(IFile jadFile) {
		ILaunchConfiguration config = null;
		try {
			ILaunchConfigurationType configType = getEmulatorConfigType();

			String launchConfigName = DebugPlugin.getDefault()
					.getLaunchManager()
					.generateUniqueLaunchConfigurationNameFrom(
							jadFile.getName());
			ILaunchConfigurationWorkingCopy wc = configType.newInstance(null,
					launchConfigName);
			wc.setAttribute(ILaunchConstants.DO_JAD_LAUNCH, true);
			String jadFileURL = getLaunchingJadFileLocation(jadFile);
			wc.setAttribute(ILaunchConstants.SPECIFIED_JAD_URL, jadFileURL);
			wc.setAttribute(
					IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
					jadFile.getProject().getName());
			wc.setAttribute(ILaunchConstants.DO_OTA, false);

			DebugUITools.setLaunchPerspective(configType,
					ILaunchManager.RUN_MODE,
					IDebugUIConstants.PERSPECTIVE_DEFAULT);
			DebugUITools.setLaunchPerspective(configType,
					ILaunchManager.DEBUG_MODE,
					IDebugUIConstants.PERSPECTIVE_DEFAULT);

			config = wc.doSave();

		} catch (CoreException ce) {
			MTJCorePlugin.log(IStatus.WARNING, "createConfiguration", ce);
		}

		return config;
	}

	/**
	 * Return the JAD file location for launching. The launching JAD file is at
	 * /%DEPLOY_FOLDER%/launchFromJAD/. In which DEPLOY_FOLDER is project's
	 * deployment folder.
	 * 
	 * @param jadFile
	 * @return
	 */
	private String getLaunchingJadFileLocation(IFile jadFile) {
		IMidletSuiteProject suite = getMidletSuiteProject(jadFile);
		IPath launchBasePath = LaunchingUtils.getJadLaunchBasePath(suite);
		IPath launchingJadPath = launchBasePath.append(jadFile.getName());
		String launchingJadFileLocation = launchingJadPath.toPortableString();
		return launchingJadFileLocation;
	}

	/**
	 * Get the launch configuration type for wireless toolkit emulator.
	 * 
	 * @return
	 */
	private ILaunchConfigurationType getEmulatorConfigType() {
		ILaunchManager lm = DebugPlugin.getDefault().getLaunchManager();
		ILaunchConfigurationType configType = lm
				.getLaunchConfigurationType(ILaunchConstants.LAUNCH_CONFIG_TYPE);
		return configType;
	}

	/**
	 * Get the active workbench window's shell.
	 * 
	 * @return
	 */
	private Shell getShell() {
		Shell shell = null;

		IWorkbenchWindow workbenchWindow = MTJUIPlugin.getDefault()
				.getWorkbench().getActiveWorkbenchWindow();
		if (workbenchWindow != null) {
			shell = workbenchWindow.getShell();
		}

		return shell;
	}

	/**
	 * Return the IMidletSuiteProject which contains the JAD file.
	 * 
	 * @param selectedFile
	 * @return
	 */
	private IMidletSuiteProject getMidletSuiteProject(IFile selectedFile) {
		IJavaProject javaProject = getJavaProject(selectedFile);
		IMidletSuiteProject midletSuiteProject = MidletSuiteFactory
				.getMidletSuiteProject(javaProject);
		return midletSuiteProject;
	}

	private IJavaProject getJavaProject(IFile selectedFile) {
		IProject project = selectedFile.getProject();
		IJavaProject javaProject = JavaCore.create(project);
		return javaProject;
	}

}
