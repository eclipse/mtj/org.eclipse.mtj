/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Moving debugger settings verification to a
 *                                more appropriate location
 */
package org.eclipse.mtj.ui.internal.launching;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.IDebugModelPresentation;
import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jdt.core.IClassFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.ui.JavaElementLabelProvider;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.launching.ILaunchConstants;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;


/**
 * Emulator launch shortcut implementation.
 * 
 * @author Craig Setera
 */
public class EmulatorLaunchShortcut implements ILaunchShortcut {

    /**
     * Default constructor.
     */
    public EmulatorLaunchShortcut() {
        super();
    }

    /**
     * @see org.eclipse.debug.ui.ILaunchShortcut#launch(org.eclipse.jface.viewers.ISelection,
     *      java.lang.String)
     */
    public void launch(ISelection selection, String mode) {
        if (selection instanceof IStructuredSelection) {
            launch(((IStructuredSelection) selection).toArray(), mode);
        }
    }

    /**
     * @see org.eclipse.debug.ui.ILaunchShortcut#launch(org.eclipse.ui.IEditorPart,
     *      java.lang.String)
     */
    public void launch(IEditorPart editor, String mode) {
        IEditorInput input = editor.getEditorInput();
        IJavaElement javaElement = (IJavaElement) input
                .getAdapter(IJavaElement.class);
        if (javaElement != null) {
            launch(new Object[] { javaElement }, mode);
        }
    }

    /**
     * Show a selection dialog that allows the user to choose one of the
     * specified launch configurations. Return the chosen config, or
     * <code>null</code> if the user canceled the dialog.
     */
    private ILaunchConfiguration chooseConfiguration(List configList,
            String mode) {
        IDebugModelPresentation labelProvider = DebugUITools
                .newDebugModelPresentation();
        ElementListSelectionDialog dialog = new ElementListSelectionDialog(
                getShell(), labelProvider);
        dialog.setElements(configList.toArray());
        dialog.setTitle("Launch configuration");
        dialog.setMessage("Select launch configuration");
        dialog.setMultipleSelection(false);

        int result = dialog.open();
        labelProvider.dispose();
        if (result == ElementListSelectionDialog.OK) {
            return (ILaunchConfiguration) dialog.getFirstResult();
        }
        return null;
    }

    /**
     * Prompts the user to select a type
     * 
     * @return the selected type or <code>null</code> if none.
     */
    private IType chooseType(IType[] types, String mode) {
        IType selectedType = null;

        ElementListSelectionDialog dialog = new ElementListSelectionDialog(
                getShell(), new JavaElementLabelProvider());
        dialog.setElements(types);
        dialog.setTitle("Type Selection");

        if (mode.equals(ILaunchManager.DEBUG_MODE)) {
            dialog.setMessage("Select the midlet to be debugged.");
        } else {
            dialog.setMessage("Select the midlet to be run.");
        }

        dialog.setMultipleSelection(false);
        if (dialog.open() == ElementListSelectionDialog.OK) {
            selectedType = (IType) dialog.getFirstResult();
        }

        return selectedType;
    }

    /**
     * Collect up the MIDlet types related to the specified object.
     * 
     * @param object
     * @param monitor
     * @param result
     * @throws JavaModelException
     */
    private void collectTypes(Object object, IProgressMonitor monitor,
            Set result) throws JavaModelException {
        IType type = null;

        if (object instanceof ICompilationUnit) {
            IType[] types = ((ICompilationUnit) object).getAllTypes();
            for (int i = 0; i < types.length; i++) {
                collectTypes(types[i], monitor, result);
            }
        } else if (object instanceof IClassFile) {
            type = ((IClassFile) object).getType();
        } else if (object instanceof IJavaElement) {
            type = (IType) ((IJavaElement) object)
                    .getAncestor(IJavaElement.TYPE);
        } else if (object instanceof IResource) {
            collectTypes(JavaCore.create((IResource) object), monitor, result);
        }

        if (type != null) {
            if (Utils.isMidlet(type, monitor)) {
                result.add(type);
            }
        }
    }

    /**
     * Create & return a new configuration based on the specified
     * <code>IType</code>.
     */
    private ILaunchConfiguration createConfiguration(IType type) {
        ILaunchConfiguration config = null;
        try {
            ILaunchConfigurationType configType = getEmulatorConfigType();

            String launchConfigName = DebugPlugin.getDefault()
                    .getLaunchManager()
                    .generateUniqueLaunchConfigurationNameFrom(
                            type.getElementName());
            ILaunchConfigurationWorkingCopy wc = configType.newInstance(null,
                    launchConfigName);

            wc.setAttribute(ILaunchConstants.EMULATED_CLASS, Utils
                    .getQualifiedClassName(type));
            wc.setAttribute(
                    IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, type
                            .getJavaProject().getElementName());
            wc.setAttribute(ILaunchConstants.DO_OTA, false);

            DebugUITools.setLaunchPerspective(configType,
                    ILaunchManager.RUN_MODE,
                    IDebugUIConstants.PERSPECTIVE_DEFAULT);
            DebugUITools.setLaunchPerspective(configType,
                    ILaunchManager.DEBUG_MODE,
                    IDebugUIConstants.PERSPECTIVE_DEFAULT);

            config = wc.doSave();

        } catch (CoreException ce) {
            MTJCorePlugin.log(IStatus.WARNING, "createConfiguration", ce);
        }

        return config;
    }

    /**
     * Locate a configuration to re-launch for the given type. If one cannot be
     * found, create one.
     * 
     * @return a re-usable config or <code>null</code> if none
     */
    private ILaunchConfiguration findLaunchConfiguration(IType type, String mode) {
        ILaunchConfiguration configuration = null;
        List candidateConfigs = getCandidateConfigs(type);

        // If there are no existing configs associated with the IType, create
        // one.
        // If there is exactly one config associated with the IType, return it.
        // Otherwise, if there is more than one config associated with the
        // IType, prompt the
        // user to choose one.
        int candidateCount = candidateConfigs.size();
        if (candidateCount < 1) {
            configuration = createConfiguration(type);
        } else if (candidateCount == 1) {
            configuration = (ILaunchConfiguration) candidateConfigs.get(0);
        } else {
            // Prompt the user to choose a config. A null result means the user
            // canceled the dialog, in which case this method returns null,
            // since canceling the dialog should also cancel launching
            // anything.
            ILaunchConfiguration config = chooseConfiguration(candidateConfigs,
                    mode);
            if (config != null) {
                configuration = config;
            }
        }

        return configuration;
    }

    /**
     * Get the candidate launch configurations for the specified type.
     * 
     * @param type
     * @return
     */
    private List getCandidateConfigs(IType type) {
        ILaunchConfigurationType configType = getEmulatorConfigType();
        List candidateConfigs = Collections.EMPTY_LIST;
        try {
            ILaunchConfiguration[] configs = DebugPlugin.getDefault()
                    .getLaunchManager().getLaunchConfigurations(configType);
            candidateConfigs = new ArrayList(configs.length);
            for (int i = 0; i < configs.length; i++) {
                ILaunchConfiguration config = configs[i];
                String midletName = config.getAttribute(
                        ILaunchConstants.EMULATED_CLASS, "");
                String projectName = config
                        .getAttribute(
                                IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
                                "");

                if (midletName.equals(Utils.getQualifiedClassName(type))
                        && projectName.equals(type.getJavaProject()
                                .getElementName())) {
                    candidateConfigs.add(config);
                }
            }
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.WARNING, "getCandidateConfigs", e);
        }

        return candidateConfigs;
    }

    /**
     * Get the launch configuration type for wireless toolkit emulator.
     * 
     * @return
     */
    private ILaunchConfigurationType getEmulatorConfigType() {
        ILaunchManager lm = DebugPlugin.getDefault().getLaunchManager();
        ILaunchConfigurationType configType = lm
                .getLaunchConfigurationType(ILaunchConstants.LAUNCH_CONFIG_TYPE);
        return configType;
    }

    /**
     * Find all of the IType instances related to the specified selection.
     * 
     * @param selection
     * @return
     */
    private IType[] findTypes(final Object[] selection) {
        final Set result = new HashSet();

        if (selection.length > 0) {
            IRunnableWithProgress runnable = getTypeCollectionRunnable(
                    selection, result);

            try {
                new ProgressMonitorDialog(getShell()).run(true, true, runnable);
            } catch (InvocationTargetException e) {
                MTJCorePlugin.log(IStatus.WARNING, "findTypes", e);
            } catch (InterruptedException e) {
                // Ignore
            }
        }

        return (IType[]) result.toArray(new IType[result.size()]);
    }

    /**
     * Get the runnable to be used in collecting IType instances.
     * 
     * @param selection
     * @param result
     * @return
     */
    private IRunnableWithProgress getTypeCollectionRunnable(
            final Object[] selection, final Set result) {
        return new IRunnableWithProgress() {
            public void run(IProgressMonitor pm) throws InterruptedException {
                int nElements = selection.length;
                pm.beginTask("Searching", nElements);
                try {
                    for (int i = 0; i < nElements; i++) {
                        try {
                            collectTypes(selection[i], new SubProgressMonitor(
                                    pm, 1), result);
                        } catch (JavaModelException e) {
                            MTJCorePlugin.log(IStatus.WARNING, "collectTypes",
                                    e);
                        }
                        if (pm.isCanceled()) {
                            throw new InterruptedException();
                        }
                    }
                } finally {
                    pm.done();
                }
            }
        };
    }

    /**
     * Get the active workbench window's shell.
     * 
     * @return
     */
    private Shell getShell() {
        Shell shell = null;

        IWorkbenchWindow workbenchWindow = MTJUIPlugin.getDefault()
                .getWorkbench().getActiveWorkbenchWindow();
        if (workbenchWindow != null) {
            shell = workbenchWindow.getShell();
        }

        return shell;
    }

    /**
     * Attempt to find and launch the objects in the selection.
     * 
     * @param selection
     * @param mode
     */
    private void launch(Object[] selection, String mode) {
        IType type = null;
        IType[] types = findTypes(selection);

        // Choose a type
        if (types.length == 1) {
            type = types[0];
        } else if (types.length > 1) {
            type = chooseType(types, mode);
        }

        if (type != null) {
			ILaunchConfiguration config = findLaunchConfiguration(type, mode);
			if (config != null) {
				try {
					config.launch(mode, null);
				} catch (CoreException e) {
					ErrorDialog.openError(getShell(), "Error Launching "
							+ config.getName(), e.getMessage(), e.getStatus());
				}
			}

		}
    }
}
