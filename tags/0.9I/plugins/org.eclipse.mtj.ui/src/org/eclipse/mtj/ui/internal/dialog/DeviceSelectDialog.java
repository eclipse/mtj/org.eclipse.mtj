/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.ui.devices.DeviceSelector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

/**
 * A dialog implementation for use in selecting a device from the registry.
 * 
 * @author Craig Setera
 */
public class DeviceSelectDialog extends Dialog {
    private DeviceSelector deviceSelector;
    private IDevice selectedDevice;

    /**
     * Construct a dialog
     * 
     * @param parentShell
     */
    public DeviceSelectDialog(Shell parentShell) {
        super(parentShell);
    }

    /**
     * Construct a dialog
     * 
     * @param parentShell
     */
    public DeviceSelectDialog(IShellProvider parentShell) {
        super(parentShell);
    }

    /**
     * Return the device selected by the user.
     * 
     * @return
     */
    public IDevice getSelectedDevice() {
        return selectedDevice;
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
     */
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.OK_ID) {
            selectedDevice = deviceSelector.getSelectedDevice();
        }

        super.buttonPressed(buttonId);
    }

    /**
     * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
     */
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);
        newShell.setText("Select Device for Project");
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    protected Control createDialogArea(Composite parent) {
        deviceSelector = new DeviceSelector();

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, true));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
        deviceSelector.createContents(parent, false);

        return composite;
    }
}
