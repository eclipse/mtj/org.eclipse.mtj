/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.ui.internal.actions;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

/**
 * Superclass of actions that operate on IJavaProject instances or things that
 * can be adapted to an IJavaProject.
 * 
 * @author Craig Setera
 */
public abstract class AbstractJavaProjectAction implements
        IObjectActionDelegate {
    protected IStructuredSelection selection;
    protected IWorkbenchPart workbenchPart;

    /**
     * Construct a new instance
     */
    public AbstractJavaProjectAction() {
        super();
    }

    /**
     * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction,
     *      org.eclipse.ui.IWorkbenchPart)
     */
    public void setActivePart(IAction action, IWorkbenchPart targetPart) {
        workbenchPart = targetPart;
    }

    /**
     * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction,
     *      org.eclipse.jface.viewers.ISelection)
     */
    public void selectionChanged(IAction action, ISelection selection) {
        if (selection instanceof IStructuredSelection) {
            this.selection = (IStructuredSelection) selection;
        } else {
            this.selection = null;
        }
    }

    /**
     * Return the java project for the selected object or
     * <code>null</null> if it is not a valid selection.
     * 
     * @param selected
     * @return
     */
    protected IJavaProject getJavaProject(Object selected) {
        IJavaProject javaProject = null;

        if (selected != null) {
            if (selected instanceof IJavaProject) {
                javaProject = (IJavaProject) selected;
            } else if (selected instanceof IProject) {
                javaProject = JavaCore.create((IProject) selected);
            } else if (selected instanceof IAdaptable) {
                IAdaptable adaptable = (IAdaptable) selected;
                javaProject = (IJavaProject) adaptable
                        .getAdapter(IJavaProject.class);

                if (javaProject == null) {
                    IProject project = (IProject) adaptable
                            .getAdapter(IProject.class);
                    javaProject = JavaCore.create(project);
                }
            }
        }

        return javaProject;
    }

    /**
     * Get the shell to use for opening the dialog.
     * 
     * @return
     */
    protected Shell getShell() {
        Shell shell = null;

        if (workbenchPart != null) {
            shell = workbenchPart.getSite().getShell();
        }

        return shell;
    }
}
