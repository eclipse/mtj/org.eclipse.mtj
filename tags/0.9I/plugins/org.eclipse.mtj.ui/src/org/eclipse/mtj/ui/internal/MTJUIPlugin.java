/**
 * Copyright (c) 2004,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Adapting code to use Jetty version 5.1.11                                
 */
package org.eclipse.mtj.ui.internal;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.preference.IPersistentPreferenceStore;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.overtheair.OTAServer;
import org.eclipse.mtj.ui.internal.utils.PluginPreferenceStore;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.osgi.framework.BundleContext;

/**
 * The plug-in implementation class.
 * 
 * @author Craig Setera
 */
public class MTJUIPlugin extends AbstractUIPlugin {
    /**
     * A class wrapper to act as a key into the color cache
     */
    private static class ColorCacheKey {
        private Display display;
        private int red;
        private int green;
        private int blue;

        /**
         * Constructor.
         * 
         * @param display
         * @param red
         * @param green
         * @param blue
         */
        private ColorCacheKey(Display display, int red, int green, int blue) {
            this.display = display;
            this.red = red;
            this.green = green;
            this.blue = blue;
        }

        /**
         * @see java.lang.Object#equals(java.lang.Object)
         */
        public boolean equals(Object obj) {
            boolean equals = false;

            if (obj instanceof ColorCacheKey) {
                ColorCacheKey other = (ColorCacheKey) obj;
                equals = display.equals(other.display) && (red == other.red)
                        && (green == other.green) && (blue == other.blue);
            }

            return equals;
        }

        /**
         * @see java.lang.Object#hashCode()
         */
        public int hashCode() {
            return display.hashCode() ^ (red << 24) ^ (green << 16) ^ blue;
        }
    }

    /**
     * A class wrapper to act as a key into the color cache
     */
    private static class FontCacheKey {
        private Display display;
        private String name;
        private int height;
        private int style;

        /**
         * Constructor
         * 
         * @param display
         * @param name
         * @param height
         * @param style
         */
        private FontCacheKey(Display display, String name, int height, int style) {
            this.display = display;
            this.name = name;
            this.height = height;
            this.style = style;
        }

        /**
         * @see java.lang.Object#equals(java.lang.Object)
         */
        public boolean equals(Object obj) {
            boolean equals = false;

            if (obj instanceof FontCacheKey) {
                FontCacheKey other = (FontCacheKey) obj;
                equals = display.equals(other.display)
                        && name.equals(other.name) && (height == other.height)
                        && (style == other.style);
            }

            return equals;
        }

        /**
         * @see java.lang.Object#hashCode()
         */
        public int hashCode() {
            return display.hashCode() ^ name.hashCode() ^ (height << 16)
                    ^ style;
        }
    }

    // The shared instance.
    private static MTJUIPlugin plugin;

    // Some caches
    private Map colorCache;
    private Map fontCache;

    // A preference store wrapper around the core plugin plugin preferences
    private IPreferenceStore corePreferenceStore;

    /**
     * The constructor.
     */
    public MTJUIPlugin() {
        super();
        if (plugin == null) {
            plugin = this;
        }

        colorCache = new HashMap();
        fontCache = new HashMap();
    }

    /**
     * Display an error to the user given the specified information.
     * 
     * @param shell
     * @param severity
     * @param code
     * @param title
     * @param message
     * @param exception
     */
    public static void displayError(Shell shell, int severity, int code,
            String title, String message, Throwable exception) {
        String id = getDefault().getBundle().getSymbolicName();
        Status status = new Status(severity, id, code, message, exception);
        ErrorDialog.openError(shell, title, message, status);
    }

    /**
     * Return the active window's shell.
     * 
     * @return the active window shell
     */
    public static Shell getActiveWindowShell() {
        MTJUIPlugin plugin = getDefault();
        IWorkbenchWindow activeWindow = plugin.getWorkbench()
                .getActiveWorkbenchWindow();

        return activeWindow.getShell();
    }

    /**
     * Get a color from the cache.
     * 
     * @param display
     * @param red
     * @param green
     * @param blue
     * @return
     */
    public static Color getColor(Display display, int red, int green, int blue) {
        Map cache = getDefault().colorCache;
        ColorCacheKey key = new ColorCacheKey(display, red, green, blue);

        Color color = (Color) cache.get(key);
        if (color == null) {
            color = new Color(display, red, green, blue);
            cache.put(key, color);
        }

        return color;
    }

    /**
     * Get the specified font from the cache.
     * 
     * @param display
     * @param name
     * @param height
     * @param style
     * @return
     */
    public static Font getFont(Display display, String name, int height,
            int style) {
        Map cache = getDefault().fontCache;
        FontCacheKey key = new FontCacheKey(display, name, height, style);

        Font font = (Font) cache.get(key);
        if (font == null) {
            font = new Font(display, name, height, style);
            cache.put(key, font);
        }

        return font;
    }

    /**
     * Get an ImageDescriptor for the for specified plugin image.
     */
    public static ImageDescriptor getIconImageDescriptor(String imageName) {
        ImageDescriptor img = null;

        try {
            URL pluginURL = getDefault().getBundle().getEntry("/");
            URL imgURL = new URL(pluginURL, "icons/" + imageName);

            img = ImageDescriptor.createFromURL(imgURL);
        } catch (MalformedURLException e) {
            MTJCorePlugin.log(IStatus.INFO, "getIconImage", e);
        }

        return img;
    }

    /**
     * Find and return the specified image. Use the cache whenever possible.
     * 
     * @param imageName
     * @return
     */
    public static Image getImageFromCache(String imageName) {
        ImageRegistry registry = getDefault().getImageRegistry();
        Image image = registry.get(imageName);
        if (image == null) {
            ImageDescriptor descriptor = getIconImageDescriptor(imageName);
            registry.put(imageName, descriptor);
            image = registry.get(imageName);
        }

        return image;
    }

    /**
     * Return a preference store that sits on top of the MTJ core
     * preferences for the specified project.
     * 
     * @param context
     * @return
     */
    public static IPersistentPreferenceStore getCoreProjectPreferenceStore(
            IProject context) {
        ProjectScope projectScope = new ProjectScope(context);
        return new ScopedPreferenceStore(projectScope,
                IMTJCoreConstants.PLUGIN_ID);
    }

    /**
     * This method is called upon plug-in activation
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);

        // Dispose of the cached values
        Iterator iter = colorCache.values().iterator();
        while (iter.hasNext()) {
            Color color = (Color) iter.next();
            color.dispose();
        }

        iter = fontCache.values().iterator();
        while (iter.hasNext()) {
            Font font = (Font) iter.next();
            font.dispose();
        }

        // Startup the OTA server if set to startup immediately
        IPreferenceStore store = getPreferenceStore();
        boolean startAtStart = store
                .getBoolean(IMTJCoreConstants.PREF_OTA_SERVER_START_AT_START);
        if (startAtStart) {
            startupOTAServer();
        }
    }

    /**
     * This method is called when the plug-in is stopped
     */
    public void stop(BundleContext context) throws Exception {
        super.stop(context);
    }

    /**
     * Returns the shared instance.
     */
    public static MTJUIPlugin getDefault() {
        return plugin;
    }

    /**
     * Return the dialog settings section within the plugin's root settings
     * object. This method will create the new section if necessary.
     * 
     * @param dialogSettings
     * @param sectionName
     * @return
     */
    public static IDialogSettings getDialogSettings(String sectionName) {
        return getDialogSettings(getDefault().getDialogSettings(), sectionName);
    }

    /**
     * Return the dialog settings section within the specified parent settings
     * object.  This method will create the new section if necessary.
     * 
     * @param dialogSettings
     * @param sectionName
     * @return
     */
    public static IDialogSettings getDialogSettings(
            IDialogSettings dialogSettings, String sectionName) {
        IDialogSettings settings = dialogSettings.getSection(sectionName);
        if (settings == null) {
            settings = dialogSettings.addNewSection(sectionName);
        }

        return settings;
    }

    /**
     * Return an IPreferenceStore wrapper around the MTJ core
     * plugin preferences.
     * 
     * @return
     */
    public IPreferenceStore getCorePreferenceStore() {
        if (corePreferenceStore == null) {
            MTJCorePlugin plugin = MTJCorePlugin.getDefault();
            corePreferenceStore = new PluginPreferenceStore(plugin, plugin
                    .getPluginPreferences());
        }

        return corePreferenceStore;
    }

    /**
     * Start up the OTA server if the user requested it
     * be started up immediately. 
     */
    private void startupOTAServer() {
        try {
            OTAServer.instance.start();
        } catch (Exception e) {
            MTJCorePlugin.log(IStatus.ERROR, "startupOTAServer", e);
        }
    }
}
