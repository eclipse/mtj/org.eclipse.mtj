/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.preferences;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ICellEditorListener;
import org.eclipse.jface.viewers.ICellEditorValidator;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSetRegistry;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.ui.viewers.TableViewerConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * Preference page for defining and editing the available symbol definition
 * instances that are used for handling device fragmentation.
 * 
 * @author Craig Setera
 */
public class SymbolDefinitionsPreferencePage extends PreferencePage implements
        IWorkbenchPreferencePage {

    // A regular expression compiled Pattern that matches on whitespace
    // within a string
    private static final Pattern WHITESPACE_PATTERN = Pattern
            .compile(".*\\s+.*");

    // Column information structure
    private static final Object[] NO_ELEMENTS = new Object[0];

    // Column property names
    private static final String PROP_SYMBOL = "symbol";
    private static final String PROP_VALUE = "value";

    // All of the properties in order
    private static final String[] PROPERTIES = new String[] { PROP_SYMBOL,
            PROP_VALUE };

    // Column information structure
    private static final int DEFAULT_TABLE_WIDTH = 650;
    private static final TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
            new TableColumnInfo("Symbol", 50f, null),
            new TableColumnInfo("Value", 50f, null), };

    // A cell modifier implementation for the device libraries editor
    private class CellModifier implements ICellModifier {
        public boolean canModify(Object element, String property) {
            return true;
        }

        public Object getValue(Object element, String property) {
            String value = null;

            SymbolDefinition def = (SymbolDefinition) element;
            if (property.equals(PROP_SYMBOL)) {
                value = def.name;
            } else {
                value = def.value;
            }
            return value;
        }

        public void modify(Object element, String property, Object value) {
            TableItem item = (TableItem) element;

            String symbolName = item.getText(0);
            String currentValue = item.getText(1);

            int itemIndex = findSymbolDefinition(symbolName);
            if (itemIndex != -1) {
                if (property.equals(PROP_SYMBOL)) {
                    String newSymbol = (String) value;
                    if (isValidSymbol(newSymbol)) {
                        currentDefinitions.set(itemIndex, new SymbolDefinition(
                                newSymbol, currentValue));
                    }
                } else {
                    SymbolDefinition def = (SymbolDefinition) currentDefinitions
                            .get(itemIndex);
                    def.value = (String) value;
                }

                tableViewer.refresh();
            }

            setErrorMessage(null);
        }

        /**
         * Attempt to find the symbol definition with the specified name in the
         * current definitions. Return the index or <code>-1</code> if not
         * found.
         * 
         * @param name
         * @return
         */
        private int findSymbolDefinition(String name) {
            int index = -1;

            for (int i = 0; i < currentDefinitions.size(); i++) {
                SymbolDefinition def = (SymbolDefinition) currentDefinitions
                        .get(i);
                if (def.name.equals(name)) {
                    index = i;
                    break;
                }
            }

            return index;
        }
    }

    // Label provider for the definitions set combo viewer
    private static class DefinitionSetLabelProvider extends LabelProvider {
        public String getText(Object element) {
            return ((SymbolDefinitionSet) element).getName();
        }
    }

    // Content provider for the definitions set combo viewer
    private class DefinitionSetsContentProvider implements
            IStructuredContentProvider {
        public Object[] getElements(Object inputElement) {
            Object[] definitions = NO_ELEMENTS;

            try {
                definitions = SymbolDefinitionSetRegistry.singleton
                        .getAllSetDefinitions();
            } catch (PersistenceException e) {
                handleException("Error retrieving symbol definitions", e);
            }

            return definitions;
        }

        public void dispose() {
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    private static class SymbolDefinition {
        String name;
        String value;

        public SymbolDefinition(String name, String value) {
            super();
            this.name = name;
            this.value = value;
        }

        public SymbolDefinition(Map.Entry symbolDefinitionEntry) {
            this((String) symbolDefinitionEntry.getKey(),
                    (String) symbolDefinitionEntry.getValue());
        }
    }

    /**
     * Implementation of the table's content provider.
     */
    private class TableContentProvider implements IStructuredContentProvider {
        /**
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            return (Object[]) currentDefinitions
                    .toArray(new Object[currentDefinitions.size()]);
        }

        /**
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /**
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer,
         *      java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
            if (oldInput != null) {
                commitSymbolDefinitions((SymbolDefinitionSet) oldInput);
            }

            // Track the current definitions for the UI...
            // These definitions don't matter until a commit occurs
            if (currentDefinitions == null) {
                currentDefinitions = new ArrayList();
            } else {
                currentDefinitions.clear();
            }

            if (newInput instanceof SymbolDefinitionSet) {
                SymbolDefinitionSet set = (SymbolDefinitionSet) newInput;
                Map symbolMap = set.getDefinedSymbols();

                if (symbolMap != null) {
                    symbolsGroup.setText("Symbols in set \"" + set.getName()
                            + "\"");

                    Iterator symbolEntries = symbolMap.entrySet().iterator();
                    while (symbolEntries.hasNext()) {
                        Map.Entry symbolEntry = (Map.Entry) symbolEntries
                                .next();
                        currentDefinitions
                                .add(new SymbolDefinition(symbolEntry));
                    }
                }
            }
        }
    }

    // Cell editor that includes validation of the Symbol name
    private class SymbolNameCellEditor extends TextCellEditor {
        public SymbolNameCellEditor(Composite parent) {
            super(parent);

            setValidator(new SymbolNameCellEditorValidator());
            addListener(new ICellEditorListener() {
                public void applyEditorValue() {
                }

                public void cancelEditor() {
                }

                public void editorValueChanged(boolean oldValidState,
                        boolean newValidState) {
                    if (!newValidState) {
                        setErrorMessage(getErrorMessage());
                    } else {
                        setErrorMessage(null);
                    }
                }
            });
        }
    }

    // Validates that the value for the symbol is a valid value
    private class SymbolNameCellEditorValidator implements ICellEditorValidator {
        public String isValid(Object value) {
            String symbol = (String) value;
            return isValidSymbol(symbol) ? null
                    : "Whitespace not allowed in symbol names.";
        }
    }

    // Label provider for the strings returned by the content provider.
    private static class SymbolDefinitionLabelProvider extends LabelProvider
            implements ITableLabelProvider {
        /**
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
         *      int)
         */
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        /**
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
         *      int)
         */
        public String getColumnText(Object element, int columnIndex) {
            String text = "";
            SymbolDefinition def = (SymbolDefinition) element;

            switch (columnIndex) {
            case 0:
                text = def.name;
                break;

            case 1:
                text = def.value;
                break;
            }

            return text;
        }
    }

    // UI widgets
    private IWorkbench workbench;
    private ComboViewer definitionsComboViewer;
    private TableViewer tableViewer;
    private Button addSetButton;
    private Button removeSetButton;
    private Button addSymbolButton;
    private Button removeSymbolButton;

    // Tracks the current definitions in the table viewer until they
    // are committed back to the underlying model
    private ArrayList currentDefinitions;

    private Group symbolsGroup;

    /**
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
        this.workbench = workbench;
    }

    /**
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    protected Control createContents(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, true));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        Composite nameComposite = new Composite(composite, SWT.NONE);
        nameComposite.setLayout(new GridLayout(3, false));
        nameComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        new Label(nameComposite, SWT.NONE).setText("Definition Set: ");

        definitionsComboViewer = new ComboViewer(nameComposite, SWT.DROP_DOWN);
        definitionsComboViewer
                .setContentProvider(new DefinitionSetsContentProvider());
        definitionsComboViewer
                .setLabelProvider(new DefinitionSetLabelProvider());
        definitionsComboViewer.setSorter(new ViewerSorter());

        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.verticalAlignment = SWT.CENTER;
        definitionsComboViewer.getCombo().setLayoutData(gd);
        definitionsComboViewer.getCombo().addModifyListener(
                new ModifyListener() {
                    public void modifyText(ModifyEvent e) {
                        updateButtonEnablement();
                    }
                });

        Composite nameButtonComposite = new Composite(nameComposite, SWT.NONE);
        nameButtonComposite.setLayout(new GridLayout(1, true));

        addSetButton = new Button(nameButtonComposite, SWT.PUSH);
        addSetButton.setText("Add");
        addSetButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        addSetButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                handleAddSetButton();
            }
        });

        removeSetButton = new Button(nameButtonComposite, SWT.PUSH);
        removeSetButton.setText("Remove");
        removeSetButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        removeSetButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                handleRemoveSetButton();
            }
        });

        symbolsGroup = new Group(composite, SWT.NONE);
        symbolsGroup.setLayout(new GridLayout(2, false));

        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.minimumWidth = 400;
        gridData.heightHint = 300;
        tableViewer = createTableViewer(symbolsGroup);
        tableViewer.getTable().setLayoutData(gridData);

        Composite symbolsButtonComposite = new Composite(symbolsGroup, SWT.NONE);
        symbolsButtonComposite.setLayout(new GridLayout(1, true));

        addSymbolButton = new Button(symbolsButtonComposite, SWT.PUSH);
        addSymbolButton.setText("Add");
        addSymbolButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        addSymbolButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                handleAddSymbolButton();
            }
        });

        removeSymbolButton = new Button(symbolsButtonComposite, SWT.PUSH);
        removeSymbolButton.setText("Remove");
        removeSymbolButton
                .setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        removeSymbolButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                handleRemoveSymbolButton();
            }
        });

        definitionsComboViewer
                .addSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        updateButtonEnablement();

                        SymbolDefinitionSet set = getSelectedSymbolDefinitionSet();
                        tableViewer.setInput(set);
                    }
                });

        initializeDefinitionsCombo();

        return composite;
    }

    /**
     * @see org.eclipse.jface.preference.PreferencePage#performCancel()
     */
    public boolean performCancel() {
        // Force a reload of the symbol definitions registry
        return reloadSymbolDefinitionsRegistry() && super.performCancel();
    }

    /**
     * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
     */
    protected void performDefaults() {
        // Force a reload of the symbol definitions registry
        reloadSymbolDefinitionsRegistry();

        // Refresh all of the viewers
        initializeDefinitionsCombo();
    }

    /**
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    public boolean performOk() {
        boolean succeeded = false;

        commitSymbolDefinitions();
        try {
            SymbolDefinitionSetRegistry.singleton.store();
            succeeded = true;
        } catch (PersistenceException e) {
            handleException("Error storing symbol definitions", e);
        } catch (TransformerException e) {
            handleException("Error storing symbol definitions", e);
        } catch (IOException e) {
            handleException("Error storing symbol definitions", e);
        }

        return succeeded && super.performOk();
    }

    /**
     * Commit the contents of the current symbol definitions back to the model
     * object.
     */
    public void commitSymbolDefinitions() {
        commitSymbolDefinitions(getSelectedSymbolDefinitionSet());
    }

    /**
     * Commit the contents of the current symbol definitions back to the model
     * object.
     * 
     * @param set
     */
    private void commitSymbolDefinitions(SymbolDefinitionSet set) {
        // Save the current definitions
        if (set != null) {
            Map defs = new HashMap(currentDefinitions.size());

            Iterator iterator = currentDefinitions.iterator();
            while (iterator.hasNext()) {
                SymbolDefinition def = (SymbolDefinition) iterator.next();
                defs.put(def.name, def.value);
            }

            set.setDefinitions(defs);
        }
    }

    /**
     * Create the table viewer.
     * 
     * @param parent
     */
    private TableViewer createTableViewer(Composite composite) {
        int styles = SWT.SINGLE | SWT.V_SCROLL | SWT.BORDER
                | SWT.FULL_SELECTION;

        final Table table = new Table(composite, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        // Wire up the viewer
        TableViewer viewer = new TableViewer(table);
        viewer.setContentProvider(new TableContentProvider());
        viewer.setLabelProvider(new SymbolDefinitionLabelProvider());
        viewer.setSorter(new ViewerSorter());
        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                updateButtonEnablement();
            }
        });

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings("symbolDefsViewerSettings");
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 0);
        viewerConfiguration.configure(viewer);

        // Wire up the cell modification handling
        viewer.setCellModifier(new CellModifier());
        viewer.setColumnProperties(PROPERTIES);
        viewer.setCellEditors(new CellEditor[] {
                new SymbolNameCellEditor(table), new TextCellEditor(table), });

        return viewer;
    }

    /**
     * Return the currently selected symbol definition set or <code>null</code>
     * if no set has been selected.
     * 
     * @return
     */
    private SymbolDefinitionSet getSelectedSymbolDefinitionSet() {
        SymbolDefinitionSet set = null;

        IStructuredSelection selection = (IStructuredSelection) definitionsComboViewer
                .getSelection();

        if (selection.size() == 0) {
            set = null;
        } else {
            set = (SymbolDefinitionSet) selection.getFirstElement();
        }

        return set;
    }

    /**
     * Return the currently selected symbol definition or <code>null</code> if
     * no definition is selected.
     * 
     * @return
     */
    private SymbolDefinition getSelectedSymbolDefinition() {
        SymbolDefinition definition = null;

        IStructuredSelection selection = (IStructuredSelection) tableViewer
                .getSelection();
        if (selection != null) {
            definition = (SymbolDefinition) selection.getFirstElement();
        }

        return definition;
    }

    /**
     * The add set button has been selected.
     */
    private void handleAddSetButton() {
        try {
            String newSetName = definitionsComboViewer.getCombo().getText();
            SymbolDefinitionSet set = SymbolDefinitionSetRegistry.singleton
                    .addNewDefinitionSet(newSetName);

            definitionsComboViewer.refresh();
            definitionsComboViewer.setSelection(new StructuredSelection(set));
            tableViewer.setInput(set);

        } catch (PersistenceException e) {
            handleException("Error adding new definition set", e);
        }
    }

    /**
     * The add symbol button has been selected.
     */
    private void handleAddSymbolButton() {
        // Find a new symbol name that doesn't already exist
        // in the list
        String symbolName = "NewSymbol";
        for (int i = 1; i < 100; i++) {
            symbolName = "NewSymbol" + i;
            if (!currentDefinitions.contains(symbolName)) {
                break;
            }
        }

        currentDefinitions.add(new SymbolDefinition(symbolName, "true"));
        tableViewer.refresh();
    }

    /**
     * An exception has occured. Handle it appropriately.
     * 
     * @param t
     */
    private void handleException(String message, Throwable t) {
        MTJCorePlugin.log(IStatus.WARNING, message, t);
        MessageDialog.openError(
                workbench.getActiveWorkbenchWindow().getShell(),
                "Error Occured", message);
    }

    /**
     * The remove set button has been selected.
     */
    private void handleRemoveSetButton() {
        SymbolDefinitionSet set = getSelectedSymbolDefinitionSet();
        if (set != null) {
            SymbolDefinitionSetRegistry registry = SymbolDefinitionSetRegistry.singleton;
            registry.removeDefinitionSet(set);

            definitionsComboViewer.refresh();
            tableViewer.refresh();
        }
    }

    /**
     * The remove symbol button has been selected.
     */
    private void handleRemoveSymbolButton() {
        Table table = tableViewer.getTable();
        int index = table.getSelectionIndex();
        if (index != -1) {
            currentDefinitions.remove(index);
            tableViewer.refresh();
        }
    }

    /**
     * Initialize the definitions combo box and selection
     */
    private void initializeDefinitionsCombo() {
        Object inputObject = new Object(); // Instance doesn't matter for this
        // provider

        definitionsComboViewer.setInput(inputObject);
        IStructuredContentProvider contentProvider = (IStructuredContentProvider) definitionsComboViewer
                .getContentProvider();
        Object[] content = contentProvider.getElements(inputObject);

        if ((content != null) && (content.length > 0)) {
            definitionsComboViewer.setSelection(new StructuredSelection(
                    content[0]), true);
        }
    }

    /**
     * Return a boolean indicating whether the specified set name is valid.
     * 
     * @param name
     * @return
     */
    private boolean isValidSetName(String name) {
        boolean isValid = (name != null) && (name.trim().length() > 0);

        for (int i = 0; isValid && (i < name.length()); i++) {
            char c = name.charAt(i);
            isValid = (c == ' ') || (Character.isLetterOrDigit(c));
        }

        return isValid;
    }

    /**
     * Return a boolean indicating whether the specified symbol is valid.
     * 
     * @param symbol
     * @return
     */
    private boolean isValidSymbol(String symbol) {
        boolean valid = false;

        if (symbol != null) {
            Matcher matcher = WHITESPACE_PATTERN.matcher(symbol);
            valid = !matcher.matches();
        }

        return valid;
    }

    /**
     * Reload the symbol definitions registry from disk.
     */
    private boolean reloadSymbolDefinitionsRegistry() {
        boolean succeeded = true;

        try {
            SymbolDefinitionSetRegistry.singleton.load();
        } catch (PersistenceException e) {
            succeeded = false;
            handleException("Error reloading symbol definitions", e);
        }

        return succeeded;
    }

    /**
     * Update the enablement of the add/remove buttons.
     */
    private void updateButtonEnablement() {
        String typedSetName = definitionsComboViewer.getCombo().getText();
        SymbolDefinitionSet typedSet = null;
        try {
            typedSet = SymbolDefinitionSetRegistry.singleton
                    .getSymbolDefinitionSet(typedSetName);
        } catch (PersistenceException e) {
            // This should be safe to ignore...
        }

        SymbolDefinitionSet selectedSet = getSelectedSymbolDefinitionSet();
        addSetButton.setEnabled(isValidSetName(typedSetName)
                && (selectedSet == null) && (typedSet == null));
        removeSetButton.setEnabled(selectedSet != null);

        SymbolDefinition selectedDefinition = getSelectedSymbolDefinition();
        addSymbolButton.setEnabled(selectedSet != null);
        removeSymbolButton.setEnabled(selectedDefinition != null);
    }
}
