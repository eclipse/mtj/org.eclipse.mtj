/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.editor.jad.form;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.ui.internal.editor.jad.source.JADSourceEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.IFormPage;
import org.eclipse.ui.ide.IDE;

/**
 * A page editor for the Application Descriptor source
 * 
 * @author Diego Madruga Sandin
 */
public class JADSourceEditorPage extends JADSourceEditor implements IFormPage {

    public static final String ID = "Application Descriptor";

    private JADFormEditor editor;
    private int index;
    private Control control;

    /**
     * Creates a new Application Descriptor Source Editor Page
     * 
     * @param editor the
     * @param id
     * @param title
     */
    public JADSourceEditorPage(JADFormEditor editor) {
        super();
        initialize(editor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#canLeaveThePage()
     */
    public boolean canLeaveThePage() {
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#getEditor()
     */
    public FormEditor getEditor() {
        return this.editor;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#getId()
     */
    public String getId() {
        return ID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#getIndex()
     */
    public int getIndex() {
        return index;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#getManagedForm()
     */
    public IManagedForm getManagedForm() {
        // not a form page
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#getPartControl()
     */
    public Control getPartControl() {
        return control;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#initialize(org.eclipse.ui.forms.editor.FormEditor)
     */
    public void initialize(FormEditor editor) {
        this.editor = (JADFormEditor) editor;

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#isActive()
     */
    public boolean isActive() {
        return this.equals(editor.getActivePageInstance());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#isEditor()
     */
    public boolean isEditor() {
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#selectReveal(java.lang.Object)
     */
    public boolean selectReveal(Object object) {
        if (object instanceof IMarker) {
            IDE.gotoMarker(this, (IMarker) object);
            return true;
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#setActive(boolean)
     */
    public void setActive(boolean active) {

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.IFormPage#setIndex(int)
     */
    public void setIndex(int index) {
        this.index = index;

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    public void createPartControl(Composite parent) {
        super.createPartControl(parent);
        Control[] children = parent.getChildren();
        control = children[children.length - 1];
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.editors.text.TextEditor#isSaveAsAllowed()
     */
    public boolean isSaveAsAllowed() {
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.texteditor.AbstractTextEditor#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    public void doSave(IProgressMonitor monitor) {
        super.doSave(monitor);
    }

}
