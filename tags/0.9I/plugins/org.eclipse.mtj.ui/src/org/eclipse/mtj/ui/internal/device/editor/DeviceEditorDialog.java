/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.device.editor;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

/**
 * Dialog implementation for the editing of a device.
 * 
 * @author Craig Setera
 */
public class DeviceEditorDialog extends Dialog {

    // Widgets
    private Label errorLabel;
    private TabFolder tabFolder;
    private AbstractDeviceEditorPage[] pages;

    // The device begin edited
    private IDevice device;

    /**
     * Construct a new editor dialog.
     * 
     * @param parentShell
     */
    public DeviceEditorDialog(Shell parentShell) {
        super(parentShell);
    }

    /**
     * Set the device to be edited.
     * 
     * @param device
     */
    public void setDevice(IDevice device) {
        this.device = device;
        if (pages != null) {
            setDeviceOnPages(device);
        }
    }

    /**
     * Construct a new editor dialog.
     * 
     * @param parentShell
     */
    public DeviceEditorDialog(IShellProvider parentShell) {
        super(parentShell);
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
     */
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.OK_ID) {
            try {
                for (int i = 0; i < pages.length; i++) {
                    pages[i].commitDeviceChanges();
                }
            } catch (CoreException e) {
                handleException("Error saving device", e);
            }
        }

        super.buttonPressed(buttonId);
    }

    /**
     * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
     */
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);

        if (device != null) {
            newShell.setText("Edit " + device.getName() + " Definition");
        } else {
            newShell.setText("Edit Device Definition");
        }
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    protected Control createDialogArea(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, true));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        errorLabel = new Label(composite, SWT.NONE);
        errorLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        tabFolder = new TabFolder(composite, SWT.TOP);
        tabFolder.setLayoutData(new GridData(GridData.FILL_BOTH));

        pages = new AbstractDeviceEditorPage[] {
                new DeviceBasicEditorPage(tabFolder, isJavaExecutableDevice(),
                        SWT.NONE),
                new DeviceLibrariesEditorPage(tabFolder, SWT.NONE),
                new DevicePropertiesEditorPage(tabFolder, SWT.NONE), };

        for (int i = 0; i < pages.length; i++) {
            AbstractDeviceEditorPage page = pages[i];
            page.setDialog(this);

            TabItem item = new TabItem(tabFolder, SWT.NONE);
            item.setControl(page);
            item.setText(page.getTitle());

            if (device != null) {
                page.setDevice(device);
            }
        }

        return composite;
    }

    /**
     * Return a boolean indicating whether the device being edited is launched
     * by a Java executable.
     * 
     * @return
     */
    protected boolean isJavaExecutableDevice() {
        return false;
    }

    /**
     * Set the error message to be displayed.
     * 
     * @param message
     */
    void setErrorMessage(String message) {
        errorLabel.setText(message == null ? "" : message);
    }

    /**
     * Update the completion state of the dialog. Disable the OK button if not
     * yet valid.
     */
    void updateCompletionState() {
        if (pages != null) {
            boolean complete = true;

            for (int i = 0; i < pages.length; i++) {
                if (!pages[i].isValid()) {
                    complete = false;
                    break;
                }
            }

            Button okButton = getButton(IDialogConstants.OK_ID);
            if (okButton != null) {
                okButton.setEnabled(complete);
            }
        }
    }

    /**
     * Handle the specified exception by displaying to the user and logging.
     * 
     * @param message
     * @param throwable
     */
    private void handleException(String message, Throwable throwable) {
        MTJCorePlugin.log(IStatus.WARNING, "Error saving device", throwable);
        MTJUIPlugin.displayError(getShell(), IStatus.WARNING, -999,
                "Error saving device", message, throwable);
    }

    /**
     * Set the device onto the pages.
     * 
     * @param device2
     */
    private void setDeviceOnPages(IDevice device2) {
        for (int i = 0; i < pages.length; i++) {
            pages[i].setDevice(device2);
        }
    }
}
