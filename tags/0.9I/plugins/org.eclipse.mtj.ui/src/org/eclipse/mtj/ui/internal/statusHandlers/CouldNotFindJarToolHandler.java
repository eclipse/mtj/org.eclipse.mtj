/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */

package org.eclipse.mtj.ui.internal.statusHandlers;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * Status handler for the case when the JAR tool executable could not be found
 * during preverification.
 * 
 * @author Craig Setera
 */
public class CouldNotFindJarToolHandler implements IStatusHandler {
    
    /**
     * @see org.eclipse.debug.core.IStatusHandler#handleStatus(org.eclipse.core.runtime.IStatus,
     *      java.lang.Object)
     */
    public Object handleStatus(final IStatus status, Object source)
            throws CoreException {
        Display display = Display.getCurrent();
        Shell shell = display.getActiveShell();
        if (shell == null) {
            shell = new Shell(display, SWT.NONE);
        }

        final Shell finalShell = shell;

        display.syncExec(new Runnable() {
            public void run() {
                String title = "Could not find jar tool executable";
                String message = "Could not find jar tool executable.\n"
                        + "The jar tool requires a full JDK installation.\n"
                        + "Specify a full JDK installation in the Java preferences.";
                ErrorDialog.openError(finalShell, title, message, status);
            }
        });

        return null;
    }
}
