/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Refactoring the page to support extensibility                          
 */
package org.eclipse.mtj.ui.jadEditor;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.mtj.ui.internal.editor.jad.form.JADFormEditor;
import org.eclipse.mtj.ui.internal.utils.ManifestPreferenceStore;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

/**
 * Abstract superclass of the multipage editor pages.
 * 
 * @author Craig Setera
 */
public abstract class AbstractJADEditorPage extends FormPage {

    // The editor we are operating within
    private JADFormEditor editor;
    private Section section;

    // Whether the page contents are currently dirty
    private boolean dirty;

    public AbstractJADEditorPage(String id) {
        this(id, id);
    }

    /**
     * Constructor
     */
    public AbstractJADEditorPage(String id, String title) {
        super(id, title);
    }

    /**
     * Constructor
     */
    public AbstractJADEditorPage(JADFormEditor editor, String id, String title) {
        super(editor, id, title);
        this.editor = editor;
    }

    /**
     * @see org.eclipse.ui.IWorkbenchPart#dispose()
     */
    public void dispose() {
        super.dispose();
    }

    /**
     * @see org.eclipse.ui.ISaveablePart#doSaveAs()
     */
    public void doSaveAs() {
        // Not allowed...
    }

    /**
     * @see org.eclipse.ui.IEditorPart#gotoMarker(org.eclipse.core.resources.IMarker)
     */
    public void gotoMarker(IMarker marker) {
        // Nothing to do here
    }

    /**
     * @see org.eclipse.ui.ISaveablePart#isDirty()
     */
    public boolean isDirty() {
        return dirty || getPreferenceStore().needsSaving() || super.isDirty();
    }

    /**
     * @see org.eclipse.ui.ISaveablePart#isSaveAsAllowed()
     */
    public boolean isSaveAsAllowed() {
        return false;
    }

    /**
     * Let the editor know that the dirty state has changed.
     */
    public void editorDirtyStateChanged() {
        editor.editorDirtyStateChanged();
    }

    /**
     * An indication to this page that the editor input has been updated
     * external to Eclipse and the page should be updated.
     */
    public abstract void editorInputChanged();

    /**
     * Return a boolean indicating whether the specified property is managed by
     * this page.
     * 
     * @param property
     * @return
     */
    public boolean isManagingProperty(String property) {
        return false;
    }

    /**
     * Get the preference store being edited.
     */
    protected ManifestPreferenceStore getPreferenceStore() {
        return editor.getPreferenceStore();
    }

    /**
     * Create the top-level composite placed within a section.
     * 
     * @param managedForm
     * @return
     */
    protected Composite createSectionComposite(IManagedForm managedForm) {
        FormToolkit toolkit = managedForm.getToolkit();

        Composite formBody = managedForm.getForm().getBody();
        formBody.setLayoutData(new GridData(GridData.FILL_BOTH));
        formBody.setLayout(new GridLayout(1, false));

        int style = Section.TITLE_BAR | Section.CLIENT_INDENT
                | Section.EXPANDED | Section.DESCRIPTION;
        section = toolkit.createSection(formBody, style);
        section.setLayoutData(new GridData(GridData.FILL_BOTH));
        section.setLayout(new GridLayout(1, false));

        section.setText(getSectionTitle());
        section.setDescription(getSectionDescription());
        toolkit.createCompositeSeparator(section);

        Composite composite = toolkit.createComposite(section);
        section.setClient(composite);
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        return composite;
    }

    /**
     * Return the title for the section heading.
     * 
     * @return
     */
    protected abstract String getSectionTitle();

    /**
     * Return the description for the section heading.
     * 
     * @return
     */
    protected abstract String getSectionDescription();

    /**
     * Set the dirty flag and let the editor know the state has changed.
     * 
     * @param dirty
     */
    protected void setDirty(boolean dirty) {
        this.dirty = dirty;
        editorDirtyStateChanged();
    }

    public void setEditor(JADFormEditor editor) {
        initialize(editor);
        this.editor = editor;
    }

    /**
     * Override the super method to bypass it's default behavior. This method is
     * not intended to be override by clients.
     * 
     * @see org.eclipse.ui.part.EditorPart#setInitializationData(org.eclipse.core.runtime.IConfigurationElement,
     *      java.lang.String, java.lang.Object)
     */

    public void setInitializationData(IConfigurationElement cfig,
            String propertyName, Object data) {
        // do nothing
    }
}
