/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Hugo Raniere (Motorola)  - Wizard class must be aware of device changes to handle the classpath correctly
 */
package org.eclipse.mtj.ui.internal.wizards.project.page;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.project.impl.MidletSuiteProject;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.devices.DeviceSelector;
import org.eclipse.mtj.ui.internal.wizards.project.NewMIDLetProjectWizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * Wizard page used to define the initial properties for the created MIDlet
 * project.
 * 
 * @author Craig Setera
 */
public class NewMidletProjectPropertiesPage extends WizardPage {

    /**
     * The key used to register this page in the wizard
     */
    public static final String PAGE_NAME = "NewMidletProjectProperties";

    // Widget variables
    private DeviceSelector deviceSelector;
    private Text jadFileNameText;
    private boolean jadNameInitialized;

    private int deviceCount = 0;

    /**
     * Construct a NewMidletProjectPropertiesPage instance.
     */
    public NewMidletProjectPropertiesPage() {
        super(PAGE_NAME);

        /* Setting the page title and description */
        setTitle(MTJUIStrings.getString("wiz.newproj.properties.title"));
        setDescription(MTJUIStrings
                .getString("wiz.newproj.properties.description"));

        deviceCount = getDeviceCount();

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        Font font = parent.getFont();

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setFont(font);
        setControl(composite);
        initializeDialogUnits(parent);

        composite.setLayout(new GridLayout(3, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        // Create the device selection controls
        deviceSelector = new DeviceSelector();
        deviceSelector.createContents(composite, true);
        deviceSelector
                .setSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        updateStatus();
                    }
                });

        (new Label(composite, SWT.NONE)).setText("Application Descriptor:");
        jadFileNameText = new Text(composite, SWT.BORDER);
        jadFileNameText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        jadFileNameText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                updateStatus();
            }
        });

        updateStatus();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.DialogPage#dispose()
     */
    public void dispose() {
        deviceSelector.dispose();
        super.dispose();
    }

    /**
     * Return the device selected by the user.
     * 
     * @return
     */
    public IDevice getSelectedDevice() {
        return deviceSelector.getSelectedDevice();
    }

    /**
     * Return the JAD file name specified by the user.
     * 
     * @return
     */
    public String getJadFileName() {
        return jadFileNameText.getText();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.DialogPage#setVisible(boolean)
     */
    public void setVisible(boolean visible) {
        if (visible) {
            if (!jadNameInitialized) {
                jadFileNameText.setText(getDefaultJadName());
                jadNameInitialized = true;
            }
        }

        super.setVisible(visible);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.wizard.WizardPage#isPageComplete()
     */
    @Override
    public boolean isPageComplete() {

        return isValidDeviceCount() && isValidDeviceSelection()
                && isValidJadName();
    }

    /**
     * Return the default JAD file name.
     * 
     * @return
     */
    private String getDefaultJadName() {
        IProject project = ((NewMIDLetProjectWizard) getWizard())
                .getCreatedProjectHandle();
        return MidletSuiteProject.getDefaultJadFileName(project);
    }

    /**
     * Set the status based on the current values on the page.
     */
    private void updateStatus() {
        String message = null;

        deviceCount = getDeviceCount();

        if (!isValidDeviceCount()) {
            message = "There are no devices defined.\n Please use device "
                    + "management to create some devices.\n";
        } else if (!isValidDeviceSelection()) {
            message = "No Device Selected\n";
        } else if (!isValidJadName()) {
            message = "Invalid JAD name.";
        }

        setErrorMessage(message);
        if (message == null) {
            setPageComplete(true);
            ((NewMIDLetProjectWizard) getWizard())
                    .setProjectDevice(getSelectedDevice());
        } else {
            setPageComplete(false);
        }

    }

    /**
     * @return
     */
    private int getDeviceCount() {
        int count = 0;
        /* Check the number of devices already registered */
        DeviceRegistry registry = DeviceRegistry.singleton;

        try {
            count = registry.getDeviceCount();
        } catch (PersistenceException e) {
            MTJCorePlugin.log(IStatus.WARNING, "Error retrieving device count",
                    e);
        }
        return count;
    }

    /**
     * Check if the user specified a valid JAD name
     * 
     * @return if the JAD name is valid
     */
    private boolean isValidJadName() {
        String jadFileName = getJadFileName();

        boolean isValid = false;

        if ((jadFileName != null) && (jadFileName.length() > 0)
                && (jadFileName.charAt(0) != '/')
                && (jadFileName.charAt(0) != '\\')
                && (jadFileName.endsWith(".jad"))) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * Check if at least one device was imported by user
     * 
     * @return if at least one device was imported
     */
    private boolean isValidDeviceCount() {
        return (deviceCount <= 0) ? false : true;
    }

    /**
     * Check if the user selected one device
     * 
     * @return if the user selected one device
     */
    private boolean isValidDeviceSelection() {
        return (getSelectedDevice() == null) ? false : true;
    }

}
