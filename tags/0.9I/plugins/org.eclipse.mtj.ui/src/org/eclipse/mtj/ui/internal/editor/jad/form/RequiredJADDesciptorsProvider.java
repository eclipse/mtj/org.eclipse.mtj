/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma 		(Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.editor.jad.form;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.jad.DescriptorPropertyDescription;
import org.eclipse.mtj.core.model.jad.IJADConstants;
import org.eclipse.mtj.core.model.library.LibrarySpecification;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.jadEditor.IJADDescriptorsProvider;
import org.eclipse.mtj.ui.jadEditor.ListDescriptorPropertyDescription;

/**
 * provide required JAD attributes descriptors
 * 
 * @author Gang Ma
 */
public class RequiredJADDesciptorsProvider implements IJADDescriptorsProvider {

    /**
     * Required property descriptors.
     */
    private static final DescriptorPropertyDescription[] REQUIRED_DESCRIPTORS = new DescriptorPropertyDescription[] {
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_JAR_URL,
                    getResourceString("property.jad.midlet_jar_url"),
                    DescriptorPropertyDescription.DATATYPE_URL),
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_NAME,
                    getResourceString("property.jad.midlet_name"),
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_VENDOR,
                    getResourceString("property.jad.midlet_vendor"),
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_VERSION,
                    getResourceString("property.jad.midlet_version"),
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new ListDescriptorPropertyDescription(
                    IJADConstants.JAD_MICROEDITION_CONFIG,
                    getResourceString("property.jad.microedition_configuration"),
                    getConfigurationNamesAndValues()),
            new ListDescriptorPropertyDescription(
                    IJADConstants.JAD_MICROEDITION_PROFILE,
                    getResourceString("property.jad.microedition_profile"),
                    getProfileNamesAndValues()), };

    /**
     * Get a string value from the resource bundle
     * 
     * @param key
     * @return
     */
    protected static String getResourceString(String key) {
        return MTJUIStrings.getString(key);
    }

    /**
     * Return the J2ME configuration names and values.
     * 
     * @return
     */
    private static String[][] getConfigurationNamesAndValues() {
        String[][] namesAndValues = null;

        try {
            LibrarySpecification[] specifications = MTJCorePlugin
                    .getConfigurationSpecifications();

            namesAndValues = new String[specifications.length][];
            for (int i = 0; i < specifications.length; i++) {
                LibrarySpecification spec = specifications[i];
                namesAndValues[i] = new String[2];
                namesAndValues[i][0] = spec.getName();
                namesAndValues[i][1] = spec.getIdentifier();
            }
        } catch (CoreException e) {
            namesAndValues = new String[0][0];
            MTJCorePlugin.log(IStatus.WARNING, e);
        }

        return namesAndValues;
    }

    /**
     * Return the J2ME profile names and values.
     * 
     * @return
     */
    private static String[][] getProfileNamesAndValues() {
        String[][] namesAndValues = null;

        try {
            LibrarySpecification[] specifications = MTJCorePlugin
                    .getProfileSpecifications();

            namesAndValues = new String[specifications.length][];
            for (int i = 0; i < specifications.length; i++) {
                LibrarySpecification spec = specifications[i];
                namesAndValues[i] = new String[2];
                namesAndValues[i][0] = spec.getName();
                namesAndValues[i][1] = spec.getIdentifier();
            }
        } catch (CoreException e) {
            namesAndValues = new String[0][0];
            MTJCorePlugin.log(IStatus.WARNING, e);
        }

        return namesAndValues;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.jadEditor.IJADDescriptorsProvider#getDescriptorPropertyDescriptions()
     */
    public DescriptorPropertyDescription[] getDescriptorPropertyDescriptions() {
        return REQUIRED_DESCRIPTORS;
    }

}
