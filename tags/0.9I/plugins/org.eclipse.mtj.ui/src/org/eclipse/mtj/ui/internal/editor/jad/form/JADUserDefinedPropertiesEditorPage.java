/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Refactoring the page to add expansibilities
 */
package org.eclipse.mtj.ui.internal.editor.jad.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.internal.utils.ManifestPreferenceStore;
import org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * A property page editor for user-defined properties.
 * 
 * @author Craig Setera
 */
public class JADUserDefinedPropertiesEditorPage extends AbstractJADEditorPage {

    // Column property names
    private static final String PROP_KEY = "key";
    private static final String PROP_VALUE = "value";

    // All of the properties in order
    private static final String[] PROPERTIES = new String[] { PROP_KEY,
            PROP_VALUE };
    private static final List PROPERTY_LIST = Arrays.asList(PROPERTIES);

    /**
     * Implementation of the ICellModifier interface.
     */
    private class CellModifier implements ICellModifier {
        /**
         * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object,
         *      java.lang.String)
         */
        public boolean canModify(Object element, String property) {
            // All columns are modifiable
            return true;
        }

        /**
         * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object,
         *      java.lang.String)
         */
        public Object getValue(Object element, String property) {
            Object value = null;

            if (element instanceof KeyValuePair) {
                KeyValuePair pair = (KeyValuePair) element;

                int fieldIndex = getFieldIndex(property);
                if (fieldIndex != -1) {
                    value = pair.fields[fieldIndex];
                }
            }

            return value;
        }

        /**
         * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object,
         *      java.lang.String, java.lang.Object)
         */
        public void modify(Object element, String property, Object value) {
            if (element instanceof TableItem) {
                Object data = ((TableItem) element).getData();
                String newValue = (String) value;

                if (data instanceof KeyValuePair) {
                    int fieldIndex = getFieldIndex(property);
                    KeyValuePair pair = (KeyValuePair) data;

                    if (fieldIndex != -1) {
                        updateField(pair, property, fieldIndex, newValue);
                    }
                }
            }
        }

        /**
         * Return the field index to match the specified property name. Returns
         * <code>-1</code> if the property is not recognized.
         * 
         * @param property
         * @return
         */
        private int getFieldIndex(String property) {
            return PROPERTY_LIST.indexOf(property);
        }

        /**
         * Update the specified field as necessary.
         * 
         * @param pair
         * @param property
         * @param fieldIndex
         * @param newValue
         */
        private void updateField(KeyValuePair pair, String property,
                int fieldIndex, String newValue) {
            if (!pair.fields[fieldIndex].equals(newValue)) {
                pair.fields[fieldIndex] = newValue;
                setDirty(true);
                tableViewer.update(pair, new String[] { property });
            }
        }
    }

    /**
     * A holder class for a key/value pair.
     */
    private static class KeyValuePair {
        String[] fields;

        KeyValuePair(String key, String value) {
            fields = new String[] { key, value };
        }
    }

    /**
     * Implementation of the table's content provider.
     */
    private class TableContentProvider implements IStructuredContentProvider {
        /**
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            return (Object[]) userDefinedProperties
                    .toArray(new Object[userDefinedProperties.size()]);
        }

        /**
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /**
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer,
         *      java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    /**
     * Implementation of the table's label provider.
     */
    private static class TableLabelProvider extends LabelProvider implements
            ITableLabelProvider {
        /**
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
         *      int)
         */
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        /**
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
         *      int)
         */
        public String getColumnText(Object element, int columnIndex) {
            KeyValuePair pair = (KeyValuePair) element;
            return pair.fields[columnIndex];
        }
    }

    // The list of user-defined properties
    private List userDefinedProperties;

    // Widgets
    private TableViewer tableViewer;
    private Button removeButton;
    private Button addButton;

    /**
     * Constructor
     */
    public JADUserDefinedPropertiesEditorPage(JADFormEditor editor, String title) {
        super(editor, "user_defined", title);
        userDefinedProperties = new ArrayList();
    }

    public JADUserDefinedPropertiesEditorPage() {
        super("user_defined", getResourceString("editor.jad.tab.user_defined_properties"));
        userDefinedProperties = new ArrayList();
    }
    
    /**
     * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    public void doSave(IProgressMonitor monitor) {
        IPreferenceStore store = getPreferenceStore();
        Iterator entries = userDefinedProperties.iterator();
        while (entries.hasNext()) {
            KeyValuePair entry = (KeyValuePair) entries.next();
            store.setValue(entry.fields[0], entry.fields[1]);
        }

        setDirty(false);
    }

    /**
     * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    protected void createFormContent(IManagedForm managedForm) {
        FormToolkit toolkit = managedForm.getToolkit();

        // Set the top-level layout
        Composite parent = createSectionComposite(managedForm);
        parent.setLayoutData(new GridData(GridData.FILL_BOTH));
        parent.setLayout(new GridLayout(2, false));
        new Label(parent, SWT.NONE);
        new Label(parent, SWT.NONE);

        createTableViewer(toolkit, parent);
        createButtons(toolkit, parent);

        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_JADUserDefinedPropertiesEditorPage");
    }

    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#getSectionTitle()
     */
    protected String getSectionTitle() {
        return "User Defined Properties";
    }

    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#getSectionDescription()
     */
    protected String getSectionDescription() {
        return "User Defined properties may be specified on this page";
    }

    /**
     * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
     */
    public void setFocus() {
        tableViewer.getTable().setFocus();
    }

    /**
     * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
     */
    protected void setInput(IEditorInput input) {
        super.setInput(input);
        if (tableViewer != null) {
            tableViewer.setInput(input);
        }

        updateMidletProperties();
    }

    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#editorInputChanged()
     */
    public void editorInputChanged() {
        updateMidletProperties();
    }

    /**
     * Add a new item to the table.
     */
    private void addItem() {
        KeyValuePair keyValuePair = new KeyValuePair("New Key", "New Value");
        userDefinedProperties.add(keyValuePair);
        tableViewer.refresh();
        setDirty(true);
    }

    /**
     * Create the add and remove buttons to the composite.
     * 
     * @param toolkit the Eclipse Form's toolkit
     * @param parent
     */
    private void createButtons(FormToolkit toolkit, Composite parent) {
        Composite composite = toolkit.createComposite(parent);
        FillLayout layout = new FillLayout();
        layout.type = SWT.VERTICAL;
        composite.setLayout(layout);

        addButton = toolkit.createButton(composite,
                getResourceString("editor.button.add"), SWT.PUSH);
        addButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent se) {
                addItem();
            }
        });

        toolkit.createLabel(composite, "");

        removeButton = toolkit.createButton(composite,
                getResourceString("editor.button.remove"), SWT.PUSH);
        removeButton.setEnabled(false);
        removeButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent se) {
                removeSelectedItems();
            }
        });
    }

    /**
     * Create the table viewer for this editor.
     * 
     * @param toolkit The Eclipse form's toolkit
     * @param parent
     */
    private void createTableViewer(FormToolkit toolkit, Composite parent) {
        String[] columns = new String[] {
                getResourceString("property.jad.userdef.key"),
                getResourceString("property.jad.userdef.value"), };

        // Setup the table
        int styles = SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER
                | SWT.FULL_SELECTION;
        Table table = toolkit.createTable(parent, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        table.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                TableItem selected = (TableItem) e.item;
                removeButton.setEnabled(selected.getParent()
                        .getSelectionCount() > 0);
            }
        });
        tableViewer = new TableViewer(table);

        // Set the table layout on the table
        TableLayout layout = new TableLayout();

        int width = 100 / columns.length;
        for (int i = 0; i < columns.length; i++) {
            TableColumn column = new TableColumn(table, SWT.NONE);
            column.setText(columns[i]);
            layout.addColumnData(new ColumnWeightData(width));
        }
        table.setLayout(layout);

        // Set the content providers
        tableViewer.setContentProvider(new TableContentProvider());
        tableViewer.setLabelProvider(new TableLabelProvider());

        // Wire up the cell modification handling
        tableViewer.setCellModifier(new CellModifier());
        tableViewer.setColumnProperties(PROPERTIES);
        tableViewer.setCellEditors(new CellEditor[] {
                new TextCellEditor(table), new TextCellEditor(table), });

        // Get some data into the viewer
        tableViewer.setInput(getEditorInput());
        tableViewer.refresh();
    }

    /**
     * Return a boolean indicating whether the specified key is a user-defined
     * property.
     * 
     * @param key
     * @return
     */
    private boolean isUserDefinedPropertyKey(String key) {
        JADFormEditor jadEditor = (JADFormEditor) getEditor();
        return jadEditor.isUserDefinedPropertyKey(key);
    }

    /**
     * Remove the items currently selected within the table.
     */
    private void removeSelectedItems() {
        int[] indices = tableViewer.getTable().getSelectionIndices();

        for (int i = indices.length; i > 0; i--) {
            int index = indices[i - 1];
            KeyValuePair keyValuePair = (KeyValuePair) userDefinedProperties
                    .remove(index);
            getPreferenceStore().setToDefault(keyValuePair.fields[0]);
        }

        setDirty(true);
        tableViewer.refresh();
    }

    /**
     * Update the user properties from the current preference store.
     */
    private void updateMidletProperties() {
        userDefinedProperties.clear();
        ManifestPreferenceStore store = (ManifestPreferenceStore) getPreferenceStore();

        String[] names = store.preferenceNames();
        for (int i = 0; i < names.length; i++) {
            String propName = names[i];

            if (isUserDefinedPropertyKey(propName)) {
                userDefinedProperties.add(new KeyValuePair(propName, store
                        .getString(propName)));
            }
        }

        if (tableViewer != null)
            tableViewer.refresh();
    }
    
    /**
     * Get a string value from the resource bundle
     * 
     * @param key
     * @return
     */
    private static String getResourceString(String key) {
        return MTJUIStrings.getString(key);
    }
}
