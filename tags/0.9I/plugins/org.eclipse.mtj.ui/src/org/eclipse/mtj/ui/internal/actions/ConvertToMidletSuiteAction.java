/**
 * Copyright (c) 2004,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.ui.internal.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.core.model.project.impl.MidletSuiteProject;
import org.eclipse.mtj.core.nature.J2MENature;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.ui.internal.dialog.DeviceSelectDialog;

/**
 * An action delegate implementation for converting a Java Project to a J2ME
 * MIDlet Suite.
 * 
 * @author Craig Setera
 */
public class ConvertToMidletSuiteAction extends AbstractJavaProjectAction {

    /**
     * Construct a new action delegate.
     */
    public ConvertToMidletSuiteAction() {
        super();
    }

    /**
     * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
     */
    public void run(IAction action) {
        if ((selection != null) && !selection.isEmpty()) {
            // Get the platform definition to be used in the
            // conversion
            try {
                IDevice device = getDevice();
                if (device == null) {
                    MessageDialog.openError(getShell(),
                            "Error During Conversion",
                            "No Device Available or Selected for Conversion");
                } else {
                    convertSelectedProjects(device);
                }
            } catch (PersistenceException e) {
                handleException(e);
            }
        }
    }

    /**
     * Convert the specified java project to a Java ME MIDlet suite.
     * 
     * @param javaProject
     * @param device
     * @param monitor
     */
    private void convertProject(final IJavaProject javaProject,
            final IDevice device, final IProgressMonitor monitor)
            throws InvocationTargetException, InterruptedException,
            JavaModelException {
        // First, convert to a Java ME MIDlet Suite
        final IProject project = javaProject.getProject();
        String jadFileName = MidletSuiteProject.getDefaultJadFileName(project);
        MidletSuiteFactory.getMidletSuiteCreationRunnable(project, javaProject,
                device, jadFileName).run(monitor);

        // Now, remove the Java SE libraries
        removeJ2SELibraries(javaProject, monitor);
    }

    /**
     * Convert the selected projects using the specified platform definition.
     * 
     * @param device
     */
    private void convertSelectedProjects(IDevice device) {
        // Setup the progress monitoring
        ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());

        try {
            // Run as an atomic Workspace operation
            dialog.open();
            IProgressMonitor monitor = dialog.getProgressMonitor();
            MTJCorePlugin.getWorkspace().run(getRunnable(device), monitor);
            dialog.close();
        } catch (CoreException e) {
            handleException(e);
        }
    }

    /**
     * Get the device to be used when converting the projects.
     * 
     * @return
     * @throws PersistenceException
     */
    private IDevice getDevice() throws PersistenceException {
        IDevice device = null;

        // Check that there are platform definitions to choose
        // from...
        if (DeviceRegistry.singleton.getDeviceCount() > 0) {
            // Prompt the user
            DeviceSelectDialog dialog = new DeviceSelectDialog(getShell());

            if (dialog.open() == Dialog.OK) {
                device = dialog.getSelectedDevice();
            }
        }

        return device;
    }

    /**
     * Return the workspace runnable that will make all of the changes to
     * convert the projects to Java ME MIDlet suites.
     * 
     * @param def
     * @param dialog
     * @return
     */
    private IWorkspaceRunnable getRunnable(final IDevice def) {
        return new IWorkspaceRunnable() {
            public void run(IProgressMonitor monitor) throws CoreException {
                // Collect the projects to be converted
                monitor.beginTask("Converting Projects", selection.size());

                Iterator iter = selection.iterator();
                while (iter.hasNext()) {
                    IJavaProject javaProject = getJavaProject(iter.next());
                    if (javaProject != null) {
                        monitor.setTaskName("Converting Project "
                                + javaProject.getElementName());
                        boolean hasNature = false;
                        try {
                            IProject project = javaProject.getProject();
                            hasNature = J2MENature.hasJ2MENature(project);
                        } catch (CoreException e) { /* Munch */
                        }

                        if (!hasNature) {
                            try {
                                convertProject(javaProject, def, monitor);
                            } catch (InvocationTargetException e1) {
                                MTJCorePlugin.throwCoreException(IStatus.ERROR,
                                        -999, e1.getTargetException());
                            } catch (InterruptedException e1) {
                                MTJCorePlugin.throwCoreException(IStatus.ERROR,
                                        -999, e1);
                            }
                        }
                    }

                    monitor.worked(1);
                }

                monitor.done();
            }
        };
    }

    /**
     * Handle an exception during the conversion process.
     * 
     * @param e
     */
    private void handleException(Throwable e) {
        MTJCorePlugin.log(IStatus.ERROR, e);
        MessageDialog.openError(getShell(), "Error During Conversion", e
                .toString());
    }

    /**
     * Return a boolean indicating whether or not the specified class path entry
     * represents the standard J2SE libraries.
     * 
     * @param entry
     * @return
     */
    private boolean isJ2SELibraryEntry(IClasspathEntry entry) {
        boolean isJ2SEEntry = false;

        if (entry.getEntryKind() == IClasspathEntry.CPE_LIBRARY) {
            if (entry.getPath().lastSegment().equals("JRE_LIB")) {
                isJ2SEEntry = true;
            }
        } else if (entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER) {
            if (entry.getPath().lastSegment().equals(
                    "org.eclipse.jdt.launching.JRE_CONTAINER")) {
                isJ2SEEntry = true;
            }
        }
        return isJ2SEEntry;
    }

    /**
     * Remove the Java SE standard libraries from the specified IJavaProject
     * instance.
     * 
     * @param javaProject
     */
    private void removeJ2SELibraries(IJavaProject javaProject,
            IProgressMonitor monitor) throws JavaModelException {
        IClasspathEntry[] entries = javaProject.getRawClasspath();
        ArrayList list = new ArrayList();

        for (int i = 0; i < entries.length; i++) {
            if (!isJ2SELibraryEntry(entries[i])) {
                list.add(entries[i]);
            }
        }

        entries = (IClasspathEntry[]) list.toArray(new IClasspathEntry[list
                .size()]);
        javaProject.setRawClasspath(entries, monitor);
    }
}
