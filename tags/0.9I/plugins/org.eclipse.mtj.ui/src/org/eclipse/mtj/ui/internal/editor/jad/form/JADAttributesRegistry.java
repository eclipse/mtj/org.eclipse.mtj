/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma 		(Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.editor.jad.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.jad.DescriptorPropertyDescription;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.jadEditor.IJADDescriptorsProvider;

/**
 * Provide registry functionality for getting JAD attributes according to the
 * specify device and editor page.
 * 
 * @author Gang Ma
 */
public class JADAttributesRegistry {
    /**
     * The JAD attributes extension point
     */
    public static final String JAD_ATTRIBUTE_EXTENSION = "jadAttributes";

    /**
     * use to store generic JAD attributes(not vendor specific) for each page.
     */
    private static Map<String, JADAttributesConfigElement[]> genericPageJADAttrMap = new HashMap<String, JADAttributesConfigElement[]>();

    /**
     * all JAD attributes configElements
     */
    private static JADAttributesConfigElement[] allJADAttrElements;

    /**
     * @param pageID the target page's ID
     * @param device the device user used
     * @return the vendor specific JAD descriptors
     */
    public static DescriptorPropertyDescription[] getJADAttrDescriptorsByPage(
            String pageID) {
        JADAttributesConfigElement[] relatedElements = getRelatedAttrElements(pageID);
        DescriptorPropertyDescription[] resultAttributes = getDescriptorsFromElements(relatedElements);
        return resultAttributes;
    }

    /**
     * return the related jadAttributes configElements
     */
    private static JADAttributesConfigElement[] getRelatedAttrElements(
            String pageID) {
        JADAttributesConfigElement[] genericElements = null;

        // get the page's generic JAD attribute descriptors
        if (!genericPageJADAttrMap.containsKey(pageID)) {
            genericPageJADAttrMap.put(pageID, getGenericElements(pageID));
        }
        genericElements = genericPageJADAttrMap.get(pageID);

        return genericElements;
    }

    /**
     * @param pageID the JAD Editor page's ID
     * @return the generic jadAttributes configElements for specific page
     */
    private static JADAttributesConfigElement[] getGenericElements(String pageID) {
        JADAttributesConfigElement[] elements = getAllJADAttributeElements();
        return filterElementsByPageAndVendorSpec(elements, pageID, true);

    }

    private static JADAttributesConfigElement[] filterElementsByPageAndVendorSpec(
            JADAttributesConfigElement[] elements, String pageID,
            boolean excludeVendorSpec) {
        return filterElements(elements, null, pageID, excludeVendorSpec);
    }

    private static JADAttributesConfigElement[] filterElements(
            JADAttributesConfigElement[] elements, IDevice device,
            String pageID, boolean excludeVendorSpec) {
        ArrayList<JADAttributesConfigElement> resultDescriptorList = new ArrayList<JADAttributesConfigElement>();
        for (int i = 0; i < elements.length; i++) {
            JADAttributesConfigElement element = elements[i];
            boolean satisfied = true;
            if (device != null)
                satisfied &= element.isVendorSpec()
                        && isDeviceMatchVendor(device, element);
            if (pageID != null)
                satisfied &= element.getAttributesShowPage().equalsIgnoreCase(
                        pageID);
            if (excludeVendorSpec)
                satisfied &= !element.isVendorSpec();

            if (satisfied)
                resultDescriptorList.add(element);

        }
        return resultDescriptorList.toArray(new JADAttributesConfigElement[0]);
    }

    /**
     * judge whether the configuration element is for the device
     * 
     * @param device
     * @param element
     * @return if matched return true else false
     */
    private static boolean isDeviceMatchVendor(IDevice device,
            JADAttributesConfigElement element) {

        return true;
    }

    /**
     * @param elements config elements
     * @param pageID editor page ID
     * @param device target device
     * @return DescriptorPropertyDescription array
     */
    private static DescriptorPropertyDescription[] getDescriptorsFromElements(
            JADAttributesConfigElement[] elements) {
        ArrayList<DescriptorPropertyDescription> descriptorList = new ArrayList<DescriptorPropertyDescription>();
        for (int i = 0; i < elements.length; i++) {
            JADAttributesConfigElement element = elements[i];
            try {
                IJADDescriptorsProvider provider = element
                        .getJadDescriptorsProvider();
                DescriptorPropertyDescription[] descriptorArray = provider
                        .getDescriptorPropertyDescriptions();
                descriptorList.addAll(Arrays.asList(descriptorArray));

            } catch (Exception e) {
                MTJCorePlugin
                        .log(
                                IStatus.WARNING,
                                "Errors happens while judging the device has vendor specifc JAD attributes",
                                e);
            }
        }

        return descriptorList
                .toArray(new DescriptorPropertyDescription[descriptorList
                        .size()]);
    }

    private static JADAttributesConfigElement[] getAllJADAttributeElements() {
        if (allJADAttrElements == null) {
            allJADAttrElements = readAllJADAttributes();
        }

        return allJADAttrElements;
    }

    private static JADAttributesConfigElement[] readAllJADAttributes() {
        String plugin = MTJUIPlugin.getDefault().getBundle().getSymbolicName();
        IConfigurationElement[] configElements = Platform
                .getExtensionRegistry().getConfigurationElementsFor(plugin,
                        JAD_ATTRIBUTE_EXTENSION);

        JADAttributesConfigElement[] elements = new JADAttributesConfigElement[configElements.length];
        for (int i = 0; i < configElements.length; i++) {
            elements[i] = new JADAttributesConfigElement(configElements[i]);
        }

        return elements;
    }

    /**
     * class wrapped IConfigurationElement instance for jadAttributes
     * extension-point extensions
     * 
     * @author gma
     */
    static class JADAttributesConfigElement {
        private static final String JAD_DESCRIPTOR_PROVIDER_ELEMENT = "jadDescriptorsProvider";
        private static final String JAD_DESCRIPTOR_PROVIDER_CLASS = "class";
        private static final String JAD_ATTR_SHOW_PAGE = "pageID";
        private static final String VENDOR_SPEC_ATTR = "vendorSpec";

        private IConfigurationElement element;

        private IJADDescriptorsProvider jadDescriptorsProvider;

        public JADAttributesConfigElement(IConfigurationElement jadAttrElement) {
            this.element = jadAttrElement;
        }

        public IJADDescriptorsProvider getJadDescriptorsProvider()
                throws CoreException {
            if (jadDescriptorsProvider == null) {
                IConfigurationElement[] providers = element
                        .getChildren(JAD_DESCRIPTOR_PROVIDER_ELEMENT);
                if (providers != null && providers.length > 0) {
                    jadDescriptorsProvider = (IJADDescriptorsProvider) providers[0]
                            .createExecutableExtension(JAD_DESCRIPTOR_PROVIDER_CLASS);
                }
            }
            return jadDescriptorsProvider;
        }

        public String getAttributesShowPage() {
            return element.getAttribute(JAD_ATTR_SHOW_PAGE);
        }

        public boolean isVendorSpec() {
            String value = element.getAttribute(VENDOR_SPEC_ATTR);
            return value == null ? false : "true".equalsIgnoreCase(value);
        }

    }
}
