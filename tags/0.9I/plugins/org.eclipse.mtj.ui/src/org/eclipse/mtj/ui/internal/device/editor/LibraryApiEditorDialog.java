/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.device.editor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.mtj.core.model.Version;
import org.eclipse.mtj.core.model.library.api.API;
import org.eclipse.mtj.core.model.library.api.APIType;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.ui.viewers.TableViewerConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

/**
 * A dialog for the editing of the API's associated with a library in a
 * classpath.
 * 
 * @author Craig Setera
 */
public class LibraryApiEditorDialog extends Dialog {
    
    private static final Object[] NO_ELEMENTS = new Object[0];

    // Column property names
    private static final String PROP_IDENTIFIER = "identifier";
    private static final String PROP_VERSION = "version";
    private static final String PROP_TYPE = "type";
    private static final String PROP_NAME = "name";

    // All of the properties in order
    private static final String[] PROPERTIES = new String[] { PROP_IDENTIFIER,
            PROP_VERSION, PROP_TYPE, PROP_NAME };

    // Column information structure
    private static final int DEFAULT_TABLE_WIDTH = 650;
    private static final TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
            new TableColumnInfo("Identifier", 15f, null),
            new TableColumnInfo("Version", 15f, null),
            new TableColumnInfo("Type", 20f, null),
            new TableColumnInfo("Name", 50f, null), };

    // Label provider for API instances
    private static class APILabelProvider extends LabelProvider implements
            ITableLabelProvider {
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        public String getColumnText(Object element, int columnIndex) {
            API api = (API) element;
            String text = "";

            switch (columnIndex) {
            case 0:
                text = api.getIdentifier();
                break;

            case 1:
                text = api.getVersion().toString();
                break;

            case 2:
                text = api.getType().toString();
                break;

            case 3:
                text = api.getName();
                break;
            }

            return text;
        }
    }

    // A cell modifier implementation for the device libraries editor
    private class CellModifier implements ICellModifier {
        public boolean canModify(Object element, String property) {
            return true;
        }

        public Object getValue(Object element, String property) {
            Object value = null;
            API api = (API) element;

            switch (getColumnIndex(property)) {
            case 0:
                value = api.getIdentifier();
                break;

            case 1:
                value = api.getVersion().toString();
                break;

            case 2:
                value = Integer.valueOf(api.getType().getTypeCode());
                break;

            case 3:
                value = api.getName();
                break;
            }

            return value;
        }

        public void modify(Object element, String property, Object value) {
            TableItem item = (TableItem) element;
            API api = (API) item.getData();

            switch (getColumnIndex(property)) {
            case 0:
                api.setIdentifier((String) value);
                break;

            case 1:
                api.setVersion(new Version((String) value));
                break;

            case 2:
                Integer integerCode = (Integer) value;
                api.setType(APIType.typeForCode(integerCode.intValue()));
                break;

            case 3:
                api.setName((String) value);
                break;
            }

            viewer.refresh(api, true);
        }

        /**
         * Return the column index for the property.
         * 
         * @param property
         * @return
         */
        private int getColumnIndex(String property) {
            int index = -1;

            for (int i = 0; i < PROPERTIES.length; i++) {
                if (PROPERTIES[i].equals(property)) {
                    index = i;
                    break;
                }
            }

            return index;
        }
    }

    // Content provider that makes a library's API's available
    private class LibraryApiContentProvider implements
            IStructuredContentProvider {
        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            if (apis != null) {
                elements = (API[]) apis.toArray(new API[apis.size()]);
            }

            return elements;
        }

        public void dispose() {
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    // Widgets
    private TableViewer viewer;
    private List apis;

    /**
     * Construct a new dialog.
     * 
     * @param parentShell
     */
    public LibraryApiEditorDialog(Shell parentShell) {
        super(parentShell);
    }

    /**
     * Construct a new dialog.
     * 
     * @param parentShell
     */
    public LibraryApiEditorDialog(IShellProvider parentShell) {
        super(parentShell);
    }

    /**
     * Set the library to be edited.
     * 
     * @param library
     */
    void setAPIs(API[] apis) {
        this.apis = new ArrayList();
        this.apis.addAll(Arrays.asList(apis));
    }

    /**
     * Get the selected apis.
     * 
     * @return
     */
    API[] getAPIs() {
        return (apis == null) ? null : (API[]) apis
                .toArray(new API[apis.size()]);
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
     */
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            apis = null;
        }

        super.buttonPressed(buttonId);
    }

    /**
     * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
     */
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);

        String title = "Edit Library API's";
        newShell.setText(title);
    }

    /**
     * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    protected Control createDialogArea(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(2, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.minimumWidth = DEFAULT_TABLE_WIDTH;
        gridData.heightHint = 400;
        viewer = createTableViewer(composite);
        viewer.getTable().setLayoutData(gridData);

        Composite buttonComposite = new Composite(composite, SWT.NONE);
        buttonComposite.setLayout(new GridLayout(1, true));
        buttonComposite.setLayoutData(new GridData(GridData.FILL_VERTICAL));

        Button addButton = new Button(buttonComposite, SWT.PUSH);
        addButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        addButton.setText("Add");
        addButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                handleAddButton();
            }
        });

        final Button removeButton = new Button(buttonComposite, SWT.PUSH);
        removeButton.setEnabled(false);
        removeButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        removeButton.setText("Remove");
        removeButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                handleRemoveButton();
            }
        });

        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                API api = getSelectedAPI();
                removeButton.setEnabled(api != null);
            }
        });

        return composite;
    }

    /**
     * Create the devices table viewer.
     * 
     * @param parent
     */
    private TableViewer createTableViewer(Composite composite) {
        int styles = SWT.MULTI | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION;
        Table table = new Table(composite, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        // Wire up the viewer
        TableViewer viewer = new TableViewer(table);
        viewer.setContentProvider(new LibraryApiContentProvider());
        viewer.setLabelProvider(new APILabelProvider());

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings("librayApiViewerSettings");
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 0);
        viewerConfiguration.configure(viewer);

        // Wire up the cell modification handling
        viewer.setCellModifier(new CellModifier());
        viewer.setColumnProperties(PROPERTIES);
        viewer.setCellEditors(new CellEditor[] { new TextCellEditor(table),
                new TextCellEditor(table),
                new ComboBoxCellEditor(table, APIType.TYPE_STRINGS),
                new TextCellEditor(table), });

        viewer.setInput(new Object());

        return viewer;
    }

    /**
     * Return the API element currently selected or <code>null</code> if not
     * selected.
     * 
     * @return
     */
    private API getSelectedAPI() {
        IStructuredSelection selection = (IStructuredSelection) viewer
                .getSelection();
        return (API) selection.getFirstElement();
    }

    /**
     * Handle the add button being pressed.
     */
    private void handleAddButton() {
        API newApi = new API();
        newApi.setIdentifier("APIID");
        newApi.setName("New API");
        newApi.setType(APIType.UNKNOWN);
        newApi.setVersion(new Version("1.0"));

        apis.add(newApi);
        viewer.refresh();
    }

    /**
     * Handle the remove button being pressed.
     */
    private void handleRemoveButton() {
        API selected = getSelectedAPI();
        if (selected != null) {
            apis.remove(selected);
            viewer.refresh();
        }
    }
}
