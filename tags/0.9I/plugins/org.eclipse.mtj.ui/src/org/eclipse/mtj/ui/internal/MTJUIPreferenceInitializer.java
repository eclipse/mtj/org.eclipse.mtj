/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal;

import org.eclipse.core.runtime.Preferences;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.mtj.core.internal.MTJCorePreferenceInitializer;

/**
 * Preference initializer for default MTJ UI preferences.
 * 
 * @author Craig Setera
 */
public class MTJUIPreferenceInitializer extends AbstractPreferenceInitializer {

    /**
     * Construct a new Initializer instance.
     */
    public MTJUIPreferenceInitializer() {

        super();
    }

    /**
     * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
     */
    public void initializeDefaultPreferences() {

        // Synchronize the preferences between the core and UI plug-ins.
        Preferences prefs = MTJUIPlugin.getDefault().getPluginPreferences();
        MTJCorePreferenceInitializer.initializeDefaultPreferences(prefs);
    }
}
