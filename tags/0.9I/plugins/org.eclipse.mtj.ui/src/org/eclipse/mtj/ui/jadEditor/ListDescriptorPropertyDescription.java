/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Move the class to the new package so that 
 *     							  it can be used in other plug-ins
 */
package org.eclipse.mtj.ui.jadEditor;

import org.eclipse.mtj.core.model.jad.DescriptorPropertyDescription;

/**
 * Application descriptor property description holding a list of strings.
 * 
 * @author Craig Setera
 */
public class ListDescriptorPropertyDescription extends
        DescriptorPropertyDescription {

    // The list of names and values
    private String[][] namesAndValues;

    /**
     * Constructor
     * 
     * @param propertyName
     * @param displayName
     * @param namesAndValues
     */
    public ListDescriptorPropertyDescription(String propertyName,
            String displayName, String[][] namesAndValues) {
        super(propertyName, displayName, DATATYPE_LIST);
        this.namesAndValues = namesAndValues;
    }

    /**
     * @return Returns the namesAndValues.
     */
    public String[][] getNamesAndValues() {
        return namesAndValues;
    }
}
