/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma 		(Sybase) - Initial implementation   
 */
package org.eclipse.mtj.toolkit.uei.jadEditor;

import org.eclipse.mtj.core.model.jad.DescriptorPropertyDescription;
import org.eclipse.mtj.ui.jadEditor.JADPropertiesEditorPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;

/**
 * Nokia S60 specific JAD editor page.
 * 
 * @author Gang Ma
 */
public class NokiaSpecPropertiesEditorPage extends JADPropertiesEditorPage {

    public static final String ID = "nokiaSpecific";
    public static final String TITLE = "Nokia specific";

    private static final String NOKIA_S60_CATEGORY = "Nokia-MIDlet-Category";
    private static final String NOKIA_S60_CATEGORY_LBL = "Nokia-MIDlet-Category:";
    private static final String NOKIA_S60_ORIGIN_DISPLAY_SIZE = "Nokia-MIDlet-Original-Display-Size";
    private static final String NOKIA_S60_ORIGIN_DISPLAY_SIZE_LBL = "Nokia-MIDlet-Original-Display-Size:";
    private static final String NOKIA_S60_TARGET_DISPLAY_SIZE = "Nokia-MIDlet-Target-Display-Size";
    private static final String NOKIA_S60_TARGET_DISPLAY_SIZE_LBL = "Nokia-MIDlet-Target-Display-Size:";

    /**
     * List of application descriptor property descriptions
     */
    private static final DescriptorPropertyDescription[] NOKIA_S60_JAD_DESCRIPTORS = new DescriptorPropertyDescription[] {
            new DescriptorPropertyDescription(NOKIA_S60_CATEGORY,
                    NOKIA_S60_CATEGORY_LBL,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(NOKIA_S60_TARGET_DISPLAY_SIZE,
                    NOKIA_S60_ORIGIN_DISPLAY_SIZE_LBL,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(NOKIA_S60_ORIGIN_DISPLAY_SIZE,
                    NOKIA_S60_TARGET_DISPLAY_SIZE_LBL,
                    DescriptorPropertyDescription.DATATYPE_STRING) };

    /**
     * Creates a new NokiaSpecPropertiesEditorPage.
     */
    public NokiaSpecPropertiesEditorPage() {
        super(ID, TITLE);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.jadEditor.JADPropertiesEditorPage#doGetDescriptors()
     */
    @Override
    protected DescriptorPropertyDescription[] doGetDescriptors() {

        return NOKIA_S60_JAD_DESCRIPTORS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.jadEditor.JADPropertiesEditorPage#getSectionTitle()
     */
    protected String getSectionTitle() {
        return "Nokia Specific Properties";
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.jadEditor.JADPropertiesEditorPage#getSectionDescription()
     */
    protected String getSectionDescription() {
        return "Nokia specific JAD properties can be specified on this page";
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.jadEditor.JADPropertiesEditorPage#addContextHelp(org.eclipse.swt.widgets.Composite)
     */
    protected void addContextHelp(Composite c) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(c,
                "org.eclipse.mtj.ui.help_NokiaJADPropertiesEditorPage");
    }
}
