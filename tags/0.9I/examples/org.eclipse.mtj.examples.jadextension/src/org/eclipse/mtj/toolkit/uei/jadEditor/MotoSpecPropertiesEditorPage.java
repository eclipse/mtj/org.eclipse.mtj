/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma 		(Sybase) - Initial implementation
 *     
 */
package org.eclipse.mtj.toolkit.uei.jadEditor;

import org.eclipse.mtj.ui.jadEditor.JADPropertiesEditorPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;

/**
 * motorola specific JAD editor page.
 * 
 * @author gma
 */
public class MotoSpecPropertiesEditorPage extends JADPropertiesEditorPage {
    
    public static final String ID = "motoSpecific";
    public static final String TITLE = "Motorola specific";

   
    

    public MotoSpecPropertiesEditorPage() {
        super(ID, TITLE);
    }
    
    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#getSectionTitle()
     */
    protected String getSectionTitle() {
        return "Motorola Specific Properties";
    }

    /**
     * @see org.eclipse.mtj.ui.jadEditor.AbstractJADEditorPage#getSectionDescription()
     */
    protected String getSectionDescription() {
        return "Motorola specific JAD properties can be specified on this page";
    }

    protected void addContextHelp(Composite c) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(c,
                "org.eclipse.mtj.ui.help_MotorolaJADPropertiesEditorPage");
    }
}
