/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.toolkit.uei;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;

import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.toolkit.uei.properties.UEIDeviceDefinition;

/**
 * This manager handles the available Device Definitions found in the
 * <code>uei_device.properties</code> file.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class DeviceDefinitionManager {

    /**
     * The properties file holding our implementation information
     */
    public static final String PROPS_FILE = "uei_device.properties"; //$NON-NLS-1$

    /**
     * 
     */
    private static final String DEVICES = "devices"; //$NON-NLS-1$

    /**
     * The unique instance of the DeviceDefinitionManager.
     */
    private static DeviceDefinitionManager instance;

    /**
     * Get the single instance of DeviceDefinitionManager
     * 
     * @return the unique instance of the DeviceDefinitionManager
     */
    public static synchronized DeviceDefinitionManager getInstance() {
        if (instance == null) {
            instance = new DeviceDefinitionManager();
        }

        return instance;
    }

    /**
     * The list of UEI device invocations read from the properties file This
     * list holds instances of Properties
     */
    private List<UEIDeviceDefinition> deviceDefinitions = null;

    /**
     * Creates a new DeviceDefinitionManager.
     * <p>
     * When created, tries to read the device definitions from the
     * uei_device.properties file.
     * </p>
     */
    private DeviceDefinitionManager() {
        if (deviceDefinitions == null) {
            initializeDeviceDefinitions();
        }
    }

    /**
     * Return a device definition to be used in attempting UEI device definition
     * matching.
     * 
     * @param device the device from which the definitions will be used.
     * @return a device definition referent to the device argument.
     */
    public UEIDeviceDefinition getDeviceDefinition(final String device) {

        UEIDeviceDefinition deviceDef = null;
        Iterator<UEIDeviceDefinition> iterator = deviceDefinitions.iterator();

        while (iterator.hasNext() && (deviceDef == null)) {

            UEIDeviceDefinition defElement = iterator.next();
            Matcher matcher = defElement.getMatchPattern().matcher(device);
            if (matcher.find()) {
                deviceDef = defElement;
            }
        }

        return deviceDef;
    }

    /**
     * Try to create an {@link InputStream} for reading from the
     * <code>uei_device.properties</code> file.
     * 
     * @return an InputStream for reading from the file.
     */
    private InputStream getUEIDevicesPropertiesStream() {

        InputStream stream = null;

        try {
            URL propsFileURL = UeiPlugin.getDefault().getBundle().getEntry(
                    PROPS_FILE);

            if (propsFileURL != null) {
                stream = propsFileURL.openStream();
            } else {
                UeiPlugin
                        .debugLog(Messages.UEIDeviceImporter_getUEIDevicesPropertiesStream_file_not_found);
            }
        } catch (IllegalStateException e) {
            UeiPlugin
                    .debugLog(
                            Messages.UEIDeviceImporter_getUEIDevicesPropertiesStream_IllegalState,
                            e);
        } catch (IOException ioe) {
            UeiPlugin
                    .debugLog(
                            Messages.UEIDeviceImporter_getUEIDevicesPropertiesStream_IOError,
                            ioe);
        }

        return stream;
    }

    /**
     * Read in the device definitions from the properties file.
     * 
     * @return the list of device definitions available in the
     *         uei_device.properties file.
     */
    private void initializeDeviceDefinitions() {

        deviceDefinitions = new ArrayList<UEIDeviceDefinition>();
        Properties defProps = new Properties();
        InputStream is = getUEIDevicesPropertiesStream();

        if (is != null) {
            try {
                defProps.load(is);
            } catch (IOException ioe) {
                UeiPlugin.debugLog(
                        Messages.DeviceDefinitionManager_failed_reading_file,
                        ioe);
            } catch (IllegalArgumentException iae) {
                UeiPlugin
                        .debugLog(
                                Messages.DeviceDefinitionManager_malformed_unicode_esc_seq,
                                iae);
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    UeiPlugin
                            .debugLog(Messages.DeviceDefinitionManager_readDeviceDefinitions_closeStream_fail);
                }
            }

            String devCatList = defProps.getProperty(DEVICES,
                    Utils.EMPTY_STRING);

            if (!devCatList.equals(Utils.EMPTY_STRING)) {

                String[] deviceCategories = devCatList.split(","); //$NON-NLS-1$

                for (String element : deviceCategories) {
                    try {

                        /* Try to add device definitions to the list */
                        deviceDefinitions.add(new UEIDeviceDefinition(element,
                                defProps));

                    } catch (IllegalArgumentException iae) {
                        UeiPlugin.debugLog(iae.getMessage(), iae);
                    } catch (NullPointerException npe) {
                        UeiPlugin.debugLog(npe.getMessage(), npe);
                    }
                }
            } else {
                UeiPlugin
                        .debugLog(Messages.DeviceDefinitionManager_readDeviceDefinitions_invalid_file);
            }
        }
    }
}
