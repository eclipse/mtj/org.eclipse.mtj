/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.refactoring;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.mapping.IResourceChangeDescriptionFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jdt.core.IType;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RenameParticipant;
import org.eclipse.ltk.core.refactoring.participants.ResourceChangeChecker;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.build.MTJBuildProperties;
import org.eclipse.osgi.util.NLS;

/**
 * BuildPropertiesRenameParticipant class is responsible for updating
 * build.properties file references to resources after they have been renamed.
 * 
 * @author David Marques
 */
public class BuildPropertiesRenameParticipant extends RenameParticipant {

	private IType type;
	private IFile file;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#
	 * checkConditions(org.eclipse.core.runtime.IProgressMonitor,
	 * org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext)
	 */
	public RefactoringStatus checkConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws OperationCanceledException {
		ResourceChangeChecker checker = (ResourceChangeChecker) context.getChecker(ResourceChangeChecker.class);
		if (checker != null) {
			if (file.exists()) {
				IResourceChangeDescriptionFactory factory = checker.getDeltaFactory();
				factory.change(file);
			}
		}
		return new RefactoringStatus();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#
	 * createChange(org.eclipse.core.runtime.IProgressMonitor)
	 */
	public Change createChange(IProgressMonitor pm) throws CoreException,
			OperationCanceledException {
		Change result = null;
		
		IResource resource = type.getResource();
		if (resource != null) {
			IPath resPath = resource.getProjectRelativePath();
			IPath newPath = resPath.removeLastSegments(1);
			newPath = newPath.append(NLS.bind("{0}.java", getArguments().getNewName())); //$NON-NLS-1$
			
			CompositeChange compositeChange = new CompositeChange(Messages.BuildPropertiesRenameParticipant_buildPropertiesChangeMessage);
			BuildPropertiesChange propertiesChange = new BuildPropertiesChange(
					file, resPath.toString(), newPath.toString());
			if (propertiesChange.hasPropertiesChanges()) {
				compositeChange.add(propertiesChange);
				result = compositeChange;
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#
	 * initialize(java.lang.Object)
	 */
	protected boolean initialize(Object element) {
		boolean result = false;
		if (element instanceof IType) {
			this.type = (IType) element;
			
			IProject project = this.type.getJavaProject().getProject();
			file = project.getFile(MTJBuildProperties.FILE_NAME);
			result = true;
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#getName()
	 */
	public String getName() {
		return Messages.BuildPropertiesRenameParticipant_buildPropertiesParticipantName;
	}

}
