/**
 * Copyright (c) 2004,2009 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 */
package org.eclipse.mtj.internal.core.project.midp;

import java.io.File;

import org.eclipse.core.runtime.CoreException;

/**
 * This interface provides the methods used to sign a MIDlet suite.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 * @author Kevin Hunter
 */
public interface IJadSignature {

    /**
     * Computes the JAR file signature on the specified file.
     * 
     * @param jarFile <code>File</code> of the JAR to be signed.
     * @throws CoreException A variety of error conditions can occur, which are
     *             wrapped in a CoreException. See the SIGNING error codes in
     *             MTJCoreErrors.
     */

    public void computeSignature(File jarFile) throws CoreException;

    /**
     * Returns the JAR signature string that should be added to the JAD file
     * using the key "MIDlet-Jar-RSA-SHA1" following to a call to
     * <code>computeSignature</code>.
     * 
     * @return <code>String</code> containing the encrypted JAR signature.
     */
    public String getJarSignatureString();

    /**
     * Returns an array of Strings containing the encoded certificates that can
     * be used to verify the MIDlet suite signature following to a call to
     * <code>computeSignature</code>. These Strings should be added to the JAD
     * file using the tags "MIDlet-Certificate-1-1" through
     * "MIDlet-Certificate-1-n" (where "n" is the number of certificates)
     * 
     * @return Array of <code>String</code>s containing encoded certificates.
     */
    public String[] getCertificateStrings();
}
