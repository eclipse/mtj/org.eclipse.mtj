/**
 * Copyright (c) 2004,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Reimplementing JavaMEClasspathContainer
 */
package org.eclipse.mtj.internal.core.build.preverifier.builder;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.project.midp.JavaMEClasspathContainer;
import org.eclipse.mtj.internal.core.text.IResourceFilter;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * A resource filter implementation to handle filtering of the source folder
 * resources.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * 
 * @author Craig Setera
 */
public class SourceResourcesFilter implements IResourceFilter {
    // The ignored file name suffixes
    private static final String[] IGNORED_FILE_SUFFIXES = new String[] {
            "java", "jad" };

    // Ignored filenames in a project directory
    private static final String[] IGNORED_PROJECT_FILES = new String[] {
            ".classpath", ".project", ".mtj" };

    // The .settings folder should always be ignored for resource copies...
    private static final String SETTINGS_FOLDER_NAME = ".settings";

    private IJavaProject javaProject;
    private Set<String> ignoredFileSuffixes;
    private Set<String> ignoredProjectFolders;
    private Set<String> ignoredProjectFiles;
    private Set<IPath> classpathLibs;

    /**
     * Construct a new filter instance.
     */
    public SourceResourcesFilter(IJavaProject javaProject)
            throws JavaModelException {
        this.javaProject = javaProject;

        initIgnoredProjectFiles();
        initIgnoredFileSuffixes();
        initIgnoredProjectFolders();
        initIgnoredClasspathLibs(javaProject);
    }

    /**
     * @see org.eclipse.mtj.internal.core.text.IResourceFilter#shouldTraverseContainer(org.eclipse.core.resources.IContainer)
     */
    public boolean shouldTraverseContainer(IContainer container) {
        return !isIgnoredProjectFolder(container);
    }

    /**
     * @see org.eclipse.mtj.internal.core.text.IResourceFilter#shouldBeIncluded(org.eclipse.core.resources.IFile)
     */
    public boolean shouldBeIncluded(IFile file) {
        boolean isIgnoredProjectFile = parentIsProject(file)
                && ignoredProjectFiles.contains(file.getName());

        return !isIgnoredProjectFile
                && !isIgnoredProjectFolder(file.getParent())
                && !classpathLibs.contains(file.getFullPath())
                && !ignoredFileSuffixes.contains(file.getFileExtension());
    }

    /**
     * Initialize the ignored classpath libraries.
     * 
     * @param javaProject
     * @throws JavaModelException
     */
    private void initIgnoredClasspathLibs(IJavaProject javaProject)
            throws JavaModelException {
        classpathLibs = new HashSet<IPath>();
        IClasspathEntry[] rawEntries = javaProject.getRawClasspath();
        for (int i = 0; i < rawEntries.length; i++) {
            IClasspathEntry entry = rawEntries[i];
            addClasspathEntryIfLib(entry);
        }
    }

    /**
     * Initialize the ignored project folders.
     */
    private void initIgnoredProjectFolders() {
        ignoredProjectFolders = new HashSet<String>();
        ignoredProjectFolders.add(SETTINGS_FOLDER_NAME);
        ignoredProjectFolders.add(MTJCore.getDeploymentDirectoryName());
        ignoredProjectFolders.add(MTJCore.getVerifiedOutputDirectoryName());
    }

    /**
     * Initialize the ignored file suffixes.
     */
    private void initIgnoredFileSuffixes() {
        ignoredFileSuffixes = new HashSet<String>();
        for (int i = 0; i < IGNORED_FILE_SUFFIXES.length; i++) {
            ignoredFileSuffixes.add(IGNORED_FILE_SUFFIXES[i]);
        }
    }

    /**
     * Initialize the ignored project files.
     */
    private void initIgnoredProjectFiles() {
        ignoredProjectFiles = new HashSet<String>();
        for (int i = 0; i < IGNORED_PROJECT_FILES.length; i++) {
            ignoredProjectFiles.add(IGNORED_PROJECT_FILES[i]);
        }
    }

    /**
     * Return a boolean indicating whether the specified container is an ignored
     * project folder.
     * 
     * @param container
     * @return
     */
    private boolean isIgnoredProjectFolder(IContainer container) {
        return parentIsProject(container)
                && ignoredProjectFolders.contains(container.getName());
    }

    /**
     * Return a boolean indicating whether this resource is located directly
     * within the project.
     * 
     * @param resource
     * @return
     */
    private boolean parentIsProject(IResource resource) {
        return (resource.getParent().getType() == IResource.PROJECT);
    }

    /**
     * Add the classpath entry IPath if it is a library or a container
     * containing a library.
     * 
     * @param entry
     */
    private void addClasspathEntryIfLib(IClasspathEntry entry)
            throws JavaModelException {
        switch (entry.getEntryKind()) {
            case IClasspathEntry.CPE_LIBRARY:
                classpathLibs.add(entry.getPath());
                break;

            case IClasspathEntry.CPE_CONTAINER: {
                IPath path = entry.getPath();
                if (!path.segment(0).equals(
                        JavaMEClasspathContainer.JAVAME_CONTAINER)) {
                    IClasspathContainer cpContainer = JavaCore
                            .getClasspathContainer(path, javaProject);

                    if (cpContainer != null) {
                        IClasspathEntry[] entries = cpContainer
                                .getClasspathEntries();

                        for (int i = 0; i < entries.length; i++) {
                            addClasspathEntryIfLib(entries[i]);
                        }
                    } else {
                        MTJLogger.log(IStatus.ERROR,
                                "Unable to resolve classpath container entry "
                                        + path);
                    }
                }
            }
                break;

            case IClasspathEntry.CPE_VARIABLE:
                IClasspathEntry entry2 = JavaCore
                        .getResolvedClasspathEntry(entry);
                addClasspathEntryIfLib(entry2);
                break;

            case IClasspathEntry.CPE_PROJECT:
            case IClasspathEntry.CPE_SOURCE:
                break;

        }
    }
}