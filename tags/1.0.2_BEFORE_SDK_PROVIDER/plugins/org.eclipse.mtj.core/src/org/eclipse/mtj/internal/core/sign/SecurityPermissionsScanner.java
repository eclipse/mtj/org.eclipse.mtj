/**
 * Copyright (c) 2009 Motorola.
 * Portion Copyright (c) 2008 Nokia Corporation and/or its subsidiary(-ies).
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     Gorkem Ercan (Nokia) - 276056 Scanning for the permissions do not work
 */
package org.eclipse.mtj.internal.core.sign;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.core.search.SearchMatch;
import org.eclipse.jdt.core.search.SearchParticipant;
import org.eclipse.jdt.core.search.SearchPattern;
import org.eclipse.jdt.core.search.SearchRequestor;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * SecurityPermissionsScanner class scans class files for usage of
 * classes that require security permissions.
 * <br>
 * The scanner calculates all required permissions during the scan 
 * process.
 * 
 * @author David Marques
 * @since 1.0
 *
 */
public class SecurityPermissionsScanner extends SearchRequestor {

    private List<PermissionsGroup> permissions;
    private IMidletSuiteProject midletSuiteProject;
    private PermissionsGroup currentPermission;
    
 
    /**
     * Creates a SecurityScanner to scan the specified
     * class folder.
     * 
     * @param _classFolder target folder.
     */
    public SecurityPermissionsScanner(IMidletSuiteProject midletSuiteProject) {
        this.permissions = new ArrayList<PermissionsGroup>();
        this.midletSuiteProject = midletSuiteProject;
    }
    
    // Public --------------------------------------------------------

    /**
     * Gets the required permissions found for the 
     * midlet suite project
     * 
     * @return permissions list.
     */
	public List<PermissionsGroup> getRequiredPermissions() {
		IJavaSearchScope scope = SearchEngine.createJavaSearchScope(new IJavaElement[] { midletSuiteProject.getJavaProject() },
				IJavaSearchScope.SOURCES | IJavaSearchScope.REFERENCED_PROJECTS);
		int matchRule = SearchPattern.R_EXACT_MATCH	| SearchPattern.R_CASE_SENSITIVE;
		SearchParticipant[] participants = new SearchParticipant[] { SearchEngine.getDefaultSearchParticipant() };
		List<PermissionsGroup> allPermissions = PermissionsGroupsRegistry.getInstance().getPermissions();
		SearchEngine searchEngine = new SearchEngine();
		for (PermissionsGroup permission : allPermissions) {
			currentPermission = permission;
			SearchPattern suitePattern = SearchPattern.createPattern(permission.getClassName(), IJavaSearchConstants.TYPE,
					IJavaSearchConstants.REFERENCES, matchRule);
			try {
				searchEngine.search(suitePattern, participants, scope, this, new NullProgressMonitor());
			} catch (CoreException e) {
				MTJLogger.log(IStatus.WARNING,
						"Error while scanning for permissions", e); //$NON-NLS-1$
			}
		}

		return permissions;
	}

	@Override
	public void acceptSearchMatch(SearchMatch match) throws CoreException {
		if( !permissions.contains(currentPermission))
			permissions.add( currentPermission);
	}

    
}
