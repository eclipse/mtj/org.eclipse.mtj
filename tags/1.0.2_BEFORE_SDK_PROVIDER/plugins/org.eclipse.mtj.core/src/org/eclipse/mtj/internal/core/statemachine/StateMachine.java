/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.statemachine;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * StateMachine class implements a state machine. State transitions 
 * are triggered by {@link AbstractStateMachineEvent} instances sent 
 * using the {@link #postEvent(AbstractStateMachineEvent)} method. 
 * 
 * @see AbstractStateMachineEvent
 * @see AbstractStateTransition
 * 
 * @author David Marques
 */
public class StateMachine {

	private List<StateMachineListener> listeners;
	private List<AbstractState>        states;
	private AbstractState              current;
	private boolean                    started;
	
	/**
	 * Creates a {@link StateMachine} instance.
	 */
	public StateMachine() {
		this.listeners = new ArrayList<StateMachineListener>();
		this.states    = new ArrayList<AbstractState>();
	}

	/**
	 * Sets the state machine initial state. The initial state
	 * is entered right after the machine is stated.
	 * 
	 * @param _state initial state.
	 */
	public synchronized void setInitialState(AbstractState _state) {
		if (_state == null) {
			throw new IllegalArgumentException(Messages.StateMachine_nullInitialState);
		}
		
		if (started) {
			throw new IllegalStateException(Messages.StateMachine_machineAlreadyStarted);
		}
		
		if (!this.states.contains(_state)) {
			throw new IllegalStateException(Messages.StateMachine_invalidInitialState);
		}
		
		this.current = _state;
	}
	
	/**
	 * Starts the state machine.
	 */
	public synchronized void start() {
		if (started) {
			throw new IllegalStateException(Messages.StateMachine_machineAlreadyStarted);
		}
		
		if (current == null) {
			throw new IllegalStateException(Messages.StateMachine_noInitialState);
		}
		this.started = true;
		for (StateMachineListener listener : this.listeners) {
			try {				
				listener.started();
			} catch (Throwable t) {
				MTJLogger.log(IStatus.WARNING, t);
			}
		}
		this.current.onEnterState();
	}
	
	/**
	 * Stops the state machine.
	 */
	public synchronized void stop() {
		if (!started) {
			throw new IllegalStateException(Messages.StateMachine_machineNotYetStarted);
		}
		this.started = false;
		
		this.current.onExitState();
		for (StateMachineListener listener : this.listeners) {
			try {				
				listener.stopped();
			} catch (Throwable t) {
				MTJLogger.log(IStatus.WARNING, t);
			}
		}
	}
	
	/**
	 * Posts the event to the state machine. The current state will receive
	 * this event and call {@link AbstractStateTransition#isTransitionReady(AbstractStateMachineEvent)}
	 * method in order to test if the event triggers any state transition.
	 * 
	 * @param _event new event instance.
	 */
	public synchronized void postEvent(AbstractStateMachineEvent _event) {
		if (!started) {
			throw new IllegalStateException(Messages.StateMachine_machineNotYetStarted);
		}
		
		AbstractState nextState = this.current.postEvent(_event);
		if (nextState != null) {
			this.current.onExitState();
			this.current = nextState;
			this.current.onEnterState();
			
			if (this.current instanceof AbstractFinalState) {
				for (StateMachineListener listener : this.listeners) {
					try {				
						listener.finished();
					} catch (Throwable t) {
						MTJLogger.log(IStatus.WARNING, t);
					}
				}
				this.stop();
			}
		}
	}
	
	/**
	 * Adds the state to the machine.
	 * 
	 * @param _state new state.
	 */
	public void addState(AbstractState _state) {
		if (started) {
			throw new IllegalStateException(Messages.StateMachine_machineAlreadyStarted);
		}
		
		synchronized (this.states) {			
			if (!this.states.contains(_state)) {
				this.states.add(_state);
			}
		}
	}
	
	/**
	 * Removes the state from the machine.
	 * 
	 * @param _state old state.
	 */
	public void removeState(AbstractState _state) {
		if (started) {
			throw new IllegalStateException(Messages.StateMachine_machineAlreadyStarted);
		}
		
		synchronized (this.states) {			
			this.states.remove(_state);
		}
	}
	
	/**
	 * Gets all states.
	 * 
	 * @return states array.
	 */
	public AbstractState[] getStates() {
		return this.states.toArray(new AbstractState[this.states.size()]);
	}
	
	/**
	 * Adds a {@link StateMachineListener} instance to be notified
	 * upon state machine events.
	 * 
	 * @param _listener listener to add.
	 */
	public void addStateMachineListener(StateMachineListener _listener) {
		synchronized (this.listeners) {			
			if (!this.listeners.contains(_listener)) {
				this.listeners.add(_listener);
			}
		}
	}
	
	/**
	 * Removes the {@link StateMachineListener} instance to be notified
	 * upon state machine events.
	 * 
	 * @param _listener listener to remove.
	 */
	public void removeStateMachineListener(StateMachineListener _listener) {
		synchronized (this.listeners) {			
			this.listeners.remove(_listener);
		}
	}
}
