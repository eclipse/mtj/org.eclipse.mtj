/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.util.migration;

import java.io.File;

import org.w3c.dom.Document;

/**
 * @author Diego Madruga Sandin
 *
 */
public interface IMigration {

    /**
     * @param document
     * @return
     */
    public abstract Document migrate(Document document);

    /**
     * @return the migrated
     */
    public abstract boolean isMigrated();

    /**
     * @param document
     * @return
     */
    public abstract Document migrate(File storeFile);

}