/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Diego Sandin (Motorola) - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.sdk.device;

import java.io.File;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.MTJCore;

/**
 * The device finder allows access to device instances available in a specified
 * folder. These devices will be imported using the registered device importers.
 * <p>
 * Clients may access the IDeviceFinder implementation through the
 * {@link MTJCore#getDeviceFinder()} method.
 * </p>
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * 
 * @since 1.0
 */
public interface IDeviceFinder {

    /**
     * Return all devices that can be found in the specified directory or any
     * sub-directories. This method will consult all registered IDeviceImporter
     * implementations that have been registered with the system.
     * 
     * @param directory the directory in which the devices must be searched.
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @return a list of devices instances found in the specified directory or
     *         an empty list if no device was found.
     * @throws CoreException if a device importer found an error occurred while
     *             searching for the devices.
     * @throws InterruptedException if the operation detects a request to
     *             cancel, using IProgressMonitor.isCanceled(), it should exit
     *             by throwing InterruptedException.
     */
    public List<IDevice> findDevices(File directory, IProgressMonitor monitor)
            throws CoreException, InterruptedException;

    /**
     * Return all devices that can be found in the specified directory or any
     * sub-directories. This method will consult the IDeviceImporter
     * implementation that have been registered with the informed
     * deviceImporterID.
     * 
     * @param deviceImporterID a fully-qualified identifier for a particular
     *            device importer. The list of importer IDs provided by MTJ can
     *            be found at {@link IDeviceImporter}.
     * @param directory the directory in which the devices must be searched.
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @return a list of devices instances found in the specified directory or
     *         an empty list if no device was found.
     * @throws CoreException if the specified device importer found an error
     *             occurred while searching for the devices.
     * @throws InterruptedException if the operation detects a request to
     *             cancel, using IProgressMonitor.isCanceled(), it should exit
     *             by throwing InterruptedException.
     */
    public List<IDevice> findDevices(final String deviceImporterID,
            File directory, IProgressMonitor monitor) throws CoreException,
            InterruptedException;
}