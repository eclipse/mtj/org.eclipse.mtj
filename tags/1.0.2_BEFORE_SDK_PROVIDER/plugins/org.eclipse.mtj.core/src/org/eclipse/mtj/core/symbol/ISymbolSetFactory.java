/**
 * Copyright (c) 2008, 2009 Motorola Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gustavo de Paula (Motorola) - Initial implementation
 *     David Marques (Motorola)    - Using IPath instead of URI.
 */
package org.eclipse.mtj.core.symbol;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.device.IDevice;

/**
 * This API provides a mechanism to create symbols set. Two different ways are
 * available: create from a device and create from a j2mepolish device file. The
 * first one will be based on the device properties and the device libraries.
 * The later is based on j2mepolish device format and will create an array of
 * symbol sets for each device that is available on the database.
 * 
 * <p>
 * Clients must use {@link MTJCore#getSymbolSetFactory()} to retrieve an
 * {@link ISymbolSetFactory} instance.
 * </p>
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @since 1.0
 */
public interface ISymbolSetFactory {

    /**
     * Constant that represents the type j2mepolish from file
     */
    public static final String DEVICE_DB_J2MEPOLISH_FILE = "org.eclipse.mtj.davicedb.j2mepolish.file"; //$NON-NLS-1$

    /**
     * Constant that represents the type j2mepolish from antenna jar
     */
    public static final String DEVICE_DB_J2MEPOLISH_JAR = "org.eclipse.mtj.davicedb.j2mepolish.antennajar"; //$NON-NLS-1$

    /**
     * Creates a new symbol based on the given name and value.
     * 
     * @param name the symbol name. This is case-sensitive and must not be
     *            <code>null</code> or an empty String <code>""</code>.
     * @param value the symbol value.
     * @return the newly created symbol.
     */
    public ISymbol createSymbol(String name, String value);

    /**
     * Create a symbol set with the given name.
     * 
     * @param name the SymbolSet name. This is case-sensitive and must not be
     *            <code>null</code> or an empty String <code>""</code>.
     * @return the newly created SymbolSet.
     */
    public ISymbolSet createSymbolSet(String name);

    /**
     * Creates a list of symbol sets based on a device data base. The device
     * database is specified via an input URI and a type. Currently MTJ supports
     * only J2MEPolish database.
     * 
     * @param type a string that represents the type of the database to be
     *            imported. Currently MTJ supports only J2MEPolish database (
     *            {@link #DEVICE_DB_J2MEPOLISH_JAR} or
     *            {@link #DEVICE_DB_J2MEPOLISH_FILE}).
     * @param databasePath path of the database.
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * 
     * @return a List of SymbolSets.
     * @throws IOException if failed to read the database.
     */
    public List<ISymbolSet> createSymbolSetFromDataBase(String type,
            IPath databasePath, IProgressMonitor monitor) throws IOException;

    /**
     * Create SymbolSet from device. The content of the symbolset will be based
     * on the libraries that the device has. The SymbolSet name will be the same
     * as {@link IDevice#getName()}.
     * 
     * @param device the device instance to be used as base to the new
     *            SymbolSet.
     * @return the newly created SymbolSet.
     */
    public ISymbolSet createSymbolSetFromDevice(IDevice device);

    /**
     * Create SymbolSet from properties. The content of the symbolset will be
     * based on the properties instance. The SymbolSet name will be
     * "Properties".
     * 
     * @param properties the properties to be used as base to the new SymbolSet.
     * @return the newly created SymbolSet.
     */
    public ISymbolSet createSymbolSetFromProperties(Properties properties);

}