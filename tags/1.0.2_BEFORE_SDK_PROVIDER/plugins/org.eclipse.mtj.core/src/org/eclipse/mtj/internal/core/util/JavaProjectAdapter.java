/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Eric S. Dias  (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.util;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;

/**
 * JavaProjectAdapter class adapts an {@link IJavaProject} instance since it can
 * not be implemented, adding new methods acting upon it.
 * 
 * @see Adapter Pattern.
 * @author Eric Dias
 * @since 1.0
 */

public class JavaProjectAdapter {

    private IJavaProject iJavaProject;

    /**
     * Creates an adapter for the specified {@link IJavaProject} instance.
     * 
     * @param iJavaProject target java project.
     */
    public JavaProjectAdapter(IJavaProject iJavaProject) {
        this.iJavaProject = iJavaProject;
    }

    /**
     * Notifies all PackageFragmentRoots in a JavaProject.
     * 
     * @param visitor target IJavaProjectVisitor.
     */
    public void accept(IJavaProjectVisitor visitor) throws CoreException {

        IPackageFragmentRoot[] roots = iJavaProject.getPackageFragmentRoots();
        for (IPackageFragmentRoot root : roots) {
            visitor.visitPackageFragmentRoot(root);
            IJavaElement[] children = root.getChildren();
            for (IJavaElement child : children) {
                if (!(child instanceof IPackageFragment)) {
                    continue;
                }

                IPackageFragment packageFragment = (IPackageFragment) child;
                visitor.visitPackageFragment(packageFragment);
                ICompilationUnit[] units = packageFragment
                        .getCompilationUnits();
                for (ICompilationUnit unit : units) {
                    visitor.visitCompilatioUnit(unit);
                }

            }
        }
    }
}
