/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Handling the case that there is no valid preverifier
 *     Gustavo de Paula (Motorola)  - Preverifier api refactoring
 *     David Marques (Motorola)     - Implementing build properties support.
 *     David Marques (Motorola)     - Refactoring MTJBuildProperties class.
 *     David Marques(Motorola)  - Removing packaging from this builder.         
 *     David Marques(Motorola)  - Fixing project dependencies support.                       
 */
package org.eclipse.mtj.internal.core.build.preverifier.builder;

import java.io.FileFilter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaModelMarker;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverificationError;
import org.eclipse.mtj.core.build.preverifier.PreverifierNotFoundException;
import org.eclipse.mtj.internal.core.build.BuildConsoleProxy;
import org.eclipse.mtj.internal.core.build.BuildLoggingConfiguration;
import org.eclipse.mtj.internal.core.build.preverifier.IClassErrorInformation;
import org.eclipse.mtj.internal.core.build.preverifier.IPreverificationErrorLocation;
import org.eclipse.mtj.internal.core.build.preverifier.PreverificationErrorLocationType;
import org.eclipse.mtj.internal.core.build.preverifier.PreverificationUtils;
import org.eclipse.mtj.internal.core.util.AbstractClasspathEntryVisitor;
import org.eclipse.mtj.internal.core.util.FilteringClasspathEntryVisitor;
import org.eclipse.mtj.internal.core.util.Utils;

import de.schlichtherle.io.File;

/**
 * A build helper that builds an individual project's resource delta.
 * 
 * @author Craig Setera
 */
class ResourceDeltaBuilder {

    /**
     * Classpath entry visitor to collect up the libraries.
     */
    private static class LibraryCollectionVisitor extends
            FilteringClasspathEntryVisitor {
        private ArrayList<IClasspathEntry> libraryEntries;

        /** Construct a new instance */
        private LibraryCollectionVisitor() {
            libraryEntries = new ArrayList<IClasspathEntry>();
        }

        /**
         * @return Returns the libraryEntries.
         */
        public ArrayList<IClasspathEntry> getLibraryEntries() {
            return libraryEntries;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.core.util.AbstractClasspathEntryVisitor#visitLibraryEntry(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IProgressMonitor)
         */
        @Override
        public void visitLibraryEntry(IClasspathEntry entry,
                IJavaProject javaProject, IProgressMonitor monitor)
                throws CoreException {
            if (isLibraryExported(entry)) {
                libraryEntries.add(entry);
            }
        }
    }

    /**
     * Classpath entry visitor to collect up the output locations in a java
     * project.
     */
    private static class OutputLocationsCollectionVisitor extends
            AbstractClasspathEntryVisitor {
        private Set<IPath> outputLocations;

        private OutputLocationsCollectionVisitor() {
            outputLocations = new HashSet<IPath>();
        }

        /**
         * @return Returns the outputLocations.
         */
        public Set<IPath> getOutputLocations() {
            return outputLocations;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.core.util.AbstractClasspathEntryVisitor#visitSourceEntry(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IProgressMonitor)
         */
        @Override
        public void visitSourceEntry(IClasspathEntry entry,
                IJavaProject javaProject, IProgressMonitor monitor)
                throws CoreException {
            IPath outputLocation = entry.getOutputLocation();
            if (outputLocation == null) {
                outputLocation = javaProject.getOutputLocation();
            }

            outputLocations.add(outputLocation);
        }
    }

    /**
     * Implementation of a resource delta visitor for the preverification
     * builder.
     */
    private class ResourceDeltaVisitor implements IResourceVisitor, IResourceDeltaVisitor {
        private List<IFile> classes;
        private IProgressMonitor monitor;

        /**
         * Constructor
         * 
         * @param monitor
         * @throws CoreException
         */
        public ResourceDeltaVisitor(IProgressMonitor monitor) throws CoreException {
            this.monitor   = monitor;
            this.classes   = new ArrayList<IFile>();
        }

		/**
		 * @return the classes
		 */
		public List<IFile> getClasses() {
			List<IFile> copy = new ArrayList<IFile>();
        	copy.addAll(this.classes);
        	return copy;
		}

		/* (non-Javadoc)
         * @see org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.core.resources.IResourceDelta)
         */
        public boolean visit(IResourceDelta delta) throws CoreException {
            IResource resource = delta.getResource();
			if (isClassFile(resource)) {
                 switch (delta.getKind()) {
                 	 case IResourceDelta.CHANGED:
                     case IResourceDelta.ADDED:
                         visit(delta.getResource());
                     break;

                     case IResourceDelta.REMOVED:
                    	 removeVerifiedResource(buildInfo, resource, monitor);
                     break;
                 }
            }
            return true;
        }

		public boolean visit(IResource resource) throws CoreException {
			if (isClassFile(resource)) {
				IFile file = (IFile) resource;
				this.classes.add(file);
			}
			return true;
		}

	    private void removeVerifiedResource(BuildInfo buildInfo, IResource resource
	    		, IProgressMonitor monitor) throws CoreException {
	    	IJavaProject javaProject = buildInfo.getCurrentJavaProject();
	    	IPath path = Utils.extractsSourceFolderRelativePath(javaProject, resource);
	    	if (path != null) {
	    		IFolder   verifiedFolder = buildInfo.getVerifiedClassesFolder(monitor);
	    		IResource verifiedClass  = verifiedFolder.findMember(path);
	    		if (verifiedClass != null) {
					verifiedClass.delete(true, monitor);
				}
			}
		}
		
		private boolean isClassFile(IResource resource) {
			return resource.getType() == IResource.FILE  &&
			resource.getName().endsWith(".class") &&
			buildInfo.isOutputResource(resource);
		}
    }

    // Build information
    private BuildInfo buildInfo;

    private BuildLoggingConfiguration buildLoggingConfig;

    private BuildConsoleProxy consoleProxy = BuildConsoleProxy.getInstance();

    // Shortcuts to resources
    private IWorkspaceRoot workspaceRoot;

    /**
     * Construct a new Resource Delta Builder instance.
     * 
     * @param buildInfo
     */
    public ResourceDeltaBuilder(BuildInfo buildInfo) {
        this.buildInfo = buildInfo;
        buildLoggingConfig = BuildLoggingConfiguration.getInstance();
        workspaceRoot = MTJCore.getWorkspace().getRoot();
    }

    /**
     * Attempt to preverify the specified library.
     * 
     * @param srcResource
     * @param srcFile
     * @param verifiedLibsFolder
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    private void attemptLibraryPreverification(IResource srcResource,
            File srcFile, IFolder verifiedLibsFolder, IProgressMonitor monitor)
            throws CoreException, PreverifierNotFoundException {
        buildInfo.setPackageDirty(true);
        
        try {
            // Run the preverifier
            IPreverificationError[] errors = buildInfo.getMidletSuite()
                    .preverifyJarFile(srcFile, verifiedLibsFolder, monitor);
            // Handle errors that may have occurred.
            // Should this actually be bubbled up as an exception?
            if (errors.length > 0) {
                createJarErrorMarker(srcResource, srcFile, errors);
            }
        } finally {
            verifiedLibsFolder.refreshLocal(IResource.DEPTH_INFINITE, monitor);
        }
    }

    /**
     * Create an error marker for the specific type with the specified error
     * message.
     * 
     * @param error
     */
    private void createErrorMarkerFor(IPreverificationError error)
            throws JavaModelException, CoreException {
        IMarker marker = null;

        // Calculate the resource
        IClassErrorInformation classInfo = ((IPreverificationErrorLocation) error
                .getLocation()).getClassInformation();
        String typeName = (classInfo == null) ? "" : classInfo.getName()
                .replace('/', '.');
        String message = PreverificationUtils.getErrorText(error);

        IType type = buildInfo.getCurrentJavaProject().findType(typeName);
        if (type != null) {
            IResource resource = type.getResource();

            // Sometimes the resource doesn't come back... This is supposed
            // to be only when the resource is in an external archive.
            if (resource != null) {
                // Create the marker and set the attributes
                marker = resource
                        .createMarker(IJavaModelMarker.JAVA_MODEL_PROBLEM_MARKER);
                marker.setAttribute(IMarker.MESSAGE, message);
                marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);

                int lineNumber = ((IPreverificationErrorLocation) error
                        .getLocation()).getLineNumber();
                if (lineNumber != -1) {
                    marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
                }

                setMarkerRangeAttributes(marker, error, type);
            }
        }

        // Fallback position if nothing specific is possible
        if (marker == null) {
            createProjectLevelPreverifyMarker(typeName, message);
        }
    }

    /**
     * Get the output locations (IPath instances) for the specified java
     * project.
     * 
     * @param javaProject
     * @param monitor
     * @return
     * @throws CoreException
     */
    private IPath[] getOutputLocations(IJavaProject javaProject,
            IProgressMonitor monitor) throws CoreException {
        OutputLocationsCollectionVisitor visitor = new OutputLocationsCollectionVisitor();
        visitor.getRunner().run(javaProject, visitor, monitor);

        // Collect the unique output locations
        Set<IPath> outputLocations = visitor.getOutputLocations();
        return outputLocations.toArray(new IPath[outputLocations.size()]);
    }
    
    /**
     * Log any errors that occurred during preverification.
     * 
     * @param srcResource
     * @param srcFile
     * @param errors
     * @throws CoreException
     */
    private void createJarErrorMarker(IResource srcResource, File srcFile,
            IPreverificationError[] errors) throws CoreException {
        StringBuffer sb = new StringBuffer("Preverification errors:\n");

        for (IPreverificationError error : errors) {
            sb.append(PreverificationUtils.getErrorText(error)).append("\n");
        }

        IMarker marker = srcResource
                .createMarker(IJavaModelMarker.JAVA_MODEL_PROBLEM_MARKER);
        marker.setAttribute(IMarker.MESSAGE, sb.toString());
        marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
    }

    /**
     * Create an error marker on the project about a type verification. This
     * will happen when an error occurs trying to preverify and yet the type
     * resource cannot be located for some reason.
     * 
     * @param typeName
     * @param message
     * @throws CoreException
     */
    private void createProjectLevelPreverifyMarker(String typeName,
            String message) throws CoreException {
        StringBuffer sb = new StringBuffer("Type ");
        sb.append(typeName).append(" ").append(message);

        IProject project = buildInfo.getCurrentJavaProject().getProject();
        IMarker marker = project
                .createMarker(IJavaModelMarker.JAVA_MODEL_PROBLEM_MARKER);
        marker.setAttribute(IMarker.MESSAGE, sb.toString());
        marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
    }

    /**
     * Return a boolean indicating whether or not the classes in the specified
     * directory and subdirectories need preverification.
     * 
     * @param srcFile
     * @param tgtFile
     * @return
     */
    private boolean doClassesRequirePreverification(File srcFile, File tgtFile) {
        boolean requirePreverify = false;

        if (srcFile.isDirectory()) {
            if (!tgtFile.exists()) {
                requirePreverify = true;
            } else {
                java.io.File[] classesAndDirectories = srcFile
                        .listFiles(new FileFilter() {
                            public boolean accept(java.io.File pathname) {
                                return pathname.isDirectory()
                                        || pathname.getName()
                                                .endsWith(".class");
                            }
                        });

                for (java.io.File classesAndDirectorie : classesAndDirectories) {
                    File srcFile2 = new File(classesAndDirectorie);
                    File tgtFile2 = new File(tgtFile, classesAndDirectorie
                            .getName());
                    requirePreverify = doClassesRequirePreverification(
                            srcFile2, tgtFile2);
                    if (requirePreverify) {
                        break;
                    }
                }
            }
        } else {
            requirePreverify = isSourceNewerThanTarget(srcFile, tgtFile);
        }

        return requirePreverify;
    }

    private void preverifyClasses(List<IFile> classes, IProgressMonitor monitor) throws CoreException,
            PreverifierNotFoundException {
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy
                    .traceln("> ResourceDeltaBuilder.handleClassAddsAndChanges; classFiles count = "
                            + classes.size());
        }
        
        this.buildInfo.setPackageDirty(true);
        IResource[] resources = classes.toArray(new IResource[classes.size()]);
        IFolder outputFolder = buildInfo.getVerifiedClassesFolder(monitor);
	   	 try {
	         // Run the preverification
	         IPreverificationError[] errors = buildInfo.getMidletSuite()
	                 .preverify(resources, outputFolder, monitor);
	         outputFolder.refreshLocal(IResource.DEPTH_INFINITE, monitor);
	         for (IPreverificationError error : errors) {
	             createErrorMarkerFor(error);
	         }
	     } finally {
	    	 outputFolder.refreshLocal(IResource.DEPTH_INFINITE, monitor);
	     }
        
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy
                    .traceln("< ResourceDeltaBuilder.handleClassAddsAndChanges");
        }
    }

    /**
     * Return a boolean indicating whether the source file is newer than the
     * target file.
     * 
     * @param srcFile
     * @param tgtFile
     * @return
     */
    private boolean isSourceNewerThanTarget(File srcFile, File tgtFile) {
        return !tgtFile.exists()
                || (tgtFile.lastModified() < srcFile.lastModified());
    }

    /**
     * Set the attributes for character range on the marker.
     * 
     * @param marker
     * @param error
     * @param type
     * @throws CoreException
     */
    private void setMarkerRangeAttributes(IMarker marker,
            IPreverificationError error, IType type) throws CoreException {
        int start = 1;
        int end = 1;

        switch (((IPreverificationErrorLocation) error.getLocation())
                .getLocationType().getTypeCode()) {
            case PreverificationErrorLocationType.CLASS_DEFINITION_CODE: {
                ISourceRange sourceRange = type.getNameRange();
                start = sourceRange.getOffset();
                end = start + sourceRange.getLength();
            }
                break;

        }

        // Final fallback...
        marker.setAttribute(IMarker.CHAR_START, start);
        marker.setAttribute(IMarker.CHAR_END, end);
    }

    /**
     * Do the build for the project and resource delta specified in the build
     * info.
     * 
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    void build(IProgressMonitor monitor) throws CoreException,
            PreverifierNotFoundException {
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln("> ResourceDeltaBuilder.build");
        }

        ResourceDeltaVisitor visitor = new ResourceDeltaVisitor(monitor);
        // Handle the actual resource changes
        IResourceDelta resourceDelta = buildInfo.getCurrentResourceDelta();
        if (resourceDelta == null) {        	
        	IPath[] outputs = getOutputLocations(buildInfo.getCurrentJavaProject(), monitor);
        	for (IPath output : outputs) {
        		IResource resource = workspaceRoot.findMember(output.makeAbsolute());
        		if (resource != null) {				
        			resource.accept(visitor);
        		}
        	}
        } else {
        	resourceDelta.accept((IResourceDeltaVisitor)visitor);
        }
        preverifyClasses(visitor.getClasses(), monitor);
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln("< ResourceDeltaBuilder.build");
        }
    }

    /**
     * Preverify any libraries that exist in the build path that may be out of
     * date.
     * 
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    void preverifyLibraries(IProgressMonitor monitor) throws CoreException,
            PreverifierNotFoundException {
        IFolder verifiedClassesFolder = buildInfo.getVerifiedClassesFolder(monitor);
        IFolder verifiedLibsFolder    = buildInfo.getVerifiedLibsFolder(monitor);

        // Walk through the classpath, looking for jars that need to
        // be preverified
        LibraryCollectionVisitor visitor = new LibraryCollectionVisitor();
        visitor.getRunner().run(buildInfo.getCurrentJavaProject(), visitor,
                monitor);
        List<IClasspathEntry> resolvedEntries = visitor.getLibraryEntries();

        Iterator<IClasspathEntry> iter = resolvedEntries.iterator();
        while (iter.hasNext()) {
            IClasspathEntry entry = iter.next();

            // Figure out the source resource. If this is outside
            // the workspace, set it to the project so we have
            // something to attach an error marker to.
            Object resolvedEntry = Utils.getResolvedClasspathEntry(entry);
            IResource srcResource = (resolvedEntry instanceof IResource) ? (IResource) resolvedEntry
                    : buildInfo.getCurrentJavaProject().getProject();

            // Convert to a java.io.File
            IPath srcPath = entry.getPath().makeAbsolute();
            File srcFile = new File(Utils.getResolvedClasspathEntryFile(entry));

            // Check to see if there has been an attempt to preverify the file
            if ((srcFile != null)
                    && !buildInfo.hasLibraryBeenPreverified(srcFile)) {
                // Log that we tried to preverify this file
                buildInfo.addPreverifiedLibrary(srcFile);

                // Look up the appropriate target folder and resource
                IResource target = null;
                IFolder targetFolder = null;
                if (srcFile.isDirectory() && !srcFile.isArchive()) {
                    target = verifiedClassesFolder;
                    targetFolder = verifiedClassesFolder;
                } else {
                    target = verifiedLibsFolder.getFile(srcPath.lastSegment());
                    targetFolder = verifiedLibsFolder;
                }

                File tgtFile = new File(target.getLocation().toFile());

                // Determine whether or not the library needs preverification
                boolean requiresPreverification = false;
                if (srcFile.isDirectory()) {
                    requiresPreverification = doClassesRequirePreverification(
                            srcFile, tgtFile);
                } else {
                    requiresPreverification = isSourceNewerThanTarget(srcFile,
                            tgtFile);
                }

                if (requiresPreverification) {
                    attemptLibraryPreverification(srcResource, srcFile,
                            targetFolder, monitor);
                }
            }
        }
    }
}
