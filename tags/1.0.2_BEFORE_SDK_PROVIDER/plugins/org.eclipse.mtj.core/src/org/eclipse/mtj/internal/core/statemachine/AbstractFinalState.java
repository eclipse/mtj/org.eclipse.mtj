/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.statemachine;

/**
 * AbstractFinalState class defines an abstract final state
 * to be extended by user final states.
 * 
 * @see AbstractState
 * 
 * @author David Marques
 */
public abstract class AbstractFinalState extends AbstractState {

	/**
	 * Creates an AbstractFinalState instance with
	 * the specified parent state. 
	 *  
	 * @param _parent parent state.
	 */
	public AbstractFinalState(AbstractState _parent) {
		super(_parent);
	}
}
