/**
 * Copyright (c) 2007,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.IDocument;

/**
 * @since 0.9.1
 */
public abstract class NodeDocumentHandler extends DocumentHandler {

    private IDocumentNodeFactory fFactory;

    /**
     * @param reconciling
     * @param factory
     */
    public NodeDocumentHandler(boolean reconciling, IDocumentNodeFactory factory) {
        super(reconciling);
        fFactory = factory;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.DocumentHandler#getDocument()
     */
    @Override
    protected abstract IDocument getDocument();

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.DocumentHandler#getDocumentAttribute(java.lang.String, java.lang.String, org.eclipse.mtj.internal.core.text.IDocumentElementNode)
     */
    @Override
    protected IDocumentAttributeNode getDocumentAttribute(String name,
            String value, IDocumentElementNode parent) {
        IDocumentAttributeNode attr = parent.getDocumentAttribute(name);
        try {
            if (attr == null) {
                attr = fFactory.createAttribute(name, value, parent);
            } else {
                if (!name.equals(attr.getAttributeName())) {
                    attr.setAttributeName(name);
                }
                if (!value.equals(attr.getAttributeValue())) {
                    attr.setAttributeValue(value);
                }
            }
        } catch (CoreException e) {
        }
        return attr;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.DocumentHandler#getDocumentNode(java.lang.String, org.eclipse.mtj.internal.core.text.IDocumentElementNode)
     */
    @Override
    protected IDocumentElementNode getDocumentNode(String name,
            IDocumentElementNode parent) {
        IDocumentElementNode node = null;
        if (parent == null) {
            node = getRootNode();
            if (node != null) {
                node.setOffset(-1);
                node.setLength(-1);
            }
        } else {
            IDocumentElementNode[] children = parent.getChildNodes();
            for (IDocumentElementNode element : children) {
                if (element.getOffset() < 0) {
                    if (name.equals(element.getXMLTagName())) {
                        node = element;
                    }
                    break;
                }
            }
        }

        if (node == null) {
            return fFactory.createDocumentNode(name, parent);
        }

        IDocumentAttributeNode[] attrs = node.getNodeAttributes();
        for (IDocumentAttributeNode attr : attrs) {
            attr.setNameOffset(-1);
            attr.setNameLength(-1);
            attr.setValueOffset(-1);
            attr.setValueLength(-1);
        }

        for (int i = 0; i < node.getChildNodes().length; i++) {
            IDocumentElementNode child = node.getChildAt(i);
            child.setOffset(-1);
            child.setLength(-1);
        }

        // clear text nodes if the user is typing on the source page
        // they will be recreated in the characters() method
        if (isReconciling()) {
            node.removeTextNode();
            node.setIsErrorNode(false);
        }

        return node;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.DocumentHandler#getDocumentTextNode(java.lang.String, org.eclipse.mtj.internal.core.text.IDocumentElementNode)
     */
    @Override
    protected IDocumentTextNode getDocumentTextNode(String content,
            IDocumentElementNode parent) {

        IDocumentTextNode textNode = parent.getTextNode();
        if (textNode == null) {
            if (content.trim().length() > 0) {
                textNode = fFactory.createDocumentTextNode(content, parent);
            }
        } else {
            String newContent = textNode.getText() + content;
            textNode.setText(newContent);
        }
        return textNode;
    }

    /**
     * @return
     */
    protected IDocumentNodeFactory getFactory() {
        return fFactory;
    }

    /**
     * @return
     */
    protected abstract IDocumentElementNode getRootNode();

}
