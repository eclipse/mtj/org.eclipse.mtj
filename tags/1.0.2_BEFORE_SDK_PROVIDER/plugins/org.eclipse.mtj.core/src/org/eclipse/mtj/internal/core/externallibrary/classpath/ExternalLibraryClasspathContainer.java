/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.externallibrary.classpath;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.mtj.internal.core.externallibrary.model.IExternalLibrary;

/**
 * A classpath container that provides a way to indirectly reference a set of
 * classpath entries specified by 3rd party vendors.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class ExternalLibraryClasspathContainer implements IClasspathContainer {

    /**
     * Name of the MIDlet MIDPLibrary Container id.
     */
    public static final String EXTERNAL_LIBRARY_CONTAINER_ID = "org.eclipse.mtj.core.EXTERNAL_LIBRARY"; //$NON-NLS-1$

    /**
     * The MIDlet library associated to this container
     */
    private IExternalLibrary library;

    /**
     * The name of this container
     */
    private String description;

    /**
     * Creates a new MIDletExternalLibraryClasspathContainer
     * 
     * @param library the library to be associated to this container
     */
    public ExternalLibraryClasspathContainer(IExternalLibrary library) {
        this.description = library.getName() + " [ "
                + library.getVersion().toString() + " ]";
        this.library = library;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.IClasspathContainer#getClasspathEntries()
     */
    public IClasspathEntry[] getClasspathEntries() {
        return library.getClasspathEntryList().toArray(new IClasspathEntry[0]);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.IClasspathContainer#getDescription()
     */
    public String getDescription() {
        return description;
    }

    /**
     * Answers the kind of this container.
     * 
     * @return <code>K_APPLICATION</code> because this container always maps to
     *         an application library
     * @see org.eclipse.jdt.core.IClasspathContainer#getKind()
     */
    public int getKind() {
        return K_APPLICATION;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.IClasspathContainer#getPath()
     */
    public IPath getPath() {
        return new Path(EXTERNAL_LIBRARY_CONTAINER_ID).append(this.description);
    }

}
