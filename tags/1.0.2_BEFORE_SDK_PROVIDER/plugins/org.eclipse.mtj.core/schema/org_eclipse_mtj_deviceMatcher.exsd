<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.eclipse.mtj.core" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appInfo>
         <meta.schema plugin="org.eclipse.mtj.core" id="devicematcher" name="Device Matcher"/>
      </appInfo>
      <documentation>
         The device matcher extension point matches a given device group and device name against the locally installed ones.

When importing projects their runtime configuration may refer to devices that are not installed. Instead of just returning a null device, this creates an opportunity to match it against installed devices. Implementors of this extension point must provide instances of the &lt;code&gt;&lt;b&gt;org.eclipse.mtj.core.sdk.device.IDeviceMatcher&lt;/b&gt;&lt;/code&gt; interface.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appInfo>
            <meta.element />
         </appInfo>
      </annotation>
      <complexType>
         <sequence>
            <element ref="matcher" minOccurs="1" maxOccurs="unbounded"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  a fully qualified identifier of the target extension point
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  an optional identifier of the extension instance
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  an optional name of the extension instance
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="matcher">
      <complexType>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  a required fully-qualified identifier for this particular device matcher extension
               </documentation>
            </annotation>
         </attribute>
         <attribute name="priority" type="string" use="required">
            <annotation>
               <documentation>
                  The priority order for this matcher  The priority may range from 1 to 99.  The matchers will be consulted in priority order starting with the lower numbered priority and working toward the highest numbered priority.  The first device matcher that returns a non-null result will &quot;win&quot; and no further matcher instances will be consulted.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  the required implementation class for the &lt;code&gt;org.eclipse.mtj.core.sdk.device.IDeviceMatcher&lt;/code&gt; interface
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.mtj.core.sdk.device.IDeviceMatcher"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         1.0.1
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         Example of a declaration of a &lt;code&gt;devicematcher&lt;/code&gt;:
&lt;pre&gt;
&lt;extension
     point=&quot;org.eclipse.mtj.core.devicematcher&quot;&gt;
  &lt;importer
        class=&quot;org.eclipse.mtj.internal.ui.DeviceMatcher&quot;
        id=&quot;org.eclipse.mtj.devicematcher&quot;/&gt;
&lt;/extension&gt;
&lt;/pre&gt;
      </documentation>
   </annotation>


   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         MTJ provides an implementation for the &lt;code&gt;devicematcher&lt;/code&gt; in the &lt;code&gt; org.eclipse.mtj.ui&lt;/code&gt; plugin that presents the user with a dialog to choose from availiable devices.
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="copyright"/>
      </appInfo>
      <documentation>
         Copyright (c) 2009 Sony Ericsson.&lt;br&gt;
Parts copied from org_eclipse_mtj_deviceImporter.exsd with copyright (c) 2003,2009 Craig Setera and others.&lt;br&gt;
All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at &lt;a 
href=&quot;http://www.eclipse.org/legal/epl-v10.html&quot;&gt;http://www.eclipse.org/legal/epl-v10.html&lt;/a&gt;
      </documentation>
   </annotation>

</schema>
