/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n.pages;

import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nObject;
import org.eclipse.mtj.internal.ui.editor.elements.DefaultContentProvider;

/**
 * 
 */
public class L10nContentProvider extends DefaultContentProvider implements
        ITreeContentProvider {

    /**
     * 
     */
    public L10nContentProvider() {
        // NO-OP
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
     */
    public Object[] getChildren(Object parentElement) {
        if (parentElement instanceof L10nModel) {
            return new Object[] { ((L10nModel) parentElement).getLocales() };
        } else if (parentElement instanceof L10nObject) {
            List<IDocumentElementNode> list = ((L10nObject) parentElement)
                    .getChildren();
            // List is never null
            if (list.size() > 0) {
                return list.toArray();
            }
        }
        return new Object[0];
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
     */
    public Object[] getElements(Object inputElement) {
        return getChildren(inputElement);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
     */
    public Object getParent(Object element) {
        if (element instanceof L10nObject) {
            return ((L10nObject) element).getParent();
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
     */
    public boolean hasChildren(Object element) {
        return (getChildren(element).length > 0);
    }

}
