/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant.template;

import java.util.Map;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.ToolFactory;
import org.eclipse.jdt.core.formatter.CodeFormatter;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.templates.TemplateBuffer;
import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;

/**
 * This formatter can be used to format a preprocess template buffer.
 * 
 * @author gma
 * @since 0.9.1
 */
public class PreprocessTemplateFormatter {

    public void format(TemplateBuffer buffer, TemplateContext context,
            IJavaProject project) throws BadLocationException {
        VariableTracker tracker = new VariableTracker(buffer);
        IDocument document = tracker.getDocument();

        Map<?, ?> options;
        if (project != null) {
            options = project.getOptions(true);
        } else {
            options = JavaCore.getOptions();
        }

        String originSource = document.get();
        int[] formatKinds = { CodeFormatter.K_EXPRESSION,
                CodeFormatter.K_STATEMENTS, CodeFormatter.K_UNKNOWN };
        TextEdit edit = null;
        int indent = getIndent();
        for (int element : formatKinds) {

            edit = ToolFactory.createCodeFormatter(options).format(element,
                    originSource, // source to format
                    0, // starting position
                    originSource.length(), // length
                    indent, // initial indentation
                    System.getProperty("line.separator") // line separator
                    );
        }

        try {
            if (edit != null) {
                edit.apply(document, TextEdit.UPDATE_REGIONS);
            }
        } catch (MalformedTreeException e) {
            e.printStackTrace();
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        trimStart(document);
        tracker.updateBuffer();

    }

    private int getIndent() {
        return 2;
    }

    private void trimStart(IDocument document) throws BadLocationException {
        int i = 0;
        while ((i != document.getLength())
                && Character.isWhitespace(document.getChar(i))) {
            i++;
        }

        document.replace(0, i, "");
    }

}
