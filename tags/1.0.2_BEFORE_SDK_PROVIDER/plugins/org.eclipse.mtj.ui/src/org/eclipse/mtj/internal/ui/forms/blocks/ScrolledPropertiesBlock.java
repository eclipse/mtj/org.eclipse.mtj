/**
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation          - initial API and implementation
 *     Diego Sandin (Motorola)  - Adapted code from org.eclipse.pde.ui
 *     David Marques (Motorola) - Adding setCurrentSelectedItem method
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     David Marques (Motorola) - Adding all buttons except scan.
 *     Edwin Carlo  (Motorola)  - Fixing MIDlets tab model synchronization.
 */
package org.eclipse.mtj.internal.ui.forms.blocks;

import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.MasterDetailsBlock;
import org.eclipse.ui.forms.SectionPart;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

/**
 *
 */
public class ScrolledPropertiesBlock extends MasterDetailsBlock {

    /**
     * 
     */
    private ButtonBarBlock buttonBarBlock = null;

    private ISelection currentSelectedItem;

    private List<DetailPage> detailPages;

    /**
     * 
     */
    private IStructuredContentProvider masterContentProvider;

    /**
     * 
     */
    private MasterLabelProvider masterLabelProvider;

    /**
     * 
     */
    private FormPage page;

    private TableViewer viewer;

    /**
     * @param page
     * @param contentProvider
     * @param labelProvider
     */
    public ScrolledPropertiesBlock(FormPage page,
            IStructuredContentProvider contentProvider,
            MasterLabelProvider labelProvider, List<DetailPage> detailPages) {
        this.page = page;
        masterContentProvider = contentProvider;
        masterLabelProvider = labelProvider;
        this.detailPages = detailPages;
    }

    /**
     * @return
     */
    public ButtonBarBlock getButtonBarBlock() {
        return buttonBarBlock;
    }

    /**
     * @return the currentSelectedItem
     */
    public ISelection getCurrentSelectedItem() {
        return currentSelectedItem;
    }

    /**
     * @return
     */
    public IStructuredContentProvider getMasterContentProvider() {
        return masterContentProvider;
    }

    /**
     * @return
     */
    public MasterLabelProvider getMasterLabelProvider() {
        return masterLabelProvider;
    }

    /**
     * 
     */
    public void refresh() {
        viewer.refresh();
        currentSelectedItem = viewer.getSelection();
    }

    /**
     * Sets the current selected item.
     * 
     * @param selection selected item.
     */
    public void setCurrentSelectedItem(ISelection selection) {
        viewer.setSelection(selection);
    }

    /**
     * @param masterContentProvider
     */
    public void setMasterContentProvider(
            IStructuredContentProvider masterContentProvider) {
        this.masterContentProvider = masterContentProvider;
    }

    /**
     * @param masterLabelProvider
     */
    public void setMasterLabelProvider(MasterLabelProvider masterLabelProvider) {
        this.masterLabelProvider = masterLabelProvider;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.MasterDetailsBlock#createMasterPart(org.eclipse.ui.forms.IManagedForm, org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createMasterPart(final IManagedForm managedForm,
            Composite parent) {

        // final ScrolledForm form = managedForm.getForm();
        FormToolkit toolkit = managedForm.getToolkit();
        Section section = toolkit.createSection(parent, Section.DESCRIPTION
                | ExpandableComposite.TITLE_BAR);
        section
                .setText(MTJUIMessages.ScrolledPropertiesBlock_midlet_list_section_title);
        section
                .setDescription(MTJUIMessages.ScrolledPropertiesBlock_midlet_list_section_description);

        section.marginWidth = 10;
        section.marginHeight = 5;

        // toolkit.createCompositeSeparator(section);
        Composite client = toolkit.createComposite(section, SWT.WRAP);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.marginWidth = 2;
        layout.marginHeight = 2;
        client.setLayout(layout);

        new Label(client, SWT.NONE);
        new Label(client, SWT.NONE);

        Table table = toolkit.createTable(client, SWT.NULL);
        GridData gd = new GridData(GridData.FILL_BOTH);
        gd.heightHint = 20;
        gd.widthHint = 100;
        table.setLayoutData(gd);
        toolkit.paintBordersFor(client);

        Composite composite = toolkit.createComposite(client);
        FillLayout ButtonBarBlockLayout = new FillLayout(SWT.VERTICAL);
        ButtonBarBlockLayout.spacing = 3;
        composite.setLayout(ButtonBarBlockLayout);

        buttonBarBlock = new ButtonBarBlock(composite, toolkit,
                ButtonBarBlock.BUTTON_ALL & ~ButtonBarBlock.BUTTON_SCAN);

        section.setClient(client);

        final SectionPart spart = new SectionPart(section);
        managedForm.addPart(spart);
        viewer = new TableViewer(table);
        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                currentSelectedItem = event.getSelection();
                managedForm.fireSelectionChanged(spart, event.getSelection());

            }
        });
        viewer.setContentProvider(masterContentProvider);
        viewer.setLabelProvider(masterLabelProvider);
        viewer.setInput(page.getEditor().getEditorInput());
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.MasterDetailsBlock#createToolBarActions(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createToolBarActions(IManagedForm managedForm) {

        final ScrolledForm form = managedForm.getForm();

        Action haction = new Action("hor", IAction.AS_RADIO_BUTTON) { //$NON-NLS-1$
            @Override
            public void run() {
                sashForm.setOrientation(SWT.HORIZONTAL);
                form.reflow(true);
            }
        };
        haction.setChecked(true);
        haction
                .setToolTipText(MTJUIMessages.ScrolledPropertiesBlock_hor_action_toolTipText);
        haction.setImageDescriptor(MTJUIPluginImages.DESC_HORIZONTAL);

        Action vaction = new Action("ver", IAction.AS_RADIO_BUTTON) { //$NON-NLS-1$

            /* (non-Javadoc)
             * @see org.eclipse.jface.action.Action#run()
             */
            @Override
            public void run() {
                sashForm.setOrientation(SWT.VERTICAL);
                form.reflow(true);
            }
        };
        vaction.setChecked(false);
        vaction
                .setToolTipText(MTJUIMessages.ScrolledPropertiesBlock_ver_action_toolTipText);
        vaction.setImageDescriptor(MTJUIPluginImages.DESC_VERTICAL);

        form.getToolBarManager().add(haction);
        form.getToolBarManager().add(vaction);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.MasterDetailsBlock#registerPages(org.eclipse.ui.forms.DetailsPart)
     */
    @Override
    protected void registerPages(DetailsPart detailsPart) {

        for (DetailPage detailPage : detailPages) {
            DetailPage type = detailPage;

            detailsPart.registerPage(type.getObjectClass(), type
                    .getDetailsPage());
        }
    }
}
