/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 *     David Marques (Motorola) - Fixing win32 bug.
 *     David Marques (Motorola) - Setting context text uneditable.
 */
package org.eclipse.mtj.internal.ui.wizards.l10n;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IBuffer;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

/**
 * StringLocalizationWizardPage1 class provides the page 1
 * for the {@link StringLocalizationWizard} wizard.
 * 
 * @author David Marques
 */
public class StringLocalizationWizardPage1 extends WizardPage {

    private static final String      COLUMNS[] = { MTJUIMessages.StringLocalizationWizardPage1_valueColumn, MTJUIMessages.StringLocalizationWizardPage1_keyColumn };

    private IBuffer                  buffer;

    private TableViewer              viewer;
    private Table                    table;
    private Text                     text;

    private RegExpStringExternalizer externalizer;

    /**
     * Creates a StringLocalizationWizardPage1 instance
     * to display string literals from the specified
     * buffer.
     * 
     * @param _name source class name.
     * @param _buffer source class buffer.
     */
    protected StringLocalizationWizardPage1(String _name, IBuffer _buffer) {
        super("StringLocalizationWizardPage1"); //$NON-NLS-1$
        if (_buffer == null) {
            throw new IllegalArgumentException(MTJUIMessages.StringLocalizationWizardPage1_nullBuffer);
        }
        setTitle(NLS.bind(MTJUIMessages.StringLocalizationWizardPage1_pageTitle, _name));
        setImageDescriptor(MTJUIPluginImages.DESC_ADD_L10N);
        externalizer = new RegExpStringExternalizer();
        this.buffer = _buffer;
        
        // Changing the \n(s) by \r\n(s) on win32
        // in order to get the right regions of
        // text.
        String  content = this.buffer.getContents();
        if (Platform.getOS().equals(Platform.OS_WIN32)){
            content = content.replaceAll("\n", "\r\n"); //$NON-NLS-1$ //$NON-NLS-2$
            this.buffer.setContents(content);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        Composite main = new Composite(parent, SWT.NONE);

        GridLayout layout = new GridLayout(0x02, false);
        main.setLayout(layout);

        createTableSection(main);
        createSourceSection(main);
        setControl(main);
    }

    /**
     * Gets all externalized strings.
     * 
     * @return array of strings to externalize.
     */
    public StringLocalizationData[] getExternalizedStrings() {
        List<StringLocalizationData> result = new ArrayList<StringLocalizationData>();

        TableItem items[] = this.table.getItems();
        for (TableItem item : items) {
            Object object = item.getData();
            if (item.getChecked() && !item.getGrayed()
                    && object instanceof StringLocalizationData) {
                result.add((StringLocalizationData) object);
            }
        }
        return result.toArray(new StringLocalizationData[0x00]);
    }

    /**
     * Gets all ignored strings.
     * 
     * @return array of strings to ignored.
     */
    public StringLocalizationData[] getIgnoredStrings() {
        List<StringLocalizationData> result = new ArrayList<StringLocalizationData>();

        TableItem items[] = this.table.getItems();
        for (TableItem item : items) {
            Object object = item.getData();
            if (item.getChecked() && item.getGrayed()
                    && object instanceof StringLocalizationData) {
                result.add((StringLocalizationData) object);
            }
        }
        return result.toArray(new StringLocalizationData[0x00]);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.WizardPage#isPageComplete()
     */
    public boolean isPageComplete() {
        return getExternalizedStrings().length > 0x00
                || getIgnoredStrings().length > 0x00;
    }

    private void createTableSection(Composite _parent) {
        GridData gridData = null;

        Label label1 = new Label(_parent, SWT.NONE);
        label1.setText(MTJUIMessages.StringLocalizationWizardPage1_tableLabel);

        gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        label1.setLayoutData(gridData);

        final Button button1 = new Button(_parent, SWT.CHECK);
        button1.setText(MTJUIMessages.StringLocalizationWizardPage1_filterLabel);
        button1.setSelection(true);
        button1.addSelectionListener(new SelectionListener() {
            public void widgetDefaultSelected(SelectionEvent e) {
            }

            public void widgetSelected(SelectionEvent e) {
                updateStringsTable(button1.getSelection());
            }
        });

        gridData = new GridData(SWT.RIGHT, SWT.CENTER, true, false);
        button1.setLayoutData(gridData);

        createTableView(_parent);
    }

    private void createTableView(Composite _parent) {
        GridData gridData = null;

        gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
        gridData.horizontalSpan = 0x02;

        Composite tableParent = new Composite(_parent, SWT.NONE);
        tableParent.setLayoutData(gridData);

        GridLayout layout = new GridLayout(0x02, false);
        layout.marginHeight = 0x0;
        layout.marginWidth = 0x0;
        tableParent.setLayout(layout);

        table = new Table(tableParent, SWT.MULTI | SWT.CHECK | SWT.BORDER
                | SWT.FULL_SELECTION);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        table.addSelectionListener(new SelectionListener() {
            public void widgetDefaultSelected(SelectionEvent e) {
            }

            public void widgetSelected(SelectionEvent e) {
                showSourceString();
                getWizard().getContainer().updateButtons();
            }
        });

        TableLayout tableLayout = new TableLayout();
        tableLayout.addColumnData(new ColumnWeightData(50, true));
        tableLayout.addColumnData(new ColumnWeightData(50, true));
        table.setLayout(tableLayout);

        for (int i = 0; i < COLUMNS.length; i++) {
            TableColumn column = new TableColumn(table, SWT.LEFT);
            column.setText(COLUMNS[i]);
        }

        viewer = new TableViewer(table);
        viewer.setLabelProvider(new ClassFileStringsLabelProvider(buffer));
        viewer.setContentProvider(new ClassFileStringsContentProvider());
        viewer.setColumnProperties(COLUMNS);

        this.updateStringsTable(true);
        TableItem items[] = this.table.getItems();
        for (TableItem item : items) {
            item.setChecked(true);
        }
        
        CellEditor editors[] = { new TextCellEditor(table),
                new TextCellEditor(table) };
        viewer.setCellEditors(editors);
        viewer.setCellModifier(new StringLocalizationCellModifier());

        gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
        gridData.verticalSpan = 3;
        gridData.heightHint = 200;
        table.setLayoutData(gridData);

        Button button1 = new Button(tableParent, SWT.NONE);
        button1.setText(MTJUIMessages.StringLocalizationWizardPage1_externalizeButtonText);
        gridData = new GridData(SWT.FILL, SWT.TOP, false, false);
        button1.setLayoutData(gridData);
        button1.addSelectionListener(new SelectionListener() {

            public void widgetDefaultSelected(SelectionEvent e) {
            }

            public void widgetSelected(SelectionEvent e) {
                TableItem items[] = table.getSelection();
                for (TableItem item : items) {
                    item.setChecked(true);
                    item.setGrayed(false);
                }
                getWizard().getContainer().updateButtons();
            }
        });

        Button button2 = new Button(tableParent, SWT.NONE);
        button2.setText(MTJUIMessages.StringLocalizationWizardPage1_internalizeButtonText);
        gridData = new GridData(SWT.FILL, SWT.TOP, false, false);
        button2.setLayoutData(gridData);
        button2.addSelectionListener(new SelectionListener() {

            public void widgetDefaultSelected(SelectionEvent e) {
            }

            public void widgetSelected(SelectionEvent e) {
                TableItem items[] = table.getSelection();
                for (TableItem item : items) {
                    item.setChecked(false);
                }
                getWizard().getContainer().updateButtons();
            }
        });

        Button button3 = new Button(tableParent, SWT.NONE);
        button3.setText(MTJUIMessages.StringLocalizationWizardPage1_ignoreButtonText);
        gridData = new GridData(SWT.FILL, SWT.TOP, false, false);
        button3.setLayoutData(gridData);
        button3.addSelectionListener(new SelectionListener() {

            public void widgetDefaultSelected(SelectionEvent e) {
            }

            public void widgetSelected(SelectionEvent e) {
                TableItem items[] = table.getSelection();
                for (TableItem item : items) {
                    item.setChecked(true);
                    item.setGrayed(true);
                }
                getWizard().getContainer().updateButtons();
            }
        });
    }

    /**
     * Updates the table strings filtering or not the
     * already ignored string literals.
     * 
     * @param filterIgnored true if wants to filter
     * ignored strings false if not.
     */
    private void updateStringsTable(boolean filterIgnored) {
        List<StringLocalizationData> data = new ArrayList<StringLocalizationData>();
        IRegion[] regions = externalizer.externalize(buffer, !filterIgnored);
        for (int i = 0; i < regions.length; i++) {
            String value = buffer.getText(regions[i].getOffset(), regions[i]
                    .getLength());
            data.add(new StringLocalizationData(regions[i], NLS.bind("key_{0}", //$NON-NLS-1$
                    String.valueOf(i)), value.replaceAll("\"", ""))); //$NON-NLS-1$ //$NON-NLS-2$
        }

        if (data.size() == 0x00) {
            setMessage(MTJUIMessages.StringLocalizationWizardPage1_noStringsToLocalize, WizardPage.WARNING);
        } else {
            setMessage(MTJUIMessages.StringLocalizationWizardPage1_pageText);
        }
        viewer.setInput(data.toArray(new StringLocalizationData[0x00]));
    }

    /**
     * Shows the selected string into the source code.
     */
    private void showSourceString() {
        if (this.buffer == null) {
            return;
        }

        StringLocalizationData data[] = getSelectedData();
        if (data != null && data.length > 0x00) {
            IRegion region = data[0x00].getRegion();
            this.text.setSelection(region.getOffset(), region.getOffset()
                    + region.getLength());
            this.text.showSelection();
        }
    }

    /**
     * Gets the current selected {@link StringLocalizationData} instance.
     * 
     * @return current selected data.
     */
    private StringLocalizationData[] getSelectedData() {
        List<StringLocalizationData> result = new ArrayList<StringLocalizationData>();

        TableItem items[] = this.table.getSelection();
        for (TableItem item : items) {
            Object object = item.getData();
            if (object instanceof StringLocalizationData) {
                result.add((StringLocalizationData) object);
            }
        }
        return result.toArray(new StringLocalizationData[0x00]);
    }

    private class StringLocalizationCellModifier implements ICellModifier {

        public boolean canModify(Object element, String property) {
            return property.equals(COLUMNS[0x01]);
        }

        public Object getValue(Object element, String property) {
            Object result = null;
            if (element instanceof StringLocalizationData) {
                StringLocalizationData data = (StringLocalizationData) element;
                if (property.equals(COLUMNS[0x01])) {
                    result = data.getKey();
                }
            }
            return result;
        }

        public void modify(Object element, String property, Object value) {
            StringLocalizationData data = null;
            if (element instanceof StringLocalizationData) {
                data = (StringLocalizationData) element;
            } else if (element instanceof Item) {
                data = (StringLocalizationData) ((Item) element).getData();
            }

            if (data != null && property.equals(COLUMNS[0x01])) {
                data.setKey((String) value);
                viewer.refresh();
            }
        }

    }

    private void createSourceSection(Composite _parent) {
        GridData gridData = null;

        Label label1 = new Label(_parent, SWT.NONE);
        label1.setText(MTJUIMessages.StringLocalizationWizardPage1_context);

        gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        gridData.horizontalSpan = 2;
        label1.setLayoutData(gridData);

        text = new Text(_parent, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL
                | SWT.V_SCROLL);
        gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        gridData.horizontalSpan = 2;
        text.setLayoutData(gridData);
        text.setEditable(false);
        if (this.buffer != null) {
            text.setText(this.buffer.getContents());
        }
    }
}
