/**
 * Copyright (c) 2008 Ales Milan and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Ales Milan               - Initial implementation
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     David Marques (Motorola) - Using IPath instead of URI.
 */

package org.eclipse.mtj.internal.ui.preferences;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.core.symbol.ISymbolSetFactory;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.swt.widgets.Display;

/**
 * Wizard for importing SymbolDefinitionSets from J2ME Polish.
 * 
 * @author Ales "afro" Milan
 */
public class SymbolDefinitionsImportWizard extends Wizard {
	private Display display = null;
	
    private String importDirectory = null;

    private SymbolDefinitionsImportWizardPage wizardPage;

    public SymbolDefinitionsImportWizard() {

        super();
        setNeedsProgressMonitor(true);
        setWindowTitle(MTJUIMessages.SymbolDefinitionsImportWizardPage_title);

    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#addPages()
     */
    @Override
    public void addPages() {
        wizardPage = new SymbolDefinitionsImportWizardPage();
        addPage(wizardPage);
        this.display = getShell().getDisplay();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public boolean performFinish() {

        switch (wizardPage.getImportType()) {
        case SymbolDefinitionsImportWizardPage.IMPORT_FROM_ANTENNA_JAR:
            try {
                getContainer().run(true, true,
                        getProcessImportFromAntennaJarFile());
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            break;

        case SymbolDefinitionsImportWizardPage.IMPORT_J2MEPOLISH_FORMAT:
            importDirectory = wizardPage.getDirectory();
            try {
                getContainer().run(true, true, getProcessImportFromXMLFiles());
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            break;
        case SymbolDefinitionsImportWizardPage.IMPORT_MTJ_FORMAT:

            break;
        }
        return true;
    }

    private IRunnableWithProgress getProcessImportFromAntennaJarFile() {

        // The runnable to do import definitions
        return new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor) {
                try {
                    IPreferenceStore preferenceStore = MTJUIPlugin.getDefault()
                            .getCorePreferenceStore();
                    String antennaFile = preferenceStore
                            .getString(IMTJCoreConstants.PREF_ANTENNA_JAR);

                	List <ISymbolSet> ss = MTJCore.getSymbolSetFactory().
                		createSymbolSetFromDataBase(ISymbolSetFactory.DEVICE_DB_J2MEPOLISH_JAR, new Path(antennaFile), monitor);
                	MTJCore.getSymbolSetRegistry().addSymbolSet(ss);
        	
                } catch (IOException e) {
                    handleException(
                            MTJUIMessages.SymbolDefinitionsImportWizardPage_error_errorDuringImportSymbolDefinitionSet,
                            e);
                } catch (PersistenceException e) {
                    handleException(
                            MTJUIMessages.SymbolDefinitionsImportWizardPage_error_errorDuringImportSymbolDefinitionSet,
                            e);
				} 
            }
        };

    }

    private IRunnableWithProgress getProcessImportFromXMLFiles() {

        // The runnable to do import definitions
        return new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor) {
            	try {
	                List <ISymbolSet> ss= MTJCore.
	                	getSymbolSetFactory().createSymbolSetFromDataBase(ISymbolSetFactory.DEVICE_DB_J2MEPOLISH_FILE, new Path(importDirectory), monitor);
	                MTJCore.getSymbolSetRegistry().addSymbolSet(ss);            	
                } catch (IOException e) {
                    handleException(
                            MTJUIMessages.SymbolDefinitionsImportWizardPage_error_errorDuringImportSymbolDefinitionSet,
                            e);
                } catch (PersistenceException e) {
                    handleException(
                            MTJUIMessages.SymbolDefinitionsImportWizardPage_error_errorDuringImportSymbolDefinitionSet,
                            e);
				} 
            }
        };

    }

    /**
     * An exception has occurred. Handle it appropriately.
     * 
     * @param t
     */
    private void handleException(final String message, Throwable t) {
    	t.printStackTrace();
    	MTJLogger.log(IStatus.WARNING, message, t);
    	display.syncExec(
    			  new Runnable() {
    			    public void run(){
    			        MessageDialog.openError(getShell(),
    			                MTJUIMessages.SymbolDefinitionsImportWizardPage_error, message);
    			    }
    			  });
    	
    }

}
