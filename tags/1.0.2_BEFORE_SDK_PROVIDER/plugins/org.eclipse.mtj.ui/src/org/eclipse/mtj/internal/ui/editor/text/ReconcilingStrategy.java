/**
 * Copyright (c) 2006,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.text;

import java.util.ArrayList;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.reconciler.DirtyRegion;
import org.eclipse.jface.text.reconciler.IReconcilingStrategy;
import org.eclipse.mtj.internal.core.text.IReconcilingParticipant;

/**
 * The reconciling strategy is used by the reconciler to reconcile a model based
 * on text of a particular content type. It provides methods for incremental as
 * well as non-incremental reconciling.
 * <p>
 * If a reconcile strategy consists of several steps between which model
 * transformation is desired the each step should implement
 * {@link org.eclipse.jface.text.reconciler.IReconcileStep}.
 * </p>
 * 
 * @since 0.9.1
 */
public class ReconcilingStrategy implements IReconcilingStrategy {

    private IDocument fDocument;
    private ArrayList<IReconcilingParticipant> fParticipants = new ArrayList<IReconcilingParticipant>();

    /**
     * Creates a new ReconcilingStrategy
     */
    public ReconcilingStrategy() {
    }

    /**
     * @param participant
     */
    public void addParticipant(IReconcilingParticipant participant) {
        fParticipants.add(participant);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.reconciler.IReconcilingStrategy#reconcile(org.eclipse.jface.text.reconciler.DirtyRegion, org.eclipse.jface.text.IRegion)
     */
    public void reconcile(DirtyRegion dirtyRegion, IRegion subRegion) {
        if (fDocument != null) {
            notifyParticipants();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.reconciler.IReconcilingStrategy#reconcile(org.eclipse.jface.text.IRegion)
     */
    public void reconcile(IRegion partition) {
        if (fDocument != null) {
            notifyParticipants();
        }
    }

    /**
     * @param participant
     */
    public void removeParticipant(IReconcilingParticipant participant) {
        fParticipants.remove(participant);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.reconciler.IReconcilingStrategy#setDocument(org.eclipse.jface.text.IDocument)
     */
    public void setDocument(IDocument document) {
        fDocument = document;
    }

    /**
     * 
     */
    private synchronized void notifyParticipants() {
        for (int i = 0; i < fParticipants.size(); i++) {
            (fParticipants.get(i)).reconciled(fDocument);
        }
    }

}
