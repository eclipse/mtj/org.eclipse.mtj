/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.util;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

/**
 * @since 0.9.1
 */
public class FolderValidator implements ISelectionStatusValidator {

    /* (non-Javadoc)
     * @see org.eclipse.ui.dialogs.ISelectionStatusValidator#validate(java.lang.Object[])
     */
    public IStatus validate(Object[] selection) {

        if (selection.length > 0 && selection[0] instanceof IContainer) {

            if (((IContainer) selection[0]).getType() == IContainer.FOLDER) {
                return new Status(IStatus.OK, MTJUIPlugin.getPluginId(),
                        IStatus.OK, Utils.EMPTY_STRING, null);
            }
        }
        return new Status(IStatus.ERROR, MTJUIPlugin.getPluginId(),
                IStatus.ERROR, Utils.EMPTY_STRING, null);
    }

}
