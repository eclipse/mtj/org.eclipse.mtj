#!/bin/bash

# *****************************************************************************************************************************************
# Copyright (c) 2008 Motorola.
# 
# All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 
# which accompanies this distribution, and is available at: http://www.eclipse.org/legal/epl-v10.html 
# 
# Contributors:
#     Motorola - initial version
# *****************************************************************************************************************************************
#         										Mobile Tools for Java nightly build script
# *****************************************************************************************************************************************

. build.sh -publish -notify -updateSite -updateSiteDirectory "/home/data/httpd/download.eclipse.org/dsdp/mtj/updates/0.9/NightlyBuilds/" N