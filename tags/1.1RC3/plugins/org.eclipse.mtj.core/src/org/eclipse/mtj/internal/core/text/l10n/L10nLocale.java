/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola)  - Initial Version
 *     David Marques (Motorola) - Implementing validation.
 *     David Marques (Motorola) - Adding getEntry(String key) method.
 *     Renato Franca (Motorola) - Implementing nullpointer validation.
 *     Renato Franca (Motorola) - Fixing bug 257367. Error catching improvement
 *     Renato Franca (Motorola) - Implementing validate method [Bug 285589].
 *     Renato Franca (Motorola) - Allowing UTF-8 document encoding for validation [Bug 286873].
 */
package org.eclipse.mtj.internal.core.text.l10n;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.MTJPluginSchemas;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.osgi.util.NLS;

public class L10nLocale extends L10nObject {

    private static final long serialVersionUID = 1L;
    private Vector<L10nEntry> entryList;
    private Schema schema;

    /**
     * @param model
     */
    public L10nLocale(L10nModel model) {
        super(model, ELEMENT_LOCALE);
        entryList = new Vector<L10nEntry>();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#canBeParent()
     */
    @Override
    public boolean canBeParent() {
        return true;
    }

    /**
     * @return
     */
    public String getLanguageCode() {
        return getXMLAttributeValue(ATTRIBUTE_LANGUAGE_CODE);
    }

    /**
     * @return
     */
    public String getCountryCode() {
        return getXMLAttributeValue(ATTRIBUTE_COUNTRY_CODE);
    }

    /**
     * @return
     */
    public void setLanguageCode(String lc) {
        setXMLAttribute(ATTRIBUTE_LANGUAGE_CODE, lc);
    }

    /**
     * @return
     */
    public void setCountryCode(String cc) {
        setXMLAttribute(ATTRIBUTE_COUNTRY_CODE, cc);
    }

    /**
     * @return
     */
    public String getLocaleName() {
        migrateFromPrevVersion();
        return NLS.bind("{0}-{1}", new String[] { getLanguageCode(), //$NON-NLS-1$
                getCountryCode() });
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getName()
     */
    @Override
    public String getName() {
        return getLocaleName();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getType()
     */
    @Override
    public int getType() {
        return TYPE_LOCALE;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#validate()
     */
    @Override
    public void validate() {

        
        if (!validateLocaleSchema()) {

            IDocumentElementNode[] entries = this.getChildNodes();

            for (int i = 0; i < entries.length; i++) {
                L10nEntry entry;
                try {
                    entry = (L10nEntry) entries[i];
                } catch (Exception e) {
                    // ClasCastException can be thrown
                    continue;
                }
                entry.validate();
            }
        } else {
            //Search for invalid or duplicated tags

            String[] countries = Locale.getISOCountries();
            String[] languages = Locale.getISOLanguages();

            String lc = getLanguageCode();
            String cc = getCountryCode();

            boolean ccValid = false;
            boolean lcValid = false;

            for (String code : countries) {
                if (code.equalsIgnoreCase(cc)) {
                    ccValid = true;
                    break;
                }
            }

            for (String code : languages) {
                if (code.equalsIgnoreCase(lc)) {
                    lcValid = true;
                    break;
                }
            }

            String message = ""; //$NON-NLS-1$

            if ((!ccValid) || (!lcValid)) {

                if ((!lcValid)) {
                    message = Messages.L10nLocale_0;
                }
                if ((!ccValid)) {
                    message += Messages.L10nLocale_2;
                }

                setStatus(new Status(IStatus.ERROR,
                        IMTJCoreConstants.PLUGIN_ID, message));

            } else {
                setStatus(new Status(IStatus.OK, IMTJCoreConstants.PLUGIN_ID,
                        "Valid Locale")); //$NON-NLS-1$
            }

            syncronizeEntryStates();
        }
    }

    /**
     * This method synchronizes the state of all entries in the locale.
     * 
     * @param locale target locale.
     */
    private void syncronizeEntryStates() {

        // Search for duplicated entries
        IDocumentElementNode[] entryNodes = this.getChildNodes();
        List<L10nEntry> cache = new LinkedList<L10nEntry>();
        for (IDocumentElementNode entryNode : entryNodes) {
            L10nEntry entry;
            try {
                entry = (L10nEntry) entryNode;
            } catch (Exception e) {
                // ClasCastException can be thrown
                continue;
            }
            if (!cache.contains(entry)) {
                List<L10nEntry> duplicates = this.findDuplicatedEntries(entry);
                if (duplicates.size() > 0) {
                    for (int j = 0; j < duplicates.size(); j++) {
                        L10nEntry duplicate;

                        try {
                            duplicate = (L10nEntry) duplicates.get(j);
                        } catch (Exception e) {
                            // ClasCastException can be thrown
                            continue;
                        }

                        duplicate
                                .setStatus(new Status(IStatus.ERROR,
                                        IMTJCoreConstants.PLUGIN_ID,
                                        Messages.L10nLocale_4));
                        cache.add(duplicate);
                    }
                    entry.setStatus(new Status(IStatus.ERROR,
                            IMTJCoreConstants.PLUGIN_ID, Messages.L10nLocale_5));
                } else {
                    entry.setStatus(new Status(IStatus.OK,
                            IMTJCoreConstants.PLUGIN_ID, null));
                }
            }
        }
    }

    /**
     * This method returns a list of all locale entries containing the specified
     * key.
     * 
     * @param locale target locale.
     * @param key entry key.
     * @return duplicated entries.
     */
    private List<L10nEntry> findDuplicatedEntries(L10nLocale locale, String key) {
        List<L10nEntry> duplicates = new LinkedList<L10nEntry>();
        IDocumentElementNode nodes[] = locale.getChildNodes();
        for (IDocumentElementNode node : nodes) {
            L10nEntry entry;
            
            if(node instanceof L10nEntry) {
                
                entry = (L10nEntry) node;
            } else {
                continue;
            }
            
            if (entry != null && entry.getKey() != null && key != null
                    && entry.getKey().toUpperCase().equals(key.toUpperCase())) {
                duplicates.add(entry);
            }
        }
        return duplicates;
    }

    /**
     * This method finds all locale entries duplicated with the specified
     * locale.
     * 
     * @param localeEntry target locale.
     * @return duplicated entries.
     */
    public List<L10nEntry> findDuplicatedEntries(L10nEntry localeEntry) {
        List<L10nEntry> duplicates = findDuplicatedEntries(
                (L10nLocale) localeEntry.getParent(), localeEntry.getKey());
        duplicates.remove(localeEntry);
        return duplicates;
    }

    /**
     * Gets the locale entry with the specified
     * key in this locale.
     * 
     * @param key entry key.
     * @return target entry or null if no entry
     * with the specified key.
     */
    public L10nEntry getEntry(String key) {
    	L10nEntry result = null;
    	IDocumentElementNode nodes[] = this.getChildNodes();
        for (IDocumentElementNode node : nodes) {
            L10nEntry entry = (L10nEntry) node;
            if (entry.getKey().toUpperCase().equals(key.toUpperCase())) {
                result = entry;
                break;
            }
        }
        return result;
    }
    
    /**
     * Since the compatibility with the LOcalization Data from MTJ version
     * 0.9.1RC1 was broken, we try to convert the old XML format to the new one
     */
    private void migrateFromPrevVersion() {

        boolean fail = false;
        String name = getXMLAttributeValue(ATTRIBUTE_NAME);

        if (name != null) {
            if ((getLanguageCode() == null) && (getCountryCode() == null)) {
                try {
                    StringTokenizer tokenizer = new StringTokenizer(name, "-"); //$NON-NLS-1$

                    if (tokenizer.countTokens() == 2) {
                        setLanguageCode(tokenizer.nextToken());
                        setCountryCode(tokenizer.nextToken());
                    } else {
                        fail = true;
                    }
                } catch (Exception e) {
                    fail = true;
                }
            } else {
                setXMLAttribute(ATTRIBUTE_NAME, null);
            }
        }

        if (fail) {
            setStatus(new Status(IStatus.ERROR, IMTJCoreConstants.PLUGIN_ID,
                    Messages.L10nLocale_7));
        }
    }

    /**
     * @return An vector with the document errors
     */
    public Vector<L10nMarkerError> getMarkerErrors() {

        IDocumentElementNode[] entryNodes = this.getChildNodes();
        Vector<L10nMarkerError> markerErrors = new Vector<L10nMarkerError>();
        int errorLine = -1;

        for (int i = 0; i < entryNodes.length; i++) {
            L10nEntry entry;

            try {
                entry = (L10nEntry) entryNodes[i];
                // validates the Locale's entry
                if (entry.getStatus().isOK()) {
                    continue;
                } else {

                    try {
                        // gets the error line
                        int offSet = getModel().getDocument().getLineOfOffset(
                                entry.getOffset());
                        int length = entry.getLength();
                        errorLine = offSet
                                + getModel().getDocument().getNumberOfLines(
                                        entry.getOffset(), length);

                    } catch (BadLocationException e) {
                        continue;
                    }
                    // creates the error object
                    L10nMarkerError marker = new L10nMarkerError(entry
                            .getStatus().getMessage(), errorLine);
                    markerErrors.add(marker);
                }

            } catch (Exception e) {
                continue;
            }
        }

        return markerErrors;
    }

    /**
     * 
     * @return A new Entry element
     */
    public L10nEntry createEntry() {
        L10nEntry entry = null;

        entry = new L10nEntry(getModel());

        entryList.add(entry);
        return entry;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.core.text.DocumentObject#removeChildNode(org
     * .eclipse.mtj.internal.core.text.IDocumentElementNode, boolean)
     */
    public IDocumentElementNode removeChildNode(IDocumentElementNode child,
            boolean fireEvent) {

        entryList.remove(child);

        return super.removeChildNode(child, fireEvent);

    }

    /*
     * Validate the Locale tag using a specific schema
     */
    private boolean validateLocaleSchema() {

        boolean isOK = true;
        String doc = this.toString();
        InputStream is = null;

        try {
            
            is = new BufferedInputStream(new ByteArrayInputStream(doc.getBytes("UTF-8"))); //$NON-NLS-1$

            String schemaLang = "http://www.w3.org/2001/XMLSchema";//$NON-NLS-1$

            SchemaFactory factory = SchemaFactory.newInstance(schemaLang);

            if (schema == null) {
                schema = factory.newSchema(MTJPluginSchemas.getInstance()
                        .create(MTJPluginSchemas.LOCALE_SCHEMA));
            }

            Validator validator = schema.newValidator();

            validator.validate(new StreamSource(is));

            Status status = new Status(IStatus.OK, IMTJCoreConstants.PLUGIN_ID,
                    null);
            this.setStatus(status);

        } catch (Exception ex) {
            Status status = new Status(IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID, ex.getMessage(), ex);
            this.setStatus(status);
            isOK = false;
        }

        return isOK;
    }

}
