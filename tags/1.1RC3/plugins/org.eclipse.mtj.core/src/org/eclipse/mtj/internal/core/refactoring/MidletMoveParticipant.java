/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Renato Franca (Motorola) - Fixing Application descriptor refresh call
 */
package org.eclipse.mtj.internal.core.refactoring;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.MoveParticipant;

/**
 * MTJ participant in type move refactoring operations
 * 
 * @author Craig Setera
 */
public class MidletMoveParticipant extends MoveParticipant {
    private IType type;

    /**
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#initialize(java.lang.Object)
     */
    protected boolean initialize(Object element) {
        type = (IType) element;
        return true;
    }

    /**
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#getName()
     */
    public String getName() {
        return "Update Application Descriptors";
    }

    /**
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#checkConditions(org.eclipse.core.runtime.IProgressMonitor,
     *      org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext)
     */
    public RefactoringStatus checkConditions(IProgressMonitor pm,
            CheckConditionsContext context) throws OperationCanceledException {
        return new RefactoringStatus();
    }

    /**
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#createChange(org.eclipse.core.runtime.IProgressMonitor)
     */
    public Change createChange(IProgressMonitor pm) throws CoreException,
            OperationCanceledException {
        SubProgressMonitor subMonitor = new SubProgressMonitor(pm, 5);
        subMonitor.setTaskName("Creating Application Descriptor Changes");
        
        IPackageFragment packageFragment = (IPackageFragment) getArguments()
        .getDestination();
        
        IJavaProject destinationJavaProject = packageFragment.getJavaProject();
        IJavaProject originalJavaProject = type.getJavaProject();
        
        // It's not necessary refresh the application descriptor when the
        // MIDlet file is moved to other project
        if (!destinationJavaProject.equals(originalJavaProject)) {
            return null;
        }
        
        String newName = getNewName();
        
        CompositeChange compositeChange = MidletJadFileChangesCollector
                .collectChange(MidletJadFileChangesCollector.MODE_RENAME, type,
                        newName, subMonitor);

        subMonitor.done();
        return (compositeChange.getChildren().length > 0) ? compositeChange
                : null;
    }

    /**
     * Return the new name to be used for the updates.
     * 
     * @return
     */
    private String getNewName() {
        StringBuffer sb = new StringBuffer();

        IPackageFragment packageFragment = (IPackageFragment) getArguments()
                .getDestination();
        String packageName = packageFragment.getElementName();
        if ((packageName != null) && (packageName.length() > 0)) {
            sb.append(packageName).append(".");
        }

        sb.append(type.getElementName());

        return sb.toString();
    }
}
