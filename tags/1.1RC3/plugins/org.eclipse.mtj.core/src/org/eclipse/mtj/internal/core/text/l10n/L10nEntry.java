/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Renato Franca (Motorola) - Implementing validate method [Bug 285589].
 *     Renato Franca (Motorola) - Allowing UTF-8 document encoding for validation [Bug 286873].
 */
package org.eclipse.mtj.internal.core.text.l10n;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.MTJPluginSchemas;

/**
 * @since 0.9.1
 */
public class L10nEntry extends L10nObject {

    private static final long serialVersionUID = 1L;
    private Schema schema;

    /**
     * @param model
     */
    public L10nEntry(L10nModel model) {
        super(model, ELEMENT_ENTRY);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#canBeParent()
     */
    @Override
    public boolean canBeParent() {
        return false;
    }

    /**
     * @return
     */
    public String getKey() {
        return getXMLAttributeValue(ATTRIBUTE_KEY);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getName()
     */
    @Override
    public String getName() {
        return getXMLAttributeValue(ATTRIBUTE_KEY);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getType()
     */
    @Override
    public int getType() {
        return TYPE_ENTRY;
    }

    /**
     * @return
     */
    public String getValue() {
        return getXMLAttributeValue(ATTRIBUTE_VALUE);
    }

    /**
     * @param Key
     */
    public void setKey(String Key) {
        setXMLAttribute(ATTRIBUTE_KEY, Key);
    }

    /**
     * @param value
     */
    public void setValue(String value) {
        setXMLAttribute(ATTRIBUTE_VALUE, value);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#validate()
     */
    @Override
    public void validate() {
        
        validateEntrySchema();

    }
    
    /*
     * Validate the entry with his specific schema
     */
    private void validateEntrySchema() {

        String doc = this.toString();
        InputStream is = null;
        
        try {
            is = new BufferedInputStream(new ByteArrayInputStream(doc.getBytes("UTF-8"))); //$NON-NLS-1$

            String schemaLang = "http://www.w3.org/2001/XMLSchema";//$NON-NLS-1$

            SchemaFactory factory = SchemaFactory.newInstance(schemaLang);

            if (schema == null) {
                schema = factory.newSchema(MTJPluginSchemas.getInstance()
                        .create(MTJPluginSchemas.ENTRY_SCHEMA));
            }

            Validator validator = schema.newValidator();

            validator.validate(new StreamSource(is));
            
            Status status = new Status(IStatus.OK, IMTJCoreConstants.PLUGIN_ID, null);
            this.setStatus(status);

        }  catch (Exception ex) {
            Status status = new Status(IStatus.ERROR, IMTJCoreConstants.PLUGIN_ID, ex.getMessage(), ex);
            this.setStatus(status);
        }
        
    }
}
