/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *     Gustavo de Paula (Motorola) - refactor symbol api
 *     David Marques (Motorola)    - Implementing cloneable
 */
package org.eclipse.mtj.internal.core.symbol;

import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.osgi.util.NLS;

/**
 * The model of preprocess symbol. The instance of this class contains the name
 * and value of a preprocess symbol.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 * @author Feng Wang
 */
public class Symbol implements ISymbol, Cloneable {

    public static final int TYPE_ABILITY = 0;
    
    /**
     * Project Configuration
     */
    public static final int TYPE_CONFIG = 1;

    private String name;
    private int type;
    private String value;

    /**
     * The constructor.
     * 
     * @param name - The name of the symbol, should be unique in a SymbolSet.
     * @param value - The value of the symbol.
     */
    Symbol(String name) {
        this.name = name;
        this.value = "true";
    }
    
    /**
     * The constructor.
     * 
     * @param name - The name of the symbol, should be unique in a SymbolSet.
     * @param value - The value of the symbol.
     */
    public Symbol(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * The constructor.
     * 
     * @param name - The name of the symbol, should be unique in a SymbolSet.
     * @param value - The value of the symbol.
     */
    public Symbol(String name, String value, int type) {
        this.name = name;
        this.value = value;
        this.type = type;
    }    
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.symbol.ISymbol#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.symbol.ISymbol#getSafeValue()
	 */
	public String getSafeValue() {
		return SymbolUtils.getSafeSymbolValue(value);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.symbol.ISymbol#getType()
	 */
	public int getType() {
		return type;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.symbol.ISymbol#getValue()
	 */
	public String getValue() {
		return value;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.symbol.ISymbol#setName(java.lang.String)
	 */
	public void setName(String identifier) {
		name = identifier;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.symbol.ISymbol#setType(int)
	 */
	public void setType(int type) {
		this.type = type;

	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.symbol.ISymbol#setValue(java.lang.String)
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
	 */
	public void loadUsing(IPersistenceProvider persistenceProvider)
			throws PersistenceException {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
	 */
	public void storeUsing(IPersistenceProvider persistenceProvider)
			throws PersistenceException {
	}

    /**
     * If name equals, then symbol equals. {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Symbol other = (Symbol) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }
	
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return NLS
                .bind(
                        "{0}: {1}={2}", new Object[] { getClass().getSimpleName(), name, value }); //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
 
    /* (non-Javadoc)
     * @see java.lang.Object#clone()
     */
    public Object clone() throws CloneNotSupportedException {
    	return new Symbol(getName(), getValue(), getType());
    }
    
}
