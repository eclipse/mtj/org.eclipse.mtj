/**
 * Copyright (c) 2004,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 *     David Marques (Motorola)  - Adding m_bProjectSpecific property.
 *     David Marques (Motorola)  - Adding checks on setters for empty Strings.
 */
package org.eclipse.mtj.internal.core.build.sign;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;

/**
 * This is a simple container class (bean) designed to hold the various
 * project-specific properties relating to potential signing operations. It is
 * used by the dialog class that allows the user to manipulate the settings, as
 * well as the project properties persistence stuff.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Kevin Hunter
 */
public class SignatureProperties implements ISignatureProperties {

    // true if user wants project signed
    private boolean m_bSignProject;

    // true if it represents project specific settings
    private boolean m_bProjectSpecific;
    
    // display path to the keystore
    private String m_strKeyStoreDisplayPath;

    // alias that identifies signing key
    private String m_strKeyAlias;

    // how passwords are stored
    private int m_nPasswordStorageMethod;

    // keystore password
    private String m_strKeyStorePassword;

    // key password
    private String m_strKeyPassword;

    // optional type of the keystore
    private String m_strKeyStoreType;

    // optional provider of the crypto classes
    private String m_strKeyStoreProvider;

    /**
     * Standard constructor
     */
    public SignatureProperties() {
    }

    /**
     * Indicates whether or not the project is to be signed.
     * 
     * @return <code>true</code> if the project is to be signed,
     *         <code>false</code> otherwise
     */
    public void copy(ISignatureProperties other) {
        m_bSignProject = other.isSignProject();
        m_bProjectSpecific = other.isProjectSpecific();
        m_strKeyStoreDisplayPath = other.getKeyStoreDisplayPath();
        m_strKeyAlias = other.getKeyAlias();
        m_nPasswordStorageMethod = other.getPasswordStorageMethod();
        m_strKeyStorePassword = other.getKeyStorePassword();
        m_strKeyPassword = other.getKeyPassword();
        m_strKeyStoreType = other.getKeyStoreType();
        m_strKeyStoreProvider = other.getKeyStoreProvider();
    }

    /**
     * Resets the class to its default values
     */
    public void clear() {
    	m_bProjectSpecific = false;
        m_bSignProject = false;
        m_strKeyStoreDisplayPath = null;
        m_strKeyAlias = null;
        m_nPasswordStorageMethod = PASSMETHOD_PROMPT;
        m_strKeyStorePassword = null;
        m_strKeyPassword = null;
        m_strKeyStoreType = null;
        m_strKeyStoreProvider = null;
    }

    /**
     * Indicates whether or not the project is to be signed.
     * 
     * @return <code>true</code> if the project is to be signed,
     *         <code>false</code> otherwise
     */
    public boolean isSignProject() {
        return (m_bSignProject);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.build.sign.ISignatureProperties#isProjectSpecific()
     */
    public boolean isProjectSpecific() {
    	return this.m_bProjectSpecific;
    }
    
    /**
     * Indicates whether or not the project is to be signed.
     * 
     * @param bValue <code>true</code> if the project is to be signed,
     *            <code>false</code> otherwise.
     */
    public void setSignProject(boolean bValue) {
        m_bSignProject = bValue;
    }

    /**
     * Returns the password storage method.
     * 
     * @return One of <code>PASSMETHOD_PROMPT</code>,
     *         <code>PASSMETHOD_IN_KEYRING</code> or
     *         <code>PASSMETHOD_IN_PROJECT</code>.
     */
    public int getPasswordStorageMethod() {
        return (m_nPasswordStorageMethod);
    }

    /**
     * Sets the password storage method.
     * 
     * @param nMethod One of <code>PASSMETHOD_PROMPT</code>,
     *            <code>PASSMETHOD_IN_KEYRING</code> or
     */
    public void setPasswordStorageMethod(int nMethod) {
        m_nPasswordStorageMethod = nMethod;
    }

    /**
     * Type of the keystore file. <code>null</code> indicates the system
     * standard type. This string is passed to <code>KeyStore.getInstance</code>
     * as part of loading the keystore.
     * 
     * @return <code>String</code> indicating keystore file type.
     * @see java.security.KeyStore#getInstance(java.lang.String)
     * @see java.security.KeyStore#getInstance(java.lang.String,
     *      java.lang.String)
     */
    public String getKeyStoreType() {
        return (m_strKeyStoreType);
    }

    /**
     * Type of the keystore file. <code>null</code> indicates the system
     * standard type. This string is passed to <code>KeyStore.getInstance</code>
     * as part of loading the keystore.
     * 
     * @param strValue KeyStore type string, or <code>null</code> to use the
     *            system default type.
     * @see java.security.KeyStore#getInstance(java.lang.String)
     * @see java.security.KeyStore#getInstance(java.lang.String,
     *      java.lang.String)
     */
    public void setKeyStoreType(String strValue) {
    	if (strValue != null && strValue.length() > 0x00) {
    		m_strKeyStoreType = strValue;
		} else {			
			m_strKeyStoreType = null;
		}
    }

    /**
     * Returns the crypto provider string. <code>null</code> indicates the
     * system standard type. This string is passed to
     * <code>KeyStore.getInstance</code> as part of loading the keystore.
     * 
     * @return KeyStore provider string, or <code>null</code> to use the system
     *         default provider.
     * @see java.security.KeyStore#getInstance(java.lang.String,
     *      java.lang.String)
     */
    public String getKeyStoreProvider() {
        return (m_strKeyStoreProvider);
    }

    /**
     * Sets the crypto provider string. <code>null</code> indicates the system
     * standard type.
     * 
     * @param strValue KeyStore provider string, or <code>null</code> to use the
     *            system default provider.
     * @see java.security.KeyStore#getInstance(java.lang.String,
     *      java.lang.String)
     */
    public void setKeyStoreProvider(String strValue) {
    	if (strValue != null && strValue.length() > 0x00) {
    		m_strKeyStoreProvider = strValue;
		} else {			
			m_strKeyStoreProvider = null;
		}
    }

    /**
     * Returns the password for the keystore file, if passwords are being saved.
     * 
     * @return <code>String</code> containing keystore password.
     */
    public String getKeyStorePassword() {
        return (m_strKeyStorePassword);
    }

    /**
     * Sets the password for the keystore file.
     * 
     * @param strValue <code>String</code> containing keystore password.
     */
    public void setKeyStorePassword(String strValue) {
    	if (strValue != null && strValue.length() > 0x00) {
    		m_strKeyStorePassword = strValue;
		} else {			
			m_strKeyStorePassword = null;
		}
    }

    /**
     * Returns the "alias" string identifying the key and certificate that will
     * be used to sign the project.
     * 
     * @return <code>String</code> containing the alias identifying the key and
     *         certificate.
     */
    public String getKeyAlias() {
        return (m_strKeyAlias);
    }

    /**
     * Sets the "alias" string identifying the key and certificate that will be
     * used to sign the project.
     * 
     * @param strValue <code>String</code> containing the alias identifying the
     *            key and certificate.
     */
    public void setKeyAlias(String strValue) {
    	if (strValue != null && strValue.length() > 0x00) {
    		m_strKeyAlias = strValue;
		} else {			
			m_strKeyAlias = null;
		}
    }

    /**
     * Returns the key password, if passwords are being saved. Will return
     * <code>null</code> if passwords are not being saved.
     * 
     * @return <code>String</code> containing the key password.
     */
    public String getKeyPassword() {
        return (m_strKeyPassword);
    }

    /**
     * Sets the key password, if passwords are being saved. Ignored if passwords
     * are not being saved.
     * 
     * @param strValue <code>String</code> containing the key password.
     */
    public void setKeyPassword(String strValue) {
    	if (strValue != null && strValue.length() > 0x00) {
    		m_strKeyPassword = strValue;
		} else {			
			m_strKeyPassword = null;
		}
    }

    /**
     * Returns the display string representing the keystore path. This may be an
     * absolute path, or it may be a project-relative path. Relative paths are
     * of the form "<code>$/[Folder[/Folder...]]filename</code>". Absolute paths
     * have OS-dependent form.
     * 
     * @return <code>String</code> containing displayed path.
     * @see #isKeyStorePathExternal()
     */
    public String getKeyStoreDisplayPath() {
        return (m_strKeyStoreDisplayPath);
    }

    /**
     * Sets the display string representing the keystore path. This may be an
     * absolute path, or it may be a project-relative path.
     * 
     * @param path <code>String</code> containing displayed path.
     * @see #getKeyStoreDisplayPath()
     */
    public void setKeyStoreDisplayPath(String strValue) {
    	if (strValue != null && strValue.length() > 0x00) {
    		m_strKeyStoreDisplayPath = strValue;
		} else {			
			m_strKeyStoreDisplayPath = null;
		}
    }

    /**
     * Returns the absolute file system path to the keystore file. The specified
     * <code>project</code> instance is used to convert a project-relative path
     * to an absolute path.
     * 
     * @param project <code>IProject</code> to which this
     *            <code>ISignatureProperties</code> belongs.
     * @return <code>String</code> containing absolute path to keystore file.
     * @throws CoreException
     */
    public String getAbsoluteKeyStorePath(IProject project)
            throws CoreException {
        if (isKeyStorePathExternal()) {
            return (m_strKeyStoreDisplayPath);
        }

        return (toAbsolute(m_strKeyStoreDisplayPath, project));
    }

    /**
     * Indicates whether the keystore path is external to the project or
     * project-relative.
     * 
     * @return <code>true</code> if the keystore path is external to the
     *         project, <code>false</code> if it's relative to the current
     *         project.
     */
    public boolean isKeyStorePathExternal() {
        if (m_strKeyStoreDisplayPath == null) {
            return (true);
        }

        if (m_strKeyStoreDisplayPath.length() < ISignatureProperties.PROJECT_RELATIVE_PREFIX
                .length()) {
            return (true);
        }

        if (!ISignatureProperties.PROJECT_RELATIVE_PREFIX
                .equals(m_strKeyStoreDisplayPath.substring(0,
                        ISignatureProperties.PROJECT_RELATIVE_PREFIX.length()))) {
            return (true);
        }

        return (false);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.build.sign.ISignatureProperties#setProjectSpecific(boolean)
     */
    public void setProjectSpecific(boolean value) {
    	this.m_bProjectSpecific = value;
    }
    
    /**
     * Converts a project-relative path into an absolute one, based on the
     * location of the project containing the file.
     * 
     * @param path Relative path to be converted.
     * @param project <code>IProject</code> instance containing the relative
     *            path.
     * @return <code>String</code> containing the absolute path to the file.
     * @throws CoreException
     */
    public static String toAbsolute(String path, IProject project)
            throws CoreException {
        String relativePath = path
                .substring(ISignatureProperties.PROJECT_RELATIVE_PREFIX
                        .length());
        IPath projectPath = project.getLocation();
        IPath absolutePath = projectPath.append(relativePath);

        return (absolutePath.toString());
    }

    /**
     * Converts a basic project-relative path into the format that we recognize
     * internally. This is done by ensuring that the path string starts with our
     * "relative prefix."
     * 
     * @param path <code>String</code> containing the path to be converted.
     * @return <code>String</code> with the converted value.
     */
    public static String toRelative(String path) {
        if (path.charAt(0) == '/') {
            path = ISignatureProperties.PROJECT_RELATIVE_PREFIX
                    + path.substring(1);
        } else {
            path = ISignatureProperties.PROJECT_RELATIVE_PREFIX + path;
        }

        return (path);
    }
}
