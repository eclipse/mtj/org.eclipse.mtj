/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.build.export.states;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.build.export.AntennaExportException;
import org.eclipse.mtj.internal.core.build.export.AntennaBuildExport.CreateAntTaskDoneEvent;
import org.eclipse.mtj.internal.core.statemachine.AbstractState;
import org.eclipse.mtj.internal.core.statemachine.StateMachine;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;

/**
 * AbstractCreateAntTaskState defines an abstract state to be
 * extended by concrete states during the build.xml creation
 * process.
 * 
 * @author David Marques
 * @since 1.0
 */
public abstract class AbstractCreateAntTaskState extends AbstractState {

	private StateMachine stateMachine;
	private Document     document;
	private String       dependencies;
	
	private IMidletSuiteProject suiteProject;
	
	/**
	 * Creates an {@link AbstractCreateAntTaskState} instance bound to the
	 * specified state machine in order to create a target for the
	 * specified project.
	 * 
	 * @param machine bound {@link StateMachine} instance.
	 * @param project target {@link IMidletSuiteProject} instance.
	 * @param _document target {@link Document}.
	 */
	public AbstractCreateAntTaskState(StateMachine _stateMachine
			, IMidletSuiteProject _suiteProject, Document _document) {
		super(null);
		
		if (_stateMachine == null) {
			throw new IllegalArgumentException(Messages.AbstractCreateAntTaskState_stateMachineNotNull);
		}
		this.stateMachine = _stateMachine;
		
		if (_suiteProject == null) {
			throw new IllegalArgumentException(Messages.AbstractCreateAntTaskState_projectCanNotBeNull);
		}
		this.suiteProject = _suiteProject;
		
		if (_document == null) {
			throw new IllegalArgumentException(Messages.AbstractCreateAntTaskState_documentNotNull);
		}
		this.document     = _document;
		this.dependencies = ""; //$NON-NLS-1$
	}
	
	/**
	 * Called when the state is entered for the specified {@link MTJRuntime}.
	 * 
	 * @param runtime current runtime to build target for.
	 * @throws AntennaExportException Any error occurs.
	 */
	protected abstract void onEnter(MTJRuntime runtime) throws AntennaExportException;

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.statemachine.AbstractState#onEnter()
	 */
	protected void onEnter() {
		MTJRuntimeList runtimeList = getMidletSuiteProject().getRuntimeList();
		for (MTJRuntime runtime : runtimeList) {
			try {				
				this.onEnter(runtime);
			} catch (AntennaExportException e) {
				MTJLogger.log(IStatus.ERROR, e);
			}
		}
		this.stateMachine.postEvent(new CreateAntTaskDoneEvent());
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.statemachine.AbstractState#onExit()
	 */
	protected void onExit() {};
	
	/**
	 * Sets the ant target dependencies.
	 * 
	 * @param _dependencies string list.
	 */
	public void setDependencies(String _dependencies) {
		if (_dependencies != null) {			
			this.dependencies = _dependencies;
		}
	}
	
	/**
	 * Gets the build target {@link IMidletSuiteProject} instance.
	 * 
	 * @return project instance.
	 */
	protected IMidletSuiteProject getMidletSuiteProject() {
		return suiteProject;
	}

	/**
	 * Gets the dependencies list.
	 * 
	 * @return string list.
	 */
	public String getDependencies() {
		return dependencies;
	}
	
	/**
	 * Gets the state machine this state is
	 * bound to.
	 * 
	 * @return state machine instance.
	 */
	protected StateMachine getStateMachine() {
		return stateMachine;
	}
	
	/**
	 * Gets the target {@link Document} instance.
	 * 
	 * @return target document.
	 */
	protected Document getDocument() {
		return document;
	}
	
	/**
	 * Gets the required projects for the specified project.
	 * 
	 * @param _project target project.
	 * @return set of dependencies.
	 */
	protected Set<IProject> getRequiredProjects(IProject _project) {
		Set<IProject> result = new HashSet<IProject>();
		try {			
			for (IProject required : _project.getReferencedProjects()) {
				result.addAll(getRequiredProjects(required));
				result.add(required);
			}
		} catch (CoreException e) {
			MTJLogger.log(IStatus.ERROR, e);
		}
		return result;
	}
	
	/**
	 * Gets the formatted name for the specified name.
	 * 
	 * @param _name source name.
	 * @return formatted name.
	 */
	protected String getFormatedName(String _name) {
		return _name.replace(" ", "_"); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	/**
	 * Gets the dependencies for the specified runtime.
	 * 
	 * @param runtime target runtime.
	 * @return dependencies list.
	 */
	protected String getDependencies(MTJRuntime runtime) {
		StringBuffer buffer = new StringBuffer();
		String[]     deps   = getDependencies().split(","); //$NON-NLS-1$
		for (String dep : deps) {
			if (dep.length() > 0x00) {				
				buffer.append(NLS.bind("{0}-{1}" //$NON-NLS-1$
						, new String[] {dep, getFormatedName(runtime.getName())}));
			}
		}
		return buffer.toString();
	}
}
