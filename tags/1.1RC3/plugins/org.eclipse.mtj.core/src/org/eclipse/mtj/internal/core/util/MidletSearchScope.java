/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID.
 *     David Aragao (Motorola)  - Refactoring to make the class
 *     							  extends AbstractSuperClassSearchScope                          
 */
package org.eclipse.mtj.internal.core.util;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;

/**
 * A midlet search scope built from a set of other search scopes.
 * 
 * @author Craig Setera
 */
public class MidletSearchScope extends AbstractSuperClassSearchScope {

	private static final String searchClassName = "javax/microedition/midlet/MIDlet.class";

	public MidletSearchScope(IJavaProject javaProject)
			throws JavaModelException {
		super();
		this.setScopes(this.createSearchScopes(javaProject, searchClassName,IMTJCoreConstants.MIDLET_SUPERCLASS));
	}

}
