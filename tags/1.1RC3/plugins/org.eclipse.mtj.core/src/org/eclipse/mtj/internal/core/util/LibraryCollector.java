/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;

/**
 * LibraryCollector class extends {@link AbstractClasspathEntryVisitor} class
 * in order to collect all library paths for a target project.
 * 
 * @author David Marques
 */
public class LibraryCollector extends AbstractClasspathEntryVisitor {
    
    private List<IClasspathEntry> libs = new ArrayList<IClasspathEntry>();
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.util.AbstractClasspathEntryVisitor#visitProject(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IProgressMonitor)
     */
    public boolean visitProject(IClasspathEntry entry,
            IJavaProject javaProject, IJavaProject classpathProject,
            IProgressMonitor monitor) throws CoreException {
        this.libs.clear();
        return true;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.util.AbstractClasspathEntryVisitor#visitContainerBegin(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IPath, org.eclipse.core.runtime.IProgressMonitor)
     */
    public boolean visitContainerBegin(IClasspathEntry entry,
            IJavaProject javaProject, IPath containerPath,
            IProgressMonitor monitor) throws CoreException {
        return true;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.util.AbstractClasspathEntryVisitor#visitLibraryEntry(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IProgressMonitor)
     */
    public void visitLibraryEntry(IClasspathEntry entry, IJavaProject javaProject
            , IProgressMonitor monitor) throws CoreException {
        libs.add(entry);
    }
    
    /**
     * Gets the collected libraries' paths.
     * 
     * @param excludeNonExported if true only
     * includes exported libraries.
     * 
     * @return array of library paths.
     */
    public IPath[] getLibraryPaths(boolean excludeNonExported) {
        List<IPath> result = new ArrayList<IPath>();
        for (IClasspathEntry lib : this.libs) {
            if (excludeNonExported) {
                if (lib.isExported()) {                    
                    result.add(lib.getPath());
                }
            } else {
                result.add(lib.getPath());
            }
        }
        return result.toArray(new IPath[0x00]);
    }
}
