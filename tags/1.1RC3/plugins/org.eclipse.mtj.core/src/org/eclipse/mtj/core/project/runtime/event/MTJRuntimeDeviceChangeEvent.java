/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)          - Initial implementation
 *     Gustavo de Paula (Motorola) - Configuration refactory [Bug 265522]
 *     Diego Sandin (Motorola)     - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.project.runtime.event;

import java.util.EventObject;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.sdk.device.IDevice;

/**
 * MTJRuntimeDeviceChangeEvent is used to notify that the device of the
 * MTJRuntime changed.
 * <p>
 * All Events are constructed with a reference to the object, the "source", that
 * is logically deemed to be the object upon which the Event in question
 * initially occurred upon.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 * 
 * @since 1.0
 */
public class MTJRuntimeDeviceChangeEvent extends EventObject {

    /**
     * The default serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The new device instance.
     */
    private IDevice newDevice;

    /**
     * The previous device instance.
     */
    private IDevice oldDevice;

    /**
     * Creates a new instance of MTJRuntimeDeviceChangeEvent.
     * 
     * @param source the MTJRuntime that was changed.
     * @param oldDevice the previous device instance.
     * @param newDevice the new device instance.
     */
    public MTJRuntimeDeviceChangeEvent(MTJRuntime source, IDevice oldDevice,
            IDevice newDevice) {
        super(source);
        this.oldDevice = oldDevice;
        this.newDevice = newDevice;
    }

    /**
     * Return the new device instance.
     * 
     * @return the new device instance.
     */
    public IDevice getNewDevice() {
        return newDevice;
    }

    /**
     * Return the previous device instance.
     * 
     * @return the previous device instance.
     */
    public IDevice getOldDevice() {
        return oldDevice;
    }

    /**
     * Set the new device instance.
     * 
     * @param newDevice the new device instance.
     */
    public void setNewDevice(IDevice newDevice) {
        this.newDevice = newDevice;
    }

    /**
     * Set the previous device instance.
     * 
     * @param oldDevice the previous device instance.
     */
    public void setOldDevice(IDevice oldDevice) {
        this.oldDevice = oldDevice;
    }

}
