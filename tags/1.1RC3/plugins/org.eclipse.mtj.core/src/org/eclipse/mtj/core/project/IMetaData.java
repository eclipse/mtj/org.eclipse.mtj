/**
 * Copyright (c) 2009 Motorola and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Gustavo de Paula (Motorola) - Runtime refactoring
 *     Jon Dearden (Research In Motion) - Added new APIs for supporting SDK provider 
 *                                        meta data [Bug 286675]. 
 */
package org.eclipse.mtj.core.project;

import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.project.midp.IMIDPMetaData;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.osgi.framework.Version;

/**
 * Implementors of the IMetaData interface must provide a way to store and
 * retrieve IMTJProject related Metadata.
 * <p>
 * Each {@link IMTJProject} may have an specialized type of IMetaData
 * implementation. For example, {@link IMidletSuiteProject}'s uses an
 * {@link IMIDPMetaData} implementation to store it's metadata.
 * </p>
 * <p>
 * For retrieving the IMetaData implementation referent to a specific
 * IMTJProject type, clients must use
 * {@link MTJCore#createMetaData(org.eclipse.core.resources.IProject, ProjectType)}
 * method from the MTJCore facade.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noimplement This class is not intended to be implemented by clients.
 * @since 1.0
 */
public interface IMetaData {

    /**
     * The metadata file name for {@link IMTJProject} projects.
     */
    public static final String METADATA_FILE = ".mtj"; //$NON-NLS-1$

    /**
     * Return the list of runtimes that are associated to the project. From the
     * list it is possible to read each runtime and information such as the
     * device of each runtime.
     * <p>
     * If no list of runtimes was set previously, this method will create a new
     * empty MTJRuntimeList and associate it to the project.
     * </p>
     * 
     * @return list of runtimes that are associated to the project or an empty
     *         list if no runtime list was previously defined.
     */
    public abstract MTJRuntimeList getRuntimeList();

    /**
     * Returns the container class that holds the various project-specific
     * properties relating to potential signing operations.
     * 
     * @return an {@link ISignatureProperties} instance for use in signing
     *         operations.
     */
    public abstract ISignatureProperties getSignatureProperties();

    /**
     * Returns the version of the Metadata file.
     * 
     * @return Returns the version.
     */
    public abstract Version getVersion();

    /**
     * Save the current metadata state to the file system.
     * 
     * @throws CoreException if this method fails to save the metadata file.
     */
    public abstract void saveMetaData() throws CoreException;
    
    /**
     * Set a Properties file of meta data to be associated with an IMTJProject.
     * This meta data is arbitrary vendor-defined data that will be persisted 
     * with the project and automatically disposed when a project is deleted. 
     * <p>
     * A example use case is to save project level user preferences associated 
     * with a vendor-supplied JAD editor page.
     * <p>
     * The Properties file is not cloned. The SDK provider may set the Properties 
     * files once and continue to make changes to it. Values will be persisted the
     * next time saveMetaData() is called.
     * <p>
     * @param sdkId Unique identifier for the SDK. If meta data associated with
     * the ID is already set, the old meta data is replaced with the new.
     * @param metaData A Properties file of arbitrary vendor-defined values.
     * Passing in null is allowed and will 'erase' the meta data for that ID.
     * @throws IllegalArgumentException if sdkId is null or empty.
     * @since 1.1
     */
    public abstract void setSDKProviderMetaData(String sdkId,
            final Properties metaData) throws IllegalArgumentException;
    
    /**
     * Retrieve a Properties file of vendor-specific meta data associated with the 
     * IMTJProject.
     * <p>
     * The Properties file is not cloned and is therefore mutable. The SDK provider 
     * may continue to make changes to it. Values will be persisted the next time 
     * saveMetaData() is called.
     * <p>
     * @param sdkId Unique identifier for the SDK. 
     * @return A Properties object of vendor-specific meta data, or null if no such 
     * object is associated with the SDK ID.
     * @throws IllegalArgumentException if sdkId is null or empty.
     * @since 1.1
     */
    public abstract Properties getSDKProviderMetaData(String sdkId)
            throws IllegalArgumentException; 

    /**
     * Set the list of runtimes that are associated to the project.
     * <p>
     * If a <code>null</code> runtime is given, this method must throws an
     * IllegalArgumentException. A <code>null</code> runtime list would make the
     * MTJ build process fail when building the {@link IMTJProject} resources.
     * </p>
     * @param runtimeList the list of runtimes that are associated to the
     *            project.
     * @throws IllegalArgumentException if <code>null</code> was passed as the
     *             runtime list.
     */
    public abstract void setMTJRuntimeList(MTJRuntimeList runtimeList)
            throws IllegalArgumentException;

    /**
     * Sets the container class that holds the various project-specific
     * properties relating to potential signing operations.
     * 
     * @param signatureProperties the project-specific properties for use in
     *            signing operations.
     */
    public abstract void setSignatureProperties(
            ISignatureProperties signatureProperties);

}