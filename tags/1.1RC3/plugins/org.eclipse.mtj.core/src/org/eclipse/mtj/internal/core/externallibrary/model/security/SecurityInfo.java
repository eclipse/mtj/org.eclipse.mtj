/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.externallibrary.model.security;

/**
 * This Class represents the security information available in a library.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class SecurityInfo {

    private PermissionList permissionList;
    private ProtectionDomain protectionDomain;

    /**
     * Creates a new SecurityInfo instance.
     * 
     * @param permissionList
     * @param protectionDomain
     */
    public SecurityInfo(ProtectionDomain protectionDomain,
            PermissionList permissionList) {
        super();
        this.protectionDomain = protectionDomain;
        this.permissionList = permissionList;
    }

    /**
     * @return the permissionList
     */
    public PermissionList getPermissionList() {
        return permissionList;
    }

    /**
     * @return the protectionDomain
     */
    public ProtectionDomain getProtectionDomain() {
        return protectionDomain;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((permissionList == null) ? 0 : permissionList.hashCode());
        result = prime
                * result
                + ((protectionDomain == null) ? 0 : protectionDomain.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof SecurityInfo)) {
            return false;
        }
        SecurityInfo other = (SecurityInfo) obj;
        if (permissionList == null) {
            if (other.permissionList != null) {
                return false;
            }
        } else if (!permissionList.equals(other.permissionList)) {
            return false;
        }
        if (protectionDomain == null) {
            if (other.protectionDomain != null) {
                return false;
            }
        } else if (!protectionDomain.equals(other.protectionDomain)) {
            return false;
        }
        return true;
    }
}
