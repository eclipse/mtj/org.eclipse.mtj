/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.build.preverifier;

import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * A simple holder class that tracks the parameters for preverification.
 * 
 * @author Craig Setera
 */
public class StandardPreverifierParameters implements IPersistable {

    public String[] cldc10;
    public String[] cldc11;

    /**
     * Construct a new instance of the parameters.
     */
    public StandardPreverifierParameters() {
    }

    /**
     * Construct a new instance of the parameters.
     * 
     * @param cldc10
     * @param cldc11
     */
    public StandardPreverifierParameters(String[] cldc10, String[] cldc11) {
        this.cldc10 = cldc10;
        this.cldc11 = cldc11;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        cldc10 = loadArrayUsing(persistenceProvider, "cldc10");
        cldc11 = loadArrayUsing(persistenceProvider, "cldc11");
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        storeArrayUsing(persistenceProvider, "cldc10", cldc10);
        storeArrayUsing(persistenceProvider, "cldc11", cldc11);
    }

    /**
     * Load the specified array from the persistence provider.
     * 
     * @param persistenceProvider
     * @param baseName
     * @return
     * @throws PersistenceException
     */
    private String[] loadArrayUsing(IPersistenceProvider persistenceProvider,
            String baseName) throws PersistenceException {
        int count = persistenceProvider.loadInteger(baseName + "_count");
        String[] strings = new String[count];
        for (int i = 0; i < count; i++) {
            strings[i] = persistenceProvider.loadString(baseName + "_" + i);
        }

        return strings;
    }

    /**
     * Store the specified array in the persistence provider.
     * 
     * @param persistenceProvider
     * @param baseName
     * @param strings
     * @throws PersistenceException
     */
    private void storeArrayUsing(IPersistenceProvider persistenceProvider,
            String baseName, String[] strings) throws PersistenceException {
        persistenceProvider.storeInteger(baseName + "_count", strings.length);
        for (int i = 0; i < strings.length; i++) {
            persistenceProvider.storeString(baseName + "_" + i, strings[i]);
        }
    }
}
