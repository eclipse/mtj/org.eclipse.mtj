/**
 * Copyright (c) 2009 Research In Motion Limited.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial implementation.
 */
package org.eclipse.mtj.core.sdk.device;

/**
 * IManagedDevice is an IDevice offered by an ISDKProvider and contained within
 * a ManagedSDK. Such a device typically extends AbstractMIDPDevice and
 * implements IManagedDevice.
 * 
 * @Since 1.1
 */
public interface IManagedDevice extends IDevice {
   
    /**
     * This method should be called exclusively by a ManagedSDK to mark a device
     * as a duplicate. When a user duplicates an IManaged device, the ManagedSDK
     * will be asked to create and return the duplicate to the Device Manager.
     */
    public void setAsDuplicate();

    /**
     * Determine if this device is a duplicate of a previously provided device.
     * For devices provided by an ISDKProvider, duplicate devices may be deleted
     * whereas originals may not be.
     * 
     * @return True if this device is a duplicate, otherwise false.
     */
    public boolean isDuplicate();

}
