/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 *     David Marques (Motorola) - Changing action behavior.
 */
package org.eclipse.mtj.internal.ui.actions.l10n;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.l10n.L10nApi;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.actions.AbstractJavaProjectAction;
import org.eclipse.mtj.internal.ui.dialog.StringLocalizationCandidatesDialog;
import org.eclipse.mtj.internal.ui.wizards.l10n.StringLocalizationWizard;

/**
 * LocalizeStringsAction class implements the action
 * for the string localization.
 * 
 * @author David Marques
 */
public class LocalizeStringsAction extends AbstractJavaProjectAction {

    /* (non-Javadoc)
     * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
     */
    public void run(IAction action) {
        try {
            IJavaProject javaProject = this.getJavaProject(selection);
            if (javaProject == null) {
                return;
            }

            IProject project = javaProject.getProject();
            if (!project.hasNature(IMTJCoreConstants.L10N_NATURE_ID)) {                
                EnableLocalizationAction enableL10n = new EnableLocalizationAction();
                enableL10n.selectionChanged(action, new StructuredSelection(javaProject));
                enableL10n.run(action);
                
                if (!project.hasNature(IMTJCoreConstants.L10N_NATURE_ID)) {
                    MessageDialog.openInformation(getShell(),
                            MTJUIMessages.LocalizeStringsAction_errorDialogTitle,
                            MTJUIMessages.LocalizeStringsAction_projectL10nNotEnabled);
                    return;
                }
            }

            L10nModel   model   = L10nApi.loadL10nModel(project);
            L10nLocales locales = model.getLocales(); 
            if (locales.getChildCount() == 0x00) {
                L10nLocale locale = new L10nLocale(model);
                locale.setLanguageCode("en");
                locale.setCountryCode("US");
                locales.addChild(locale);
                model.save();
            }

            ICompilationUnit classes[] = getCandiateClasses();
            if (classes.length == 0x00) {
                return;
            }

            ICompilationUnit target = null;

            if (classes.length > 0x01) {
                StringLocalizationCandidatesDialog dialog = new StringLocalizationCandidatesDialog(
                        getShell(), classes);
                if (dialog.open() == Dialog.OK) {
                    target = dialog.getSelectedClass();
                }
            } else {
                target = classes[0x00];
            }

            if (target != null) {
                StringLocalizationWizard wizard = new StringLocalizationWizard(
                        target, model);
                WizardDialog dialog = new WizardDialog(getShell(), wizard);
                dialog.create();
                if (dialog.open() == Dialog.OK) {

                }
            }
        } catch (CoreException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }

    /**
     * Gets the candidate classes for the selection.
     * 
     * @return array of compilation units.
     */
    private ICompilationUnit[] getCandiateClasses() {
        List<ICompilationUnit> result = new ArrayList<ICompilationUnit>();
        if (this.selection != null && !this.selection.isEmpty()) {
            Object[] objects = this.selection.toArray();
            for (Object object : objects) {
                if (object instanceof ICompilationUnit) {
                    result.add((ICompilationUnit) object);
                } else if (object instanceof IPackageFragment) {
                    IPackageFragment ppackage = (IPackageFragment) object;
                    collectPackageClasses(result, ppackage);
                } else if (object instanceof IPackageFragmentRoot) {
                    IPackageFragmentRoot source = (IPackageFragmentRoot) object;
                    collectSourceClasses(result, source);
                } else if (object instanceof IJavaProject) {
                    IJavaProject javaProject = (IJavaProject) object;
                    collectProjectClasses(result, javaProject);
                }
            }
        }
        return result.toArray(new ICompilationUnit[0x00]);
    }

    private void collectProjectClasses(List<ICompilationUnit> result,
            IJavaProject javaProject) {
        try {
            IPackageFragmentRoot roots[] = javaProject
                    .getPackageFragmentRoots();
            for (IPackageFragmentRoot root : roots) {
                if (root.getKind() == IPackageFragmentRoot.K_SOURCE) {
                    collectSourceClasses(result, root);
                }
            }
        } catch (JavaModelException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }

    private void collectSourceClasses(List<ICompilationUnit> result,
            IPackageFragmentRoot source) {
        try {
            IJavaElement children[] = source.getChildren();
            for (IJavaElement child : children) {
                if (child instanceof IPackageFragment) {
                    collectPackageClasses(result, (IPackageFragment) child);
                }
            }
        } catch (JavaModelException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }

    private void collectPackageClasses(List<ICompilationUnit> result,
            IPackageFragment ppackage) {
        try {
            IJavaElement children[] = ppackage.getChildren();
            for (IJavaElement child : children) {
                if (child instanceof IPackageFragment) {
                    collectPackageClasses(result, (IPackageFragment) child);
                } else if (child instanceof ICompilationUnit) {
                    result.add((ICompilationUnit) child);
                }
            }
        } catch (JavaModelException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }

    protected IJavaProject getJavaProject(Object selected) {
        IJavaProject javaProject = super.getJavaProject(selected);
        if (javaProject == null && selected instanceof IStructuredSelection) {
            Object object = ((IStructuredSelection) selection)
                    .getFirstElement();
            if (object instanceof IJavaProject) {
                javaProject = (IJavaProject) object;
            } else if (object instanceof IJavaElement) {
                javaProject = ((IJavaElement) object).getJavaProject();
            }
        }
        return javaProject;
    }

}
