/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.ui.templates.midlets;

/**
 * MIDletTemplateBuilderException wraps all exceptions raised
 * in the template build process.
 * 
 * @author David Marques
 * @since 1.0
 */
public class MIDletTemplateBuilderException extends Exception {

    // Constants -----------------------------------------------------

    private static final long serialVersionUID = -3550918827370402565L;

    // Attributes ----------------------------------------------------

    // Static --------------------------------------------------------

    // Constructors --------------------------------------------------

    /**
     * Creates a MIDletTemplateBuilderException with the
     * specified message.
     * 
     * @param message error message.
     */
    public MIDletTemplateBuilderException(String message) {
        super(message);
    }

    /**
     * Create a MIDletTemplateBuilderException with the
     * specified throwable as cause.
     * 
     * @param cause throwable root cause.
     */
    public MIDletTemplateBuilderException(Throwable cause) {
        super(cause);
    }
    
    // Public --------------------------------------------------------

    // X implementation ----------------------------------------------

    // Y overrides ---------------------------------------------------

    // Package protected ---------------------------------------------

    // Protected -----------------------------------------------------

    // Private -------------------------------------------------------
}
