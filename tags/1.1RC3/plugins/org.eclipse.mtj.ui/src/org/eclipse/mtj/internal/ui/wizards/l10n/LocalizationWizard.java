/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Adding canFinish method.
 *     David Marques (Motorola) - Refactoring Localization API.
 *     David Marques (Motorola) - Refactoring to use L10nModel.
 *     Fernando Rocha(Motorola) - Refactoring Localization API.
 */
package org.eclipse.mtj.internal.ui.wizards.l10n;

import java.io.IOException;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.internal.core.l10n.L10nApi;
import org.eclipse.mtj.internal.ui.MTJUIMessages;

/**
 * LocalizationWizard class provides a wizard for setting up localization into a
 * java me project.
 * 
 * @since 0.9.1
 */
public class LocalizationWizard extends Wizard {

    private LocalizationPage page;
    private IJavaProject jProject;

    /**
     * Creates a LocalizationWizard instance associated to the specified
     * project.
     * 
     * @param project instance.
     */
    public LocalizationWizard(IJavaProject project) {
        super();
        jProject = project;
        setWindowTitle(MTJUIMessages.LocalizationWizard_window_title);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#addPages()
     */
    @Override
    public void addPages() {
        page = new LocalizationPage(jProject, getContainer());
        addPage(page);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#canFinish()
     */
    @Override
    public boolean canFinish() {
        IResource destination = page.getDestination();
        IJavaElement targetPack = page.getPackage();
        return (destination != null) && (targetPack != null);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        IResource properties = page.getDestination();
        IJavaElement targetPack = page.getPackage();

        boolean result = false;

        if (targetPack instanceof IPackageFragment) {
            try {
                result = L10nApi.createLocalizationFile(jProject.getProject(), properties
                        .getProjectRelativePath(), targetPack);
                jProject.getProject().refreshLocal(IResource.DEPTH_INFINITE,
                        new NullProgressMonitor());

                result &= L10nApi.createLocalizationApi(jProject.getProject(),
                        (IPackageFragment) targetPack, properties
                                .getProjectRelativePath());
                jProject.getProject().refreshLocal(IResource.DEPTH_INFINITE,
                        new NullProgressMonitor());
            } catch (CoreException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
