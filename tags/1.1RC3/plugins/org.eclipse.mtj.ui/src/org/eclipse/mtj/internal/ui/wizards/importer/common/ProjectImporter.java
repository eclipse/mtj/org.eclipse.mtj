/**
 * Copyright (c) 2008 Motorola Inc.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola) - Initial implementation                            
 */
package org.eclipse.mtj.internal.ui.wizards.importer.common;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.statushandlers.StatusManager;

/**
 * Class used to import a project.
 * @author Hugo Raniere
 *
 */
public abstract class ProjectImporter {

	/**
	 * Search for valid projects to import in the specified provider
	 * @param structureProvider
	 * @param monitor
	 * @return
	 */
	public ProjectRecord[] searchProjectsFromProvider(
			ILeveledImportStructureProvider structureProvider,
			IProgressMonitor monitor) {
		Collection<ProjectRecord> collected = new ArrayList<ProjectRecord>();
		Object child = structureProvider.getRoot();
		collectProjectsFromProvider(collected, child, structureProvider,
				monitor);
		return collected.toArray(new ProjectRecord[0]);
	}

	/**
	 * Search for valid projects to import in the specified direcoty
	 * @param directory
	 * @param monitor
	 * @return
	 */
	public ProjectRecord[] searchProjectsFromDirectory(File directory,
			IProgressMonitor monitor) {
		Collection<ProjectRecord> collected = new ArrayList<ProjectRecord>();
		collectProjectsFromDirectory(collected, directory,
				new HashSet<String>(), monitor);
		return collected.toArray(new ProjectRecord[0]);
	}

	/**
	 * Set of folders to skip while looking for projects. Must be filled by subclasses
	 */
	protected Set<String> skipFolders = new HashSet<String>();

	/**
	 * Collect the list of projects that are under directory into 'projects'.
	 * 
	 * @param projects
	 * @param root
	 * @param directoriesVisited
	 *            Set of canonical paths of directories, used as recursion guard
	 * @param monitor
	 *            The monitor to report to
	 */
	private void collectProjectsFromDirectory(
			Collection<ProjectRecord> projects, File root,
			Set<String> directoriesVisited, IProgressMonitor monitor) {

		if ((monitor == null) || (monitor.isCanceled())
				|| (!root.isDirectory())) {
			return;
		}
		SubMonitor progress = SubMonitor.convert(monitor);
		progress
				.setTaskName(NLS
						.bind(
								ProjectImporterMessage.WizardProjectsImportPage_CheckingMessage,
								root.getPath()));

		// Initialize recursion guard for recursive symbolic links
		try {
			directoriesVisited.add(root.getCanonicalPath());
		} catch (IOException exception) {
			StatusManager.getManager().handle(
					StatusUtil.newStatus(IStatus.ERROR, exception
							.getLocalizedMessage(), exception));
		}

		File projectDescriptor = new File(root, getProjectDescriptorFile());

		if (projectDescriptor.exists()) {
			ProjectRecord project = createProjectRecord(root.getAbsolutePath(),
					null);
			if (project != null) {
				projects.add(project);
				progress.done();
				// don't search sub-directories since we can't have nested
				// projects
				return;
			}
		}

		File[] nestedFolders = root.listFiles(new FileFilter() {

			public boolean accept(File pathname) {
				return pathname.isDirectory();
			}

		});

		progress.setWorkRemaining(nestedFolders.length);

		// recurse into sub-directories
		for (File newRoot : nestedFolders) {
			if (!skipFolders.contains(newRoot.getName())) {
				try {
					String canonicalPath = newRoot.getCanonicalPath();
					if (!directoriesVisited.add(canonicalPath)) {
						// already been here --> do not recurse
						continue;
					}
				} catch (IOException exception) {
					StatusManager.getManager().handle(
							StatusUtil.newStatus(IStatus.ERROR, exception
									.getLocalizedMessage(), exception));

				}
				collectProjectsFromDirectory(projects, newRoot,
						directoriesVisited, progress.newChild(1));
			}
		}
		progress.done();
	}

	/**
	 * Creates a project record correctly filled according to the specified root.
	 * Return null if the root contains an invalid project
	 * @param root
	 * @param provider
	 * @return
	 */
	protected abstract ProjectRecord createProjectRecord(String root,
			ILeveledImportStructureProvider provider);

	public abstract String getProjectDescriptorFile();

	/**
	 * Collect the list of projects that are under directory into 'projects'.
	 * 
	 * @param projects
	 * @param monitor
	 *            The monitor to report to
	 */
	private void collectProjectsFromProvider(
			Collection<ProjectRecord> projects, Object entry,
			ILeveledImportStructureProvider structureProvider,
			IProgressMonitor monitor) {

		if ((monitor == null) || (monitor.isCanceled())) {
			return;
		}
		SubMonitor progress = SubMonitor.convert(monitor);
		progress
				.setTaskName(NLS
						.bind(
								ProjectImporterMessage.WizardProjectsImportPage_CheckingMessage,
								structureProvider.getLabel(entry)));

		Object projectDescriptor = ArchiveUtils.getChild(structureProvider,
				entry, getProjectDescriptorFile());

		if (projectDescriptor != null) {
			ProjectRecord project = createProjectRecord(structureProvider
					.getFullPath(entry), structureProvider);
			if (project != null) {
				projects.add(project);
				progress.done();
				return;
			}
		}

		Object[] nestedFolders = ArchiveUtils.listSubfolders(structureProvider,
				entry);
		
		progress.setWorkRemaining(nestedFolders.length);
		// recurse into sub-directories
		for (Object newRoot : nestedFolders) {
			if (!skipFolders.contains(structureProvider.getLabel(newRoot))) {
				collectProjectsFromProvider(projects, newRoot,
						structureProvider, progress
								.newChild(1));
			}
		}
		progress.done();
	}

	/**
	 * Callback method to be called when the project is created, in a way that subclasses
	 * can perform other necessary changes
	 * @param projectRecord
	 * @param project
	 * @param monitor
	 * @throws CoreException
	 */
	public abstract void projectCreated(ProjectRecord projectRecord,
			IProject project, IProgressMonitor monitor) throws CoreException;

	/**
	 * Retrieves the Device used by the specified record
	 * @param record
	 * @return
	 * @throws CoreException
	 */
	public abstract IDevice getProjectDevice(ProjectRecord record)
			throws CoreException;

}
