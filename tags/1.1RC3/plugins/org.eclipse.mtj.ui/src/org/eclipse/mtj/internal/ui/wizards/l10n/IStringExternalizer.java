/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.ui.wizards.l10n;

import org.eclipse.jdt.core.IBuffer;
import org.eclipse.jface.text.IRegion;

/**
 * IStringExternalizer interface defines methods to be implemented by classes
 * that extract String literals from {@link IBuffer} instances.
 * 
 * @author David Marques
 */
public interface IStringExternalizer {

    public static final String NON_NLS = "/**$NON-NLS**/";

    /**
     * Gets all regions for the String literals within the specified buffer.
     * 
     * @param _buffer source buffer.
     * @param includeIgnored if true includes all the ignored Strings, excludes
     *            them otherwise
     * @return
     */
    public IRegion[] externalize(IBuffer _buffer, boolean includeIgnored);

}
