/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.ui.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.wizards.l10n.IStringExternalizer;
import org.eclipse.mtj.internal.ui.wizards.l10n.RegExpStringExternalizer;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

/**
 * StringLocalizationCandidatesDialog class provides a dialog for displaying the
 * number of localizable strings on a set of {@link ICompilationUnit} instances.
 * 
 * @author David Marques
 */
public class StringLocalizationCandidatesDialog extends Dialog {

    private ICompilationUnit cus[];
    private ICompilationUnit cu;
    private ListViewer       viewer;

    /**
     * Creates a StringLocalizationCandidatesDialog instance in order to list
     * the number of localizable strings on the specified array of
     * {@link ICompilationUnit} instances.
     * 
     * @param _parentShell parent {@link Shell}.
     * @param _cus compilation untis to analyse.
     */
    public StringLocalizationCandidatesDialog(Shell _parentShell,
            ICompilationUnit _cus[]) {
        super(_parentShell);
        this.cus = _cus;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets
     * .Shell)
     */
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);
        newShell.setText("Localize Strings");
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
     * .Composite)
     */
    protected Control createDialogArea(Composite _parent) {
        _parent.setLayout(new GridLayout(0x01, false));

        Composite parent = new Composite(_parent, SWT.NULL);
        parent.setLayout(new GridLayout(0x01, false));
        parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        viewer = new ListViewer(parent, SWT.SINGLE | SWT.H_SCROLL
                | SWT.V_SCROLL | SWT.BORDER);
        viewer.setContentProvider(new IStructuredContentProvider() {

            public Object[] getElements(Object inputElement) {
                List<StringOccurences> result = new ArrayList<StringOccurences>();
                if (inputElement instanceof ICompilationUnit[]) {
                    for (ICompilationUnit cu : (ICompilationUnit[]) inputElement) {
                        IStringExternalizer externalizer = new RegExpStringExternalizer();
                        try {
                            IRegion[] regions = externalizer.externalize(cu
                                    .getBuffer(), true);
                            if (regions.length > 0x00) {
                                StringOccurences occurences = new StringOccurences();
                                occurences.occurrences = regions.length;
                                occurences.className = cu.getResource()
                                        .getFullPath().toString();
                                occurences.cu = cu;
                                result.add(occurences);
                            }
                        } catch (JavaModelException e) {
                            MTJLogger.log(IStatus.ERROR, e);
                        }
                    }
                }
                return result.toArray(new StringOccurences[0x00]);
            }

            public void dispose() {
            }

            public void inputChanged(Viewer viewer, Object oldInput,
                    Object newInput) {
            }
        });

        viewer.setLabelProvider(new LabelProvider() {
            public String getText(Object element) {
                String result = null;
                if (element instanceof StringOccurences) {
                    StringOccurences data = (StringOccurences) element;
                    result = NLS.bind("{0} in {1}", new String[] {
                            String.valueOf(data.occurrences), data.className });
                }
                return result;
            }
        });

        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                ISelection selection = viewer.getSelection();
                if (!selection.isEmpty()
                        && selection instanceof IStructuredSelection) {
                    StringOccurences occurences = (StringOccurences) ((IStructuredSelection) selection)
                            .getFirstElement();
                    cu = occurences.cu;
                }
            }
        });

        viewer.setInput(cus);

        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        gridData.heightHint = 200;
        viewer.getList().setLayoutData(gridData);
        return parent;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.dialogs.Dialog#createButton(org.eclipse.swt.widgets
     * .Composite, int, java.lang.String, boolean)
     */
    protected Button createButton(Composite parent, int id, String label,
            boolean defaultButton) {
        Button button = super.createButton(parent, id, label, defaultButton);
        if (button != null && id == IDialogConstants.OK_ID) {
            button.setText("Localize...");
        }
        return button;
    }

    /**
     * Gets the selected class amongst the
     * candidates for localization.
     * 
     * @return target compilation unit.
     */
    public ICompilationUnit getSelectedClass() {
        return cu;
    }

    private class StringOccurences {
        public ICompilationUnit cu;
        public String           className;
        public int              occurrences;
    }

}
