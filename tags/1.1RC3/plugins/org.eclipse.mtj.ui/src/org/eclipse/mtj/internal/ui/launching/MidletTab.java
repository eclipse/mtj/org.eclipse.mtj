/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Fixed problems of launching in JAD mode.
 *     Feng Wang (Sybase)       - Replace ILaunchConstants.EMULATED_CLASS with
 *                                IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
 *                                to take advantage of JDT launch configuration 
 *                                refactoring participates.
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     David Marques (Motorola) - Decoupling JAD running from project reference.                                
 */
package org.eclipse.mtj.internal.ui.launching;

import java.io.File;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.debug.ui.launchConfigurations.JavaLaunchTab;
import org.eclipse.jdt.debug.ui.launchConfigurations.JavaMainTab;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.ui.JavaElementLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.launching.midp.IMIDPLaunchConstants;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.util.MidletSelectionDialogCreator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.eclipse.ui.dialogs.SelectionDialog;

/**
 * Specialization of the JavaMainTab for selecting MIDlets for launch.
 * 
 * @author Craig Setera
 */
public class MidletTab extends JavaLaunchTab {

    private Button jadBrowseButton;
    private Button jadRadio;
    private Text jadText;
    private JavaMainTab javaMainTab = new JavaMainTab();

    private Button midletRadio;
    private Text midletText;
    private Button otaRadio;

    private Button projectButton;
    private Label projectLabel;
    // Dialog widgets
    private Text projectText;

    private Button searchButton;

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.ILaunchConfigurationTab#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        Composite comp = new Composite(parent, SWT.NONE);
        setControl(comp);

        comp.setLayout(new GridLayout());

        createVerticalSpacer(comp, 3);
        createProjectComponents(comp);

        createVerticalSpacer(comp, 3);
        createExecutableComponents(comp);

        updateEnablement();
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.AbstractLaunchConfigurationTab#getId()
     */
    @Override
    public String getId() {
        return javaMainTab.getId();
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.AbstractLaunchConfigurationTab#getImage()
     */
    @Override
    public Image getImage() {
        return javaMainTab.getImage();
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.ILaunchConfigurationTab#getName()
     */
    public String getName() {
        return MTJUIMessages.MidletTab_tab_name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.debug.ui.launchConfigurations.JavaMainTab#initializeFrom(org.eclipse.debug.core.ILaunchConfiguration)
     */
    @Override
    public void initializeFrom(ILaunchConfiguration config) {
        updateProjectFromConfig(config);

        String midletName = getStringOrDefault(config,
                IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
                Utils.EMPTY_STRING);
        midletText.setText(midletName);

        String jadUrl = getStringOrDefault(config,
                IMIDPLaunchConstants.SPECIFIED_JAD_URL, Utils.EMPTY_STRING);
        jadText.setText(jadUrl);

        boolean doOTA = true;
        boolean doJAD = false;
        try {
            doOTA = config.getAttribute(IMIDPLaunchConstants.DO_OTA, true);
            doJAD = config.getAttribute(IMIDPLaunchConstants.DO_JAD_LAUNCH,
                    false);
        } catch (CoreException e) {
        }

        midletRadio.setSelection(!doOTA && !doJAD);
        jadRadio.setSelection(doJAD);
        otaRadio.setSelection(doOTA && !doJAD);

        updateEnablement();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.debug.ui.launchConfigurations.JavaMainTab#isValid(org.eclipse.debug.core.ILaunchConfiguration)
     */
    @Override
    public boolean isValid(ILaunchConfiguration config) {
        String errorMessage = null;

        String name = projectText.getText().trim();
        if (name.length() > 0) {
            if (!MTJCore.getWorkspace().getRoot().getProject(name)
                    .exists()) {
                errorMessage = MTJUIMessages.MidletTab_project_does_not_exist_error;
            }
        }

        // TODO Check this is really a MIDlet class
        if ((errorMessage == null) && midletRadio.getSelection()) {
            name = midletText.getText().trim();
            if (name.length() == 0) {
                errorMessage = MTJUIMessages.MidletTab_MIDlet_not_specified_error;
            }
        }

        if ((errorMessage == null) && (jadRadio.getSelection())) {

            File jadFile = new File(jadText.getText());
            if (!jadFile.exists()) {
                errorMessage = MTJUIMessages.MidletTab_invalid_JAD_URL_specified_error;
            }
        }

        setErrorMessage(errorMessage);

        return (errorMessage == null);
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.ILaunchConfigurationTab#performApply(org.eclipse.debug.core.ILaunchConfigurationWorkingCopy)
     */
    public void performApply(ILaunchConfigurationWorkingCopy config) {
        config.setAttribute(
                IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
                projectText.getText().trim());

        config.setAttribute(IMIDPLaunchConstants.DO_JAD_LAUNCH, jadRadio
                .getSelection());
        config.setAttribute(IMIDPLaunchConstants.SPECIFIED_JAD_URL, jadText
                .getText());

        config.setAttribute(IMIDPLaunchConstants.DO_OTA, otaRadio
                .getSelection());
        config.setAttribute(
                IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
                midletText.getText());
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.ILaunchConfigurationTab#setDefaults(org.eclipse.debug.core.ILaunchConfigurationWorkingCopy)
     */
    public void setDefaults(ILaunchConfigurationWorkingCopy config) {
        IJavaElement javaElement = getContext();
        if (javaElement != null) {
            initializeJavaProject(javaElement, config);
        } else {
            /*
             * We set empty attributes for project & main type so that when one
             * config is compared to another, the existence of empty attributes
             * doesn't cause an incorrect result (the performApply() method can
             * result in empty values for these attributes being set on a config
             * if there is nothing in the corresponding text boxes)
             */
            config.setAttribute(
                    IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
                    Utils.EMPTY_STRING);
        }

        config.setAttribute(
                IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
                Utils.EMPTY_STRING);
    }

    /**
     * Chooses a project for the type of java launch config that it is
     * 
     * @return
     */
    private IJavaProject chooseJavaProject() {

        IJavaProject project = null;

        ILabelProvider labelProvider = new JavaElementLabelProvider(
                JavaElementLabelProvider.SHOW_DEFAULT);
        ElementListSelectionDialog dialog = new ElementListSelectionDialog(
                getShell(), labelProvider);
        dialog.setTitle(MTJUIMessages.MidletTab_chooseJavaProject_title);
        dialog.setMessage(MTJUIMessages.MidletTab_chooseJavaProject_message);

        try {
            IWorkspaceRoot root = MTJCore.getWorkspace().getRoot();
            dialog.setElements(JavaCore.create(root).getJavaProjects());
        } catch (JavaModelException jme) {
            MTJLogger.log(IStatus.WARNING, jme);
        }

        IJavaProject javaProject = getJavaProject();
        if (javaProject != null) {
            dialog.setInitialSelections(new Object[] { javaProject });
        }

        if (dialog.open() == Window.OK) {
            project = (IJavaProject) dialog.getFirstResult();
        }

        return project;
    }

    /**
     * Create the components that handle the executable information.
     * 
     * @param parent
     */
    private void createExecutableComponents(Composite parent) {
        GridData gd;

        Font font = parent.getFont();
        Group group = new Group(parent, SWT.NONE);
        group.setText(MTJUIMessages.MidletTab_executable_text);
        
        // Set up the layout
        GridLayout mainLayout = new GridLayout(3, false);
        mainLayout.marginHeight = 0;
        mainLayout.marginWidth = 0;
        group.setLayout(mainLayout);

        gd = new GridData(GridData.FILL_HORIZONTAL);
        group.setLayoutData(gd);
        group.setFont(font);

        createMidletComponents(group);
        createJadComponents(group);

        otaRadio = new Button(group, SWT.RADIO);
        otaRadio.setText(MTJUIMessages.MidletTab_ota_text);
        otaRadio.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                updateEnablement();
                updateLaunchConfigurationDialog();
            }
        });

        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 2;
        otaRadio.setLayoutData(gd);
    }

    /**
     * Create the components that make up the JAD prompting.
     * 
     * @param parent
     */
    private void createJadComponents(Group parent) {
        GridData gd;

        Font font = parent.getFont();

        jadRadio = new Button(parent, SWT.RADIO);
        jadRadio.setText(MTJUIMessages.MidletTab_jad_url_text);
        jadRadio.setSelection(false);
        jadRadio.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                updateEnablement();
                updateLaunchConfigurationDialog();
            }
        });

        jadText = new Text(parent, SWT.SINGLE | SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        jadText.setLayoutData(gd);
        jadText.setFont(font);
        jadText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent evt) {
                updateLaunchConfigurationDialog();
            }
        });

        jadBrowseButton = createPushButton(parent,
                MTJUIMessages.MidletTab_browse_btn, null);
        jadBrowseButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent evt) {
                handleJadBrowseSelected();
            }
        });
    }

    /**
     * Create the components that make up the MIDlet prompting.
     * 
     * @param parent
     */
    private void createMidletComponents(Group parent) {
        GridData gd;

        Font font = parent.getFont();

        midletRadio = new Button(parent, SWT.RADIO);
        midletRadio.setText(MTJUIMessages.MidletTab_midlet_text);
        midletRadio.setSelection(true);
        midletRadio.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                updateEnablement();
                updateLaunchConfigurationDialog();
            }
        });

        midletText = new Text(parent, SWT.SINGLE | SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        midletText.setLayoutData(gd);
        midletText.setFont(font);
        midletText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent evt) {
                updateLaunchConfigurationDialog();
            }
        });

        searchButton = createPushButton(parent,
                MTJUIMessages.MidletTab_search_btn, null);
        searchButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent evt) {
                handleSearchButtonSelected();
            }
        });
    }

    /**
     * Create the components that handle the project information.
     * 
     * @param parent
     */
    private void createProjectComponents(Composite parent) {
        GridData gd;

        Font font = parent.getFont();
        Composite projComp = new Composite(parent, SWT.NONE);
        GridLayout projLayout = new GridLayout();
        projLayout.numColumns = 2;
        projLayout.marginHeight = 0;
        projLayout.marginWidth = 0;
        projComp.setLayout(projLayout);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        projComp.setLayoutData(gd);
        projComp.setFont(font);

        projectLabel = new Label(projComp, SWT.NONE);
        projectLabel.setText(MTJUIMessages.MidletTab_project_text);
        gd = new GridData();
        gd.horizontalSpan = 2;
        projectLabel.setLayoutData(gd);
        projectLabel.setFont(font);

        projectText = new Text(projComp, SWT.SINGLE | SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        projectText.setLayoutData(gd);
        projectText.setFont(font);
        projectText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent evt) {
                updateLaunchConfigurationDialog();
            }
        });

        projectButton = createPushButton(projComp,
                MTJUIMessages.MidletTab_browse_btn, null);
        projectButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent evt) {
                IJavaProject project = chooseJavaProject();
                if (project != null) {
                    String projectName = project.getElementName();
                    projectText.setText(projectName);
                }
            }
        });
    }

    /**
     * Get a string attribute from the launch configuration or the specified
     * default value.
     * 
     * @param launchConfig
     * @param attributeName
     * @param defaultValue
     * @return
     */
    private String getStringOrDefault(ILaunchConfiguration launchConfig,
            String attributeName, String defaultValue) {
        String value = null;

        try {
            value = launchConfig.getAttribute(attributeName, defaultValue);
        } catch (CoreException e) {
            MTJLogger.log(IStatus.WARNING, e);
            value = defaultValue;
        }

        return value;
    }

    /**
     * Update the enablement of controls based on the status of the other
     * controls.
     */
    private void updateEnablement() {
        boolean doMidlet = midletRadio.getSelection();
        midletText.setEnabled(doMidlet);
        searchButton.setEnabled(doMidlet);

        boolean doJAD = jadRadio.getSelection();
        jadText.setEnabled(doJAD);
        jadBrowseButton.setEnabled(doJAD);
        
        projectText.setEnabled(!doJAD);
        projectButton.setEnabled(!doJAD);
    }

    /**
     * @return
     */
    protected IJavaProject getJavaProject() {
        IJavaProject javaProject = null;

        String projectName = projectText.getText().trim();
        if (projectName.length() > 0) {
            IProject project = MTJCore.getWorkspace().getRoot()
                    .getProject(projectName);
            javaProject = JavaCore.create(project);
        }

        return javaProject;
    }

    /**
     * The JAD Browse button was selected. Allow the user to select a JAD file
     * and then enter in the URL of that JAD file into the text.
     */
    protected void handleJadBrowseSelected() {
        FileDialog fileDialog = new FileDialog(getShell());
        fileDialog.setFilterExtensions(new String[] { "*.jad" }); //$NON-NLS-1$
        fileDialog.setFilterNames(new String[] { "JAD File" }); //$NON-NLS-1$

        String filename = fileDialog.open();
        if (filename != null) {
            jadText.setText(filename);
        }
    }

    /**
     * 
     */
    protected void handleSearchButtonSelected() {
        try {
            IJavaProject javaProject = getJavaProject();
            SelectionDialog dialog = MidletSelectionDialogCreator
                    .createMidletSelectionDialog(getShell(),
                            getLaunchConfigurationDialog(), javaProject, false);

            if (dialog.open() == Window.OK) {
                Object[] results = dialog.getResult();
                if ((results != null) && (results.length > 0)) {
                    IType type = (IType) results[0];
                    if (type != null) {
                        midletText.setText(type.getFullyQualifiedName());
                        javaProject = type.getJavaProject();
                        projectText.setText(javaProject.getElementName());
                    }
                }
            }
        } catch (JavaModelException e) {
            MTJLogger
                    .log(
                            IStatus.ERROR,
                            MTJUIMessages.MidletTab_handleSearchButtonSelected_error,
                            e);
        }
    }

    /**
     * Update the project field from the launch configuration.
     * 
     * @param config a launch configuration for the project
     */
    protected void updateProjectFromConfig(ILaunchConfiguration config) {
        String projectName = Utils.EMPTY_STRING;
        try {
            projectName = config.getAttribute(
                    IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
                    Utils.EMPTY_STRING);
        } catch (CoreException ce) {
            MTJLogger.log(IStatus.WARNING,
                    MTJUIMessages.MidletTab_updateProjectFromConfig_error, ce);
        }

        projectText.setText(projectName);
    }
}
