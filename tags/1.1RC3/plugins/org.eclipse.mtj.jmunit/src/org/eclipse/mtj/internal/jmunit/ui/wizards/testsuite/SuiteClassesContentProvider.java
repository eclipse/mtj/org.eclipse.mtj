/**
 * Copyright (c) 2006,2008 Nokia and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nokia                    - Initial version
 *     Diego Madruga (Motorola) - Refactored some parts of code to follow MTJ 
 *                                standards
 *     Diego Madruga (Motorola) - Re-implemented class to check for a specific 
 *                                test case superclass      
 *     David Marques (Motorola) - Reimplementing getElements method.                     
 */
package org.eclipse.mtj.internal.jmunit.ui.wizards.testsuite;

import java.util.HashSet;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.internal.jmunit.IJMUnitContants;
import org.eclipse.mtj.internal.jmunit.JMUnitPlugin;
import org.eclipse.mtj.internal.jmunit.core.api.JMUnitTestFinder;

/**
 * Content providers for the Classes to be Included in Suite Table .
 * 
 * @author Gorkem Ercan
 * @since 0.9.1
 */
public class SuiteClassesContentProvider implements IStructuredContentProvider {

    private String fSuperclass;

    /**
     * Creates a new SuiteClassesContentProvider.
     * <p>
     * By default, sets {@link IJMUnitContants#JMUNIT_TESTCASE_CLDC11} as
     * superclass.
     * </p>
     */
    public SuiteClassesContentProvider() {
        fSuperclass = IJMUnitContants.JMUNIT_TESTCASE_CLDC11;
    }

    /**
     * Creates a new SuiteClassesContentProvider.
     * 
     * @param superclass the test superclass
     */
    public SuiteClassesContentProvider(String superclass) {
        fSuperclass = superclass;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IContentProvider#dispose()
     */
    public void dispose() {

    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
     */
    public Object[] getElements(Object inputElement) {
        HashSet<IType>   result = new HashSet<IType>();
        JMUnitTestFinder finder = null;

        try {            
            if (inputElement instanceof IJavaElement) {   
                IJavaElement javaElement = (IJavaElement) inputElement;
                finder = new JMUnitTestFinder(fSuperclass);
                finder.findTestsInContainer(javaElement, result, null);
            }
        } catch (CoreException e) {
            JMUnitPlugin.log(e);
        }
        return result.toArray(new IType[result.size()]);
    }

    /**
     * @return the superclass
     */
    public String getSuperclass() {
        return fSuperclass;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
     */
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
    }

    /**
     * @param superclass the superclass to set
     */
    public void setSuperclass(String superclass) {
        fSuperclass = superclass;
    }

}
