/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.ui.templates.midlets;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

/**
 * MIDletTemplateRegistry class is the registry for the extensions
 * of the org.eclipse.mtj.ui.midletTemplate extension point.
 * 
 * @author David Marques
 * @since 1.0
 */
public class MIDletTemplateRegistry {

    // Constants -----------------------------------------------------

    private static final String TEMPLATE_EXT_ID = "org.eclipse.mtj.ui.midlettemplate";
    
    // Attributes ----------------------------------------------------

    private static MIDletTemplateRegistry instance;
    private Map<String, MIDletTemplateObject> extensions;
    
    // Static --------------------------------------------------------

    /**
     * Gets the single instance of this class.
     * 
     * @return the singleton.
     */
    public static synchronized MIDletTemplateRegistry getInstance() {
        if (MIDletTemplateRegistry.instance == null) {
            MIDletTemplateRegistry.instance = new MIDletTemplateRegistry();
        }
        return MIDletTemplateRegistry.instance;
    }
    
    // Constructors --------------------------------------------------

    /**
     * Create a MIDletTemplateRegistry.
     */
    private MIDletTemplateRegistry() {
        IExtensionRegistry      registry   = null;
        IConfigurationElement[] extensions = null;
        MIDletTemplateObject    object     = null;
        
        this.extensions = new Hashtable<String, MIDletTemplateObject>();
        registry   = Platform.getExtensionRegistry();
        extensions = registry.getConfigurationElementsFor(TEMPLATE_EXT_ID);
        for (IConfigurationElement extension : extensions) {
            object = new MIDletTemplateObject(extension);
            this.extensions.put(object.getName(), object);
        }
    }
    
    // Public --------------------------------------------------------

    /**
     * Gets all registered templates.
     * 
     * @return template collection.
     */
    public Collection<MIDletTemplateObject> getTemplates() {
        return this.extensions.values();
    }
    
    /**
     * Gets the template with the specified name.
     * 
     * @param name - template name.
     * @return the template.
     */
    public MIDletTemplateObject getTemplate(String name) {
        return this.extensions.get(name);
    }
    
    // X implementation ----------------------------------------------

    // Y overrides ---------------------------------------------------

    // Package protected ---------------------------------------------

    // Protected -----------------------------------------------------

    // Private -------------------------------------------------------
}
