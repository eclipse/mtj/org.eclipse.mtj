/**
 * Copyright (c) 2008 Motorola Inc.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola) - Initial implementation                            
 */
package org.eclipse.mtj.internal.ui.wizards.importer.common;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class used to manipulate the fylesystem inside a compressed file (zip, tar) through
 * a ILeveledImportStructureProvider
 * @author Hugo Raniere
 *
 */
public class ArchiveUtils {
	
	private static final String PATH_SEPARATOR = "/|\\\\"; //$NON-NLS-1$

	/**
	 * Returns the element representing a file inside 'parent' in a compressed file.
	 * If the file cannot be found, return null
	 * @param provider
	 * @param parent
	 * @param name The name of the file to be searched.
	 * 			Can be a relative path separeted by '\' or '/'
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Object getChild(ILeveledImportStructureProvider provider, Object parent, String name) {
		String[] sections = name.split(PATH_SEPARATOR);
		boolean found;
		for (String section : sections) {
			found = false;
			List<Object> children = provider.getChildren(parent);
			for (Object child : children) {
				if (section.equals(provider.getLabel(child))) {
					parent = child;
					found = true;
					break;
				}
			}
			if (!found) {
				return null;
			}
		}
		return parent;
	}

	/**
	 * List all immediate subfolders inside 'parent' in a compressed file.
	 * @param provider
	 * @param parent
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Object[] listSubfolders(
			ILeveledImportStructureProvider provider, Object parent) {
		List<Object> subfolders = new ArrayList<Object>();
		List<Object> children = provider.getChildren(parent);
		for (Object child : children) {
			if (provider.isFolder(child)) {
				subfolders.add(child);
			}
		}
		return subfolders.toArray();
	}

	/**
	 * Returns the number of directories that the provider must strip
	 * from paths and file names
	 * @param element The element against which the strip number will be guessed
	 * @return
	 */
	public static int guessStripLevel(String element) {
		String[] sections = element.split(PATH_SEPARATOR);
		return sections.length;
	}

}
