/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.jface.text.hyperlink.IHyperlinkDetector;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.editor.MTJSourcePage;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * @since 0.9.1
 */
public class HyperlinkAction extends Action implements MouseListener,
        KeyListener {

    protected IHyperlinkDetector fDetector;
    protected IHyperlink fLink;
    protected StyledText fStyledText;

    /**
     * 
     */
    public HyperlinkAction() {
        setImageDescriptor(MTJUIPluginImages.DESC_LINK_OBJ);
        setEnabled(false);
    }

    /**
     * @return
     */
    public boolean detectHyperlink() {
        fLink = null;
        if (!hasDetector() || isTextDisposed()) {
            return false;
        }

        Point p = fStyledText.getSelection();
        IHyperlink[] links = fDetector.detectHyperlinks(null, new Region(p.x,
                p.y - p.x), false);

        if ((links == null) || (links.length == 0)) {
            return false;
        }

        fLink = links[0];
        return true;
    }

    /**
     * FIXME
     */
    public void generateActionText() {
        String text = MTJUIMessages.HyperlinkAction_text;
        setText(text);
        setToolTipText(text);
    }

    /**
     * @return
     */
    public IHyperlink getHyperLink() {
        return fLink;
    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.events.KeyListener#keyPressed(org.eclipse.swt.events.KeyEvent)
     */
    public void keyPressed(KeyEvent e) {
        setEnabled(detectHyperlink());
        generateActionText();
    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.events.KeyListener#keyReleased(org.eclipse.swt.events.KeyEvent)
     */
    public void keyReleased(KeyEvent e) {
        // Ignore
    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.events.MouseListener#mouseDoubleClick(org.eclipse.swt.events.MouseEvent)
     */
    public void mouseDoubleClick(MouseEvent e) {
        // Ignore
    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.events.MouseListener#mouseDown(org.eclipse.swt.events.MouseEvent)
     */
    public void mouseDown(MouseEvent e) {
        // Ignore
    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.events.MouseListener#mouseUp(org.eclipse.swt.events.MouseEvent)
     */
    public void mouseUp(MouseEvent e) {
        setEnabled(detectHyperlink());
        generateActionText();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {
        if (fLink != null) {
            fLink.open();
        }
    }

    /**
     * @param editor
     */
    public void setTextEditor(ITextEditor editor) {
        StyledText newText = editor instanceof MTJSourcePage ? ((MTJSourcePage) editor)
                .getViewer().getTextWidget()
                : null;
        if ((fStyledText != null) && fStyledText.equals(newText)) {
            return;
        }

        // remove the previous listeners if there were any
        removeListeners();
        fStyledText = newText;
        fDetector = editor instanceof MTJSourcePage ? (IHyperlinkDetector) ((MTJSourcePage) editor)
                .getAdapter(IHyperlinkDetector.class)
                : null;
        // Add new listeners, if hyperlinks are present
        addListeners();

        setEnabled(detectHyperlink());
        generateActionText();
    }

    /**
     * @return
     */
    private boolean isTextDisposed() {
        return (fStyledText == null) || fStyledText.isDisposed();
    }

    /**
     * 
     */
    protected void addListeners() {
        if (!hasDetector() || isTextDisposed()) {
            return;
        }
        fStyledText.addMouseListener(this);
        fStyledText.addKeyListener(this);
    }

    /**
     * @return
     */
    protected boolean hasDetector() {
        return fDetector != null;
    }

    /**
     * 
     */
    protected void removeListeners() {
        if (!hasDetector() || isTextDisposed()) {
            return;
        }
        fStyledText.removeMouseListener(this);
        fStyledText.removeKeyListener(this);
    }

}
