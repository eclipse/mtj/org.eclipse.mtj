/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.internal.ui.forms.blocks;

import org.eclipse.ui.forms.IDetailsPage;

/**
 * @author Diego Madruga Sandin
 */
public class DetailPage {

    /**
     * 
     */
    private Object objectClass;

    /**
     * 
     */
    private IDetailsPage detailsPage;

    /**
     * @param objectClass
     * @param detailsPage
     */
    public DetailPage(Object objectClass, IDetailsPage detailsPage) {
        super();
        this.objectClass = objectClass;
        this.detailsPage = detailsPage;
    }

    /**
     * @return
     */
    public IDetailsPage getDetailsPage() {
        return detailsPage;
    }

    /**
     * @return
     */
    public Object getObjectClass() {
        return objectClass;
    }

    /**
     * @param detailsPage
     */
    public void setDetailsPage(IDetailsPage detailsPage) {
        this.detailsPage = detailsPage;
    }

    /**
     * @param objectClass
     */
    public void setObjectClass(Object objectClass) {
        this.objectClass = objectClass;
    }

}
