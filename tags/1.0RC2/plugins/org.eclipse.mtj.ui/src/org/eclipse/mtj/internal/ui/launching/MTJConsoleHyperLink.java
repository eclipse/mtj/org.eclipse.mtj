/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version.
 */
package org.eclipse.mtj.internal.ui.launching;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.launching.StackTraceEntry;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.IHyperlink;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * MTJConsoleHyperLink class links opens s stack entry source file and selects
 * the relative line.
 */
public class MTJConsoleHyperLink implements IHyperlink {

    private StackTraceEntry stackEntry;

    /**
     * Creates a MTJConsoleHyperLink instance associated to a specified
     * StackTraceEntry.
     * 
     * @param _stackEntry instance.
     */
    public MTJConsoleHyperLink(StackTraceEntry _stackEntry) {
        this.stackEntry = _stackEntry;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.console.IHyperlink#linkActivated()
     */
    public void linkActivated() {
        File sourceFile = this.stackEntry.getJavaFile();
        /*The First line is at 0x00 so SUB 1*/
        int line = this.stackEntry.getLine() - 1;
        if (sourceFile != null) {
            try {
                IWorkbenchPage page = PlatformUI.getWorkbench()
                        .getActiveWorkbenchWindow().getActivePage();
                IEditorDescriptor desc = PlatformUI.getWorkbench()
                        .getEditorRegistry().getDefaultEditor(
                                sourceFile.getName());
                IFile[] files = MTJCore.getWorkspace().getRoot()
                        .findFilesForLocation(
                                new Path(sourceFile.getAbsolutePath()));
                IEditorPart editor = page.openEditor(new FileEditorInput(
                        files[0]), desc.getId());

                ITextEditor textEditor = (ITextEditor) editor
                        .getAdapter(ITextEditor.class);
                IDocument document = textEditor.getDocumentProvider()
                        .getDocument(textEditor.getEditorInput());
                textEditor.selectAndReveal(document.getLineOffset(line),
                        document.getLineLength(line) - 1);
            } catch (PartInitException e) {
                MTJLogger.log(IStatus.ERROR, e.getMessage(), e);
            } catch (BadLocationException e) {
                MTJLogger.log(IStatus.ERROR, e.getMessage(), e);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.console.IHyperlink#linkEntered()
     */
    public void linkEntered() {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.console.IHyperlink#linkExited()
     */
    public void linkExited() {
    }

}
