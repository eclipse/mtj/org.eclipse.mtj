/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.dialog;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class SrcFolderFilter extends ViewerFilter {

    private String[] srcFolders;
    private IProject project;

    /**
     * @param project
     * @param strings
     */
    public SrcFolderFilter(IProject project, String[] strings) {
        this.srcFolders = strings;
        this.project = project;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
     */
    @Override
    public boolean select(Viewer viewer, Object parentElement, Object element) {

        /* Only folders */
        if (element instanceof IFile) {
            return false;
        }

        /* the project root can't be used */
        if ((element instanceof IProject)
                && ((IProject) element).getName().equals(project.getName())) {
            return true;
        }

        if (element instanceof IResource) {

            if (((IResource) element).getType() == IResource.FOLDER) {
                IResource resources = ((IContainer) element);
                String rootfolder = resources.getProjectRelativePath().segment(
                        0);

                for (int j = 0; j < srcFolders.length; j++) {
                    if (srcFolders[j].equals(rootfolder)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
