/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant.template;

import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IInformationControlCreatorExtension;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Shell;

/**
 * @author gma
 * @since 0.9.1
 */

public class PreprocessTemplateInformationControlCreator implements
        IInformationControlCreator, IInformationControlCreatorExtension {
    private IInformationControl fControl;

    /*
     * @see org.eclipse.jface.text.IInformationControlCreatorExtension#canReplace(org.eclipse.jface.text.IInformationControlCreator)
     */
    public boolean canReplace(IInformationControlCreator creator) {
        return ((creator != null) && (getClass() == creator.getClass()));
    }

    /*
     * @see org.eclipse.jface.text.IInformationControlCreatorExtension#canReuse(org.eclipse.jface.text.IInformationControl)
     */
    public boolean canReuse(IInformationControl control) {
        return (fControl == control) && (fControl != null);
    }

    public IInformationControl createInformationControl(Shell parent) {
        fControl = new SourceViewerInformationControl(parent, false,
                SWT.LEFT_TO_RIGHT, null);
        fControl.addDisposeListener(new DisposeListener() {
            public void widgetDisposed(DisposeEvent e) {
                fControl = null;
            }
        });
        return fControl;
    }
}
