/**
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.mtj.internal.ui.parts.StructuredViewerPart;
import org.eclipse.mtj.internal.ui.parts.TreePart;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;

public abstract class TreeSection extends StructuredViewerSection {

    class PartAdapter extends TreePart {
        public PartAdapter(String[] buttonLabels) {
            super(buttonLabels);
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.parts.TreePart#buttonSelected(org.eclipse.swt.widgets.Button, int)
         */
        @Override
        public void buttonSelected(Button button, int index) {
            TreeSection.this.buttonSelected(index);
            if (fHandleDefaultButton) {
                button.getShell().setDefaultButton(null);
            }
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.parts.TreePart#handleDoubleClick(org.eclipse.jface.viewers.IStructuredSelection)
         */
        @Override
        public void handleDoubleClick(IStructuredSelection selection) {
            TreeSection.this.handleDoubleClick(selection);
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.parts.TreePart#selectionChanged(org.eclipse.jface.viewers.IStructuredSelection)
         */
        @Override
        public void selectionChanged(IStructuredSelection selection) {
            getManagedForm().fireSelectionChanged(TreeSection.this, selection);
            TreeSection.this.selectionChanged(selection);
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.parts.SharedPartWithButtons#createButtons(org.eclipse.swt.widgets.Composite, org.eclipse.ui.forms.widgets.FormToolkit)
         */
        @Override
        protected void createButtons(Composite parent, FormToolkit toolkit) {
            super.createButtons(parent, toolkit);
            enableButtons();
            if (parent.getData("filtered") != null) { //$NON-NLS-1$
                GridLayout layout = (GridLayout) fButtonContainer.getLayout();
                layout.marginHeight = 28;
            }
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.parts.TreePart#createTreeViewer(org.eclipse.swt.widgets.Composite, int)
         */
        @Override
        protected TreeViewer createTreeViewer(Composite parent, int style) {
            return TreeSection.this.createTreeViewer(parent, style);
        }

    }

    protected boolean fHandleDefaultButton = true;

    /**
     * Constructor for TableSection.
     * 
     * @param formPage
     */
    public TreeSection(MTJFormPage formPage, Composite parent, int style,
            String[] buttonLabels) {
        super(formPage, parent, style, buttonLabels);
    }

    protected TreeViewer createTreeViewer(Composite parent, int style) {
        return new TreeViewer(parent, style);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.StructuredViewerSection#createViewerPart(java.lang.String[])
     */
    @Override
    protected StructuredViewerPart createViewerPart(String[] buttonLabels) {
        return new PartAdapter(buttonLabels);
    }

    protected void enableButtons() {
    }

    protected TreePart getTreePart() {
        return (TreePart) fViewerPart;
    }

    /**
     * Expands or collapsed selected node according to its current state
     * 
     * @param selection
     */
    protected void handleDoubleClick(IStructuredSelection selection) {
        TreeViewer viewer = (TreeViewer) fViewerPart.getViewer();
        boolean expandedState = viewer.getExpandedState(selection
                .getFirstElement());
        viewer.setExpandedState(selection.getFirstElement(), !expandedState);
    }

    protected void selectionChanged(IStructuredSelection selection) {
    }
}
