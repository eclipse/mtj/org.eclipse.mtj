/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.eclipse.swt.dnd.ByteArrayTransfer;
import org.eclipse.swt.dnd.TransferData;

/**
 * The class ModelDataTransfer provides a platform specific mechanism for
 * converting a java byte[] to a platform specific representation of the byte
 * array and vice versa.
 * 
 * @since 0.9.1
 */
public class ModelDataTransfer extends ByteArrayTransfer {

    /**
     * Create a unique ID to make sure that different Eclipse applications use
     * different "types" of <code>ModelDataTransfer</code>
     */
    public static final String TYPE_PREFIX = "mtj-model-transfer-format"; //$NON-NLS-1$

    /**
     * Singleton instance.
     */
    private static final ModelDataTransfer instance = new ModelDataTransfer();

    private static final String TYPE_NAME = TYPE_PREFIX + ":" //$NON-NLS-1$
            + System.currentTimeMillis() + ":" //$NON-NLS-1$
            + instance.hashCode();

    private static final int TYPEID = registerType(TYPE_NAME);

    /**
     * Get the unique instance for the ModelDataTransfer.
     * 
     * @return the unique ModelDataTransfer instance.
     */
    public static ModelDataTransfer getInstance() {
        return instance;
    }

    /**
     * Construct a new ModelDataTransfer for MTJ.
     */
    public ModelDataTransfer() {
        super();
    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.dnd.Transfer#getTypeIds()
     */
    @Override
    protected int[] getTypeIds() {
        return new int[] { TYPEID };
    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.dnd.Transfer#getTypeNames()
     */
    @Override
    protected String[] getTypeNames() {
        return new String[] { TYPE_NAME };
    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.dnd.ByteArrayTransfer#javaToNative(java.lang.Object, org.eclipse.swt.dnd.TransferData)
     */
    @Override
    protected void javaToNative(Object data, TransferData transferData) {
        if (!(data instanceof Object[])) {
            return;
        }
        Object[] objects = (Object[]) data;
        int count = objects.length;

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream objectOut = new ObjectOutputStream(out);

            // write the number of resources
            objectOut.writeInt(count);

            // write each object
            for (Object object : objects) {
                objectOut.writeObject(object);
            }

            // cleanup
            objectOut.close();
            out.close();
            byte[] bytes = out.toByteArray();
            super.javaToNative(bytes, transferData);
        } catch (IOException e) {
            // it's best to send nothing if there were problems
            System.out.println(e);
        }

    }

    /* (non-Javadoc)
     * @see org.eclipse.swt.dnd.ByteArrayTransfer#nativeToJava(org.eclipse.swt.dnd.TransferData)
     */
    @Override
    protected Object nativeToJava(TransferData transferData) {
        byte[] bytes = (byte[]) super.nativeToJava(transferData);
        if (bytes == null) {
            return null;
        }
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new ByteArrayInputStream(bytes));

            int count = in.readInt();
            Object[] objects = new Object[count];
            for (int i = 0; i < count; i++) {
                objects[i] = in.readObject();
            }
            in.close();
            return objects;
        } catch (ClassNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }
}
