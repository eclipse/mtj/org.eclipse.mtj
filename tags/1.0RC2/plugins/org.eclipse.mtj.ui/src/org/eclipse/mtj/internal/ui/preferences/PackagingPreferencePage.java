/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.ui.IEmbeddableWorkbenchPreferencePage;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

/**
 * Preference page implementation for setting Java ME Over the Air preferences.
 * 
 * @author Craig Setera
 */
public class PackagingPreferencePage extends FieldEditorPreferencePage
        implements IEmbeddableWorkbenchPreferencePage, IMTJCoreConstants {

    private boolean embeddedInProperties;

    /**
     * Default constructor.
     */
    public PackagingPreferencePage() {
        this(false, MTJUIPlugin.getDefault().getCorePreferenceStore());
    }

    /**
     * Constructor for use when embedding the preference page within a
     * properties page.
     * 
     * @param embeddedInProperties
     * @param preferenceStore
     */
    public PackagingPreferencePage(boolean embeddedInProperties,
            IPreferenceStore preferenceStore) {
        super(GRID);

        this.embeddedInProperties = embeddedInProperties;
        setPreferenceStore(preferenceStore);

        if (!embeddedInProperties) {
            setDescription(MTJUIMessages.PackagingPreferencePage_description);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
     */
    @Override
    public void createFieldEditors() {
        final Composite parent = getFieldEditorParent();

        addField(new BooleanFieldEditor(PREF_PKG_AUTOVERSION,
                MTJUIMessages.PackagingPreferencePage_increment_label_text,
                parent));

        addField(new MultiValuedTableFieldEditor(PREF_PKG_EXCLUDED_PROPS,
                MTJUIMessages.PackagingPreferencePage_excluded_label_text,
                parent));
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performApply()
     */
    @Override
    public void performApply() {
        super.performApply();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performDefaults()
     */
    @Override
    public void performDefaults() {
        super.performDefaults();
    }

    /**
     * Overridden so that we get the help context where it belongs so that it
     * works when the focus is in the left pane.
     * 
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        if (embeddedInProperties) {
            noDefaultAndApplyButton();
        }

        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_PackagingPreferencePage"); //$NON-NLS-1$
        return super.createContents(parent);
    }
}
