/**
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.texteditor.AbstractMarkerAnnotationModel;

/**
 * A marker annotation model whose underlying source of markers is a resource in
 * the workspace.
 * <p>
 * This class may be instantiated; it is not intended to be sub-classed.
 * </p>
 * 
 * @since 0.9.1
 */
public class SystemFileMarkerAnnotationModel extends
        AbstractMarkerAnnotationModel {

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractMarkerAnnotationModel#deleteMarkers(org.eclipse.core.resources.IMarker[])
     */
    @Override
    protected void deleteMarkers(IMarker[] markers) throws CoreException {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractMarkerAnnotationModel#isAcceptable(org.eclipse.core.resources.IMarker)
     */
    @Override
    protected boolean isAcceptable(IMarker marker) {
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractMarkerAnnotationModel#listenToMarkerChanges(boolean)
     */
    @Override
    protected void listenToMarkerChanges(boolean listen) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractMarkerAnnotationModel#retrieveMarkers()
     */
    @Override
    protected IMarker[] retrieveMarkers() throws CoreException {
        return null;
    }

}
