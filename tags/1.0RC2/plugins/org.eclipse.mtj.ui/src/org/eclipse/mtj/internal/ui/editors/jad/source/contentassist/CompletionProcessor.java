/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 *     Gang  Ma     (Sybase)   - Do the real implementation
 */
package org.eclipse.mtj.internal.ui.editors.jad.source.contentassist;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.internal.core.project.midp.ApplicationDescriptor;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.sdk.device.midp.Configuration;
import org.eclipse.mtj.internal.core.sdk.device.midp.Profile;
import org.eclipse.mtj.internal.core.util.ColonDelimitedProperties;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.editors.jad.source.JADSourceEditor;

/**
 * A content assist processor to propose completions on {@link JADSourceEditor}
 * 
 * @author Diego Madruga Sandin
 */
public class CompletionProcessor implements IContentAssistProcessor {

    private static final String MIDLET_PREFIX = ApplicationDescriptor.MIDLET_PREFIX;
    private static final Pattern MIDLET_PATTERN = Pattern.compile("("
            + MIDLET_PREFIX + "\\d+:).*");

    private final IContextInformation[] NO_CONTEXTS = new IContextInformation[0];

    private final char[] PROPOSAL_ACTIVATION_CHARS = new char[] { 'M', 'm',
            ' ', ',' };

    private ICompletionProposal[] NO_COMPLETIONS = new ICompletionProposal[0];
    private String midletHeader;
    private ColonDelimitedProperties existedPropertis;

    private JADSourceEditor sourcePage;

    public CompletionProcessor(JADSourceEditor editor) {
        this.sourcePage = editor;
    }

    protected ICompletionProposal[] computeCompletionProposals(IDocument doc,
            int startOffset, int offset) {
        try {
            String currentLineValue = doc
                    .get(startOffset, offset - startOffset);
            if (!isHeader(currentLineValue)) {
                return computeValue(currentLineValue, startOffset, offset);
            }
            return computeHeader(currentLineValue, startOffset, offset);
        } catch (BadLocationException e) {
        }
        return new ICompletionProposal[0];
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#computeCompletionProposals(org.eclipse.jface.text.ITextViewer,
     *      int)
     */
    public ICompletionProposal[] computeCompletionProposals(ITextViewer viewer,
            int offset) {

        try {
            IDocument doc = viewer.getDocument();
            parseDocument(doc);
            int lineNum = doc.getLineOfOffset(offset);
            int lineStart = doc.getLineOffset(lineNum);
            return computeCompletionProposals(doc, lineStart, offset);
        } catch (BadLocationException e) {
        }

        return NO_COMPLETIONS;

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#computeContextInformation(org.eclipse.jface.text.ITextViewer,
     *      int)
     */
    public IContextInformation[] computeContextInformation(ITextViewer viewer,
            int offset) {
        return NO_CONTEXTS;
    }

    protected ICompletionProposal[] computeHeader(String currentValue,
            int startOffset, int offset) {
        ArrayList<JADAttributeCompletionProposal> completions = new ArrayList<JADAttributeCompletionProposal>();
        String[] headers = JADAttributeHeadersProvider
                .getSupportJADAttrHeaders();
        for (String element : headers) {
            if (element.regionMatches(true, 0, currentValue, 0, currentValue
                    .length())
                    && emptyString(existedPropertis.getProperty(element))) {
                JADAttributeCompletionProposal proposal = new JADAttributeCompletionProposal(
                        element + ":", null, element, startOffset, currentValue
                                .length());
                // proposal.setAdditionalProposalInfo(getDescription(fHeader[i]));
                completions.add(proposal);
            }
        }
        if (MIDLET_PREFIX.regionMatches(true, 0, currentValue, 0, currentValue
                .length())) {
            JADAttributeCompletionProposal proposal = new JADAttributeCompletionProposal(
                    midletHeader + ":", null, midletHeader, startOffset,
                    currentValue.length());
            completions.add(proposal);
        }
        return sortProposals(completions
                .toArray(new ICompletionProposal[completions.size()]));
    }

    protected ICompletionProposal[] computeValue(String currentValue,
            int startOffset, int offset) throws BadLocationException {
        Matcher midletMacher = MIDLET_PATTERN.matcher(currentValue);
        if (midletMacher.matches()) {
            return handleMIDletCompletion(currentValue.substring(midletMacher
                    .group(1).length() + 1), offset);
        }
        if (currentValue.regionMatches(true, 0,
                IJADConstants.JAD_MICROEDITION_CONFIG, 0, Math.min(currentValue
                        .length(), IJADConstants.JAD_MICROEDITION_CONFIG
                        .length()))) {
            return handleConfigurationCompletion(
                    currentValue
                            .substring(IJADConstants.JAD_MICROEDITION_CONFIG
                                    .length() + 1), offset);
        }
        if (currentValue.regionMatches(true, 0,
                IJADConstants.JAD_MICROEDITION_PROFILE, 0, Math.min(
                        currentValue.length(),
                        IJADConstants.JAD_MICROEDITION_PROFILE.length()))) {
            return handleProfileCompletion(
                    currentValue
                            .substring(IJADConstants.JAD_MICROEDITION_PROFILE
                                    .length() + 1), offset);
        }
        return NO_COMPLETIONS;
    }

    private boolean emptyString(Object o) {
        return (o == null) || "".equals(o);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getCompletionProposalAutoActivationCharacters()
     */
    public char[] getCompletionProposalAutoActivationCharacters() {
        return PROPOSAL_ACTIVATION_CHARS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getContextInformationAutoActivationCharacters()
     */
    public char[] getContextInformationAutoActivationCharacters() {
        return new char[0];
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getContextInformationValidator()
     */
    public IContextInformationValidator getContextInformationValidator() {
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.text.contentassist.IContentAssistProcessor#getErrorMessage()
     */
    public String getErrorMessage() {
        return null;
    }

    private IJavaProject getJavaProject() {
        return sourcePage.getJavaProject();
    }

    private ICompletionProposal[] handleConfigurationCompletion(String value,
            int offset) {

        ArrayList<JADAttributeCompletionProposal> completions = new ArrayList<JADAttributeCompletionProposal>();

        IAPI[] specifications = Configuration.values();
        value = value.trim();
        for (IAPI spec : specifications) {
            String configID = spec.toString();
            if (configID.regionMatches(true, 0, value, 0, value.length())) {
                JADAttributeCompletionProposal proposal = new JADAttributeCompletionProposal(
                        configID, null, configID, offset - value.length(),
                        value.length());

                completions.add(proposal);
            }
        }

        return completions.toArray(new ICompletionProposal[completions.size()]);
    }

    private ICompletionProposal[] handleMIDletCompletion(String value,
            int offset) {
        String[] tokens = value.split(",", -1);
        ArrayList<JADAttributeCompletionProposal> completions = new ArrayList<JADAttributeCompletionProposal>();

        // MIDlet class completion propose
        if ((tokens != null) && (tokens.length == 3)) {
            IJavaProject jp = getJavaProject();
            List<IType> midlets = Utils.getMidletTypesInProject(
                    new NullProgressMonitor(), jp);
            Iterator<IType> it = midlets.iterator();
            while (it.hasNext()) {
                IType midlet = it.next();
                String elementName = midlet.getElementName();
                String fullName = midlet.getFullyQualifiedName();
                if (elementName.regionMatches(true, 0, tokens[2], 0, tokens[2]
                        .length())) {
                    JADAttributeCompletionProposal proposal = new JADAttributeCompletionProposal(
                            fullName, MTJUIPluginImages.DESC_MIDLET_ICON
                                    .createImage(), elementName + " - "
                                    + fullName, offset - tokens[2].length(),
                            tokens[2].length());

                    completions.add(proposal);
                }
            }
            return completions.toArray(new ICompletionProposal[completions
                    .size()]);
        }
        return NO_COMPLETIONS;
    }

    private ICompletionProposal[] handleProfileCompletion(String value,
            int offset) {

        ArrayList<JADAttributeCompletionProposal> completions = new ArrayList<JADAttributeCompletionProposal>();

        IAPI[] specifications = Profile.values();
        value = value.trim();
        for (IAPI spec : specifications) {
            String profileID = spec.toString();
            if (profileID.regionMatches(true, 0, value, 0, value.length())) {
                JADAttributeCompletionProposal proposal = new JADAttributeCompletionProposal(
                        profileID, null, profileID, offset - value.length(),
                        value.length());

                completions.add(proposal);
            }
        }

        return completions.toArray(new ICompletionProposal[completions.size()]);
    }

    private boolean isHeader(String currentLineValue) {
        return currentLineValue.indexOf(':') == -1;

    }

    private void parseDocument(IDocument doc) {
        existedPropertis = new ColonDelimitedProperties();
        try {
            existedPropertis.load(new StringReader(doc.get()));
        } catch (IOException e) {
        }
        Iterator<?> iter = existedPropertis.keySet().iterator();
        int currMaxMidletNumber = 0;
        while (iter.hasNext()) {
            String key = (String) iter.next();
            if (key.startsWith(MIDLET_PREFIX)) {
                int midletNumber = -1;
                try {
                    midletNumber = Integer.parseInt(key.substring(MIDLET_PREFIX
                            .length()));
                } catch (NumberFormatException e) {
                }

                if ((midletNumber != -1)
                        && (midletNumber > currMaxMidletNumber)) {
                    currMaxMidletNumber = midletNumber;
                }
            }
        }
        midletHeader = MIDLET_PREFIX + (currMaxMidletNumber + 1);
    }

    private ICompletionProposal[] sortProposals(ICompletionProposal[] proposals) {
        Arrays.sort(proposals, new Comparator<ICompletionProposal>() {

            public int compare(ICompletionProposal o1, ICompletionProposal o2) {
                return o1.getDisplayString().compareTo(o2.getDisplayString());
            }

        });
        return proposals;
    }
}
