/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version                     
 */
package org.eclipse.mtj.internal.jmunit.ui.actions;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
    private static final String BUNDLE_NAME = "org.eclipse.mtj.internal.jmunit.ui.actions.messages"; //$NON-NLS-1$
    public static String UpdateJMUnitTestSuiteAction_ErrorUpdatingTestSuite;
    public static String UpdateJMUnitTestSuiteAction_MarkersNotFound;
    public static String UpdateJMUnitTestSuiteAction_UpdateTestSuite;
    public static String UpdateJMUnitTestSuiteAction_UpdateTestSuiteTests;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
