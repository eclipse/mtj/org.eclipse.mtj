/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Gorkem Ercan (Nokia) - enum definition mixed with profile
 */
package org.eclipse.mtj.internal.core.sdk.device.midp;

import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType;
import org.osgi.framework.Version;

/**
 * @author Diego Madruga Sandin
 */
public enum Configuration implements IMIDPAPI {

    /**
     * Connected Limited Device Configuration (1.0)
     */
    CLDC_10("CLDC", "Connected Limited Device Configuration (1.0)", //$NON-NLS-1$//$NON-NLS-2$
            new Version("1.0")), //$NON-NLS-1$

    /**
     * Connected Limited Device Configuration (1.1)
     */
    CLDC_11("CLDC", "Connected Limited Device Configuration (1.1)", //$NON-NLS-1$//$NON-NLS-2$
            new Version("1.1")); //$NON-NLS-1$

    private String identifier;
    private String name;
    private Version version;

    /**
     * @param identifier the profile identifier
     * @param name the profile name
     * @param version the profile version
     */
    Configuration(String identifier, String name, Version version) {
        this.identifier = identifier;
        this.name = name;
        this.version = version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#getIdentifier()
     */
    public String getIdentifier() {
        return identifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#getVersion()
     */
    public Version getVersion() {
        return version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI#getType()
     */
    public MIDPAPIType getType() {
        return MIDPAPIType.CONFIGURATION;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#setIdentifier(java.lang.String)
     */
    public void setIdentifier(String identifier) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#setName(java.lang.String)
     */
    public void setName(String name) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#setType(org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType)
     */
    public void setType(MIDPAPIType type) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#setVersion(org.osgi.framework.Version)
     */
    public void setVersion(Version version) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return getIdentifier() + "-" + getVersion().getMajor() + "."
                + getVersion().getMinor();
    }
}
