/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.sign;

/**
 * KeyStoreManagerException class wraps all exceptions
 * raised by an {@link IKeyStoreManager} implementation.
 * 
 * @author David Marques
 * @since 1.0
 */
public class KeyStoreManagerException extends Exception {

	private static final long serialVersionUID = -4296926260989785233L;

	/**
	 * Creates a KeyStoreManagerException instance
	 * associated to the specified message.
	 * 
	 * @param message error message.
	 */
	public KeyStoreManagerException(String message) {
		super(message);
	}

	/**
	 * Creates a KeyStoreManagerException instance
	 * associated to the specified {@link Throwable}.
	 * 
	 * @param message error root cause.
	 */
	public KeyStoreManagerException(Throwable cause) {
		super(cause);
	}
}
