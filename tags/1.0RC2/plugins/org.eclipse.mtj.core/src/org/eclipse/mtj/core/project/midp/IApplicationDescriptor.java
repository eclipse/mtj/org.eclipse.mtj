/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.project.midp;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.osgi.framework.Version;

/**
 * This in interface is a representation of a Java Application Descriptor (jad)
 * for a MIDlet Suite.
 * <p>
 * Each JAR file <strong>MAY</strong> be accompanied by an application
 * descriptor. The application descriptor is used in conjunction with the JAR
 * manifest by the application management software to manage the MIDlet and is
 * used by the MIDlet itself for configuration specific attributes. The
 * descriptor allows the application management software on the device to verify
 * that the MIDlet is suited to the device before loading the full JAR file of
 * the MIDlet suite. It also allows configuration-specific attributes
 * (parameters) to be supplied to the MIDlet(s) without modifying the JAR file.
 * </p>
 * <p>
 * A predefined set of attributes is specified to allow the application
 * management software to identify, retrieve, and install the MIDlet(s). All
 * attributes appearing in the descriptor file are made available to the
 * MIDlet(s). The developer may use attributes not beginning with
 * <code>MIDlet-</code> or <code>MicroEdition-</code> for application-specific
 * purposes. Attribute names are case-sensitive and <strong>MUST</strong> match
 * exactly. An attribute <strong>MUST NOT</strong> appear more than once within
 * the manifest. If an attribute is duplicated the effect is unspecified. The
 * MIDlet retrieves attributes by name by calling the
 * <code>MIDlet.getAppProperty</code> method. The application descriptor
 * <strong>MUST</strong> contain the following attributes:
 * <ul>
 * <li>MIDlet-Name</li>
 * <li>MIDlet-Version</li>
 * <li>MIDlet-Vendor</li>
 * <li>MIDlet-Jar-URL</li>
 * <li>MIDlet-Jar-Size</li>
 * </ul>
 * The application descriptor <strong>MAY</strong> contain:
 * <ul>
 * <li>MIDlet-&lt;n&gt; for each MIDlet</li>
 * <li>MicroEdition-Profile</li>
 * <li>MicroEdition-Configuration</li>
 * <li>MIDlet-Description</li>
 * <li>MIDlet-Icon</li>
 * <li>MIDlet-Info-URL</li>
 * <li>MIDlet-Data-Size</li>
 * <li>MIDlet-Permissions</li>
 * <li>MIDlet-Permissions-Opt</li>
 * <li>MIDlet-Push-&lt;n&gt;</li>
 * <li>MIDlet-Install-Notify</li>
 * <li>MIDlet-Delete-Notify</li>
 * <li>MIDlet-Delete-Confirm</li>
 * <li>Any application-specific attributes that do not begin with
 * <code>MIDlet-</code> or <code>MicroEdition-</code></li>
 * </ul>
 * The mandatory attributes MIDlet-Name, MIDlet-Version, and MIDlet-Vendor MUST
 * be duplicated in the descriptor and manifest files since they uniquely
 * identify the application. If they are not identical (not from the same
 * application), then the JAR <strong>MUST NOT</strong> be installed.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * 
 * @since 1.0
 */
public interface IApplicationDescriptor {

    /**
     * Add a new MidletDefinition instance to list of published MIDLets.
     * 
     * @param midletDefinition the MIDlet definition to be added
     */
    public abstract void addMidletDefinition(IMidletDefinition midletDefinition);

    /**
     * Return the configuration specification version associated with this JAD
     * file.
     * 
     * @return
     * @throws CoreException
     */
    public abstract Version getConfigurationSpecificationVersion()
            throws CoreException;

    /**
     * Return the overall manifest properties.
     * 
     * @return the manifest properties.
     */
    public abstract Properties getManifestProperties();

    /**
     * Return the Java ME Configuration required using the same format and value
     * as the System property <code>microedition.profiles</code> (for example
     * "CLDC-1.0").
     * <p>
     * This value is retrieved reading the
     * <code>MicroEdition-Configuration</code> attribute from the application
     * descriptor.
     * </p>
     * 
     * @return a string containing a blank separated list of required Java ME
     *         configurations.
     */
    public abstract String getMicroEditionConfiguration();

    /**
     * Return the Java ME profiles required, using the same format and value as
     * the System property <code>microedition.profiles</code> (for example
     * "MIDP-2.0").
     * <p>
     * This value is retrieved reading the <code>MicroEdition-Profile</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @return a string containing a blank separated list of required Java ME
     *         profiles.
     */
    public abstract String getMicroEditionProfile();

    /**
     * Return the current count of MidletDefinition instances within this
     * application descriptor.
     * 
     * @return the number of MidletDefinition instances
     */
    public abstract int getMidletCount();

    /**
     * Return the minimum number of bytes of persistent data required by the
     * MIDlet.
     * <p>
     * This value is retrieved reading the <code>MIDlet-Jar-Size</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @return the minimum number of bytes of persistent data required by the
     *         MIDlet or <code>null</code> if this attribute has not been
     *         specified.
     */
    public abstract String getMIDletDataSize();

    /**
     * Return the list of MidletDefinition instances currently managed by the
     * ApplicationDescriptor.
     * 
     * @return a list of MidletDefinition instances
     */
    public abstract List<IMidletDefinition> getMidletDefinitions();

    /**
     * Return the text message to be provided to the user when prompted to
     * confirm deletion of this MIDlet suite.
     * <p>
     * This value is retrieved reading the <code>MIDlet-Delete-Confirm</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @return text message to be provided to the user when prompted to confirm
     *         deletion of this MIDlet suite or <code>null</code> if this
     *         attribute has not been specified.
     */
    public abstract String getMIDletDeleteConfirm();

    /**
     * Return the URL to which a POST request is sent to report the deletion of
     * this MIDlet suite.
     * <p>
     * This value is retrieved reading the <code>MIDlet-Delete-Notify</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @return the URL to which a POST request is sent to report the deletion of
     *         this MIDlet suite or <code>null</code> if this attribute has not
     *         been specified.
     */
    public abstract String getMIDletDeleteNotify();

    /**
     * Return the description of the MIDlet suite.
     * <p>
     * This value is retrieved reading the <code>MIDlet-Description</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @return the description of the MIDlet suite or <code>null</code> if this
     *         attribute has not been specified.
     */
    public abstract String getMIDletDescription();

    /**
     * Return the case-sensitive absolute name of a PNG file within the JAR used
     * to represent the MIDlet suite. It <strong>SHOULD</strong> be used when
     * the Application Management Software displays an icon to identify the
     * suite.
     * <p>
     * This value is retrieved reading the <code>MIDlet-Icon</code> attribute
     * from the application descriptor.
     * </p>
     * 
     * @return the case-sensitive absolute name of a PNG file within the JAR
     *         used to represent the MIDlet suite or <code>null</code> if this
     *         attribute has not been specified.
     */
    public abstract String getMIDletIcon();

    /**
     * Return a URL for information further describing the MIDlet suite.
     * <p>
     * The syntax and meaning of this URL conform to RFC2396 and RFCs that
     * define each scheme.
     * </p>
     * <p>
     * <strong>NOTE:</strong> This method does not check the validity of the
     * value.
     * </p>
     * <p>
     * This value is retrieved reading the <code>MIDlet-Info-URL</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @return a URL for information further describing the MIDlet suite or
     *         <code>null</code> if this attribute has not been specified.
     */
    public abstract String getMIDletInfoURL();

    /**
     * Return the URL to which a POST request is sent to report the installation
     * status (whether a new installation or MIDlet suite update) of this MIDlet
     * suite.
     * <p>
     * This value is retrieved reading the <code>MIDlet-Install-Notify</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @return the URL to which a POST request is sent to report the
     *         installation status (whether a new installation or MIDlet suite
     *         update) of this MIDlet suite or <code>null</code> if this
     *         attribute has not been specified.
     */
    public abstract String getMIDletInstallNotify();

    /**
     * Return the number of bytes in the JAR file as a string.
     * <p>
     * This value is retrieved reading the <code>MIDlet-Jar-Size</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @return the number of bytes in the JAR file as a string or
     *         <code>null</code> if this attribute has not been specified.
     */
    public abstract String getMIDletJarSize();

    /**
     * Return the URL from which the JAR file can be loaded.
     * <p>
     * The syntax and meaning of this URL conform to RFC2396 and RFCs that
     * define each scheme. Both absolute and relative URLs are supported. The
     * context for a relative URL is the URL from which this application
     * descriptor was loaded.
     * </p>
     * <p>
     * <strong>NOTE:</strong> This method does not check the validity of the
     * value.
     * </p>
     * <p>
     * This value is retrieved reading the <code>MIDlet-Jar-URL</code> attribute
     * from the application descriptor.
     * </p>
     * 
     * @return the URL from which the JAR file can be loaded or
     *         <code>null</code> if this attribute has not been specified.
     */
    public abstract String getMIDletJarURL();

    /**
     * Return the name of the MIDlet suite that identifies the MIDlets to the
     * user.
     * <p>
     * This value is retrieved reading the <code>MIDlet-Name</code> attribute
     * from the application descriptor.
     * </p>
     * 
     * @return the name of the MIDlet suite that identifies the MIDlets to the
     *         user or <code>null</code> if this attribute has not been
     *         specified.
     */
    public abstract String getMIDletName();

    /**
     * Return the permissions that are critical to the function of the MIDlet
     * suite.
     * <p>
     * This value is retrieved reading the <code>MIDlet-Permissions</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @return a string containing a comma separated list of permissions that
     *         are critical to the function of the MIDlet suite or
     *         <code>null</code> if this attribute has not been specified.
     */
    public abstract String getMIDletPermissions();

    /**
     * Return the permissions that are non-critical to the function of the
     * MIDlet suite.
     * <p>
     * This value is retrieved reading the <code>MIDlet-Permissions-Opt</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @return a string containing a comma separated list of permissions that
     *         are non-critical to the function of the MIDlet suite or
     *         <code>null</code> if this attribute has not been specified.
     */
    public abstract String getMIDletPermissionsOpt();

    /**
     * Return the name of the organization that provides the MIDlet suite.
     * <p>
     * This value is retrieved reading the <code>MIDlet-Vendor</code> attribute
     * from the application descriptor.
     * </p>
     * 
     * @return the name of the organization that provides the MIDlet suite or
     *         <code>null</code> if this attribute has not been specified.
     */
    public abstract String getMIDletVendor();

    /**
     * Return the version number of the MIDlet suite. Version numbers are
     * formatted so they can be used by the application management software for
     * install and upgrade uses, as well as communication with the user.
     * <p>
     * A missing <code>MIDlet-Version</code> tag is assumed to be
     * <code>0.0.0</code>, which means that any non-zero version number is
     * considered as a newer version of the MIDlet suite.
     * </p>
     * <p>
     * This value is retrieved reading the <code>MIDlet-Version</code> attribute
     * from the application descriptor.
     * </p>
     * 
     * @return the version number of the MIDlet suite.
     */
    public abstract Version getMIDletVersion();

    /**
     * Set the Java ME Configuration required using the same format and value as
     * the System property <code>microedition.profiles</code> (for example
     * "CLDC-1.0").
     * <p>
     * This value will be written to the <code>MicroEdition-Configuration</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @param configurations a string containing a blank separated list of
     *            required Java ME configurations.
     */
    public abstract void setMicroEditionConfiguration(String configurations);

    /**
     * Set the Java ME profiles required, using the same format and value as the
     * System property <code>microedition.profiles</code> (for example
     * "MIDP-2.0").
     * <p>
     * This value will be written to the <code>MicroEdition-Profile</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @param profiles a string containing a blank separated list of required
     *            Java ME profiles.
     */
    public abstract void setMicroEditionProfile(String profiles);

    /**
     * Set the minimum number of bytes of persistent data required by the
     * MIDlet.
     * <p>
     * This will be written to the <code>MIDlet-Jar-Size</code> attribute from
     * the application descriptor.
     * </p>
     * 
     * @param datasize the minimum number of bytes of persistent data required
     *            by the MIDlet.
     */
    public abstract void setMIDletDataSize(String datasize);

    /**
     * Set the text message to be provided to the user when prompted to confirm
     * deletion of this MIDlet suite.
     * <p>
     * This value will be written to the <code>MIDlet-Delete-Confirm</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @param message text message to be provided to the user when prompted to
     *            confirm deletion of this MIDlet suite.
     */
    public abstract void setMIDletDeleteConfirm(String message);

    /**
     * Set the URL to which a POST request is sent to report the deletion of
     * this MIDlet suite.
     * <p>
     * <strong>NOTE:</strong> The URL MUST be no longer than 256 UTF-8 encoded
     * characters.
     * </p>
     * <p>
     * This value will be written to the <code>MIDlet-Delete-Notify</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @param url the URL to which a POST request is sent to report the deletion
     *            of this MIDlet suite.
     */
    public abstract void setMIDletDeleteNotify(String url);

    /**
     * Set the description of the MIDlet suite.
     * <p>
     * This value will be written to the <code>MIDlet-Description</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @param description the description of the MIDlet suite.
     */
    public abstract void setMIDletDescription(String description);

    /**
     * Set the case-sensitive absolute name of a PNG file within the JAR used to
     * represent the MIDlet suite. It <strong>SHOULD</strong> be used when the
     * Application Management Software displays an icon to identify the suite.
     * <p>
     * This value will be written to the <code>MIDlet-Icon</code> attribute from
     * the application descriptor.
     * </p>
     * 
     * @param icon the case-sensitive absolute name of a PNG file within the JAR
     *            used to represent the MIDlet suite.
     */
    public abstract void setMIDletIcon(String icon);

    /**
     * Set a URL for information further describing the MIDlet suite.
     * <p>
     * The syntax and meaning of this URL MUST conform to RFC2396 and RFCs that
     * define each scheme.
     * </p>
     * <p>
     * <strong>NOTE:</strong> This method does not check the validity of the
     * value.
     * </p>
     * <p>
     * This value will be written to the <code>MIDlet-Info-URL</code> attribute
     * from the application descriptor.
     * </p>
     * 
     * @param url a URL for information further describing the MIDlet suite.
     */
    public abstract void setMIDletInfoURL(String url);

    /**
     * Set the URL to which a POST request is sent to report the installation
     * status (whether a new installation or MIDlet suite update) of this MIDlet
     * suite.
     * <p>
     * <strong>NOTE:</strong> The URL MUST be no longer than 256 UTF-8 encoded
     * characters.
     * </p>
     * <p>
     * This value will be written to the <code>MIDlet-Install-Notify</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @param url the URL to which a POST request is sent to report the
     *            installation status (whether a new installation or MIDlet
     *            suite update) of this MIDlet suite.
     */
    public abstract void setMIDletInstallNotify(String url);

    /**
     * Set the number of bytes in the JAR file as a string.
     * <p>
     * This value will be written to the <code>MIDlet-Jar-Size</code> attribute
     * from the application descriptor.
     * </p>
     * 
     * @param jarsize the number of bytes in the JAR file as a string.
     */
    public abstract void setMIDletJarSize(String jarsize);

    /**
     * Set the URL from which the JAR file can be loaded.
     * <p>
     * The syntax and meaning of this URL MUST conform to RFC2396 and RFCs that
     * define each scheme. Both absolute and relative URLs are supported. The
     * context for a relative URL is the URL from which this application
     * descriptor was loaded.
     * </p>
     * <p>
     * <strong>NOTE:</strong> This method does not check the validity of the
     * value.
     * </p>
     * <p>
     * This value will be written to the <code>MIDlet-Jar-URL</code> attribute
     * from the application descriptor.
     * </p>
     * 
     * @param url the URL from which the JAR file can be loaded.
     */
    public abstract void setMIDletJarURL(String url);

    /**
     * Set the name of the MIDlet suite that identifies the MIDlets to the user.
     * <p>
     * This value will be written to the <code>MIDlet-Name</code> attribute from
     * the application descriptor.
     * </p>
     * 
     * @return the name of the MIDlet suite that identifies the MIDlets to the
     *         user or <code>null</code> if this attribute has not been
     *         specified.
     */
    public abstract void setMIDletName(String name);

    /**
     * Set the permissions that are critical to the function of the MIDlet
     * suite.
     * <p>
     * This value will be written to the <code>MIDlet-Permissions</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @param permissions a string containing a comma separated list of
     *            permissions that are critical to the function of the MIDlet
     *            suite.
     */
    public abstract void setMIDletPermissions(String permissions);

    /**
     * Set the permissions that are non-critical to the function of the MIDlet
     * suite.
     * <p>
     * This value will be written to the <code>MIDlet-Permissions-Opt</code>
     * attribute from the application descriptor.
     * </p>
     * 
     * @param permissions a string containing a comma separated list of
     *            permissions that are non-critical to the function of the
     *            MIDlet suite.
     */
    public abstract void setMIDletPermissionsOpt(String permissions);

    /**
     * Set the name of the organization that provides the MIDlet suite.
     * <p>
     * This value will be written to the <code>MIDlet-Vendor</code> attribute
     * from the application descriptor.
     * </p>
     * 
     * @param vendor the name of the organization that provides the MIDlet
     *            suite.
     */
    public abstract void setMIDletVendor(String vendor);

    /**
     * Set the version number of the MIDlet suite. Version numbers are formatted
     * so they can be used by the application management software for install
     * and upgrade uses, as well as communication with the user.
     * <p>
     * This value will be written to the <code>MIDlet-Version</code> attribute
     * from the application descriptor.
     * </p>
     * 
     * @param version the version number of the MIDlet suite.
     */
    public abstract void setMIDletVersion(Version version);

    /**
     * Store the ApplicationDescriptor instance into the same File from which it
     * was originally read.
     * 
     * @throws IOException when an error occurs while storing the descriptor
     */
    public abstract void store() throws IOException;

    /**
     * Store the ApplicationDescriptor instance into the specified file.
     * 
     * @param jadFile the file into which the descriptor will be written
     * @throws IOException when an error occurs while storing the descriptor
     */
    public abstract void store(File jadFile) throws IOException;
}