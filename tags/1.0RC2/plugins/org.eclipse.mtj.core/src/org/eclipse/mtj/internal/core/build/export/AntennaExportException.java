/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID.
 */
package org.eclipse.mtj.internal.core.build.export;

/**
 * An exception subclass that may be throw in the course of doing the export.
 * The exception text in this case is meant to be displayed to the user.
 * 
 * @author Craig Setera
 */
public class AntennaExportException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new instance.
     * 
     * @param message
     */
    public AntennaExportException(String message) {
        super(message);
    }
}
