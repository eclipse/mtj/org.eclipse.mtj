/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Included new constants and enhanced javadoc 
 *                                [bug 270532]                 
 */
package org.eclipse.mtj.core.persistence;

import java.util.Properties;

/**
 * Implementors of this interface must be capable of saving and restoring their
 * own state.
 * <p>
 * Clients should always implement this interface for elements in which their
 * state should always be persisted by MTJ persistence solution.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface IPersistable {

    /**
     * The persistable attribute for storing the class identifier.
     */
    public static final String CLASS_PERSISTABLE_ATTRIBUTE = "class"; //$NON-NLS-1$

    /**
     * The persistable attribute for storing the id of persistable elements.
     */
    public static final String ID_PERSISTABLE_ATTRIBUTE = "id"; //$NON-NLS-1$

    /**
     * The persistable attribute for storing the key of persistable elements.
     */
    public static final String KEY_PERSISTABLE_ATTRIBUTE = "key"; //$NON-NLS-1$

    /**
     * The persistable element name for storing {@link Properties} elements.
     */
    public static final String PROPERTY_PERSISTABLE_ELEMENT = "property"; //$NON-NLS-1$

    /**
     * The persistable attribute for storing the value of persistable elements.
     */
    public static final String VALUE_PERSISTABLE_ATTRIBUTE = "value"; //$NON-NLS-1$

    /**
     * The persistable attribute for storing a reference to another persistable
     * element.
     */
    public static final String REFID_PERSISTABLE_ATTRIBUTE = "refid"; //$NON-NLS-1$

    /**
     * Load the state of this object using the specified persistence state
     * information.
     * 
     * @param persistenceProvider the {@link IPersistenceProvider}
     *            implementation that provides the facilities for storing and
     *            retrieving persistable objects.
     * @throws PersistenceException if any error occur while loading the
     *             persisted information.
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException;

    /**
     * Save the state of this object using the specified persistence state
     * information.
     * 
     * @param persistenceProvider the {@link IPersistenceProvider}
     *            implementation that provides the facilities for storing and
     *            retrieving persistable objects.
     * @throws PersistenceException if any error occur while saving the
     *             persistable information.
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException;
}
