/**
 * Copyright (c) 2005,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Kevin Hunter             - Initial implementation
 *     Craig Setera (EclipseME) - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     David Marques (Motorola) - Adding already resolved passwords to 
 *                                MTJCore.statusPrompt method call.
 *     David Marques (Motorola) - Generating error logs instead of null pointers.
 */
package org.eclipse.mtj.internal.core.build.sign;

import java.io.FileInputStream;
import java.io.IOException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.project.midp.IJadSignature;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * This class is used to construct IJadSignature objects.
 * 
 * @author Kevin Hunter
 */
public class SignatureUtils {
    /**
     * Obtain a signature object for the specified IMidletSuiteProject. The
     * signature properties used will be those found in the project's persistant
     * properties.
     * 
     * @param project
     * @return
     * @throws CoreException
     */
    public static IJadSignature getSignatureObject(IMidletSuiteProject project)
            throws CoreException {
        ISignatureProperties sigProps = project.getSignatureProperties();

        return (getSignatureObject(project, sigProps));
    }

    /**
     * @param project
     * @param sigProps
     * @return
     * @throws CoreException
     */
    public static IJadSignature getSignatureObject(IMidletSuiteProject project,
            ISignatureProperties sigProps) throws CoreException {
        if (!sigProps.isSignProject()) {
            return (null);
        }

        if (sigProps.getAbsoluteKeyStorePath(project.getProject()) == null) {
			MTJLogger.log(IStatus.ERROR, Messages.SignatureUtils_noKeystoreConfigured);
			return null;
		}
        
        if (sigProps.getKeyAlias() == null) {
			MTJLogger.log(IStatus.ERROR, Messages.SignatureUtils_noAliasSelected);
			return null;
		}
        
        JadSignature sigObject = null;
        FileInputStream fis = null;
        String strKeyStorePass = null;
        String strKeyPass = null;

        switch (sigProps.getPasswordStorageMethod()) {
            case ISignatureProperties.PASSMETHOD_IN_KEYRING:
            case ISignatureProperties.PASSMETHOD_IN_PROJECT:
                strKeyStorePass = sigProps.getKeyStorePassword();
                strKeyPass = sigProps.getKeyPassword();
                break;

            case ISignatureProperties.PASSMETHOD_PROMPT:
            default:
                if (project != null) {
                    strKeyStorePass = project.getTempKeystorePassword();
                    strKeyPass = project.getTempKeyPassword();
                }
                break;
        }

        boolean bPromptedForPasswords = false;

        if (strKeyStorePass == null || strKeyPass == null) {
            IStatus status = new Status(IStatus.INFO,
                    IMTJCoreConstants.PLUGIN_ID,
                    IMTJCoreConstants.INFO_NEED_SIGNATURE_PASSWORDS,
                    Messages.SignatureUtils_passwordDialogTitle, null);

            // Passes the passwords to the prompt so in case the keystore
            // password is already resolved it can be displayed and the user
            // will need to fill in only the keypair's password.
            SignaturePasswords passwords = (SignaturePasswords) MTJStatusHandler
                    .statusPrompt(status, new Object[] {project.getProject(), strKeyStorePass, strKeyPass});

            if (passwords == null) {
                return (null);
            }

            strKeyStorePass = passwords.getKeystorePassword();
            strKeyPass = passwords.getKeyPassword();
            bPromptedForPasswords = true;
        }

        try {
            fis = new FileInputStream(sigProps.getAbsoluteKeyStorePath(project
                    .getProject()));
            KeyChainSet set = KeyChainSet.getInstance(fis, sigProps
                    .getKeyStoreType(), sigProps.getKeyStoreProvider(),
                    strKeyStorePass, sigProps.getKeyAlias(), strKeyPass);
            sigObject = new JadSignature(set);
            sigObject.checkKeyChainSet();

            if (bPromptedForPasswords) {
                switch (sigProps.getPasswordStorageMethod()) {
                    case ISignatureProperties.PASSMETHOD_IN_KEYRING:
                    case ISignatureProperties.PASSMETHOD_IN_PROJECT:
                        sigProps.setKeyPassword(strKeyPass);
                        sigProps.setKeyStorePassword(strKeyStorePass);
                        if (project != null) {
                            project.saveMetaData();
                        }
                        break;

                    case ISignatureProperties.PASSMETHOD_PROMPT:
                    default:
                        if (project != null) {
                            project.setTempKeystorePassword(strKeyStorePass);
                            project.setTempKeyPassword(strKeyPass);
                        }
                        break;
                }
            }

        } catch (IOException ioe) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, 99999, ioe);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e) {
                }
            }
        }

        return (sigObject);
    }
}