/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.sdk.device.midp;

/**
 * JAR files containing emulator APIs in the lib directory should provide
 * additional information to IDEs by providing manifest files with the
 * attributes contained in this enum.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @author Diego Madruga Sandin
 * @since 1.0
 */
public enum UEIAPIManifestAttributes {

    /**
     * Code name for the API. This name is only used internally and has no
     * significance beyond its use for calculating API dependencies.
     */
    API("API"), //$NON-NLS-1$

    /**
     * External name for the API. This text can be shown to developers.
     */
    API_NAME("API-Name"), //$NON-NLS-1$

    /**
     * Use one of the following values for this attribute.
     * <ul>
     * <li>{@link APIType#CONFIGURATION Configuration} - The API is a
     * configuration such as CLDC.</li>
     * <li>{@link APIType#PROFILE Profile} - The API is a profile such as MIDP
     * or PDAP.</li>
     * <li>{@link APIType#OPTIONAL Optional} - The API is an optional API such
     * as MMAPI or WMA.</li>
     * </ul>
     */
    API_TYPE("API-Type"), //$NON-NLS-1$

    /**
     * A comma-separated list of APIs that are required by the API contained in
     * this JAR. Each API dependency can contain just the code name of the API,
     * or can additionally contain <code>= version-number</code> to require that
     * a specific version be installed, or <code>&gt;= version-number</code> to
     * specify that at least a certain version be installed.
     */
    API_DEPENDENCIES("API-Dependencies"), //$NON-NLS-1$

    /**
     * The version number of the specification that is implemented by this JAR
     * file.
     */
    API_VERSION("API-Specification-Version"); //$NON-NLS-1$

    /**
     * The attribute name
     */
    private final String attribute;

    /**
     * Creates a new APIManifestEntries
     * 
     * @param attribute the attribute name
     */
    UEIAPIManifestAttributes(final String attribute) {
        this.attribute = attribute;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return attribute;
    }
}
