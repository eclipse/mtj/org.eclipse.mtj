/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.util.xml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * A class wrapper for a {@link SAXParser} instance.
 * 
 * @since 0.9.1
 */
public class SAXParserWrapper {

    protected SAXParser fParser;
    protected boolean isdisposed;

    /**
     * Creates a new SAXParserWrapper.
     * 
     * @throws ParserConfigurationException if a parser cannot be created which
     *             satisfies the requested configuration.
     * @throws SAXException for SAX errors.
     * @throws FactoryConfigurationError if MTJXMLHelper fails to create
     *             factories instances.
     */
    public SAXParserWrapper() throws ParserConfigurationException, SAXException {
        fParser = MTJXMLHelper.Instance().getDefaultSAXParser();
        isdisposed = false;
    }

    /**
     * Explicit disposal
     */
    public void dispose() {
        if (isdisposed == false) {
            MTJXMLHelper.Instance().recycleSAXParser(fParser);
            isdisposed = true;
        }
    }

    /**
     * Parse the content of the file specified as XML using the specified
     * {@link DefaultHandler}.
     * 
     * @param f the file containing the XML to parse.
     * @param dh the SAX DefaultHandler to use.
     * @throws SAXException If any SAX errors occur during processing.
     * @throws IOException If any IO errors occur.
     * @throws IllegalArgumentException If the File object is <code>null</code>.
     */
    public void parse(File f, DefaultHandler dh) throws SAXException,
            IOException, IllegalArgumentException {
        fParser.parse(f, dh);
    }

    /**
     * Parse the content given {@link InputSource} as XML using the specified
     * {@link DefaultHandler}.
     * 
     * @param is the InputSource containing the XML to parse.
     * @param dh the SAX DefaultHandler to use.
     * @throws SAXException If any SAX errors occur during processing.
     * @throws IOException If any IO errors occur.
     * @throws IllegalArgumentException If the <code>InputSource</code> object
     *             is <code>null</code>.
     */
    public void parse(InputSource is, DefaultHandler dh) throws SAXException,
            IOException {
        fParser.parse(is, dh);
    }

    /**
     * Parse the content of the given {@link InputStream} instance as XML using
     * the specified {@link DefaultHandler}.
     * 
     * @param is InputStream containing the content to be parsed.
     * @param dh The SAX DefaultHandler to use.
     * @throws IllegalArgumentException If the given InputStream is null.
     * @throws IOException If any IO errors occur.
     * @throws SAXException If any SAX errors occur during processing.
     */
    public void parse(InputStream is, DefaultHandler dh) throws SAXException,
            IOException {
        fParser.parse(is, dh);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#finalize()
     */
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        dispose();
    }
}
