/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards, renamed Launch Constants values
 *                                from eclipseme.* to mtj.*
 *     Feng Wang (Sybase) - Remove constant EMULATED_CLASS, use 
 *                          IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME
 *                          instead, we do this to take advantage of JDT launch
 *                          configuration refactoring participates.
 */
package org.eclipse.mtj.internal.core.launching;

/**
 * Constant definitions for launching support.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface ILaunchConstants {

    /**
     * The launch configuration type for the wireless emulator
     */
    public static final String LAUNCH_CONFIG_TYPE = "MTJ.emulatorLaunchConfigurationType"; //$NON-NLS-1$

    /**
     * The emulated classpath for the emulated class
     */
    public static final String EMULATED_CLASSPATH = "mtj.emulated_classpath"; //$NON-NLS-1$

    /**
     * The verbosity options
     */
    public static final String VERBOSITY_OPTIONS = "mtj.verbosity_options"; //$NON-NLS-1$

    /**
     * Whether or not to use the project's device for emulation
     */
    public static final String USE_PROJECT_DEVICE = "mtj.use_project_device"; //$NON-NLS-1$

    /** The device to be emulated */
    public static final String EMULATED_DEVICE = "mtj.emulated_device"; //$NON-NLS-1$

    /**
     * The name of the group for the device to be emulated
     */
    public static final String EMULATED_DEVICE_GROUP = "mtj.emulated_device_group"; //$NON-NLS-1$

    /**
     * Extra device launch parameters
     */
    public static final String LAUNCH_PARAMS = "mtj.launch_params"; //$NON-NLS-1$
}
