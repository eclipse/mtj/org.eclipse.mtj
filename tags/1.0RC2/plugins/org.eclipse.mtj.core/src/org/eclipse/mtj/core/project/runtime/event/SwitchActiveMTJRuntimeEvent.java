/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)          - Initial implementation
 *     Gustavo de Paula (Motorola) - Configuration refactory [Bug 265522]
 *     Diego Sandin (Motorola)     - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.project.runtime.event;

import java.util.EventObject;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;

/**
 * SwitchActiveMTJRuntimeEvent is used to notify that active MTJRuntime for a
 * MTJProject switched.
 * <p>
 * All Events are constructed with a reference to the object, the "source", that
 * is logically deemed to be the object upon which the Event in question
 * initially occurred upon.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 * 
 * @since 1.0
 */
public class SwitchActiveMTJRuntimeEvent extends EventObject {

    /**
     * The default serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The previously active runtime.
     */
    private MTJRuntime oldActiveRuntime;

    /**
     * The newly activated runtime.
     */
    private MTJRuntime newActiveRuntime;

    /**
     * Creates a new instance of SwitchActiveMTJRuntimeEvent.
     * 
     * @param source the MTJRuntimeList in which the active MTJRuntime has been
     *            switched.
     * @param oldActiveConfig the previous active runtime.
     * @param newActiveConfig the newly activated runtime.
     */
    public SwitchActiveMTJRuntimeEvent(MTJRuntimeList source,
            MTJRuntime oldActiveConfig, MTJRuntime newActiveConfig) {
        super(source);
        this.oldActiveRuntime = oldActiveConfig;
        this.newActiveRuntime = newActiveConfig;
    }

    /**
     * Return the newly activated runtime.
     * 
     * @return the newly activated runtime.
     */
    public MTJRuntime getNewActiveMTJRuntime() {
        return newActiveRuntime;
    }

    /**
     * Return the previous active runtime.
     * 
     * @return the previous active runtime.
     */
    public MTJRuntime getOldActiveMTJRuntime() {
        return oldActiveRuntime;
    }

    /**
     * Set the newly activated runtime.
     * 
     * @param newActiveRuntime the newly activated runtime.
     */
    public void setNewActiveMTJRuntime(MTJRuntime newActiveRuntime) {
        this.newActiveRuntime = newActiveRuntime;
    }

    /**
     * Set the previous active runtime.
     * 
     * @param oldActiveRuntime the previous active runtime.
     */
    public void setOldActiveMTJRuntime(MTJRuntime oldActiveRuntime) {
        this.oldActiveRuntime = oldActiveRuntime;
    }

}
