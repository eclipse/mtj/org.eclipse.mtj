/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.project.midp;

/**
 * This class represents an application descriptor property description.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * 
 * @since 1.0
 */
public class DescriptorPropertyDescription {

    /** Integer property type */
    public static final int DATATYPE_INT = 3;

    /** List property type */
    public static final int DATATYPE_LIST = 4;

    /** String property type */
    public static final int DATATYPE_STRING = 1;

    /** URL property type */
    public static final int DATATYPE_URL = 2;

    /** The data type for this property */
    private int dataType;

    /** The displayable name for this property */
    private String displayName;

    /** The name of the underlying property */
    private String propertyName;

    /**
     * Creates a new instance of DescriptorPropertyDescription.
     * 
     * @param propertyName the name of the underlying property. This is
     *            case-sensitive and must not be <code>null</code> or an empty
     *            String <code>""</code>.
     * @param displayName the displayable name for this property. This is
     *            case-sensitive and must not be <code>null</code> or an empty
     *            String <code>""</code>.
     * @param dataType the data type for this property. Possible data types are:
     *            <ul>
     *            <li>{@link #DATATYPE_STRING}</li>
     *            <li>{@link #DATATYPE_URL}</li>
     *            <li>{@link #DATATYPE_INT}</li>
     *            <li>{@link #DATATYPE_LIST}</li>
     *            </ul>
     */
    public DescriptorPropertyDescription(String propertyName,
            String displayName, int dataType) {
        super();
        this.propertyName = propertyName;
        this.displayName = displayName;
        this.dataType = dataType;
    }

    /**
     * Returns the data type for this property.
     * 
     * @return the data type. Possible data types are:
     *         <ul>
     *         <li>{@link #DATATYPE_STRING}</li>
     *         <li>{@link #DATATYPE_URL}</li>
     *         <li>{@link #DATATYPE_INT}</li>
     *         <li>{@link #DATATYPE_LIST}</li>
     *         </ul>
     */
    public int getDataType() {
        return dataType;
    }

    /**
     * Returns the displayable name for this property.
     * 
     * @return the display name.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Returns the name of the underlying property
     * 
     * @return the property name.
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * Sets the data type for this property.
     * 
     * @param i the data type. Possible data types are:
     *            <ul>
     *            <li>{@link #DATATYPE_STRING}</li>
     *            <li>{@link #DATATYPE_URL}</li>
     *            <li>{@link #DATATYPE_INT}</li>
     *            <li>{@link #DATATYPE_LIST}</li>
     *            </ul>
     */
    public void setDataType(int i) {
        dataType = i;
    }

    /**
     * Sets the displayable name for this property.
     * 
     * @param string the display name. This is case-sensitive and must not be
     *            <code>null</code> or an empty String <code>""</code>.
     */
    public void setDisplayName(String string) {
        displayName = string;
    }

    /**
     * Sets the data type for this property.
     * 
     * @param string the property name. This is case-sensitive and must not be
     *            <code>null</code> or an empty String <code>""</code>.
     */
    public void setPropertyName(String string) {
        propertyName = string;
    }
}
