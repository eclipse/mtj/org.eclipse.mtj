/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID.
 */
package org.eclipse.mtj.internal.core.util;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;

/**
 * A midlet search scope built from a set of other search scopes.
 * 
 * @author Craig Setera
 */
public class MidletSearchScope extends AbstractSearchScope {

    /**
     * @author Craig Setera
     */
    private static class MidletSuperclassExclusionScope extends
            AbstractSearchScope {

        /**
         * @see org.eclipse.jdt.core.search.IJavaSearchScope#encloses(java.lang.String)
         */
        public boolean encloses(String resourcePath) {
            boolean excluded = resourcePath.endsWith("java/lang/Object.class")
                    || resourcePath
                            .endsWith("javax/microedition/midlet/MIDlet.class");

            return !excluded;
        }

        /**
         * @see org.eclipse.jdt.core.search.IJavaSearchScope#encloses(org.eclipse.jdt.core.IJavaElement)
         */
        public boolean encloses(IJavaElement element) {
            return true;
        }

        /**
         * @see org.eclipse.jdt.core.search.IJavaSearchScope#enclosingProjectsAndJars()
         */
        public IPath[] enclosingProjectsAndJars() {
            return new IPath[0];
        }
    }

    private IJavaSearchScope[] scopes;

    public MidletSearchScope(IJavaProject javaProject)
            throws JavaModelException {
        super();
        this.scopes = createSearchScopes(javaProject);
    }

    /**
     * @see org.eclipse.jdt.core.search.IJavaSearchScope#encloses(java.lang.String)
     */
    public boolean encloses(String resourcePath) {
        boolean encloses = true;

        for (int i = 0; encloses && i < scopes.length; i++) {
            encloses = scopes[i].encloses(resourcePath);
        }

        return encloses;
    }

    /**
     * @see org.eclipse.jdt.core.search.IJavaSearchScope#encloses(org.eclipse.jdt.core.IJavaElement)
     */
    public boolean encloses(IJavaElement element) {
        boolean encloses = true;

        for (int i = 0; encloses && i < scopes.length; i++) {
            encloses = scopes[i].encloses(element);
        }

        return encloses;
    }

    /**
     * @see org.eclipse.jdt.core.search.IJavaSearchScope#enclosingProjectsAndJars()
     */
    public IPath[] enclosingProjectsAndJars() {
        Set<IPath> set = new HashSet<IPath>();

        for (int i = 0; i < scopes.length; i++) {
            IJavaSearchScope scope = scopes[i];
            IPath[] scopePaths = scope.enclosingProjectsAndJars();
            for (int j = 0; j < scopePaths.length; j++) {
                set.add(scopePaths[j]);
            }
        }

        return (IPath[]) set.toArray(new IPath[set.size()]);
    }

    /**
     * Create the search scopes to be used by the midlet search scope.
     * 
     * @param javaProject
     * @return
     * @throws JavaModelException
     */
    private IJavaSearchScope[] createSearchScopes(IJavaProject javaProject)
            throws JavaModelException {
        IJavaSearchScope[] scopes = null;

        if ((javaProject == null) || (!javaProject.exists())) {
            IJavaSearchScope searchScope = SearchEngine.createWorkspaceScope();
            scopes = new IJavaSearchScope[] { searchScope };
        } else {
            IType type = javaProject
                    .findType(IMTJCoreConstants.MIDLET_SUPERCLASS);
            IJavaElement[] elements = new IJavaElement[] { javaProject };

            scopes = new IJavaSearchScope[] {
                    SearchEngine.createHierarchyScope(type),
                    SearchEngine.createJavaSearchScope(elements),
                    new MidletSuperclassExclusionScope(), };
        }

        return scopes;
    }
}
