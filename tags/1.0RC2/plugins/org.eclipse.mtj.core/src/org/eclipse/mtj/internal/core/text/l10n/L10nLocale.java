/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola)  - Initial Version
 *     David Marques (Motorola) - Implementing validation.
 */
package org.eclipse.mtj.internal.core.text.l10n;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.osgi.util.NLS;

public class L10nLocale extends L10nObject {

    private static final long serialVersionUID = 1L;

    /**
     * @param model
     */
    public L10nLocale(L10nModel model) {
        super(model, ELEMENT_LOCALE);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#canBeParent()
     */
    @Override
    public boolean canBeParent() {
        return true;
    }

    /**
     * @return
     */
    public String getLanguageCode() {
        return getXMLAttributeValue(ATTRIBUTE_LANGUAGE_CODE);
    }

    /**
     * @return
     */
    public String getCountryCode() {
        return getXMLAttributeValue(ATTRIBUTE_COUNTRY_CODE);
    }

    /**
     * @return
     */
    public void setLanguageCode(String lc) {
        setXMLAttribute(ATTRIBUTE_LANGUAGE_CODE, lc);
    }

    /**
     * @return
     */
    public void setCountryCode(String cc) {
        setXMLAttribute(ATTRIBUTE_COUNTRY_CODE, cc);
    }

    /**
     * @return
     */
    public String getLocaleName() {
        migrateFromPrevVersion();
        return NLS.bind("{0}-{1}", new String[] { getLanguageCode(), //$NON-NLS-1$
                getCountryCode() });
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getName()
     */
    @Override
    public String getName() {
        return getLocaleName();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getType()
     */
    @Override
    public int getType() {
        return TYPE_LOCALE;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#validate()
     */
    @Override
    public void validate() {

        String[] countries = Locale.getISOCountries();
        String[] languages = Locale.getISOLanguages();

        String lc = getLanguageCode();
        String cc = getCountryCode();

        boolean ccValid = false;
        boolean lcValid = false;

        for (String code : countries) {
            if (code.equalsIgnoreCase(cc)) {
                ccValid = true;
                break;
            }
        }

        for (String code : languages) {
            if (code.equalsIgnoreCase(lc)) {
                lcValid = true;
                break;
            }
        }

        String message = "";

        if ((!ccValid) || (!lcValid)) {

            if ((!lcValid)) {
                message = "Invalid language code. It must conform to ISO-639. ";
            }
            if ((!ccValid)) {
                message += "Invalid country code. It must conform to ISO-3166";
            }

            setStatus(new Status(IStatus.ERROR, IMTJCoreConstants.PLUGIN_ID,
                    message));

        } else {
            setStatus(new Status(IStatus.OK, IMTJCoreConstants.PLUGIN_ID,
                    "Valid Locale"));
        }

        syncronizeKeyStates();
    }

    /**
     * This method synchronizes the state of all entries in the locale.
     * 
     * @param locale target locale.
     */
    private void syncronizeKeyStates() {
        IDocumentElementNode[] entryNodes = this.getChildNodes();
        List<L10nEntry> cache = new LinkedList<L10nEntry>();
        for (IDocumentElementNode entryNode : entryNodes) {
            L10nEntry entry = (L10nEntry) entryNode;
            if (!cache.contains(entry)) {
                List<L10nEntry> duplicates = this.findDuplicatedEntries(entry);
                if (duplicates.size() > 0) {
                    for (L10nEntry duplicate : duplicates) {
                        duplicate.setStatus(new Status(IStatus.ERROR,
                                IMTJCoreConstants.PLUGIN_ID, null));
                        cache.add(duplicate);
                    }
                    entry.setStatus(new Status(IStatus.ERROR,
                            IMTJCoreConstants.PLUGIN_ID, null));
                } else {
                    entry.setStatus(new Status(IStatus.OK,
                            IMTJCoreConstants.PLUGIN_ID, null));
                }
            }
        }
    }

    /**
     * This method returns a list of all locale entries containing the specified
     * key.
     * 
     * @param locale target locale.
     * @param key entry key.
     * @return duplicated entries.
     */
    private List<L10nEntry> findDuplicatedEntries(L10nLocale locale, String key) {
        List<L10nEntry> duplicates = new LinkedList<L10nEntry>();
        IDocumentElementNode nodes[] = locale.getChildNodes();
        for (IDocumentElementNode node : nodes) {
            L10nEntry entry = (L10nEntry) node;
            if (entry.getKey().toUpperCase().equals(key.toUpperCase())) {
                duplicates.add(entry);
            }
        }
        return duplicates;
    }

    /**
     * This method finds all locale entries duplicated with the specified
     * locale.
     * 
     * @param localeEntry target locale.
     * @return duplicated entries.
     */
    public List<L10nEntry> findDuplicatedEntries(L10nEntry localeEntry) {
        List<L10nEntry> duplicates = findDuplicatedEntries(
                (L10nLocale) localeEntry.getParent(), localeEntry.getKey());
        duplicates.remove(localeEntry);
        return duplicates;
    }

    /**
     * Since the compatibility with the LOcalization Data from MTJ version
     * 0.9.1RC1 was broken, we try to convert the old XML format to the new one
     */
    private void migrateFromPrevVersion() {

        boolean fail = false;
        String name = getXMLAttributeValue(ATTRIBUTE_NAME);

        if (name != null) {
            if ((getLanguageCode() == null) && (getCountryCode() == null)) {
                try {
                    StringTokenizer tokenizer = new StringTokenizer(name, "-");

                    if (tokenizer.countTokens() == 2) {
                        setLanguageCode(tokenizer.nextToken());
                        setCountryCode(tokenizer.nextToken());
                    } else {
                        fail = true;
                    }
                } catch (Exception e) {
                    fail = true;
                }
            } else {
                setXMLAttribute(ATTRIBUTE_NAME, null);
            }
        }

        if (fail) {
            setStatus(new Status(IStatus.ERROR, IMTJCoreConstants.PLUGIN_ID,
                    "Invalid locale"));
        }
    }
}
