/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.internal.core.IModelChangeProviderExtension;
import org.eclipse.mtj.internal.core.IModelChangedEvent;
import org.eclipse.mtj.internal.core.IModelChangedListener;
import org.eclipse.mtj.internal.core.IModelChangedListenerFilter;
import org.eclipse.mtj.internal.core.ModelChangedEvent;

/**
 * @since 0.9.1
 */
public abstract class AbstractEditingModel extends PlatformObject implements
        IEditingModel, IModelChangeProviderExtension {

    private String fCharset;
    private boolean fDirty;
    private IDocument fDocument;
    private String fInstallLocation;
    private ArrayList<IModelChangedListener> fListeners = new ArrayList<IModelChangedListener>();
    private boolean fStale;
    private IResource fUnderlyingResource;
    protected boolean fDisposed;
    protected boolean fInSync = true;
    protected boolean fLoaded = false;
    protected boolean fReconciling;
    protected long fTimestamp;

    /**
     * @param document
     * @param isReconciling
     */
    public AbstractEditingModel(IDocument document, boolean isReconciling) {
        fDocument = document;
        fReconciling = isReconciling;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangeProvider#addModelChangedListener(org.eclipse.mtj.core.model.IModelChangedListener)
     */
    public void addModelChangedListener(IModelChangedListener listener) {
        if (!fListeners.contains(listener)) {
            fListeners.add(listener);
        }
    }

    /**
     * @param document
     * @throws CoreException
     */
    public abstract void adjustOffsets(IDocument document) throws CoreException;

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IBaseModel#dispose()
     */
    public void dispose() {
        fDisposed = true;
        fListeners.clear();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangeProvider#fireModelChanged(org.eclipse.mtj.core.model.IModelChangedEvent)
     */
    public void fireModelChanged(IModelChangedEvent event) {
        if ((event.getChangeType() == IModelChangedEvent.CHANGE)
                && (event.getOldValue() != null)
                && event.getOldValue().equals(event.getNewValue())) {
            return;
        }
        setDirty(event.getChangeType() != IModelChangedEvent.WORLD_CHANGED);
        for (int i = 0; i < fListeners.size(); i++) {
            (fListeners.get(i)).modelChanged(event);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangeProvider#fireModelObjectChanged(java.lang.Object, java.lang.String, java.lang.Object, java.lang.Object)
     */
    public void fireModelObjectChanged(Object object, String property,
            Object oldValue, Object newValue) {
        fireModelChanged(new ModelChangedEvent(this, object, property,
                oldValue, newValue));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.IEditingModel#getCharset()
     */
    public String getCharset() {
        return fCharset != null ? fCharset : "UTF-8"; //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.IEditingModel#getDocument()
     */
    public IDocument getDocument() {
        return fDocument;
    }

    /**
     * @return
     */
    public String getInstallLocation() {
        if ((fInstallLocation == null) && (fUnderlyingResource != null)) {
            IPath path = fUnderlyingResource.getProject().getLocation();
            return path != null ? path.addTrailingSeparator().toString() : null;
        }
        return fInstallLocation;
    }

    /**
     * @return
     */
    public IModelTextChangeListener getLastTextChangeListener() {
        for (int i = fListeners.size() - 1; i >= 0; i--) {
            Object obj = fListeners.get(i);
            if (obj instanceof IModelTextChangeListener) {
                return (IModelTextChangeListener) obj;
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModel#getTimeStamp()
     */
    public final long getTimeStamp() {
        return fTimestamp;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModel#getUnderlyingResource()
     */
    public IResource getUnderlyingResource() {
        return fUnderlyingResource;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IEditable#isDirty()
     */
    public boolean isDirty() {
        return fDirty;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IBaseModel#isDisposed()
     */
    public boolean isDisposed() {
        return fDisposed;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IBaseModel#isEditable()
     */
    public boolean isEditable() {
        return fReconciling;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModel#isInSync()
     */
    public boolean isInSync() {
        return fInSync;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModel#isLoaded()
     */
    public boolean isLoaded() {
        return fLoaded;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModel#isReconcilingModel()
     */
    public boolean isReconcilingModel() {
        return fReconciling;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.IEditingModel#isStale()
     */
    public boolean isStale() {
        return fStale;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IBaseModel#isValid()
     */
    public boolean isValid() {
        return isLoaded();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModel#load()
     */
    public final void load() throws CoreException {
        try {
            load(getInputStream(getDocument()), false);
        } catch (UnsupportedEncodingException e) {
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.IReconcilingParticipant#reconciled(org.eclipse.jface.text.IDocument)
     */
    public final void reconciled(IDocument document) {
        if (isReconcilingModel()) {
            try {
                if (isStale()) {
                    adjustOffsets(document);
                    setStale(false);
                } else {
                    reload(getInputStream(document), false);
                }
            } catch (UnsupportedEncodingException e) {
            } catch (CoreException e) {
            }
            if (isDirty()) {
                setDirty(false);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModel#reload(java.io.InputStream, boolean)
     */
    public final void reload(InputStream source, boolean outOfSync)
            throws CoreException {
        load(source, outOfSync);
        fireModelChanged(new ModelChangedEvent(this,
                IModelChangedEvent.WORLD_CHANGED, new Object[] { this }, null));

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangeProvider#removeModelChangedListener(org.eclipse.mtj.core.model.IModelChangedListener)
     */
    public void removeModelChangedListener(IModelChangedListener listener) {
        fListeners.remove(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IEditable#save(java.io.PrintWriter)
     */
    public void save(PrintWriter writer) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.IEditingModel#setCharset(java.lang.String)
     */
    public void setCharset(String charset) {
        fCharset = charset;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IEditable#setDirty(boolean)
     */
    public void setDirty(boolean dirty) {
        this.fDirty = dirty;
    }

    /**
     * @param location
     */
    public void setInstallLocation(String location) {
        fInstallLocation = location;
    }

    /**
     * @param loaded
     */
    public void setLoaded(boolean loaded) {
        fLoaded = loaded;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.IEditingModel#setStale(boolean)
     */
    public void setStale(boolean stale) {
        fStale = stale;
    }

    /**
     * @param resource
     */
    public void setUnderlyingResource(IResource resource) {
        fUnderlyingResource = resource;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangeProviderExtension#transferListenersTo(org.eclipse.mtj.core.model.IModelChangeProviderExtension, org.eclipse.mtj.core.model.IModelChangedListenerFilter)
     */
    @SuppressWarnings("unchecked")
    public void transferListenersTo(IModelChangeProviderExtension target,
            IModelChangedListenerFilter filter) {
        List oldList = (List) fListeners.clone();
        for (int i = 0; i < oldList.size(); i++) {
            IModelChangedListener listener = (IModelChangedListener) oldList
                    .get(i);
            if ((filter == null) || filter.accept(listener)) {
                // add the listener to the target
                target.addModelChangedListener(listener);
                // remove the listener from our list
                fListeners.remove(listener);
            }
        }
    }

    /**
     * @param document
     * @return
     * @throws UnsupportedEncodingException
     */
    protected InputStream getInputStream(IDocument document)
            throws UnsupportedEncodingException {
        return new BufferedInputStream(new ByteArrayInputStream(document.get()
                .getBytes(getCharset())));
    }

}
