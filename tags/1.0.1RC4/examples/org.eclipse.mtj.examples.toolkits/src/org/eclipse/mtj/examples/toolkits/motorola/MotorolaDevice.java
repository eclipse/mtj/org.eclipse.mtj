/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Gustavo de Paula (Motorola)  - Adapt to new AbstractMIDPDevice interface
 */
package org.eclipse.mtj.examples.toolkits.motorola;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.launching.LaunchEnvironment;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.launching.ILaunchConstants;
import org.eclipse.mtj.internal.core.launching.midp.IMIDPLaunchConstants;
import org.eclipse.mtj.internal.core.sdk.device.AbstractMIDPDevice;
import org.eclipse.mtj.internal.core.sdk.device.LaunchTemplateProperties;
import org.eclipse.mtj.internal.core.util.ReplaceableParametersProcessor;
import org.eclipse.mtj.internal.core.util.Utils;

/**
 * Implementation of the IDevice interface for Motorola devices.
 * 
 * @author Craig Setera
 */
public class MotorolaDevice extends AbstractMIDPDevice {

    /**
     * 
     */
    private static final String PROPERTIES_FILE = "propertiesFile";
    private File propertiesFile;

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDevice#getLaunchCommand(org.eclipse.mtj.core.launching.LaunchEnvironment, org.eclipse.core.runtime.IProgressMonitor)
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException {

        Map<String, String> executionProperties = new HashMap<String, String>();

        if (launchEnvironment.getProject() instanceof IMidletSuiteProject) {

            IMidletSuiteProject midletSuite = (IMidletSuiteProject) launchEnvironment
                    .getProject();
            ILaunchConfiguration launchConfiguration = launchEnvironment
                    .getLaunchConfiguration();

            boolean launchFromJAD = shouldDirectLaunchJAD(launchConfiguration);
            File tempDeployed = copyForLaunch(midletSuite, monitor,
                    launchFromJAD);

            // Fill in our properties so we can use them for resolution
            // against the raw executable command line
            executionProperties.put(LaunchTemplateProperties.EXECUTABLE
                    .toString(), executable.getPath());
            executionProperties.put(LaunchTemplateProperties.DEVICE.toString(),
                    getName());
            executionProperties
                    .put("devicePropsFile", propertiesFile.getPath());

            // Debug information
            if (launchEnvironment.isDebugLaunch()) {
                executionProperties.put(LaunchTemplateProperties.DEBUGPORT
                        .toString(), new Integer(launchEnvironment
                        .getDebugListenerPort()).toString());
            }

            // Classpath
            boolean shouldDirectLaunchJAD = shouldDirectLaunchJAD(launchConfiguration);
            if (!shouldDirectLaunchJAD) {
                String classpathString = getProjectClasspathString(midletSuite,
                        tempDeployed, monitor);
                executionProperties.put(LaunchTemplateProperties.CLASSPATH
                        .toString(), classpathString);
            }

            // Add launch configuration values
            addLaunchConfigurationValue(executionProperties,
                    LaunchTemplateProperties.VERBOSE.toString(),
                    launchConfiguration, ILaunchConstants.VERBOSITY_OPTIONS);
            addLaunchConfigurationValue(executionProperties,
                    LaunchTemplateProperties.HEAPSIZE.toString(),
                    launchConfiguration, IMIDPLaunchConstants.HEAP_SIZE);

            if (shouldDirectLaunchJAD) {
                executionProperties.put(LaunchTemplateProperties.JADFILE
                        .toString(), getSpecifiedJadURL(launchConfiguration));
            } else if (!shouldDoOTA(launchConfiguration)) {
                File jadFile = getJadForLaunch(midletSuite, tempDeployed,
                        monitor);
                if (jadFile.exists()) {
                    executionProperties.put(LaunchTemplateProperties.JADFILE
                            .toString(), jadFile.toString());
                }
            }

            String extraArguments = launchConfiguration.getAttribute(
                    ILaunchConstants.LAUNCH_PARAMS, Utils.EMPTY_STRING);
            executionProperties.put(
                    LaunchTemplateProperties.USERSPECIFIEDARGUMENTS.toString(),
                    extraArguments);

        }
        // Do the property resolution given the previous information
        return ReplaceableParametersProcessor.processReplaceableValues(
                launchCommandTemplate, executionProperties);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.AbstractMIDPDevice#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.loadUsing(persistenceProvider);
        propertiesFile = new File(persistenceProvider
                .loadString(PROPERTIES_FILE));
    }

    /**
     * @param propertiesFile The propertiesFile to set.
     */
    public void setPropertiesFile(File propertiesFile) {
        this.propertiesFile = propertiesFile;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.AbstractMIDPDevice#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.storeUsing(persistenceProvider);
        persistenceProvider.storeString(PROPERTIES_FILE, propertiesFile
                .toString());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice#getWorkingDirectory()
     */
    public File getWorkingDirectory() {
        return null;
    }
}
