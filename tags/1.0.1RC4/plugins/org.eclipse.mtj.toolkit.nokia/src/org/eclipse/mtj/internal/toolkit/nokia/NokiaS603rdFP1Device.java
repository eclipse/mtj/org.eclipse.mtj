/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.toolkit.nokia;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.project.ProjectType;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.ILibrary;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary;
import org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.toolkit.uei.EmulatorInfoArgs;
import org.eclipse.mtj.internal.toolkit.uei.UEIDeviceInternal;
import org.eclipse.mtj.internal.toolkit.uei.properties.DeviceSpecificProperties;
import org.eclipse.mtj.internal.toolkit.uei.properties.UEIDeviceDefinition;
import org.osgi.framework.Version;

/**
 * @author Diego Madruga Sandin
 * @since 1.0
 */
public class NokiaS603rdFP1Device extends UEIDeviceInternal {

    /**
     * Creates a new instance of NokiaS603rdFP1Device.
     */
    public NokiaS603rdFP1Device() {

    }

    /**
     * Create a new instance of NokiaS603rdFP1Device based on the properties for
     * that device found with the
     * <code>emulator {@link EmulatorInfoArgs#XQUERY -Xquery}</code> command.
     * 
     * @param name the device name.
     * @param groupName the device group name.
     * @param description the device description.
     * @param properties the properties for the device.
     * @param definition information on how the UEI emulator will be launched.
     * @param emulatorExecutable the emulator application.
     * @param preverifier the preverifier application.
     * @throws IllegalArgumentException if the properties or definition are
     *             <code>null</code>.
     */
    public NokiaS603rdFP1Device(String name, String groupName,
            String description, Properties properties,
            UEIDeviceDefinition definition, File emulatorExecutable,
            IPreverifier preverifier) throws IllegalArgumentException {
        super(name, groupName, description, properties, definition,
                emulatorExecutable, preverifier);
        setBundle(NokiaCore.getDefault().getBundle().getSymbolicName());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.toolkit.uei.UEIDeviceInternal#getBootClasspath(java.util.Properties)
     */
    @Override
    protected IDeviceClasspath getBootClasspath(Properties deviceProperties) {

        IDeviceClasspath deviceClasspath = MTJCore.createNewDeviceClasspath();
        String classpathString = deviceProperties.getProperty(
                DeviceSpecificProperties.BOOTCLASSPATH.toString(),
                Utils.EMPTY_STRING);

        deviceClasspath.addEntry(createLibraries(classpathString));
        return deviceClasspath;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.sdk.device.AbstractMIDPDevice#getBundle()
     */
    @Override
    public String getBundle() {
        return NokiaCore.getDefault().getBundle().getSymbolicName();
    }

    /**
     * Create the APIs for: <br>
     * Profile: MIDP-2.0<br>
     * Configuration: CLDC-1.1<br>
     * Optional:
     * <ul>
     * <li>JSR75-1.0</li>
     * <li>JSR82-1.0</li>
     * <li>MMAPI-1.1</li>
     * <li>WMA-2.0</li>
     * <li>JSR172-1.0</li>
     * <li>JSR177-1.0</li>
     * <li>JSR179-1.0</li>
     * <li>JSR180-1.0.1</li>
     * <li>JSR184-1.1</li>
     * <li>JSR226-1.0</li>
     * <li>JSR234-1.0</li>
     * <li>NOKAUI-1.1</li>
     * </ul>
     * 
     * @param classpath
     * @return
     */
    private ILibrary createLibraries(String classpath) {

        List<IMIDPAPI> apis = new ArrayList<IMIDPAPI>();

        IMIDPAPI midp = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        midp.setIdentifier("MIDP"); //$NON-NLS-1$
        midp.setType(MIDPAPIType.PROFILE);
        midp.setName("Mobile Information Device Profile"); //$NON-NLS-1$
        midp.setVersion(new Version("2.0")); //$NON-NLS-1$
        apis.add(midp);

        IMIDPAPI cldc = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        cldc.setIdentifier("CLDC"); //$NON-NLS-1$
        cldc.setType(MIDPAPIType.CONFIGURATION);
        cldc.setName("Connected Limited Device Configuration"); //$NON-NLS-1$
        cldc.setVersion(new Version("1.1")); //$NON-NLS-1$
        apis.add(cldc);

        IMIDPAPI jsr75 = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        jsr75.setIdentifier("JSR75"); //$NON-NLS-1$
        jsr75.setType(MIDPAPIType.OPTIONAL);
        jsr75.setName("PDA Optional Packages for the J2ME Platform"); //$NON-NLS-1$
        jsr75.setVersion(new Version("1.0")); //$NON-NLS-1$
        apis.add(jsr75);

        IMIDPAPI jsr82 = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        jsr82.setIdentifier("JSR82"); //$NON-NLS-1$
        jsr82.setType(MIDPAPIType.OPTIONAL);
        jsr82.setName("Java APIs for Bluetooth"); //$NON-NLS-1$
        jsr82.setVersion(new Version("1.1")); //$NON-NLS-1$
        apis.add(jsr82);

        IMIDPAPI jsr135 = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        jsr135.setIdentifier("MMAPI"); //$NON-NLS-1$
        jsr135.setType(MIDPAPIType.OPTIONAL);
        jsr135.setName("Mobile Media API"); //$NON-NLS-1$
        jsr135.setVersion(new Version("1.1")); //$NON-NLS-1$
        apis.add(jsr135);

        IMIDPAPI wma = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        wma.setIdentifier("WMA"); //$NON-NLS-1$
        wma.setType(MIDPAPIType.OPTIONAL);
        wma.setName("Wireless Messaging API"); //$NON-NLS-1$
        wma.setVersion(new Version("2.0")); //$NON-NLS-1$
        apis.add(wma);

        IMIDPAPI jsr172 = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        jsr172.setIdentifier("JSR172"); //$NON-NLS-1$
        jsr172.setType(MIDPAPIType.OPTIONAL);
        jsr172.setName("Web Services"); //$NON-NLS-1$
        jsr172.setVersion(new Version("1.0")); //$NON-NLS-1$
        apis.add(jsr172);

        IMIDPAPI jsr177 = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        jsr177.setIdentifier("JSR177"); //$NON-NLS-1$
        jsr177.setType(MIDPAPIType.OPTIONAL);
        jsr177.setName("Security and Trust Services API"); //$NON-NLS-1$
        jsr177.setVersion(new Version("1.0")); //$NON-NLS-1$
        apis.add(jsr177);

        IMIDPAPI jsr179 = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        jsr179.setIdentifier("JSR179"); //$NON-NLS-1$
        jsr179.setType(MIDPAPIType.OPTIONAL);
        jsr179.setName("Location API"); //$NON-NLS-1$
        jsr179.setVersion(new Version("1.0")); //$NON-NLS-1$
        apis.add(jsr179);

        IMIDPAPI jsr180 = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        jsr180.setIdentifier("JSR180"); //$NON-NLS-1$
        jsr180.setType(MIDPAPIType.OPTIONAL);
        jsr180.setName("SIP API"); //$NON-NLS-1$
        jsr180.setVersion(new Version("1.0.1")); //$NON-NLS-1$
        apis.add(jsr180);

        IMIDPAPI jsr184 = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        jsr184.setIdentifier("JSR184"); //$NON-NLS-1$
        jsr184.setType(MIDPAPIType.OPTIONAL);
        jsr184.setName("Mobile 3D Graphics API"); //$NON-NLS-1$
        jsr184.setVersion(new Version("1.1")); //$NON-NLS-1$
        apis.add(jsr184);

        IMIDPAPI jsr226 = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        jsr226.setIdentifier("JSR226"); //$NON-NLS-1$
        jsr226.setType(MIDPAPIType.OPTIONAL);
        jsr226.setName("Scalable 2D Vector Graphics API"); //$NON-NLS-1$
        jsr226.setVersion(new Version("1.0")); //$NON-NLS-1$
        apis.add(jsr226);

        IMIDPAPI jsr234 = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        jsr234.setIdentifier("JSR234"); //$NON-NLS-1$
        jsr234.setType(MIDPAPIType.OPTIONAL);
        jsr234.setName("Advanced Multimedia Supplements"); //$NON-NLS-1$
        jsr234.setVersion(new Version("1.0")); //$NON-NLS-1$
        apis.add(jsr234);

        IMIDPAPI nokiaApi = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        nokiaApi.setIdentifier("Nokia UI"); //$NON-NLS-1$
        nokiaApi.setType(MIDPAPIType.OPTIONAL);
        nokiaApi.setName("Nokia UI API"); //$NON-NLS-1$
        nokiaApi.setVersion(new Version("1.1")); //$NON-NLS-1$
        apis.add(nokiaApi);

        IMIDPLibrary library = (IMIDPLibrary) MTJCore
                .createNewLibrary(ProjectType.MIDLET_SUITE);
        library.setLibraryFile(new File(classpath));
        library.setApis(apis);

        return library;
    }
}
