/**
 * Copyright (c) 2006,2008 Nokia and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nokia                    - Initial version
 *     Diego Madruga (Motorola) - Refactored some parts of code to follow MTJ 
 *                                standards
 *     Diego Madruga (Motorola) - Refactored code to use a new superclass                          
 */
package org.eclipse.mtj.internal.jmunit.ui.wizards.testcase;

import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.junit.wizards.NewTestCaseWizardPageTwo;
import org.eclipse.mtj.internal.jmunit.JMUnitMessages;
import org.eclipse.mtj.internal.jmunit.JMUnitPluginImages;
import org.eclipse.mtj.internal.jmunit.ui.wizards.JMUnitWizard;

/**
 * A wizard for creating JMUnit compatible test cases.
 * 
 * @author Gorkem Ercan
 * @since 0.9.1
 */
public class NewJMUnitTestCaseWizard extends JMUnitWizard {

    private NewJMUnitTestCasePageOne fPage1;
    private NewTestCaseWizardPageTwo fPage2;

    public NewJMUnitTestCaseWizard() {
        super();
        setWindowTitle(JMUnitMessages.NewJMUnitTestCaseWizard_window_title);
        initDialogSettings();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#addPages()
     */
    @Override
    public void addPages() {
        super.addPages();
        fPage2 = new NewTestCaseWizardPageTwo();
        fPage1 = new NewJMUnitTestCasePageOne(fPage2);
        addPage(fPage1);
        fPage1.init(getSelection());
        addPage(fPage2);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public boolean performFinish() {

        if (finishPage(fPage1.getRunnable())) {
            IType newClass = fPage1.getCreatedType();

            IResource resource = newClass.getCompilationUnit().getResource();
            if (resource != null) {
                selectAndReveal(resource);
                openResource(resource);
            }
            return true;
        }
        return false;

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.jmunit.ui.wizards.JMUnitWizard#initializeDefaultPageImageDescriptor()
     */
    @Override
    protected void initializeDefaultPageImageDescriptor() {
        setDefaultPageImageDescriptor(JMUnitPluginImages.DESC_NEW_TEST_CASE_WIZ);

    }

}
