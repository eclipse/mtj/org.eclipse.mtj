/*******************************************************************************
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nokia Corporation - initial implementation 
 *******************************************************************************/
package org.eclipse.mtj.eswt.internal.templates;

import org.eclipse.osgi.util.NLS;

public class TemplateMessages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.mtj.eswt.internal.templates.messages"; //$NON-NLS-1$
	public static String HelloWorldTemplateMessage;
	public static String HelloWorldTemplateMessageHelloWorld;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, TemplateMessages.class);
	}

	private TemplateMessages() {
	}
}
