/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.build.export.states;

import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.internal.core.build.export.AntennaBuildExport;
import org.eclipse.mtj.internal.core.build.export.AntennaExportException;
import org.eclipse.mtj.internal.core.statemachine.StateMachine;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * CreateAntCleanTaskState cleans all temporary folders in order to start a new
 * build process.
 * 
 * @author David Marques
 * @since 1.0
 */
public class CreateAntCleanTaskState extends AbstractCreateAntTaskState {

	private Element cleanAll;

	/**
	 * Creates a {@link CreateAntCleanTaskState} instance bound to the specified
	 * state machine in order to create clean target for the specified project.
	 * 
	 * @param machine
	 *            bound {@link StateMachine} instance.
	 * @param project
	 *            target {@link IMidletSuiteProject} instance.
	 * @param _document
	 *            target {@link Document}.
	 */
	public CreateAntCleanTaskState(StateMachine _stateMachine,
			IMidletSuiteProject _suiteProject, Document _document) {
		super(_stateMachine, _suiteProject, _document);

		Document document = getDocument();
		Element root = document.getDocumentElement();
		cleanAll = XMLUtils
				.createTargetElement(document, root, "clean-all", ""); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mtj.internal.core.build.export.states.AbstractCreateAntTaskState
	 * #onEnter(org.eclipse.mtj.core.project.runtime.MTJRuntime)
	 */
	protected void onEnter(MTJRuntime runtime) throws AntennaExportException {
		Document document = getDocument();
		Element root = document.getDocumentElement();
		String configName = getFormatedName(runtime.getName());

		Element clean = XMLUtils.createTargetElement(document, root, NLS.bind(
				"clean-{0}", configName), getDependencies(runtime)); //$NON-NLS-1$
		Element delete = document.createElement("delete"); //$NON-NLS-1$
		clean.appendChild(delete);
		delete.setAttribute("failonerror", "false"); //$NON-NLS-1$ //$NON-NLS-2$
		delete
				.setAttribute(
						"dir", NLS.bind("{0}/{1}", new String[] { AntennaBuildExport.BUILD_FOLDER, configName })); //$NON-NLS-1$ //$NON-NLS-2$

		Element antCall = document.createElement("antcall"); //$NON-NLS-1$
		antCall.setAttribute("target", NLS.bind("clean-{0}", configName)); //$NON-NLS-1$ //$NON-NLS-2$
		cleanAll.appendChild(antCall);
	}
}
