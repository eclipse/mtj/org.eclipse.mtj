/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Gustavo de Paula (Motorola) - Runtime refactoring     
 */
package org.eclipse.mtj.core.project;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaConventions;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;

/**
 * This interface provides the basic methods that must be available in all
 * projects provided by MTJ.
 * <p>
 * Features of MTJ projects include:
 * <ul>
 * <li>Collects together a list of runtimes.</li>
 * <li>Carry references to a java project.</li>
 * <li>Controls the process of creating deployable applications based on its
 * resources.</li>
 * <li>Carry properties related to the signing process.</li>
 * <li>Notify listeners when it's state changes.</li>
 * </ul>
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noimplement This class is not intended to be implemented by clients.
 * @since 1.0
 */
public interface IMTJProject {

    /**
     * Adds the listener to the collection of listeners who will be notified
     * when the project state changes. The listener is notified by invoking one
     * of methods defined in the <code>IMTJProjectListener</code> interface.
     * 
     * @param projectListener the listener that should be notified when the
     *            project state changes.
     */
    public abstract void addMTJProjectListener(
            IMTJProjectListener projectListener);

    /**
     * Create a deployable JAR file package based on the contents available in
     * the project.
     * <p>
     * The process of creating a deployable is described bellow:
     * <ol>
     * <li>Clean the output of the specified project.</li>
     * <li>Invoke the build method of the specified builders for this project.</li>
     * <li>Create the package for the available configurations according the
     * given packageInactiveConfigs flag.</li>
     * <li>Notify listeners.</li>
     * </ol>
     * <p>
     * This method notifies all listeners after the package creation through the
     * invocation of {@link IMTJProjectListener#packageCreated()}. Listeners
     * wont be notified in case the package creation fails.
     * </p>
     * 
     * @param obfuscate a boolean indicating whether to obfuscate the resulting
     *            packaged code.
     * @param packageInactiveConfigs a boolean indicating whether to create
     *            packages for all configurations (including inactive
     *            configurations) or just for the active one.
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * 
     * @throws CoreException
     */
    public abstract void createPackage(boolean obfuscate,
            boolean packageInactiveConfigs, IProgressMonitor monitor)
            throws CoreException;

    /**
     * Returns the <code>IJavaProject</code> on which this
     * <code>IMTJProject</code> was created.
     * 
     * @return the non-<code>null</code> the <code>IJavaProject</code> on which
     *         this <code>IMTJProject</code> was created.
     */
    public abstract IJavaProject getJavaProject();

    /**
     * Returns the <code>IProject</code> on which this <code>IMTJProject</code>
     * was created.
     * 
     * @return the non-<code>null</code> the <code>IProject</code> on which this
     *         <code>IMTJProject</code> was created.
     */
    public abstract IProject getProject();

    /**
     * Return the list of runtimes that are associated to the project. From the
     * list it is possible to read each runtime and information such as the
     * device of each runtime
     * 
     * @return MTJRuntimeList list of MTJRuntime
     */
    public abstract MTJRuntimeList getRuntimeList();

    /**
     * Get the ISignatureProperties associated with this project.
     * <p>
     * If the project has no specific signing settings, the signature properties
     * available in the workspace secure preferences will be used as default.
     * </p>
     * <p>
     * In case the project specific signing settings were not set correctly
     * (reading these settings from the project metadata is returning
     * <code>null</code>) this method will return <code>null</code>.
     * </p>
     * 
     * @return the currently associated ISignatureProperties for this project or
     *         <code>null</code> in case of invalid properties.
     * @throws CoreException an error occur while decrypting the information
     *             available in the signature properties.
     */
    public abstract ISignatureProperties getSignatureProperties()
            throws CoreException;

    /**
     * Refresh the classpath for this project.
     * <p>
     * This method removes all MTJ classpath specific markers previously set on
     * the project, refreshes the classpath based on the active configuration
     * and forces a full rebuild.
     * </p>
     * <p>
     * This method notifies all listeners after the package creation through the
     * invocation of {@link IMTJProjectListener#classpathRefreshed()}. Listeners
     * wont be notified in case the classpath couldn't be refreshed.
     * </p>
     * 
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @throws CoreException if the classpath could not be set. Reasons include:
     *             <ul>
     *             <li>The associated javaProject does not exist
     *             (JavaModelException).</li>
     *             <li>Fails to clear all markers on the project before
     *             refreshing the classpath.</li>
     *             <li>The classpath is being modified during resource change
     *             event notification(JavaModelException).</li>
     *             <li>The classpath failed the validation check as defined by
     *             {@link JavaConventions#validateClasspath(IJavaProject, IClasspathEntry[], IPath)}
     *             (JavaModelException).</li>
     *             <li>The rebuild fails.</li>
     *             </ul>
     * @throws OperationCanceledException if the rebuild was canceled during
     *             execution.
     */
    public abstract void refreshClasspath(IProgressMonitor monitor)
            throws CoreException, OperationCanceledException;

    /**
     * Removes the listener from the collection of listeners who will be
     * notified when the project state changes.
     * 
     * @param projectListener the listener that should no longer be notified
     *            when the project state changes.
     */
    public abstract void removeMTJProjectListener(
            IMTJProjectListener projectListener);

    /**
     * Save the project's metadata.
     * <p>
     * Should be called after
     * {@link #setSignatureProperties(ISignatureProperties)} to persist the
     * information set.
     * </p>
     * <p>
     * This method notifies all listeners after saving the metadata through the
     * invocation of {@link IMTJProjectListener#metaDataSaved()}. Listeners wont
     * be notified in case the metadata could not be saved.
     * </p>
     * 
     * @throws CoreException if fails to save the metadata.
     */
    public abstract void saveMetaData() throws CoreException;

    /**
     * Set the signature properties for using when signing deployable packages
     * generated for this MTJ project.
     * <p>
     * This method notifies all listeners after setting the signature properties
     * through the invocation of
     * {@link IMTJProjectListener#signaturePropertiesChanged()}. Listeners wont
     * be notified in case the signature properties could not be set.
     * </p>
     * 
     * @param props the signature properties for use on signing the deployable
     *            packages.
     */
    public abstract void setSignatureProperties(ISignatureProperties props);

}