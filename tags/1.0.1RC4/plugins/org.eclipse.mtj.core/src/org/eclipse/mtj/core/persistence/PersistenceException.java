/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID
 *     Diego Sandin (Motorola)  - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.persistence;

/**
 * PersistenceException class wraps any exception that may be thrown during the 
 * persistence of any class implementing the {@link IPersistable} interface.
 * 
 * @since 1.0
 */
public class PersistenceException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Create a PersistenceException instance without any
     * message or {@link Throwable} cause.
     */
    public PersistenceException() {
        super();
    }

    /**
     * Creates a PersistenceException with the specified 
     * error message.
     * 
     * @param message error message.
     */
    public PersistenceException(String message) {
        super(message);
    }

    /**
     * Creates a PersistenceException with the specified 
     * error message and the specified exception as the
     * root cause.
     * 
     * @param message error message.
     * @param cause error cause.
     */
    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Creates a PersistenceException with the specified 
     * exception as the root cause.
     * 
     * @param cause error cause.
     */
    public PersistenceException(Throwable cause) {
        super(cause);
    }
}
