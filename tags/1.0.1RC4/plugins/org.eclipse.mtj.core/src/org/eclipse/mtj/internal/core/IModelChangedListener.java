/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core;


/**
 * Classes that need to be notified on model changes should implement this
 * interface and add themselves as listeners to the model they want to listen
 * to.
 * 
 * @since 0.9.1
 */
public interface IModelChangedListener {

    /**
     * Called when there is a change in the model this listener is registered
     * with.
     * 
     * @param event a change event that describes the kind of the model change
     */
    public void modelChanged(IModelChangedEvent event);
}