/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.refactoring;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEdit;

/**
 * BuildPropertiesChange class extends {@link TextFileChange} class
 * in order to represent changes made into a build.properties file
 * after renaming a resource.
 * 
 * @author David Marques
 */
public class BuildPropertiesChange extends TextFileChange {

	private boolean changed;
	private String  oldName;
	private String  newName;
	
	/**
	 * Creates a BuildPropertiesChange instance to represent changes
	 * of a resource from the specified old name to the specified new 
	 * name within the specified build.properties file.
	 * 
	 * @param _file target file.
	 * @param _oldName old name.
	 * @param _newName new name.
	 */
	public BuildPropertiesChange(IFile _file, String _oldName, String _newName) {
		super(Messages.BuildPropertiesChange_changeMessage, _file);
		this.oldName = _oldName;
		this.newName = _newName;
		
		IProgressMonitor monitor = new NullProgressMonitor();
		try {
			// Get the file contents
			String content = getCurrentContent(monitor);
			// Create a multi text edit instance and add
			// all changes into it
			MultiTextEdit multiTextEdit = new MultiTextEdit();
			TextEdit edits[] = this.createTextEdits(content, monitor);
			for (TextEdit textEdit : edits) {
				multiTextEdit.addChild(textEdit);
			}
			
			this.changed = edits.length > 0x00;
			this.setEdit(multiTextEdit);
		} catch (CoreException e) {
			MTJLogger.log(IStatus.ERROR, e);
		}
	}

	/**
	 * Checks whether the properties file has been changed
	 * after the resource name changes.
	 * 
	 * @return true if changed false otherwise.
	 */
	public boolean hasPropertiesChanges() {
		return this.changed;
	}
	
	/**
	 * Extracts all text edits representing changes on the properties
	 * file content after the resource name change.
	 * 
	 * @param content file content.
	 * @param monitor progress monitor.
	 * @return an array of text edits.
	 */
	private TextEdit[] createTextEdits(String content, IProgressMonitor monitor) {
		List<TextEdit> edits = new ArrayList<TextEdit>();
		
		Pattern pattern = Pattern.compile(oldName);
		Matcher matcher = pattern.matcher(content);
		
		while (matcher.find()) {
			int start  = matcher.start();
			int length = matcher.end() - start;
			ReplaceEdit edit = new ReplaceEdit(start, length, newName);
			edits.add(edit);
		}
		
		TextEdit array[] = new TextEdit[edits.size()];
		edits.toArray(array);
		return array;
	}
	
}
