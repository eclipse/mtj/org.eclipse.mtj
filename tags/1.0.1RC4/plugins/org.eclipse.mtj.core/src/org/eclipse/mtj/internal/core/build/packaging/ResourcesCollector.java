/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.build.packaging;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IJavaProject;

/**
 * ResourcesCollector class collects all non class
 * files from within an {@link IJavaProject} output
 * folder.
 * 
 * @author David Marques
 *
 */
public class ResourcesCollector implements IResourceVisitor {

	private IJavaProject javaProject;
	private List<IFile> resources;

	/**
	 * Creates an instance in order to collect resources
	 * from the specified {@link IJavaProject} instance.
	 * 
	 * @param _javaProject target project.
	 */
	public ResourcesCollector(IJavaProject _javaProject) {
		if (_javaProject == null) {
			throw new IllegalArgumentException("project can not be null.");
		}
		this.javaProject = _javaProject;
		this.resources = new ArrayList<IFile>();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.resources.IResourceVisitor#visit(org.eclipse.core.resources.IResource)
	 */
	public boolean visit(IResource resource) throws CoreException {
		IPath binaryFolderPath = this.javaProject.getOutputLocation();
		if (binaryFolderPath.isPrefixOf(resource.getFullPath())) {
			if (resource.getType() == IResource.FILE
					&& !resource.getName().endsWith(".class")) {
				this.resources.add((IFile) resource);
			}
		}
		return true;
	}

	/**
	 * Gets the collected resources.
	 * 
	 * @return resources list.
	 */
	public List<IFile> getResources() {
		return resources;
	}
}
