/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Aragao (Motorola) - Initial version
 */

package org.eclipse.mtj.internal.core.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;

/**
 * AbstractSuperClassSearchScope class implements a search
 * scope based on a class hierarchy.
 * 
 * @author David Aragao
 */
public abstract class AbstractSuperClassSearchScope extends AbstractSearchScope {
	
	private IJavaSearchScope[] scopes;

    public IJavaSearchScope[] getScopes() {
		return scopes;
	}

	public void setScopes(IJavaSearchScope[] scopes) {
		this.scopes = scopes;
	}

	public AbstractSuperClassSearchScope() {
		super();
	}
	
	/**
	 * @author Craig Setera
	 */	
	private static class MidletMidletExclusionScope extends
			AbstractSearchScope {
		
		String searchClassName;
		
		public MidletMidletExclusionScope(String searchClassName) {
			super();
			this.searchClassName = searchClassName;
		}
		
		/**
		 * @see org.eclipse.jdt.core.search.IJavaSearchScope#encloses(java.lang.String)
		 */
		public boolean encloses(String resourcePath) {
			boolean excluded = resourcePath.endsWith("java/lang/Object.class")
					|| resourcePath
							.endsWith(this.searchClassName);

			return !excluded;
		}

		 /**
		 * @see org.eclipse.jdt.core.search.IJavaSearchScope#encloses(org.eclipse.jdt.core.IJavaElement)
		 */
		public boolean encloses(IJavaElement element) {
			return true;
		}

		/**
		 * @see org.eclipse.jdt.core.search.IJavaSearchScope#enclosingProjectsAndJars()
		 */
		public IPath[] enclosingProjectsAndJars() {
			return new IPath[0];
		}

	}
	
	public AbstractSuperClassSearchScope(IJavaProject javaProject)
			throws JavaModelException {
		super();
	}

	
	/**
     * @see org.eclipse.jdt.core.search.IJavaSearchScope#encloses(java.lang.String)
     */
    public boolean encloses(String resourcePath) {
        boolean encloses = true;

        for (int i = 0; encloses && i < scopes.length; i++) {
            encloses = scopes[i].encloses(resourcePath);
        }

        return encloses;
    }

    /**
     * @see org.eclipse.jdt.core.search.IJavaSearchScope#encloses(org.eclipse.jdt.core.IJavaElement)
     */
    public boolean encloses(IJavaElement element) {
        boolean encloses = true;

        for (int i = 0; encloses && i < scopes.length; i++) {
            encloses = scopes[i].encloses(element);
        }

        return encloses;
    }

    /**
     * @see org.eclipse.jdt.core.search.IJavaSearchScope#enclosingProjectsAndJars()
     */
    public IPath[] enclosingProjectsAndJars() {
        Set<IPath> set = new HashSet<IPath>();

        for (int i = 0; i < scopes.length; i++) {
            IJavaSearchScope scope = scopes[i];
            IPath[] scopePaths = scope.enclosingProjectsAndJars();
            for (int j = 0; j < scopePaths.length; j++) {
                set.add(scopePaths[j]);
            }
        }

        return (IPath[]) set.toArray(new IPath[set.size()]);
    }


	/**
     * Create the search scopes to be used by the midlet search scope.
     * 
     * @param javaProject
     * @return
     * @throws JavaModelException
     */
    public IJavaSearchScope[] createSearchScopes(IJavaProject javaProject, String searchClassName, String searchClassType)
            throws JavaModelException {
        IJavaSearchScope[] scopes = null;

        if ((javaProject == null) || (!javaProject.exists())) {
            IJavaSearchScope searchScope = SearchEngine.createWorkspaceScope();
            scopes = new IJavaSearchScope[] { searchScope };
        } else {
                    	
            List<IPackageFragmentRoot> roots = new ArrayList<IPackageFragmentRoot>();
            for (IPackageFragmentRoot root : javaProject.getAllPackageFragmentRoots()) {
				if (root.getKind() == IPackageFragmentRoot.K_SOURCE) {
					roots.add(root);
				}
			}
            IJavaElement[] elements = roots.toArray(new IJavaElement[roots.size()]);

            IType type = javaProject.findType(searchClassType);
            if (type != null) {
				scopes = new IJavaSearchScope[] {
						SearchEngine.createHierarchyScope(type),
						SearchEngine.createJavaSearchScope(elements),
						new MidletMidletExclusionScope(searchClassName), };
			} else {
				scopes = new IJavaSearchScope[0];
			}
        }

        return scopes;
    }
}