/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Renato Franca (Motorola) - Initial implementation
 */

package org.eclipse.mtj.internal.core;

import java.io.IOException;
import java.net.URL;

import javax.xml.transform.stream.StreamSource;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.mtj.core.MTJCore;
import org.osgi.framework.Bundle;

/**
 * Bundle of all images used by the MTJ UI plug-in.
 * 
 * @since 1.0.1
 */
public class MTJPluginSchemas {

    private static MTJPluginSchemas instance;
    
    /**
     * Locales schema constant
     */
    public static final String LOCALES_SCHEMA = "locales.xsd"; //$NON-NLS-1$
    
    /**
     * Locale schema constant
     */
    public static final String LOCALE_SCHEMA = "locale.xsd"; //$NON-NLS-1$
    
    /**
     * Entry schema constant
     */
    public static final String ENTRY_SCHEMA = "entry.xsd"; //$NON-NLS-1$
    
    /**
     * schema folder constant
     */
    public final static String SCHEMA_PATH = "schema/"; //$NON-NLS-1$

    /*
     * Private constructor
     */
    private MTJPluginSchemas() {
    }
    
    /**
     * @return The MTJPluginSchemas singleton instance
     */
    public static MTJPluginSchemas getInstance() {
        if (instance ==null) {
            instance = new MTJPluginSchemas(); 
        }
        return instance;
    }

    /**
     * 
     * @param schema The schema name
     * @return The Stream source of schema
     */
    public StreamSource create(String schema) {
        
        URL url = makeSchemaURL(SCHEMA_PATH, schema);
        
        StreamSource src = null;
        try {
            src = new StreamSource(url.openStream());
        } catch (IOException e) {
        //Nothing to do
        }
        
        return src;
    }

    /*
     * Build  a schema URL
     */
    private URL makeSchemaURL(String prefix, String name) {
        String path = prefix + name;
        
        Bundle b = MTJCore.getMTJCore().getBundle();
        
        Path p= new Path("$nl$/" + path); //$NON-NLS-1$
        
        URL url = FileLocator.find(b, p, null);
        
        return url;
    }
}

