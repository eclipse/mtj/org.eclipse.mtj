/**
 * Copyright (c) 2008, 2009 Motorola Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gustavo de Paula (Motorola) - Initial implementation
 */
package org.eclipse.mtj.core.symbol;

import java.io.IOException;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * The SymbolSet registry is the main entry point that is used to manage the
 * SymbolSets that are currently available.
 * <p>
 * Clients must use {@link MTJCore#getSymbolSetRegistry()} method to retrieve an
 * instance of ISymbolSetRegistry.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface ISymbolSetRegistry extends IPersistable {

    /**
     * Add the specified SymbolSet object to the registry of SymbolSets.
     * 
     * @param symbolset the SymbolSet to be added to the registry.
     * @throws PersistenceException if any error occur while loading the
     *             persisted information.
     * @throws IllegalStateException if the provided definition set has a
     *             <code>null</code> name.
     */
    public void addSymbolSet(ISymbolSet symbolset) throws PersistenceException;

    /**
     * Add an array of SymbolSet to the registry
     * 
     * @param ss symbol set array
     * @throws PersistenceException if any error occur while loading the
     *             persisted information.
     */
    public void addSymbolSet(List<ISymbolSet> ss) throws PersistenceException;

    /**
     * Adds the listener to the collection of listeners who will be notified
     * when the SymbolSetRegistry state changes. The listener is notified by
     * invoking one of methods defined in the
     * <code>ISymbolSetRegistryChangeListener</code> interface.
     * 
     * @param listener the listener that should be notified when the
     *            SymbolSetRegistry state changes.
     */
    public void addSymbolSetRegistryChangeListener(
            ISymbolSetRegistryChangeListener listener);

    /**
     * Clear all of the registered SymbolSet objects.
     * 
     * @throws PersistenceException if any error occur while loading the
     *             persisted information.
     */
    public void clear() throws PersistenceException;

    /**
     * Return all of the SymbolSet names registered.
     * 
     * @return all of the SymbolSet names registered.
     * @throws PersistenceException if any error occur while loading the
     *             persisted information.
     */
    public String[] getAllSymbolSetNames() throws PersistenceException;

    /**
     * Returns an array of the SymbolSets contained in this registry.
     * 
     * @return array of the SymbolSets contained in this registry.
     * @throws PersistenceException if any error occur while loading the
     *             persisted information.
     */
    public ISymbolSet[] getAllSymbolSets() throws PersistenceException;

    /**
     * Return the SymbolSet instance registered with the specified name or
     * <code>null</code> if the object cannot be found.
     * 
     * @param name the SymbolSet name.
     * @return the SymbolSet instance registered with the specified name or
     *         <code>null</code> if the object cannot be found.
     * @throws PersistenceException if any error occur while loading the
     *             persisted information.
     */
    public ISymbolSet getSymbolSet(String name) throws PersistenceException;

    /**
     * Load the contents of the symbol SymbolSetRegistry from the storage file
     * in the plug-in state location.
     * 
     * @throws PersistenceException if any error occur while loading the
     *             persisted information.
     */
    public void load() throws PersistenceException;

    /**
     * Remove the specified SymbolSet set from the registry. Does nothing if the
     * specified set cannot be found in the registry.
     * 
     * @param setName the name of the SymbolSet to be removed from the registry.
     */
    public void removeSymbolSet(String setName);

    /**
     * Removes the listener from the collection of listeners who will be
     * notified when the SymbolSetRegistry state changes.
     * 
     * @param listener the listener that should no longer be notified when the
     *            SymbolSetRegistry state changes.
     */
    public void removeSymbolSetRegistryChangeListener(
            ISymbolSetRegistryChangeListener listener);

    /**
     * Store out the contents of the SymbolSetRegistry into the standard device
     * storage file in the plug-in state location.
     * 
     * @throws PersistenceException if any error occur while writing the
     *             persisted information.
     * @throws TransformerException if any error occur while writing the
     *             persisted information.
     * @throws IOException if any error occur while writing the persisted
     *             information.
     */
    public void store() throws PersistenceException, TransformerException,
            IOException;
}
