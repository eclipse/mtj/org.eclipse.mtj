/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

/**
 * Work around class to enable displaying errors on a StringFieldEditor.
 * 
 * @author Diego Madruga Sandin
 */
public class ExtendedStringFieldEditor extends StringFieldEditor {

    /**
     * Creates a ExtendedStringFieldEditor field editor of unlimited width. Use
     * the method <code>setTextLimit</code> to limit the text.
     * 
     * @param name the name of the preference this field editor works on
     * @param labelText the label text of the field editor
     * @param parent the parent of the field editor's control
     */
    public ExtendedStringFieldEditor(String name, String labelText,
            Composite parent) {
        super(name, labelText, parent);
    }

    /**
     * Creates a ExtendedStringFieldEditor field editor. Use the method
     * <code>setTextLimit</code> to limit the text.
     * 
     * @param name the name of the preference this field editor works on
     * @param labelText the label text of the field editor
     * @param width the width of the text input field in characters, or
     *                <code>UNLIMITED</code> for no limit
     * @param parent the parent of the field editor's control
     */
    public ExtendedStringFieldEditor(String name, String labelText, int width,
            Composite parent) {
        super(name, labelText, width, parent);
    }

    /**
     * Creates a ExtendedStringFieldEditor field editor. Use the method
     * <code>setTextLimit</code> to limit the text.
     * 
     * @param name the name of the preference this field editor works on
     * @param labelText the label text of the field editor
     * @param width the width of the text input field in characters, or
     *                <code>UNLIMITED</code> for no limit
     * @param strategy either <code>VALIDATE_ON_KEY_STROKE</code> to perform
     *                on the fly checking (the default), or
     *                <code>VALIDATE_ON_FOCUS_LOST</code> to perform
     *                validation only after the text has been typed in
     * @param parent the parent of the field editor's control
     */
    public ExtendedStringFieldEditor(String name, String labelText, int width,
            int strategy, Composite parent) {
        super(name, labelText, width, strategy, parent);
    }

    /**
     * Returns this field editor's text control.
     * 
     * @return the text control, or <code>null</code> if no text field is
     *         created yet
     * @see org.eclipse.jface.preference.StringFieldEditor#getTextControl()
     */
    public Text getFieldEditorTextControl() {
        return getTextControl();
    }

}
