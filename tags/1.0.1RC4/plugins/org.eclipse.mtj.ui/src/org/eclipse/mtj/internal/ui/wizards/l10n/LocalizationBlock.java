/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Updating Wizard buttons upon change.      
 *     Diego Madruga (Motorola) - Show the String (Default Package) on field 
 *                                instead of leaving it blank, see bug 259566            
 */
package org.eclipse.mtj.internal.ui.wizards.l10n;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizardContainer;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.dialog.SrcFolderFilter;
import org.eclipse.mtj.internal.ui.util.FolderValidator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.SelectionDialog;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

/**
 * LocalizationBlock builds a UI components block for localization addition into
 * a java me project.
 * 
 * @since 0.9.1
 */
public class LocalizationBlock implements MouseListener {

    private Composite composite;

    private Button dstFolderButton;
    private Text dstFolderText;
    private IJavaElement jPackage;

    private IJavaProject jProject;
    private IResource jSource;
    private Button pkgFolderButton;
    private Text pkgFolderText;
    private IWizardContainer wizard;

    /**
     * Creates a LocalizationBlock associated to the specified project.
     * 
     * @param project target project.
     * @param wizardContainer wizard.
     */
    public LocalizationBlock(IJavaProject project, IWizardContainer wizard) {
        this.jProject = project;
        this.wizard = wizard;
    }

    /**
     * Creates the UI block as child of the specified composite.
     * 
     * @param parent composite.
     * @return the built composite.
     */
    public Composite createMainComposite(Composite parent) {
        // Create the main Composite.
        composite = new Composite(parent, SWT.NONE);
        composite.setFont(parent.getFont());

        GridLayout layout = new GridLayout();
        layout.numColumns = 3;
        composite.setLayout(layout);

        Label dstFolderLabel = new Label(composite, SWT.NONE);
        dstFolderLabel
                .setText(MTJUIMessages.LocalizationBlock_DestinationFolder);

        dstFolderText = new Text(composite, SWT.BORDER);
        dstFolderText.setEnabled(false);

        dstFolderButton = new Button(composite, SWT.NONE);
        dstFolderButton.setText(MTJUIMessages.LocalizationBlock_Browse);
        dstFolderButton.addMouseListener(this);

        Label packageFolderLabel = new Label(composite, SWT.NONE);
        packageFolderLabel.setText(MTJUIMessages.LocalizationBlock_PackageName);

        pkgFolderText = new Text(composite, SWT.BORDER);
        pkgFolderText.setEnabled(false);

        pkgFolderButton = new Button(composite, SWT.NONE);
        pkgFolderButton.setText(MTJUIMessages.LocalizationBlock_Browse);
        pkgFolderButton.addMouseListener(this);

        Dialog.applyDialogFont(composite);

        int srcLabelWidth = this.getWidgetTextWidth(dstFolderLabel,
                dstFolderLabel.getText());
        int pkgLabelWidth = this.getWidgetTextWidth(packageFolderLabel,
                packageFolderLabel.getText());

        GridData data = new GridData();
        data.widthHint = Math.max(srcLabelWidth, pkgLabelWidth);
        dstFolderLabel.setLayoutData(data);
        packageFolderLabel.setLayoutData(data);

        data = new GridData(GridData.FILL_HORIZONTAL);
        dstFolderText.setLayoutData(data);
        pkgFolderText.setLayoutData(data);

        data = new GridData();
        data.widthHint = this.getWidgetTextWidth(dstFolderButton,
                dstFolderButton.getText()) + 20;
        dstFolderButton.setLayoutData(data);
        pkgFolderButton.setLayoutData(data);

        return composite;
    }

    /**
     * Gets the UI block composite.
     * 
     * @return block composite.
     */
    public Composite getControl() {
        return this.composite;
    }

    /**
     * Gets the selected source folder.
     * 
     * @return source folder.
     */
    public IResource getDestination() {
        return this.jSource;
    }

    /**
     * Gets the selected package.
     * 
     * @return selected package.
     */
    public IJavaElement getPackage() {
        return this.jPackage;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.swt.events.MouseListener#mouseDoubleClick(org.eclipse.swt
     * .events.MouseEvent)
     */
    public void mouseDoubleClick(MouseEvent e) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.swt.events.MouseListener#mouseDown(org.eclipse.swt.events
     * .MouseEvent)
     */
    public void mouseDown(MouseEvent e) {
    }

    /*
     * (non-Javadoc)
     * 
     * @seeorg.eclipse.swt.events.MouseListener#mouseUp(org.eclipse.swt.events.
     * MouseEvent)
     */
    public void mouseUp(MouseEvent event) {
        if (event.getSource() == this.dstFolderButton) {
            selectDestinationFolder();
        } else if (event.getSource() == this.pkgFolderButton) {
            selectPackageFolder();
        }
        this.wizard.updateButtons();
    }

    /**
     * Gets the string width of the specified text.
     * 
     * @param control Containing control.
     * @param text Control text.
     * @return width of text.
     */
    private int getWidgetTextWidth(Control control, String text) {
        GC gc = new GC(control);
        gc.setFont(control.getFont());
        return gc.stringExtent(text).x;
    }

    /**
     * Opens a folder selection dialog.
     */
    private void selectDestinationFolder() {

        ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
                this.dstFolderButton.getShell(), new WorkbenchLabelProvider(),
                new WorkbenchContentProvider());

        dialog.setValidator(new FolderValidator());
        dialog.setAllowMultiple(false);
        dialog.setTitle(MTJUIMessages.LocalizationBlock_DestinationFolderTitle);
        dialog
                .setMessage(MTJUIMessages.LocalizationBlock_DestinationFolderMessage);

        try {
            dialog.addFilter(new SrcFolderFilter(jProject.getProject(), Utils
                    .getJavaProjectSourceDirectories(jProject)));
        } catch (CoreException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }

        dialog.setInput(MTJUIPlugin.getWorkspace().getRoot());

        if (dialog.open() == Window.OK) {
            IResource folder = (IResource) dialog.getFirstResult();
            this.jSource = folder;
            this.dstFolderText.setText(folder.getProjectRelativePath()
                    .toString());
        }
    }

    /**
     * Opens a package selection dialog.
     */
    private void selectPackageFolder() {
        try {
            SelectionDialog dialog = JavaUI.createPackageDialog(
                    this.dstFolderButton.getShell(), this.jProject, 0x00);
            dialog
                    .setMessage(MTJUIMessages.LocalizationBlock_PackageDialogMessage);
            dialog.setTitle(MTJUIMessages.LocalizationBlock_PackageDialogTitle);
            if (dialog.open() == Window.OK) {
                Object result[] = dialog.getResult();
                if (result.length > 0) {
                    this.jPackage = (IPackageFragment) dialog.getResult()[0];
                    String defaultPackage = "(Default Package)"; //$NON-NLS-1$
                    if (this.jPackage.getElementName().equals("")) { //$NON-NLS-1$
                        this.pkgFolderText.setText(defaultPackage);
                    } else {
                        this.pkgFolderText.setText(this.jPackage
                                .getElementName());
                    }
                }
            }
        } catch (JavaModelException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }
}
