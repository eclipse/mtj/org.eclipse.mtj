/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Verifying password length
 */
package org.eclipse.mtj.internal.ui.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.mtj.internal.core.sign.KeyPairInfo;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * NewKeyPairDialog class provides a dialog for users to enter a new key pair
 * information.
 * 
 * @author David Marques
 * @since 1.0
 */
public class NewKeyPairDialog extends Dialog {

	private KeyPairInfo info;
	private String title;

	private Text aliasText;
	private Text passwdText;
	private Text cnText;
	private Text ouText;
	private Text oText;
	private Text lText;
	private Text sText;
	private Text cText;

	/**
	 * Creates a dialog instance.
	 * 
	 * @param parentShell
	 *            parent shell.
	 */
	public NewKeyPairDialog(Shell parentShell) {
		super(parentShell);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	protected Control createDialogArea(Composite parent) {
		Composite main = new Composite(parent, SWT.NONE);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.widthHint = 400;
		main.setLayoutData(gridData);
		main.setLayout(new GridLayout(0x02, false));

		Label label = new Label(main, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		label.setText(MTJUIMessages.NewKeyPairDialog_alias);

		aliasText = new Text(main, SWT.BORDER);
		aliasText
				.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		label = new Label(main, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		label.setText(MTJUIMessages.NewKeyPairDialog_password);

		passwdText = new Text(main, SWT.PASSWORD | SWT.BORDER);
		passwdText
				.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		label = new Label(main, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		label.setText(MTJUIMessages.NewKeyPairDialog_commonName);

		cnText = new Text(main, SWT.BORDER);
		cnText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		label = new Label(main, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		label.setText(MTJUIMessages.NewKeyPairDialog_organizationUnit);

		ouText = new Text(main, SWT.BORDER);
		ouText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		label = new Label(main, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		label.setText(MTJUIMessages.NewKeyPairDialog_organizationName);

		oText = new Text(main, SWT.BORDER);
		oText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		label = new Label(main, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		label.setText(MTJUIMessages.NewKeyPairDialog_localityName);

		lText = new Text(main, SWT.BORDER);
		lText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		label = new Label(main, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		label.setText(MTJUIMessages.NewKeyPairDialog_stateName);

		sText = new Text(main, SWT.BORDER);
		sText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		label = new Label(main, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		label.setText(MTJUIMessages.NewKeyPairDialog_country);

		cText = new Text(main, SWT.BORDER);
		cText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		return main;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets
	 * .Shell)
	 */
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		if (this.title != null) {
			newShell.setText(this.title);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	protected void okPressed() {
		if (this.passwdText.getText().trim().length() < 0x06) {
			MessageDialog.openInformation(this.getShell(),
					MTJUIMessages.NewKeyPairDialog_invalidPassword,
					MTJUIMessages.NewKeyPairDialog_invalidPasswordMessage);
			return;
		}

		info = new KeyPairInfo(this.aliasText.getText(), this.passwdText
				.getText());
		info.setCommonName(this.cnText.getText());
		info.setOrganizationUnit(this.ouText.getText());
		info.setOrganizationName(this.oText.getText());
		info.setLocalityName(this.lText.getText());
		info.setStateName(this.sText.getText());
		info.setCountry(this.cText.getText());
		super.okPressed();
	}

	/**
	 * Sets the dialog's title.
	 * 
	 * @param title
	 *            dialog title.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets an instance of {@link KeyPairInfo} built from the specified values
	 * from the user.
	 * 
	 * @return {@link KeyPairInfo} instance.
	 */
	public KeyPairInfo getKeyPairInfo() {
		return info;
	}

}
