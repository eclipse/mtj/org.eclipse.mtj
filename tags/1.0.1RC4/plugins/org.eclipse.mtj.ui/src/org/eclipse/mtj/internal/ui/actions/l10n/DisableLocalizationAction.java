/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version                     
 */
package org.eclipse.mtj.internal.ui.actions.l10n;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.mtj.core.project.MTJNature;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.actions.AbstractJavaProjectAction;

/**
 * DisableLocalizationAction class disables localization feature into MTJ
 * projects.
 * 
 * @author David Marques
 * @since 0.9.1
 */
public class DisableLocalizationAction extends AbstractJavaProjectAction {

    /* (non-Javadoc)
     * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
     */
    public void run(IAction action) {
        if ((selection != null) && !selection.isEmpty()) {
            IJavaProject javaProject = getJavaProject(selection
                    .getFirstElement());
            if (javaProject == null) {
                return;
            }
            try {
                MTJNature.removeNatureFromProject(javaProject.getProject(),
                        IMTJCoreConstants.L10N_NATURE_ID,
                        new NullProgressMonitor());
            } catch (CoreException e) {
                MTJLogger.log(IStatus.ERROR, e);
            }
        }
    }

}
