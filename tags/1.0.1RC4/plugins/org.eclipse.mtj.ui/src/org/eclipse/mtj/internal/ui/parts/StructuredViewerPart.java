/**
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.parts;

import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.FormToolkit;

public abstract class StructuredViewerPart extends SharedPartWithButtons {

    private Point fMinSize;

    private StructuredViewer fViewer;

    public StructuredViewerPart(String[] buttonLabels) {
        super(buttonLabels);
    }

    public Control getControl() {
        return fViewer.getControl();
    }

    public StructuredViewer getViewer() {
        return fViewer;
    }

    public void setMinimumSize(int width, int height) {
        fMinSize = new Point(width, height);
        if (fViewer != null) {
            applyMinimumSize();
        }
    }

    private void applyMinimumSize() {
        if (fMinSize != null) {
            GridData gd = (GridData) fViewer.getControl().getLayoutData();
            gd.widthHint = fMinSize.x;
            gd.heightHint = fMinSize.y;
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.parts.SharedPartWithButtons#createMainControl(org.eclipse.swt.widgets.Composite, int, int, org.eclipse.ui.forms.widgets.FormToolkit)
     */
    @Override
    protected void createMainControl(Composite parent, int style, int span,
            FormToolkit toolkit) {
        fViewer = createStructuredViewer(parent, style, toolkit);
        Control control = fViewer.getControl();
        GridData gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = span;
        control.setLayoutData(gd);
        applyMinimumSize();
    }

    protected abstract StructuredViewer createStructuredViewer(
            Composite parent, int style, FormToolkit toolkit);

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.parts.SharedPartWithButtons#updateEnabledState()
     */
    @Override
    protected void updateEnabledState() {
        getControl().setEnabled(isEnabled());
        super.updateEnabledState();
    }
}
