/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *******************************************************************************/

package org.eclipse.mtj.internal.ui.launching;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IClassFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * Utilities for MIDlet launching
 * 
 * @author Feng Wang
 */
public class MidletLaunchConfigUtils {

    /**
     * Collect MIDlets from project.
     * 
     * @param monitor
     * @param project
     * @return
     */
    private static List<IType> collectMidletTypesInProject(
            IProgressMonitor monitor, IJavaProject project) {
        IType[] types;
        HashSet<IType> result = new HashSet<IType>(5);
        try {
            IType midlet = project
                    .findType(IMTJCoreConstants.MIDLET_SUPERCLASS);
            ITypeHierarchy hierarchy = midlet.newTypeHierarchy(project,
                    new SubProgressMonitor(monitor, 1));
            types = hierarchy.getAllSubtypes(midlet);
            int length = types.length;
            if (length != 0) {
                for (int i = 0; i < length; i++) {
                    if (!types[i].isBinary()) {
                        result.add(types[i]);
                    }
                }
            }
        } catch (JavaModelException jme) {
        }
        monitor.done();
        return new ArrayList<IType>(result);
    }

    /**
     * Collect MIDlet(s) from given Java element.
     * 
     * @param element - a <code>IJavaElement</code> within which the
     *                collecting is performed.
     * @param monitor
     * @param result - a <code>Set</code> contains collected MIDlets.
     */
    private static void collectTypes(Object element,
            SubProgressMonitor monitor, Set<IType> result) {
        element = computeScope(element);
        try {
            // if the selected is a member(field, method) of MIDlet, collect
            // is't parent MIDlet
            while (element instanceof IMember) {
                if (element instanceof IType) {
                    if (Utils.isMidlet((IType) element, monitor)) {
                        result.add((IType) element);
                        monitor.done();
                        return;
                    }
                }
                element = ((IJavaElement) element).getParent();
            }
            // if the selected is a MIDet, collect it
            if (element instanceof ICompilationUnit) {
                ICompilationUnit cu = (ICompilationUnit) element;
                IType[] types = cu.getAllTypes();
                for (IType element2 : types) {
                    if (Utils.isMidlet(element2, monitor)) {
                        result.add(element2);
                    }
                }
            }
            // if the selected is a MIDlet class file, collect the MIDlet
            else if (element instanceof IClassFile) {
                IType type = ((IClassFile) element).getType();
                if (Utils.isMidlet(type, monitor)) {
                    result.add(type);
                }
            }
            // if the selected is a container, collect all MIDlets in it
            else if (element instanceof IJavaElement) {
                // the container
                IJavaElement parent = (IJavaElement) element;
                // get all MIDlets in the project
                List<IType> found = collectMidletTypesInProject(monitor, parent
                        .getJavaProject());
                // filter within the parent element
                // only collect MIDlets that are located in the container
                Iterator<IType> iterator = found.iterator();
                while (iterator.hasNext()) {
                    IJavaElement target = iterator.next();
                    IJavaElement child = target;
                    while (child != null) {
                        if (child.equals(parent)) {
                            result.add((IType) target);
                            break;
                        }
                        child = child.getParent();
                    }
                }
            }
        } catch (JavaModelException e) {
            MTJLogger.log(IStatus.WARNING, "collectTypes", e);
        }
    }

    /**
     * If the element is not a <code>IJavaElement</code>, try to get the
     * <code>IJavaElement</code> from it.
     * 
     * @param element
     * @return
     */
    private static Object computeScope(Object element) {
        if (element instanceof IJavaElement) {
            return element;
        }
        if (element instanceof IAdaptable) {
            element = ((IAdaptable) element).getAdapter(IResource.class);
        }
        if (element instanceof IResource) {
            IJavaElement javaElement = JavaCore.create((IResource) element);
            if (javaElement != null && !javaElement.exists()) {
                // do not consider the resource - corresponding java element
                // does not exist
                element = null;
            } else {
                element = javaElement;
            }

        }
        return element;
    }

    /**
     * Searches for MIDlets from within the given scope of elements
     * 
     * @param context
     * @param elements - a array of <code>IJavaElement</code>s. The search
     *                scope.
     * @return - an array of <code>IType</code>s of matches for java types
     *         that extend <code>javax.microedition.midlet.MIDlet</code>
     *         (directly or indirectly)
     * @throws InvocationTargetException
     * @throws InterruptedException
     */
    public static IType[] findMidlets(IRunnableContext context,
            final Object[] elements) throws InvocationTargetException,
            InterruptedException {
        final Set<IType> result = new HashSet<IType>();

        if (elements.length > 0) {
            IRunnableWithProgress runnable = new IRunnableWithProgress() {

                public void run(IProgressMonitor monitor)
                        throws InvocationTargetException, InterruptedException {
                    for (Object element : elements) {
                        collectTypes(element,
                                new SubProgressMonitor(monitor, 1), result);
                    }
                }

            };
            context.run(true, true, runnable);
        }

        IType[] types = result.toArray(new IType[result.size()]);
        return types;
    }
}
