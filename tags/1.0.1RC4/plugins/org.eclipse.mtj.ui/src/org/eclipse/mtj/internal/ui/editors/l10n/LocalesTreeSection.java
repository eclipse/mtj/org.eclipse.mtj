/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola)  - Initial Version
 *     David Marques (Motorola) - Synchronizing key states with outline. 
 *     David Marques (Motorola) - Adding call to updateMessageManager method.
 *     Fernando Rocha(Motorola) - Validate the locales in the add action and model changed
 */
package org.eclipse.mtj.internal.ui.editors.l10n;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.mtj.internal.core.IModelChangedEvent;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.l10n.IL10nConstants;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nObject;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.editor.FormFilteredTree;
import org.eclipse.mtj.internal.ui.editor.MTJFormPage;
import org.eclipse.mtj.internal.ui.editor.TreeSection;
import org.eclipse.mtj.internal.ui.editor.actions.CollapseAction;
import org.eclipse.mtj.internal.ui.editors.l10n.actions.L10nAddLocaleAction;
import org.eclipse.mtj.internal.ui.editors.l10n.actions.L10nAddLocaleEntryAction;
import org.eclipse.mtj.internal.ui.editors.l10n.actions.L10nAddObjectAction;
import org.eclipse.mtj.internal.ui.editors.l10n.actions.L10nRemoveObjectAction;
import org.eclipse.mtj.internal.ui.parts.TreePart;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.dialogs.PatternFilter;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.keys.IBindingService;

/**
 * LocalesTreeSection - The section that displays the Locales tree structure and
 * any buttons used to manipulate it. This is the main section that the user
 * will interact with the Locales through.
 * 
 * @since 0.9.1
 */
public class LocalesTreeSection extends TreeSection {

    /*
     * The indices for each button attached to the Tree Viewer. This type of UI
     * form does not permit direct access to each particular button. However,
     * using these indices, one can perform any typical SWT operation on any
     * button.
     */
    private static final int F_BUTTON_ADD_LOCALE = 0;
    private static final int F_BUTTON_ADD_ENTRY = 1;
    private static final int F_BUTTON_REMOVE = 2;
    private static final int F_BUTTON_UP = 4;
    private static final int F_BUTTON_DOWN = 5;
    private static final int F_DOWN_FLAG = 1;
    private static final int F_UP_FLAG = -1;

    // The actions that will add each type of Locales object
    private L10nAddLocaleAction addLocaleAction;
    private L10nAddLocaleEntryAction addLocaleEntryAction;

    /**
     * The action that collapses down the Locales tree
     */
    private CollapseAction fCollapseAction;

    /*
     * If items are dragged and dropped within this tree, then this flag
     * inhibits reselection on the removal (drag) action, thus ensuring that the
     * selected objects are the ones that were dropped.
     */
    private boolean fDragFromHere;

    private FormFilteredTree filteredTree;
    private L10nModel model;

    // The object removal action
    private L10nRemoveObjectAction removeObjectAction;

    private TreeViewer localesTree;

    /**
     * Constructs a new Locales tree section.
     * 
     * @param formPage The page that will hold this new tree section
     * @param parent The parent composite in the page that will contain the
     *            section widgets
     */
    public LocalesTreeSection(MTJFormPage formPage, Composite parent) {

        /*
         * Create a new section with a description area, and some buttons. The
         * null entries in the String array will become blank space separators
         * between the buttons.
         */
        super(formPage, parent, Section.DESCRIPTION, new String[] {
                MTJUIMessages.LocalesTreeSection_addLocale_button_label,
                MTJUIMessages.LocalesTreeSection_addEntry_button_label,
                MTJUIMessages.LocalesTreeSection_remove_button_label, null,
                MTJUIMessages.LocalesTreeSection_up_button_label,
                MTJUIMessages.LocalesTreeSection_down_button_label });

        // Initialize all the actions
        addLocaleAction = new L10nAddLocaleAction();
        addLocaleEntryAction = new L10nAddLocaleEntryAction();
        removeObjectAction = new L10nRemoveObjectAction();

    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.forms.AbstractFormPart#dispose()
     */
    @Override
    public void dispose() {
        MTJUIPlugin.getDefault().getLabelProvider().disconnect(this);
        super.dispose();
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.MTJSection#doGlobalAction(java.lang
     * .String)
     */
    @Override
    public boolean doGlobalAction(String actionId) {
        boolean cutAction = actionId.equals(ActionFactory.CUT.getId());

        if (cutAction || actionId.equals(ActionFactory.DELETE.getId())) {
            handleDeleteAction();
            return !cutAction;
        }

        if (actionId.equals(ActionFactory.PASTE.getId())) {
            doPaste();
            return true;
        }

        return false;
    }

    /**
     * Fire a selection change event and refresh the viewer's selection
     */
    public void fireSelection() {
        localesTree.setSelection(localesTree.getSelection());
    }

    /**
     * @return the selection of the tree section
     */
    public ISelection getSelection() {
        return localesTree.getSelection();
    }

    /**
     * Handle the dragging of objects out of this Locales.
     * 
     * @param itemsDragged The items dragged out of the Locales
     */
    public void handleDrag(List<IStructuredSelection> itemsDragged) {
        handleRemove(itemsDragged);
        fDragFromHere = false;
    }

    /**
     * Remove the items listed from the Locales.
     * 
     * @param itemsToRemove The list of items to remove from the Locales
     */
    public void handleRemove(List<IStructuredSelection> itemsToRemove) {
        if (!itemsToRemove.isEmpty()) { // Target the objects for removal
            removeObjectAction.setToRemove(itemsToRemove
                    .toArray(new L10nObject[itemsToRemove.size()]));

            // Run the removal action
            removeObjectAction.run();
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.MTJSection#modelChanged(org.eclipse
     * .mtj.core.model.IModelChangedEvent)
     */
    @Override
    public void modelChanged(IModelChangedEvent event) {
        // No need to call super, world changed event handled here
        if (event.getChangeType() == IModelChangedEvent.WORLD_CHANGED) {
            handleModelEventWorldChanged(event);
        } else if (event.getChangeType() == IModelChangedEvent.INSERT) {
            handleModelInsertType(event);
        } else if (event.getChangeType() == IModelChangedEvent.REMOVE) {
            handleModelRemoveType(event);
        } else if ((event.getChangeType() == IModelChangedEvent.CHANGE)
                && event.getChangedProperty().equals(
                        IDocumentElementNode.F_PROPERTY_CHANGE_TYPE_SWAP)) {
            handleModelChangeTypeSwap(event);
        } else if (event.getChangeType() == IModelChangedEvent.CHANGE) {
            handleModelChangeType(event);
        }

    }

    /**
     * Perform a drop of the specified objects on the target in the widget
     * 
     * @param currentTarget The object that the drop will occur near/on
     * @param dropped The dropped objects
     * @param location The location of the drop relative to the target
     * @return <code>true</code> if the drop was successful
     */
    public boolean performDrop(Object currentTarget, Object dropped,
            int location) {

        return false;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.forms.AbstractFormPart#refresh()
     */
    @Override
    public void refresh() {
        L10nModel model = (L10nModel) getPage().getModel();
        model.validate();

        localesTree.setInput(model);
        localesTree.expandToLevel(2);
        localesTree.setSelection(new StructuredSelection(model.getLocales()),
                true);
        getManagedForm().fireSelectionChanged(this, localesTree.getSelection());
        super.refresh();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.forms.AbstractFormPart#setFormInput(java.lang.Object)
     */
    @Override
    public boolean setFormInput(Object object) {
        if (object instanceof L10nObject) { // Select the item in the tree
            ((L10nObject) object).validate();
            localesTree.setSelection(new StructuredSelection(object), true);

            // Verify that something was actually selected
            ISelection selection = localesTree.getSelection();
            if ((selection != null) && !selection.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param selection the new selection for the tree section
     */
    public void setSelection(ISelection selection) {
        localesTree.setSelection(selection);
    }

    /**
     * Update the buttons in the section based on the current selection
     */
    @SuppressWarnings("unchecked")
    public void updateButtons() {
        if (!model.isEditable()) {
            return;
        }

        // 'Add' actions are enabled if any object in the selection can
        // be added to
        boolean canAddLocale = false;
        boolean canAddEntry = false;

        // 'Remove' is enabled if any object in the selection is removable
        boolean canRemove = false;

        IStructuredSelection sel = (IStructuredSelection) localesTree
                .getSelection();

        // 'Up' is disabled if any object in the selection can't be moved up.
        boolean canMoveUp = sel.size() == 1;

        // 'Down' is disabled if any object in the selection can't be moved
        // down.
        boolean canMoveDown = sel.size() == 1;

        for (Iterator<L10nObject> iter = sel.iterator(); iter.hasNext();) {
            L10nObject l10nObject = iter.next();

            if (l10nObject != null) {
                if (l10nObject.canBeRemoved()) {
                    canRemove = true;
                }

                L10nObject parent = l10nObject.getParent();

                if (sel.size() == 1) {
                    if (l10nObject.getType() == IL10nConstants.TYPE_LOCALES) {
                        canAddLocale = true;
                        canAddEntry = false;
                    } else if (l10nObject.getType() == IL10nConstants.TYPE_LOCALE) {
                        canAddLocale = false;
                        canAddEntry = true;
                    } else if (l10nObject.getType() == IL10nConstants.TYPE_ENTRY) {
                        canAddLocale = false;
                        canAddEntry = false;
                    }
                }

                // Semantic rule:
                // You cannot rearrange the Locales root itself
                if (l10nObject.getType() == IL10nConstants.TYPE_LOCALES) {
                    canMoveUp = false;
                    canMoveDown = false;
                } else {
                    if (parent != null) {
                        L10nObject parentNode = parent;
                        if (parentNode.isFirstChildNode(l10nObject,
                                L10nObject.class)) {
                            canMoveUp = false;
                        }

                        if (parentNode.isLastChildNode(l10nObject,
                                L10nObject.class)) {
                            canMoveDown = false;
                        }
                    }
                }
            } else { // How anyone can select a null object, I don't know.
                // However, if it happens, disable all buttons.
                canAddLocale = false;
                canAddEntry = false;
                canRemove = false;
                canMoveUp = false;
                canMoveDown = false;

                break;
            }
        }

        getTreePart().setButtonEnabled(F_BUTTON_ADD_LOCALE, canAddLocale);
        getTreePart().setButtonEnabled(F_BUTTON_ADD_ENTRY, canAddEntry);
        getTreePart().setButtonEnabled(F_BUTTON_REMOVE, canRemove);
        getTreePart().setButtonEnabled(F_BUTTON_UP, canMoveUp);
        getTreePart().setButtonEnabled(F_BUTTON_DOWN, canMoveDown);
    }

    /**
     * Adds a link (with hand cursor) for tree 'Collapse All' action, which
     * collapses the Locales tree down to the second level
     * 
     * @param section The section that the toolbar will belong to
     * @param toolkit The toolkit that will be used to make the toolbar
     */
    private void createSectionToolbar(Section section, FormToolkit toolkit) {
        ToolBarManager toolBarManager = new ToolBarManager(SWT.FLAT);
        ToolBar toolbar = toolBarManager.createControl(section);

        final Cursor handCursor = new Cursor(Display.getCurrent(),
                SWT.CURSOR_HAND);
        toolbar.setCursor(handCursor);
        // Cursor needs to be explicitly disposed
        toolbar.addDisposeListener(new DisposeListener() {
            public void widgetDisposed(DisposeEvent e) {
                if ((handCursor != null) && (handCursor.isDisposed() == false)) {
                    handCursor.dispose();
                }
            }
        });

        // Add collapse action to the tool bar
        fCollapseAction = new CollapseAction(localesTree,
                MTJUIMessages.LocalesTreeSection_collapseAllAction_Text, 1,
                model.getLocales());
        toolBarManager.add(fCollapseAction);

        toolBarManager.update(true);
        section.setTextClient(toolbar);
    }

    /**
     * Create the tree widget that will contain the Locales
     * 
     * @param container The container of the tree widget
     * @param toolkit The toolkit used to create the tree
     */
    private void createTree(Composite container, FormToolkit toolkit) {
        TreePart treePart = getTreePart();
        createViewerPartControl(container, SWT.MULTI, 2, toolkit);

        localesTree = treePart.getTreeViewer();
        localesTree.setContentProvider(new L10nContentProvider());
        localesTree.setLabelProvider(MTJUIPlugin.getDefault()
                .getLabelProvider());

        MTJUIPlugin.getDefault().getLabelProvider().connect(this);

        createTreeListeners();
    }

    /**
     * Create the action listeners for the tree.
     */
    private void createTreeListeners() {
        // Create listener for the outline view 'link with editor' toggle button
        localesTree
                .addPostSelectionChangedListener(getPage().getMTJEditor().new MTJFormEditorChangeListener());
    }

    /**
     * Add the addition actions (Topic, Link, Anchor) to the specified submenu
     * 
     * @param submenu The submenu to add the addition actions to
     * @param l10nObject The object that the additions would occur relative to
     */
    private void fillContextMenuAddActions(MenuManager submenu,
            L10nObject l10nObject) {

        if ((l10nObject != null) && l10nObject.canBeParent()) {
            if (l10nObject instanceof L10nLocales) {
                // Add the 'Add Locale' action to the sub-menu
                addLocaleAction.setParentObject(l10nObject);
                addLocaleAction.setEnabled(model.isEditable());
                submenu.add(addLocaleAction);
            } else if (l10nObject instanceof L10nLocale) {
                // Add the 'Add Entry' action to the sub-menu
                addLocaleEntryAction.setParentObject(l10nObject);
                addLocaleEntryAction.setEnabled(model.isEditable());
                submenu.add(addLocaleEntryAction);
            }
        }
    }

    /**
     * Add the remove action to the context menu.
     * 
     * @param manager The context menu to add the remove action to
     * @param l10nObject The object that would be targeted for removal
     */
    private void fillContextMenuRemoveAction(IMenuManager manager,
            L10nObject l10nObject) {
        // Add to the main context menu

        // Delete task object action
        removeObjectAction.setToRemove(l10nObject);
        manager.add(removeObjectAction);

        removeObjectAction.setEnabled(l10nObject.canBeRemoved()
                && model.isEditable());
    }

    private void fillContextMenuShowInAction(IMenuManager manager) {
        String showInLabel = MTJUIMessages.LocalesTreeSection_showInLabel;

        // Add a label for the key binding for Show In action, if one exists
        IBindingService bindingService = (IBindingService) PlatformUI
                .getWorkbench().getAdapter(IBindingService.class);
        if (bindingService != null) {
            String keyBinding = bindingService
                    .getBestActiveBindingFormattedFor("org.eclipse.ui.navigate.showInQuickMenu"); //$NON-NLS-1$
            if (keyBinding != null) {
                showInLabel += '\t' + keyBinding;
            }
        }

        // Add the "Show In" action and its contributions
        IMenuManager showInMenu = new MenuManager(showInLabel);
        showInMenu.add(ContributionItemFactory.VIEWS_SHOW_IN.create(getPage()
                .getSite().getWorkbenchWindow()));

        manager.add(showInMenu);
    }

    /**
     * Handle the addition of an object by preparing and running the specified
     * action.
     * 
     * @param action The action to run for the addition
     */
    private void handleAddAction(L10nAddObjectAction action) {

        // Currently, all additions in the Locales editor are semantically
        // similar
        // Thus, all addition operations can follow the same procedure

        ISelection sel = localesTree.getSelection();
        Object object = ((IStructuredSelection) sel).getFirstElement();
        if (object == null) {
            return;
        }

        L10nObject l10nObject = (L10nObject) object;

        if (l10nObject.canBeParent()) {
            // If the selected object can be a parent, then add
            // the new object as a child of this object
            action.setParentObject(l10nObject);
            action.run();
        } else { // If the selected object cannot be a parent, then add
            // the new object as a direct sibling of this object
            action.setParentObject(l10nObject.getParent());
            action.setTargetObject(l10nObject);
            action.run();
        }
        model.getLocales().validate();
        FormEditor editor = getPage().getEditor();
        if (editor instanceof LocalizationDataEditor) {
            ((LocalizationDataEditor) editor).updateMessageManager(model
                    .getLocales());
        }
        localesTree.refresh();
    }

    /**
     * Remove the selected objects from the Locales tree
     */
    @SuppressWarnings("unchecked")
    private void handleDeleteAction() {
        ArrayList<IStructuredSelection> objects = new ArrayList<IStructuredSelection>(
                ((IStructuredSelection) localesTree.getSelection()).toList());
        boolean beep = false;

        // Iterate through the list of selected objects, removing ones
        // that cannot be removed
        for (Iterator<IStructuredSelection> i = objects.iterator(); i.hasNext();) {
            Object object = i.next();
            if (object instanceof L10nObject) {
                L10nObject l10nObject = (L10nObject) object;

                if (!l10nObject.canBeRemoved()) {
                    i.remove();
                    beep = true;
                }
            }
        }

        if (beep) { // If any object cannot be removed, beep to notify the user
            Display.getCurrent().beep();
        }

        // Remove the remaining objects
        handleRemove(objects);

        model.getLocales().validate();
        FormEditor editor = getPage().getEditor();
        if (editor instanceof LocalizationDataEditor) {
            ((LocalizationDataEditor) editor).updateMessageManager(model
                    .getLocales());
        }
        localesTree.refresh();
    }

    /**
     * Handle an update to a L10nObject's properties
     * 
     * @param event the update event
     */
    private void handleModelChangeType(final IModelChangedEvent event) {

        localesTree.getControl().getDisplay().asyncExec(new Runnable() {
            public void run() {
                if (localesTree.getControl().isDisposed()) {
                    return;
                }

                model.getLocales().validate();
                
                // Get the changed object
                Object[] objects = event.getChangedObjects();
                L10nObject object = (L10nObject) objects[0];

                if (object != null) { // Update the element in the tree viewer
                    localesTree.update(object, null);
                    localesTree.refresh();
                }
            }
        });
    }

    /**
     * @param event
     */
    private void handleModelChangeTypeSwap(IModelChangedEvent event) {
        // Swap event
        // Get the changed object
        Object[] objects = event.getChangedObjects();
        L10nObject object = (L10nObject) objects[0];

        if (object != null) { // Update the element in the tree viewer
            object.validate();
            localesTree.refresh(object);
        }
    }

    /**
     * The model is stale, refresh the UI
     * 
     * @param event The world-change event
     */
    private void handleModelEventWorldChanged(IModelChangedEvent event) {
        markStale();
    }

    /**
     * Handle insertions in the model
     * 
     * @param event the insertion event
     */
    private void handleModelInsertType(IModelChangedEvent event) {
        // Insert event
        Object[] objects = event.getChangedObjects();
        L10nObject object = (L10nObject) objects[0];
        if (object != null) {
            object.validate();
            localesTree.refresh(object.getParent());
            // Select the new object in the tree
            localesTree.setSelection(new StructuredSelection(object), true);
        }
    }

    /**
     * Handle removals in the model
     * 
     * @param event the removal event
     */
    private void handleModelRemoveType(IModelChangedEvent event) {
        // Remove event
        Object[] objects = event.getChangedObjects();
        L10nObject object = (L10nObject) objects[0];
        if (object != null) {
            if (object.getType() != IL10nConstants.TYPE_LOCALES) {
                handleTaskObjectRemove(object);
            }
        }
    }

    /**
     * Move an object within the Locales.
     * 
     * @param positionFlag The direction that the object will move
     */
    @SuppressWarnings("unchecked")
    private void handleMoveAction(int positionFlag) {
        IStructuredSelection sel = (IStructuredSelection) localesTree
                .getSelection();

        for (Iterator<Object> iter = sel.iterator(); iter.hasNext();) {
            Object object = iter.next();
            if (object == null) {
                return;
            } else if (object instanceof L10nObject) {
                L10nObject l10nObject = (L10nObject) object;
                L10nObject parent = l10nObject.getParent();

                // Determine the parent type
                if (parent != null) { // Move the object up or down one position
                    parent.moveChildNode(l10nObject, positionFlag, true);
                }
            }
        }
    }

    /**
     * An object was removed, update the UI to respond to the removal
     * 
     * @param object The object that was removed
     */
    private void handleTaskObjectRemove(L10nObject object) {
        // Remove the item
        localesTree.remove(object);

        // Select the appropriate object
        L10nObject l10nObject = removeObjectAction.getNextSelection();
        if (l10nObject == null) {
            l10nObject = object.getParent();
        }

        if (l10nObject.equals(object.getParent())) {
            localesTree.refresh(object.getParent());
        }

        if (!fDragFromHere) {
            localesTree.setSelection(new StructuredSelection(l10nObject), true);
        }
    }

    /**
     * Initialize the tree viewer widget and its buttons.
     */
    private void initializeTreeViewer() {
        if (model == null) {
            return;
        }

        // Connect the tree viewer to the Locales model
        localesTree.setInput(model);
        L10nLocales localesModel = model.getLocales();

        // Nodes can always be added to the root Locales node
        getTreePart().setButtonEnabled(F_BUTTON_ADD_LOCALE, isEditable());
        getTreePart().setButtonEnabled(F_BUTTON_ADD_ENTRY, isEditable());

        // Set to false because initial node selected is the root Locales node
        getTreePart().setButtonEnabled(F_BUTTON_REMOVE, false);
        // Set to false because initial node selected is the root Locales node
        getTreePart().setButtonEnabled(F_BUTTON_UP, false);
        // Set to false because initial node selected is the root Locales node
        getTreePart().setButtonEnabled(F_BUTTON_DOWN, false);

        // Initially, the root Locales element is selected
        localesTree.setSelection(new StructuredSelection(localesModel), true);
        localesTree.expandToLevel(2);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.StructuredViewerSection#buttonSelected
     * (int)
     */
    @Override
    protected void buttonSelected(int index) {
        switch (index) {
        case F_BUTTON_ADD_LOCALE:
            handleAddAction(addLocaleAction);
            break;
        case F_BUTTON_ADD_ENTRY:
            handleAddAction(addLocaleEntryAction);
            break;
        case F_BUTTON_REMOVE:
            handleDeleteAction();
            break;
        case F_BUTTON_UP:
            handleMoveAction(F_UP_FLAG);
            break;
        case F_BUTTON_DOWN:
            handleMoveAction(F_DOWN_FLAG);
            break;
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.StructuredViewerSection#canPaste(java
     * .lang.Object, java.lang.Object[])
     */
    @Override
    protected boolean canPaste(Object targetObject, Object[] sourceObjects) {
        return true;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.MTJSection#createClient(org.eclipse
     * .ui.forms.widgets.Section, org.eclipse.ui.forms.widgets.FormToolkit)
     */
    @Override
    protected void createClient(Section section, FormToolkit toolkit) {
        // Get the model
        model = (L10nModel) getPage().getModel();
        model.validate();

        // Create a container in the section
        Composite container = createClientContainer(section, 2, toolkit);
        // Create a Locales tree in the new container
        createTree(container, toolkit);
        toolkit.paintBordersFor(container);
        section.setText(MTJUIMessages.LocalesTreeSection_title);
        section.setDescription(MTJUIMessages.LocalesTreeSection_description);
        section.setClient(container);

        initializeTreeViewer();
        createSectionToolbar(section, toolkit);

        // Create the adapted listener for the filter entry field
        filteredTree.createUIListenerEntryFilter(this);

        L10nLocales locales = model.getLocales();
        locales.validate();

        FormEditor editor = getPage().getEditor();
        if (editor instanceof LocalizationDataEditor) {
            ((LocalizationDataEditor) editor).updateMessageManager(locales);
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.TreeSection#createTreeViewer(org.eclipse
     * .swt.widgets.Composite, int)
     */
    @Override
    protected TreeViewer createTreeViewer(Composite parent, int style) {
        filteredTree = new FormFilteredTree(parent, style, new PatternFilter());
        parent.setData("filtered", Boolean.TRUE); //$NON-NLS-1$
        return filteredTree.getViewer();
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.StructuredViewerSection#doPaste(java
     * .lang.Object, java.lang.Object[])
     */
    @Override
    protected void doPaste(Object targetObject, Object[] sourceObjects) {
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.StructuredViewerSection#fillContextMenu
     * (org.eclipse.jface.action.IMenuManager)
     */
    @Override
    protected void fillContextMenu(IMenuManager manager) {
        // Get the current selection
        ISelection selection = localesTree.getSelection();
        Object object = ((IStructuredSelection) selection).getFirstElement();
        // Has to be null or a Locales object
        L10nObject l10nObject = (L10nObject) object;

        if (l10nObject != null) {
            boolean emptyMenu = true;

            if (l10nObject.canBeParent()) {
                // Create the "New" sub-menu
                MenuManager submenu = new MenuManager(
                        MTJUIMessages.LocalesTreeSection_new_submenu_text);
                // Populate the "New" sub-menu
                fillContextMenuAddActions(submenu, l10nObject);
                // Add the "New" sub-menu to the main context menu
                manager.add(submenu);
                emptyMenu = false;
            }

            if (!emptyMenu) { // Add a separator to the main context menu
                manager.add(new Separator());
            }
        }

        // Add clipboard actions
        getPage().getMTJEditor().getContributor().contextMenuAboutToShow(
                manager);
        manager.add(new Separator());

        if (l10nObject != null) { // Add the Remove action and Show In action if
            // an object is selected
            fillContextMenuRemoveAction(manager, l10nObject);
            manager.add(new Separator());

            fillContextMenuShowInAction(manager);
            manager.add(new Separator());
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.TreeSection#handleDoubleClick(org.
     * eclipse.jface.viewers.IStructuredSelection)
     */
    @Override
    protected void handleDoubleClick(IStructuredSelection selection) {
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.TreeSection#selectionChanged(org.eclipse
     * .jface.viewers.IStructuredSelection)
     */
    @Override
    protected void selectionChanged(IStructuredSelection selection) {
        getPage().getMTJEditor().setSelection(selection);
        updateButtons();
    }
}
