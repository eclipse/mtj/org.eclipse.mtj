/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)      - initial implementation
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874] 
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.configurations;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.internal.ui.MTJUIMessages;

/**
 * Wizard for Configuration add and edit
 * 
 * @author wangf
 */
public class ConfigAddAndEditWizard extends Wizard {

    private ConfigAddAndEditWizardPage wizardPage;
    private MTJRuntimeList configurations;
    private MTJRuntime currentConfig;

    public ConfigAddAndEditWizard(MTJRuntimeList configurations,
            MTJRuntime currentConfig) {
        this.configurations = configurations;
        init(currentConfig);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#addPages()
     */
    @Override
    public void addPages() {
        wizardPage = new ConfigAddAndEditWizardPage(configurations,
                currentConfig);
        addPage(wizardPage);
    }

    public MTJRuntime getConfiguration() {
        return wizardPage.getConfiguration();
    }

    private void init(MTJRuntime currentConfig) {
        this.currentConfig = currentConfig;
        setNeedsProgressMonitor(true);
        if (currentConfig == null) {
            setWindowTitle(MTJUIMessages.Configuration_AddConfiguration);
        } else {
            setWindowTitle(MTJUIMessages.Configuration_EditConfiguration);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performCancel()
     */
    @Override
    public boolean performCancel() {
        wizardPage.performCancel();
        return true;
    }


    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        wizardPage.performFinish();
        return true;
    }

    /**
     * @param midletSuiteProject
     */
    public void setMidletSuiteProject(IMTJProject midletSuiteProject) {
        wizardPage.setMidletSuiteProject(midletSuiteProject);
    }
}
