/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.internal.ui.preferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.internal.core.PreferenceAccessor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

/**
 * A FieldEditor implementation for managing a multi-valued list.
 * 
 * @author Craig Setera
 */
public class MultiValuedTableFieldEditor extends FieldEditor {
    /**
     * Implementation of the ICellModifier interface.
     */
    private class CellModifier implements ICellModifier {
        /**
         * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object,
         *      java.lang.String)
         */
        public boolean canModify(Object element, String property) {
            return (property != null) && property.equals(COLUMN_PROPS[0]);
        }

        /**
         * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object,
         *      java.lang.String)
         */
        public Object getValue(Object element, String property) {
            Object value = null;

            if (canModify(element, property)) {
                value = element.toString();
            }

            return value;
        }

        /**
         * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object,
         *      java.lang.String, java.lang.Object)
         */
        public void modify(Object element, String property, Object value) {
            if (element instanceof TableItem) {
                Object data = ((TableItem) element).getData();
                String newValue = (String) value;
                if (data instanceof StringHolder) {
                    ((StringHolder) data).value = newValue;
                    tableViewer.update(data, new String[] { property });
                }
            }
        }
    }

    /**
     * Simple holder class around a String Proguard Keep Expression. This holder
     * is necessary for interacting with the Cell modifier code.
     */
    private static class StringHolder {
        public String value;

        /**
         * Construct a new StringHolder.
         * 
         * @param value
         */
        public StringHolder(String value) {
            this.value = value;
        }

        /**
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return value;
        }
    }

    /**
     * Implementation of the table's content provider.
     */
    private class TableContentProvider implements IStructuredContentProvider {
        /**
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /**
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            return holders;
        }

        /**
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer,
         *      java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    /**
     * Implementation of the table's label provider.
     */
    private static class TableLabelProvider extends LabelProvider implements
            ITableLabelProvider {
        /**
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
         *      int)
         */
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        /**
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
         *      int)
         */
        public String getColumnText(Object element, int columnIndex) {
            return (columnIndex == 0) ? element.toString() : "";
        }
    }

    // The columns in the table
    private static final String[] COLUMNS = { "Value", };

    // The cell editor property names
    private static final String[] COLUMN_PROPS = { "propNames", };

    // UI Widgets
    private Group group;
    private TableViewer tableViewer;

    // Content holder
    private StringHolder[] holders;

    /**
     * Construct a new field editor.
     * 
     * @param name
     * @param labelText
     * @param parent
     */
    public MultiValuedTableFieldEditor(String name, String labelText,
            Composite parent) {
        super(name, labelText, parent);
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#getNumberOfControls()
     */
    @Override
    public int getNumberOfControls() {
        return 1;
    }

    /**
     * Add a new keep expression to the viewer.
     */
    private void addNewPropertyName() {
        // Create a new array with an extra slot for the new
        // expression
        StringHolder[] newExpressionArray = new StringHolder[holders.length + 1];
        System.arraycopy(holders, 0, newExpressionArray, 0, holders.length);

        // Create a new expression with the default value
        newExpressionArray[holders.length] = new StringHolder("New Value");

        // Refresh everything
        setExcludedNames(newExpressionArray);
    }

    /**
     * Convert the specified array of string holders into a delimited
     * multi-value string.
     * 
     * @param holders
     * @return
     */
    private String getMultiValuedString(StringHolder[] holders) {
        // Convert back to a pipe delimited string
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < holders.length; i++) {
            StringHolder holder = holders[i];
            if (i != 0) {
                sb.append("|");
            }
            sb.append(holder.value);
        }

        return sb.toString();
    }

    /**
     * Return the values parsed out from the specified multi-value string.
     * 
     * @param multiValueString
     * @return
     */
    private StringHolder[] getParsedValues(String multiValueString) {
        StringTokenizer st = new StringTokenizer(multiValueString,
                PreferenceAccessor.MULTI_VALUE_SEPARATOR);

        int count = st.countTokens();
        StringHolder[] holders = new StringHolder[count];
        for (int i = 0; i < count; i++) {
            holders[i] = new StringHolder(st.nextToken());
        }

        return holders;
    }

    /**
     * Remove the selected keep expressions from the viewer.
     */
    private void removeSelectedNames() {
        int[] indices = tableViewer.getTable().getSelectionIndices();

        List<?> temp = new ArrayList<Object>(Arrays.asList(holders));

        for (int i = indices.length; i > 0; i--) {
            int index = indices[i - 1];
            temp.remove(index);
        }

        setExcludedNames(temp.toArray(new StringHolder[temp.size()]));
    }

    /**
     * Set the current property names being handled.
     * 
     * @param newPropertyNames
     */
    private void setExcludedNames(StringHolder[] newPropertyNames) {
        holders = newPropertyNames;
        tableViewer.setInput(holders);
        tableViewer.refresh();
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#adjustForNumColumns(int)
     */
    @Override
    protected void adjustForNumColumns(int numColumns) {
        ((GridData) group.getLayoutData()).horizontalSpan = numColumns;
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#doFillIntoGrid(org.eclipse.swt.widgets.Composite,
     *      int)
     */
    @Override
    protected void doFillIntoGrid(Composite parent, int numColumns) {
        // Create the group to hold the components
        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.horizontalSpan = numColumns;

        group = new Group(parent, SWT.NONE);
        group.setLayout(new GridLayout(2, false));
        group.setLayoutData(gridData);
        group.setText(getLabelText());

        // Create the Table viewer
        tableViewer = new TableViewer(group, SWT.SINGLE | SWT.H_SCROLL
                | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
        tableViewer.setColumnProperties(COLUMN_PROPS);
        tableViewer.setCellModifier(new CellModifier());

        // Layout the table within the grid
        final Table table = tableViewer.getTable();
        table.setHeaderVisible(false);
        table.setLinesVisible(true);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));

        // Add the add and remove buttons
        Composite buttonHolder = new Composite(group, SWT.NONE);
        buttonHolder.setLayout(new GridLayout(1, true));
        final Button addButton = new Button(buttonHolder, SWT.PUSH);
        addButton.setText("Add");
        addButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        addButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                addNewPropertyName();
            }
        });
        new Label(buttonHolder, SWT.NONE);

        final Button removeButton = new Button(buttonHolder, SWT.PUSH);
        removeButton.setText("Remove");
        removeButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        removeButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                removeSelectedNames();
            }
        });

        // Hook up a table selection listener to handle
        // remove button enablement
        table.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                TableItem selected = (TableItem) e.item;
                removeButton.setEnabled(selected.getParent()
                        .getSelectionCount() > 0);
            }
        });

        tableViewer.setCellEditors(new CellEditor[] {
                new TextCellEditor(table), null });

        // Create the columns
        final TableColumn[] columns = new TableColumn[COLUMNS.length];
        for (int i = 0; i < COLUMNS.length; i++) {
            columns[i] = new TableColumn(table, SWT.NONE);
            columns[i].setText(COLUMNS[i]);
        }

        // Attach a listener for resize events
        table.addControlListener(new ControlAdapter() {
            /**
             * Sent when the size (width, height) of a control changes. The
             * default behavior is to do nothing.
             * 
             * @param e an event containing information about the resize
             */
            @Override
            public void controlResized(ControlEvent e) {
                // Always force our single column to be the full width
                // of the table.
                columns[0].setWidth(table.getSize().x);
            }
        });

        // Set the content providers
        tableViewer.setContentProvider(new TableContentProvider());
        tableViewer.setLabelProvider(new TableLabelProvider());
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#doLoad()
     */
    @Override
    protected void doLoad() {
        if (group != null) {
            String propNames = getPreferenceStore().getString(
                    getPreferenceName());
            setExcludedNames(getParsedValues(propNames));
        }
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#doLoadDefault()
     */
    @Override
    protected void doLoadDefault() {
        if (group != null) {
            String propNames = getPreferenceStore().getDefaultString(
                    getPreferenceName());
            setExcludedNames(getParsedValues(propNames));
        }
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#doStore()
     */
    @Override
    protected void doStore() {
        String prefValue = getMultiValuedString(holders);
        getPreferenceStore().setValue(getPreferenceName(), prefValue);
    }
}
