/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 *     David Marques(Motorola) - Enabling MIDlets ordering.
 */
package org.eclipse.mtj.internal.ui.editors.jad.form.pages;

import java.util.ArrayList;

import org.eclipse.mtj.internal.ui.editors.IModel;
import org.eclipse.mtj.internal.ui.editors.IModelListener;
import org.eclipse.mtj.internal.ui.forms.blocks.NamedObject;

/**
 * @author Diego Madruga Sandin
 */
public class MidletsModel implements IModel {

    public static enum Direction {
        UP(-1), DOWN(+1);
        
        int value;
        
        /**
         * Creates a Direction.
         * 
         * @param value direction value.
         */
        Direction(int value) {
            this.value = value;
        }
        
        /**
         * Gets the direction value.
         * 
         * @return value
         */
        public int getValue() {
            return this.value;
        }
    }
	
    private ArrayList<IModelListener> modelListeners;

    private ArrayList<NamedObject> objects;

    /**
     * Create a new MIDlets model.
     * 
     * @param midlets the initial MIDlets to be added to the model.
     */
    public MidletsModel(final NamedObject[] midlets) {
        modelListeners = new ArrayList<IModelListener>();
        objects = new ArrayList<NamedObject>();

        initialize(midlets);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.internal.ui.editors.jad.form.pages.IModel#add(org.eclipse.mtj.internal.ui.editors.jad.form.pages.block.NamedObject[],
     *      boolean)
     */
    public void add(NamedObject[] objs, boolean notify) {
        for (NamedObject element : objs) {
            objects.add(element);
            element.setModel(this);
        }
        if (notify) {
            fireModelChanged(objs, IModelListener.ADDED, "");
        }
    }

    /**
	 * Reorders the object position according to the specified direction.
	 * 
	 * @param obj
	 *            object to reorder.
	 * @param direction
	 *            ordering direction.
	 */
	public void reorder(NamedObject obj, Direction direction) {
		for (int i = 0; i < objects.size(); i++) {
			NamedObject target = objects.get(i);
			if (target == obj) {
				if ((direction == Direction.DOWN && i == objects.size() - 1)
						|| (direction == Direction.UP && i == 0x00)) {
					return;
				}

				objects.remove(i);
				i += direction.getValue();
				objects.add(i, obj);

				fireModelChanged(new Object[] { obj }, IModelListener.ADDED, "");
				break;
			}
		}
	}
    
    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.internal.ui.editors.jad.form.pages.IModel#addModelListener(org.eclipse.mtj.internal.ui.editors.jad.form.pages.block.IModelListener)
     */
    public void addModelListener(IModelListener listener) {
        if (!modelListeners.contains(listener)) {
            modelListeners.add(listener);
        }
    }

    public void clear() {
        objects.clear();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.internal.ui.editors.jad.form.pages.IModel#fireModelChanged(java.lang.Object[],
     *      java.lang.String, java.lang.String)
     */
    public void fireModelChanged(Object[] objects, String type, String property) {
        for (int i = 0; i < modelListeners.size(); i++) {
            (modelListeners.get(i)).modelChanged(objects, type, property);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.internal.ui.editors.jad.form.pages.IModel#getContents()
     */
    public Object[] getContents() {
        return objects.toArray();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.internal.ui.editors.jad.form.pages.IModel#remove(org.eclipse.mtj.internal.ui.editors.jad.form.pages.block.NamedObject[],
     *      boolean)
     */
    public void remove(NamedObject[] objs, boolean notify) {
        for (NamedObject element : objs) {
            objects.remove(element);
            element.setModel(null);
        }
        if (notify) {
            fireModelChanged(objs, IModelListener.REMOVED, "");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.internal.ui.editors.jad.form.pages.IModel#removeModelListener(org.eclipse.mtj.internal.ui.editors.jad.form.pages.block.IModelListener)
     */
    public void removeModelListener(IModelListener listener) {
        modelListeners.remove(listener);
    }

    /**
     * @param objects
     */
    private void initialize(NamedObject[] objects) {

        if (objects != null) {
            add(objects, false);
        }
    }
}
