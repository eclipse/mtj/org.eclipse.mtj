/**
 * Copyright (c) 2004,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 *     David Marques(Motorola)   - Updating dialog to support already resolved
 *                                 passwords.
 */
package org.eclipse.mtj.internal.ui.statusHandlers;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.mtj.internal.core.build.sign.SignaturePasswords;
import org.eclipse.mtj.internal.ui.dialog.SigningPasswordsDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * This class is a bit of a hack. Status handlers normally are used to report
 * status back to the user. This one is used to actually prompt the user for
 * passwords as required. The
 * org.eclipse.mtj.core.internal.signing.SignatureUtils class calls this through
 * the status handler mechanism when it needs to obtain passwords from the user.
 * 
 */
public class SignaturePasswordHandler implements IStatusHandler {
    
    /**
     * @see org.eclipse.debug.core.IStatusHandler#handleStatus(org.eclipse.core.runtime.IStatus,
     *      java.lang.Object)
     */
    public Object handleStatus(final IStatus status, Object source) {
        Display display = Display.getCurrent();
        Object[] array  = (Object[]) source;
        
        DialogRunner runner = new DialogRunner((IProject) array[0], (String)array[1], (String)array[2]);

        display.syncExec(runner);

        return (runner.getPasswords());
    }

    /**
     *
     */
    private static class DialogRunner implements Runnable {
        IProject project;
        SignaturePasswords passwords;

        /**
         * Creates a DialogRunner instance.
         * 
         * @param project target project.
         * @param keyStorePass keystore password.
         * @param privateKeyPass private key password.
         */
        public DialogRunner(IProject project, String keyStorePass, String privateKeyPass) {
            this.project = project;
            passwords = new SignaturePasswords();
            passwords.setKeystorePassword(keyStorePass);
            passwords.setKeyPassword(privateKeyPass);
        }

        /* (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        public void run() {
            Display display = Display.getCurrent();
            Shell shell = display.getActiveShell();
            if (shell == null) {
                shell = new Shell(display, SWT.NONE);
            }

            SigningPasswordsDialog dlg = new SigningPasswordsDialog(shell,
                    project);
            dlg.setKeystorePassword(passwords.getKeystorePassword());
            dlg.setKeyPassword(passwords.getKeyPassword());
            
            if (dlg.open() != IDialogConstants.OK_ID) {
                passwords = null;
            } else {
                passwords = new SignaturePasswords();
                passwords.setKeystorePassword(dlg.getKeystorePassword());
                passwords.setKeyPassword(dlg.getKeyPassword());
            }
        }

        /**
         * @return
         */
        public SignaturePasswords getPasswords() {
            return (passwords);
        }
    }
}
