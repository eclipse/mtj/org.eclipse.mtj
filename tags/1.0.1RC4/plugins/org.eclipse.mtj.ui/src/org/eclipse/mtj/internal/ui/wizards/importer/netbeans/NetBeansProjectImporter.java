/**
 * Copyright (c) 2008 Motorola Inc.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola) - Initial implementation                            
 */
package org.eclipse.mtj.internal.ui.wizards.importer.netbeans;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ILeveledImportStructureProvider;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ProjectImporter;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ProjectImporterMessage;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ProjectRecord;
import org.eclipse.mtj.internal.ui.wizards.importer.common.StatusUtil;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Class used to import projects from NetBeans
 * 
 * @author wha006
 */
public class NetBeansProjectImporter extends ProjectImporter {

    private static final String NB_PROJECT_PROPERTIES = "project.properties"; //$NON-NLS-1$
    private static final String NB_DEVICE_NAME = "platform.device"; //$NON-NLS-1$
    private static final String NB_DEVICE_GROUP = "platform.active.description"; //$NON-NLS-1$
    private static final String MTJ_APPLICATION_DESCRIPTOR = "Application Descriptor"; //$NON-NLS-1$
    private static final String JAD_EXTENSION = ".jad"; //$NON-NLS-1$
    private static final String NB_BUILD_XML = "build.xml"; //$NON-NLS-1$
    private static final String NB_DIST_FOLDER = "dist"; //$NON-NLS-1$
    private static final String NB_METADATA_FOLDER = "nbproject"; //$NON-NLS-1$
    private static final String NB_BUILD_FOLDER = "build"; //$NON-NLS-1$
    private static final String NB_PROJECT_NAME_KEY = "name"; //$NON-NLS-1$
    private static final String NB_PROJECT_TYPE_MOBILITY = "org.netbeans.modules.kjava.j2meproject"; //$NON-NLS-1$
    private static final String NB_PROJECT_TYPE_KEY = "type"; //$NON-NLS-1$
    private static final String NB_PROJECT_DESCRIPTOR_FILE = "nbproject/project.xml"; //$NON-NLS-1$
    private static final HashMap<String, String> JAD_MAPPING = new HashMap<String, String>();

    static {
        JAD_MAPPING.put("manifest.others", null); //$NON-NLS-1$
        JAD_MAPPING.put("platform.configuration", //$NON-NLS-1$
                IJADConstants.JAD_MICROEDITION_CONFIG);
        JAD_MAPPING.put("platform.profile", //$NON-NLS-1$
                IJADConstants.JAD_MICROEDITION_PROFILE);
        JAD_MAPPING.put("dist.jar", //$NON-NLS-1$
                IJADConstants.JAD_MIDLET_JAR_URL);
        JAD_MAPPING.put("manifest.midlets", null); //$NON-NLS-1$
        JAD_MAPPING.put("manifest.pushregistry", null); //$NON-NLS-1$
        JAD_MAPPING.put("manifest.apipermissions", null); //$NON-NLS-1$
        JAD_MAPPING.put("manifest.jad", null); //$NON-NLS-1$
    }

    @Override
    public String getProjectDescriptorFile() {
        return NB_PROJECT_DESCRIPTOR_FILE;
    }

    @Override
    public ProjectRecord createProjectRecord(String root,
            ILeveledImportStructureProvider provider) {
        try {
            ProjectRecord projectRecord = new ProjectRecord();
            projectRecord.setProjectRoot(root);
            projectRecord.setProvider(provider);
            InputStream projectDescriptor = projectRecord
                    .getResourceContents(getProjectDescriptorFile());
            if (parseDescriptor(projectDescriptor, projectRecord)) {
                return projectRecord;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    private boolean parseDescriptor(InputStream projectDescriptor,
            ProjectRecord project) throws Exception {
        Document doc = XMLUtils.readDocument(projectDescriptor);
        Element mainElement = doc.getDocumentElement();

        Element type = (Element) mainElement.getElementsByTagName(
                NB_PROJECT_TYPE_KEY).item(0);
        String projectType = type.getTextContent();
        if (!projectType.equals(NB_PROJECT_TYPE_MOBILITY)) {
            return false;
        }

        Element name = (Element) mainElement.getElementsByTagName(
                NB_PROJECT_NAME_KEY).item(0);
        project.setProjectName(name.getTextContent());

        return true;
    }

    @Override
    public void projectCreated(ProjectRecord projectRecord, IProject project,
            IProgressMonitor monitor) throws CoreException {
        removeFolder(project.getFolder(NB_BUILD_FOLDER), monitor);
        removeFolder(project.getFolder(NB_METADATA_FOLDER), monitor);
        setApplicationDescriptor(projectRecord, project, monitor);
        removeFolder(project.getFolder(NB_DIST_FOLDER), monitor);
        removeFile(project.getFile(NB_BUILD_XML), monitor);
    }

    private byte[] loadApplicationDescriptorContents(ProjectRecord projectRecord) {
        try {
            Properties projectProps = loadProjectProperties(projectRecord);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            OutputStreamWriter output = new OutputStreamWriter(bos);
            for (String key : JAD_MAPPING.keySet()) {
                String property = projectProps.getProperty(key);

                // if property exists
                if ((property != null) && (!"".equals(property))) { //$NON-NLS-1$
                    if (JAD_MAPPING.get(key) == null) { // property is a full
                        // line
                        output.write(property);
                    } else { // property needs identifier (ex:
                        // "MicroEdition-Configuration")
                        output.write(JAD_MAPPING.get(key) + ": " + property //$NON-NLS-1$
                                + "\n"); //$NON-NLS-1$
                    }
                }

            }

            output.close();
            return bos.toByteArray();
        } catch (Exception e) {
            return null;
        }
    }

    private void setApplicationDescriptor(ProjectRecord projectRecord,
            IProject project, IProgressMonitor monitor) throws CoreException {
        IFile applicationDescriptor = project.getFile(NB_DIST_FOLDER + "/" //$NON-NLS-1$
                + project.getName() + JAD_EXTENSION);
        IFile destination = project.getFile(MTJ_APPLICATION_DESCRIPTOR);
        if (applicationDescriptor.exists()) {
            if (destination.exists()) {
                destination.delete(true, monitor);
            }
            applicationDescriptor
                    .move(destination.getFullPath(), true, monitor);
        } else {
            byte[] data = loadApplicationDescriptorContents(projectRecord);
            if ((data != null) && data.length > 0) {
                if (destination.exists()) {
                    destination.delete(true, monitor);
                }
                destination.create(new ByteArrayInputStream(data), true,
                        monitor);
            }
        }
    }

    private void removeFile(IFile file, IProgressMonitor monitor)
            throws CoreException {
        if (file.exists()) {
            file.delete(true, monitor);
        }
    }

    private void removeFolder(IFolder folder, IProgressMonitor monitor)
            throws CoreException {
        if (folder.exists()) {
            folder.delete(true, monitor);
        }
    }

    @Override
    public IDevice getProjectDevice(ProjectRecord record) throws CoreException {
        IDevice device = null;
        try {
            Properties projectProperties = loadProjectProperties(record);
            String groupName = projectProperties.getProperty(NB_DEVICE_GROUP);
            String deviceName = projectProperties.getProperty(NB_DEVICE_NAME);

            device = MTJCore.getDeviceRegistry().getDevice(groupName,
                    deviceName);
            if (device != null) {
                return device;
            }
        } catch (Exception e) {
        }
        device = MTJCore.getDeviceRegistry().getDefaultDevice();
        if (device == null) {
            throw new CoreException(
                    StatusUtil
                            .newStatus(
                                    Status.ERROR,
                                    NLS
                                            .bind(
                                                    ProjectImporterMessage.ProjectImporter_DeviceNotFound,
                                                    record.getProjectName()),
                                    null));
        }
        return device;
    }

    private Properties loadProjectProperties(ProjectRecord record)
            throws FileNotFoundException, IOException {
        InputStream contents = record.getResourceContents(NB_METADATA_FOLDER
                + "/" + NB_PROJECT_PROPERTIES);
        Properties projectProperties = new Properties();
        projectProperties.load(contents);
        return projectProperties;
    }

}
