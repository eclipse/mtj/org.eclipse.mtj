/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Fernando Rocha (Motorola) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.wizards.dialogfields;

/**
 * Change listener used by <code>ComboButtonDialogField</code>
 * 
 * @author Fernando Rocha
 * @since 1.0
 */
public interface IComboButtonAdapter {

	void changeControlPressed(DialogField field);
	
}
