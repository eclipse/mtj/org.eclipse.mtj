/**
 * Copyright (c) 2005, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.editor.XMLConfiguration;
import org.eclipse.mtj.internal.ui.editor.context.XMLDocumentSetupParticipant;
import org.eclipse.mtj.internal.ui.editor.text.ChangeAwareSourceViewerConfiguration;
import org.eclipse.mtj.internal.ui.editor.text.IColorManager;
import org.eclipse.mtj.internal.ui.editor.text.IMTJColorConstants;

public class XMLSyntaxColorTab extends SyntaxColorTab {

    private static final String[][] COLOR_STRINGS = new String[][] {
            { MTJUIMessages.XMLSyntaxColorTab_text_label, IMTJColorConstants.P_DEFAULT },
            { MTJUIMessages.XMLSyntaxColorTab_process_label, IMTJColorConstants.P_PROC_INSTR },
            { MTJUIMessages.XMLSyntaxColorTab_tags_label, IMTJColorConstants.P_TAG },
            { MTJUIMessages.XMLSyntaxColorTab_constants_label, IMTJColorConstants.P_STRING },
            { MTJUIMessages.XMLSyntaxColorTab_comments_label, IMTJColorConstants.P_XML_COMMENT } };

    /**
     * @param manager
     */
    public XMLSyntaxColorTab(IColorManager manager) {
        super(manager);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.preferences.SyntaxColorTab#getColorStrings()
     */
    @Override
    protected String[][] getColorStrings() {
        return COLOR_STRINGS;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.preferences.SyntaxColorTab#getDocument()
     */
    @Override
    protected IDocument getDocument() {

        StringBuffer buffer = new StringBuffer();
        String delimiter = System.getProperty("line.separator"); //$NON-NLS-1$
        buffer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"); //$NON-NLS-1$
        buffer.append(delimiter);
        buffer.append("<locales package=\"my.package\" destination=\"res\">"); //$NON-NLS-1$
        buffer.append(delimiter);
        buffer.append("<!-- Comment -->"); //$NON-NLS-1$
        buffer.append(delimiter);
        buffer.append("   <locale language=\"en\" country=\"US\">"); //$NON-NLS-1$
        buffer.append(delimiter);
        buffer.append("      <entry key=\"name\" value=\"some value\"/>"); //$NON-NLS-1$
        buffer.append(delimiter);
        buffer.append("   </locale>"); //$NON-NLS-1$
        buffer.append(delimiter);
        buffer.append("</locales>"); //$NON-NLS-1$

        IDocument document = new Document(buffer.toString());
        new XMLDocumentSetupParticipant().setup(document);
        return document;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.preferences.SyntaxColorTab#getSourceViewerConfiguration()
     */
    @Override
    protected ChangeAwareSourceViewerConfiguration getSourceViewerConfiguration() {
        return new XMLConfiguration(fColorManager);
    }
}
