/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Extracted class from UEIDeviceImporter Class.
 */
package org.eclipse.mtj.internal.toolkit.uei.properties;

import java.util.Properties;
import java.util.regex.Pattern;

import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.toolkit.uei.Messages;

/**
 * Holder class for the information the defines the way that a UEI emulator will
 * be launched.
 * 
 * @author Craig Setera
 * @since 0.9.1
 */
public class UEIDeviceDefinition {

    /**
     * 
     */
    private boolean debugServer;

    /**
     * 
     */
    private String launchTemplate;

    /**
     * 
     */
    private Pattern matchPattern;

    /**
     * 
     */
    private String name;

    /**
     * 
     */
    private boolean predeployRequired;

    /**
     * Creates a new device definition based on the specified information.
     * 
     * @param deviceName
     * @param properties
     * @throws IllegalArgumentException Could not find a match.expression for
     *             the specified device.
     * @throws NullPointerException if any of the deviceName or properties
     *             arguments are <code>null</code>.
     */
    public UEIDeviceDefinition(final String deviceName,
            final Properties properties) throws IllegalArgumentException,
            NullPointerException {

        if (deviceName == null) {
            throw new NullPointerException(
                    Messages.UEIDeviceDefinition_deviceName_null);
        } else if (properties == null) {
            throw new NullPointerException(
                    Messages.UEIDeviceDefinition_properties_null);
        }

        String matchExpression = properties
                .getProperty(UEIDeviceDefinitionProperties.MATCH_EXPRESSION
                        .createDeviceProperty(deviceName));

        if (matchExpression != null) {

            // The calculated fields
            Pattern matchPattern = Pattern.compile(matchExpression);

            boolean debugServer = properties.getProperty(
                    UEIDeviceDefinitionProperties.DEBUG_SERVER
                            .createDeviceProperty(deviceName),
                    Utils.EMPTY_STRING).equalsIgnoreCase(
                    Boolean.TRUE.toString());

            boolean predeployRequired = properties.getProperty(
                    UEIDeviceDefinitionProperties.PREDEPLOY_REQUIRED
                            .createDeviceProperty(deviceName),
                    Utils.EMPTY_STRING).equalsIgnoreCase(
                    Boolean.TRUE.toString());

            String launchTemplate = properties.getProperty(
                    UEIDeviceDefinitionProperties.LAUNCH_TEMPLATE
                            .createDeviceProperty(deviceName),
                    Utils.EMPTY_STRING);

            setDebugServer(debugServer);
            setName(deviceName);
            setMatchPattern(matchPattern);
            setLaunchTemplate(launchTemplate);
            setPredeployRequired(predeployRequired);

        } else {
            throw new IllegalArgumentException(
                    Messages.UEIDeviceDefinition_invalid_match_expression);
        }

    }

    /**
     * @return the launchTemplate
     */
    public String getLaunchTemplate() {
        return launchTemplate;
    }

    /**
     * @return the matchPattern
     */
    public Pattern getMatchPattern() {
        return matchPattern;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the isDebugServer
     */
    public boolean isDebugServer() {
        return debugServer;
    }

    /**
     * @return the predeployRequired
     */
    public boolean isPredeployRequired() {
        return predeployRequired;
    }

    /**
     * @param isDebugServer the isDebugServer to set
     */
    public void setDebugServer(boolean isDebugServer) {
        this.debugServer = isDebugServer;
    }

    /**
     * @param launchTemplate the launchTemplate to set
     */
    public void setLaunchTemplate(String launchTemplate) {
        this.launchTemplate = launchTemplate;
    }

    /**
     * @param matchPattern the matchPattern to set
     */
    public void setMatchPattern(Pattern matchPattern) {
        this.matchPattern = matchPattern;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param predeployRequired the predeployRequired to set
     */
    public void setPredeployRequired(boolean predeployRequired) {
        this.predeployRequired = predeployRequired;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (debugServer ? 1231 : 1237);
        result = prime * result
                + ((launchTemplate == null) ? 0 : launchTemplate.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + (predeployRequired ? 1231 : 1237);
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UEIDeviceDefinition other = (UEIDeviceDefinition) obj;
        if (debugServer != other.debugServer) {
            return false;
        }
        if (launchTemplate == null) {
            if (other.launchTemplate != null) {
                return false;
            }
        } else if (!launchTemplate.equals(other.launchTemplate)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (predeployRequired != other.predeployRequired) {
            return false;
        }
        return true;
    }

}
