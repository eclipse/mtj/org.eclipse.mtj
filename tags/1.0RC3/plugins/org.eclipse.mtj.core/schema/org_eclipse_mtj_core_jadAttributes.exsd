<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.eclipse.mtj.core" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appInfo>
         <meta.schema plugin="org.eclipse.mtj.core" id="jadattributes" name="Vendor Specific JAD Attributes"/>
      </appInfo>
      <documentation>
         Provides an extension point to add the some JAD attributes.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appInfo>
            <meta.element />
         </appInfo>
      </annotation>
      <complexType>
         <sequence>
            <element ref="jadAttributes"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  a fully qualified identifier of the target extension point
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  an optional identifier of the extension instance
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  an optional name of the extension instance
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="jadAttributes">
      <complexType>
         <sequence minOccurs="1" maxOccurs="unbounded">
            <element ref="jadDescriptorsProvider"/>
         </sequence>
         <attribute name="pageID" type="string" use="required">
            <annotation>
               <documentation>
                  indicate which JAD editor page the attributes will be shown.
currently, there are 3 pages you can add:
&lt;p&gt;
Page name:Required, pageID:required
&lt;p&gt;
Page name:Optional, pageID:optional
&lt;p&gt;
Page name:Over the Air, pageID:ota
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="jadDescriptorsProvider">
      <complexType>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  the required implementation class for the &lt;code&gt;org.eclipse.mtj.ui.jadEditor.IJADDescriptorsProvider&lt;/code&gt; interface that will be used to provide JAD Attributes descriptors.
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.mtj.ui.jadEditor.IJADDescriptorsProvider"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         1.0
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         &lt;p&gt;
An extension example that add some JAD attributes to the Overview page:
&lt;pre&gt; 
&lt;extension
          point=&quot;org.eclipse.mtj.core.jadattributes&quot;&gt;
       &lt;jadAttributes pageID=&quot;overview&quot;&gt;
         &lt;jadDescriptorsProvider
   class=&quot;org.eclipse.mtj.internal.ui.editors.jad.form.pages.RequiredJADDesciptorsProvider&quot;/&gt;
       &lt;/jadAttributes&gt;
    &lt;/extension&gt;
&lt;/pre&gt;
&lt;/p&gt;
&lt;p&gt;
An extension example that add some motorola specific JAD attributes to the OTA page:
&lt;pre&gt; 
    &lt;extension
          point=&quot;org.eclipse.mtj.core.jadAttributes&quot;&gt;
       &lt;jadAttributes pageID=&quot;motoSpecific&quot;&gt;
         &lt;jadDescriptorsProvider
          class=&quot;org.eclipse.mtj.examples.jadextension.MotoJADDesciptorsProvider&quot;/&gt;
       &lt;/jadAttributes&gt;
    &lt;/extension&gt;
&lt;/pre&gt;
&lt;/p&gt;
      </documentation>
   </annotation>


   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         MTJ provides the &quot;Required&quot;, &quot;Optional&quot; and &quot;OTA&quot; atributes implementations for the &lt;code&gt;jadattributes&lt;/code&gt; E.P., available in the &lt;code&gt;&lt;b&gt;org.eclipse.mtj.ui&lt;/b&gt;&lt;/code&gt; plug-in.
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="copyright"/>
      </appInfo>
      <documentation>
         Copyright (c) 2008,2009 Sybase Corporation and others.&lt;br&gt;
All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at &lt;a 
href=&quot;http://www.eclipse.org/legal/epl-v10.html&quot;&gt;http://www.eclipse.org/legal/epl-v10.html&lt;/a&gt;
      </documentation>
   </annotation>

</schema>
