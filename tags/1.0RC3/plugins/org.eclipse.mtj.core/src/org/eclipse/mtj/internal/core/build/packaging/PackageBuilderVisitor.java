/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Fixing path problems.
 */
package org.eclipse.mtj.internal.core.build.packaging;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.internal.core.build.MTJBuildProperties;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.osgi.util.NLS;

import de.schlichtherle.io.File;

/**
 * PackageBuilderVisitor class implements both {@link IResourceDeltaVisitor} and
 * {@link IResourceVisitor} in order to allow collecting resources from either
 * an {@link IResourceDelta} or an {@link IResource}. The resources collected
 * must be in the build properties file if any in order to be packaged. It
 * collects both classes and non class resources within the project's source
 * folders.
 * 
 * @author David Marques
 */
public class PackageBuilderVisitor implements IResourceVisitor,
		IResourceDeltaVisitor {
	private IMidletSuiteProject suiteProject;
	private MTJBuildProperties properties;
	private List<IFile> resources;
	private List<IFile> classes;
	private File jar;

	/**
	 * Creates a PackageBuilderVisitor to collect resources from the specified
	 * {@link IMidletSuiteProject} instance in order to package resources into
	 * the specified JAR.
	 * 
	 * @param suiteProject
	 *            source project.
	 * @param jarFile
	 *            target JAR file.
	 */
	public PackageBuilderVisitor(IMidletSuiteProject suiteProject, File jarFile) {
		this.resources = new ArrayList<IFile>();
		this.classes = new ArrayList<IFile>();
		this.properties = MTJBuildProperties.getBuildProperties(suiteProject);
		this.suiteProject = suiteProject;
		this.jar = jarFile;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.resources.IResourceVisitor#visit(org.eclipse.core.resources
	 * .IResource)
	 */
	public boolean visit(IResource resource) throws CoreException {
		if (resource instanceof IFile) {
			IFile file = (IFile) resource;
			if (isOnBuildFolder(file)) {
				if (file.getName().endsWith(".class")) {
					IFile preverified = getPreVerifiedClassFile(file);
					if (preverified != null) {
						if (isOnBuildProperties(file, true)) {
							this.classes.add(preverified);
						} else {
							this.removeFromJAR(preverified);
						}
					}
				} else {
					if (isOnBuildProperties(resource, false)) {
						this.resources.add(file);
					} else {
						this.removeFromJAR(file);
					}
				}
			}
		}
		return true;
	}

	/**
	 * Checks whether the {@link IFile} instance is on the build folder.
	 * 
	 * @param file
	 *            target file.
	 * @return true if on build folder, false otherwise.
	 * @throws JavaModelException
	 *             if element does not exist.
	 */
	private boolean isOnBuildFolder(IFile file) throws JavaModelException {
		IFolder folder = PackageBuilder.getOutputFolder(this.suiteProject
				.getJavaProject());
		return folder.getLocation().isPrefixOf(file.getLocation());
	}

	/**
	 * Checks whether a resource is listed on the build.properties file in order
	 * to be packaged.
	 * 
	 * @param resource
	 *            target resource.
	 * @param isClassFile
	 *            true if class file.
	 * @return true if listed false otherwise.
	 * @throws CoreException
	 *             Any core error occurred.
	 */
	private boolean isOnBuildProperties(IResource resource, boolean isClassFile)
			throws CoreException {
		MTJRuntimeList runtimes = this.suiteProject.getRuntimeList();
		MTJRuntime active = runtimes.getActiveMTJRuntime();
		boolean result = false;
		if (active != null) {
			IPath resourcePath = getOutputFolderRelativePath(resource);
			if (!resourcePath.isEmpty()) {
				IPath resourcePack = resourcePath.removeLastSegments(1);
				String resourceFile = resourcePath.lastSegment().replace(
						".class", "");
				IResource[] resources = properties.getBuildProperty(active);
				for (IResource candidate : resources) {
					IPath candidatePath = Utils
							.extractsSourceFolderRelativePath(this.suiteProject
									.getJavaProject(), candidate, true);
					IPath candidatePack = candidatePath.removeLastSegments(1);
					String candidateFile = candidatePath.lastSegment().replace(
							".java", "");
					// Filter the class name with the source file name.
					// Also checks for inner classes like Class$InnerClass
					result = Pattern.matches(NLS.bind("{0}([$].+)?",
							candidatePack.append(candidateFile)), resourcePack
							.append(resourceFile).toString());
					if (result) {
						break;
					}
				}
			}
		} else {
			result = true;
		}
		return result;
	}

	/**
	 * Gets the output folder relative path for a resource.
	 * 
	 * @param resource target resource.
	 * @return relative path.
	 * @throws CoreException Any core error occurred.
	 */
	private IPath getOutputFolderRelativePath(IResource resource)
			throws CoreException {
		IFolder folder = PackageBuilder.getOutputFolder(this.suiteProject
				.getJavaProject());
		if (folder.getFullPath().isPrefixOf(resource.getFullPath())) {
			IPath path = resource.getFullPath().removeFirstSegments(
					folder.getFullPath().segmentCount());
			return new Path(path.toString());
		}
		return new Path("");
	}

	/**
	 * Gets the preverified class file from a class file
	 * within the binary folder.
	 * 
	 * @param clazz target class file.
	 * @return preverified class file.
	 * @throws CoreException Any core error occurs.
	 */
	private IFile getPreVerifiedClassFile(IFile clazz) throws CoreException {
		IFolder folder = this.suiteProject
				.getVerifiedClassesOutputFolder(new NullProgressMonitor());
		IFile preverfied = folder.getFile(getOutputFolderRelativePath(clazz));
		if (preverfied.exists()) {
			return preverfied;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.core.resources.IResourceDelta)
	 */
	public boolean visit(IResourceDelta delta) throws CoreException {
		IResource resource = delta.getResource();
		if (resource instanceof IFile) {
			IFile file = (IFile) resource;
			switch (delta.getKind()) {
			case IResourceDelta.CHANGED:
			case IResourceDelta.ADDED:
				this.visit(file);
				break;
			case IResourceDelta.REMOVED:
				this.removeFromJAR(file);
				break;
			}
		}
		return true;
	}

	/**
	 * Removes the file from the jar file.
	 * 
	 * @param file target file.
	 * @throws CoreException Any core error occurred.
	 */
	private void removeFromJAR(IFile file) throws CoreException {
		IPath zipPath = null;
		if (file.getName().endsWith(".class")) {
			zipPath = PackageBuilder.getClassZipPath(this.suiteProject, file);
		} else {
			zipPath = PackageBuilder
					.getResourceZipPath(this.suiteProject, file);
		}

		if (zipPath != null) {
			File zipFile = new File(jar, zipPath.toString());
			if (zipFile.exists()) {
				zipFile.delete();
			}
		}
	}

	/**
	 * Gets all resource files collected.
	 * 
	 * @return resources list.
	 */
	public List<IFile> getResourcesToPackage() {
		List<IFile> copy = new ArrayList<IFile>();
		copy.addAll(this.resources);
		return copy;
	}

	/**
	 * Gets all class files collected.
	 * 
	 * @return class list.
	 */
	public List<IFile> getClassesToPackage() {
		List<IFile> copy = new ArrayList<IFile>();
		copy.addAll(this.classes);
		return copy;
	}
}
