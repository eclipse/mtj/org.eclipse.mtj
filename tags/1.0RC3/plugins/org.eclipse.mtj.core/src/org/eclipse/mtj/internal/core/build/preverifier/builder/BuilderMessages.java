/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola)  - Initial implementation
 *                                
 */
package org.eclipse.mtj.internal.core.build.preverifier.builder;

import org.eclipse.osgi.util.NLS;

public class BuilderMessages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.internal.core.build.preverifier.builder.messages"; //$NON-NLS-1$

    public static String PackageBuilder_cleaningDeployedFolder;

	public static String PackageBuilder_cleaningRuntimeFolder;

	public static String PackageBuilder_collectingSources;

	public static String PackageBuilder_generatingDeploymentPackage;

	public static String PackageBuilder_obfuscating;

	public static String PackageBuilder_packagingClasses;

	public static String PackageBuilder_packagingDependencies;

	public static String PackageBuilder_packagingLibraries;

	public static String PackageBuilder_packagingResources;

	public static String PackageBuilder_updatingJadVersion;

	public static String PreverificationBuilder_0;

    public static String PreverificationBuilder_1;

    public static String PreverificationBuilder_10;

    public static String PreverificationBuilder_11;

    public static String PreverificationBuilder_12;

    public static String PreverificationBuilder_13;

    public static String PreverificationBuilder_14;

    public static String PreverificationBuilder_15;

    public static String PreverificationBuilder_16;

    public static String PreverificationBuilder_17;

    public static String PreverificationBuilder_18;

    public static String PreverificationBuilder_19;

    public static String PreverificationBuilder_2;

    public static String PreverificationBuilder_20;

    public static String PreverificationBuilder_21;

    public static String PreverificationBuilder_22;

    public static String PreverificationBuilder_23;

    public static String PreverificationBuilder_24;

    public static String PreverificationBuilder_25;

    public static String PreverificationBuilder_26;

    public static String PreverificationBuilder_27;

    public static String PreverificationBuilder_29;

    public static String PreverificationBuilder_3;

    public static String PreverificationBuilder_30;

    public static String PreverificationBuilder_31;

    public static String PreverificationBuilder_32;

    public static String PreverificationBuilder_33;

    public static String PreverificationBuilder_34;

    public static String PreverificationBuilder_35;

    public static String PreverificationBuilder_37;

    public static String PreverificationBuilder_38;

    public static String PreverificationBuilder_39;

    public static String PreverificationBuilder_4;

    public static String PreverificationBuilder_40;

    public static String PreverificationBuilder_41;

    public static String PreverificationBuilder_42;

    public static String PreverificationBuilder_43;

    public static String PreverificationBuilder_45;

    public static String PreverificationBuilder_47;

    public static String PreverificationBuilder_48;

    public static String PreverificationBuilder_49;

    public static String PreverificationBuilder_5;

    public static String PreverificationBuilder_50;

    public static String PreverificationBuilder_51;

    public static String PreverificationBuilder_52;

    public static String PreverificationBuilder_53;

    public static String PreverificationBuilder_57;

    public static String PreverificationBuilder_58;

    public static String PreverificationBuilder_59;

    public static String PreverificationBuilder_6;

    public static String PreverificationBuilder_60;

    public static String PreverificationBuilder_61;

    public static String PreverificationBuilder_62;

    public static String PreverificationBuilder_63;

    public static String PreverificationBuilder_64;

    public static String PreverificationBuilder_65;

    public static String PreverificationBuilder_7;

    public static String PreverificationBuilder_8;

    public static String PreverificationBuilder_9;

    public static String PreverificationBuilder_PreverifierNotFoundErrorMessage;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, BuilderMessages.class);
    }

    private BuilderMessages() {
    }
}
