/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.build;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.compiler.CompilationParticipant;
import org.eclipse.mtj.core.build.MTJBuildState;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * MTJCompilationParticipant class participates on the JDT build in order to set
 * the compilation states on the {@link BuildStateMachine} instance for the
 * {@link IMTJProject} instances.
 * 
 * @author David Marques
 */
public class MTJCompilationParticipant extends CompilationParticipant {

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.compiler.CompilationParticipant#aboutToBuild(org.eclipse.jdt.core.IJavaProject)
     */
    public int aboutToBuild(IJavaProject project) {
        try {
            IMTJProject mtjProject = MidletSuiteFactory
                    .getMidletSuiteProject(project);
            BuildStateMachine stateMachine = BuildStateMachine
                    .getInstance(mtjProject);
            IProgressMonitor monitor = new NullProgressMonitor();

            BuildSpecManipulator manipulator = new BuildSpecManipulator(
                    mtjProject.getProject());
            if (manipulator.isFirstBuilder(JavaCore.BUILDER_ID)) {
                stateMachine.start(monitor);
            }

            stateMachine.changeState(MTJBuildState.PRE_COMPILATION, monitor);
        } catch (CoreException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
        return super.aboutToBuild(project);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.compiler.CompilationParticipant#buildFinished(org.eclipse.jdt.core.IJavaProject)
     */
    public void buildFinished(IJavaProject project) {
        try {
            IMTJProject mtjProject = MidletSuiteFactory
                    .getMidletSuiteProject(project);
            BuildStateMachine stateMachine = BuildStateMachine
                    .getInstance(mtjProject);
            IProgressMonitor monitor = new NullProgressMonitor();

            stateMachine.changeState(MTJBuildState.POST_COMPILATION, monitor);
            
            BuildSpecManipulator manipulator = new BuildSpecManipulator(
                    mtjProject.getProject());
            if (manipulator.isLastBuilder(JavaCore.BUILDER_ID)) {
                stateMachine.changeState(MTJBuildState.POST_BUILD, monitor);
            }
        } catch (CoreException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
        super.buildFinished(project);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.compiler.CompilationParticipant#isActive(org.eclipse.jdt.core.IJavaProject)
     */
    public boolean isActive(IJavaProject project) {
        boolean result = false;
        try {
            result = project.getProject().hasNature(
                    IMTJCoreConstants.MTJ_NATURE_ID);
        } catch (CoreException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
        return result;
    }

}
