/**
 * Copyright (c) 2007,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

import java.io.Serializable;

import org.eclipse.core.runtime.CoreException;

/**
 * @since 0.9.1
 */
public interface IDocumentAttributeNode extends IDocumentRange, Serializable,
        IDocumentXMLNode {

    /**
     * @param parent
     */
    public void reconnect(IDocumentElementNode parent);

    /**
     * @return
     */
    String getAttributeName();

    /**
     * @return
     */
    String getAttributeValue();

    /**
     * @return
     */
    IDocumentElementNode getEnclosingElement();

    /**
     * @return
     */
    int getNameLength();

    /**
     * @return
     */
    int getNameOffset();

    /**
     * @return
     */
    int getValueLength();

    /**
     * @return
     */
    int getValueOffset();

    /**
     * @param name
     * @throws CoreException
     */
    void setAttributeName(String name) throws CoreException;

    /**
     * @param value
     * @throws CoreException
     */
    void setAttributeValue(String value) throws CoreException;

    /**
     * @param node
     */
    void setEnclosingElement(IDocumentElementNode node);

    /**
     * @param length
     */
    void setNameLength(int length);

    /**
     * @param offset
     */
    void setNameOffset(int offset);

    /**
     * @param length
     */
    void setValueLength(int length);

    /**
     * @param offset
     */
    void setValueOffset(int offset);

    /**
     * @return
     */
    String write();

}
