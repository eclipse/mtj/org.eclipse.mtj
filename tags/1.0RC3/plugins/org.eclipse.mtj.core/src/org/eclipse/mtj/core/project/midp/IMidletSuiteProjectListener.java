/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.core.project.midp;

import org.eclipse.mtj.core.project.IMTJProjectListener;

/**
 * Classes which implement this interface provide a method that deals with the
 * events that are generated when the {@link IMidletSuiteProject} is modified.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface IMidletSuiteProjectListener extends IMTJProjectListener {

    /**
     * Sent when the {@link IMidletSuiteProject#setJadFileName(String)} is
     * invoked.
     */
    public void jadFileNameChanged();

    /**
     * Sent when the {@link IMidletSuiteProject#setTempKeyPassword(String)} is
     * invoked.
     */
    public abstract void tempKeyPasswordChanged();

    /**
     * Sent when the {@link IMidletSuiteProject#setTempKeystorePassword(String)}
     * is invoked.
     */
    public abstract void tempKeystorePasswordChanged();

}
