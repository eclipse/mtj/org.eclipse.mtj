/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.util;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.Messages;

/**
 * @author Diego Sandin
 * @since 1.0
 * 
 */
public class MTJStatusHandler {

    /**
     * Status code for which a UI prompter is registered.
     */
    public static final IStatus OK_STATUS = new Status(IStatus.OK,
            IMTJCoreConstants.PLUGIN_ID, 0, Messages.MTJCore_OK_STATUS_msg,
            null);

    /**
     * Status code for which a UI prompter is registered.
     */
    public static final IStatus PROMPTER_STATUS = new Status(IStatus.INFO,
            "org.eclipse.debug.ui", // TODO This should probably not be done.... //$NON-NLS-1$
            200, "", null); //$NON-NLS-1$

    /**
     * Attempt to prompt on a status object. If prompting fails, a CoreException
     * will be thrown.
     * 
     * @param status
     * @param source
     * @return
     * @throws CoreException
     */
    public static Object statusPrompt(IStatus status, Object source)
            throws CoreException {
        Object result = null;

        IStatusHandler prompterStatus = DebugPlugin.getDefault()
                .getStatusHandler(PROMPTER_STATUS);

        if (prompterStatus == null) {
            // if there is no handler, throw the exception
            throw new CoreException(status);
        } else {
            result = prompterStatus.handleStatus(status, source);
        }

        return result;
    }

    /**
     * Creates a new status object for our plug-in. The created status has no
     * children.
     * 
     * @param severity the severity; one of <code>OK</code>, <code>ERROR</code>,
     *            <code>INFO</code>, or <code>WARNING</code>
     * @param code the plug-in-specific status code, or <code>OK</code>
     * @param message a human-readable message, localized to the current locale
     */
    public static IStatus newStatus(int severity, int code, String message) {
        return MTJStatusHandler.newStatus(severity, code, message, null);
    }

    /**
     * Creates a new status object for our plug-in. The created status has no
     * children.
     * 
     * @param severity the severity; one of <code>OK</code>, <code>ERROR</code>,
     *            <code>INFO</code>, or <code>WARNING</code>
     * @param code the plug-in-specific status code, or <code>OK</code>
     * @param message a human-readable message, localized to the current locale
     * @param exception a low-level exception, or <code>null</code> if not
     *            applicable
     */
    public static IStatus newStatus(int severity, int code, String message,
            Throwable exception) {
        return new Status(severity, IMTJCoreConstants.PLUGIN_ID, code, message,
                exception);
    }

    /**
     * Throw a new CoreException wrapped around the specified exception.
     * 
     * @param severity
     * @param code
     * @param exception
     * @throws CoreException
     */
    public static void throwCoreException(int severity, int code,
            Throwable exception) throws CoreException {
        // Make sure we create a valid status object
        String message = null;
        if (exception != null) {
            message = exception.getMessage();
        }
        if (message == null) {
            message = Messages.MTJCore_no_message2;
        }
    
        IStatus status = new Status(severity, IMTJCoreConstants.PLUGIN_ID,
                code, message, exception);
        throw new CoreException(status);
    }

    /**
     * Throw a new CoreException wrapped around the specified String.
     * 
     * @param severity
     * @param code
     * @param message
     * @throws CoreException
     */
    public static void throwCoreException(int severity, int code, String message)
            throws CoreException {
        if (message == null) {
            message = Messages.MTJCore_no_message2;
        }
    
        IStatus status = new Status(severity, IMTJCoreConstants.PLUGIN_ID,
                code, message, null);
        throw new CoreException(status);
    }

}
