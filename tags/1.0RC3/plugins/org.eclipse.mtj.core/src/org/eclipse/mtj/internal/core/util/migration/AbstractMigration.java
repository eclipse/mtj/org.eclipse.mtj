/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.util.migration;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public abstract class AbstractMigration implements IMigration {

    protected boolean migrated = false;

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.util.migration.IMigration#migrate(org.w3c.dom.Document)
     */
    public abstract Document migrate(Document document);

    public AbstractMigration() {
        super();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.util.migration.IMigration#isMigrated()
     */
    public boolean isMigrated() {
        return migrated;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.util.migration.IMigration#migrate(java.io.File)
     */
    public Document migrate(File storeFile) {
        Document document = null;
    
        try {
            document = XMLUtils.readDocument(storeFile);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    
        return migrate(document);
    }

}