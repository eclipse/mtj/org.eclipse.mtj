/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.templates;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * GameTemplateWizardPage provides a template for games 
 * with a built in game engine.
 * 
 * @author David Marques
 * @since 1.0
 */
public class GameTemplateWizardPage extends AbstractTemplateWizardPage {

    private Text xText;
    private Text yText;
    private Text wText;
    private Text hText;
    private Text speedText;
    private Text v1ColorText;
    private Text v2ColorText;
    private Text fpsText;

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        parent.setLayout(new GridLayout(0x01, false));
        createGameGroup(parent);
        createObjectGroup(parent);        
    }
    
    private void createObjectGroup(Composite parent) {
        GridData data = null;
        
        Group gameGroup = new Group(parent, SWT.NONE);
        gameGroup.setText(Messages.GameTemplateProvider_0);
        
        data = new GridData(SWT.FILL, SWT.FILL, true, false);
        gameGroup.setLayoutData(data);
        gameGroup.setLayout(new GridLayout(4, false));
        
        Label xLabel = new Label(gameGroup, SWT.NONE);
        xLabel.setText(Messages.GameTemplateProvider_1);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        xLabel.setLayoutData(data);
        
        xText = new Text(gameGroup, SWT.BORDER);
        xText.setText("10"); //$NON-NLS-1$
        
        xText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        xText.setLayoutData(data); 
        
        Label yLabel = new Label(gameGroup, SWT.NONE);
        yLabel.setText(Messages.GameTemplateProvider_3);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        yLabel.setLayoutData(data);
        
        yText = new Text(gameGroup, SWT.BORDER);
        yText.setText("10"); //$NON-NLS-1$
        
        yText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        yText.setLayoutData(data); 
        
        Label wLabel = new Label(gameGroup, SWT.NONE);
        wLabel.setText(Messages.GameTemplateProvider_5);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        wLabel.setLayoutData(data);
        
        wText = new Text(gameGroup, SWT.BORDER);
        wText.setText("50"); //$NON-NLS-1$
        
        wText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        wText.setLayoutData(data); 
        
        Label hLabel = new Label(gameGroup, SWT.NONE);
        hLabel.setText(Messages.GameTemplateProvider_7);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        hLabel.setLayoutData(data);
        
        hText = new Text(gameGroup, SWT.BORDER);
        hText.setText("50"); //$NON-NLS-1$
        
        hText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        hText.setLayoutData(data); 
        
        Label speedLabel = new Label(gameGroup, SWT.NONE);
        speedLabel.setText(Messages.GameTemplateProvider_9);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        speedLabel.setLayoutData(data);
        
        speedText = new Text(gameGroup, SWT.BORDER);
        speedText.setText("5"); //$NON-NLS-1$
        
        speedText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        speedText.setLayoutData(data); 
        
        Label pixelFormatLabel = new Label(gameGroup, SWT.NONE);
        pixelFormatLabel.setText(Messages.GameTemplateProvider_11);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        data.horizontalSpan = 2;
        pixelFormatLabel.setLayoutData(data);
        
        Label v1ColorLabel = new Label(gameGroup, SWT.NONE);
        v1ColorLabel.setText(Messages.GameTemplateProvider_12);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        v1ColorLabel.setLayoutData(data);
        
        v1ColorText = new Text(gameGroup, SWT.BORDER);
        v1ColorText.setText("0xFF0000"); //$NON-NLS-1$
        
        v1ColorText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        v1ColorText.setLayoutData(data);
        
        Label color1FormatLabel = new Label(gameGroup, SWT.NONE);
        color1FormatLabel.setText(Messages.GameTemplateProvider_14);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        data.horizontalSpan = 2;
        color1FormatLabel.setLayoutData(data);
        
        Label v2ColorLabel = new Label(gameGroup, SWT.NONE);
        v2ColorLabel.setText(Messages.GameTemplateProvider_15);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        v2ColorLabel.setLayoutData(data);
        
        v2ColorText = new Text(gameGroup, SWT.BORDER);
        v2ColorText.setText("0x00FF00"); //$NON-NLS-1$
        
        v2ColorText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        v2ColorText.setLayoutData(data);
        
        Label color2FormatLabel = new Label(gameGroup, SWT.NONE);
        color2FormatLabel.setText(Messages.GameTemplateProvider_17);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        data.horizontalSpan = 2;
        color2FormatLabel.setLayoutData(data);
        
    }

    private void createGameGroup(Composite parent) {
        GridData data = null;
        
        Group gameGroup = new Group(parent, SWT.NONE);
        gameGroup.setText(Messages.GameTemplateProvider_18);
        
        data = new GridData(SWT.FILL, SWT.FILL, true, false);
        gameGroup.setLayoutData(data);
        gameGroup.setLayout(new GridLayout(2, false));
        
        Label portLabel = new Label(gameGroup, SWT.NONE);
        portLabel.setText(Messages.GameTemplateProvider_19);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        portLabel.setLayoutData(data);
        
        fpsText = new Text(gameGroup, SWT.BORDER);
        fpsText.setText("10"); //$NON-NLS-1$
        
        fpsText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        fpsText.setLayoutData(data);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.ITemplateProvider#getDictionary()
     */
    public Map<String, String> getDictionary() {
        Map<String, String> dictionary = new HashMap<String, String>();
        
        dictionary.put("$object-speed$", this.speedText.getText()); //$NON-NLS-1$
        dictionary.put("$game-fps$", this.fpsText.getText()); //$NON-NLS-1$
        dictionary.put("$object-x$", this.xText.getText()); //$NON-NLS-1$
        dictionary.put("$object-y$", this.yText.getText()); //$NON-NLS-1$
        dictionary.put("$object-w$", this.wText.getText()); //$NON-NLS-1$
        dictionary.put("$object-h$", this.hText.getText()); //$NON-NLS-1$
        dictionary.put("$object-color1$", this.v1ColorText.getText()); //$NON-NLS-1$
        dictionary.put("$object-color2$", this.v2ColorText.getText()); //$NON-NLS-1$
        
        return dictionary;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.ITemplateProvider#isPageComplete()
     */
    public boolean isPageComplete() {
        boolean result = true;
        result &= Pattern.matches("(0x)([a-fA-F0-9]){6}", this.v1ColorText.getText()); //$NON-NLS-1$
        result &= Pattern.matches("(0x)([a-fA-F0-9]){6}", this.v2ColorText.getText()); //$NON-NLS-1$
        result &= Pattern.matches("\\d+", this.speedText.getText()); //$NON-NLS-1$
        result &= Pattern.matches("\\d+", this.fpsText.getText()); //$NON-NLS-1$
        result &= Pattern.matches("\\d+", this.xText.getText()); //$NON-NLS-1$
        result &= Pattern.matches("\\d+", this.yText.getText()); //$NON-NLS-1$
        result &= Pattern.matches("\\d+", this.wText.getText()); //$NON-NLS-1$
        result &= Pattern.matches("\\d+", this.hText.getText()); //$NON-NLS-1$
        return result;
    }

}
