/*******************************************************************************
 * Copyright (c) 2000, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Adapted code available in JDT
 */
package org.eclipse.mtj.internal.ui.wizards;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.actions.WorkbenchRunnableAdapter;
import org.eclipse.mtj.internal.ui.util.ExceptionHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.wizards.newresource.BasicNewResourceWizard;

public abstract class NewElementWizard extends Wizard implements INewWizard {

    private IWorkbench workbench;
    private IStructuredSelection selection;

    public NewElementWizard() {
        setNeedsProgressMonitor(true);
    }

    protected void openResource(final IFile resource) {
        final IWorkbenchPage activePage = MTJUIPlugin.getActivePage();
        if (activePage != null) {
            final Display display = getShell().getDisplay();
            if (display != null) {
                display.asyncExec(new Runnable() {
                    public void run() {
                        try {
                            IDE.openEditor(activePage, resource, true);
                        } catch (PartInitException e) {
                            MTJLogger.log(Status.ERROR, e);
                        }
                    }
                });
            }
        }
    }

    /**
     * Subclasses should override to perform the actions of the wizard. This
     * method is run in the wizard container's context as a workspace runnable.
     * 
     * @param monitor
     * @throws InterruptedException
     * @throws CoreException
     */
    protected abstract void finishPage(IProgressMonitor monitor)
            throws InterruptedException, CoreException;

    /**
     * Returns the scheduling rule for creating the element.
     * 
     * @return returns the scheduling rule
     */
    protected ISchedulingRule getSchedulingRule() {
        return MTJCore.getWorkspace().getRoot(); // look all by default
    }

    protected boolean canRunForked() {
        return true;
    }

    public abstract IJavaElement getCreatedElement();

    protected void handleFinishException(Shell shell,
            InvocationTargetException e) {
        String title = "New";
        String message = "Creation of element failed.";
        ExceptionHandler.handle(e, shell, title, message);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    public boolean performFinish() {
        IWorkspaceRunnable op = new IWorkspaceRunnable() {
            public void run(IProgressMonitor monitor) throws CoreException,
                    OperationCanceledException {
                try {
                    finishPage(monitor);
                } catch (InterruptedException e) {
                    throw new OperationCanceledException(e.getMessage());
                }
            }
        };
        try {
            ISchedulingRule rule = null;
            Job job = Job.getJobManager().currentJob();
            if (job != null)
                rule = job.getRule();
            IRunnableWithProgress runnable = null;
            if (rule != null)
                runnable = new WorkbenchRunnableAdapter(op, rule, true);
            else
                runnable = new WorkbenchRunnableAdapter(op, getSchedulingRule());
            getContainer().run(canRunForked(), true, runnable);
        } catch (InvocationTargetException e) {
            handleFinishException(getShell(), e);
            return false;
        } catch (InterruptedException e) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
     */
    public void init(IWorkbench workbench, IStructuredSelection currentSelection) {
        this.workbench = workbench;
        selection = currentSelection;
    }

    public IStructuredSelection getSelection() {
        return selection;
    }

    public IWorkbench getWorkbench() {
        return workbench;
    }

    protected void selectAndReveal(IResource newResource) {
        BasicNewResourceWizard.selectAndReveal(newResource, workbench
                .getActiveWorkbenchWindow());
    }

}
