/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 *      Gang  Ma     (Sybase)  - Add content assist support
 */
package org.eclipse.mtj.internal.ui.editors.jad.source;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.eclipse.mtj.internal.ui.editors.jad.source.contentassist.CompletionProcessor;
import org.eclipse.mtj.internal.ui.editors.jad.source.rules.JadScanner;

/**
 * This class bundles the configuration space of {@link JADSourceEditor}
 * 
 * @author Diego Madruga Sandin
 */
public class JADSourceViewerConfiguration extends SourceViewerConfiguration {
    private JADSourceEditor sourcePage;

    public JADSourceViewerConfiguration(JADSourceEditor sourcePage) {
        this.sourcePage = sourcePage;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getContentAssistant(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IContentAssistant getContentAssistant(ISourceViewer sourceViewer) {
        ContentAssistant ca = new ContentAssistant();
        IContentAssistProcessor cap = new CompletionProcessor(sourcePage);
        ca.setContentAssistProcessor(cap, IDocument.DEFAULT_CONTENT_TYPE);
        ca.enableAutoActivation(true);
        ca
                .setInformationControlCreator(getInformationControlCreator(sourceViewer));

        return ca;

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getPresentationReconciler(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IPresentationReconciler getPresentationReconciler(
            ISourceViewer sourceViewer) {

        PresentationReconciler pr = new PresentationReconciler();
        DefaultDamagerRepairer ddr = new DefaultDamagerRepairer(new JadScanner());
        pr.setRepairer(ddr, IDocument.DEFAULT_CONTENT_TYPE);
        pr.setDamager(ddr, IDocument.DEFAULT_CONTENT_TYPE);

        return pr;
    }
}
