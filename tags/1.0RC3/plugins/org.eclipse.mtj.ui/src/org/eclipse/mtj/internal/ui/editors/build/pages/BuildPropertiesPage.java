/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Refactoring build.properties
 *     David Marques (Motorola) - Always call package builder.
 */
package org.eclipse.mtj.internal.ui.editors.build.pages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.MTJBuildProperties;
import org.eclipse.mtj.internal.core.build.MTJBuildPropertiesChangeListener;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.dialogs.ContainerCheckedTreeViewer;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

/**
 * BuildPropertiesPage class provides a {@link FormPage} instance
 * to manage build.properties files.
 * 
 * @author David Marques
 */
public class BuildPropertiesPage extends FormPage implements MTJBuildPropertiesChangeListener {

	private static final String PAGE_TITLE = MTJUIMessages.BuildPropertiesPage_pageTitle;
	private static final String PAGE_ID    = "buildPropertiesPage"; //$NON-NLS-1$
	
	private MTJBuildProperties properties;
	private IMTJProject mtjProject;
	private MTJRuntime  currentRuntime;
	
	private ContainerCheckedTreeViewer resourcesViewer;
	private ComboViewer configurationsViewer;
	private boolean dirty;
	
	/**
	 * Creates an instance of a BuildPropertiesPage to manage
	 * the build.properties of the specified MTJ project.
	 * 
	 * @param editor parent editor.
	 * @param buildProperties target project.
	 */
	public BuildPropertiesPage(FormEditor editor, MTJBuildProperties buildProperties) {
		super(editor, PAGE_ID, PAGE_TITLE);
		this.properties = buildProperties;
		this.properties.addPropertiesChangeListener(this);
		this.mtjProject = buildProperties.getMTJProject();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
	 */
	protected void createFormContent(IManagedForm managedForm) {
		FormToolkit  toolkit = managedForm.getToolkit();
		ScrolledForm form    = managedForm.getForm();

		toolkit.decorateFormHeading(form.getForm());
		form.setExpandHorizontal(true);
		form.setExpandVertical(true);
		form.setText(getTitle());

		Composite body = form.getBody();
		body.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		GridLayout layout = new GridLayout(0x01, true);
		layout.verticalSpacing = 10;
		body.setLayout(layout);
		
		this.createConfigurationsSection(managedForm, body);
		this.createResourcesSection(managedForm, body);
		form.reflow(true);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.forms.editor.FormPage#setActive(boolean)
	 */
	public void setActive(boolean active) {
		super.setActive(active);
		
		if (active) {
			this.configurationsViewer.refresh();
			this.updateViewer();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.forms.editor.FormPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void doSave(IProgressMonitor monitor) {
		try {
			this.properties.store();
			this.setDirty(false);
			
			this.mtjProject.getProject().build(
					IncrementalProjectBuilder.FULL_BUILD,
					IMTJCoreConstants.PACKAGE_BUILDER_ID,
					new HashMap<String, String>(), monitor);
		} catch (IOException e) {
			MTJLogger.log(IStatus.ERROR, e);
		} catch (CoreException e) {
			MTJLogger.log(IStatus.ERROR, e);
		}
	}
	
	/**
	 * Creates the runtime configurations section.
	 * 
	 * @param managedForm managed form
	 * @param parent parent composite.
	 */
	private void createConfigurationsSection(IManagedForm managedForm, Composite parent) {
		GridData gridData = null;
		
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		Composite section = createSection(managedForm, parent, MTJUIMessages.BuildPropertiesPage_runtimeSectionTitle
				, MTJUIMessages.BuildPropertiesPage_runtimeSectionDetails, gridData);
		section.setLayout(new GridLayout(0x01, true));
		
		Combo combo = new Combo(section, SWT.DROP_DOWN | SWT.READ_ONLY);
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		combo.setFocus();
		
		configurationsViewer = new ComboViewer(combo);
		configurationsViewer.setContentProvider(new IStructuredContentProvider(){

			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {}
			
			public Object[] getElements(Object inputElement) {
				List<Object> result = new ArrayList<Object>();
				if (inputElement instanceof MTJRuntimeList) {
					MTJRuntimeList runtimeList = (MTJRuntimeList) inputElement;
					result.addAll(runtimeList);
				}
				return result.toArray();
			}

			public void dispose() {}
		});
		
		configurationsViewer.setLabelProvider(new LabelProvider() {

			@Override
			public Image getImage(Object element) {
				return null;
			}

			@Override
			public String getText(Object element) {
				String text = null;
				if (element instanceof MTJRuntime) {
					MTJRuntime runtime = (MTJRuntime) element;
					text = runtime.getName();
				}
				return text;
			}
		});
		
		combo.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {}

			public void widgetSelected(SelectionEvent e) {
				updateViewer();
			}
		});
		
		MTJRuntimeList runtimeList = this.mtjProject.getRuntimeList();
		this.currentRuntime = runtimeList.getActiveMTJRuntime();
		configurationsViewer.setInput(runtimeList);
		configurationsViewer.setSelection(new StructuredSelection(this.currentRuntime));
	}

	/**
	 *  Stores the current configuration build properties.
	 */
	private void updateModel() {
		List<IResource> resources = new ArrayList<IResource>();
		Object[] selected = this.resourcesViewer.getCheckedElements();
		for (Object object : selected) {
			if (object instanceof IResource) {
				resources.add(((IResource)object));
			}
		}
		this.properties.setBuildProperty(this.currentRuntime, 
				resources.toArray(new IResource[resources.size()]));
	}

	/**
	 * Updates the resource viewer selection.
	 */
	private void updateViewer() {
		ISelection selection = this.configurationsViewer.getSelection();
		if (!(selection instanceof IStructuredSelection) || selection.isEmpty()) {
			this.currentRuntime = this.mtjProject.getRuntimeList().getActiveMTJRuntime();
			this.configurationsViewer.setSelection(new StructuredSelection(this.currentRuntime));
		} else {			
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			this.currentRuntime = (MTJRuntime) structuredSelection.getFirstElement();
		}
		
		IResource[] resources = this.properties.getBuildProperty(this.currentRuntime);
		resourcesViewer.setCheckedElements(resources);
	}
	
	/**
	 * Creates the resources tree section.
	 * 
	 * @param managedForm managed form
	 * @param parent parent composite.
	 */
	private void createResourcesSection(IManagedForm managedForm, Composite parent) {
		GridData gridData = null;
		
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		Composite section = createSection(managedForm, parent, MTJUIMessages.BuildPropertiesPage_resourcesSectionTitle
				, MTJUIMessages.BuildPropertiesPage_resourcesSectionDescription, gridData);
		section.setLayout(new GridLayout(0x01, true));
		
		Tree tree = new Tree(section, SWT.MULTI | SWT.CHECK | SWT.BORDER);
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		resourcesViewer = new ContainerCheckedTreeViewer(tree);
		resourcesViewer.setContentProvider(new WorkbenchContentProvider());
		resourcesViewer.setLabelProvider(new WorkbenchLabelProvider());
		resourcesViewer.setInput(this.mtjProject.getProject());
		resourcesViewer.setFilters(new ViewerFilter[] {
				new SourceFolderFilter(this.mtjProject.getJavaProject())});
		resourcesViewer.addCheckStateListener(new ICheckStateListener(){

			/* (non-Javadoc)
			 * @see org.eclipse.jface.viewers.ICheckStateListener#checkStateChanged(org.eclipse.jface.viewers.CheckStateChangedEvent)
			 */
			public void checkStateChanged(CheckStateChangedEvent event) {
				updateModel();
				setDirty(true);
			}
		});
		this.updateViewer();
	}

	/**
	 * Creates a generic section.
	 * 
	 * @param managedForm
	 *            parent form.
	 * @param body
	 *            parent composite.
	 */
	private Composite createSection(IManagedForm managedForm, Composite parent
			, String text, String description, GridData gridData) {
		FormToolkit toolkit = managedForm.getToolkit();

		Section section = toolkit.createSection(parent,
				Section.DESCRIPTION | ExpandableComposite.TITLE_BAR);
		if (text != null) {
			section.setText(text);
		}
		if (description != null) {
			section.setDescription(description);
		}
		section.setLayoutData(gridData);
		section.setLayout(new GridLayout(0x01, false));

		Composite client = toolkit.createComposite(section, SWT.NONE);
		client.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		section.setClient(client);
		return client;
	}

    /**
     * Set the dirty flag and let the editor know the state has changed.
     * 
     * @param dirty whether the page contents are currently dirty
     */
    private void setDirty(boolean dirty) {
        this.dirty = dirty;
        getEditor().editorDirtyStateChanged();
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#isDirty()
     */
    @Override
    public boolean isDirty() {
        return dirty || super.isDirty();
    }
    
    private class SourceFolderFilter extends ViewerFilter {

    	private IPath[] sources;
    	
    	public SourceFolderFilter(IJavaProject javaProject) {
			List<IPath> paths = new ArrayList<IPath>();
			try {
				IClasspathEntry[] classpath = javaProject.getRawClasspath();
				for (IClasspathEntry entry : classpath) {
					if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
						paths.add(entry.getPath());
					}
				}				
			} catch (JavaModelException e) {
				MTJLogger.log(IStatus.ERROR, e);
			}
			this.sources = paths.toArray(new IPath[paths.size()]);
		}
    	
		@Override
		public boolean select(Viewer viewer, Object parentElement, Object element) {
			boolean result = false;
			if (element instanceof IResource) {
				IResource resource = (IResource) element;
				IPath path = resource.getFullPath();
				for (IPath source : this.sources) {
					if (source.isPrefixOf(path)) {
						result = true;
						break;
					}
				}
			}
			return result;
		}
    	
    }

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.build.MTJBuildPropertiesChangeListener#propertiesChanged(org.eclipse.mtj.internal.core.build.MTJBuildProperties)
	 */
	public void propertiesChanged(final MTJBuildProperties buildProperties) {
		if (this.isActive() && this.currentRuntime != null) {			
			Display.getDefault().syncExec(new Runnable(){
				public void run() {					
					IResource[] resources = buildProperties.getBuildProperty(currentRuntime);
					resourcesViewer.refresh();
					resourcesViewer.setCheckedElements(resources);
					setDirty(false);
				}
			});
		}
	}
}
