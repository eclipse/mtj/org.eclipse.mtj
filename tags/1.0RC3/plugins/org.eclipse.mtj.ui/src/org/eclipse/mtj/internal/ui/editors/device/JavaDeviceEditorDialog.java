/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.internal.ui.editors.device;

import org.eclipse.jface.window.IShellProvider;
import org.eclipse.mtj.ui.editors.device.DeviceEditorDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * A {@link DeviceEditorDialog} that edits devices that are launched by the Java
 * executable.
 * 
 * @author Craig Setera
 */
public class JavaDeviceEditorDialog extends DeviceEditorDialog {

    /**
     * Construct a new Java Device editor dialog.
     * 
     * @param parentShell the parent shell, or <code>null</code> to create a
     *                top-level shell
     */
    public JavaDeviceEditorDialog(Shell parentShell) {
        super(parentShell);
    }

    /**
     * Construct a new Java Device editor dialog.
     * 
     * @param parentShell object that returns the current parent shell
     */
    public JavaDeviceEditorDialog(IShellProvider parentShell) {
        super(parentShell);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.device.editor.DeviceEditorDialog#isJavaExecutableDevice()
     */
    protected boolean isJavaExecutableDevice() {
        return true;
    }
}
