/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui;

/**
 * Listing of constants used in MTJ preferences
 * 
 * @since 0.9.1
 */
public interface IPreferenceConstants {

    /**
     * Editor folding preference
     */
    public static final String EDITOR_FOLDING_ENABLED = "editor.folding"; //$NON-NLS-1$ 
}
