/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Fixed invalid behavior while displaying error
 *                                messages.   
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     David Aragao (Motorola)  - Validating deployment directory name.                     
 */
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

/**
 * Preference page implementation for setting J2ME preferences.
 * 
 * @author Craig Setera
 */
public class J2MEPreferencePage extends FieldEditorPreferencePage implements
        IWorkbenchPreferencePage, IMTJCoreConstants {

    public static final String ID = "org.eclipse.mtj.ui.preferences.J2MEPreferencePage"; //$NON-NLS-1$
    private IntegerFieldEditor dbgServPoll;
    private IntegerFieldEditor dbgTimeOut;
    private StringFieldEditor deploymentDirectory;

    /**
     * Default constructor.
     */
    public J2MEPreferencePage() {
        super(GRID);
        setPreferenceStore(MTJUIPlugin.getDefault().getCorePreferenceStore());
        setDescription(MTJUIMessages.J2MEPreferencePage_description);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
     */
    @Override
    public void createFieldEditors() {
        int columnSpan = 3;
        Composite parent = getFieldEditorParent();

        deploymentDirectory =  new StringFieldEditor(PREF_DEPLOYMENT_DIR,
                MTJUIMessages.J2MEPreferencePage_deployment_directory, parent);
        deploymentDirectory.setErrorMessage(deploymentDirectory.getLabelText() + " "
                + deploymentDirectory.getErrorMessage());

        addField(deploymentDirectory);
        
        addSpacer(parent, columnSpan);

        Group antennaGroup = new Group(parent, SWT.NONE);
        antennaGroup.setText(MTJUIMessages.J2MEPreferencePage_antenna_settings);
        antennaGroup.setLayout(new GridLayout(1, true));
        GridData antennaGD = getColumnSpanGridData(columnSpan);
        antennaGD.minimumWidth = 500;
        antennaGroup.setLayoutData(antennaGD);

        Composite antennaComposite = new Composite(antennaGroup, SWT.NONE);
        antennaComposite.setLayout(new GridLayout(3, true));
        antennaComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

        FileFieldEditor antennaJarEditor = new FileFieldEditor(
                PREF_ANTENNA_JAR, MTJUIMessages.J2MEPreferencePage_antenna_JAR,
                true, antennaComposite);
        antennaJarEditor.setFileExtensions(new String[] { "*.jar" }); //$NON-NLS-1$
        addField(antennaJarEditor);

        DirectoryFieldEditor wtkRootEditor = new DirectoryFieldEditor(
                PREF_WTK_ROOT, MTJUIMessages.J2MEPreferencePage_WTK_root,
                antennaComposite);
        addField(wtkRootEditor);

        addSpacer(parent, columnSpan);

        dbgTimeOut = new IntegerFieldEditor(PREF_RMTDBG_TIMEOUT,
                MTJUIMessages.J2MEPreferencePage_debug_server_time_out, parent);
        dbgTimeOut.setErrorMessage(dbgTimeOut.getLabelText()
                + MTJUIMessages.J2MEPreferencePage_0
                + dbgTimeOut.getErrorMessage());

        addField(dbgTimeOut);

        dbgServPoll = new IntegerFieldEditor(PREF_RMTDBG_INTERVAL,
                MTJUIMessages.J2MEPreferencePage_debug_server_poll_interval,
                parent);
        dbgServPoll.setErrorMessage(dbgServPoll.getLabelText() + " "
                + dbgServPoll.getErrorMessage());

        addField(dbgServPoll);

        Label l = new Label(parent, SWT.NONE);
        l.setText(MTJUIMessages.J2MEPreferencePage_maximum_duration_launch);
        l.setLayoutData(getColumnSpanGridData(columnSpan));
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent event) {
        super.propertyChange(event);

        /*
         * Check if "Debug Server Time-out (ms)" and "Debug Server Launch Poll
         * Interval (ms)" have valid values. If not, display the correct error
         * message.
         * Check if "Deployment directory" is a valid folder name. If not, show a error 
         * message
         */
        boolean dbgServPollValid = dbgServPoll.isValid();
        boolean dbgTimeOutValid = dbgTimeOut.isValid();
        boolean deploymentDirectoryIsValid = Utils.isValidFolderName(deploymentDirectory.getStringValue());        

        if(!deploymentDirectoryIsValid || !dbgServPollValid || !dbgTimeOutValid){
	        if(!deploymentDirectoryIsValid){
	        	this.setErrorMessage(deploymentDirectory.getErrorMessage());
	        	this.setValid(Utils.isValidFolderName(deploymentDirectory.getStringValue()));
	        }
	        if (!dbgServPollValid) {
	            this.setErrorMessage(dbgServPoll.getErrorMessage());
	        } else if (!dbgTimeOutValid) {
	            this.setErrorMessage(dbgTimeOut.getErrorMessage());
	        }
        }else {
        	this.setValid(true);
		}
    }

    /**
     * Add some horizontal space to the dialog.
     * 
     * @param parent
     * @param columnSpan
     */
    private void addSpacer(Composite parent, int columnSpan) {
        Label l = new Label(parent, SWT.NONE);
        l.setLayoutData(getColumnSpanGridData(columnSpan));
    }

    /**
     * Return a new GridData object with the specified column span.
     * 
     * @param columnSpan
     * @return
     */
    private GridData getColumnSpanGridData(int columnSpan) {
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = columnSpan;

        return gd;
    }

    /**
     * Overridden so that we get the help context where it belongs so that it
     * works when the focus is in the left pane.
     * 
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_J2MEPreferencePage"); //$NON-NLS-1$

        return (super.createContents(parent));
    }
}
