/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Refactoring build.properties
 */
package org.eclipse.mtj.internal.ui.editors.build.pages;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.internal.core.build.MTJBuildProperties;
import org.eclipse.mtj.internal.core.build.MTJBuildPropertiesChangeListener;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;

/**
 * BuildPropertiesSourcePage class provides a {@link FormPage} instance
 * to manage build.properties files as a source editor.
 * 
 * @author David Marques
 */
public class BuildPropertiesSourcePage extends FormPage implements MTJBuildPropertiesChangeListener {

	private MTJBuildProperties properties;
	private boolean dirty;
	private Text text;
	
	/**
	 * Creates an instance of a BuildPropertiesSourcePage to manage
	 * the build.properties of the specified MTJ project.
	 * 
	 * @param editor parent editor.
	 * @param buildProperties target project.
	 */
	public BuildPropertiesSourcePage(FormEditor editor, MTJBuildProperties buildProperties) {
		super(editor, "buildPropertiesPageSource", "build.properties"); //$NON-NLS-1$ //$NON-NLS-2$
		this.properties = buildProperties;
		this.properties.addPropertiesChangeListener(this);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
	 */
	protected void createFormContent(IManagedForm managedForm) {
		FormToolkit  toolkit = managedForm.getToolkit();
		ScrolledForm form    = managedForm.getForm();

		toolkit.decorateFormHeading(form.getForm());
		form.setExpandHorizontal(true);
		form.setExpandVertical(true);

		Composite body = form.getBody();
		body.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		GridLayout layout = new GridLayout(0x01, true);
		body.setLayout(layout);
		
		this.createTextArea(managedForm, body);
		form.reflow(true);
	}

	/**
	 * Creates a text editing area.
	 * 
	 * @param managedForm parent form.
	 * @param body parent composite.
	 */
	private void createTextArea(IManagedForm managedForm, Composite body) {
		text = new Text(body, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		text.addModifyListener(new ModifyListener(){
			public void modifyText(ModifyEvent e) {
				updateModel();
				setDirty(true);
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.forms.editor.FormPage#setActive(boolean)
	 */
	public void setActive(boolean active) {
		super.setActive(active);
		
		if (active) {
			this.updateViewer();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.forms.editor.FormPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void doSave(IProgressMonitor monitor) {
		this.setDirty(false);
	}
	
    /**
     * Set the dirty flag and let the editor know the state has changed.
     * 
     * @param dirty whether the page contents are currently dirty
     */
    private void setDirty(boolean dirty) {
        this.dirty = dirty;
        getEditor().editorDirtyStateChanged();
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#isDirty()
     */
    @Override
    public boolean isDirty() {
        return dirty || super.isDirty();
    }
    
	/**
	 * Updates the model.
	 */
	private void updateModel() {
    	try {
			ByteArrayInputStream stream = new ByteArrayInputStream(text
					.getText().getBytes());
			properties.load(stream);
		} catch (IOException e) {
			MTJLogger.log(IStatus.ERROR, e);
		}
	}
    
	/**
	 * Updates the viewer.
	 */
    private void updateViewer() {
    	this.text.setText(this.properties.getContent().toString());
    	//Avoids setting the page dirty.
    	this.setDirty(false);
    }
    
    /* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.build.MTJBuildPropertiesChangeListener#propertiesChanged(org.eclipse.mtj.internal.core.build.MTJBuildProperties)
	 */
	public void propertiesChanged(MTJBuildProperties buildProperties) {
		if (this.isActive()) {
			Display.getDefault().syncExec(new Runnable(){
				public void run() {
					updateViewer();
				}
			});
		}
	}
}
