/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.toolkit.uei.properties;

/**
 * When the emulator runs with the {@link EmulatorInfoArgs#XQUERY -Xquery}
 * option, information describing the emulator and its capabilities are sent to
 * standard output. The general format is that of a properties file.
 * <p>
 * The properties describing <i>common information</i> for all devices are
 * represented by this enum.
 * </p>
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public enum DevicesProperties {

    /**
     * <b>Property Key</b>: <code>device.list</code><br>
     * <b>Value</b>: Comma-separated list of device names. Even if an emulator
     * supports only one device, this property must still be produced.
     */
    DEVICE_LIST("device.list", UEIImplementationReq.REQUIRED),

    /**
     * <b>Property Key</b>: <code>uei.arguments</code><br>
     * <b>Value</b>: List of the UEI arguments supported by all devices.
     */
    UEI_ARGUMENTS("uei.arguments", UEIImplementationReq.OPTIONAL),

    /**
     * <b>Property Key</b>: <code>uei.version</code><br>
     * <b>Value</b>: The version of the UEI specification supported by this
     * emulator. The value must be either <code>1.0</code> or <code>1.0.1</code>
     * . If this property is not specified, its value is <code>1.0</code>.
     */
    UEI_VERSION("uei.version", UEIImplementationReq.OPTIONAL),

    /**
     * <b>Property Key</b>: <code>security.domains</code><br>
     * <b>Value</b>: Comma-separated list of security domains supported by the
     * emulator.
     */
    SECURITY_DOMAINS("security.domains", UEIImplementationReq.OPTIONAL);

    /**
     * Defines the implementation Requirement for each property.
     */
    private UEIImplementationReq implementationRequirement;

    /**
     * A property key.
     */
    private String propertyKey;

    /**
     * Creates a new DevicesProperties instance.
     * 
     * @param propertyKey the property key
     * @param impl the implementation requirement for this property.
     */
    private DevicesProperties(String propertyKey, UEIImplementationReq req) {
        this.propertyKey = propertyKey;
        this.implementationRequirement = req;
    }

    /**
     * Return the implementation requirement for this property.
     * 
     * @return {@link UEIImplementationReq#REQUIRED} if this property must be
     *         implemented or {@link UEIImplementationReq#OPTIONAL} if not.
     */
    public UEIImplementationReq getImplementationRequirement() {
        return implementationRequirement;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return propertyKey;
    }
}
