/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.refactoring;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RenameParticipant;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.build.MTJBuildProperties;

/**
 * BuildPropertiesPackageRenameParticipant class is responsible for updating
 * build.properties package references to resources after they have been renamed.
 * 
 * @author David Marques
 */
public class BuildPropertiesPackageRenameParticipant extends RenameParticipant {

    private IPackageFragment packageFragment;

    /* (non-Javadoc)
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#checkConditions(org.eclipse.core.runtime.IProgressMonitor, org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext)
     */
    public RefactoringStatus checkConditions(IProgressMonitor pm,
            CheckConditionsContext context) throws OperationCanceledException {
        return new RefactoringStatus();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#createChange(org.eclipse.core.runtime.IProgressMonitor)
     */
    public Change createChange(IProgressMonitor pm) throws CoreException,
            OperationCanceledException {
        
    	CompositeChange result = new CompositeChange(Messages.BuildPropertiesPackageRenameParticipant_buildPropertiesChangeMessage);
        // Creates the changes needed by package renames
        this.createBuildPropertiesChanges(result);
        return result.getChildren().length > 0 ? result : null;
    }

    /**
     * Creates all {@link Change} instances needed by the package rename in
     * order to update the build.properties file. 
     * 
     * @param compositeChange parent {@link CompositeChange} instance.
     */
    private void createBuildPropertiesChanges(CompositeChange compositeChange) {
    	if (this.packageFragment == null) {
			return;
		}
    	
    	IResource resource = this.packageFragment.getResource();
    	if (resource != null) {    		
    		String oldName = this.packageFragment.getElementName().replace(".", "/"); //$NON-NLS-1$ //$NON-NLS-2$
    		String newName = getArguments().getNewName().replace(".", "/"); //$NON-NLS-1$ //$NON-NLS-2$
			
    		IPath oldPath = resource.getProjectRelativePath();
    		IPath newPath = new Path(oldPath.toString().replace(oldName, newName));
			
			IFile properties = resource.getProject().getFile(MTJBuildProperties.FILE_NAME);
			if (properties.exists()) {				
				BuildPropertiesChange change = new BuildPropertiesChange(
						properties, oldPath.toString(), newPath.toString());
				if (change.hasPropertiesChanges()) {
					compositeChange.add(change);
				}
			}
		}
	}

    /* (non-Javadoc)
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#getName()
     */
    public String getName() {
        return Messages.BuildPropertiesPackageRenameParticipant_buildPropertiesPackageRenamePaticipant;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#initialize(java.lang.Object)
     */
    protected boolean initialize(Object element) {
        packageFragment = (IPackageFragment) element;
        return true;
    }

}
