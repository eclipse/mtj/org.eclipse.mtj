/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)          - Initial implementation
 *     Gustavo de Paula (Motorola) - Configuration refactory [Bug 265522]
 *     Diego Sandin (Motorola)     - Enhanced javadoc [bug 270532]
 */

package org.eclipse.mtj.core.project.runtime.event;

import java.util.EventObject;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;

/**
 * MTJRuntimeNameChangeEvent is used to notify that the name of the MTJRuntime
 * has changed.
 * <p>
 * All Events are constructed with a reference to the object, the "source", that
 * is logically deemed to be the object upon which the Event in question
 * initially occurred upon.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 * 
 * @since 1.0
 */
public class MTJRuntimeNameChangeEvent extends EventObject {

    /**
     * The default serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The previous MTJRuntime name.
     */
    private String oldRuntimeName;

    /**
     * The new MTJRuntime name.
     */
    private String newRuntimeName;

    /**
     * Creates a new instance of MTJRuntimeNameChangeEvent.
     * 
     * @param source The MTJRuntime that has been changed.
     * @param oldRuntimeName The previous MTJRuntime name.
     * @param newRuntimeName The new MTJRuntime name.
     */
    public MTJRuntimeNameChangeEvent(MTJRuntime source, String oldRuntimeName,
            String newRuntimeName) {
        super(source);
        this.oldRuntimeName = oldRuntimeName;
        this.newRuntimeName = newRuntimeName;
    }

    /**
     * Return the new MTJRuntime name.
     * 
     * @return the new MTJRuntime name.
     */
    public String getNewRuntimeName() {
        return newRuntimeName;
    }

    /**
     * Return the previous MTJRuntime name.
     * 
     * @return the previous MTJRuntime name.
     */
    public String getOldRuntimeName() {
        return oldRuntimeName;
    }

    /**
     * Set the new MTJRuntime name.
     * 
     * @param newRuntimeName the new MTJRuntime name.
     */
    public void setNewRuntimeName(String newRuntimeName) {
        this.newRuntimeName = newRuntimeName;
    }

    /**
     * Set the previous MTJRuntime name.
     * 
     * @param oldRuntimeName the previous MTJRuntime name.
     */
    public void setOldRuntimeName(String oldRuntimeName) {
        this.oldRuntimeName = oldRuntimeName;
    }

}
