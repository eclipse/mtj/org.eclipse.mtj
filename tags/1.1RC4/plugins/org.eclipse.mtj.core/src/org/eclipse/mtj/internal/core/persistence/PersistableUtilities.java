/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.persistence;

import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * Utility methods related to the persistence mechanism.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 * @author Craig Setera
 */
public class PersistableUtilities {

    /**
     * Clone the specified persistable using the underlying persistence
     * functionality.
     * 
     * @param persistable
     * @return
     * @throws PersistenceException
     */
    public static IPersistable clonePersistable(IPersistable persistable)
            throws PersistenceException {
        XMLPersistenceProvider writeProvider = new XMLPersistenceProvider(
                "clone"); //$NON-NLS-1$
        writeProvider.storePersistable("persistable", persistable); //$NON-NLS-1$
        XMLPersistenceProvider readProvider = new XMLPersistenceProvider(
                writeProvider.getDocument());

        return readProvider.loadPersistable("persistable"); //$NON-NLS-1$
    }

    /**
     * Static-only access
     */
    private PersistableUtilities() {
        super();
    }
}
