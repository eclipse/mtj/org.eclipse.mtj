/**
 * Copyright (c) 2009 Research In Motion Limited.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial Version
 */
package org.eclipse.mtj.internal.core.sdk;

import java.util.Collection;

import org.eclipse.mtj.core.sdk.ISDKProvider;

/**
 * A listener for additions to and removals from the ISDKProviderRegistry.
 * @since 1.1
 */
public interface ISDKProviderRegistryListener {

    /**
     * The specified SDK providers have been added to the registry.
     * <p>
     * @param sdkProviders The SDK providers that were added to the registry.
     */
    public void sdkProvidersAdded(Collection<ISDKProvider> providers);

    /**
     * The specified SDK providers have been removed from the registry.
     * <p>
     * @param sdkProviders the SDK providers that were removed from the registry.
     */
    public void sdkProvidersRemoved(Collection<ISDKProvider> providers);
}
