/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.refactoring;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.mapping.IResourceChangeDescriptionFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.MoveParticipant;
import org.eclipse.ltk.core.refactoring.participants.ResourceChangeChecker;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.build.MTJBuildProperties;
import org.eclipse.osgi.util.NLS;

/**
 * BuildPropertiesMoveParticipant class is responsible for updating
 * build.properties file references to resources after they have been moved.
 * 
 * @author David Marques
 */
public class BuildPropertiesMoveParticipant extends MoveParticipant {

	private IType type;
	private IFile file;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#
	 * checkConditions(org.eclipse.core.runtime.IProgressMonitor,
	 * org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext)
	 */
	public RefactoringStatus checkConditions(IProgressMonitor pm,
			CheckConditionsContext context) throws OperationCanceledException {
		ResourceChangeChecker checker = (ResourceChangeChecker) context.getChecker(ResourceChangeChecker.class);
		if (checker != null) {
			if (file.exists()) {
				IResourceChangeDescriptionFactory factory = checker.getDeltaFactory();
				factory.change(file);
			}
		}
		return new RefactoringStatus();
	}

	public Change createChange(IProgressMonitor pm) throws CoreException,
			OperationCanceledException {
		CompositeChange compositeChange = new CompositeChange(Messages.BuildPropertiesMoveParticipant_buildPropertiesChangeMessage);
		
		Object destination = getArguments().getDestination();
		if (destination instanceof IPackageFragment) {
			IPackageFragment newPackage = (IPackageFragment) destination;
			
			IPath oldPath = this.type.getResource().getProjectRelativePath();
			IPath newPath = newPackage.getResource().getProjectRelativePath();
			
			String newName = NLS.bind("{0}/{1}.java",  //$NON-NLS-1$
					new String[] {newPath.toString(), type.getElementName()});
			
			BuildPropertiesChange change = new BuildPropertiesChange(this.file
					, oldPath.toString(), newName);
			if (change.hasPropertiesChanges()) {
				compositeChange.add(change);
			}
		}
		return compositeChange.getChildren().length > 0x00 ? compositeChange : null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#getName()
	 */
	public String getName() {
		return Messages.BuildPropertiesMoveParticipant_buildPropertiesMoveParticipantName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#
	 * initialize(java.lang.Object)
	 */
	protected boolean initialize(Object element) {
		boolean result = false;
		if (element instanceof IType) {
			this.type = (IType) element;
			
			IProject project = this.type.getJavaProject().getProject();
			this.file = project.getFile(MTJBuildProperties.FILE_NAME);
			result = true;
		}
		return result;
	}

}
