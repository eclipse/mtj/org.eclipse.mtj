/**
 * Copyright (c) 2009 Research In Motion Limited.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial Version
 */
package org.eclipse.mtj.core.sdk;

import java.util.List;

import org.eclipse.swt.graphics.Image;

/**
 * This interface represents an SDK provider. An SDK provider automatically
 * presents its SDKs and devices to MTJ through the sdkprovider extension point.
 * These devices do not have to be manually installed (imported) by the user. An
 * SDK provider may be called upon to present its list of SDKs through the
 * getSDKs() method at any time. MTJ does not cache ManagedSDKs nor
 * IManagedDevices obtained from an ISDKProvider.
 * 
 * @since 1.1
 */
public interface ISDKProvider extends Comparable<ISDKProvider> {
    
    /**
     * Return the name of this SDK provider. This name will be displayed within
     * the user interface and must never be <code>null</code>.
     * 
     * @return the name of this SDK provider.
     */
    public abstract String getName();

    /**
     * Return the displayable description of this SDK provider. This description
     * will be displayed within the user interface. If this method returns a
     * <code>null</code> value, the SDK provider's name will be used as the
     * description instead.
     * 
     * @return the description of this SDK provider or <code>null</code> if the
     *         SDK provider's name should be used instead.
     */
    public abstract String getDescription();

    /**
     * Return the unique identifier for this SDK provider. This identifier will
     * be used internally to manage the SDK provider, but will not be displayed
     * to the user.
     * 
     * @return the string that represents the unique identifier of this SDK
     *         provider.
     */
    public String getIdentifier();
    
    /**
     * Obtain the number of ISDKProviders registered.
     */
    public int getSDKCount();

    /**
     * Return the fully configured SDK instances available from this SDK provider.
     * No sort order is guaranteed.
     * 
     * @return the list of SDKs available from this provider
     */
    public List<ISDK> getSDKs();

    /**
     * Return a 16x16 pixel SDK provider logo for use in the device tree in the
     * DeviceManagementPreferencePage. Returning null will cause the
     * DeviceManagementPreferencePage to present a default file folder image.
     */
    public Image getLogo();
    
}
