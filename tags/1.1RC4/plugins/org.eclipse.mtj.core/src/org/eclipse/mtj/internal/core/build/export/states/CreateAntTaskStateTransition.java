/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.build.export.states;

import org.eclipse.mtj.internal.core.build.export.AntennaBuildExport.CreateAntTaskDoneEvent;
import org.eclipse.mtj.internal.core.statemachine.AbstractState;
import org.eclipse.mtj.internal.core.statemachine.AbstractStateMachineEvent;
import org.eclipse.mtj.internal.core.statemachine.AbstractStateTransition;

/**
 * CreateAntTaskStateTransition class extends an {@link AbstractStateTransition}
 * in order to trigger transitions every time a {@link CreateAntTaskDoneEvent}
 * occurs.
 * 
 * @author David Marques
 * @since 1.0
 */
public class CreateAntTaskStateTransition extends AbstractStateTransition {

	/**
	 * Creates a {@link CreateAntTaskStateTransition} instance with
	 * the specified source and target states.
	 * 
	 * @param _source source state.
	 * @param _target target state.
	 */
	public CreateAntTaskStateTransition(AbstractState _source, AbstractState _target) {
		super(_source, _target);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.statemachine.AbstractStateTransition#isTransitionReady(org.eclipse.mtj.internal.core.statemachine.AbstractStateMachineEvent)
	 */
	protected boolean isTransitionReady(AbstractStateMachineEvent event) {
		return event instanceof CreateAntTaskDoneEvent;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.statemachine.AbstractStateTransition#onTransition()
	 */
	protected void onTransition() {}

}
