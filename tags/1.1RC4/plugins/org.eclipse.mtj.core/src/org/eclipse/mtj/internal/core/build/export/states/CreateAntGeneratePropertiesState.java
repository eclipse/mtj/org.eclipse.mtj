/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Eric Sousa Dias (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.build.export.states;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.internal.core.build.export.AntennaBuildExport;
import org.eclipse.mtj.internal.core.build.export.AntennaExportException;
import org.eclipse.mtj.internal.core.statemachine.StateMachine;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * CreateAntGeneratePropertiesState creates property files from the Application
 * Descriptor file for the build export.
 * 
 * @author Eric Dias
 * @since 1.0
 */
public class CreateAntGeneratePropertiesState extends
        AbstractCreateAntTaskState {

    /**
     * The project's Localization Data file name.
     */
    public static final String LOCALIZATION_DATA_NAME = "Localization Data"; //$NON-NLS-1$

    /**
     * Creates a {@link CreateAntGeneratePropertiesState} instance bound to the
     * specified state machine in order to create generating-resources target
     * for the specified project.
     * 
     * @param machine bound {@link StateMachine} instance.
     * @param project target {@link IMidletSuiteProject} instance.
     * @param _document target {@link Document}.
     */
    public CreateAntGeneratePropertiesState(StateMachine machine,
            IMidletSuiteProject project, Document _document) {
        super(machine, project, _document);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.core.build.export.states.AbstractCreateAntTaskState
     * #onEnter(org.eclipse.mtj.core.project.runtime.MTJRuntime)
     */
    protected void onEnter(MTJRuntime runtime) throws AntennaExportException {
        IProject project = getMidletSuiteProject().getProject();
        Document document = getDocument();
        Element root = document.getDocumentElement();
        String configName = getFormatedName(runtime.getName());

        Element generate = XMLUtils
                .createTargetElement(
                        document,
                        root,
                        NLS.bind("generate-resources-{0}", configName), getDependencies(runtime)); //$NON-NLS-1$

        IFile file = project.getFile(LOCALIZATION_DATA_NAME);

        String values[] = new String[] { AntennaBuildExport.BUILD_FOLDER,
                configName, getFormatedName(project.getName()) };

        if (file != null && file.exists()) {
            Element generating = document.createElement("generatingProperties"); //$NON-NLS-1$

            generating.setAttribute(
                    "source", NLS.bind("../{0}", file.getName())); //$NON-NLS-1$
            generating.setAttribute(
                    "dest", NLS.bind("../{0}/{1}/{2}/resources/", values)); //$NON-NLS-1$); //$NON-NLS-1$
            generate.appendChild(generating);
        }
    }

}
