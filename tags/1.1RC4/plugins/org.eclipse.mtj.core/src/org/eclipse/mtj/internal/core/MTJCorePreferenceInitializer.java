/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Default value for default preverifier
 *     Gang Ma      (Sybase)    - Default value for preprocess debug level
 *     David Aragao (Motorola)  - Add proguard preverifier refactoring
 *     Jon Dearden  (Research In Motion) - Replaced deprecated use of Preferences 
 *                                         [Bug 285699]
 */
package org.eclipse.mtj.internal.core;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.mtj.internal.core.build.preprocessor.PreprocessorHelper;
import org.eclipse.mtj.internal.core.util.Utils;

/**
 * Preference initializer for default MTJ preferences.
 * 
 * @author Craig Setera
 */
public class MTJCorePreferenceInitializer extends AbstractPreferenceInitializer
        implements IMTJCoreConstants {

    // Default values
    public static final String PREF_DEF_DEPLOYMENT_DIR = "deployed"; //$NON-NLS-1$
    public static final String PREF_DEF_VERIFIED_DIR = "verified"; //$NON-NLS-1$

    public static final boolean PREF_DEF_USE_RESOURCES_DIR = true;
    public static final String PREF_DEF_RESOURCES_DIR = "res"; //$NON-NLS-1$
    public static final boolean PREF_DEF_FORCE_JAVA11 = true;

    public static final boolean PREF_DEF_OTA_SERVER_START_AT_START = false;
    public static final boolean PREF_DEF_OTA_PORT_DEFINED = false;
    public static final int PREF_DEF_OTA_PORT = 0;
    public static final boolean PREF_DEF_OTA_AUTODEPLOY = true;

    public static final boolean PREF_DEF_OBFUSCATION_USE_PROJECT = false;
    public static final String PREF_DEF_PROGUARD_DIR = Utils.EMPTY_STRING;
    public static final boolean PREF_DEF_PROGUARD_USE_SPECIFIED = false;
    public static final String PREF_DEF_PROGUARD_OPTIONS = "-dontusemixedcaseclassnames -dontnote -defaultpackage \'\'"; //$NON-NLS-1$
    public static final String PREF_DEF_PROGUARD_KEEP = "public class * extends javax.microedition.midlet.MIDlet"; //$NON-NLS-1$

    public static final boolean PREF_DEF_PKG_USE_PROJECT = false;
    public static final boolean PREF_DEF_PKG_AUTOVERSION = false;
    public static final String PREF_DEF_PKG_EXCLUDED_PROPS = "MIDlet-Jar-URL|MIDlet-Jar-Size"; //$NON-NLS-1$

    public static final boolean PREF_DEF_PREVERIFY_USE_PROJECT = false;
    public static final String PREF_DEF_PREVERIFY_CONFIG_LOCATION = PREF_PREVERIFY_CONFIG_LOCATION_PLATFORM;
    public static final String PREF_DEF_PREVERIFY_CONFIG_VALUE = Utils.EMPTY_STRING;
    public static final String PREF_DEF_DEFAULT_PREVERIFIER = Utils.EMPTY_STRING;
    public static final String PREF_DEF_PREVERIFY_TYPE = IMTJCoreConstants.PREF_PREVERIFY_TYPE_EMULATOR;

    public static final String PREF_DEF_WTK_ROOT = Utils.EMPTY_STRING;
    public static final String PREF_DEF_ANTENNA_JAR = Utils.EMPTY_STRING;

    public static final int PREF_DEF_RMTDBG_DELAY = 60000;
    public static final int PREF_DEF_RMTDBG_INTERVAL = 500;
    public static final boolean PREF_DEF_AUTO_LAUNCH_MIGRATION = true;

    public static final String PREF_DEF_PREPROCESS_DEBUGLEVEL = PreprocessorHelper.J2ME_PREPROCESS_DEBUG;

    /**
     * Constructor
     */
    public MTJCorePreferenceInitializer() {
        super();
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
     */
    public void initializeDefaultPreferences() {
        IEclipsePreferences defs = new DefaultScope().getNode(PLUGIN_ID);
        defs.put(PREF_DEPLOYMENT_DIR, PREF_DEF_DEPLOYMENT_DIR);
        defs.put(PREF_RESOURCES_DIR, PREF_DEF_RESOURCES_DIR);

        defs.putBoolean(PREF_USE_RESOURCES_DIR, PREF_DEF_USE_RESOURCES_DIR);
        defs.put(PREF_VERIFIED_DIR, PREF_DEF_VERIFIED_DIR);
        defs.putBoolean(PREF_FORCE_JAVA11, PREF_DEF_FORCE_JAVA11);

        defs.putBoolean(PREF_OTA_SERVER_START_AT_START,
                PREF_DEF_OTA_SERVER_START_AT_START);
        defs.putBoolean(PREF_OTA_PORT_DEFINED, PREF_DEF_OTA_PORT_DEFINED);
        defs.putInt(PREF_OTA_PORT, PREF_DEF_OTA_PORT);
        defs.putBoolean(PREF_OTA_AUTODEPLOY, PREF_DEF_OTA_AUTODEPLOY);

        defs.putBoolean(PREF_OBFUSCATION_USE_PROJECT,
                PREF_DEF_OBFUSCATION_USE_PROJECT);
        defs.put(PREF_PROGUARD_DIR, PREF_DEF_PROGUARD_DIR);
        defs.putBoolean(PREF_PROGUARD_USE_SPECIFIED,
                PREF_DEF_PROGUARD_USE_SPECIFIED);
        defs.put(PREF_PROGUARD_OPTIONS, PREF_DEF_PROGUARD_OPTIONS);
        defs.put(PREF_PROGUARD_KEEP, PREF_DEF_PROGUARD_KEEP);

        defs.putBoolean(PREF_PKG_USE_PROJECT, PREF_DEF_PKG_USE_PROJECT);
        defs.putBoolean(PREF_PKG_AUTOVERSION, PREF_DEF_PKG_AUTOVERSION);
        defs.put(PREF_PKG_EXCLUDED_PROPS, PREF_DEF_PKG_EXCLUDED_PROPS);

        defs.putBoolean(PREF_PREVERIFY_USE_PROJECT,
                PREF_DEF_PREVERIFY_USE_PROJECT);
        defs.put(PREF_PREVERIFY_CONFIG_LOCATION,
                PREF_DEF_PREVERIFY_CONFIG_LOCATION);
        defs.put(PREF_PREVERIFY_CONFIG_VALUE, PREF_DEF_PREVERIFY_CONFIG_VALUE);
        defs.put(PREF_PREVERIFY_CONFIG_VALUE, PREF_DEF_PREVERIFY_TYPE);
        defs.put(PREF_DEFAULT_PREVERIFIER, PREF_DEF_DEFAULT_PREVERIFIER);

        defs.put(PREF_ANTENNA_JAR, PREF_DEF_ANTENNA_JAR);
        defs.put(PREF_WTK_ROOT, PREF_DEF_WTK_ROOT);

        defs.putInt(PREF_RMTDBG_TIMEOUT, PREF_DEF_RMTDBG_DELAY);
        defs.putInt(PREF_RMTDBG_INTERVAL, PREF_DEF_RMTDBG_INTERVAL);
        defs.putBoolean(PREF_AUTO_LAUNCH_MIGRATION,
                PREF_DEF_AUTO_LAUNCH_MIGRATION);

        defs.put(PREF_PREPROCESS_DEBUG_LEVEL, PREF_DEF_PREPROCESS_DEBUGLEVEL);
    }

}
