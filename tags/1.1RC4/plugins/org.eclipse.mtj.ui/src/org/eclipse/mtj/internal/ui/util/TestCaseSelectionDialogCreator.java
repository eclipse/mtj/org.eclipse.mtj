/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Aragao (Motorola) - Initial version
 */

package org.eclipse.mtj.internal.ui.util;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.ui.IJavaElementSearchConstants;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.mtj.internal.core.util.TestCaseSearchScope;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.SelectionDialog;

/**
 * A simple helper class for creating a Type selection dialog that allows for
 * selection of Test Case subclasses.
 * 
 * @author David Aragao
 */

public class TestCaseSelectionDialogCreator {
	
	 /**
     * Create a new Test Case selection dialog.
     * 
     * @param shell parent the parent shell of the dialog to be created
     * @param context the runnable context used to show progress when the dialog
     *            is being populated
     * @param javaProject the project that contains the TestCases to be included
     * @param multipleSelect true if multiple selection is allowed
     * @return The dialog created
     * @throws JavaModelException if the selection dialog could not be opened
     */
    public static SelectionDialog createTestCaseSelectionDialog(Shell shell,
            IRunnableContext context, IJavaProject javaProject,
            boolean multipleSelect) throws JavaModelException {
        return createTestCaseSelectionDialog(
                shell,
                context,
                javaProject,
                multipleSelect,
                MTJUIMessages.TestCaseSelectionDialogCreator_createTestCaseSelectionDialog_message);
    }

    /**
     * Create a new Test Case selection dialog.
     * 
     * @param shell parent the parent shell of the dialog to be created
     * @param context the runnable context used to show progress when the dialog
     *            is being populated
     * @param javaProject the project that contains the TestCases to be included
     * @param multipleSelect true if multiple selection is allowed
     * @param message a custom message to show in the dialog
     * @return The dialog created
     * @throws JavaModelException if the selection dialog could not be opened
     */
    public static SelectionDialog createTestCaseSelectionDialog(Shell shell,
            IRunnableContext context, IJavaProject javaProject,
            boolean multipleSelect, String message) throws JavaModelException {
        IJavaSearchScope searchScope = new TestCaseSearchScope(javaProject);

        SelectionDialog dialog = JavaUI.createTypeDialog(shell, context,
                searchScope,
                IJavaElementSearchConstants.CONSIDER_CLASSES,
                multipleSelect, "**"); //$NON-NLS-1$

        dialog
                .setTitle(MTJUIMessages.TestCaseSelectionDialogCreator_createTestCaseSelectionDialog_title);
        dialog
                .setMessage(MTJUIMessages.TestCaseSelectionDialogCreator_createTestCaseSelectionDialog_message);

        return dialog;
    }

    // Private constructor for static access
    private TestCaseSelectionDialogCreator() {
    }

}
