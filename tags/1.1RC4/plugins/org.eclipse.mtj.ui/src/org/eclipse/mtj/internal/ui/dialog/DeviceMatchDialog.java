/**
 * Copyright (c) 2009 Sony Ericsson.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Daniel Olsson (Sony Ericsson) - Initial contribution
 */
package org.eclipse.mtj.internal.ui.dialog;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.MessageDialogWithToggle;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.devices.DeviceSelector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * The DeviceMatchDialog lets the user match a SDK and a device against all
 * installed SDKs and devices.
 * 
 * @author Daniel Olsson
 */
public class DeviceMatchDialog extends MessageDialogWithToggle {

    private DeviceSelector deviceSelector;
    private IDevice matchedDevice;
    private String deviceGroup;
    private String deviceName;
    private String previousDeviceName;
    private String configurationName = null;
    private Text configurationNameText;

    public DeviceMatchDialog(Shell parentShell) {
        super(parentShell, MTJUIMessages.DeviceMatchDialog_DialogTitle, null,
                null, MessageDialog.NONE, new String[] {
                        MTJUIMessages.DeviceMatchDialog_OKButton,
                        MTJUIMessages.DeviceMatchDialog_CancelButton }, 0,
                MTJUIMessages.DeviceMatchDialog_ToggleButton, false);
        setShellStyle(SWT.APPLICATION_MODAL | SWT.TITLE | SWT.BORDER);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
     */
    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.OK_ID) {
            matchedDevice = deviceSelector.getSelectedDevice();
            if (configurationNameText != null) {
                configurationName = configurationNameText.getText();
            }
        }
        super.buttonPressed(buttonId);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.dialogs.Dialog#createCustomArea(org.eclipse.swt.widgets
     * .Composite)
     */
    @Override
    protected Control createCustomArea(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, true));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        /* Config name edit box */
        Composite configCombo = new Composite(composite, SWT.NONE);
        configCombo.setLayoutData(new GridData(GridData.FILL_BOTH));
        configCombo.setLayout(new GridLayout(2, false));

        if (configurationName != null) {
            // The following code (configuration name), has been copied
            // from ConfigAddAndEditWizardPage
            Label label = new Label(configCombo, SWT.NONE);
            label
                    .setText(MTJUIMessages.Configuration_ConfigurationAddWizardPage_NewConfigurationName);
            configurationNameText = new Text(configCombo, SWT.BORDER);
            configurationNameText
                    .setLayoutData(new GridData(GridData.FILL_BOTH));
        }

        /* SDK, device to match */
        Group group = new Group(composite, SWT.NONE);
        group.setText(MTJUIMessages.DeviceMatchDialog_GroupToMatch);

        group.setLayoutData(new GridData(GridData.FILL_BOTH));
        group.setLayout(new GridLayout(2, false));

        (new Label(group, SWT.NONE))
                .setText(MTJUIMessages.DeviceMatchDialog_GroupToMatchDesc);
        new Label(group, SWT.NONE);

        Composite comboComposite = new Composite(group, SWT.NONE);
        comboComposite.setLayout(new GridLayout(2, false));

        (new Label(comboComposite, SWT.NONE))
                .setText(MTJUIMessages.DeviceSelector_4);
        Label deviceGroupLabel = new Label(comboComposite, SWT.NONE);
        deviceGroupLabel.setText(getDeviceGroup());
        (new Label(comboComposite, SWT.NONE))
                .setText(MTJUIMessages.DeviceSelector_device_label);
        Label deviceNameLabel = new Label(comboComposite, SWT.NONE);
        deviceNameLabel.setText(getDeviceName());

        /* SDK, device to match the above with */
        deviceSelector = new DeviceSelector();
        deviceSelector
                .setDeviceGroupLabel(MTJUIMessages.DeviceMatchDialog_GroupInstalled);
        deviceSelector
                .setDeviceGroupComentLabel(MTJUIMessages.DeviceMatchDialog_GroupInstalledDesc);
        deviceSelector.createContents(composite, true, true);
        previousDeviceName = getDeviceName();
        deviceSelector
                .setSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        if ((configurationNameText != null)
                                && (previousDeviceName
                                        .equals(configurationNameText.getText()))) {
                            configurationNameText.setText(deviceSelector
                                    .getSelectedDevice().getName());
                            previousDeviceName = deviceSelector
                                    .getSelectedDevice().getName();
                        }
                    }
                });

        if (configurationNameText != null) {
            configurationNameText.setText(configurationName);
        }

        return composite;
    }

    private String getDeviceName() {
        return deviceName;
    }

    private String getDeviceGroup() {
        return deviceGroup;
    }

    /**
     * @param deviceGroup the deviceGroup to set
     */
    public void setDeviceGroup(String deviceGroup) {
        this.deviceGroup = deviceGroup;
    }

    /**
     * @param deviceName the deviceName to set
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     * @return The by user selected device
     */
    public IDevice getSelectedDevice() {
        return matchedDevice;
    }

    /**
     * @return The by user entered configuration name
     */
    public String getConfigurationName() {
        return configurationName;
    }

    /**
     * @param configurationName the configurationName to set
     */
    public void setConfigurationName(String configurationName) {
        this.configurationName = configurationName;
    }
}
