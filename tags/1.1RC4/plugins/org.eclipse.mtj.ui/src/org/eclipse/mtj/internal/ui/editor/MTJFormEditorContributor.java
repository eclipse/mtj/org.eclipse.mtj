/**
 * Copyright (c) 2003, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import java.util.Hashtable;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.mtj.internal.core.IBaseModel;
import org.eclipse.mtj.internal.core.IEditable;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.forms.editor.IFormPage;
import org.eclipse.ui.ide.IDEActionFactory;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;
import org.eclipse.ui.texteditor.IUpdate;

/**
 * @since 0.9.1
 */
public class MTJFormEditorContributor extends
        MultiPageEditorActionBarContributor {

    class ClipboardAction extends GlobalAction {

        /**
         * @param id
         */
        public ClipboardAction(String id) {
            super(id);
            setEnabled(false);
        }

        /**
         * @return
         */
        public boolean isEditable() {
            if (mtjFormEditor == null) {
                return false;
            }
            IBaseModel model = mtjFormEditor.getAggregateModel();
            return model instanceof IEditable ? ((IEditable) model)
                    .isEditable() : false;
        }

        /**
         * @param selection
         */
        public void selectionChanged(ISelection selection) {
        }
    }

    class CopyAction extends ClipboardAction {

        /**
         * 
         */
        public CopyAction() {
            super(ActionFactory.COPY.getId());
            setText(MTJUIMessages.MTJFormEditorContributor_copyAction_text);
            setImageDescriptor(getSharedImages().getImageDescriptor(
                    ISharedImages.IMG_TOOL_COPY));
            setDisabledImageDescriptor(getSharedImages().getImageDescriptor(
                    ISharedImages.IMG_TOOL_COPY_DISABLED));
            setActionDefinitionId("org.eclipse.ui.edit.copy"); //$NON-NLS-1$
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditorContributor.ClipboardAction#selectionChanged(org.eclipse.jface.viewers.ISelection)
         */
        @Override
        public void selectionChanged(ISelection selection) {
            setEnabled(mtjFormEditor.canCopy(selection));
        }
    }

    class CutAction extends ClipboardAction {

        /**
         * 
         */
        public CutAction() {
            super(ActionFactory.CUT.getId());
            setText(MTJUIMessages.MTJFormEditorContributor_cutAction_text);
            setImageDescriptor(getSharedImages().getImageDescriptor(
                    ISharedImages.IMG_TOOL_CUT));
            setDisabledImageDescriptor(getSharedImages().getImageDescriptor(
                    ISharedImages.IMG_TOOL_CUT_DISABLED));
            setActionDefinitionId("org.eclipse.ui.edit.cut"); //$NON-NLS-1$
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditorContributor.ClipboardAction#selectionChanged(org.eclipse.jface.viewers.ISelection)
         */
        @Override
        public void selectionChanged(ISelection selection) {
            setEnabled(isEditable() && mtjFormEditor.canCut(selection));
        }
    }

    class GlobalAction extends Action implements IUpdate {
        private String id;

        /**
         * @param id
         */
        public GlobalAction(String id) {
            this.id = id;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.action.Action#run()
         */
        @Override
        public void run() {
            mtjFormEditor.performGlobalAction(id);
            updateSelectableActions(mtjFormEditor.getSelection());
        }

        /* (non-Javadoc)
         * @see org.eclipse.ui.texteditor.IUpdate#update()
         */
        public void update() {
            getActionBars().updateActionBars();
        }
    }

    class PasteAction extends ClipboardAction {

        /**
         * 
         */
        public PasteAction() {
            super(ActionFactory.PASTE.getId());
            setText(MTJUIMessages.MTJFormEditorContributor_pasteAction_text);
            setImageDescriptor(getSharedImages().getImageDescriptor(
                    ISharedImages.IMG_TOOL_PASTE));
            setDisabledImageDescriptor(getSharedImages().getImageDescriptor(
                    ISharedImages.IMG_TOOL_PASTE_DISABLED));
            setActionDefinitionId("org.eclipse.ui.edit.paste"); //$NON-NLS-1$
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditorContributor.ClipboardAction#selectionChanged(org.eclipse.jface.viewers.ISelection)
         */
        @Override
        public void selectionChanged(ISelection selection) {
            setEnabled(isEditable() && mtjFormEditor.canPasteFromClipboard());
        }
    }

    class RevertAction extends Action implements IUpdate {

        /**
         * 
         */
        public RevertAction() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.action.Action#run()
         */
        @Override
        public void run() {
            if (mtjFormEditor != null) {
                mtjFormEditor.doRevert();
            }
        }

        /* (non-Javadoc)
         * @see org.eclipse.ui.texteditor.IUpdate#update()
         */
        public void update() {
            setEnabled(mtjFormEditor != null ? mtjFormEditor.isDirty() : false);
        }
    }

    class SaveAction extends Action implements IUpdate {

        /**
         * 
         */
        public SaveAction() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.action.Action#run()
         */
        @Override
        public void run() {
            if (mtjFormEditor != null) {
                MTJUIPlugin.getActivePage().saveEditor(mtjFormEditor, false);
            }
        }

        /* (non-Javadoc)
         * @see org.eclipse.ui.texteditor.IUpdate#update()
         */
        public void update() {
            setEnabled(mtjFormEditor != null ? mtjFormEditor.isDirty() : false);
        }
    }

    private ClipboardAction copyAction;

    private ClipboardAction cutAction;

    private Hashtable<String, Action> globalActions = new Hashtable<String, Action>();

    private ClipboardAction pasteAction;

    private SaveAction saveAction;

    private ISharedImages sharedImages;

    protected IFormPage formPage;

    protected MTJFormEditor mtjFormEditor;

    protected RevertAction revertAction;

    public MTJFormEditorContributor(String menuName) {
    }

    /**
     * @param mng
     */
    public void addClipboardActions(IMenuManager mng) {
        mng.add(cutAction);
        mng.add(copyAction);
        mng.add(pasteAction);
        mng.add(new Separator());
        mng.add(revertAction);
    }

    /**
     * @param mng
     */
    public void contextMenuAboutToShow(IMenuManager mng) {
        contextMenuAboutToShow(mng, true);
    }

    /**
     * @param mng
     * @param addClipboard
     */
    public void contextMenuAboutToShow(IMenuManager mng, boolean addClipboard) {
        if (mtjFormEditor != null) {
            updateSelectableActions(mtjFormEditor.getSelection());
        }
        if (addClipboard) {
            addClipboardActions(mng);
        }
        mng.add(saveAction);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorActionBarContributor#contributeToCoolBar(org.eclipse.jface.action.ICoolBarManager)
     */
    @Override
    public void contributeToCoolBar(ICoolBarManager cbm) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorActionBarContributor#contributeToMenu(org.eclipse.jface.action.IMenuManager)
     */
    @Override
    public void contributeToMenu(IMenuManager mm) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorActionBarContributor#contributeToStatusLine(org.eclipse.jface.action.IStatusLineManager)
     */
    @Override
    public void contributeToStatusLine(IStatusLineManager slm) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorActionBarContributor#contributeToToolBar(org.eclipse.jface.action.IToolBarManager)
     */
    @Override
    public void contributeToToolBar(IToolBarManager tbm) {
    }

    /**
     * @return
     */
    public MTJFormEditor getEditor() {
        return mtjFormEditor;
    }

    /**
     * @param id
     * @return
     */
    public IAction getGlobalAction(String id) {
        return globalActions.get(id);
    }

    /**
     * @return
     */
    public IAction getRevertAction() {
        return revertAction;
    }

    /**
     * @return
     */
    public IAction getSaveAction() {
        return saveAction;
    }

    /**
     * @return
     */
    public IEditorActionBarContributor getSourceContributor() {
        return null;
    }

    public IStatusLineManager getStatusLineManager() {
        return getActionBars().getStatusLineManager();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorActionBarContributor#init(org.eclipse.ui.IActionBars)
     */
    @Override
    public void init(IActionBars bars) {
        super.init(bars);
        makeActions();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.MultiPageEditorActionBarContributor#setActiveEditor(org.eclipse.ui.IEditorPart)
     */
    @Override
    public void setActiveEditor(IEditorPart targetEditor) {
        if (targetEditor instanceof MTJSourcePage) {
            // Fixing the 'goto line' problem -
            // the action is thinking that source page
            // is a standalone editor and tries to activate it
            // #19361
            MTJSourcePage page = (MTJSourcePage) targetEditor;
            MTJUIPlugin.getActivePage().activate(page.getEditor());
            return;
        }
        if (!(targetEditor instanceof MTJFormEditor)) {
            return;
        }

        mtjFormEditor = (MTJFormEditor) targetEditor;
        mtjFormEditor.updateUndo(getGlobalAction(ActionFactory.UNDO.getId()),
                getGlobalAction(ActionFactory.REDO.getId()));
        setActivePage(mtjFormEditor.getActiveEditor());
        updateSelectableActions(mtjFormEditor.getSelection());
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.MultiPageEditorActionBarContributor#setActivePage(org.eclipse.ui.IEditorPart)
     */
    @Override
    public void setActivePage(IEditorPart newEditor) {
        if (mtjFormEditor == null) {
            return;
        }
        IFormPage oldPage = formPage;
        formPage = mtjFormEditor.getActivePageInstance();
        if (formPage != null) {
            updateActions();
            if ((oldPage != null) && !oldPage.isEditor()
                    && !formPage.isEditor()) {
                getActionBars().updateActionBars();
            }
        }
    }

    /**
     * 
     */
    public void updateActions() {
        saveAction.update();
        revertAction.update();
    }

    /**
     * @param selection
     */
    public void updateSelectableActions(ISelection selection) {
        if (mtjFormEditor != null) {
            cutAction.selectionChanged(selection);
            copyAction.selectionChanged(selection);
            pasteAction.selectionChanged(selection);
        }
    }

    /**
     * @param id
     */
    private void addGlobalAction(String id) {
        GlobalAction action = new GlobalAction(id);
        addGlobalAction(id, action);
    }

    /**
     * @param id
     * @param action
     */
    private void addGlobalAction(String id, Action action) {
        globalActions.put(id, action);
        getActionBars().setGlobalActionHandler(id, action);
    }

    /**
     * @return
     */
    protected ISharedImages getSharedImages() {
        if (sharedImages == null) {
            sharedImages = getPage().getWorkbenchWindow().getWorkbench()
                    .getSharedImages();
        }
        return sharedImages;
    }

    /**
     * 
     */
    protected void makeActions() {

        // clipboard actions
        cutAction = new CutAction();
        copyAction = new CopyAction();
        pasteAction = new PasteAction();
        addGlobalAction(ActionFactory.CUT.getId(), cutAction);
        addGlobalAction(ActionFactory.COPY.getId(), copyAction);
        addGlobalAction(ActionFactory.PASTE.getId(), pasteAction);
        addGlobalAction(ActionFactory.DELETE.getId());

        // undo/redo
        addGlobalAction(ActionFactory.UNDO.getId());
        addGlobalAction(ActionFactory.REDO.getId());

        // select/find
        addGlobalAction(ActionFactory.SELECT_ALL.getId());
        addGlobalAction(ActionFactory.FIND.getId());

        // bookmark
        addGlobalAction(IDEActionFactory.BOOKMARK.getId());
        // save/revert
        saveAction = new SaveAction();
        saveAction.setText(MTJUIMessages.MTJFormEditorContributor_saveAction_text);
        revertAction = new RevertAction();
        revertAction.setText(MTJUIMessages.MTJFormEditorContributor_revertAction_text);
        addGlobalAction(ActionFactory.REVERT.getId(), revertAction);
    }
}
