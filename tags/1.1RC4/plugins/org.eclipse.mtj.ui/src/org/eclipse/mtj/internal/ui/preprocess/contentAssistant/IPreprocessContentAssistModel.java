/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant;

/**
 * All preprocess content assist model should implements this interface
 * 
 * @author gma
 */
public interface IPreprocessContentAssistModel {
    /**
     * @return the description of the model
     */
    String getDescription();

    /**
     * @return the relevant document of the model
     */
    String getJavaDoc();

    /**
     * @return the name of the model
     */
    String getName();
}
