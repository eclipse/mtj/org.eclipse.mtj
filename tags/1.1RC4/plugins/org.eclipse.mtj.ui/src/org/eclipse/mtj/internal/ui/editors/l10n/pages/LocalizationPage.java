/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin  (Motorola) - Initial Version
 *     David Marques (Motorola) - Adding default locale verification on
 *                                page activation.
 */
package org.eclipse.mtj.internal.ui.editors.l10n.pages;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.mtj.internal.core.IModelChangedEvent;
import org.eclipse.mtj.internal.core.IModelChangedListener;
import org.eclipse.mtj.internal.core.text.IDocumentAttributeNode;
import org.eclipse.mtj.internal.core.text.IDocumentRange;
import org.eclipse.mtj.internal.core.text.IDocumentTextNode;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nObject;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.editor.MTJFormPage;
import org.eclipse.mtj.internal.ui.editor.MTJMasterDetailsBlock;
import org.eclipse.mtj.internal.ui.editors.l10n.L10nInputContext;
import org.eclipse.mtj.internal.ui.editors.l10n.LocalesBlock;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.IMessageManager;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.IFormPage;
import org.eclipse.ui.forms.widgets.ScrolledForm;

/**
 * 
 */
public class LocalizationPage extends MTJFormPage implements
        IModelChangedListener {

    public static final String PAGE_ID = "localizationPage"; //$NON-NLS-1$

    private LocalesBlock fBlock;

    /**
     * @param editor
     */
    public LocalizationPage(FormEditor editor) {
        super(editor, PAGE_ID, MTJUIMessages.LocalizationPage_title);

        fBlock = new LocalesBlock(this);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormPage#dispose()
     */
    @Override
    public void dispose() {

        L10nModel l10nModel = (L10nModel) getModel();
        if (l10nModel != null) {
            l10nModel.removeModelChangedListener(this);
        }
        super.dispose();
    }

    /**
     * @return
     */
    public MTJMasterDetailsBlock getBlock() {
        return fBlock;
    }

    /**
     * @return
     */
    public ISelection getSelection() {
        return fBlock.getSelection();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangedListener#modelChanged(org.eclipse.mtj.core.model.IModelChangedEvent)
     */
    public void modelChanged(IModelChangedEvent event) {
        // Inform the block
        fBlock.modelChanged(event);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormPage#setActive(boolean)
     */
    @Override
    public void setActive(boolean active) {
        super.setActive(active);
        if (active) {
            IFormPage page = getMTJEditor().findPage(
                    L10nInputContext.CONTEXT_ID);
            if ((page instanceof L10nSourcePage)
                    && ((L10nSourcePage) page).getInputContext()
                            .isInSourceMode()) {
                ISourceViewer viewer = ((L10nSourcePage) page).getViewer();
                if (viewer == null) {
                    return;
                }

                StyledText text = viewer.getTextWidget();
                if (text == null) {
                    return;
                }

                int offset = text.getCaretOffset();
                if (offset < 0) {
                    return;
                }

                IDocumentRange range = ((L10nSourcePage) page).getRangeElement(
                        offset, true);
                if (range instanceof IDocumentAttributeNode) {
                    range = ((IDocumentAttributeNode) range)
                            .getEnclosingElement();
                } else if (range instanceof IDocumentTextNode) {
                    range = ((IDocumentTextNode) range).getEnclosingElement();
                }

                if (range instanceof L10nObject) {
                    fBlock.getMasterSection().setSelection(
                            new StructuredSelection(range));
                }
            }
            
            L10nModel l10nModel = (L10nModel) getModel();
            if (l10nModel != null) {            	
            	L10nLocales locales = l10nModel.getLocales();
            	if (locales != null && locales.getDefaultLocale() == null) {
            		IMessageManager messageManager = getManagedForm().getMessageManager();
            		messageManager.addMessage(this
            				, MTJUIMessages.L10nLocaleDetails_noDefaultLocale
            					, null, IMessageProvider.WARNING);
            	}
            }
        }
    }

    /**
     * @param managedForm
     * @param model
     */
    private void createErrorContent(IManagedForm managedForm, L10nModel model) {
        createFormErrorContent(managedForm,
                MTJUIMessages.LocalizationPage_formErrorContent_title,
                MTJUIMessages.LocalizationPage_formErrorContent_message);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createFormContent(IManagedForm managedForm) {
        ScrolledForm form = managedForm.getForm();
        L10nModel model = (L10nModel) getModel();

        // Ensure the model was loaded properly
        if ((model == null) || (model.isLoaded() == false)) {
            createErrorContent(managedForm, model);
            return;
        }

        // Create the rest of the actions in the form title area
        super.createFormContent(managedForm);

        form.setText(MTJUIMessages.LocalizationPage_text);
        // Create the master details block
        fBlock.createContent(managedForm);
        // Force the selection in the masters tree section to load the
        // proper details section
        fBlock.getMasterSection().fireSelection();

        // Register this page to be informed of model change events
        model.addModelChangedListener(this);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormPage#getHelpResource()
     */
    @Override
    protected String getHelpResource() {
        return "/org.eclipse.mtj.doc.user/html/reference/editors/localization/l10nEditorPage.html";
    }
}
