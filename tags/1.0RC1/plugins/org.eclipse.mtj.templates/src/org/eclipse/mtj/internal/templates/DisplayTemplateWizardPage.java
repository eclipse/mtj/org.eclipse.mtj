/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.templates;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

/**
 * DisplayTemplateWizardPage provides a template for an application with a Display
 * Manager.
 * 
 * @author David Marques
 * @since 1.0
 */
public class DisplayTemplateWizardPage extends AbstractTemplateWizardPage {

    private static final String NEW_TEXT_BOX = "new TextBox(\"Text [Screen {0}]\", \"\", 100, TextField.ANY)"; //$NON-NLS-1$
    private static final String NEW_LIST = "new List(\"List [Screen {0}]\", List.IMPLICIT)"; //$NON-NLS-1$
    private static final String NEW_FORM = "new Form(\"Form [Screen {0}]\")"; //$NON-NLS-1$

    private Button textBtn1;
    private Button listBtn1;
    private Button formBtn1;
    private Button textBtn2;
    private Button listBtn2;
    private Button formBtn2;
    private Button listBtn3;
    private Button formBtn3;
    private Button textBtn3;
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        for (int i = 0x01; i <= 0x03; i++) {
            createScreenGroup(i, parent);
        }
    }
    
    private void createScreenGroup(int num, Composite parent) {
        GridData data = null;
        parent.setLayout(new GridLayout(0x01, false));
        
        Group group = new Group(parent, SWT.NONE);
        group.setText(NLS.bind(Messages.DisplayTemplateProvider_0, num));

        data = new GridData(SWT.FILL, SWT.FILL, true, false);
        group.setLayoutData(data);
        group.setLayout(new GridLayout(1, false));

        Button textBtn = new Button(group, SWT.RADIO);
        textBtn.setText("javax.microedition.lcdui.TextBox"); //$NON-NLS-1$
        data = new GridData(GridData.FILL_HORIZONTAL);
        textBtn.setLayoutData(data);
        textBtn.setSelection(true);

        Button listBtn = new Button(group, SWT.RADIO);
        listBtn.setText("javax.microedition.lcdui.List"); //$NON-NLS-1$
        data = new GridData(GridData.FILL_HORIZONTAL);
        listBtn.setLayoutData(data);

        Button formBtn = new Button(group, SWT.RADIO);
        formBtn.setText("javax.microedition.lcdui.Form"); //$NON-NLS-1$
        data = new GridData(GridData.FILL_HORIZONTAL);
        formBtn.setLayoutData(data);

        switch (num) {
            case 0x01:
                this.textBtn1 = textBtn;
                this.listBtn1 = listBtn;
                this.formBtn1 = formBtn;
                break;
            case 0x02:
                this.textBtn2 = textBtn;
                this.listBtn2 = listBtn;
                this.formBtn2 = formBtn;
                break;
            case 0x03:
                this.textBtn3 = textBtn;
                this.listBtn3 = listBtn;
                this.formBtn3 = formBtn;
                break;
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.ITemplateProvider#getDictionary()
     */
    public Map<String, String> getDictionary() {
        Map<String, String> dictionary = new HashMap<String, String>();

        dictionary.put("$screen1$", getScreen1()); //$NON-NLS-1$
        dictionary.put("$screen2$", getScreen2()); //$NON-NLS-1$
        dictionary.put("$screen3$", getScreen3()); //$NON-NLS-1$

        return dictionary;
    }

    private String getScreen3() {
        StringBuffer buffer = new StringBuffer();
        if (this.textBtn3.getSelection()) {
            buffer.append(NLS.bind(NEW_TEXT_BOX, 3));
        } else if (this.listBtn3.getSelection()) {
            buffer.append(NLS.bind(NEW_LIST, 3));
        } else if (this.formBtn3.getSelection()) {
            buffer.append(NLS.bind(NEW_FORM, 3));
        } else {
            buffer.append("null"); //$NON-NLS-1$
        }
        return buffer.toString();
    }

    private String getScreen2() {
        StringBuffer buffer = new StringBuffer();
        if (this.textBtn2.getSelection()) {
            buffer.append(NLS.bind(NEW_TEXT_BOX, 2));
        } else if (this.listBtn2.getSelection()) {
            buffer.append(NLS.bind(NEW_LIST, 2));
        } else if (this.formBtn2.getSelection()) {
            buffer.append(NLS.bind(NEW_FORM, 2));
        } else {
            buffer.append("null"); //$NON-NLS-1$
        }
        return buffer.toString();
    }

    private String getScreen1() {
        StringBuffer buffer = new StringBuffer();
        if (this.textBtn1.getSelection()) {
            buffer.append(NLS.bind(NEW_TEXT_BOX, 1));
        } else if (this.listBtn1.getSelection()) {
            buffer.append(NLS.bind(NEW_LIST, 1));
        } else if (this.formBtn1.getSelection()) {
            buffer.append(NLS.bind(NEW_FORM, 1));
        } else {
            buffer.append("null"); //$NON-NLS-1$
        }
        return buffer.toString();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.ITemplateProvider#isPageComplete()
     */
    public boolean isPageComplete() {
        return true;
    }
}
