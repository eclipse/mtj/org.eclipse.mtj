/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Feng Wang (Sybase) - 1. Modify to fit for AbstractMIDPDevice#copyForLaunch(...)
 *                                   signature changing.
 *                          2. Replace ILaunchConstants.EMULATED_CLASS with
 *                             IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME
 *                             , to take advantage of JDT launch configuration 
 *                             refactoring participates.
 *     Diego Sandin (Motorola)  - Added a new constructor.
 */
package org.eclipse.mtj.internal.toolkit.uei;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.mtj.core.launching.LaunchEnvironment;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.launching.ILaunchConstants;
import org.eclipse.mtj.internal.core.launching.midp.IMIDPLaunchConstants;
import org.eclipse.mtj.internal.core.sdk.device.AbstractMIDPDevice;
import org.eclipse.mtj.internal.core.sdk.device.LaunchTemplateProperties;
import org.eclipse.mtj.internal.core.util.ReplaceableParametersProcessor;
import org.eclipse.mtj.internal.core.util.Utils;

/**
 * Unified Emulator Interface (UEI) implementation of the IDevice interface.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class has been added as part of a work in
 * progress. There is no guarantee that this API will work or that it will
 * remain the same. Please do not use this API without consulting with the MTJ
 * team.
 * </p>
 * 
 * @author Craig Setera
 */
public class UEIDevice extends AbstractMIDPDevice implements IMIDPDevice {

    /**
     * Creates a new UEIDeviceInternal
     */
    public UEIDevice() {
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals = false;

        if (obj instanceof UEIDevice) {
            equals = equals((UEIDevice) obj);
        }

        return equals;
    }

    /**
     * Test equality on a UEI device and return a boolean indicating equality.
     * 
     * @param device the reference device with which to compare.
     * @return <code>true</code> if this object is the same as the device
     *         argument; <code>false</code> otherwise.
     */
    public boolean equals(UEIDevice device) {
        return super.equals(device)
                && launchCommandTemplate.equals(device.launchCommandTemplate);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDevice#getLaunchCommand(org.eclipse.mtj.core.launching.LaunchEnvironment, org.eclipse.core.runtime.IProgressMonitor)
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException {

        boolean isWin32 = Platform.getOS().equals(Platform.OS_WIN32);
        String baseCommand = Utils.EMPTY_STRING;

        if (launchEnvironment.getProject() instanceof IMidletSuiteProject) {

            IMidletSuiteProject midletSuite = (IMidletSuiteProject) launchEnvironment
                    .getProject();

            ILaunchConfiguration launchConfiguration = launchEnvironment
                    .getLaunchConfiguration();

            boolean launchFromJAD = shouldDirectLaunchJAD(launchConfiguration);

            // Copy the things we need for launch to a temporary location to
            // avoid
            // locking files
            File deployedTemp = copyForLaunch(midletSuite, monitor,
                    launchFromJAD);

            // Fill in our properties so we can use them for resolution
            // against the raw executable command line
            Map<String, String> executionProperties = new HashMap<String, String>();
            executionProperties.put(LaunchTemplateProperties.EXECUTABLE
                    .toString(), isWin32 ? executable.getName() : executable
                    .getAbsolutePath());

            executionProperties.put(LaunchTemplateProperties.DEVICE.toString(),
                    getName());

            // Debug information
            if (launchEnvironment.isDebugLaunch()) {
                executionProperties.put(LaunchTemplateProperties.DEBUGPORT
                        .toString(), Integer.toString(launchEnvironment
                        .getDebugListenerPort()));
            }

            // Classpath
            if (!launchFromJAD) {
                String classpathString = getProjectClasspathString(
                        launchEnvironment.getProject(), deployedTemp, monitor);
                executionProperties.put(LaunchTemplateProperties.CLASSPATH
                        .toString(), classpathString);
            }

            // Add launch configuration values
            addLaunchConfigurationValue(executionProperties,
                    LaunchTemplateProperties.VERBOSE.toString(),
                    launchConfiguration, ILaunchConstants.VERBOSITY_OPTIONS);

            addLaunchConfigurationValue(executionProperties,
                    LaunchTemplateProperties.HEAPSIZE.toString(),
                    launchConfiguration, IMIDPLaunchConstants.HEAP_SIZE);

            String securityDomainName = launchConfiguration.getAttribute(
                    IMIDPLaunchConstants.SECURITY_DOMAIN,
                    IMIDPLaunchConstants.NO_SECURITY_DOMAIN);
            if (!securityDomainName
                    .equals(IMIDPLaunchConstants.NO_SECURITY_DOMAIN)) {
                executionProperties.put(LaunchTemplateProperties.SECURITYDOMAIN
                        .toString(), securityDomainName);
            }

            String extraArguments = launchConfiguration.getAttribute(
                    ILaunchConstants.LAUNCH_PARAMS, Utils.EMPTY_STRING);
            executionProperties.put(
                    LaunchTemplateProperties.USERSPECIFIEDARGUMENTS.toString(),
                    extraArguments);

            if (launchFromJAD) {
                executionProperties.put(LaunchTemplateProperties.JADFILE
                        .toString(), getSpecifiedJadURL(launchConfiguration));
            } else if (shouldDoOTA(launchConfiguration)) {
                String url = getOTAURL(launchConfiguration, midletSuite);
                executionProperties.put(LaunchTemplateProperties.OTAURL
                        .toString(), url);
            } else {
                File jadFile = getJadForLaunch(midletSuite, deployedTemp,
                        monitor);
                if (jadFile.exists()) {
                    executionProperties.put(LaunchTemplateProperties.JADFILE
                            .toString(), jadFile.toString());
                }

                addLaunchConfigurationValue(executionProperties,
                        LaunchTemplateProperties.TARGET.toString(),
                        launchConfiguration,
                        IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME);
            }

            // Do the property resolution given the previous information
            baseCommand = ReplaceableParametersProcessor
                    .processReplaceableValues(launchCommandTemplate,
                            executionProperties);
        }
        return isWin32 ? "cmd /c " + baseCommand : baseCommand; //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice#getWorkingDirectory()
     */
    public File getWorkingDirectory() {
        return executable.getParentFile();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return deviceClasspath.hashCode() ^ executable.hashCode()
                ^ name.hashCode() ^ launchCommandTemplate.hashCode()
                ^ groupName.hashCode();
    }
}
