/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin  (Motorola) - Initial Version
 *     David Marques (Motorola) - Sending fully qualified names to writer.
 */
package org.eclipse.mtj.internal.jmunit.ui.wizards.testsuite;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.junit.wizards.NewTestSuiteWizardPage;
import org.eclipse.jdt.ui.JavaElementLabelProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.internal.jmunit.IJMUnitContants;
import org.eclipse.mtj.internal.jmunit.JMUnitMessages;
import org.eclipse.mtj.internal.jmunit.core.api.TestSuiteWriter;
import org.eclipse.mtj.internal.jmunit.util.JMUnitStatus;
import org.eclipse.mtj.internal.jmunit.util.LayoutUtil;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * The class NewJMUnitTestSuiteWizardPageOne contains controls and validation
 * routines for the single page in the 'New JMUnit Test Suite Wizard'.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class NewJMUnitTestSuiteWizardPageOne extends NewTestSuiteWizardPage {

    /** The wizard page's name */
    private final static String PAGE_NAME = "NewJMUnitTestSuiteWizardPageOne"; //$NON-NLS-1$

    /** Field ID of the class in suite field. */
    public final static String CLASSES_IN_SUITE = PAGE_NAME + ".classesinsuite"; //$NON-NLS-1$

    private static final String ALL_TESTS = "AllTestSuite"; //$NON-NLS-1$

    private IStatus fClassesInSuiteStatus;
    private CheckboxTableViewer fClassesInSuiteTable;
    private Label fSelectedClassesLabel;
    private boolean fUpdatedExistingClassButton;
    private SuiteClassesContentProvider provider;

    /**
     * Creates a new NewJMUnitTestSuiteWizardPageOne wizard page.
     */
    public NewJMUnitTestSuiteWizardPageOne() {
        super();
        setTitle(JMUnitMessages.NewJMUnitTestSuiteWizardPageOne_title);
        setDescription(JMUnitMessages.NewJMUnitTestSuiteWizardPageOne_description);
        fClassesInSuiteStatus = new JMUnitStatus();
        provider = new SuiteClassesContentProvider(
                IJMUnitContants.JMUNIT_TESTCASE_CLDC11);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.ui.wizards.NewTypeWizardPage#getSuperClass()
     */
    @Override
    public String getSuperClass() {
        return IJMUnitContants.JMUNIT_TESTSUITE_CLDC11;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestSuiteWizardPage#hasUpdatedExistingClass()
     */
    @Override
    public boolean hasUpdatedExistingClass() {
        return fUpdatedExistingClassButton;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestSuiteWizardPage#init(org.eclipse.jface.viewers.IStructuredSelection)
     */
    @Override
    public void init(IStructuredSelection selection) {

        super.init(selection);

        setTypeName(ALL_TESTS, true);
        fTypeNameStatus = typeNameChanged();
    }

    /**
     * @return
     */
    private IStatus classesInSuiteChanged() {
        JMUnitStatus status = new JMUnitStatus();
        if (fClassesInSuiteTable.getCheckedElements().length <= 0) {
            status
                    .setWarning(JMUnitMessages.NewJMUnitTestSuiteWizardPageOne_no_test_classes_selected);
        }
        return status;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestSuiteWizardPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createControl(Composite parent) {
        initializeDialogUnits(parent);

        Composite composite = new Composite(parent, SWT.NONE);
        int nColumns = 4;

        GridLayout layout = new GridLayout();
        layout.numColumns = nColumns;

        composite.setLayout(layout);
        createContainerControls(composite, nColumns);
        createPackageControls(composite, nColumns);
        createTypeNameControls(composite, nColumns);
        createClassesInSuiteControl(composite, nColumns);
        createMethodStubSelectionControls(composite, nColumns);
        setControl(composite);
        Dialog.applyDialogFont(composite);
    }

    /**
     * 
     */
    private void doStatusUpdate() {
        // status of all used components
        IStatus[] status = new IStatus[] { fContainerStatus, fPackageStatus,
                fTypeNameStatus, fClassesInSuiteStatus };

        // the most severe status will be displayed and the OK button
        // enabled/disabled.
        updateStatus(status);
    }

    /**
     * 
     */
    private void updateSelectedClassesLabel() {
        int noOfClassesChecked = fClassesInSuiteTable.getCheckedElements().length;
        String key = (noOfClassesChecked == 1) ? JMUnitMessages.NewJMUnitTestSuiteWizardPageOne_class_selected
                : JMUnitMessages.NewJMUnitTestSuiteWizardPageOne_classes_selected;
        fSelectedClassesLabel.setText(NLS.bind(key, new Integer(
                noOfClassesChecked)));

    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestSuiteWizardPage#createClassesInSuiteControl(org.eclipse.swt.widgets.Composite, int)
     */
    @Override
    protected void createClassesInSuiteControl(Composite parent, int nColumns) {
        if (fClassesInSuiteTable == null) {

            Label label = new Label(parent, SWT.LEFT);
            label
                    .setText(JMUnitMessages.NewJMUnitTestSuiteWizardPageOne_createClassesInSuiteControl_label);
            GridData gd = new GridData();
            gd.horizontalAlignment = GridData.FILL;
            gd.horizontalSpan = nColumns;
            label.setLayoutData(gd);

            fClassesInSuiteTable = CheckboxTableViewer.newCheckList(parent,
                    SWT.BORDER);
            gd = new GridData(GridData.FILL_BOTH);
            gd.heightHint = 80;
            gd.horizontalSpan = nColumns - 1;

            fClassesInSuiteTable.getTable().setLayoutData(gd);
            fClassesInSuiteTable.setContentProvider(provider);
            fClassesInSuiteTable
                    .setLabelProvider(new JavaElementLabelProvider());
            fClassesInSuiteTable
                    .addCheckStateListener(new ICheckStateListener() {
                        public void checkStateChanged(
                                CheckStateChangedEvent event) {
                            handleFieldChanged(CLASSES_IN_SUITE);
                        }
                    });
            fClassesInSuiteTable.setInput(getJavaProject());
            
            Composite buttonContainer = new Composite(parent, SWT.NONE);
            gd = new GridData(GridData.FILL_VERTICAL);
            buttonContainer.setLayoutData(gd);
            GridLayout buttonLayout = new GridLayout();
            buttonLayout.marginWidth = 0;
            buttonLayout.marginHeight = 0;
            buttonContainer.setLayout(buttonLayout);

            Button selectAllButton = new Button(buttonContainer, SWT.PUSH);
            selectAllButton
                    .setText(JMUnitMessages.NewJMUnitTestSuiteWizardPageOne_select_all);
            GridData bgd = new GridData(GridData.FILL_HORIZONTAL
                    | GridData.VERTICAL_ALIGN_BEGINNING);
            bgd.widthHint = LayoutUtil.getButtonWidthHint(selectAllButton);
            selectAllButton.setLayoutData(bgd);
            selectAllButton.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    fClassesInSuiteTable.setAllChecked(true);
                    handleFieldChanged(CLASSES_IN_SUITE);
                }
            });

            Button deselectAllButton = new Button(buttonContainer, SWT.PUSH);
            deselectAllButton
                    .setText(JMUnitMessages.NewJMUnitTestSuiteWizardPageOne_deselect_all);
            bgd = new GridData(GridData.FILL_HORIZONTAL
                    | GridData.VERTICAL_ALIGN_BEGINNING);
            bgd.widthHint = LayoutUtil.getButtonWidthHint(deselectAllButton);
            deselectAllButton.setLayoutData(bgd);
            deselectAllButton.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    fClassesInSuiteTable.setAllChecked(false);
                    handleFieldChanged(CLASSES_IN_SUITE);
                }
            });

            // No of selected classes label
            fSelectedClassesLabel = new Label(parent, SWT.LEFT | SWT.WRAP);
            fSelectedClassesLabel.setFont(parent.getFont());
            updateSelectedClassesLabel();
            gd = new GridData();
            gd.horizontalSpan = 2;
            fSelectedClassesLabel.setLayoutData(gd);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestSuiteWizardPage#createTypeMembers(org.eclipse.jdt.core.IType, org.eclipse.jdt.ui.wizards.NewTypeWizardPage.ImportsManager, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    protected void createTypeMembers(IType type, ImportsManager imports,
            IProgressMonitor monitor) throws CoreException {
        Object[] types = this.fClassesInSuiteTable.getCheckedElements();
        String[] names = new String[types.length];

        for (int i = 0; i < types.length; i++) {
            names[i] = ((IType) types[i]).getFullyQualifiedName();
        }

        TestSuiteWriter writer = new TestSuiteWriter(type, type
                .getElementName());
        writer.writeCode(names, monitor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestSuiteWizardPage#handleFieldChanged(java.lang.String)
     */
    @Override
    protected void handleFieldChanged(String fieldName) {
        if (fieldName.equals(PACKAGE) || fieldName.equals(CONTAINER)) {
            updateClassesInSuiteTable();
        } else if (fieldName.equals(CLASSES_IN_SUITE)) {
            fClassesInSuiteStatus = classesInSuiteChanged();
            fTypeNameStatus = typeNameChanged(); // must check this one too
            updateSelectedClassesLabel();
        }

        doStatusUpdate();

    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.junit.wizards.NewTestSuiteWizardPage#updateClassesInSuiteTable()
     */
    @Override
    protected void updateClassesInSuiteTable() {
        if (fClassesInSuiteTable != null) {
            fClassesInSuiteTable.setInput(getJavaProject());
            fClassesInSuiteTable.setAllChecked(true);
            updateSelectedClassesLabel();
        }
    }
}
