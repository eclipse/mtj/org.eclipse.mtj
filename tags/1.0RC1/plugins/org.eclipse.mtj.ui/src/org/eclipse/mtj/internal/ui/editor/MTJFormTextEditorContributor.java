/**
 * Copyright (c) 2006, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDEFormTextEditorContributor
 */
package org.eclipse.mtj.internal.ui.editor;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.ui.IContextMenuConstants;
import org.eclipse.jdt.ui.actions.IJavaEditorActionDefinitionIds;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.mtj.internal.ui.editor.actions.FormatAction;
import org.eclipse.mtj.internal.ui.editor.actions.HyperlinkAction;
import org.eclipse.mtj.internal.ui.editor.actions.MTJActionConstants;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.SubActionBars;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.editors.text.TextEditorActionContributor;
import org.eclipse.ui.forms.editor.IFormPage;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;
import org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds;
import org.eclipse.ui.texteditor.RetargetTextEditorAction;

/**
 * 
 */
public class MTJFormTextEditorContributor extends MTJFormEditorContributor {

    private RetargetTextEditorAction fCorrectionAssist;
    private HyperlinkAction fHyperlinkAction;
    private FormatAction formatAction;

    /**
     * @param formatAction the formatAction to set
     */
    public void setFormatAction(FormatAction formatAction) {
        this.formatAction = formatAction;
    }

    private RetargetTextEditorAction fContentAssist;

    private TextEditorActionContributor fSourceContributor;
    private SubActionBars fSourceActionBars;

    /**
     *
     */
    class MTJTextEditorActionContributor extends TextEditorActionContributor {
        public void contributeToMenu(IMenuManager mm) {
            super.contributeToMenu(mm);
            IMenuManager editMenu = mm
                    .findMenuUsingPath(IWorkbenchActionConstants.M_EDIT);
            if (editMenu != null) {
                editMenu.add(new Separator(IContextMenuConstants.GROUP_OPEN));
                editMenu
                        .add(new Separator(IContextMenuConstants.GROUP_GENERATE));
                editMenu.add(new Separator(
                        IContextMenuConstants.GROUP_ADDITIONS));
                if (fCorrectionAssist != null)
                    editMenu.appendToGroup(
                            IContextMenuConstants.GROUP_GENERATE,
                            fCorrectionAssist);
                if (fContentAssist != null)
                    editMenu.appendToGroup(
                            IContextMenuConstants.GROUP_GENERATE,
                            fContentAssist);
            }
        }

        /* (non-Javadoc)
         * @see org.eclipse.ui.part.EditorActionBarContributor#contributeToToolBar(org.eclipse.jface.action.IToolBarManager)
         */
        public void contributeToToolBar(IToolBarManager toolBarManager) {
            super.contributeToToolBar(toolBarManager);
            if (fHyperlinkAction != null)
                toolBarManager.add(fHyperlinkAction);
        }

        /* (non-Javadoc)
         * @see org.eclipse.ui.editors.text.TextEditorActionContributor#setActiveEditor(org.eclipse.ui.IEditorPart)
         */
        public void setActiveEditor(IEditorPart part) {
            super.setActiveEditor(part);
            IActionBars actionBars = getActionBars();
            IStatusLineManager manager = actionBars.getStatusLineManager();
            manager.setMessage(null);
            manager.setErrorMessage(null);

            ITextEditor textEditor = (part instanceof ITextEditor) ? (ITextEditor) part
                    : null;
            if (fCorrectionAssist != null)
                fCorrectionAssist.setAction(getAction(textEditor,
                        ITextEditorActionConstants.QUICK_ASSIST)); //$NON-NLS-1$
            if (fHyperlinkAction != null)
                fHyperlinkAction.setTextEditor(textEditor);
            if (formatAction != null)
                formatAction.setTextEditor(textEditor);
            if (fContentAssist != null)
                fContentAssist
                        .setAction(getAction(textEditor, "ContentAssist")); //$NON-NLS-1$
        }
    }

    /**
     * @param menuName
     */
    public MTJFormTextEditorContributor(String menuName) {
        super(menuName);
        fSourceContributor = createSourceContributor();
        if (supportsCorrectionAssist()) {
            fCorrectionAssist = new RetargetTextEditorAction(MTJSourcePage
                    .getBundleForConstructedKeys(), "CorrectionAssistProposal."); //$NON-NLS-1$
            fCorrectionAssist
                    .setActionDefinitionId(ITextEditorActionDefinitionIds.QUICK_ASSIST);
        }
        if (supportsHyperlinking()) {
            fHyperlinkAction = new HyperlinkAction();
            fHyperlinkAction
                    .setActionDefinitionId(IJavaEditorActionDefinitionIds.OPEN_EDITOR);
        }

        if ((supportsFormatAction()) && (formatAction != null)) {
            // FIXME
            // formatAction.setActionDefinitionId(MTJActionConstants.DEFN_FORMAT);
        }

        if (supportsContentAssist()) {
            fContentAssist = new RetargetTextEditorAction(MTJSourcePage
                    .getBundleForConstructedKeys(), "ContentAssistProposal."); //$NON-NLS-1$
            fContentAssist
                    .setActionDefinitionId(ITextEditorActionDefinitionIds.CONTENT_ASSIST_PROPOSALS);
        }
    }

    /**
     * @return
     */
    public boolean supportsCorrectionAssist() {
        return false;
    }

    /**
     * @return
     */
    public boolean supportsContentAssist() {
        return false;
    }

    /**
     * @return
     */
    public boolean supportsFormatAction() {
        return false;
    }

    /**
     * @return
     */
    public boolean supportsHyperlinking() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditorContributor#getSourceContributor()
     */
    public IEditorActionBarContributor getSourceContributor() {
        return fSourceContributor;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditorContributor#init(org.eclipse.ui.IActionBars)
     */
    public void init(IActionBars bars) {
        super.init(bars);
        fSourceActionBars = new SubActionBars(bars);
        fSourceContributor.init(fSourceActionBars);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorActionBarContributor#dispose()
     */
    public void dispose() {
        fSourceActionBars.dispose();
        fSourceContributor.dispose();
        super.dispose();
    }

    /**
     * @param active
     */
    protected void setSourceActionBarsActive(boolean active) {
        IActionBars rootBars = getActionBars();
        rootBars.clearGlobalActionHandlers();
        rootBars.updateActionBars();
        if (active) {
            fSourceActionBars.activate();
            Map<?, ?> handlers = fSourceActionBars.getGlobalActionHandlers();
            if (handlers != null) {
                Set<?> keys = handlers.keySet();
                for (Iterator<?> iter = keys.iterator(); iter.hasNext();) {
                    String id = (String) iter.next();
                    rootBars.setGlobalActionHandler(id, (IAction) handlers
                            .get(id));
                }
            }
        } else {
            fSourceActionBars.deactivate();
            registerGlobalActionHandlers();
        }
        rootBars.setGlobalActionHandler(MTJActionConstants.OPEN,
                active ? fHyperlinkAction : null);
        rootBars.setGlobalActionHandler(MTJActionConstants.FORMAT,
                active ? formatAction : null);
        // Register the revert action
        rootBars.setGlobalActionHandler(ActionFactory.REVERT.getId(),
                getRevertAction());

        rootBars.updateActionBars();
    }

    /**
     * 
     */
    private void registerGlobalActionHandlers() {
        registerGlobalAction(ActionFactory.DELETE.getId());
        registerGlobalAction(ActionFactory.UNDO.getId());
        registerGlobalAction(ActionFactory.REDO.getId());
        registerGlobalAction(ActionFactory.CUT.getId());
        registerGlobalAction(ActionFactory.COPY.getId());
        registerGlobalAction(ActionFactory.PASTE.getId());
        registerGlobalAction(ActionFactory.SELECT_ALL.getId());
        registerGlobalAction(ActionFactory.FIND.getId());
    }

    /**
     * @param id
     */
    private void registerGlobalAction(String id) {
        IAction action = getGlobalAction(id);
        getActionBars().setGlobalActionHandler(id, action);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditorContributor#setActivePage(org.eclipse.ui.IEditorPart)
     */
    public void setActivePage(IEditorPart newEditor) {
        if (mtjFormEditor == null)
            return;

        IFormPage oldPage = formPage;
        formPage = mtjFormEditor.getActivePageInstance();
        if (formPage == null)
            return;
        // Update the quick outline action to the navigate menu
        updateQuickOutlineMenuEntry();

        updateActions();
        if (oldPage != null && !oldPage.isEditor() && !formPage.isEditor()) {
            getActionBars().updateActionBars();
            return;
        }

        boolean isSourcePage = formPage instanceof MTJSourcePage;
        if (isSourcePage && formPage.equals(oldPage))
            return;
        fSourceContributor.setActiveEditor(formPage);
        setSourceActionBarsActive(isSourcePage);
    }

    /**
     * 
     */
    private void updateQuickOutlineMenuEntry() {
        // Get the main action bar
        IActionBars actionBars = getActionBars();
        IMenuManager menuManager = actionBars.getMenuManager();
        // Get the navigate menu
        IMenuManager navigateMenu = menuManager
                .findMenuUsingPath(IWorkbenchActionConstants.M_NAVIGATE);
        // Ensure there is a navigate menu
        if (navigateMenu == null) {
            return;
        }
        // Remove the previous version of the quick outline menu entry - if
        // one exists
        // Prevent duplicate menu entries
        // Prevent wrong quick outline menu from being brought up for the wrong
        // page
        navigateMenu.remove(MTJActionConstants.COMMAND_ID_QUICK_OUTLINE);

        // Ensure the active page is a source page
        // Only add the quick outline menu to the source pages
        if ((formPage instanceof MTJProjectionSourcePage) == false) {
            return;
        }

        MTJProjectionSourcePage page = (MTJProjectionSourcePage) formPage;
        // Only add the action if the source page supports it
        if (page.isQuickOutlineEnabled() == false) {
            return;
        }
        // Get the appropriate quick outline action associated with the active
        // source page
        IAction quickOutlineAction = page
                .getAction(MTJActionConstants.COMMAND_ID_QUICK_OUTLINE);
        // Ensure it is defined
        if (quickOutlineAction == null) {
            return;
        }
        // Add the quick outline action after the "Show In" menu contributed
        // by JDT
        // This could break if JDT changes the "Show In" menu ID
        try {
            navigateMenu.insertAfter("showIn", quickOutlineAction); //$NON-NLS-1$
        } catch (IllegalArgumentException e) {
            // Ignore
        }
    }

    /**
     * @return
     */
    protected TextEditorActionContributor createSourceContributor() {
        return new MTJTextEditorActionContributor();
    }

    /**
     * @return
     */
    protected HyperlinkAction getHyperlinkAction() {
        return fHyperlinkAction;
    }

    /**
     * @return
     */
    protected FormatAction getFormatAction() {
        return formatAction;
    }

}
