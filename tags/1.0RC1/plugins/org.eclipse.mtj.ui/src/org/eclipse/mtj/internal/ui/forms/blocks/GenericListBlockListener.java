/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.ui.forms.blocks;

import java.util.List;

/**
 * GenericListBlockListener interface must be implemented by
 * classes that wish to be notified upon changes on the state
 * of a GenericListBlock instance.
 * 
 * @author David Marques
 *
 * @param <T> The type of items for the GenericListBlock.
 * @since 1.0
 */
public interface GenericListBlockListener<T extends GenericListBlockItem> {

	/**
	 * Called upon add button selection.
	 */
	public void addButtonPressed();
	
	/**
	 * Called upon up button selection.
	 */
	public void upButtonPressed();
	
	/**
	 * Called upon down button selection.
	 */
	public void downButtonPressed();
	
	/**
	 * Called upon the removal of items.
	 * 
	 * @param _items list of items removed.
	 */
	public void itemsRemoved(List<T> _items);
	
	/**
	 * Called upon scan button selection.
	 */
	public void scan();
	
}
