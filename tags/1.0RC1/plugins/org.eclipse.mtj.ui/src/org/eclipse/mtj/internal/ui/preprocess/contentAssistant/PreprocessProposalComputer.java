/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.ui.text.java.ContentAssistInvocationContext;
import org.eclipse.jdt.ui.text.java.IJavaCompletionProposalComputer;
import org.eclipse.jdt.ui.text.java.JavaContentAssistInvocationContext;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.mtj.internal.ui.preprocess.contentAssistant.template.PreprocessTemplateAccess;
import org.eclipse.mtj.internal.ui.preprocess.contentAssistant.template.PreprocessTemplateContextType;
import org.eclipse.mtj.internal.ui.preprocess.contentAssistant.template.PreprocessTemplateEngine;

/**
 * Computes preprocess completion proposals
 * 
 * @author gma
 * @since 0.9.1
 */
public class PreprocessProposalComputer implements
        IJavaCompletionProposalComputer {

    private PreprocessTemplateEngine tmplateEngine;
    private PreprocessCompletionEngine pPEngine;

    public PreprocessProposalComputer() {

        pPEngine = new PreprocessCompletionEngine();
        PreprocessTemplateContextType contextType = (PreprocessTemplateContextType) PreprocessTemplateAccess
                .getDefault().getTemplateContextRegistry().getContextType(
                        PreprocessTemplateContextType.PREPROCESS_CONTEXTTYPE);
        tmplateEngine = new PreprocessTemplateEngine(contextType);

    }

    public List<ICompletionProposal> computeCompletionProposals(
            ContentAssistInvocationContext context, IProgressMonitor monitor) {

        JavaContentAssistInvocationContext javaContext = (JavaContentAssistInvocationContext) context;
        IProject project = javaContext.getCompilationUnit().getJavaProject()
                .getProject();

        int offset = context.getInvocationOffset();
        IDocument doc = context.getDocument();
        PreprocessContext ppContext = new PreprocessContext(doc, offset,
                project);

        ArrayList<ICompletionProposal> resultProposals = new ArrayList<ICompletionProposal>();

        for (ProposalType propsalType : ppContext.getPossibleProposalTypes()) {
            switch (propsalType) {
            case DIRECTIVE:
                resultProposals.addAll(pPEngine.completeDirective(ppContext));
                break;
            case SYMBOL:
                resultProposals.addAll(pPEngine.completeSymbol(ppContext));
                break;
            case DEBUG_LEVER:
                resultProposals.addAll(pPEngine.completeDebugLevel(ppContext));
                break;
            case TEMPLATE:
                resultProposals.addAll(tmplateEngine.complete(ppContext));
                break;
            }
        }

        return resultProposals;

    }

    public List<?> computeContextInformation(
            ContentAssistInvocationContext context, IProgressMonitor monitor) {
       
        return null;
    }

    public String getErrorMessage() {
       
        return null;
    }

    public void sessionEnded() {
       

    }

    public void sessionStarted() {
        

    }

}
