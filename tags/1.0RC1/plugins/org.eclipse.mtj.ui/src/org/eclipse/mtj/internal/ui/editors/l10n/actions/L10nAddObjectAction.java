/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.mtj.internal.core.text.l10n.L10nObject;

/**
 * L10nAddObjectAction - the abstract implementation for adding objects to a
 * L10n object.
 * 
 * @since 0.9.1
 */
public abstract class L10nAddObjectAction extends Action {

    // The parent L10n object, which the new object will be
    // a child of.
    L10nObject parentObject;

    // The target object to insert after
    L10nObject targetObject;

    /**
     * @return The names of the children of this L10n object
     */
    public String[] getChildNames() {

        int numChildren = parentObject.getChildren().size();

        L10nObject[] l10nObjects = parentObject.getChildren().toArray(
                new L10nObject[numChildren]);

        String[] l10nObjectNames = new String[l10nObjects.length];

        for (int i = 0; i < numChildren; ++i) {
            l10nObjectNames[i] = l10nObjects[i].getName();
        }

        return l10nObjectNames;
    }

    /**
     * Set the parent object that this action will add objects to.
     * 
     * @param parent The new parent object for this action
     */
    public void setParentObject(L10nObject parent) {
        parentObject = parent;
    }

    /**
     * Set the target object that this action will add objects after.
     * 
     * @param target The new target object for this action
     */
    public void setTargetObject(L10nObject target) {
        targetObject = target;
    }

    /**
     * Add the child to the parent object. If a target object is specified, add
     * the child as a direct sibling after that object.
     * 
     * @param child The object to add to the parent
     */
    protected void addChild(L10nObject child) {
        if (targetObject == null) {
            ((L10nObject) parentObject).addChild(child);
        } else {
            ((L10nObject) parentObject).addChild(child, targetObject, false);
            targetObject = null;

        }
    }
}
