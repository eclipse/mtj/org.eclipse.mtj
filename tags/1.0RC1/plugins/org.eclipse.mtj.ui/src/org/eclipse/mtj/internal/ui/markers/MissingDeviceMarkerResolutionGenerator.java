/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.ui.markers;

import org.eclipse.core.resources.IMarker;
import org.eclipse.ui.IMarkerResolution;
import org.eclipse.ui.IMarkerResolutionGenerator;

/**
 * Create resolution for the "org.eclipse.mtj.core.device.missing" marker.
 * 
 * @author Diego Madruga Sandin
 */
public class MissingDeviceMarkerResolutionGenerator implements
        IMarkerResolutionGenerator {

    /**
     * Creates a new MissingDeviceMarkerResolutionGenerator.
     */
    public MissingDeviceMarkerResolutionGenerator() {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IMarkerResolutionGenerator#getResolutions(org.eclipse.core.resources.IMarker)
     */
    public IMarkerResolution[] getResolutions(IMarker marker) {
        return new IMarkerResolution[] { new MissingDeviceMarkerResolution() };
    }

}
