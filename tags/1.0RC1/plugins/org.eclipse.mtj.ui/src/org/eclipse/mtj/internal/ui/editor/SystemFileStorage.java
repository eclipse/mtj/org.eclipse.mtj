/**
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.eclipse.core.resources.IStorage;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;

/**
 * @since 0.9.1
 */
public class SystemFileStorage extends PlatformObject implements IStorage {
    private File file;

    /**
     * Constructor a new SystemFileStorage.
     */
    public SystemFileStorage(File file) {
        this.file = file;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object) {
        return (object instanceof SystemFileStorage)
                && getFile().equals(((SystemFileStorage) object).getFile());
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IStorage#getContents()
     */
    public InputStream getContents() throws CoreException {
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            IStatus status = new Status(IStatus.ERROR, MTJUIPlugin
                    .getPluginId(), IStatus.OK, null, e);
            throw new CoreException(status);
        }
    }

    /**
     * @return
     */
    public File getFile() {
        return file;
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IStorage#getFullPath()
     */
    public IPath getFullPath() {
        return new Path(file.getAbsolutePath());
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IStorage#getName()
     */
    public String getName() {
        return file.getName();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return getFile().hashCode();
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IStorage#isReadOnly()
     */
    public boolean isReadOnly() {
        return true;
    }
}
