/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant.template;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension4;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateBuffer;
import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.jface.text.templates.TemplateException;
import org.eclipse.jface.text.templates.TemplateProposal;
import org.eclipse.swt.graphics.Image;

/**
 * @author gma
 * @since 0.9.1
 */
public class PreprocessTemplateProposal extends TemplateProposal implements
        ICompletionProposalExtension4 {

    public PreprocessTemplateProposal(Template template,
            TemplateContext context, IRegion region, Image image, int relevance) {
        super(template, context, region, image, relevance);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.text.templates.TemplateProposal#getAdditionalProposalInfo()
     */
    @Override
    public String getAdditionalProposalInfo() {
        try {
            PreprocessTemplateContext context = (PreprocessTemplateContext) getContext();
            context.setReadOnly(true);
            TemplateBuffer templateBuffer;
            try {
                templateBuffer = context
                        .evaluateWithoutFormatter(getTemplate());
            } catch (TemplateException e) {
                return null;
            }

            return templateBuffer.getString();

        } catch (BadLocationException e) {
            return null;
        }
    }

    public boolean isAutoInsertable() {
        return getTemplate().isAutoInsertable();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.text.templates.TemplateProposal#validate(org.eclipse.jface.text.IDocument,
     *      int, org.eclipse.jface.text.DocumentEvent)
     */
    @Override
    public boolean validate(IDocument document, int offset, DocumentEvent event) {
        boolean valid = false;
        try {
            int ppDirectiveStartPos = ((PreprocessTemplateContext) getContext())
                    .getPpDirectiveBeginOffset();
            String directivePrefix = document.get(ppDirectiveStartPos, offset
                    - ppDirectiveStartPos);
            valid = getTemplate().getName().toLowerCase().startsWith(
                    directivePrefix.toLowerCase());
        } catch (BadLocationException e) {

            e.printStackTrace();
        }
        return valid;

    }
}
