/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.ui.actions;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IconAndMessageDialog;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

/**
 * A simple error dialog that display the fact that there is an error with the
 * current configuration. In addition, the dialog provides the user with a
 * button to launch into the preferences to set the configuration.
 * 
 * @author Craig Setera
 */
public class ConfigurationErrorDialog extends IconAndMessageDialog {

    private String preferenceNodeId;
    private String title;
    private String buttonText;

    /**
     * Creates a dialog instance informing that some configuration must be done
     * and displays a button to launch into the preferences to set the
     * configuration.
     * 
     * @param parentShell the parent shell, or <code>null</code> to create a
     *                top-level shell
     * @param preferenceNodeId the identifier for the page
     * @param title the dialog title
     * @param message the dialog message
     * @param buttonText the configuration button label
     */
    public ConfigurationErrorDialog(Shell parentShell, String preferenceNodeId,
            String title, String message, String buttonText) {
        super(parentShell);
        this.title = title;
        this.message = message;
        this.preferenceNodeId = preferenceNodeId;
        this.buttonText = buttonText;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
     */
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);
        newShell.setText(title);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
     */
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
                true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    protected Control createDialogArea(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(2, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        createMessageArea(composite);

        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);

        Button configureProguardButton = new Button(composite, SWT.PUSH);
        configureProguardButton.setText(buttonText);
        configureProguardButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                Shell shell = e.widget.getDisplay().getActiveShell();
                PreferenceManager manager = PlatformUI.getWorkbench()
                        .getPreferenceManager();
                PreferenceDialog dialog = new PreferenceDialog(shell, manager);
                dialog.setSelectedNode(preferenceNodeId);
                dialog.open();
            }
        });

        return composite;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.IconAndMessageDialog#getImage()
     */
    protected Image getImage() {
        return getErrorImage();
    }

}
