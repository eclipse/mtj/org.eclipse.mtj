/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDEActionConstants
 */
package org.eclipse.mtj.internal.ui.editor.actions;

/**
 * @since 0.91
 */
public class MTJActionConstants {

    // action IDs
    public static final String OPEN = "org.eclipse.mtj.ui.actions.Open"; //$NON-NLS-1$
    public static final String FORMAT = "org.eclipse.mtj.ui.actions.Format"; //$NON-NLS-1$

    // command IDs   
    public static final String COMMAND_ID_QUICK_OUTLINE = "org.eclipse.mtj.ui.quickOutline"; //$NON-NLS-1$

}
