/**
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Gang Ma (Sybase)        - Copy from org.eclipse.jdt.internal.ui.text.SimpleJavaSourceViewerConfiguration,
 *                               which located in org.eclipse.jdt.ui plug-in.
 *     Diego Sandin (Motorola) - Used the Decorator Pattern to avoid extending 
 *                               JavaSourceViewerConfiguration                  
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant.template;

import java.util.Arrays;

import org.eclipse.jdt.ui.text.IColorManager;
import org.eclipse.jdt.ui.text.JavaSourceViewerConfiguration;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.IUndoManager;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.formatter.IContentFormatter;
import org.eclipse.jface.text.hyperlink.IHyperlinkDetector;
import org.eclipse.jface.text.hyperlink.IHyperlinkPresenter;
import org.eclipse.jface.text.information.IInformationPresenter;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.quickassist.IQuickAssistAssistant;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.source.IAnnotationHover;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * A simple {@linkplain org.eclipse.jdt.ui.text.JavaSourceViewerConfiguration
 * Java source viewer configuration}.
 * <p>
 * This simple source viewer configuration basically provides syntax coloring
 * and disables all other features like code assist, quick outlines,
 * hyperlinking, etc.
 * </p>
 */
public class SimpleJavaSourceViewerConfiguration extends
        SourceViewerConfiguration {

    private boolean fConfigureFormatter;

    private JavaSourceViewerConfiguration javaSourceViewerConfiguration = null;

    /**
     * Creates a new Java source viewer configuration for viewers in the given
     * editor using the given preference store, the color manager and the
     * specified document partitioning.
     * 
     * @param colorManager the color manager
     * @param preferenceStore the preference store, can be read-only
     * @param editor the editor in which the configured viewer(s) will reside,
     *            or <code>null</code> if none
     * @param partitioning the document partitioning for this configuration, or
     *            <code>null</code> for the default partitioning
     * @param configureFormatter <code>true</code> if a content formatter should
     *            be configured
     */
    public SimpleJavaSourceViewerConfiguration(IColorManager colorManager,
            IPreferenceStore preferenceStore, ITextEditor editor,
            String partitioning, boolean configureFormatter) {

        javaSourceViewerConfiguration = new JavaSourceViewerConfiguration(
                colorManager, preferenceStore, editor, partitioning);

        fConfigureFormatter = configureFormatter;
    }

    /*
     * @see SourceViewerConfiguration#getAnnotationHover(ISourceViewer)
     */
    @Override
    public IAnnotationHover getAnnotationHover(ISourceViewer sourceViewer) {
        return null;
    }

    /*
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getAutoEditStrategies(org.eclipse.jface.text.source.ISourceViewer,
     *      java.lang.String)
     */
    @Override
    public IAutoEditStrategy[] getAutoEditStrategies(
            ISourceViewer sourceViewer, String contentType) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getConfiguredContentTypes(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
        return javaSourceViewerConfiguration
                .getConfiguredContentTypes(sourceViewer);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getConfiguredDocumentPartitioning(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public String getConfiguredDocumentPartitioning(ISourceViewer sourceViewer) {
        return javaSourceViewerConfiguration
                .getConfiguredDocumentPartitioning(sourceViewer);
    }

    /*
     * @see SourceViewerConfiguration#getConfiguredTextHoverStateMasks(ISourceViewer,
     *      String)
     */
    @Override
    public int[] getConfiguredTextHoverStateMasks(ISourceViewer sourceViewer,
            String contentType) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getContentAssistant(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IContentAssistant getContentAssistant(ISourceViewer sourceViewer) {
        return javaSourceViewerConfiguration.getContentAssistant(sourceViewer);
    }

    /*
     * @see SourceViewerConfiguration#getContentFormatter(ISourceViewer)
     */
    @Override
    public IContentFormatter getContentFormatter(ISourceViewer sourceViewer) {
        if (fConfigureFormatter) {
            return super.getContentFormatter(sourceViewer);
        } else {
            return null;
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getDefaultPrefixes(org.eclipse.jface.text.source.ISourceViewer, java.lang.String)
     */
    @Override
    public String[] getDefaultPrefixes(ISourceViewer sourceViewer,
            String contentType) {
        return javaSourceViewerConfiguration.getDefaultPrefixes(sourceViewer,
                contentType);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getDoubleClickStrategy(org.eclipse.jface.text.source.ISourceViewer, java.lang.String)
     */
    @Override
    public ITextDoubleClickStrategy getDoubleClickStrategy(
            ISourceViewer sourceViewer, String contentType) {
        return javaSourceViewerConfiguration.getDoubleClickStrategy(
                sourceViewer, contentType);
    }

    /*
     * @see org.eclipse.jdt.ui.text.JavaSourceViewerConfiguration#getHierarchyPresenter(org.eclipse.jface.text.source.ISourceViewer,
     *      boolean)
     */
    public IInformationPresenter getHierarchyPresenter(
            ISourceViewer sourceViewer, boolean doCodeResolve) {
        return null;
    }

    /*
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getHyperlinkDetectors(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IHyperlinkDetector[] getHyperlinkDetectors(ISourceViewer sourceViewer) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getHyperlinkPresenter(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IHyperlinkPresenter getHyperlinkPresenter(ISourceViewer sourceViewer) {
        return javaSourceViewerConfiguration
                .getHyperlinkPresenter(sourceViewer);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getHyperlinkStateMask(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public int getHyperlinkStateMask(ISourceViewer sourceViewer) {
        return javaSourceViewerConfiguration
                .getHyperlinkStateMask(sourceViewer);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getIndentPrefixes(org.eclipse.jface.text.source.ISourceViewer, java.lang.String)
     */
    @Override
    public String[] getIndentPrefixes(ISourceViewer sourceViewer,
            String contentType) {
        return javaSourceViewerConfiguration.getIndentPrefixes(sourceViewer,
                contentType);
    }

    /*
     * @see SourceViewerConfiguration#getInformationControlCreator(ISourceViewer)
     */
    @Override
    public IInformationControlCreator getInformationControlCreator(
            ISourceViewer sourceViewer) {
        return null;
    }

    /*
     * @see SourceViewerConfiguration#getInformationPresenter(ISourceViewer)
     */
    @Override
    public IInformationPresenter getInformationPresenter(
            ISourceViewer sourceViewer) {
        return null;
    }

    /*
     * @see org.eclipse.jdt.ui.text.JavaSourceViewerConfiguration#getOutlinePresenter(org.eclipse.jface.text.source.ISourceViewer,
     *      boolean)
     */
    public IInformationPresenter getOutlinePresenter(
            ISourceViewer sourceViewer, boolean doCodeResolve) {
        return null;
    }

    /*
     * @see SourceViewerConfiguration#getOverviewRulerAnnotationHover(ISourceViewer)
     */
    @Override
    public IAnnotationHover getOverviewRulerAnnotationHover(
            ISourceViewer sourceViewer) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getPresentationReconciler(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IPresentationReconciler getPresentationReconciler(
            ISourceViewer sourceViewer) {
        return javaSourceViewerConfiguration
                .getPresentationReconciler(sourceViewer);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getQuickAssistAssistant(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IQuickAssistAssistant getQuickAssistAssistant(
            ISourceViewer sourceViewer) {
        return javaSourceViewerConfiguration
                .getQuickAssistAssistant(sourceViewer);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getReconciler(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IReconciler getReconciler(ISourceViewer sourceViewer) {
        return javaSourceViewerConfiguration.getReconciler(sourceViewer);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getTabWidth(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public int getTabWidth(ISourceViewer sourceViewer) {
        return javaSourceViewerConfiguration.getTabWidth(sourceViewer);
    }

    /*
     * @see SourceViewerConfiguration#getTextHover(ISourceViewer, String)
     */
    @Override
    public ITextHover getTextHover(ISourceViewer sourceViewer,
            String contentType) {
        return null;
    }

    /*
     * @see SourceViewerConfiguration#getTextHover(ISourceViewer, String, int)
     */
    @Override
    public ITextHover getTextHover(ISourceViewer sourceViewer,
            String contentType, int stateMask) {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getUndoManager(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IUndoManager getUndoManager(ISourceViewer sourceViewer) {
        return javaSourceViewerConfiguration.getUndoManager(sourceViewer);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getIndentPrefixesForTab(int)
     */
    @Override
    protected String[] getIndentPrefixesForTab(int tabWidth) {
        String[] indentPrefixes = new String[tabWidth + 2];
        for (int i = 0; i <= tabWidth; i++) {
            char[] spaceChars = new char[i];
            Arrays.fill(spaceChars, ' ');
            String spaces = new String(spaceChars);
            if (i < tabWidth) {
                indentPrefixes[i] = spaces + '\t';
            } else {
                indentPrefixes[i] = new String(spaces);
            }
        }
        indentPrefixes[tabWidth + 1] = ""; //$NON-NLS-1$
        return indentPrefixes;
    }
}
