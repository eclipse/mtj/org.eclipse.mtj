/**
 * Copyright (c) 2005, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.core.resources.IFile;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPersistableElement;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.widgets.ScrolledForm;

/**
 * Page used when the resource is missing and couldn't be opened.
 * 
 * @since 0.9.1
 */
public class MissingResourcePage extends MTJFormPage {

    /**
     * Creates a new page for missing resources info.
     * 
     * @param editor
     */
    public MissingResourcePage(FormEditor editor) {
        super(editor, MTJUIMessages.MissingResourcePage_title, MTJUIMessages.MissingResourcePage_message); 
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createFormContent(IManagedForm managedForm) {
        ScrolledForm form = managedForm.getForm();
        Composite comp = managedForm.getToolkit().createComposite(form);
        comp.setLayout(new GridLayout());
        IPersistableElement persistable = getEditorInput().getPersistable();
        String text;
        if (persistable instanceof IFileEditorInput) {
            IFile file = ((IFileEditorInput) persistable).getFile();
            text = NLS.bind(
                    MTJUIMessages.MissingResourcePage_resource_unavailable,
                    new String[] { MTJUIMessages.MissingResourcePage_editor_failed,
                            file.getProjectRelativePath().toString(),
                            file.getProject().getName() });
        } else {
            text = MTJUIMessages.MissingResourcePage_editor_failed;
        }
        form.setText(text);
        comp.setLayoutData(new GridData(GridData.FILL_BOTH));
    }
}
