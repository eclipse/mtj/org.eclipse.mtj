/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     David Marques (Motorola) - Saving only keys with non empty values
 */
package org.eclipse.mtj.internal.ui.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;

import org.eclipse.jface.preference.PreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.mtj.ui.editors.jad.IManifestPreferenceStore;

/**
 * An IPreferenceStore implementation that reads and writes the information as a
 * Manifest format file.
 * 
 * @author Craig Setera
 */
public class ManifestPreferenceStore implements  IManifestPreferenceStore {
    // We need to track our own dirty flag instead of using
    // the one in the wrapped preference store.
    private boolean dirty;

    // The name of the file to be used for storage
    private String filename;

    // Wrap a JFace preference store to do the hard work and delegate most
    // functions
    private PreferenceStore wrappedPrefStore;

    /**
     * Creates an empty preference store that loads from and saves to the a
     * file.
     * <p>
     * Use the methods <code>load()</code> and <code>save()</code> to load
     * and store this preference store.
     * </p>
     * 
     * @param filename the file name
     * @see #load()
     * @see #save()
     */
    public ManifestPreferenceStore() {
        this(null);
    }

    /**
     * Creates an empty preference store that loads from and saves to the a
     * file.
     * <p>
     * Use the methods <code>load()</code> and <code>save()</code> to load
     * and store this preference store.
     * </p>
     * 
     * @param filename the file name
     * @see #load()
     * @see #save()
     */
    public ManifestPreferenceStore(String filename) {
        this.filename = filename;
        wrappedPrefStore = new PreferenceStore();
        dirty = false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#addPropertyChangeListener(org.eclipse.jface.util.IPropertyChangeListener)
     */
    public void addPropertyChangeListener(IPropertyChangeListener listener) {
        wrappedPrefStore.addPropertyChangeListener(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#contains(java.lang.String)
     */
    public boolean contains(String name) {
        return wrappedPrefStore.contains(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#firePropertyChangeEvent(java.lang.String, java.lang.Object, java.lang.Object)
     */
    public void firePropertyChangeEvent(String name, Object oldValue,
            Object newValue) {
        wrappedPrefStore.firePropertyChangeEvent(name, oldValue, newValue);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getBoolean(java.lang.String)
     */
    public boolean getBoolean(String name) {
        return wrappedPrefStore.getBoolean(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getDefaultBoolean(java.lang.String)
     */
    public boolean getDefaultBoolean(String name) {
        return wrappedPrefStore.getDefaultBoolean(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getDefaultDouble(java.lang.String)
     */
    public double getDefaultDouble(String name) {
        return wrappedPrefStore.getDefaultDouble(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getDefaultFloat(java.lang.String)
     */
    public float getDefaultFloat(String name) {
        return wrappedPrefStore.getDefaultFloat(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getDefaultInt(java.lang.String)
     */
    public int getDefaultInt(String name) {
        return wrappedPrefStore.getDefaultInt(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getDefaultLong(java.lang.String)
     */
    public long getDefaultLong(String name) {
        return wrappedPrefStore.getDefaultLong(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getDefaultString(java.lang.String)
     */
    public String getDefaultString(String name) {
        return wrappedPrefStore.getDefaultString(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getDouble(java.lang.String)
     */
    public double getDouble(String name) {
        return wrappedPrefStore.getDouble(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getFloat(java.lang.String)
     */
    public float getFloat(String name) {
        return wrappedPrefStore.getFloat(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getInt(java.lang.String)
     */
    public int getInt(String name) {
        return wrappedPrefStore.getInt(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getLong(java.lang.String)
     */
    public long getLong(String name) {
        return wrappedPrefStore.getLong(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#getString(java.lang.String)
     */
    public String getString(String name) {
        return wrappedPrefStore.getString(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#isDefault(java.lang.String)
     */
    public boolean isDefault(String name) {
        return wrappedPrefStore.isDefault(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#load()
     */
    public void load() throws IOException {
        if (filename == null)
            throw new IOException("File name not specified");//$NON-NLS-1$

        FileInputStream in = new FileInputStream(filename);
        load(in);
        in.close();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#load(java.io.InputStream)
     */
    public void load(InputStream in) throws IOException {
        Reader dataReader = new InputStreamReader(in, "UTF-8");
        BufferedReader bufferedReader = new BufferedReader(dataReader);

        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            // Find the first colon and break the string up
            int colonIndex = line.indexOf(':');
            if (colonIndex != -1) {
                String name = line.substring(0, colonIndex);
                String value = line.substring(colonIndex + 2);
                setValue(name, value);
            }
        }

        dirty = false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#needsSaving()
     */
    public boolean needsSaving() {
        return dirty;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#preferenceNames()
     */
    public String[] preferenceNames() {
        return wrappedPrefStore.preferenceNames();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#putValue(java.lang.String, java.lang.String)
     */
    public void putValue(String name, String value) {
        String oldValue = getString(name);
        if (oldValue == null || !oldValue.equals(value)) {
            wrappedPrefStore.putValue(name, value);
            dirty = true;
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#removePropertyChangeListener(org.eclipse.jface.util.IPropertyChangeListener)
     */
    public void removePropertyChangeListener(IPropertyChangeListener listener) {
        wrappedPrefStore.removePropertyChangeListener(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#save()
     */
    public void save() throws IOException {
        if (filename == null)
            throw new IOException("File name not specified");//$NON-NLS-1$

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filename);
            save(out);
        } finally {
            if (out != null)
                out.close();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#save(java.io.OutputStream)
     */
    public void save(OutputStream out) throws IOException {
        OutputStreamWriter osw = new OutputStreamWriter(out, "UTF-8");
        PrintWriter writer = new PrintWriter(osw);

        String[] prefNames = preferenceNames();
        for (int i = 0; i < prefNames.length; i++) {
            String name = prefNames[i];
            String value = getString(name);
            // Only write keys with some value
            if (value.trim().length() > 0x00) {				
            	writer.println(name + ": " + value);
			}
        }

        writer.flush();
        dirty = false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setDefault(java.lang.String, double)
     */
    public void setDefault(String name, double value) {
        wrappedPrefStore.setDefault(name, value);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setDefault(java.lang.String, float)
     */
    public void setDefault(String name, float value) {
        wrappedPrefStore.setDefault(name, value);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setDefault(java.lang.String, int)
     */
    public void setDefault(String name, int value) {
        wrappedPrefStore.setDefault(name, value);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setDefault(java.lang.String, long)
     */
    public void setDefault(String name, long value) {
        wrappedPrefStore.setDefault(name, value);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setDefault(java.lang.String, java.lang.String)
     */
    public void setDefault(String name, String defaultObject) {
        wrappedPrefStore.setDefault(name, defaultObject);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setDefault(java.lang.String, boolean)
     */
    public void setDefault(String name, boolean value) {
        wrappedPrefStore.setDefault(name, value);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setToDefault(java.lang.String)
     */
    public void setToDefault(String name) {
        dirty = true;
        wrappedPrefStore.setToDefault(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setValue(java.lang.String, double)
     */
    public void setValue(String name, double value) {
        double oldValue = getDouble(name);
        if (value != oldValue) {
            dirty = true;
            wrappedPrefStore.setValue(name, value);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setValue(java.lang.String, float)
     */
    public void setValue(String name, float value) {
        float oldValue = getFloat(name);
        if (value != oldValue) {
            dirty = true;
            wrappedPrefStore.setValue(name, value);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setValue(java.lang.String, int)
     */
    public void setValue(String name, int value) {
        int oldValue = getInt(name);
        if (value != oldValue) {
            dirty = true;
            wrappedPrefStore.setValue(name, value);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setValue(java.lang.String, long)
     */
    public void setValue(String name, long value) {
        long oldValue = getLong(name);
        if (value != oldValue) {
            dirty = true;
            wrappedPrefStore.setValue(name, value);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setValue(java.lang.String, java.lang.String)
     */
    public void setValue(String name, String value) {
        String oldValue = getString(name);
        if (value != oldValue) {
            dirty = true;
            wrappedPrefStore.setValue(name, value);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.IManifestPreferenceStore#setValue(java.lang.String, boolean)
     */
    public void setValue(String name, boolean value) {
        boolean oldValue = getBoolean(name);
        if (value != oldValue) {
            dirty = true;
            wrappedPrefStore.setValue(name, value);
        }
    }
}
