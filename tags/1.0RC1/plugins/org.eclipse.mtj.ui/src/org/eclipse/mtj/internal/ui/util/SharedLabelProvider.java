/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class SharedLabelProvider extends LabelProvider implements
        ITableLabelProvider {

    public static final int F_BINARY = 16;
    public static final int F_EDIT = 8;
    public static final int F_ERROR = 1;
    public static final int F_EXPORT = 4;
    public static final int F_EXTERNAL = 32;
    public static final int F_FRIEND = 2048;
    public static final int F_INTERNAL = 1024;
    public static final int F_JAR = 128;
    public static final int F_JAVA = 64;
    public static final int F_OPTIONAL = 512;
    public static final int F_PROJECT = 256;
    public static final int F_WARNING = 2;

    private Image fBlankImage;

    ArrayList<Object> consumers = new ArrayList<Object>();
    Hashtable<Object, Image> images = new Hashtable<Object, Image>();

    /**
     * 
     */
    public SharedLabelProvider() {

    }

    /**
     * @param consumer
     */
    public void connect(Object consumer) {
        if (!consumers.contains(consumer)) {
            consumers.add(consumer);
        }
    }

    /**
     * @param consumer
     */
    public void disconnect(Object consumer) {
        consumers.remove(consumer);
        if (consumers.size() == 0) {
            dispose();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.BaseLabelProvider#dispose()
     */
    @Override
    public void dispose() {
        if (consumers.size() == 0) {
            for (Enumeration<Image> elements = images.elements(); elements
                    .hasMoreElements();) {
                (elements.nextElement()).dispose();
            }
            images.clear();
            if (fBlankImage != null) {
                fBlankImage.dispose();
                fBlankImage = null;
            }
        }
    }

    /**
     * @param image
     * @param flags
     * @return
     */
    public Image get(Image image, int flags) {
        if (flags == 0) {
            return image;
        }
        String key = getKey(image.hashCode(), flags);
        Image resultImage = images.get(key);
        if (resultImage == null) {
            resultImage = createImage(image, flags);
            images.put(key, resultImage);
        }
        return resultImage;
    }

    /**
     * @param desc
     * @return
     */
    public Image get(ImageDescriptor desc) {
        return get(desc, 0);
    }

    /**
     * @param desc
     * @param flags
     * @return
     */
    public Image get(ImageDescriptor desc, int flags) {
        Object key = desc;

        if (flags != 0) {
            key = getKey(desc.hashCode(), flags);
        }
        Image image = images.get(key);
        if (image == null) {
            image = createImage(desc, flags);
            images.put(key, image);
        }
        return image;
    }

    /**
     * @return
     */
    public Image getBlankImage() {
        if (fBlankImage == null) {
            fBlankImage = ImageDescriptor.getMissingImageDescriptor()
                    .createImage();
        }
        return fBlankImage;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
     */
    public Image getColumnImage(Object obj, int index) {
        return getImage(obj);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
     */
    public String getColumnText(Object obj, int index) {
        return getText(obj);
    }

    /**
     * @param bundleID
     * @param path
     * @return
     */
    public Image getImageFromPlugin(String bundleID, String path) {
        ImageDescriptor desc = AbstractUIPlugin.imageDescriptorFromPlugin(
                bundleID, path);
        return (desc != null) ? get(desc) : getBlankImage();
    }

    /**
     * @param baseImage
     * @param flags
     * @return
     */
    private Image createImage(Image baseImage, int flags) {
        if (flags == 0) {
            return baseImage;
        }
        ImageDescriptor[] lowerLeft = getLowerLeftOverlays(flags);
        ImageDescriptor[] upperRight = getUpperRightOverlays(flags);
        ImageDescriptor[] lowerRight = getLowerRightOverlays(flags);
        ImageDescriptor[] upperLeft = getUpperLeftOverlays(flags);
        ImageOverlayIcon compDesc = new ImageOverlayIcon(baseImage,
                new ImageDescriptor[][] { upperRight, lowerRight, lowerLeft,
                        upperLeft });
        return compDesc.createImage();
    }

    /**
     * @param baseDesc
     * @param flags
     * @return
     */
    private Image createImage(ImageDescriptor baseDesc, int flags) {
        if (flags == 0) {
            return baseDesc.createImage();
        }
        ImageDescriptor[] lowerLeft = getLowerLeftOverlays(flags);
        ImageDescriptor[] upperRight = getUpperRightOverlays(flags);
        ImageDescriptor[] lowerRight = getLowerRightOverlays(flags);
        ImageDescriptor[] upperLeft = getUpperLeftOverlays(flags);
        OverlayIcon compDesc = new OverlayIcon(baseDesc,
                new ImageDescriptor[][] { upperRight, lowerRight, lowerLeft,
                        upperLeft });
        return compDesc.createImage();
    }

    /**
     * @param hashCode
     * @param flags
     * @return
     */
    private String getKey(long hashCode, int flags) {
        return ("" + hashCode) + ":" + flags; //$NON-NLS-1$ //$NON-NLS-2$
    }

    /**
     * @param flags
     * @return
     */
    private ImageDescriptor[] getLowerLeftOverlays(int flags) {
        if ((flags & F_ERROR) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_ERROR_CO };
        }
        if ((flags & F_WARNING) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_WARNING_CO };
        }
        return null;
    }

    /**
     * @param flags
     * @return
     */
    private ImageDescriptor[] getLowerRightOverlays(int flags) {
        if ((flags & F_JAR) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_JAR_CO };
        }
        if ((flags & F_PROJECT) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_PROJECT_CO };
        }
        if ((flags & F_OPTIONAL) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_OPTIONAL_CO };
        }
        if ((flags & F_INTERNAL) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_INTERNAL_CO };
        }
        if ((flags & F_FRIEND) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_FRIEND_CO };
        }
        return null;
    }

    /**
     * @param flags
     * @return
     */
    private ImageDescriptor[] getUpperLeftOverlays(int flags) {
        if ((flags & F_EXTERNAL) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_EXTERNAL_CO };
        }
        if ((flags & F_BINARY) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_BINARY_CO };
        }
        return null;
    }

    /**
     * @param flags
     * @return
     */
    private ImageDescriptor[] getUpperRightOverlays(int flags) {
        if ((flags & F_EXPORT) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_EXPORT_CO };
        }
        if ((flags & F_EDIT) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_DOC_CO };
        }
        if ((flags & F_JAVA) != 0) {
            return new ImageDescriptor[] { MTJUIPluginImages.DESC_JAVA_CO };
        }
        return null;
    }
}
