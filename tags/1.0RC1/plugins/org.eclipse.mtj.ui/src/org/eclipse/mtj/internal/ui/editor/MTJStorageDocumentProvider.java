/**
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDEStorageDocumentProvider
 */
package org.eclipse.mtj.internal.ui.editor;

import java.io.File;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.editors.text.StorageDocumentProvider;

/**
 * Shared document provider specialized for
 * {@link org.eclipse.core.resources.IStorage}s.
 * 
 * @since 0.9.1
 */
public class MTJStorageDocumentProvider extends StorageDocumentProvider {

    /**
     * Participates in the setup of a text file buffer document.
     */
    private IDocumentSetupParticipant setupParticipant;

    /**
     * Creates a new {@link MTJStorageDocumentProvider#}
     * 
     * @param setupParticipant the participant in the setup of a text file
     *            buffer document.
     */
    public MTJStorageDocumentProvider(IDocumentSetupParticipant setupParticipant) {
        this.setupParticipant = setupParticipant;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.editors.text.StorageDocumentProvider#createAnnotationModel(java.lang.Object)
     */
    @Override
    protected IAnnotationModel createAnnotationModel(Object element)
            throws CoreException {
        if (element instanceof IAdaptable) {
            IAdaptable input = (IAdaptable) element;
            File file = (File) input.getAdapter(File.class);
            if (file != null) {
                return new SystemFileMarkerAnnotationModel();
            }
        }
        return super.createAnnotationModel(element);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.editors.text.StorageDocumentProvider#setupDocument(java.lang.Object, org.eclipse.jface.text.IDocument)
     */
    @Override
    protected void setupDocument(Object element, IDocument document) {
        if ((document != null) && (setupParticipant != null)) {
            setupParticipant.setup(document);
        }
    }
}
