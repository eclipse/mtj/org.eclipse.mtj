/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma (Sybase)        - Initial implementation
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874]
 *     David Marques (Motorola)- Removing permissions fields.  
 */
package org.eclipse.mtj.internal.ui.editors.jad.form.pages;

import org.eclipse.mtj.core.project.midp.DescriptorPropertyDescription;
import org.eclipse.mtj.core.project.midp.IJADDescriptorsProvider;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;

/**
 * provide optional JAD attributes descriptors
 * 
 * @author Gang Ma
 */
public class OptionalJADDescriptorsProvider implements IJADDescriptorsProvider {

    /**
     * Optional property descriptors.
     */
    private static final DescriptorPropertyDescription[] OPTIONAL_DESCRIPTORS = new DescriptorPropertyDescription[] {
            new DescriptorPropertyDescription(
                    IJADConstants.JAD_MIDLET_DATA_SIZE, MTJUIMessages.OptionalJADDescriptorsProvider_midlet_data_size,
                    DescriptorPropertyDescription.DATATYPE_INT),
            new DescriptorPropertyDescription(
                    IJADConstants.JAD_MIDLET_DESCRIPTION, MTJUIMessages.OptionalJADDescriptorsProvider_midlet_description,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_ICON,
                    MTJUIMessages.OptionalJADDescriptorsProvider_midlet_icon,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(
                    IJADConstants.JAD_MIDLET_INFO_URL,
                    MTJUIMessages.OptionalJADDescriptorsProvider_midlet_info_url,
                    DescriptorPropertyDescription.DATATYPE_URL), };

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider#getDescriptorPropertyDescriptions()
     */
    public DescriptorPropertyDescription[] getDescriptorPropertyDescriptions() {
        return OPTIONAL_DESCRIPTORS;
    }

}
