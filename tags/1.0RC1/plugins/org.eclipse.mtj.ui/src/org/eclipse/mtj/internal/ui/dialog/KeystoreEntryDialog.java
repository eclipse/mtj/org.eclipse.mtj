/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.ui.dialog;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * KeystoreEntryDialog class provides a dialog for
 * users to enter a	keystore alias and it's password.
 * 
 * @author David Marques
 * @since 1.0
 */
public class KeystoreEntryDialog extends Dialog {

	private Text aliasText;
	private Text passwdText;

	private String title;
	private String alias;
	private String password;
	
	/**
	 * Creates a dialog instance.
	 * 
	 * @param parentShell parent shell.
	 */
	public KeystoreEntryDialog(Shell parentShell) {
		this(parentShell, null);
	}

	/**
	 * Creates a dialog instance with the specified
	 * alias already filled on the dialog's widget.
	 * 
	 * @param parentShell parent shell.
	 * @param alias alias name.
	 */
	public KeystoreEntryDialog(Shell parentShell, String alias) {
		super(parentShell);
		this.alias = alias;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	protected Control createDialogArea(Composite parent) {
		Composite main = new Composite(parent, SWT.NONE);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.widthHint = 400;
		main.setLayoutData(gridData);
		main.setLayout(new GridLayout(0x02, false));
		
		Label label = new Label(main, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		label.setText("Alias:");
		
		aliasText = new Text(main, SWT.BORDER);
		aliasText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		if (this.alias != null) {
			aliasText.setText(alias);
		}
		
		label = new Label(main, SWT.NONE);
		label.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));
		label.setText("Password:");
		
		passwdText = new Text(main, SWT.PASSWORD | SWT.BORDER);
		passwdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		return main;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		if (this.title != null) {			
			newShell.setText(this.title);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	protected void okPressed() {
		this.alias    = this.aliasText.getText();
		this.password = this.passwdText.getText();
		super.okPressed();
	}
	
	/**
	 * Sets the dialog's title.
	 * 
	 * @param title dialog title.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
}
