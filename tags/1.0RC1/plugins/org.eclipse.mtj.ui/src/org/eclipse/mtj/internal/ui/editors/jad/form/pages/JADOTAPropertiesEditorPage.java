/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Refactoring the page to add expansibilities
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]                                 
 */
package org.eclipse.mtj.internal.ui.editors.jad.form.pages;

import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.editors.jad.form.JADFormEditor;
import org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;

/**
 * JAD editor page for handling the over the air properties.
 * 
 * @author Craig Setera
 */
public class JADOTAPropertiesEditorPage extends JADPropertiesEditorPage {

    /**
     * The page unique identifier
     */
    public static final String OTA_PAGEID = "ota"; //$NON-NLS-1$

    /**
     * A constructor that creates the JAD Over the Air (OTA) EditorPage. The
     * parent editor need to be passed in the <code>initialize</code> method if
     * this constructor is used.
     */
    public JADOTAPropertiesEditorPage() {
        super(OTA_PAGEID, MTJUIMessages.JADOTAPropertiesEditorPage_title);
    }

    /**
     * A constructor that creates the JAD Over the Air (OTA) EditorPage and
     * initializes it with the editor.
     * 
     * @param editor the parent editor
     */
    public JADOTAPropertiesEditorPage(JADFormEditor editor) {
        super(editor, OTA_PAGEID,
                MTJUIMessages.JADOTAPropertiesEditorPage_title);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getTitle()
     */
    @Override
    public String getTitle() {
        return MTJUIMessages.JADOTAPropertiesEditorPage_title;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#addContextHelp(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void addContextHelp(Composite c) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(c,
                "org.eclipse.mtj.ui.help_JADOTAPropertiesEditorPage"); //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
     */
    @Override
    protected String getHelpResource() {
        return "/org.eclipse.mtj.doc.user/html/reference/editors/jad_editor/ota.html"; //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionDescription()
     */
    @Override
    protected String getSectionDescription() {
        return MTJUIMessages.JADOTAPropertiesEditorPage_SectionDescription;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionTitle()
     */
    @Override
    protected String getSectionTitle() {
        return MTJUIMessages.JADOTAPropertiesEditorPage_SectionTitle;
    }
}
