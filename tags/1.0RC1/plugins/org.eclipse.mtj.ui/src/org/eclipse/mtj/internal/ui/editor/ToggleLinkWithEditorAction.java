/**
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.jface.action.Action;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;

/**
 * This action toggles whether the Outline page links its selection to the
 * active editor.
 * 
 * @since 0.9.1
 */
public class ToggleLinkWithEditorAction extends Action {

    /**
     * 
     */
    MTJFormEditor fEditor;

    /**
     * @param editor
     */
    public ToggleLinkWithEditorAction(MTJFormEditor editor) {

        super(MTJUIMessages.ToggleLinkWithEditorAction_name);
        boolean isLinkingEnabled = MTJUIPlugin.getDefault()
                .getPreferenceStore().getBoolean(
                        "ToggleLinkWithEditorAction.isChecked"); //$NON-NLS-1$
        setChecked(isLinkingEnabled);
        fEditor = editor;
        setToolTipText(MTJUIMessages.ToggleLinkWithEditorAction_toolTipText);
        setDescription(MTJUIMessages.ToggleLinkWithEditorAction_description);
        setImageDescriptor(MTJUIPluginImages.DESC_LINK_WITH_EDITOR);
        setDisabledImageDescriptor(MTJUIPluginImages.DESC_LINK_WITH_EDITOR_DISABLED);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {
        MTJUIPlugin.getDefault().getPreferenceStore().setValue(
                "ToggleLinkWithEditorAction.isChecked", isChecked()); //$NON-NLS-1$
        if (isChecked()) {
            fEditor.synchronizeOutlinePage();
        }
    }
}
