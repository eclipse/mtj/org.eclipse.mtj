/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Refactoring the page to support extensibility
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     David Marques (Motorola) - Removing hyperlinks from message manager.                       
 */
package org.eclipse.mtj.ui.editors.jad;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.editors.FormLayoutFactory;
import org.eclipse.mtj.internal.ui.editors.jad.form.JADFormEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.IMessage;
import org.eclipse.ui.forms.IMessageManager;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.events.IHyperlinkListener;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;

/**
 * Abstract superclass of the multi-page editor pages.
 * 
 * @since 1.0
 */
public abstract class AbstractJADEditorPage extends FormPage implements
        IHyperlinkListener {

    /** Whether the page contents are currently dirty */
    private boolean dirty;

    /** The editor we are operating within */
    private JADFormEditor editor;

    private IJavaProject javaProject;

    IMessageManager errorMessageManager;

    /**
     * A constructor that creates the JAD EditorPage and initializes it with the
     * editor.
     * 
     * @param editor the parent editor
     * @param id the unique identifier
     * @param title the page title
     * @noreference This constructor is not intended to be referenced by clients.
     */
    public AbstractJADEditorPage(JADFormEditor editor, String id, String title) {
        super(editor, id, title);
        this.editor = editor;
        this.javaProject = JavaCore.create((editor).getJadFile().getProject());
    }

    /**
     * A constructor that creates the JAD EditorPage. The parent editor need to
     * be passed in the <code>initialize</code> method if this constructor is
     * used. The page title will be the same as the id.
     * 
     * @param id a unique page identifier
     */
    public AbstractJADEditorPage(String id) {
        this(id, id);
    }

    /**
     * A constructor that creates the JAD EditorPage. The parent editor need to
     * be passed in the <code>initialize</code> method if this constructor is
     * used.
     * 
     * @param id a unique page identifier
     * @param title a user-friendly page title
     */
    public AbstractJADEditorPage(String id, String title) {
        super(id, title);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#dispose()
     */
    @Override
    public void dispose() {
        super.dispose();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#doSaveAs()
     */
    @Override
    public void doSaveAs() {
        // Not allowed...
    }

    /**
     * Let the editor know that the dirty state has changed.
     */
    public void editorDirtyStateChanged() {
        editor.editorDirtyStateChanged();
    }

    /**
     * An indication to this page that the editor input has been updated
     * external to Eclipse and the page should be updated.
     */
    public abstract void editorInputChanged();

    public IMessageManager getErrorMessageManager() {
        return errorMessageManager;
    }

    /**
     * Return the java project holding the JAD file.
     * 
     * @return A reference to the IJavaProject that contains this jad file
     */
    public IJavaProject getJavaProject() {
        return javaProject;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.WorkbenchPart#getTitle()
     */
    @Override
    public abstract String getTitle();

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#initialize(org.eclipse.ui.forms.editor.FormEditor)
     */
    @Override
    public void initialize(FormEditor editor) {
        super.initialize(editor);
        this.editor = (JADFormEditor) editor;
        this.javaProject = JavaCore.create((this.editor).getJadFile()
                .getProject());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#isDirty()
     */
    @Override
    public boolean isDirty() {
        return dirty || getPreferenceStore().needsSaving() || super.isDirty();
    }

    /**
     * Return a boolean indicating whether the specified property is managed by
     * this page.
     * 
     * @param property
     * @return
     */
    public abstract boolean isManagingProperty(String property);

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#isSaveAsAllowed()
     */
    @Override
    public boolean isSaveAsAllowed() {
        return false;
    }

    /**
     * Subclasses can extend this method and add custom link activation
     * handling. By default this method is just an stub.
     * 
     * @param e an event containing information about the hyperlink
     * @see org.eclipse.ui.forms.events.IHyperlinkListener#linkActivated(org.eclipse.ui.forms.events.HyperlinkEvent)
     */

    public void linkActivated(HyperlinkEvent e) {
        // Nothing to be done
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.events.IHyperlinkListener#linkEntered(org.eclipse.ui.forms.events.HyperlinkEvent)
     */
    public void linkEntered(HyperlinkEvent e) {
        IStatusLineManager mng = getEditor().getEditorSite().getActionBars()
                .getStatusLineManager();
        mng.setMessage(e.getLabel());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.events.IHyperlinkListener#linkExited(org.eclipse.ui.forms.events.HyperlinkEvent)
     */
    public void linkExited(HyperlinkEvent e) {
        IStatusLineManager mng = getEditor().getEditorSite().getActionBars()
                .getStatusLineManager();
        mng.setMessage(null);
    }

    /**
     * Set the dirty flag and let the editor know the state has changed.
     * 
     * @param dirty whether the page contents are currently dirty
     */
    public void setDirty(boolean dirty) {
        this.dirty = dirty;
        editorDirtyStateChanged();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.EditorPart#setInitializationData(org.eclipse.core.runtime.IConfigurationElement,
     *      java.lang.String, java.lang.Object)
     */
    @Override
    public void setInitializationData(IConfigurationElement cfig,
            String propertyName, Object data) {
        super.setInitializationData(cfig, propertyName, data);
    }

    /**
     * @param type
     * @return
     */
    private Image getImage(int type) {
        switch (type) {
            case IMessageProvider.ERROR:
                return PlatformUI.getWorkbench().getSharedImages().getImage(
                        ISharedImages.IMG_OBJS_ERROR_TSK);
            case IMessageProvider.WARNING:
                return PlatformUI.getWorkbench().getSharedImages().getImage(
                        ISharedImages.IMG_OBJS_WARN_TSK);
            case IMessageProvider.INFORMATION:
                return PlatformUI.getWorkbench().getSharedImages().getImage(
                        ISharedImages.IMG_OBJS_INFO_TSK);
        }
        return null;
    }

    /**
     * @param form
     * @param text
     */
    protected void configureFormText(final Form form, FormText text) {
        text.addHyperlinkListener(new HyperlinkAdapter() {
            @Override
            public void linkActivated(HyperlinkEvent e) {
                String is = (String) e.getHref();
                try {
                    int index = Integer.parseInt(is);
                    IMessage[] messages = form.getChildrenMessages();
                    IMessage message = messages[index];
                    Control c = message.getControl();
                    ((FormText) e.widget).getShell().dispose();
                    if (c != null) {
                        c.setFocus();
                    }
                } catch (Throwable throwable) {
                    ((FormText) e.widget).getShell().dispose();
                }
            }
        });
        text.setImage("error", getImage(IMessageProvider.ERROR));
        text.setImage("warning", getImage(IMessageProvider.WARNING));
        text.setImage("info", getImage(IMessageProvider.INFORMATION));
    }

    /**
     * @param section
     * @param content
     * @param toolkit
     * @return
     */
    protected final FormText createClient(Composite section, String content,
            FormToolkit toolkit, IHyperlinkListener listener) {
        FormText text = toolkit.createFormText(section, true);
        try {
            text.setText(content, true, false);
        } catch (SWTException e) {
            text.setText(e.getMessage(), false, false);
        }
        text.addHyperlinkListener(listener);
        return text;
    }

    /**
     * @param managedForm
     */
    protected void createErrorMessageHandler(final IManagedForm managedForm) {
        errorMessageManager = managedForm.getMessageManager();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createFormContent(IManagedForm managedForm) {
        final ScrolledForm form = managedForm.getForm();
        final FormToolkit toolkit = managedForm.getToolkit();

        form.setText(getTitle());
        toolkit.decorateFormHeading(form.getForm());

        createErrorMessageHandler(managedForm);
        /*
         * launch the help system UI, displaying the documentation identified by
         * the href parameter.
         */
        final String href = getHelpResource();
        if (href != null) {
            IToolBarManager manager = form.getToolBarManager();
            Action helpAction = new Action("help") { //$NON-NLS-1$
                @Override
                public void run() {
                    PlatformUI.getWorkbench().getHelpSystem()
                            .displayHelpResource(href);
                }
            };
            helpAction.setImageDescriptor(MTJUIPluginImages.DESC_LINKTOHELP);
            manager.add(helpAction);
        }
        form.updateToolBar();
    }

    /**
     * @param toolkit
     * @param parent
     * @param text
     * @param description
     * @return
     */
    protected final Section createStaticBasicSection(FormToolkit toolkit,
            Composite parent, String text, String description) {
        Section section = toolkit.createSection(parent, Section.DESCRIPTION
                | ExpandableComposite.TITLE_BAR);
        section.setText(text);
        section.setDescription(description);
        section.setLayout(FormLayoutFactory
                .createClearTableWrapLayout(false, 1));
        TableWrapData data = new TableWrapData(TableWrapData.FILL_GRAB);
        section.setLayoutData(data);
        return section;
    }

    /**
     * @param toolkit
     * @param parent
     * @param text
     * @return
     */
    protected final Section createStaticSection(FormToolkit toolkit,
            Composite parent, String text) {
        Section section = toolkit.createSection(parent,
                ExpandableComposite.TITLE_BAR);
        section.clientVerticalSpacing = FormLayoutFactory.SECTION_HEADER_VERTICAL_SPACING;
        section.setText(text);
        section.setLayout(FormLayoutFactory
                .createClearTableWrapLayout(false, 1));
        TableWrapData data = new TableWrapData(TableWrapData.FILL_GRAB);
        section.setLayoutData(data);
        return section;
    }

    /**
     * @param toolkit
     * @param parent
     * @return
     */
    protected Composite createStaticSectionClient(FormToolkit toolkit,
            Composite parent) {
        Composite container = toolkit.createComposite(parent, SWT.NONE);
        container.setLayout(FormLayoutFactory
                .createSectionClientTableWrapLayout(false, 1));
        TableWrapData data = new TableWrapData(TableWrapData.FILL_GRAB);
        container.setLayoutData(data);
        return container;
    }

    /**
     * @return the URL of the help resource.
     */
    protected abstract String getHelpResource();

    /**
     * Get the preference store being edited.
     */
    protected IManifestPreferenceStore getPreferenceStore() {
        return editor.getPreferenceStore();
    }
}
