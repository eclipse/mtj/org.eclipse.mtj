/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Fixing propeties folder path
 *     David Marques (Motorola) - Removing old classes from L10n API
 *                                upon API creation.
 *     David Marques (Motorola) - Generating keys on upper case.
 *     David Marques (Motorola) - Refactoring to support default locale.
 */
package org.eclipse.mtj.internal.core.l10n;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.l10n.L10nEntry;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.ReplaceableParametersProcessor;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.osgi.util.NLS;

/**
 * L10nApi Class contains utilitary classes for management of the Localization
 * Client API.
 */
public class L10nApi {

    public static final String LOCALIZATION_DATA_FILE = "Localization Data"; //$NON-NLS-1$
    public static final String DESTINATION = "destination"; //$NON-NLS-1$
    public static final String PACKAGE = "package"; //$NON-NLS-1$
    private static final String DEFAULT_LOCALE = "defaultlocale"; //$NON-NLS-1$

    public static final String L10N_RESOURCES_CLASS = "L10nResources.java"; //$NON-NLS-1$
    public static final String L10N_CONSTANTS_CLASS = "L10nConstants.java"; //$NON-NLS-1$

    /**
     * Creates the Localization API classes.
     * 
     * @param project Target project
     * @param _package Class package name.
     * @param _properties Resources destination.
     * @return true upon success false otherwise.
     * @throws CoreException If any error occurs.
     */
    public static boolean createLocalizationApi(IProject project,
            IPackageFragment _package, IPath _properties) throws CoreException {
        return updateLocalizationApi(project, _package, _properties, null);
    }

    /**
     * Creates the L10nConstant Class code.
     * 
     * @param project Target project
     * @param _package Class package name.
     * @return the class code.
     * @throws CoreException If any error occurs.
     */
    public static String buildL10nConstantsClass(IProject project,
            IPackageFragment _package) throws CoreException {
        InputStream stream = MTJCore.getResourceAsStream(new Path(
                "templates/L10nConstants.java.template"));
        if (stream == null) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, -999,
                    "Unable to load templates/L10nResources.java.template.");
        }

        StringBuffer localesBuffer = new StringBuffer();
        List<String> keys = new ArrayList<String>();
        L10nModel model = loadL10nModel(project);
        if (!model.isValid()) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, -999,
                    Messages.L10nBuilder_InvalidModel);
        }

        List<IDocumentElementNode> localeNodes = model.getLocales()
                .getChildren();
        for (IDocumentElementNode localeElement : localeNodes) {
            L10nLocale locale = (L10nLocale) localeElement;
            localesBuffer.append(writeStringConstantDeclaration(locale
                    .getName().toUpperCase(), locale.getName()));

            IDocumentElementNode[] entryNodes = locale.getChildNodes();
            for (int i = 0; i < entryNodes.length; i++) {
                L10nEntry entry = (L10nEntry) entryNodes[i];
                if (!keys.contains(entry.getKey().toUpperCase())) {
                    keys.add(entry.getKey().toUpperCase());
                }
            }
        }

        StringBuffer constntBuffer = new StringBuffer();
        for (String key : keys) {
            constntBuffer.append(writeStringConstantDeclaration(key, key));
        }

        Map<String, String> map = new HashMap<String, String>();
        String packName = _package.getElementName();
        if (packName != null && packName.length() > 0x00) {
            map.put(PACKAGE, NLS.bind("package {0};", packName));
        } else {
            map.put(PACKAGE, Utils.EMPTY_STRING);
        }
        map.put("locales", localesBuffer.toString());
        map.put("keys", constntBuffer.toString());

        StringBuffer buffer = new StringBuffer(Utils.getStreamContent(stream));
        String code = ReplaceableParametersProcessor.processReplaceableValues(
                buffer.toString(), map);
        return code;
    }

    /**
     * Creates the L10nResources Class code.
     * 
     * @param _package Class package name.
     * @param _properties The properties folder path.
     * @param _defaultLocale default locale.
     * @return the class code.
     * @throws CoreException If any error occurs.
     */
    public static String buildL10nResourcesClass(IPackageFragment _package,
            IPath _properties, String _defaultLocale) throws CoreException {
        InputStream stream = MTJCore.getResourceAsStream(new Path(
                "templates/L10nResources.java.template"));
        if (stream == null) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, -999,
                    "Unable to load templates/L10nResources.java.template.");
        }

        Map<String, String> map = new HashMap<String, String>();
        map.put(DESTINATION, _properties.removeFirstSegments(1).toString());
        String packName = _package.getElementName();
        if (packName != null && packName.length() > 0x00) {
            map.put(PACKAGE, NLS.bind("package {0};", packName));
        } else {
            map.put(PACKAGE, Utils.EMPTY_STRING);
        }
        if (_defaultLocale != null) {
        	map.put(DEFAULT_LOCALE, _defaultLocale);
		}
        
        StringBuffer buffer = new StringBuffer(Utils.getStreamContent(stream));
        String code = ReplaceableParametersProcessor.processReplaceableValues(
                buffer.toString(), map);
        return code;
    }

    /**
     * Loads the L10n Model from the specified project.
     * 
     * @param project target project.
     * @return the model.
     * @throws CoreException - If could not load model.
     */
    public static L10nModel loadL10nModel(IProject project)
            throws CoreException {
        L10nModel model = null;
        IFile l10nData = project.getFile(LOCALIZATION_DATA_FILE);
        if (l10nData.exists()) {
            IPath l10nDataPath = l10nData.getLocation();
            if (l10nDataPath != null) {
                model = new L10nModel(Utils.getTextDocument(l10nData
                        .getContents()), false);
                model.load();
            } else {
                throw new CoreException(
                        MTJStatusHandler
                                .newStatus(
                                        IStatus.ERROR,
                                        -999,
                                        Messages.L10nBuilder_loadL10nModel_CanNotRealizeLocation));
            }
        } else {
            throw new CoreException(MTJStatusHandler.newStatus(IStatus.ERROR, -999,
                    Messages.L10nBuilder_LocalizationDataDoesNotExist));
        }
        return model;
    }

    /**
     * Writes a constant declaration.
     * 
     * @param name constant name.
     * @param value constant value.
     * @return the declaration.
     */
    private static String writeStringConstantDeclaration(String name,
            String value) {
        return NLS.bind("\t\tpublic static final String {0} = \"{1}\";\n",
                new String[] { name.replace("-", "_"), value });
    }

    /**
     * Synchronizes the API classes with the model.
     * 
     * @param model model instance.
     * @throws CoreException any error occurs.
     */
    public static void syncronizeApi(L10nModel model) throws CoreException {
        IResource resource = model.getUnderlyingResource();
        IJavaProject project = JavaCore.create(resource.getProject());

        IPackageFragment[] packages = project.getPackageFragments();
        for (IPackageFragment packageFragment : packages) {
            if (packageFragment.getElementName().equals(
                    model.getLocales().getPackage())) {
            	L10nLocales locales     = model.getLocales();
            	IPath       destination = new Path(locales.getDestination());
            	String      defLocale   = null;
            	if (model.getLocales().getDefaultLocale() != null) {
					defLocale = model.getLocales().getDefaultLocale().getName();
				}
            	
                L10nApi.updateLocalizationApi(project.getProject(),
                        packageFragment, destination, defLocale);
                break;
            }
        }
    }

	/**
	 * Updates the L10n API.
	 * 
	 * @param project Target project
     * @param _package Class package name.
     * @param _properties Resources destination.
	 * @param _defaultLocale default locale
	 * @return true upon success false otherwise.
     * @throws CoreException If any error occurs.
	 */
	private static boolean updateLocalizationApi(IProject project,
			IPackageFragment _package, IPath _properties, String _defaultLocale)  throws CoreException {
		ICompilationUnit resourcesClass = null;
        ICompilationUnit constantsClass = null;

        IPackageFragment targetPackage = (IPackageFragment) _package;

        project.accept(new IResourceVisitor() {
            public boolean visit(IResource resource) throws CoreException {
                if (resource.getName().equals(L10N_RESOURCES_CLASS)
                        || resource.getName().equals(L10N_CONSTANTS_CLASS)) {
                    resource.delete(true, new NullProgressMonitor());
                }
                return true;
            }
        });

        String code = buildL10nResourcesClass(_package, _properties, _defaultLocale);
        resourcesClass = targetPackage.createCompilationUnit(
                L10N_RESOURCES_CLASS, code, true, null);

        code = buildL10nConstantsClass(project, _package);
        constantsClass = targetPackage.createCompilationUnit(
                L10N_CONSTANTS_CLASS, code, true, null);

        return resourcesClass != null && constantsClass != null;
	}

}
