package org.eclipse.mtj.internal.core.util.xml;

import org.w3c.dom.Element;

/**
 * DocumentVisitor interface defines a set of operations for a visitor of a xml
 * document. <br>
 * <b>NOTE: see visitor pattern at <a
 * href="http://en.wikipedia.org/wiki/Visitor_pattern"></a> </b>
 * 
 * @author David Marques
 */
public interface DocumentVisitor {

    /**
     * Visits the element of a document.
     * 
     * @param element.
     */
    public void visitElement(Element element);

}
