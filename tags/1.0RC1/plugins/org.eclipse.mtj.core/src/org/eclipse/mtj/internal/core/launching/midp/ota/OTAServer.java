/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Diego Sandin (Motorola)  - Adapting code to use Jetty version 5.1.11 
 *     Diego Sandin (Motorola)  - Fixed NPE [Bug 267668]                               
 */
package org.eclipse.mtj.internal.core.launching.midp.ota;

import java.io.File;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Preferences;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.mortbay.http.HttpContext;
import org.mortbay.http.HttpListener;
import org.mortbay.http.HttpServer;
import org.mortbay.http.SocketListener;
import org.mortbay.http.handler.NotFoundHandler;
import org.mortbay.log.LogImpl;
import org.mortbay.log.OutputStreamLogSink;

/**
 * Singleton management of the Over the Air HTTP Server.
 * 
 * @author Craig Setera
 */
public class OTAServer {

    /**
     * Singleton instance of the OTAServer
     */
    private static OTAServer instance;

    /**
     * Return the unique instance of the OTAServer
     * 
     * @return a unique OTAServer instance.
     */
    public static synchronized OTAServer getInstance() {
        if (instance == null) {
            instance = new OTAServer();
        }
        return instance;
    }

    /**
     * Return the port to be listened on for OTA connections.
     * 
     * @return
     */
    public static synchronized int getPort() {
        HttpListener listener = getInstance().getHttpServer().getListeners()[0];
        return listener.getPort();
    }

    // The HttpServer instance in use for serving the content
    private HttpServer httpServer;

    /**
     * Instance of Log
     */
    private LogImpl logImpl;

    /**
     * The HTTP Server instance
     */
    private HttpServer server;

    /**
     * Creates a new instance of OTAServer.
     */
    private OTAServer() {
        super();
        logImpl = new LogImpl();
        server = getHttpServer();
    }

    /**
     * @throws Exception
     */
    public synchronized void start() throws Exception {
        if (!server.isStarted()) {
            server.start();
        }
    }

    /**
     * Stop the HttpServer if it is currently running.
     * 
     * @throws InterruptedException
     */
    public synchronized void stop() throws InterruptedException {
        HttpServer server = getHttpServer();
        if (server.isStarted()) {
            server.stop();
        }
    }

    /**
     * Configure the context(s) for the HttpServer.
     * 
     * @param server
     */
    private void configureContext(HttpServer server) {
        // Configure the context
        HttpContext context = new HttpContext();
        context.setContextPath("/ota/*"); //$NON-NLS-1$
        server.addContext(context);

        // Configure the handlers in the context
        context.addHandler(new OTAHandler());
        context.addHandler(new NotFoundHandler());
    }

    /**
     * Configure the HttpServer listener.
     * 
     * @param server the HTTP Server.
     */
    private void configureListener(HttpServer server) {
        SocketListener listener = new SocketListener();
        listener.setMinThreads(1);
        listener.setMaxThreads(2);
        listener.setMaxIdleTimeMs(10000);
        listener.setDaemon(true);

        Preferences prefs = MTJCore.getMTJCore().getPluginPreferences();
        if (prefs.getBoolean(IMTJCoreConstants.PREF_OTA_PORT_DEFINED)) {
            int specifiedPortNumber = prefs
                    .getInt(IMTJCoreConstants.PREF_OTA_PORT);
            listener.setPort(specifiedPortNumber);
        }

        server.addListener(listener);
    }

    /**
     * Configure the Jetty logging support so that it writes out to our metadata
     * directory.
     * 
     * @throws Exception
     */
    private void configureLogging() throws Exception {
        // Set some System properties so that they are available
        // when creating the logging
        System.setProperty("LOG_FILE_RETAIN_DAYS", "5"); //$NON-NLS-1$ //$NON-NLS-2$

        // Calculate the name of the file to be used for logging
        IPath stateLocation = MTJCore.getMTJCore().getStateLocation();
        IPath logDirectory = stateLocation.append("jetty"); //$NON-NLS-1$

        File logDirectoryFile = logDirectory.toFile();
        if (!logDirectoryFile.exists()) {
            logDirectoryFile.mkdir();
        }

        File logFile = new File(logDirectoryFile, "logfile"); //$NON-NLS-1$
        String filename = logFile + "yyyy_mm_dd.txt"; //$NON-NLS-1$

        // Create the output sink and add it to the logging class
        OutputStreamLogSink sink = new OutputStreamLogSink(filename);

        sink.start();

        logImpl.add(sink);
    }

    /**
     * Create and configure the HTTP server instance.
     * 
     * @return
     */
    private HttpServer createHttpServer() {
        HttpServer server = new HttpServer();
        configureListener(server);
        configureContext(server);
        return server;
    }

    /**
     * Get the HttpServer instance.
     * 
     * @return
     */
    private HttpServer getHttpServer() {
        if (httpServer == null) {
            httpServer = createHttpServer();
            try {
                configureLogging();
            } catch (Exception e) {
                MTJLogger.log(IStatus.WARNING, "configureLogging", e); //$NON-NLS-1$
            }
        }

        return httpServer;
    }
}
