/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Changed methods signatures
 */
package org.eclipse.mtj.internal.core.util;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class provides processing for a string containing replaceable values.
 * This class supports strings that are formatted as follows:
 * 
 * <pre>
 * [%heapsize%|-Xheapsize:%heapsize%] [%securityDomain%|-Xdomain:%securityDomain%] %userSpecifiedArguments%
 * </pre>
 * 
 * Strings specified within '%' characters are replaceable. The string between
 * the characters provides the name of a property to be found within the
 * provided properties object. Expressions of the form
 * <code>[%value1%|optional expression]</code> specify an optional expression.
 * If the result of evaluating <code>%value1%</code> is non-null and has a
 * length greater than zero, the entire optional expression with be replaced
 * with the value of <code>optional expression</code>. If the result of
 * evaluating <code>%value1%</code> is <code>null</code> or an empty string, the
 * entire conditional expression will be removed.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @since 1.0
 */
public class ReplaceableParametersProcessor {

    /**
     * Regular expression for matching replaceable optional values within a
     * device command-line.
     */
    private static Pattern OPTIONAL_BLOCK_PATTERN = Pattern
            .compile("\\[%(.*?)%\\|(.*?)\\]"); //$NON-NLS-1$

    /**
     * Regular expression for matching replaceable values within a device XML
     * property file.
     */
    private static Pattern PROPS_PATTERN = Pattern.compile("%(.*?)%"); //$NON-NLS-1$

    private static final String TEMP_PROP = "__mtjTemp"; //$NON-NLS-1$

    /**
     * Process the replaceable values in the specified string with values from
     * the replacement values provided.
     * 
     * @param replaceableString
     * @param replacementValues
     * @return
     */
    public static String processReplaceableValues(String replaceableString,
            Map<String, String> replacementValues) {
        // Stuff the replaceable string into the values and pull it out later
        replacementValues.put(TEMP_PROP, replaceableString);
        String resolved = getResolvedPropertyValue(TEMP_PROP, replacementValues);
        replacementValues.remove(TEMP_PROP);

        return resolved;
    }

    /**
     * Get the property value correctly resolved.
     * 
     * @param propValue
     * @param resolvedValueCache
     * @return
     */
    private static String getResolvedPropertyValue(String key,
            Map<String, String> values) {
        Object valueObject = values.get(key);
        String value = (valueObject == null) ? Utils.EMPTY_STRING : valueObject
                .toString();

        StringBuffer sb = new StringBuffer(value);

        // First, resolve the optional blocks
        resolveOptionalBlocks(sb, values);

        // Now, resolve the rest of the remaining values
        resolvePropertyValues(sb, values);

        value = sb.toString();
        values.put(key, value);

        return value;
    }

    /**
     * Resolve the optional blocks that are found in the string buffer.
     * 
     * @param sb
     * @param values
     */
    private static void resolveOptionalBlocks(StringBuffer sb,
            Map<String, String> values) {
        int offset = 0;
        Matcher matcher = OPTIONAL_BLOCK_PATTERN.matcher(sb);
        if (matcher.find()) {
            while (matcher.find(offset)) {
                String booleanProperty = matcher.group(1);
                String blockValue = matcher.group(2);
                String booleanPropertyValue = getResolvedPropertyValue(
                        booleanProperty, values);

                String replacement = Utils.EMPTY_STRING;
                if ((booleanPropertyValue != null)
                        && (booleanPropertyValue.length() > 0)) {
                    replacement = blockValue;
                }

                // Figure out the new offset, based on the replaced
                // string length
                sb.replace(matcher.start(), matcher.end(), replacement);
                offset = matcher.start() + replacement.length();
            }
        }
    }

    /**
     * Resolve the property references that are found in the string buffer.
     * 
     * @param sb
     * @param values
     */
    private static void resolvePropertyValues(StringBuffer sb,
            Map<String, String> values) {
        int offset = 0;

        Matcher matcher = PROPS_PATTERN.matcher(sb);
        if (matcher.find()) {
            while (matcher.find(offset)) {
                String propertyName = matcher.group(1);
                String propertyValue = getResolvedPropertyValue(propertyName,
                        values);

                if (propertyValue != null) {
                    // Figure out the new offset, based on the replaced
                    // string length
                    sb.replace(matcher.start(), matcher.end(), propertyValue);
                    offset = matcher.start() + propertyValue.length();
                }
            }
        }
    }

    /**
     * Static utility functions only.
     */
    private ReplaceableParametersProcessor() {
        super();
    }
}
