/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Feng Wang (Sybase) - Enhance exported Antenna build files to support
 *                          build for multi configurations.
 *     David Marques (Motorola) - Adding exclusion patterns to wtkbuid.
 */
package org.eclipse.mtj.internal.core.build.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Preferences;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.midp.IApplicationDescriptor;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.PreferenceAccessor;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.symbol.SymbolUtils;
import org.eclipse.mtj.internal.core.util.FilteringClasspathEntryVisitor;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Tool for exporting an Antenna build file.
 * 
 * @author Craig Setera
 */
public class AntennaBuildExporter {

    /**
     * Classpath entry visitor for helping build the build.xml file
     */
    private class BuildClasspathEntryVisitor extends
            FilteringClasspathEntryVisitor {

        private Map<IJavaProject, ProjectInfo> projectInfoMap;

        /**
         * Construct a new visitor that will be responsible for creating the
         * necessary elements.
         * 
         * @param rootProject
         */
        public BuildClasspathEntryVisitor(IJavaProject rootProject) {
            projectInfoMap = new LinkedHashMap<IJavaProject, ProjectInfo>();
            createProjectInfo(rootProject, true);
        }

        /**
         * Return the map of project information.
         * 
         * @return
         */
        public Map<IJavaProject, ProjectInfo> getProjectInfoMap() {
            return projectInfoMap;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.internal.utils.AbstractClasspathEntryVisitor#visitLibraryEntry(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IProgressMonitor)
         */
        public void visitLibraryEntry(IClasspathEntry entry,
                IJavaProject javaProject, IProgressMonitor monitor)
                throws CoreException {
            ProjectInfo projectInfo = getProjectInfo(javaProject);
            Element classpathElement = projectInfo.getClasspathElement();

            IPath libLocation = getLibraryLocation(entry);
            if (libLocation != null) {
                File libFile = libLocation.toFile();

                Element pathElement = newChildElement(classpathElement,
                        ELEM_PATH);
                String relativePath = getProjectRelativeValue(javaProject,
                        projectInfo, libLocation);
                pathElement.setAttribute(ATTR_LOCATION, relativePath);
                addIncludesAndExcludes(entry, pathElement);

                if (isLibraryExported(entry)) {
                    if (libFile.isDirectory()) {
                        // Create a new zip fileset for the packaging
                        Element filesetElement = mtjBuildXmlDocument
                                .createElement(ELEM_FILESET);
                        projectInfo.addPackageFilesetElement(filesetElement);
                        filesetElement.setAttribute(ATTR_DIR, relativePath);
                    } else {
                        // Create a new zip fileset for the packaging
                        Element zipFilesetElement = mtjBuildXmlDocument
                                .createElement("zipfileset"); //$NON-NLS-1$
                        projectInfo.addPackageFilesetElement(zipFilesetElement);
                        zipFilesetElement.setAttribute("src", relativePath); //$NON-NLS-1$
                        addIncludesAndExcludes(entry, zipFilesetElement);
                    }
                }
            }
        }

        /* (non-Javadoc)
        * @see org.eclipse.mtj.core.internal.utils.FilteringClasspathEntryVisitor#visitProject(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IProgressMonitor)
        */
        public boolean visitProject(IClasspathEntry entry,
                IJavaProject javaProject, IJavaProject classpathProject,
                IProgressMonitor monitor) throws CoreException {
            // Force the project info into the map so that it
            // is held in the correct order
            boolean exported = entry.isExported();
            createProjectInfo(classpathProject, exported);

            return super.visitProject(entry, javaProject, classpathProject,
                    monitor);
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.internal.utils.AbstractClasspathEntryVisitor#visitSourceEntry(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IProgressMonitor)
         */
        public void visitSourceEntry(IClasspathEntry entry,
                IJavaProject javaProject, IProgressMonitor monitor)
                throws CoreException {
            IPath srcLocation = getSourceLocation(entry);

            if (srcLocation != null) {
                ProjectInfo projectInfo = getProjectInfo(javaProject);

                // Create the wtkbuild task call
                String buildTask = projectInfo.isExported() ? ELEM_WTKBUILD
                        : "javac"; //$NON-NLS-1$
                Element buildElement = mtjBuildXmlDocument
                        .createElement(buildTask);
                projectInfo.addWtkBuildElement(entry, buildElement);
                buildElement.setAttribute("destdir", projectInfo //$NON-NLS-1$
                        .getBuildDestination());
                buildElement.setAttribute("sourcepath", ""); //$NON-NLS-1$ //$NON-NLS-2$
                buildElement.setAttribute("encoding", "${src.encoding}"); //$NON-NLS-1$ //$NON-NLS-2$
                buildElement.setAttribute("source", "1.3"); //$NON-NLS-1$ //$NON-NLS-2$

                // Calculate the src directory relative to the specified
                // project.
                String relativePath = getProjectRelativeValue(javaProject,
                        projectInfo, srcLocation);
                buildElement.setAttribute(ATTR_SRCDIR, relativePath);

                // Add the inclusion and exclusion patterns as necessary
                addIncludesAndExcludes(entry, buildElement);

                // Add the classpath reference
                Element classpathElement = newChildElement(buildElement,
                        ELEM_CLASSPATH);
                classpathElement.setAttribute(ATTR_REFID, projectInfo
                        .getClasspathElementId());

                // Make sure to copy the non-Java resources into the package
                Element srcPackageElement = mtjBuildXmlDocument
                        .createElement(ELEM_FILESET);
                srcPackageElement.setAttribute(ATTR_DIR, relativePath);

                Element javaExclude = newChildElement(srcPackageElement,
                        "exclude"); //$NON-NLS-1$
                javaExclude.setAttribute(ATTR_NAME, "**/*.java"); //$NON-NLS-1$
                Element buildExclude = newChildElement(srcPackageElement,
                        "exclude"); //$NON-NLS-1$
                buildExclude.setAttribute(ATTR_NAME, "build/"); //$NON-NLS-1$
                addIncludesAndExcludes(entry, srcPackageElement);
                projectInfo.addPackageFilesetElement(srcPackageElement);
            } else {
                MTJLogger.log(IStatus.WARNING, NLS.bind(
                        Messages.AntennaBuildExporter_unresolved_classpath, entry));
            }
        }

        /**
         * Add the inclusion and exclusion elements as necessary.
         * 
         * @param entry
         * @param element
         */
        private void addIncludesAndExcludes(IClasspathEntry entry,
                Element element) {
            for (int i = 0; i < entry.getExclusionPatterns().length; i++) {
                IPath pattern = entry.getExclusionPatterns()[i];
                Element exclusionElement = newChildElement(element, "exclude"); //$NON-NLS-1$
                exclusionElement.setAttribute(ATTR_NAME, pattern.toString());
            }

            for (int i = 0; i < entry.getInclusionPatterns().length; i++) {
                IPath pattern = entry.getInclusionPatterns()[i];
                Element inclusionElement = newChildElement(element, "include"); //$NON-NLS-1$
                inclusionElement.setAttribute(ATTR_NAME, pattern.toString());
            }
        }

        /**
         * Create a new project information structure.
         * 
         * @param javaProject
         * @param exported
         */
        private void createProjectInfo(IJavaProject javaProject,
                boolean exported) {
            ProjectInfo info = new ProjectInfo(javaProject, exported);
            projectInfoMap.put(javaProject, info);
        }

        /**
         * Get the file system location for the specified classpath entry.
         * 
         * @param entry
         * @return
         * @throws CoreException
         */
        private IPath getLibraryLocation(IClasspathEntry entry)
                throws CoreException {
            IPath libLocation = null;

            Object resolved = Utils.getResolvedClasspathEntry(entry);
            if (resolved instanceof IResource) {
                IResource libResource = (IResource) resolved;
                libLocation = libResource.getLocation();
            } else if (resolved instanceof File) {
                libLocation = entry.getPath();
            }

            return libLocation;
        }

        /**
         * Get the holder of information for the specified project.
         * 
         * @param javaProject
         * @return
         */
        private ProjectInfo getProjectInfo(IJavaProject javaProject) {
            return (ProjectInfo) projectInfoMap.get(javaProject);
        }

        /**
         * Return the project relative path.
         * 
         * @param javaProject
         * @param projectInfo
         * @param location
         * @return
         * @throws CoreException
         */
        private String getProjectRelativeValue(IJavaProject javaProject,
                ProjectInfo projectInfo, IPath location) throws CoreException {
            StringBuffer sb = new StringBuffer();
            IPath projectPath = javaProject.getProject().getLocation();

            String relativePath = getRelativePath(projectPath, location);
            if (relativePath == null) {
                // Can't get relation to projectPath, use location as it is
                sb.append(location.toString());
            } else {
                sb.append("${").append(projectInfo.getAntProjectPropertyName()) //$NON-NLS-1$
                        .append("}").append(relativePath); //$NON-NLS-1$
            }

            return sb.toString();
        }

        /**
         * Get the file system location for the specified classpath entry.
         * 
         * @param entry
         * @return
         * @throws CoreException
         */
        private IPath getSourceLocation(IClasspathEntry entry)
                throws CoreException {
            IPath sourceLocation = null;

            Object resolved = Utils.getResolvedClasspathEntry(entry);
            if (resolved instanceof IResource) {
                IResource srcResource = (IResource) resolved;
                sourceLocation = srcResource.getLocation();
            }

            return sourceLocation;
        }
    }

    /**
     * Holder for project-specific information built during the visitation of
     * the classpath
     */
    private class ProjectInfo {
        private Element classpathElement;
        private boolean exported;
        private IJavaProject javaProject;
        private List<Element> packageFilesetElements;
        private String safeProjectName;
        private Map<IClasspathEntry, Element> wtkBuildElements;

        /**
         * Construct a new project information instance for the specified java
         * project.
         * 
         * @param javaProject
         */
        ProjectInfo(IJavaProject javaProject, boolean exported) {
            this.javaProject = javaProject;
            this.exported = exported;

            wtkBuildElements = new HashMap<IClasspathEntry, Element>();
            packageFilesetElements = new ArrayList<Element>();

            // Calculate a "safe" project name
            safeProjectName = javaProject.getElementName();
            safeProjectName = safeProjectName.replace(' ', '_');
        }

        /**
         * Add a new fileset element to the list.
         * 
         * @param element
         */
        public void addPackageFilesetElement(Element element) {
            packageFilesetElements.add(element);
        }

        /**
         * Add a new wtkbuild element to the list.
         * 
         * @param srcEntry
         * @param element
         */
        public void addWtkBuildElement(IClasspathEntry srcEntry, Element element) {
            if (!wtkBuildElements.containsKey(srcEntry)) {
                wtkBuildElements.put(srcEntry, element);
            }
        }

        /**
         * Return the Ant property name to be used for this project.
         */
        public String getAntProjectPropertyName() {
            return "project.root." + safeProjectName; //$NON-NLS-1$
        }

        /**
         * Return the Ant project property value.
         * 
         * @return
         */
        public String getAntProjectPropertyValue() {
            String relativePath = null;

            // Figure out the property value relative to the base directory
            IPath projectPath = javaProject.getProject().getLocation();
            relativePath = getRelativePath(basedirPath, projectPath);
            if (relativePath == null) {
                relativePath = projectPath.toString();
            }

            return relativePath;
        }

        /**
         * Return the destination directory to be used for class build output.
         * 
         * @return
         */
        public String getBuildDestination() {
            return exported ? PATH_BUILD_CLASSES : PATH_BUILD_CLASSES_NO_EXPORT;
        }

        /**
         * Return the classpath element.
         * 
         * @return
         */
        public Element getClasspathElement() {
            if (classpathElement == null) {
                classpathElement = mtjBuildXmlDocument.createElement(ELEM_PATH);
                classpathElement.setAttribute(ATTR_ID, getClasspathElementId());

                Element pathElement = newChildElement(classpathElement,
                        ELEM_PATH);
                pathElement.setAttribute(ATTR_LOCATION, PATH_BUILD_CLASSES);

                pathElement = newChildElement(classpathElement, ELEM_PATH);
                pathElement.setAttribute(ATTR_LOCATION,
                        PATH_BUILD_CLASSES_NO_EXPORT);
            }

            return classpathElement;
        }

        /**
         * Return the identifier of the classpath path element.
         * 
         * @return
         */
        public String getClasspathElementId() {
            return "classpath." + safeProjectName; //$NON-NLS-1$
        }

        /**
         * @return Returns the packageFilesetElements.
         */
        public List<Element> getPackageFilesetElements() {
            return packageFilesetElements;
        }

        /**
         * Gets all source folder classpath entries.
         * 
         * @return class path entries.
         */
        public Collection<IClasspathEntry> getClassPathEntries() {
        	return wtkBuildElements.keySet();
        }
        
        /**
         * @return Returns the wtkBuildElements.
         */
        public Collection<Element> getWtkBuildElements() {
            return wtkBuildElements.values();
        }

        /**
         * @return Returns the exported flag indication
         */
        public boolean isExported() {
            return exported;
        }
    }

    public static final String ATTR_EXCLUDE = "exclude"; //$NON-NLS-1$
    public static final String ATTR_BOOTCLASSPATH = "bootclasspath"; //$NON-NLS-1$
    public static final String ATTR_DEPENDS = "depends"; //$NON-NLS-1$

    public static final String ATTR_DESTDIR = "destdir"; //$NON-NLS-1$
    public static final String ATTR_DIR = "dir"; //$NON-NLS-1$
    public static final String ATTR_ID = "id"; //$NON-NLS-1$
    public static final String ATTR_JADFILE = "jadfile"; //$NON-NLS-1$
    public static final String ATTR_JARFILE = "jarfile"; //$NON-NLS-1$
    public static final String ATTR_LOCATION = "location"; //$NON-NLS-1$
    public static final String ATTR_MESSAGE = "message"; //$NON-NLS-1$
    public static final String ATTR_NAME = "name"; //$NON-NLS-1$
    public static final String ATTR_PRCFILE = "prcfile"; //$NON-NLS-1$
    public static final String ATTR_REFID = "refid"; //$NON-NLS-1$
    public static final String ATTR_SRCDIR = "srcdir"; //$NON-NLS-1$
    public static final String ATTR_TARGET = "target"; //$NON-NLS-1$
    public static final String ATTR_TOFILE = "tofile"; //$NON-NLS-1$
    public static final String ATTR_VERBOSE = "verbose"; //$NON-NLS-1$
    public static final String BUILD_XML_FILE_NAME = "build.xml"; //$NON-NLS-1$
    public static final String ELEM_ANTCALL = "antcall"; //$NON-NLS-1$
    public static final String ELEM_CLASSPATH = "classpath"; //$NON-NLS-1$
    public static final String ELEM_COPY = "copy"; //$NON-NLS-1$
    public static final String ELEM_ECHO = "echo"; //$NON-NLS-1$
    public static final String ELEM_FILESET = "fileset"; //$NON-NLS-1$
    public static final String ELEM_MKDIR = "mkdir"; //$NON-NLS-1$
    public static final String ELEM_PATH = "path"; //$NON-NLS-1$
    public static final String ELEM_TARGET = ATTR_TARGET;
    public static final String ELEM_WTKBUILD = "wtkbuild"; //$NON-NLS-1$
    public static final String ELEM_WTKMAKEPRC = "wtkmakeprc"; //$NON-NLS-1$
    public static final String ELEM_WTKOBFUSCATE = "wtkobfuscate"; //$NON-NLS-1$
    public static final String ELEM_WTKPACKAGE = "wtkpackage"; //$NON-NLS-1$
    public static final String ELEM_WTKPREPROCESS = "wtkpreprocess"; //$NON-NLS-1$
    public static final String ELEM_WTKPREVERIFY = "wtkpreverify"; //$NON-NLS-1$
    public static final String ELEM_WTKRUN = "wtkrun"; //$NON-NLS-1$
    public static final String MTJ_BUILD_XML_FILE_NAME = "mtj-build.xml"; //$NON-NLS-1$

    public static final String TASK_MTJ_BUILD = "-mtj-build"; //$NON-NLS-1$
    public static final String TASK_MTJ_INITIALIZE = "-mtj-initialize"; //$NON-NLS-1$
    private static final String NO_EXPORT = "_no_export"; //$NON-NLS-1$

    private static final String PATH_BUILD_CLASSES = "${path.build.classes}"; //$NON-NLS-1$

    private static final String PATH_BUILD_CLASSES_NO_EXPORT = "${path.build.classes}/" //$NON-NLS-1$
            + NO_EXPORT;

    private static final Pattern SUBSTITUTION_PATTERN = Pattern
            .compile("\\@\\{(.+?)\\}"); //$NON-NLS-1$

    private IPath basedirPath;
    private Properties buildProperties;
    private IJavaProject javaProject;
    private IMidletSuiteProject midletSuite;
    private Document mtjBuildXmlDocument;
    private String projectName;

    /**
     * Construct a new exporter for the specified MIDlet suite project.
     * 
     * @param midletSuite
     */
    public AntennaBuildExporter(IMidletSuiteProject midletSuite) {
        super();
        this.midletSuite = midletSuite;
        this.javaProject = midletSuite.getJavaProject();

        basedirPath = javaProject.getProject().getLocation();
        projectName = midletSuite.getProject().getName();
    }

    /**
     * Do the export for the MIDlet suite.
     * 
     * @throws CoreException
     */
    public void doExport(IProgressMonitor monitor) throws CoreException,
            AntennaExportException {
        // Validate the environment is ok.
        validateEnvironment(monitor);

        try {
            // Read in the templates
            buildProperties = createInitialProperties();
            mtjBuildXmlDocument = readMTJBuildTemplate();

            // Traverse the classpath and update the results along the way
            BuildClasspathEntryVisitor visitor = traverseClasspath(monitor);
            updateMTJBuildXml(visitor.getProjectInfoMap());

            // Write out the results
            exportBuildXml(monitor);
            writeBuildProperties(monitor);
            writeMTJBuildXml(monitor);
            exportSymbolFileForConfigs(monitor);

            // Refresh so these files show in the workbench.
            javaProject.getProject().refreshLocal(IResource.DEPTH_ONE, monitor);

        } catch (Exception e) {
            if (e instanceof CoreException) {
                throw (CoreException) e;
            } else {
                MTJStatusHandler.throwCoreException(IStatus.ERROR, -999, e);
            }
        }
    }

    /**
     * Add the obfuscation-related properties.
     * 
     * @param props
     */
    private void addObfuscationProperties(Properties props) {
        File proguardFile = MTJCore.getProguardJarFile();
        boolean obfuscate = ((proguardFile != null) && proguardFile.exists());

        props.setProperty("flag.should.obfuscate", Boolean.toString(obfuscate)); //$NON-NLS-1$

        if (obfuscate) {
            File proguardHome = proguardFile.getParentFile().getParentFile();
            props.setProperty("wtk.proguard.home", proguardHome.toString()); //$NON-NLS-1$
        }
    }

    /**
     * Add the appropriate J2ME version properties to match the JAD versions.
     * 
     * @param props
     */
    private void addVersionProperties(Properties props) {
        IApplicationDescriptor descriptor = midletSuite
                .getApplicationDescriptor();
        Properties manifestProperties = descriptor.getManifestProperties();

        String versionString = manifestProperties
                .getProperty(IJADConstants.JAD_MICROEDITION_CONFIG);
        if (versionString != null) {
            int index = versionString.indexOf('-');
            versionString = (index == -1) ? versionString : versionString
                    .substring(index + 1);
            props.setProperty("wtk.cldc.version", versionString); //$NON-NLS-1$
        }

        versionString = manifestProperties
                .getProperty(IJADConstants.JAD_MICROEDITION_PROFILE);
        if (versionString != null) {
            int index = versionString.indexOf('-');
            versionString = (index == -1) ? versionString : versionString
                    .substring(index + 1);
            props.setProperty("wtk.midp.version", versionString); //$NON-NLS-1$
        }
    }

    /**
     * Add a new argument to the WTK obfuscate element.
     * 
     * @param wktobfuscateElement
     * @param argument
     */
    private void addWtkObfuscateArgument(Element wktobfuscateElement,
            String argument) {
        Element argumentElement = newChildElement(wktobfuscateElement,
                "argument"); //$NON-NLS-1$
        argumentElement.setAttribute("value", argument); //$NON-NLS-1$
    }

    /**
     * Create the initial properties object.
     * 
     * @return
     */
    private Properties createInitialProperties() {
        Properties props = new Properties();
        props.setProperty("midlet.name", replaceSpace(javaProject.getProject() //$NON-NLS-1$
                .getName()));
        props.setProperty("jad.name", //$NON-NLS-1$
                replaceSpace(midletSuite.getJadFileName()));
        props.setProperty("descriptor.name", midletSuite //$NON-NLS-1$
                .getApplicationDescriptorFile().getName());
        props.setProperty("path.build", "${basedir}/build"); //$NON-NLS-1$ //$NON-NLS-2$
        props.setProperty("path.build.classes", "${basedir}/build/classes"); //$NON-NLS-1$ //$NON-NLS-2$
        props.setProperty("path.preprocess.output", //$NON-NLS-1$
                "${basedir}/preprocessOutput"); //$NON-NLS-1$

        setClassesFolderForConfigs(props);

        setPreprocessOutputFolderForConfigs(props);

        setDeployFolderForConfigs(props);

        // Antenna stuff
        Preferences prefs = MTJCore.getMTJCore().getPluginPreferences();
        props.setProperty("wtk.home", prefs //$NON-NLS-1$
                .getString(IMTJCoreConstants.PREF_WTK_ROOT));
        props.setProperty("path.antenna.jar", prefs //$NON-NLS-1$
                .getString(IMTJCoreConstants.PREF_ANTENNA_JAR));
        boolean autoVersion = PreferenceAccessor.instance
                .getAutoversionPackage(midletSuite.getProject());
        props.setProperty("flag.autoversion", Boolean.toString(autoVersion)); //$NON-NLS-1$
        props.setProperty("flag.preverify", "true"); //$NON-NLS-1$ //$NON-NLS-2$

        // Set default encoding to UTF-8
        props.setProperty("src.encoding", "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$

        setBootclasspathForConfigs(props);

        // Properties necessary for running in the emulator
        props.setProperty("run.device.name", "DefaultColorPhone"); //$NON-NLS-1$ //$NON-NLS-2$
        props.setProperty("run.trace.options", ""); //$NON-NLS-1$ //$NON-NLS-2$

        // Add some default properties based on various things
        addObfuscationProperties(props);
        addVersionProperties(props);

        return props;
    }

    /**
     * Export the build.xml file.
     * 
     * @param monitor
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws TransformerException
     * @throws CoreException
     */
    private void exportBuildXml(IProgressMonitor monitor)
            throws ParserConfigurationException, SAXException,
            TransformerException, IOException, CoreException {
        // Check to make sure that we don't overwrite a user's build.xml file
        // with our template file.
        // TODO What do we do, if anything, when the build.xml file already
        // exists?
        File buildXmlFile = getMidletSuiteFile("build.xml", monitor); //$NON-NLS-1$
        if (!buildXmlFile.exists()) {
            // Template values
            Properties props = new Properties();
            props.setProperty("mtj.version", MTJCore.getMTJCoreVersion()); //$NON-NLS-1$
            props.setProperty("date", (new Date()).toString()); //$NON-NLS-1$
            props.setProperty("project.name", projectName); //$NON-NLS-1$

            InputStream is = getClass()
                    .getResourceAsStream("buildtemplate.xml"); //$NON-NLS-1$
            if (is != null) {
                // Read the template and do the replacements
                StringBuffer sb = readBuildXmlTemplate(is);
                replaceTemplateValues(sb, props);

                // Write the results
                FileOutputStream fos = new FileOutputStream(buildXmlFile);
                OutputStreamWriter writer = new OutputStreamWriter(fos, "UTF-8"); //$NON-NLS-1$
                writer.write(sb.toString());
                writer.close();
            }
        }
    }

    /**
     * Antenna wtkpreprocess task need a symbol file(which contains symbols) to
     * do preprocess for each configuration.<br>
     * Note: Should not use
     * {@link Properties#store(java.io.OutputStream, String)} to write symbol
     * file. Because any line start by "#" will cause error for Antenna
     * preprocessor parse symbols.
     * 
     * @throws IOException
     */
    private void exportSymbolFileForConfigs(IProgressMonitor monitor)
            throws IOException {
        for (MTJRuntime config : midletSuite.getRuntimeList()) {

            String safeConfigName = replaceSpace(config.getName());
            File symbolFile = getMidletSuiteFile(safeConfigName + ".symbols", //$NON-NLS-1$
                    monitor);
            FileOutputStream fos = new FileOutputStream(symbolFile);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos,
                    "8859_1")); //$NON-NLS-1$
            // write symbols contained by configuration
            for (ISymbol s : config.getSymbolSetForPreprocessing().getSymbols()) {
                bw.write(s.getName() + "=" + s.getSafeValue()); //$NON-NLS-1$
                bw.newLine();
            }
            // write symbols from EnabledSymbolDefinitionSet which come from
            // workspace scope
            try {
                List<ISymbolSet> sets = config
                        .getWorkspaceScopeSymbolSets();
                for (ISymbolSet symbols : sets) {
                    for (ISymbol s : symbols.getSymbols()) {
                        bw.write(s.getName() + "=" //$NON-NLS-1$
                                + SymbolUtils.getSafeSymbolValue(s.getValue()));
                        bw.newLine();
                    }
                }
            } catch (Exception e) {
                MTJLogger.log(IStatus.ERROR, e);
            }

            bw.flush();
            fos.close();
        }
    }

    /**
     * Return the specified element within the specified parent. If more
     * elements exist with that name, the first will be returned. If not found,
     * <code>null</code> will be returned.
     * 
     * @param parentElement
     * @param elementName
     * @return
     */
    private Element findElement(Element parentElement, String elementName) {
        Element element = null;

        NodeList elements = parentElement.getElementsByTagName(elementName);
        if (elements.getLength() > 0) {
            element = (Element) elements.item(0);
        }

        return element;
    }

    private String getCompileDestDirForConfig(String destDir, String configName) {
        return destDir.replaceFirst("}", "." + configName + "}"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
    }

    /**
     * Return a File reference to a file with the specified name in the MIDlet
     * suite project.
     * 
     * @param filename
     * @param monitor
     * @return
     * @throws CoreException
     */
    private File getMidletSuiteFile(String filename, IProgressMonitor monitor) {
        IFile file = midletSuite.getProject().getFile(filename);
        return file.getLocation().toFile();
    }

    /**
     * Get the options to be specified when calling Proguard for obfuscation.
     * 
     * @return
     */
    private String getProguardOptions() {
        IProject project = midletSuite.getProject();
        PreferenceAccessor obfuscationPrefs = PreferenceAccessor.instance;
        String specifiedOptions = obfuscationPrefs
                .getSpecifiedProguardOptions(project);
        boolean useSpecified = obfuscationPrefs
                .isUseSpecifiedProguardOptions(project);

        return useSpecified ? specifiedOptions : obfuscationPrefs
                .getDefaultProguardOptions();
    }

    /**
     * Return the relative path.
     * 
     * @param basePath the path that acts as the base for comparison
     * @param tgtPath the path being compared to the base path
     * @return relative path, or null if relation not possible
     */
    private String getRelativePath(IPath basePath, IPath tgtPath) {
        String path = null;

        // Find the common path prefix
        int matchingSegments = tgtPath.matchingFirstSegments(basePath);

        String baseDevice = basePath.getDevice();
        String tgtDevice = tgtPath.getDevice();
        boolean bothNull = ((baseDevice == null) && (tgtDevice == null));

        if (bothNull || baseDevice.equals(tgtDevice)) {
            // Step up the directory tree
            StringBuffer relativePath = new StringBuffer();
            int upSteps = basedirPath.segmentCount() - matchingSegments;
            for (int i = 0; i < upSteps; i++) {
                relativePath.append("/.."); //$NON-NLS-1$
            }

            // Tack on the src folder segments
            tgtPath = tgtPath.removeFirstSegments(matchingSegments);
            String[] segments = tgtPath.segments();
            for (String segment : segments) {
                relativePath.append("/").append(segment); //$NON-NLS-1$
            }

            path = relativePath.toString();
        }

        return path;
    }

    /**
     * Return the root of the first located Sun wireless toolkit for use in
     * setting the appropriate Antenna property or <code>null</code> if one
     * cannot be found.
     * 
     * @return
     */
    private File getWTKRoot() {
        Preferences prefs = MTJCore.getMTJCore().getPluginPreferences();
        return new File(prefs.getString(IMTJCoreConstants.PREF_WTK_ROOT));
    }

    /**
     * insert build tasks into mtj build xml doc.
     * 
     * @param projectInfoMap
     */
    private void insertBuildTasks(Map<IJavaProject, ProjectInfo> projectInfoMap) {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        Element insertBuildBeforeThisElement = findElement(documentElement,
                "insert_build_task_for_configs"); //$NON-NLS-1$
        NodeList tasks = mtjBuildXmlDocument.getElementsByTagName(ELEM_TARGET);
        Element mtjBuildElem = null;
        for (int i = 0; i < tasks.getLength(); i++) {
            Element task = (Element) tasks.item(i);
            if (TASK_MTJ_BUILD.equals(task.getAttribute(ATTR_NAME))) {
                mtjBuildElem = task;
                break;
            }
        }
        for (MTJRuntime config : midletSuite.getRuntimeList()) {
            String configName = replaceSpace(config.getName());
            Element buildConfigTask = mtjBuildXmlDocument
                    .createElement(ELEM_TARGET);
            buildConfigTask.setAttribute(ATTR_DEPENDS, TASK_MTJ_INITIALIZE);
            buildConfigTask.setAttribute(ATTR_NAME, "-mtj-build-" + configName); //$NON-NLS-1$
            Element echo = XMLUtils.createChild(buildConfigTask, ELEM_ECHO);
            echo.setAttribute(ATTR_MESSAGE, NLS.bind("", configName)); //$NON-NLS-1$
            Iterator<Map.Entry<IJavaProject, ProjectInfo>> iterator = projectInfoMap
                    .entrySet().iterator();
            String preprocessOutput = "${path.preprocess.output." + configName //$NON-NLS-1$
                    + "}"; //$NON-NLS-1$
            // String destDir = "${path.build.classes." + configName + "}";
            while (iterator.hasNext()) {
                Map.Entry<IJavaProject, ProjectInfo> entry = (Map.Entry<IJavaProject, ProjectInfo>) iterator
                        .next();
                ProjectInfo info = (ProjectInfo) entry.getValue();
                String destDirWihoutConfigName = ""; //$NON-NLS-1$
                if (info.javaProject == this.javaProject) {
                    for (Element element : info.getWtkBuildElements()) {
                        // Element element = (Element) elements.next();
                        String srcDir = element.getAttribute(ATTR_SRCDIR);
                        Element preprocessTask = XMLUtils.createChild(
                                buildConfigTask, ELEM_WTKPREPROCESS);
                        preprocessTask.setAttribute(ATTR_SRCDIR, srcDir);
                        preprocessTask.setAttribute(ATTR_VERBOSE, "true"); //$NON-NLS-1$
                        preprocessTask.setAttribute(ATTR_DESTDIR,
                                preprocessOutput);
                        preprocessTask.setAttribute("printsymbols", "true"); //$NON-NLS-1$ //$NON-NLS-2$
                        preprocessTask.setAttribute("savesymbols", //$NON-NLS-1$
                                preprocessOutput + "/usedSymbols.txt"); //$NON-NLS-1$
                        preprocessTask.setAttribute("debuglevel", "info"); //$NON-NLS-1$ //$NON-NLS-2$

                        Element symbolFile = XMLUtils.createChild(
                                preprocessTask, "symbols_file"); //$NON-NLS-1$
                        symbolFile.setAttribute(ATTR_NAME, "${project.root." //$NON-NLS-1$
                                + replaceSpace(midletSuite.getProject()
                                        .getName()) + "}/" + configName //$NON-NLS-1$
                                + ".symbols"); //$NON-NLS-1$
                        destDirWihoutConfigName = element
                                .getAttribute(ATTR_DESTDIR);

                    }
                    Element wtkBuildRootProject = XMLUtils.createChild(
                            buildConfigTask, ELEM_WTKBUILD);
                    wtkBuildRootProject.setAttribute(ATTR_BOOTCLASSPATH,
                            "${bootclasspath." + configName + "}"); //$NON-NLS-1$ //$NON-NLS-2$
                    wtkBuildRootProject.setAttribute(ATTR_SRCDIR,
                            preprocessOutput);
                    wtkBuildRootProject.setAttribute(ATTR_DESTDIR,
                            getCompileDestDirForConfig(destDirWihoutConfigName,
                                    configName));
                    wtkBuildRootProject.setAttribute("encoding", //$NON-NLS-1$
                            "${src.encoding}"); //$NON-NLS-1$
                    wtkBuildRootProject.setAttribute("source", "1.3"); //$NON-NLS-1$ //$NON-NLS-2$
                    wtkBuildRootProject.setAttribute("sourcepath", ""); //$NON-NLS-1$ //$NON-NLS-2$

                    Element classpath = XMLUtils.createChild(
                            wtkBuildRootProject, ELEM_CLASSPATH);
                    classpath.setAttribute(ATTR_REFID, info
                            .getClasspathElementId()
                            + "." + configName); //$NON-NLS-1$
                    
                    // Adds the exclusion patterns to the wtkbuild task
                    for (IClasspathEntry classpathEntry : info.getClassPathEntries()) {
						IPath[] patterns = classpathEntry.getExclusionPatterns();
						for (IPath pattern : patterns) {
							Element excludes = XMLUtils.createChild(wtkBuildRootProject, ATTR_EXCLUDE);
							excludes.setAttribute(ATTR_NAME, pattern.toString());
						}
					}
                    
                } else {
                    for (Element element : info.getWtkBuildElements()) {
                        Element clonedElement = (Element) element
                                .cloneNode(true);
                        destDirWihoutConfigName = clonedElement
                                .getAttribute(ATTR_DESTDIR);
                        clonedElement.setAttribute(ATTR_DESTDIR,
                                getCompileDestDirForConfig(
                                        destDirWihoutConfigName, configName));
                        Element classpath = XMLUtils
                                .getFirstElementWithTagName(clonedElement,
                                        ELEM_CLASSPATH);
                        classpath.setAttribute(ATTR_REFID, info
                                .getClasspathElementId()
                                + "." + configName); //$NON-NLS-1$
                        buildConfigTask.insertBefore(clonedElement, echo
                                .getNextSibling());
                    }
                }

            }
            insertBuildBeforeThisElement.getParentNode().insertBefore(
                    buildConfigTask, insertBuildBeforeThisElement);

            if (mtjBuildElem != null) {
                Element antCall = newChildElement(mtjBuildElem, ELEM_ANTCALL);
                antCall.setAttribute(ATTR_TARGET, buildConfigTask
                        .getAttribute(ATTR_NAME));
            }
        }

        insertBuildBeforeThisElement.getParentNode().removeChild(
                insertBuildBeforeThisElement);
    }

    /**
     * Insert the comment text before the specified element.
     * 
     * @param element
     * @param commentString
     */
    private void insertCommentBefore(Element element, String commentString) {
        Document document = element.getOwnerDocument();

        Comment comment = document.createComment(commentString);
        document.insertBefore(comment, element);
    }

    /**
     * insert wtkpackage tasks into mtj build xml doc.
     * 
     * @param projectInfoMap
     */
    private void insertPackageTasks(
            Map<IJavaProject, ProjectInfo> projectInfoMap) {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        Element wtkPackageElement = findElement(documentElement,
                ELEM_WTKPACKAGE);

        Iterator<Map.Entry<IJavaProject, ProjectInfo>> iterator = projectInfoMap
                .entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<IJavaProject, ProjectInfo> entry = (Map.Entry<IJavaProject, ProjectInfo>) iterator
                    .next();
            ProjectInfo info = (ProjectInfo) entry.getValue();
            // Insert the fileset definitions for packaging from this project
            if (info.isExported()) {
                // Add the classpath reference
                Element classpathElement = newChildElement(wtkPackageElement,
                        ELEM_CLASSPATH);
                classpathElement.setAttribute(ATTR_REFID, info
                        .getClasspathElementId());

                Iterator<Element> filesetElements = info
                        .getPackageFilesetElements().iterator();
                while (filesetElements.hasNext()) {
                    Element element = (Element) filesetElements.next();
                    wtkPackageElement.appendChild(element);
                }
            }
        }
        Element packageTask = (Element) wtkPackageElement.getParentNode();
        for (MTJRuntime config : midletSuite.getRuntimeList()) {
            String configName = replaceSpace(config.getName());
            String buildTaskName = "-mtj-build-" + configName; //$NON-NLS-1$
            Element packConfigElem = (Element) packageTask.cloneNode(true);
            // change attributes
            packConfigElem.setAttribute(ATTR_DEPENDS, buildTaskName);
            packConfigElem.setAttribute(ATTR_NAME, "-mtj-internal-package-" //$NON-NLS-1$
                    + configName);
            // change fileset with dir = ${path.build.classes}
            NodeList filesets = packConfigElem
                    .getElementsByTagName(ELEM_FILESET);
            for (int i = 0; i < filesets.getLength(); i++) {
                Element fileset = (Element) filesets.item(i);
                if ("${path.build.classes}".equals(fileset //$NON-NLS-1$
                        .getAttribute(ATTR_DIR))) {
                    fileset.setAttribute(ATTR_DIR, "${path.build.classes." //$NON-NLS-1$
                            + configName + "}"); //$NON-NLS-1$
                    break;
                }
            }
            // change classpath
            NodeList classPathes = packConfigElem
                    .getElementsByTagName(ELEM_CLASSPATH);
            for (int i = 0; i < classPathes.getLength(); i++) {
                Element classpath = (Element) classPathes.item(i);
                String oldRefid = classpath.getAttribute(ATTR_REFID);
                String newRefid = oldRefid + "." + configName; //$NON-NLS-1$
                classpath.setAttribute(ATTR_REFID, newRefid);
            }
            // change copy task
            Element copyTask = XMLUtils.getFirstElementWithTagName(
                    packConfigElem, ELEM_COPY);
            String tofile = "${path.build.output." + configName //$NON-NLS-1$
                    + "}/${jad.name}"; //$NON-NLS-1$
            copyTask.setAttribute(ATTR_TOFILE, tofile);
            // change wtkpackage task
            Element wtkpackage = XMLUtils.getFirstElementWithTagName(
                    packConfigElem, ELEM_WTKPACKAGE);
            String jadfile = "${path.build.output." + configName //$NON-NLS-1$
                    + "}/${midlet.name}.jad"; //$NON-NLS-1$
            String jarfile = "${path.build.output." + configName //$NON-NLS-1$
                    + "}/${midlet.name}.jar"; //$NON-NLS-1$
            wtkpackage.setAttribute(ATTR_JADFILE, jadfile);
            wtkpackage.setAttribute(ATTR_JARFILE, jarfile);
            // change antcall task
            Element antcall = XMLUtils.getFirstElementWithTagName(
                    packConfigElem, ELEM_ANTCALL);
            antcall.setAttribute(ATTR_TARGET, "-mtj-obfuscate-" + configName); //$NON-NLS-1$

            // appent packConfigElem to mtjBuildXmlDocument
            packageTask.getParentNode().insertBefore(packConfigElem,
                    packageTask.getNextSibling());
        }
        // add package antcall to root package task
        Element newRootPackageTask = (Element) packageTask.cloneNode(false);
        for (MTJRuntime config : midletSuite.getRuntimeList()) {

            String configName = replaceSpace(config.getName());
            String packageConfigTaskName = "-mtj-internal-package-" //$NON-NLS-1$
                    + configName;
            Element antcallPack = newChildElement(newRootPackageTask,
                    ELEM_ANTCALL);
            antcallPack.setAttribute(ATTR_TARGET, packageConfigTaskName);
        }
        // replace root package task
        packageTask.getParentNode().replaceChild(newRootPackageTask,
                packageTask);

    }

    /**
     * Insert path elements into the exported mtj-build xml document.
     * 
     * @param pathElement
     * @param info
     */
    private void insertPathElements(
            Map<IJavaProject, ProjectInfo> projectInfoMap) {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        // must retrieve path element before insert any new path element.
        Element pathElement = findElement(documentElement, ELEM_PATH);

        Iterator<Map.Entry<IJavaProject, ProjectInfo>> iterator = projectInfoMap
                .entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<IJavaProject, ProjectInfo> entry = (Map.Entry<IJavaProject, ProjectInfo>) iterator
                    .next();
            ProjectInfo info = (ProjectInfo) entry.getValue();
            String pathID = info.getClasspathElementId();
            for (MTJRuntime config : midletSuite.getRuntimeList()) {
                String configName = replaceSpace(config.getName());
                Element classPathElem = info.getClasspathElement();
                Element cpElemForConfig = (Element) classPathElem
                        .cloneNode(true);
                cpElemForConfig
                        .setAttribute(ATTR_ID, pathID + "." + configName); //$NON-NLS-1$

                String buildPathForConfig = "${path.build.classes." //$NON-NLS-1$
                        + configName + "}"; //$NON-NLS-1$
                NodeList pathes = cpElemForConfig
                        .getElementsByTagName(ELEM_PATH);
                for (int i = 0; i < pathes.getLength(); i++) {
                    Element currentPathElem = (Element) pathes.item(i);
                    if (PATH_BUILD_CLASSES.equals(currentPathElem
                            .getAttribute(ATTR_LOCATION))) {
                        currentPathElem.setAttribute(ATTR_LOCATION,
                                buildPathForConfig);
                    } else if (PATH_BUILD_CLASSES_NO_EXPORT
                            .equals(currentPathElem.getAttribute(ATTR_LOCATION))) {
                        currentPathElem.setAttribute(ATTR_LOCATION,
                                buildPathForConfig + "/_no_export"); //$NON-NLS-1$
                    }
                }
                pathElement.getParentNode().insertBefore(cpElemForConfig,
                        pathElement);
            }
        }

        pathElement.getParentNode().removeChild(pathElement);
    }

    /**
     * Insert wtkobfuscate tasks into mtj build xml doc.
     */
    private void insertWtkobfuscateTasks() {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        Element wtkobfuscateElement = findElement(documentElement,
                ELEM_WTKOBFUSCATE);
        updateWtkObfuscateElement(wtkobfuscateElement);
        Element rootObfuscateElement = (Element) wtkobfuscateElement
                .getParentNode();
        for (MTJRuntime config : midletSuite.getRuntimeList()) {
            String configName = replaceSpace(config.getName());
            Element obfuscateConfigElem = (Element) rootObfuscateElement
                    .cloneNode(true);
            obfuscateConfigElem.setAttribute(ATTR_NAME, "-mtj-obfuscate-" //$NON-NLS-1$
                    + configName);

            Element wtkobfuscate = XMLUtils.getFirstElementWithTagName(
                    obfuscateConfigElem, ELEM_WTKOBFUSCATE);
            String jadfile = "${path.build.output." + configName //$NON-NLS-1$
                    + "}/${midlet.name}.jad"; //$NON-NLS-1$
            String jarfile = "${path.build.output." + configName //$NON-NLS-1$
                    + "}/${midlet.name}.jar"; //$NON-NLS-1$
            wtkobfuscate.setAttribute(ATTR_JADFILE, jadfile);
            wtkobfuscate.setAttribute(ATTR_JARFILE, jarfile);

            Element wtkpreverify = XMLUtils.getFirstElementWithTagName(
                    obfuscateConfigElem, ELEM_WTKPREVERIFY);
            wtkpreverify.setAttribute(ATTR_JADFILE, jadfile);
            wtkpreverify.setAttribute(ATTR_JARFILE, jarfile);

            // append obfuscateConfigElem to mtjBuildXmlDocument
            rootObfuscateElement.getParentNode().insertBefore(
                    obfuscateConfigElem, rootObfuscateElement.getNextSibling());
        }
        // add obfuscate allcall to rootObfuscateElement
        Element newRootObfuscateElement = (Element) rootObfuscateElement
                .cloneNode(false);
        for (MTJRuntime config : midletSuite.getRuntimeList()) {
            String configName = replaceSpace(config.getName());
            String obfuscateConfigTaksName = "-mtj-obfuscate-" + configName; //$NON-NLS-1$

            Element antcallObfuscate = newChildElement(newRootObfuscateElement,
                    ELEM_ANTCALL);
            antcallObfuscate.setAttribute(ATTR_TARGET, obfuscateConfigTaksName);
        }
        // replace root package task
        rootObfuscateElement.getParentNode().replaceChild(
                newRootObfuscateElement, rootObfuscateElement);
    }

    /**
     * Create and return a new child element under the specified parent element.
     * 
     * @param parentElement
     * @param name
     * @return
     */
    private Element newChildElement(Element parentElement, String name) {
        Element element = parentElement.getOwnerDocument().createElement(name);
        parentElement.appendChild(element);

        return element;
    }

    /**
     * Read the build.xml template file.
     * 
     * @param is
     * @return
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    private StringBuffer readBuildXmlTemplate(InputStream is)
            throws UnsupportedEncodingException, IOException {
        // Read in the template
        int charsRead;
        char[] buffer = new char[1024];

        Reader reader = new InputStreamReader(is, "UTF-8"); //$NON-NLS-1$
        StringBuffer sb = new StringBuffer();
        while ((charsRead = reader.read(buffer)) != -1) {
            sb.append(buffer, 0, charsRead);
        }

        is.close();
        return sb;
    }

    /**
     * Read the template MTJ build file.
     * 
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    private Document readMTJBuildTemplate()
            throws ParserConfigurationException, SAXException, IOException {
        Document document = null;

        InputStream is = getClass().getResourceAsStream("mtj-build.xml"); //$NON-NLS-1$
        if (is == null) {
            throw new IOException(Messages.AntennaBuildExporter_template_notfound);
        } else {
            // Read the document
            document = XMLUtils.readDocument(is);

            // Alter the "project" element's name attribute
            Element projectElement = document.getDocumentElement();
            projectElement.setAttribute(ATTR_NAME, "mtj-" + projectName); //$NON-NLS-1$

            // Add some warning comments
            StringBuffer comment = new StringBuffer();
            comment.append(NLS.bind(Messages.AntennaBuildExporter_warning_comment, new Date()));
            insertCommentBefore(projectElement, comment.toString());
        }

        return document;
    }

    /**
     * Replace space with underscore.
     * 
     * @param string
     * @return
     */
    private String replaceSpace(String string) {
        if (string == null) {
            return ""; //$NON-NLS-1$
        }
        return string.replace(' ', '_');
    }

    /**
     * Replace the build xml template values.
     * 
     * @param sb
     * @param props
     */
    private void replaceTemplateValues(StringBuffer sb, Properties props) {
        // Replace template values
        int offset = 0;
        Matcher matcher = SUBSTITUTION_PATTERN.matcher(sb);

        while (matcher.find(offset)) {
            String referencedValue = matcher.group(1);
            String resolvedValue = props.getProperty(referencedValue,
                    referencedValue);
            sb.replace(matcher.start(), matcher.end(), resolvedValue);

            // Figure out the new offset, based on the replaced
            // string length
            offset = matcher.start() + resolvedValue.length();
        }
    }

    /**
     * Each configuration should have its own bootclasspath.
     * 
     * @param props
     */
    private void setBootclasspathForConfigs(Properties props) {
        for (MTJRuntime config : midletSuite.getRuntimeList()) {
            props.setProperty(
                    "bootclasspath." + replaceSpace(config.getName()), config //$NON-NLS-1$
                            .getDevice().getClasspath().toString());
        }
    }

    /**
     * Each configuration should have its own build output folder.
     * 
     * @param props
     */
    private void setClassesFolderForConfigs(Properties props) {
        for (MTJRuntime config : midletSuite.getRuntimeList()) {
            String safeConfigName = replaceSpace(config.getName());
            props.setProperty("path.build.classes." + safeConfigName, //$NON-NLS-1$
                    "${basedir}/build/classes/" + safeConfigName); //$NON-NLS-1$
        }
    }

    /**
     * Each configuration should have its own deployment folder.
     * 
     * @param props
     */
    private void setDeployFolderForConfigs(Properties props) {
        for (MTJRuntime config : midletSuite.getRuntimeList()) {
            String safeConfigName = replaceSpace(config.getName());
            props.setProperty("path.build.output." + safeConfigName, //$NON-NLS-1$
                    "${basedir}/" + MTJCore.getDeploymentDirectoryName() + "/" //$NON-NLS-1$ //$NON-NLS-2$
                            + safeConfigName);
        }
    }

    /**
     * Each configuration should have its own prepross output folder.
     * 
     * @param props
     */
    private void setPreprocessOutputFolderForConfigs(Properties props) {
        for (MTJRuntime config : midletSuite.getRuntimeList()) {
            String safeConfigName = replaceSpace(config.getName());
            props.setProperty("path.preprocess.output." + safeConfigName, //$NON-NLS-1$
                    "${basedir}/preprocessOutput/" + safeConfigName); //$NON-NLS-1$
        }
    }

    /**
     * Traverse the classpath and update the build information along the way.
     * 
     * @throws CoreException
     */
    private BuildClasspathEntryVisitor traverseClasspath(
            IProgressMonitor monitor) throws CoreException {
        // Use a classpath visitor to build up the build information
        BuildClasspathEntryVisitor visitor = new BuildClasspathEntryVisitor(
                javaProject);
        visitor.getRunner(true).run(javaProject, visitor, monitor);

        return visitor;
    }

    /**
     * Insert project.root.&lt;PROJECT_NAME&gt; property entries into build
     * properties.
     * 
     * @param projectInfoMap
     */
    private void updateBuildProperties(
            Map<IJavaProject, ProjectInfo> projectInfoMap) {
        Iterator<Map.Entry<IJavaProject, ProjectInfo>> iterator = projectInfoMap
                .entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<IJavaProject, ProjectInfo> entry = (Map.Entry<IJavaProject, ProjectInfo>) iterator
                    .next();
            ProjectInfo info = (ProjectInfo) entry.getValue();

            // Set the property for the root of the project
            String propertyValue = info.getAntProjectPropertyValue();
            propertyValue = (propertyValue.length() == 0) ? "${basedir}" //$NON-NLS-1$
                    : "${basedir}" + propertyValue; //$NON-NLS-1$
            buildProperties.setProperty(info.getAntProjectPropertyName(),
                    propertyValue);
        }
    }

    /**
     * Update the mtj build file based on the classpath information collected
     * during traversal.
     * 
     * @param projectInfoMap
     */
    private void updateMTJBuildXml(Map<IJavaProject, ProjectInfo> projectInfoMap) {

        // updata "-mtj-initialize" target
        updateMtjInitializeTarget();
        // update wtkmakeprc target
        updateWtkmakeprcTarget();
        // Insert the project's wtkbuild calls
        insertBuildTasks(projectInfoMap);
        // Insert package tasks
        insertPackageTasks(projectInfoMap);
        // Insert this project's classpath path definition
        insertPathElements(projectInfoMap);
        // update build properties
        updateBuildProperties(projectInfoMap);
        // Update the wtkobfuscate parameters
        insertWtkobfuscateTasks();
        // update wtkrun target
        updateWtkrunTarget();

    }

    /**
     * Update "-mtj-initialize" target in mtj build xml doc.
     */
    private void updateMtjInitializeTarget() {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        NodeList targets = documentElement.getElementsByTagName(ELEM_TARGET);
        for (int i = 0; i < targets.getLength(); i++) {
            Element element = (Element) targets.item(i);
            if ("-mtj-initialize".equals(element.getAttribute(ATTR_NAME))) { //$NON-NLS-1$
                for (MTJRuntime config : midletSuite.getRuntimeList()) {
                    String configName = replaceSpace(config.getName());
                    Element mkdir = newChildElement(element, ELEM_MKDIR);
                    mkdir.setAttribute(ATTR_DIR, "${path.build.classes." //$NON-NLS-1$
                            + configName + "}/_no_export"); //$NON-NLS-1$
                }
                break;
            }
        }
    }

    /**
     * Update wtkmakeprc target in mtj build xml doc.
     */
    private void updateWtkmakeprcTarget() {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        NodeList targets = documentElement.getElementsByTagName(ELEM_TARGET);
        for (int i = 0; i < targets.getLength(); i++) {
            Element element = (Element) targets.item(i);
            if ("-mtj-makeprc".equals(element.getAttribute(ATTR_NAME))) { //$NON-NLS-1$
                for (MTJRuntime config : midletSuite.getRuntimeList()) {
                    String configName = replaceSpace(config.getName());
                    String jadfile = "${path.build.output." + configName //$NON-NLS-1$
                            + "}/${midlet.name}.jad"; //$NON-NLS-1$
                    String prcfile = "${path.build.output." + configName //$NON-NLS-1$
                            + "}/${midlet.name}.prc"; //$NON-NLS-1$
                    Element wtkmakeprc = XMLUtils.createChild(element,
                            ELEM_WTKMAKEPRC);
                    wtkmakeprc.setAttribute(ATTR_JADFILE, jadfile);
                    wtkmakeprc.setAttribute(ATTR_PRCFILE, prcfile);
                }
                break;
            }
        }
    }

    /**
     * Update the WTK obfuscate element to include the parameters.
     * 
     * @param wktobfuscateElement
     */
    private void updateWtkObfuscateElement(Element wktobfuscateElement) {
        String[] keepExpressions = PreferenceAccessor.instance
                .getProguardKeepExpressions(midletSuite.getProject());

        for (String keepExpression : keepExpressions) {
            StringBuffer sb = new StringBuffer("'-keep "); //$NON-NLS-1$
            sb.append(keepExpression).append("'"); //$NON-NLS-1$

            addWtkObfuscateArgument(wktobfuscateElement, sb.toString());
        }

        addWtkObfuscateArgument(wktobfuscateElement, getProguardOptions());
    }

    /**
     * Update wtkrun target in mtj build xml doc.
     */
    private void updateWtkrunTarget() {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        Element runElement = findElement(documentElement, ELEM_WTKRUN);
        String activeConfigName = replaceSpace(midletSuite.getRuntimeList()
                .getActiveMTJRuntime().getName());
        String jadfile = "${path.build.output." + activeConfigName //$NON-NLS-1$
                + "}/${midlet.name}.jad"; //$NON-NLS-1$
        runElement.setAttribute(ATTR_JADFILE, jadfile);
    }

    /**
     * Validate that the antenna property is valid.
     * 
     * @param monitor
     * @throws AntennaExportException
     */
    private void validateAntenna(IProgressMonitor monitor)
            throws AntennaExportException {
        boolean valid = false;

        Preferences prefs = MTJCore.getMTJCore().getPluginPreferences();
        String antennaPref = prefs
                .getString(IMTJCoreConstants.PREF_ANTENNA_JAR);
        File antennaFile = new File(antennaPref);
        if (antennaFile.exists()) {
            try {
                ZipFile zipFile = new ZipFile(antennaFile);
                ZipEntry entry = zipFile.getEntry("antenna.properties"); //$NON-NLS-1$
                valid = (entry != null);
                zipFile.close();
            } catch (IOException e) {
                // Assume these cases mean the file is invalid
                MTJLogger.log(IStatus.WARNING, Messages.AntennaBuildExporter_file_is_invalid,
                        e);
            }
        }

        if (!valid) {
            throw new AntennaExportException(
                    Messages.AntennaBuildExporter_Antennalibrary_not_found);
        }
    }

    /**
     * Validate the environment prior to the export. Throw an exception if the
     * environment is not valid.
     * 
     * @param monitor
     * @throws AntennaExportException if the environment is not valid
     */
    private void validateEnvironment(IProgressMonitor monitor)
            throws AntennaExportException {
        validateAntenna(monitor);
        validateWTK(monitor);
    }

    /**
     * Validate that a Sun WTK can be found.
     * 
     * @param monitor
     * @throws AntennaExportException
     */
    private void validateWTK(IProgressMonitor monitor)
            throws AntennaExportException {
        File wtkRoot = getWTKRoot();
        if ((wtkRoot == null) || !wtkRoot.exists()) {
            throw new AntennaExportException(
                    Messages.AntennaBuildExporter_WTK_not_found);
        }
    }

    /**
     * Write out the build properties file.
     * 
     * @param monitor
     * @throws IOException
     * @throws CoreException
     */
    private void writeBuildProperties(IProgressMonitor monitor)
            throws IOException, CoreException {
        File buildPropsFile = getMidletSuiteFile("mtj-build.properties", //$NON-NLS-1$
                monitor);
        FileOutputStream fos = new FileOutputStream(buildPropsFile);
        buildProperties.store(fos,
                Messages.AntennaBuildExporter_buildProperties_comment);
        fos.close();
    }

    /**
     * Write the MTJ build.xml file.
     * 
     * @throws TransformerException
     * @throws IOException
     * @throws CoreException
     */
    private void writeMTJBuildXml(IProgressMonitor monitor)
            throws TransformerException, IOException, CoreException {
        // Write the output
        File buildXmlFile = getMidletSuiteFile("mtj-build.xml", monitor); //$NON-NLS-1$
        XMLUtils.writeDocument(buildXmlFile, mtjBuildXmlDocument);
    }
}