/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola) - Initial implementation                      
 */
package org.eclipse.mtj.core.build.preverifier;

/**
 * The <code>PreverifierNotFoundException</code> is thrown when the
 * preverification process discovers that no valid preverifier was found.
 * 
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @since 1.0
 */
public class PreverifierNotFoundException extends Exception {

    /**
     * The serial version UID for this class
     */
    private static final long serialVersionUID = -6884070458528264387L;

    /**
     * Creates a new instance of PreverifierNotFoundException with the specified
     * detail message.
     * 
     * @param message the detail message. The detail message may be retrieved by
     *            the {@link #getMessage()} method.
     */
    public PreverifierNotFoundException(String message) {
        super(message);
    }

}
