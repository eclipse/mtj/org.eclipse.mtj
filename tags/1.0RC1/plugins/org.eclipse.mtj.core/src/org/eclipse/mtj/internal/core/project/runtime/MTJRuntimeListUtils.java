/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.internal.core.project.runtime;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.IMetaData;
import org.eclipse.mtj.core.project.ProjectType;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.project.midp.MetaData;

/**
 * Provides Configuration related utils.
 * 
 * @since 1.0
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class MTJRuntimeListUtils {
    /**
     * The same as
     * {@link MTJRuntimeListUtils#mtjRuntimeListEquals(MTJRuntime, MTJRuntime)}
     * , except ignore active configuration. This means, if two Configuration
     * are same except that active configuration are different, also return
     * true.
     * 
     * @param cs1
     * @param cs2
     * @return
     */
    public static boolean mtjRuntimeListContentsEquals(MTJRuntimeList cs1,
            MTJRuntimeList cs2) {
        if (cs1.size() != cs2.size()) {
            return false;
        }
        for (MTJRuntime cInCs1 : cs1) {
            int index = cs2.indexOf(cInCs1);
            if (index < 0) {
                return false;
            }
            MTJRuntime cInCs2 = cs2.get(index);
            if (!deviceEquals(cInCs1, cInCs2)) {
                return false;
            }
            if (!workspaceSymbolsetsEquals(
                    cInCs1.getWorkspaceScopeSymbolSets(), cInCs2
                            .getWorkspaceScopeSymbolSets())) {
                return false;
            }
            if (!symbolSetEquals(cInCs1, cInCs2)) {
                return false;
            }

        }
        return true;
    }

    /**
     * Determine if two Configuration are totally equal.<br>
     * If<br>
     * 1, two Configuration have same size and<br>
     * 2, both contains configuration with same name and<br>
     * 3, configuration with the same name have same device and same symbols,<br>
     * this method will return true and<br>
     * 4, two Configuration have same active configuration.<br>
     * In which same symbols means two SymbolSet have same size, and both have
     * symbols with the same name, and symbols with the same name have same
     * value.
     * 
     * @param cs1
     * @param cs2
     * @return
     */
    public static boolean mtjRuntimeListEquals(MTJRuntimeList cs1,
            MTJRuntimeList cs2) {
        if (!mtjRuntimeListContentsEquals(cs1, cs2)) {
            return false;
        }
        if (!cs1.getActiveMTJRuntime().equals(cs2.getActiveMTJRuntime())) {
            return false;
        }
        return true;
    }

    /**
     * Determine if devices in two Configuration is the same instance.
     * 
     * @param cInCs1
     * @param cInCs2
     * @return
     */
    private static boolean deviceEquals(MTJRuntime cInCs1, MTJRuntime cInCs2) {
        if (cInCs1.getDevice() == cInCs2.getDevice()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * If Configuration have any modification that not save to metadata file,
     * return true.
     * 
     * @param midletProject
     * @return
     */
    public static boolean isMTJRuntimeListDirty(IMTJProject midletProject) {
        MTJRuntimeList configsInMemory = midletProject.getRuntimeList();

        boolean isDirty = true;

        IMetaData metaData = MTJCore.createMetaData(midletProject.getProject(),
                ProjectType.MIDLET_SUITE);

        MTJRuntimeList configsInMetadataFile = metaData.getRuntimeList();
        isDirty = !mtjRuntimeListEquals(configsInMemory, configsInMetadataFile);

        return isDirty;
    }

    /**
     * If you only change active configuration(any other contents not change)
     * and not save it to metadata file, return true.
     * 
     * @param midletProject
     * @return
     */
    public static boolean isOnlyActiveMTJRuntimeDirty(IMTJProject midletProject) {
        MTJRuntimeList configsInMemory = midletProject.getRuntimeList();
        MTJRuntimeList configsInMetadataFile = new MetaData(midletProject
                .getProject()).getRuntimeList();
        boolean same = mtjRuntimeListContentsEquals(configsInMemory,
                configsInMetadataFile);
        return same;
    }

    /**
     * If<br>
     * 1, two SymbolSet have the same size and<br>
     * 2, both SymbolSet contains the symbol with a certain name and<br>
     * 3, two symbol with the same name have the same value,<br>
     * return true.
     * 
     * @param cInCs1
     * @param cInCs2
     * @return
     */
    private static boolean symbolSetEquals(MTJRuntime cInCs1, MTJRuntime cInCs2) {
        ISymbolSet symbolSet1 = cInCs1.getSymbolSet();
        ISymbolSet symbolSet2 = cInCs2.getSymbolSet();
        
        if (!symbolSet1.equals(symbolSet2)) {
            return false;
        }
        List<ISymbol> symbolList1 = new ArrayList<ISymbol>(symbolSet1
                .getSymbols());
        List<ISymbol> symbolList2 = new ArrayList<ISymbol>(symbolSet2
                .getSymbols());

        for (Iterator<ISymbol> iterator = symbolList1.iterator(); iterator
                .hasNext();) {
            ISymbol symbol = (ISymbol) iterator.next();

            if (!symbolList2.contains(symbol))
                return false;
        }

        return true;
    }

    public static boolean workspaceSymbolsetsEquals(List<ISymbolSet> sets1,
            List<ISymbolSet> sets2) {
        if ((sets1 == null) && (sets2 == null)) {
            return true;
        }
        if ((sets1 == null) && (sets2 != null)) {
            return false;
        }
        if ((sets1 != null) && (sets2 == null)) {
            return false;
        }
        if (sets1.size() != sets2.size()) {
            return false;
        }
        for (ISymbolSet s : sets1) {
            if (!sets2.contains(s)) {
                return false;
            }
        }
        return true;
    }
}
