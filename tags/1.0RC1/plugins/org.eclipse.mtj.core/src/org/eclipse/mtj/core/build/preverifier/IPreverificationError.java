/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Extracted interface
 *     Gustavo de Paula (Motorola)  - Preverification refactoring     
 */
package org.eclipse.mtj.core.build.preverifier;

import org.eclipse.mtj.core.project.IMTJProject;

/**
 * The interface <code>IPreverificationError</code> and its implementors
 * represents a preverification error that was caught while preverifing of the
 * compiled classes of a {@link IMTJProject}.
 * <p>
 * All implementations of this interface are <b>internal</b>, and should not be
 * accessed directly by clients(compatibility will not be maintained). This
 * Interface was only made available publicly to maintain consistency of the
 * {@link IPreverifier} interface. Most clients will never use it.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @since 1.0
 */
public interface IPreverificationError {

    /**
     * Returns the detail message string of this PreverificationError.
     * 
     * @return the detail message string of this <tt>PreverificationError</tt>
     *         instance (which may be <tt>null</tt>).
     */
    public String getDetail();

    /**
     * Returns the location where the error occurred or <code>null</code> if the
     * location is unknown.
     * 
     * @return Returns the error location of this PreverificationError or
     *         <code>null</code> if the location is unknown.
     */
    public Object getLocation();

    /**
     * Returns the type of the error found during preverification.
     * 
     * @return a type representing an error found during preverification.
     */
    public Object getType();

}