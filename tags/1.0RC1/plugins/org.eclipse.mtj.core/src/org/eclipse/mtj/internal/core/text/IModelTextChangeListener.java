/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

import org.eclipse.mtj.internal.core.IModelChangedListener;
import org.eclipse.text.edits.TextEdit;

/**
 * @since 0.9.1
 */
public interface IModelTextChangeListener extends IModelChangedListener {

    /**
     * @return
     */
    TextEdit[] getTextOperations();

    /**
     * Get a human readable name for the given TextEdit for use in a refactoring
     * preview, for instance.
     * 
     * @param edit the edit to get a name for
     * @return the name associated to the given edit, or null if there is none
     */
    String getReadableName(TextEdit edit);

}
