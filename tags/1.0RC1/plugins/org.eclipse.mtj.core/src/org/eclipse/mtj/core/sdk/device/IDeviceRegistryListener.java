/**
 * Copyright (c) 2003, 2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.sdk.device;

/**
 * Classes that implement this interface provide methods that deal with the
 * events that are generated when the device registry changes.
 * 
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface IDeviceRegistryListener {

    /**
     * The specified device has been added to the registry.
     * <p>
     * Sent when the {@link IDeviceRegistry#addDevice(IDevice)} is invoked.
     * </p>
     * 
     * @param device the device that was added to the registry.
     */
    public void deviceAdded(IDevice device);

    /**
     * The specified device has been removed to the registry.
     * <p>
     * Sent when the {@link IDeviceRegistry#removeDevice(IDevice)} is invoked.
     * </p>
     * 
     * @param device the device that was removed from the registry.
     */
    public void deviceRemoved(IDevice device);
}
