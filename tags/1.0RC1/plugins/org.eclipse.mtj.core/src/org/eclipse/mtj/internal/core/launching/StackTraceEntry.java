/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.core.launching;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.jface.text.IRegion;

/**
 * StackTraceEntry Class wraps information for an occurrence of a method call
 * inside a stack. Since the same method can appear more than once the same
 * StackTraceEntry instance will be bound to all traces.
 * 
 * @author David Marques
 */
public class StackTraceEntry {

    private static final String AT = "at ";

    private List<IRegion> regions;

    private String methodName;
    private String className;
    private File sourceFile;

    private int offset;
    private int line = Integer.MIN_VALUE;;

    /**
     * Creates a StackTraceEntry to wrap information about the stack trace.
     * 
     * @param trace stack trace line.
     */
    public StackTraceEntry(String trace) {
        if (Pattern.matches("(\tat .+[(].*[.java][:]\\d+[)])", trace)) {
            parseLineWithLine(trace);
        } else if (Pattern.matches("(\tat .+[(][+]\\d+[)])", trace)) {
            parseLineWithOffset(trace);
        } else {
            throw new IllegalArgumentException("Invalid stack trace format.");
        }
    }

    private void parseLineWithOffset(String trace) {
        int atIndex = trace.indexOf(AT);
        atIndex += AT.length();

        trace = trace.substring(atIndex);
        int dotIndex = trace.lastIndexOf(".");
        int lprIndex = trace.lastIndexOf("(");
        int rprIndex = trace.lastIndexOf(")");

        this.className = trace.substring(0x00, dotIndex);
        this.methodName = trace.substring(++dotIndex, lprIndex++);
        this.offset = Integer.parseInt(trace.substring(++lprIndex, rprIndex));
    }

    private void parseLineWithLine(String trace) {
        int atIndex = trace.indexOf(AT);
        atIndex += AT.length();

        trace = trace.substring(atIndex);
        int lprIndex = trace.lastIndexOf("(");
        int rprIndex = trace.lastIndexOf(")");
        int colIndex = trace.lastIndexOf(":");

        String part1 = trace.substring(0x00, lprIndex);
        int dotIndex = part1.lastIndexOf(".");

        this.className = part1.substring(0x00, dotIndex);
        this.methodName = part1.substring(++dotIndex);
        this.line = Integer.parseInt(trace.substring(++colIndex, rprIndex));
    }

    /**
     * Adds a region to the stack entry.
     * 
     * @param region stack region related to the stack entry.
     */
    public synchronized void addRegion(IRegion region) {
        if (this.regions == null) {
            this.regions = new LinkedList<IRegion>();
        }
        this.regions.add(region);
    }

    /**
     * Gets all regions where a stack entry appears on the console.
     * 
     * @return regions.
     */
    public List<IRegion> getRegions() {
        return this.regions;
    }

    /**
     * Sets the line number.
     * 
     * @param line the line to set
     */
    public void setLine(int line) {
        this.line = line;
    }

    /**
     * Gets the line number.
     * 
     * @return the line
     */
    public int getLine() {
        return line;
    }

    /**
     * Gets the method name.
     * 
     * @return the methodName
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * Gets teh class name.
     * 
     * @return the className
     */
    public String getClassName() {
        return className.replace(".", "/");
    }

    /**
     * Gets the method offset.
     * 
     * @return the offset
     */
    public int getOffset() {
        return offset;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(this.className);
        buffer.append(".");
        buffer.append(this.methodName);
        buffer.append("(");
        buffer.append(this.className);
        buffer.append(".java:");
        buffer.append(this.line);
        buffer.append(")");
        return buffer.toString();
    }

    /**
     * Sets the Java File.
     * 
     * @param sourceFile file instance.
     */
    public void setSourcePath(File sourceFile) {
        this.sourceFile = sourceFile;
    }

    /**
     * Gets the source file.
     * 
     * @return
     */
    public File getJavaFile() {
        return sourceFile;
    }
}
