/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.project.midp;

/**
 * Constants related to editing JAD files.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 * @author Craig Setera
 */
public interface IJADConstants {

    // Required:
    public static final String JAD_MIDLET_JAR_SIZE = "MIDlet-Jar-Size"; //$NON-NLS-1$
    public static final String JAD_MIDLET_JAR_URL = "MIDlet-Jar-URL"; //$NON-NLS-1$
    public static final String JAD_MIDLET_NAME = "MIDlet-Name"; //$NON-NLS-1$
    public static final String JAD_MIDLET_VENDOR = "MIDlet-Vendor"; //$NON-NLS-1$
    public static final String JAD_MIDLET_VERSION = "MIDlet-Version"; //$NON-NLS-1$
    public static final String JAD_MICROEDITION_CONFIG = "MicroEdition-Configuration"; //$NON-NLS-1$
    public static final String JAD_MICROEDITION_PROFILE = "MicroEdition-Profile"; //$NON-NLS-1$

    // Optional:
    public static final String JAD_MIDLET_DATA_SIZE = "MIDlet-Data-Size"; //$NON-NLS-1$
    public static final String JAD_MIDLET_DELETE_CONFIRM = "MIDlet-Delete-Confirm"; //$NON-NLS-1$
    public static final String JAD_MIDLET_DELETE_NOTIFY = "MIDlet-Delete-Notify"; //$NON-NLS-1$
    public static final String JAD_MIDLET_DESCRIPTION = "MIDlet-Description"; //$NON-NLS-1$
    public static final String JAD_MIDLET_ICON = "MIDlet-Icon"; //$NON-NLS-1$
    public static final String JAD_MIDLET_INFO_URL = "MIDlet-Info-URL"; //$NON-NLS-1$
    public static final String JAD_MIDLET_INSTALL_NOTIFY = "MIDlet-Install-Notify"; //$NON-NLS-1$

    // The permission properties
    public static final String JAD_MIDLET_PERMISSIONS = "MIDlet-Permissions"; //$NON-NLS-1$
    public static final String JAD_MIDLET_PERMISSIONS_OPTIONAL = "MIDlet-Permissions-Opt"; //$NON-NLS-1$

    // Signature-related:
    public static final String JAD_MIDLET_JAR_RSA_SHA1 = "MIDlet-Jar-RSA-SHA1"; //$NON-NLS-1$

    // (needs "N" appended to it)
    public static final String JAD_MIDLET_CERTIFICATE = "MIDlet-Certificate-1-"; //$NON-NLS-1$
}
