/**
 * Copyright (c) 2008,2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)          - Initial implementation
 *     Gustavo de Paula (Motorola) - Configuration refactory [Bug 265522]
 *     Diego Sandin (Motorola)     - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.project.runtime.event;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;

/**
 * Classes that implement this interface provide methods that deal with the
 * events that are generated when the properties of a {@link MTJRuntimeList}
 * change.
 * <p>
 * All Events are constructed with a reference to the object, the "source", that
 * is logically deemed to be the object upon which the Event in question
 * initially occurred upon.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface IMTJRuntimeListChangeListener {

    /**
     * The currently active {@link MTJRuntimeList} has changed.
     * <p>
     * Sent when the {@link MTJRuntimeList#switchActiveMTJRuntime(MTJRuntime)}
     * method is invoked.
     * </p>
     * 
     * @param event an SwitchActiveMTJRuntimeEvent object containing the active
     *            MTJRuntime information.
     */
    void activeMTJRuntimeSwitched(SwitchActiveMTJRuntimeEvent event);

    /**
     * A {@link MTJRuntime} was added to the {@link MTJRuntimeList}.
     * 
     * <p>
     * Sent when the {@link MTJRuntimeList#add(MTJRuntime)} method is invoked.
     * </p>
     * 
     * @param event an AddMTJRuntimeEvent object containing the added MTJRuntime
     *            information.
     */
    void mtjRuntimeAdded(AddMTJRuntimeEvent event);

    /**
     * A {@link MTJRuntime} was removed from the {@link MTJRuntimeList}.
     * 
     * <p>
     * Sent when the {@link MTJRuntimeList#remove(Object)} method is invoked.
     * </p>
     * 
     * @param event an RemoveMTJRuntimeEvent object containing the removed
     *            MTJRuntime information.
     */
    void mtjRuntimeRemoved(RemoveMTJRuntimeEvent event);
}
