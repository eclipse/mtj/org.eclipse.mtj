/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)    - Initial implementation
 *     Kevin Hunter (Individual)   - Initial implementation
 *     Diego Sandin (Motorola)     - Refactoring package name to follow eclipse 
 *                                   standards and removed setPredeploymentRequired
 *                                   invocation
 *     Gustavo de Paula (Motorola) - Fix after classpath refactoring
 *     Gustavo de Paula (Motorola) - Preverifier refactoring 
 *     Diego Sandin (Motorola)     - IDeviceImporter API refactoring  
 */
package org.eclipse.mtj.examples.toolkits.motorola;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.IDeviceImporter;
import org.eclipse.mtj.core.sdk.device.ILibraryImporter;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.examples.toolkits.Activator;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.osgi.framework.Bundle;

/**
 * A device importer implementation for the Motorola toolkits.
 * 
 * @author Craig Setera
 * @author Kevin Hunter
 */
public class MotorolaDeviceImporter implements IDeviceImporter {

    // Maintains information about an individual emulator
    // within a Motorola SDK
    private class EmulatorData {
        public boolean debugServer;
        public String[] devices;
        public String emulator;
        public String executable;
        public String launchCommandTemplate;
        public boolean predeployRequired;
    }

    private static final String DEBUG_SERVER_SUFFIX = "_debug.server";
    private static final String DEVICES_SUFFIX = "_devices";
    private static final String EXECUTABLE_SUFFIX = "_executable";
    private static final String LAUNCH_COMMAND_SUFFIX = "_launch.command";
    private static final String PREDEPLOY_REQUIRED_SUFFIX = "_predeploy.required";
    // Various property file keys
    private static final String PROPS_EMULATORS_LIST = "emulators.list";

    // Properties file holding emulator/device information
    private static final String PROPS_FILE = "moto_emulators.properties";

    private Map<String, EmulatorData> emulatorDataMap;

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceImporter#importDevices(java.io.File, org.eclipse.core.runtime.IProgressMonitor)
     */
    public List<IDevice> importDevices(File directory, IProgressMonitor monitor) {
        List<IDevice> devices = null;

        File resourcesDirectory = new File(directory, "resources");
        if (resourcesDirectory.exists()) {
            devices = getResourcesDevices(resourcesDirectory, monitor);
        }

        return devices;
    }

    /**
     * Create a new device based on the specified information.
     * 
     * @param propsFile
     * @param deviceProperties
     * @param devicename
     * @return
     */
    private IDevice createDevice(File propsFile, Properties deviceProperties,
            String devicename) {
        IDevice device = null;

        EmulatorData emulatorData = getEmulatorData(devicename);
        if (emulatorData != null) {
            device = createDevice(propsFile, deviceProperties, devicename,
                    emulatorData);
        }

        return device;
    }

    /**
     * Create a new device based on the specified information.
     * 
     * @param propsFile
     * @param deviceProperties
     * @param devicename
     * @param emulatorData
     * @return
     */
    private IDevice createDevice(File propsFile, Properties deviceProperties,
            String devicename, EmulatorData emulatorData) {

        // We need to look at the contents of parent directories
        File emulatorBinDirectory = propsFile.getParentFile().getParentFile();
        File emulatorDirectory = emulatorBinDirectory.getParentFile();
        File sdkDirectory = emulatorDirectory.getParentFile();

        MotorolaDevice device = new MotorolaDevice();

        try {
            device.setBundle(Activator.getDefault().getBundle()
                    .getSymbolicName());
            device.setClasspath(getClasspath(emulatorDirectory));
            device.setDebugServer(emulatorData.debugServer);
            device.setDescription(devicename);
            device.setDeviceProperties(deviceProperties);
            device.setExecutable(new File(emulatorBinDirectory,
                    emulatorData.executable));
            device.setGroupName(sdkDirectory.getName());
            device.setName(devicename);
            device.setPreverifier(getPreverifier(emulatorBinDirectory,
                    sdkDirectory));
            device.setPropertiesFile(propsFile);
            device.setProtectionDomains(new String[0]);
            device.setLaunchCommandTemplate(emulatorData.launchCommandTemplate);

            ISymbolSet dss = MTJCore.getSymbolSetFactory()
                    .createSymbolSetFromDevice(device);
            ISymbolSet pss = MTJCore.getSymbolSetFactory()
                    .createSymbolSetFromProperties(deviceProperties);
            dss.add(pss.getSymbols());
            dss.setName(device.getName());
            device.setSymbolSet(dss);

        } catch (CoreException e) {
            MTJLogger.log(IStatus.WARNING,
                    "Error importing device " + devicename, e);
            device = null;
        }

        return device;
    }

    /**
     * Find a preverify executable in the specified directory or subdirectories.
     * 
     * @param directory
     * @return
     */
    private File findPreverifyExecutable(File directory) {
        File[] files = directory.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                String name = pathname.getName();
                return pathname.isDirectory()
                        || (name.equals("preverify.exe") || name
                                .equals("preverify"));
            }
        });

        File executable = null;
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                executable = findPreverifyExecutable(files[i]);
            } else {
                executable = files[i];
                break;
            }
        }

        return executable;
    }

    /**
     * Return the classpath given the specified emulator directory. The current
     * implementation assumes that the classpath is created using all of the
     * files in the lib directory.
     * 
     * @param emulatorDirectory
     * @return
     */
    private IDeviceClasspath getClasspath(File emulatorDirectory) {
        IDeviceClasspath classpath = MTJCore.createNewDeviceClasspath();

        File libDirectory = new File(emulatorDirectory, "lib");
        File[] libraries = libDirectory.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                String name = pathname.getName();
                return pathname.isFile()
                        && (name.endsWith(".zip") || name.endsWith(".jar"));
            }
        });

        ILibraryImporter libImporter = MTJCore
                .getLibraryImporter(ILibraryImporter.LIBRARY_IMPORTER_UEI);

        for (int i = 0; i < libraries.length; i++) {
            IMIDPLibrary library = (IMIDPLibrary) libImporter
                    .createLibraryFor(libraries[i]);
            classpath.addEntry(library);
        }

        return classpath;
    }

    /**
     * Return the emulator data associated with the specified device name or
     * <code>null</code> if none can be found.
     * 
     * @param deviceName
     * @return
     */
    private EmulatorData getEmulatorData(String deviceName) {
        return (EmulatorData) getEmulatorDataMap().get(deviceName);
    }

    /**
     * Return the map of emulator data. Each entry is keyed by the device name
     * mapped to an instance of EmulatorData.
     * 
     * @return
     */
    private Map<String, EmulatorData> getEmulatorDataMap() {
        if (emulatorDataMap == null) {
            emulatorDataMap = readEmulatorDataMap();
        }

        return emulatorDataMap;
    }

    /**
     * Return a preverifier for use with the specified emulator. If a specific
     * preverifier cannot be found in the specified emulator directory, search
     * for a backup preverify.exe in the SDK.
     * 
     * @param emulatorBinDirectory
     * @param sdkDirectory
     * @return
     * @throws CoreException
     */
    private IPreverifier getPreverifier(File emulatorBinDirectory,
            File sdkDirectory) throws CoreException {
        File preverifyExecutable = findPreverifyExecutable(emulatorBinDirectory);
        if (preverifyExecutable == null) {
            preverifyExecutable = findPreverifyExecutable(sdkDirectory);
        }

        return MTJCore.createPreverifier(IPreverifier.PREVERIFIER_STANDARD,
                preverifyExecutable);
    }

    /**
     * Return a properties object for the properties in the file.
     * 
     * @param propsFile
     * @return
     */
    private Properties getProperties(File propsFile) {
        Properties properties = new Properties();

        InputStream is = null;
        try {
            is = new FileInputStream(propsFile);
            properties.load(is);
        } catch (IOException e) {
            MTJLogger.log(IStatus.WARNING, "Error reading properties file "
                    + propsFile, e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        }

        return properties;
    }

    /**
     * Return the devices found in the specified resources directory.
     * 
     * @param resourcesDirectory
     * @param monitor
     * @return
     */
    private List<IDevice> getResourcesDevices(File resourcesDirectory,
            IProgressMonitor monitor) {
        File[] propsFiles = resourcesDirectory.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.isFile()
                        && pathname.getName().endsWith(".props");
            }
        });

        ArrayList<IDevice> devices = new ArrayList<IDevice>();
        for (int i = 0; i < propsFiles.length; i++) {
            File propsFile = propsFiles[i];
            Properties deviceProperties = getProperties(propsFile);
            String devicename = deviceProperties.getProperty("devicename");
            if (devicename != null) {
                IDevice device = createDevice(propsFile, deviceProperties,
                        devicename);
                if (device != null) {
                    devices.add(device);
                }
            }
        }
        return devices;
    }

    /**
     * Parse out the information for the specific emulator.
     * 
     * @param emulatorMap
     * @param emulatorsProperties
     * @param emulator
     */
    private void parseEmulatorDataProperties(
            Map<String, EmulatorData> emulatorMap,
            Properties emulatorsProperties, String emulator) {
        String devicesString = emulatorsProperties.getProperty(emulator
                + DEVICES_SUFFIX, "");
        String debugServerString = emulatorsProperties.getProperty(emulator
                + DEBUG_SERVER_SUFFIX, "");
        String predeployString = emulatorsProperties.getProperty(emulator
                + PREDEPLOY_REQUIRED_SUFFIX, "");

        EmulatorData data = new EmulatorData();
        data.emulator = emulator;
        data.executable = emulatorsProperties.getProperty(emulator
                + EXECUTABLE_SUFFIX, "");
        data.launchCommandTemplate = emulatorsProperties.getProperty(emulator
                + LAUNCH_COMMAND_SUFFIX, "");
        if (devicesString.length() > 0) {
            data.devices = devicesString.split(",");
        } else {
            data.devices = new String[0];
        }
        data.debugServer = debugServerString.equalsIgnoreCase("true");
        data.predeployRequired = predeployString.equalsIgnoreCase("true");

        for (int i = 0; i < data.devices.length; i++) {
            String device = data.devices[i];
            emulatorMap.put(device, data);
        }
    }

    /**
     * Parse out the emulator data information from the properties.
     * 
     * @param emulatorsProperties
     * @return
     */
    private Map<String, EmulatorData> parseEmulatorDataProperties(
            Properties emulatorsProperties) {
        Map<String, EmulatorData> emulatorDataMap = new HashMap<String, EmulatorData>();

        String emulatorsList = emulatorsProperties.getProperty(
                PROPS_EMULATORS_LIST, "");
        String[] emulators = emulatorsList.split(",");
        for (int i = 0; i < emulators.length; i++) {
            parseEmulatorDataProperties(emulatorDataMap, emulatorsProperties,
                    emulators[i]);
        }

        return emulatorDataMap;
    }

    /**
     * Read in the emulator data map.
     * 
     * @return
     */
    private Map<String, EmulatorData> readEmulatorDataMap() {
        // Read the properties the define the emulator data
        Properties emulatorsProperties = new Properties();

        InputStream emulatorsStream = null;
        try {
            Bundle bundle = Activator.getDefault().getBundle();
            URL propsFileURL = bundle.getEntry(PROPS_FILE);

            emulatorsStream = propsFileURL.openStream();
            if (emulatorsStream != null) {
                emulatorsProperties.load(emulatorsStream);
            }

        } catch (IOException e) {
            MTJLogger.log(IStatus.ERROR,
                    "Error loading Motorola emulator properties", e);
        } finally {
            if (emulatorsStream != null) {
                try {
                    emulatorsStream.close();
                } catch (IOException e) {
                }
            }
        }

        return parseEmulatorDataProperties(emulatorsProperties);
    }
}
