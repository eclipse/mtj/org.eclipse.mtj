/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma (Sybase)         - Initial implementation
 *     Diego Madruga (Motorola) - Refactored class after API updates.  
 */
package org.eclipse.mtj.examples.jadextension;

import org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;

/**
 * Motorola specific JAD editor page.
 * 
 * @author Gang Ma
 */
public class MotoSpecPropertiesEditorPage extends JADPropertiesEditorPage {

    /**
     * The page unique identifier
     */
    public static final String MOTOROLA_PAGEID = "motoSpecific";

    /**
     * A constructor that creates the Motorola Specific Properties JAD
     * EditorPage. The parent editor need to be passed in the
     * <code>initialize</code> method if this constructor is used.
     */
    public MotoSpecPropertiesEditorPage() {
        super(MOTOROLA_PAGEID,
                Messages.motoSpecPropertiesEditorPage_page_title);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getTitle()
     */
    @Override
    public String getTitle() {
        return Messages.motoSpecPropertiesEditorPage_page_title;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#addContextHelp(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void addContextHelp(Composite c) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(c,
                "org.eclipse.mtj.ui.help_MotorolaJADPropertiesEditorPage"); //$NON-NLS-1$
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
     */
    @Override
    protected String getHelpResource() {
        return "/org.eclipse.mtj.docs/docs/jadeditor.html"; //$NON-NLS-1$
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionDescription()
     */
    @Override
    protected String getSectionDescription() {
        return Messages.motoSpecPropertiesEditorPage_section_description;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionTitle()
     */
    @Override
    protected String getSectionTitle() {
        return Messages.motoSpecPropertiesEditorPage_section_title;
    }
}
