<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="copyright"
	content="Copyright (c) Motorola and others 2008. This page is made available under license. For full details see the LEGAL in the documentation book that contains this page.">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style/book.css" />
<title>Adding preprocessing support to a MIDlet project</title>
</head>
<body>

<h1>Adding preprocessing support to a MIDlet project</h1>

<p>Device fragmentation is a difficult issue in the Micro Edition
world. Devices are constrained in a variety of different and
inconsistent ways. While there are many techniques that can be used to
deal with these inconsistencies, the limited memory and processing power
of many mobile devices points to a solution that occurs off-device
rather than something that happens at runtime.</p>

<p>MTJ provides some help in the area of device fragmentation
through the inclusion of a source code "preprocessor". Using the limited
processing power of the preprocessor, it is possible to do some build
time source code transformations.</p>

<ul style="list-style-type: none">
	<li><a href="#enablement">Enabling and Disabling Preprocessing</a></li>
	<li><a href="#compilation">Controlling Compilation</a></li>
	<li><a href="#symbols">Defining Symbols</a></li>
	<li><a href="#referSymbolSets">Referencing Symbol Definition
	Sets</a></li>
</ul>
<hr>

<h2 id="enablement">Enabling and Disabling Preprocessing</h2>

<p>Use of the preprocessing functionality is an optional part of the
MTJ functionality. Preprocessing is enabled or disabled on a
project-level basis, allowing significant flexibility in the structure
of workspace projects.</p>

<h3>Enabling Preprocessing</h3>

<p>Enabling preprocessing functionality can be accomplished in two
different ways. During the creation of a new MIDlet Suite, preprocessing
may be enabled on the first page of the wizard.</p>

<img src="images/preprocessing/newProjectWizard2.png" />

<p>After a project has been created preprocessing may be enabled via
the Java ME project context menu option.</p>

<img border="1px" src="images/preprocessing/crtpkg.png" />

<h3>Disabling Preprocessing</h3>

<p>Projects with preprocessing functionality enabled, may also
disable the functionality if it is no longer necessary or useful.
Disabling the preprocessor is done via the project context menu, just as
enablement is done. For projects with processing enabled, there will be
a "Disable Preprocessing" menu item rather than an item to enable
processing.</p>

<h2 id="compilation">Controlling Compilation</h2>

<p>Preprocessing controls the final compiled code using a set of
preprocessor directives. The preprocessor supports the following
directives inside a Java source file. All directives must follow a "//"
comment that starts at the beginning of a line (whitespace is allowed
left of them, but no Java code). That way, they don't interfere with
normal Java compilation. Directives must not span multiple lines of
code.</p>

<table class="datatable">
	<tr>
		<th width="30%">Directive</th>

		<th>Description</th>
	</tr>

	<tr>
		<td>#define &lt;identifier&gt;</td>

		<td>Defines an identifier, thus making its value "true" when it
		is referenced in further expressions.</td>
	</tr>

	<tr>
		<td>#undefine &lt;identifier&gt;</td>

		<td>Undefines an identifier, thus making its value "false" when
		it is referenced in further expressions.</td>
	</tr>

	<tr>
		<td>#ifdef &lt;identifier&gt;</td>

		<td rowspan="8">The following lines are compiled only if the
		given identifier is defined (or undefined, in the case of an "#ifndef"
		directive). "#else" does exactly what your think it does. Each
		directive must be ultimately closed by an "#endif" directive. The
		"#elifdef" and "#elifndef" directives help to specify longer
		conditional cascades without having to nest each level.

		<p />The "#if" and "#elif" directives even allow to use complex
		expressions. These expressions are very much like Java boolean
		expressions. They may consist of identifiers, parentheses and the
		usual "&amp;&amp;", "||", "^", and "!" operators.
		<p />Please note that "#ifdef" and "#ifndef" don't support complex
		expressions. They only expect a single argument - the symbol to be
		checked.
		</td>
	</tr>

	<tr>
		<td>#ifndef &lt;identifier&gt;</td>
	</tr>


	<tr>

		<td>#else</td>
	</tr>

	<tr>
		<td>#endif</td>
	</tr>

	<tr>
		<td>#elifdef &lt;identifier&gt;</td>
	</tr>

	<tr>
		<td>#elifndef &lt;identifier&gt;</td>
	</tr>

	<tr>
		<td>#if &lt;expression&gt;</td>
	</tr>

	<tr>
		<td>#elif &lt;expression&gt;</td>
	</tr>

	<tr>
		<td>#include &lt;filename&gt;</td>

		<td rowspan="2">Includes the given source file at the current
		position. Must be terminated by "#endinclude" for technical reasons.
		Note that relative file names are interpreted as relative to the
		project root directory.</td>

	</tr>

	<tr>
		<td>#endinclude</td>
	</tr>

</table>
</p>

<h2 id="symbols">Defining Symbols</h2>

<p>The compilation directive expressions are dependent on the
definition of a set of "symbols". These symbols are either defined/true
or undefined/false. It is possible to control whether a particular
symbol is defined or undefined directly via preprocessing directives or
indirectly through the definition of symbol definition sets.</p>

<h3>Direct Definitions</h3>

<p>The <code>#define</code> and <code>#undefine</code> directives
directly control whether a particular symbol is currently defined or
undefined. Using these directives, it is possible to override the
current state of a symbol from a previous setting.</p>

<h3>Symbol Definition Sets</h3>

<p>Symbol definition sets provide a means to define a group of
related symbols with a particular name to reference that group. A
definition set may then be specified for a project via the user
interface to control the compilation.</p>

<p>Symbol definition sets are most useful for controlling
definitions without the need to alter source code. For example, symbol
definition sets can be used to remove debugging information to create a
production build. Start by defining a "Debug" symbol definition set in
which the symbol "DEBUG" has been defined. Wrap the debugging code in
your project with "#ifdef DEBUG" directives. When building a production
version of the code, use a different symbol definition set that does not
have DEBUG defined and the debugging code will be removed.</p>

<pre>
...
// #ifdef DEBUG
System.out.println("Some debug output");
// #endif
...
</pre>
<h3>Defining Configuration Scope Symbol Definition Sets</h3>
<p>When you add configuration to project, it will generate a set of
symbols according the device you choose. You can also modify the
generated symbols. For details, please refer <a
	href="../reference/advanced_topics/multi-configurations.html">Configuration
Document</a></p>

<h3>Defining Workspace Scope Symbol Definition Sets</h3>

<p>Workspace Scope Symbol Definition Sets can be shared by all
projects in the workspace. Symbol set definitions are accessed via the
MTJ preferences. From the Window menu, choose the <i>Preferences...</i>
menu item. Expand the Mobiles Tools for Java category and select the <i>Symbol
Set Definitions</i> category.</p>

<img src="images/preprocessing/symbols.png" />

<h4>Creating a New Set</h4>

<p>Creating a new symbol definition set is accomplished by
specifying the name of the definition set and selecting the <i>Add</i>
button. An empty set will be created.</p>

<h4>Defining Symbols</h4>

<p>To define a new symbol:</p>
<ol>
	<li>Select the set via the definition set drop-down.</li>
	<li>Press the <i>Add</i> button to the right of the symbols list.</li>
	<li>Select the newly created default symbol name and edit the name
	within the cell.</li>
</ol>
<h4>Removing Symbols</h4>
<p>To remove a symbol from the set:
<ol>
	<li>Select the set via the definition set drop-down.</li>
	<li>Select symbol definition to be removed.</li>
	<li>Press the <i>Remove</i> button to the right of the symbols
	list.</li>
</ol>
</p>

<h4>Removing a Set</h4>

<p>If a symbol definition set is no longer necessary, it can be
removed:
<ol>
	<li>Select the set via the definition set drop-down.</li>
	<li>Press the <i>Remove</i> button to the right of the definition
	set drop-down.</li>
</ol>
</p>
<h2 id="referSymbolSets">Referencing Symbol Definition Sets</h2>
<p>Once a Symbol Definition Set has been defined, it may be
referenced by a project. This is accomplished from the <a
	href="../reference/advanced_topics/multi-configurations.html#addConfig">Add/Edit
Configuration Dialog</a>.</p>
<p>Both configuration scope symbol set and Workspace scope symbol
sets you choose will then be used by the preprocessor directives in
processing the source code.</p>
</body>
</html>
