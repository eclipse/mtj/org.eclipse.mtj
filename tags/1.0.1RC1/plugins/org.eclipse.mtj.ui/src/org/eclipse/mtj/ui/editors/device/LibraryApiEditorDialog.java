/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874] 
 */
package org.eclipse.mtj.ui.editors.device;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.sdk.device.midp.MIDPAPI;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.internal.ui.viewers.TableViewerConfiguration;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.osgi.framework.Version;

/**
 * A dialog for the editing of the API's associated with a library in a
 * classpath.
 * 
 * @since 1.0
 */
public class LibraryApiEditorDialog extends Dialog {

    // Label provider for API instances
    private static class APILabelProvider extends LabelProvider implements
            ITableLabelProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
         */
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
         */
        public String getColumnText(Object element, int columnIndex) {
            IMIDPAPI api = (IMIDPAPI) element;
            String text = Utils.EMPTY_STRING;

            switch (columnIndex) {
                case 0:
                    text = api.getIdentifier();
                    break;

                case 1:
                    text = NLS.bind(
                            IMTJCoreConstants.VERSION_NLS_BIND_TEMPLATE,
                            new Object[] { api.getVersion().getMajor(),
                                    api.getVersion().getMinor() });
                    break;

                case 2:
                    text = api.getType().toString();
                    break;

                case 3:
                    text = api.getName();
                    break;
            }

            return text;
        }
    }

    // A cell modifier implementation for the device libraries editor
    private class CellModifier implements ICellModifier {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object, java.lang.String)
         */
        public boolean canModify(Object element, String property) {
            return true;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object, java.lang.String)
         */
        public Object getValue(Object element, String property) {
            Object value = null;
            IMIDPAPI api = (IMIDPAPI) element;

            switch (getColumnIndex(property)) {
                case 0:
                    value = api.getIdentifier();
                    break;

                case 1:
                    value = NLS.bind(
                            IMTJCoreConstants.VERSION_NLS_BIND_TEMPLATE,
                            new Object[] { api.getVersion().getMajor(),
                                    api.getVersion().getMinor() });

                    break;

                case 2:
                    value = Integer.valueOf(api.getType().getTypecode());
                    break;

                case 3:
                    value = api.getName();
                    break;
            }

            return value;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object, java.lang.String, java.lang.Object)
         */
        public void modify(Object element, String property, Object value) {
            TableItem item = (TableItem) element;
            IMIDPAPI api = (IMIDPAPI) item.getData();

            switch (getColumnIndex(property)) {
                case 0:
                    api.setIdentifier((String) value);
                    break;

                case 1:
                    api.setVersion(new Version((String) value));
                    break;

                case 2:
                    Integer integerCode = (Integer) value;
                    api.setType(MIDPAPIType.getMIDPAPITypeByCode(integerCode
                            .intValue()));
                    break;

                case 3:
                    api.setName((String) value);
                    break;
            }

            viewer.refresh(api, true);
        }

        /**
         * Return the column index for the property.
         * 
         * @param property
         * @return
         */
        private int getColumnIndex(String property) {
            int index = -1;

            for (int i = 0; i < PROPERTIES.length; i++) {
                if (PROPERTIES[i].equals(property)) {
                    index = i;
                    break;
                }
            }

            return index;
        }
    }

    // Content provider that makes a library's IAPI's available
    private class LibraryApiContentProvider implements
            IStructuredContentProvider {
        public void dispose() {
        }

        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            if (apis != null) {
                elements = apis.toArray(new IAPI[apis.size()]);
            }

            return elements;
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    private static final TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
            new TableColumnInfo(
                    MTJUIMessages.LibraryApiEditorDialog_identifier_columnInfo,
                    15f, null),
            new TableColumnInfo(
                    MTJUIMessages.LibraryApiEditorDialog_version_columnInfo,
                    15f, null),
            new TableColumnInfo(
                    MTJUIMessages.LibraryApiEditorDialog_type_columnInfo, 20f,
                    null),
            new TableColumnInfo(
                    MTJUIMessages.LibraryApiEditorDialog_name_columnInfo, 50f,
                    null), };
    // Column information structure
    private static final int DEFAULT_TABLE_WIDTH = 650;

    private static final Object[] NO_ELEMENTS = new Object[0];

    // Column property names
    private static final String PROP_IDENTIFIER = "identifier"; //$NON-NLS-1$
    private static final String PROP_NAME = "name"; //$NON-NLS-1$

    private static final String PROP_TYPE = "type"; //$NON-NLS-1$

    private static final String PROP_VERSION = "version"; //$NON-NLS-1$

    // All of the properties in order
    private static final String[] PROPERTIES = new String[] { PROP_IDENTIFIER,
            PROP_VERSION, PROP_TYPE, PROP_NAME };

    private List<IAPI> apis;
    // Widgets
    private TableViewer viewer;

    /**
     * Construct a new dialog.
     * 
     * @param parentShell
     */
    public LibraryApiEditorDialog(IShellProvider parentShell) {
        super(parentShell);
    }

    /**
     * Construct a new dialog.
     * 
     * @param parentShell
     */
    public LibraryApiEditorDialog(Shell parentShell) {
        super(parentShell);
    }

    /**
     * Get the selected APIs.
     * 
     * @return
     */
    public IAPI[] getAPIs() {
        return (apis == null) ? null : (IAPI[]) apis.toArray(new IAPI[apis
                .size()]);
    }

    /**
     * Set the library to be edited.
     * 
     * @param library
     */
    public void setAPIs(IAPI[] apis) {
        this.apis = new ArrayList<IAPI>();
        this.apis.addAll(Arrays.asList(apis));
    }

    /**
     * Create the devices table viewer.
     * 
     * @param parent
     */
    private TableViewer createTableViewer(Composite composite) {
        final String[] comboValues = { MIDPAPIType.CONFIGURATION.toString(),
                MIDPAPIType.PROFILE.toString(),
                MIDPAPIType.OPTIONAL.toString(), MIDPAPIType.UNKNOWN.toString() };

        int styles = SWT.MULTI | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION;
        Table table = new Table(composite, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        // Wire up the viewer
        TableViewer viewer = new TableViewer(table);
        viewer.setContentProvider(new LibraryApiContentProvider());
        viewer.setLabelProvider(new APILabelProvider());

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings("librayApiViewerSettings"); //$NON-NLS-1$
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 0);
        viewerConfiguration.configure(viewer);

        // Wire up the cell modification handling
        viewer.setCellModifier(new CellModifier());
        viewer.setColumnProperties(PROPERTIES);
        viewer.setCellEditors(new CellEditor[] { new TextCellEditor(table),
                new TextCellEditor(table),
                new ComboBoxCellEditor(table, comboValues),
                new TextCellEditor(table), });

        viewer.setInput(new Object());

        return viewer;
    }

    /**
     * Return the API element currently selected or <code>null</code> if not
     * selected.
     * 
     * @return
     */
    private IAPI getSelectedAPI() {
        IStructuredSelection selection = (IStructuredSelection) viewer
                .getSelection();
        return (IAPI) selection.getFirstElement();
    }

    /**
     * Handle the add button being pressed.
     */
    private void handleAddButton() {
        IMIDPAPI newApi = new MIDPAPI();
        apis.add(newApi);
        viewer.refresh();
    }

    /**
     * Handle the remove button being pressed.
     */
    private void handleRemoveButton() {
        IAPI selected = getSelectedAPI();
        if (selected != null) {
            apis.remove(selected);
            viewer.refresh();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
     */
    @Override
    protected void buttonPressed(int buttonId) {
        if (buttonId == IDialogConstants.CANCEL_ID) {
            apis = null;
        }

        super.buttonPressed(buttonId);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
     */
    @Override
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);

        String title = MTJUIMessages.LibraryApiEditorDialog_title;
        newShell.setText(title);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(2, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.minimumWidth = DEFAULT_TABLE_WIDTH;
        gridData.heightHint = 400;
        viewer = createTableViewer(composite);
        viewer.getTable().setLayoutData(gridData);

        Composite buttonComposite = new Composite(composite, SWT.NONE);
        buttonComposite.setLayout(new GridLayout(1, true));
        buttonComposite.setLayoutData(new GridData(GridData.FILL_VERTICAL));

        Button addButton = new Button(buttonComposite, SWT.PUSH);
        addButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        addButton
                .setText(MTJUIMessages.LibraryApiEditorDialog_addButton_label_text);
        addButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleAddButton();
            }
        });

        final Button removeButton = new Button(buttonComposite, SWT.PUSH);
        removeButton.setEnabled(false);
        removeButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        removeButton
                .setText(MTJUIMessages.LibraryApiEditorDialog_removeButton_label_text);
        removeButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleRemoveButton();
            }
        });

        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                IAPI api = getSelectedAPI();
                removeButton.setEnabled(api != null);
            }
        });

        return composite;
    }
}
