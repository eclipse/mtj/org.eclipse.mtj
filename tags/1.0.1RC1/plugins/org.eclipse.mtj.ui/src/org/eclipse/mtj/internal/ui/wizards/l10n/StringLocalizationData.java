/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.ui.wizards.l10n;

import org.eclipse.jface.text.IRegion;

/**
 * StringLocalizationData class represents an extracted region
 * of a buffer that will be replaced by a localization key.
 * 
 * @author David Marques
 */
public class StringLocalizationData {

    private IRegion region;
    private String  key;
    private String  value;

    /**
     * Creates a StringLocalizationData instance to replace the
     * specified value within the specified region by the
     * localized key.
     * 
     * @param _region buffer region.
     * @param _key l10n key.
     * @param _value region's value.
     */
    public StringLocalizationData(IRegion _region, String _key, String _value) {
        this.region = _region;
        this.key = _key;
        this.value = _value;
    }

    /**
     * Gets the code region.
     * 
     * @return region.
     */
    public IRegion getRegion() {
        return region;
    }

    /**
     * Gets the code value.
     * 
     * @return value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the key for the
     * value.
     * 
     * @param key key value.
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Gets the l10n key.
     * 
     * @return key value.
     */
    public String getKey() {
        return key;
    }
}
