/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n;

import org.eclipse.mtj.internal.core.IBaseModel;
import org.eclipse.mtj.internal.ui.editor.MTJFormEditor;
import org.eclipse.mtj.internal.ui.editor.context.InputContext;
import org.eclipse.mtj.internal.ui.editor.context.InputContextManager;

/**
 * @author Diego Madruga Sandin
 */
public class L10nInputContextManager extends InputContextManager {

    /**
     * @param editor
     */
    public L10nInputContextManager(MTJFormEditor editor) {
        super(editor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.InputContextManager#getAggregateModel()
     */
    @Override
    public IBaseModel getAggregateModel() {
        InputContext context = findContext(L10nInputContext.CONTEXT_ID);
        if (context == null) {
            return null;
        }
        return context.getModel();
    }

}
