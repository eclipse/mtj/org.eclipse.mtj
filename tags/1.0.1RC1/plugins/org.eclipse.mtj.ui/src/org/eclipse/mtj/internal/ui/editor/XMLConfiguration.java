/**
 * Copyright (c) 2003, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.quickassist.IQuickAssistAssistant;
import org.eclipse.jface.text.source.IAnnotationHover;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.mtj.internal.ui.editor.context.XMLDocumentSetupParticipant;
import org.eclipse.mtj.internal.ui.editor.text.AnnotationHover;
import org.eclipse.mtj.internal.ui.editor.text.BaseMTJScanner;
import org.eclipse.mtj.internal.ui.editor.text.ChangeAwareSourceViewerConfiguration;
import org.eclipse.mtj.internal.ui.editor.text.IColorManager;
import org.eclipse.mtj.internal.ui.editor.text.IMTJColorConstants;
import org.eclipse.mtj.internal.ui.editor.text.MTJQuickAssistAssistant;
import org.eclipse.mtj.internal.ui.editor.text.MultilineDamagerRepairer;
import org.eclipse.mtj.internal.ui.editor.text.SourceInformationProvider;
import org.eclipse.mtj.internal.ui.editor.text.XMLDoubleClickStrategy;
import org.eclipse.mtj.internal.ui.editor.text.XMLPartitionScanner;
import org.eclipse.mtj.internal.ui.editor.text.XMLScanner;
import org.eclipse.mtj.internal.ui.editor.text.XMLTagScanner;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;

public class XMLConfiguration extends ChangeAwareSourceViewerConfiguration {

    private AnnotationHover fAnnotationHover;
    private MultilineDamagerRepairer fDamagerRepairer;
    private XMLDoubleClickStrategy fDoubleClickStrategy;
    private XMLScanner fPdeScanner;

    private MTJQuickAssistAssistant fQuickAssistant;
    private XMLTagScanner fTagScanner;
    private TextAttribute fXMLCommentAttr;

    /**
     * @param colorManager
     */
    public XMLConfiguration(IColorManager colorManager) {
        this(colorManager, null);
    }

    /**
     * @param colorManager
     * @param page
     */
    public XMLConfiguration(IColorManager colorManager, MTJSourcePage page) {
        super(page, colorManager);
    }

    /**
     * Preference colors or fonts have changed. Update the default tokens of the
     * scanners.
     */
    @Override
    public void adaptToPreferenceChange(PropertyChangeEvent event) {
        if (fTagScanner == null) {
            return; // property change before the editor is fully created
        }
        if (affectsColorPresentation(event)) {
            fColorManager.handlePropertyChangeEvent(event);
        }
        fTagScanner.adaptToPreferenceChange(event);
        fPdeScanner.adaptToPreferenceChange(event);
        String property = event.getProperty();
        if (property.startsWith(IMTJColorConstants.P_XML_COMMENT)) {
            adaptTextAttribute(event);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.text.ChangeAwareSourceViewerConfiguration#affectsColorPresentation(org.eclipse.jface.util.PropertyChangeEvent)
     */
    @Override
    public boolean affectsColorPresentation(PropertyChangeEvent event) {
        String property = event.getProperty();
        return property.equals(IMTJColorConstants.P_DEFAULT)
                || property.equals(IMTJColorConstants.P_PROC_INSTR)
                || property.equals(IMTJColorConstants.P_STRING)
                || property.equals(IMTJColorConstants.P_TAG)
                || property.equals(IMTJColorConstants.P_XML_COMMENT);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.text.ChangeAwareSourceViewerConfiguration#affectsTextPresentation(org.eclipse.jface.util.PropertyChangeEvent)
     */
    @Override
    public boolean affectsTextPresentation(PropertyChangeEvent event) {
        String property = event.getProperty();
        return property.startsWith(IMTJColorConstants.P_DEFAULT)
                || property.startsWith(IMTJColorConstants.P_PROC_INSTR)
                || property.startsWith(IMTJColorConstants.P_STRING)
                || property.startsWith(IMTJColorConstants.P_TAG)
                || property.startsWith(IMTJColorConstants.P_XML_COMMENT);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.text.ChangeAwareSourceViewerConfiguration#dispose()
     */
    @Override
    public void dispose() {
        if (fQuickAssistant != null) {
            fQuickAssistant.dispose();
        }
    }

    @Override
    public IAnnotationHover getAnnotationHover(ISourceViewer sourceViewer) {
        if (fAnnotationHover == null) {
            fAnnotationHover = new AnnotationHover();
        }
        return fAnnotationHover;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getConfiguredContentTypes(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
        return new String[] { IDocument.DEFAULT_CONTENT_TYPE,
                XMLPartitionScanner.XML_COMMENT, XMLPartitionScanner.XML_TAG };
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getConfiguredDocumentPartitioning(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public String getConfiguredDocumentPartitioning(ISourceViewer sourceViewer) {
        return XMLDocumentSetupParticipant.XML_PARTITIONING;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getDoubleClickStrategy(org.eclipse.jface.text.source.ISourceViewer, java.lang.String)
     */
    @Override
    public ITextDoubleClickStrategy getDoubleClickStrategy(
            ISourceViewer sourceViewer, String contentType) {
        if (fDoubleClickStrategy == null) {
            fDoubleClickStrategy = new XMLDoubleClickStrategy();
        }
        return fDoubleClickStrategy;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getPresentationReconciler(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IPresentationReconciler getPresentationReconciler(
            ISourceViewer sourceViewer) {
        PresentationReconciler reconciler = new PresentationReconciler();
        reconciler
                .setDocumentPartitioning(XMLDocumentSetupParticipant.XML_PARTITIONING);

        MultilineDamagerRepairer dr = new MultilineDamagerRepairer(
                getMTJScanner());
        reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
        reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

        dr = new MultilineDamagerRepairer(getMTJTagScanner());
        reconciler.setDamager(dr, XMLPartitionScanner.XML_TAG);
        reconciler.setRepairer(dr, XMLPartitionScanner.XML_TAG);

        fXMLCommentAttr = BaseMTJScanner.createTextAttribute(fColorManager,
                IMTJColorConstants.P_XML_COMMENT);
        fDamagerRepairer = new MultilineDamagerRepairer(null, fXMLCommentAttr);
        reconciler
                .setDamager(fDamagerRepairer, XMLPartitionScanner.XML_COMMENT);
        reconciler.setRepairer(fDamagerRepairer,
                XMLPartitionScanner.XML_COMMENT);

        return reconciler;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.editors.text.TextSourceViewerConfiguration#getQuickAssistAssistant(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IQuickAssistAssistant getQuickAssistAssistant(
            ISourceViewer sourceViewer) {
        if (sourceViewer.isEditable()) {
            if (fQuickAssistant == null) {
                fQuickAssistant = new MTJQuickAssistAssistant();
            }
            return fQuickAssistant;
        }
        return null;
    }

    /**
     * @param event
     */
    private void adaptTextAttribute(PropertyChangeEvent event) {
        String property = event.getProperty();
        if (property.endsWith(IMTJColorConstants.P_BOLD_SUFFIX)) {
            fXMLCommentAttr = adaptToStyleChange(event, SWT.BOLD,
                    fXMLCommentAttr);
        } else if (property.endsWith(IMTJColorConstants.P_ITALIC_SUFFIX)) {
            fXMLCommentAttr = adaptToStyleChange(event, SWT.ITALIC,
                    fXMLCommentAttr);
        } else {
            fXMLCommentAttr = new TextAttribute(fColorManager.getColor(event
                    .getProperty()), fXMLCommentAttr.getBackground(),
                    fXMLCommentAttr.getStyle());
        }
        fDamagerRepairer.setDefaultTextAttribute(fXMLCommentAttr);
    }

    /**
     * @param event
     * @param styleAttribute
     * @param textAttribute
     * @return
     */
    private TextAttribute adaptToStyleChange(PropertyChangeEvent event,
            int styleAttribute, TextAttribute textAttribute) {
        boolean eventValue = false;
        Object value = event.getNewValue();
        if (value instanceof Boolean) {
            eventValue = ((Boolean) value).booleanValue();
        }

        boolean activeValue = (textAttribute.getStyle() & styleAttribute) == styleAttribute;
        if (activeValue != eventValue) {
            Color foreground = textAttribute.getForeground();
            Color background = textAttribute.getBackground();
            int style = eventValue ? textAttribute.getStyle() | styleAttribute
                    : textAttribute.getStyle() & ~styleAttribute;
            textAttribute = new TextAttribute(foreground, background, style);
        }
        return textAttribute;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.text.ChangeAwareSourceViewerConfiguration#getInfoImplementationType()
     */
    @Override
    protected int getInfoImplementationType() {
        return SourceInformationProvider.F_XML_IMP;
    }

    /**
     * @return
     */
    protected XMLScanner getMTJScanner() {
        if (fPdeScanner == null) {
            fPdeScanner = new XMLScanner(fColorManager);
        }
        return fPdeScanner;
    }

    /**
     * @return
     */
    protected XMLTagScanner getMTJTagScanner() {
        if (fTagScanner == null) {
            fTagScanner = new XMLTagScanner(fColorManager);
        }
        return fTagScanner;
    }

}
