/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 *     David Marques (Motorola) - Fixing bug.
 */
package org.eclipse.mtj.internal.ui.wizards.l10n;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jdt.core.IBuffer;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;

/**
 * RegExpStringExternalizer class implements the {@link IStringExternalizer}
 * interface based on a regular expression search for string literals.
 * 
 * @author David Marques
 */
public class RegExpStringExternalizer implements IStringExternalizer {

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.wizards.l10n.IStringExternalizer#externalize
     * (org.eclipse.jdt.core.IBuffer, boolean)
     */
    public IRegion[] externalize(IBuffer _buffer, boolean includeIgnored) {
        List<IRegion> regions = new ArrayList<IRegion>();

        Pattern pattern = Pattern.compile("\"([^\"])*\"");
        Matcher matcher = pattern.matcher(_buffer.getContents());
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();

            if (!includeIgnored) {
                if (end + IStringExternalizer.NON_NLS.length() < _buffer
                        .getLength()) {
                    String data = _buffer.getText(end,
                            IStringExternalizer.NON_NLS.length());
                    if (!data.equals(IStringExternalizer.NON_NLS)) {
                        regions.add(new Region(start, end - start));
                    }
                } else {
                    regions.add(new Region(start, end - start));
                }
            } else {
                regions.add(new Region(start, end - start));
            }
        }
        return regions.toArray(new IRegion[0x00]);
    }
}
