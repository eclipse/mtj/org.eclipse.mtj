/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.ui.wizards.l10n;

import org.eclipse.jdt.core.IBuffer;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * ClassFileStringsLabelProvider class implements the
 * {@link ITableLabelProvider} interface in order to
 * extract content from a {@link StringLocalizationData}.
 * 
 * @author David Marques
 */
public class ClassFileStringsLabelProvider implements ITableLabelProvider {

    private IBuffer buffer;

    /**
     * Creates a ClassFileStringsLabelProvider instance
     * to read Strings from the specified {@link IBuffer}.
     * 
     * @param _buffer source buffer.
     */
    public ClassFileStringsLabelProvider(IBuffer _buffer) {
        this.buffer = _buffer;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
     */
    public String getColumnText(Object element, int columnIndex) {
        String result = null;

        if (element instanceof StringLocalizationData) {
            StringLocalizationData data = (StringLocalizationData) element;
            switch (columnIndex) {
                case 0x00:
                    IRegion region = data.getRegion();
                    result = this.buffer.getText(region.getOffset(),
                            region.getLength()).replaceAll("\"", "");
                break;
                case 0x01:
                    result = data.getKey();
                break;
            }
        }
        return result;
    }

    public Image getColumnImage(Object element, int columnIndex) {
        return null;
    }
    
    public boolean isLabelProperty(Object element, String property) {
        return false;
    }

    public void addListener(ILabelProviderListener listener) {
    }

    public void removeListener(ILabelProviderListener listener) {
    }

    public void dispose() {
    }
}
