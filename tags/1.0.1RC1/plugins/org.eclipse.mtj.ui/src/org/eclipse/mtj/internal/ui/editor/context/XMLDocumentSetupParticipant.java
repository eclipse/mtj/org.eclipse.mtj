/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.context;

import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentExtension3;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.rules.FastPartitioner;
import org.eclipse.mtj.internal.ui.editor.text.XMLPartitionScanner;

/**
 * @since 0.9.1
 */
public class XMLDocumentSetupParticipant implements IDocumentSetupParticipant {

    public static final String XML_PARTITIONING = "_mtj_xml_partitioning"; //$NON-NLS-1$

    /* (non-Javadoc)
     * @see org.eclipse.core.filebuffers.IDocumentSetupParticipant#setup(org.eclipse.jface.text.IDocument)
     */
    public void setup(IDocument document) {
        IDocumentPartitioner partitioner = createDocumentPartitioner();
        if (partitioner != null) {
            partitioner.connect(document);
            if (document instanceof IDocumentExtension3) {
                IDocumentExtension3 de3 = (IDocumentExtension3) document;
                de3.setDocumentPartitioner(XML_PARTITIONING, partitioner);
            } else {
                document.setDocumentPartitioner(partitioner);
            }
        }
    }

    /**
     * @return
     */
    private IDocumentPartitioner createDocumentPartitioner() {
        return new FastPartitioner(new XMLPartitionScanner(),
                XMLPartitionScanner.PARTITIONS);
    }

}
