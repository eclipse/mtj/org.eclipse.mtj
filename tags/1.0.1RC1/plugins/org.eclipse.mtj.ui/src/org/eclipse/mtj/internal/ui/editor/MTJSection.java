/**
 * Copyright (c) 2006, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDESection
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.mtj.internal.core.IBaseModel;
import org.eclipse.mtj.internal.core.IModelChangedEvent;
import org.eclipse.mtj.internal.core.IModelChangedListener;
import org.eclipse.mtj.internal.ui.editors.FormLayoutFactory;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.SectionPart;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

/**
 * @since 0.9.1
 */
public abstract class MTJSection extends SectionPart implements
        IModelChangedListener, IContextPart, IAdaptable {

    private MTJFormPage fPage;

    /**
     * Creates a new MTJSection.
     * 
     * @param page
     * @param parent
     * @param style
     */
    public MTJSection(MTJFormPage page, Composite parent, int style) {
        this(page, parent, style, true);
    }

    /**
     * Creates a new MTJSection.
     * 
     * @param page
     * @param parent
     * @param style
     * @param titleBar
     */
    public MTJSection(MTJFormPage page, Composite parent, int style,
            boolean titleBar) {
        super(parent, page.getManagedForm().getToolkit(),
                titleBar ? (ExpandableComposite.TITLE_BAR | style) : style);
        fPage = page;
        initialize(page.getManagedForm());
        getSection().clientVerticalSpacing = FormLayoutFactory.SECTION_HEADER_VERTICAL_SPACING;
        getSection().setData("part", this); //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.IContextPart#cancelEdit()
     */
    public void cancelEdit() {
        super.refresh();
    }

    /**
     * Sub-classes to override.
     * 
     * @param selection
     * @return
     */
    public boolean canCopy(ISelection selection) {
        return false;
    }

    /**
     * Sub-classes to override.
     * 
     * @param selection
     * @return
     */
    public boolean canCut(ISelection selection) {
        return false;
    }

    /**
     * Sub-classes to override.
     * 
     * @param clipboard
     * @return
     */
    public boolean canPaste(Clipboard clipboard) {
        return false;
    }

    /**
     * Sub-classes to override.
     * 
     * @param actionId
     * @return
     */
    public boolean doGlobalAction(String actionId) {
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.IContextPart#fireSaveNeeded()
     */
    public void fireSaveNeeded() {
        markDirty();
        if (getContextId() != null) {
            getPage().getMTJEditor().fireSaveNeeded(getContextId(), false);
        }
    }

    @SuppressWarnings("unchecked")
    public Object getAdapter(Class adapter) {
        return null;
    }

    public String getContextId() {
        return null;
    }

    public MTJFormPage getPage() {
        return fPage;
    }

    public boolean isEditable() {
        // getAggregateModel() can (though never should) return null
        IBaseModel model = getPage().getMTJEditor().getAggregateModel();
        return model == null ? false : model.isEditable();
    }

    public void modelChanged(IModelChangedEvent e) {
        if (e.getChangeType() == IModelChangedEvent.WORLD_CHANGED) {
            markStale();
        }
    }

    protected abstract void createClient(Section section, FormToolkit toolkit);

    protected IProject getProject() {
        return fPage.getMTJEditor().getCommonProject();
    }
}
