/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Extracted interface                     
 */
package org.eclipse.mtj.ui.editors.jad;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.jface.preference.IPersistentPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;

/**
 * An IPreferenceStore implementation that reads and writes the information as a
 * Manifest format file.
 * 
 * @since 1.0
 * @noimplement This class is not intended to be implemented by clients.
 */
public interface IManifestPreferenceStore extends IPersistentPreferenceStore {

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#addPropertyChangeListener(org.eclipse.jface.util.IPropertyChangeListener)
     */
    public abstract void addPropertyChangeListener(
            IPropertyChangeListener listener);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#contains(java.lang.String)
     */
    public abstract boolean contains(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#firePropertyChangeEvent(java.lang.String, java.lang.Object, java.lang.Object)
     */
    public abstract void firePropertyChangeEvent(String name, Object oldValue,
            Object newValue);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getBoolean(java.lang.String)
     */
    public abstract boolean getBoolean(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getDefaultBoolean(java.lang.String)
     */
    public abstract boolean getDefaultBoolean(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getDefaultDouble(java.lang.String)
     */
    public abstract double getDefaultDouble(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getDefaultFloat(java.lang.String)
     */
    public abstract float getDefaultFloat(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getDefaultInt(java.lang.String)
     */
    public abstract int getDefaultInt(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getDefaultLong(java.lang.String)
     */
    public abstract long getDefaultLong(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getDefaultString(java.lang.String)
     */
    public abstract String getDefaultString(String name);


    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getDouble(java.lang.String)
     */
    public abstract double getDouble(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getFloat(java.lang.String)
     */
    public abstract float getFloat(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getInt(java.lang.String)
     */
    public abstract int getInt(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getLong(java.lang.String)
     */
    public abstract long getLong(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#getString(java.lang.String)
     */
    public abstract String getString(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#isDefault(java.lang.String)
     */
    public abstract boolean isDefault(String name);

    /**
     * Loads this preference store from the file established in the constructor
     * <code>PreferenceStore(java.lang.String)</code> (or by
     * <code>setFileName</code>). Default preference values are not affected.
     * 
     * @exception java.io.IOException if there is a problem loading this store
     */
    public abstract void load() throws IOException;

    /**
     * Loads this preference store from the given input stream. Default
     * preference values are not affected.
     * 
     * @param in the input stream
     * @exception java.io.IOException if there is a problem loading this store
     */
    public abstract void load(InputStream in) throws IOException;

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#needsSaving()
     */
    public abstract boolean needsSaving();

    /**
     * Returns an array of all preferences known to this store which have
     * current values other than their default value.
     * 
     * @return an array of preference names
     */
    public abstract String[] preferenceNames();


    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#putValue(java.lang.String, java.lang.String)
     */
    public abstract void putValue(String name, String value);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#removePropertyChangeListener(org.eclipse.jface.util.IPropertyChangeListener)
     */
    public abstract void removePropertyChangeListener(
            IPropertyChangeListener listener);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPersistentPreferenceStore#save()
     */
    public abstract void save() throws IOException;

    /**
     * Saves this preference store to the given output stream. The given string
     * is inserted as header information.
     * 
     * @param out the output stream
     * @exception java.io.IOException if there is a problem saving this store
     */
    public abstract void save(OutputStream out) throws IOException;

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setDefault(java.lang.String, boolean)
     */
    public abstract void setDefault(String name, boolean value);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setDefault(java.lang.String, double)
     */
    public abstract void setDefault(String name, double value);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setDefault(java.lang.String, float)
     */
    public abstract void setDefault(String name, float value);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setDefault(java.lang.String, int)
     */
    public abstract void setDefault(String name, int value);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setDefault(java.lang.String, long)
     */
    public abstract void setDefault(String name, long value);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setDefault(java.lang.String, java.lang.String)
     */
    public abstract void setDefault(String name, String defaultObject);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setToDefault(java.lang.String)
     */
    public abstract void setToDefault(String name);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setValue(java.lang.String, boolean)
     */
    public abstract void setValue(String name, boolean value);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setValue(java.lang.String, double)
     */
    public abstract void setValue(String name, double value);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setValue(java.lang.String, float)
     */
    public abstract void setValue(String name, float value);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setValue(java.lang.String, int)
     */
    public abstract void setValue(String name, int value);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setValue(java.lang.String, long)
     */
    public abstract void setValue(String name, long value);

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.IPreferenceStore#setValue(java.lang.String, java.lang.String)
     */
    public abstract void setValue(String name, String value);

}