/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.text;

import org.eclipse.jface.text.rules.IWhitespaceDetector;

/**
 * Determines whether a given character is to be considered whitespace in the
 * current context.
 * 
 * @since 0.9.1
 */
public class XMLWhitespaceDetector implements IWhitespaceDetector {

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.rules.IWhitespaceDetector#isWhitespace(char)
     */
    public boolean isWhitespace(char c) {
        return (c == ' ' || c == '\t' || c == '\n' || c == '\r');
    }

}
