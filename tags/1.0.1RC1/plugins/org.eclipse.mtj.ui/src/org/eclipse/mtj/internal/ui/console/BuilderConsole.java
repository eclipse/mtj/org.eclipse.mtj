/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874] 
 */
package org.eclipse.mtj.internal.ui.console;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.debug.ui.console.ConsoleColorProvider;
import org.eclipse.debug.ui.console.IConsoleColorProvider;
import org.eclipse.mtj.internal.core.build.BuildConsoleProxy;
import org.eclipse.mtj.internal.core.build.IBuildConsoleProxy;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.console.IOConsole;
import org.eclipse.ui.console.IOConsoleOutputStream;

/**
 * A console that holds the information from the execution of the MTJ builders.
 * 
 * @author Craig Setera
 */
public class BuilderConsole extends IOConsole implements IBuildConsoleProxy {

    private static final String CONSOLE_TYPE = "MTJBuildConsole"; //$NON-NLS-1$

    private IConsoleColorProvider colorProvider;
    private Map<Stream, Color> colors;
    private Map<Stream, PrintWriter> writers;

    /**
     * Create the BuilderConsole.
     */
    public BuilderConsole() {
        super(MTJUIMessages.BuilderConsole_name, CONSOLE_TYPE, null, true);

        colorProvider = new ConsoleColorProvider();
        BuildConsoleProxy.getInstance().setProxy(this);

        writers = new HashMap<Stream, PrintWriter>(3);
        colors = new HashMap<IBuildConsoleProxy.Stream, Color>(3);
        colors.put(Stream.ERROR, colorProvider
                .getColor(IDebugUIConstants.ID_STANDARD_ERROR_STREAM));
        colors.put(Stream.OUTPUT, colorProvider
                .getColor(IDebugUIConstants.ID_STANDARD_OUTPUT_STREAM));
        colors.put(Stream.TRACE, new Color(Display.getDefault(), 0, 128, 128));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.console.IBuildConsoleProxy#getConsoleWriter(java.lang.String)
     */
    public PrintWriter getConsoleWriter(Stream id) {
        PrintWriter writer = writers.get(id);
        if (writer == null) {
            final Color color = getColor(id);
            final IOConsoleOutputStream stream = newOutputStream();

            writer = new PrintWriter(stream);
            writers.put(id, writer);

            if (color != null) {
                Display.getDefault().asyncExec(new Runnable() {
                    public void run() {
                        stream.setColor(color);
                    }
                });
            }
        }
        return writer;
    }

    /**
     * Return the color to be used for the specified console writer.
     * 
     * @param id
     * @return
     */
    private Color getColor(Stream id) {
        return colors.get(id);
    }
}
