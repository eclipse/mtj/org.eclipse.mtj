/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)       - Initial implementation
 *     David Marques (Motorola) - Fixing devices comparison.
 *     Daniel Olsson (Sony Ericsson) - Matching of devices  [Bug - 284262]
 *******************************************************************************/

package org.eclipse.mtj.internal.core.project.runtime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.IMetaData;
import org.eclipse.mtj.core.project.ProjectType;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.project.midp.MetaData;
import org.eclipse.mtj.internal.core.sdk.device.IDeviceMatcher;

/**
 * Provides Configuration related utils.
 * 
 * @since 1.0
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 */
public class MTJRuntimeListUtils {

    /**
     * 
     */
    public static final String MATCH_ATT_CLASS = "class"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String MATCH_EXTENSIONPOINT_ID = "org.eclipse.mtj.core.devicematcher"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String MATCH_ELEMENT_MATCHER = "matcher"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String MATCH_ATT_PRIORITY = "priority"; //$NON-NLS-1$

    /**
     * The same as
     * {@link MTJRuntimeListUtils#mtjRuntimeListEquals(MTJRuntime, MTJRuntime)}
     * , except ignore active configuration. This means, if two Configuration
     * are same except that active configuration are different, also return
     * true.
     * 
     * @param cs1
     * @param cs2
     * @return
     */
    public static boolean mtjRuntimeListContentsEquals(MTJRuntimeList cs1,
            MTJRuntimeList cs2) {
        if (cs1.size() != cs2.size()) {
            return false;
        }
        for (MTJRuntime cInCs1 : cs1) {
            int index = cs2.indexOf(cInCs1);
            if (index < 0) {
                return false;
            }
            MTJRuntime cInCs2 = cs2.get(index);
            if (!deviceEquals(cInCs1, cInCs2)) {
                return false;
            }
            if (!workspaceSymbolsetsEquals(
                    cInCs1.getWorkspaceScopeSymbolSets(), cInCs2
                            .getWorkspaceScopeSymbolSets())) {
                return false;
            }
            if (!symbolSetEquals(cInCs1, cInCs2)) {
                return false;
            }

        }
        return true;
    }

    /**
     * Determine if two Configuration are totally equal.<br>
     * If<br>
     * 1, two Configuration have same size and<br>
     * 2, both contains configuration with same name and<br>
     * 3, configuration with the same name have same device and same symbols,<br>
     * this method will return true and<br>
     * 4, two Configuration have same active configuration.<br>
     * In which same symbols means two SymbolSet have same size, and both have
     * symbols with the same name, and symbols with the same name have same
     * value.
     * 
     * @param cs1
     * @param cs2
     * @return
     */
    public static boolean mtjRuntimeListEquals(MTJRuntimeList cs1,
            MTJRuntimeList cs2) {
        if (!mtjRuntimeListContentsEquals(cs1, cs2)) {
            return false;
        }
        if (!cs1.getActiveMTJRuntime().equals(cs2.getActiveMTJRuntime())) {
            return false;
        }
        return true;
    }

    /**
     * Determine if devices in two Configuration is the same instance.
     * 
     * @param cInCs1
     * @param cInCs2
     * @return
     */
    private static boolean deviceEquals(MTJRuntime cInCs1, MTJRuntime cInCs2) {
        return cInCs1.getDevice().equals(cInCs2.getDevice());
    }

    /**
     * If Configuration have any modification that not save to metadata file,
     * return true.
     * 
     * @param midletProject
     * @return
     */
    public static boolean isMTJRuntimeListDirty(IMTJProject midletProject) {
        MTJRuntimeList configsInMemory = midletProject.getRuntimeList();

        boolean isDirty = true;

        IMetaData metaData = MTJCore.createMetaData(midletProject.getProject(),
                ProjectType.MIDLET_SUITE);

        MTJRuntimeList configsInMetadataFile = metaData.getRuntimeList();
        isDirty = !mtjRuntimeListEquals(configsInMemory, configsInMetadataFile);

        return isDirty;
    }

    /**
     * If you only change active configuration(any other contents not change)
     * and not save it to metadata file, return true.
     * 
     * @param midletProject
     * @return
     */
    public static boolean isOnlyActiveMTJRuntimeDirty(IMTJProject midletProject) {
        MTJRuntimeList configsInMemory = midletProject.getRuntimeList();
        MTJRuntimeList configsInMetadataFile = new MetaData(midletProject
                .getProject()).getRuntimeList();
        boolean same = mtjRuntimeListContentsEquals(configsInMemory,
                configsInMetadataFile);
        return same;
    }

    /**
     * If<br>
     * 1, two SymbolSet have the same size and<br>
     * 2, both SymbolSet contains the symbol with a certain name and<br>
     * 3, two symbol with the same name have the same value,<br>
     * return true.
     * 
     * @param cInCs1
     * @param cInCs2
     * @return
     */
    private static boolean symbolSetEquals(MTJRuntime cInCs1, MTJRuntime cInCs2) {
        ISymbolSet symbolSet1 = cInCs1.getSymbolSet();
        ISymbolSet symbolSet2 = cInCs2.getSymbolSet();
        
        if (!symbolSet1.equals(symbolSet2)) {
            return false;
        }
        List<ISymbol> symbolList1 = new ArrayList<ISymbol>(symbolSet1
                .getSymbols());
        List<ISymbol> symbolList2 = new ArrayList<ISymbol>(symbolSet2
                .getSymbols());

        for (Iterator<ISymbol> iterator = symbolList1.iterator(); iterator
                .hasNext();) {
            ISymbol symbol = (ISymbol) iterator.next();

            if (!symbolList2.contains(symbol))
                return false;
        }

        return true;
    }

    public static boolean workspaceSymbolsetsEquals(List<ISymbolSet> sets1,
            List<ISymbolSet> sets2) {
        if ((sets1 == null) && (sets2 == null)) {
            return true;
        }
        if ((sets1 == null) && (sets2 != null)) {
            return false;
        }
        if ((sets1 != null) && (sets2 == null)) {
            return false;
        }
        if (sets1.size() != sets2.size()) {
            return false;
        }
        for (ISymbolSet s : sets1) {
            if (!sets2.contains(s)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Match an existing device against the given device group and device name.
     * This method is intended to change the given MTJRuntime instance.
     * 
     * @param configurationName name of the MTJRuntime
     * @param deviceGroup name of the SDK to match
     * @param deviceName name of the device to match
     * @return String[] consisting of [&ltnew configuration name&gt, &ltSDK&gt,
     *         &ltdevice&gt], <b>null</b> if no match
     * @see {@link MTJRuntime}
     */
    public static String[] match(String configName, String sdkName,
            String deviceName) {
        List<IConfigurationElement> matchers = getMatcherExtensions();
        if (matchers == null)
            return null;
        sortElementsByPriority(matchers);
        return executeMatchers(matchers, configName, sdkName, deviceName);
    }

    /**
     * Match the non-found device against available devices (done through
     * extension points)
     * 
     * @param sdkName The sdk name as a string
     * @param deviceName The device name as a string
     * @return The matched IDevice, <b>null</b> if not found
     */
    public static IDevice match(String sdkName, String deviceName) {
        List<IConfigurationElement> matchers = getMatcherExtensions();
        if (matchers == null)
            return null;
        sortElementsByPriority(matchers);
        return executeMatchers(matchers, sdkName, deviceName);
    }

    /**
     * Execute the found matchers (implementing {@link IDeviceMatcher}) in
     * priority order until a match is found
     * 
     * @param matchers A list of found matchers from extension points
     * @param configName The configuration name as a string
     * @param sdkName The sdk name as a string
     * @param deviceName The device name as a string
     * @return true if matched, false otherwise
     * @see {@link IDeviceMatcher}
     */
    private static String[] executeMatchers(
            List<IConfigurationElement> matchers, String configName,
            String sdkName, String deviceName) {
        String[] matched = null;
        Iterator<IConfigurationElement> iter = matchers.iterator();
        while (iter.hasNext()) {
            IConfigurationElement elem = iter.next();
            try {
                IDeviceMatcher matcher = (IDeviceMatcher) elem
                        .createExecutableExtension(MTJRuntimeListUtils.MATCH_ATT_CLASS);
                matched = matcher.match(configName, sdkName, deviceName);
                if (matched != null) {
                    break;
                }
            } catch (CoreException e) {
                e.printStackTrace();
            }
        }
        return matched;
    }

    /**
     * Execute the found matchers (implementing {@link IDeviceMatcher}) in
     * priority order until a match is found
     * 
     * @param matchers A list of found matchers from extension points
     * @param sdkName SDK string to match
     * @param Device string to match
     * @return IDevice that got matched, <b>null</b> if no match
     * @see {@link IDeviceMatcher}
     */
    private static IDevice executeMatchers(
            List<IConfigurationElement> matchers, String sdkName,
            String deviceName) {
        IDevice device = null;
        Iterator<IConfigurationElement> iter = matchers.iterator();
        while (iter.hasNext()) {
            IConfigurationElement elem = iter.next();
            try {
                IDeviceMatcher matcher = (IDeviceMatcher) elem
                        .createExecutableExtension(MTJRuntimeListUtils.MATCH_ATT_CLASS);
                device = matcher.match(sdkName, deviceName);
                if (device != null) {
                    break;
                }
            } catch (CoreException e) {
                e.printStackTrace();
            }
        }
        return device;
    }

    /**
     * @return The found matchers as a list of {@link IConfigurationElement}
     * @see {@link IConfigurationElement}
     */
    private static List<IConfigurationElement> getMatcherExtensions() {
        List<IConfigurationElement> matchers = new ArrayList<IConfigurationElement>();
        IExtensionRegistry registry = Platform.getExtensionRegistry();
        IExtensionPoint point = registry
                .getExtensionPoint(MTJRuntimeListUtils.MATCH_EXTENSIONPOINT_ID);
        if (point == null)
            return null;
        IExtension[] extensions = point.getExtensions();
        for (int i = 0; i < extensions.length; i++) {
            IExtension extension = extensions[i];
            IConfigurationElement[] elements = extension
                    .getConfigurationElements();
            for (int j = 0; j < elements.length; j++) {
                IConfigurationElement element = elements[j];
                if (!element.getName().equals(
                        MTJRuntimeListUtils.MATCH_ELEMENT_MATCHER)) {
                    continue;
                }
                matchers.add(element);
            }
        }
        return matchers;
    }

    /**
     * Sorts the given list of {@link IConfigurationElement} by a priority
     * attribute (given by {@link MTJRuntimeListUtils#MATCH_ATT_PRIORITY}).
     * 
     * @param matchers List of {@link IConfigurationElement} that should be
     *            sorted
     */
    private static void sortElementsByPriority(
            List<IConfigurationElement> matchers) {
        Collections.sort(matchers, new Comparator<IConfigurationElement>() {
            public int compare(IConfigurationElement e1,
                    IConfigurationElement e2) {
                // return 0 for match, -1 for less than, +1 for greater than
                int prio1 = Integer.parseInt(e1
                        .getAttribute(MTJRuntimeListUtils.MATCH_ATT_PRIORITY));
                int prio2 = Integer.parseInt(e2
                        .getAttribute(MTJRuntimeListUtils.MATCH_ATT_PRIORITY));
                if (prio1 == prio2) {
                    return 0;
                } else if (prio1 > prio1) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });
    }
}
