/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Fixing antenna export bugs.
 */
package org.eclipse.mtj.internal.core.build.export.states;

import java.io.File;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.ILibrary;
import org.eclipse.mtj.internal.core.build.export.AntennaBuildExport;
import org.eclipse.mtj.internal.core.build.export.AntennaExportException;
import org.eclipse.mtj.internal.core.statemachine.StateMachine;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * CreateAntBuildTaskState builds all sources for the target and required
 * projects.
 * 
 * @author David Marques
 * @since 1.0
 */
public class CreateAntBuildTaskState extends AbstractCreateAntTaskState {

	/**
	 * Creates a {@link CreateAntBuildTaskState} instance bound to the specified
	 * state machine in order to create build target for the specified project.
	 * 
	 * @param machine
	 *            bound {@link StateMachine} instance.
	 * @param project
	 *            target {@link IMidletSuiteProject} instance.
	 * @param _document
	 *            target {@link Document}.
	 */
	public CreateAntBuildTaskState(StateMachine machine,
			IMidletSuiteProject project, Document _document) {
		super(machine, project, _document);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mtj.internal.core.build.export.states.AbstractCreateAntTaskState
	 * #onEnter(org.eclipse.mtj.core.project.runtime.MTJRuntime)
	 */
	protected void onEnter(MTJRuntime runtime) throws AntennaExportException {
		Document document = getDocument();
		Element root = document.getDocumentElement();
		String configName = getFormatedName(runtime.getName());

		Element build = XMLUtils.createTargetElement(document, root, NLS.bind(
				"build-{0}", configName), getDependencies(runtime)); //$NON-NLS-1$

		Set<IProject> requiredProjects = getRequiredProjects(getMidletSuiteProject()
				.getProject());
		if (requiredProjects.size() > 0x00) {
			Element copy = document.createElement("copy"); //$NON-NLS-1$
			build.appendChild(copy);

			String values[] = new String[] { AntennaBuildExport.BUILD_FOLDER,
					configName,
					getFormatedName(getMidletSuiteProject().getProject().getName()) };
			copy.setAttribute("todir", NLS.bind("{0}/{1}/{2}/resources/", values)); //$NON-NLS-1$ //$NON-NLS-2$
			
			for (IProject required : requiredProjects) {
				Element fileset = document.createElement("fileset"); //$NON-NLS-1$
				copy.appendChild(fileset);
				fileset.setAttribute("dir", NLS.bind("{0}/{1}/{2}/resources/", //$NON-NLS-1$ //$NON-NLS-2$
						new String[] { AntennaBuildExport.BUILD_FOLDER, configName,
						getFormatedName(required.getName()) }));
			}
		}

		IDevice device = runtime.getDevice();
		createWtkBuildTarget(build, configName, device);
	}

	/**
	 * Creates the build target for the specified runtime bound to the specified
	 * {@link IDevice} instance.
	 * 
	 * @param parent
	 *            parent {@link Element}.
	 * @param configName
	 *            current configuration.
	 * @param device
	 *            configuration's device.
	 */
	private void createWtkBuildTarget(Element parent, String configName,
			IDevice device) {
		Document document = getDocument();

		StringBuffer buffer = new StringBuffer();
		IDeviceClasspath classpath = device.getClasspath();
		for (ILibrary library : classpath.getEntries()) {
			File file = library.toFile();
			buffer.append(file.getAbsolutePath()).append(":"); //$NON-NLS-1$
		}

		String values[] = new String[] { AntennaBuildExport.BUILD_FOLDER,
				configName,
				getFormatedName(getMidletSuiteProject().getProject().getName()) };

		Element wtkBuild = document.createElement("wtkbuild"); //$NON-NLS-1$
		wtkBuild.setAttribute("bootclasspath", buffer.toString()); //$NON-NLS-1$
		wtkBuild.setAttribute("destdir", NLS.bind("{0}/{1}/{2}/bin/", values)); //$NON-NLS-1$ //$NON-NLS-2$
		wtkBuild.setAttribute("encoding", "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$
		wtkBuild.setAttribute("source", "1.3"); //$NON-NLS-1$ //$NON-NLS-2$
		wtkBuild.setAttribute("sourcepath", ""); //$NON-NLS-1$ //$NON-NLS-2$
		wtkBuild.setAttribute(
				"srcdir", NLS.bind("{0}/{1}/{2}/classes/", values)); //$NON-NLS-1$ //$NON-NLS-2$
		parent.appendChild(wtkBuild);
	}

}
