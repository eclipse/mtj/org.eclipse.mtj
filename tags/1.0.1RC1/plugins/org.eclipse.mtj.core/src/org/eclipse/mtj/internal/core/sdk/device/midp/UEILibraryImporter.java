/**
 * Copyright (c) 2003, 2009 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Gang Ma 		(Sybase)	- Add javadoc automatically search for library
 */
package org.eclipse.mtj.internal.core.sdk.device.midp;

import java.io.File;
import java.util.List;

import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.ProjectType;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.core.sdk.device.ILibraryImporter;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary;
import org.eclipse.mtj.internal.core.sdk.device.JavadocDetector;

/**
 * A helper class for the importing of a MIDPLibrary instance based on a
 * java.io.File.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @author Craig Setera
 */
public class UEILibraryImporter implements ILibraryImporter {

    public static final String ID = "UEI_LIBRARY_IMPORTER";

    // a javadoc detector
    JavadocDetector javadocDetector;

    /**
     * Construct a new library importer.
     */
    public UEILibraryImporter() {
        super();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibraryImporter#createLibraryFor(java.io.File)
     */
    public IMIDPLibrary createLibraryFor(File libraryFile) {
        List<IAPI> apis = (List<IAPI>) APIRegistry.getAPIs(libraryFile);

        IMIDPLibrary library = (IMIDPLibrary) MTJCore
                .createNewLibrary(ProjectType.MIDLET_SUITE);

        library.setApis(apis);
        library.setLibraryFile(libraryFile);

        if (javadocDetector != null)
            library.setJavadocURL(javadocDetector.detectJavadoc(library));
        return library;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibraryImporter#createLibraryFor(java.io.File, java.util.List)
     */
    public IMIDPLibrary createLibraryFor(File libraryFile,
            List<IAccessRule> accessRules) {
        List<IAPI> apis = APIRegistry.getAPIs(libraryFile);

        IMIDPLibrary library = (IMIDPLibrary) MTJCore
                .createNewLibrary(ProjectType.MIDLET_SUITE);
        library.setAccessRules(accessRules);
        library.setApis(apis);
        library.setLibraryFile(libraryFile);

        return library;
    }

    /**
     * Set the {@link JavadocDetector} implementation to be used to detect
     * javadoc location.
     * 
     * @param javadocDetector the {@link JavadocDetector} implementation to be
     *            used to detect javadoc location.
     */
    public void setJavadocDetector(JavadocDetector javadocDetector) {
        this.javadocDetector = javadocDetector;
    }

}
