/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.core.build;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.project.IMTJProject;

/**
 * IMTJBuildHook Interface provides a way for third party components to hook
 * themselves to the MTJ project's build process. The build process has several
 * states and the hook is called upon every state transition in order to
 * eventually do some action. <br>
 * <strong>NOTE:</strong> The hook callback must return as soon as it ends its
 * action if any, so it won't represent an overhead to the build process.
 * 
 * @author David Marques
 * @since 1.0
 */
public interface IMTJBuildHook {

    /**
     * This method is called upon state transitions within the MTJ build
     * process. This method implementation <strong>must</strong> be lightweight
     * in order to avoid introducing overhead to the build process. In case the
     * hook has nothing to do on the new state it <strong>must</strong> return
     * as soon as possible.
     * <p>
     * The project instance passed during the invocation of this method provides
     * everything a build hook might require, including access to all resources
     * available.
     * </p>
     * <p>
     * A progress monitor is provided to report in the user interface the
     * current statuses inside the hook.<br>
     * <strong>Note</strong>: Progress messages should be "user readable" to be
     * displayed in the User Interface.
     * </p>
     * <p>
     * In case an error has occurred, clients implementing this interface must
     * throw a {@link CoreException} that will be treated by MTJ and correctly
     * displayed. <br>
     * <strong>Note</strong>: Exception messages should be "user readable" to be
     * displayed in the User Interface.
     * </p>
     * 
     * @param project the {@link IMTJProject} being built.
     * @param state new build state. For more info on available build states see
     *            {@link MTJBuildState}.
     * @param monitor a progress monitor.
     * @throws CoreException any error occurred.
     * @see org.eclipse.mtj.core.build.MTJBuildState
     */
    public void buildStateChanged(IMTJProject project, MTJBuildState state,
            IProgressMonitor monitor) throws CoreException;

}
