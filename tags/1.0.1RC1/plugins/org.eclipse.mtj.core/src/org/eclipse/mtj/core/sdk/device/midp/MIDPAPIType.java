/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk.device.midp;

import org.eclipse.mtj.internal.core.sdk.device.midp.MIDPAPI;

/**
 * This enumeration represents the four types of {@link IMIDPAPI} possible.
 * 
 * @author Diego Sandin
 * @since 1.0
 */
public enum MIDPAPIType {

    /**
     * A configuration provides the most basic set of libraries and virtual
     * machine capabilities for a broad range of devices.
     */
    CONFIGURATION(0, "Configuration"), //$NON-NLS-1$

    /**
     * A profile is a set of APIs that support a narrower range of devices.
     */
    PROFILE(1, "Profile"), //$NON-NLS-1$

    /**
     * An optional package is a set of technology-specific APIs.
     */
    OPTIONAL(2, "Optional"), //$NON-NLS-1$

    /**
     * An Unknown type of {@link MIDPAPI}.
     */
    UNKNOWN(3, "Unknown"); //$NON-NLS-1$

    /**
     * The numerical code that represents this type.
     */
    private final int typecode;

    /**
     * The identifier that represents this type.
     */
    private final String typeid;

    /**
     * Creates a new instance of MIDPAPIType.
     * 
     * @param code the code that represents this type.
     * @param id an identifier that represents this type.
     */
    MIDPAPIType(final int code, String id) {
        typecode = code;
        typeid = id;
    }

    /**
     * Returns the numerical code that represents this type.
     * 
     * @return the numerical code that represents this type.
     */
    public int getTypecode() {
        return typecode;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return typeid;
    }

    /**
     * Return a MIDPAPIType associated with the informed code
     * 
     * @param code the code that represents an specific {@link MIDPAPIType}
     *            instance.
     * @return the MIDPAPIType instance associated with the informed code.
     */
    public static MIDPAPIType getMIDPAPITypeByCode(final int code) {
        switch (code) {
            case 0:
                return CONFIGURATION;
            case 1:
                return PROFILE;
            case 2:
                return OPTIONAL;
            default:
                return UNKNOWN;
        }
    }
}
