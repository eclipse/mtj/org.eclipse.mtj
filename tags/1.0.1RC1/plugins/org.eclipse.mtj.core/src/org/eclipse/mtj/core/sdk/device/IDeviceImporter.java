/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Diego Sandin (Motorola)  - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.sdk.device;

import java.io.File;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

/**
 * Instances of IDeviceImporter are presented with a set of directories for
 * which the instance may return one or more device instances. IDeviceImporters
 * are provided to the system via the <code>deviceImporter</code> extension
 * point.
 * <p>
 * MTJ provides 3 implementations for the <code>deviceimporter</code> E.P..
 * Bellow is the list of IDs:
 * <ul>
 * <li><b><code>org.eclipse.mtj.toolkit.uei.importer</code></b></li> responsible
 * for importing devices for all UEI compatible SDK's.
 * <li><b><code>org.eclipse.mtj.toolkit.mpowerplayer.importer</code></b></li>
 * responsible for importing devices from the MPowerPlayer SDK.
 * <li><b><code>org.eclipse.mtj.toolkit.microemu.importer</code></b></li>
 * responsible for importing devices from the MicroEmu SDK.
 * </ul>
 * </p>
 * 
 * @see IDevice
 * 
 * @since 1.0
 */
public interface IDeviceImporter {

    /**
     * The extension point for use in registering new device importers.
     */
    public static final String EXT_DEVICE_IMPORTERS = "deviceimporter"; //$NON-NLS-1$

    /**
     * Return the fully configured device instances found in the specified
     * directory or <code>null</code> if no devices are recognized by this
     * importer in this directory.
     * 
     * @param directory the directory in which the devices must be searched.
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @return a list of devices instances imported from the specified directory
     *         or an empty list if no device was found.
     * @throws CoreException if the device importer found an error occurred
     *             while searching for the devices.
     * @throws InterruptedException if the operation detects a request to
     *             cancel, using {@link IProgressMonitor#isCanceled()}, it
     *             should exit by throwing {@link InterruptedException}.
     */
    public List<IDevice> importDevices(File directory, IProgressMonitor monitor)
            throws CoreException, InterruptedException;
}
