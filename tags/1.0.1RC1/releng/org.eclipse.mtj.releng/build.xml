<project default="main">

	<target name="init">

		<record name="${basedir}/build.log" action="start" loglevel="verbose" />
		<echo message="MTJ ant build script" />

		<fail unless="buildType" message="ERROR: buildType is NOT defined" />

		<property name="subprojectName" value="mtj" />
		<property name="mapsDirectory" value="${basedir}/maps" />

		<tstamp />
		<property name="date" value="${DSTAMP}" />
		<property name="time" value="${TSTAMP}" />
		<property name="timestamp" value="${date}${time}" />

		<property name="buildId" value="${buildType}${date}" />

		<!--this naming convention used by php scripts on download server-->
		<property name="buildLabel"
		          value="${buildType}-${buildId}-${timestamp}" />

		<!-- Seting build properties. -->
		<property name="buildDirectory" value="${basedir}/src/eclipse" />

		<property name="rootScriptsDirectory" value="${basedir}" />

		<property name="buildAlias" value="${buildLabel}" />
		<property name="buildId" value="${buildAlias}" />

		<!-- Verify buildDirectory ends in eclipse. -->
		<property name="fullBuild" location="${buildDirectory}" />
		<basename file="${fullBuild}" property="lastSeg" />
		<condition property="endsInEclipse">
			<equals arg1="${lastSeg}" arg2="eclipse" />
		</condition>
		<fail message="buildDirectory's last segment is not eclipse."
		      unless="endsInEclipse" />
		<property name="jvm15.home"
		          value="/shared/dsdp/tm/ibm-java2-ppc64-50" />

		<!-- Temporary update site -->
		<property name="tmpUpdateSiteDirectory"
		          value="${buildDirectory}/tmpUpdateSite" />
		<mkdir dir="${tmpUpdateSiteDirectory}" />

		<!-- Load the SVN repository properties -->
		<property file="repoInfo.properties" />

		<!-- Load the build properties -->
		<property file="build.properties" />

	</target>

	<target name="buildAll" unless="component">
		<ant antfile="build.xml" dir="${pde.build.scripts}">
			<property name="builder" value="${basedir}/builder/main" />
		</ant>
	</target>

	<target name="buildComponent" if="component">
		<ant antfile="build.xml" dir="${pde.build.scripts}">
			<property name="builder" value="${basedir}/builder/${component}" />
		</ant>
	</target>

	<target name="main" depends="init">
		<echoproperties destfile="buildProperties_${buildType}_${buildId}.log" />
		<antcall target="buildAll" />
		<antcall target="buildComponent" />
		<antcall target="runTesting" />
		<antcall target="publishBuild" />
		<antcall target="analysis" />
		<antcall target="optimize.updatesite" />
		<antcall target="createUpdateSite" />
		<antcall target="generate.p2.metadata" />
		<antcall target="updateRSSfeed" />
	</target>

	<!-- runs a complete API analysis of an API profile relative to a baseline, 
	  including API use, binary compatibility, and bundle version number validation. -->
	<target name="analysis" if="baselinePath">

		<!-- output location where the reports will be generated -->
		<property name="analysisReportOutputFolder"
		          value="${outputDirectory}/${buildLabel}/apitools/analysis" />
		<mkdir dir="${analysisReportOutputFolder}" />

		<property name="tmpsite" value="${outputDirectory}/tmpsite/eclipse"/>
		<property name="tmpprofile" value="${outputDirectory}/tmpprofile/eclipse"/>
		<mkdir dir="${outputDirectory}/tmpsite"/>
        <mkdir dir="${outputDirectory}/tmpprofile"/>
		
        <exec executable="unzip" dir="${outputDirectory}/${buildLabel}">
            <arg line="-q dsdp-mtj-SDK-${buildId}.zip -d ${tmpsite}" />
        </exec>
		<unpackUpdateJars site="${tmpsite}" output="${tmpprofile}"/>
		
		<!-- set profile that will be compared against the reference baseline -->
		<property name="profilePath"
		          value="${tmpprofile}" />

		<!-- retrieve the API filters to be used during the analysis -->
		<property name="filtersPath" value="${buildDirectory}/apifilters" />
		<mkdir dir="${filtersPath}" />
		<copy todir="${filtersPath}">
			<fileset dir="${buildDirectory}/plugins"
			         includes="**/.settings/.api_filters" />
			<regexpmapper from="^(org.eclipse.*\/)(\.settings\/)(\.api_filters)"
			              to="\1\3" />
		</copy>

		<!-- create a backup copy of the API filters used in the build -->
		<property name="apifilterzip"
		          value="${analysisReportOutputFolder}/apifilters-${buildId}.zip" />
		<exec executable="zip" dir="${analysisReportOutputFolder}">
			<arg line="-r ${apifilterzip} ${filtersPath}" />
		</exec>

		<!-- specify the list of bundles that should be excluded from the report -->
		<property name="excludelistFilePath"
		          value="${basedir}/apitooling/exclude_list.txt" />

		<!-- set the preferences used to configure problem severities -->
		<property name="preferencesFilePath"
		          value="${basedir}/apitooling/org.eclipse.pde.api.tools.prefs" />

		<!-- API use, binary compatibility, and bundle version number validation -->
		<apitooling.analysis baseline="${baselinePath}"
		                     profile="${profilePath}"
		                     report="${analysisReportOutputFolder}"
		                     filters="${filtersPath}"
		                     excludelist="${excludelistFilePath}"
		                     debug="true" />

		<!-- convert XML reports created into HTML files -->
		<apitooling.analysis_reportconversion xmlfiles="${analysisReportOutputFolder}"
		                                      htmlfiles="${analysisReportOutputFolder}"
		                                      debug="true" />

		<!-- output location where the reports will be generated -->
		<property name="apifreezeReportOutputFolder"
		          value="${outputDirectory}/${buildLabel}/apitools/apifreeze" />
		<mkdir dir="${apifreezeReportOutputFolder}" />

		<!-- identifies APIs that have been added, modified, or removed relative to the API baseline -->
		<apitooling.apifreeze baseline="${baselinePath}"
		                      profile="${profilePath}"
		                      report="${apifreezeReportOutputFolder}/report.xml"
		                      debug="true" />

		<!-- convert XML reports created into HTML files -->
		<apitooling.apifreeze_reportconversion xmlfile="${apifreezeReportOutputFolder}/report.xml"
		                                       debug="true" />


		<property name="apifreezeReport" value="${apifreezeReportOutputFolder}/report.html"/>
		<available file="${apifreezeReport}" property="apifreezeReport.present"/>
	    <antcall target="createApifreezeReportTemplate"/>
		
		<delete dir="${tmpsite}"/>
		<delete dir="${tmpprofile}"/>	

	</target>
	
	<target name="createApifreezeReportTemplate" unless="apifreezeReport.present">
		<property name="template.folder" value="${rootScriptsDirectory}/templateFiles" />
		<property name="apifreezeReportTemplate" value="${template.folder}/apifreezeReport.html.template" />
        
		<!-- Copy the template site.xml file to the update site directory -->
        <copy file="${apifreezeReportTemplate}" tofile="${apifreezeReportOutputFolder}/report.html"  overwrite="false"/>
	       <replace file="${apifreezeReportOutputFolder}/report.html">
	            <replacefilter token="@build@" value="${buildId}" />
	        </replace>
	</target>

	<target name="publishBuild" depends="init" if="publish">
		<property name="publish.xml"
		          value="${eclipse.build.tools}/publish.xml" />

		<property name="indexFileName" value="index.html" />
		<property name="result" value="${buildDirectory}/${buildLabel}" />

		<ant antfile="${publish.xml}" dir="${basedir}/">
			<property name="platformIdentifierToken" value="%platform%" />
			<property name="platformSpecificTemplateList"
			          value="Windows,${basedir}/templateFiles/index.html.template,index.html,Linux,${basedir}/templateFiles/index.html.template,index.html" />
			<property name="dropTokenList"
			          value="%runtime%,%sdk%,%examples%,%pulsar%" />
			<property name="isBuildTested" value="false" />
		</ant>

		<!--  Get the build map over for the results to point to. -->
		<copy file="${mapsDirectory}/mtj.map"
		      tofile="${result}/directory.txt" />

		<replace file="${result}/${indexFileName}">
			<!--  Insert url for supported eclipse drop-->
			<replacefilter token="@eclipseBuildID@" value="${eclipseBuildID}" />
			<replacefilter token="@eclipseIndexURL@"
			               value="${eclipseDirectURL}${eclipseFolder}" />
			<replacefilter token="@eclipseMirrorURL@"
			               value="${eclipseMirrorURL}" />
			<replacefilter token="@eclipseWin32ZipURL@"
			               value="${eclipseWin32ZipURL}" />


			<!-- Insert url for all-in-one package -->
			<replacefilter token="@buildId@" value="${buildId}" />
		</replace>

		<!-- Copy to "bldindex.html". This html will be used to reference locally without download counting. This file will not be pushed to main download site. -->
		<copy file="${result}/${indexFileName}"
		      tofile="${result}/bld${indexFileName}" />

		<!-- Replace the base variables to be nothing for bld-index, and the full reroute url for the main index file. -->
		<replace file="${result}/${indexFileName}"
		         token="@base@"
		         value="http://www.eclipse.org/downloads/download.php?file=/dsdp/mtj/downloads/drops/${buildLabel}/" />
		<replace file="${result}/bld${indexFileName}" token="@base@" value="" />

		<delete dir="${outputDirectory}/${buildLabel}" />
		<mkdir dir="${outputDirectory}/${buildLabel}" />
		<copy todir="${outputDirectory}/${buildLabel}">
			<fileset dir="${buildDirectory}/${buildLabel}" />
		</copy>

		<copy todir="${outputDirectory}/${buildLabel}">
			<fileset dir="${buildDirectory}/${buildLabel}" />
		</copy>

		<!--copy nightly build to N.latest folder. this is ncessary to publish the docs plugins-->
		<delete failonerror="true" includeEmptyDirs="true">
			<fileset dir="${outputDirectory}/N.latest" includes="**/*" />
		</delete>
		<copy toFile="${outputDirectory}/N.latest/dsdp-mtj-SDK-latest.zip"
		      file="${buildDirectory}/${buildLabel}/dsdp-mtj-SDK-${buildId}.zip" />
		<copy toFile="${outputDirectory}/N.latest/dsdp-mtj-runtime-latest.zip"
		      file="${buildDirectory}/${buildLabel}/dsdp-mtj-runtime-${buildId}.zip" />
		<copy toFile="${outputDirectory}/N.latest/dsdp-mtj-examples-latest.zip"
		      file="${buildDirectory}/${buildLabel}/dsdp-mtj-examples-${buildId}.zip" />
		<copy toFile="${outputDirectory}/N.latest/dsdp-mtj-Pulsar-latest.zip"
		      file="${buildDirectory}/${buildLabel}/dsdp-mtj-Pulsar-${buildId}.zip" />

		<chmod perm="g+rw" type="file">
			<fileset dir="${outputDirectory}/N.latest">
				<include name="**.**" />
			</fileset>
		</chmod>
	</target>

	<target name="publishBuildLocal" if="publish">

		<property name="dropFolder"
		          value="${outputDirectory}/${buildType}_${buildId}" />
		<property name="buildlogs" value="${dropFolder}/buildlogs" />

		<mkdir dir="${dropFolder}" />
		<mkdir dir="${buildlogs}" />

		<copy todir="${dropFolder}" overwrite="true">
			<fileset dir="${buildDirectory}/${buildType}_${buildId}" />
		</copy>

		<!-- Copy build logs. -->
		<copy todir="${buildlogs}" overwrite="true">
			<fileset dir="${basedir}">
				<include name="*.log" />
			</fileset>
		</copy>

	</target>

	<!-- Create the Update Site -->
	<target name="createUpdateSite" if="updateSite">
		<ant antfile="buildUpdateSite.xml" target="RemoteSite">
			<property name="siteOutputDirectory"
			          value="${updateSiteDirectory}" />
		</ant>
	</target>

	<!-- Optimize update site -->
	<target name="optimize.updatesite" if="optimize">
		<echo message="Pack200 Optimization" />

		<!--pack200-->
		<java jar="${eclipse.home}/plugins/org.eclipse.equinox.launcher.jar"
		      fork="true"
		      timeout="10800000"
		      jvm="${jvm15.home}/bin/java"
		      failonerror="true"
		      maxmemory="768m"
		      error="${buildDirectory}/errorlog.txt"
		      dir="${buildDirectory}"
		      output="${buildDirectory}/jarprocessorlog.txt">
			<arg line="-consolelog -application org.eclipse.update.core.siteOptimizer" />
			<arg line="-jarProcessor -verbose -processAll -pack -repack -outputDir ${tmpUpdateSiteDirectory} ${tmpUpdateSiteDirectory}" />
		</java>

		<echo message="Deploy site" />
		<copy todir="${updateSiteDirectory}" verbose="true">
			<fileset dir="${tmpUpdateSiteDirectory}" />
		</copy>
		<delete dir="${tmpUpdateSiteDirectory}" />
	</target>

	<target name="runTesting" if="runTests">
		<echo message="Not implemented" />
	</target>

	<target name="updateRSSfeed" if="notify">
		<echo message="Not implemented" />
	</target>

	<target name="generate.p2.metadata" if="metadata">
		<echo message="Generate Metadata" />
		<java jar="${eclipse.home}/plugins/org.eclipse.equinox.launcher.jar"
		      fork="true"
		      timeout="10800000"
		      jvm="${jvm15.home}/bin/java"
		      failonerror="true"
		      maxmemory="768m"
		      error="${buildDirectory}/errorlog.txt"
		      dir="${buildDirectory}"
		      output="${buildDirectory}/p2metadata.txt">
			<arg line="-application org.eclipse.equinox.p2.metadata.generator.EclipseGenerator" />
			<arg line="-updateSite ${updateSiteDirectory}" />
			<arg line="-site file:${updateSiteDirectory}/site.xml" />
			<arg line="-metadataRepository file:${updateSiteDirectory}" />
			<arg line="-metadataRepositoryName Mobile Tools for Java Update Site" />
			<arg line="-artifactRepository file:${updateSiteDirectory}" />
			<arg line="-artifactRepositoryName Mobile Tools for Java Artifacts" />
			<arg line="-compress" />
			<arg line="-reusePack200Files" />
			<arg line="-noDefaultIUs" />
			<arg line="-vmargs -Xmx256M" />
		</java>
	</target>
</project>