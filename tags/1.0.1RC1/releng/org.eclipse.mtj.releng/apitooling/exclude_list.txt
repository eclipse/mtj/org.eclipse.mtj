###############################################################################
# Copyright (c) 2009 Motorola.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#     Diego Madruga - initial version
###############################################################################

# DOC BUNDLES
org.eclipse.mtj.doc.isv
org.eclipse.mtj.doc.user

# LIBRARY BUNDLES
jmunit.framework
antenna.preprocessor.v3
de.schlichtherle.truezip

# ORBIT BUNDLES
org.objectweb.asm
org.antlr.runtime
org.mortbay.jetty

# FEATURES BUNDLES
org.eclipse.mtj
org.eclipse.mtj.sdk
org.eclipse.mtj.examples
org.eclipse.mtj.pulsar
