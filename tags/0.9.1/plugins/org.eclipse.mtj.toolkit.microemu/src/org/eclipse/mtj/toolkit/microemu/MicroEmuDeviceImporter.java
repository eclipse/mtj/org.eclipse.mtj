/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)   - Initial implementation
 *     Diego Sandin (Motorola)    - Refactoring package name to follow eclipse 
 *                                  standards
 *     Daniel Murphy (Individual) - Set up the classpath correctly  
 *     Diego Sandin (Motorola)    - Changed the device discovery algorithm
 *     Diego Sandin (Motorola)    - Use JavaEmulatorDeviceProperties enum instead of 
 *                                  hard-coded strings
 */
package org.eclipse.mtj.toolkit.microemu;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.importer.IDeviceImporter;
import org.eclipse.mtj.core.importer.LibraryImporter;
import org.eclipse.mtj.core.importer.impl.JavaEmulatorDeviceImporter;
import org.eclipse.mtj.core.importer.properties.JavaEmulatorDeviceProperties;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.model.Classpath;
import org.eclipse.mtj.core.model.ReplaceableParametersProcessor;
import org.eclipse.mtj.core.model.Version;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.library.api.API;
import org.eclipse.mtj.core.model.library.api.APIType;
import org.eclipse.mtj.toolkit.microemu.internal.Activator;
import org.eclipse.mtj.toolkit.microemu.internal.MicoEmuConstants;
import org.eclipse.mtj.toolkit.microemu.internal.MicroEmuDeviceSkin;
import org.eclipse.mtj.toolkit.microemu.internal.MicroEmuLaunchTemplateProperties;

/**
 * An {@link IDeviceImporter} implementation that recognizes and imports
 * MicroEmulator toolkit instances.
 * 
 * @author Craig Setera
 */
public class MicroEmuDeviceImporter extends JavaEmulatorDeviceImporter {

    /* Devices Skins */
    private static final MicroEmuDeviceSkin EMULATOR_DEVICE_SKINS[] = {
            new MicroEmuDeviceSkin(MicoEmuConstants.EMULATOR_STANDARDSKIN_JAR,
                    MicoEmuConstants.EMULATOR_STANDARDSKIN_PATH,
                    MicoEmuConstants.EMULATOR_STANDARDSKIN_NAME),
            new MicroEmuDeviceSkin(MicoEmuConstants.EMULATOR_LARGESKIN_JAR,
                    MicoEmuConstants.EMULATOR_LARGESKIN_PATH,
                    MicoEmuConstants.EMULATOR_LARGESKIN_NAME),
            new MicroEmuDeviceSkin(MicoEmuConstants.EMULATOR_MINIMUNSKIN_JAR,
                    MicoEmuConstants.EMULATOR_MINIMUNSKIN_PATH,
                    MicoEmuConstants.EMULATOR_MINIMUNSKIN_NAME) };

    /**
     * The MicroEmulator jar name
     */
    private static final String EMULATOR_JAR_NAME = "microemulator.jar"; //$NON-NLS-1$

    /**
     * The MicroEmulator main class name
     */
    private static final String MAIN_CLASS_NAME = "org.microemu.app.Main"; //$NON-NLS-1$

    /**
     * Properties file holding emulator/device information
     */
    private static final String PROPS_FILE = "microemu.properties"; //$NON-NLS-1$

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.importer.IDeviceImporter#getMatchingDevices(java.io.File, org.eclipse.core.runtime.IProgressMonitor)
     */
    public IDevice[] getMatchingDevices(File directory, IProgressMonitor monitor) {
        IDevice[] matchingDevices = new MicroEmuDevice[0];

        try {
            /* Check if the MicoEmulator Jar exists */
            File jarFile = new File(directory, EMULATOR_JAR_NAME);
            if (jarFile.exists()
                    && hasMainClassAttribute(jarFile, MAIN_CLASS_NAME)) {

                ArrayList<IDevice> deviceList = new ArrayList<IDevice>();

                /* Import all devices */
                for (MicroEmuDeviceSkin element : EMULATOR_DEVICE_SKINS) {
                    IDevice singleDevice = createDevice(jarFile, element);
                    deviceList.add(singleDevice);
                }

                if (!deviceList.isEmpty()) {
                    matchingDevices = deviceList.toArray(matchingDevices);
                }
            }
        } catch (Exception e) {
            MTJCorePlugin.log(IStatus.WARNING,
                    Messages.MicroEmuDeviceImporter_import_error, e);
        }

        return matchingDevices;
    }

    /**
     * Add the device libraries.
     * 
     * @param jarFile
     * @param classpath
     * @param importer
     */
    private void addDeviceLibraries(File jarFile, Classpath classpath,
            LibraryImporter importer) {
        // Now add the player libraries
        String classpathString = getDeviceProperties().getProperty(
                JavaEmulatorDeviceProperties.CLASSPATH.toString(),
                Utils.EMPTY_STRING);

        Map<String, String> replaceableParameters = new HashMap<String, String>();
        replaceableParameters.put(MicroEmuLaunchTemplateProperties.TOOLKITROOT
                .toString(), jarFile.getParent());

        classpathString = ReplaceableParametersProcessor
                .processReplaceableValues(classpathString,
                        replaceableParameters);
        String[] entries = classpathString.split(";"); //$NON-NLS-1$

        for (String entrie : entries) {
            ILibrary library = importer.createLibraryFor(new File(entrie));

            // Because of the structure of the libraries,
            // we need to hard-code the CLDC and MIDP libraries
            API api = library.getAPI(APIType.UNKNOWN);
            if (api != null) {
                if (api.getIdentifier().equalsIgnoreCase("cldcapi11.jar")) { //$NON-NLS-1$
                    api.setIdentifier("CLDC"); //$NON-NLS-1$
                    api.setType(APIType.CONFIGURATION);
                    api.setName("Connected Limited Device Configuration"); //$NON-NLS-1$
                    api.setVersion(new Version("1.1")); //$NON-NLS-1$

                } else if (api.getIdentifier()
                        .equalsIgnoreCase("midpapi20.jar")) { //$NON-NLS-1$
                    api.setIdentifier("MIDP"); //$NON-NLS-1$
                    api.setType(APIType.PROFILE);
                    api.setName("Mobile Information Device Profile"); //$NON-NLS-1$
                    api.setVersion(new Version("2.0")); //$NON-NLS-1$
                }
                classpath.addEntry(library);
            }
        }
    }

    /**
     * Create the new device instance for the specified Microemulator jar file.
     * 
     * @param jarFile
     * @param skin TODO
     * @return
     */
    private IDevice createDevice(File jarFile, MicroEmuDeviceSkin skin) {

        MicroEmuDevice device = new MicroEmuDevice();

        device.setBundle(Activator.getDefault().getBundle().getSymbolicName());
        device.setClasspath(getDeviceClasspath(jarFile));
        device.setDebugServer(isDebugServer());
        device.setDescription("Microemulator Device"); //$NON-NLS-1$
        device.setDeviceProperties(new Properties());
        device.setGroupName("Microemulator"); //$NON-NLS-1$
        device.setName(skin.getName());
        device.setPreverifier(getPreverifier(jarFile));
        device.setProtectionDomains(new String[0]);
        device.setLaunchCommandTemplate(getLaunchCommand());
        device.setRoot(jarFile.getParentFile());
        device.setSkin(skin);

        return device;
    }

    /**
     * Get the device classpath based on the specified player.jar file.
     * 
     * @param jarFile
     * @return
     */
    private Classpath getDeviceClasspath(File jarFile) {
        Classpath classpath = new Classpath();
        addDeviceLibraries(jarFile, classpath, new LibraryImporter());

        return classpath;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.importer.impl.JavaEmulatorDeviceImporter#getDevicePropertiesURL()
     */
    @Override
    protected URL getDevicePropertiesURL() {
        return Activator.getDefault().getBundle().getEntry(PROPS_FILE);
    }
}
