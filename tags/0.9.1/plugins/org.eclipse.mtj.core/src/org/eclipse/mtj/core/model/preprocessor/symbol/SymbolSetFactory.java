/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.model.preprocessor.symbol;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.library.ILibrary;

/**
 * A factory that create SymbolSet from Device.
 * 
 * @author wangf
 */
public class SymbolSetFactory {
    // Properties that should not use as symbols
    public static final String PROP_UEI_VERSION = "uei.version";
    public static final String PROP_UEI_ARGUMENTS = "uei.arguments";
    public static final String PROP_DEVICE_LIST = "device.list";
    public static final String PROP_SECURITY_DOMAINS = "security.domains";
    public static final String PROP_BOOTCLASSPATH = "bootclasspath";
    public static final String PROP_APIS = "apis";
    public static final String PROP_DESCRIPTION = "description";
    // The properties that used for creating symbols.
    private static List<String> excludedProperties;
    static {
        excludedProperties = new ArrayList<String>();
        excludedProperties.add(PROP_UEI_VERSION);
        excludedProperties.add(PROP_UEI_ARGUMENTS);
        excludedProperties.add(PROP_DEVICE_LIST);
        excludedProperties.add(PROP_SECURITY_DOMAINS);
        excludedProperties.add(PROP_BOOTCLASSPATH);
        excludedProperties.add(PROP_APIS);
        excludedProperties.add(PROP_DESCRIPTION);
    }

    /**
     * Create symbols from device libraries and add them to SymbolSet.
     * 
     * @param symbolSet
     * @param device
     */
    private static void addLibrarySymbols(SymbolSet symbolSet, IDevice device) {
        if (device == null) {
            return;
        }
        ILibrary[] libraries = device.getClasspath().getEntries();

        if (libraries != null) {
            for (ILibrary librarie : libraries) {
                String key = (librarie.getAPIs()[0].getIdentifier()).replace(
                        ' ', '_');
                key = replaceFirstNonLetterChar(key);
                String value = librarie.getAPIs()[0].getVersion().toString();

                symbolSet.add(new Symbol(key, value));
            }
        }
    }

    /**
     * Create SymbolSet from device.
     * 
     * @param device
     * @return
     */
    public static SymbolSet createSymbolSet(IDevice device) {
        SymbolSet symbolSet = new SymbolSet();
        populateSymbolSetFromProperties(symbolSet, device);
        addLibrarySymbols(symbolSet, device);
        return symbolSet;
    }

    /**
     * Create symbols from device properties and add them to SymbolSet.
     * 
     * @param symbolSet
     * @param device
     */
    private static void populateSymbolSetFromProperties(SymbolSet symbolSet,
            IDevice device) {
        if (device == null) {
            return;
        }
        Properties properties = device.getDeviceProperties();
        Set<Entry<Object, Object>> set = properties.entrySet();

        for (Entry<Object, Object> entry : set) {
            String key = String.valueOf(entry.getKey());
            if (!excludedProperties.contains(key)) {
                key = replaceFirstNonLetterChar(key);
                String value = String.valueOf(entry.getValue());
                symbolSet.add(new Symbol(key, value));
            }
        }
    }

    /**
     * If a name of a symbol do not start with letters, preprocessor will throw
     * exception, so we should add letters at first.
     * 
     * @param lib
     * @return
     */
    public static String replaceFirstNonLetterChar(String lib) {
        lib = lib.replaceFirst("^[^a-zA-Z].+", "a_" + lib);//$NON-NLS-1$
        return lib;
    }
}
