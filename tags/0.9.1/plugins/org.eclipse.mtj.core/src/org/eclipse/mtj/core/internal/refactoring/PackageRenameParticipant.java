/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.internal.refactoring;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RenameParticipant;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.TextEdit;

/**
 * This class update Application Descriptor file when package been renamed.
 * 
 * @author wangf
 */
public class PackageRenameParticipant extends RenameParticipant {

    private IPackageFragment packageFragment;

    @Override
    public RefactoringStatus checkConditions(IProgressMonitor pm,
            CheckConditionsContext context) throws OperationCanceledException {
        return new RefactoringStatus();
    }

    @Override
    public Change createChange(IProgressMonitor pm) throws CoreException,
            OperationCanceledException {
        SubProgressMonitor subMonitor = new SubProgressMonitor(pm, 5);
        subMonitor.setTaskName("Creating Application Descriptor Changes");
        // collect all the midlets in the package and child package
        Set<IType> midlets = MidLetsCollector.collectMidletsInPackage(
                packageFragment, subMonitor);
        CompositeChange result = new CompositeChange(
                RefactoringMessages.RefectoringChangeName_PackageRenameParticipant);
        // use a map to record Jad file and its change
        Map<IFile, MultiTextEdit> jadFileEditMap = new HashMap<IFile, MultiTextEdit>();
        for (IType midlet : midlets) {
            String newName = getArguments().getNewName() + "."
                    + midlet.getElementName();
            CompositeChange compositeChange = MidletJadFileChangesCollector
                    .collectChange(MidletJadFileChangesCollector.MODE_RENAME,
                            midlet, newName, subMonitor);
            Change[] changes = compositeChange.getChildren();
            for (Change change : changes) {
                MidletJadTextFileMidletChange jadChange = (MidletJadTextFileMidletChange) change;
                TextEdit edit = jadChange.getEdit();
                IFile jadFile = jadChange.getFile();

                MultiTextEdit multiTextEdit = jadFileEditMap.get(jadFile);
                if (multiTextEdit == null) {
                    multiTextEdit = new MultiTextEdit();
                    jadFileEditMap.put(jadFile, multiTextEdit);
                }
                // use MultiTextEdit to combine TextEdit for every midlet
                multiTextEdit.addChild(edit);
            }
        }
        for (Map.Entry<IFile, MultiTextEdit> entry : jadFileEditMap.entrySet()) {
            IFile jadFile = entry.getKey();
            TextFileChange textFileChange = new TextFileChange(jadFile
                    .getName(), jadFile);
            textFileChange.setEdit(entry.getValue());
            result.add(textFileChange);
        }
        subMonitor.done();
        return (result.getChildren().length > 0) ? result : null;
    }

    @Override
    public String getName() {
        return RefactoringMessages.UpdateApplicationDescriptor_refactoringParticipantName;
    }

    @Override
    protected boolean initialize(Object element) {
        packageFragment = (IPackageFragment) element;
        return true;
    }

}
