/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.converter;

/**
 * ProjectConvertionException Class wraps exceptions during
 * a project conversion process.
 * 
 * @author David Marques
 */
public class ProjectConvertionException extends Exception {
    
    // Constants -----------------------------------------------------
    
    // Attributes ----------------------------------------------------
    
    // Static --------------------------------------------------------
    
    // Constructors --------------------------------------------------
    
    private static final long serialVersionUID = 1L;

    /**
     * Creates a ProjectConvertionException instance.
     * 
     * @param message   - error message.
     * @param throwable - wrapped throwable. 
     */
    public ProjectConvertionException(String message, Throwable throwable) {
        super(message, throwable);
    }
    
    /**
     * Creates a ProjectConvertionException instance.
     * 
     * @param message   - error message.
     */
    public ProjectConvertionException(String message) {
        super(message);
    }
    
    // Public --------------------------------------------------------
    
    // Z implementation ----------------------------------------------
    
    // Y overrides ---------------------------------------------------
    
    // Package protected ---------------------------------------------
    
    // Protected -----------------------------------------------------
    
    // Private -------------------------------------------------------
    
}
