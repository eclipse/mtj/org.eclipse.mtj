/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core;

/**
 * A simple class that wraps the configuration for build logging.
 * 
 * @author Craig Setera
 */
public class BuildLoggingConfiguration {
    
    
    /**
     * The BuildLoggingConfiguration instance
     */
    public static final BuildLoggingConfiguration instance = new BuildLoggingConfiguration();
    
    static {
        
        String configString = System.getProperty("mtj.build.logging", "");

        String[] values = configString.split(",");
        for (int i = 0; i < values.length; i++) {
            String value = values[i];
            if (value.equals("obfuscationOutput")) {
                instance.obfuscationOutputEnabled = true;
            } else if (value.equals("preverifierOutput")) {
            } else if (value.equals("preprocessorTrace")) {
                instance.preprocessorTraceEnabled = true;
                instance.preverifierOutputEnabled = true;
            } else if (value.equals("preverifierTrace")) {
                instance.preverifierTraceEnabled = true;
            } else if (value.equals("all")) {
                instance.obfuscationOutputEnabled = true;
                instance.preprocessorTraceEnabled = true;
                instance.preverifierOutputEnabled = true;
                instance.preverifierTraceEnabled = true;
            }
        }
    }

    private boolean obfuscationOutputEnabled;
    private boolean preprocessorTraceEnabled;
    private boolean preverifierOutputEnabled;
    private boolean preverifierTraceEnabled;

    private BuildLoggingConfiguration() {
    }

    /**
     * @return
     */
    public boolean isObfuscationOutputEnabled() {
        return obfuscationOutputEnabled;
    }

    /**
     * @return
     */
    public boolean isPreprocessorTraceEnabled() {
        return preprocessorTraceEnabled;
    }

    /**
     * @return
     */
    public boolean isPreverifierTraceEnabled() {
        return preverifierTraceEnabled;
    }

    /**
     * @return
     */
    public boolean isPreverifierOutputEnabled() {
        return preverifierOutputEnabled;
    }
}
