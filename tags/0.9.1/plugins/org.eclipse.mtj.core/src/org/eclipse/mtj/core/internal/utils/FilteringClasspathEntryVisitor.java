/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera  (EclipseME) - Initial implementation
 *     Diego Sandin  (Motorola)  - Refactoring package name to follow eclipse 
 *                                 standards and added serialVersionUID.
 *     Hugo Raniere  (Motorola)  - Reimplementing JavaMEClasspathContainer
 */
package org.eclipse.mtj.core.internal.utils;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.mtj.core.internal.JavaMEClasspathContainer;
import org.eclipse.mtj.core.nature.J2MENature;

/**
 * An IClasspathEntryVisitor that filters out the J2ME classpath container and
 * the base JRE containers and libraries.
 * 
 * @author Craig Setera
 */
public abstract class FilteringClasspathEntryVisitor extends
        AbstractClasspathEntryVisitor {
    private IClasspathEntry containerEntry;

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.internal.utils.AbstractClasspathEntryVisitor#visitContainerBegin(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IPath, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public boolean visitContainerBegin(IClasspathEntry entry,
            IJavaProject javaProject, IPath containerPath,
            IProgressMonitor monitor) throws CoreException {
        String firstSegment = containerPath.segment(0);

        boolean traverse = !firstSegment
                .equals(JavaMEClasspathContainer.JAVAME_CONTAINER)
                && !firstSegment.equals(JavaRuntime.JRE_CONTAINER);

        if (traverse) {
            containerEntry = entry;
        }

        return traverse;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.internal.utils.AbstractClasspathEntryVisitor#visitContainerEnd(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IPath, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void visitContainerEnd(IClasspathEntry entry,
            IJavaProject javaProject, IPath containerPath,
            IProgressMonitor monitor) throws CoreException {
        containerEntry = null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.internal.utils.AbstractClasspathEntryVisitor#visitProject(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public boolean visitProject(IClasspathEntry entry,
            IJavaProject javaProject, IJavaProject classpathProject,
            IProgressMonitor monitor) throws CoreException {
        return !J2MENature.hasMtjCoreNature(classpathProject.getProject());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.internal.utils.AbstractClasspathEntryVisitor#visitVariable(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, java.lang.String, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public boolean visitVariable(IClasspathEntry entry,
            IJavaProject javaProject, String variableName,
            IProgressMonitor monitor) throws CoreException {
        return !variableName.equals(JavaRuntime.JRELIB_VARIABLE);
    }

    /**
     * Return a boolean indicating whether the specified library classpath entry
     * is exported either directly or via a classpath container.
     * 
     * @param entry
     * @return
     */
    protected boolean isLibraryExported(IClasspathEntry entry) {
        if (this.containerEntry != null) {
            return containerEntry.isExported();
        } else {
            return entry.isExported();
        }
    }
}
