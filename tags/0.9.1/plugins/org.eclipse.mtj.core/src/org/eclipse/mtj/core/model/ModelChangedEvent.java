/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.core.model;

/**
 * @since 0.9.1
 */
public class ModelChangedEvent implements IModelChangedEvent {

    private Object[] changedObjects;
    private String changedProperty;
    private Object oldValue, newValue;
    private IModelChangeProvider provider;
    private int type;

    /**
     * The constructor of the event.
     * 
     * @param provider the change provider
     * @param type the event type
     * @param objects the changed objects
     * @param changedProperty or <code>null</code> if not applicable
     */
    public ModelChangedEvent(IModelChangeProvider provider, int type,
            Object[] objects, String changedProperty) {

        this.type = type;
        this.provider = provider;
        this.changedObjects = objects;
        this.changedProperty = changedProperty;
    }

    /**
     * A constructor that should be used for changes of object properties.
     * 
     * @param provider the event provider
     * @param object affected object
     * @param changedProperty changed property of the affected object
     * @param oldValue the value before the change
     * @param newValue the value after the change
     */
    public ModelChangedEvent(IModelChangeProvider provider, Object object,
            String changedProperty, Object oldValue, Object newValue) {
        this.type = CHANGE;
        this.provider = provider;
        this.changedObjects = new Object[] { object };
        this.changedProperty = changedProperty;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangedEvent#getChangedObjects()
     */
    public Object[] getChangedObjects() {
        return (changedObjects == null) ? new Object[0] : changedObjects;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangedEvent#getChangedProperty()
     */
    public String getChangedProperty() {
        return changedProperty;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangedEvent#getChangeProvider()
     */
    public IModelChangeProvider getChangeProvider() {
        return provider;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangedEvent#getChangeType()
     */
    public int getChangeType() {
        return type;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangedEvent#getNewValue()
     */
    public Object getNewValue() {
        return newValue;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModelChangedEvent#getOldValue()
     */
    public Object getOldValue() {
        return oldValue;
    }
}
