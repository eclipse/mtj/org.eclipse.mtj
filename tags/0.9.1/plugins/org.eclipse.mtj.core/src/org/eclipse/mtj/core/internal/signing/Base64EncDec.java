/**
 * Copyright (c) 2004,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Kevin Hunter             - Initial implementation
 *     Craig Setera (EclipseME) - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.internal.signing;

import java.io.ByteArrayOutputStream;

/**
 * This is a utility class that performs Base64 encoding and decoding
 * 
 * @author Kevin Hunter
 */
public class Base64EncDec {

    /**
     * Encode the specified byte array
     * 
     * @param input Array of <code>byte</code>s to be encoded.
     * 
     * @return <code>String</code> containing the Base64 representation of the
     *         input byte array.
     */
    public static String encode(byte[] input) {
        return (encode(input, 0, input.length));
    }

    /**
     * Encode a portion of the specified byte array
     * 
     * @param input Array of <code>byte</code>s containing the data to be
     *                encoded.
     * @param nStart Zero-based offset of the first byte to encode
     * @param nLen Number of bytes to encode.
     * 
     * @return <code>String</code> containing the Base64 representation of the
     *         portion of the input byte array.
     */
    public static String encode(byte[] input, int nStart, int nLen) {
        StringBuffer result = new StringBuffer(nLen * 4 / 3 + 4);
        int c0, c1, c2;
        char quantum0, quantum1, quantum2, quantum3;

        while (nLen >= 3) {
            c0 = (int) input[nStart++] & 0xFF;
            c1 = (int) input[nStart++] & 0xFF;
            c2 = (int) input[nStart++] & 0xFF;

            quantum0 = Base64EncodeTbl[c0 >> 2];
            quantum1 = Base64EncodeTbl[((0x03 & c0) << 4) | ((0xf0 & c1) >> 4)];
            quantum2 = Base64EncodeTbl[((0x0f & c1) << 2) | ((0xc0 & c2) >> 6)];
            quantum3 = Base64EncodeTbl[c2 & 0x3f];

            result.append(quantum0);
            result.append(quantum1);
            result.append(quantum2);
            result.append(quantum3);

            nLen -= 3;
        }

        switch (nLen) {
        case 0:
            break;
        case 1:
            c0 = (int) input[nStart++] & 0xFF;
            quantum0 = Base64EncodeTbl[c0 >> 2];
            quantum1 = Base64EncodeTbl[((0x03 & c0) << 4)];
            quantum2 = '=';
            quantum3 = '=';
            result.append(quantum0);
            result.append(quantum1);
            result.append(quantum2);
            result.append(quantum3);
            break;
        case 2:
            c0 = (int) input[nStart++] & 0xFF;
            c1 = (int) input[nStart++] & 0xFF;
            quantum0 = Base64EncodeTbl[c0 >> 2];
            quantum1 = Base64EncodeTbl[((0x03 & c0) << 4) | ((0xf0 & c1) >> 4)];
            quantum2 = Base64EncodeTbl[(0x0f & c1) << 2];
            quantum3 = '=';
            result.append(quantum0);
            result.append(quantum1);
            result.append(quantum2);
            result.append(quantum3);
            break;
        }

        return (result.toString());
    }

    /**
     * Decode the specified <code>String</code> into a byte array.
     * 
     * @param input The string to be decoded
     * @return The decoded version of the string. Returns <code>null</code> if
     *         the input string is not validly encoded.
     */
    public static byte[] decode(String input) {
        return (decode(input, 0, input.length()));
    }

    /**
     * Decode a portion of the specified <code>String</code> into a byte
     * array.
     * 
     * @param input String containing the part to be decoded.
     * @param nStart Zero-based index of the first character to decode
     * @param nLen Number of characters to decode.
     * 
     * @return The decoded version of the string. Returns <code>null</code> if
     *         the input string is not validly encoded.
     */
    public static byte[] decode(String input, int nStart, int nLen) {
        ByteArrayOutputStream result = new ByteArrayOutputStream(nLen);

        int i;
        int c0, c1, c2;
        short[] quantum = new short[4];

        while (nLen > 4) {
            for (i = 0; i < 4; i++) {
                int c = (int) input.charAt(nStart++);
                if (c < 0 || c > 255 || Base64DecodeTbl[c] < 0) {
                    return (null);
                }

                quantum[i] = Base64DecodeTbl[c];
            }

            nLen -= 4;

            c0 = ((quantum[0] << 2) & 0xFC) | ((quantum[1] >> 4) & 0x03);
            c1 = ((quantum[1] << 4) & 0xF0) | ((quantum[2] >> 2) & 0x0F);
            c2 = ((quantum[2] << 6) & 0xC0) | (quantum[3] & 0x3F);

            result.write(c0);
            result.write(c1);
            result.write(c2);
        }

        if (nLen != 4) {
            return (null);
        }

        int nPad = 0;
        if (input.charAt(nStart + 3) == '=') {
            nPad++;
        }
        if (input.charAt(nStart + 2) == '=') {
            nPad++;
        }

        for (i = 0; i < 4; i++) {
            int c = (int) input.charAt(nStart++);
            if (c < 0 || c > 255 || Base64DecodeTbl[c] < 0) {
                return (null);
            }

            quantum[i] = Base64DecodeTbl[c];
        }

        c0 = ((quantum[0] << 2) & 0xFC) | ((quantum[1] >> 4) & 0x03);
        c1 = ((quantum[1] << 4) & 0xF0) | ((quantum[2] >> 2) & 0x0F);
        c2 = ((quantum[2] << 6) & 0xC0) | (quantum[3] & 0x3F);

        /*
         * Now determine if we have 1, 2, or 3 bytes of data. If we have no pad
         * characters, then we have 3 bytes, 1 pad character means we have 2
         * bytes, and 2 pad characters means we only have 1 byte of data in the
         * quantum.
         */

        switch (nPad) {
        case 1:
            result.write(c0);
            result.write(c1);
            break;
        case 2:
            result.write(c0);
            break;
        default:
            result.write(c0);
            result.write(c1);
            result.write(c2);
            break;
        }

        return (result.toByteArray());
    }

    private static char[] Base64EncodeTbl = { 'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
            'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
            't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', '+', '/' };

    /*
     * This table is used to decode base 64 bytes. -1 has been used as the place
     * holder to indicate an invalid character (in other words, a character
     * which should never appear in a validly encoded base 64 string).
     */

    private static short[] Base64DecodeTbl = { -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1,
            -1, -1, 0, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
            13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1,
            -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1 };
}
