/**
 * Copyright (c) 2007,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

import java.util.ArrayList;

import org.eclipse.mtj.core.model.IModel;
import org.eclipse.mtj.core.model.IWritable;

/**
 * @since 0.9.1
 */
public interface IDocumentObject extends IDocumentElementNode, IWritable {

    public void addChildNode(IDocumentElementNode child, boolean fireEvent);

    public void addChildNode(IDocumentElementNode child, int position,
            boolean fireEvent);

    public IDocumentElementNode clone(IDocumentElementNode node);

    public boolean getBooleanAttributeValue(String name, boolean defaultValue);

    public IDocumentElementNode getChildNode(Class<?> clazz);

    public int getChildNodeCount(Class<?> clazz);

    public ArrayList<IDocumentElementNode> getChildNodesList(Class<?> clazz,
            boolean match);

    public ArrayList<IDocumentElementNode> getChildNodesList(
            Class<?>[] classes, boolean match);

    public IDocumentElementNode getNextSibling(IDocumentElementNode node,
            Class<?> clazz);

    public IDocumentElementNode getPreviousSibling(IDocumentElementNode node,
            Class<?> clazz);

    public IModel getSharedModel();

    public boolean hasChildNodes(Class<?> clazz);

    public boolean isEditable();

    public boolean isFirstChildNode(IDocumentElementNode node, Class<?> clazz);

    public boolean isInTheModel();

    public boolean isLastChildNode(IDocumentElementNode node, Class<?> clazz);

    public void moveChildNode(IDocumentElementNode node, int newRelativeIndex,
            boolean fireEvent);

    public IDocumentElementNode removeChildNode(IDocumentElementNode child,
            boolean fireEvent);

    public IDocumentElementNode removeChildNode(int index, Class<?> clazz);

    public IDocumentElementNode removeChildNode(int index, Class<?> clazz,
            boolean fireEvent);

    public void reset();

    public boolean setBooleanAttributeValue(String name, boolean value);

    public void setChildNode(IDocumentElementNode newNode, Class<?> clazz);

    public void setInTheModel(boolean inModel);

    public void setSharedModel(IModel model);

    public void swap(IDocumentElementNode child1, IDocumentElementNode child2,
            boolean fireEvent);

}
