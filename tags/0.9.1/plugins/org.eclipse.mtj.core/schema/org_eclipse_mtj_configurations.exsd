<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.eclipse.mtj.core">
<annotation>
      <appInfo>
         <meta.schema plugin="org.eclipse.mtj.core" id="configurations" name="Supported Java ME Configurations"/>
      </appInfo>
      <documentation>
         Extension that provides a listing of supported Java ME Configurations.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <documentation>
            extension that provides a listing of supported J2ME Configurations.
         </documentation>
      </annotation>
      <complexType>
         <sequence>
            <element ref="configuration" minOccurs="1" maxOccurs="unbounded"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  a fully qualified identifier of the target extension point
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  an optional identifier of the extension instance
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  an optional name of the extension instance
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="configuration">
      <annotation>
         <documentation>
            definition of a single configuration
         </documentation>
      </annotation>
      <complexType>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  required identifier of the configuration.  This value will be used within the Java Application Descriptor for a midlet suite.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string" use="required">
            <annotation>
               <documentation>
                  required name of this configuration.  This value will be presented to the user via the Java Application Descriptor editor.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="version" type="string" use="required">
            <annotation>
               <documentation>
                  required version of configuration specification
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         [Enter the first release in which this extension point appears.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         [Enter extension point usage example here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="apiInfo"/>
      </appInfo>
      <documentation>
         [Enter API information here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         [Enter information about supplied implementation of this extension point.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="copyright"/>
      </appInfo>
      <documentation>
         Copyright (c) 2003,2008 Craig Setera and others.
 
All rights reserved. This program and the accompanying materials 
are made available under the terms of the Eclipse Public License v1.0 
which accompanies this distribution, and is available at 
http://www.eclipse.org/legal/epl-v10.html

Contributors:
    Craig Setera (EclipseME) - Initial implementation;
    Diego Sandin (Motorola)  - Refactoring extension name to follow eclipse 
                               standards
      </documentation>
   </annotation>

</schema>
