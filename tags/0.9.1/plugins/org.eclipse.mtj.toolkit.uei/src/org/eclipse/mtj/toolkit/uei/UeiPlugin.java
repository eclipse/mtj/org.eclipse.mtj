/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.toolkit.uei;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.osgi.framework.BundleContext;

/**
 * The main plug-in class to be used in the desktop.
 */
public class UeiPlugin extends Plugin {

    /**
     * A debug flag used for debugging the UEI device import process.
     */
    public static String UEI_DEBUG_SYSTEM_PROPERTY = "org.eclipse.mtj.toolkit.uei.debug"; //$NON-NLS-1$

    /**
     * Debug is enabled by specifying the System property
     * {@link #UEI_DEBUG_SYSTEM_PROPERTY} as <code>true</code>.
     */
    private static final boolean debugEnabled = System.getProperty(
            UEI_DEBUG_SYSTEM_PROPERTY, Boolean.FALSE.toString())
            .equalsIgnoreCase(Boolean.TRUE.toString());

    // The shared instance.
    private static UeiPlugin plugin;

    /**
     * Log the specified message for debug purposes.
     * <p>
     * This method will log the message argument when System property
     * {@link #UEI_DEBUG_SYSTEM_PROPERTY} is set as <code>true</code>.
     * </p>
     * <p>
     * The logged message will be: <code>[UEI] &lt;message argument&gt; </code>
     * </p>
     * 
     * @param message the message to be logged
     */
    public static void debugLog(String message) {
        if (debugEnabled) {
            MTJCorePlugin.log(IStatus.INFO, Messages.bind(
                    Messages.UeiPlugin_ueiDebug_tag, message));
        }
    }

    /**
     * Log the specified message for debug purposes.
     * <p>
     * This method will log the message argument when System property
     * {@link #UEI_DEBUG_SYSTEM_PROPERTY} is set as <code>true</code>.
     * </p>
     * <p>
     * The logged message will be: <code>[UEI] &lt;message argument&gt; </code>
     * </p>
     * 
     * @param message the message to be logged
     * @param e the exception to be logged
     */
    public static void debugLog(String message, Exception e) {
        if (debugEnabled) {
            MTJCorePlugin.log(IStatus.INFO, Messages.bind(
                    Messages.UeiPlugin_ueiDebug_tag, message), e);
        }
    }

    /**
     * Returns the shared instance.
     */
    public static UeiPlugin getDefault() {
        return plugin;
    }

    /**
     * Check if the UEI device import debugging is enable.
     * 
     * @return <code>true</code> if debug is enabled; <code>false</code>
     *         otherwise.
     */
    public static boolean isUEIDebugEnabled() {
        return debugEnabled;
    }

    /**
     * The constructor.
     */
    public UeiPlugin() {
        plugin = this;
    }

    /**
     * This method is called upon plug-in activation
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        debugLog(Messages.UeiPlugin_start_message);
    }

    /**
     * This method is called when the plug-in is stopped
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        super.stop(context);
        plugin = null;
    }

}
