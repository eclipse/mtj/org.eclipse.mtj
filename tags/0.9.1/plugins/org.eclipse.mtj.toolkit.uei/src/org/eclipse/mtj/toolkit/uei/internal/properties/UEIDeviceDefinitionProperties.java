package org.eclipse.mtj.toolkit.uei.internal.properties;

import org.eclipse.mtj.toolkit.uei.model.EmulatorInfoArgs;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public enum UEIDeviceDefinitionProperties {

    /**
     * 
     */
    DEBUG_SERVER("debug.server"), //$NON-NLS-1$

    /**
     * 
     */
    LAUNCH_TEMPLATE("launch.template"), //$NON-NLS-1$

    /**
     * <b>Property Key</b>: [&lt;<i>device id</i>&gt;.]<code>match.expression</code><br>
     * <b>Value</b>: a regular expression to match against the output of
     * <code>emulator {@link EmulatorInfoArgs#XQUERY -Xquery}</code> command.
     */
    MATCH_EXPRESSION("match.expression"), //$NON-NLS-1$

    /**
     * 
     */
    PREDEPLOY_REQUIRED("predeploy.required"); //$NON-NLS-1$

    /**
     * 
     */
    private final String property;

    /**
     * @param property
     */
    private UEIDeviceDefinitionProperties(final String property) {
        this.property = property;
    }

    /**
     * @param device
     * @return
     */
    public String createDeviceProperty(final String device) {
        return device + "." + this.toString(); //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return property;
    }
}
