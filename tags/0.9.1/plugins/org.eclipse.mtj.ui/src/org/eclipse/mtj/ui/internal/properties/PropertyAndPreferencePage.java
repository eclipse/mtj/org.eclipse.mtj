/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)  - Initial implementation
 *     Kevin Hunter (Individual) - Added signature support
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse
 *                                 standards
 *     Diego Sandin (Motorola)   - Added work around to remove the built in 
 *                                 preverifier enabling option
 *     Diego Sandin (Motorola)   - Fixed problem on "Obfuscation" properties 
 *                                 page    
 *     Diego Sandin (Motorola)   - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.ui.internal.properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.ui.internal.IEmbeddableWorkbenchPreferencePage;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbenchPropertyPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PropertyPage;

/**
 * Property page implementation that allows a preference page to be embedded
 * within the property page. Provides access to a project-specific preference
 * store that can be used by the preference page for storing project-specific
 * preferences.
 * 
 * @author Craig Setera
 */
public abstract class PropertyAndPreferencePage extends PropertyPage implements
        IWorkbenchPropertyPage {

    protected PreferencePage preferencePage;
    protected Button projectSpecificSettings;

    /**
     * Default constructor.
     */
    public PropertyAndPreferencePage() {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#isValid()
     */
    @Override
    public boolean isValid() {
        boolean isValid = true;

        if (projectSpecificSettings.getSelection()) {
            isValid = preferencePage.isValid();
        }

        return isValid;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performCancel()
     */
    @Override
    public boolean performCancel() {
        return preferencePage.performCancel() && super.performCancel();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        storeProjectSpecificSettingsValue();
        return preferencePage.performOk() && super.performOk();
    }

    /**
     * Recursively enable the specified control.
     * 
     * @param control
     * @param enabled
     */
    private void recursivelySetEnablement(Control control, boolean enabled) {
        if (control instanceof Composite) {
            Control[] children = ((Composite) control).getChildren();
            for (Control element2 : children) {
                recursivelySetEnablement(element2, enabled);
            }
        }

        control.setEnabled(enabled);

    }

    /**
     * Store the current value of the project specific settings into the
     * preference store.
     */
    private void storeProjectSpecificSettingsValue() {
        getPreferenceStore().setValue(getProjectSpecificSettingsKey(),
                projectSpecificSettings.getSelection());
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_J2MEProjectPropertiesPage"); //$NON-NLS-1$

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout());

        createProjectSpecificControls(composite);
        embedPreferencePage(composite);

        initializeState();

        return composite;
    }

    /**
     * Create the controls for project specific setting enablement and
     * disablement.
     * 
     * @param composite
     */
    protected void createProjectSpecificControls(Composite composite) {
        projectSpecificSettings = new Button(composite, SWT.CHECK);
        projectSpecificSettings.setText(MTJUIMessages.PropertyAndPreferencePage_projectSpecificSettings_btn_label_text);
        projectSpecificSettings.addSelectionListener(new SelectionAdapter() {

            /* (non-Javadoc)
             * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                boolean useProjectSpecificSettings = projectSpecificSettings
                        .getSelection();
                updateEmbeddedEnablement(useProjectSpecificSettings);

                ((IEmbeddableWorkbenchPreferencePage) preferencePage)
                        .performDefaults();

            }
        });

        Label horizontalLine = new Label(composite, SWT.SEPARATOR
                | SWT.HORIZONTAL);
        horizontalLine.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#doGetPreferenceStore()
     */
    @Override
    protected IPreferenceStore doGetPreferenceStore() {
        return MTJUIPlugin.getCoreProjectPreferenceStore(getProject());
    }

    /**
     * Embed a preference page into the parent composite, setting things up
     * correctly as we go along.
     * 
     * @param composite
     */
    protected abstract void embedPreferencePage(Composite composite);

    /**
     * Get the selected project or <code>null</code> if a project is not
     * selected.
     * 
     * @return
     */
    protected IProject getProject() {
        IProject project = null;
        IAdaptable adaptable = getElement();

        if (adaptable instanceof IProject) {
            project = (IProject) adaptable;
        } else if (adaptable instanceof IJavaProject) {
            project = ((IJavaProject) adaptable).getProject();
        }

        return project;
    }

    /**
     * Return the key under which the project specific settings boolean is
     * stored in the properties/preferences.
     * 
     * @return
     */
    protected abstract String getProjectSpecificSettingsKey();

    /**
     * Return a boolean indicating whether or not there are project-specific
     * properties.
     * 
     * @return
     */
    protected boolean hasProjectSpecificProperties() {
        String key = getProjectSpecificSettingsKey();
        IPreferenceStore store = getPreferenceStore();
        return store.getBoolean(key);
    }

    /**
     * Initialize the state of the various controls.
     */
    protected void initializeState() {
        boolean readOnly = isReadOnly();

        boolean useProjectSettings = hasProjectSpecificProperties();
        projectSpecificSettings.setSelection(useProjectSettings);
        projectSpecificSettings.setEnabled(!readOnly);

        if (!useProjectSettings || readOnly) {
            updateEmbeddedEnablement(false);
        }

    }

    /**
     * Return a boolean indicating whether the project being edited is
     * preprocessed output.
     * 
     * @return
     */
    protected boolean isPreprocessedOutputProject() {
        boolean preprocessed = false;

        try {
            preprocessed = getProject().hasNature(
                    IMTJCoreConstants.J2ME_PREPROCESSED_NATURE_ID);
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.WARNING, e);
        }

        return preprocessed;
    }

    /**
     * Return a boolean indicating whether this page should be read-only.
     * 
     * @return
     */
    protected abstract boolean isReadOnly();

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performApply()
     */
    @Override
    protected void performApply() {
        storeProjectSpecificSettingsValue();
        ((IEmbeddableWorkbenchPreferencePage) preferencePage).performApply();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
     */
    @Override
    protected void performDefaults() {
        boolean defaultValue = false;

        ((IEmbeddableWorkbenchPreferencePage) preferencePage).performDefaults();
        projectSpecificSettings.setSelection(defaultValue);

        updateEmbeddedEnablement(defaultValue);
    }

    /**
     * Update the enablement of the embedded componentry.
     * 
     * @param enabled
     */
    protected void updateEmbeddedEnablement(boolean enabled) {
        recursivelySetEnablement(preferencePage.getControl(), enabled);
    }
}
