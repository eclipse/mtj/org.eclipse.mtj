/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant;

/**
 * This class is used to store preprocess directive information
 * 
 * @author gma
 * @since 0.9.1
 */
public class PreprocessDirective implements IPreprocessContentAssistModel {
    private String name;
    private String description;
    private String javaDoc;

    public PreprocessDirective() {
        this("");
    }

    public PreprocessDirective(String name) {
        this(name, null);
    }

    public PreprocessDirective(String name, String description) {
        this(name, description, null);
    }

    public PreprocessDirective(String name, String description, String javaDoc) {
        this.name = name;
        this.description = description;
        this.javaDoc = javaDoc;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PreprocessDirective other = (PreprocessDirective) obj;
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    public String getDescription() {
        return description;
    }

    public String getJavaDoc() {
        if (javaDoc == null) {
            javaDoc = PreprocessJavaDocProvider.getDefault()
                    .getDirectiveJavaDoc(this);
        }
        return javaDoc;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.javaDoc = additionalInfo;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        assert name != null;
        this.name = name;
    }

}
