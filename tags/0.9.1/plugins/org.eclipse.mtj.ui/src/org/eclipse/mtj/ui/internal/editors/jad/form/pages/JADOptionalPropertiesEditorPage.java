/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Refactoring the page to add expansibilities
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]                         
 */
package org.eclipse.mtj.ui.internal.editors.jad.form.pages;

import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage;
import org.eclipse.mtj.ui.internal.editors.jad.form.JADFormEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;

/**
 * JAD editor page for handling the optional properties.
 * 
 * @author Craig Setera
 */
public class JADOptionalPropertiesEditorPage extends JADPropertiesEditorPage {

    /**
     * The unique page identifier.
     */
    public static final String ID = "optional"; //$NON-NLS-1$

    /**
     * Creates the <b>Optional</b> JAD Properties EditorPage.
     */
    public JADOptionalPropertiesEditorPage() {
        super(ID, MTJUIMessages.JADOptionalPropertiesEditorPage_title);
    }

    /**
     * Creates the <b>Optional</b> JAD Properties EditorPage.
     * 
     * @param editor the parent editor
     */
    public JADOptionalPropertiesEditorPage(JADFormEditor editor) {
        super(editor, ID, MTJUIMessages.JADOptionalPropertiesEditorPage_title);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getTitle()
     */
    @Override
    public String getTitle() {
        return MTJUIMessages.JADOptionalPropertiesEditorPage_title; 
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#addContextHelp(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void addContextHelp(Composite c) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(c,
                "org.eclipse.mtj.ui.help_JADOptionalPropertiesEditorPage"); //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
     */
    @Override
    protected String getHelpResource() {
        return "/org.eclipse.mtj.doc.user/html/reference/editors/jad_editor/optional.html"; //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionDescription()
     */
    @Override
    protected String getSectionDescription() {
        return MTJUIMessages.JADOptionalPropertiesEditorPage_description;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionTitle()
     */
    @Override
    protected String getSectionTitle() {
        return MTJUIMessages.JADOptionalPropertiesEditorPage_SectionTitle;
    }
}
