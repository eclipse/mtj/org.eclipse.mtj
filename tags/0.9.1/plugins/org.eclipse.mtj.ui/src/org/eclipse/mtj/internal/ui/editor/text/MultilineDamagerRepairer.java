/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.text;

import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.ITokenScanner;

/**
 * Syntax driven presentation damager and presentation repairer. It uses a token
 * scanner to scan the document and to determine its damage and new text
 * presentation. The tokens returned by the scanner are supposed to return text
 * attributes as their data.
 * 
 * @since 0.9.1
 */
public class MultilineDamagerRepairer extends DefaultDamagerRepairer {

    /**
     * @param scanner
     */
    public MultilineDamagerRepairer(ITokenScanner scanner) {
        super(scanner);
    }

    /**
     * @param scanner
     * @param defaultTextAttribute
     */
    public MultilineDamagerRepairer(ITokenScanner scanner,
            TextAttribute defaultTextAttribute) {

        super(scanner, defaultTextAttribute);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.presentation.IPresentationDamager#getDamageRegion(org.eclipse.jface.text.ITypedRegion, org.eclipse.jface.text.DocumentEvent, boolean)
     */
    @Override
    public IRegion getDamageRegion(ITypedRegion partition, DocumentEvent e,
            boolean documentPartitioningChanged) {
        return partition;
    }

    /**
     * Configures the scanner's default return token. This is the text attribute
     * which is returned when none is returned by the current token.
     */
    public void setDefaultTextAttribute(TextAttribute defaultTextAttribute) {
        fDefaultTextAttribute = defaultTextAttribute;
    }
}
