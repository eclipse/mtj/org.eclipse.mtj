/**
 * Copyright (c) 2006,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.mtj.internal.core.text.IEditingModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.ui.editors.l10n.L10nFoldingStructureProvider;

/**
 * @since 0.9.1
 */
public class FoldingStructureProviderFactory {

    /**
     * Creates a new FoldingStructureProvider based on the editor argument.
     * 
     * @param editor
     * @param model
     * @return
     */
    public static IFoldingStructureProvider createProvider(
            MTJSourcePage editor, IEditingModel model) {

        if (model instanceof L10nModel) {
            return new L10nFoldingStructureProvider(editor, model);
        }

        return null;
    }

}
