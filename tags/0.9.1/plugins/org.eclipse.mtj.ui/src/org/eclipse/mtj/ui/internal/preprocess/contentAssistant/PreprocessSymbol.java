/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.mtj.core.model.preprocessor.symbol.Symbol;

/**
 * This class is used to store preprocess symbol information
 * 
 * @author gma
 * @since 0.9.1
 */
public class PreprocessSymbol implements IPreprocessContentAssistModel {
    private static final int DEFAULTSYMBOLTYPE = Symbol.TYPE_ABILITY;

    private String name;

    private int symbolType;

    private List<PreprocessSymbolProviderInfo> providerInfos;

    public PreprocessSymbol(String symbolName) {
        this(symbolName, DEFAULTSYMBOLTYPE);
    }

    public PreprocessSymbol(String symbolName, int symbolType) {
        this.name = symbolName;
        this.symbolType = symbolType;
    }

    public boolean addProvider(PreprocessSymbolProviderInfo provider) {
        return getProviderInfos().add(provider);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PreprocessSymbol other = (PreprocessSymbol) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    public String getDescription() {

        if (symbolType == Symbol.TYPE_CONFIG) {
            return getConfigurationSymbolDesc();
        } else {
            return getAbilitySymbolDesc();
        }

    }

    private String getConfigurationSymbolDesc() {
        return PreprocessContentAssistMessages.symbol_configuration;
    }

    private String getAbilitySymbolDesc() {
        StringBuffer sb = new StringBuffer();
        if (isActive()) {
            sb.append(PreprocessContentAssistMessages.symbol_active);
        } else {
            sb.append(PreprocessContentAssistMessages.symbol_inactive);
        }
        sb.append("(");

        Iterator<String> it = getPossibleValues().iterator();
        while (it.hasNext()) {
            String value = it.next();
            sb.append(value);
            if (it.hasNext()) {
                sb.append(",");
            }
        }

        sb.append(")");
        return sb.toString();
    }

    public String getJavaDoc() {
        return PreprocessJavaDocProvider.getDefault().getSymbolJavaDoc(this);
    }

    public String getName() {
        return name;
    }

    private HashSet<String> getPossibleValues() {
        HashSet<String> values = new HashSet<String>();
        for (PreprocessSymbolProviderInfo provider : getProviderInfos()) {
            values.add(provider.getProvidedSymbolValue());
        }
        return values;
    }

    public List<PreprocessSymbolProviderInfo> getProviderInfos() {
        if (providerInfos == null) {
            providerInfos = new ArrayList<PreprocessSymbolProviderInfo>();
        }
        return providerInfos;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public boolean isActive() {
        for (PreprocessSymbolProviderInfo provider : getProviderInfos()) {
            if (provider.isActive) {
                return true;
            }
        }
        return false;
    }

}
