/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preferences;

import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.preprocess.contentAssistant.template.PreprocessTemplateAccess;
import org.eclipse.ui.texteditor.templates.TemplatePreferencePage;

/**
 * @author gma
 * @see org.eclipse.jface.preference.PreferencePage
 */
public class PreprocessTemplatePreferencePage extends TemplatePreferencePage {

    public PreprocessTemplatePreferencePage() {
        setPreferenceStore(MTJUIPlugin.getDefault().getPreferenceStore());
        setTemplateStore(PreprocessTemplateAccess.getDefault()
                .getTemplateStore());
        setContextTypeRegistry(PreprocessTemplateAccess.getDefault()
                .getTemplateContextRegistry());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.texteditor.templates.TemplatePreferencePage#getFormatterPreferenceKey()
     */
    @Override
    protected String getFormatterPreferenceKey() {
        return IMTJUIConstants.TEMPLATES_USE_CODEFORMATTER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.texteditor.templates.TemplatePreferencePage#isShowFormatterSetting()
     */
    @Override
    protected boolean isShowFormatterSetting() {
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.IPreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        boolean ok = super.performOk();
        MTJUIPlugin.getDefault().savePluginPreferences();
        return ok;
    }
}