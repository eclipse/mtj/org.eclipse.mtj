/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     David Marques (Motorola) - Overriding doSave method.
 *     David Marques (Motorola) - Synchronizing key states with outline. 
 */
package org.eclipse.mtj.internal.ui.editors.l10n;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.internal.core.l10n.L10nApi;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nObject;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.internal.ui.editor.ISortableContentOutlinePage;
import org.eclipse.mtj.internal.ui.editor.MTJFormEditor;
import org.eclipse.mtj.internal.ui.editor.MTJSourcePage;
import org.eclipse.mtj.internal.ui.editor.MultiSourceEditor;
import org.eclipse.mtj.internal.ui.editor.SystemFileEditorInput;
import org.eclipse.mtj.internal.ui.editor.context.InputContext;
import org.eclipse.mtj.internal.ui.editor.context.InputContextManager;
import org.eclipse.mtj.internal.ui.editors.l10n.pages.L10nSourcePage;
import org.eclipse.mtj.internal.ui.editors.l10n.pages.LocalizationPage;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IStorageEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.IMessageManager;
import org.eclipse.ui.forms.editor.IFormPage;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class LocalizationDataEditor extends MultiSourceEditor {

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#canCut(org.eclipse.jface.viewers.ISelection)
     */
    @Override
    public boolean canCut(ISelection selection) {
        if (selection instanceof IStructuredSelection) {
            IStructuredSelection sel = (IStructuredSelection) selection;
            for (Iterator<?> iter = sel.iterator(); iter.hasNext();) {
                Object obj = iter.next();
                if (obj instanceof L10nObject
                        && ((L10nObject) obj).canBeRemoved()) {
                    return canCopy(selection);
                }
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.IInputContextListener#contextRemoved(org.eclipse.mtj.internal.ui.editor.context.InputContext)
     */
    public void contextRemoved(InputContext context) {
        close(false);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#editorContextAdded(org.eclipse.mtj.internal.ui.editor.context.InputContext)
     */
    @Override
    public void editorContextAdded(InputContext context) {
        addSourcePage(context.getId());

    }

    @Override
    protected void pageChange(int newPageIndex) {
        super.pageChange(newPageIndex);
        IFormPage page = getActivePageInstance();
        if (page instanceof LocalizationPage) {
            L10nModel model = (L10nModel) inputContextManager.findContext(L10nInputContext.CONTEXT_ID).getModel();
            if (model != null) {                
                displayLocaleKeysConflict(model.getLocales());
            }
        }
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#getSelection()
     */
    @Override
    public ISelection getSelection() {
        IFormPage formPage = getActivePageInstance();
        if (formPage != null && formPage instanceof LocalizationPage) {
            // Synchronizes the selection made in the master tree view with the
            // selection in the outline view when the link with editor button
            // is toggled on
            return ((LocalizationPage) formPage).getSelection();
        }

        return super.getSelection();
    }


    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    public void doSave(IProgressMonitor monitor) {
        super.doSave(monitor);
        
        try {            
            L10nModel model = (L10nModel) inputContextManager.findContext(L10nInputContext.CONTEXT_ID).getModel();
            model.validate();
            L10nApi.syncronizeApi(model);
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        }
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#isSaveAsAllowed()
     */
    @Override
    public boolean isSaveAsAllowed() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.IInputContextListener#monitoredFileAdded(org.eclipse.core.resources.IFile)
     */
    public void monitoredFileAdded(IFile monitoredFile) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.IInputContextListener#monitoredFileRemoved(org.eclipse.core.resources.IFile)
     */
    public boolean monitoredFileRemoved(IFile monitoredFile) {
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#addEditorPages()
     */
    @Override
    protected void addEditorPages() {

        try {
            addPage(new LocalizationPage(this));
        } catch (PartInitException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        }
        addSourcePage(L10nInputContext.CONTEXT_ID);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#createContentOutline()
     */
    @Override
    protected ISortableContentOutlinePage createContentOutline() {
        return new L10nFormOutlinePage(this);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#createInputContextManager()
     */
    @Override
    protected InputContextManager createInputContextManager() {
        return new L10nInputContextManager(this);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#createResourceContexts(org.eclipse.mtj.internal.ui.editor.context.InputContextManager, org.eclipse.ui.IFileEditorInput)
     */
    @Override
    protected void createResourceContexts(InputContextManager contexts,
            IFileEditorInput input) {

        contexts.putContext(input, new L10nInputContext(this, input, true));
        contexts.monitorFile(input.getFile());

    }

    /* (non-Javadoc)
     * @see org.eclipse.pde.internal.ui.editor.MultiSourceEditor#createSourcePage(org.eclipse.pde.internal.ui.editor.PDEFormEditor, java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    protected MTJSourcePage createSourcePage(MTJFormEditor editor,
            String title, String name, String contextId) {
        return new L10nSourcePage(editor, title, name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#createStorageContexts(org.eclipse.mtj.internal.ui.editor.context.InputContextManager, org.eclipse.ui.IStorageEditorInput)
     */
    @Override
    protected void createStorageContexts(InputContextManager contexts,
            IStorageEditorInput input) {
        contexts.putContext(input, new L10nInputContext(this, input, true));

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#createSystemFileContexts(org.eclipse.mtj.internal.ui.editor.context.InputContextManager, org.eclipse.mtj.internal.ui.editor.SystemFileEditorInput)
     */
    @Override
    protected void createSystemFileContexts(InputContextManager contexts,
            SystemFileEditorInput input) {
        File file = (File) input.getAdapter(File.class);
        if (file != null) {
            IEditorInput in = new SystemFileEditorInput(file);
            contexts.putContext(in, new L10nInputContext(this, in, true));
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#getEditorID()
     */
    @Override
    protected String getEditorID() {
        return IMTJUIConstants.LOCALIZATION_DATA_EDITOR;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormEditor#getInputContext(java.lang.Object)
     */
    @Override
    protected InputContext getInputContext(Object object) {
        return inputContextManager.findContext(L10nInputContext.CONTEXT_ID);
    }
    
    /**
     * Displays conflicting locales message.
     * 
     * @param locales locales.
     */
    public void displayLocaleKeysConflict(L10nLocales locales) {
        List<L10nLocale> conflicted = L10nUtil.findConflictedLocales(locales);
        IMessageManager  manager = getActivePageInstance().getManagedForm().getMessageManager();
        manager.removeAllMessages();
        for (L10nLocale locale : conflicted) {
            String message = NLS.bind("The locale {0} has duplicated keys.", locale.getName());
            manager.addMessage(locale, message, null, IMessageProvider.ERROR);
        }
    }
}
