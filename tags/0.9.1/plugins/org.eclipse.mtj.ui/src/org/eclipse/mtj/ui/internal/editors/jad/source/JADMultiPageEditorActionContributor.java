/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.editors.jad.source;

import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.SubActionBars;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;
/**
 * JAD muti-page editor action bar contributor 
 * @author gma
 *
 */
public class JADMultiPageEditorActionContributor extends
        MultiPageEditorActionBarContributor {
    private IEditorActionBarContributor fSourceViewerActionContributor;
    private IEditorActionBarContributor fDesignViewerActionContributor;

    private SubActionBars fSourceActionBars;

    public JADMultiPageEditorActionContributor() {
        fSourceViewerActionContributor = new JADSourceEditorActionContributor();
    }

    @Override
    public void dispose() {
        fSourceActionBars.dispose();
        fSourceViewerActionContributor.dispose();
        super.dispose();
    }

    @Override
    public void init(IActionBars bars) {
        super.init(bars);
        fSourceActionBars = new SubActionBars(bars);
        initSourceViewerActionContributor(fSourceActionBars);
        initDesignViewerActionContributor(bars);
    }

    private void initDesignViewerActionContributor(IActionBars bars) {
        if (fDesignViewerActionContributor != null) {
            fDesignViewerActionContributor.init(bars, getPage());
        }
    }

    private void initSourceViewerActionContributor(IActionBars actionBars) {
        if (fSourceViewerActionContributor != null) {
            fSourceViewerActionContributor.init(actionBars, getPage());
        }
    }

    @Override
    public void setActivePage(IEditorPart activeEditor) {

        boolean isSourcePage = activeEditor instanceof JADSourceEditor;
        if (isSourcePage) {
            fSourceViewerActionContributor.setActiveEditor(activeEditor);
        }
        setSourceActionBarsActive(isSourcePage);
    }

    private void setSourceActionBarsActive(boolean isSourcePage) {
        IActionBars rootBars = getActionBars();
        rootBars.clearGlobalActionHandlers();
        rootBars.updateActionBars();

        if (isSourcePage) {
            fSourceActionBars.activate();
        } else {
            fSourceActionBars.deactivate();
        }
    }

}
