/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * this class provides preprocess prefix handler for Antenna preprocessor
 * 
 * @author gma
 * @since 0.9.1
 */
public class AntennaPPPrefixHandler implements IPreprocessPrefixHandler {

    public static Pattern preprocessStatPattern = Pattern
            .compile("(\\s*//\\s*#).*");

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.preprocess.contentAssistant.IPreprocessPrefixHandler#getCurrPrefix(java.lang.String)
     */
    public String getCurrPrefix(String line) {
        Matcher matcher = preprocessStatPattern.matcher(line);
        matcher.matches();
        return matcher.group(1);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.preprocess.contentAssistant.IPreprocessPrefixHandler#hasLegalPrefix(java.lang.String)
     */
    public boolean hasLegalPrefix(String line) {
        Matcher matcher = preprocessStatPattern.matcher(line);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

}
