/**
 * Copyright (c) 2006,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDEQuickAssistAssistant
 */
package org.eclipse.mtj.internal.ui.editor.text;

import java.util.Iterator;
import java.util.ResourceBundle;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationAccessExtension;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.IVerticalRulerInfo;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.texteditor.AbstractMarkerAnnotationModel;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.ITextEditorExtension;
import org.eclipse.ui.texteditor.SelectMarkerRulerAction;

/**
 * A ruler action which can select the textual range of a marker that has a
 * visual representation in a vertical ruler.
 * 
 * @since 0.9.1
 */
public class MTJSelectAnnotationRulerAction extends SelectMarkerRulerAction {

    private boolean fIsEditable;
    private ITextEditor fTextEditor;
    private Position fPosition;
    private ResourceBundle fBundle;
    private String fPrefix;

    /**
     * @param bundle
     * @param prefix
     * @param editor
     * @param ruler
     */
    public MTJSelectAnnotationRulerAction(ResourceBundle bundle, String prefix,
            ITextEditor editor, IVerticalRulerInfo ruler) {
        super(bundle, prefix, editor, ruler);
        fTextEditor = editor;
        fBundle = bundle;
        fPrefix = prefix;
    }

    public void run() {
        runWithEvent(null);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.action.Action#runWithEvent(org.eclipse.swt.widgets.Event)
     */
    public void runWithEvent(Event event) {
        if (fIsEditable) {
            ITextOperationTarget operation = (ITextOperationTarget) fTextEditor
                    .getAdapter(ITextOperationTarget.class);
            final int opCode = ISourceViewer.QUICK_ASSIST;
            if (operation != null && operation.canDoOperation(opCode)) {
                fTextEditor.selectAndReveal(fPosition.getOffset(), fPosition
                        .getLength());
                operation.doOperation(opCode);
            }
            return;
        }

        super.run();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.SelectMarkerRulerAction#update()
     */
    public void update() {
        checkReadOnly();

        if (fIsEditable) {
            initialize(fBundle, fPrefix + "QuickFix."); //$NON-NLS-1$
        }

        super.update();
    }

    /**
     * 
     */
    private void checkReadOnly() {
        fPosition = null;
        fIsEditable = false;

        AbstractMarkerAnnotationModel model = getAnnotationModel();
        IAnnotationAccessExtension annotationAccess = getAnnotationAccessExtension();

        IDocument document = getDocument();
        if (model == null)
            return;

        Iterator<?> iter = model.getAnnotationIterator();
        int layer = Integer.MIN_VALUE;

        while (iter.hasNext()) {
            Annotation annotation = (Annotation) iter.next();
            if (annotation.isMarkedDeleted())
                continue;

            int annotationLayer = annotationAccess.getLayer(annotation);
            if (annotationAccess != null)
                if (annotationLayer < layer)
                    continue;

            Position position = model.getPosition(annotation);
            if (!includesRulerLine(position, document))
                continue;

            boolean isReadOnly = fTextEditor instanceof ITextEditorExtension
                    && ((ITextEditorExtension) fTextEditor)
                            .isEditorInputReadOnly();
            if (!isReadOnly) {
                fPosition = position;
                fIsEditable = true;
                layer = annotationLayer;
                continue;
            }
        }
    }
}
