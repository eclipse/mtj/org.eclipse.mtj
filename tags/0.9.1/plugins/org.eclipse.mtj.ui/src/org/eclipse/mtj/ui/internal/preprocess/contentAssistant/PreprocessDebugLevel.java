/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant;

/**
 * This class is used to store debug level information
 * 
 * @author gma
 * @since 0.9.1
 */
public class PreprocessDebugLevel implements IPreprocessContentAssistModel {
    private String name;
    private String description;
    private String javaDoc;

    public PreprocessDebugLevel(String name) {
        this(name, null);
    }

    public PreprocessDebugLevel(String name, String description) {
        this(name, description, null);
    }

    public PreprocessDebugLevel(String name, String description, String javaDoc) {
        this.name = name;
        this.description = description;
        this.javaDoc = javaDoc;
    }

    public String getDescription() {
        return description;
    }

    public String getJavaDoc() {
        return javaDoc;
    }

    public String getName() {
        return name;
    }

}
