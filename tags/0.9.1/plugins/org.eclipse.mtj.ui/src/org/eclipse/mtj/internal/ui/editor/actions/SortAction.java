/**
 * Copyright (c) 2006,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;

/**
 * @since 0.9.1
 */
public class SortAction extends Action {

    private ViewerComparator comparator;

    private ViewerComparator defaultComparator;

    private boolean sorted;

    private StructuredViewer viewer;

    /**
     * @param viewer
     * @param tooltipText
     * @param sorter
     * @param defaultSorter
     * @param listener
     * @param useMiniImage
     */
    public SortAction(StructuredViewer viewer, String tooltipText,
            ViewerComparator sorter, ViewerComparator defaultSorter,
            IPropertyChangeListener listener) {

        super(tooltipText, IAction.AS_CHECK_BOX);

        // Set the tooltip
        setToolTipText(tooltipText);

        // Set the image
        setImageDescriptor(MTJUIPluginImages.DESC_ALPHAB_SORT_CO);

        // Set the default comparator
        defaultComparator = defaultSorter;

        // Set the viewer
        this.viewer = viewer;

        // Set the comparator
        // If one was not specified, use the default
        if (sorter == null) {
            comparator = new ViewerComparator();
        } else {
            comparator = sorter;
        }

        // Determine if the viewer is already sorted
        // Note: Most likely the default comparator is null
        if (viewer.getComparator() == defaultComparator) {
            sorted = false;
        } else {
            sorted = true;
        }

        // Set the status of this action depending on whether it is sorted or
        // not
        setChecked(sorted);

        // If a listener was specified, use it
        if (listener != null) {
            addListenerObject(listener);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {

        // Toggle sorting on/off
        if (sorted) {
            // Sorting is on
            // Turn it off
            viewer.setComparator(defaultComparator);
            sorted = false;
        } else {
            // Sorting is off
            // Turn it on
            viewer.setComparator(comparator);
            sorted = true;
        }
        notifyResult(true);
    }
}
