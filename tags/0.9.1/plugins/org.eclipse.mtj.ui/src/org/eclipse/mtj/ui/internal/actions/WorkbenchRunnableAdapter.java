/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Adapted code available in JDT
 */
package org.eclipse.mtj.ui.internal.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.operation.IThreadListener;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;

/**
 * An <code>IRunnableWithProgress</code> that adapts and
 * <code>IWorkspaceRunnable</code> so that is can be executed inside
 * <code>IRunnableContext</code>. <code>OperationCanceledException</code> thrown
 * by the adapted runnable are caught and re-thrown as a
 * <code>InterruptedException</code>.
 */
public class WorkbenchRunnableAdapter implements IRunnableWithProgress,
        IThreadListener {

    private ISchedulingRule rule;
    private boolean transfer = false;
    private IWorkspaceRunnable workspaceRunnable;

    /**
     * Runs a workspace runnable with the workspace lock.
     * 
     * @param runnable the runnable
     */
    public WorkbenchRunnableAdapter(IWorkspaceRunnable runnable) {
        this(runnable, ResourcesPlugin.getWorkspace().getRoot());
    }

    /**
     * Runs a workspace runnable with the given lock or <code>null</code> to run
     * with no lock at all.
     * 
     * @param runnable the runnable
     * @param rule the scheduling rule
     */
    public WorkbenchRunnableAdapter(IWorkspaceRunnable runnable,
            ISchedulingRule rule) {
        workspaceRunnable = runnable;
        this.rule = rule;
    }

    /**
     * Runs a workspace runnable with the given lock or <code>null</code> to run
     * with no lock at all.
     * 
     * @param runnable the runnable
     * @param rule the scheduling rule
     * @param transfer <code>true</code> if the rule is to be transfered to the
     *            model context thread. Otherwise <code>false</code>
     */
    public WorkbenchRunnableAdapter(IWorkspaceRunnable runnable,
            ISchedulingRule rule, boolean transfer) {
        workspaceRunnable = runnable;
        this.rule = rule;
        this.transfer = transfer;
    }

    /**
     * @return the scheduling rule
     */
    public ISchedulingRule getSchedulingRule() {
        return rule;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.operation.IRunnableWithProgress#run(org.eclipse.core.runtime.IProgressMonitor)
     */
    public void run(IProgressMonitor monitor) throws InvocationTargetException,
            InterruptedException {
        try {
            JavaCore.run(workspaceRunnable, rule, monitor);
        } catch (OperationCanceledException e) {
            throw new InterruptedException(e.getMessage());
        } catch (CoreException e) {
            throw new InvocationTargetException(e);
        }
    }

    /**
     * @param name
     * @param jobFamiliy
     */
    public void runAsUserJob(String name, final Object jobFamiliy) {

        Job job = new Job(name) {

            /* (non-Javadoc)
             * @see org.eclipse.core.runtime.jobs.Job#belongsTo(java.lang.Object)
             */
            @Override
            public boolean belongsTo(Object family) {
                return jobFamiliy == family;
            }

            /* (non-Javadoc)
             * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.IProgressMonitor)
             */
            @Override
            protected IStatus run(IProgressMonitor monitor) {
                try {
                    WorkbenchRunnableAdapter.this.run(monitor);
                } catch (InvocationTargetException e) {
                    Throwable cause = e.getCause();
                    if (cause instanceof CoreException) {
                        return ((CoreException) cause).getStatus();
                    } else {
                        return new Status(IStatus.ERROR,
                                IMTJUIConstants.PLUGIN_ID, cause.getMessage(),
                                cause);
                    }
                } catch (InterruptedException e) {
                    return Status.CANCEL_STATUS;
                } finally {
                    monitor.done();
                }
                return Status.OK_STATUS;
            }
        };
        job.setRule(rule);
        job.setUser(true);
        job.schedule();

    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.operation.IThreadListener#threadChange(java.lang.Thread)
     */
    public void threadChange(Thread thread) {
        if (transfer) {
            Job.getJobManager().transferRule(rule, thread);
        }
    }
}
