/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.core.library.model.security;

/**
 * A protection domain is a way to differentiate between downloaded MIDlet
 * suites based on the entity that signed the MIDlet suite, and to grant or make
 * available to a MIDlet suite a set of permissions. A domain binds a Protection
 * Domain Root Certificate to a set of permissions.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class ProtectionDomain {

    /**
     * @author Diego Madruga Sandin
     * @since 0.9.1
     */
    public enum ProtectionDomainType {

        /**
         * The trusted manufacturer Protection Domain Root Certificate is used
         * to verify manufacturer MIDlet suites.
         */
        MANUFACTURER,

        /**
         * A trusted operator Protection Domain Root Certificate is used to
         * verify operator MIDlet suites.
         */
        OPERATOR,

        /**
         * A trusted third party Protection Domain Root Certificate is used to
         * verify third party MIDlet suites.
         */
        THIRD_PARTY,

        /**
         * MIDlets suites that are unsigned will belong to the Untrusted domain.
         */
        UNTRUSTED;

        /**
         * The string key correspondent to
         * {@link ProtectionDomainType#MANUFACTURER}
         */
        private static final String MANUFACTURER_KEY = "MANUFACTURER"; //$NON-NLS-1$

        /**
         * The string key correspondent to {@link ProtectionDomainType#OPERATOR}
         */
        private static final String OPERATOR_KEY = "OPERATOR"; //$NON-NLS-1$

        /**
         * The string key correspondent to
         * {@link ProtectionDomainType#THIRD_PARTY}
         */
        private static final String THIRD_PARTY_KEY = "THIRD_PARTY"; //$NON-NLS-1$

        /**
         * The string key correspondent to
         * {@link ProtectionDomainType#UNTRUSTED}
         */
        private static final String UNTRUSTED_KEY = "UNTRUSTED"; //$NON-NLS-1$

        /**
         * Get the correspondent {@link ProtectionDomainType} value for the
         * protectionDomain string.
         * 
         * @param protectionDomain one of the following values:
         *            <b>MANUFACTURER</b> | <b>OPERATOR</b> | <b>THIRD_PARTY</b>
         *            | <b>UNTRUSTED</b>
         * @return the correspondent {@link ProtectionDomainType} for the
         *         protectionDomain string.
         * @throws IllegalArgumentException when the protectionDomain string is
         *             different from one of the following values:
         *             <b>MANUFACTURER</b> | <b>OPERATOR</b> |
         *             <b>THIRD_PARTY</b> | <b>UNTRUSTED</b>;
         */
        public static ProtectionDomainType getProtectionDomainType(
                String protectionDomain) throws IllegalArgumentException {

            if (protectionDomain.equals(MANUFACTURER_KEY)) {
                return ProtectionDomainType.MANUFACTURER;
            } else if (protectionDomain.equals(OPERATOR_KEY)) {
                return ProtectionDomainType.OPERATOR;
            } else if (protectionDomain.equals(THIRD_PARTY_KEY)) {
                return ProtectionDomainType.THIRD_PARTY;
            } else if (protectionDomain.equals(UNTRUSTED_KEY)) {
                return ProtectionDomainType.UNTRUSTED;
            } else {
                throw new IllegalArgumentException(
                        "Protection  Domain string must be one of the following: " //$NON-NLS-1$
                                + "MANUFACTURER | OPERATOR | THIRD_PARTY | UNTRUSTED"); //$NON-NLS-1$
            }
        }
    }

    private ProtectionDomainType protectionDomain = ProtectionDomainType.UNTRUSTED;

    /**
     * Creates a new ProtectionDomain.
     * 
     * @param protectionDomain a protection domain type.
     */
    public ProtectionDomain(ProtectionDomainType protectionDomain) {
        this.protectionDomain = protectionDomain;
    }

    /**
     * Creates a new ProtectionDomain.
     * 
     * @param protectionDomainType one of the following string values:
     *            <b>MANUFACTURER</b> | <b>OPERATOR</b> | <b>THIRD_PARTY</b> |
     *            <b>UNTRUSTED</b>
     * @throws IllegalArgumentException if protectionDomainType is null or
     *             different from one of the following values:
     *             <b>MANUFACTURER</b> | <b>OPERATOR</b> | <b>THIRD_PARTY</b> |
     *             <b>UNTRUSTED</b>.
     */
    public ProtectionDomain(final String protectionDomainType)
            throws IllegalArgumentException {

        if (protectionDomainType == null) {
            throw new IllegalArgumentException("Domain string must not be null"); //$NON-NLS-1$
        }

        this.protectionDomain = ProtectionDomainType
                .getProtectionDomainType(protectionDomainType);
    }

    /**
     * Return the current protection domain type.
     * 
     * @return the protectionDomain the protection domain type for this
     *         instance.
     */
    public ProtectionDomainType getProtectionDomainType() {
        return protectionDomain;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((protectionDomain == null) ? 0 : protectionDomain.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ProtectionDomain)) {
            return false;
        }
        ProtectionDomain other = (ProtectionDomain) obj;
        if (protectionDomain == null) {
            if (other.protectionDomain != null) {
                return false;
            }
        } else if (!protectionDomain.equals(other.protectionDomain)) {
            return false;
        }
        return true;
    }
}
