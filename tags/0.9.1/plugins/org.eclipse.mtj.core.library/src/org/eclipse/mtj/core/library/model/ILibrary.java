/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.core.library.model;

import java.util.List;

import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.mtj.core.library.model.licence.LicenceInfo;
import org.eclipse.mtj.core.library.model.security.SecurityInfo;
import org.osgi.framework.Version;

/**
 * Representation of the libraries that are provided by 3rd party vendors.
 * <p>
 * Each library will be composed by the following members:
 * <ul>
 * <li>Name</li>
 * <li>Description</li>
 * <li>{@link Version}</li>
 * <li>{@link Visibility}</li>
 * <li><b>Security</b> (0 - 1)
 * <ul>
 * <li>Protection Domain</li>
 * <li>Permissions</li>
 * </ul>
 * </li>
 * <li><b>License</b> (1 - 1)
 * <ul>
 * <li>Name</li>
 * <li>Url</li>
 * </ul>
 * </li>
 * <li><b>ClasspathEntries</b> (1 - *)
 * <ul>
 * <li>Jar File Path</li>
 * <li>Javadoc Path</li>
 * <li>Source Path</li>
 * <li><b>AccessRule</b> (0 - *)
 * <ul>
 * <li>Kind</li>
 * <li>Pattern</li>
 * </ul>
 * </li>
 * </ul>
 * </li>
 * </ul>
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public interface ILibrary {

    /**
     * Appends the specified element to the end of the ClasspathEntry List.
     * 
     * @param classpathEntry element to be appended to the ClasspathEntry List.
     */
    public void addClasspathEntry(IClasspathEntry classpathEntry);

    /**
     * @return the classpathEntry
     */
    public List<IClasspathEntry> getClasspathEntryList();

    /**
     * @return
     */
    public String getDescription();

    /**
     * @return Returns the identifier.
     */
    public String getIdentifier();

    /**
     * @return the license.
     */
    public LicenceInfo getLicence();

    /**
     * @return Returns the name.
     */
    public String getName();

    /**
     * @return the security info.
     */
    public SecurityInfo getSecurity();

    /**
     * @return Returns the version.
     */
    public Version getVersion();

    /**
     * @return the visibility for this library.
     */
    public Visibility getVisibility();

    /**
     * Removes the first occurrence in the LibraryItem List of the specified
     * element.
     * 
     * @param classpathEntry element to be removed from the LibraryItem List, if
     *            present.
     */
    public void removeClasspathEntry(IClasspathEntry classpathEntry);

    /**
     * @param classpathEntryList the classpathEntryList to set
     */
    public void setClasspathEntryList(List<IClasspathEntry> classpathEntryList);

    /**
     * @param description
     */
    public void setDescription(String description);

    /**
     * @param identifier The identifier to set.
     */
    public void setIdentifier(String identifier);

    /**
     * @param name The name to set.
     */
    public void setName(String name);

    /**
     * @param version The version to set.
     */
    public void setVersion(Version version);

    /**
     * Sets the visibility for the library.
     * 
     * @param visibility
     */
    public void setVisibility(Visibility visibility);

}
