/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.example.library;

/**
 * An accessible class from outside the library bundle.
 * 
 * @author Diego Madruga Sandin
 */
public class AccessibleLibraryClass {

    /**
     * Creates a new AccessibleLibraryClass
     */
    public AccessibleLibraryClass() {
    }

    /**
     * An accessible method from outside the library bundle.
     * 
     * @return the
     *         <code>"This method is accessible from outside the library bundle."</code>
     *         string.
     */
    public String AccessibleMethod() {
        return "This method is accessible from outside the library bundle.";
    }
}
