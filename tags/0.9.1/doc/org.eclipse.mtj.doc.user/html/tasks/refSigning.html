<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="copyright"
	content="Copyright (c) Motorola and others 2008. This page is made available under license. For full details see the LEGAL in the documentation book that contains this page.">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style/book.css"/>
<title>Signing MIDlet suites</title>
</head>

<body>
<h1>Signing MIDlet suites</h1>
<p>
MTJ supports cryptographic signing of MIDlet
suites.  This page provides background material on the signing process,
provides an overview of how you go about signing a MIDlet suite manually,
and then describes how MTJ handles this.
</p>
<ol>
	<li class="TOC"><a class="TOC" href="#background">Background</a></li>
	<li class="TOC"><a class="TOC" href="#basicSteps">Basic steps</a></li>
	<li class="TOC"><a class="TOC" href="#MTJSigning">Signing using MTJ</a></li>
	<li class="TOC"><a class="TOC" href="#passwords">Password management</a></li>
	<li class="TOC"><a class="TOC" href="#emulation">Emulation</a></li>
	<li class="TOC"><a class="TOC" href="#refs">References</a></li>
</ol>
<hr>
<h2 id="background">Background</h2>
<p>
Under the MIDP 1.0 specification, all MIDlets operated using a
"sandbox" security model.  Essentially, a MIDlet could only access
API's that were specifically included as part of the MIDP 1.0
specification, or to libraries that were bundled as part of the
same MIDlet suite.
</p>
<p>
MIDP 2.0 (<a href="#refs">JSR-118</a>) 
introduced the concept of "trusted" and "untrusted"
MIDlet suites.  An "untrusted" suite operates under the same
restrictions as in MIDP 1.0 - the only API's that can be accessed
are those that are part of the MIDP specification.  "Trusted" suites
can be granted access to a wider selection of API's on the device.
In addition, as part of the MIDP 2.0 specification, certain
"standard" API's were provided that required authorization to use.
For example, an untrusted MIDlet suite must be able to access the
javax.microedition.io.HttpConnection API, but the system is required
to make the user confirm that it is OK for the MIDlet suite to
use this API.
</p>
<p>
Trusted MIDlet suites can, on the other hand, be granted access to 
API's without explicit user authorization, and may have access to
API's that an untrusted MIDlet is not allowed to access. Details of
all of this are spelled out in JSR-118, and so will not be repeated
here.
</p>
<h2 id="basicSteps">Basic Steps in Signing a MIDlet Suite</h2>
<p>
The basic steps in signing a MIDlet suite are:
</p>
<ol>
<li><a href="#permissions">Include the list of permissions your MIDlet suite is requesting in the JAD file and manifest</a></li>
<li><a href="#getKey">Obtain an appropriate cryptographic key/certificate pair to do the signing</a></li>
<li><a href="#sign">Sign the MIDlet suite as the final step in deploying it</a></li>
</ol>
<h3 id="permissions">Including the list of permissions your MIDlet suite is requesting 
in the JAD file and manifest</h3>
<p>
A MIDlet requests permissions by declaring them in the application descriptor and
JAR manifest, using either 
<strong>MIDlet-Permissions</strong> or <strong>MIDlet-Permissions-Opt</strong>
tag. 
Multiple permissions can be specified with either tag, and are separated by commas.
During the installation process, the device is required to look at the requested permissions.
If a permission is declared in 
<strong>MIDlet-Permissions</strong>, then the device must either be prepared to
install the MIDlet in a protection domain that grants access to these features, or
the device must abort the installation. 
However, if the permission is declared in <strong>MIDlet-Permissions-Opt</strong>, 
and the MIDlet suite fails to gain access to an appropriate protection domain, the installation 
can continue but the suite will not have access to the requested permission. So, if your MIDlet 
suite depends on having a permission, and cannot operate without it, this permission should be 
requested in <strong>MIDlet-Permissions</strong>. 
</p>
<p>
If your application can live without a specific permission, but would make use of the 
feature if access is granted, request the permission using <strong>MIDlet-Permissions-Opt</strong>.
If your MIDlet is not granted permission to the feature, you should get a SecurityException when you
try to access it.  Note that for HTTP in particular, certain device providers violate the specification just 
slightly and throw an IOException, instead of a SecurityException, in an odd attempt to be compatible with
MIDP 1.0.  Check your tookit provider's documentation on this.
</p>
<p> 
The following is an example of requested permissions in a JAD file. The MIDlet suite is indicating 
it must have access to HTTP and HTTPS and would like to have access to the 
<strong>PushRegistry, VideoControl.getSnapshot</strong>, and SMS. If the latter 
three are denied, the suite can adjust and still provide value to the user. 
</p>
<pre class="codeindent">MIDlet-Permissions: javax.microedition.io.Connector.http, javax.microedition.io.Connector.https
MIDlet-Permissions-Opt: javax.microedition.io.PushRegistry, javax.microedition.media.control.VideoControl.getSnapshot, javax.wireless.messaging.sms.receive, javax.wireless.messaging.sms.send
</pre>
<p>
In this example, HTTP and HTTPS are not requested as optional. Because the application has 
indicated it cannot function without these resources, if the device is unable to grant these 
permissions on installation, the installation will abort.  Whether or not the device will be
able to grant permission is dependent on its own security policies, and whether or not the
MIDlet is trusted.  In order to be trusted, the MIDlet must be signed.
</p>
<h3 id="getKey">Obtaining an appropriate cryptographic key/certificate pair to do the signing</h3>
<p>
MIDlet security is based around public key cryptography.  The basic steps are:
</p>
<ol>
<li>You create a public key/private key pair.</li>
<li>You use the private key to digitally "sign" the MIDlet suite.  </li>
<li>You then include the public key
as part of the MIDlet suite so that anybody else can validate the signed information. </li>
</ol>
<p>
Since only your
private key can generate information that your public key can decrypt, so as long as you keep your
private key a secret, the world can be sure that anything that can be verified by your public
key was prepared by you.
</p>
<p>
Of course, this still begs the question of how someone knows that a particular public key
really came from you.  What is to prevent me from claiming to be Bill Gates, and digitally
signing documents accordingly?  Instead of just including your public key in the MIDlet
suite, you include a certificate that, in turn,
includes the public key.  The certificate is issued by a trusted third party such as Verisign,
and essentially promises that this public key really did come from you.
</p>
<p>
How does the device know to trust the Verisign certificate?  Devices are programmed with a
set of "root certificate authorities" that they will always trust.  Certificates are themselves
signed, using information that is pre-programmed into the phones.  Thus, the device can
verify that your certificate was actually issued by Verisign by using its pre-programmed
information.
Alternately, your certificate may be signed by an "intermediate" key, which is itself,
in turn, signed by a trusted key.  This is called a "certificate chain."  Chains can actually
be any number of keys in length, although it's rare that they contain more than two or three
links.
</p>
<p>
As a result, the process for getting your keys set up typically works like this:
</p>
<ul>
<li>You generate a public/private key pair using a convenient tool.</li>
<li>You generate a "certificate request" that contains your public key, and send it to a
certificate authority.</li>
<li>The certificate authority does a little checking to make sure that you are who you say you are.
What kind of checking is done varies from authority to authority.</li>
<li>Once the certificate authority is satisfied that you are who you say you are (and
you pay them their fee) the certificate authority wraps your
public key in a certificate, and sends it back to you.</li>
<li>You import the certificate back into your keystore so that you will have it
when you need to sign something.</li>
</ul>
<p>
As part of its JDK, Sun provides a tool that will help with the management of keys.  The tool is,
appropriately enough, named <span class="code">keytool</span>.
</p>
<p>
To generate a new public/private key pair, you execute the command:
</p>
<pre class="codeindent">keytool -genkey -keyalg RSA -keystore &lt;keystore file name&gt;</pre>
<p>
This instructs the tool to build a new RSA public/private key pair, and place it in the specified
"keystore" file.  As part of this process, you will be asked a variety of information about
yourself.  In addition, you will be asked for the following items:
</p>
<table>
<tr>
<td class="bold">Key&nbsp;Alias</td>
<td>A keystore can hold multiple key/certificate pairs.  The "alias" identifies the particular
one with the keystore file</td>
</tr>
<tr>
<td class="bold">Keystore&nbsp;password</td>
<td>Your keystore obviously contains information that not just anybody should have access to.
As a result, the keystore file is stored in an encrypted format.  This is the password that
grants access to the keystore file</td>
</tr>
<tr>
<td class="bold">Key&nbsp;password</td>
<td>With the keystore password, you gain access to the "public" half of the keystore - the
public keys and certificates.  In order to access the private key, an additional, key-specific
password is required.  This protects the private key even more than the rest of the keystore
information.</td>
</tr>
</table>
<p>
It is possible to specify all these quantities from the command line of <span class="code">keytool</span>
if you want.  Run
</p>
<pre class="codeindent">keytool -help</pre>
<p>
or consult the documentation on Sun's site for more information about this.
</p>
<p>
If all you want to do is to "play" with signing, you can skip the rest of this section
and jump down to the discussion on <a href="#sign">signing</a>.
When you generate your key, <span class="code">keytool</span> automatically adds a
"self-signed certificate" to the keystore.  This certificate is of the correct format
to allow a MIDlet suite to be signed with it.  Note that this will <strong>NOT</strong>
result in your MIDlet suite becoming trusted in production, since your mobile device
will not be able to verify this "personal" certificate with any of its trusted
certificate authorities.  To do that, you have to go through the proper certificate request
process described below.  The "self-signed" certificate, however, is sufficient if all you
want to do is experiment with MTJ's signing support - you can wait and get a
"real" certificate later.
</p>
<p>
To generate a certificate request using keytool, execute the following command:
</p>
<pre class="codeindent">keytool -certreq -keystore &lt;keystore file name&gt; [-storepass &lt;storepass&gt;]
             [-alias &lt;alias&gt;] [-file &lt;request file&gt;] [-keypass &lt;keypass&gt;]</pre>
<p>
Items in brackets are optional - <span class="code">keytool</span> will automatically prompt you
for any items you don't specify on the command line.
</p>
<p>
Once you have generated your certificate request, send it off to your certificate authority.
When they return the certificate to you, you then import it into your keystore using the
command:
</p>
<pre class="codeindent">keytool -import -keystore &lt;keystore file name&gt; [-storepass &lt;storepass&gt;]
             [-alias &lt;alias&gt;] [-file &lt;certificate file&gt;] [-keypass &lt;keypass&gt;]</pre>
<p>
This replaces the "self signed" certificate generated by <span class="code">keytool</span> with the
propertly signed certificate from the certificate authority.  At this point you are ready to
use the information in the keystore file to sign your MIDlet suite.
</p>
<h3 id="sign">Signing the MIDlet suite as the final step in deploying it</h3>
<p>
If you were not using MTJ, you would probably use <span class="code">JADTool</span> to
sign your suite.  JADTool is a signing tool provided by Sun
and many other vendors as part of its Wireless Toolkits.
</p>
<p>
Signing using JADTool is a two-step process: first you add the JAR signature to the JAD file
using the command
</p>
<pre class="codeindent">java -jar JADTool.jar -addjarsig -keypass &lt;password&gt; -alias &lt;key alias&gt;
        -storepass &lt;password&gt; -keystore &lt;keystore&gt; -inputjad &lt;filename&gt; 
         [-jarfile &lt;filename&gt;] [-outputjad &lt;filename&gt;]</pre>
<p>
This computes a checksum on the JAR file, signs it with your private key, and encodes the result into
the JAD file.  The second step is to put your public key certificate into the JAD file, using the
command
</p>
<pre class="codeindent">java -jar JADTool.jar -addcert -keypass &lt;password&gt; -alias &lt;key alias&gt;
        -storepass &lt;password&gt; -keystore &lt;keystore&gt; -inputjad &lt;filename&gt;
         [-jarfile &lt;filename&gt;] [-outputjad &lt;filename&gt;]</pre>
<p>
This extracts the certificate from the keystore, encodes it, and places it in the JAD file.  At this
point, your MIDlet suite is fully signed.  If you inspected the JAD file after signing, you would see
that it contains two lines that look something like this:
</p>
<pre class="codeindent">
MIDlet-Jar-RSA-SHA1: VPHnTUqz5+R6G29HTtEfIC4DOpXzpCa9U3zBqe0kGhOitgh2wwdcK4jcQnfj<br>
STD9kPRfheWiIwC8xeCs08wdlE9xY/v8veYic0cj6GtSm03EgL5Mc+KRSNfitVIL7xa5LWY7yTCi7IkiI<br>
DqC+dP8KQjoCReGsU0YoPM9iq6b6dM=<br>
<br>
MIDlet-Certificate-1-1: MIICUjCCAbsCBEGjQ2AwDQYJKoZIhvcNAQEEBQAwcDELMAkGA1UEBhMCV<br>
VMxCzAJBgNVBAgTAkZMMRMwEQYDVQQHEwpGb3J0IE15ZXJzMRIwEAYDVQQKEwlFY2xpcHNlTUUxFDASBg<br>
NVBAsTC0RldmVsb3BtZW50MRUwEwYDVQQDEwxLZXZpbiBIdW50ZXIwHhcNMDQxMTIzMTQwNDE2WhcNMDU<br>
wMjIxMTQwNDE2WjBwMQswCQYDVQQGEwJVUzELMAkGA1UECBMCRkwxEzARBgNVBAcTCkZvcnQgTXllcnMx<br>
EjAQBgNVBAoTCUVjbGlwc2VNRTEUMBIGA1UECxMLRGV2ZWxvcG1lbnQxFTATBgNVBAMTDEtldmluIEh2b<br>
nRlcjCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEA1uL1PGDcD/u1Ki12gZntby2lJmizbzDpbLSV30<br>
fs5BRO21dC2XY99X7igoc0+O2Ic5qSzY7x2r9rQf0c+d8hA9T3w4tjQBNizEdkvxFNcFFNrAo5MdzWvcU<br>
PiCODqthy1O9LrCAomp/+2d5N+TGOgtUXocgDPouaT/rfKQujI5sCAwEAATANBgkqhkiG9w0BAQQFAAOB<br>
gQBIQ4ECwjj3spYOhxsih55F7e0Prx+evi6VYBaWoZTGKjevnW3IkKTijytdoMpBX3r7oBjAIibFlSEqU<br>
JfJRMszq/L9JOnIcEKRIvkW8yN/Ls8pWB5VEir2EUh3kiIgk2zo7uhbjs58b5bOjWhTZMhQjPI22I23Tq<br>
a/HKXNBsTL3A==
</pre>
<p>
The first line is the encrypted JAR file checksum, while the second is the certificate containing your
public key.  If your certificate authority gave you back a certificate with an intermediate certificate
(a multi-part certificate chain), there might be a <span class="code">MIDlet-Certificate-1-2</span>
item as well.
</p>
<p>
Having signed your MIDlet suite, your mobile device can verify the MIDlet suite by:
</p>
<ol>
<li>Repeating the checksum generation process on the JAR file.</li>
<li>Verifying the certificate(s) in the JAD file can be traced back to a certificate authority
that your device trusts.</li>
<li>Extracting your public key from the certificate.</li>
<li>Extracting the encrypted JAR checksum you put in the JAD file from the JAD file.</li>
<li>Decrypting the checksum using your public key.</li>
<li>Comparing the decrypted checksum with the one it computed in Step 1.</li>
</ol>
<p>
If the two items match in the last step, the mobile device can be confident that
</p>
<ul>
<li>The MIDlet suite had actually been signed by you.  If somebody else signed it, the
decryption process would not have succeeded, since your public key would not match the
other person's private key.</li>
<li>The MIDlet suite had not been modified since you signed it.  If it had, the checksum
would have come out differently.</li>
</ul>
<p>
Finally, many devices will then check to see that the set of permissions requested in the
JAR file's <span class="code">MANIFEST.MF</span> file match the permissions requested
in the JAD file.
</p>

<h2 id="MTJSigning">Signing using MTJ</h2>
<p>
As you can see from the above, using JADTool with MTJ would require you
to deploy your MIDlet in a two-step process.  First, you would need to  build
the MIDlet suite using MTJ, then you would have to use JADTool manually
in order to add the signature information into the JAD file.  You would have
to repeat this process every time you made a change to your MIDlet suite.
Obviously, it could get a bit tedious to do this every time you changed a
line of code and then wanted to fire up the emulator.
</p>
<p>
MTJ can now perform the functions that
would have required JADTool in the past.  To do this, you need to do
the following:
</p>
<ol>
<li>
Add the <strong>MIDlet-Permissions</strong> and
<strong>MIDlet-Permissions-Opt</strong> items you require
to the JAD file using the <a href="jadeditor.html#optional">
Optional page on the JAD Editor</a>.
As part of the deployment operation, MTJ will automatically copy
these to the <span class="code">MANIFEST.MF</span> file inside your JAR.
</li>
<li>
On the
<a href="../reference/property_pages/sign_properties.html">MIDlet Suite Signing page</a>, enable
sign project, and specify the keystore and key alias to be used.
</li>
</ol>
<p>
Once this is done, MTJ will then handle both of the steps for which JADTool would have 
been used.  As a result, your MIDlet suite will be automatically signed
as part of every deployment operation.
</p>
<p>
When setting up signing, it is best to use the <strong>Verify Settings</strong>
button.  This will ensure that the path to the keystore, the key alias, and the
keystore and key passwords are all correct.  In addition, since keystores can
manage multiple different kinds of keys, this operation verifies that the
key identified by the alias is of the proper type (RSA) to be used for
MIDlet suite signing.
</p>
<p>
Note that MTJ is not capable, at present, of helping you to actually
obtain the key/certificate pair required to sign your MIDlet suite.  For the moment,
you will still need to go through the steps described above to generate a key
and arrange for a certificate to be issued for it.  Once you have the proper
information in your keystore, however, MTJ can take it from there.
</p>
<h2 id="passwords">Password Management</h2>
<p>
One thing that we need to point out is that MTJ needs access to the
keystore password and key password in order to sign the MIDlet suite.
We have provided three methods that you can use for managing these passwords:
</p>
<ul>
<li>You can opt for MTJ to store these passwords for you as part of
the MIDlet suite project.  If you opt for this, you type the passwords in
as part of the Project Properties.  MTJ adds them to the
<span class="code">.mtj</span> file that it adds to the project.
In order to help protect the passwords, they are stored in an encrypted form.
Thus, casual inspection of the <span class="code">.mtj</span> file will
not allow somebody else to deduce the keys.  This is not completely foolproof,
however, since someone with access to the MTJ source code could still figure
out how the keys are stored.  Still, in many environments, the provided
security is sufficient, and has the convenience of "set it and forget it,"
even if the project is being shared by multiple developers.
</li>
<li>
A slightly more secure option is to have MTJ store the passwords in
the workspace keyring file.
By default, this file is located in<br>
<span class="directory">[workspace>]\.metadata\.plugins\org.eclipse.pde.core\eASEE\org.eclipse.core.runtime\.keyring</span>,<br>
although you can specify a different file using Eclipse's
<span class="code">-keyring</span> command line argument.
If you share your project using CVS, this file is <strong>not</strong> shared.
Eclipse automatically encrypts the contents of the keyring file.  (This is where Eclipse stores
your CVS passwords.)  You can increase the security of this option by using the
<span class="code">-keyring</span> and <span class="code">-password</span>
Eclipse command line options to store the keyring in a non-default location and
to encrypt it using a non-default password.
</li>
<li>
The most secure option is not to have MTJ or Eclipse store your
passwords at all, but to prompt you for them as required.
With this setting, MTJ will present a dialog box when it
needs the passwords.  To save you some aggravation, MTJ will then
store the passwords in RAM for the remainder of the session, so that you
don't have to type them in again and again.  Once you 
close Eclipse, or change workspaces, the passwords are then "forgotten."
With this setting, the passwords are never stored to your hard disk.
</li>
</ul>
<h2 id="emulation">Emulation</h2>
<p>
Different emulators behave differently with regard to signed MIDlets.
Many emulators allow you to specify a particular security domain under
which a MIDlet is to be emulated.  
For emulators that support this, there
is an additional item on the 
<a href="../reference/launchers/wireless_toolkit/launcher_emulation.html">Launch Configuration Settings</a>
that allows you to specify the security domain.
</p>
<p>
Note: If you are signing your MIDlet with a certificate obtained from a certificate
authority (or one you have created yourself for testing purposes), it may be
necessary for you to import that certificate into your emulator for your
signed MIDlet to work properly.  This is particularly the case for Over-The-Air
(OTA) emulation.  Alternately, it may be necessary for you to sign your MIDlet
with an emulator-supported certificate.
Consult your emulator's documentation for more information.
</p>
<h2 id="refs">References</h2>
<p>
The following are some (hopefully) useful references to the signing process and the
properties of signed MIDlets:
</p>
<ul>
<li>
<a href="http://jcp.org/aboutJava/communityprocess/final/jsr118/index.html" target="_blank"><strong>JSR-118</strong></a><br>
This is the MIDP 2.0 specification.  It contains the formal specification of the signing
process and the various security implementations.
</li>
<li>
<a href="http://java.sun.com/j2me/docs/wtk2.1/user_html/Ap_CMU.html#wp22950" target="_blank">Description of JADTool</a><br>
This is from the User's Guide to version 2.1 of Sun's Wireless Toolkit.
</li>
<li>
<a href="http://www.forum.nokia.com/ndsCookieBuilder?fileParamID=5075" target="_blank">Nokia Tutorial on Signed MIDlets</a><br>
This provides an overview of the security model, the signing process, and shows some example code.
</li>
</ul>
</body>
</html>
