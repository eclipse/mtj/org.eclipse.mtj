/**
 * Copyright (c) 2009 Research In Motion Limited.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial implementation.
 */
package org.eclipse.mtj.toolkit.sdkprovidertestclamp2;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.launching.LaunchEnvironment;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.IManagedDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary;
import org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.sdk.device.AbstractMIDPDevice;
import org.eclipse.mtj.internal.core.sdk.device.DeviceClasspath;
import org.eclipse.mtj.internal.core.sdk.device.midp.MIDPAPI;
import org.eclipse.mtj.internal.core.sdk.device.midp.MIDPLibrary;
import org.osgi.framework.Version;

/**
 * Implementation of a fake IManagedDevice for use in an SDK provider test clamp. 
 * 
 * @author jdearden
 */
@SuppressWarnings("restriction")
public class Device extends AbstractMIDPDevice implements IManagedDevice, Cloneable {
    
    private String identifier;
    private boolean isDuplicate;

    public Device() {

    }

    public Device(Sdk sdk, String deviceKey) {
        Properties p = Plugin.getDefault().getProperties();
        identifier = p.getProperty(deviceKey + ".id"); //$NON-NLS-1$
        setName(p.getProperty(deviceKey + ".name")); //$NON-NLS-1$
        setDescription(p.getProperty(deviceKey + ".description")); //$NON-NLS-1$
        setGroupName(sdk.getName());
        setClasspath(buildClasspath());
        // Throw some executable in here to trick MTJ
        setExecutable(new File("c:\\windows\\notepad.exe")); //$NON-NLS-1$
        setBundle(Plugin.getDefault().getBundle().getSymbolicName());
        setPreverifier(null);
        setDeviceProperties(buildDeviceSymbols());
        setProtectionDomains(new String[0]);
        setDebugServer(false);
        ISymbolSet dss = MTJCore.getSymbolSetFactory().createSymbolSetFromDevice(this);
        ISymbolSet pss = MTJCore.getSymbolSetFactory().createSymbolSetFromProperties(buildDeviceSymbols());
        dss.add(pss.getSymbols());
        dss.setName(getName());
        setSymbolSet(dss);
        setSDK(sdk);
        setLaunchCommandTemplate("Fake launch command template");
    }
    
    public String getIdentifier() {
        return identifier;
    }

    // << IMIDPDevice >>
    public File getWorkingDirectory() {
        return null;
    }
    
    private IDeviceClasspath buildClasspath() {
        ArrayList<IMIDPLibrary> libraries = new ArrayList<IMIDPLibrary>();
        libraries.add(buildLibrary1());
        libraries.add(buildLibrary2());
        IDeviceClasspath classpath = new DeviceClasspath();
        for( IMIDPLibrary library : libraries ) {
            classpath.addEntry( library );
        }
        return classpath;
    }
    
    @SuppressWarnings("deprecation")
    private MIDPLibrary buildLibrary1() {
        
        Properties p = Plugin.getDefault().getProperties();
        String cldc11Loc = p.getProperty("library.cldc11"); //$NON-NLS-1$
        File javadocsDirectory = new File("c:\\"); //$NON-NLS-1$
        File libFile = new File(cldc11Loc); //$NON-NLS-1$
        ArrayList<IMIDPAPI> apis = new ArrayList<IMIDPAPI>();

        MIDPAPI api = new MIDPAPI();
        api.setIdentifier("CLDC");
        api.setName("Connected Limited Device Configuration");
        api.setType(MIDPAPIType.CONFIGURATION);
        api.setVersion(new Version("1.1")); //$NON-NLS-1$ 
        apis.add(api);
    
        URL javadocUrl = null;
        try {
            javadocUrl = javadocsDirectory.toURL();
        } catch (MalformedURLException ignore) {
        }
        return new MIDPLibrary(apis, javadocUrl, libFile, null, null,
                new ArrayList<IAccessRule>(0));
    }
    
    @SuppressWarnings("deprecation")
    private MIDPLibrary buildLibrary2() {
        
        Properties p = Plugin.getDefault().getProperties();
        String midp21Loc = p.getProperty("library.midp21"); //$NON-NLS-1$
        File javadocsDirectory = new File("c:\\"); //$NON-NLS-1$
        File libFile = new File(midp21Loc); //$NON-NLS-1$
        ArrayList<IMIDPAPI> apis = new ArrayList<IMIDPAPI>();

        MIDPAPI api = new MIDPAPI();
        api.setIdentifier("MIDP");
        api.setName("Mobile Information Device Profile");
        api.setType(MIDPAPIType.PROFILE);
        api.setVersion(new Version("2.1")); //$NON-NLS-1$
        apis.add(api);
    
        URL javadocUrl = null;
        try {
            javadocUrl = javadocsDirectory.toURL();
        } catch (MalformedURLException ignore) {
        }
        return new MIDPLibrary(apis, javadocUrl, libFile, null, null,
                new ArrayList<IAccessRule>(0));
    }
    
    private Properties buildDeviceSymbols() {
        Properties symbols = new Properties();
        symbols.put("aFakeSymbolSet", "true");
        symbols.put("aFakeVendor", "MTJ Corporation");
        symbols.put("screen.isColor", "true");
        symbols.put("hasTouchScreen", "false");
        return symbols;
    }

    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException {

        return "";
    }
    
    public boolean isDuplicate() {
        return isDuplicate;
    }

    public void setAsDuplicate() {
        isDuplicate = true;
    }
    
    protected Object clone() throws CloneNotSupportedException {
        Device clone = (Device) super.clone();
        clone.identifier = this.identifier;
        clone.isDuplicate = true;
        return clone;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.loadUsing(persistenceProvider);
        identifier = persistenceProvider.loadString("identifier"); //$NON-NLS-1$
        isDuplicate = persistenceProvider.loadBoolean("isDuplicate"); //$NON-NLS-1$
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.storeUsing(persistenceProvider);
        persistenceProvider.storeString("identifier", identifier); //$NON-NLS-1$
        persistenceProvider.storeBoolean("isDuplicate", isDuplicate); //$NON-NLS-1$
    }

}
