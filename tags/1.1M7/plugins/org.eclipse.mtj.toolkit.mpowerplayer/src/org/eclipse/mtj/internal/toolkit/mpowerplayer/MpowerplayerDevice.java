/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Feng Wang (Sybase)       - Modify to fit for AbstractMIDPDevice#copyForLaunch(...)
 *                                signature changing.
 *     Diego Sandin (Motorola)  - Use LaunchTemplateProperties enum instead of 
 *                                hard-coded property strings 
 *     David Marques (Motorola) - Decoupling JAD running from project reference. 
 *     Dan Murphy               - Fixing device comparisson.
 */
package org.eclipse.mtj.internal.toolkit.mpowerplayer;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.launching.LaunchEnvironment;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.launching.ILaunchConstants;
import org.eclipse.mtj.internal.core.sdk.device.JavaEmulatorDevice;
import org.eclipse.mtj.internal.core.sdk.device.LaunchTemplateProperties;
import org.eclipse.mtj.internal.core.util.ReplaceableParametersProcessor;
import org.eclipse.mtj.internal.core.util.Utils;

/**
 * Device implementation for the MPowerPlayer SDK device.
 * 
 * @author Craig Setera
 */
public class MpowerplayerDevice extends JavaEmulatorDevice {

    private static final String MPP_ROOT_PERSISTENCE_KEY = "mppRoot"; //$NON-NLS-1$

    /**
     * MPowerPlayer SDK root folder
     */
    private File mppRoot;

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDevice#getLaunchCommand(org.eclipse.mtj.core.launching.LaunchEnvironment, org.eclipse.core.runtime.IProgressMonitor)
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException {
        
        Map<String, String> executionProperties = new HashMap<String, String>();

		ILaunchConfiguration launchConfiguration = launchEnvironment
				.getLaunchConfiguration();
		boolean launchFromJAD = shouldDirectLaunchJAD(launchConfiguration);

		// Fill in our properties so we can use them for resolution
		// against the raw executable command line
		executionProperties.put(LaunchTemplateProperties.EXECUTABLE.toString(),
				getJavaExecutable().toString());
		executionProperties.put("mpproot", mppRoot.toString()); //$NON-NLS-1$

		// Debug information
		if (launchEnvironment.isDebugLaunch()) {
			executionProperties.put(LaunchTemplateProperties.DEBUGPORT
					.toString(), new Integer(launchEnvironment
					.getDebugListenerPort()).toString());
		}

		// Add launch configuration values
		String extraArguments = launchConfiguration.getAttribute(
				ILaunchConstants.LAUNCH_PARAMS, Utils.EMPTY_STRING);
		executionProperties.put(LaunchTemplateProperties.USERSPECIFIEDARGUMENTS
				.toString(), extraArguments);

		if (launchFromJAD) {
			executionProperties.put(
					LaunchTemplateProperties.JADFILE.toString(),
					getSpecifiedJadURL(launchConfiguration));
		} else {
			if (launchEnvironment.getProject() instanceof IMidletSuiteProject) {
				IMidletSuiteProject midletSuite = (IMidletSuiteProject) launchEnvironment
						.getProject();
				File tempDeployed = copyForLaunch(midletSuite, monitor,
						launchFromJAD);

				File jadFile = getJadForLaunch(midletSuite, tempDeployed,
						monitor);
				if (jadFile.exists()) {
					executionProperties.put(LaunchTemplateProperties.JADFILE
							.toString(), jadFile.toString());
				}
			}
		}
		// Do the property resolution given the previous information
		return ReplaceableParametersProcessor.processReplaceableValues(
				launchCommandTemplate, executionProperties);
	}

    /**
     * Get the MPowerPlayer SDK root folder.
     * 
     * @return Returns the MPowerPlayer SDK root folder.
     */
    public File getMppRoot() {
        return mppRoot;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.AbstractMIDPDevice#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    @Override
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.loadUsing(persistenceProvider);

        String rootString = persistenceProvider
                .loadString(MPP_ROOT_PERSISTENCE_KEY);
        mppRoot = new File(rootString);
    }

    /**
     * @param mppRoot The mppRoot to set.
     */
    public void setMppRoot(File mppRoot) {
        this.mppRoot = mppRoot;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.AbstractMIDPDevice#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    @Override
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.storeUsing(persistenceProvider);
        persistenceProvider.storeString(MPP_ROOT_PERSISTENCE_KEY, mppRoot
                .toString());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice#getWorkingDirectory()
     */
    public File getWorkingDirectory() {
        return null;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		boolean areEqual = false;
		if (obj instanceof MpowerplayerDevice) {
			areEqual = super.equals((MpowerplayerDevice) obj);
		}
		return areEqual;
	}
}
