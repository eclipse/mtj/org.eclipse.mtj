/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards, fixed some invocations to 
 *                                deprecated methods
 *     Jon Dearden (Research In Motion) - Replaced deprecated use of Preferences 
 *                                        [Bug 285699]
 */
package org.eclipse.mtj.internal.ui.util;

import java.io.IOException;

import org.eclipse.core.internal.preferences.EclipsePreferences;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.IPreferenceChangeListener;
import org.eclipse.core.runtime.preferences.IEclipsePreferences.PreferenceChangeEvent;
import org.eclipse.jface.preference.IPersistentPreferenceStore;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.SafeRunnable;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.osgi.service.prefs.BackingStoreException;

public class PluginPreferenceStore implements IPersistentPreferenceStore {

    /** The owner of the preferences being wrapped */
    private String ownerPlugingId;

    /**
     * The underlying core runtime preference store.
     */
    private IEclipsePreferences instancePreferences;
    private IEclipsePreferences defaultPreferences;

    /**
     * Identity list of old listeners (element type:
     * <code>org.eclipse.jface.util.IPropertyChangeListener</code>).
     */
    private ListenerList listeners = new ListenerList();

    /**
     * Indicates whether property change events should be suppressed (used in implementation of
     * <code>putValue</code>). Initially and usually <code>false</code>.
     * 
     * @see IPreferenceStore#putValue
     */
    private boolean silentRunning = false;

    /**
     * Creates a new instance for the this plug-in.
     */
    public PluginPreferenceStore(String pluginId) {
        ownerPlugingId = pluginId;
        instancePreferences = new InstanceScope().getNode(ownerPlugingId);
        defaultPreferences = new DefaultScope().getNode(ownerPlugingId);

        // register listener that funnels everything to firePropertyChangeEvent
        instancePreferences.addPreferenceChangeListener(new IPreferenceChangeListener() {
            public void preferenceChange(PreferenceChangeEvent event) {
                if (!silentRunning) {
                    firePropertyChangeEvent(event.getKey(), event.getOldValue(), event
                            .getNewValue());
                }
            }
        });
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void addPropertyChangeListener(final IPropertyChangeListener listener) {
        listeners.add(listener);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void removePropertyChangeListener(IPropertyChangeListener listener) {
        listeners.remove(listener);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void firePropertyChangeEvent(String name, Object oldValue, Object newValue) {

        // efficiently handle case of 0 listeners
        if (listeners.isEmpty()) {
            // no one interested
            return;
        }

        // important: create intermediate array to protect against listeners
        // being added/removed during the notification
        final Object[] list = listeners.getListeners();
        final org.eclipse.jface.util.PropertyChangeEvent event = new org.eclipse.jface.util.PropertyChangeEvent(
                this, name, oldValue, newValue);
        for (int i = 0; i < list.length; i++) {
            final IPropertyChangeListener listener = (IPropertyChangeListener) list[i];
            SafeRunner.run(new SafeRunnable("Preference Change Error") {
                public void run() {
                    listener.propertyChange(event);
                }
            });
        }
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public boolean contains(String name) {
        return containsKey(instancePreferences, name);
    }

    private boolean containsKey(IEclipsePreferences prefs, String key) {
        if (key == null)
            return false;
        String[] keys;
        try {
            keys = prefs.keys();
        } catch (BackingStoreException e) {
            e.printStackTrace();
            return false;
        }
        for (String str : keys) {
            if (key.equals(str))
                return true;
        }
        return false;
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public boolean getBoolean(String name) {
        if (containsKey(instancePreferences, name))
            return instancePreferences.getBoolean(name, false);
        return getDefaultBoolean(name);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public boolean getDefaultBoolean(String name) {
        return defaultPreferences.getBoolean(name, false);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public double getDefaultDouble(String name) {
        return defaultPreferences.getDouble(name, 0.0D);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public float getDefaultFloat(String name) {
        return defaultPreferences.getFloat(name, 0.0F);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public int getDefaultInt(String name) {
        return defaultPreferences.getInt(name, 0);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public long getDefaultLong(String name) {
        return defaultPreferences.getLong(name, 0L);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public String getDefaultString(String name) {
        return defaultPreferences.get(name, IMTJUIConstants.EMPTY_STRING);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public double getDouble(String name) {
        if (containsKey(instancePreferences, name))
            return instancePreferences.getDouble(name, 0D);
        return getDefaultDouble(name);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public float getFloat(String name) {
        if (containsKey(instancePreferences, name))
            return instancePreferences.getFloat(name, 0F);
        return getDefaultFloat(name);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public int getInt(String name) {
        if (containsKey(instancePreferences, name))
            return instancePreferences.getInt(name, 0);
        return getDefaultInt(name);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public long getLong(String name) {
        if (containsKey(instancePreferences, name))
            return instancePreferences.getLong(name, 0L);
        return getDefaultLong(name);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public String getString(String name) {
        if (containsKey(instancePreferences, name))
            return instancePreferences.get(name, IMTJUIConstants.EMPTY_STRING);
        return getDefaultString(name);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public boolean isDefault(String name) {
        return !containsKey(instancePreferences, name);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public boolean needsSaving() {
        if (instancePreferences instanceof EclipsePreferences)
            return ((EclipsePreferences) instancePreferences).isDirty();
        return false;
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void putValue(String name, String value) {
        try {
            // temporarily suppress event notification while setting value
            silentRunning = true;
            instancePreferences.put(name, value);
        } finally {
            silentRunning = false;
        }
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setDefault(String name, double value) {
        defaultPreferences.putDouble(name, value);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setDefault(String name, float value) {
        defaultPreferences.putFloat(name, value);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setDefault(String name, int value) {
        defaultPreferences.putInt(name, value);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setDefault(String name, long value) {
        defaultPreferences.putLong(name, value);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setDefault(String name, String value) {
        defaultPreferences.put(name, value);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setDefault(String name, boolean value) {
        defaultPreferences.putBoolean(name, value);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setToDefault(String name) {
        instancePreferences.remove(name);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setValue(String name, double value) {
        instancePreferences.putDouble(name, value);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setValue(String name, float value) {
        instancePreferences.putFloat(name, value);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setValue(String name, int value) {
        instancePreferences.putInt(name, value);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setValue(String name, long value) {
        instancePreferences.putLong(name, value);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setValue(String name, String value) {
        instancePreferences.put(name, value);
    }

    /*
     * (non-javadoc) Method declared on IPreferenceStore
     */
    public void setValue(String name, boolean value) {
        instancePreferences.putBoolean(name, value);
    }

    /**
     * @see org.eclipse.jface.preference.IPersistentPreferenceStore#save()
     */
    public void save() throws IOException {
        try {
            instancePreferences.flush();
        } catch (BackingStoreException e) {
            IOException ioe = new IOException(e.getMessage());
            ioe.setStackTrace(e.getStackTrace());
            ioe.initCause(e.getCause());
            throw ioe;
        }
    }
}
