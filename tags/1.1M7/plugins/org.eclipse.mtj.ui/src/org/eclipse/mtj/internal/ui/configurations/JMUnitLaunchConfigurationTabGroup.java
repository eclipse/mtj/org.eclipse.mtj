/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Aragao (Motorola) - Initial version
 */

package org.eclipse.mtj.internal.ui.configurations;

import org.eclipse.debug.ui.AbstractLaunchConfigurationTabGroup;
import org.eclipse.debug.ui.CommonTab;
import org.eclipse.debug.ui.ILaunchConfigurationDialog;
import org.eclipse.debug.ui.ILaunchConfigurationTab;
import org.eclipse.debug.ui.sourcelookup.SourceLookupTab;
import org.eclipse.mtj.internal.ui.launching.DeviceTab;
import org.eclipse.mtj.internal.ui.launching.JMUnitTab;

/**
 * Tab group implementation for the launch of the JMUnit Test Cases
 * and Test Suites
 * 
 * @author David Aragao
 */
public class JMUnitLaunchConfigurationTabGroup extends
		AbstractLaunchConfigurationTabGroup {

	/**
     * @see org.eclipse.debug.ui.ILaunchConfigurationTabGroup#createTabs(org.eclipse.debug.ui.ILaunchConfigurationDialog,
     *      java.lang.String)
     */
    public void createTabs(ILaunchConfigurationDialog dialog, String mode) {
        ILaunchConfigurationTab[] tabs = new ILaunchConfigurationTab[] {
                new JMUnitTab(), new DeviceTab(), new SourceLookupTab(),
                new CommonTab() };

        for (int i = 0; i < tabs.length; i++) {
            tabs[i].setLaunchConfigurationDialog(dialog);
        }

        setTabs(tabs);
    }

}
