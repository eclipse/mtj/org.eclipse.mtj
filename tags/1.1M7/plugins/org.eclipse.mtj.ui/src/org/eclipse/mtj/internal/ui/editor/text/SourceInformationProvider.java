/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.text;

import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.information.IInformationProvider;
import org.eclipse.jface.text.information.IInformationProviderExtension2;
import org.eclipse.mtj.internal.ui.editor.MTJSourcePage;
import org.eclipse.mtj.internal.ui.editors.l10n.L10nXMLTextHover;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;

/**
 * Provides information related to the content of the text viewer.
 * 
 * @since 0.9.1
 */
public class SourceInformationProvider implements IInformationProvider,
        IInformationProviderExtension2, IPartListener {

    public static final int NO_IMP = 0;
    public static final int F_XML_IMP = 1;

    private IInformationControlCreator fPresenterControlCreator;

    protected String fCurrentPerspective;
    protected ITextHover fImplementation;
    protected int fImpType;
    protected MTJSourcePage fSourcePage;

    /**
     * Creates a new SourceInformationProvider.
     * 
     * @param editor
     * @param creator
     * @param impType
     */
    public SourceInformationProvider(MTJSourcePage editor,
            IInformationControlCreator creator, int impType) {
        fSourcePage = editor;
        fPresenterControlCreator = creator;
        fImpType = impType;
        if ((fSourcePage != null) && (fImpType != NO_IMP)) {
            fSourcePage.getSite().getWorkbenchWindow().getPartService()
                    .addPartListener(this);
            update();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.information.IInformationProvider#getInformation(org.eclipse.jface.text.ITextViewer, org.eclipse.jface.text.IRegion)
     */
    public String getInformation(ITextViewer textViewer, IRegion subject) {
        if (fImplementation != null) {
            String s = fImplementation.getHoverInfo(textViewer, subject);
            if ((s != null) && (s.trim().length() > 0)) {
                return s;
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.information.IInformationProviderExtension2#getInformationPresenterControlCreator()
     */
    public IInformationControlCreator getInformationPresenterControlCreator() {
        return fPresenterControlCreator;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.information.IInformationProvider#getSubject(org.eclipse.jface.text.ITextViewer, int)
     */
    public IRegion getSubject(ITextViewer textViewer, int offset) {
        if (textViewer != null) {
            return MTJWordFinder.findWord(textViewer.getDocument(), offset);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IPartListener#partActivated(org.eclipse.ui.IWorkbenchPart)
     */
    public void partActivated(IWorkbenchPart part) {
        update();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IPartListener#partBroughtToTop(org.eclipse.ui.IWorkbenchPart)
     */
    public void partBroughtToTop(IWorkbenchPart part) {
        update();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IPartListener#partClosed(org.eclipse.ui.IWorkbenchPart)
     */
    public void partClosed(IWorkbenchPart part) {
        if ((fSourcePage != null) && (part == fSourcePage.getEditor())
                && (fImpType != NO_IMP)) {
            fSourcePage.getSite().getWorkbenchWindow().getPartService()
                    .removePartListener(this);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IPartListener#partDeactivated(org.eclipse.ui.IWorkbenchPart)
     */
    public void partDeactivated(IWorkbenchPart part) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IPartListener#partOpened(org.eclipse.ui.IWorkbenchPart)
     */
    public void partOpened(IWorkbenchPart part) {
    }

    /**
     * FIXME
     */
    protected void update() {

        IWorkbenchPage page = fSourcePage.getSite().getWorkbenchWindow()
                .getActivePage();

        if (page != null) {

            IPerspectiveDescriptor perspective = page.getPerspective();

            if (perspective != null) {

                String perspectiveId = perspective.getId();

                if ((fCurrentPerspective == null)
                        || (fCurrentPerspective != perspectiveId)) {
                    fCurrentPerspective = perspectiveId;

                    switch (fImpType) {
                        case F_XML_IMP:
                            fImplementation = new L10nXMLTextHover(fSourcePage);
                            break;
                    }
                }
            }
        }
    }

}
