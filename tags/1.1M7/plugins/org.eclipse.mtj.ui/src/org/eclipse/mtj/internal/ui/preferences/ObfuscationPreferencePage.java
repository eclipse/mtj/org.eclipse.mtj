/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     David Marques(Motorola)  - Adding ProguardDirectoryFieldEditor.
 *     David Aragao (Motorola)  - Move proguard root directory field editor to JavaME Preference page
 */
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.MTJCorePreferenceInitializer;
import org.eclipse.mtj.internal.ui.IEmbeddableWorkbenchPreferencePage;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

/**
 * Preference page implementation for setting Java ME obfuscation preferences.
 * 
 * @author Craig Setera
 */
public class ObfuscationPreferencePage extends FieldEditorPreferencePage
        implements IEmbeddableWorkbenchPreferencePage {

    public static final String ID = "org.eclipse.mtj.ui.preferences.ObfuscationPreferencePage"; //$NON-NLS-1$

    private boolean embeddedInProperties;

    // Widgets
    private Button specifiedArgumentsButton;
    private Text specifiedArgumentsText;

    /**
     * Default constructor.
     */
    public ObfuscationPreferencePage() {
        this(false, MTJUIPlugin.getDefault().getCorePreferenceStore());
    }

    /**
     * Constructor for use when embedding the preference page within a
     * properties page.
     * 
     * @param embeddedInProperties
     * @param preferenceStore
     */
    public ObfuscationPreferencePage(boolean embeddedInProperties,
            IPreferenceStore preferenceStore) {
        super(GRID);

        this.embeddedInProperties = embeddedInProperties;
        setPreferenceStore(preferenceStore);

        if (!embeddedInProperties) {
            setDescription(MTJUIMessages.ObfuscationPreferencePage_description);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
     */
    @Override
    public void createFieldEditors() {
        final Composite parent = getFieldEditorParent();

        Font font = parent.getFont();
        specifiedArgumentsButton = new Button(parent, SWT.CHECK);
        specifiedArgumentsButton
                .setText(MTJUIMessages.ObfuscationPreferencePage_specified_arguments);
        specifiedArgumentsButton.setFont(font);

        specifiedArgumentsText = new Text(parent, SWT.SINGLE | SWT.BORDER);
        specifiedArgumentsText.setFont(font);
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 2;
        specifiedArgumentsText.setLayoutData(gd);
        specifiedArgumentsText.setEnabled(false);
        specifiedArgumentsButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                specifiedArgumentsText.setEnabled(specifiedArgumentsButton
                        .getSelection());
            }
        });

        addField(new MultiValuedTableFieldEditor(
                IMTJCoreConstants.PREF_PROGUARD_KEEP,
                MTJUIMessages.ObfuscationPreferencePage_proguard_keep_expressions,
                parent));

        addProguardNotes(parent, font);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performApply()
     */
    @Override
    public void performApply() {
        super.performApply();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performDefaults()
     */
    @Override
    public void performDefaults() {
        super.performDefaults();

        setState(MTJCorePreferenceInitializer.PREF_DEF_PROGUARD_USE_SPECIFIED,
                MTJCorePreferenceInitializer.PREF_DEF_PROGUARD_OPTIONS);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        IPreferenceStore store = getPreferenceStore();

        boolean useSpecified = specifiedArgumentsButton.getSelection();
        store.setValue(IMTJCoreConstants.PREF_PROGUARD_USE_SPECIFIED,
                useSpecified);
        store.setValue(IMTJCoreConstants.PREF_PROGUARD_OPTIONS,
                specifiedArgumentsText.getText());

        return super.performOk();
    }

    /**
     * Add the notes label for Proguard.
     * 
     * @param parent
     * @param font
     */
    private void addProguardNotes(final Composite parent, Font font) {
        GridData gd;
        Label notes = new Label(parent, SWT.NONE);
        notes.setFont(font);
        notes.setText(MTJUIMessages.ObfuscationPreferencePage_proguard_note);

        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 3;
        notes.setLayoutData(gd);
    }

    /**
     * Set the current state of the widgets.
     * 
     * @param useSpecified
     * @param specified
     */
    private void setState(boolean useSpecified, String specified) {
        specifiedArgumentsButton.setSelection(useSpecified);
        specifiedArgumentsText.setText(specified);
        specifiedArgumentsText.setEnabled(useSpecified);
    }

    /**
     * Overridden so that we get the help context where it belongs so that it
     * works when the focus is in the left pane.
     * 
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        if (embeddedInProperties) {
            noDefaultAndApplyButton();
        }

        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_ObfuscationPreferencePage"); //$NON-NLS-1$
        return (super.createContents(parent));
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#initialize()
     */
    @Override
    protected void initialize() {
        super.initialize();

        IPreferenceStore store = getPreferenceStore();
        setState(store
                .getBoolean(IMTJCoreConstants.PREF_PROGUARD_USE_SPECIFIED),
                store.getString(IMTJCoreConstants.PREF_PROGUARD_OPTIONS));
    }
}
