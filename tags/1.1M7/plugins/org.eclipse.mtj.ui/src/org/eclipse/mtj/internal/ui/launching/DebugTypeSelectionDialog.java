/**
 * Copyright (c) 2006, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation          - initial API and implementation
 *     Feng Wang (Sybase)       - Adapted from from org.eclipse.jdt.ui/DebugTypeSelectionDialog
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.internal.ui.launching;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.FilteredItemsSelectionDialog;
import org.eclipse.ui.model.IWorkbenchAdapter;

/**
 * This is a specialization of <code>FilteredItemsSelectionDialog</code> used to
 * present users with a listing of <code>IType</code>s that contain main methods
 * <p>
 * This class is used by {@link JavaLaunchShortcut}. In future when MTJ do not
 * need to support Eclipse 3.3, {@link JavaLaunchShortcut} will no longer useful
 * and will be deleted. This class will be deleted too.
 * </p>
 */
public class DebugTypeSelectionDialog extends FilteredItemsSelectionDialog {

    /**
     * Main list label provider
     */
    public class DebugTypeLabelProvider implements ILabelProvider {

        HashMap<ImageDescriptor, Image> fImageMap = new HashMap<ImageDescriptor, Image>();

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
         */
        public void addListener(ILabelProviderListener listener) {
        }


        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
         */
        public void dispose() {
            fImageMap.clear();
            fImageMap = null;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ILabelProvider#getImage(java.lang.Object)
         */
        public Image getImage(Object element) {
            if (element instanceof IAdaptable) {
                IWorkbenchAdapter adapter = (IWorkbenchAdapter) ((IAdaptable) element)
                        .getAdapter(IWorkbenchAdapter.class);
                if (adapter != null) {
                    ImageDescriptor descriptor = adapter
                            .getImageDescriptor(element);
                    Image image = fImageMap.get(descriptor);
                    if (image == null) {
                        image = descriptor.createImage();
                        fImageMap.put(descriptor, image);
                    }
                    return image;
                }
            }
            return null;
        }


        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ILabelProvider#getText(java.lang.Object)
         */
        public String getText(Object element) {
            if (element instanceof IType) {
                IType type = (IType) element;
                String label = type.getElementName();
                String container = getDeclaringContainerName(type);
                if ((container != null) && !"".equals(container)) { //$NON-NLS-1$
                    label += " - " + container; //$NON-NLS-1$
                }
                return label;
            }
            return null;
        }


        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
         */
        public boolean isLabelProperty(Object element, String property) {
            return false;
        }


        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
         */
        public void removeListener(ILabelProviderListener listener) {
        }

        /**
         * Returns the narrowest enclosing <code>IJavaElement</code> which is
         * either an <code>IType</code> (enclosing) or an
         * <code>IPackageFragment</code> (contained in)
         * 
         * @param type the type to find the enclosing <code>IJavaElement</code>
         *            for.
         * @return the enclosing element or <code>null</code> if none
         */
        protected IJavaElement getDeclaringContainer(IType type) {
            IJavaElement outer = type.getDeclaringType();
            if (outer == null) {
                outer = type.getPackageFragment();
            }
            return outer;
        }

        /**
         * Returns the name of the declaring container name
         * 
         * @param type the type to find the container name for
         * @return the container name for the specified type
         */
        protected String getDeclaringContainerName(IType type) {
            IType outer = type.getDeclaringType();
            if (outer != null) {
                return outer.getFullyQualifiedName('.');
            } else {
                String name = type.getPackageFragment().getElementName();
                if ("".equals(name)) { //$NON-NLS-1$
                    name = MTJUIMessages.MainMethodLabelProvider_0;
                }
                return name;
            }
        }
    }

    /**
     * Provides a label and image for the details area of the dialog
     */
    class DebugTypeDetailsLabelProvider extends DebugTypeLabelProvider {


        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.launching.DebugTypeSelectionDialog.DebugTypeLabelProvider#getImage(java.lang.Object)
         */
        @Override
        public Image getImage(Object element) {
            if (element instanceof IType) {
                return super.getImage(getDeclaringContainer(((IType) element)));
            }
            return super.getImage(element);
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.launching.DebugTypeSelectionDialog.DebugTypeLabelProvider#getText(java.lang.Object)
         */
        @Override
        public String getText(Object element) {
            if (element instanceof IType) {
                IType type = (IType) element;
                String name = getDeclaringContainerName(type);
                if (name != null) {
                    if (name.equals(MTJUIMessages.MainMethodLabelProvider_0)) {
                        IJavaProject project = type.getJavaProject();
                        if (project != null) {
                            try {
                                return project.getOutputLocation().toOSString()
                                        .substring(1)
                                        + " - " + name; //$NON-NLS-1$
                            } catch (JavaModelException e) {
                                MTJLogger.log(IStatus.ERROR, e);
                            }
                        }
                    } else {
                        return name;
                    }
                }
            }
            return null;
        }
    }

    /**
     * Simple items filter
     */
    class DebugTypeItemsFilter extends ItemsFilter {


        /* (non-Javadoc)
         * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog.ItemsFilter#isConsistentItem(java.lang.Object)
         */
        @Override
        public boolean isConsistentItem(Object item) {
            return item instanceof IType;
        }

        /* (non-Javadoc)
         * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog.ItemsFilter#matchItem(java.lang.Object)
         */
        @Override
        public boolean matchItem(Object item) {
            if (!(item instanceof IType)
                    || !Arrays.asList(fTypes).contains(item)) {
                return false;
            }
            return matches(((IType) item).getElementName());
        }
    }

    /**
     * The selection history for the dialog
     */
    class DebugTypeSelectionHistory extends SelectionHistory {


        /* (non-Javadoc)
         * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog.SelectionHistory#restoreItemFromMemento(org.eclipse.ui.IMemento)
         */
        @Override
        protected Object restoreItemFromMemento(IMemento memento) {
            IJavaElement element = JavaCore.create(memento.getTextData());
            return element instanceof IType ? element : null;
        }
        
        /* (non-Javadoc)
         * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog.SelectionHistory#storeItemToMemento(java.lang.Object, org.eclipse.ui.IMemento)
         */
        @Override
        protected void storeItemToMemento(Object item, IMemento memento) {
            if (item instanceof IType) {
                memento.putTextData(((IType) item).getHandleIdentifier());
            }
        }
    }

    private static final String SELECT_MAIN_METHOD_DIALOG = "org.eclipse.jdt.debug.ui.select_main_method_dialog";

    private static final String SETTINGS_ID = IMTJUIConstants.PLUGIN_ID
            + ".MIDLET_SELECTION_DIALOG"; //$NON-NLS-1$

    private IType[] fTypes = null;

    /**
     * Constructor
     * 
     * @param elements the types to display in the dialog
     */
    public DebugTypeSelectionDialog(Shell shell, IType[] elements, String title) {
        super(shell, false);
        setTitle(title);
        fTypes = elements;
        setMessage(MTJUIMessages.JavaMainTab_Choose_a_main__type_to_launch__12);
        setInitialPattern("**"); //$NON-NLS-1$
        setListLabelProvider(new DebugTypeLabelProvider());
        setDetailsLabelProvider(new DebugTypeDetailsLabelProvider());
        setSelectionHistory(new DebugTypeSelectionHistory());
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog#getElementName(java.lang.Object)
     */
    @Override
    public String getElementName(Object item) {
        if (item instanceof IType) {
            return ((IType) item).getElementName();
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        Control ctrl = super.createDialogArea(parent);
        PlatformUI.getWorkbench().getHelpSystem().setHelp(ctrl,
                SELECT_MAIN_METHOD_DIALOG);
        return ctrl;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog#createExtendedContentArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createExtendedContentArea(Composite parent) {
        return null;
    }


    /* (non-Javadoc)
     * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog#createFilter()
     */
    @Override
    protected ItemsFilter createFilter() {
        return new DebugTypeItemsFilter();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog#fillContentProvider(org.eclipse.ui.dialogs.FilteredItemsSelectionDialog.AbstractContentProvider, org.eclipse.ui.dialogs.FilteredItemsSelectionDialog.ItemsFilter, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    protected void fillContentProvider(AbstractContentProvider contentProvider,
            ItemsFilter itemsFilter, IProgressMonitor progressMonitor)
            throws CoreException {
        if ((fTypes != null) && (fTypes.length > 0)) {
            for (IType element : fTypes) {
                if (itemsFilter.isConsistentItem(element)) {
                    contentProvider.add(element, itemsFilter);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog#getDialogSettings()
     */
    @Override
    protected IDialogSettings getDialogSettings() {
        IDialogSettings settings = MTJUIPlugin.getDefault().getDialogSettings();
        IDialogSettings section = settings.getSection(SETTINGS_ID);
        if (section == null) {
            section = settings.addNewSection(SETTINGS_ID);
        }
        return section;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog#getItemsComparator()
     */
    @Override
    protected Comparator<IType> getItemsComparator() {
        Comparator<IType> comp = new Comparator<IType>() {

            public int compare(IType o1, IType o2) {
                return o1.getElementName().compareTo(o2.getElementName());
            }
        };
        return comp;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.dialogs.FilteredItemsSelectionDialog#validateItem(java.lang.Object)
     */
    @Override
    protected IStatus validateItem(Object item) {
        return Status.OK_STATUS;
    }
}
