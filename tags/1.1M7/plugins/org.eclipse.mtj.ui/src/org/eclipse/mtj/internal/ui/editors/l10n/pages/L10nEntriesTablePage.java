/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 *     Eric Carlo (Motorola)    - Fixing table style.
 */
package org.eclipse.mtj.internal.ui.editors.l10n.pages;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.internal.core.IModelChangedEvent;
import org.eclipse.mtj.internal.core.IModelChangedListener;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.l10n.L10nEntry;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.editor.MTJFormPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

/**
 * L10nEntriesTablePage class implements the entries
 * page for the localization editor.
 * 
 * @author David Marques
 */
public class L10nEntriesTablePage extends MTJFormPage {

    private static final String KEY_COLUMN_LABEL = "Key"; //$NON-NLS-1$
    private L10nModel   model;
    private TableViewer viewer;

    /**
     * Creates an instance of the L10nEntriesTablePage.
     * 
     * @param editor parent editor.
     */
    public L10nEntriesTablePage(FormEditor editor) {
        super(editor, "l10nEntriesTablePage", MTJUIMessages.L10nEntriesTablePage_pageTitle); //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    protected void createFormContent(IManagedForm managedForm) {
        model = (L10nModel) getModel();
        // Ensure the model was loaded properly
        if ((model == null) || (model.isLoaded() == false)) {
            createErrorContent(managedForm, model);
            return;
        }

        FormToolkit toolkit = managedForm.getToolkit();
        ScrolledForm form = managedForm.getForm();
        toolkit.decorateFormHeading(form.getForm());
        form.setExpandHorizontal(true);
        form.setExpandVertical(true);
        form.setText(getTitle());

        Composite body = form.getBody();
        body.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        body.setLayout(new GridLayout(0x01, false));

        this.createL10nEntriesTableSection(managedForm, body);
        form.reflow(true);
    }

    private void createL10nEntriesTableSection(IManagedForm managedForm,
            Composite body) {
        GridData gridData = null;

        gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        Composite section = createSection(managedForm, body,
                MTJUIMessages.L10nEntriesTablePage_sectionTitle,
                MTJUIMessages.L10nEntriesTablePage_sectionText, gridData);
        section.setLayout(new GridLayout(0x01, true));

        viewer = new TableViewer(section, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
        Table table = viewer.getTable();
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        table.setLayoutData(gridData);

        viewer.setContentProvider(new L10nEntriesContentProvider());
        viewer.setCellModifier(new L10nEntriesTableCellModifier());
        viewer.setLabelProvider(new L10nEntriesLabelProvider());
        viewer.setInput(this.model);
    }

    /**
     * Creates a generic section.
     * 
     * @param managedForm parent form.
     * @param body parent composite.
     */
    private Composite createSection(IManagedForm managedForm, Composite parent,
            String text, String description, GridData gridData) {
        FormToolkit toolkit = managedForm.getToolkit();

        Section section = toolkit.createSection(parent, Section.DESCRIPTION
                | ExpandableComposite.TITLE_BAR);
        if (text != null) {
            section.setText(text);
        }
        if (description != null) {
            section.setDescription(description);
        }
        section.setLayoutData(gridData);
        section.setLayout(new GridLayout(0x01, false));

        Composite client = toolkit.createComposite(section, SWT.NONE);
        client.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        section.setClient(client);
        return client;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormPage#setActive(boolean)
     */
    public void setActive(boolean active) {
        if (active) {
            updateColumns(this.model);
        }
    }
    
    /**
     * Updates all columns and it's rows.
     * 
     * @param _model source model.
     */
    private void updateColumns(L10nModel _model) {
        Display.getDefault().syncExec(new Runnable() {
            public void run() {
                Table table = viewer.getTable();
                TableColumn columns[] = table.getColumns();
                for (TableColumn column : columns) {
                    column.dispose();
                }
                
                // Build Column Names
                List<String> names = new ArrayList<String>();
                names.add(KEY_COLUMN_LABEL);
                for (IDocumentElementNode node : model.getLocales().getChildNodes()) {
                    if (node instanceof L10nLocale) {
                        names.add(((L10nLocale) node).getLocaleName());
                    }
                }
                viewer.setColumnProperties(names.toArray(new String[0x00]));
                
                List<CellEditor> editors = new ArrayList<CellEditor>();
                for (String name : names) {
                    TableColumn column = new TableColumn(table, SWT.LEFT);
                    column.setText(name);
                    editors.add(new TextCellEditor(table));
                }
                viewer.setCellEditors(editors.toArray(new CellEditor[0x00]));
                
                TableLayout tableLayout = new TableLayout();
                int numColumns = names.size();
                for (int i = 0; i < numColumns; i++) {
                    tableLayout.addColumnData(new ColumnWeightData(100 / numColumns,
                            true));
                }
                table.setLayout(tableLayout);
                table.layout(true, true);
                viewer.refresh();
            }
        });
    }

    private void createErrorContent(IManagedForm managedForm, L10nModel model) {
        createFormErrorContent(managedForm,
                MTJUIMessages.LocalizationPage_formErrorContent_title,
                MTJUIMessages.LocalizationPage_formErrorContent_message);
    }

    private class L10nEntriesContentProvider implements
            IStructuredContentProvider, IModelChangedListener {

        private Viewer    viewer;
        private L10nModel model;

        public Object[] getElements(Object inputElement) {
            Object result[] = null;
            if (inputElement instanceof L10nModel) {
                L10nModel model = (L10nModel) inputElement;
                result = getAllL10nEntries(model);
            }
            return result;
        }

        private L10nEntry[] getAllL10nEntries(L10nModel model) {
            List<String>    keys    = new ArrayList<String>();
            List<L10nEntry> entries = new ArrayList<L10nEntry>();
            L10nLocales locales = model.getLocales();
            for (IDocumentElementNode localesNode : locales.getChildNodes()) {
                if (!(localesNode instanceof L10nLocale)) {
                    continue;
                }
                L10nLocale locale = (L10nLocale) localesNode;
                for (IDocumentElementNode localeNode : locale.getChildNodes()) {
                    if (!(localeNode instanceof L10nEntry)) {
                        continue;
                    }
                    L10nEntry entry = (L10nEntry) localeNode;
                    String    key   = entry.getKey();
                    if (!keys.contains(key)) {                                
                        entries.add(entry);
                        keys.add(key);
                    }
                }
            }
            return entries.toArray(new L10nEntry[0x00]);
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
            if (newInput instanceof L10nModel) {
                this.model = (L10nModel) newInput;
                model.addModelChangedListener(this);
                updateColumns(model);
            }
            this.viewer = viewer;
        }

        public void dispose() {
            this.viewer = null;
        }

        public void modelChanged(IModelChangedEvent event) {
            if (this.viewer != null) {
                updateColumns(this.model);
            }
        }
    }

    private class L10nEntriesLabelProvider implements ITableLabelProvider {

        public String getColumnText(Object element, int columnIndex) {
            String result = null;
            if (element instanceof L10nEntry) {
                L10nEntry entry = (L10nEntry) element;
                if (columnIndex > 0x00) {
                    result = resolveKeyValue(entry.getModel()
                            , entry.getKey(), columnIndex - 0x01);
                } else {
                    result = entry.getKey();
                }
            }
            return result;
        }

        private String resolveKeyValue(L10nModel model, String key, int index) {
            String value = null;
            
            L10nLocales locales = model.getLocales();
            IDocumentElementNode localeNode = locales.getChildAt(index);
            if (localeNode instanceof L10nLocale) {
                L10nEntry entry = ((L10nLocale)localeNode).getEntry(key);
                if (entry != null) {
                    value = entry.getValue();
                }
            }
            return value;
        }

        public boolean isLabelProperty(Object element, String property) {
            return false;
        }

        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        public void addListener(ILabelProviderListener listener) {}

        public void removeListener(ILabelProviderListener listener) {}

        public void dispose() {}
    }

    private class L10nEntriesTableCellModifier implements ICellModifier {

        public Object getValue(Object element, String property) {
            Object value = Utils.EMPTY_STRING;
            if (element instanceof L10nEntry) {
                L10nEntry entry = (L10nEntry) element; 
                L10nEntry other = resolveKeyEntry(entry.getModel(), entry.getKey(), property);
                if (other != null) {
                    value = other.getValue();
                }
            }
            return value;
        }

        private L10nEntry resolveKeyEntry(L10nModel model, String key, String localeName) {
            L10nEntry result = null;
            L10nLocales locales = model.getLocales();
            IDocumentElementNode localeNodes[] = locales.getChildNodes();
            for (IDocumentElementNode localeNode : localeNodes) {                
                if (!(localeNode instanceof L10nLocale)) {
                    continue;
                }
                L10nLocale locale = (L10nLocale) localeNode;
                if (locale.getLocaleName().equals(localeName)) {                        
                    L10nEntry entry = locale.getEntry(key);
                    if (entry != null) {
                        result = entry;
                    }
                }
            }
            return result;
        }

        public void modify(Object element, String property, Object value) {
            if (element instanceof Item) {
                element = ((Item)element).getData();
            }
            
            if (element instanceof L10nEntry) {
                L10nEntry entry = (L10nEntry) element;
                L10nModel model = entry.getModel();
                L10nEntry other = resolveKeyEntry(model, entry.getKey(), property);
                if (other == null) {
                    L10nLocale locale = model.getLocales().getLocale(property);
                    if (locale != null) {
                        other = new L10nEntry(model);
                        other.setKey(entry.getKey());
                        other.setValue((String)value);
                        locale.addChild(other);
                    }
                } else {
                    other.setValue((String)value);                    
                }
            }
        }
        
        public boolean canModify(Object element, String property) {
            return !property.equals(KEY_COLUMN_LABEL);
        }
    }
    
}
