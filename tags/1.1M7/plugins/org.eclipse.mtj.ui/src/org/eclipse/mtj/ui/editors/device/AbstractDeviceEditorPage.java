/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.editors.device;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.sdk.device.AbstractMIDPDevice;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * Abstract superclass for all of the device editor pages.
 * 
 * @since 1.0
 */
public abstract class AbstractDeviceEditorPage extends Composite {
    protected DeviceEditorDialog dialog;
    protected AbstractMIDPDevice editDevice;
    protected String errorMessage;
    protected boolean valid;

    /**
     * Construct a new editor page.
     * 
     * @param parent
     * @param style
     */
    public AbstractDeviceEditorPage(Composite parent, int style) {
        super(parent, style);

        // Assume we start valid...
        valid = true;

        setLayoutData(new GridData(GridData.FILL_BOTH));
        setLayout(new GridLayout(1, true));

        Composite topComposite = new Composite(this, SWT.NONE);
        topComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
        topComposite.setLayout(new GridLayout(1, true));

        Label label = new Label(topComposite, SWT.NONE);
        label.setText(getDescription());
        label.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Composite controlsComposite = new Composite(topComposite, SWT.NONE);
        controlsComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

        addPageControls(controlsComposite);
    }

    /**
     * Commit all of the changes on this page to the device.
     * 
     * @throws CoreException
     */
    public abstract void commitDeviceChanges() throws CoreException;

    /**
     * Return a description of what is being specified on this page.
     * 
     * @return
     */
    public abstract String getDescription();

    /**
     * Return the title for this page.
     * 
     * @return
     */
    public abstract String getTitle();

    /**
     * Return a boolean indicating whether the values on the page are valid.
     * 
     * @return
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * Set the device to be edited.
     * 
     * @param device
     */
    public void setDevice(IMIDPDevice device) {
        editDevice = (AbstractMIDPDevice) device;
    }

    /**
     * Set the dialog in which this page resides.
     * 
     * @param dialog
     */
    public void setDialog(DeviceEditorDialog dialog) {
        this.dialog = dialog;
    }

    /**
     * Create the controls specific to this page.
     * 
     * @param parent
     */
    protected abstract void addPageControls(Composite parent);

    /**
     * Set the error message to be displayed to the user.
     * 
     * @param message
     */
    protected void setErrorMessage(String message) {
        this.errorMessage = message;
        if ((dialog != null) && isVisible()) {
            dialog.setErrorMessage(message);
        }
    }

    /**
     * Update the validity status of this page.
     * 
     * @param isValid
     */
    protected void setValid(boolean isValid) {
        valid = isValid;

        if (dialog != null) {
            dialog.updateCompletionState();
        }
    }
}
