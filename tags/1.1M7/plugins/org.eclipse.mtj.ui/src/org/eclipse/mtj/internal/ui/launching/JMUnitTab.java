/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Aragao (Motorola) - Initial version
 */

package org.eclipse.mtj.internal.ui.launching;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.debug.ui.launchConfigurations.JavaLaunchTab;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.ui.JavaElementLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.launching.midp.IMIDPLaunchConstants;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.util.TestCaseSelectionDialogCreator;
import org.eclipse.mtj.internal.ui.util.TestSuiteSelectionDialogCreator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.eclipse.ui.dialogs.SelectionDialog;

/**
 * Specialization of the JavaMainTab for selecting Test Cases and Test Suites
 * for launch.
 * 
 * @author David Aragao
 */
public class JMUnitTab extends JavaLaunchTab {

	private Button projectButton;
	private Label projectLabel;
	// Dialog widgets
	private Text projectText;

	Group groupExecutable;
	
	private Button testSuiteRadio;
	private Text testSuiteText;
	private Button testSuiteSearchButton;

	private Button testCaseRadio;
	private Text testCaseText;
	private Button testCaseSearchButton;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.debug.ui.ILaunchConfigurationTab#getName()
	 */
	public String getName() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.debug.ui.ILaunchConfigurationTab#createControl(org.eclipse
	 * .swt.widgets.Composite)
	 */
	public void createControl(Composite parent) {
		Composite comp = new Composite(parent, SWT.NONE);
		setControl(comp);

		comp.setLayout(new GridLayout());

		createVerticalSpacer(comp, 3);
		createProjectComponents(comp);

		createVerticalSpacer(comp, 3);
		createExecutableComponents(comp);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jdt.debug.ui.launchConfigurations.JavaLaunchTab#initializeFrom
	 * (org.eclipse.debug.core.ILaunchConfiguration)
	 */
	public void initializeFrom(
			org.eclipse.debug.core.ILaunchConfiguration config) {
		updateProjectFromConfig(config);

		boolean doTestCase = false;

		try {
			doTestCase = config.getAttribute(
					IMIDPLaunchConstants.DO_TEST_CASE_LAUNCH, false);
		} catch (CoreException e) {
		}

		testSuiteRadio.setSelection(!doTestCase);
		testCaseRadio.setSelection(doTestCase);

		if (!doTestCase) {
			String testSuiteName = getStringOrDefault(config,
					IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
					Utils.EMPTY_STRING);
			testSuiteText.setText(testSuiteName);
		}

		if (doTestCase) {
			String testCaseName = getStringOrDefault(config,
					IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
					Utils.EMPTY_STRING);
			testCaseText.setText(testCaseName);
		}

		updateEnablement();
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jdt.debug.ui.launchConfigurations.JavaMainTab#isValid(org
	 * .eclipse.debug.core.ILaunchConfiguration)
	 */
	public boolean isValid(ILaunchConfiguration config) {
		String errorMessage = null;

		String name = projectText.getText().trim();
		if (name.length() > 0) {
			IProject selectedProject = MTJCore.getWorkspace().getRoot().getProject(name);
			if (!selectedProject.exists()) {
				errorMessage = MTJUIMessages.JMUnitTab_project_does_not_exist_error;
			} else
				try {
					if ((errorMessage == null) && !selectedProject.hasNature(IMTJCoreConstants.JMUNIT_NATURE_ID)) {
						errorMessage = MTJUIMessages.JMUnitTab_project_isnt_JMUnit_enabled_error;
						testSuiteSearchButton.setEnabled(false);
						testSuiteText.setEnabled(false);
						testCaseSearchButton.setEnabled(false);
						testCaseText.setEnabled(false);
					}else {
						updateEnablement();
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
		}

		if ((errorMessage == null) && testCaseRadio.getSelection()) {
			name = testCaseText.getText().trim();
			if (name.length() == 0) {
				errorMessage = MTJUIMessages.JMUnit_test_case_not_specified_error;
			}
		}

		if ((errorMessage == null) && testSuiteRadio.getSelection()) {
			name = testSuiteText.getText().trim();
			if (name.length() == 0) {
				errorMessage = MTJUIMessages.JMUnit_test_suite_not_specified_error;
			}
		}

		setErrorMessage(errorMessage);

		return (errorMessage == null);
	}

	/**
	 * Create the components that handle the executable information.
	 * 
	 * @param parent
	 */
	private void createExecutableComponents(Composite parent) {
		GridData gd;

		Font font = parent.getFont();
		groupExecutable = new Group(parent, SWT.NONE);
		groupExecutable.setText(MTJUIMessages.JMUnitTab_testType_text);

		// Set up the layout
		GridLayout mainLayout = new GridLayout(3, false);
		mainLayout.marginHeight = 0;
		mainLayout.marginWidth = 0;
		groupExecutable.setLayout(mainLayout);

		gd = new GridData(GridData.FILL_HORIZONTAL);
		groupExecutable.setLayoutData(gd);
		groupExecutable.setFont(font);

		this.createSuiteComponents(groupExecutable);
		this.createTestCaseComponents(groupExecutable);
	}

	/**
	 * Create the components that handle the project information.
	 * 
	 * @param parent
	 */
	private void createProjectComponents(Composite parent) {
		GridData gd;

		Font font = parent.getFont();
		Composite projComp = new Composite(parent, SWT.NONE);
		GridLayout projLayout = new GridLayout();
		projLayout.numColumns = 2;
		projLayout.marginHeight = 0;
		projLayout.marginWidth = 0;
		projComp.setLayout(projLayout);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		projComp.setLayoutData(gd);
		projComp.setFont(font);

		projectLabel = new Label(projComp, SWT.NONE);
		projectLabel.setText(MTJUIMessages.JMUnitTab_project_text);
		gd = new GridData();
		gd.horizontalSpan = 2;
		projectLabel.setLayoutData(gd);
		projectLabel.setFont(font);

		projectText = new Text(projComp, SWT.SINGLE | SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		projectText.setLayoutData(gd);
		projectText.setFont(font);
		projectText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent evt) {
				updateLaunchConfigurationDialog();
			}
		});

		projectButton = createPushButton(projComp,
				MTJUIMessages.JMUnitTab_browse_btn, null);
		projectButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent evt) {
				IJavaProject project = chooseJavaProject();
				if (project != null) {
					String projectName = project.getElementName();
					projectText.setText(projectName);
				}
			}
		});
	}

	/**
	 * Create the components that make up the suite Tests prompting.
	 * 
	 * @param parent
	 */
	private void createSuiteComponents(Group parent) {
		GridData gd;

		Font font = parent.getFont();

		testSuiteRadio = new Button(parent, SWT.RADIO);
		testSuiteRadio.setText(MTJUIMessages.JMUnitTab_Suite_text);
		testSuiteRadio.setSelection(true);
		testSuiteRadio.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateEnablement();
				updateLaunchConfigurationDialog();
			}
		});

		testSuiteText = new Text(parent, SWT.SINGLE | SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		testSuiteText.setLayoutData(gd);
		testSuiteText.setFont(font);
		testSuiteText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent evt) {
				updateLaunchConfigurationDialog();
			}
		});

		testSuiteSearchButton = createPushButton(parent,
				MTJUIMessages.JMUnitTab_browse_btn, null);
		testSuiteSearchButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent evt) {
				handleSuiteSearchButtonSelected();
			}
		});
	}

	/**
	 * Create the components that make up the suite Tests prompting.
	 * 
	 * @param parent
	 */
	private void createTestCaseComponents(Group parent) {
		GridData gd;

		Font font = parent.getFont();

		testCaseRadio = new Button(parent, SWT.RADIO);
		testCaseRadio.setText(MTJUIMessages.JMUnitTab_TestCase_text);
		testCaseRadio.setSelection(true);
		testCaseRadio.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				updateEnablement();
				updateLaunchConfigurationDialog();
			}
		});

		testCaseText = new Text(parent, SWT.SINGLE | SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		testCaseText.setLayoutData(gd);
		testCaseText.setFont(font);
		testCaseText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent evt) {
				updateLaunchConfigurationDialog();
			}
		});

		testCaseSearchButton = createPushButton(parent,
				MTJUIMessages.JMUnitTab_browse_btn, null);
		testCaseSearchButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent evt) {
				handleTestCaseSearchButtonSelected();
			}
		});
	}

	/**
	 * Chooses a project for the type of java launch config that it is
	 * 
	 * @return
	 */
	private IJavaProject chooseJavaProject() {

		IJavaProject project = null;

		ILabelProvider labelProvider = new JavaElementLabelProvider(
				JavaElementLabelProvider.SHOW_DEFAULT);
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(
				getShell(), labelProvider);
		dialog.setTitle(MTJUIMessages.JMUnitTab_chooseJavaProject_title);
		dialog.setMessage(MTJUIMessages.JMUnitTab_chooseJavaProject_message);

		try {
			IWorkspaceRoot root = MTJCore.getWorkspace().getRoot();
			dialog.setElements(JavaCore.create(root).getJavaProjects());
		} catch (JavaModelException jme) {
			MTJLogger.log(IStatus.WARNING, jme);
		}

		IJavaProject javaProject = getJavaProject();
		if (javaProject != null) {
			dialog.setInitialSelections(new Object[] { javaProject });
		}

		if (dialog.open() == Window.OK) {
			project = (IJavaProject) dialog.getFirstResult();
		}

		return project;
	}

	/**
	 * @return
	 */
	protected IJavaProject getJavaProject() {
		IJavaProject javaProject = null;

		String projectName = projectText.getText().trim();
		if (projectName.length() > 0) {
			IProject project = MTJCore.getWorkspace().getRoot().getProject(
					projectName);
			javaProject = JavaCore.create(project);
		}

		return javaProject;
	}

	/**
	 * Update the enablement of controls based on the status of the other
	 * controls.
	 */
	private void updateEnablement() {
		boolean doSuite = testSuiteRadio.getSelection();
		testSuiteText.setEnabled(doSuite);
		testSuiteSearchButton.setEnabled(doSuite);

		boolean doTestCase = testCaseRadio.getSelection();
		testCaseText.setEnabled(doTestCase);
		testCaseSearchButton.setEnabled(doTestCase);
	}

	/**
	 * Update the project field from the launch configuration.
	 * 
	 * @param config
	 *            a launch configuration for the project
	 */
	protected void updateProjectFromConfig(ILaunchConfiguration config) {
		String projectName = Utils.EMPTY_STRING;
		try {
			projectName = config.getAttribute(
					IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
					Utils.EMPTY_STRING);
		} catch (CoreException ce) {
			MTJLogger.log(IStatus.WARNING,
					MTJUIMessages.JMUnitTab_updateProjectFromConfig_error, ce);
		}

		projectText.setText(projectName);
	}

	/**
	 * Get a string attribute from the launch configuration or the specified
	 * default value.
	 * 
	 * @param launchConfig
	 * @param attributeName
	 * @param defaultValue
	 * @return
	 */
	private String getStringOrDefault(ILaunchConfiguration launchConfig,
			String attributeName, String defaultValue) {
		String value = null;

		try {
			value = launchConfig.getAttribute(attributeName, defaultValue);
		} catch (CoreException e) {
			MTJLogger.log(IStatus.WARNING, e);
			value = defaultValue;
		}

		return value;
	}

	/**
     * 
     */
	protected void handleSuiteSearchButtonSelected() {
		try {
			IJavaProject javaProject = getJavaProject();
			SelectionDialog dialog = TestSuiteSelectionDialogCreator
					.createTestSuiteSelectionDialog(getShell(),
							getLaunchConfigurationDialog(), javaProject, false);

			if (dialog.open() == Window.OK) {
				Object[] results = dialog.getResult();
				if ((results != null) && (results.length > 0)) {
					IType type = (IType) results[0];
					if (type != null) {
						testSuiteText.setText(type.getFullyQualifiedName());
						javaProject = type.getJavaProject();
						projectText.setText(javaProject.getElementName());
					}
				}
			}
		} catch (JavaModelException e) {
			MTJLogger
					.log(
							IStatus.ERROR,
							MTJUIMessages.JMUnitTab_handleSearchTestSuiteButtonSelected_error,
							e);
		}
	}

	/**
     * 
     */
	protected void handleTestCaseSearchButtonSelected() {
		try {
			IJavaProject javaProject = getJavaProject();
			SelectionDialog dialog = TestCaseSelectionDialogCreator
					.createTestCaseSelectionDialog(getShell(),
							getLaunchConfigurationDialog(), javaProject, false);

			if (dialog.open() == Window.OK) {
				Object[] results = dialog.getResult();
				if ((results != null) && (results.length > 0)) {
					IType type = (IType) results[0];
					if (type != null) {
						testCaseText.setText(type.getFullyQualifiedName());
						javaProject = type.getJavaProject();
						projectText.setText(javaProject.getElementName());
					}
				}
			}
		} catch (JavaModelException e) {
			MTJLogger
					.log(
							IStatus.ERROR,
							MTJUIMessages.JMUnitTab_handleSearchTestCaseButtonSelected_error,
							e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.debug.ui.ILaunchConfigurationTab#performApply(org.eclipse
	 * .debug.core.ILaunchConfigurationWorkingCopy)
	 */
	public void performApply(ILaunchConfigurationWorkingCopy config) {
		config.setAttribute(
				IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
				projectText.getText().trim());

		if (testCaseRadio.getSelection()) {
			config.setAttribute(IMIDPLaunchConstants.DO_TEST_CASE_LAUNCH, true);
			config.setAttribute(IMIDPLaunchConstants.DO_SUITE_TEST_LAUNCH,
					false);
			config.setAttribute(
					IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
					testCaseText.getText());
		} else if (testSuiteRadio.getSelection()) {
			config
					.setAttribute(IMIDPLaunchConstants.DO_SUITE_TEST_LAUNCH,
							true);
			config
					.setAttribute(IMIDPLaunchConstants.DO_TEST_CASE_LAUNCH,
							false);
			config.setAttribute(
					IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
					testSuiteText.getText());
		}

		config.setAttribute(IMIDPLaunchConstants.DO_OTA, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.debug.ui.ILaunchConfigurationTab#setDefaults(org.eclipse.
	 * debug.core.ILaunchConfigurationWorkingCopy)
	 */
	public void setDefaults(ILaunchConfigurationWorkingCopy config) {
        IJavaElement javaElement = getContext();
        if (javaElement != null) {
            initializeJavaProject(javaElement, config);
        } else {
            /*
             * We set empty attributes for project & main type so that when one
             * config is compared to another, the existence of empty attributes
             * doesn't cause an incorrect result (the performApply() method can
             * result in empty values for these attributes being set on a config
             * if there is nothing in the corresponding text boxes)
             */
            config.setAttribute(
                    IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
                    Utils.EMPTY_STRING);
        }

        config.setAttribute(
                IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
                Utils.EMPTY_STRING);
    }


}
