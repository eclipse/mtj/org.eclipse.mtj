/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code
 *     David Marques (Motorola) - Using Utils nature management functions.       
 *     David Marques (Motorola) - Avoid adding builder twice.                         
 */
package org.eclipse.mtj.internal.ui.actions.preprocessing;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.mtj.core.project.MTJNature;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.actions.AbstractJavaProjectAction;

/**
 * An action that enables the natures, etc such that a project can have
 * preprocessor functionality.
 * 
 * @author Craig Setera
 */
public class EnablePreprocessingAction extends AbstractJavaProjectAction {

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
     */
    public void run(IAction action) {
        if ((selection != null) && !selection.isEmpty()) {
            IJavaProject javaProject = getJavaProject(selection
                    .getFirstElement());
            if (javaProject != null) {
                try {
                    enablePreprocessing(javaProject);
                } catch (CoreException e) {
                    MTJLogger.log(IStatus.ERROR, e);
                }
            }
        }
    }

    /**
     * Enable preprocessing on the specified java project.
     * 
     * @param javaProject
     * @throws CoreException
     */
    private void enablePreprocessing(IJavaProject javaProject)
            throws CoreException {
        IProject project = javaProject.getProject();
        MTJNature.addNatureToProject(project,
                IMTJCoreConstants.J2ME_PREPROCESSING_NATURE_ID,
                new NullProgressMonitor());
    }

}
