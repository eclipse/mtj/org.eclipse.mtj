/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 *     Feng Wang (Sybase)      - Add configurations into Metadata for Multi-Configs
 *                               support.
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874]
 *     Jon Dearden (Research In Motion) - Replaced deprecated use of Preferences 
 *                                        [Bug 285699]
 */
package org.eclipse.mtj.internal.ui.wizards.projects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileInfo;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceStatus;
import org.eclipse.core.resources.IWorkspaceDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.PreferenceConstants;
import org.eclipse.jdt.ui.wizards.JavaCapabilityConfigurationPage;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.IMetaData;
import org.eclipse.mtj.core.project.ProjectType;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.PreferenceAccessor;
import org.eclipse.mtj.internal.core.build.preprocessor.PreprocessorBuilder;
import org.eclipse.mtj.internal.core.externallibrary.classpath.ExternalLibraryClasspathContainer;
import org.eclipse.mtj.internal.core.externallibrary.manager.ExternalLibraryManager;
import org.eclipse.mtj.internal.core.externallibrary.model.IExternalLibrary;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.project.midp.JavaMEClasspathContainer;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.util.ExceptionHandler;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IWorkingSet;
import org.eclipse.ui.IWorkingSetManager;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyDelegatingOperation;

/**
 * The New MIDlet Project Java Capability Wizard Page allows the user to
 * configure the build path and output location of the MIDlet Project. As
 * addition to the {@link JavaCapabilityConfigurationPage}, the wizard page does
 * an early project creation (so that linked folders can be defined).
 * 
 * @author Diego Madruga Sandin
 * @since 0.9
 */
public class NewMidletProjectWizardPageJavaSettings extends
        JavaCapabilityConfigurationPage {

    private static final String FILENAME_CLASSPATH = ".classpath"; //$NON-NLS-1$

    private static final String FILENAME_PROJECT = ".project"; //$NON-NLS-1$

    /**
     * @param projectName
     * @param location
     * @return
     */
    private static URI getRealLocation(String projectName, URI location) {
        if (location == null) { // inside workspace
            try {
                URI rootLocation = MTJCore.getWorkspace().getRoot()
                        .getLocationURI();

                location = new URI(rootLocation.getScheme(), null, Path
                        .fromPortableString(rootLocation.getPath()).append(
                                projectName).toString(), null);
            } catch (URISyntaxException e) {
                Assert.isTrue(false, "Can't happen"); //$NON-NLS-1$
            }
        }
        return location;
    }

    public Boolean isAutobuild;
    private IProject currProject;
    private URI currProjectLocation;
    private File fDotClasspathBackup;
    private File fDotProjectBackup;
    private NewMidletProjectWizardPageOne firstPage;
    private NewMidletProjectWizardPageProperties propertiesPage;
    private HashSet<IFileStore> fOrginalFolders;

    private boolean keepContent;

    private NewMidletProjectWizardPageLibrary pageLibrary;

    /**
     * Creates a new New MIDlet Project Java Capability Wizard Page.
     * 
     * @param workbench
     */
    public NewMidletProjectWizardPageJavaSettings(NewMidletProjectWizardPageOne pageOne,
            NewMidletProjectWizardPageProperties pageTwo,
            NewMidletProjectWizardPageLibrary pageLibrary) {
        this.currProjectLocation = null;
        this.currProject = null;
        this.firstPage = pageOne;
        this.propertiesPage = pageTwo;
        this.pageLibrary = pageLibrary;
        isAutobuild = null;
    }

    /**
     * Called from the wizard on cancel.
     */
    public void performCancel() {
        if (currProject != null) {
            removeProvisonalProject();
        }
    }

    /**
     * Called from the wizard on finish.
     * 
     * @param monitor the progress monitor
     * @throws CoreException thrown when the project creation or configuration
     *             failed
     * @throws InterruptedException thrown when the user canceled the project
     *             creation
     */
    @SuppressWarnings("unchecked")
    public void performFinish(IProgressMonitor monitor) throws CoreException,
            InterruptedException {
        try {

            monitor
                    .beginTask(
                            MTJUIMessages.NewMidletProjectWizardPageThree_performFinish_monitor_taskname,
                            3);
            if (currProject == null) {
                updateProject(new SubProgressMonitor(monitor, 1));
            }

            configureJavaProject(new SubProgressMonitor(monitor, 2));

            if (!keepContent) {
                IJavaProject project = JavaCore.create(currProject);
                Map options = project.getOptions(false);

                // complete compliance options
                project.setOptions(options);
            }

        } finally {
            monitor.done();
            currProject = null;
            if (isAutobuild != null) {
                IWorkspaceDescription description = ResourcesPlugin
                        .getWorkspace().getDescription();
                description.setAutoBuilding(isAutobuild.booleanValue());
                MTJCore.getWorkspace().setDescription(description);
                isAutobuild = null;
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.dialogs.IDialogPage#setVisible(boolean)
     */
    @Override
    public void setVisible(boolean visible) {
        boolean isShownFirstTime = visible && (currProject == null);
        if (visible) {
            if (isShownFirstTime) {
                // entering from the first page
                createProvisonalProject();
            }
        } else {
            if ((getContainer().getCurrentPage() == firstPage)
                    || (getContainer().getCurrentPage() == pageLibrary)) {
                // leaving back to previous pages
                removeProvisonalProject();
            }
        }
        super.setVisible(visible);
        if (isShownFirstTime) {
            setFocus();
        }
    }

    /**
     * Update the java configuration before making the page visible.
     */
    public void updateConfiguration() {
        String projectName = firstPage.getProjectName();

        currProject = MTJCore.getWorkspace().getRoot().getProject(
                projectName);

        IJavaProject javaProject = JavaCore.create(currProject);
        IPath projectPath = currProject.getFullPath();

        // Initialize the classpath entries using the source directories
        // and classpath container
        ArrayList<IClasspathEntry> entryList = new ArrayList<IClasspathEntry>();
        entryList.add(getSrcPathEntry(projectPath));
        addResourcesDirectoryIfRequested(entryList, currProject);
        IPath entryPath = new Path(JavaMEClasspathContainer.JAVAME_CONTAINER
                + "/" + firstPage.getSelectedDevice()); //$NON-NLS-1$

        entryList.add(JavaCore.newContainerEntry(entryPath));

        IClasspathEntry[] entries = entryList
                .toArray(new IClasspathEntry[entryList.size()]);

        init(javaProject, null, entries, false);
    }

    /**
     * Add a resources directory as a source path entry if the user preferences
     * requested.
     * 
     * @param entryList
     * @param project
     */
    private void addResourcesDirectoryIfRequested(
            List<IClasspathEntry> entryList, IProject project) {
        PreferenceAccessor prefs = PreferenceAccessor.instance;

        if (useSourceAndBinaryFolders()
                && prefs.getBoolean(IMTJCoreConstants.PREF_USE_RESOURCES_DIR)) {
            // Create the resources directory if it doesn't already exist
            String resDirName = prefs
                    .getString(IMTJCoreConstants.PREF_RESOURCES_DIR);
            IFolder resFolder = project.getFolder(resDirName);

            if (!resFolder.exists()) {
                try {
                    resFolder.create(true, true, new NullProgressMonitor());
                } catch (CoreException e) {
                    e.printStackTrace();
                }
            }

            // Add it as a source folder to the java project
            entryList.add(JavaCore.newSourceEntry(resFolder.getFullPath()));
        }
    }

    private IStatus changeToNewProject() {
        class UpdateRunnable implements IRunnableWithProgress {
            public IStatus infoStatus = Status.OK_STATUS;

            public void run(IProgressMonitor monitor)
                    throws InvocationTargetException, InterruptedException {
                try {
                    if (isAutobuild == null) {
                        IWorkspaceDescription description = ResourcesPlugin
                                .getWorkspace().getDescription();
                        isAutobuild = Boolean.valueOf(description
                                .isAutoBuilding());
                        description.setAutoBuilding(false);
                        MTJCore.getWorkspace().setDescription(
                                description);
                    }
                    infoStatus = updateProject(monitor);
                } catch (CoreException e) {
                    throw new InvocationTargetException(e);
                } catch (OperationCanceledException e) {
                    throw new InterruptedException();
                } finally {
                    monitor.done();
                }
            }
        }

        UpdateRunnable op = new UpdateRunnable();
        try {
            getContainer().run(true, false,
                    new WorkspaceModifyDelegatingOperation(op));

            return op.infoStatus;
        } catch (InvocationTargetException e) {

            final String title = MTJUIMessages.NewMidletProjectWizardPageThree_changeToNewProject_errordialog_title;
            final String message = MTJUIMessages.NewMidletProjectWizardPageThree_changeToNewProject_errordialog_message;
            ExceptionHandler.handle(e, getShell(), title, message);

        } catch (InterruptedException e) {
            // cancel pressed
        }
        return null;
    }

    /**
     * @param source
     * @param target
     * @param monitor
     * @throws IOException
     * @throws CoreException
     */
    private void copyFile(File source, IFileStore target,
            IProgressMonitor monitor) throws IOException, CoreException {
        FileInputStream is = new FileInputStream(source);
        OutputStream os = target.openOutputStream(EFS.NONE, monitor);
        copyFile(is, os);
    }

    /**
     * @param source
     * @param target
     * @throws IOException
     * @throws CoreException
     */
    private void copyFile(IFileStore source, File target) throws IOException,
            CoreException {
        InputStream is = source.openInputStream(EFS.NONE, null);
        FileOutputStream os = new FileOutputStream(target);
        copyFile(is, os);
    }

    /**
     * @param is
     * @param os
     * @throws IOException
     */
    private void copyFile(InputStream is, OutputStream os) throws IOException {
        try {
            byte[] buffer = new byte[8192];
            while (true) {
                int bytesRead = is.read(buffer);
                if (bytesRead == -1) {
                    break;
                }

                os.write(buffer, 0, bytesRead);
            }
        } finally {
            try {
                is.close();
            } finally {
                os.close();
            }
        }
    }

    /**
     * @param source
     * @param name
     * @return
     * @throws CoreException
     */
    private File createBackup(IFileStore source, String name)
            throws CoreException {
        try {
            File bak = File.createTempFile("eclipse-" + name, ".bak"); //$NON-NLS-1$//$NON-NLS-2$
            copyFile(source, bak);
            return bak;
        } catch (IOException e) {
            IStatus status = new Status(
                    IStatus.ERROR,
                    IMTJUIConstants.PLUGIN_ID,
                    IStatus.ERROR,
                    MTJUIMessages.NewMidletProjectWizardPageThree_createBackup_error_1
                            + name
                            + MTJUIMessages.NewMidletProjectWizardPageThree_createBackup_error_2,
                    e);
            throw new CoreException(status);
        }
    }

    private void deleteProjectFile(URI projectLocation) throws CoreException {
        IFileStore file = EFS.getStore(projectLocation);
        if (file.fetchInfo().exists()) {
            IFileStore projectFile = file.getChild(FILENAME_PROJECT);
            if (projectFile.fetchInfo().exists()) {
                projectFile.delete(EFS.NONE, null);
            }
        }
    }

    private final void doRemoveProject(IProgressMonitor monitor)
            throws InvocationTargetException {
        final boolean noProgressMonitor = (currProjectLocation == null); // inside
        // workspace
        if ((monitor == null) || noProgressMonitor) {
            monitor = new NullProgressMonitor();
        }
        monitor
                .beginTask(
                        MTJUIMessages.NewMidletProjectWizardPageThree_doRemoveProject_taskname,
                        3);
        try {
            try {
                URI projLoc = currProject.getLocationURI();

                boolean removeContent = !keepContent
                        && currProject.isSynchronized(IResource.DEPTH_INFINITE);
                if ((!removeContent)
                        && (currProject.getProject().getFullPath().toFile()
                                .exists())) {
                    restoreExistingFolders(projLoc);
                }
                currProject.delete(removeContent, false,
                        new SubProgressMonitor(monitor, 2));

                MidletSuiteFactory.removeMidletSuiteProject(getJavaProject());

                restoreExistingFiles(projLoc,
                        new SubProgressMonitor(monitor, 1));
            } finally {
                IWorkspaceDescription description = ResourcesPlugin
                        .getWorkspace().getDescription();
                description.setAutoBuilding(isAutobuild.booleanValue());
                MTJCore.getWorkspace().setDescription(description);
                // isAutobuild must be set
                isAutobuild = null;
            }
        } catch (CoreException e) {
            throw new InvocationTargetException(e);
        } finally {
            monitor.done();
            currProject = null;
            keepContent = false;
        }
    }

    /**
     * Get the source path for the project taking into account the new project
     * preferences that the user has specified.
     * 
     * @param projectPath
     * @return
     */
    private IPath getSrcPath(IPath projectPath) {
        IPath srcPath = projectPath;

        if (useSourceAndBinaryFolders()) {
            IPreferenceStore store = PreferenceConstants.getPreferenceStore();
            String srcPathName = store
                    .getString(PreferenceConstants.SRCBIN_SRCNAME);
            srcPath = projectPath.append(srcPathName);
        }

        return srcPath;
    }

    /**
     * Return an IClasspathEntry for the source path.
     * 
     * @param projectPath
     * @return
     */
    private IClasspathEntry getSrcPathEntry(IPath projectPath) {
        IPath srcPath = getSrcPath(projectPath);

        // Set up exclusions for the verified and deployed directories
        // if the source and project directories are the same
        IPath[] exclusions = null;
        if (srcPath.equals(projectPath)) {
            exclusions = new IPath[3];
            exclusions[0] = new Path(MTJCore.getDeploymentDirectoryName() + "/"); //$NON-NLS-1$
            exclusions[1] = new Path(IMTJCoreConstants.TEMP_FOLDER_NAME + "/"); //$NON-NLS-1$
            exclusions[2] = new Path(PreprocessorBuilder.PROCESSED_DIRECTORY
                    + "/"); //$NON-NLS-1$
        } else {
            exclusions = new IPath[0];
        }

        return JavaCore.newSourceEntry(srcPath, exclusions);
    }

    /**
     * @param realLocation
     * @return
     * @throws CoreException
     */
    private boolean hasExistingContent(URI realLocation) throws CoreException {
        IFileStore file = EFS.getStore(realLocation);
        return file.fetchInfo().exists();
    }

    private void rememberExisitingFolders(URI projectLocation) {
        fOrginalFolders = new HashSet<IFileStore>();

        try {
            IFileStore[] children = EFS.getStore(projectLocation).childStores(
                    EFS.NONE, null);
            for (IFileStore child : children) {
                IFileInfo info = child.fetchInfo();
                if (info.isDirectory() && info.exists()
                        && !fOrginalFolders.contains(child.getName())) {
                    fOrginalFolders.add(child);
                }
            }
        } catch (CoreException e) {
            final String title = MTJUIMessages.NewMidletProjectWizardPageThree_rememberExisitingFolders_errordialog_title;
            final String message = MTJUIMessages.NewMidletProjectWizardPageThree_rememberExisitingFolders_errordialog_message;
            ExceptionHandler.handle(e, getShell(), title, message);
        }
    }

    private void rememberExistingFiles(URI projectLocation)
            throws CoreException {
        fDotProjectBackup = null;
        fDotClasspathBackup = null;

        IFileStore file = EFS.getStore(projectLocation);
        if (file.fetchInfo().exists()) {
            IFileStore projectFile = file.getChild(FILENAME_PROJECT);
            if (projectFile.fetchInfo().exists()) {
                fDotProjectBackup = createBackup(projectFile, "project-desc"); //$NON-NLS-1$ 
            }
            IFileStore classpathFile = file.getChild(FILENAME_CLASSPATH);
            if (classpathFile.fetchInfo().exists()) {
                fDotClasspathBackup = createBackup(classpathFile,
                        "classpath-desc"); //$NON-NLS-1$ 
            }
        }
    }

    private void restoreExistingFiles(URI projectLocation,
            IProgressMonitor monitor) throws CoreException {
        int ticks = ((fDotProjectBackup != null ? 1 : 0) + (fDotClasspathBackup != null ? 1
                : 0)) * 2;
        monitor.beginTask("", ticks); //$NON-NLS-1$
        try {
            IFileStore projectFile = EFS.getStore(projectLocation).getChild(
                    FILENAME_PROJECT);
            projectFile.delete(EFS.NONE, new SubProgressMonitor(monitor, 1));
            if (fDotProjectBackup != null) {
                copyFile(fDotProjectBackup, projectFile,
                        new SubProgressMonitor(monitor, 1));
            }
        } catch (IOException e) {
            IStatus status = new Status(
                    IStatus.ERROR,
                    IMTJUIConstants.PLUGIN_ID,
                    IStatus.ERROR,
                    MTJUIMessages.NewMidletProjectWizardPageThree_restoreExistingFiles_problem_restoring_dotproject,
                    e);
            throw new CoreException(status);
        }
        try {
            IFileStore classpathFile = EFS.getStore(projectLocation).getChild(
                    FILENAME_CLASSPATH);
            classpathFile.delete(EFS.NONE, new SubProgressMonitor(monitor, 1));
            if (fDotClasspathBackup != null) {
                copyFile(fDotClasspathBackup, classpathFile,
                        new SubProgressMonitor(monitor, 1));
            }
        } catch (IOException e) {
            IStatus status = new Status(
                    IStatus.ERROR,
                    IMTJUIConstants.PLUGIN_ID,
                    IStatus.ERROR,
                    MTJUIMessages.NewMidletProjectWizardPageThree_restoreExistingFiles_problem_restoring_dotclasspath,
                    e);
            throw new CoreException(status);
        }
    }

    private void restoreExistingFolders(URI projectLocation) {
        try {
            IFileStore[] children = EFS.getStore(projectLocation).childStores(
                    EFS.NONE, null);
            for (IFileStore child : children) {
                IFileInfo info = child.fetchInfo();
                if (info.isDirectory() && info.exists()
                        && !fOrginalFolders.contains(child)) {
                    child.delete(EFS.NONE, null);
                    fOrginalFolders.remove(child);
                }
            }

            for (IFileStore fileStore : fOrginalFolders) {
                IFileStore deleted = fileStore;
                deleted.mkdir(EFS.NONE, null);
            }
        } catch (CoreException e) {
            final String title = MTJUIMessages.NewMidletProjectWizardPageThree_rememberExisitingFolders_errordialog_title;
            final String message = MTJUIMessages.NewMidletProjectWizardPageThree_rememberExisitingFolders_errordialog_message;
            ExceptionHandler.handle(e, getShell(), title, message);
        }
    }

    /**
     * @param monitor
     * @return
     * @throws CoreException
     * @throws InterruptedException
     */
    private final IStatus updateProject(IProgressMonitor monitor)
            throws CoreException, InterruptedException {
        IStatus result = Status.OK_STATUS;
        if (monitor == null) {
            monitor = new NullProgressMonitor();
        }
        try {
            monitor
                    .beginTask(
                            MTJUIMessages.NewMidletProjectWizardPageThree_updateProject_taskname,
                            7);
            if (monitor.isCanceled()) {
                throw new OperationCanceledException();
            }

            String projectName = firstPage.getProjectName();

            currProject = MTJCore.getWorkspace().getRoot().getProject(
                    projectName);
            currProjectLocation = firstPage.getProjectLocationURI();

            URI realLocation = getRealLocation(projectName, currProjectLocation);
            keepContent = hasExistingContent(realLocation);

            if (monitor.isCanceled()) {
                throw new OperationCanceledException();
            }

            if (keepContent) {
                rememberExistingFiles(realLocation);
                rememberExisitingFolders(realLocation);
            }

            if (monitor.isCanceled()) {
                throw new OperationCanceledException();
            }

            try {

                JavaCapabilityConfigurationPage.createProject(currProject,
                        firstPage.getProjectLocationURI(),
                        new SubProgressMonitor(monitor, 1));

                // Set the device into the project metadata to make
                // the java project creation happy.
                IDevice device = firstPage.getSelectedDevice();
                MTJRuntimeList configurations = firstPage.getConfigurations();
                // Make up Metadata and save it.
                IMetaData metadata = MTJCore.createMetaData(currProject,
                        ProjectType.MIDLET_SUITE);
                metadata.setMTJRuntimeList(configurations);
                try {
                    metadata.saveMetaData();
                } catch (CoreException e) {
                    throw new InvocationTargetException(e);
                }

                updateConfiguration();

                // Get the java nature
                try {
                    IRunnableWithProgress progress = super.getRunnable();
                    progress.run(monitor);
                } catch (Throwable e) {
                    throw new CoreException(new Status(
                            IResourceStatus.FAILED_READ_METADATA, "", e //$NON-NLS-1$
                                    .getMessage()));
                }

                // Get the J2ME nature and metadata set up
                String jadFileName = firstPage.getJadFileName();

                IJavaProject javaProject = super.getJavaProject();

                MidletSuiteFactory.MidletSuiteCreationRunnable runnable = MidletSuiteFactory
                        .getMidletSuiteCreationRunnable(currProject,
                                javaProject, (IMIDPDevice) device, jadFileName);

                Map<String, String> properties = new HashMap<String, String>();
                properties.put(IJADConstants.JAD_MIDLET_NAME, propertiesPage.getMIDletName());
                properties.put(IJADConstants.JAD_MIDLET_VENDOR, propertiesPage.getMIDletVendor());
                properties.put(IJADConstants.JAD_MIDLET_VERSION, propertiesPage.getMIDletVersion());
                properties.put(IJADConstants.JAD_MICROEDITION_CONFIG, propertiesPage.getMeConfiguration());
                properties.put(IJADConstants.JAD_MICROEDITION_PROFILE, propertiesPage.getMeProfile());
                
                runnable.setProperties(properties);
                
                
                runnable.setPreprocessingEnable(propertiesPage
                        .isPreprocessingEnabled());
                
                boolean localizationEnabled = propertiesPage
                .isLocalizationEnabled();
                runnable.setLocalizationEnabled(localizationEnabled);
                if (localizationEnabled) {
                    runnable.setPropertiesFolderName(propertiesPage
                            .getPropertiesFolderName());
                    runnable.setPackageName(propertiesPage.getPackageName());
                }
                runnable.setJMUnitSupport(propertiesPage.isJMUnitEnabled());

                runnable.run(monitor);
                
                IAdaptable[] adaptableElements = new IAdaptable[] { currProject };
                IWorkingSet workingSet = firstPage.getWorkingSet();
                if (workingSet != null) {
                    List<IAdaptable> list = new Vector<IAdaptable>();

                    for (IAdaptable adaptable : workingSet.getElements()) {
                        list.add(adaptable);
                    }
                    for (IAdaptable adaptable : workingSet
                            .adaptElements(adaptableElements)) {
                        list.add(adaptable);
                    }
                    IAdaptable[] newElements = new IAdaptable[list.size()];
                    workingSet.setElements(list.toArray(newElements));
                    IWorkingSetManager workingSetManager = PlatformUI
                            .getWorkbench().getWorkingSetManager();
                    workingSetManager.addRecentWorkingSet(workingSet);
                }


            } catch (CoreException e) {
                if (e.getStatus().getCode() == IResourceStatus.FAILED_READ_METADATA) {
                    result = new Status(
                            IStatus.INFO,
                            IMTJUIConstants.PLUGIN_ID,
                            MTJUIMessages.NewMidletProjectWizardPageThree_updateProject_fail_read_metadata);

                    deleteProjectFile(realLocation);
                    if (currProject.exists()) {
                        currProject.delete(true, null);
                    }

                    createProject(currProject, currProjectLocation, null);
                } else {
                    throw e;
                }
            } catch (Throwable e) {
                throw new CoreException(Status.CANCEL_STATUS);
            }

            if (monitor.isCanceled()) {
                throw new OperationCanceledException();
            }

            initializeBuildPath(JavaCore.create(currProject),
                    new SubProgressMonitor(monitor, 2));

            configureJavaProject(new SubProgressMonitor(monitor, 3));
            // create the Java project to allow the use of the new source folder
            // page
        } catch (Exception e) {
            throw new CoreException(Status.CANCEL_STATUS);
        } finally {
            monitor.done();
        }

        return result;
    }

    /**
     * Return a boolean indicating whether there will be separate source and
     * binary folders in the project.
     * 
     * @return
     */
    private boolean useSourceAndBinaryFolders() {
        IPreferenceStore store = PreferenceConstants.getPreferenceStore();
        return store.getBoolean(PreferenceConstants.SRCBIN_FOLDERS_IN_NEWPROJ);
    }

    /**
     * Creates the provisional project on which the wizard is working on. The
     * provisional project is typically created when the page is entered the
     * first time. The early project creation is required to configure linked
     * folders.
     * 
     * @return the provisional project
     */
    protected IProject createProvisonalProject() {
        IStatus status = changeToNewProject();
        if ((status != null) && !status.isOK()) {
            ErrorDialog.openError(getShell(), "createProvisonalProject", null, //$NON-NLS-1$
                    status);
        }
        return currProject;
    }

    /**
     * Evaluates the new build path and output folder according to the settings
     * on the first page. The resulting build path is set by calling
     * {@link #init(IJavaProject, IPath, IClasspathEntry[], boolean)}. Clients
     * can override this method.
     * 
     * @param javaProject the new project which is already created when this
     *            method is called.
     * @param monitor the progress monitor
     * @throws CoreException thrown when initializing the build path failed
     */
    protected void initializeBuildPath(IJavaProject javaProject,
            IProgressMonitor monitor) throws CoreException {
        if (monitor == null) {
            monitor = new NullProgressMonitor();
        }
        monitor
                .beginTask(
                        MTJUIMessages.NewMidletProjectWizardPageThree_updateProject_monitor_buildpath_name,
                        2);

        try {
            IClasspathEntry[] entries = null;
            IProject project = javaProject.getProject();

            List<IClasspathEntry> cpEntries = new ArrayList<IClasspathEntry>();
            IWorkspaceRoot root = project.getWorkspace().getRoot();

            IClasspathEntry sourceClasspathEntry = getSrcPathEntry(project
                    .getFullPath());

            if (sourceClasspathEntry.getPath() != project.getFullPath()) {

                IFolder folder = root.getFolder(sourceClasspathEntry.getPath());

                if (!folder.exists()) {
                    folder.create(true, true,
                            new SubProgressMonitor(monitor, 1));
                }
            }
            cpEntries.add(sourceClasspathEntry);
            addResourcesDirectoryIfRequested(cpEntries, project);

            IPath entryPath = new Path(
                    JavaMEClasspathContainer.JAVAME_CONTAINER + "/" //$NON-NLS-1$
                            + firstPage.getSelectedDevice());
            cpEntries.add(JavaCore.newContainerEntry(entryPath));

            if (pageLibrary != null) {
                List<IPath> list = pageLibrary.getSelectedLibraries();

                // Add the JMUnit library, if necessary
                if(propertiesPage.isJMUnitEnabled()) {
                    String libName = "JMUnit for CLDC 1.1"; //$NON-NLS-1$
                    IExternalLibrary jmunitLibrary = ExternalLibraryManager.getInstance().getMidletLibrary(libName);
                    if (jmunitLibrary == null) {
                        String message = NLS.bind(
                                "ExternalLibraryManager does not contain a {0} library.",
                                libName);
                        MTJStatusHandler.throwCoreException(IStatus.ERROR, -999, message);
                    }
                    
                    IPath path = new Path(ExternalLibraryClasspathContainer.EXTERNAL_LIBRARY_CONTAINER_ID + File.separator + libName);
                    list.add(path);
                }
                
                for (IPath path : list) {
                    cpEntries.add(JavaCore.newContainerEntry(path, true));
                }
                    
            }

            entries = cpEntries.toArray(new IClasspathEntry[cpEntries.size()]);

            if (monitor.isCanceled()) {
                throw new OperationCanceledException();
            }

            init(javaProject, getOutputLocation(), entries, true);
        } finally {
            monitor.done();
        }
    }

    /**
     * Removes the provisional project. The provisional project is typically
     * removed when the user cancels the wizard or goes back to the first page.
     */
    protected void removeProvisonalProject() {
        if (!currProject.exists()) {
            currProject = null;
            return;
        }

        IRunnableWithProgress op = new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor)
                    throws InvocationTargetException, InterruptedException {
                doRemoveProject(monitor);
            }
        };

        try {
            getContainer().run(true, true,
                    new WorkspaceModifyDelegatingOperation(op));
        } catch (InvocationTargetException e) {

            final String title = MTJUIMessages.NewMidletProjectWizardPageThree_updateProject_errordialog_title;
            final String message = MTJUIMessages.NewMidletProjectWizardPageThree_updateProject_errordialog_message;
            ExceptionHandler.handle(e, getShell(), title, message);

        } catch (InterruptedException e) {
            // cancel pressed
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jdt.ui.wizards.JavaCapabilityConfigurationPage#useNewSourcePage
     * ()
     */
    @Override
    protected final boolean useNewSourcePage() {
        return true;
    }
}
