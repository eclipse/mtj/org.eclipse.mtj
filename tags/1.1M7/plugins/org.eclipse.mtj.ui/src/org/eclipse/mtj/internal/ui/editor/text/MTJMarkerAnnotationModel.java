/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDEMarkerAnnotationModel
 */
package org.eclipse.mtj.internal.ui.editor.text;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.quickassist.IQuickFixableAnnotation;
import org.eclipse.ui.texteditor.MarkerAnnotation;
import org.eclipse.mtj.internal.ui.editor.text.ResourceMarkerAnnotationModel;

/**
 * @since 0.9.1
 */
public class MTJMarkerAnnotationModel extends ResourceMarkerAnnotationModel {

    /**
     * @since 0.9.1
     */
    class MTJMarkerAnnotation extends MarkerAnnotation implements
            IQuickFixableAnnotation {

        boolean isQuickFixable;
        boolean quickFixableState;

        /**
         * @param marker
         */
        public MTJMarkerAnnotation(IMarker marker) {
            super(marker);
        }

        /* (non-Javadoc)
         * @see org.eclipse.ui.texteditor.MarkerAnnotation#isQuickFixable()
         */
        public boolean isQuickFixable() {
            return isQuickFixable;
        }

        /* (non-Javadoc)
         * @see org.eclipse.ui.texteditor.MarkerAnnotation#isQuickFixableStateSet()
         */
        public boolean isQuickFixableStateSet() {
            return quickFixableState;
        }

        /* (non-Javadoc)
         * @see org.eclipse.ui.texteditor.MarkerAnnotation#setQuickFixable(boolean)
         */
        public void setQuickFixable(boolean state) {
            isQuickFixable = state;
            quickFixableState = true;
        }

    }

    /**
     * @param resource
     */
    public MTJMarkerAnnotationModel(IResource resource) {
        super(resource);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractMarkerAnnotationModel#createMarkerAnnotation(org.eclipse.core.resources.IMarker)
     */
    @Override
    protected MarkerAnnotation createMarkerAnnotation(IMarker marker) {
        return new MTJMarkerAnnotation(marker);
    }
}
