/**
 * Copyright (c) 2009, 2010 Motorola and others
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Fixing antenna export bugs.
 *     Igor Gatis (Google) - [305767] fix for project dependencies
 *
 */
package org.eclipse.mtj.internal.core.build.export.states;

import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.internal.core.build.export.AntennaBuildExport;
import org.eclipse.mtj.internal.core.build.export.AntennaExportException;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.statemachine.StateMachine;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * CreateAntCollectSourcesState collects all sources from the required and
 * target projects in order to compile it.
 *
 * @author David Marques
 * @since 1.0
 */
public class CreateAntCollectSourcesState extends AbstractCreateAntTaskState {

	/**
	 * Creates a {@link CreateAntCollectSourcesState} instance bound to the
	 * specified state machine in order to create collect-sources target for the
	 * specified project.
	 *
	 * @param machine
	 *            bound {@link StateMachine} instance.
	 * @param project
	 *            target {@link IMidletSuiteProject} instance.
	 * @param _document
	 *            target {@link Document}.
	 */
	public CreateAntCollectSourcesState(StateMachine _stateMachine,
			IMidletSuiteProject _suiteProject, Document _document) {
		super(_stateMachine, _suiteProject, _document);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.eclipse.mtj.internal.core.build.export.states.AbstractCreateAntTaskState
	 * #onEnter(org.eclipse.mtj.core.project.runtime.MTJRuntime)
	 */
	protected void onEnter(MTJRuntime runtime) throws AntennaExportException {
		Document document = getDocument();
		Element root = document.getDocumentElement();
		IProject project = getMidletSuiteProject().getProject();

		String configName = getFormatedName(runtime.getName());
		Element preprocess = XMLUtils
				.createTargetElement(
						document,
						root,
						NLS.bind("collect-sources-{0}", configName), getDependencies(runtime)); //$NON-NLS-1$

		Set<IProject> requiredProjects = getRequiredProjects(project);
		for (IProject required : requiredProjects) {
			copyProjectClasses(required, document, preprocess, runtime);
		}
		copyProjectClasses(project, document, preprocess, runtime);

		if (requiredProjects.size() > 0x00) {
			Element copy = document.createElement("copy"); //$NON-NLS-1$
			preprocess.appendChild(copy);
			copy.setAttribute("overwrite", "true"); //$NON-NLS-1$ //$NON-NLS-2$
			copy
					.setAttribute(
							"todir", NLS //$NON-NLS-1$
									.bind(
											"{0}/{1}/{2}/classes/", new String[] { AntennaBuildExport.BUILD_FOLDER, configName, //$NON-NLS-1$
													getFormatedName(project
															.getName()) }));

			for (IProject required : requiredProjects) {
				Element fileset = document.createElement("fileset"); //$NON-NLS-1$
				copy.appendChild(fileset);
				fileset.setAttribute("dir", NLS.bind("{0}/{0}/{1}/classes/", //$NON-NLS-1$ //$NON-NLS-2$
						new String[] { AntennaBuildExport.BUILD_FOLDER, configName,
						getFormatedName(required.getName()) }));
			}
		}
	}

	/**
	 * Creates a task to create the specified project classes.
	 *
	 * @param _project
	 *            target project
	 * @param _document
	 *            target document
	 * @param _root
	 *            document root
	 * @param _runtime
	 *            current runtime
	 * @throws AntennaExportException
	 *             Any error occurs
	 */
	private void copyProjectClasses(IProject _project, Document _document,
			Element _root, MTJRuntime _runtime) throws AntennaExportException {
		IJavaProject javaProject = JavaCore.create(_project);
		if (javaProject == null) {
			return;
		}

		IMTJProject suiteProject = MidletSuiteFactory
				.getMidletSuiteProject(javaProject);
		if (suiteProject == null) {
			return;
		}
		String configName = getFormatedName(_runtime.getName());

		Element copy = _document.createElement("copy"); //$NON-NLS-1$
		_root.appendChild(copy);
		copy.setAttribute("overwrite", "true"); //$NON-NLS-1$ //$NON-NLS-2$
		copy
				.setAttribute(
						"todir", NLS //$NON-NLS-1$
								.bind(
										"{0}/{1}/{2}/classes/", new String[] { AntennaBuildExport.BUILD_FOLDER, configName, //$NON-NLS-1$
												getFormatedName(_project
														.getName()) }));

		IResource[] sources = Utils.getSourceFolders(suiteProject
				.getJavaProject());
		for (IResource source : sources) {
			Element fileset = _document.createElement("fileset"); //$NON-NLS-1$
			fileset.setAttribute("dir", NLS.bind("..{0}", source //$NON-NLS-1$ //$NON-NLS-2$
					.getFullPath()));
			fileset.setAttribute("includes", "**/**.java"); //$NON-NLS-1$ //$NON-NLS-2$
			copy.appendChild(fileset);
		}
	}
}
