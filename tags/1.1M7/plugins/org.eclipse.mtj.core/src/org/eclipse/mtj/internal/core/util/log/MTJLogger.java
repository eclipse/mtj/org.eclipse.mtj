/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.util.log;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.Messages;


/**
 * @author Diego Sandin
 * @since 1.0
 *
 */
public class MTJLogger {

    /**
     * Creates a new instance of MTJLogger.
     */
    public MTJLogger() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Log the specified message.
     * 
     * @param severity
     * @param message
     */
    public static void log(int severity, String message) {
        MTJLogger.log(severity, message, null);
    }

    /**
     * Log the specified message and exception
     * 
     * @param severity
     * @param message
     * @param throwable
     */
    public static void log(int severity, String message, Throwable throwable) {
        if (message == null) {
            message = throwable.getMessage();
        }
        if (message == null) {
            message = Messages.MTJCore_no_message;
        }
    
        MTJCore plugin = MTJCore.getMTJCore();
        String id = IMTJCoreConstants.PLUGIN_ID;
        Status status = new Status(severity, id, IStatus.OK, message, throwable);
        plugin.getLog().log(status);
    }

    /**
     * Log the specified exception.
     * 
     * @param severity
     * @param throwable
     */
    public static void log(int severity, Throwable throwable) {
        log(severity, throwable.getMessage(), throwable);
    }

}
