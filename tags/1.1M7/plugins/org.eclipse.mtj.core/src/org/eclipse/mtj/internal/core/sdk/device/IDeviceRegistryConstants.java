/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.sdk.device;

/**
 * @author Diego Madruga Sandin
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IDeviceRegistryConstants {

    /**
     * The file where the device definitions are stored.
     */
    public static final String DEVICES_FILENAME = "devices.xml"; //$NON-NLS-1$

    /**
     * The root element for the device.xml file.
     */
    public static final String ELEMENT_DEVICEREGISTRY = "deviceRegistry"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String ELEMENT_DEVICECOUNT = "deviceCount"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String ELEMENT_DEVICE = "device"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String ELEMENT_DEVICEPROPERTIES = "deviceProperties"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String ELEMENT_CLASSPATH = "classpath"; //$NON-NLS-1$
    
    /**
     * 
     */
    public static final String ELEMENT_PREVERIFIER = "preverifier"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String ELEMENT_DEFAULTPREVERIFIER = "defaultPreverifier";  //$NON-NLS-1$
    /**
     * 
     */
    public static final String ELEMENT_PARAMETERS = "parameters"; //$NON-NLS-1$
    
    /**
     * 
     */
    public static final String ELEMENT_ENTRY = "entry"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String ATT_VERSION = "version"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String ATT_CLASS = "class"; //$NON-NLS-1$

    /**
     * 
     */
    public static final String ATT_BUNDLE = "bundle"; //$NON-NLS-1$

}
