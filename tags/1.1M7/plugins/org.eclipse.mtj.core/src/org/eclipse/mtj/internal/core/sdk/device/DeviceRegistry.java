/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Storing default preverifier
 *     David Aragao (Motorola)  - Fixing device removal.
 *     Fernando Rocha(Motorola) - Loading the default devices [Bug 261873].
 *     Rafael Amaral (Motorola) - Fixing management of SDK references [Bug 282738].
 *     Jon Dearden  (Research In Motion) - Major changes to allow for SDK providers 
 *                                         and a new device tree interface [Bug 264736].
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceRegistry;
import org.eclipse.mtj.core.sdk.device.IDeviceRegistryListener;
import org.eclipse.mtj.core.sdk.device.IManagedDevice;
import org.eclipse.mtj.internal.core.persistence.XMLPersistenceProvider;
import org.eclipse.mtj.internal.core.util.migration.DeviceRegistryMigration;
import org.eclipse.mtj.internal.core.util.migration.IMigration;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * The device registry provides a place to store and retrieve devices by name.
 * Manually installed (imported) devices are stored in the
 * ImportedDeviceRegistry while devices provided by an ISDKProvider are handled
 * by ManagedDeviceRegistry which is a wrapper around the SDK providers. Both
 * kinds of devices are available through the DeviceRegistry.getDevices() method.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @author Craig Setera
 */
public class DeviceRegistry implements IDeviceRegistry {

    private static IDeviceRegistry instance = null;
    private ManagedDeviceRegistry  managedDeviceRegistry;
    private ImportedDeviceRegistry importedDeviceRegistry;

    /**
     * @return the instance
     */
    public synchronized static IDeviceRegistry getInstance() {
        if (instance == null) {
            instance = new DeviceRegistry();
        }
        return instance;
    }

    // The default device
    private IDevice defaultDevice;

    // The default preverifier
    private IPreverifier defaultPreverifier;
    
    private boolean fireDeviceAddedEvent;

    // A list of registry listeners
    private ArrayList<IDeviceRegistryListener> listenerList;

    protected DeviceRegistry() {
        super();
        managedDeviceRegistry = new ManagedDeviceRegistry();
        importedDeviceRegistry = new ImportedDeviceRegistry();
        listenerList = new ArrayList<IDeviceRegistryListener>();
        try {
            this.load();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#addDevice(org.eclipse.mtj.core.sdk.device.IDevice, boolean)
     */
    public void addDevice(IDevice device) throws IllegalArgumentException,
            PersistenceException {
        // SDK providers store their own devices
        if (device instanceof IManagedDevice)
            return;
        importedDeviceRegistry.addDevice(device);
        if (fireDeviceAddedEvent) {
            fireDeviceAddedEvent(device);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#addRegistryListener(org.eclipse.mtj.core.sdk.device.IDeviceRegistryListener)
     */
    public void addRegistryListener(IDeviceRegistryListener listener) {
        listenerList.add(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#clear()
     */
    public void clear() throws PersistenceException {
        importedDeviceRegistry.clear();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#enableDeviceAddedEvent(boolean)
     */
    public void enableDeviceAddedEvent(boolean fire) {
        fireDeviceAddedEvent = fire;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getAllDevices()
     */
    public List<IDevice> getAllDevices() throws PersistenceException {
        ArrayList<IDevice> allDevices 
            = new ArrayList<IDevice>(managedDeviceRegistry.getAllDevices());
        allDevices.addAll(importedDeviceRegistry.getAllDevices());
        return allDevices;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getDefaultDevice()
     */
    public IDevice getDefaultDevice() {
        return defaultDevice;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getDefaultPreferifier()
     */
    public IPreverifier getDefaultPreferifier() {
        return defaultPreverifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getDevice(java.lang.String, java.lang.String)
     */
    public IDevice getDevice(String groupName, String deviceName)
            throws PersistenceException {
        IDevice device = managedDeviceRegistry.getDevice(groupName, deviceName);
        if (device == null) {
            device = importedDeviceRegistry.getDevice(groupName, deviceName);
        }
        return device;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getDeviceCount()
     */
    public int getDeviceCount() throws PersistenceException {
        return managedDeviceRegistry.getDeviceCount() + importedDeviceRegistry.getDeviceCount();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getDevices(java.lang.String)
     */
    public List<IDevice> getDevices(String groupName)
            throws PersistenceException {
        List<IDevice> devices = managedDeviceRegistry.getDevices(groupName);
        if (devices == null) {
            devices = importedDeviceRegistry.getDevices(groupName);
        }
        return devices;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#getSDKNames()
     */
    public List<String> getSDKNames() throws PersistenceException {
        ArrayList<String> sdksNames 
            = new ArrayList<String>(managedDeviceRegistry.getSDKNames());
        sdksNames.addAll(importedDeviceRegistry.getSDKNames());
        return sdksNames;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#isDeviceAddedEventEnabled()
     */
    public boolean isDeviceAddedEventEnabled() {
        return fireDeviceAddedEvent;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#load()
     */
    public void load() throws PersistenceException {
        load(false);
    }
    
    /**
     * Similar to load()
     * 
     * @param loadOnlyValidDevices - false to load all devices, true to load
     *            only valid devices
     * @throws PersistenceException
     */
    private void load(boolean loadOnlyValidDevices) throws PersistenceException {
        XMLPersistenceProvider persistenceProvider = createPersistenceProvider();
        if (persistenceProvider != null) {
            loadUsing(persistenceProvider);
            importedDeviceRegistry.loadUsing(persistenceProvider, loadOnlyValidDevices);
        }
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        String defaultDeviceSdkName;
        String defaultDeviceName;
        try {
            enableDeviceAddedEvent(false);
            defaultDeviceSdkName = persistenceProvider.loadString("defaultDeviceSdkName"); //$NON-NLS-1$
            defaultDeviceName = persistenceProvider.loadString("defaultDevicename"); //$NON-NLS-1$
            defaultPreverifier = (IPreverifier) persistenceProvider
                    .loadPersistable("defaultPreverifier"); //$NON-NLS-1$
        } finally {
            enableDeviceAddedEvent(true);
        }
        defaultDevice = getDevice(defaultDeviceSdkName, defaultDeviceName);
    }
    
    /**
     * Updates the device's persistence file with only valid devices
     * 
     * @throws PersistenceException
     * @throws TransformerException
     * @throws IOException
     */
    public void updateReferences() throws PersistenceException,
            TransformerException, IOException {
        load(true);
        store();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#removeDevice(org.eclipse.mtj.core.sdk.device.IDevice)
     */
    public synchronized void removeDevice(IDevice device)
            throws PersistenceException {
        // Only imported devices may be removed
        if (!(device instanceof IManagedDevice)) {
            importedDeviceRegistry.removeDevice(device);
            fireDeviceRemovedEvent(device);
            // Clear the default device if it is being removed
            if (device.equals(defaultDevice)) {
                defaultDevice = null;
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#removeRegistryListener
     * (org.eclipse.mtj.core.sdk.device.IDeviceRegistryListener)
     */
    public void removeRegistryListener(IDeviceRegistryListener listener) {
        listenerList.remove(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#setDefaultDevice(org.eclipse.mtj.core.sdk.device.IDevice)
     */
    public void setDefaultDevice(IDevice device) {
        defaultDevice = device;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#setDefaultPreverifer(org.eclipse.mtj.core.build.preverifier.IPreverifier)
     */
    public void setDefaultPreverifer(IPreverifier preverifier) {
        defaultPreverifier = preverifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#store()
     */
    public void store() throws PersistenceException, TransformerException,
            IOException {
        XMLPersistenceProvider provider = new XMLPersistenceProvider(
                "deviceRegistry"); //$NON-NLS-1$
        storeUsing(provider);
        XMLUtils.writeDocument(getComponentStoreFile(), provider.getDocument());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceRegistry#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        if (defaultPreverifier != null) {
            persistenceProvider.storePersistable("defaultPreverifier", //$NON-NLS-1$
                    defaultPreverifier);
        }
        if (defaultDevice != null) {
            String defaultDeviceSdkName = defaultDevice.getSDKName();
            String defaultDeviceName = defaultDevice.getName();
            if (defaultDeviceSdkName != null && defaultDeviceName != null) {
                persistenceProvider.storeString(
                        "defaultDeviceSdkName", defaultDeviceSdkName); //$NON-NLS-1$
                persistenceProvider.storeString(
                        "defaultDevicename", defaultDeviceName); //$NON-NLS-1$
            }
        }
        importedDeviceRegistry.storeUsing(persistenceProvider);
    }

    /**
     * Fire an event that indicates a device has been added.
     * 
     * @param device
     */
    private void fireDeviceAddedEvent(IDevice device) {
        Iterator<IDeviceRegistryListener> listeners = listenerList.iterator();
        while (listeners.hasNext()) {
            IDeviceRegistryListener listener = listeners.next();
            listener.deviceAdded(device);
        }
    }

    /**
     * Fire an event that indicates a device has been removed.
     * 
     * @param device
     */
    private void fireDeviceRemovedEvent(IDevice device) {
        Iterator<IDeviceRegistryListener> listeners = listenerList.iterator();
        while (listeners.hasNext()) {
            IDeviceRegistryListener listener = listeners.next();
            listener.deviceRemoved(device);
        }
    }

    /**
     * Get the file for the device store file.
     * 
     * @return
     */
    static File getComponentStoreFile() {
        IPath pluginStatePath = MTJCore.getMTJCore().getStateLocation();
        IPath storePath = pluginStatePath
                .append(IDeviceRegistryConstants.DEVICES_FILENAME);
        return storePath.toFile();
    }
    
    /**
     * Return a list of names of SDKs containing invalid devices.  In general,
     * this is due to SDKs which have been removed from the file system.
     * 
     * @return list of SDK names which have invalid devices.
     */
    public List<String> getInvalidSDKNames() {
        return importedDeviceRegistry.getInvalidSDKNames();
    }
    
    /**
     * Create an XMLPersistenceProvider from the ComponentStoreFile. Return null
     * if the ComponentStoreFile (devices.xml) does not yet exist.
     */
    static XMLPersistenceProvider createPersistenceProvider() throws PersistenceException {
        
        File storeFile = getComponentStoreFile();
        XMLPersistenceProvider persistenceProvider = null;
        
        if (storeFile.exists()) {
            try {
                Document document = XMLUtils.readDocument(storeFile);

                if (document == null) {
                    return null;
                }

                Element rootXmlElement = document.getDocumentElement();
                if (!rootXmlElement.getNodeName().equals(
                        IDeviceRegistryConstants.ELEMENT_DEVICEREGISTRY)) {
                    return null;
                }

                IMigration migration = new DeviceRegistryMigration();

                migration.migrate(document);

                if (migration.isMigrated()) {

                    try {
                        XMLUtils.writeDocument(storeFile, document);
                    } catch (TransformerException e) {
                        e.printStackTrace();
                    }

                    document = XMLUtils.readDocument(storeFile);
                }

                persistenceProvider =  new XMLPersistenceProvider(document);

            } catch (ParserConfigurationException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (SAXException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (IOException e) {
                throw new PersistenceException(e.getMessage(), e);
            }
        }
        
        return persistenceProvider;
    }
    
}
