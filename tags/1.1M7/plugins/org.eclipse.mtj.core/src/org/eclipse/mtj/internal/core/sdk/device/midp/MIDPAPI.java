/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.sdk.device.midp;

import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Version;

/**
 * @author Diego Sandin
 * @since 1.0
 */
public class MIDPAPI implements IMIDPAPI {

    /**
     * Persistence entry for storing version information for this {@link IAPI}
     * instance.
     */
    private static final String VERSION_ENTRY = "version"; //$NON-NLS-1$

    /**
     * Persistence entry for storing the type of class to be used as binding for
     * all MIDPAPI information.
     */
    private static final String TYPE_ENTRY = "type"; //$NON-NLS-1$

    /**
     * Persistence entry for storing the name of this {@link IAPI} instance.
     */
    private static final String NAME_ENTRY = "name"; //$NON-NLS-1$

    /**
     * Persistence entry for storing the identifier for this {@link IAPI}
     * instance.
     */
    private static final String IDENTIFIER_ENTRY = "identifier"; //$NON-NLS-1$

    /**
     * The identifier for this {@link IAPI} instance.
     */
    private String identifier;

    /**
     * The name to be used to represent this {@link IAPI} instance.
     */
    private String name;

    private MIDPAPIType type;
    private Version version;

    /**
     * Creates a new instance of MIDPAPI.
     */
    public MIDPAPI() {
        type = MIDPAPIType.UNKNOWN;
        identifier = MIDPAPIType.UNKNOWN.toString();
        name = MIDPAPIType.UNKNOWN.toString();
        version = Version.emptyVersion;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI#getType()
     */
    public MIDPAPIType getType() {
        return type;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#getIdentifier()
     */
    public String getIdentifier() {
        return identifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#getVersion()
     */
    public Version getVersion() {
        return version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#setIdentifier(java.lang.String)
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#setName(java.lang.String)
     */
    public void setName(String name) {
        this.name = name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI#setType(org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType)
     */
    public void setType(MIDPAPIType type) {
        this.type = type;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#setVersion(org.osgi.framework.Version)
     */
    public void setVersion(Version version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        identifier = persistenceProvider.loadString(IDENTIFIER_ENTRY);
        version = new Version(persistenceProvider.loadString(VERSION_ENTRY));
        name = persistenceProvider.loadString(NAME_ENTRY);
        int typeCode = persistenceProvider.loadInteger(TYPE_ENTRY);
        type = MIDPAPIType.getMIDPAPITypeByCode(typeCode);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        persistenceProvider.storeString(IDENTIFIER_ENTRY, identifier);
        persistenceProvider.storeString(NAME_ENTRY, name);
        persistenceProvider.storeInteger(TYPE_ENTRY, type.getTypecode());
        persistenceProvider.storeString(VERSION_ENTRY, NLS.bind(
                IMTJCoreConstants.VERSION_NLS_BIND_TEMPLATE, new Object[] {
                        version.getMajor(), version.getMinor() }));
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getIdentifier() + "-" + getVersion().getMajor() + "."
                + getVersion().getMinor();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((identifier == null) ? 0 : identifier.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MIDPAPI other = (MIDPAPI) obj;
        if (identifier == null) {
            if (other.identifier != null) {
                return false;
            }
        } else if (!identifier.equals(other.identifier)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (type == null) {
            if (other.type != null) {
                return false;
            }
        } else if (!type.equals(other.type)) {
            return false;
        }
        if (version == null) {
            if (other.version != null) {
                return false;
            }
        } else if (!version.equals(other.version)) {
            return false;
        }
        return true;
    }

}
