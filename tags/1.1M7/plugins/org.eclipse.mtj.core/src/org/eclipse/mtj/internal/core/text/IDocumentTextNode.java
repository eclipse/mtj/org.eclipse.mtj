/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

import java.io.Serializable;

/**
 * @since 0.9.1
 */
public interface IDocumentTextNode extends IDocumentRange, Serializable,
        IDocumentXMLNode {

    public static final String F_PROPERTY_CHANGE_TYPE_PCDATA = "type_pcdata"; //$NON-NLS-1$

    /**
     * Not used by text edit operations
     * 
     * @param parent
     */
    public void reconnect(IDocumentElementNode parent);

    /**
     * Not used by text edit operations
     * 
     * @return
     */
    public String write();

    /**
     * @return
     */
    IDocumentElementNode getEnclosingElement();

    /**
     * @return
     */
    String getText();

    /**
     * Used by text edit operations
     * 
     * @param node
     */
    void setEnclosingElement(IDocumentElementNode node);

    /**
     * @param length
     */
    void setLength(int length);

    /**
     * @param offset
     */
    void setOffset(int offset);

    /**
     * @param text
     */
    void setText(String text);

}
