/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Using L10n Models.
 *     David Marques (Motorola) - Fixing cleaning.
 *     David Marques (Motorola) - Removing all .properties from project
 *                                upon cleaning project.    
 *     David Marques (Motorola) - Generating keys on upper case.
 *     David Marques (Motorola) - Extending MTJIncrementalProjectBuilder  
 *     David Marques(Motorola)  - Refactoring Builders
 *     David Marques(Motorola)  - Including l10n properties into build.properties                                               
 */
package org.eclipse.mtj.internal.core.l10n;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.build.MTJBuildState;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.MTJBuildProperties;
import org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.l10n.L10nEntry;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.osgi.util.NLS;

/**
 * L10nBuilder Class builds localization properties files from a Localization
 * Data file found in the project root folder.
 * 
 * @author David Marques
 * @since 0.9.1
 */
public class L10nBuilder extends MTJIncrementalProjectBuilder {

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#doBuild(int, java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
     */
    @SuppressWarnings("unchecked")
    protected IProject[] doBuild(int kind, Map args, IProgressMonitor monitor)
            throws CoreException {

        IProject project = this.getProject();
        try {
            IResourceDelta delta = this.getDelta(project);
            if (delta != null) {
                if (hasLocalizationDataChanged(project, delta)) {
                    doBuild(project, monitor);
                }
            } else {
                doBuild(project, monitor);
            }
        } catch (FileNotFoundException e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, 0x00, e);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#doClean(int, org.eclipse.core.runtime.IProgressMonitor)
     */
    protected void doClean(int kind, IProgressMonitor monitor) throws CoreException {
        monitor.subTask(Messages.L10nBuilder_clean_CleaningOldProperties);

        IJavaProject javaProject = JavaCore.create(getProject());
        IPackageFragmentRoot[] roots = javaProject.getPackageFragmentRoots();
        for (IPackageFragmentRoot root : roots) {
            if (root.getKind() != IPackageFragmentRoot.K_SOURCE) {
                continue;
            }

            root.getCorrespondingResource().accept(new IResourceVisitor() {

                /* (non-Javadoc)
                 * @see org.eclipse.core.resources.IResourceVisitor#visit(org.eclipse.core.resources.IResource)
                 */
                public boolean visit(IResource resource) throws CoreException {
                    String name = resource.getName();
                    if (name.startsWith("messages") //$NON-NLS-1$
                            && name.endsWith(".properties")) { //$NON-NLS-1$
                        resource.delete(true, new NullProgressMonitor());
                    }
                    return true;
                }
            });
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#getBuilderId()
     */
    protected String getBuilderId() {
    	return IMTJCoreConstants.L10N_BUILDER_ID;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#getEnterState()
     */
    protected MTJBuildState getEnterState() {
    	return MTJBuildState.PRE_LOCALIZATION;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#getExitState()
     */
    protected MTJBuildState getExitState() {
    	return MTJBuildState.POST_LOCALIZATION;
    }
    
    /**
     * Checks if the localization data file has changed.
     * 
     * @param project mtj project instance.
     * @param delta project delta.
     * @return true if has changed false otherwise.
     * @throws FileNotFoundException if the xml file does not exist.
     */
    private boolean hasLocalizationDataChanged(IProject project,
            IResourceDelta delta) throws FileNotFoundException {
        boolean result = false;

        IFile locationsXml = project.getFile(L10nApi.LOCALIZATION_DATA_FILE);
        if (locationsXml.exists()) {
            IResourceDelta xmlDelta = delta.findMember(locationsXml
                    .getProjectRelativePath());
            if (xmlDelta != null) {
                switch (xmlDelta.getKind()) {
                    case IResourceDelta.CHANGED:
                    case IResourceDelta.REMOVED:
                    case IResourceDelta.ADDED:
                        result = true;
                        break;
                }
            }
        } else {
            throw new FileNotFoundException(
                    Messages.L10nBuilder_LocalizationDataDoesNotExist);
        }
        return result;
    }

    /**
     * Builds the specified project.
     * 
     * @param project mtj project.
     * @param monitor progress monitor.
     * @throws CoreException any core error occurs.
     */
    private void doBuild(IProject project, IProgressMonitor monitor)
            throws CoreException {
        try {
            L10nModel model = L10nApi.loadL10nModel(project);
            if (model.isValid()) {
                monitor.subTask(Messages.L10nBuilder_BuildingLocalizationData);
                L10nLocales locales = model.getLocales();
                IFolder propertiesFolder = project.getFolder(locales.getDestination());
                if (!propertiesFolder.exists()) {
                    propertiesFolder.create(true, true, monitor);
                }
                monitor.subTask(Messages.L10nBuilder_generatingProperties);
                this.createProperties(project, locales, propertiesFolder,
                        monitor);

                IJavaProject javaProject = JavaCore.create(this.getProject());
                IPackageFragment[] packages = javaProject.getPackageFragments();
                for (int i = 0; i < packages.length; i++) {
                    if (packages[i].getElementName().equals(
                            locales.getPackage())) {
                    	monitor.subTask(Messages.L10nBuilder_updatingConstants);
                        String code = L10nApi.buildL10nConstantsClass(project,
                                packages[i]);

                        packages[i].createCompilationUnit(
                                L10nApi.L10N_CONSTANTS_CLASS, code, true,
                                monitor);
                        break;
                    }
                }

                IFolder folder = project.getFolder(new Path(locales
                        .getDestination()));
                if (folder.exists()) {
                    folder.refreshLocal(IResource.DEPTH_ONE, monitor);
                }
            } else {
                MTJLogger.log(IStatus.ERROR, Messages.L10nBuilder_InvalidModel);
            }
        } catch (Exception e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, 0x00, e);
        }
    }

    /**
     * Creates the localization properties files.
     * 
     * @param project current project.
     * @param locales supported locales
     * @param propertiesFolder target folder.
     * @param monitor active monitor
     * @throws IOException if any IO error occurs.
     */
    private void createProperties(IProject project, L10nLocales locales,
            IFolder propertiesFolder, IProgressMonitor monitor) throws IOException {

        monitor.subTask(Messages.L10nBuilder_ProcessingLocalizationData);
        List<IDocumentElementNode> nodes = locales.getChildren();
        for (IDocumentElementNode localeElement : nodes) {
            L10nLocale locale = (L10nLocale) localeElement;
            IFile file = writePropertyFile(locale, propertiesFolder, monitor);
            IMTJProject suiteProject = MidletSuiteFactory.getMidletSuiteProject(project.getName());
            MTJBuildProperties  properties   = MTJBuildProperties.getBuildProperties(suiteProject);
            for (MTJRuntime runtime : suiteProject.getRuntimeList()) {                
                properties.includeResource(file, runtime);
            }
        }
    }

    /**
     * Creates a properties file for the specified locale.
     * 
     * @param locale desired locale.
     * @param propertiesFolder target folder.
     * @param monitor active monitor.
     * @throws IOException if any IO error occurs.
     * 
     * @return the new properties file
     */
    private IFile writePropertyFile(L10nLocale locale, IFolder propertiesFolder,
            IProgressMonitor monitor) throws IOException {
        monitor
                .subTask(NLS
                        .bind(
                                "Writing Localization Data for locale {0}", locale.getLocaleName())); //$NON-NLS-1$

        if (!propertiesFolder.exists()) {
            throw new FileNotFoundException(
                    Messages.L10nBuilder_PackageFolderNotFound);
        }

        ByteArrayInputStream stream = null;
        IFile  file = null;
        String name = null;
        if (locale.getLocaleName() != null) {
            name = NLS.bind("messages_{0}.properties", locale.getLocaleName()); //$NON-NLS-1$
        } else {
            name = "messages.properties"; //$NON-NLS-1$
        }

        try {
            StringBuffer buffer = new StringBuffer();

            IDocumentElementNode[] nodes = locale.getChildNodes();
            for (int i = 0; i < nodes.length; i++) {
                L10nEntry entry = (L10nEntry) nodes[i];
                buffer.append(entry.getKey().toUpperCase());
                buffer.append("="); //$NON-NLS-1$
                buffer.append(entry.getValue());
                buffer.append("\n"); //$NON-NLS-1$
            }
            stream = new ByteArrayInputStream(buffer.toString().getBytes());
            file   = propertiesFolder.getFile(name);
            if (file.exists()) {
                file.setContents(stream, IResource.FORCE, monitor);
            } else {
                file.create(stream, true, monitor);
            }
        } catch (Exception e) {
            throw new IOException(NLS.bind(
                    Messages.L10nBuilder_ErrorWritingProperty, locale
                            .getLocaleName())); //$NON-NLS-2$
        } finally {
            if (stream != null)
                stream.close();
        }
        return file;
    }
}
