/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Diego Sandin (Motorola) - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.sdk.device;

import java.io.IOException;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * The device registry is the main entry point that is used to manage the
 * devices that are currently supported on a MTJ installation.
 * 
 * <p>
 * When a SDK is imported it will return a list of devices that will be added to
 * the registry in order to have them available to MTJ clients.
 * </p>
 * <p>
 * Clients may access the IDeviceRegistry implementation through the
 * {@link MTJCore#getDeviceRegistry()} method.
 * </p>
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * 
 * @see IDevice
 * @see IPreverifier
 * 
 * @since 1.0
 */
public interface IDeviceRegistry extends IPersistable {

    /**
     * Add a new device instance to the device registry.
     * 
     * @param device the device instance to be added to the device registry.
     * @throws IllegalArgumentException if the device is not well formed.
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load.
     */
    public abstract void addDevice(IDevice device)
            throws IllegalArgumentException, PersistenceException;

    /**
     * Adds the listener to the collection of listeners who will be notified
     * when the registry state changes. The listener is notified by invoking one
     * of methods defined in the <code>IDeviceRegistryListener</code> interface.
     * 
     * @param listener the listener that should be notified when the registry
     *            state changes.
     */
    public abstract void addRegistryListener(IDeviceRegistryListener listener);

    /**
     * Clear the registry of all entries.
     * 
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    public abstract void clear() throws PersistenceException;

    /**
     * Enable the DeviceAdded Event to be fired to all
     * {@link IDeviceRegistryListener} instances registered using
     * {@link #addRegistryListener(IDeviceRegistryListener)} when the
     * {@link #addDevice(IDevice)} is invoked.
     * <p>
     * By default, this event is enabled. Use the
     * {@link #isDeviceAddedEventEnabled()} to get current event firing status.
     * </p>
     * 
     * @param fire flag indicating if the DeviceAdded Event should be fired or
     *            not.
     */
    public abstract void enableDeviceAddedEvent(boolean fire);

    /**
     * Return the list of all of the devices in the registry.
     * 
     * @return the list of all of the devices in the registry or an empty list
     *         if no device is registered.
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    public abstract List<IDevice> getAllDevices() throws PersistenceException;

    /**
     * Return the device set as the default.
     * 
     * @return the device set as the default or <code>null</code> if none was
     *         not specified.
     */
    public abstract IDevice getDefaultDevice();

    /**
     * Return the default preverifier to be used if no preverifier was available
     * in a given device.
     * 
     * @return the default preverifier or <code>null</code> if none was
     *         specified.
     */
    public abstract IPreverifier getDefaultPreferifier();

    /**
     * Return the device from a given SDK with the specified name.
     * 
     * @param sdkName the SDK name.
     * @param deviceName the device name.
     * @return the device from a given SDK with the specified name or
     *         <code>null</code> if no such device is found in the registry.
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load.
     */
    public abstract IDevice getDevice(String sdkName, String deviceName)
            throws PersistenceException;

    /**
     * Return the number of the registered devices.
     * 
     * @return the number of the registered devices.
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load.
     */
    public abstract int getDeviceCount() throws PersistenceException;

    /**
     * Return the list of identifiers of all registered SDKs.
     * 
     * @return the list of identifiers of all registered SDKs.
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load.
     */
    public abstract List<String> getSDKNames() throws PersistenceException;

    /**
     * Return the list of devices associated to the given SDK name.
     * 
     * @param sdkName the SDK name.
     * @return the list of devices associated to the given SDK name or
     *         <code>null</code> if the specified group cannot be found.
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load.
     */
    public abstract List<IDevice> getDevices(String sdkName)
            throws PersistenceException;

    /**
     * Checks if the DeviceAdded Event will be fired to all
     * {@link IDeviceRegistryListener} instances registered using
     * {@link #addRegistryListener(IDeviceRegistryListener)} or not when
     * invoking {@link #addDevice(IDevice)}.
     * 
     * @return <code>true</code> is DeviceAdded Event will be fired when
     *         {@link #addDevice(IDevice)} is invoked, <code>false</code>
     *         otherwise.
     */
    public abstract boolean isDeviceAddedEventEnabled();

    /**
     * Load the contents of the device registry from the storage file in the
     * plug-in state location.
     * 
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load.
     */
    public abstract void load() throws PersistenceException;

    /**
     * Remove the specified device from the registry if it exists.
     * 
     * @param device the device to be removed from the registry.
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    public abstract void removeDevice(IDevice device)
            throws PersistenceException;

    /**
     * Removes the listener from the collection of listeners who will be
     * notified when the registry state changes.
     * 
     * @param listener the listener that should no longer be notified when the
     *            registry state changes.
     */
    public abstract void removeRegistryListener(IDeviceRegistryListener listener);

    /**
     * Set the default device.
     * 
     * @param device the device to be set as default.
     */
    public abstract void setDefaultDevice(IDevice device);

    /**
     * Set the default preverifier.
     * 
     * @param preverifier the default preverifier.
     */
    public abstract void setDefaultPreverifer(IPreverifier preverifier);

    /**
     * Store out the contents of the registry into the standard device storage
     * file in the plug-in state location.
     * 
     * @throws PersistenceException if any error occur while saving the
     *             persistable information.
     * @throws TransformerException if any error occur while saving the
     *             persistable information.
     * @throws IOException if any error occur while saving the persistable
     *             information.
     */
    public abstract void store() throws PersistenceException,
            TransformerException, IOException;
}