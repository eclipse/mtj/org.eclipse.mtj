/*******************************************************************************
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nokia Corporation - initial implementation 
 *******************************************************************************/
package org.eclipse.mtj.eswt.internal.templates;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
/**
 * HelloWorld template for a MIDlet using eSWT. 
 * @author gercan
 *
 */
public class HelloWorldTemplatePage extends AbstractTemplateWizardPage {

	private Text messageTxt;

	public Map<String, String> getDictionary() {
        Map<String, String> dictionary = new HashMap<String, String>();
        dictionary.put("$message$",  messageTxt.getText()); //$NON-NLS-1$
        return dictionary;
	}

	public boolean isPageComplete() {
		
		return (messageTxt != null && 
		messageTxt.getText()!= null && 
		messageTxt.getText().length() > 0);
	}
	
	@Override
	public void createControl(Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		Composite composite = new Composite(parent,SWT.NONE);
		composite.setLayout(new GridLayout(2,false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		Label messageLbl = new Label(composite, SWT.NONE);
		messageLbl.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false,
				false));
		messageLbl.setText(TemplateMessages.HelloWorldTemplateMessage);
		
		messageTxt = new Text(composite, SWT.SINGLE | SWT.LEAD | SWT.BORDER);
		messageTxt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		messageTxt.setText(TemplateMessages.HelloWorldTemplateMessageHelloWorld);	
	}

}
