/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.jmunit.core.api;

import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.IClassFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IRegion;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.internal.jmunit.JMUnitMessages;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class JMUnitTestFinder implements ITestFinder {

    private String fSuperclass;

    /**
     * 
     */
    public JMUnitTestFinder(String superclass) {
        fSuperclass = superclass;
    }

    /**
     * @param typeHierarchy
     * @param testInterface
     * @param region
     * @param result
     * @throws JavaModelException
     */
    public void findTestImplementorClasses(ITypeHierarchy typeHierarchy,
            IType testInterface, IRegion region, Set<IType> result)
            throws JavaModelException {
        IType[] subtypes = typeHierarchy.getAllSubtypes(testInterface);
        for (IType type : subtypes) {
            int cachedFlags = typeHierarchy.getCachedFlags(type);
            if (!Flags.isInterface(cachedFlags)
                    && !Flags.isAbstract(cachedFlags) // do the cheaper tests
                    // first
                    && region.contains(type) && isAccessibleClass(type)) {
                result.add(type);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.jmunit.api.ITestFinder#findTestsInContainer(org.eclipse.jdt.core.IJavaElement, java.util.Set, org.eclipse.core.runtime.IProgressMonitor)
     */
    public void findTestsInContainer(IJavaElement element, Set<IType> result,
            IProgressMonitor pm) throws CoreException {

        if ((element == null) || (result == null)) {
            throw new IllegalArgumentException();
        }

        if (pm == null) {
            pm = new NullProgressMonitor();
        }

        pm.beginTask(JMUnitMessages.JMUnitTestFinder_findTestsInContainer_taskName, 10);

        try {
            if (element instanceof IType) {
                if (isTest((IType) element)) {
                    result.add((IType) element);
                }
            } else if (element instanceof ICompilationUnit) {
                IType[] types = ((ICompilationUnit) element).getAllTypes();
                for (IType type : types) {
                    if (isTest(type)) {
                        result.add(type);
                    }
                }
            } else {
                findTestCases(element, result, new SubProgressMonitor(pm, 7));
                if (pm.isCanceled()) {
                    return;
                }
            }
            if (pm.isCanceled()) {
                return;
            }
        } finally {
            pm.done();
        }

    }

    /**
     * @param element
     * @return
     * @throws JavaModelException
     */
    public IRegion getRegion(IJavaElement element) throws JavaModelException {
        IRegion result = JavaCore.newRegion();
        if (element.getElementType() == IJavaElement.JAVA_PROJECT) {
            // for projects only add the contained source folders
            IPackageFragmentRoot[] roots = ((IJavaProject) element)
                    .getPackageFragmentRoots();
            for (int i = 0; i < roots.length; i++) {
                if (!roots[i].isArchive()) {
                    result.add(roots[i]);
                }
            }
        } else {
            result.add(element);
        }
        return result;
    }

    /**
     * @param type
     * @return
     * @throws JavaModelException
     */
    public boolean isAccessibleClass(IType type) throws JavaModelException {
        int flags = type.getFlags();
        if (Flags.isInterface(flags)) {
            return false;
        }
        IJavaElement parent = type.getParent();
        while (true) {
            if ((parent instanceof ICompilationUnit)
                    || (parent instanceof IClassFile)) {
                return true;
            }
            if (!(parent instanceof IType) || !Flags.isStatic(flags)
                    || !Flags.isPublic(flags)) {
                return false;
            }
            flags = ((IType) parent).getFlags();
            parent = parent.getParent();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.jmunit.api.ITestFinder#isTest(org.eclipse.jdt.core.IType)
     */
    public boolean isTest(IType type) throws CoreException {

        return isAccessibleClass(type)
                && (!Flags.isAbstract(type.getFlags()) && isTestImplementor(type));
    }

    /**
     * @param type
     * @return
     * @throws JavaModelException
     */
    public boolean isTestImplementor(IType type) throws JavaModelException {
        ITypeHierarchy typeHier = type.newSupertypeHierarchy(null);
        IType[] superClasses = typeHier.getAllClasses();
        for (IType superInterface : superClasses) {
            if (fSuperclass.equals(superInterface.getFullyQualifiedName('.'))) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param element
     * @param result
     * @param pm
     * @throws JavaModelException
     */
    private void findTestCases(IJavaElement element, Set<IType> result,
            IProgressMonitor pm) throws JavaModelException {
        IJavaProject javaProject = element.getJavaProject();

        IType testCaseType = javaProject.findType(fSuperclass);
        if (testCaseType == null) {
            return;
        }

        IRegion region = getRegion(element);
        ITypeHierarchy typeHierarchy = javaProject.newTypeHierarchy(
                testCaseType, region, pm);
        findTestImplementorClasses(typeHierarchy, testCaseType, region, result);
    }
}
