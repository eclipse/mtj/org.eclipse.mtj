/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.nature;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.IMTJCoreConstants;

/**
 * Project nature for J2ME projects.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 * @see IProjectNature
 */
public class J2MENature extends AbstractNature {
    
    /**
     * Return a boolean indicating whether the specified project has the J2ME
     * nature associated with it.
     * 
     * @param project the project to be tested for the J2ME nature
     * @return a boolean indicating whether the specified project has the J2ME
     *         nature
     * @throws CoreException
     */
    public static boolean hasJ2MENature(IProject project) throws CoreException {
        return project.hasNature(IMTJCoreConstants.J2ME_NATURE_ID);
    }

    /**
     * @see IProjectNature#configure
     */
    public void configure() throws CoreException {
        BuildSpecManipulator manipulator = new BuildSpecManipulator(
                getProject());
        manipulator.addBuilderAfter(JavaCore.BUILDER_ID,
                IMTJCoreConstants.J2ME_PREVERIFIER_ID, null);
        manipulator.commitChanges(new NullProgressMonitor());
    }
}
