/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola)  - Initial implementation
 *                                
 */
package org.eclipse.mtj.core.model.preverifier;

/**
 * Exception used when there is a need to preverify and there no valid
 * preverifier
 * 
 * @author Hugo Raniere
 */
public class PreverifierNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Exception constructor
	 * 
	 * @param message
	 */
	public PreverifierNotFoundException(String message) {
		super(message);
	}

}
