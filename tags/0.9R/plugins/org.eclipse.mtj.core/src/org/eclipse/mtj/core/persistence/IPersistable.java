/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.persistence;

/**
 * Instances of this interface are capable of saving and restoring their state.
 * 
 * @author Craig Setera
 */
public interface IPersistable {

    /**
     * Load the state of this object using the specified persistence state
     * information.
     * 
     * @param persistenceProvider
     * @throws PersistenceException
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException;

    /**
     * Save the state of this object using the specified persistence state
     * information.
     * 
     * @param persistenceProvider
     * @throws PersistenceException
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException;
}
