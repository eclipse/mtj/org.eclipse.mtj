/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Reimplementing JavaMEClasspathContainer
 *     Diego Sandin (Motorola)  - Add workaround while embedded preverifier 
 *     							  is not available:
 *                                isEmbeddedPreverifierEnabled will always 
 *                                return false
 *     Hugo Raniere (Motorola)  - Handling the case that there is no valid preverifier
 *     Hugo Raniere (Motorola)  - Using default preverifier if device does not specify one.
 */
package org.eclipse.mtj.core.model.project.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.JavaMEClasspathContainer;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.preverification.builder.PreverificationBuilder;
import org.eclipse.mtj.core.internal.preverifier.EmbeddedPreverifier;
import org.eclipse.mtj.core.internal.utils.FilteringClasspathEntryVisitor;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.jad.ApplicationDescriptor;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSetRegistry;
import org.eclipse.mtj.core.model.preverifier.IPreverifier;
import org.eclipse.mtj.core.model.preverifier.PreverifierNotFoundException;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.IMidletSuiteProjectListener;
import org.eclipse.mtj.core.model.project.Messages;
import org.eclipse.mtj.core.model.project.MetaData;
import org.eclipse.mtj.core.model.sign.ISignatureProperties;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.preverifier.results.PreverificationError;

/**
 * Implementation of the IMidletSuiteProject interface providing access to
 * MIDlet suite specific information.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */
public class MidletSuiteProject implements IMidletSuiteProject {

    /**
     * Classpath entry visitor implementation for collecting up the classpath.
     */
    private static class ClasspathCollectionVisitor extends
            FilteringClasspathEntryVisitor {

        private IPath classesPath;
        private Set<IPath> cpEntries;
        private IPath libsPath;

        /**
         * Constructor
         * 
         * @param classesPath
         * @param libsPath
         */
        private ClasspathCollectionVisitor(IPath classesPath, IPath libsPath) {
            this.classesPath = classesPath;
            this.libsPath = libsPath;
            cpEntries = new LinkedHashSet<IPath>();
        }

        /**
         * @return Returns the cpEntries.
         */
        public Set<IPath> getCpEntries() {
            return cpEntries;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.internal.utils.AbstractClasspathEntryVisitor#visitLibraryEntry(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IProgressMonitor)
         */
        @Override
        public void visitLibraryEntry(IClasspathEntry entry,
                IJavaProject javaProject, IProgressMonitor monitor)
                throws CoreException {
            File resolvedEntry = Utils.getResolvedClasspathEntryFile(entry);
            if (resolvedEntry.isFile()) {
                IPath libPath = libsPath.append(entry.getPath().lastSegment());
                cpEntries.add(libPath);
            }
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.core.internal.utils.AbstractClasspathEntryVisitor#visitSourceEntry(org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject, org.eclipse.core.runtime.IProgressMonitor)
         */
        @Override
        public void visitSourceEntry(IClasspathEntry entry,
                IJavaProject javaProject, IProgressMonitor monitor)
                throws CoreException {
            cpEntries.add(classesPath);
        }
    }

    /**
     * The verified sub-directory for classes
     */
    public static final String CLASSES_DIRECTORY = "classes"; //$NON-NLS-1$

    /**
     * The verified sub-directory for libraries
     */
    public static final String LIBS_DIRECTORY = "libs"; //$NON-NLS-1$

    // The up-to-date flag for the deployed jar file
    private static final QualifiedName DEPLOYMENT_UP_TO_DATE_PROP = new QualifiedName(
            IMTJCoreConstants.PLUGIN_ID, "deployed_up_to_date"); //$NON-NLS-1$

    // Storage for the symbol definition sets
    private static final QualifiedName SYMBOL_SETS_PROP = new QualifiedName(
            IMTJCoreConstants.PLUGIN_ID, "symbol_sets"); //$NON-NLS-1$

    /**
     * Return a boolean indicating whether the project contains the JavaME
     * classpath container.
     * 
     * @param javaProject the project to be tested
     * @return whether the project has the J2ME classpath container
     * @throws JavaModelException
     */
    public static boolean containsJavaMEClasspathContainer(
            IJavaProject javaProject) throws JavaModelException {
        boolean contains = false;

        IClasspathEntry[] classpath = javaProject.getRawClasspath();
        for (IClasspathEntry entry : classpath) {
            if ((entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER)
                    && (entry.getPath().segment(0)
                            .equals(JavaMEClasspathContainer.JAVAME_CONTAINER))) {
                contains = true;
                break;
            }
        }

        return contains;
    }

    /**
     * Return the default JAD file name for the specified project.
     * 
     * @param project
     * @return
     */
    public static String getDefaultJadFileName(IProject project) {
        String projectName = project.getName();
        return projectName.replace(' ', '_') + ".jad"; //$NON-NLS-1$

    }

    // The java project on which this MIDlet suite is based
    private IJavaProject javaProject;

    // The metadata about this project
    private MetaData metaData;

    private String tempKeyPassword;

    private String tempKeystorePassword;

    List<IMidletSuiteProjectListener> listenerList = new ArrayList<IMidletSuiteProjectListener>();

    /**
     * Private constructor for singleton instances of the class.
     */
    public MidletSuiteProject(IJavaProject javaProject) {
        super();
        this.javaProject = javaProject;
        initializeMetadata();

        if (this.getMetaData().getDevice() == null) {
            try {
                if (getProject().findMarkers(
                        IMTJCoreConstants.JAVAME_MISSING_DEVICE_MARKER, false,
                        IResource.DEPTH_ZERO).length == 0) {
                    IMarker marker = getProject().createMarker(
                            IMTJCoreConstants.JAVAME_MISSING_DEVICE_MARKER);
                    marker
                            .setAttribute(IMarker.MESSAGE,
                                    Messages.MidletSuiteProject_device_not_available);
                    marker.setAttribute(IMarker.SEVERITY,
                            IMarker.SEVERITY_ERROR);
                    
                }
            } catch (CoreException e) {
                e.printStackTrace();
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#addMidletSuiteProjectListener(org.eclipse.mtj.core.model.project.IMidletSuiteProjectListener)
     */
    public void addMidletSuiteProjectListener(
            IMidletSuiteProjectListener listener) {
        listenerList.add(listener);

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#createPackage(org.eclipse.core.runtime.IProgressMonitor, boolean)
     */
    public void createPackage(IProgressMonitor monitor, boolean obfuscate)
            throws CoreException {
        Map<String, Boolean> args = new HashMap<String, Boolean>(2);
        args.put(PreverificationBuilder.ARG_DO_PACKAGE, Boolean.TRUE);
        args.put(PreverificationBuilder.ARG_DO_OBFUSCATION, Boolean
                .valueOf(obfuscate));
        args.put(PreverificationBuilder.ARG_UPDATE_VERSION, Boolean.TRUE);

        PreverificationBuilder.cleanProject(getProject(), true, monitor);
        this.getProject().build(IncrementalProjectBuilder.FULL_BUILD,
                IMTJCoreConstants.J2ME_PREVERIFIER_ID, args, monitor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#getApplicationDescriptor()
     */
    public ApplicationDescriptor getApplicationDescriptor() {

        ApplicationDescriptor descriptor = null;

        IFile jadFile = getApplicationDescriptorFile();
        if (!jadFile.exists()) {
            // Create JAD file
        }

        try {
            File jFile = jadFile.getLocation().toFile();
            descriptor = new ApplicationDescriptor(jFile);
        } catch (IOException e) {
            MTJCorePlugin.log(IStatus.ERROR, "getApplicationDescriptor", e); //$NON-NLS-1$
        }

        return descriptor;
    }

    /**
     * Return the Application Descriptor file
     * 
     * @return the Application Descriptor file
     */
    public IFile getApplicationDescriptorFile() {
        IFile jadFile = null;
        IProject project = javaProject.getProject();
        jadFile = project.getFile(APPLICATION_DESCRIPTOR_NAME);
        return jadFile;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#getClasspath(org.eclipse.core.runtime.IProgressMonitor)
     */
    public File[] getClasspath(IProgressMonitor monitor) throws CoreException {
        // The verified output locations
        IPath classesPath = getVerifiedClassesOutputFolder(monitor)
                .getLocation();
        IPath libsPath = getVerifiedLibrariesOutputFolder(monitor)
                .getLocation();

        // Build up the list of IPath entries based on the project
        // classpath setup to maintain relative ordering
        ClasspathCollectionVisitor visitor = new ClasspathCollectionVisitor(
                classesPath, libsPath);
        visitor.getRunner().run(javaProject, visitor, monitor);

        int index = 0;
        Set<?> cpEntries = visitor.getCpEntries();
        File[] entries = new File[cpEntries.size()];

        Iterator<?> iterator = cpEntries.iterator();
        while (iterator.hasNext()) {
            IPath path = (IPath) iterator.next();
            entries[index++] = path.toFile();
        }

        return entries;
    }

    /**
     * Return the device referenced by this project.
     */
    public IDevice getDevice() {
        return metaData.getDevice();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#getEnabledSymbolDefinitionSet()
     */
    public SymbolDefinitionSet getEnabledSymbolDefinitionSet()
            throws CoreException, PersistenceException {
        SymbolDefinitionSet set = null;

        String setName = getProject().getPersistentProperty(SYMBOL_SETS_PROP);
        if (setName != null) {
            set = SymbolDefinitionSetRegistry.singleton
                    .getSymbolDefinitionSet(setName);
        }

        return set;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#getJadFileName()
     */
    public String getJadFileName() {
        return metaData.getJadFileName();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.IMidletSuiteProject#getJarFilename()
     */
    public String getJarFilename() {
        String filename = null;

        String jarUrl = getApplicationDescriptor().getMidletJarURL();
        if (jarUrl != null) {
            // If this is really a URL, we want to just get the name
            // from the end of the URL.
            int lastSlashIndex = jarUrl.lastIndexOf('/');
            if (lastSlashIndex != -1) {
                filename = jarUrl.substring(lastSlashIndex + 1);
            } else {
                filename = jarUrl;
            }
        }

        if (filename == null) {
            filename = getProjectNameWithoutSpaces() + ".jar"; //$NON-NLS-1$
        }

        return filename;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.IMidletSuiteProject#getJavaProject()
     */
    public IJavaProject getJavaProject() {
        return javaProject;
    }

    /**
     * Return the metadata for this MIDlet suite.<br>
     * NOTE: This method is not considered part of the API and is only available
     * to aid the implementation.
     * 
     * @return the metadata.
     */
    public MetaData getMetaData() {
        return metaData;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.IMidletSuiteProject#getProject()
     */
    public IProject getProject() {
        return javaProject.getProject();
    }

    /*(non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#getSignatureProperties()
     */
    public ISignatureProperties getSignatureProperties() throws CoreException {
        return (metaData.getSignatureProperties());
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#getTempKeyPassword()
     */
    public String getTempKeyPassword() {
        return (tempKeyPassword);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#getTempKeystorePassword()
     */
    public String getTempKeystorePassword() {
        return (tempKeystorePassword);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.IMidletSuiteProject#getVerifiedClassesOutputFolder(org.eclipse.core.runtime.IProgressMonitor)
     */
    public IFolder getVerifiedClassesOutputFolder(IProgressMonitor monitor)
            throws CoreException {
        return getVerifiedOutputFolder(monitor).getFolder(CLASSES_DIRECTORY);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#getVerifiedLibrariesOutputFolder(org.eclipse.core.runtime.IProgressMonitor)
     */
    public IFolder getVerifiedLibrariesOutputFolder(IProgressMonitor monitor)
            throws CoreException {
        return getVerifiedOutputFolder(monitor).getFolder(LIBS_DIRECTORY);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#getVerifiedOutputFolder(org.eclipse.core.runtime.IProgressMonitor)
     */
    public IFolder getVerifiedOutputFolder(IProgressMonitor monitor)
            throws CoreException {
        IFolder tempFolder = getProject().getFolder(
                IMTJCoreConstants.TEMP_FOLDER_NAME);
        return tempFolder.getFolder(IMTJCoreConstants.VERIFIED_FOLDER_NAME);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#isDeployedJarUpToDate()
     */
    public boolean isDeployedJarUpToDate() throws CoreException {
        String upToDateString = getProject().getPersistentProperty(
                DEPLOYMENT_UP_TO_DATE_PROP);
        return (upToDateString == null) ? false : upToDateString.equals("true"); //$NON-NLS-1$
    }

    /**
     * Return a boolean indicating whether the underlying project is a
     * preprocessed project.
     * 
     * @return
     * @throws CoreException
     */
    public boolean isPreprocessedProject() throws CoreException {
        IProject project = getProject();
        return project.exists()
                && project.isOpen()
                && project
                        .hasNature(IMTJCoreConstants.J2ME_PREPROCESSED_NATURE_ID);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#preverify(org.eclipse.core.resources.IResource[], org.eclipse.core.resources.IFolder, org.eclipse.core.runtime.IProgressMonitor)
     */
    public PreverificationError[] preverify(IResource[] toVerify,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException, PreverifierNotFoundException {
        return getPreverifier()
                .preverify(this, toVerify, outputFolder, monitor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#preverifyJarFile(java.io.File, org.eclipse.core.resources.IFolder, org.eclipse.core.runtime.IProgressMonitor)
     */
    public PreverificationError[] preverifyJarFile(File jarFile,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException, PreverifierNotFoundException {
        return getPreverifier().preverifyJarFile(this, jarFile, outputFolder,
                monitor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#removeMidletSuiteProjectListener(org.eclipse.mtj.core.model.project.IMidletSuiteProjectListener)
     */
    public void removeMidletSuiteProjectListener(
            IMidletSuiteProjectListener listener) {
        listenerList.remove(listener);

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#saveMetaData()
     */
    public void saveMetaData() throws CoreException {
        // Don't save the metadata if this is a preprocessed project,
        // as it belongs to the base project
        if (!isPreprocessedProject()) {
            metaData.saveMetaData();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#setDeployedJarFileUpToDate(boolean)
     */
    public void setDeployedJarFileUpToDate(boolean upToDate)
            throws CoreException {
        String value = upToDate ? "true" : "false"; //$NON-NLS-1$ //$NON-NLS-2$
        getProject().setPersistentProperty(DEPLOYMENT_UP_TO_DATE_PROP, value);

        for (Iterator<IMidletSuiteProjectListener> iterator = listenerList
                .iterator(); iterator.hasNext();) {
            iterator.next().deployedJarFileUpToDateFlagChanged();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#setDevice(org.eclipse.mtj.core.model.device.IDevice, org.eclipse.core.runtime.IProgressMonitor)
     */
    public void setDevice(IDevice device, IProgressMonitor monitor)
            throws CoreException {

        if ((metaData.getDevice() == null)
                || (!metaData.getDevice().equals(device))) {

            getProject().deleteMarkers(
                    IMTJCoreConstants.JAVAME_MISSING_DEVICE_MARKER, false,
                    IResource.DEPTH_ZERO);

            metaData.setDevice(device);

            // Make sure that we have a place to put the
            // classpath updates
            addClasspathContainerIfMissing(monitor);

            // Changing the classpath container for the project to match the
            // selected device
            IClasspathEntry[] rawClasspath = javaProject.getRawClasspath();
            for (int i = 0; i < rawClasspath.length; i++) {
                IClasspathEntry entry = rawClasspath[i];
                if (entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER) {
                    IPath path = entry.getPath();
                    if (path.segment(0).equals(
                            JavaMEClasspathContainer.JAVAME_CONTAINER)) {
                        IPath entryPath = new Path(
                                JavaMEClasspathContainer.JAVAME_CONTAINER + "/" //$NON-NLS-1$
                                        + metaData.getDevice());
                        if (!entry.equals(entryPath)) { // otherwise nothing to
                            // update
                            rawClasspath[i] = JavaCore
                                    .newContainerEntry(entryPath);
                            javaProject.setRawClasspath(rawClasspath, monitor);

                            // Force a rebuild
                            getProject().build(
                                    IncrementalProjectBuilder.FULL_BUILD,
                                    monitor);
                            break;
                        }
                    }
                }
            }
        }
        for (Iterator<IMidletSuiteProjectListener> iterator = listenerList
                .iterator(); iterator.hasNext();) {
            iterator.next().deviceChanded();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#setEnabledSymbolDefinitionSet(org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet)
     */
    public void setEnabledSymbolDefinitionSet(SymbolDefinitionSet set)
            throws CoreException, PersistenceException {
        String setName = null;

        if (set != null) {
            setName = set.getName();
        }

        getProject().setPersistentProperty(SYMBOL_SETS_PROP, setName);
        for (Iterator<IMidletSuiteProjectListener> iterator = listenerList
                .iterator(); iterator.hasNext();) {
            iterator.next().enabledSymbolDefinitionSetChanged();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#setJadFileName(java.lang.String)
     */
    public void setJadFileName(String jadFileName) {
        metaData.setJadFileName(jadFileName);
        for (Iterator<IMidletSuiteProjectListener> iterator = listenerList
                .iterator(); iterator.hasNext();) {
            iterator.next().jadFileNameChanged();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#setSignatureProperties(org.eclipse.mtj.core.model.sign.ISignatureProperties)
     */
    public void setSignatureProperties(ISignatureProperties props) {
        metaData.setSignatureProperties(props);
        for (Iterator<IMidletSuiteProjectListener> iterator = listenerList
                .iterator(); iterator.hasNext();) {
            iterator.next().signaturePropertiesChanged();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#setTempKeyPassword(java.lang.String)
     */
    public void setTempKeyPassword(String pass) {
        tempKeyPassword = pass;
        for (Iterator<IMidletSuiteProjectListener> iterator = listenerList
                .iterator(); iterator.hasNext();) {
            iterator.next().tempKeyPasswordChanged();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.project.IMidletSuiteProject#setTempKeystorePassword(java.lang.String)
     */
    public void setTempKeystorePassword(String pass) {
        tempKeystorePassword = pass;
        for (Iterator<IMidletSuiteProjectListener> iterator = listenerList
                .iterator(); iterator.hasNext();) {
            iterator.next().tempKeystorePasswordChanged();
        }
    }

    /**
     * Add the JavaME classpath container to the java project we wrap if it is
     * currently missing. This step provides migration for projects created with
     * earlier releases of MTJ as well as providing a means to fix projects that
     * have lost their platform definition association.
     * 
     * @param monitor
     * @throws JavaModelException
     */
    private void addClasspathContainerIfMissing(IProgressMonitor monitor)
            throws CoreException {

        boolean hasClasspathContainer = containsJavaMEClasspathContainer(getJavaProject());
        boolean hasPreprocessingNature = isPreprocessedProject();

        if (!hasClasspathContainer && !hasPreprocessingNature) {
            // Create a new classpath entry for the classpath
            // container
            IPath entryPath = new Path(
                    JavaMEClasspathContainer.JAVAME_CONTAINER + "/" //$NON-NLS-1$
                            + metaData.getDevice());
            IClasspathEntry newEntry = JavaCore.newContainerEntry(entryPath);

            // Get the current classpath entries
            IClasspathEntry[] rawClasspath = javaProject.getRawClasspath();
            Set<IClasspathEntry> currentClasspath = new LinkedHashSet<IClasspathEntry>(
                    rawClasspath.length);
            for (IClasspathEntry entry : rawClasspath) {
                if (entry.getEntryKind() == IClasspathEntry.CPE_VARIABLE) {
                    entry = JavaCore.getResolvedClasspathEntry(entry);
                }

                currentClasspath.add(entry);
            }

            // The classpath entries that are provided by
            // the platform definition currently
            IClasspathEntry[] platformEntries = getDevice().getClasspath()
                    .asClasspathEntries();

            // Remove the classpath entries from the project
            // if they are provided by the platform definition
            for (IClasspathEntry entry : platformEntries) {
                if (currentClasspath.contains(entry)) {
                    currentClasspath.remove(entry);
                }
            }

            // Set the updated classpath
            currentClasspath.add(newEntry);
            IClasspathEntry[] newClasspath = currentClasspath
                    .toArray(new IClasspathEntry[currentClasspath.size()]);
            javaProject.setRawClasspath(newClasspath, monitor);
        }
    }

    /**
     * Return the preverifier to use for resources in this project.
     * 
     * @throws PreverifierNotFoundException
     */
    private IPreverifier getPreverifier() throws CoreException,
            PreverifierNotFoundException {
        IPreverifier preverifier = null;

        if (isEmbeddedPreverifierEnabled()) {
            preverifier = new EmbeddedPreverifier();
        } else {

            IDevice device = getDevice();
            if (device == null) {
                throw new PreverifierNotFoundException(Messages.MidletSuiteProject_preverifier_missing_device);
            }
            preverifier = device.getPreverifier() != null ? device
                    .getPreverifier() : DeviceRegistry.singleton
                    .getDefaultPreferifier();
            if (preverifier == null) {
                throw new PreverifierNotFoundException(
                        Messages.MidletSuiteProject_preverifier_missing_default);
            }
        }
        return preverifier;
    }

    /**
     * Get the project name, replacing spaces with underscores.
     * 
     * @return
     */
    private String getProjectNameWithoutSpaces() {
        String projectName = javaProject.getProject().getName();
        return projectName.replace(' ', '_');
    }

    /**
     * Initialize the project metadata.
     */
    private void initializeMetadata() {
        try {
            if (isPreprocessedProject()) {
                ICommand preverifierCommand = null;

                ICommand[] buildCommands = getProject().getDescription()
                        .getBuildSpec();
                for (ICommand command : buildCommands) {
                    if (command.getBuilderName().equals(
                            IMTJCoreConstants.J2ME_PREVERIFIER_ID)) {
                        preverifierCommand = command;
                        break;
                    }
                }

                if (preverifierCommand != null) {
                    // Pull out the target project and use its metadata
                    String targetProjectName = (String) preverifierCommand
                            .getArguments().get(
                                    IPreverifier.BUILD_ARG_PREVERIFY_TARGET);
                    IProject targetProject = ResourcesPlugin.getWorkspace()
                            .getRoot().getProject(targetProjectName);
                    if (targetProject != null) {
                        metaData = new MetaData(targetProject);
                    }
                }
            }
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        }

        if (metaData == null) {
            metaData = new MetaData(this);
        }
    }

    /**
     * Return a boolean indicating whether or not to use the embedded
     * preverifier.
     * 
     * @param project
     * @return
     */
    private boolean isEmbeddedPreverifierEnabled() {
        // return PreferenceAccessor.instance
        // .getUseBuiltInPreverifier(getProject());
        return false;
    }

}
