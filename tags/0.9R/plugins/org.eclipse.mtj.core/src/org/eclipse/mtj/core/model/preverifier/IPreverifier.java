/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.preverifier;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.preverifier.results.PreverificationError;


/**
 * Required interface for preverification support. Each IPlatformDefinition is
 * required to provide a preverifier instance for use during builds.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */
public interface IPreverifier extends IPersistable {

    // Declare whether classes should be pre-verified by the preverifier
    public static final String BUILD_ARG_PREVERIFY_CLASSES = "preverifyClasses";

    // Declare whether libraries should be pre-verified by the preverifier
    public static final String BUILD_ARG_PREVERIFY_LIBS = "preverifyLibraries";

    // Declare what project the output of the preverifier should be placed
    public static final String BUILD_ARG_PREVERIFY_TARGET = "preverifyTargetProject";

    // A temporary project property that is passed through to the preverifier
    // to determine to what project the preverification output will be written
    public static final QualifiedName PREVERIFY_TARGET_PROJECT = new QualifiedName(
            MTJCorePlugin.getDefault().getBundle().getSymbolicName(),
            BUILD_ARG_PREVERIFY_TARGET);

    /**
     * Launch the preverification process on the specified resources.
     * 
     * @param midletProject The project in which the resources to be
     *                pre-verified reside.
     * @param toVerify The resources to be pre-verified.
     * @param outputFolder The folder into which the output is to be placed.
     * @param monitor Progress monitor
     * @return An array of preverification error instances.
     * @throws CoreException
     * @throws IOException
     */
    public PreverificationError[] preverify(IMidletSuiteProject midletProject,
            IResource[] toVerify, IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException;

    /**
     * Launch the preverification process on the specified jar file.
     * 
     * @param midletProject The project in which the resources to be
     *                pre-verified reside.
     * @param jarFile The jar file to be pre-verified.
     * @param outputFolder The folder into which the output is to be placed.
     * @param monitor Progress monitor
     * @return An array of preverification error instances.
     * @throws CoreException
     * @throws IOException
     */
    public PreverificationError[] preverifyJarFile(
            IMidletSuiteProject midletProject, File jarFile,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException;
}
