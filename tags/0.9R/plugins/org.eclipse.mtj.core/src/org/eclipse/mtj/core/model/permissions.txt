# javax.microedition.io.Connector
javax.microedition.io.Connector.http
javax.microedition.io.Connector.https
javax.microedition.io.Connector.datagram
javax.microedition.io.Connector.datagramreceiver
javax.microedition.io.Connector.socket
javax.microedition.io.Connector.serversocket
javax.microedition.io.Connector.ssl
javax.microedition.io.Connector.comm

# Push registry
javax.microedition.io.PushRegistry

# Bluetooth
javax.microedition.io.Connector.bluetooth.client
javax.microedition.io.Connector.bluetooth.server

# Wireless Messaging
javax.microedition.io.Connector.sms
javax.wireless.messaging.sms.send
javax.wireless.messaging.sms.receive
javax.microedition.io.Connector.cbs
javax.wireless.messaging.cbs.receive
javax.microedition.io.Connector.mms
javax.wireless.messaging.mms.send
javax.wireless.messaging.mms.receive

# Mobile Media
javax.microedition.media.control.RecordControl
javax.microedition.media.control.VideoControl.getSnapshot

# Location API
javax.microedition.location.Location
javax.microedition.location.ProximityListener
javax.microedition.location.Orientation
javax.microedition.location.LandmarkStore.read
javax.microedition.location.LandmarkStore.write
javax.microedition.location.LandmarkStore.category
javax.microedition.location.LandmarkStore.management

# File Connection API
javax.microedition.io.Connector.file.read
javax.microedition.io.Connector.file.write

# PIM API
javax.microedition.pim.ContactList.read
javax.microedition.pim.ContactList.write
javax.microedition.pim.EventList.read
javax.microedition.pim.EventList.write
javax.microedition.pim.ToDoList.read
javax.microedition.pim.ToDoList.write

# SIP API
javax.microedition.io.Connector.sip
javax.microedition.io.Connector.sips

# Security and Trust Services API
javax.microedition.apdu.sat
javax.microedition.apdu.aid
javax.microedition.jcrmi
javax.microedition.securityservice.CMSMessageSignatureService
