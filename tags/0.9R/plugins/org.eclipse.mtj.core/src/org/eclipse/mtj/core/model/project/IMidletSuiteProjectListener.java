/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.core.model.project;

/**
 * Classes which implement this interface provide a method that deals with the
 * events that are generated when the {@link IMidletSuiteProject} is modified.
 * 
 * @author Diego Madruga Sandin
 */
public interface IMidletSuiteProjectListener {

    /**
     * Sent when the
     * {@link IMidletSuiteProject#setDevice(org.eclipse.mtj.core.model.device.IDevice, org.eclipse.core.runtime.IProgressMonitor)}
     * is invoked.
     */
    public void deviceChanded();

    /**
     * Sent when the
     * {@link IMidletSuiteProject#setSignatureProperties(org.eclipse.mtj.core.model.sign.ISignatureProperties)}
     * is invoked.
     */
    public void signaturePropertiesChanged();

    /**
     * Sent when the
     * {@link IMidletSuiteProject#setEnabledSymbolDefinitionSet(org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet)}
     * is invoked.
     */
    public void enabledSymbolDefinitionSetChanged();

    /**
     * Sent when the
     * {@link IMidletSuiteProject#setDeployedJarFileUpToDate(boolean)} is
     * invoked.
     */
    public void deployedJarFileUpToDateFlagChanged();

    /**
     * Sent when the {@link IMidletSuiteProject#setJadFileName(String)} is
     * invoked.
     */
    public void jadFileNameChanged();

    /**
     * Sent when the {@link IMidletSuiteProject#setTempKeyPassword(String)} is
     * invoked.
     */
    public void tempKeyPasswordChanged();

    /**
     * Sent when the {@link IMidletSuiteProject#setTempKeystorePassword(String)}
     * is invoked.
     */
    public void tempKeystorePasswordChanged();
}
