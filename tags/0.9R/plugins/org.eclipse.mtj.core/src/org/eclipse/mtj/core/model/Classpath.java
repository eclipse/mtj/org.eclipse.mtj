/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.library.api.API;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * A representation of a classpath that can be converted to various other
 * representations.
 * 
 * @author Craig Setera
 */
public class Classpath implements IPersistable {

    private ArrayList<IPersistable> entries;

    /**
     * Construct a new classpath instance
     */
    public Classpath() {
        super();
        entries = new ArrayList<IPersistable>();
    }

    /**
     * Add a new classpath entry.
     * 
     * @param pathEntry
     */
    public void addEntry(ILibrary pathEntry) {
        entries.add(pathEntry);
    }

    /**
     * Return the classpath as a set of Eclipse JDT classpath entries.
     * 
     * @return
     */
    public IClasspathEntry[] asClasspathEntries() {
        ILibrary[] allEntries = getEntries();
        IClasspathEntry[] cpEntries = new IClasspathEntry[allEntries.length];

        for (int i = 0; i < cpEntries.length; i++) {
            cpEntries[i] = allEntries[i].toClasspathEntry();
        }

        return cpEntries;
    }

    /**
     * Return the classpath as an array of the file entries in the classpath.
     * 
     * @return
     */
    public File[] asFileArray() {
        ILibrary[] allEntries = getEntries();
        File[] files = new File[allEntries.length];

        for (int i = 0; i < files.length; i++) {
            files[i] = allEntries[i].toFile();
        }

        return files;
    }

    /**
     * Return the libraries as skeleton API files. If the skeleton URL's cannot
     * be resolved to local file system files, they will not be returned.
     * 
     * @return
     */
    public File[] asSkeletonFileArray() {
        URL[] skeletonURLs = asSkeletonURLArray();
        ArrayList<File> skeletonFiles = new ArrayList<File>(skeletonURLs.length);

        for (int i = 0; i < skeletonURLs.length; i++) {
            try {
                URL fileURL = FileLocator.toFileURL(skeletonURLs[i]);
                skeletonFiles.add(new File(fileURL.getPath()));
            } catch (IOException e) {
                MTJCorePlugin.log(IStatus.WARNING, e);
            }
        }

        return (File[]) skeletonFiles.toArray(new File[skeletonFiles.size()]);
    }

    /**
     * Return the libraries with skeleton API file URLs where possible.
     * 
     * @return
     */
    public URL[] asSkeletonURLArray() {
        ArrayList<Serializable> files = new ArrayList<Serializable>();

        ILibrary[] allEntries = getEntries();
        for (int i = 0; i < allEntries.length; i++) {
            ILibrary lib = allEntries[i];

            API[] apis = lib.getAPIs();
            for (int j = 0; j < apis.length; j++) {
                API api = apis[j];

                URL[] replacements = api.getSkeletonReplacements();
                if (replacements == null) {
                    files.add(lib.toFile());
                } else {
                    for (int k = 0; k < replacements.length; k++) {
                        files.add(replacements[k]);
                    }
                }
            }
        }

        return (URL[]) files.toArray(new URL[files.size()]);
    }

    /**
     * Return the classpath as an array of URL's.
     * 
     * @return
     */
    public URL[] asURLArray() {
        ILibrary[] allEntries = getEntries();
        URL[] urls = new URL[allEntries.length];

        for (int i = 0; i < urls.length; i++) {
            urls[i] = allEntries[i].toURL();
        }

        return urls;
    }

    /**
     * Test the equality of this class with another classpath.
     * 
     * @param classpath
     * @return
     */
    public boolean equals(Classpath classpath) {
        ILibrary[] myEntries = getEntries();
        ILibrary[] otherEntries = classpath.getEntries();

        boolean equals = (myEntries.length == otherEntries.length);
        for (int i = 0; equals && (i < myEntries.length); i++) {
            equals = myEntries[i].equals(otherEntries[i]);
        }

        return equals;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        boolean equals = false;

        if (obj instanceof Classpath) {
            equals = equals((Classpath) obj);
        }

        return equals;
    }

    /**
     * Return the entries in the classpath.
     * 
     * @return
     */
    public ILibrary[] getEntries() {
        return (ILibrary[]) entries.toArray(new ILibrary[entries.size()]);
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        ILibrary[] allEntries = getEntries();
        int hashCode = allEntries.length << 24;

        for (int i = 0; i < allEntries.length; i++) {
            hashCode = hashCode ^ allEntries[i].hashCode();
        }

        return hashCode;
    }

    /**
     * 
     * @param persistenceProvider
     * @throws PersistenceException
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        entries.clear();
        int entryCount = persistenceProvider.loadInteger("entryCount");
        for (int i = 0; i < entryCount; i++) {
            entries.add(persistenceProvider.loadPersistable("entry" + i));
        }
    }

    /**
     * Remove the specified library from the classpath, if it is in the
     * classpath.
     * 
     * @param library
     */
    public void removeEntry(ILibrary library) {
        entries.remove(library);
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        persistenceProvider.storeInteger("entryCount", entries.size());

        int i = 0;
        Iterator<IPersistable> iterator = entries.iterator();
        while (iterator.hasNext()) {
            ILibrary library = (ILibrary) iterator.next();
            persistenceProvider.storePersistable("entry" + i++, library);
        }
    }

    /**
     * Converts the classpath to a classpath string with
     * platform-dependent path separator characters.
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();

        ILibrary[] allEntries = getEntries();
        for (int i = 0; i < allEntries.length; i++) {
            if (i != 0) {
                sb.append(File.pathSeparatorChar);
            }

            sb.append(allEntries[i].toFile());
        }

        return sb.toString();
    }
}
