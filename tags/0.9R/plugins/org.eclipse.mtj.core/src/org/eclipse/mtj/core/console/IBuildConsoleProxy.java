/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 */
package org.eclipse.mtj.core.console;

import java.io.PrintWriter;

/**
 * An interface from which callers can access the BuildConsole.
 * 
 * @author Craig Setera
 */
public interface IBuildConsoleProxy {
    public static final String ID_ERROR_STREAM = "error";
    public static final String ID_OUTPUT_STREAM = "output";
    public static final String ID_TRACE_STREAM = "trace";

    /**
     * Return the PrintWriter for the console with the specified identifier.
     * 
     * @param id
     * @return
     */
    public PrintWriter getConsoleWriter(String id);
}
