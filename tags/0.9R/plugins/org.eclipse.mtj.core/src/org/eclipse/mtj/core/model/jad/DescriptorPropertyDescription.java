/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.jad;

/**
 * This class represents an application descriptor property description.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */

public class DescriptorPropertyDescription {
    
    /** 
     * String property type 
     */
    public static final int DATATYPE_STRING = 1;

    /** 
     * URL property type 
     */
    public static final int DATATYPE_URL = 2;

    /** 
     * Integer property type 
     */
    public static final int DATATYPE_INT = 3;

    /** 
     * List property type 
     */
    public static final int DATATYPE_LIST = 4;

    // The name of the underlying property
    private String propertyName;

    // The displayable name for this property
    private String displayName;

    // The data type for this property
    private int dataType;

    /**
     * Constructor
     */
    public DescriptorPropertyDescription(String propertyName,
            String displayName, int dataType) {
        super();
        this.propertyName = propertyName;
        this.displayName = displayName;
        this.dataType = dataType;
    }

    /**
     * @return the data type
     */
    public int getDataType() {
        return dataType;
    }

    /**
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @return the property name
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param i the data type
     */
    public void setDataType(int i) {
        dataType = i;
    }

    /**
     * @param string the display name
     */
    public void setDisplayName(String string) {
        displayName = string;
    }

    /**
     * @param string the property name
     */
    public void setPropertyName(String string) {
        propertyName = string;
    }
}
