/**
 * Copyright (c) 2004,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.jad;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.internal.utils.ColonDelimitedProperties;
import org.eclipse.mtj.core.model.Version;

/**
 * This class is a representation of a Java Application Descriptor (jad) for a
 * MIDlet Suite.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */

public class ApplicationDescriptor {

    /**
     * The definition of a MIDlet within the application descriptor.
     */
    public static class MidletDefinition {

        private String className;
        private String icon;
        private String name;
        private int number;

        MidletDefinition(int number, String definitionString) {
            int fieldCount = 0;
            this.number = number;

            name = "";
            icon = "";
            className = "";

            StringTokenizer st = new StringTokenizer(definitionString, ",",
                    true);
            while (st.hasMoreTokens()) {
                String token = st.nextToken();
                if (token.equals(",")) {
                    fieldCount++;
                } else {
                    switch (fieldCount) {
                    case 0:
                        name = token;
                        break;
                    case 1:
                        icon = token;
                        break;
                    case 2:
                        className = token;
                        break;
                    }
                }
            }
        }

        /**
         * Construct a new MIDlet definition with the specified information.
         * 
         * @param name
         * @param icon
         * @param className
         */
        public MidletDefinition(int number, String name, String icon,
                String className) {
            super();
            this.number = number;
            this.name = name;
            this.icon = icon;
            this.className = className;
        }

        /**
         * @return Returns the className.
         */
        public String getClassName() {
            return className;
        }

        /**
         * @return Returns the icon.
         */
        public String getIcon() {
            return icon;
        }

        /**
         * @return Returns the name.
         */
        public String getName() {
            return name;
        }

        /**
         * Return the MIDlet number.
         * 
         * @return
         */
        public int getNumber() {
            return number;
        }

        /**
         * @param className The className to set.
         */
        public void setClassName(String className) {
            this.className = className;
        }

        /**
         * @param icon The icon to set.
         */
        public void setIcon(String icon) {
            this.icon = icon;
        }

        /**
         * @param name The name to set.
         */
        public void setName(String name) {
            this.name = name;
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append(name).append(',');
            sb.append(icon).append(',');
            sb.append(className);

            return sb.toString();
        }
    }

    /**
     * The prefix of all MIDlet definition properties
     */
    public static final String MIDLET_PREFIX = "MIDlet-";

    private ColonDelimitedProperties manifestProperties;
    private List<MidletDefinition> midletDefinitions;
    private File sourceFile;

    /**
     * Construct a new ApplicationDescriptor instance based on the data in the
     * specified file.
     * 
     * @param jadFile The file in which the descriptor is being held.
     * @throws IOException when an error occurs reading the file
     */
    public ApplicationDescriptor(File jadFile) throws IOException {
        sourceFile = jadFile;
        midletDefinitions = new ArrayList<MidletDefinition>();
        parseDescriptor(jadFile);
    }

    /**
     * Add a new MidletDefinition instance.
     * 
     * @param midletDefinition the MIDlet definition to be added
     */
    public void addMidletDefinition(MidletDefinition midletDefinition) {
        midletDefinitions.add(midletDefinition);
    }

    /**
     * Copy the manifest properties.
     * 
     * @return the copy of the properties
     */
    private ColonDelimitedProperties copyProperties() {
        ColonDelimitedProperties copy = new ColonDelimitedProperties();

        Iterator<?> keys = manifestProperties.keySet().iterator();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            String value = manifestProperties.getProperty(key);
            copy.setProperty(key, value);
        }

        return copy;
    }

    /**
     * Return the configuration specification version associated with this JAD
     * file.
     * 
     * @return
     * @throws CoreException
     */
    public Version getConfigurationSpecificationVersion() throws CoreException {
        Version version = null;

        String configIdentifier = manifestProperties
                .getProperty(IJADConstants.JAD_MICROEDITION_CONFIG);

        // Attempt to parse the value
        if (configIdentifier != null) {
            String[] components = configIdentifier.split("-");
            if (components.length == 2) {
                version = new Version(components[1]);
            }
        }

        // Make sure we fall back to something reasonable
        if (version == null) {
            version = new Version("1.0");
        }

        return version;
    }

    /**
     * Return the overall manifest properties.
     * 
     * @return the manifest properties.
     */
    public ColonDelimitedProperties getManifestProperties() {
        return manifestProperties;
    }

    /**
     * Return the current count of MidletDefinition instances within this
     * application descriptor.
     * 
     * @return the number of MidletDefinition instances
     */
    public int getMidletCount() {
        return midletDefinitions.size();
    }

    /**
     * Return the list of MidletDefinition instances currently managed by the
     * ApplicationDescriptor.
     * 
     * @return a list of MidletDefinition instances
     */
    public List<MidletDefinition> getMidletDefinitions() {
        return midletDefinitions;
    }

    /**
     * Return the JAD URL specified in the application descriptor or
     * <code>null</code> if it has not been specified. This method does not
     * check the validity of the value.
     * 
     * @return
     */
    public String getMidletJarURL() {
        String url = null;

        if (manifestProperties != null) {
            url = manifestProperties
                    .getProperty(IJADConstants.JAD_MIDLET_JAR_URL);
        }

        return url;
    }

    /**
     * Parse the application descriptor file and store the information into this
     * instance.
     * 
     * @param jadFile the file to be parsed
     * @throws IOException
     */
    private void parseDescriptor(File jadFile) throws IOException {
        manifestProperties = new ColonDelimitedProperties();

        if (jadFile.exists()) {
            // Parse as a set of properties
            FileInputStream fis = new FileInputStream(jadFile);
            manifestProperties.load(fis);

            // Pull out the MIDlet definitions
            parseMidletDefinitions();
        }
    }

    /**
     * Parse out the MIDlet definitions from the manifest properties.
     */
    private void parseMidletDefinitions() {
        ArrayList<String> keysToRemove = new ArrayList<String>(
                manifestProperties.size());

        Iterator<?> iter = manifestProperties.keySet().iterator();
        while (iter.hasNext()) {
            String key = (String) iter.next();
            if (key.startsWith(MIDLET_PREFIX)) {
                int midletNumber = -1;
                try {
                    midletNumber = Integer.parseInt(key.substring(MIDLET_PREFIX
                            .length()));
                } catch (NumberFormatException e) {
                }

                if (midletNumber != -1) {
                    String value = manifestProperties.getProperty(key);
                    midletDefinitions.add(new MidletDefinition(midletNumber,
                            value));

                    // Mark for removal from the properties
                    keysToRemove.add(key);
                }
            }
        }

        // Remove the MIDlets
        Iterator<String> keyIter = keysToRemove.iterator();
        while (keyIter.hasNext()) {
            String key = (String) keyIter.next();
            manifestProperties.remove(key);
        }
    }

    /**
     * Store the ApplicationDescriptor instance into the same File from which it
     * was originally read.
     * 
     * @throws IOException when an error occurs while storing the descriptor
     */
    public void store() throws IOException {
        store(sourceFile);
    }

    /**
     * Store the ApplicationDescriptor instance into the specified file.
     * 
     * @param jadFile the file into which the descriptor will be written
     * @throws IOException when an error occurs while storing the descriptor
     */
    public void store(File jadFile) throws IOException {
        // Copy the current properties and add the MIDlets
        ColonDelimitedProperties copy = copyProperties();
        Iterator<MidletDefinition> iter = midletDefinitions.iterator();
        while (iter.hasNext()) {
            MidletDefinition def = (MidletDefinition) iter.next();
            String key = MIDLET_PREFIX + def.getNumber();
            String value = def.toString();
            copy.setProperty(key, value);
        }

        // Write out the resulting manifest properties
        FileOutputStream fos = new FileOutputStream(jadFile);
        try {
            copy.store(fos, "MIDlet Property Definitions");
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
            }
        }
    }
}
