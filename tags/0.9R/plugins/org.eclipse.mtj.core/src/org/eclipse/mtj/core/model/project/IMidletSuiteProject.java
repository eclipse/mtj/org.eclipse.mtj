/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Added application descriptor constant value                           
 *     Hugo Raniere (Motorola)  - Handling the case that there is no valid preverifier 
 */
package org.eclipse.mtj.core.model.project;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.jad.ApplicationDescriptor;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;
import org.eclipse.mtj.core.model.preverifier.PreverifierNotFoundException;
import org.eclipse.mtj.core.model.sign.ISignatureProperties;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.preverifier.results.PreverificationError;

/**
 * This interface represents access to MIDlet suite project specific
 * information. It acts as a wrapper around an IJavaProject.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */
public interface IMidletSuiteProject {

    /**
     * The project's application descriptor.
     */
    public static final String APPLICATION_DESCRIPTOR_NAME = "Application Descriptor";

    /**
     * Create a deployed JAR file package for this MIDlet suite project.
     * 
     * @param monitor progress monitor
     * @param obfuscate a boolean indicating whether to obfuscate the resulting
     *                packaged code.
     * @throws CoreException
     */
    public void createPackage(IProgressMonitor monitor, boolean obfuscate)
            throws CoreException;

    /**
     * Return an ApplicationDescriptor instance wrapped around the Application
     * Descriptor (JAD) file for this MIDlet suite.
     * 
     * @return the suite's application descriptor
     */
    public ApplicationDescriptor getApplicationDescriptor();

    /**
     * @return
     */
    public IFile getApplicationDescriptorFile();

    /**
     * Return the classpath for this project. This classpath does not include
     * the entries for the J2ME libraries.
     * 
     * @param monitor
     * @return
     * @throws CoreException
     */
    public File[] getClasspath(IProgressMonitor monitor) throws CoreException;

    /**
     * Return the device associated with this MIDlet suite project.
     * 
     * @return
     */
    public IDevice getDevice();

    /**
     * Return the symbol definition set that is enabled for this project.
     * 
     * @return
     * @throws CoreException
     * @throws {@link PersistenceException}
     */
    public SymbolDefinitionSet getEnabledSymbolDefinitionSet()
            throws CoreException, PersistenceException;

    /**
     * Return the file holding the JAD file. There is a chance that this file
     * may not actually exist if the user moved the file without using the
     * refactoring functionality.
     * 
     * @return the java application descriptor file
     */
    public String getJadFileName();

    /**
     * Return the name to use for the JAR file.
     * 
     * @return the jar file name
     */
    public String getJarFilename();

    /**
     * Return the underlying java project.
     * 
     * @return the underlying java project
     */
    public IJavaProject getJavaProject();

    /**
     * Return the underlying project instance.
     * 
     * @return the underlying project
     */
    public IProject getProject();

    /**
     * Get the ISignatureProperties associated with this MIDlet suite
     * 
     * @return the currently associated ISignatureProperties
     * @throws CoreException
     */
    public ISignatureProperties getSignatureProperties() throws CoreException;

    public String getTempKeyPassword();

    public String getTempKeystorePassword();

    /**
     * Get the IFolder into which verified classes should be written.
     * 
     * @param monitor progress monitor
     * @return the verified classes output folder
     * @throws CoreException
     */
    public IFolder getVerifiedClassesOutputFolder(IProgressMonitor monitor)
            throws CoreException;

    /**
     * Get the IFolder into which verified libraries should be written.
     * 
     * @param monitor progress monitor
     * @return the verified libraries output folder
     * @throws CoreException
     */
    public IFolder getVerifiedLibrariesOutputFolder(IProgressMonitor monitor)
            throws CoreException;

    /**
     * Get the IFolder into which verified classes should be written.
     * 
     * @param monitor progress monitor
     * @return the verified root output folder
     * @throws CoreException
     */
    public IFolder getVerifiedOutputFolder(IProgressMonitor monitor)
            throws CoreException;

    /**
     * Return a boolean indicating whether the project's deployed jar file
     * exists and is up to date compared with the code contained within the jar.
     * 
     * @return whether the deployed jar file is currently up to date.
     * @throws CoreException if there is a problem retrieving the information.
     */
    public boolean isDeployedJarUpToDate() throws CoreException;

    /**
     * Preverify the specified resources. Return the map of class names with
     * preverification errors mapped to the error that was caused.
     * 
     * @param toVerify the resources to be preverified
     * @param outputFolder the folder into which the output will be written
     * @param monitor progress monitor
     * @return An array of errors found during preverification.
     * @throws CoreException
     * @throws IOException
     */
    public PreverificationError[] preverify(IResource[] toVerify,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException,  PreverifierNotFoundException;

    /**
     * Launch the preverification process on the specified jar file.
     * 
     * @param jarFile The jar file to be preverified.
     * @param outputFolder The folder into which the output is to be placed.
     * @param monitor Progress monitor
     * @return An array of errors found during preverification.
     * @throws CoreException
     * @throws IOException
     * @throws PreverifierNotFoundException 
     */
    public PreverificationError[] preverifyJarFile(File jarFile,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException, PreverifierNotFoundException;

    /**
     * Save the MIDlet suite metadata. Should be called after
     * setPlatformDefinition or setSignatureProperties are called to persist the
     * information set.
     * 
     * @throws CoreException
     */
    public void saveMetaData() throws CoreException;

    /**
     * Set the flag indicating whether or not the deployed jar file for this
     * MIDlet suite project is currently up to date.
     * 
     * @param upToDate whether the deployed jar file is up to date.
     * @throws CoreException if there is a problem setting the information.
     */
    public void setDeployedJarFileUpToDate(boolean upToDate)
            throws CoreException;

    /**
     * Set the device being used by this project.
     * 
     * @param device
     * @param monitor
     * @throws CoreException
     */
    public void setDevice(IDevice device, IProgressMonitor monitor)
            throws CoreException;

    /**
     * Set the symbol definition set that is enabled for this project.
     * 
     * @param set the set to be enabled
     * @throws CoreException
     * @throws {@link PersistenceException}
     */
    public void setEnabledSymbolDefinitionSet(SymbolDefinitionSet set)
            throws CoreException, PersistenceException;

    /**
     * @return
     */
    public void setJadFileName(String jadFileName);

    /**
     * Set the ISignatureProperties to use for this MIDlet suite
     * 
     * @param props
     * @throws CoreException
     */
    public void setSignatureProperties(ISignatureProperties props)
            throws CoreException;

    public void setTempKeyPassword(String pass);

    public void setTempKeystorePassword(String pass);

    /**
     * @param listener
     */
    void addMidletSuiteProjectListener(IMidletSuiteProjectListener listener);

    /**
     * @param listener
     */
    void removeMidletSuiteProjectListener(IMidletSuiteProjectListener listener);
}
