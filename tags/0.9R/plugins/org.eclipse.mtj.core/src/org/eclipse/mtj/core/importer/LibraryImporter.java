/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Gang Ma 		(Sybase)	- Add javadoc automatically search for library
 */
package org.eclipse.mtj.core.importer;

import java.io.File;

import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.library.api.API;
import org.eclipse.mtj.core.model.library.api.APIRegistry;
import org.eclipse.mtj.core.model.library.impl.Library;

/**
 * A helper class for the importing of a Library instance based on a
 * java.io.File.
 * 
 * @author Craig Setera
 */
public class LibraryImporter {
	// a javadoc detector
	JavadocDetector javadocDetector;
    /**
     * Construct a new library importer.
     */
    public LibraryImporter() {
        super();
    }

    /**
     * Construct and return a new Library instance for the specified library
     * file.
     * 
     * @param libraryFile the file to be wrapped up as a library.
     * @return the newly created library wrapper
     */
    public ILibrary createLibraryFor(File libraryFile) {
        API[] apis = APIRegistry.getAPIs(libraryFile);

        Library library = new Library();
        library.setApis(apis);
        library.setLibraryFile(libraryFile);
        
        if(javadocDetector!=null)
        	library.setJavadocURL(javadocDetector.detectJavadoc(library));
        return library;
    }

    /**
     * Construct and return a new Library instance for the specified library
     * file with the provided access rules.
     * 
     * @param libraryFile
     * @param accessRules
     * @return
     */
    public ILibrary createLibraryFor(File libraryFile, IAccessRule[] accessRules) {
        API[] apis = APIRegistry.getAPIs(libraryFile);

        Library library = new Library();
        library.setAccessRules(accessRules);
        library.setApis(apis);
        library.setLibraryFile(libraryFile);

        return library;
    }

	public void setJavadocDetector(JavadocDetector javadocDetector) {
		this.javadocDetector = javadocDetector;
	}
    
    
}
