/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Feng Wang (Sybase) - Modify to fit for AbstractDevice#copyForLaunch(...)
 *                          signature changing.
 */
package org.eclipse.mtj.toolkit.uei;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.launching.ILaunchConstants;
import org.eclipse.mtj.core.model.LaunchEnvironment;
import org.eclipse.mtj.core.model.ReplaceableParametersProcessor;
import org.eclipse.mtj.core.model.device.IDevice2;
import org.eclipse.mtj.core.model.device.impl.AbstractDevice;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;

/**
 * Unified Emulator Interface (UEI) implementation of the IDevice interface.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */
public class UEIDevice extends AbstractDevice implements IDevice2 {

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals = false;

        if (obj instanceof UEIDevice) {
            equals = equals((UEIDevice) obj);
        }

        return equals;
    }

    /**
     * Test equality on a UEI device and return a boolean indicating equality.
     * 
     * @param device
     * @return
     */
    public boolean equals(UEIDevice device) {
        return super.equals(device)
                && launchCommandTemplate.equals(device.launchCommandTemplate);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.device.IDevice#getLaunchCommand(org.eclipse.mtj.core.model.LaunchEnvironment,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException {
        boolean isWin32 = Platform.getOS().equals(Platform.OS_WIN32);

        IMidletSuiteProject midletSuite = launchEnvironment.getMidletSuite();
        ILaunchConfiguration launchConfiguration = launchEnvironment
                .getLaunchConfiguration();
        boolean launchFromJAD = shouldDirectLaunchJAD(launchConfiguration);
        // Copy the things we need for launch to a temporary location to avoid
        // locking files
        File deployedTemp = copyForLaunch(midletSuite, monitor, launchFromJAD);

        // Fill in our properties so we can use them for resolution
        // against the raw executable command line
        Map<String, String> executionProperties = new HashMap<String, String>();
        executionProperties.put("executable", isWin32 ? executable.getName()
                : executable.getAbsolutePath());

        executionProperties.put("device", getName());

        // Debug information
        if (launchEnvironment.isDebugLaunch()) {
            executionProperties.put("debugPort", Integer
                    .toString(launchEnvironment.getDebugListenerPort()));
        }

        // Classpath
        if (!launchFromJAD) {
            String classpathString = getProjectClasspathString(
                    launchEnvironment.getMidletSuite(), deployedTemp, monitor);
            executionProperties.put("classpath", classpathString);
        }

        // Add launch configuration values
        addLaunchConfigurationValue(executionProperties, "verbose",
                launchConfiguration, ILaunchConstants.VERBOSITY_OPTIONS);
        addLaunchConfigurationValue(executionProperties, "heapsize",
                launchConfiguration, ILaunchConstants.HEAP_SIZE);

        String securityDomainName = launchConfiguration.getAttribute(
                ILaunchConstants.SECURITY_DOMAIN,
                ILaunchConstants.NO_SECURITY_DOMAIN);
        if (!securityDomainName.equals(ILaunchConstants.NO_SECURITY_DOMAIN)) {
            executionProperties.put("securityDomain", securityDomainName);
        }

        String extraArguments = launchConfiguration.getAttribute(
                ILaunchConstants.LAUNCH_PARAMS, "");
        executionProperties.put("userSpecifiedArguments", extraArguments);

        if (launchFromJAD) {
            executionProperties.put("jadfile",
                    getSpecifiedJadURL(launchConfiguration));
        } else if (shouldDoOTA(launchConfiguration)) {
            String url = getOTAURL(launchConfiguration, midletSuite);
            executionProperties.put("otaurl", url);
        } else {
            File jadFile = getJadForLaunch(midletSuite, deployedTemp, monitor);
            if (jadFile.exists()) {
                executionProperties.put("jadfile", jadFile.toString());
            }

            addLaunchConfigurationValue(executionProperties, "target",
                    launchConfiguration, ILaunchConstants.EMULATED_CLASS);
        }

        // Do the property resolution given the previous information
        String baseCommand = ReplaceableParametersProcessor
                .processReplaceableValues(launchCommandTemplate,
                        executionProperties);

        return isWin32 ? "cmd /c " + baseCommand : baseCommand;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.device.IDevice2#getWorkingDirectory()
     */
    public File getWorkingDirectory() {
        return executable.getParentFile();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return classpath.hashCode() ^ executable.hashCode() ^ name.hashCode()
                ^ launchCommandTemplate.hashCode() ^ groupName.hashCode();
    }
}
