<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.eclipse.me.ui">
<annotation>
      <appInfo>
         <meta.schema plugin="org.eclipse.me.ui" id="deviceEditor" name="Device Editor"/>
      </appInfo>
      <documentation>
         Provides an extension point for registering an editor for use in editing a device from the device management user interface.  Device types that do not have a registered editor will not be enabled for editing.
      </documentation>
   </annotation>

   <element name="extension">
      <complexType>
         <sequence>
            <element ref="editor"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  a fully qualified identifier of the target extension point
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  an optional identifier of the extension instance
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  an optional name of the extension instance
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="editor">
      <complexType>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  a required fully-qualified identifier for this particular device editor extension
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  an optional displayable name for this particular device editor extension
               </documentation>
            </annotation>
         </attribute>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  the required implementation class for the &lt;code&gt;org.eclipse.ui.IActionDelegate&lt;/code&gt; interface that will launch the edit function for the type of device.
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn="org.eclipse.ui.IActionDelegate"/>
               </appInfo>
            </annotation>
         </attribute>
         <attribute name="deviceClass" type="string" use="required">
            <annotation>
               <documentation>
                  the required name of the implementation class for the &lt;code&gt;eclipseme.core.model.device.IDevice&lt;/code&gt; interface implementation for which this particular editor will be used for editing.
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.mtj.core.model.device.IDevice"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         [Enter the first release in which this extension point appears.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         [Enter extension point usage example here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="apiInfo"/>
      </appInfo>
      <documentation>
         [Enter API information here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         [Enter information about supplied implementation of this extension point.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="copyright"/>
      </appInfo>
      <documentation>
         
      </documentation>
   </annotation>

</schema>
