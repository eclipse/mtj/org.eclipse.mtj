/**
 * Copyright (c) 2008 Motorola and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola)  - Initial implementation
 *                                
 */
package org.eclipse.mtj.ui.internal.devices;

import org.eclipse.osgi.util.NLS;

public class DevicesMessages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.mtj.ui.internal.devices.messages"; //$NON-NLS-1$
	public static String DeviceImportWizardPage_InvalidPreverifierMessage;
	public static String DeviceImportWizardPage_SelectDirectoryMessage;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, DevicesMessages.class);
	}

	private DevicesMessages() {
	}
}
