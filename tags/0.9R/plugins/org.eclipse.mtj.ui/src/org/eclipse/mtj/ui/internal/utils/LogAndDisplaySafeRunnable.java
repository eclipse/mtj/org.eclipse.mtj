/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.utils;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.LoggingSafeRunnable;
import org.eclipse.swt.widgets.Shell;

/**
 * Simple implementation of the ISafeRunnable interface that logs and displays
 * the exception.
 * 
 * @author Craig Setera
 */
public abstract class LogAndDisplaySafeRunnable extends LoggingSafeRunnable {
    
    private Shell shell;
    private String action;

    /**
     * @param shell
     * @param action
     */
    public LogAndDisplaySafeRunnable(Shell shell, String action) {
        this.shell = shell;
        this.action = action;
    }

    /**
     * @see org.eclipse.core.runtime.ISafeRunnable#handleException(java.lang.Throwable)
     */
    public void handleException(Throwable exception) {
        super.handleException(exception);

        IStatus status = MTJCorePlugin.newStatus(IStatus.ERROR, -999, exception
                .getMessage(), exception);

        ErrorDialog.openError(shell, "Error Occurred",
                "An error occurred during " + action, status);
    }
}
