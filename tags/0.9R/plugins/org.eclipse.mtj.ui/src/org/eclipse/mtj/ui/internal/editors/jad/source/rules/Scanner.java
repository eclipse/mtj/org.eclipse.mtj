/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.editors.jad.source.rules;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IWhitespaceDetector;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordPatternRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.mtj.core.model.jad.IJADConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

/**
 * A scanner programmed with a sequence of rules specific to the JAD file.
 * 
 * @author Diego Madruga Sandin
 */
public class Scanner extends RuleBasedScanner {

    /**
     * The code highlight color for Keywords
     */
    public static final Color KEYWORD = new Color(Display.getCurrent(), 127, 0,
            85);

    /**
     * The code highlight color for Strings
     */
    public static final Color STRING = new Color(Display.getCurrent(), 0, 0, 0);

    /**
     * Creates a new Scanner
     */
    public Scanner() {

        IWordDetector detector = new IWordDetector() {

            public boolean isWordPart(char c) {
                if (c == '-') {
                    return true;
                } else {
                    return Character.isJavaIdentifierPart(c);
                }
            }

            public boolean isWordStart(char c) {
                return Character.isJavaIdentifierStart(c);
            }

        };

        WordRule rule = new WordRule(detector);

        Token keyword = new Token(new TextAttribute(KEYWORD, null, SWT.BOLD));
        Token string = new Token(new TextAttribute(STRING));

        // add tokens for each reserved word
        rule.addWord(IJADConstants.JAD_MIDLET_JAR_SIZE, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_JAR_URL, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_NAME, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_VENDOR, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_VERSION, keyword);
        rule.addWord(IJADConstants.JAD_MICROEDITION_CONFIG, keyword);
        rule.addWord(IJADConstants.JAD_MICROEDITION_PROFILE, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_DATA_SIZE, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_DELETE_CONFIRM, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_DELETE_NOTIFY, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_DESCRIPTION, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_ICON, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_INFO_URL, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_INSTALL_NOTIFY, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_PERMISSIONS, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_PERMISSIONS_OPTIONAL, keyword);
        rule.addWord(IJADConstants.JAD_MIDLET_JAR_RSA_SHA1, keyword);

        setRules(new IRule[] {
                rule,
                new SingleLineRule("\"", "\"", string, '\\'),
                new SingleLineRule("'", "'", string, '\\'),
                new SingleLineRule(" ", null, string, '\\'),
                new WordPatternRule(detector, "MIDlet-", null, keyword),
                new WordPatternRule(detector,
                        IJADConstants.JAD_MIDLET_CERTIFICATE, null, keyword),
                new WhitespaceRule(new IWhitespaceDetector() {
                    public boolean isWhitespace(char c) {
                        return Character.isWhitespace(c);
                    }
                }),

        });
    }
}
