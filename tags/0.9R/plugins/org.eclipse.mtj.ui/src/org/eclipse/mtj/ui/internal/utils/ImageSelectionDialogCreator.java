/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version  
 *     Diego Sandin (Motorola) - Image selector will now only look for images inside 
 *                               source folders                   
 */
package org.eclipse.mtj.ui.internal.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ResourceListSelectionDialog;
import org.eclipse.ui.dialogs.SelectionDialog;

/**
 * @author Diego Madruga Sandin
 */
public class ImageSelectionDialogCreator {

    /**
     * Create a new image selection dialog.
     * 
     * @param shell parent the parent shell of the dialog to be created
     * @param javaProject the project that contains the images to be included
     */
    public static SelectionDialog createImageSelectionDialog(Shell shell,
            IJavaProject javaProject) {

        IResource[] imageResourcesArray;

        final List<IResource> imageResourcesList = new ArrayList<IResource>();
        List<IResource> srcFoldersPaths = new ArrayList<IResource>();
        IClasspathEntry[] classpathEntries = null;

        try {
            classpathEntries = javaProject.getResolvedClasspath(true);
        } catch (JavaModelException e1) {
            e1.printStackTrace();
        }

        for (IClasspathEntry entry : classpathEntries) {
            /* Retrieve all source folders */
            if (entry.getContentKind() == IPackageFragmentRoot.K_SOURCE) {
                IPath srcFolderPath = entry.getPath();

                if (srcFolderPath != javaProject.getPath()) {
                    srcFoldersPaths.add(javaProject.getProject().getFolder(
                            srcFolderPath.removeFirstSegments(1)));
                } else {
                    srcFoldersPaths.add(javaProject.getProject());
                }
            }
        }

        try {

            for (IResource srcFlder : srcFoldersPaths) {
                IResource resource = srcFlder;

                resource.accept(new IResourceProxyVisitor() {

                    public boolean visit(IResourceProxy proxy)
                            throws CoreException {

                        if (proxy.isDerived()) {
                            return false;
                        }
                        int type = proxy.getType();
                        if ((IResource.FILE & type) != 0) {
                            IResource res = proxy.requestResource();

                            String fileExtension = res.getFileExtension();
                            if ((fileExtension != null)
                                    && (fileExtension.equalsIgnoreCase("gif")
                                            || fileExtension
                                                    .equalsIgnoreCase("png") || fileExtension
                                            .equalsIgnoreCase("jpg"))) {

                                imageResourcesList.add(res);
                                return true;
                            }

                            return false;
                        }

                        if (type == IResource.FILE) {
                            return false;
                        }

                        return true;
                    }
                }, IResource.NONE);
            }
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.ERROR, "createImageSelectionDialog", e); //$NON-NLS-1$
        }

        imageResourcesArray = imageResourcesList.toArray(new IResource[0]);

        SelectionDialog dialog = new ResourceListSelectionDialog(shell,
                imageResourcesArray);
        dialog.setInitialSelections(imageResourcesArray);
        dialog.setTitle("Choose Image");
        return dialog;
    }
}
