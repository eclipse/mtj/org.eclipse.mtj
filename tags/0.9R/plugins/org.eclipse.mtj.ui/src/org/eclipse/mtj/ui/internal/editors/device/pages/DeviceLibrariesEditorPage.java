/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.editors.device.pages;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IClasspathAttribute;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.ui.wizards.BuildPathDialogAccess;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.core.importer.LibraryImporter;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.Classpath;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.device.impl.AbstractDevice;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.library.api.API;
import org.eclipse.mtj.core.model.library.impl.Library;
import org.eclipse.mtj.core.persistence.PersistableUtilities;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage;
import org.eclipse.mtj.ui.editors.device.LibraryApiEditorDialog;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.ui.viewers.TableViewerConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

/**
 * Implements an editor page for editing the libraries available for the device
 * being edited.
 * 
 * @author Craig Setera
 */
public class DeviceLibrariesEditorPage extends AbstractDeviceEditorPage {
    private static final Object[] NO_ELEMENTS = new Object[0];

    // Column property names
    private static final String PROP_FILE = "file";
    private static final String PROP_PATH = "path";
    private static final String PROP_APIS = "apis";
    private static final String PROP_JAVADOC = "javadoc";
    private static final String PROP_SOURCE = "source";

    // All of the properties in order
    private static final String[] PROPERTIES = new String[] { PROP_FILE,
            PROP_PATH, PROP_APIS, PROP_JAVADOC, PROP_SOURCE };

    // Column information structure
    private static final int DEFAULT_TABLE_WIDTH = 650;
    private static final TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
            new TableColumnInfo("File", 15f, null),
            new TableColumnInfo("Path", 20f, null),
            new TableColumnInfo("APIs", 15f, null),
            new TableColumnInfo("Javadoc", 25f, null),
            new TableColumnInfo("Source", 25f, null), };

    // A cell modifier implementation for the device libraries editor
    private class CellModifier implements ICellModifier {
        public boolean canModify(Object element, String property) {
            return true;
        }

        public Object getValue(Object element, String property) {
            Object value = null;
            Library library = (Library) element;

            switch (getColumnIndex(property)) {
            case 0:
            case 1:
                value = library.getLibraryFile();
                break;

            case 2:
                value = library.getAPIs();
                break;

            case 3:
            case 4:
                value = library.toClasspathEntry();
                break;
            }

            return value;
        }

        public void modify(Object element, String property, Object value) {
            TableItem item = (TableItem) element;
            Library library = (Library) item.getData();

            switch (getColumnIndex(property)) {
            case 0:
            case 1:
                library.setLibraryFile((File) value);
                break;

            case 2:
                library.setApis((API[]) value);
                break;

            case 3: {
                URL url = getJavadocURL((IClasspathEntry) value);
                library.setJavadocURL(url);
            }
                break;

            case 4: {
                IClasspathEntry entry = (IClasspathEntry) value;
                library
                        .setSourceAttachmentPath(entry
                                .getSourceAttachmentPath());
                library.setSourceAttachmentRootPath(entry
                        .getSourceAttachmentRootPath());
            }
                break;
            }

            viewer.refresh(library, true);
        }

        /**
         * Return the column index for the property.
         * 
         * @param property
         * @return
         */
        private int getColumnIndex(String property) {
            int index = -1;

            for (int i = 0; i < PROPERTIES.length; i++) {
                if (PROPERTIES[i].equals(property)) {
                    index = i;
                    break;
                }
            }

            return index;
        }
    }

    // Content provider for a device's classpath entries
    private static class DeviceClasspathContentProvider implements
            IStructuredContentProvider {
        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            if (inputElement instanceof Classpath) {
                Classpath classpath = (Classpath) inputElement;
                elements = classpath.getEntries();
            }

            return elements;
        }

        public void dispose() {
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    // Cell editor for editing library APIs
    private class APIFileSelectionDialogCellEditor extends DialogCellEditor {
        public APIFileSelectionDialogCellEditor(Composite parent) {
            super(parent);
        }

        protected Object openDialogBox(Control cellEditorWindow) {
            LibraryApiEditorDialog dialog = new LibraryApiEditorDialog(
                    cellEditorWindow.getShell());

            API[] apis = (API[]) doGetValue();
            dialog.setAPIs(apis);

            return (dialog.open() == Dialog.OK) ? dialog.getAPIs() : apis;
        }

        /**
         * @see org.eclipse.jface.viewers.DialogCellEditor#updateContents(java.lang.Object)
         */
        protected void updateContents(Object value) {
            Label defaultLabel = getDefaultLabel();
            if (defaultLabel == null)
                return;

            String text = "";//$NON-NLS-1$
            if (value != null) {
                API[] apis = (API[]) value;
                text = getApisLabel(apis);
            }
            defaultLabel.setText(text);
        }
    }

    // Cell editor for selecting an archive file
    private class ArchiveFileSelectionDialogCellEditor extends DialogCellEditor {
        private boolean filepath;

        public ArchiveFileSelectionDialogCellEditor(Composite parent,
                boolean filepath) {
            super(parent);
            this.filepath = filepath;
        }

        protected Object openDialogBox(Control cellEditorWindow) {
            File value = (File) doGetValue();
            return promptForArchiveFile(cellEditorWindow.getShell(), value);
        }

        /**
         * @see org.eclipse.jface.viewers.DialogCellEditor#updateContents(java.lang.Object)
         */
        protected void updateContents(Object value) {
            Label defaultLabel = getDefaultLabel();
            if (defaultLabel == null)
                return;

            String text = "";//$NON-NLS-1$
            if (value != null) {
                File file = (File) value;
                text = filepath ? file.getParent() : file.getName();
            }
            defaultLabel.setText(text);
        }
    }

    // Label provider for Library instances
    private class LibraryLabelProvider extends LabelProvider implements
            ITableLabelProvider {
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        public String getColumnText(Object element, int columnIndex) {
            Library library = (Library) element;
            String text = "";

            if (library != null) {
                switch (columnIndex) {
                case 0:
                    text = library.getLibraryFile().getName();
                    break;

                case 1:
                    text = library.getLibraryFile().getParent();
                    break;

                case 2:
                    text = getApisLabel(library.getAPIs());
                    break;

                case 3: {
                    URL url = library.getJavadocURL();
                    if (url != null) {
                        text = url.toString();
                    }
                }
                    break;

                case 4: {
                    IPath path = library.getSourceAttachmentPath();
                    if (path != null) {
                        text = path.toString();
                    }
                }
                    break;
                }
            }

            return text;
        }
    }

    // A dialog cell editor for selecting the javadoc URL for a library
    private class JavadocAttachDialogCellEditor extends DialogCellEditor {
        public JavadocAttachDialogCellEditor(Composite parent) {
            super(parent);
        }

        protected Object openDialogBox(Control cellEditorWindow) {
            Shell shell = cellEditorWindow.getShell();

            IClasspathEntry newEntry = null;
            IClasspathEntry entry = (IClasspathEntry) doGetValue();
            if (entry != null) {
                newEntry = BuildPathDialogAccess.configureJavadocLocation(
                        shell, entry);
            }

            return newEntry;
        }

        /**
         * @see org.eclipse.jface.viewers.DialogCellEditor#updateContents(java.lang.Object)
         */
        protected void updateContents(Object value) {
            Label defaultLabel = getDefaultLabel();
            if (defaultLabel == null)
                return;

            String text = "";//$NON-NLS-1$
            if (value != null) {
                IClasspathEntry entry = (IClasspathEntry) value;
                URL url = getJavadocURL(entry);
                if (url != null) {
                    text = url.toString();
                }
            }
            defaultLabel.setText(text);
        }
    }

    // A dialog cell editor for selecting the source locations for a library
    private static class SourceAttachDialogCellEditor extends DialogCellEditor {
        public SourceAttachDialogCellEditor(Composite parent) {
            super(parent);
        }

        protected Object openDialogBox(Control cellEditorWindow) {
            Shell shell = cellEditorWindow.getShell();

            IClasspathEntry newEntry = null;
            IClasspathEntry entry = (IClasspathEntry) doGetValue();
            if (entry != null) {
                newEntry = BuildPathDialogAccess.configureSourceAttachment(
                        shell, entry);
            }

            return newEntry;
        }

        /**
         * @see org.eclipse.jface.viewers.DialogCellEditor#updateContents(java.lang.Object)
         */
        protected void updateContents(Object value) {
            Label defaultLabel = getDefaultLabel();
            if (defaultLabel == null)
                return;

            String text = "";//$NON-NLS-1$
            if (value != null) {
                IClasspathEntry entry = (IClasspathEntry) value;
                IPath attachPath = entry.getSourceAttachmentPath();
                if (attachPath != null) {
                    text = attachPath.toString();
                }
            }
            defaultLabel.setText(text);
        }
    }

    // Widgets
    private TableViewer viewer;

    /**
     * Construct the editor page.
     * 
     * @param parent
     * @param style
     */
    public DeviceLibrariesEditorPage(Composite parent, int style) {
        super(parent, style);
    }

    /**
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#commitDeviceChanges()
     */
    public void commitDeviceChanges() {
        Object viewerInput = viewer.getInput();
        if (viewerInput instanceof Classpath) {
            editDevice.setClasspath((Classpath) viewerInput);
        }
    }

    /**
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#getDescription()
     */
    public String getDescription() {
        return "Specify the libraries that are available for the device";
    }

    /**
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#getTitle()
     */
    public String getTitle() {
        return "Libraries";
    }

    /**
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#setDevice(org.eclipse.mtj.core.model.device.IDevice)
     */
    public void setDevice(IDevice device) {
        super.setDevice(device);

        if (device instanceof AbstractDevice) {
            Classpath classpath = ((AbstractDevice) device).getClasspath();
            try {
                Classpath clone = (Classpath) PersistableUtilities
                        .clonePersistable(classpath);
                viewer.setInput(clone);
            } catch (PersistenceException e) {
                MTJCorePlugin.log(IStatus.WARNING,
                        "Error cloning device classpath", e);
            }
        }
    }

    /**
     * Return the javadoc url for the specified entry.
     * 
     * @param entry
     * @return
     */
    protected URL getJavadocURL(IClasspathEntry entry) {
        URL url = null;

        if ((entry != null) && (entry.getExtraAttributes() != null)) {
            IClasspathAttribute[] attributes = entry.getExtraAttributes();
            for (int i = 0; i < attributes.length; i++) {
                IClasspathAttribute attribute = attributes[i];
                if (attribute.getName().equals(
                        IClasspathAttribute.JAVADOC_LOCATION_ATTRIBUTE_NAME)) {
                    try {
                        url = new URL(attribute.getValue());
                    } catch (MalformedURLException e) {
                        MTJCorePlugin.log(IStatus.WARNING,
                                "Error getting new Javadoc URL", e);
                    }
                }
            }
        }

        return url;
    }

    /**
     * Create the devices table viewer.
     * 
     * @param parent
     */
    private TableViewer createTableViewer(Composite composite) {
        int styles = SWT.SINGLE | SWT.V_SCROLL | SWT.BORDER
                | SWT.FULL_SELECTION;
        Table table = new Table(composite, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        // Wire up the viewer
        TableViewer viewer = new TableViewer(table);
        viewer.setContentProvider(new DeviceClasspathContentProvider());
        viewer.setLabelProvider(new LibraryLabelProvider());

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings("deviceLibrariesViewerSettings");
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 0);
        viewerConfiguration.configure(viewer);

        // Wire up the cell modification handling
        viewer.setCellModifier(new CellModifier());
        viewer.setColumnProperties(PROPERTIES);
        viewer.setCellEditors(new CellEditor[] {
                new ArchiveFileSelectionDialogCellEditor(table, false),
                new ArchiveFileSelectionDialogCellEditor(table, true),
                new APIFileSelectionDialogCellEditor(table),
                new JavadocAttachDialogCellEditor(table),
                new SourceAttachDialogCellEditor(table), });

        return viewer;
    }

    /**
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#addPageControls(org.eclipse.swt.widgets.Composite)
     */
    protected void addPageControls(Composite parent) {
        parent.setLayoutData(new GridData(GridData.FILL_BOTH));
        parent.setLayout(new GridLayout(2, false));

        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.minimumWidth = DEFAULT_TABLE_WIDTH;
        gridData.heightHint = 400;
        viewer = createTableViewer(parent);
        viewer.getTable().setLayoutData(gridData);

        Composite buttonComposite = new Composite(parent, SWT.NONE);
        buttonComposite.setLayout(new GridLayout(1, true));
        buttonComposite.setLayoutData(new GridData(GridData.FILL_VERTICAL));

        Button addButton = new Button(buttonComposite, SWT.PUSH);
        addButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        addButton.setText("Add...");
        addButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                handleAddButton();
            }
        });

        final Button removeButton = new Button(buttonComposite, SWT.PUSH);
        removeButton.setEnabled(false);
        removeButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        removeButton.setText("Remove");
        removeButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                handleRemoveButton();
            }
        });

        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                ILibrary selectedLibrary = getSelectedLibrary();
                removeButton.setEnabled(selectedLibrary != null);
            }
        });
    }

    /**
     * Return the API's for the library.
     * 
     * @param library
     * @return
     */
    private String getApisLabel(API[] apis) {
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < apis.length; i++) {
            API api = apis[i];
            if (i != 0) {
                sb.append(", ");
            }

            sb.append(api);
        }

        return sb.toString();
    }

    /**
     * Return the classpath being edited.
     * 
     * @return
     */
    private Classpath getClasspath() {
        return (Classpath) viewer.getInput();
    }

    /**
     * Return the currently selected library or <code>null</code> if nothing
     * is selected.
     * 
     * @return
     */
    private ILibrary getSelectedLibrary() {
        IStructuredSelection selection = (IStructuredSelection) viewer
                .getSelection();
        return (selection.size() > 0) ? (ILibrary) selection.getFirstElement()
                : null;
    }

    /**
     * Handle the add button being pressed.
     */
    private void handleAddButton() {
        File archiveFile = promptForArchiveFile(getShell(), null);
        if (archiveFile != null) {
            LibraryImporter importer = new LibraryImporter();
            ILibrary library = importer.createLibraryFor(archiveFile);
            getClasspath().addEntry(library);
            viewer.refresh();
        }
    }

    /**
     * Handle the remove button being pressed.
     */
    private void handleRemoveButton() {
        if (MessageDialog.openConfirm(getShell(), "Confirm Remove",
                "Remove the selected library?")) {
            ILibrary selectedLibrary = getSelectedLibrary();
            getClasspath().removeEntry(selectedLibrary);
            viewer.refresh();
        }
    }

    /**
     * Prompt for an archive file.
     * 
     * @param shell
     * @param currentFile
     * @return
     */
    private File promptForArchiveFile(Shell shell, File currentFile) {
        FileDialog fileDialog = new FileDialog(shell);
        fileDialog.setFilterNames(new String[] { "*.jar;*.zip" });

        if ((currentFile != null) && (currentFile.exists())) {
            fileDialog.setFileName(currentFile.toString());
        }

        String filename = fileDialog.open();
        return (filename == null) ? null : new File(filename);
    }
}
