/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)  - Initial implementation
 *     Kevin Hunter (Individual) - Added signature support
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse
 *                                 standards
 *     Hugo Raniere (Motorola)  - Removing Preprocessor code
 *     Gang Ma      (Sybase)	- Add jar/jad names configuration support
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code
 */
package org.eclipse.mtj.ui.internal.properties;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.preverification.builder.PreverificationBuilder;
import org.eclipse.mtj.core.internal.utils.ColonDelimitedProperties;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.jad.ApplicationDescriptor;
import org.eclipse.mtj.core.model.jad.IJADConstants;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSetRegistry;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.core.nature.J2MENature;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.ui.MTJUIErrors;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.devices.DeviceSelector;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPropertyPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PropertyPage;

/**
 * Property page implementation for Java ME properties associated with the
 * project.
 * 
 * @author Craig Setera
 * @see PropertyPage
 */
public class J2MEProjectPropertiesPage extends PropertyPage implements
        IWorkbenchPropertyPage {

    public static final String PAGEID = "org.eclipse.mtj.project.J2MEPropertiesPage";

    // Comparator for definition sets based on name
    private static class DefinitionSetComparator implements Comparator<Object>,
            Serializable {

        private static final long serialVersionUID = 1L;

        /*
         * (non-Javadoc)
         * 
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        public int compare(Object obj1, Object obj2) {
            SymbolDefinitionSet set1 = (SymbolDefinitionSet) obj1;
            SymbolDefinitionSet set2 = (SymbolDefinitionSet) obj2;

            return set1.getName().compareTo(set2.getName());
        }
    }

    // Label provider wrapped around a SymbolDefinitionSet
    private static class SymbolDefinitionSetLabelProvider extends LabelProvider {

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
         */
        @Override
        public String getText(Object element) {
            String text = "";

            if (element instanceof SymbolDefinitionSet) {
                text = ((SymbolDefinitionSet) element).getName();
            }

            return text;
        }
    }

    // A content provider wrapped around the available symbol definitions.
    private static class SymbolDefinitionSetsContentProvider implements
            IStructuredContentProvider {

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
            // Nothing to do
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            try {
                elements = SymbolDefinitionSetRegistry.singleton
                        .getAllSetDefinitions();
                Arrays.sort(elements, new DefinitionSetComparator());
            } catch (PersistenceException e) {
                MTJCorePlugin.log(IStatus.WARNING, e);
            }

            return elements;
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer,
         *      java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
            // Nothing to do
        }
    }

    private static final Object[] NO_ELEMENTS = new Object[0];

    private DeviceSelector deviceSelector;

    private ComboViewer symbolDefinitionSetViewer;

    private Text jadFileNameText;
    private Text jarFileNameText;

    /**
     * Valid the jad file name and jar file name
     */
    private boolean fileNameValid = true;

    /**
     * Returns <code>true</code> if the page data is currently valid.
     * 
     * @see org.eclipse.jface.preference.PreferencePage#isValid()
     */
    @Override
    public boolean isValid() {
        return (deviceSelector.getSelectedDevice() != null) && fileNameValid;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        boolean succeeded = false;
        IProject project = getProject();

        try {
            if (J2MENature.hasJ2MENature(project)) {
                ProgressMonitorDialog dialog = new ProgressMonitorDialog(
                        getShell());
                dialog.setCancelable(false);
                dialog.setOpenOnRun(true);

                try {
                    final IDevice device = deviceSelector.getSelectedDevice();
                    final IMidletSuiteProject midletProject = getMidletSuiteProject();
                    final String newJadFileName = jadFileNameText.getText();
                    final String newJarFileName = jarFileNameText.getText();
                    dialog.run(true, false, new IRunnableWithProgress() {

                        /*
                         * (non-Javadoc)
                         * 
                         * @see org.eclipse.jface.operation.IRunnableWithProgress#run(org.eclipse.core.runtime.IProgressMonitor)
                         */
                        public void run(IProgressMonitor monitor)
                                throws InvocationTargetException,
                                InterruptedException {

                            try {
                                midletProject.setDevice(device, monitor);
                                boolean needCleanProject = false;
                                // the jad file name has been changed
                                if (!midletProject.getJadFileName()
                                        .equalsIgnoreCase(newJadFileName)) {

                                    // must set the new jad file's location
                                    midletProject
                                            .setJadFileName(newJadFileName);
                                }
                                // the jar file name has been changed
                                if (!midletProject.getJarFilename()
                                        .equalsIgnoreCase(newJarFileName)) {
                                    ApplicationDescriptor appDescriptor = midletProject
                                            .getApplicationDescriptor();
                                    ColonDelimitedProperties jadProperties = appDescriptor
                                            .getManifestProperties();
                                    // Update the jar file URL
                                    jadProperties.setProperty(
                                            IJADConstants.JAD_MIDLET_JAR_URL,
                                            newJarFileName);

                                    try {
                                        appDescriptor.store();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    needCleanProject = true;
                                }

                                midletProject.saveMetaData();

                                // need to clean the project
                                if (needCleanProject) {
                                    PreverificationBuilder.cleanProject(
                                            midletProject.getProject(), true,
                                            monitor);
                                }
                                midletProject.getProject().build(
                                        IncrementalProjectBuilder.FULL_BUILD,
                                        monitor);

                            } catch (CoreException e) {
                                throw new InvocationTargetException(e);
                            }

                            try {
                                final SymbolDefinitionSet[] setholder = new SymbolDefinitionSet[1];
                                Display.getDefault().syncExec(new Runnable() {
                                    public void run() {
                                        setholder[0] = getSelectedDefinitionSet();
                                    }
                                });

                                midletProject
                                        .setEnabledSymbolDefinitionSet(setholder[0]);
                            } catch (CoreException e) {
                                throw new InvocationTargetException(e);
                            } catch (PersistenceException e) {
                                throw new InvocationTargetException(e);
                            }
                        }
                    });

                    succeeded = true;

                } catch (InvocationTargetException e) {
                    MTJUIErrors.displayError(getShell(),
                            "MTJUiError.Exception", //$NON-NLS-1$
                            "MTJUiError.SetPlatformFailed", //$NON-NLS-1$
                            e);
                } catch (InterruptedException e) {
                    // Ignore this
                } finally {
                    dialog.close();
                }
            }
        } catch (CoreException e) {
            MTJUIErrors.displayError(getShell(), "MTJUiError.Exception", //$NON-NLS-1$
                    "MTJUiError.SetPlatformFailed", //$NON-NLS-1$
                    e);
        }

        return succeeded;
    }

    /**
     * add jad/jar filename configuration control to the page
     * 
     * @param parent
     */
    private void addJadJarConfigure(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        composite.setLayout(new GridLayout(2, false));

        IMidletSuiteProject midletProject = getMidletSuiteProject();

        new Label(composite, SWT.NONE).setText(MTJUIStrings
                .getString("J2MEProjectPropertiesPage.JADFileName"));
        jadFileNameText = new Text(composite, SWT.BORDER);
        jadFileNameText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        jadFileNameText.setText(midletProject.getJadFileName());
        jadFileNameText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validatePage();
            }
        });

        new Label(composite, SWT.NONE).setText(MTJUIStrings
                .getString("J2MEProjectPropertiesPage.JarFileName"));
        jarFileNameText = new Text(composite, SWT.BORDER);
        jarFileNameText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        jarFileNameText.setText(midletProject.getJarFilename());
        jarFileNameText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validatePage();
            }
        });

    }

    /**
     * Add the symbol selector information if it is in use.
     * 
     * @param parent
     */
    private void addSymbolSelector(Composite parent) {
        if (isPreprocessingEnabled(getProject())) {
            Composite composite = new Composite(parent, SWT.NONE);
            composite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
            composite.setLayout(new GridLayout(2, false));

            new Label(composite, SWT.NONE).setText("Symbol Definitions:");

            symbolDefinitionSetViewer = new ComboViewer(composite,
                    SWT.READ_ONLY);
            symbolDefinitionSetViewer.getCombo().setLayoutData(
                    new GridData(GridData.FILL_HORIZONTAL));
            symbolDefinitionSetViewer
                    .setContentProvider(new SymbolDefinitionSetsContentProvider());
            symbolDefinitionSetViewer
                    .setLabelProvider(new SymbolDefinitionSetLabelProvider());
            symbolDefinitionSetViewer.setInput(new Object());

            // Attempt to get the current setting and use it to set the
            // current selection
            try {
                SymbolDefinitionSet currentSet = getMidletSuiteProject()
                        .getEnabledSymbolDefinitionSet();
                if (currentSet != null) {
                    IStructuredSelection selection = new StructuredSelection(
                            currentSet);
                    symbolDefinitionSetViewer.setSelection(selection, true);
                }
            } catch (CoreException e) {
                MTJCorePlugin.log(IStatus.WARNING, e);
            } catch (PersistenceException e) {
                MTJCorePlugin.log(IStatus.WARNING, e);
            }
        }
    }

    /**
     * Get the MIDlet suite project for this project.
     * 
     * @return
     */
    private IMidletSuiteProject getMidletSuiteProject() {
        IJavaProject javaProject = JavaCore.create(getProject());
        IMidletSuiteProject midletProject = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);

        return midletProject;
    }

    /**
     * Get the selected project or <code>null</code> if a project is not
     * selected.
     * 
     * @return
     */
    private IProject getProject() {
        IProject project = null;
        IAdaptable adaptable = getElement();

        if (adaptable instanceof IProject) {
            project = (IProject) adaptable;
        } else if (adaptable instanceof IJavaProject) {
            project = ((IJavaProject) adaptable).getProject();
        }

        return project;
    }

    /**
     * Return the selected symbol definition set.
     * 
     * @return
     */
    private SymbolDefinitionSet getSelectedDefinitionSet() {
        SymbolDefinitionSet set = null;

        if (isPreprocessingEnabled(getProject())) {
            IStructuredSelection selection = (IStructuredSelection) symbolDefinitionSetViewer
                    .getSelection();
            if (selection.size() > 0) {
                set = (SymbolDefinitionSet) selection.getFirstElement();
            }
        }

        return set;
    }

    /**
     * Return a boolean indicating whether the selected element is a J2ME
     * project.
     * 
     * @param project
     * @return
     */
    private boolean isJ2MEProject(IProject project) {
        boolean j2meProject = false;

        if (project != null) {
            try {
                j2meProject = project
                        .hasNature(IMTJCoreConstants.J2ME_NATURE_ID);
            } catch (CoreException e) {
            }
        }

        return j2meProject;
    }

    /**
     * Return a boolean indicating whether this is a J2ME project with
     * preprocessing enabled.
     * 
     * @param project
     * @return
     */
    private boolean isPreprocessingEnabled(IProject project) {
        boolean enabled = false;

        if (project != null) {
            try {
                enabled = isJ2MEProject(project)
                        && project
                                .hasNature(IMTJCoreConstants.J2ME_PREPROCESSING_NATURE_ID);
            } catch (CoreException e) {
                MTJCorePlugin.log(IStatus.WARNING, e);
            }
        }

        return enabled;
    }

    /**
     * Return a boolean indicating whether the controls should be read only for
     * the specified project.
     * 
     * @param project
     * @return
     */
    private boolean isReadOnly(IProject project) {
        boolean readOnly = false;

        try {
            readOnly = project
                    .hasNature(IMTJCoreConstants.J2ME_PREPROCESSED_NATURE_ID);
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.WARNING, e);
        }

        return readOnly;
    }

    /**
     * check whether the page is valid
     */
    private void validatePage() {
        String message = null;

        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        IStatus result;
        String jadFileName = jadFileNameText.getText();
        result = workspace.validateName(jadFileName, IResource.FILE);
        if (!result.isOK()) {
            message = result.getMessage();
        } else if (!jadFileName.endsWith(".jad")) {
            message = "JAD file name must end with .jad";
        }

        String jarFileName = jarFileNameText.getText();
        result = workspace.validateName(jarFileName, IResource.FILE);
        if (!result.isOK()) {
            message = result.getMessage();
            ;
        } else if (!jarFileName.endsWith(".jar")) {
            message = "Jar file name must end with .jar";
        }

        setErrorMessage(message);
        if (message != null) {
            fileNameValid = false;
        } else {
            fileNameValid = true;
        }
        updateApplyButton();
        getContainer().updateButtons();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        Control control = null;

        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_J2MEProjectPropertiesPage");

        IProject project = getProject();

        if (isJ2MEProject(project)) {
            Composite composite = new Composite(parent, SWT.NONE);
            composite.setLayout(new GridLayout(1, true));

            // Create the device selector
            deviceSelector = new DeviceSelector();
            deviceSelector.createContents(composite, true, true);
            deviceSelector
                    .setSelectionChangedListener(new ISelectionChangedListener() {
                        public void selectionChanged(SelectionChangedEvent event) {
                            // update container state
                            if (getContainer() != null) {
                                getContainer().updateButtons();
                            }

                            // update page state
                            updateApplyButton();
                        }
                    });

            // Get the associated MIDlet suite project
            IMidletSuiteProject midletProject = getMidletSuiteProject();

            deviceSelector.setSelectedDevice(midletProject.getDevice());

            deviceSelector.setEnabled(!isReadOnly(project));

            addSymbolSelector(composite);

            // add jad/jar file name configuration control
            addJadJarConfigure(composite);
            control = composite;

        } else {
            Label lbl = new Label(parent, SWT.NONE);
            lbl
                    .setText(MTJUIStrings
                            .getString("J2MEProjectPropertiesPage.NotMidletSuiteProject")); //$NON-NLS-1$
            control = lbl;
        }

        return control;
    }

}
