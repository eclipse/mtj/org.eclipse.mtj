/**
 * Copyright (c) 2007 mFoundry
 * All Rights Reserved.
 */

package org.eclipse.mtj.ui.internal.preferences;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

/**
 * A field editor capable of selecting and remembering either a workspace-based
 * file or file system file.
 * <p />
 * Copyright (c) 2007 mFoundry All Rights Reserved. <br>
 * 
 * @author Craig Setera
 * @version $Id$
 */
public class WorkspaceAndExternalFileFieldEditor extends FieldEditor {
    private static final IStatus OK_STATUS = MTJCorePlugin.newStatus(
            IStatus.OK, 0, "");
    private static final IStatus ERROR_STATUS = MTJCorePlugin.newStatus(
            IStatus.ERROR, 1, "Select a single XML file");

    private Group group;
    private Text fileText;
    private Button browseExternalButton;
    private Button browseWorkspaceButton;
    private String[] filterExtensions;

    public WorkspaceAndExternalFileFieldEditor(String name, String labelText,
            String[] filterExtensions, Composite parent) {
        super(name, labelText, parent);
        this.filterExtensions = filterExtensions;
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#adjustForNumColumns(int)
     */
    protected void adjustForNumColumns(int numColumns) {
        ((GridData) group.getLayoutData()).horizontalSpan = numColumns;
    }

    /**
     * Browse the external file system for a file.
     */
    protected void browseExternal() {
        FileDialog dialog = new FileDialog(fileText.getShell());
        dialog.setText("Select File");
        dialog.setFilterExtensions(filterExtensions);

        String filename = dialog.open();
        if (filename != null) {
            fileText.setText(filename);
        }
    }

    /**
     * Browse the workspace for a file.
     */
    protected void browseWorkspace() {
        ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
                fileText.getShell(), new WorkbenchLabelProvider(),
                new WorkbenchContentProvider());

        ISelectionStatusValidator validator = new ISelectionStatusValidator() {
            public IStatus validate(Object[] selection) {
                IStatus status = ERROR_STATUS;

                if (selection.length == 1) {
                    if (selection[0] instanceof IFile) {
                        IFile file = (IFile) selection[0];

                        if (file.getFullPath().getFileExtension().equals("xml")) {
                            status = OK_STATUS;
                        }
                    }
                }

                return status;
            }
        };

        dialog.setTitle("Select File");
        dialog.setMessage("Select File");
        dialog.setInput(ResourcesPlugin.getWorkspace().getRoot());
        dialog.setAllowMultiple(false);
        dialog.setEmptyListMessage("Must select file");
        dialog.setStatusLineAboveButtons(true);
        dialog.setValidator(validator);

        if (dialog.open() == ElementTreeSelectionDialog.OK) {
            Object[] result = dialog.getResult();

            IFile theFile = (IFile) result[0];
            IPath fullPath = theFile.getFullPath();

            fileText.setText(fullPath.toString());
        }
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#doFillIntoGrid(org.eclipse.swt.widgets.Composite,
     *      int)
     */
    protected void doFillIntoGrid(Composite parent, int numColumns) {
        group = new Group(parent, SWT.NONE);
        group.setText(getLabelText());
        group.setLayout(new GridLayout(3, false));

        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = numColumns;
        group.setLayoutData(gd);

        fileText = new Text(group, SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.minimumWidth = 350;
        fileText.setLayoutData(gd);

        browseWorkspaceButton = new Button(group, SWT.PUSH);
        browseWorkspaceButton.setText("Workspace...");
        browseWorkspaceButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                browseWorkspace();
            }
        });

        browseExternalButton = new Button(group, SWT.PUSH);
        browseExternalButton.setText("External...");
        browseExternalButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                browseExternal();
            }
        });
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#doLoad()
     */
    protected void doLoad() {
        // TODO Auto-generated method stub

    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#doLoadDefault()
     */
    protected void doLoadDefault() {
        // TODO Auto-generated method stub

    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#doStore()
     */
    protected void doStore() {
        // TODO Auto-generated method stub

    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#getNumberOfControls()
     */
    public int getNumberOfControls() {
        return 3;
    }
}
