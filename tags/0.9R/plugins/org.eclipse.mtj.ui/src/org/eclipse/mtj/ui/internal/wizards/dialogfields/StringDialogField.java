/*******************************************************************************
 * Copyright (c) 2000, 2005 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Adapted code available in JDT
 */
package org.eclipse.mtj.ui.internal.wizards.dialogfields;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * Dialog field containing a label and a text control.
 */
public class StringDialogField extends DialogField {

    /**
     * @param span
     * @return
     */
    protected static GridData gridDataForText(int span) {
        GridData gd = new GridData();
        gd.horizontalAlignment = GridData.FILL;
        gd.grabExcessHorizontalSpace = false;
        gd.horizontalSpan = span;
        return gd;
    }

    private ModifyListener fModifyListener;
    private String fText;

    private Text fTextControl;

    /**
     * 
     */
    public StringDialogField() {
        super();
        fText = ""; //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.wizards.testing.dialogfields.DialogField#doFillIntoGrid(org.eclipse.swt.widgets.Composite, int)
     */
    @Override
    public Control[] doFillIntoGrid(Composite parent, int nColumns) {
        assertEnoughColumns(nColumns);

        Label label = getLabelControl(parent);
        label.setLayoutData(gridDataForLabel(1));
        Text text = getTextControl(parent);
        text.setLayoutData(gridDataForText(nColumns - 1));

        return new Control[] { label, text };
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.wizards.testing.dialogfields.DialogField#getNumberOfControls()
     */
    @Override
    public int getNumberOfControls() {
        return 2;
    }

    /**
     * Gets the text. Can not be <code>null</code>
     */
    public String getText() {
        return fText;
    }

    /**
     * Creates or returns the created text control.
     * 
     * @param parent The parent composite or <code>null</code> when the widget
     *            has already been created.
     */
    public Text getTextControl(Composite parent) {
        if (fTextControl == null) {
            assertCompositeNotNull(parent);
            fModifyListener = new ModifyListener() {
                public void modifyText(ModifyEvent e) {
                    doModifyText(e);
                }
            };

            fTextControl = new Text(parent, SWT.SINGLE | SWT.BORDER);
            fTextControl.setText(fText);
            fTextControl.setFont(parent.getFont());
            fTextControl.addModifyListener(fModifyListener);

            fTextControl.setEnabled(isEnabled());
        }
        return fTextControl;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jdt.internal.ui.wizards.dialogfields.DialogField#refresh()
     */
    @Override
    public void refresh() {
        super.refresh();
        if (isOkToUse(fTextControl)) {
            setTextWithoutUpdate(fText);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.wizards.testing.dialogfields.DialogField#setFocus()
     */
    @Override
    public boolean setFocus() {
        if (isOkToUse(fTextControl)) {
            fTextControl.setFocus();
            fTextControl.setSelection(0, fTextControl.getText().length());
        }
        return true;
    }

    /**
     * Sets the text. Triggers a dialog-changed event.
     */
    public void setText(String text) {
        fText = text;
        if (isOkToUse(fTextControl)) {
            fTextControl.setText(text);
        } else {
            dialogFieldChanged();
        }
    }

    /**
     * Sets the text without triggering a dialog-changed event.
     */
    public void setTextWithoutUpdate(String text) {
        fText = text;
        if (isOkToUse(fTextControl)) {
            fTextControl.removeModifyListener(fModifyListener);
            fTextControl.setText(text);
            fTextControl.addModifyListener(fModifyListener);
        }
    }

    /**
     * @param e
     */
    private void doModifyText(ModifyEvent e) {
        if (isOkToUse(fTextControl)) {
            fText = fTextControl.getText();
        }
        dialogFieldChanged();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.wizards.testing.dialogfields.DialogField#updateEnableState()
     */
    @Override
    protected void updateEnableState() {
        super.updateEnableState();
        if (isOkToUse(fTextControl)) {
            fTextControl.setEnabled(isEnabled());
        }
    }

}
