/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.ui.internal.forms.blocks;

import java.util.ArrayList;

import org.eclipse.mtj.ui.internal.editors.EditorsUIContent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * @author Diego Madruga Sandin
 */
public class ButtonBarBlock {

    /**
     * All buttons will be disabled
     */
    public static final int BUTTON_NONE = 0;

    /**
     * Add Button
     */
    public static final int BUTTON_ADD = 1 << 1;;

    public static final int BUTTON_ADD_INDEX = 0;

    /**
     * Button
     */
    public static final int BUTTON_REMOVE = 1 << 2;

    public static final int BUTTON_REMOVE_INDEX = 1;

    /**
     * Button
     */
    public static final int BUTTON_UP = 1 << 3;

    public static final int BUTTON_UP_INDEX = 2;

    /**
     * Button
     */
    public static final int BUTTON_DOWN = 1 << 4;

    public static final int BUTTON_DOWN_INDEX = 3;

    private ArrayList<Button> buttons;

    /**
     * @param parent
     * @param toolkit
     * @param enabledButtons
     */
    public ButtonBarBlock(Composite parent, FormToolkit toolkit,
            int enabledButtons) {

        initialize(parent, toolkit);

        if ((enabledButtons & BUTTON_ADD) != 0) {
            setEnabled(BUTTON_ADD_INDEX, true);
        }
        if ((enabledButtons & BUTTON_REMOVE) != 0) {
            setEnabled(BUTTON_REMOVE_INDEX, true);
        }
        if ((enabledButtons & BUTTON_UP) != 0) {
            setEnabled(BUTTON_UP_INDEX, true);
        }
        if ((enabledButtons & BUTTON_DOWN) != 0) {
            setEnabled(BUTTON_DOWN_INDEX, true);
        }
    }

    /**
     * @param button
     * @param eventType
     * @param listener
     */
    public void addButtonListener(int button, int eventType, Listener listener) {
        buttons.get(button).addListener(eventType, listener);
    }

    /**
     * @param index
     * @return
     */
    public Button getButton(int index) {
        return buttons.get(index);
    }

    /**
     * @param index
     * @return
     */
    public boolean isButtonEnabled(int index) {
        return buttons.get(index).isEnabled();
    }

    /**
     * @param index
     * @param listener
     */
    public void setButtonMouseListener(int index, MouseListener listener) {
        buttons.get(index).addMouseListener(listener);
    }

    /**
     * @param index
     * @param enabled
     */
    public void setEnabled(int index, boolean enabled) {
        buttons.get(index).setEnabled(enabled);
    }

    /**
     * @param parent
     * @param toolkit
     */
    private void initialize(Composite parent, FormToolkit toolkit) {

        buttons = new ArrayList<Button>(4);

        Button add = toolkit.createButton(parent,
                EditorsUIContent.buttonBarBlock_button_add, SWT.PUSH);
        add.setEnabled(false);
        buttons.add(add);

        Button remove = toolkit.createButton(parent,
                EditorsUIContent.buttonBarBlock_button_remove, SWT.PUSH);
        remove.setEnabled(false);
        buttons.add(remove);

        Button up = toolkit.createButton(parent,
                EditorsUIContent.buttonBarBlock_button_up, SWT.PUSH);
        up.setEnabled(false);
        buttons.add(up);

        Button down = toolkit.createButton(parent,
                EditorsUIContent.buttonBarBlock_button_down, SWT.PUSH);
        down.setEnabled(false);
        buttons.add(down);
    }
}
