/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Gustavo de Paula (Motorola)  - Adapt to new AbstractDevice interface
 *                                
 */
package org.eclipse.mtj.toolkit.me4se;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.launching.ILaunchConstants;
import org.eclipse.mtj.core.model.LaunchEnvironment;
import org.eclipse.mtj.core.model.ReplaceableParametersProcessor;
import org.eclipse.mtj.core.model.device.impl.JavaEmulatorDevice;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * Device implementation for the ME4SE SDK device.
 * 
 * @author Craig Setera
 */
public class ME4SEDevice extends JavaEmulatorDevice {
    private File jarFile;

    /**
     * @see org.eclipse.mtj.core.model.device.IDevice#getLaunchCommand(org.eclipse.mtj.core.model.LaunchEnvironment,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException {
    	
        IMidletSuiteProject midletSuite = launchEnvironment.getMidletSuite();
        ILaunchConfiguration launchConfiguration = launchEnvironment
                .getLaunchConfiguration();
        
        boolean launchFromJAD = shouldDirectLaunchJAD(launchConfiguration);
        File tempDeployed = copyForLaunch(midletSuite, monitor, launchFromJAD);

        // Fill in our properties so we can use them for resolution
        // against the raw executable command line
        Map<String, Comparable<?>> executionProperties = new HashMap<String, Comparable<?>>();
        executionProperties.put("executable", getJavaExecutable());
        executionProperties.put("me4sejar", jarFile);
        executionProperties.put("classpath", getProjectClasspathString(
                midletSuite, tempDeployed, monitor));

        // Debug information
        if (launchEnvironment.isDebugLaunch()) {
            executionProperties.put("debugPort", new Integer(launchEnvironment
                    .getDebugListenerPort()));
        }

        // Add launch configuration values
        String extraArguments = launchConfiguration.getAttribute(
                ILaunchConstants.LAUNCH_PARAMS, "");
        executionProperties.put("userSpecifiedArguments", extraArguments);

        if (shouldDirectLaunchJAD(launchConfiguration)) {
            executionProperties.put("jadfile",
                    getSpecifiedJadURL(launchConfiguration));
        } else {
            File jadFile = getJadForLaunch(midletSuite, tempDeployed, monitor);
            if (jadFile.exists()) {
                executionProperties.put("jadfile", jadFile.toString());
            }
        }

        // Do the property resolution given the previous information
        return ReplaceableParametersProcessor.processReplaceableValues(
                launchCommandTemplate, executionProperties);
    }

    /**
     * @return Returns the root.
     */
    public File getJarFile() {
        return jarFile;
    }

    /**
     * @see org.eclipse.mtj.core.model.device.impl.AbstractDevice#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.loadUsing(persistenceProvider);

        String jarFileName = persistenceProvider.loadString("jarFile");
        jarFile = new File(jarFileName);
    }

    /**
     * @param jar The mppRoot to set.
     */
    public void setJarFile(File jar) {
        this.jarFile = jar;
    }

    /**
     * @see org.eclipse.mtj.core.model.device.impl.AbstractDevice#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.storeUsing(persistenceProvider);
        persistenceProvider.storeString("jarFile", jarFile.toString());
    }

    /**
     * @see org.eclipse.mtj.core.model.device.impl.AbstractDevice#getProjectClasspathString(org.eclipse.mtj.core.model.project.IMidletSuiteProject,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    protected String getProjectClasspathString(IMidletSuiteProject midletSuite,
            File temporaryDirectory, IProgressMonitor monitor)
            throws CoreException {
        StringBuffer sb = new StringBuffer(super.getProjectClasspathString(
                midletSuite, temporaryDirectory, monitor));
        sb.append(File.pathSeparatorChar);
        sb.append(jarFile);

        return sb.toString();
    }
}
