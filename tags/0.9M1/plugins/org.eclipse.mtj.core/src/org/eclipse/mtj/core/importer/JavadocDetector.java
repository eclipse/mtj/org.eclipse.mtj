/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma 		(Sybase) - Initial implementation
 *     
 */
package org.eclipse.mtj.core.importer;

import java.io.File;
import java.io.FileFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.eclipse.mtj.core.model.library.ILibrary;

/**
 * helper class to detect the javadoc location
 * 
 * @author gma
 */
public class JavadocDetector {
    ArrayList<IJavadocSearchStrategy> searchStrategies = new ArrayList<IJavadocSearchStrategy>();

    public JavadocDetector() {

    }

    /**
     * add the javadoc searchStrategy to the detector
     * 
     * @param searchStrategy
     * @return
     */
    public JavadocDetector addJavadocSearchStrategy(
            IJavadocSearchStrategy searchStrategy) {
        searchStrategies.add(searchStrategy);
        return this;
    }

    public URL detectJavadoc(ILibrary library) {

        URL retURL = null;
        for (IJavadocSearchStrategy strategy : searchStrategies) {
            retURL = strategy.searchForJavaDoc(library);
            if (retURL != null)
                break;
        }
        return retURL;
    }

    /**
     * a generic javadoc search strategy, it search the library's javadoc on the
     * local file system,
     * 
     * @author gma
     */
    public static class GenericLocalFSSearch implements IJavadocSearchStrategy {
        // the common midp or cldc library's javadoc directory name contains
        // the strings: midp,jsr118
        public static final String[] PROFILEJAVADOCDIRECTORIES = new String[] {
                "midp", "jsr118" };

        // this constant indicates the depth for the search of javadoc directory
        // from the library file
        public static final int DEFAULTSEARCHDEPTH = 5;

        // the SDK's javadoc's root directories
        private File[] docRootDirectories = null;

        public GenericLocalFSSearch() {
        }

        public GenericLocalFSSearch(String docRootDirectoryPath) {
            this(new String[] { docRootDirectoryPath });
        }

        public GenericLocalFSSearch(String[] docrootDirectoryPaths) {
            if (docrootDirectoryPaths != null) {
                docRootDirectories = new File[docrootDirectoryPaths.length];
                for (int i = 0; i < docrootDirectoryPaths.length; i++) {
                    docRootDirectories[i] = new File(docrootDirectoryPaths[i]);
                }
            }

        }

        /**
         * search from the directory which name contains "doc" and in the "doc"
         * directory, recursively search the subdirectory whose name is similar
         * to the library's name.
         */
        public URL searchForJavaDoc(ILibrary library) {
            URL foundURL = null;

            File[] rootDirs = getDocRootDirectories(library);
            // a javadoc root directory exist
            if (rootDirs != null) {
                for (File docDir : rootDirs) {
                    foundURL = searchForLibraryDoc(docDir, library);
                    if (foundURL != null)
                        break;
                }
            }

            return foundURL;
        }

        private URL searchForLibraryDoc(File parentDir, final ILibrary library) {
            String libFileName = library.toFile().getName();
            int dotIdx = libFileName.lastIndexOf('.');
            final String libName;
            if (dotIdx > -1)
                libName = libFileName.substring(0, dotIdx);
            else
                libName = libFileName;

            File[] files = parentDir.listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    boolean accept = false;
                    // if the directory's name starts with the library's name
                    // or the library's name starts with the directory's name
                    // it is may be the library's javadoc directory, it is just
                    // a very simple strategy
                    accept = pathname.getName().startsWith(libName)
                            || libName.startsWith(pathname.getName());

                    // if not found and the library has configuration or profile
                    // API, deal with it specially
                    if (!accept
                            && (library.hasConfiguration() || library
                                    .hasProfile())) {
                        for (int i = 0; i < PROFILEJAVADOCDIRECTORIES.length; i++) {
                            if (pathname.getName().startsWith(
                                    PROFILEJAVADOCDIRECTORIES[i])) {
                                accept = true;
                                break;
                            }
                        }
                    }

                    return accept;

                }
            });

            if (files != null && files.length > 0) {
                try {
                    return files[0].toURI().toURL();
                } catch (MalformedURLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else {
                // Now recurse to sub directories
                File[] subdirectories = parentDir.listFiles(new FileFilter() {
                    public boolean accept(File pathname) {
                        return pathname.isDirectory();
                    }
                });
                for (int i = 0; i < subdirectories.length; i++) {
                    return searchForLibraryDoc(subdirectories[i], library);
                }
            }
            return null;
        }

        /**
         * @param library
         * @return the SDK's javadoc root directory
         */
        public File[] getDocRootDirectories(ILibrary library) {
            if (docRootDirectories == null) {
                docRootDirectories = getDefaultDocRootDirectories(library);
            }
            return docRootDirectories;
        }

        // get the default javadoc directory for the library
        public File[] getDefaultDocRootDirectories(ILibrary library) {
            if (library == null)
                return null;

            ArrayList<File> docfiles = new ArrayList<File>();
            File parentDir = library.toFile().getParentFile();
            for (int i = 0; i < DEFAULTSEARCHDEPTH && parentDir != null; i++) {
                File[] tmpDocfiles = parentDir.listFiles(new FileFilter() {
                    public boolean accept(File pathname) {
                        // find the directory whose name contains "doc" string
                        return pathname.getName().indexOf("doc") != -1;
                    }
                });
                for (File file : tmpDocfiles) {
                    docfiles.add(file);
                }
                parentDir = parentDir.getParentFile();

            }

            if (docfiles.size() > 0)
                return docfiles.toArray(new File[0]);
            else
                return null;

        }

    }

    /**
     * a javadoc search strategy interface, user can implement this interface to
     * provide different javadoc search strategy such as search the library's
     * javadoc on local file system or search the javadoc on the website.
     * 
     * @author gma
     */
    public static interface IJavadocSearchStrategy {
        /**
         * search the library's javadoc
         * 
         * @param library
         * @return
         */
        URL searchForJavaDoc(ILibrary library);
    }

}
