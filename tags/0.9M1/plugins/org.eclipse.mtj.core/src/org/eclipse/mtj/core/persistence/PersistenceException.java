/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID
 */
package org.eclipse.mtj.core.persistence;

/**
 * An exception that may occur during the process of persisting platform
 * components.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */
public class PersistenceException extends Exception {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new exception.
     */
    public PersistenceException() {
        super();
    }

    /**
     * Construct a new exception.
     * 
     * @param message
     */
    public PersistenceException(String message) {
        super(message);
    }

    /**
     * Construct a new exception.
     * 
     * @param cause
     */
    public PersistenceException(Throwable cause) {
        super(cause);
    }

    /**
     * Construct a new exception.
     * 
     * @param message
     * @param cause
     */
    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }
}
