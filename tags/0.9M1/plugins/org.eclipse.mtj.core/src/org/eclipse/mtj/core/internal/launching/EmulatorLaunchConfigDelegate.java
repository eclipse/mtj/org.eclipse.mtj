/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Gang Ma 		(Sybase) 	- Add check debugger setting support
 */
package org.eclipse.mtj.core.internal.launching;

import java.io.File;
import java.util.HashMap;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.launching.AbstractJavaLaunchConfigurationDelegate;
import org.eclipse.jdt.launching.IVMRunner;
import org.eclipse.jdt.launching.VMRunnerConfiguration;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.MTJCoreStrings;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.launching.ILaunchConstants;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.jad.ApplicationDescriptor;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * Launch configuration for launching a Wireless Toolkit emulator that launches
 * as an executable rather than a Java class.
 * 
 * @author Craig Setera
 */
public class EmulatorLaunchConfigDelegate extends
        AbstractJavaLaunchConfigurationDelegate {
    /** Status code for which the no midlet status is registered. */
    private static final IStatus NO_MIDLET_STATUS = new Status(IStatus.ERROR,
            IMTJCoreConstants.PLUGIN_ID,
            IMTJCoreConstants.ERR_OTA_NO_MIDLETS, "", null);

    /**
     * Construct a new Executable emulator launch config delegate.
     */
    public EmulatorLaunchConfigDelegate() {
        super();
    }

    /**
     * @see org.eclipse.mtj.core.launching.AbstractEmulatorLaunchConfigDelegate#getVMRunner(org.eclipse.debug.core.ILaunchConfiguration,
     *      java.lang.String)
     */
    protected IVMRunner getVMRunner(ILaunchConfiguration launchConfig,
            IDevice device, String mode) throws CoreException {
        // Set up the VM runner
        IMidletSuiteProject suite = getMidletSuite(launchConfig);
        return new EmulatorRunner(suite, device, mode);
    }

    /**
     * @see org.eclipse.mtj.core.launching.AbstractEmulatorLaunchConfigDelegate#getVMRunnerConfiguration(org.eclipse.debug.core.ILaunchConfiguration,
     *      java.lang.String)
     */
    protected VMRunnerConfiguration getVMRunnerConfiguration(
            ILaunchConfiguration launchConfig, IDevice device, String mode)
            throws CoreException {
        File workingDir = verifyWorkingDirectory(launchConfig);
        String workingDirName = null;
        if (workingDir != null) {
            workingDirName = workingDir.getAbsolutePath();
        }

        // Create VM config
        VMRunnerConfiguration runConfig = new VMRunnerConfiguration(device
                .getName(), new String[0]);
        runConfig.setVMArguments(new String[0]);
        runConfig.setWorkingDirectory(workingDirName);
        runConfig.setVMSpecificAttributesMap(new HashMap());

        return runConfig;
    }

    /**
     * @see org.eclipse.debug.core.model.ILaunchConfigurationDelegate#launch(org.eclipse.debug.core.ILaunchConfiguration,
     *      java.lang.String, org.eclipse.debug.core.ILaunch,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public void launch(ILaunchConfiguration launchConfig, String mode,
            ILaunch launch, IProgressMonitor monitor) throws CoreException {
        // Make sure we have a progress monitor
        if (monitor == null) {
            monitor = new NullProgressMonitor();
        }

        // Get the platform definition and test that it isn't
        // the unspecified platform definition
        IDevice device = getDevice(launchConfig);
        if (device == null) {
            // The platform definition isn't available, so we can't
            // launch
            MTJCorePlugin.throwCoreException(IStatus.ERROR, -999,
                    "Device is unspecified or unavailable");
        }

        monitor.beginTask(MTJCoreStrings.getString(
                "launchdelegate.launching", new Object[] { launchConfig
                        .getName() }), 3);

        // check for cancellation
        if (monitor.isCanceled()) {
            return;
        }

        // Set up the VM runner
        monitor.subTask(MTJCoreStrings
                .getString("launchdelegate.verifying_attrs"));
        IVMRunner runner = getVMRunner(launchConfig, device, mode);

        // Create VM config
        VMRunnerConfiguration runConfig = getVMRunnerConfiguration(
                launchConfig, device, mode);

        // check for cancellation
        if (monitor.isCanceled()) {
            return;
        }

        // done the verification phase
        monitor.worked(1);

        // Handle the source locator support
        monitor.subTask(MTJCoreStrings
                .getString("launchdelegate.source_locator"));

        setDefaultSourceLocator(launch, launchConfig);
        monitor.worked(1);

        // Launch the configuration - 1 unit of work
        ((EmulatorRunner) runner).run(runConfig, launchConfig, launch, monitor);

        // check for cancellation
        if (!monitor.isCanceled()) {
            monitor.done();
        }
    }

    /**
     * @see org.eclipse.debug.core.model.ILaunchConfigurationDelegate2#preLaunchCheck(org.eclipse.debug.core.ILaunchConfiguration,
     *      java.lang.String, org.eclipse.core.runtime.IProgressMonitor)
     */
    public boolean preLaunchCheck(ILaunchConfiguration configuration,
            String mode, IProgressMonitor monitor) throws CoreException {
        boolean continueLaunch = super.preLaunchCheck(configuration, mode,
                monitor);
        if (continueLaunch) {
            continueLaunch = verifyEmulationSettings(configuration, mode, monitor);
        }

        return continueLaunch;
    }

    /**
     * Return the device selected by the launch configuration.
     * 
     * @param launchConfig
     * @return
     * @throws CoreException
     */
    private IDevice getDevice(ILaunchConfiguration launchConfig)
            throws CoreException {
        IDevice device = null;

        if (launchConfig
                .getAttribute(ILaunchConstants.USE_PROJECT_DEVICE, true)) {
            device = getMidletSuite(launchConfig).getDevice();
        } else {
            String toolkitName = launchConfig.getAttribute(
                    ILaunchConstants.EMULATED_DEVICE_GROUP, "");
            String deviceName = launchConfig.getAttribute(
                    ILaunchConstants.EMULATED_DEVICE, "");

            try {
                device = DeviceRegistry.singleton.getDevice(toolkitName,
                        deviceName);
            } catch (PersistenceException e) {
                MTJCorePlugin.throwCoreException(IStatus.ERROR, -999, e);
            }
        }

        return device;
    }

    /**
     * Return the MIDlet suite that is selected for this launch.
     * 
     * @param launchConfig
     * @return
     * @throws CoreException
     */
    private IMidletSuiteProject getMidletSuite(ILaunchConfiguration launchConfig)
            throws CoreException {
        IJavaProject javaProject = getJavaProject(launchConfig);
        return MidletSuiteFactory.getMidletSuiteProject(javaProject);
    }

    /**
     * Verify the current emulation settings before launching. Return a boolean
     * indicating whether the specified settings are valid.
     * 
     * @param configuration
     * @param monitor
     * @return
     * @throws CoreException
     */
    private boolean verifyEmulationSettings(ILaunchConfiguration configuration, String mode,
            IProgressMonitor monitor) throws CoreException {
        boolean valid = true;
        //if in debug mode,check the debugger setting
        if(ILaunchManager.DEBUG_MODE.equals(mode)){
        	IStatus status = new Status(IStatus.INFO,
                    IMTJCoreConstants.PLUGIN_ID,
                    IMTJCoreConstants.INFO_DEBUGGER_SETTINGS_CHECK, "", null);
        	valid = promptWhetherToContinue(status);
        	if(!valid)return valid;
        }
        // If this is OTA, there should be at least one midlet
        // defined in the JAD file.
        boolean doOTA = configuration.getAttribute(ILaunchConstants.DO_OTA,
                false);
        if (doOTA) {
            IJavaProject javaProject = getJavaProject(configuration);
            if (javaProject != null) {
                IMidletSuiteProject suite = MidletSuiteFactory
                        .getMidletSuiteProject(javaProject);
                ApplicationDescriptor desc = suite.getApplicationDescriptor();
                if (desc != null) {
                    valid = (desc.getMidletCount() > 0);
                    if (!valid) {
                        valid = promptWhetherToContinue(NO_MIDLET_STATUS);
                    }
                }
            }
        }

        return valid;
    }

    /**
     * Return a boolean concerning whether the user wants to continue the
     * launch.
     * 
     * @return
     * @throws CoreException
     */
    private boolean promptWhetherToContinue(IStatus status) throws CoreException {
        Boolean shouldContinue = (Boolean) MTJCorePlugin.statusPrompt(
        		status, this);
        return (shouldContinue != null) ? shouldContinue.booleanValue() : false;
    }
}
