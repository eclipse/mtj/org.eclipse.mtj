/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.device;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.persistence.XMLPersistenceProvider;
import org.eclipse.mtj.core.internal.utils.XMLUtils;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * The device registry provides a place to store and retrieve devices by name.
 * <p>
 * This registry is implemented as a multi-level registry in which the top-level
 * device groups reference lower-level devices.
 * </p>
 * 
 * @author Craig Setera
 */
public class DeviceRegistry implements IPersistable {
    /** Singleton instance of the device registry */
    public static final DeviceRegistry singleton = new DeviceRegistry();

    private static final String FILENAME = "devices.xml";

    // Map of device group names that maps to a lower-level
    // map of device instances by name
    private Map<String, Map<String, IDevice>> deviceGroupsMap;

    // A list of registry listeners
    private ArrayList<IDeviceRegistryListener> listenerList;

    // The default device
    private IDevice defaultDevice;

    private DeviceRegistry() {
        super();
        listenerList = new ArrayList<IDeviceRegistryListener>();
    }

    /**
     * Add a new device instance to the device registry.
     * 
     * @param device
     * @param fireDeviceAddedEvent TODO
     * @throws IllegalArgumentException if the device is not well formed.
     * @throws PersistenceException if there is a problem doing the initial
     *                 registry load
     */
    public void addDevice(IDevice device, boolean fireDeviceAddedEvent)
            throws IllegalArgumentException, PersistenceException {
        if ((device.getGroupName() == null) || (device.getName() == null)) {
            throw new IllegalArgumentException();
        }

        Map<String, IDevice> groupMap = getDeviceGroupMap(
                device.getGroupName(), true);
        groupMap.put(device.getName(), device);

        if (fireDeviceAddedEvent) {
            fireDeviceAddedEvent(device);
        }
    }

    /**
     * Add the specified listener to the list of listeners for changes in this
     * registry.
     * 
     * @param listener
     */
    public void addRegistryListener(IDeviceRegistryListener listener) {
        listenerList.add(listener);
    }

    /**
     * Clear the registry of all entries.
     * 
     * @throws PersistenceException if there is a problem doing the initial
     *                 registry load
     */
    public void clear() throws PersistenceException {
        getDeviceGroupsMap().clear();
    }

    /**
     * Return a list of all of the devices in the registry.
     * 
     * @return
     * @throws PersistenceException if there is a problem doing the initial
     *                 registry load
     */
    public List<IDevice> getAllDevices() throws PersistenceException {
        ArrayList<IDevice> deviceList = new ArrayList<IDevice>();

        Iterator<Map<String, IDevice>> groups = getDeviceGroupsMap().values()
                .iterator();
        while (groups.hasNext()) {
            Map<String, IDevice> deviceMap = (Map<String, IDevice>) groups
                    .next();
            deviceList.addAll(deviceMap.values());
        }

        return deviceList;
    }

    /**
     * Return the default device or <code>null</code> if one is not specified.
     * 
     * @return
     */
    public IDevice getDefaultDevice() {
        return defaultDevice;
    }

    /**
     * Return the device with the specified key or <code>null</code> if no
     * such device is found in the registry.
     * 
     * @param groupName
     * @param deviceName
     * @return
     * @throws PersistenceException if there is a problem doing the initial
     *                 registry load
     */
    public IDevice getDevice(String groupName, String deviceName)
            throws PersistenceException {
        IDevice device = null;

        Map<String, IDevice> groupMap = getDeviceGroupMap(groupName, false);
        if (groupMap != null) {
            device = (IDevice) groupMap.get(deviceName);
        }

        return device;
    }

    /**
     * Return the number of devices registered.
     * 
     * @return
     * @throws PersistenceException if there is a problem doing the initial
     *                 registry load
     */
    public int getDeviceCount() throws PersistenceException {
        int deviceCount = 0;

        Iterator<Map<String, IDevice>> groupMaps = getDeviceGroupsMap()
                .values().iterator();
        while (groupMaps.hasNext()) {
            Map<String, IDevice> groupMap = (Map<String, IDevice>) groupMaps
                    .next();
            deviceCount += groupMap.size();
        }

        return deviceCount;
    }

    /**
     * Return the registered device groups.
     * 
     * @return
     * @throws PersistenceException if there is a problem doing the initial
     *                 registry load
     */
    public List<String> getDeviceGroups() throws PersistenceException {
        return new ArrayList<String>(getDeviceGroupsMap().keySet());
    }

    /**
     * Return the devices found in the specified group or <code>null</code> if
     * the specified group cannot be found.
     * 
     * @param groupName
     * @return
     * @throws PersistenceException
     */
    public List<IDevice> getDevices(String groupName)
            throws PersistenceException {
        List<IDevice> devices = null;

        Map<String, IDevice> devicesGroupMap = getDeviceGroupMap(groupName,
                false);
        if (devicesGroupMap != null) {
            devices = new ArrayList<IDevice>(devicesGroupMap.values());
        }

        return devices;
    }

    /**
     * Load the contents of the device registry from the storage file in the
     * plug-in state location.
     * 
     * @throws PersistenceException
     */
    public void load() throws PersistenceException {
        File storeFile = getComponentStoreFile();
        if (storeFile.exists()) {
            try {
                Document document = XMLUtils.readDocument(storeFile);
                XMLPersistenceProvider pprovider = new XMLPersistenceProvider(
                        document);
                loadUsing(pprovider);
            } catch (ParserConfigurationException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (SAXException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (IOException e) {
                throw new PersistenceException(e.getMessage(), e);
            }
        }
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        getDeviceGroupsMap().clear();

        int deviceCount = persistenceProvider.loadInteger("deviceCount");

        for (int i = 0; i < deviceCount; i++) {
            IDevice device = (IDevice) persistenceProvider
                    .loadPersistable("device" + i);
            addDevice(device, false);
        }

        defaultDevice = (IDevice) persistenceProvider
                .loadReference("defaultDevice");
    }

    /**
     * Remove the specified device from the registry if it exists.
     * 
     * @param device
     * @throws PersistenceException if there is a problem doing the initial
     *                 registry load
     */
    public void removeDevice(IDevice device) throws PersistenceException {
        Map<String, IDevice> groupMap = getDeviceGroupMap(
                device.getGroupName(), false);
        if (groupMap != null) {
            groupMap.remove(device.getName());

            // Remove empty groups
            if (groupMap.isEmpty()) {
                deviceGroupsMap.remove(device.getGroupName());
            }

            // Clear the default device if it is being removed
            if (device.equals(defaultDevice)) {
                defaultDevice = null;
            }
        }

        fireDeviceRemovedEvent(device);
    }

    /**
     * Remove the specified listener to the list of listeners for changes in
     * this registry.
     * 
     * @param listener
     */
    public void removeRegistryListener(IDeviceRegistryListener listener) {
        listenerList.remove(listener);
    }

    /**
     * Set the default device.
     * 
     * @param device
     */
    public void setDefaultDevice(IDevice device) {
        defaultDevice = device;
    }

    /**
     * Store out the contents of the registry into the standard device storage
     * file in the plug-in state location.
     * 
     * @throws PersistenceException
     * @throws TransformerException
     * @throws IOException
     */
    public void store() throws PersistenceException, TransformerException,
            IOException {
        XMLPersistenceProvider pprovider = new XMLPersistenceProvider(
                "deviceRegistry");
        storeUsing(pprovider);
        XMLUtils
                .writeDocument(getComponentStoreFile(), pprovider.getDocument());
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        List<?> allDevices = getAllDevices();
        persistenceProvider.storeInteger("deviceCount", allDevices.size());

        int index = 0;
        Iterator<?> iterator = allDevices.iterator();
        while (iterator.hasNext()) {
            IDevice device = (IDevice) iterator.next();
            persistenceProvider.storePersistable("device" + index++, device);
        }

        if (defaultDevice != null) {
            persistenceProvider.storeReference("defaultDevice", defaultDevice);
        }
    }

    /**
     * Do the initial device group maps creation and loading.
     * 
     * @throws PersistenceException
     */
    private void createAndLoadDeviceGroups() throws PersistenceException {
        deviceGroupsMap = new HashMap<String, Map<String, IDevice>>();
        load();
    }

    /**
     * Fire an event that indicates a device has been added.
     * 
     * @param device
     */
    private void fireDeviceAddedEvent(IDevice device) {
        Iterator<IDeviceRegistryListener> listeners = listenerList.iterator();
        while (listeners.hasNext()) {
            IDeviceRegistryListener listener = (IDeviceRegistryListener) listeners
                    .next();
            listener.deviceAdded(device);
        }
    }

    /**
     * Fire an event that indicates a device has been removed.
     * 
     * @param device
     */
    private void fireDeviceRemovedEvent(IDevice device) {
        Iterator<IDeviceRegistryListener> listeners = listenerList.iterator();
        while (listeners.hasNext()) {
            IDeviceRegistryListener listener = (IDeviceRegistryListener) listeners
                    .next();
            listener.deviceRemoved(device);
        }
    }

    /**
     * Get the file for the device store file.
     * 
     * @return
     */
    private File getComponentStoreFile() {
        IPath pluginStatePath = MTJCorePlugin.getDefault().getStateLocation();
        IPath storePath = pluginStatePath.append(FILENAME);

        return storePath.toFile();
    }

    /**
     * Return the map of devices for the specified group. If the map is null and
     * createIfNull is specified, a new map will be created.
     * 
     * @param groupName
     * @param createIfNull
     * @return
     * @throws PersistenceException if there is a problem doing the initial
     *                 registry load
     */
    private Map<String, IDevice> getDeviceGroupMap(String groupName,
            boolean createIfNull) throws PersistenceException {
        Map<String, Map<String, IDevice>> groupsMap = getDeviceGroupsMap();
        Map<String, IDevice> deviceGroupMap = (Map<String, IDevice>) groupsMap
                .get(groupName);

        if ((deviceGroupMap == null) && createIfNull) {
            deviceGroupMap = new HashMap<String, IDevice>();
            groupsMap.put(groupName, deviceGroupMap);
        }

        return deviceGroupMap;
    }

    /**
     * Return the device groups map.
     * 
     * @return
     * @throws PersistenceException
     */
    private synchronized Map<String, Map<String, IDevice>> getDeviceGroupsMap()
            throws PersistenceException {
        if (deviceGroupsMap == null) {
            createAndLoadDeviceGroups();
        }

        return deviceGroupsMap;
    }
}
