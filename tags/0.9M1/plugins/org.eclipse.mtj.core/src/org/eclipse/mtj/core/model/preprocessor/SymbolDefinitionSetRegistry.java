/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Added ChangeListener capabilities
 */
package org.eclipse.mtj.core.model.preprocessor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.persistence.XMLPersistenceProvider;
import org.eclipse.mtj.core.internal.utils.XMLUtils;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * Provides a registry of SymbolDefinitionSet instances for use by various parts
 * of the core system.
 * 
 * @author Craig Setera
 */
public class SymbolDefinitionSetRegistry implements IPersistable {

    /**
     * Singleton instance of the registry
     */
    public static final SymbolDefinitionSetRegistry singleton = new SymbolDefinitionSetRegistry();

    // The filename used to store and retrieve the symbol definitions registry
    private static final String FILENAME = "symbolDefinitions.xml";

    // The actual mapping of symbol definition instances
    private Map<String, IPersistable> registryMap;

    List<ISymbolDefinitionSetChangeListener> listeners = new ArrayList<ISymbolDefinitionSetChangeListener>();

    /**
     * Private constructor for singleton access.
     */
    private SymbolDefinitionSetRegistry() {

    }

    /**
     * Add the specified SymbolDefinitions object to the registry of
     * definitions.
     * 
     * @param definitions
     * @throws PersistenceException
     * @throws IllegalStateException if the provided definition set has a
     *                 <code>null</code> name.
     */
    public void addDefinitionSet(SymbolDefinitionSet definitions)
            throws PersistenceException {
        String name = definitions.getName();
        if (name == null) {
            throw new IllegalStateException(
                    "Definition must have a name specified.");
        }

        getRegistryMap().put(definitions.getName(), definitions);
        notifyListeners();
    }

    /**
     * Create and add a new SymbolDefinitions object to the registry of
     * definitions with the specified name.
     * 
     * @param name
     * @return
     * @throws PersistenceException
     */
    public SymbolDefinitionSet addNewDefinitionSet(String name)
            throws PersistenceException {
        SymbolDefinitionSet set = new SymbolDefinitionSet();
        set.setName(name);
        addDefinitionSet(set);
        notifyListeners();
        return set;
    }

    /**
     * @param listener
     */
    public void addSymbolDefinitionSetChangeListener(
            ISymbolDefinitionSetChangeListener listener) {

        listeners.add(listener);

    }

    /**
     * Clear all of the registered SymbolDefinitions objects.
     * 
     * @throws PersistenceException
     */
    public void clear() throws PersistenceException {
        getRegistryMap().clear();
    }

    /**
     * Return all of the definition names registered.
     * 
     * @return
     * @throws PersistenceException
     */
    public String[] getAllDefinitionSetNames() throws PersistenceException {
        Set<String> keys = registryMap.keySet();
        return keys.toArray(new String[keys.size()]);
    }

    /**
     * Return all of the definitions registered.
     * 
     * @return
     * @throws PersistenceException
     */
    public SymbolDefinitionSet[] getAllSetDefinitions()
            throws PersistenceException {
        Collection<?> definitions = getRegistryMap().values();
        return definitions.toArray(new SymbolDefinitionSet[definitions.size()]);
    }

    /**
     * Return the SymbolDefinitions instance registered with the specified name
     * or <code>null</code> if the object cannot be found.
     * 
     * @param name
     * @return
     * @throws PersistenceException
     */
    public SymbolDefinitionSet getSymbolDefinitionSet(String name)
            throws PersistenceException {
        return (SymbolDefinitionSet) getRegistryMap().get(name);
    }

    /**
     * Load the contents of the symbol definitions registry from the storage
     * file in the plug-in state location.
     * 
     * @throws PersistenceException
     */
    public void load() throws PersistenceException {
        File storeFile = getStoreFile();
        if (storeFile.exists()) {
            try {
                Document document = XMLUtils.readDocument(storeFile);
                XMLPersistenceProvider pprovider = new XMLPersistenceProvider(
                        document);
                loadUsing(pprovider);
            } catch (ParserConfigurationException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (SAXException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (IOException e) {
                throw new PersistenceException(e.getMessage(), e);
            }
        }
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        if (registryMap != null) {
            String keysString = persistenceProvider.loadString("mtjKeys");
            if ((keysString != null) && (keysString.length() > 0)) {
                String[] keys = keysString.split(",");

                for (String key : keys) {
                    registryMap.put(key, persistenceProvider
                            .loadPersistable(key));
                }
            }
        }
    }

    /**
     * Remove the specified definition set from the registry. Does nothing if
     * the specified set cannot be found in the registry.
     * 
     * @param setName
     */
    public void removeDefinitionSet(String setName) {
        registryMap.remove(setName);
        notifyListeners();
    }

    /**
     * Remove the specified definition set from the registry. Does nothing if
     * the specified set cannot be found in the registry.
     * 
     * @param set
     */
    public void removeDefinitionSet(SymbolDefinitionSet set) {
        removeDefinitionSet(set.getName());
        notifyListeners();
    }

    /**
     * @param listener
     */
    public void removeSymbolDefinitionSetChangeListener(
            ISymbolDefinitionSetChangeListener listener) {

        listeners.remove(listener);
    }

    /**
     * Store out the contents of the registry into the standard device storage
     * file in the plug-in state location.
     * 
     * @throws PersistenceException
     * @throws TransformerException
     * @throws IOException
     */
    public void store() throws PersistenceException, TransformerException,
            IOException {
        XMLPersistenceProvider pprovider = new XMLPersistenceProvider(
                "symbolDefinitionsRegistry");
        storeUsing(pprovider);
        XMLUtils.writeDocument(getStoreFile(), pprovider.getDocument());
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        StringBuffer keyString = new StringBuffer();

        Iterator<?> definitions = getRegistryMap().values().iterator();
        while (definitions.hasNext()) {
            SymbolDefinitionSet definition = (SymbolDefinitionSet) definitions
                    .next();

            String name = definition.getStorableName();
            persistenceProvider.storePersistable(name, definition);
            keyString.append(name);
            if (definitions.hasNext()) {
                keyString.append(",");
            }
        }

        persistenceProvider.storeString("mtjKeys", keyString.toString());
    }

    /**
     * Return the registry map. The map will be loaded from persistent storage
     * if this is the first creation.
     * 
     * @return
     * @throws PersistenceException
     */
    private synchronized Map<String, IPersistable> getRegistryMap()
            throws PersistenceException {
        if (registryMap == null) {
            registryMap = new HashMap<String, IPersistable>();
            load();
        }

        return registryMap;
    }

    /**
     * Get the file used to persist the symbol definitions.
     * 
     * @return
     */
    private File getStoreFile() {
        IPath pluginStatePath = MTJCorePlugin.getDefault().getStateLocation();
        IPath storePath = pluginStatePath.append(FILENAME);

        return storePath.toFile();
    }

    /**
     * Notify the modification listeners
     */
    private void notifyListeners() {
        for (ISymbolDefinitionSetChangeListener listener : listeners) {
            listener.symbolDefinitionSetChanged();
        }
    }
}
