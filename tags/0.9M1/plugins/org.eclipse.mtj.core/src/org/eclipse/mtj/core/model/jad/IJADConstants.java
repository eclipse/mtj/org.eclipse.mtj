/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.jad;

/**
 * Constants related to editing JAD files.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */
public interface IJADConstants {

    // Required:
    public static final String JAD_MIDLET_JAR_SIZE = "MIDlet-Jar-Size";
    public static final String JAD_MIDLET_JAR_URL = "MIDlet-Jar-URL";
    public static final String JAD_MIDLET_NAME = "MIDlet-Name";
    public static final String JAD_MIDLET_VENDOR = "MIDlet-Vendor";
    public static final String JAD_MIDLET_VERSION = "MIDlet-Version";
    public static final String JAD_MICROEDITION_CONFIG = "MicroEdition-Configuration";
    public static final String JAD_MICROEDITION_PROFILE = "MicroEdition-Profile";

    // Optional:
    public static final String JAD_MIDLET_DATA_SIZE = "MIDlet-Data-Size";
    public static final String JAD_MIDLET_DELETE_CONFIRM = "MIDlet-Delete-Confirm";
    public static final String JAD_MIDLET_DELETE_NOTIFY = "MIDlet-Delete-Notify";
    public static final String JAD_MIDLET_DESCRIPTION = "MIDlet-Description";
    public static final String JAD_MIDLET_ICON = "MIDlet-Icon";
    public static final String JAD_MIDLET_INFO_URL = "MIDlet-Info-URL";
    public static final String JAD_MIDLET_INSTALL_NOTIFY = "MIDlet-Install-Notify";

    // The permission properties
    public static final String JAD_MIDLET_PERMISSIONS = "MIDlet-Permissions";
    public static final String JAD_MIDLET_PERMISSIONS_OPTIONAL = "MIDlet-Permissions-Opt";

    // Signature-related:
    public static final String JAD_MIDLET_JAR_RSA_SHA1 = "MIDlet-Jar-RSA-SHA1";

    // (needs "N" appended to it)
    public static final String JAD_MIDLET_CERTIFICATE = "MIDlet-Certificate-1-"; 
}
