/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.device;

import java.util.ArrayList;

/**
 * Simple implementation of the IFoundDevicesList interface. This implementation
 * collects the devices, but does nothing else with that information.
 * 
 * @author Craig Setera
 */
public class SimpleFoundDevicesList implements IFoundDevicesList {
    private ArrayList<IDevice> devices;

    /**
     * Construct a new device list.
     */
    public SimpleFoundDevicesList() {
        super();
        devices = new ArrayList<IDevice>();
    }

    /**
     * @see org.eclipse.mtj.core.model.device.IFoundDevicesList#addDevices(org.eclipse.mtj.core.model.device.IDevice[])
     */
    public void addDevices(IDevice[] devices) {
        for (int i = 0; i < devices.length; i++) {
            IDevice device = devices[i];
            this.devices.add(device);
        }
    }

    /**
     * Clear the previous results from this list.
     */
    public void clear() {
        // Allow the previous results to be cleared
        devices.clear();
    }

    /**
     * @see org.eclipse.mtj.core.model.device.IFoundDevicesList#getDevices()
     */
    public IDevice[] getDevices() {
        return (IDevice[]) devices.toArray(new IDevice[devices.size()]);
    }
}
