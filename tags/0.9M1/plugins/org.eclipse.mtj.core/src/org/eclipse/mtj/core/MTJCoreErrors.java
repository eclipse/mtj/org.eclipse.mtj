/**
 * Copyright (c) 2004,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 */
package org.eclipse.mtj.core;

import java.text.MessageFormat;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

/**
 * This interface holds the error, warning codes for the MTJ
 * org.eclipse.mtj.core package.
 * 
 * @author Kevin Hunter
 */
public class MTJCoreErrors {

    /**
     * This string represents the prefix used in looking up messages for error
     * and warning codes.
     */
    public static final String MESSAGE_PREFIX = "MTJCoreError.";

    /*
     * The constants below represent errors that the org.eclipse.mtj.core
     * plug-in can generate. Messages for these will be found in the
     * MTJCoreStrings.properties file.
     */
    public static final int CORE_ERROR_BASE = 10000;
    public static final int SIGNING_BAD_KEYSTORE_OR_PASSWORD = CORE_ERROR_BASE + 1;
    public static final int SIGNING_KEYSTORE_TYPE_NOT_AVAILABLE = CORE_ERROR_BASE + 2;
    public static final int SIGNING_PROVIDER_NOT_CONFIGURED = CORE_ERROR_BASE + 3;
    public static final int SIGNING_MISSING_KEYSTORE_INTEGRITY_ALGORITHM = CORE_ERROR_BASE + 4;
    public static final int SIGNING_COULDNT_LOAD_CERTIFICATE = CORE_ERROR_BASE + 5;
    public static final int SIGNING_KEY_NOT_FOUND = CORE_ERROR_BASE + 6;
    public static final int SIGNING_INVALID_KEY_PASSWORD = CORE_ERROR_BASE + 7;
    public static final int SIGNING_BAD_KEY_TYPE = CORE_ERROR_BASE + 8;
    public static final int SIGNING_INVALID_CERTIFICATE_CHAIN = CORE_ERROR_BASE + 9;
    public static final int SIGNING_MISSING_CERTIFICATES = CORE_ERROR_BASE + 10;
    public static final int SIGNING_INVALID_KEY = CORE_ERROR_BASE + 11;
    public static final int SIGNING_NO_SUCH_ALGORITHM = CORE_ERROR_BASE + 12;
    public static final int SIGNING_SIGNATURE_EXCEPTION = CORE_ERROR_BASE + 13;
    public static final int SIGNING_CERTIFICATE_ENCODING = CORE_ERROR_BASE + 14;

    /*
     * The constants below represent warnings that the org.eclipse.mtj.core
     * plug-in can generate. Messages for these will be found in the
     * MTJCoreStrings.properties file.
     */
    public static final int CORE_WARNING_BASE = 20000;

    /*
     * The constants below represent internal errors. These indicate some kind
     * of logic fault, as opposed to being something that could happen under
     * normal conditions. As such, these items do not have messages. Instead, a
     * "generic" message is generated.
     */
    public static final int CORE_INTERNAL_BASE = 90000;
    public static final int SIGNING_INTERNAL_MISSING_KEYCHAINSET = CORE_INTERNAL_BASE + 1;
    public static final int SIGNING_INTERNAL_UNABLE_TO_BUILD_KEYRING_URL = CORE_INTERNAL_BASE + 2;

    /**
     * This routine returns the message associated with a particular error code
     * or warning. If there's a specific text message in the resource bundle,
     * associated with this code, that one is returned. Otherwise, a default
     * message is used. In either case, the error code is optionally (based on
     * the string contents) substituted into the message.
     * 
     * @param nCode The error code.
     * @return
     */
    public static final String getErrorMessage(int nCode) {
        String strTemplate;

        strTemplate = MTJCoreStrings.getBundleString(MESSAGE_PREFIX + nCode);
        if (strTemplate == null) {
            if (nCode < CORE_INTERNAL_BASE) {
                strTemplate = MTJCoreStrings.getString("MTJCoreError.Default");
            } else {
                strTemplate = MTJCoreStrings
                        .getString("MTJCoreError.InternalTemplate");
            }
        }

        String strMessage = MessageFormat.format(strTemplate,
                (Object[]) new Integer[] { Integer.valueOf(nCode) });
        return (strMessage);
    }

    /**
     * This routine throws a CoreException with a status code of "ERROR", the
     * specified error code, and a message that is internationalized.
     * 
     * @param code
     * @throws CoreException
     */
    public static void throwCoreExceptionError(int code) throws CoreException {
        IStatus status = new Status(IStatus.ERROR, IMTJCoreConstants.PLUGIN_ID,
                code, MTJCoreErrors.getErrorMessage(code), null);
        throw new CoreException(status);
    }

    /**
     * This routine throws a CoreException with a status code of "ERROR", the
     * specified error code, and a message that is internationalized.
     * 
     * @param code
     * @throws CoreException
     */
    public static void throwCoreExceptionError(int code, Throwable e)
            throws CoreException {
        IStatus status = new Status(IStatus.ERROR, IMTJCoreConstants.PLUGIN_ID,
                code, MTJCoreErrors.getErrorMessage(code), e);
        throw new CoreException(status);
    }
}
