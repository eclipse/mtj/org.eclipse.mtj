/**
 * Copyright (c) 2004,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.dialog;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * A dialog that shows a large amount of text and allows for continue or cancel.
 * (Continue returns IDialogConstants.OK_ID).
 * 
 * @author Craig Setera
 */
public class ErrorTextWithContinueDialog extends MessageDialog {
    private String dialogMessage;

    /**
     * Prompt the user concerning whether to continue. Return a boolean
     * indicating whether to continue.
     * 
     * @param parentShell
     * @param dialogTitle
     * @param dialogMessage
     * @return
     */
    public static boolean promptToContinue(Shell parentShell,
            String dialogTitle, String dialogMessage) {
        ErrorTextWithContinueDialog dialog = new ErrorTextWithContinueDialog(
                parentShell, dialogTitle, dialogMessage);

        return dialog.open() == 0;
    }

    /**
     * Construct a new dialog.
     * 
     * @param parentShell
     * @param dialogTitle
     * @param dialogMessage
     */
    public ErrorTextWithContinueDialog(Shell parentShell, String dialogTitle,
            String dialogMessage) {

        // continue is the default
        super(parentShell, dialogTitle, null, dialogTitle, WARNING,
                new String[] { "Continue", IDialogConstants.CANCEL_LABEL }, 0);

        this.dialogMessage = dialogMessage;
    }

    /**
     * @see org.eclipse.jface.dialogs.MessageDialog#createCustomArea(org.eclipse.swt.widgets.Composite)
     */
    protected Control createCustomArea(Composite parent) {
        Text textarea = new Text(parent, SWT.MULTI | SWT.READ_ONLY | SWT.WRAP);
        textarea.setText(dialogMessage);
        return textarea;
    }
}
