/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Hugo Raniere (Motorola)  - Removing Preprocessor code
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code  
 */
package org.eclipse.mtj.ui.internal.wizards.project.page;

import org.eclipse.mtj.ui.internal.wizards.project.NewMidletProjectWizardMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;

/**
 * A specialization of the {@link WizardNewProjectCreationPage} that adds the
 * ability to specify whether or not the newly created MIDlet suite project
 * should support preprocessing.
 * 
 * @author Craig Setera
 */
public class NewMidletProjectCreationPage extends WizardNewProjectCreationPage {

    /**
     * The name of the main page as registered in the wizard
     */
    public static final String MAIN_PAGE_NAME = "NewMidletProjectCreationPage";

    private Button preprocessedButton;
    private boolean preprocessingEnabled;

    /**
     * Construct a new instance of NewMidletProjectCreationPage.
     */
    public NewMidletProjectCreationPage() {
        super(MAIN_PAGE_NAME);

        /* Setting page title and description */
        setTitle(NewMidletProjectWizardMessages.NewMidletProjectCreationPage_title);
        setDescription(NewMidletProjectWizardMessages.NewMidletProjectCreationPage_description);
    }

    /**
     * Construct a new instance of NewMidletProjectCreationPage with the
     * specified title and description.
     * 
     * @param title the page title
     * @param description the page description
     */
    public NewMidletProjectCreationPage(final String title,
            final String description) {
        super(MAIN_PAGE_NAME);

        /* Setting page title and description */

        if (title != null) {
            setTitle(title);
        } else {
            setTitle("");
        }

        if (description != null) {
            setDescription(description);
        } else {
            setDescription("");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.dialogs.WizardNewProjectCreationPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createControl(Composite parent) {
        super.createControl(parent);

        // Snag the composite created by the superclass and add to
        // the composite
        Composite composite = (Composite) getControl();

        // Add an extra composite to get the layout to match up the
        // components vertically
        Composite layoutComposite = new Composite(composite, SWT.NONE);
        layoutComposite.setLayout(new GridLayout(1, true));
        layoutComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

        preprocessedButton = new Button(layoutComposite, SWT.CHECK);
        preprocessedButton
                .setText(NewMidletProjectWizardMessages.NewMidletProjectCreationPage_preprocessor);
        preprocessedButton
                .setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        preprocessedButton.addSelectionListener(new SelectionAdapter() {

            /*
             * (non-Javadoc)
             * 
             * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                preprocessingEnabled = preprocessedButton.getSelection();
            }
        });
    }

    /**
     * Return a boolean concerning whether preprocessing is enabled for the
     * newly created project.
     * 
     * @return the preprocessingEnabled
     */
    public boolean isPreprocessingEnabled() {
        return preprocessingEnabled;
    }
}
