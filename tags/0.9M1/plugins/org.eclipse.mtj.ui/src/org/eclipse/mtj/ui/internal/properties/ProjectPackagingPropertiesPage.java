/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)  - Initial implementation
 *     Kevin Hunter (Individual) - Added signature support
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse
 *                                 standards
 */
package org.eclipse.mtj.ui.internal.properties;

import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.ui.internal.preferences.PackagingPreferencePage;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.PropertyPage;

/**
 * Property page implementation for J2ME properties associated with packaging of
 * a project.
 * 
 * @author Craig Setera
 * @see PropertyPage
 */
public class ProjectPackagingPropertiesPage extends PropertyAndPreferencePage {

    /**
     * Default constructor.
     */
    public ProjectPackagingPropertiesPage() {
    }

    /**
     * Embed a preference page into the parent composite, setting things up
     * correctly as we go along.
     * 
     * @param composite
     */
    @Override
    protected void embedPreferencePage(Composite composite) {
        preferencePage = new PackagingPreferencePage(true, getPreferenceStore());
        preferencePage.createControl(composite);
        preferencePage.getControl().setLayoutData(
                new GridData(GridData.FILL_BOTH));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.properties.PropertyAndPreferencePage#getProjectSpecificSettingsKey()
     */
    @Override
    protected String getProjectSpecificSettingsKey() {
        return IMTJCoreConstants.PREF_PKG_USE_PROJECT;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.properties.PropertyAndPreferencePage#isReadOnly()
     */
    @Override
    protected boolean isReadOnly() {
        return isPreprocessedOutputProject();
    }
}
