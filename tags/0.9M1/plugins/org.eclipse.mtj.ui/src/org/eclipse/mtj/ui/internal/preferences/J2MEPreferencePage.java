/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.preferences;

import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

/**
 * Preference page implementation for setting J2ME preferences.
 * 
 * @author Craig Setera
 */
public class J2MEPreferencePage extends FieldEditorPreferencePage implements
        IWorkbenchPreferencePage, IMTJCoreConstants {

    public static final String ID = "org.eclipse.mtj.ui.preferences.J2MEPreferencePage";

    /**
     * Default constructor.
     */
    public J2MEPreferencePage() {
        super(GRID);
        setPreferenceStore(MTJUIPlugin.getDefault().getCorePreferenceStore());
        setDescription(MTJUIStrings.getString("pref.description"));
    }

    /*
     * Overridden so that we get the help context where it belongs so that it
     * works when the focus is in the left pane.
     */
    protected Control createContents(Composite parent) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_J2MEPreferencePage");

        return (super.createContents(parent));
    }

    /**
     * Creates the field editors.
     */
    public void createFieldEditors() {
        int columnSpan = 3;
        Composite parent = getFieldEditorParent();

        addField(new StringFieldEditor(PREF_DEPLOYMENT_DIR,
                "Deployment Directory:", parent));

        addSpacer(parent, columnSpan);

        Group antennaGroup = new Group(parent, SWT.NONE);
        antennaGroup.setText("Antenna Settings");
        antennaGroup.setLayout(new GridLayout(1, true));
        GridData antennaGD = getColumnSpanGridData(columnSpan);
        antennaGD.minimumWidth = 500;
        antennaGroup.setLayoutData(antennaGD);

        Composite antennaComposite = new Composite(antennaGroup, SWT.NONE);
        antennaComposite.setLayout(new GridLayout(3, true));
        antennaComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

        FileFieldEditor antennaJarEditor = new FileFieldEditor(
                PREF_ANTENNA_JAR, "Antenna JAR:", true, antennaComposite);
        antennaJarEditor.setFileExtensions(new String[] { "*.jar" });
        addField(antennaJarEditor);

        DirectoryFieldEditor wtkRootEditor = new DirectoryFieldEditor(
                PREF_WTK_ROOT, "WTK Root:", antennaComposite);
        addField(wtkRootEditor);

        addSpacer(parent, columnSpan);

        addField(new IntegerFieldEditor(PREF_RMTDBG_TIMEOUT,
                "Debug Server Time-out (ms):", parent));
        addField(new IntegerFieldEditor(PREF_RMTDBG_INTERVAL,
                "Debug Server Launch Poll Interval (ms)", parent));
        Label l = new Label(parent, SWT.NONE);
        l
                .setText("Maximum duration to launch a UEI emulator with debugger in server mode.");
        l.setLayoutData(getColumnSpanGridData(columnSpan));
    }

    /**
     * Initialize the preference page as necessary
     * 
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }

    /**
     * Add some horizontal space to the dialog.
     * 
     * @param parent
     * @param columnSpan
     */
    private void addSpacer(Composite parent, int columnSpan) {
        Label l = new Label(parent, SWT.NONE);
        l.setLayoutData(getColumnSpanGridData(columnSpan));
    }

    /**
     * Return a new GridData object with the specified column span.
     * 
     * @param columnSpan
     * @return
     */
    private GridData getColumnSpanGridData(int columnSpan) {
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = columnSpan;

        return gd;
    }
}
