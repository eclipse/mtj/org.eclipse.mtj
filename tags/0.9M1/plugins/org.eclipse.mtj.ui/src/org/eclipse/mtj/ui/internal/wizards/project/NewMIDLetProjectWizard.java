/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards, changed the use of getLocationPath 
 *                                to getLocationURI in the 
 *                                getProjectCreationRunnable method
 *     Hugo Raniere (Motorola)  - Removing Preprocessor code
 *     Hugo Raniere (Motorola)  - Wizard handle the classpath correctly according to device changes
 *     Feng Wang (Sybase)       - Promoting a dialog to ask user switch to MTJ perspective
 *                                after user click finish button
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code  
 *                                
 */
package org.eclipse.mtj.ui.internal.wizards.project;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExecutableExtension;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.wizards.JavaCapabilityConfigurationPage;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.core.internal.JavaMEClasspathContainer;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.project.MetaData;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.wizards.project.page.NewMidletProjectCreationPage;
import org.eclipse.mtj.ui.internal.wizards.project.page.NewMidletProjectJavaMECapabilityConfigPage;
import org.eclipse.mtj.ui.internal.wizards.project.page.NewMidletProjectPropertiesPage;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.actions.WorkspaceModifyDelegatingOperation;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;

/**
 * Wizard for creation of a new Java ME MIDlet Project.
 * 
 * @author Craig Setera
 */
public class NewMIDLetProjectWizard extends Wizard implements INewWizard,
        IExecutableExtension {

    // Instance variables
    private IConfigurationElement configElement;
    private IDevice projectDevice = null;

    private boolean needsClasspathUpdate = true;
    private boolean projectLocationExisted = false;

    /* Wizard Pages */
    private NewMidletProjectJavaMECapabilityConfigPage javaWizardPage;
    private NewMidletProjectCreationPage mainPage;
    private NewMidletProjectPropertiesPage suitePropsPage;

    /**
     * Construct a new MIDLet project wizard
     */
    public NewMIDLetProjectWizard() {
        super();

        setWindowTitle(NewMidletProjectWizardMessages.NewMIDLetProjectWizard_title);
        ImageDescriptor descriptor = MTJUIPlugin
                .getIconImageDescriptor("newjprj_wiz_M.gif");
        setDefaultPageImageDescriptor(descriptor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.wizard.Wizard#addPages()
     */
    @Override
    public void addPages() {
        super.addPages();

        /* Creating the wizard pages with default title and description */
        mainPage = new NewMidletProjectCreationPage();
        suitePropsPage = new NewMidletProjectPropertiesPage();
        javaWizardPage = new NewMidletProjectJavaMECapabilityConfigPage();

        /* Adding wizard pages */
        addPage(mainPage);
        addPage(suitePropsPage);
        addPage(javaWizardPage);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.wizard.Wizard#canFinish()
     */
    @Override
    public boolean canFinish() {
        return suitePropsPage.canFlipToNextPage() && super.canFinish();
    }

    /**
     * Return the handle to the project to be created.
     * 
     * @return the handle to the project to be created.
     */
    public IProject getCreatedProjectHandle() {
        return mainPage.getProjectHandle();
    }

    /**
     * Returns the classpath container that includes all libraries present in
     * the project's device
     * 
     * @return an instance of an IClasspathEntry of kind CPE_CONTAINER
     */
    public IClasspathEntry getJavaMEClasspathContainer() {
        needsClasspathUpdate = false;
        IPath entryPath = new Path(JavaMEClasspathContainer.JAVAME_CONTAINER
                + "/" + projectDevice);
        return JavaCore.newContainerEntry(entryPath);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
     *      org.eclipse.jface.viewers.IStructuredSelection)
     */
    public void init(IWorkbench workbench, IStructuredSelection selection) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.wizard.Wizard#performCancel()
     */
    @Override
    public boolean performCancel() {
        if (!projectLocationExisted) {
            removeProject();
        }

        return super.performCancel();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        boolean completed = true;

        // The steps necessary for creating the new MIDlet suite
        IRunnableWithProgress runnable = new IRunnableWithProgress() {

            /*
             * (non-Javadoc)
             * 
             * @see org.eclipse.jface.operation.IRunnableWithProgress#run(org.eclipse.core.runtime.IProgressMonitor)
             */
            public void run(IProgressMonitor monitor)
                    throws InvocationTargetException, InterruptedException {
                // Get the project created first
                getProjectCreationRunnable().run(monitor);

                // Set the device into the project metadata to make
                // the java project creation happy.
                IProject project = mainPage.getProjectHandle();
                IDevice device = suitePropsPage.getSelectedDevice();
                MetaData metadata = new MetaData(project);
                metadata.setDevice(device);
                try {
                    metadata.saveMetaData();
                } catch (CoreException e) {
                    throw new InvocationTargetException(e);
                }

                // classpath set up
                if (needsClasspathUpdate) {
                    javaWizardPage.updateConfiguration();
                    needsClasspathUpdate = false;
                }

                // Get the java nature
                getJavaNatureRunnable().run(monitor);

                // Get the J2ME nature and metadata set up
                String jadFileName = suitePropsPage.getJadFileName();
                IJavaProject javaProject = javaWizardPage.getJavaProject();

                MidletSuiteFactory.MidletSuiteCreationRunnable runnable = MidletSuiteFactory
                        .getMidletSuiteCreationRunnable(project, javaProject,
                                device, jadFileName);

                runnable.setPreprocessingEnable(mainPage
                        .isPreprocessingEnabled());
                runnable.run(monitor);
            }
        };

        // TODO Get in progress indicator updated
        try {
            getContainer().run(false, true,
                    new WorkspaceModifyDelegatingOperation(runnable));

        } catch (InvocationTargetException e) {
            MTJCorePlugin.log(IStatus.ERROR, "performFinish", e.getCause());
            completed = false;
        } catch (InterruptedException e) {
            completed = false;
        }

        // Promoting a dialog to ask user switch to MTJ perspective
        BasicNewProjectResourceWizard.updatePerspective(configElement);

        return completed;
    }

    /**
     * Stores the configuration element for the wizard. The config element will
     * be used in <code>performFinish</code> to set the result perspective.
     * 
     * @see org.eclipse.core.runtime.IExecutableExtension#setInitializationData(org.eclipse.core.runtime.IConfigurationElement,
     *      java.lang.String, java.lang.Object)
     */
    public void setInitializationData(IConfigurationElement cfig,
            String propertyName, Object data) {

        this.configElement = cfig;
    }

    /**
     * Set the device to be used by this project
     * 
     * @param device IDevice instance to be used
     */
    public void setProjectDevice(IDevice device) {
        projectDevice = device;
        needsClasspathUpdate = true;
    }

    /**
     * Return a runnable capable of setting up the Java nature and settings.
     * 
     * @return
     */
    private IRunnableWithProgress getJavaNatureRunnable() {
        IRunnableWithProgress runnable = javaWizardPage.getRunnable();

        if (runnable == null) {
            runnable = javaWizardPage.getRunnable();
        }

        return runnable;
    }

    /**
     * Return the runnable used to perform the project creation work.
     * 
     * @return
     */
    private IRunnableWithProgress getProjectCreationRunnable() {
        return new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor)
                    throws InvocationTargetException, InterruptedException {
                try {
                    // Track whether the project location already exists
                    File projectLocationFile = mainPage.getLocationPath()
                            .toFile();
                    projectLocationExisted = projectLocationFile.exists();

                    // Create the project
                    IProject project = mainPage.getProjectHandle();
                    JavaCapabilityConfigurationPage.createProject(project,
                            mainPage.getLocationPath(), new SubProgressMonitor(
                                    monitor, 1));

                } catch (CoreException e) {
                    throw new InvocationTargetException(e);
                }
            }
        };
    }

    /**
     * Remove the already created project, as the user canceled the operation.
     */
    private void removeProject() {
        IJavaProject javaProject = javaWizardPage.getJavaProject();
        if (javaProject != null) {
            final IProject project = javaProject.getProject();

            if ((project != null) && project.exists()) {

                IRunnableWithProgress op = new IRunnableWithProgress() {
                    public void run(IProgressMonitor monitor)
                            throws InvocationTargetException,
                            InterruptedException {
                        monitor.beginTask("##Remove Project", 3);

                        try {
                            project.delete(true, false, monitor);
                        } catch (CoreException e) {
                            throw new InvocationTargetException(e);
                        } finally {
                            monitor.done();
                        }
                    }
                };

                try {
                    getContainer().run(false, true, op);
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    // cancel pressed
                }
            }
        }
    }
}
