/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.wizards.midlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.core.model.jad.ApplicationDescriptor;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.wizards.midlet.page.NewMidletWizardPage;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyDelegatingOperation;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.wizards.newresource.BasicNewResourceWizard;

/**
 * Wizard for the creation of a new J2ME MIDP MIDlet.
 * 
 * @author Craig Setera
 * @see Wizard
 */
public class NewMidletWizard extends Wizard implements INewWizard {
    private IWorkbench workbench;
    private IStructuredSelection selection;
    private NewMidletWizardPage midletPage;

    /**
     * Constructor.
     */
    public NewMidletWizard() {
        setDialogSettings(MTJUIPlugin.getDialogSettings("NewMidletWizard"));
        setDefaultPageImageDescriptor(MTJUIPlugin
                .getIconImageDescriptor("newclass_wiz_M.gif"));
    }

    /**
     * @see org.eclipse.jface.wizard.IWizard#addPages()
     */
    public void addPages() {
        midletPage = new NewMidletWizardPage();
        addPage(midletPage);
        midletPage.setWizard(this);

        midletPage.init(selection);
    }

    /**
     * @see Wizard#init
     */
    public void init(IWorkbench workbench, IStructuredSelection selection) {
        this.workbench = workbench;
        this.selection = selection;
    }

    /**
     * @see Wizard#performFinish
     */
    public boolean performFinish() {
        boolean completed = true;

        IRunnableWithProgress createMidletOp = getCreateMidletOperation();

        try {
            getContainer().run(false, true, createMidletOp);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            completed = false;
        } catch (InterruptedException e) {
            completed = false;
        }

        return completed;
    }

    /**
     * Add the specified IType to the Application Descriptor.
     * 
     * @param type
     * @param monitor
     * @throws IOException
     * @throws CoreException
     */
    private void addMidletToJAD(IType type, IProgressMonitor monitor)
            throws IOException, CoreException {
        // Pull out the midlet suite that the type was created in
        IJavaProject javaProject = (IJavaProject) type
                .getAncestor(IJavaElement.JAVA_PROJECT);
        IMidletSuiteProject midletSuite = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);

        ApplicationDescriptor desc = midletSuite.getApplicationDescriptor();
        if (desc != null) {
            ApplicationDescriptor.MidletDefinition def = new ApplicationDescriptor.MidletDefinition(
                    desc.getMidletCount() + 1, type.getElementName(), "", type
                            .getFullyQualifiedName());
            desc.addMidletDefinition(def);
            desc.store();

            IFile jadFile = midletSuite.getApplicationDescriptorFile();
            jadFile.refreshLocal(IResource.DEPTH_ONE, monitor);
        }
    }

    /**
     * Return the operation that is used to create the new midlet.
     * 
     * @return
     */
    private IRunnableWithProgress getCreateMidletOperation() {
        IRunnableWithProgress runnable = new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor)
                    throws InvocationTargetException, InterruptedException {
                WorkspaceModifyDelegatingOperation op = new WorkspaceModifyDelegatingOperation(
                        midletPage.getRunnable());
                op.run(monitor);

                IType type = midletPage.getCreatedType();
                if (type != null) {
                    IResource resource = type.getResource();
                    if (resource != null) {
                        BasicNewResourceWizard.selectAndReveal(resource,
                                workbench.getActiveWorkbenchWindow());
                        openResource((IFile) resource);
                    }

                    if (midletPage.isAddToJadSelected()) {
                        try {
                            addMidletToJAD(type, monitor);
                        } catch (IOException e) {
                            throw new InvocationTargetException(e);
                        } catch (CoreException e) {
                            throw new InvocationTargetException(e);
                        }
                    }
                }
            }
        };

        return new WorkspaceModifyDelegatingOperation(runnable);
    }

    /**
     * Open the specified resource within a resource.
     * 
     * @param resource
     */
    private void openResource(final IFile resource) {
        IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
        if (window != null) {
            final IWorkbenchPage activePage = workbench
                    .getActiveWorkbenchWindow().getActivePage();

            if (activePage != null) {
                final Display display = getShell().getDisplay();

                if (display != null) {
                    display.asyncExec(new Runnable() {
                        public void run() {
                            try {
                                IDE.openEditor(activePage, resource, true);
                            } catch (PartInitException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        }
    }
}
