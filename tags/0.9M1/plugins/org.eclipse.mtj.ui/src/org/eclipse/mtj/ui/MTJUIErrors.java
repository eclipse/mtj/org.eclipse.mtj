/**
 * Copyright (c) 2004,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 */
package org.eclipse.mtj.ui;

import java.text.MessageFormat;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.mtj.ui.MTJUIErrors;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.widgets.Shell;

/**
 * This interface holds the error, warning codes for the MTJ org.eclipse.mtj.ui
 * package.
 * 
 * @author Kevin Hunter
 */
public class MTJUIErrors {
    /**
     * This string represents the prefix used in looking up messages for error
     * and warning codes.
     */

    public static final String MESSAGE_PREFIX = "MTJUIError.";

    /*
     * The constants below represent errors that the org.eclipse.mtj.ui plug-in
     * can generate. Messages for these will be found in the
     * MTJCoreStrings.properties file.
     */
    public static final int UI_ERROR_BASE = 110000;
    public static final int UI_ERROR_EXCEPTION = UI_ERROR_BASE + 1;

    /*
     * The constants below represent warnings that the org.eclipse.mtj.core
     * plug-in can generate. Messages for these will be found in the
     * MTJCoreStrings.properties file.
     */
    public static final int UI_WARNING_BASE = 120000;

    /*
     * The constants below represent internal errors. These indicate some kind
     * of logic fault, as opposed to being something that could happen under
     * normal conditions. As such, these items do not have messages. Instead, a
     * "generic" message is generated.
     */
    public static final int UI_INTERNAL_BASE = 190000;

    /**
     * This routine returns the message associated with a particular error code
     * or warning. If there's a specific text message in the resource bundle,
     * associated with this code, that one is returned. Otherwise, a default
     * message is used. In either case, the error code is optionally (based on
     * the string contents) substituted into the message.
     * 
     * @param nCode The error code.
     * @return
     */

    public static final String getErrorMessage(int nCode) {
        String strTemplate;

        strTemplate = MTJUIStrings.getBundleString(MESSAGE_PREFIX + nCode);
        if (strTemplate == null) {
            if (nCode < UI_INTERNAL_BASE) {
                strTemplate = MTJUIStrings
                        .getString(MESSAGE_PREFIX + "Default");
            } else {
                strTemplate = MTJUIStrings.getString(MESSAGE_PREFIX
                        + "InternalTemplate");
            }
        }

        String strMessage = MessageFormat.format(strTemplate,
                new Object[] { Integer.valueOf(nCode) });
        return (strMessage);
    }

    /**
     * This routine throws a CoreException with a status code of "ERROR", the
     * specified error code, and a message that is internationalized.
     * 
     * @param code
     * @throws CoreException
     */
    public static void throwCoreExceptionError(int code) throws CoreException {
        IStatus status = new Status(IStatus.ERROR, IMTJUIConstants.PLUGIN_ID,
                code, MTJUIErrors.getErrorMessage(code), null);
        throw new CoreException(status);
    }

    /**
     * This routine throws a CoreException with a status code of "ERROR", the
     * specified error code, and a message that is internationalized.
     * 
     * @param code
     * @throws CoreException
     */
    public static void throwCoreExceptionError(int code, Throwable e)
            throws CoreException {
        IStatus status = new Status(IStatus.ERROR, IMTJUIConstants.PLUGIN_ID,
                code, MTJUIErrors.getErrorMessage(code), e);
        throw new CoreException(status);
    }

    /**
     * This routine handles the problems of displaying internationalized error
     * messages.
     * 
     * @param s <code>Shell</code> on which to display.
     * @param dialogTitleKey Key in the resource file for dialog title
     * @param messageKey Key in the resource file for dialog message.
     * @param status Optional <code>IStatus</code> object that is the source
     *                of the error.
     */

    public static void displayError(Shell s, String dialogTitleKey,
            String messageKey, IStatus status) {
        if (dialogTitleKey == null) {
            dialogTitleKey = MESSAGE_PREFIX + "Default";
        }

        if (messageKey == null) {
            messageKey = MESSAGE_PREFIX + "Default";
        }

        int nCode = 0;
        if (status != null) {
            nCode = status.getCode();
        }

        Object[] substitutions = new Integer[] { Integer.valueOf(nCode) };

        String strTitle = MessageFormat.format(MTJUIStrings
                .getString(dialogTitleKey), substitutions);
        String strMessage = MessageFormat.format(MTJUIStrings
                .getString(messageKey), substitutions);

        ErrorDialog.openError(s, strTitle, strMessage, status);
    }

    public static void displayError(Shell s, String dialogTitleKey,
            String messageKey, Exception e) {
        if (dialogTitleKey == null) {
            dialogTitleKey = MESSAGE_PREFIX + "Default";
        }

        if (messageKey == null) {
            messageKey = MESSAGE_PREFIX + "Default";
        }

        Object[] substitutions = new String[] { e.getMessage() };

        String message = e.getMessage();
        if (message == null)
            message = "No Message";
        Status status = new Status(IStatus.ERROR, IMTJUIConstants.PLUGIN_ID,
                UI_ERROR_EXCEPTION, message, e);

        String strTitle = MessageFormat.format(MTJUIStrings
                .getString(dialogTitleKey), substitutions);
        String strMessage = MessageFormat.format(MTJUIStrings
                .getString(messageKey), substitutions);

        MTJUIPlugin.getDefault().getLog().log(status);
        ErrorDialog.openError(s, strTitle, strMessage, status);
    }

}
