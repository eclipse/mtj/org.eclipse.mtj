/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola) - Initial implementation
 *     Gang Ma      (Sybase)	- Refactoring the page to add expansibilities
 */
package org.eclipse.mtj.ui.internal.editors.jad.form.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage;
import org.eclipse.mtj.ui.internal.editors.jad.form.JADFormEditor;
import org.eclipse.mtj.ui.internal.utils.MidletSelectionDialogCreator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.SelectionDialog;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * A property page editor static registering MIDlets in Push Registry
 * 
 * @author Hugo Raniere
 */
public class JADPushRegistryEditorPage extends AbstractJADEditorPage {

    /**
     * Implementation of the ICellModifier interface.
     */
    private class CellModifier implements ICellModifier {

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.ICellModifier#canModify(java.lang.Object,
         *      java.lang.String)
         */
        public boolean canModify(Object element, String property) {
            // All columns are modifiable
            return true;
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.ICellModifier#getValue(java.lang.Object,
         *      java.lang.String)
         */
        public Object getValue(Object element, String property) {
            Object value = null;

            if (element instanceof PushRegEntry) {
                PushRegEntry entry = (PushRegEntry) element;

                int fieldIndex = getFieldIndex(property);
                if (fieldIndex != -1) {
                    value = entry.fields[fieldIndex];
                }
            }

            return value;
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.ICellModifier#modify(java.lang.Object,
         *      java.lang.String, java.lang.Object)
         */
        public void modify(Object element, String property, Object value) {
            if (element instanceof TableItem) {
                Object data = ((TableItem) element).getData();
                String newValue = (String) value;

                if (data instanceof PushRegEntry) {
                    int fieldIndex = getFieldIndex(property);
                    PushRegEntry entry = (PushRegEntry) data;

                    if (fieldIndex != -1) {
                        updateField(entry, property, fieldIndex, newValue);
                    }
                }
            }
        }

        /**
         * Return the field index to match the specified property name.
         * 
         * @param property property name to search for
         * @return the index that matchs the specified property name.
         *         <code>-1</code> if the property is not recognized.
         */
        private int getFieldIndex(String property) {
            return PROPERTY_LIST.indexOf(property);
        }

        /**
         * Update the specified field as necessary.
         * 
         * @param entry the Push Registry entry to be updated
         * @param property property of entry to be updated
         * @param fieldIndex the index of the field to be updated in entry
         * @param newValue the new value to be set
         */
        private void updateField(PushRegEntry entry, String property,
                int fieldIndex, String newValue) {
            if (!entry.fields[fieldIndex].equals(newValue)) {
                entry.fields[fieldIndex] = newValue;
                setDirty(true);
                tableViewer.update(entry, new String[] { property });
            }
        }
    }

    /**
     * A cell editor implementation that allows for selection of a midlet class.
     */
    private class MidletCellEditor extends DialogCellEditor {
        /** Construct a new cell editor */
        MidletCellEditor(Composite parent) {
            super(parent);
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.DialogCellEditor#openDialogBox(org.eclipse.swt.widgets.Control)
         */
        @Override
        protected Object openDialogBox(Control cellEditorWindow) {
            Object value = null;

            try {
                IJavaProject javaProject = getJavaProject();
                SelectionDialog dialog = MidletSelectionDialogCreator
                        .createMidletSelectionDialog(cellEditorWindow
                                .getShell(), getSite().getPage()
                                .getWorkbenchWindow(), javaProject, false,
                                "Choose midlet to register");

                if (dialog.open() == Window.OK) {
                    Object[] results = dialog.getResult();
                    if ((results != null) && (results.length > 0)) {
                        IType type = (IType) results[0];
                        if (type != null) {
                            value = type.getFullyQualifiedName();
                        }
                    }
                }
            } catch (JavaModelException e) {
                MTJCorePlugin.log(IStatus.ERROR, "openDialogBox", e);
            }

            return value;
        }
    }

    /**
     * Implementation of the table's content provider.
     */
    private class TableContentProvider implements IStructuredContentProvider {
        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /**
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            return pushRegEntries.toArray(new Object[pushRegEntries.size()]);
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer,
         *      java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    /**
     * Implementation of the table's label provider.
     */
    private static class TableLabelProvider extends LabelProvider implements
            ITableLabelProvider {

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
         *      int)
         */
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
         *      int)
         */
        public String getColumnText(Object element, int columnIndex) {
            return ((PushRegEntry) element).fields[columnIndex];
        }
    }

    static class PushRegEntry {
        public String[] fields;

        PushRegEntry(String entryString) {
            fields = new String[3];
            String[] tokens = entryString.split(",");

            for (int i = 0; i < 3; i++) {
                fields[i] = (i > tokens.length) ? "" : tokens[i];
            }
        }

        PushRegEntry(String connStr, String className, String allowedSender) {
            fields = new String[3];

            fields[0] = connStr;
            fields[1] = className;
            fields[2] = allowedSender;
        }

        @Override
        public String toString() {
            StringBuffer sb = new StringBuffer();
            if (fields != null) {
                for (int i = 0; i < fields.length; i++) {
                    if (i != 0) {
                        sb.append(",");
                    }
                    sb.append(fields[i]);
                }
            }
            return sb.toString();
        }

    }

    public static final String ID = "pushregistry";

    /** The prefix of all push registry definition properties */
    public static final String PUSH_REGISTRY_PREFIX = "MIDlet-Push-";

    private static final String PROP_CLASS = "class";

    // Column property names
    private static final String PROP_CONNSTR = "connection string";

    private static final String PROP_SENDER = "allowed sender";

    // All of the properties in order
    private static final String[] PROPERTIES = new String[] { PROP_CONNSTR,
            PROP_CLASS, PROP_SENDER };

    private static final List<String> PROPERTY_LIST = Arrays.asList(PROPERTIES);

    /**
     * Get a string value from the resource bundle
     * 
     * @param key
     * @return
     */
    private static String getResourceString(String key) {
        return MTJUIStrings.getString(key);
    }

    // Buttons
    private Button addButton;

    // The collections of entries representing the midlets to be registered
    private ArrayList<PushRegEntry> pushRegEntries;
    private Button removeButton;

    // The number of midlets registered
    private int storedEntriesCount;

    // The table viewer in use
    private TableViewer tableViewer;

    public JADPushRegistryEditorPage() {
        super(ID, getResourceString("editor.jad.tab.push_registry"));
        pushRegEntries = new ArrayList<PushRegEntry>();
    }

    /**
     * Constructor
     * 
     * @param editor the parent editor for this page
     * @param title the title of this page
     */
    public JADPushRegistryEditorPage(JADFormEditor editor, String title) {
        super(editor, ID, title);
        pushRegEntries = new ArrayList<PushRegEntry>();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor monitor) {
        IPreferenceStore store = getPreferenceStore();

        // Add the push registry entries to the store
        int i;
        int currentEntriesCount = pushRegEntries.size();

        for (i = 0; i < currentEntriesCount; i++) {
            PushRegEntry def = pushRegEntries.get(i);
            store.setValue(PUSH_REGISTRY_PREFIX + (i + 1), def.toString());
        }

        // removing deleted entries
        for (; i < storedEntriesCount; i++) {
            store.setToDefault(PUSH_REGISTRY_PREFIX + (i + 1));
        }

        storedEntriesCount = currentEntriesCount;
        setDirty(false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.editors.jad.AbstractJADEditorPage#editorInputChanged()
     */
    @Override
    public void editorInputChanged() {
        loadPushRegistryEntries();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.WorkbenchPart#getTitle()
     */
    @Override
    public String getTitle() {
        return getResourceString("editor.jad.tab.push_registry");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.editors.jad.AbstractJADEditorPage#isManagingProperty(java.lang.String)
     */
    @Override
    public boolean isManagingProperty(String property) {
        boolean manages = property.startsWith(PUSH_REGISTRY_PREFIX);
        if (manages) {
            String value = property.substring(PUSH_REGISTRY_PREFIX.length());
            try {
                Integer.parseInt(value);
            } catch (NumberFormatException e) {
                manages = false;
            }
        }

        return manages;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#setFocus()
     */
    @Override
    public void setFocus() {
        tableViewer.getTable().setFocus();
    }

    /**
     * Add a new item to the table.
     */
    private void addItem() {
        PushRegEntry midletDefinition = new PushRegEntry("New Push Registry",
                "", "*");
        pushRegEntries.add(midletDefinition);
        tableViewer.refresh();
        setDirty(true);
    }

    /**
     * Create the add and remove buttons to the composite.
     * 
     * @param toolkit the Eclipse Form's toolkit
     * @param parent
     */
    private void createButtons(FormToolkit toolkit, Composite parent) {
        Composite composite = toolkit.createComposite(parent);
        FillLayout layout = new FillLayout();
        layout.type = SWT.VERTICAL;
        composite.setLayout(layout);

        addButton = toolkit.createButton(composite,
                getResourceString("editor.button.add"), SWT.PUSH);
        addButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent se) {
                addItem();
            }
        });

        toolkit.createLabel(composite, "");

        removeButton = toolkit.createButton(composite,
                getResourceString("editor.button.remove"), SWT.PUSH);
        removeButton.setEnabled(false);
        removeButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent se) {
                removeSelectedItems();
            }
        });
    }

    /**
     * Create the table viewer for this editor.
     * 
     * @param toolkit The Eclipse form's toolkit
     * @param parent
     */
    private void createTableViewer(FormToolkit toolkit, Composite parent) {
        String[] columns = new String[] {
                getResourceString("property.jad.midlet_push.connStr"),
                getResourceString("property.jad.midlet.class"),
                getResourceString("property.jad.midlet_push.sender"), };

        // Setup the table
        int styles = SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER
                | SWT.FULL_SELECTION;
        Table table = toolkit.createTable(parent, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        table.setLayoutData(new GridData(GridData.FILL_BOTH));
        table.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                TableItem selected = (TableItem) e.item;
                removeButton.setEnabled(selected.getParent()
                        .getSelectionCount() > 0);
            }
        });
        tableViewer = new TableViewer(table);

        // Set the table layout on the table
        TableLayout layout = new TableLayout();

        int width = 100 / columns.length;
        for (String element : columns) {
            TableColumn column = new TableColumn(table, SWT.NONE);
            column.setText(element);
            layout.addColumnData(new ColumnWeightData(width));
        }
        table.setLayout(layout);

        // Set the content providers
        tableViewer.setContentProvider(new TableContentProvider());
        tableViewer.setLabelProvider(new TableLabelProvider());

        // Wire up the cell modification handling
        tableViewer.setCellModifier(new CellModifier());
        tableViewer.setColumnProperties(PROPERTIES);
        tableViewer.setCellEditors(new CellEditor[] {
                new TextCellEditor(table), new MidletCellEditor(table),
                new TextCellEditor(table), });

        // Get some data into the viewer
        tableViewer.setInput(getEditorInput());
        tableViewer.refresh();
    }

    /**
     * Load the Push Registry Entries from the current preference store.
     */
    private void loadPushRegistryEntries() {
        pushRegEntries.clear();
        IPreferenceStore store = getPreferenceStore();

        // This is sort of ugly, but IPreferenceStore does not
        // allow getting the complete list of preference keys
        for (int i = 1; i < 1000; i++) {
            String propName = PUSH_REGISTRY_PREFIX + i;

            if (store.contains(propName)) {
                String propValue = store.getString(propName);
                pushRegEntries.add(new PushRegEntry(propValue));
            } else {
                break;
            }
        }

        storedEntriesCount = pushRegEntries.size();
        if (tableViewer != null) {
            tableViewer.refresh();
        }
    }

    /**
     * Remove the items currently selected within the table.
     */
    private void removeSelectedItems() {
        int[] indices = tableViewer.getTable().getSelectionIndices();

        for (int i = indices.length; i > 0; i--) {
            int index = indices[i - 1];
            pushRegEntries.remove(index);
        }

        setDirty(true);
        tableViewer.refresh();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createFormContent(IManagedForm managedForm) {
        super.createFormContent(managedForm);

        FormToolkit toolkit = managedForm.getToolkit();

        // Set the top-level layout
        Composite parent = managedForm.getForm().getBody();
        parent.setLayoutData(new GridData(GridData.FILL_BOTH));
        parent.setLayout(new GridLayout(2, false));
        new Label(parent, SWT.NONE);
        new Label(parent, SWT.NONE);

        createTableViewer(toolkit, parent);
        createButtons(toolkit, parent);

        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "eclipseme.ui.help_JADPushRegistryEditorPage");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
     */
    @Override
    protected String getHelpResource() {
        return "/org.eclipse.mtj.docs/docs/jadeditor.html";
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.editors.jad.AbstractJADEditorPage#getSectionDescription()
     */
    protected String getSectionDescription() {
        return "Define the Midlets to be statically registered in Push Registry";
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.editors.jad.AbstractJADEditorPage#getSectionTitle()
     */
    protected String getSectionTitle() {
        return "Push Registry";
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
     */
    @Override
    protected void setInput(IEditorInput input) {
        super.setInput(input);
        if (tableViewer != null) {
            tableViewer.setInput(input);
        }

        setDirty(false);
        loadPushRegistryEntries();
    }
}
