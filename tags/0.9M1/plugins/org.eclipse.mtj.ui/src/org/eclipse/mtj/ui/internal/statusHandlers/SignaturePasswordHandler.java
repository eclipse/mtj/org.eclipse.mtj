/**
 * Copyright (c) 2004,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 */
package org.eclipse.mtj.ui.internal.statusHandlers;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.mtj.core.signing.SignaturePasswords;
import org.eclipse.mtj.ui.internal.dialog.SigningPasswordsDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * This class is a bit of a hack. Status handlers normally are used to report
 * status back to the user. This one is used to actually prompt the user for
 * passwords as required. The
 * org.eclipse.mtj.core.internal.signing.SignatureUtils class calls this through
 * the status handler mechanism when it needs to obtain passwords from the user.
 * 
 */
public class SignaturePasswordHandler implements IStatusHandler {
    
    /**
     * @see org.eclipse.debug.core.IStatusHandler#handleStatus(org.eclipse.core.runtime.IStatus,
     *      java.lang.Object)
     */
    public Object handleStatus(final IStatus status, Object source) {
        Display display = Display.getCurrent();

        DialogRunner runner = new DialogRunner((IProject) source);

        display.syncExec(runner);

        return (runner.getPasswords());
    }

    /**
     *
     */
    private static class DialogRunner implements Runnable {
        IProject project;
        SignaturePasswords passwords;

        /**
         * @param project
         */
        public DialogRunner(IProject project) {
            this.project = project;
        }

        /* (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        public void run() {
            Display display = Display.getCurrent();
            Shell shell = display.getActiveShell();
            if (shell == null) {
                shell = new Shell(display, SWT.NONE);
            }

            SigningPasswordsDialog dlg = new SigningPasswordsDialog(shell,
                    project);
            if (dlg.open() != IDialogConstants.OK_ID) {
                passwords = null;
            } else {
                passwords = new SignaturePasswords();
                passwords.setKeystorePassword(dlg.getKeystorePassword());
                passwords.setKeyPassword(dlg.getKeyPassword());
            }
        }

        /**
         * @return
         */
        public SignaturePasswords getPasswords() {
            return (passwords);
        }
    }
}
