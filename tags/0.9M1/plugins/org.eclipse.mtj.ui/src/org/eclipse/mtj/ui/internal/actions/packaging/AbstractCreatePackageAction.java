/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards, changed Platform#run to 
 *                                SafeRunner#run in doPackageCreation method
 */
package org.eclipse.mtj.ui.internal.actions.packaging;

import java.util.Iterator;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.LoggingSafeRunnable;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.ui.internal.actions.AbstractJavaProjectAction;
import org.eclipse.mtj.ui.internal.actions.ConfigurationErrorDialog;
import org.eclipse.mtj.ui.internal.preferences.ObfuscationPreferencePage;
import org.eclipse.ui.IWorkbenchPart;

/**
 * Abstract action delegate implementation for creating a packaged version of a
 * J2ME project. This action will create a deployed jar containing the
 * application code as well as updating and deploying the JAD file. Subclasses
 * define whether or not the created package will be obfuscated or not.
 * 
 * @author Craig Setera
 */
public abstract class AbstractCreatePackageAction extends
        AbstractJavaProjectAction {
    /**
     * Default constructor
     */
    public AbstractCreatePackageAction() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
     */
    public void run(IAction action) {

        if ((selection != null) && !selection.isEmpty()) {
            boolean proguardConfigured = true;
            if (shouldObfuscate() && !selection.isEmpty()) {
                proguardConfigured = isProguardConfigurationValid();
            }

            if (!proguardConfigured) {
                warnAboutProguardConfiguration();
            } else {
                try {
                    doPackageCreation();
                } catch (CoreException e) {
                    MTJCorePlugin.log(IStatus.ERROR, "Creating package", e);
                }
            }
        }
    }

    /**
     * Performs this action.
     * 
     * @param project the project to be used to generate the package.
     * @param workbenchPart the active editor workbench part.
     */
    public void run(IJavaProject project, IWorkbenchPart workbenchPart) {
        boolean proguardConfigured = true;

        if (shouldObfuscate()) {
            proguardConfigured = isProguardConfigurationValid();
        }

        if (!proguardConfigured) {
            warnAboutProguardConfiguration();
        } else {
            try {
                doPackageCreation(project, workbenchPart);
            } catch (Throwable e) {
                MTJCorePlugin.log(IStatus.ERROR, "Creating package", e);
            }
        }
    }

    /**
     * Create the deployed package for the specified java project.
     * 
     * @param monitor
     * @param javaProject
     */
    private void createPackageForProject(IProgressMonitor monitor,
            IJavaProject javaProject) {
        IMidletSuiteProject suite = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);
        try {
            suite.createPackage(monitor, shouldObfuscate());
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.ERROR, "createPackageForProject", e);
            ErrorDialog.openError(getShell(), "Error packaging "
                    + javaProject.getElementName(), e.getMessage(), e
                    .getStatus());

        }
    }

    /**
     * Return a boolean indicating whether the project has a valid platform
     * definition associated.
     * 
     * @param javaProject
     * @return
     * @throws CoreException
     */
    private boolean doesProjectHaveValidDevice(IJavaProject javaProject) {
        boolean hasValidDevice = false;

        IMidletSuiteProject suite = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);
        if (suite != null) {
            IDevice device = suite.getDevice();
            hasValidDevice = (device != null);
        }

        return hasValidDevice;
    }

    /**
     * Do the work of packaging.
     * 
     * @throws CoreException
     */
    private void doPackageCreation() throws CoreException {
        // Setup the progress monitoring
        ProgressMonitorDialog dialog = new ProgressMonitorDialog(workbenchPart
                .getSite().getShell());
        dialog.open();

        final IProgressMonitor monitor = dialog.getProgressMonitor();
        monitor.beginTask("Create Packages", 3);

        // Create the packages
        Iterator<?> iter = selection.iterator();
        while (iter.hasNext()) {
            final IJavaProject javaProject = getJavaProject(iter.next());

            if (javaProject != null) {
                if (doesProjectHaveValidDevice(javaProject)) {
                    SafeRunner.run(new LoggingSafeRunnable() {
                        public void run() throws Exception {
                            createPackageForProject(monitor, javaProject);
                        }
                    });
                } else {
                    warnAboutInvalidDevice(javaProject);
                }
            }
        }

        // All done
        monitor.done();
        dialog.close();
    }

    /**
     * Do the work of packaging.
     * 
     * @param project the project to be used to generate the package.
     * @param workbenchPart the active editor workbench part.
     * @throws CoreException
     */
    private void doPackageCreation(final IJavaProject javaProject,
            IWorkbenchPart workbenchPart) {
        // Setup the progress monitoring
        ProgressMonitorDialog dialog = new ProgressMonitorDialog(workbenchPart
                .getSite().getShell());
        dialog.open();

        final IProgressMonitor monitor = dialog.getProgressMonitor();
        monitor.beginTask("Create Packages", 3);

        if (javaProject != null) {
            if (doesProjectHaveValidDevice(javaProject)) {
                SafeRunner.run(new LoggingSafeRunnable() {
                    public void run() throws Exception {
                        createPackageForProject(monitor, javaProject);
                    }
                });
            } else {
                warnAboutInvalidDevice(javaProject);
            }
        }

        // All done
        monitor.done();
        dialog.close();
    }

    /**
     * Return a boolean indicating whether the proguard configuration is valid
     * for obfuscation.
     * 
     * @return
     */
    private boolean isProguardConfigurationValid() {
        return MTJCorePlugin.getProguardJarFile().exists();
    }

    /**
     * Warn the user that the project being packaged does not have a valid
     * device and won't be packaged.
     * 
     * @param javaProject
     */
    private void warnAboutInvalidDevice(IJavaProject javaProject) {
        String message = javaProject.getElementName()
                + " does not have a valid device.\n"
                + javaProject.getElementName() + " will not be packaged.";

        MessageDialog.openWarning(getShell(), "Invalid Device", message);
    }

    /**
     * Warn the user that Proguard is not correctly configured for creating
     * obfuscated packages.
     */
    private void warnAboutProguardConfiguration() {
        String message = "Proguard is not correctly configured.\n"
                + "Please configure Proguard preferences.";

        ConfigurationErrorDialog dialog = new ConfigurationErrorDialog(
                getShell(), ObfuscationPreferencePage.ID, "Obfuscation Error",
                message, "Configure Proguard...");

        dialog.open();
    }

    /**
     * Return a boolean indicating whether or not the resulting package should
     * be obfuscated using Proguard.
     * 
     * @return
     */
    protected abstract boolean shouldObfuscate();
}
