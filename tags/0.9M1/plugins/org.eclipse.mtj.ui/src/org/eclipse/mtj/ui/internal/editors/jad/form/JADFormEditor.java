/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Added source view page
 *     Gang Ma      (Sybase)	- Refactoring the editor to support expansibility
 *     Diego Sandin (Motorola)  - Editor title now shows project name to avoid 
 *                                confusion when opening multiple application 
 *                                descriptors
 *     Diego Sandin (Motorola)  -                    
 */
package org.eclipse.mtj.ui.internal.editors.jad.form;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.editors.jad.form.pages.JADSourceEditorPage;
import org.eclipse.mtj.ui.internal.editors.jad.form.pages.OverviewEditorPage;
import org.eclipse.mtj.ui.internal.utils.ManifestPreferenceStore;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IStorageEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.IFormPage;

/**
 * Editor for specifying the properties of a MIDP application descriptor.
 * 
 * @author Craig Setera
 */
public class JADFormEditor extends FormEditor {

    // all JAD editor page configuration elements
    private static JADEditorPageConfigElement[] jadEditorPageConfigElements;

    // Whether to do a clean just after a save
    private boolean cleanRequired;

    // the JAD editor pages defined as extension
    private List<AbstractJADEditorPage> configPages = new ArrayList<AbstractJADEditorPage>();

    // The source editor page in the editor
    private JADSourceEditorPage sourceEditor;

    // The underlying file being operated on
    private IFile jadFile;

    // The most recent timestamp of the JAD file
    private long modificationStamp;

    // The preference store we are using to
    // represent the edit state
    private ManifestPreferenceStore preferenceStore;

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    @SuppressWarnings("unchecked")
    public void doSave(IProgressMonitor monitor) {
        monitor.beginTask("", getPageCount() + 1);

        int i = 0;
        Iterator<Object> pageIter = pages.iterator();

        while (pageIter.hasNext()) {
            Object part = pageIter.next();

            if (part instanceof IFormPage) {

                IFormPage formPage = (IFormPage) part;

                // If the control hasn't been created yet, it can't be dirty yet
                if ((formPage != null) && (formPage.getPartControl() != null)) {
                    if (formPage.isDirty()) {
                        formPage.doSave(monitor);
                    }
                }
            }
            monitor.worked(i + 1);
        }

        try {
            // Attempt to make the file read/write as necessary, using
            // the resource API so that Team providers can get involved.
            if (jadFile.exists() && jadFile.isReadOnly()) {
                ResourceAttributes attributes = jadFile.getResourceAttributes();
                attributes.setReadOnly(false);
                jadFile.setResourceAttributes(attributes);
            }

            preferenceStore.save();

            if ((jadFile != null) && (jadFile.exists())) {
                jadFile.refreshLocal(IResource.DEPTH_ZERO, monitor);
            }

            // Do a clean if requested
            if ((jadFile != null) && cleanRequired) {
                jadFile.getProject().build(
                        IncrementalProjectBuilder.CLEAN_BUILD, monitor);
            }
        } catch (IOException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        }

        monitor.done();
        editorDirtyStateChanged();
        reloadLocalFile();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.EditorPart#doSaveAs()
     */
    @Override
    public void doSaveAs() {
        // Not allowed...
    }

    /**
     * Return the JAD file (if set)
     * 
     * @return
     */
    public IFile getJadFile() {
        return jadFile;
    }

    /**
     * Get the IPreferenceStore in use for the edit function.
     * 
     * @return
     */
    public ManifestPreferenceStore getPreferenceStore() {
        return preferenceStore;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormEditor#isDirty()
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean isDirty() {
        boolean dirty = false;

        Iterator<Object> pageIter = pages.iterator();
        while (pageIter.hasNext()) {

            Object part = pageIter.next();
            if (part instanceof IFormPage) {

                IFormPage formPage = (IFormPage) part;

                // If the control hasn't been created yet, it can't be dirty yet
                if ((formPage != null) && (formPage.getPartControl() != null)) {
                    if (formPage.isDirty()) {
                        dirty = true;
                        break;
                    }
                }
            }
        }

        return dirty;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
     */
    @Override
    public boolean isSaveAsAllowed() {
        // Don't allow "Save As..."
        return false;
    }

    /**
     * Return a boolean indicating whether the specified property key is an
     * allowable user-defined property.
     * 
     * @param key
     * @return
     */
    public boolean isUserDefinedPropertyKey(String key) {
        Iterator<AbstractJADEditorPage> pageIter = configPages.iterator();
        while (pageIter.hasNext()) {
            AbstractJADEditorPage jadEditorPage = pageIter.next();
            if (jadEditorPage != null) {
                if (jadEditorPage.isManagingProperty(key)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Set a boolean indicating whether a clean is required when the save is
     * complete.
     * 
     * @param value
     */
    public void setCleanRequired(boolean value) {
        cleanRequired = value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.MultiPageEditorPart#setFocus()
     */
    @Override
    public void setFocus() {
        File localFile = getLocalFile();

        if ((localFile != null)
                && (localFile.lastModified() > modificationStamp)) {
            if (shouldReloadLocalFile()) {
                reloadLocalFile();
            }
        }

        super.setFocus();
    }

    /**
     * Return all Config Elements
     * 
     * @return the list of Config Elements
     */
    private JADEditorPageConfigElement[] getAllEditorPageConfigElements() {
        if (jadEditorPageConfigElements == null) {
            jadEditorPageConfigElements = readAllVendorSpecJADAttributes();
        }
        return jadEditorPageConfigElements;
    }

    /**
     * Return the jad file as a local File instance.
     * 
     * @return
     */
    private File getLocalFile() {
        return jadFile.getLocation().toFile();
    }

    private JADEditorPageConfigElement[] readAllVendorSpecJADAttributes() {
        String plugin = MTJUIPlugin.getDefault().getBundle().getSymbolicName();
        IConfigurationElement[] configElements = Platform
                .getExtensionRegistry().getConfigurationElementsFor(plugin,
                        "jadEditorPages");
        JADEditorPageConfigElement[] resultElements = new JADEditorPageConfigElement[configElements.length];
        for (int i = 0; i < configElements.length; i++) {
            resultElements[i] = new JADEditorPageConfigElement(
                    configElements[i]);
        }
        return resultElements;
    }

    /**
     * 
     */
    private void reloadLocalFile() {
        updateEditorInput();

        Iterator<AbstractJADEditorPage> pageIter = configPages.iterator();
        while (pageIter.hasNext()) {
            AbstractJADEditorPage jadEditorPage = pageIter.next();
            if (jadEditorPage != null) {
                jadEditorPage.editorInputChanged();
            }
        }
    }

    /**
     * Return a boolean indicating whether the user wants to reload the updated
     * file.
     * 
     * @return <code>true</code> if the user presses the OK button,
     *         <code>false</code> otherwise
     */
    private boolean shouldReloadLocalFile() {
        return MessageDialog.openQuestion(getSite().getShell(), "File Updated",
                "The file has been updated.  Would you like to reload?");
    }

    /**
     * Update the editor input.
     * 
     * @throws IOException
     */
    private void updateEditorInput() {
        File localFile = getLocalFile();
        modificationStamp = localFile.lastModified();
        String filename = localFile.toString();
        preferenceStore = new ManifestPreferenceStore(filename);

        try {
            preferenceStore.load();
        } catch (IOException e) {
            MTJCorePlugin.log(IStatus.WARNING, e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
     */
    @Override
    protected void addPages() {

        super.setPartName(jadFile.getProject().getName());

        JADEditorPageConfigElement[] pageElements = getAllEditorPageConfigElements();

        // Sort the page elements according to the page element's priority
        Arrays.sort(pageElements, new Comparator<JADEditorPageConfigElement>() {
            public int compare(JADEditorPageConfigElement o1,
                    JADEditorPageConfigElement o2) {
                return o1.getPriority() - o2.getPriority();
            }
        });

        /* Add the overview page */
        try {
            AbstractJADEditorPage page = new OverviewEditorPage(this);
            addPage(page);
            configPages.add(page);
        } catch (PartInitException e1) {
            e1.printStackTrace();
        }

        /* Add the property editor pages */
        for (JADEditorPageConfigElement element : pageElements) {
            try {
                AbstractJADEditorPage page = element.getJADEditorPage();
                page.initialize(this);
                addPage(page);
                configPages.add(page);
            } catch (Exception e) {
                MTJCorePlugin.log(IStatus.ERROR, e);
            }
        }

        /* Add the source editor page */
        sourceEditor = new JADSourceEditorPage(this);
        try {
            addPage(sourceEditor, super.getEditorInput());
        } catch (PartInitException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
     */
    @Override
    protected void setInput(IEditorInput input) {
        super.setInput(input);

        // Update the application descriptor instance
        if (input instanceof IStorageEditorInput) {
            IStorageEditorInput storageInput = (IStorageEditorInput) input;
            try {
                // Read the storage from the file system as
                // a preference store
                IPath storagePath = storageInput.getStorage().getFullPath();
                if (storagePath != null) {
                    IWorkspaceRoot root = MTJCorePlugin.getWorkspace()
                            .getRoot();
                    jadFile = root.getFile(storagePath.makeAbsolute());

                    if ((jadFile != null) && (jadFile.exists())) {
                        updateEditorInput();
                    }
                }

            } catch (Exception e) {
                MTJCorePlugin.log(IStatus.WARNING, e);
            }
        }
    }
}
