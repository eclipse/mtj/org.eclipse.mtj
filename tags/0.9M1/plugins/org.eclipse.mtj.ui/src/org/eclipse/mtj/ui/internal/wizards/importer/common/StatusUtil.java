/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Copy from 
 *     		org.eclipse.ui.internal.wizards.datatransfer.DataTransferMessages,
 *     		which located in org.eclipse.ui.ide plug-in. Remove unused methods.
 *	   Hugo Raniere (Motorola) - Moved to commons package to be reused by
 *			other importers.
 *******************************************************************************/
package org.eclipse.mtj.ui.internal.wizards.importer.common;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.ui.IMTJUIConstants;

/**
 * Utility class to create status objects.
 * 
 * PRIVATE This class is an internal implementation class and should not be
 * referenced or sub-classed outside of the workbench
 * 
 */
public class StatusUtil {
	/**
	 * This method must not be called outside the workbench.
	 * 
	 * Utility method for creating status.
	 * 
	 * @param severity
	 * @param message
	 * @param exception
	 * @return {@link IStatus}
	 */
	public static IStatus newStatus(int severity, String message,
			Throwable exception) {

		String statusMessage = message;
		if (message == null || message.trim().length() == 0) {
			if (exception == null) {
				throw new IllegalArgumentException();
			} else if (exception.getMessage() == null) {
				statusMessage = exception.toString();
			} else {
				statusMessage = exception.getMessage();
			}
		}

		return new Status(severity, IMTJUIConstants.PLUGIN_ID, severity,
				statusMessage, exception);
	}
}
