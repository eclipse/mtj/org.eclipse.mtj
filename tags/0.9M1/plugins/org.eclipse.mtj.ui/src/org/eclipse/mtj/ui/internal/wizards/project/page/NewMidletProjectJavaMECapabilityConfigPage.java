/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Hugo Raniere (Motorola)  - Removing Preprocessor code
 *     Hugo Raniere (Motorola)  - Reimplementing JavaMEClasspathContainer
 *     Diego Sandin (Motorola)  - Fixed Java 5 warnings
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code
 */
package org.eclipse.mtj.ui.internal.wizards.project.page;

import java.util.ArrayList;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Preferences;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.ui.PreferenceConstants;
import org.eclipse.jdt.ui.wizards.JavaCapabilityConfigurationPage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.preprocessor.PreprocessorBuilder;
import org.eclipse.mtj.ui.internal.wizards.project.NewMIDLetProjectWizard;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;

/**
 * Handler for Java capability configuration.
 * 
 * @author Craig Setera
 */
public class NewMidletProjectJavaMECapabilityConfigPage extends
        JavaCapabilityConfigurationPage {

    /**
     * 
     */
    public NewMidletProjectJavaMECapabilityConfigPage() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jdt.ui.wizards.NewElementWizardPage#setVisible(boolean)
     */
    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            updateConfiguration();
        }

        super.setVisible(visible);
    }

    /**
     * Update the java configuration before making the page visible.
     */
    public void updateConfiguration() {
        // Work out the project and path information
        IProject project = getNewProjectCreatePage().getProjectHandle();
        IJavaProject javaProject = JavaCore.create(project);
        IPath projectPath = project.getFullPath();

        // Initialize the classpath entries using the source directories
        // and classpath container
        ArrayList<IClasspathEntry> entryList = new ArrayList<IClasspathEntry>();
        entryList.add(getSrcPathEntry(projectPath));
        addResourcesDirectoryIfRequested(entryList, project);
        entryList.add(getJavaMEClasspathContainer());
        IClasspathEntry[] entries = entryList
                .toArray(new IClasspathEntry[entryList.size()]);
        init(javaProject, null, entries, false);
    }

    /**
     * Add a resources directory as a source path entry if the user preferences
     * requested.
     * 
     * @param entryList
     * @param project
     */
    private void addResourcesDirectoryIfRequested(
            ArrayList<IClasspathEntry> entryList, IProject project) {
        Preferences prefs = MTJCorePlugin.getDefault().getPluginPreferences();

        if (useSourceAndBinaryFolders()
                && prefs.getBoolean(IMTJCoreConstants.PREF_USE_RESOURCES_DIR)) {
            // Create the resources directory if it doesn't already exist
            String resDirName = prefs
                    .getString(IMTJCoreConstants.PREF_RESOURCES_DIR);
            IFolder resFolder = project.getFolder(resDirName);

            // Add it as a source folder to the java project
            entryList.add(JavaCore.newSourceEntry(resFolder.getFullPath()));
        }
    }

    /**
     * Returns the classpath container that includes all the libraries present
     * in the project's device
     * 
     * @return an instance of an IClasspathEntry of kind CPE_CONTAINER
     */
    private IClasspathEntry getJavaMEClasspathContainer() {
        return ((NewMIDLetProjectWizard) getWizard())
                .getJavaMEClasspathContainer();

    }

    /**
     * Find the new project creation page in our wizard.
     * 
     * @return
     */
    private WizardNewProjectCreationPage getNewProjectCreatePage() {
        return (WizardNewProjectCreationPage) getWizard().getPage(
                NewMidletProjectCreationPage.MAIN_PAGE_NAME);
    }

    /**
     * Get the source path for the project taking into account the new project
     * preferences that the user has specified.
     * 
     * @param projectPath
     * @return
     */
    private IPath getSrcPath(IPath projectPath) {
        IPath srcPath = projectPath;

        if (useSourceAndBinaryFolders()) {
            IPreferenceStore store = PreferenceConstants.getPreferenceStore();
            String srcPathName = store
                    .getString(PreferenceConstants.SRCBIN_SRCNAME);
            srcPath = projectPath.append(srcPathName);
        }

        return srcPath;
    }

    /**
     * Return an IClasspathEntry for the source path.
     * 
     * @param projectPath
     * @return
     */
    private IClasspathEntry getSrcPathEntry(IPath projectPath) {
        IPath srcPath = getSrcPath(projectPath);

        // Set up exclusions for the verified and deployed directories
        // if the source and project directories are the same
        IPath[] exclusions = null;
        if (srcPath.equals(projectPath)) {
            exclusions = new IPath[3];
            exclusions[0] = new Path(MTJCorePlugin.getDeploymentDirectoryName()
                    + "/");
            exclusions[1] = new Path(IMTJCoreConstants.TEMP_FOLDER_NAME + "/");
            exclusions[2] = new Path(PreprocessorBuilder.PROCESSED_DIRECTORY
                    + "/");
        } else {
            exclusions = new IPath[0];
        }

        return JavaCore.newSourceEntry(srcPath, exclusions);
    }

    /**
     * Return a boolean indicating whether there will be separate source and
     * binary folders in the project.
     * 
     * @return
     */
    private boolean useSourceAndBinaryFolders() {
        IPreferenceStore store = PreferenceConstants.getPreferenceStore();
        return store.getBoolean(PreferenceConstants.SRCBIN_FOLDERS_IN_NEWPROJ);
    }
}
