/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma 		(Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.editors.jad.form.pages;

import org.eclipse.mtj.core.model.jad.DescriptorPropertyDescription;
import org.eclipse.mtj.core.model.jad.IJADConstants;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider;

/**
 * provide OTA JAD attributes descriptors
 * 
 * @author Gang Ma
 */
public class OTAJADDescriptorsProvider implements IJADDescriptorsProvider {

    /**
     * OTA property descriptors.
     */
    private static final DescriptorPropertyDescription[] OTA_DESCRIPTORS = new DescriptorPropertyDescription[] {
            new DescriptorPropertyDescription(
                    IJADConstants.JAD_MIDLET_DELETE_CONFIRM,
                    getResourceString("property.jad.midlet_delete_confirm"),
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(
                    IJADConstants.JAD_MIDLET_DELETE_NOTIFY,
                    getResourceString("property.jad.midlet_delete_notify"),
                    DescriptorPropertyDescription.DATATYPE_URL),
            new DescriptorPropertyDescription(
                    IJADConstants.JAD_MIDLET_INSTALL_NOTIFY,
                    getResourceString("property.jad.midlet_install_notify"),
                    DescriptorPropertyDescription.DATATYPE_URL), };

    /**
     * Get a string value from the resource bundle
     * 
     * @param key
     * @return
     */
    protected static String getResourceString(String key) {
        return MTJUIStrings.getString(key);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider#getDescriptorPropertyDescriptions()
     */
    public DescriptorPropertyDescription[] getDescriptorPropertyDescriptions() {
        return OTA_DESCRIPTORS;
    }

}
