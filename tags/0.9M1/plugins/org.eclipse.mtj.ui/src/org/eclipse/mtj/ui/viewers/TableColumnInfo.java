/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.viewers;

import java.util.Comparator;

import org.eclipse.swt.SWT;

/**
 * Instances of this class provide necessary metadata for the creation of a
 * table column within table viewer.
 * 
 * @author Craig Setera
 */
public class TableColumnInfo {

    private String name;
    private float defaultWidthPercent;
    private int alignment;
    private Comparator<Object> comparator;

    /**
     * Create a new table column info
     * 
     * @param name
     * @param defaultWidthPercent
     * @param comparator
     */
    public TableColumnInfo(String name, float defaultWidthPercent,
            Comparator<Object> comparator) {
        this(name, defaultWidthPercent, SWT.LEFT, comparator);
    }

    /**
     * Create a new table column info
     * 
     * @param name
     * @param defaultWidthPercent
     * @param comparator
     */
    public TableColumnInfo(String name, float defaultWidthPercent,
            int alignment, Comparator<Object> comparator) {
        super();
        this.name = name;
        this.defaultWidthPercent = defaultWidthPercent;
        this.alignment = alignment;
        this.comparator = comparator;
    }

    /**
     * @return Returns the alignment.
     */
    public int getAlignment() {
        return alignment;
    }

    /**
     * @return Returns the comparator.
     */
    public Comparator<Object> getComparator() {
        return comparator;
    }

    /**
     * @return Returns the defaultWidthPercent.
     */
    public float getDefaultWidthPercent() {
        return defaultWidthPercent;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param alignment The alignment to set.
     */
    public void setAlignment(int alignment) {
        this.alignment = alignment;
    }

    /**
     * @param comparator The comparator to set.
     */
    public void setComparator(Comparator<Object> comparator) {
        this.comparator = comparator;
    }

    /**
     * @param defaultWidthPercent The defaultWidthPercent to set.
     */
    public void setDefaultWidthPercent(float weight) {
        this.defaultWidthPercent = weight;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }
}
