/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.ui.internal.wizards.project;

import org.eclipse.osgi.util.NLS;

/**
 * Provides convenience methods for manipulating messages.
 * 
 * @author Diego Madruga Sandin
 */
public class NewMidletProjectWizardMessages extends NLS {

    public static String NewMIDLetProjectWizard_title;

    public static String NewMidletProjectCreationPage_title;
    public static String NewMidletProjectCreationPage_description;
    public static String NewMidletProjectCreationPage_preprocessor;

    public static String NewMidletProjectPropertiesPage_title;
    public static String NewMidletProjectPropertiesPage_description;

    public static String NewMidletProjectPropertiesPage_validate_devicecount_error;
    public static String NewMidletProjectPropertiesPage_validate_deviceselection_error;
    public static String NewMidletProjectPropertiesPage_validate_jadname_error_invalidname;
    public static String NewMidletProjectPropertiesPage_validate_jadname_error_extension;
    public static String NewMidletProjectPropertiesPage_validate_jadname_error_emptyname;

    public static String NewMidletProjectPropertiesPage_jad_groupname;
    public static String NewMidletProjectPropertiesPage_jad_label;

    private static final String BUNDLE_NAME = "org.eclipse.mtj.ui.internal."
            + "wizards.project.messages";

    static {
        NLS.initializeMessages(BUNDLE_NAME,
                NewMidletProjectWizardMessages.class);
    }
}
