/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Added preprocessing entries generation from 
 *                                device properties                               
 */
package org.eclipse.mtj.ui.internal.devices;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.device.preprocess.DeviceSymbolDefinitionSetFactory;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSetRegistry;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;

/**
 * Implements the device import wizard functionality.
 * 
 * @author Craig Setera
 */
public class DeviceImportWizard extends Wizard {
    private DeviceImportWizardPage wizardPage;

    /**
     * Construct a new wizard instance
     */
    public DeviceImportWizard() {
        super();
        setNeedsProgressMonitor(true);
        setWindowTitle("Import Devices");
        setDialogSettings(MTJUIPlugin.getDialogSettings(getClass().getName()));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.wizard.Wizard#addPages()
     */
    @Override
    public void addPages() {
        wizardPage = new DeviceImportWizardPage();
        addPage(wizardPage);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        boolean finished = false;

        IDevice[] selectedDevices = wizardPage.getSelectedDevices();
        boolean generateDefinitionSet = wizardPage.isGenerateDefinitionSet();

        for (IDevice device : selectedDevices) {
            try {
                DeviceRegistry.singleton.addDevice(device, true);

                if (generateDefinitionSet) {
                    SymbolDefinitionSet definitionSet = DeviceSymbolDefinitionSetFactory
                            .createSymbolDefinitionSet(device);
                    SymbolDefinitionSetRegistry.singleton
                            .addDefinitionSet(definitionSet);
                    SymbolDefinitionSetRegistry.singleton.store();

                }
                finished = true;
            } catch (Exception e) {
                MTJCorePlugin
                        .log(IStatus.WARNING, "Error adding new device", e);
                MTJUIPlugin.displayError(getShell(), IStatus.WARNING, -999,
                        "Device Registry Error",
                        "Error adding new device to device registry.", e);
            }
        }

        return finished;
    }
}
