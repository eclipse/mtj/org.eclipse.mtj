/**
 * Copyright (c) 2004,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 */
package org.eclipse.mtj.ui;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * This class provides the means to internationalize strings that are located in
 * the org.eclipse.mtj.ui package. It sets up a ResourceBundle based on the file
 * <code>MTJUIStrings.properties</code> in the org.eclipse.mtj.core package,
 * and allows retrieval of keyed strings from that bundle.
 * 
 * @author Kevin Hunter
 */
public class MTJUIStrings {

    /**
     * The base name of the MTJ resource bundle
     */
    private static final String BUNDLE_NAME = "org.eclipse.mtj.ui."
            + "MTJUIPluginResources";

    /**
     * MTJ resource bundle that contain locale-specific objects.
     */
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
            .getBundle(BUNDLE_NAME);

    /**
     * Returns the string from the plugin's resource bundle.
     * 
     * @param key key the key for the desired string
     * @return the string for the given key, or <code>null</code> if not found
     */
    public static String getBundleString(String key) {
        String result = null;
        try {
            result = RESOURCE_BUNDLE.getString(key).trim();
        } catch (MissingResourceException e) {
        }

        return (result);
    }

    /**
     * Returns the string from the plugin's resource bundle, or the key itself
     * if no string was found.
     * 
     * @param key the key for the desired string
     * @return the string for the given key, or the key itself if no string was
     *         found
     */
    public static String getString(String key) {
        String result = key;
        try {
            result = RESOURCE_BUNDLE.getString(key).trim();
        } catch (MissingResourceException e) {
        }

        return (result);
    }

    /**
     * Returns the string from the plugin's resource bundle, or 'key' if not
     * found. Substitutions will be made if supplied.
     * 
     * @param key the key for the desired string.
     * @param substitutions format arguments.
     * @return Formatted string.
     */
    public static String getString(String key, Object[] substitutions) {
        String result = '!' + key + '!';
        try {
            result = RESOURCE_BUNDLE.getString(key).trim();
        } catch (MissingResourceException e) {
        }

        return MessageFormat.format(result, substitutions);
    }

}
