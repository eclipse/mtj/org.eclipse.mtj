/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.utils;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.widgets.Composite;

/**
 * A BooleanFieldEditor subclass that enables and disables another
 * StringFieldEditor based on the state of the boolean field.
 * 
 * @author Craig Setera
 */
public class ValueChangeTrackingBooleanFieldEditor extends BooleanFieldEditor {

    private Composite parent;
    private StringFieldEditor fieldEditor;

    /**
     * Construct a new tracking field editor.
     * 
     * @param name
     * @param label
     * @param parent
     */
    public ValueChangeTrackingBooleanFieldEditor(String name, String label,
            Composite parent) {
        super(name, label, parent);
        this.parent = parent;
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditor#doLoad()
     */
    protected void doLoad() {
        super.doLoad();
        updateEditorEnablement();
    }

    /**
     * Get the field editor
     * 
     * @return
     */
    public StringFieldEditor getFieldEditor() {
        return fieldEditor;
    }

    /**
     * Set the field editor
     * 
     * @param editor
     */
    public void setFieldEditor(StringFieldEditor editor) {
        fieldEditor = editor;
    }

    /**
     * Update the enablement of the field editor controls based on the current
     * state of the boolean values.
     */
    private void updateEditorEnablement() {
        if (fieldEditor != null) {
            boolean value = getBooleanValue();
            fieldEditor.getLabelControl(parent).setEnabled(value);
            fieldEditor.getTextControl(parent).setEnabled(value);
        }
    }

    /**
     * @see org.eclipse.jface.preference.BooleanFieldEditor#valueChanged(boolean,
     *      boolean)
     */
    protected void valueChanged(boolean oldValue, boolean newValue) {
        super.valueChanged(oldValue, newValue);
        updateEditorEnablement();
    }
}
