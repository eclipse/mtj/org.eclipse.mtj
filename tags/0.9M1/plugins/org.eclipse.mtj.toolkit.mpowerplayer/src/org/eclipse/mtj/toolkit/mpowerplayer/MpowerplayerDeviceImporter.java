/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Added support to MPowerPlayer embedded 
 *                                preverifier on Mac OS X
 *     Diego Sandin (Motorola)  - Hard coded the CLDC and MIDP libraries                           
 */
package org.eclipse.mtj.toolkit.mpowerplayer;

import java.io.File;
import java.io.FileFilter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.importer.LibraryImporter;
import org.eclipse.mtj.core.importer.impl.JavaEmulatorDeviceImporter;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.Classpath;
import org.eclipse.mtj.core.model.ReplaceableParametersProcessor;
import org.eclipse.mtj.core.model.Version;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.library.api.API;
import org.eclipse.mtj.core.model.library.api.APIType;
import org.eclipse.mtj.core.model.preverifier.IPreverifier;
import org.eclipse.mtj.core.model.preverifier.StandardPreverifierFactory;

/**
 * Implementor of the IDeviceImporter interface that imports a device reference
 * to the <a href="http://www.mpowerplayer.com/">MPowerPlayer</a> device.
 * 
 * @author Craig Setera
 */
public class MpowerplayerDeviceImporter extends JavaEmulatorDeviceImporter {

    // Properties file holding emulator/device information
    private static final String PROPS_FILE = "mpowerplayer.properties";

    // Various pieces of static information
    private static final String PLAYER_JAR_NAME = "player.jar";
    private static final String PLAYER_MAIN_CLASS = "com.mpp.player.PowerPlayerApp";
    private static final String PLAYER_MACOSX_PREVERIFIER = "/osx/preverify";

    private static final String MACOSX_OS_NAME = "Mac OS X";

    private static String osName = System.getProperty("os.name");

    /**
     * @see org.eclipse.mtj.core.importer.IDeviceImporter#getMatchingDevices(java.io.File,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public IDevice[] getMatchingDevices(File directory, IProgressMonitor monitor) {
        IDevice[] devices = null;

        try {
            File jarFile = new File(directory, PLAYER_JAR_NAME);
            if (jarFile.exists()
                    && hasMainClassAttribute(jarFile, PLAYER_MAIN_CLASS)) {
                IDevice device = createDevice(jarFile);
                devices = new IDevice[] { device };
            }
        } catch (Exception e) {
            MTJCorePlugin.log(IStatus.WARNING, "Error importing mpowerplayer",
                    e);
        }

        return devices;
    }

    /**
     * Add the MPowerPlayer device libraries.
     * 
     * @param jarFile
     * @param classpath
     * @param importer
     */
    private void addMplayerDeviceLibraries(File jarFile, Classpath classpath,
            LibraryImporter importer) {

        // Now add the player libraries
        String classpathString = getDeviceProperties().getProperty("classpath",
                "");

        Map<String, String> replaceableParameters = new HashMap<String, String>();
        replaceableParameters.put("mpproot", jarFile.getParent());

        classpathString = ReplaceableParametersProcessor
                .processReplaceableValues(classpathString,
                        replaceableParameters);
        String[] entries = classpathString.split(";");

        for (int i = 0; i < entries.length; i++) {
            ILibrary library = importer.createLibraryFor(new File(entries[i]));

            // Because of the structure of the MPowerPlayer libraries,
            // we need to hard code the CLDC and MIDP libraries
            API api = library.getAPI(APIType.UNKNOWN);
            if (api != null) {
                if (api.getIdentifier().equalsIgnoreCase("cldc-1.1.jar")) {
                    api.setIdentifier("CLDC");
                    api.setType(APIType.CONFIGURATION);
                    api.setName("Connected Limited Device Configuration");
                    api.setVersion(new Version("1.1"));
                } else if (api.getIdentifier().equalsIgnoreCase("midp-2.0.jar")) {
                    api.setIdentifier("MIDP");
                    api.setType(APIType.PROFILE);
                    api.setName("Mobile Information Device Profile");
                    api.setVersion(new Version("2.0"));

                }
            }
            classpath.addEntry(library);
        }
    }

    /**
     * Create a device instance based on the specified player jar file.
     * 
     * @param jarFile
     * @return
     */
    private IDevice createDevice(File jarFile) {
        MpowerplayerDevice device = new MpowerplayerDevice();

        device.setBundle(MpowerplayerPlugin.getDefault().getBundle()
                .getSymbolicName());
        device.setClasspath(getDeviceClasspath(jarFile));
        device.setDebugServer(isDebugServer());
        device.setDescription("Mpowerplayer Device");
        device.setDeviceProperties(new Properties());
        device.setGroupName("Mpowerplayer");
        device.setName("Mpowerplayer");
        device.setPreverifier(getPreverifier(jarFile));
        device.setProtectionDomains(new String[0]);
        device.setLaunchCommandTemplate(getLaunchCommand());
        device.setMppRoot(jarFile.getParentFile());

        return device;
    }

    @Override
    protected IPreverifier getPreverifier(File jarFile) {

        IPreverifier preverifier = super.getPreverifier(jarFile);

        if (osName.equals(MACOSX_OS_NAME)) {

            File macOsPreverifierPath = new File(jarFile.getParentFile()
                    .getPath()
                    + PLAYER_MACOSX_PREVERIFIER);
            File preverifyExecutable = findEmbeddedPreverifyExtecutable(macOsPreverifierPath);

            if (preverifyExecutable != null) {
                try {
                    preverifier = StandardPreverifierFactory
                            .createPreverifier(preverifyExecutable);
                } catch (CoreException e) {
                    MTJCorePlugin
                            .log(
                                    IStatus.WARNING,
                                    "Error importing mpowerplayer Preverifier for Mac OS X",
                                    e);
                }
            }
        }
        return preverifier;
    }

    /**
     * Get the device classpath based on the specified player.jar file.
     * 
     * @param jarFile
     * @return
     */
    private Classpath getDeviceClasspath(File jarFile) {
        Classpath classpath = new Classpath();
        LibraryImporter importer = new LibraryImporter();

        addMplayerDeviceLibraries(jarFile, classpath, importer);

        return classpath;
    }

    /**
     * Return the URL to access the device properties file.
     * 
     * @return
     */
    protected URL getDevicePropertiesURL() {
        return MpowerplayerPlugin.getDefault().getBundle().getEntry(PROPS_FILE);
    }

    /**
     * Find a preverify executable in the specified directory or subdirectories.
     * 
     * @param directory
     * @return
     */
    private File findEmbeddedPreverifyExtecutable(File directory) {
        File[] files = directory.listFiles(new FileFilter() {

            /*
             * (non-Javadoc)
             * 
             * @see java.io.FileFilter#accept(java.io.File)
             */
            public boolean accept(File pathname) {
                String name = pathname.getName();
                return pathname.isDirectory() || (name.equals("preverify"));
            }
        });

        File executable = null;
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                executable = findEmbeddedPreverifyExtecutable(files[i]);
            } else {
                executable = files[i];
                break;
            }
        }

        return executable;
    }
}
