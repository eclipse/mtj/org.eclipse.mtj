/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Refactored class to handle SymbolSets instead 
 *                                of properties
 */
package org.eclipse.mtj.internal.ui.editors.device.pages;

import java.util.Collection;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.internal.ui.viewers.TableViewerConfiguration;
import org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;

/**
 * Device editor for the device properties.
 * 
 * @author Craig Setera
 */
public class DeviceSymbolSetEditorPage extends AbstractDeviceEditorPage {

    /**
     * The Dialog Settings ID that will be used to retrieve an
     * {@link IDialogSettings} using the {@link MTJUIPlugin#getDialogSettings()}
     * facade method.
     */
    private static final String DEVICE_SYMBOLSET_VIEWER_SETTINGS = "deviceSymbolsetViewerSettings"; //$NON-NLS-1$

    /**
     * Content provider for a device's SymbolSet entries
     */
    private static class DevicePropertiesContentProvider implements
            IStructuredContentProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            if (inputElement instanceof IMIDPDevice) {
                IDevice device = (IDevice) inputElement;
                ISymbolSet deviceProperties = device.getSymbolSet();
                if (deviceProperties != null) {
                    Collection<ISymbol> collection = deviceProperties
                            .getSymbols();
                    elements = collection.toArray();
                }
            }
            return elements;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    private static final TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
            new TableColumnInfo(
                    MTJUIMessages.DeviceSymbolsetEditorPage_symbol_columnInfo,
                    40f, null),
            new TableColumnInfo(
                    MTJUIMessages.DeviceSymbolsetEditorPage_value_columnInfo,
                    60f, null), };

    // Column information structure
    private static final int DEFAULT_TABLE_WIDTH = 650;
    private static final Object[] NO_ELEMENTS = new Object[0];

    // Widgets
    private TableViewer viewer;

    /**
     * Construct the editor page.
     * 
     * @param parent
     * @param style
     */
    public DeviceSymbolSetEditorPage(Composite parent, int style) {
        super(parent, style);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#commitDeviceChanges()
     */
    @Override
    public void commitDeviceChanges() {
        // TODO Not allowing changes in this editor yet
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#getDescription()
     */
    @Override
    public String getDescription() {
        return MTJUIMessages.DeviceSymbolsetEditorPage_description;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#getTitle()
     */
    @Override
    public String getTitle() {
        return MTJUIMessages.DeviceSymbolsetEditorPage_title;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#setDevice(org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice)
     */
    @Override
    public void setDevice(IMIDPDevice device) {
        super.setDevice(device);
        viewer.setInput(device);
    }

    /**
     * Create the devices table viewer.
     * 
     * @param parent
     */
    private TableViewer createTableViewer(Composite composite) {
        int styles = SWT.SINGLE | SWT.V_SCROLL | SWT.BORDER
                | SWT.FULL_SELECTION;
        Table table = new Table(composite, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        // Wire up the viewer
        TableViewer viewer = new TableViewer(table);
        viewer.setContentProvider(new DevicePropertiesContentProvider());
        viewer.setLabelProvider(MTJUIPlugin.getDefault().getLabelProvider());

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings(DEVICE_SYMBOLSET_VIEWER_SETTINGS);
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 0);
        viewerConfiguration.configure(viewer);

        return viewer;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#addPageControls(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void addPageControls(Composite parent) {
        parent.setLayoutData(new GridData(GridData.FILL_BOTH));
        parent.setLayout(new GridLayout(2, false));

        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.minimumWidth = DEFAULT_TABLE_WIDTH;
        gridData.heightHint = 400;
        viewer = createTableViewer(parent);
        viewer.getTable().setLayoutData(gridData);
    }
}
