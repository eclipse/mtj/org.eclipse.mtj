/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Aragao (Motorola) - Initial version
 */

package org.eclipse.mtj.internal.ui.util;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.ui.IJavaElementSearchConstants;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.mtj.internal.core.util.TestSuiteSarchScope;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.SelectionDialog;

/**
 * A simple helper class for creating a Type selection dialog that allows for
 * selection of Test Suite subclasses.
 * 
 * @author David Aragao
 */

public class TestSuiteSelectionDialogCreator {
	
	 /**
     * Create a new Test Suite selection dialog.
     * 
     * @param shell parent the parent shell of the dialog to be created
     * @param context the runnable context used to show progress when the dialog
     *            is being populated
     * @param javaProject the project that contains the TestSuites to be included
     * @param multipleSelect true if multiple selection is allowed
     * @return The dialog created
     * @throws JavaModelException if the selection dialog could not be opened
     */
    public static SelectionDialog createTestSuiteSelectionDialog(Shell shell,
            IRunnableContext context, IJavaProject javaProject,
            boolean multipleSelect) throws JavaModelException {
        return createTestSuiteSelectionDialog(
                shell,
                context,
                javaProject,
                multipleSelect,
                MTJUIMessages.TestSuiteSelectionDialogCreator_createTestSuiteSelectionDialog_message);
    }

    /**
     * Create a new Test Suite selection dialog.
     * 
     * @param shell parent the parent shell of the dialog to be created
     * @param context the runnable context used to show progress when the dialog
     *            is being populated
     * @param javaProject the project that contains the TestSuites to be included
     * @param multipleSelect true if multiple selection is allowed
     * @param message a custom message to show in the dialog
     * @return The dialog created
     * @throws JavaModelException if the selection dialog could not be opened
     */
    public static SelectionDialog createTestSuiteSelectionDialog(Shell shell,
            IRunnableContext context, IJavaProject javaProject,
            boolean multipleSelect, String message) throws JavaModelException {
        IJavaSearchScope searchScope = new TestSuiteSarchScope(javaProject);

        SelectionDialog dialog = JavaUI.createTypeDialog(shell, context,
                searchScope,
                IJavaElementSearchConstants.CONSIDER_CLASSES,
                multipleSelect, "**"); //$NON-NLS-1$

        dialog
                .setTitle(MTJUIMessages.TestSuiteSelectionDialogCreator_createTestSuiteSelectionDialog_title);
        dialog
                .setMessage(MTJUIMessages.TestSuiteSelectionDialogCreator_createTestSuiteSelectionDialog_message);

        return dialog;
    }

    // Private constructor for static access
    private TestSuiteSelectionDialogCreator() {
    }

}
