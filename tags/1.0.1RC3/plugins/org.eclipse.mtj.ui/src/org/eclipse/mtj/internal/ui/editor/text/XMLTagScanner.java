/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.text;

import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.util.PropertyChangeEvent;

/**
 * @since 0.9.1
 */
public class XMLTagScanner extends BaseMTJScanner {

    private Token fStringToken;

    /**
     * @param manager
     */
    public XMLTagScanner(IColorManager manager) {
        super(manager);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.text.BaseMTJScanner#affectsTextPresentation(java.lang.String)
     */
    @Override
    public boolean affectsTextPresentation(String property) {
        return property.startsWith(IMTJColorConstants.P_TAG)
                || property.startsWith(IMTJColorConstants.P_STRING);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.text.BaseMTJScanner#getTokenAffected(org.eclipse.jface.util.PropertyChangeEvent)
     */
    @Override
    protected Token getTokenAffected(PropertyChangeEvent event) {
        String property = event.getProperty();
        if (property.startsWith(IMTJColorConstants.P_STRING)) {
            return fStringToken;
        }
        return (Token) fDefaultReturnToken;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.text.BaseMTJScanner#initialize()
     */
    @Override
    protected void initialize() {
        fStringToken = new Token(
                createTextAttribute(IMTJColorConstants.P_STRING));
        IRule[] rules = new IRule[3];

        // Add rule for single and double quotes
        rules[0] = new MultiLineRule("\"", "\"", fStringToken); //$NON-NLS-1$ //$NON-NLS-2$
        rules[1] = new SingleLineRule("'", "'", fStringToken); //$NON-NLS-1$ //$NON-NLS-2$

        // Add generic whitespace rule.
        rules[2] = new WhitespaceRule(new XMLWhitespaceDetector());
        setRules(rules);

        setDefaultReturnToken(new Token(
                createTextAttribute(IMTJColorConstants.P_TAG)));
    }

}
