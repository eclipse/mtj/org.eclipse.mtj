/**
 * Copyright (c) 2003,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.AbstractFormPart;
import org.eclipse.ui.forms.IDetailsPage;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * @since 0.9.1
 */
public abstract class MTJDetails extends AbstractFormPart implements
        IDetailsPage, IContextPart {

    /**
     * Creates a new details form part.
     */
    public MTJDetails() {
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.IContextPart#cancelEdit()
     */
    public void cancelEdit() {
        super.refresh();
    }

    /**
     * Sub-classes to override.
     * 
     * @param selection
     * @return
     */
    public boolean canCopy(ISelection selection) {
        return false;
    }

    /**
     * Sub-classes to override.
     * 
     * @param selection
     * @return
     */
    public boolean canCut(ISelection selection) {
        return false;
    }

    /**
     * @param clipboard
     * @return
     */
    public boolean canPaste(Clipboard clipboard) {
        return true;
    }

    /**
     * @param actionId
     * @return
     */
    public boolean doGlobalAction(String actionId) {
        return false;
    }

    /**
     * @param toolkit
     * @param parent
     * @param span
     */
    protected void createSpacer(FormToolkit toolkit, Composite parent, int span) {
        Label spacer = toolkit.createLabel(parent, Utils.EMPTY_STRING);
        GridData gd = new GridData();
        gd.horizontalSpan = span;
        spacer.setLayoutData(gd);
    }

    /**
     * @param control
     */
    protected void markDetailsPart(Control control) {
        control.setData("part", this); //$NON-NLS-1$
    }
}
