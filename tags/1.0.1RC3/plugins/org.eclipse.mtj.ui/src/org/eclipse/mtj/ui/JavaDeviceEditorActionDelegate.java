/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui;

import org.eclipse.mtj.internal.ui.editors.device.JavaDeviceEditorDialog;
import org.eclipse.mtj.ui.editors.device.DeviceEditorDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * An action delegate to launch the Java Device editor dialog.
 * 
 * @since 1.0
 */
public class JavaDeviceEditorActionDelegate extends
        DefaultDeviceEditorActionDelegate {

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.DefaultDeviceEditorActionDelegate#getDeviceEditorDialog(org.eclipse.swt.widgets.Shell)
     */
    @Override
    protected DeviceEditorDialog getDeviceEditorDialog(Shell shell) {
        return new JavaDeviceEditorDialog(shell);
    }
}
