/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Renato Franca (Motorola) - Changing ownership of XML tags
 */ 
package org.eclipse.mtj.internal.ui.editors.l10n.actions;

import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.ui.MTJUIMessages;

/**
 * An add Locale Action.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class L10nAddLocaleAction extends L10nAddObjectAction {

    /**
     * Creates a new L10n add locale action.
     */
    public L10nAddLocaleAction() {
        setText(MTJUIMessages.L10nAddLocaleAction_text);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {

        if (parentObject != null) {

            // Create a new locale object
            L10nLocale locale = parentObject.getModel().getLocales().createlLocale();

            locale.setLanguageCode("en"); //$NON-NLS-1$
            locale.setCountryCode("US"); //$NON-NLS-1$

            // Add the new locale to the parent Locales object
            addChild(locale);
        }
    }
}
