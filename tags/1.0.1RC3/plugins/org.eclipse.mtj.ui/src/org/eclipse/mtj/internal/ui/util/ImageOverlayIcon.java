/**
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.util;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;

/**
 * An OverlayIcon consists of a main icon and several adornments.
 */
public class ImageOverlayIcon extends AbstractOverlayIcon {
    private Image base;

    /**
     * @param base
     * @param overlays
     */
    public ImageOverlayIcon(Image base, ImageDescriptor[][] overlays) {
        this(base, overlays, null);
    }

    /**
     * @param base
     * @param overlays
     * @param size
     */
    public ImageOverlayIcon(Image base, ImageDescriptor[][] overlays, Point size) {
        super(overlays, size);
        this.base = base;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.util.AbstractOverlayIcon#getBaseImageData()
     */
    @Override
    protected ImageData getBaseImageData() {
        return base.getImageData();
    }
}
