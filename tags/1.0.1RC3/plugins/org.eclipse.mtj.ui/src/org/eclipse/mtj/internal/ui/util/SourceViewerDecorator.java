/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.ui.util;

import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.IAutoIndentStrategy;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IEditingSupport;
import org.eclipse.jface.text.IEventConsumer;
import org.eclipse.jface.text.IFindReplaceTarget;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.IPainter;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.IRewriteTarget;
import org.eclipse.jface.text.ITextDoubleClickStrategy;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextInputListener;
import org.eclipse.jface.text.ITextListener;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.ITextPresentationListener;
import org.eclipse.jface.text.IUndoManager;
import org.eclipse.jface.text.IViewportListener;
import org.eclipse.jface.text.IWidgetTokenKeeper;
import org.eclipse.jface.text.TextPresentation;
import org.eclipse.jface.text.hyperlink.IHyperlinkDetector;
import org.eclipse.jface.text.hyperlink.IHyperlinkPresenter;
import org.eclipse.jface.text.quickassist.IQuickAssistAssistant;
import org.eclipse.jface.text.quickassist.IQuickAssistInvocationContext;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationHover;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.StyledTextPrintOptions;
import org.eclipse.swt.custom.VerifyKeyListener;
import org.eclipse.swt.events.HelpListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Item;

/**
 * SourceViewerDecorator class decorates a {@link SourceViewer}
 * instance in order to dynamically extend a {@link SourceViewer}
 * that does not allow compilation time inheritance.
 * 
 * @author David Marques
 */
public class SourceViewerDecorator extends SourceViewer {

    private SourceViewer decoratedViewer;

    /**
     * Creates a SourceViewerDecorator instance to decorate
     * the specified {@link SourceViewer} instance.
     * 
     * @param _decoratedViewer target source viewer.
     */
    public SourceViewerDecorator(SourceViewer _decoratedViewer) {
        super(null, null, 0x00);
        
        if (_decoratedViewer == null) {
            throw new IllegalArgumentException(MTJUIMessages.SourceViewerDecorator_invalidDecoratedViewer);
        }
        this.decoratedViewer = _decoratedViewer;
    }

    /**
     * Gets the decorated {@link SourceViewer} instance.
     * 
     * @return target source viewer.
     */
    public SourceViewer getDecoratedViewer() {
        return decoratedViewer;
    }
    
    /**
     * This method is not allowed to be overridden since
     * the decorated {@link SourceViewer} has already
     * created it's control.
     */
    protected final void createControl(Composite parent, int styles) {}
    
    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#activatePlugins()
     */
    public void activatePlugins() {

        this.decoratedViewer.activatePlugins();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#canDoOperation(int)
     */
    public boolean canDoOperation(int operation) {

        return this.decoratedViewer.canDoOperation(operation);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#configure(org.eclipse.jface
     * .text.source.SourceViewerConfiguration)
     */
    public void configure(SourceViewerConfiguration configuration) {

        this.decoratedViewer.configure(configuration);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#doOperation(int)
     */
    public void doOperation(int operation) {

        this.decoratedViewer.doOperation(operation);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#enableOperation(int,
     * boolean)
     */
    public void enableOperation(int operation, boolean enable) {

        this.decoratedViewer.enableOperation(operation, enable);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#getAnnotationModel()
     */
    public IAnnotationModel getAnnotationModel() {

        return this.decoratedViewer.getAnnotationModel();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#getControl()
     */
    public Control getControl() {

        return this.decoratedViewer.getControl();
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#getCurrentAnnotationHover()
     */
    public IAnnotationHover getCurrentAnnotationHover() {

        return this.decoratedViewer.getCurrentAnnotationHover();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#getQuickAssistAssistant()
     */
    public IQuickAssistAssistant getQuickAssistAssistant() {

        return this.decoratedViewer.getQuickAssistAssistant();
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#getQuickAssistInvocationContext
     * ()
     */
    public IQuickAssistInvocationContext getQuickAssistInvocationContext() {

        return this.decoratedViewer.getQuickAssistInvocationContext();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#getRangeIndication()
     */
    public IRegion getRangeIndication() {

        return this.decoratedViewer.getRangeIndication();
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#getVisualAnnotationModel()
     */
    public IAnnotationModel getVisualAnnotationModel() {

        return this.decoratedViewer.getVisualAnnotationModel();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#removeRangeIndication()
     */
    public void removeRangeIndication() {

        this.decoratedViewer.removeRangeIndication();
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#setAnnotationHover(org.eclipse
     * .jface.text.source.IAnnotationHover)
     */
    public void setAnnotationHover(IAnnotationHover annotationHover) {

        this.decoratedViewer.setAnnotationHover(annotationHover);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#setDocument(org.eclipse.jface
     * .text.IDocument, org.eclipse.jface.text.source.IAnnotationModel, int,
     * int)
     */
    public void setDocument(IDocument document,
            IAnnotationModel annotationModel, int modelRangeOffset,
            int modelRangeLength) {

        super.setDocument(document, annotationModel, modelRangeOffset,
                modelRangeLength);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#setDocument(org.eclipse.jface
     * .text.IDocument, org.eclipse.jface.text.source.IAnnotationModel)
     */
    public void setDocument(IDocument document, IAnnotationModel annotationModel) {

        this.decoratedViewer.setDocument(document, annotationModel);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#setDocument(org.eclipse.jface
     * .text.IDocument, int, int)
     */
    public void setDocument(IDocument document, int visibleRegionOffset,
            int visibleRegionLength) {

        this.decoratedViewer.setDocument(document, visibleRegionOffset,
                visibleRegionLength);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#setDocument(org.eclipse.jface
     * .text.IDocument)
     */
    public void setDocument(IDocument document) {

        this.decoratedViewer.setDocument(document);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#setHoverEnrichMode(org.eclipse
     * .jface.text.ITextViewerExtension8.EnrichMode)
     */
    public void setHoverEnrichMode(EnrichMode mode) {

        this.decoratedViewer.setHoverEnrichMode(mode);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#setOverviewRulerAnnotationHover
     * (org.eclipse.jface.text.source.IAnnotationHover)
     */
    public void setOverviewRulerAnnotationHover(IAnnotationHover annotationHover) {

        this.decoratedViewer.setOverviewRulerAnnotationHover(annotationHover);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#setRangeIndication(int,
     * int, boolean)
     */
    public void setRangeIndication(int start, int length, boolean moveCursor) {

        this.decoratedViewer.setRangeIndication(start, length, moveCursor);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#setRangeIndicator(org.eclipse
     * .jface.text.source.Annotation)
     */
    public void setRangeIndicator(Annotation rangeIndicator) {

        this.decoratedViewer.setRangeIndicator(rangeIndicator);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#showAnnotations(boolean)
     */
    public void showAnnotations(boolean show) {

        this.decoratedViewer.showAnnotations(show);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.source.SourceViewer#showAnnotationsOverview(boolean
     * )
     */
    public void showAnnotationsOverview(boolean show) {

        this.decoratedViewer.showAnnotationsOverview(show);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewer#unconfigure()
     */
    public void unconfigure() {

        this.decoratedViewer.unconfigure();
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#addPainter(org.eclipse.jface.text.IPainter
     * )
     */
    public void addPainter(IPainter painter) {

        this.decoratedViewer.addPainter(painter);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#addPostSelectionChangedListener(org
     * .eclipse.jface.viewers.ISelectionChangedListener)
     */
    public void addPostSelectionChangedListener(
            ISelectionChangedListener listener) {

        this.decoratedViewer.addPostSelectionChangedListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#addTextInputListener(org.eclipse.jface
     * .text.ITextInputListener)
     */
    public void addTextInputListener(ITextInputListener listener) {

        this.decoratedViewer.addTextInputListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#addTextListener(org.eclipse.jface.text
     * .ITextListener)
     */
    public void addTextListener(ITextListener listener) {

        this.decoratedViewer.addTextListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#addTextPresentationListener(org.eclipse
     * .jface.text.ITextPresentationListener)
     */
    public void addTextPresentationListener(ITextPresentationListener listener) {

        this.decoratedViewer.addTextPresentationListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#addViewportListener(org.eclipse.jface
     * .text.IViewportListener)
     */
    public void addViewportListener(IViewportListener listener) {

        this.decoratedViewer.addViewportListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#appendVerifyKeyListener(org.eclipse
     * .swt.custom.VerifyKeyListener)
     */
    public void appendVerifyKeyListener(VerifyKeyListener listener) {

        this.decoratedViewer.appendVerifyKeyListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#changeTextPresentation(org.eclipse.
     * jface.text.TextPresentation, boolean)
     */
    public void changeTextPresentation(TextPresentation presentation,
            boolean controlRedraw) {

        this.decoratedViewer.changeTextPresentation(presentation, controlRedraw);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getBottomIndex()
     */
    public int getBottomIndex() {

        return this.decoratedViewer.getBottomIndex();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getBottomIndexEndOffset()
     */
    public int getBottomIndexEndOffset() {

        return this.decoratedViewer.getBottomIndexEndOffset();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getCurrentTextHover()
     */
    public ITextHover getCurrentTextHover() {

        return this.decoratedViewer.getCurrentTextHover();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getDocument()
     */
    public IDocument getDocument() {

        return this.decoratedViewer.getDocument();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getFindReplaceTarget()
     */
    public IFindReplaceTarget getFindReplaceTarget() {

        return this.decoratedViewer.getFindReplaceTarget();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getHoverEventLocation()
     */
    public Point getHoverEventLocation() {

        return this.decoratedViewer.getHoverEventLocation();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getInput()
     */
    public Object getInput() {

        return this.decoratedViewer.getInput();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getMark()
     */
    public int getMark() {

        return this.decoratedViewer.getMark();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getModelCoverage()
     */
    public IRegion getModelCoverage() {

        return this.decoratedViewer.getModelCoverage();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getRegisteredSupports()
     */
    public IEditingSupport[] getRegisteredSupports() {

        return this.decoratedViewer.getRegisteredSupports();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getRewriteTarget()
     */
    public IRewriteTarget getRewriteTarget() {

        return this.decoratedViewer.getRewriteTarget();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getSelectedRange()
     */
    public Point getSelectedRange() {

        return this.decoratedViewer.getSelectedRange();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getSelection()
     */
    public ISelection getSelection() {

        return this.decoratedViewer.getSelection();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getSelectionProvider()
     */
    public ISelectionProvider getSelectionProvider() {

        return this.decoratedViewer.getSelectionProvider();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getTextOperationTarget()
     */
    public ITextOperationTarget getTextOperationTarget() {

        return this.decoratedViewer.getTextOperationTarget();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getTextWidget()
     */
    public StyledText getTextWidget() {

        return this.decoratedViewer.getTextWidget();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getTopIndex()
     */
    public int getTopIndex() {

        return this.decoratedViewer.getTopIndex();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getTopIndexStartOffset()
     */
    public int getTopIndexStartOffset() {

        return this.decoratedViewer.getTopIndexStartOffset();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getTopInset()
     */
    public int getTopInset() {

        return this.decoratedViewer.getTopInset();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getUndoManager()
     */
    public IUndoManager getUndoManager() {

        return this.decoratedViewer.getUndoManager();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#getVisibleRegion()
     */
    public IRegion getVisibleRegion() {

        return this.decoratedViewer.getVisibleRegion();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#isEditable()
     */
    public boolean isEditable() {

        return this.decoratedViewer.isEditable();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#modelLine2WidgetLine(int)
     */
    public int modelLine2WidgetLine(int modelLine) {

        return this.decoratedViewer.modelLine2WidgetLine(modelLine);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#modelOffset2WidgetOffset(int)
     */
    public int modelOffset2WidgetOffset(int modelOffset) {

        return this.decoratedViewer.modelOffset2WidgetOffset(modelOffset);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#modelRange2WidgetRange(org.eclipse.
     * jface.text.IRegion)
     */
    public IRegion modelRange2WidgetRange(IRegion modelRange) {

        return this.decoratedViewer.modelRange2WidgetRange(modelRange);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#moveFocusToWidgetToken()
     */
    public boolean moveFocusToWidgetToken() {

        return this.decoratedViewer.moveFocusToWidgetToken();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#overlapsWithVisibleRegion(int,
     * int)
     */
    public boolean overlapsWithVisibleRegion(int start, int length) {

        return this.decoratedViewer.overlapsWithVisibleRegion(start, length);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#prependAutoEditStrategy(org.eclipse
     * .jface.text.IAutoEditStrategy, java.lang.String)
     */
    public void prependAutoEditStrategy(IAutoEditStrategy strategy,
            String contentType) {

        this.decoratedViewer.prependAutoEditStrategy(strategy, contentType);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#prependVerifyKeyListener(org.eclipse
     * .swt.custom.VerifyKeyListener)
     */
    public void prependVerifyKeyListener(VerifyKeyListener listener) {

        this.decoratedViewer.prependVerifyKeyListener(listener);
    }

    /*
     * (non-Javadoc)
     * @seeorg.eclipse.jface.text.TextViewer#print(org.eclipse.swt.custom.
     * StyledTextPrintOptions)
     */
    public void print(StyledTextPrintOptions options) {

        this.decoratedViewer.print(options);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#refresh()
     */
    public void refresh() {

        this.decoratedViewer.refresh();
    }

    /*
     * (non-Javadoc)
     * @seeorg.eclipse.jface.text.TextViewer#register(org.eclipse.jface.text.
     * IEditingSupport)
     */
    public void register(IEditingSupport helper) {

        this.decoratedViewer.register(helper);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#releaseWidgetToken(org.eclipse.jface
     * .text.IWidgetTokenKeeper)
     */
    public void releaseWidgetToken(IWidgetTokenKeeper tokenKeeper) {

        this.decoratedViewer.releaseWidgetToken(tokenKeeper);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#removeAutoEditStrategy(org.eclipse.
     * jface.text.IAutoEditStrategy, java.lang.String)
     */
    public void removeAutoEditStrategy(IAutoEditStrategy strategy,
            String contentType) {

        this.decoratedViewer.removeAutoEditStrategy(strategy, contentType);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#removePainter(org.eclipse.jface.text
     * .IPainter)
     */
    public void removePainter(IPainter painter) {

        this.decoratedViewer.removePainter(painter);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#removePostSelectionChangedListener(
     * org.eclipse.jface.viewers.ISelectionChangedListener)
     */
    public void removePostSelectionChangedListener(
            ISelectionChangedListener listener) {

        this.decoratedViewer.removePostSelectionChangedListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#removeTextHovers(java.lang.String)
     */
    public void removeTextHovers(String contentType) {

        this.decoratedViewer.removeTextHovers(contentType);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#removeTextInputListener(org.eclipse
     * .jface.text.ITextInputListener)
     */
    public void removeTextInputListener(ITextInputListener listener) {

        this.decoratedViewer.removeTextInputListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#removeTextListener(org.eclipse.jface
     * .text.ITextListener)
     */
    public void removeTextListener(ITextListener listener) {

        this.decoratedViewer.removeTextListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#removeTextPresentationListener(org.
     * eclipse.jface.text.ITextPresentationListener)
     */
    public void removeTextPresentationListener(
            ITextPresentationListener listener) {

        this.decoratedViewer.removeTextPresentationListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#removeVerifyKeyListener(org.eclipse
     * .swt.custom.VerifyKeyListener)
     */
    public void removeVerifyKeyListener(VerifyKeyListener listener) {

        this.decoratedViewer.removeVerifyKeyListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#removeViewportListener(org.eclipse.
     * jface.text.IViewportListener)
     */
    public void removeViewportListener(IViewportListener listener) {

        this.decoratedViewer.removeViewportListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#requestWidgetToken(org.eclipse.jface
     * .text.IWidgetTokenKeeper, int)
     */
    public boolean requestWidgetToken(IWidgetTokenKeeper requester, int priority) {

        return this.decoratedViewer.requestWidgetToken(requester, priority);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#requestWidgetToken(org.eclipse.jface
     * .text.IWidgetTokenKeeper)
     */
    public boolean requestWidgetToken(IWidgetTokenKeeper requester) {

        return this.decoratedViewer.requestWidgetToken(requester);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#resetPlugins()
     */
    public void resetPlugins() {

        this.decoratedViewer.resetPlugins();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#resetVisibleRegion()
     */
    public void resetVisibleRegion() {

        this.decoratedViewer.resetVisibleRegion();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#revealRange(int, int)
     */
    public void revealRange(int start, int length) {

        this.decoratedViewer.revealRange(start, length);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setAutoIndentStrategy(org.eclipse.jface
     * .text.IAutoIndentStrategy, java.lang.String)
     */
    public void setAutoIndentStrategy(IAutoIndentStrategy strategy,
            String contentType) {

        this.decoratedViewer.setAutoIndentStrategy(strategy, contentType);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setDefaultPrefixes(java.lang.String[],
     * java.lang.String)
     */
    public void setDefaultPrefixes(String[] defaultPrefixes, String contentType) {

        this.decoratedViewer.setDefaultPrefixes(defaultPrefixes, contentType);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setDocumentPartitioning(java.lang.String
     * )
     */
    public void setDocumentPartitioning(String partitioning) {

        this.decoratedViewer.setDocumentPartitioning(partitioning);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#setEditable(boolean)
     */
    public void setEditable(boolean editable) {

        this.decoratedViewer.setEditable(editable);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setEventConsumer(org.eclipse.jface.
     * text.IEventConsumer)
     */
    public void setEventConsumer(IEventConsumer consumer) {

        this.decoratedViewer.setEventConsumer(consumer);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setHoverControlCreator(org.eclipse.
     * jface.text.IInformationControlCreator)
     */
    public void setHoverControlCreator(IInformationControlCreator creator) {

        this.decoratedViewer.setHoverControlCreator(creator);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setHyperlinkDetectors(org.eclipse.jface
     * .text.hyperlink.IHyperlinkDetector[], int)
     */
    public void setHyperlinkDetectors(IHyperlinkDetector[] hyperlinkDetectors,
            int eventStateMask) {

        this.decoratedViewer
                .setHyperlinkDetectors(hyperlinkDetectors, eventStateMask);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setHyperlinkPresenter(org.eclipse.jface
     * .text.hyperlink.IHyperlinkPresenter)
     */
    public void setHyperlinkPresenter(IHyperlinkPresenter hyperlinkPresenter)
            throws IllegalStateException {

        this.decoratedViewer.setHyperlinkPresenter(hyperlinkPresenter);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setIndentPrefixes(java.lang.String[],
     * java.lang.String)
     */
    public void setIndentPrefixes(String[] indentPrefixes, String contentType) {

        this.decoratedViewer.setIndentPrefixes(indentPrefixes, contentType);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#setInput(java.lang.Object)
     */
    public void setInput(Object input) {

        this.decoratedViewer.setInput(input);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#setMark(int)
     */
    public void setMark(int offset) {

        this.decoratedViewer.setMark(offset);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#setSelectedRange(int, int)
     */
    public void setSelectedRange(int selectionOffset, int selectionLength) {

        this.decoratedViewer.setSelectedRange(selectionOffset, selectionLength);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setSelection(org.eclipse.jface.viewers
     * .ISelection, boolean)
     */
    public void setSelection(ISelection selection, boolean reveal) {

        this.decoratedViewer.setSelection(selection, reveal);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setTabsToSpacesConverter(org.eclipse
     * .jface.text.IAutoEditStrategy)
     */
    public void setTabsToSpacesConverter(IAutoEditStrategy converter) {

        this.decoratedViewer.setTabsToSpacesConverter(converter);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setTextColor(org.eclipse.swt.graphics
     * .Color, int, int, boolean)
     */
    public void setTextColor(Color color, int start, int length,
            boolean controlRedraw) {

        this.decoratedViewer.setTextColor(color, start, length, controlRedraw);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setTextColor(org.eclipse.swt.graphics
     * .Color)
     */
    public void setTextColor(Color color) {

        this.decoratedViewer.setTextColor(color);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setTextDoubleClickStrategy(org.eclipse
     * .jface.text.ITextDoubleClickStrategy, java.lang.String)
     */
    public void setTextDoubleClickStrategy(ITextDoubleClickStrategy strategy,
            String contentType) {

        this.decoratedViewer.setTextDoubleClickStrategy(strategy, contentType);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setTextHover(org.eclipse.jface.text
     * .ITextHover, java.lang.String, int)
     */
    public void setTextHover(ITextHover hover, String contentType, int stateMask) {

        this.decoratedViewer.setTextHover(hover, contentType, stateMask);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setTextHover(org.eclipse.jface.text
     * .ITextHover, java.lang.String)
     */
    public void setTextHover(ITextHover hover, String contentType) {

        this.decoratedViewer.setTextHover(hover, contentType);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#setTopIndex(int)
     */
    public void setTopIndex(int index) {

        this.decoratedViewer.setTopIndex(index);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#setUndoManager(org.eclipse.jface.text
     * .IUndoManager)
     */
    public void setUndoManager(IUndoManager undoManager) {

        this.decoratedViewer.setUndoManager(undoManager);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#setVisibleRegion(int, int)
     */
    public void setVisibleRegion(int start, int length) {

        this.decoratedViewer.setVisibleRegion(start, length);
    }

    /*
     * (non-Javadoc)
     * @seeorg.eclipse.jface.text.TextViewer#unregister(org.eclipse.jface.text.
     * IEditingSupport)
     */
    public void unregister(IEditingSupport helper) {

        this.decoratedViewer.unregister(helper);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#widgetLine2ModelLine(int)
     */
    public int widgetLine2ModelLine(int widgetLine) {

        return this.decoratedViewer.widgetLine2ModelLine(widgetLine);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#widgetLineOfWidgetOffset(int)
     */
    public int widgetLineOfWidgetOffset(int widgetOffset) {

        return this.decoratedViewer.widgetLineOfWidgetOffset(widgetOffset);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#widgetlLine2ModelLine(int)
     */
    public int widgetlLine2ModelLine(int widgetLine) {

        return this.decoratedViewer.widgetlLine2ModelLine(widgetLine);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.text.TextViewer#widgetOffset2ModelOffset(int)
     */
    public int widgetOffset2ModelOffset(int widgetOffset) {

        return this.decoratedViewer.widgetOffset2ModelOffset(widgetOffset);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.text.TextViewer#widgetRange2ModelRange(org.eclipse.
     * jface.text.IRegion)
     */
    public IRegion widgetRange2ModelRange(IRegion widgetRange) {

        return this.decoratedViewer.widgetRange2ModelRange(widgetRange);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.viewers.Viewer#addHelpListener(org.eclipse.swt.events
     * .HelpListener)
     */
    public void addHelpListener(HelpListener listener) {

        this.decoratedViewer.addHelpListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.viewers.Viewer#addSelectionChangedListener(org.eclipse
     * .jface.viewers.ISelectionChangedListener)
     */
    public void addSelectionChangedListener(ISelectionChangedListener listener) {

        this.decoratedViewer.addSelectionChangedListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.viewers.Viewer#getData(java.lang.String)
     */
    public Object getData(String key) {
        
        return this.decoratedViewer.getData(key);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.viewers.Viewer#removeHelpListener(org.eclipse.swt.events
     * .HelpListener)
     */
    public void removeHelpListener(HelpListener listener) {
        
        this.decoratedViewer.removeHelpListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.viewers.Viewer#removeSelectionChangedListener(org.eclipse
     * .jface.viewers.ISelectionChangedListener)
     */
    public void removeSelectionChangedListener(
            ISelectionChangedListener listener) {
        
        this.decoratedViewer.removeSelectionChangedListener(listener);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.viewers.Viewer#scrollDown(int, int)
     */
    public Item scrollDown(int x, int y) {
        
        return this.decoratedViewer.scrollDown(x, y);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.viewers.Viewer#scrollUp(int, int)
     */
    public Item scrollUp(int x, int y) {
        return this.decoratedViewer.scrollUp(x, y);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.viewers.Viewer#setData(java.lang.String,
     * java.lang.Object)
     */
    public void setData(String key, Object value) {
        
        this.decoratedViewer.setData(key, value);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.viewers.Viewer#setSelection(org.eclipse.jface.viewers
     * .ISelection)
     */
    public void setSelection(ISelection selection) {
        
        this.decoratedViewer.setSelection(selection);
    }

}
