/**
 * Copyright (c) 2005, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDEOutlinePage
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.ui.views.contentoutline.ContentOutlinePage;

public class MTJOutlinePage extends ContentOutlinePage {

    /**
     * 
     */
    protected MTJFormEditor fEditor;

    /**
     * 
     */
    public MTJOutlinePage() {
    }

    /**
     * @param editor
     */
    public MTJOutlinePage(MTJFormEditor editor) {
        fEditor = editor;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.Page#makeContributions(org.eclipse.jface.action.IMenuManager, org.eclipse.jface.action.IToolBarManager, org.eclipse.jface.action.IStatusLineManager)
     */
    @Override
    public void makeContributions(IMenuManager menuManager,
            IToolBarManager toolBarManager, IStatusLineManager statusLineManager) {
    }

}
