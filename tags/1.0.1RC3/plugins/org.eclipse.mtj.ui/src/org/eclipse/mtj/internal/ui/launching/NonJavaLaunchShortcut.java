/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques, David Arag�o (Motorola) - Initial Version.
 */

package org.eclipse.mtj.internal.ui.launching;

import java.util.List;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.IDebugModelPresentation;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

public abstract class NonJavaLaunchShortcut implements ILaunchShortcut {

    /**
     * @param selection
     * @param mode
     */
    public void launch(ISelection selection, String mode) {
        List<ILaunchConfiguration> candidates = null;

        candidates = getLaunchConfigurations(selection);
        doLaunch(candidates, mode);
    }

    /**
     * @param editor
     * @param mode
     */
    public void launch(IEditorPart editor, String mode) {
        List<ILaunchConfiguration> candidates = null;

        candidates = getLaunchConfigurations(editor);
        doLaunch(candidates, mode);
    }

    private void doLaunch(List<ILaunchConfiguration> candidates, String mode) {
        ILaunchConfiguration target = null;
        if (candidates == null) {
            return;
        }

        // If there are no existing configs associated with the IType, create
        // one. If there is exactly one config associated with the IType, return
        // it. Otherwise, if there is more than one config associated with the
        // IType, prompt the user to choose one.
        int candidateCount = candidates.size();
        if (candidateCount < 1) {
            target = createConfiguration();
        } else if (candidateCount == 1) {
            target = candidates.get(0);
        } else {
            // Prompt the user to choose a config. A null result means the user
            // canceled the dialog, in which case this method returns null,
            // since canceling the dialog should also cancel launching
            // anything.
            ILaunchConfiguration config = chooseConfiguration(candidates);
            if (config != null) {
                target = config;
            }
        }
        // launch
        DebugUITools.launch(target, mode);
    }

    protected abstract List<ILaunchConfiguration> getLaunchConfigurations(
            ISelection selection);

    protected abstract List<ILaunchConfiguration> getLaunchConfigurations(
            IEditorPart editor);

    protected abstract ILaunchConfiguration createConfiguration();

    /**
     * Returns a configuration from the given collection of configurations that
     * should be launched, or <code>null</code> to cancel. Default
     * implementation opens a selection dialog that allows the user to choose
     * one of the specified launch configurations. Returns the chosen
     * configuration, or <code>null</code> if the user cancels.
     * 
     * @param configList list of configurations to choose from
     * @return configuration to launch or <code>null</code> to cancel
     */
    private ILaunchConfiguration chooseConfiguration(
            List<ILaunchConfiguration> configList) {
        IDebugModelPresentation labelProvider = DebugUITools
                .newDebugModelPresentation();
        ElementListSelectionDialog dialog = new ElementListSelectionDialog(
                getShell(), labelProvider);
        dialog.setElements(configList.toArray());
        dialog.setTitle(MTJUIMessages.launch_configSelection_title);
        dialog.setMessage(MTJUIMessages.launch_configSelection_message);
        dialog.setMultipleSelection(false);
        int result = dialog.open();
        labelProvider.dispose();
        if (result == Window.OK) {
            return (ILaunchConfiguration) dialog.getFirstResult();
        }
        return null;
    }

    /**
     * Get the active workbench window's shell.
     * 
     * @return
     */
    private Shell getShell() {
        Shell shell = null;

        IWorkbenchWindow workbenchWindow = MTJUIPlugin.getDefault()
                .getWorkbench().getActiveWorkbenchWindow();
        if (workbenchWindow != null) {
            shell = workbenchWindow.getShell();
        }

        return shell;
    }

}
