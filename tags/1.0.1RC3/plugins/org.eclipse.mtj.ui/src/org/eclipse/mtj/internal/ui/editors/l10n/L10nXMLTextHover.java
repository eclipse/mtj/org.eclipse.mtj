/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.mtj.internal.core.text.IDocumentRange;
import org.eclipse.mtj.internal.core.text.IDocumentTextNode;
import org.eclipse.mtj.internal.ui.editor.MTJSourcePage;
import org.eclipse.mtj.internal.ui.editor.text.MTJTextHover;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class L10nXMLTextHover extends MTJTextHover {

    private MTJSourcePage fSourcePage;

    /**
     * Creates a new L10nXMLTextHover.
     * 
     * @param sourcePage
     */
    public L10nXMLTextHover(MTJSourcePage sourcePage) {
        fSourcePage = sourcePage;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.ITextHover#getHoverInfo(org.eclipse.jface.text.ITextViewer, org.eclipse.jface.text.IRegion)
     */
    public String getHoverInfo(ITextViewer textViewer, IRegion hoverRegion) {
        int offset = hoverRegion.getOffset();
        IDocumentRange range = fSourcePage.getRangeElement(offset, true);

        if (range instanceof IDocumentTextNode) {
            return ((IDocumentTextNode) range).getText();
        }

        return null;
    }

}
