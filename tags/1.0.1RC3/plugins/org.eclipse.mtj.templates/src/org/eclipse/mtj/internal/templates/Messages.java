package org.eclipse.mtj.internal.templates;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
    private static final String BUNDLE_NAME = "org.eclipse.mtj.internal.templates.messages"; //$NON-NLS-1$
    public static String DisplayTemplateProvider_0;
    public static String GameTemplateProvider_0;
    public static String GameTemplateProvider_1;
    public static String GameTemplateProvider_11;
    public static String GameTemplateProvider_12;
    public static String GameTemplateProvider_14;
    public static String GameTemplateProvider_15;
    public static String GameTemplateProvider_17;
    public static String GameTemplateProvider_18;
    public static String GameTemplateProvider_19;
    public static String GameTemplateProvider_3;
    public static String GameTemplateProvider_5;
    public static String GameTemplateProvider_7;
    public static String GameTemplateProvider_9;
    public static String ImageDownloadTemplateProvider_0;
    public static String ImageDownloadTemplateProvider_1;
    public static String SMSTemplateProvider_grouptext;
    public static String SMSTemplateProvider_server_port;
    public static String SplashTemplateProvider_0;
    public static String SplashTemplateProvider_1;
    public static String SplashTemplateProvider_10;
    public static String SplashTemplateProvider_2;
    public static String SplashTemplateProvider_3;
    public static String SplashTemplateProvider_7;
    public static String SplashTemplateProvider_9;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
