<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.eclipse.mtj.core" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appInfo>
         <meta.schema plugin="org.eclipse.mtj.core" id="deviceimporter" name="Device Importer Registering"/>
      </appInfo>
      <documentation>
         The device importer extension point provides a means for registering new types of device importers.

Devices are fully-formed at import time by the appropriate instance of IDeviceImporter. These device instances are then stored and loaded automatically by the system as necessary.  Implementors of this extension point must provide instances of the &lt;code&gt;&lt;b&gt;org.eclipse.mtj.core.sdk.device.IDeviceImporter&lt;/b&gt;&lt;/code&gt; interface.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appInfo>
            <meta.element />
         </appInfo>
      </annotation>
      <complexType>
         <sequence>
            <element ref="importer" minOccurs="1" maxOccurs="unbounded"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  a fully qualified identifier of the target extension point
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  an optional identifier of the extension instance
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  an optional name of the extension instance
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="importer">
      <complexType>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  a required fully-qualified identifier for this particular device importer extension
               </documentation>
            </annotation>
         </attribute>
         <attribute name="priority" type="string" use="required">
            <annotation>
               <documentation>
                  the priority order for this importer.  The priority may range from 1 to 99.  The importers will be consulted in priority order starting with the lower numbered priority and working toward the highest numbered priority.  The first device importer that requires a non-null result for a particular folder will &quot;win&quot; and no further importer instances will be consulted.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  an optional displayable name for this particular device importer extension
               </documentation>
            </annotation>
         </attribute>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  the required implementation class for the &lt;code&gt;org.eclipse.mtj.core.sdk.device.IDeviceImporter&lt;/code&gt; interface
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.mtj.core.sdk.device.IDeviceImporter"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         1.0
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         Example of a declaration of a &lt;code&gt;deviceimporter&lt;/code&gt; named &quot;ME4SE Device Importer&quot;:
&lt;pre&gt;
&lt;extension
     point=&quot;org.eclipse.mtj.core.deviceimporter&quot;&gt;
  &lt;importer
        class=&quot;org.eclipse.mtj.examples.toolkits.me4se.ME4SEDeviceImporter&quot;
        id=&quot;org.eclipse.mtj.toolkit.me4se.importer&quot;
        name=&quot;ME4SE Device Importer&quot;
        priority=&quot;50&quot;/&gt;
&lt;/extension&gt;
&lt;/pre&gt;
      </documentation>
   </annotation>


   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         MTJ provides 3 implementations for the &lt;code&gt;deviceimporter&lt;/code&gt; E.P., available in the following plug-ins:
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;&lt;code&gt;org.eclipse.mtj.toolkit.uei&lt;/code&gt;&lt;/b&gt;&lt;/li&gt; responsible for importing all UEI compatible SDK&apos;s
&lt;li&gt;&lt;b&gt;&lt;code&gt;org.eclipse.mtj.toolkit.mpowerplayer&lt;/code&gt;&lt;/b&gt;&lt;/li&gt; responsible for importing the MPowerPlayer SDK
&lt;li&gt;&lt;b&gt;&lt;code&gt;org.eclipse.mtj.toolkit.microemu&lt;/code&gt;&lt;/b&gt;&lt;/li&gt; responsible for importing the MicroEmu SDK
&lt;/ul&gt;
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="copyright"/>
      </appInfo>
      <documentation>
         Copyright (c) 2003,2009 Craig Setera and others.&lt;br&gt;
All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at &lt;a 
href=&quot;http://www.eclipse.org/legal/epl-v10.html&quot;&gt;http://www.eclipse.org/legal/epl-v10.html&lt;/a&gt;
      </documentation>
   </annotation>

</schema>
