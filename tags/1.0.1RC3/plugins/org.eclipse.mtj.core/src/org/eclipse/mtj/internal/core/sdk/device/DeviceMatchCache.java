/**
 * Copyright (c) 2009 Sony Ericsson.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Daniel Olsson (Sony Ericsson) - Initial contribution
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.core.persistence.XMLPersistenceProvider;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/**
 * This singleton class maintains and persists the cache for the
 * {@link CachedDeviceMatcher}.
 * 
 * @author Daniel Olsson
 */
public class DeviceMatchCache implements IPersistable, IDeviceMatchCache {

    private static final String ELEMENT_DEVICECACHE = "devicematchcache"; //$NON-NLS-1$

    private static final String DEVICECACHE_FILENAME = "devicematchcache.xml"; //$NON-NLS-1$

    private static Hashtable<String, String[]> deviceCache = null;

    private static IDeviceMatchCache instance = null;

    /**
     * @return the singelton instance
     */
    public static IDeviceMatchCache getInstance() {
        if (instance == null) {
            instance = new DeviceMatchCache();
        }
        return instance;
    }

    /**
     * Get the file for the device store file.
     * 
     * @return
     */
    private File getComponentStoreFile() {
        IPath pluginStatePath = MTJCore.getMTJCore().getStateLocation();
        IPath storePath = pluginStatePath.append(DEVICECACHE_FILENAME);
        return storePath.toFile();
    }

    private void loadCache() throws PersistenceException {
        File storeFile = getComponentStoreFile();
        if (storeFile.exists()) {
            try {
                Document document = XMLUtils.readDocument(storeFile);

                if (document == null) {
                    return;
                }

                Element rootXmlElement = document.getDocumentElement();
                if (!rootXmlElement.getNodeName().equals(ELEMENT_DEVICECACHE)) {
                    return;
                }

                XMLPersistenceProvider pprovider = new XMLPersistenceProvider(
                        document);

                loadUsing(pprovider);
            } catch (ParserConfigurationException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (SAXException e) {
                throw new PersistenceException(e.getMessage(), e);
            } catch (IOException e) {
                throw new PersistenceException(e.getMessage(), e);
            }
        } else {
            deviceCache = new Hashtable<String, String[]>();
        }
    }

    private void saveCache() throws PersistenceException {
        XMLPersistenceProvider pprovider = new XMLPersistenceProvider(
                ELEMENT_DEVICECACHE); //$NON-NLS-1$
        storeUsing(pprovider);
        try {
            XMLUtils.writeDocument(getComponentStoreFile(), pprovider
                    .getDocument());
        } catch (TransformerException e) {
            throw new PersistenceException(e.getMessage(), e);
        } catch (IOException e) {
            throw new PersistenceException(e.getMessage(), e);
        }
    }

    /**
     * Get a SDK, device pair from cache given the SDK, device key.
     * 
     * @param key SDK and device as a single string, constructed as
     *            "&ltSDK&gt/&ltdevice&gt"
     * @return String array with matching SDK and device, as { "&ltSDK&gt",
     *         "&ltdevice&gt" }, <b>null</b> if not found in cache
     * @see {@link #createKey(String, String)}
     */
    public String[] getValueFromCache(String key) {
        if (deviceCache == null) {
            try {
                loadCache();
            } catch (PersistenceException e) {
                e.printStackTrace();
                return null;
            }
        }
        if (deviceCache.containsKey(key)) {
            return deviceCache.get(key);
        } else {
            return null;
        }
    }

    /**
     * Creates a key for the cache given the SDK and device
     * 
     * @param deviceGroup SDK as a string
     * @param deviceName Device as a string
     * @return String to be used as a key for the cache
     */
    public String createKey(String deviceGroup, String deviceName) {
        return new String(deviceGroup + "/" + deviceName); //$NON-NLS-1$
    }

    /**
     * Saves the value with the specified key in the cache and then flushes it
     * to disk
     */
    public void saveInCache(String key, String[] value) {
        try {
            if (deviceCache == null) {
                loadCache();
            }
            deviceCache.put(key, value);
            saveCache();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a value for the cache from an {@link IDevice}
     * 
     * @param d The IDevice to create a String[] value for
     * @return A String[] as { "&ltSDK&gt", "&ltdevice&gt" }
     */
    public String[] createValue(IDevice d) {
        return new String[] { d.getSDKName(), d.getName() };
    }

    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        int count = persistenceProvider.loadInteger("entries"); //$NON-NLS-1$
        deviceCache = new Hashtable<String, String[]>(2 * count);
        for (int i = 0; i < count; i++) {
            String key = persistenceProvider.loadString("key" + i); //$NON-NLS-1$
            String value = persistenceProvider.loadString("value" + i); //$NON-NLS-1$
            deviceCache.put(key, value.split("/")); //$NON-NLS-1$
        }
    }

    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        persistenceProvider.storeInteger("entries", deviceCache.size()); //$NON-NLS-1$
        int i = 0;
        for (Enumeration<String> e = deviceCache.keys(); e.hasMoreElements(); i++) {
            final String key = e.nextElement();
            final String[] v = deviceCache.get(key);
            final String value = v[0] + "/" + v[1]; //$NON-NLS-1$
            persistenceProvider.storeString("key" + i, key); //$NON-NLS-1$
            persistenceProvider.storeString("value" + i, value); //$NON-NLS-1$
        }
    }
}
