/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Removing Preprocessor code
 *     Diego Sandin (Motorola)  - Fix getProfileVersion method
 *     Feng Wang (Sybase)       - Add removeMidletSuiteProject
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code 
 *     Diego Sandin (Motorola)  - Changed the way to set the project compliance 
 *                                options
 *     David Marques (Motorola) - Adding getMidletSuiteProject method.
 *     David Marques (Motorola) - Adding device to configuration and updating
 *                                classpath entry.
 *     David Marques (Motorola) - Fixing project creation configuration/profile.
 *     Fernando Rocha(Motorola) - Update all Projects when a device is modified.
 *     David Aragao (Motorola) -  Problem when try to import a project without 
 *     Fernando Rocha(Motorola) - Add the project properties							  copy the files to the workspace. [Bug - 270157]
 */
package org.eclipse.mtj.internal.core.project.midp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Preferences;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceRegistryListener;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.l10n.L10nApi;
import org.eclipse.mtj.internal.core.util.ColonDelimitedProperties;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * A factory for accessing MIDlet suite instances based on projects.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 * @author Craig Setera
 */
public class MidletSuiteFactory {

    /**
     * Workspace runnable for creating a MIDlet Suite within a project.
     */
    public static class MidletSuiteCreationRunnable {

        /**
         * 
         */
        private static final String BIN_FOLDER_NAME = "bin"; //$NON-NLS-1$

        private IMIDPDevice device;

        private String jadFileName;

        private IJavaProject javaProject;

        private boolean preprocessingEnable;

        private IProject project;

        private Map<String, String> properties;

        private boolean localizationEnable;

        private String propertiesFolderName;

        private String packageName;

        private boolean jmUnitSupportEnable;

        /**
         * Construct a new runnable for MIDlet suite creation.
         * 
         * @param project
         */
        private MidletSuiteCreationRunnable(IProject project,
                IJavaProject javaProject, IMIDPDevice device, String jadFileName) {
            this.project = project;
            this.javaProject = javaProject;
            this.device = device;
            this.jadFileName = jadFileName;
        }

        /**
         * @return the preprocessEnable
         */
        public boolean isPreprocessingEnable() {
            return preprocessingEnable;
        }

        /**
         * @return the localizationEnable
         */
        public boolean isLocalizationEnable() {
            return localizationEnable;
        }

        /**
         * @return the jmUnitSupportEnable
         */
        public boolean isJMUnitSupportEnable() {
            return jmUnitSupportEnable;
        }

        /**
         * Run the specified runnable using the specified progress monitor.
         * 
         * @param monitor the monitor used to report progress
         * @throws InvocationTargetException
         * @throws InterruptedException
         */
        public void run(IProgressMonitor monitor)
                throws InvocationTargetException, InterruptedException {
            try {
                // Configure the project
                IMidletSuiteProject suite = getMidletSuiteProject(javaProject);

                // Create the localization data
                createL10nData(suite, monitor);

                suite.setJadFileName(jadFileName);
                addNatures(monitor);

                setJavaProjectOptions(monitor);

                // Set the platform definition for the project
                setProjectMetadata();

                // Add the default application descriptor file.
                createApplicationDescriptorInProject(monitor);

                // Adds the device to the configuration.
                setDeviceIntoActiveConfig(suite, device);
                suite.saveMetaData();
                // Adds the device container into classpath
                suite.refreshClasspath(monitor);
                IJavaProject javaProject2 = suite.getJavaProject();
                for(IClasspathEntry r : javaProject2.getRawClasspath()) {
                    System.out.println(r.getPath().toString());
                }
            } catch (CoreException e) {
                throw new InvocationTargetException(e);
            } catch (IOException e) {
                throw new InvocationTargetException(e);
            }
        }

        private void createL10nData(IMidletSuiteProject suite,
                IProgressMonitor monitor) throws CoreException, IOException {
            IProject project = suite.getProject();

            if (this.localizationEnable) {
                IFolder newSource = project.getFolder(propertiesFolderName);
                if (!newSource.exists()) {
                    newSource.create(true, true, monitor);
                }
                project.refreshLocal(IResource.DEPTH_ONE, monitor);

                List<IClasspathEntry> entries = new ArrayList<IClasspathEntry>();
                entries.addAll(Arrays.asList(javaProject.getRawClasspath()));

                IClasspathEntry[] array = entries
                        .toArray(new IClasspathEntry[entries.size()]);
                if (!javaProject.hasClasspathCycle(array)) {
                    javaProject.setRawClasspath(array, monitor);
                }

                IResource[] sources = Utils.getSourceFolders(javaProject);
                for (int i = 0; i < sources.length; i++) {
                    IPackageFragmentRoot source = javaProject
                            .getPackageFragmentRoot(sources[i]);
                    if (source.exists()) {
                        IPackageFragment packageFragment = source
                                .createPackageFragment(packageName, true,
                                        monitor);
                        L10nApi.createLocalizationFile(project, newSource
                                .getProjectRelativePath(), packageFragment);
                        javaProject.getProject().refreshLocal(
                                IResource.DEPTH_INFINITE,
                                new NullProgressMonitor());
                        L10nApi.createLocalizationApi(project, packageFragment,
                                newSource.getProjectRelativePath());
                        javaProject.getProject().refreshLocal(
                                IResource.DEPTH_INFINITE,
                                new NullProgressMonitor());
                        break;
                    }
                }
            }
        }

        /**
         * @param preprocessEnable the preprocessEnable to set
         */
        public void setPreprocessingEnable(boolean preprocessEnable) {
            this.preprocessingEnable = preprocessEnable;
        }

        /**
         * @param localizationEnable the localizationEnable to set
         */
        public void setLocalizationEnabled(boolean localizationEnable) {
            this.localizationEnable = localizationEnable;
        }

        /**
         * @param jmUnitSupportEnable the jmUnitSupportEnable to set
         */
        public void setJMUnitSupport(boolean jmUnitSupportEnable) {
            this.jmUnitSupportEnable = jmUnitSupportEnable;
        }

        /**
         * Add the specified nature to the list of natures if it is not already
         * in the list.
         * 
         * @param natures
         * @param nature
         * @return
         */
        private boolean addNatureIfNecessary(ArrayList<String> natures,
                String nature) {
            boolean added = false;

            if (!natures.contains(nature)) {
                natures.add(nature);
                added = true;
            }

            return added;
        }

        /**
         * Add the Java ME nature to the specified project.
         * 
         * @param monitor
         * @throws CoreException
         */
        private void addNatures(IProgressMonitor monitor) throws CoreException {
            IProjectDescription desc = project.getDescription();
            ArrayList<String> natures = new ArrayList<String>(Arrays
                    .asList(desc.getNatureIds()));

            boolean updated = addNatureIfNecessary(natures,
                    IMTJCoreConstants.MTJ_NATURE_ID);
            updated = updated
                    | addNatureIfNecessary(natures, JavaCore.NATURE_ID);

            if (isPreprocessingEnable()) {
                updated = updated
                        | addNatureIfNecessary(natures,
                                IMTJCoreConstants.J2ME_PREPROCESSING_NATURE_ID);
            }

            if (isLocalizationEnable()) {
                updated = updated
                        | addNatureIfNecessary(natures,
                                IMTJCoreConstants.L10N_NATURE_ID);
            }

            if (isJMUnitSupportEnable()) {
                updated = updated
                        | addNatureIfNecessary(natures,
                                IMTJCoreConstants.JMUNIT_NATURE_ID);
            }

            if (updated) {
                IProgressMonitor submonitor = new SubProgressMonitor(monitor, 1);
                desc.setNatureIds(natures.toArray(new String[natures.size()]));
                project.setDescription(desc, submonitor);
            }
        }

        /**
         * Sets the specified device on the active configuration. It creates a
         * new configuration in case there isn't any.
         * 
         * @param suite midlet suite.
         * @param device target device.
         */
        private void setDeviceIntoActiveConfig(IMidletSuiteProject suite,
                IDevice device) {
            MTJRuntimeList configurations = suite.getRuntimeList();
            MTJRuntime activeConfig = configurations.getActiveMTJRuntime();
            if (activeConfig != null) {
                activeConfig.setDevice(device);
                // if activeConfig is not null, must return here, make sure not
                // add
                // the acriveConfig again.
                return;
            }
            if ((activeConfig == null) && !configurations.isEmpty()) {
                activeConfig = configurations.get(0);
            }
            // if configurations is empty, we create a configuration according
            // the device, and add it into the configurations
            if (activeConfig == null) {
                activeConfig = new MTJRuntime(device.getName());
                activeConfig.setSymbolSet(device.getSymbolSet());
            }
            activeConfig.setActive(true);
            activeConfig.setDevice(device);
            configurations.add(activeConfig);
        }

        /**
         * Create the template application descriptor in the project.
         * 
         * @param monitor
         * @throws CoreException
         */
        private void createApplicationDescriptorInProject(
                IProgressMonitor monitor) throws CoreException, IOException {

            // Get the project references
            IMidletSuiteProject midletSuite = getMidletSuiteProject(javaProject);

            // Check the JAD file for existence
            IFile jadFile = midletSuite.getApplicationDescriptorFile();

            if (!jadFile.exists()) {
                InputStream is = getJADFileSource(midletSuite);
                jadFile.create(is, true, monitor);
            }
        }

        /**
         * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProject#getDefaultApplicationDescriptorProperties()
         */
        private ColonDelimitedProperties getDefaultApplicationDescriptorProperties(
                IMidletSuiteProject suite) {
            ColonDelimitedProperties descriptor = new ColonDelimitedProperties();

            // Do the OTA URL
            descriptor.setProperty(IJADConstants.JAD_MIDLET_JAR_URL, suite
                    .getJarFilename());

            // Couple of names...
            descriptor.setProperty(IJADConstants.JAD_MIDLET_NAME, properties
                    .get(IJADConstants.JAD_MIDLET_NAME));
            descriptor.setProperty(IJADConstants.JAD_MIDLET_VENDOR, properties
                    .get(IJADConstants.JAD_MIDLET_VENDOR));
            descriptor.setProperty(IJADConstants.JAD_MIDLET_VERSION, properties
                    .get(IJADConstants.JAD_MIDLET_VERSION));

            // Platform information
            descriptor.setProperty(IJADConstants.JAD_MICROEDITION_CONFIG,
                    properties.get(IJADConstants.JAD_MICROEDITION_CONFIG));

            descriptor.setProperty(IJADConstants.JAD_MICROEDITION_PROFILE,
                    properties.get(IJADConstants.JAD_MICROEDITION_PROFILE));

            return descriptor;
        }

        /**
         * Get the bytes that make up the contents of the to-be created JAD
         * file.
         * 
         * @param midletSuite
         * @return
         * @throws IOException
         * @throws CoreException
         */
        private InputStream getJADFileSource(IMidletSuiteProject midletSuite)
                throws IOException, CoreException {
            // The InputStream to be returned...
            InputStream is = null;

            // Check for a JAD source file to see if it exists in the
            // project source directory
            String jadName = midletSuite.getJadFileName();
            IFolder folder = project.getFolder(BIN_FOLDER_NAME);
            if (folder.exists()) {
                IFile jadFile = folder.getFile(jadName);
                if (jadFile.exists()) {
                    is = jadFile.getContents();
                }
            }

            // If we didn't get a previous source file, use defaults
            if (is == null) {
                // Get the application descriptor and write it to
                // a byte array
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ColonDelimitedProperties defaultProps = getDefaultApplicationDescriptorProperties(midletSuite);
                defaultProps.store(bos, ""); //$NON-NLS-1$

                // Use the byte array as the source to create the
                // new file
                is = new ByteArrayInputStream(bos.toByteArray());
            }

            return is;
        }

        /**
         * Set the necessary java project options.
         * 
         * @param monitor
         */
        @SuppressWarnings("unchecked")
        private void setJavaProjectOptions(IProgressMonitor monitor) {
            Preferences prefs = MTJCore.getMTJCore().getPluginPreferences();

            boolean forceJava11 = prefs
                    .getBoolean(IMTJCoreConstants.PREF_FORCE_JAVA11);

            if (forceJava11) {
                Map<String, String> options = javaProject.getOptions(true);
                JavaCore.setComplianceOptions(JavaCore.VERSION_1_3, options);
                javaProject.setOptions(options);
            }
        }

        /**
         * Set the persistent property on the created project to track the
         * platform definition.
         */
        private void setProjectMetadata() throws CoreException {
            IMTJProject midletprj = getMidletSuiteProject(javaProject);
            midletprj.saveMetaData();
        }

        public void setProperties(Map<String, String> properties) {
            this.properties = properties;
        }

        public void setPropertiesFolderName(String propertiesFolderName) {
            this.propertiesFolderName = propertiesFolderName;
        }

        public void setPackageName(String packageName) {
            this.packageName = packageName;
        }
    }

    // Storage of the previously created MIDlet suite projects
    private static final Map<IJavaProject, IMidletSuiteProject> midletSuiteMap = new HashMap<IJavaProject, IMidletSuiteProject>();

    static {
        MTJCore.getDeviceRegistry().addRegistryListener(new DeviceRegistryListener());
    }
    
    /**
     * Return a runnable capable of setting up the J2ME nature on the project.
     * 
     * @return a runnable that can be used to create a new MIDlet suite
     */
    public static MidletSuiteCreationRunnable getMidletSuiteCreationRunnable(
            IProject project, IJavaProject javaProject, IMIDPDevice device,
            String jadFileName) {
        return new MidletSuiteCreationRunnable(project, javaProject, device,
                jadFileName);
    }

    /**
     * Return the MIDlet suite project instance for the specified java project.
     * 
     * @param javaProject the Java project to retrieve the MIDlet suite wrapper
     * @return the MIDlet suite wrapper
     */
    public static IMidletSuiteProject getMidletSuiteProject(
            IJavaProject javaProject) {
        IMidletSuiteProject suite = null;

        synchronized (midletSuiteMap) {
            suite = midletSuiteMap.get(javaProject);
            if (suite == null) {
                suite = new MidletSuiteProject(javaProject);
                midletSuiteMap.put(javaProject, suite);
            }
        }

        return suite;
    }

    /**
     * Return the MIDlet suite project instance with the specified project name.
     * 
     * @param name the name of the project to retrieve the MIDlet suite wrapper
     * @return the MIDlet suite wrapper or null if suite is not a MIDlet Suite.
     */
    public static IMTJProject getMidletSuiteProject(String name) {
		IMTJProject suite = null;

		try {
			if (name != null) {
				IProject project = ResourcesPlugin.getWorkspace().getRoot()
						.getProject(name);
				if (project.exists()
						&& project.hasNature(IMTJCoreConstants.MTJ_NATURE_ID)) {
					suite = getMidletSuiteProject(JavaCore.create(project));
				}
			}
		} catch (CoreException e) {
			MTJLogger.log(IStatus.ERROR, e);
		}
		return suite;
	}

    /**
     * @param projectname
     * @return
     */
    public static boolean isMidletSuiteProject(IProject project) {
        if (project == null) {
            return false;
        }
        IMTJProject mtjproject = getMidletSuiteProject(project.getName());
        return mtjproject == null ? false : true;
    }

    /**
     * Remove the MIDlet suite project from the cache hash-map.
     * 
     * @param javaProject - the Java project wrapped by the MIDlet suite which
     *            will be removed.
     */
    public static void removeMidletSuiteProject(IJavaProject javaProject) {
        synchronized (midletSuiteMap) {
            midletSuiteMap.remove(javaProject);
        }
    }

    /**
     * Static-only access
     */
    private MidletSuiteFactory() {
        super();
    }
    
    public static class DeviceRegistryListener implements IDeviceRegistryListener {
        public void deviceAdded(IDevice device) {
            for(IMTJProject midletSuiteProject : midletSuiteMap.values()) {
                MTJRuntimeList runtimeList = midletSuiteProject.getRuntimeList();
                for(MTJRuntime mtjRuntime : runtimeList) {
                    IDevice runtimeDevice = mtjRuntime.getDevice();
                    if(runtimeDevice.getIdentifier().equals(device.getIdentifier())) {
                        mtjRuntime.setDevice(device);
                        if (mtjRuntime.isActive()) {
                            try {                                    
                                midletSuiteProject.refreshClasspath(new NullProgressMonitor());
                            } catch (CoreException e) {
                                MTJLogger.log(IStatus.ERROR, e);
                            }
                        }
                    }
                }
            }
        }
        public void deviceRemoved(IDevice device) {}
    }
}
