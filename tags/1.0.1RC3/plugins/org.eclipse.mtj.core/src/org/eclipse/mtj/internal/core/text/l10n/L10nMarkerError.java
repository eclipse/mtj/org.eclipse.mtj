/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Renato Franca (Motorola)  - Initial Version
 */

package org.eclipse.mtj.internal.core.text.l10n;

/**
 * Class That represents an error to be inserted on editor
 */
public class L10nMarkerError {

    private String errorMessage;
    private int line;

    /**
     * @param errorMessage The error message
     * @param lines the error line
     */
    public L10nMarkerError(String errorMessage, int line) {
        this.errorMessage = errorMessage;
        this.line = line;
    }

    /**
     * @return The error message
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage sets the error message
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return The error line
     */
    public int getLine() {
        return line;
    }

    /**
     * Sets the error line
     * 
     * @param lines new line
     */
    public void setLine(int lines) {
        this.line = lines;
    }

}
