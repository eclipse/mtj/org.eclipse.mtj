/**
 * Copyright (c) 2000, 2009 IBM Corporation and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Brock Janiczak <brockj@tpg.com.au> - bug 169373
 *     Gary Duprex <Gary.Duprex@aspectstools.com> - bug 150225
 *     Bartosz Michalik <bartosz.michalik@gmail.com> - bug 214156
 *     Renato Franca (Motorola) - Adapted code from org.eclipse.pde.internal.core.builders/PDEMarkerFactory
 */
package org.eclipse.mtj.internal.core.text.l10n;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.xml.sax.SAXParseException;

/**
 * Manages the markers of a given IFile
 */
public class L10nMarkerManager {

    /**
     * MARKER_ID constant
     */
    public static final String MARKER_ID = "org.eclipse.mtj.core.problem"; //$NON-NLS-1$

    /**
     * @param file
     * @param severity
     * @param message
     * @param line
     * @throws CoreException
     */
    private void createMarker(IFile file, int severity, String message,
            int line, boolean clean) throws CoreException {

        if (clean) {
            clearMarkers(file, IMarker.SEVERITY_ERROR);
        }

        IMarker marker = file.createMarker(MARKER_ID);
        marker.setAttribute(IMarker.SEVERITY, severity); //$NON-NLS-1$
        marker.setAttribute(IMarker.MESSAGE, message);
        marker.setAttribute(IMarker.LINE_NUMBER, line);

    }

    /**
     * @param file
     * @param fModel
     * @throws CoreException
     */
    public void createMarker(IFile file, L10nModel fModel) throws CoreException {

        if (fModel.getStatus().getException() != null) {

            createMarker(file, IMarker.SEVERITY_ERROR, fModel.getStatus()
                    .getMessage(), ((SAXParseException) fModel.getStatus()
                    .getException()).getLineNumber(), true);
        }
    }

    /**
     * @param file
     * @param fModel
     * @throws CoreException
     */
    public void createMarker(IFile file, SAXParseException saxException)
            throws CoreException {

        if (saxException != null) {

            createMarker(file, IMarker.SEVERITY_ERROR, saxException
                    .getMessage(), saxException.getLineNumber(), true);
        }
    }

    /**
     * @param file
     * @param fModel
     * @throws CoreException
     */
    public void createMarker(IFile file, int line, String errorMessage,
            boolean clean) throws CoreException {

        createMarker(file, IMarker.SEVERITY_ERROR, errorMessage, line, clean);
    }

    /**
     * @param file
     * @param fModel
     * @throws CoreException
     */
    public void clearMarkers(IFile file, int severity) throws CoreException {

        IMarker[] mark = file.findMarkers(MARKER_ID, true,
                IResource.DEPTH_INFINITE);

        if (mark.length > 0) {

            // do not delete warning markers
            for (int i = 0; i < mark.length; i++) {
                if (mark[i].getAttribute(IMarker.SEVERITY).equals(severity)) {
                    mark[i].delete();
                }
            }
        }
    }

    /**
     * 
     * @param file
     * @param line
     * @param errorMessage
     * @throws CoreException
     */
    public void addWarningMarker(IFile file, int line, String errorMessage)
            throws CoreException {

        clearWarningMarkers(file);
        createMarker(file, IMarker.SEVERITY_WARNING, errorMessage, line, false);

    }

    /**
     * 
     * @param file
     * @throws CoreException
     */
    public void clearWarningMarkers(IFile file) throws CoreException {
        clearMarkers(file, IMarker.SEVERITY_WARNING);
    }
}
