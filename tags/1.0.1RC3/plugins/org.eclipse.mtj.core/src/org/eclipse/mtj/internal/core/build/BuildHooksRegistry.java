/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     Rafael Amaral (Motorola) - Changing to make MTJ extension point handle 
 *                                dynamic add/remove of plugins
 */
package org.eclipse.mtj.internal.core.build;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionDelta;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IRegistryChangeEvent;
import org.eclipse.core.runtime.IRegistryChangeListener;
import org.eclipse.core.runtime.Platform;
import org.eclipse.mtj.core.MTJCore;

/**
 * BuildHooksRegistry is a registry for the
 * org.eclipse.mtj.core.mtjbuildhook extension
 * point.
 * 
 * @author David Marques
 */
public class BuildHooksRegistry implements IRegistryChangeListener {

    private static final String EXT_ID = "org.eclipse.mtj.core.mtjbuildhook";
    private static final String EXT_MTJ_BUILD_HOOK = "mtjbuildhook";

    private static BuildHooksRegistry instance;
    private List<BuildHookInfo> hooks;

    /**
     * Gets the single class instance.
     * 
     * @return singleton instance.
     */
    public static synchronized BuildHooksRegistry getInstance() {
        if (BuildHooksRegistry.instance == null) {
            BuildHooksRegistry.instance = new BuildHooksRegistry();
            Platform.getExtensionRegistry().addRegistryChangeListener(
                    BuildHooksRegistry.instance);
        }
        return BuildHooksRegistry.instance;
    }

    /**
     * Loads the registry.
     */
    private BuildHooksRegistry() {
        IExtensionRegistry registry = null;
        IConfigurationElement[] extensions = null;
        BuildHookInfo hooksInfo = null;

        this.hooks = new ArrayList<BuildHookInfo>();
        registry = Platform.getExtensionRegistry();
        extensions = registry.getConfigurationElementsFor(EXT_ID);
        for (IConfigurationElement extension : extensions) {
            hooksInfo = new BuildHookInfo(extension);
            this.hooks.add(hooksInfo);
        }
    }

    /**
     * Gets a list of the registered hooks hooks.
     * 
     * @return hooks list.
     */
    public List<BuildHookInfo> getBuildHooks() {
        List<BuildHookInfo> result = new ArrayList<BuildHookInfo>();
        result.addAll(this.hooks);
        return result;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.core.runtime.IRegistryChangeListener#registryChanged(org.
     * eclipse.core.runtime.IRegistryChangeEvent)
     */
    public void registryChanged(IRegistryChangeEvent event) {
        
        String pluginId = MTJCore.getMTJCore().getBundle().getSymbolicName();
        IExtensionDelta[] deltas = event.getExtensionDeltas(pluginId,
                EXT_MTJ_BUILD_HOOK);
        
        for (int i = 0; i < deltas.length; i++) {
            
            if (deltas[i].getKind() == IExtensionDelta.ADDED) {
                
                IConfigurationElement[] configurationElements = deltas[i]
                        .getExtension().getConfigurationElements();
                
                for (IConfigurationElement configurationElement : configurationElements) {
                    BuildHookInfo hooksInfo = new BuildHookInfo(
                            configurationElement);
                    this.hooks.add(hooksInfo);
                }
            } else {
                
                IConfigurationElement[] configurationElements = deltas[i]
                        .getExtension().getConfigurationElements();
                
                for (IConfigurationElement configurationElement : configurationElements) {
                    BuildHookInfo hooksInfo = new BuildHookInfo(
                            configurationElement);
                    for (int j = 0; j < this.hooks.size(); j++) {
                        if(this.hooks.get(j).getHookId().equals(hooksInfo.getHookId())){
                            this.hooks.remove(j);
                            break;
                        }
                    }
                }
            }
        }
    }

}
