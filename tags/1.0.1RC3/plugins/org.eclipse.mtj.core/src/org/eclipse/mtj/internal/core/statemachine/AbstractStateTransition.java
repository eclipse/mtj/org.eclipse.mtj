/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.statemachine;

import org.eclipse.mtj.internal.core.Messages;

/**
 * AbstractStateTransition class represents an abstract state
 * transition that must be sub-classed in order receive events
 * sent to the state machine by the {@link StateMachine#postEvent(AbstractStateMachineEvent)}
 * method.
 * <br> 
 * The {@link #isTransitionReady(AbstractStateMachineEvent)} 
 * method is called for every transition added to the current state
 * of the state machine in order to check if a transition is ready to
 * go to the target state. 
 * 
 * @see AbstractState
 * @see StateMachine
 * 
 * @author David Marques
 */
public abstract class AbstractStateTransition {

	private AbstractState source;
	private AbstractState target;
	
	/**
	 * Creates an instance of an AbstractStateTransition from the specified
	 * source to the specified target states. 
	 * 
	 * @param _source source state.
	 * @param _target target state.
	 */
	public AbstractStateTransition(AbstractState _source, AbstractState _target) {
		if (_source == null) {
			throw new IllegalArgumentException(Messages.AbstractStateTransition_sourceStateIsNull);
		}
		if (_target == null) {
			throw new IllegalArgumentException(Messages.AbstractStateTransition_targetStateIsNull);
		}
		this.source = _source;
		this.target = _target;
	}
	
	/**
	 * 
	 */
	public AbstractStateTransition() {}
	
	/**
	 * Sets the source state.
	 * 
	 * @param source source state.
	 */
	public final void setSource(AbstractState source) {
		this.source = source;
	}
	
	/**
	 * Gets the source state.
	 * 
	 * @return source state.
	 */
	public final AbstractState getSource() {
		return source;
	}
	
	/**
	 * Sets the target state.
	 * 
	 * @param target target state.
	 */
	public final void setTarget(AbstractState target) {
		this.target = target;
	}
	
	/**
	 * Gets the target state.
	 * 
	 * @return target state.
	 */
	public final AbstractState getTarget() {
		return target;
	}
	
	/**
	 * Called when a state receives a new event in order to test
	 * if the new event triggers this transition from the source
	 * to the target state. 
	 * 
	 * @param event new event.
	 * @return true if transition is to be triggered false otherwise.
	 */
	protected abstract boolean isTransitionReady(AbstractStateMachineEvent event);
	
	/**
	 * Called when the transition from the source to the target states are been done.
	 */
	protected abstract void onTransition();
}
