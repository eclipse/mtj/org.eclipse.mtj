/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.project;

import org.eclipse.core.resources.IProject;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;

/**
 * This enum represents the types of {@link IMTJProject}'s provided by MTJ.
 * <p>
 * At the moment, MTJ only provides one implementation of {@link IMTJProject},
 * the {@link IMidletSuiteProject} project type.
 * </p>
 * <p>
 * Clients must use this enum in some {@link MTJCore} methods to retrieve
 * resources associated with an specific type of project, for example, an
 * {@link IMetaData} that is accessible via the
 * {@link MTJCore#createMetaData(IProject, ProjectType)} method.
 * </p>
 * <p>
 * CLients must use the {@link #fromProject(IProject)}method to try identify
 * from which type of MTJProjet a IProject belongs.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public enum ProjectType {

    /**
     * Represents a {@link IMidletSuiteProject} type of project.
     */
    MIDLET_SUITE,

    /**
     * Represents an unknown type of project
     */
    UNKNOWN;

    /**
     * Return the project type of the given project.
     * 
     * @param project the project from which we try to define it's type.
     * @return the project type or {@link #UNKNOWN} if the type could not be
     *         defined.
     */
    public static ProjectType fromProject(IProject project) {

        if (MidletSuiteFactory.isMidletSuiteProject(project)) {
            return MIDLET_SUITE;
        }
        return UNKNOWN;
    }

}
