/**
 * Copyright (c) 2003, 2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.persistence;

import java.util.Properties;

/**
 * Implementors of this interface provide facility methods for storing and
 * retrieving persistable objects.
 * <p>
 * Clients don't need to implement this interface as MTJ already provides the
 * default implementation.
 * </p>
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @since 1.0
 */
public interface IPersistenceProvider {

    /**
     * Returns the current value of the boolean-valued persisted element with
     * the given name.
     * <p>
     * Returns the default value (<code>false</code>) if there is no persisted
     * element with the given name, or if the current value cannot be treated as
     * an boolean.
     * </p>
     * 
     * @param name the name of the persisted element.
     * @return the boolean-valued persisted element.
     * @throws PersistenceException if an error was found while retrieving the
     *             boolean-valued persisted element.
     */
    public boolean loadBoolean(String name) throws PersistenceException;

    /**
     * Returns the current value of the integer-valued persisted element with
     * the given name.
     * <p>
     * Returns the default value (<code>0</code>) if there is no persisted
     * element with the given name, or if the current value cannot be treated as
     * an integer.
     * </p>
     * 
     * @param name the name of the persisted element.
     * @return the integer-valued persisted element.
     * @throws PersistenceException if an error was found while retrieving the
     *             integer-valued persisted element.
     */
    public int loadInteger(String name) throws PersistenceException;

    /**
     * Returns the current value of the {@link IPersistable} object with the
     * given name.
     * <p>
     * Returns the <code>null</code> value if there is no persisted element with
     * the given name.
     * </p>
     * 
     * @param name the name of the persistable object.
     * @return a newly allocated instance of the persisted element.
     * @throws PersistenceException if could not create a new instance of the
     *             persisted element.
     */
    public IPersistable loadPersistable(String name)
            throws PersistenceException;

    /**
     * Returns the current value of the persisted {@link Properties} object with
     * the given name.
     * <p>
     * Returns the <code>null</code> value if there is no persisted element with
     * the given name.
     * </p>
     * 
     * @param name the name of the persisted element.
     * @return the persisted {@link Properties} object instance.
     * @throws PersistenceException if an error was found while retrieving the
     *             persisted {@link Properties}.
     */
    public Properties loadProperties(String name) throws PersistenceException;

    /**
     * Returns the current value of the referenced object.
     * <p>
     * Returns the <code>null</code> if there is no preference with the given
     * name, if the current value cannot be treated as a reference or if the
     * referenced object could not be found.
     * </p>
     * 
     * @param name the name of the persisted element.
     * @return the referenced object.
     * @throws PersistenceException if an error was found while retrieving the
     *             reference.
     */
    public Object loadReference(String name) throws PersistenceException;

    /**
     * Returns the current value of the string-valued persisted element with the
     * given name.
     * <p>
     * Returns <code>null</code> if no persisted element with the given name
     * could be found. In case the persisted element does not have a specified
     * value will return the empty string <code>""</code>.
     * </p>
     * 
     * @param name the name of the persisted element.
     * @return the string-valued persisted element.
     * @throws PersistenceException if an error was found while retrieving the
     *             string-valued persisted element.
     */
    public String loadString(String name) throws PersistenceException;

    /**
     * Store a boolean-valued element using the given name.
     * 
     * @param name the name of the boolean-valued element. This is
     *            case-sensitive and must not be <code>null</code> or an empty
     *            String <code>""</code>.
     * @param value the value to be stored.
     * @throws PersistenceException if the boolean-valued element could not be
     *             persisted.
     */
    public void storeBoolean(String name, boolean value)
            throws PersistenceException;

    /**
     * Store a integer-valued element using the given name.
     * 
     * @param name the name of the integer-valued element. This is
     *            case-sensitive and must not be <code>null</code> or an empty
     *            String <code>""</code>.
     * @param value the value to be stored.
     * @throws PersistenceException if the integer-valued element could not be
     *             persisted.
     */
    public void storeInteger(String name, int value)
            throws PersistenceException;

    /**
     * Store a persistable object value using the given name.
     * 
     * @param name the name for the {@link IPersistable} element. This is
     *            case-sensitive and must not be <code>null</code> or an empty
     *            String <code>""</code>.
     * @param value a non-<code>null</code> {@link IPersistable} to be stored.
     *            For <code>null</code> values a {@link PersistenceException}
     *            wont be thrown and the invalid value will be ignored.
     * @throws PersistenceException if the element could not be persisted.
     */
    public void storePersistable(String name, IPersistable value)
            throws PersistenceException;

    /**
     * Store a {@link Properties} object value using the given name.
     * 
     * @param name the name for the properties element. This is case-sensitive
     *            and must not be <code>null</code> or an empty String
     *            <code>""</code>.
     * @param value a non-<code>null</code> {@link Properties} to be stored. For
     *            <code>null</code> values a {@link PersistenceException} wont
     *            be thrown and the invalid value will be ignored.
     * @throws PersistenceException if the element could not be persisted.
     */
    public void storeProperties(String name, Properties value)
            throws PersistenceException;

    /**
     * Store a reference to the specified object using the given name.
     * <p>
     * <strong>NOTE:</strong> This object must have previously been stored by
     * this persistence provider or a persistence exception will be thrown.
     * </p>
     * 
     * @param name the name of the reference element. This is case-sensitive and
     *            must not be <code>null</code> or an empty String
     *            <code>""</code>.
     * @param referenceObject the object to be referenced.
     * @throws PersistenceException if the referenced object was not previously
     *             been stored by this persistence provider or if the element
     *             could not be persisted.
     */
    public void storeReference(String name, Object referenceObject)
            throws PersistenceException;

    /**
     * Store the string-valued element using the given name.
     * 
     * @param name the name of the string-valued element. This is case-sensitive
     *            and must not be <code>null</code> or an empty String
     *            <code>""</code>.
     * @param string the string-valued element to be stored. This is
     *            case-sensitive and must not be <code>null</code>. For
     *            <code>null</code> values a {@link PersistenceException} wont
     *            be thrown and the invalid value will be ignored.
     * 
     * @throws PersistenceException if the string-valued element could not be
     *             persisted.
     */
    public void storeString(String name, String string)
            throws PersistenceException;
}
