/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.core.symbol;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.ILibrary;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noextend This class is not intended to be subclassed by clients.
 * @author Feng Wang
 */
public class SymbolUtils {

    /**
     * Get symbol value in safe format for preprocessor.
     * 
     * @return
     */
    public static String getSafeSymbolValue(String value) {
        if (value == null || value.trim().length() == 0) {
            value = Boolean.TRUE.toString();
        }
        if (!value.startsWith("\"") && !value.startsWith("'")) { //$NON-NLS-1$ //$NON-NLS-2$
            value = "'" + value + "'"; //$NON-NLS-1$ //$NON-NLS-2$
        }
        return value;
    }

    // Properties that should not use as symbols
    public static final String PROP_UEI_VERSION = "uei.version"; //$NON-NLS-1$
    public static final String PROP_UEI_ARGUMENTS = "uei.arguments"; //$NON-NLS-1$
    public static final String PROP_DEVICE_LIST = "device.list"; //$NON-NLS-1$
    public static final String PROP_SECURITY_DOMAINS = "security.domains"; //$NON-NLS-1$
    public static final String PROP_BOOTCLASSPATH = "bootclasspath"; //$NON-NLS-1$
    public static final String PROP_APIS = "apis"; //$NON-NLS-1$
    public static final String PROP_DESCRIPTION = "description"; //$NON-NLS-1$

    // The properties that used for creating symbols.
    private static List<String> excludedProperties;
    static {
        excludedProperties = new ArrayList<String>();
        excludedProperties.add(PROP_UEI_VERSION);
        excludedProperties.add(PROP_UEI_ARGUMENTS);
        excludedProperties.add(PROP_DEVICE_LIST);
        excludedProperties.add(PROP_SECURITY_DOMAINS);
        excludedProperties.add(PROP_BOOTCLASSPATH);
        excludedProperties.add(PROP_APIS);
        excludedProperties.add(PROP_DESCRIPTION);
    }

    /**
     * Create SymbolSet from device.
     * 
     * @param device
     * @return
     */
    public static ISymbolSet createSymbolSet(IDevice device) {
        ISymbolSet symbolSet = SymbolSetFactory.getInstance().createSymbolSet(
                "Default");
        addLibrarySymbols(symbolSet, device);
        return symbolSet;
    }

    /**
     * Create SymbolSet from properties.
     * 
     * @param properties
     * @return
     */
    public static ISymbolSet createSymbolSet(Properties properties) {
        ISymbolSet symbolSet = SymbolSetFactory.getInstance().createSymbolSet(
                "Properties");
        populateSymbolSetFromProperties(symbolSet, properties);
        return symbolSet;
    }

    /**
     * If a name of a symbol do not start with letters, preprocessor will throw
     * exception, so we should add letters at first.
     * 
     * @param lib
     * @return
     */
    public static String replaceFirstNonLetterChar(String lib) {
        lib = lib.replaceFirst("^[^a-zA-Z].+", "a_" + lib);//$NON-NLS-1$ //$NON-NLS-2$
        return lib;
    }

    /**
     * Import and create array of SymbosDefinitionSet from J2ME polish XML files
     * (devices.xml & groups.xml).
     * 
     * @param monitor Progress monitor, inform user about process of import.
     *            Null is allowed.
     * @param devicesInputStream Input stream of device.xml file.
     * @param groupsInputStream Input stream of groups.xml file.
     * @return
     * @throws PersistenceException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @author Ales "afro" Milan
     */
    public static List<ISymbolSet> importFromJ2MEPolishFormat(
            IProgressMonitor monitor, InputStream devicesInputStream,
            InputStream groupsInputStream) throws PersistenceException,
            ParserConfigurationException, SAXException, IOException {

        Vector<ISymbolSet> result = new Vector<ISymbolSet>();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        Document devicesDOM = db.parse(devicesInputStream);
        Document groupsDOM = db.parse(groupsInputStream);

        HashMap<String, Element> groups = prepareGroups(groupsDOM);

        NodeList nl = devicesDOM.getElementsByTagName("device"); //$NON-NLS-1$
        if (monitor != null) {
            monitor.beginTask("Importing...", nl.getLength()); //$NON-NLS-1$
        }
        for (int i = 0; i < nl.getLength(); i++) {

            HashMap<String, String> capabilities = new HashMap<String, String>();
            HashMap<String, String> groupCapabilities;
            String features;

            Element deviceElement = (Element) nl.item(i);
            String name = ((Element) deviceElement.getElementsByTagName(
                    "identifier").item(0)).getTextContent(); //$NON-NLS-1$

            Element featuresElement = (Element) deviceElement
                    .getElementsByTagName("features").item(0); //$NON-NLS-1$
            if (featuresElement != null) {
                features = featuresElement.getTextContent();
                capabilities.put("features", features); //$NON-NLS-1$
            }

            Element groupElement = (Element) deviceElement
                    .getElementsByTagName("groups").item(0); //$NON-NLS-1$
            if (groupElement != null) {
                StringTokenizer st = new StringTokenizer(groupElement
                        .getTextContent(), J2MEPOLISH_CAPABILITY_SEPARATOR);
                while (st.hasMoreTokens()) {
                    String group = st.nextToken().trim();
                    groupCapabilities = getGroupCapabilities(groups, group);
                    copyCapabilities(capabilities, groupCapabilities);
                }
                capabilities.put("groups", groupElement.getTextContent() //$NON-NLS-1$
                        .toLowerCase());
            }

            NodeList capabilitiesNL = deviceElement
                    .getElementsByTagName("capability"); //$NON-NLS-1$
            for (int j = 0; j < capabilitiesNL.getLength(); j++) {
                Element capabilityElement = (Element) capabilitiesNL.item(j);
                copyCapability(capabilities, capabilityElement
                        .getAttribute("name"), capabilityElement //$NON-NLS-1$
                        .getAttribute("value")); //$NON-NLS-1$
            }

            capabilities.put(name, "true"); //$NON-NLS-1$

            StringTokenizer st = new StringTokenizer(name,
                    J2MEPOLISH_CAPABILITY_SEPARATOR);
            while (st.hasMoreTokens()) {
                String device = st.nextToken().trim();
                device = device.replace('/', '_');
                device = device.replace('(', '_');
                device = device.replace(')', '_');

                ISymbolSet definitionSet = createSymbolDefinitionSet(device,
                        capabilities);
                result.add(definitionSet);
            }
            if (monitor != null) {
                monitor.worked(1);
            }
        }
        if (monitor != null) {
            monitor.done();
        }

        return result;
    }

    /**
     * Create symbols from device libraries and add them to SymbolSet.
     * 
     * @param symbolSet
     * @param device
     */
    private static void addLibrarySymbols(ISymbolSet symbolSet, IDevice device) {
        if (device == null) {
            return;
        }
        List<ILibrary> libraries = device.getClasspath().getEntries();

        if (libraries != null) {
            for (ILibrary librarie : libraries) {
                for (IAPI iapi : librarie.getAPIs()) {

                    String key = (iapi.getIdentifier()).replace(' ', '_');
                    key = replaceFirstNonLetterChar(key);
                    String value = NLS.bind(
                            IMTJCoreConstants.VERSION_NLS_BIND_TEMPLATE,
                            new Object[] { iapi.getVersion().getMajor(),
                                    iapi.getVersion().getMinor() });
                    symbolSet.add(key, value);
                }
            }
        }
    }

    /**
     * Create symbols from device properties and add them to SymbolSet.
     * 
     * @param symbolSet
     * @param prop
     */
    private static void populateSymbolSetFromProperties(ISymbolSet symbolSet,
            Properties prop) {

        Properties properties = prop;
        Set<Entry<Object, Object>> set = properties.entrySet();

        for (Entry<Object, Object> entry : set) {
            String key = String.valueOf(entry.getKey());
            if (!excludedProperties.contains(key)) {
                key = replaceFirstNonLetterChar(key);
                String value = String.valueOf(entry.getValue());
                symbolSet.add(key, value);
            }
        }
    }

    private static final String J2MEPOLISH_CAPABILITY_SEPARATOR = ","; //$NON-NLS-1$

    /**
     * Create SymbolDefinitionSet from Map of properties.
     * 
     * @param name
     * @param symbols
     * @author Ales "afro" Milan
     * @return
     */
    private static ISymbolSet createSymbolDefinitionSet(String name,
            HashMap<String, String> symbols) {

        ISymbolSet definitionSet = new SymbolSet();
        definitionSet.setName(name);
        definitionSet.add(name);

        for (String key : symbols.keySet()) {
            try {
                String value = symbols.get(key);
                definitionSet.add(key, value);
            } catch (RuntimeException e) {
                MTJLogger.log(IStatus.ERROR, e);
            }
        }

        return definitionSet;
    }

    /**
     * Copy capabilities from Map to other Map.
     * 
     * @param capabilities dest map
     * @param parentCapabilities source map, records from this map will be
     *            copied to dest map
     */
    private static void copyCapabilities(HashMap<String, String> capabilities,
            HashMap<String, String> parentCapabilities) {
        Iterator<String> i = parentCapabilities.keySet().iterator();
        while (i.hasNext()) {
            String capability = i.next();
            copyCapability(capabilities, capability, parentCapabilities
                    .get(capability));
        }
    }

    /**
     * Copy capability. If exist in dest map capability with same key, then new
     * capability value will be merged to exist capability.
     * 
     * @param capabilities dest map of capabilities
     * @param name key of capability
     * @param capability new capability, this capability value is added or
     *            merged to map
     */
    private static void copyCapability(HashMap<String, String> capabilities,
            String name, String capability) {

        String currentCapabilityValue = capabilities.get(name);
        if ((currentCapabilityValue == null)
                || currentCapabilityValue.equals("")) { //$NON-NLS-1$
            capabilities.put(name, capability);
        } else {
            String mergedCapabilities = mergeCapabilities(
                    currentCapabilityValue, capability);
            capabilities.put(name, mergedCapabilities);
        }

    }

    /**
     * Return capabilities for specified group.
     * 
     * @param groups map of groups XML elements
     * @param name group name
     * @return map of capabilities, key/value
     */
    private static HashMap<String, String> getGroupCapabilities(
            HashMap<String, Element> groups, String name) {
        HashMap<String, String> result = new HashMap<String, String>();
        Element groupElement = groups.get(name);

        if (groupElement != null) {
            NodeList nl = groupElement.getElementsByTagName("parent"); //$NON-NLS-1$
            if ((nl != null) && (nl.getLength() > 0)) {
                Element parent = (Element) nl.item(0);
                String parentValue = parent.getTextContent();
                if ((parentValue != null)
                        && !parent.getTextContent().equals("")) { //$NON-NLS-1$
                    HashMap<String, String> parentCapabilities = getGroupCapabilities(
                            groups, parent.getTextContent());
                    copyCapabilities(result, parentCapabilities);
                }
            }

            nl = groupElement.getElementsByTagName("capability"); //$NON-NLS-1$
            if ((nl != null) && (nl.getLength() > 0)) {
                for (int i = 0; i < nl.getLength(); i++) {
                    Element capabilityElement = (Element) nl.item(i);
                    result.put(capabilityElement.getAttribute("name"), //$NON-NLS-1$
                            capabilityElement.getAttribute("value")); //$NON-NLS-1$
                }
            }
        }

        return result;
    }

    /**
     * Merge two capabilities to one string. Function ignore duplicity
     * 
     * @param currentCapability
     * @param newCapability
     * @return merged capabilities
     */
    private static String mergeCapabilities(String currentCapability,
            String newCapability) {

        Vector<String> v = new Vector<String>();
        StringTokenizer st = new StringTokenizer(currentCapability,
                J2MEPOLISH_CAPABILITY_SEPARATOR);
        while (st.hasMoreTokens()) {
            v.add(st.nextToken().trim());
        }
        st = new StringTokenizer(newCapability, J2MEPOLISH_CAPABILITY_SEPARATOR);
        while (st.hasMoreTokens()) {
            String capability = st.nextToken().trim();
            if (v.indexOf(capability) == -1) {
                v.add(capability);
            }
        }
        String result = ""; //$NON-NLS-1$
        for (int i = 0; i < v.size(); i++) {
            if (i > 0) {
                result = result.concat(","); //$NON-NLS-1$
            }
            result = result.concat(v.elementAt(i));
        }

        return result;
    }

    /**
     * Prepare device groups elements to Map.
     * 
     * @param groupsDOM XML document which contain Device Groups
     *            <p>
     *            Example of XML document: <code>
     * <groups>
     *   <group>
     *     <name>name of group</name>
     *     <description>...</description>
     *     <features>feature1, feature2, ...</features>
     *     <parent>nameOfParrentGroup</parent>
     *     <capability name="capabilityA" value="valueA" />
     *     <capability name="capabilityB" value="valueB" />
     *     ...
     *   </group>
     *   <group>
     *     ...
     *   </group>
     * </groups>
     * </code>
     *            </p>
     * @return return map of XML elements
     */
    private static HashMap<String, Element> prepareGroups(Document groupsDOM) {

        HashMap<String, Element> result = new HashMap<String, Element>();

        NodeList nl = groupsDOM.getElementsByTagName("group"); //$NON-NLS-1$
        for (int i = 0; i < nl.getLength(); i++) {
            Element groupElement = (Element) nl.item(i);
            String name = ((Element) groupElement.getElementsByTagName("name") //$NON-NLS-1$
                    .item(0)).getTextContent();
            result.put(name, groupElement);
        }

        return result;
    }

}
