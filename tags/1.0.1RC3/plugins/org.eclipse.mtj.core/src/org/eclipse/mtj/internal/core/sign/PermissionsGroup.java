/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.sign;

import java.util.ArrayList;
import java.util.List;

/**
 * PermissionsGroup class groups a set of permissions
 * required by a specific class.
 * 
 * @author David Marques
 * @since 1.0
 */
public class PermissionsGroup {

    // Constants -----------------------------------------------------

    // Attributes ----------------------------------------------------

    private List<String> permissions;
    private String       clazz;
    
    // Static --------------------------------------------------------

    // Constructors --------------------------------------------------

    /**
     * Creates a group of permissions associating the specified
     * class to the specified permissions.
     * 
     * @param _clazz container class.
     * @param _permissions class permissions.
     */
    public PermissionsGroup(String _clazz, List<String> _permissions) {
        this.clazz    	 = _clazz;
        this.permissions = _permissions;
    }
    
    // Public --------------------------------------------------------

    /**
     * Gets the list of permissions required by the class
     * on this security permission.
     * 
     * @return permissions list.
     */
    public List<String> getPermissions() {
        List<String> copy = new ArrayList<String>();
        copy.addAll(this.permissions);
        return copy;
    }
    
    /**
     * Gets the class name of this security
     * permission.
     * 
     * @return class name.
     */
    public String getClassName() {
        return this.clazz;
    }
    
    // X implementation ----------------------------------------------

    // Y overrides ---------------------------------------------------

    // Package protected ---------------------------------------------

    // Protected -----------------------------------------------------

    // Private -------------------------------------------------------
}
