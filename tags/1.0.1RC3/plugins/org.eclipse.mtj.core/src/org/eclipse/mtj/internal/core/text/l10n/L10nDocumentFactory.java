/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Renato Franca (Motorola) - Fixing ownership of internal XML tags [Bug 285589].
 */
package org.eclipse.mtj.internal.core.text.l10n;

import org.eclipse.mtj.internal.core.text.DocumentNodeFactory;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.IDocumentNodeFactory;

/**
 * @since 0.9.1
 */
public class L10nDocumentFactory extends DocumentNodeFactory implements
        IDocumentNodeFactory {

    private L10nModel fModel;

    /**
     * @param model
     */
    public L10nDocumentFactory(L10nModel model) {
        fModel = model;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.DocumentNodeFactory#createDocumentNode(java.lang.String, org.eclipse.mtj.internal.core.text.IDocumentElementNode)
     */
    @Override
    public IDocumentElementNode createDocumentNode(String name,
            IDocumentElementNode parent) {

        if (isLocales(name)) { // L10nLocales Root
            return createL10nLocales();
        }

        if (isEntry(name)) { // Entry
            return createL10nEntry(parent);
        }

        if (isLocale(name)) { // Locale
            return createL10nLocale(parent);
        }

        return super.createDocumentNode(name, parent);
    }

    /**
     * 
     * @param parent The node parent
     * @return A new Entry instance
     */
    public L10nEntry createL10nEntry(IDocumentElementNode parent) {
        L10nEntry entry = null;
        
        try {
            entry = fModel.getLocales().getLocale(parent).createEntry();
        } catch (Exception e) {
            //Do nothing
        }
        
        return entry;
    }

    /**
     * 
     * @param parent The node parent
     * @return A new Locale instance
     */
    public L10nLocale createL10nLocale(IDocumentElementNode parent) {
        return fModel.getLocales().createlLocale();
    }

    /**
     * @return A new Locales instance
     */
    public L10nLocales createL10nLocales() {
        return new L10nLocales(fModel);
    }

    /**
     * @param name The name entry
     * @return True, if it's an entry tag; false otherwise
     */
    private boolean isEntry(String name) {
        return isL10nElement(name, IL10nConstants.ELEMENT_ENTRY);
    }

    /*
     * Check if it's a L10n element
     */
    private boolean isL10nElement(String name, String elementName) {
        if (name.equals(elementName)) {
            return true;
        }
        return false;
    }

    /*
     * Check if it's a Locale
     */
    private boolean isLocale(String name) {
        return isL10nElement(name, IL10nConstants.ELEMENT_LOCALE);
    }

    /*
     * Check if it's a Locales
     */
    private boolean isLocales(String name) {
        return isL10nElement(name, IL10nConstants.ELEMENT_LOCALES);
    }
}
