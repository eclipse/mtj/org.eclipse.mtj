/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.sdk.device;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public enum JavaEmulatorDeviceProperties {

    /**
     * 
     */
    CLASSPATH("classpath"), //$NON-NLS-1$

    /**
     * 
     */
    DEBUG_SERVER("debug.server"), //$NON-NLS-1$

    /**
     * 
     */
    PREDEPLOY_REQUIRED("predeploy.required"), //$NON-NLS-1$

    /**
     * 
     */
    LAUNCH_COMMAND("launch.command"); //$NON-NLS-1$

    private String property;

    /**
     * @param property the property name
     */
    JavaEmulatorDeviceProperties(final String property) {
        this.property = property;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return property;
    }
}
