/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.templates;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * ImageDownloaderWizardPage provides a template that creates an 
 * application that downloads images from the Internet.
 * 
 * @author David Marques
 * @since 1.0
 */
public class ImageDownloaderWizardPage extends AbstractTemplateWizardPage {

    private Text urlText;

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        GridData data = null;
        parent.setLayout(new GridLayout(0x01, false));
        
        Group group = new Group(parent, SWT.NONE);
        group.setText(Messages.ImageDownloadTemplateProvider_0);
        
        data = new GridData(SWT.FILL, SWT.FILL, true, false);
        group.setLayoutData(data);
        group.setLayout(new GridLayout(2, false));
        
        Label urlLabel = new Label(group, SWT.NONE);
        urlLabel.setText(Messages.ImageDownloadTemplateProvider_1);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        urlLabel.setLayoutData(data);
        
        urlText = new Text(group, SWT.BORDER);
        urlText.setText("http://www.eclipse.org/images/egg-incubation.png"); //$NON-NLS-1$
        
        urlText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        urlText.setLayoutData(data);        
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.ITemplateProvider#getDictionary()
     */
    public Map<String, String> getDictionary() {
        Map<String, String> dictionary = new HashMap<String, String>();
        dictionary.put("$image-url$", this.urlText.getText()); //$NON-NLS-1$
        return dictionary;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.ITemplateProvider#isPageComplete()
     */
    public boolean isPageComplete() {
        boolean result = Pattern.matches("http://.+", this.urlText.getText()); //$NON-NLS-1$
        return result;
    }
}
