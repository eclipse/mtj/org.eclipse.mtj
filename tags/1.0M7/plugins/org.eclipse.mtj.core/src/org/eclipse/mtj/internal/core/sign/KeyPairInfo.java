/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.sign;

import org.eclipse.osgi.util.NLS;

/**
 * KeyPairInfo Class wraps key pair and it's certificate
 * informations.
 * 
 * @author David Marques
 * @since 1.0
 */
public class KeyPairInfo {
	
	private String alias;
	private String password;
	
	/*Parameter : CN*/
	private String commonName;
	
	/*Parameter : OU*/
	private String organizationUnit;
	
	/*Parameter : O*/
	private String organizationName;
	
	/*Parameter : L*/
	private String localityName;
	
	/*Parameter : S*/
	private String stateName;
	
	/*Parameter : C*/
	private String country;
	
	/**
	 * Creates a KeyPairInfo instance for the specified
	 * alias and password.
	 * 
	 * @param alias key pair alias.
	 * @param password key pair password.
	 */
	public KeyPairInfo(String alias, String password) {
		if (alias == null) {
			throw new IllegalArgumentException("Alias can not be null.");
		}
		
		/*Password length must be > 0x06*/
		if (password == null || password.length() < 0x06) {
			throw new IllegalArgumentException("Password can not be null.");
		}
		
		this.alias    = alias;
		this.password = password;
	}

	/**
	 * Returns the DNAME on the following format:
	 * "CN={0}, OU={1}, O={2}, L={3}, S={4}, C={5}"
	 * 
	 * @return formatted information.
	 */
	public String toString() {
		String template = "CN={0}, OU={1}, O={2}, L={3}, S={4}, C={5}";
		return NLS.bind(template, new String[]{
			this.commonName, this.organizationUnit, this.organizationName,
				this.localityName, this.stateName, this.country
		});
	}
	
	/**
	 * @return the commonName
	 */
	public String getCommonName() {
		return commonName;
	}

	/**
	 * @param commonName the commonName to set
	 */
	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	/**
	 * @return the organizationUnit
	 */
	public String getOrganizationUnit() {
		return organizationUnit;
	}

	/**
	 * @param organizationUnit the organizationUnit to set
	 */
	public void setOrganizationUnit(String organizationUnit) {
		this.organizationUnit = organizationUnit;
	}

	/**
	 * @return the organizationName
	 */
	public String getOrganizationName() {
		return organizationName;
	}

	/**
	 * @param organizationName the organizationName to set
	 */
	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	/**
	 * @return the localityName
	 */
	public String getLocalityName() {
		return localityName;
	}

	/**
	 * @param localityName the localityName to set
	 */
	public void setLocalityName(String localityName) {
		this.localityName = localityName;
	}

	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}

	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
}
