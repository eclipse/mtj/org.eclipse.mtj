/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.osgi.framework.Version;

/**
 * This interface represents an SDK. It can be used to implement a automatic SDK
 * install on MTJ.
 * 
 * @since 1.0
 */
public interface ISDK {

    /**
     * Return the displayable description of this SDK. This description will be
     * displayed within the user interface. If this method returns a
     * <code>null</code> value, the SDK's name will be used as the description
     * instead.
     * 
     * @return the description of this SDK or <code>null</code> if the SDK's
     *         name should be used instead.
     */
    public abstract String getDescription();

    /**
     * Return the fully configured device instances available in this SDK.
     * 
     * @return the list of devices in this SDK
     * @throws CoreException thrown if an error occur while retrieving the
     *             device list.
     */
    public abstract List<IDevice> getDeviceList() throws CoreException;

    /**
     * Return the name of this SDK. This name will be displayed within the user
     * interface and must never be <code>null</code>.
     * 
     * @return the name of this SDK.
     */
    public abstract String getName();

    /**
     * Return the version of this SDK. This version will be displayed within the
     * user interface and must never be <code>null</code>.
     * 
     * @return the version of this SDK.
     */
    public abstract Version getVersion();

}