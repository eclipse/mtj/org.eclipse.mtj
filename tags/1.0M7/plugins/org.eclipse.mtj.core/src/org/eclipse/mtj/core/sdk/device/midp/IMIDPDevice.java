/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Diego Sandin (Motorola) - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.sdk.device.midp;

import java.io.File;

import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceRegistry;

/**
 * The device interface specifies the representation of an emulated MIDP device.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface IMIDPDevice extends IDevice {

    /**
     * Return the configured Configuration API for the device instance.
     * 
     * @return the configured Configuration API for the device instance or
     *         <code>null</code> if no Configuration was set.
     */
    public abstract IMIDPAPI getCLDCAPI();

    /**
     * Return the preverifier to be used to preverify classes for running in
     * this device instance.
     * <p>
     * If no preverifier was specified for this device, MTJ will try to retrieve
     * an IPreverifier instance using the
     * {@link IDeviceRegistry#getDefaultPreferifier()} to be used when
     * preverifing classes for running on this device.
     * </p>
     * 
     * 
     * @return the preverifier configured for this device or <code>null</code>
     *         if preverifier was previously set.
     */
    public abstract IPreverifier getPreverifier();

    /**
     * Return the configured Profile API for the device instance.
     * 
     * @return the configured Profile API for the device instance or
     *         <code>null</code> if no Profile was set.
     */
    public abstract IMIDPAPI getMIDPAPI();

    /**
     * Return the list of protection domains specified by this device. Returning
     * <code>null</code> from this method will imply that this device does not
     * support protection domains.
     * 
     * @return the list of protection domains or <code>null</code> if the device
     *         does not provide any protection domains.
     */
    public abstract String[] getProtectionDomains();

    /**
     * Return the working directory to be used when launching the device
     * emulation.
     * 
     * @return working directory to be used or <code>null</code> if no working
     *         directory is necessary.
     */
    public File getWorkingDirectory();

}