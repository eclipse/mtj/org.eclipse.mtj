/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)          - Initial implementation
 *     Gustavo de Paula (Motorola) - Configuration refactory [Bug 265522]
 *     Diego Sandin (Motorola)     - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.project.runtime.event;

import java.util.EventObject;
import java.util.List;

import org.eclipse.mtj.core.symbol.ISymbolSet;

/**
 * MTJRuntimeWorkspaceSymbolSetsChangeEvent is used to notify that the
 * workspaceScopeSymbolSets of the MTJRuntime changed.
 * <p>
 * All Events are constructed with a reference to the object, the "source", that
 * is logically deemed to be the object upon which the Event in question
 * initially occurred upon.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 * 
 * @since 1.0
 */
public class MTJRuntimeWorkspaceSymbolSetsChangeEvent extends EventObject {

    /**
     * The default serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The list of previous Workspace SymbolSets.
     */
    private List<ISymbolSet> oldSymbolDefinitionSets;

    /**
     * The list of newer Workspace SymbolSets.
     */
    private List<ISymbolSet> newSymbolDefinitionSets;

    /**
     * Creates a new instance of MTJRuntimeWorkspaceSymbolSetsChangeEvent.
     * 
     * @param source The MTJRuntime that has been changed.
     * @param oldSets The list of previous Workspace SymbolSets.
     * @param newSets The list of newer Workspace SymbolSets.
     */
    public MTJRuntimeWorkspaceSymbolSetsChangeEvent(Object source,
            List<ISymbolSet> oldSets, List<ISymbolSet> newSets) {
        super(source);
        this.oldSymbolDefinitionSets = oldSets;
        this.newSymbolDefinitionSets = newSets;
    }

    /**
     * Get the list of newer Workspace SymbolSets.
     * 
     * @return the list of newer Workspace SymbolSets.
     */
    public List<ISymbolSet> getNewSymbolDefinitionSets() {
        return newSymbolDefinitionSets;
    }

    /**
     * Get the list of previous Workspace SymbolSets.
     * 
     * @return the list of previous Workspace SymbolSets.
     */
    public List<ISymbolSet> getOldSymbolDefinitionSets() {
        return oldSymbolDefinitionSets;
    }

    /**
     * Set the list of newer Workspace SymbolSets.
     * 
     * @param newSymbolDefinitionSets The list of newer Workspace SymbolSets.
     */
    public void setNewSymbolDefinitionSets(
            List<ISymbolSet> newSymbolDefinitionSets) {
        this.newSymbolDefinitionSets = newSymbolDefinitionSets;
    }

    /**
     * Set the list of previous Workspace SymbolSets.
     * 
     * @param oldSymbolDefinitionSets the list of previous Workspace SymbolSets.
     */
    public void setOldSymbolDefinitionSets(
            List<ISymbolSet> oldSymbolDefinitionSets) {
        this.oldSymbolDefinitionSets = oldSymbolDefinitionSets;
    }

}
