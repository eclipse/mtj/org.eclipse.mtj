/**
 * Copyright (c) 2004, 2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.launching;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.project.IMTJProject;

/**
 * The launch environment provides the necessary information to an IDevice
 * implementation in order to determine the correct command-line for execution
 * of an emulator.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @since 1.0
 */
public class LaunchEnvironment {

    /**
     * Flag for indicating if the launch should be made in debug mode.
     */
    private boolean debugLaunch;

    /**
     * The port to be listened for debugging.
     */
    private int debugListenerPort;

    /**
     * A launch configuration that describes how to launch an emulator.
     */
    private ILaunchConfiguration launchConfiguration;

    /**
     * The project that contains the resources that may be launched.
     */
    private IMTJProject mtjProject;

    /**
     * Return the port number to be used for debugging.
     * 
     * @return the debug port number.
     */
    public int getDebugListenerPort() {
        return debugListenerPort;
    }

    /**
     * Return the launch configuration that was used to launch the emulation.
     * 
     * @return A launch configuration that describes how to launch the emulator.
     */
    public ILaunchConfiguration getLaunchConfiguration() {
        return launchConfiguration;
    }

    /**
     * Return the project that contains the resources that may be launched.
     * 
     * @return Returns the {@link IMTJProject} that resources that may be
     *         launched.
     */
    public IMTJProject getProject() {
        return mtjProject;
    }

    /**
     * Return a boolean indicating whether the launch should be done for debug
     * or not.
     * 
     * @return <code>true</code> to indicate that the launch should be made in
     *         debug mode, <code>false</code> otherwise.
     */
    public boolean isDebugLaunch() {
        return debugLaunch;
    }

    /**
     * Indicate whether the launch should be done for debug or not.
     * 
     * @param debugLaunch use <code>true</code> to indicate that the launch
     *            should be made in debug mode, <code>false</code> otherwise.
     */
    public void setDebugLaunch(boolean debugLaunch) {
        this.debugLaunch = debugLaunch;
    }

    /**
     * Set the port to be listened for debugging.
     * 
     * @param debugListenerPort the port to be listened for debugging.
     */
    public void setDebugListenerPort(int debugListenerPort) {
        this.debugListenerPort = debugListenerPort;
    }

    /**
     * Set the launch configuration that must be used to launch the emulation.
     * 
     * @param launchConfiguration The launch configuration that must be used to
     *            launch the emulation.
     */
    public void setLaunchConfiguration(ILaunchConfiguration launchConfiguration) {
        this.launchConfiguration = launchConfiguration;
    }

    /**
     * Set the project that contains the resources that may be launched.
     * 
     * @param mtjProject The {@link IMTJProject} that contains the resources
     *            that may be launched.
     */
    public void setProject(IMTJProject mtjProject) {
        this.mtjProject = mtjProject;
    }
}
