/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Diego Sandin (Motorola) - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.sdk.device;

import java.util.List;

import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.IPersistable;

/**
 * This interface represents the classpath that is associated to one specific
 * device.
 * <p>
 * The classpath has a list of ILibrary and each ILibrary is associated to a set
 * of APIs. Based on that is is possible to identify all APIs thats are support
 * on each device.
 * </p>
 * <p>
 * Clients may create new IDeviceeClasspath instances through the
 * {@link MTJCore#createNewDeviceClasspath()} method.
 * </p>
 * 
 * @see ILibrary
 * @see IAPI
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * 
 * @since 1.0
 */
public interface IDeviceClasspath extends IPersistable {

    /**
     * Add a new ILibrary to the device classpath.
     * 
     * @param classpathEntry the ILibrary to be included in the classpath.
     */
    public abstract void addEntry(ILibrary classpathEntry);

    /**
     * Return the device classpath as a list of {@link IClasspathEntry} entries.
     * 
     * @return the device classpath as a list of {@link IClasspathEntry}
     *         entries.
     */
    public abstract List<IClasspathEntry> asClasspathEntries();

    /**
     * Test the equality of this class with another deviceClasspath.
     * 
     * @param deviceClasspath the reference IDeviceClasspath object with which
     *            to compare.
     * @return <code>true</code> if this IDeviceClasspath object is the same as
     *         the deviceClasspath argument; <code>false</code> otherwise.
     */
    public abstract boolean equals(IDeviceClasspath deviceClasspath);

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public abstract boolean equals(Object obj);

    /**
     * Return the list of ILibrary entries in the device classpath.
     * 
     * @return the list of ILibrary entries in the device classpath.
     */
    public abstract List<ILibrary> getEntries();

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public abstract int hashCode();

    /**
     * Removes a single instance of the specified ILibrary element from this
     * list, if it is present.
     * 
     * @param library ILibrary element to be removed from this list, if present.
     */
    public abstract void removeEntry(ILibrary library);

    /**
     * 
     * Converts the device classpath to a string with platform-dependent path
     * separator characters.
     * 
     * 
     * @see java.lang.Object#toString()
     */
    public abstract String toString();

}