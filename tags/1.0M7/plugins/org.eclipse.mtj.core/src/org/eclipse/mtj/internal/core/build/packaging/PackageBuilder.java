/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Fixing path problems.
 *     David Marques (Motorola) - Fixing project dependencies support.
 */
package org.eclipse.mtj.internal.core.build.packaging;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.IStreamListener;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IStreamMonitor;
import org.eclipse.debug.core.model.IStreamsProxy;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.MTJBuildState;
import org.eclipse.mtj.core.build.preverifier.IPreverificationError;
import org.eclipse.mtj.core.build.preverifier.PreverifierNotFoundException;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.midp.IApplicationDescriptor;
import org.eclipse.mtj.core.project.midp.IMidletDefinition;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.PreferenceAccessor;
import org.eclipse.mtj.internal.core.build.BuildStateMachine;
import org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder;
import org.eclipse.mtj.internal.core.build.preverifier.PreverificationUtils;
import org.eclipse.mtj.internal.core.build.preverifier.builder.BuilderMessages;
import org.eclipse.mtj.internal.core.packaging.midp.DeployedJADWriter;
import org.eclipse.mtj.internal.core.packaging.midp.ObfuscatorTool;
import org.eclipse.mtj.internal.core.project.midp.ApplicationDescriptor;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.util.ColonDelimitedProperties;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.RequiredProjectsCPEntryVisitor;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.osgi.framework.Version;

import de.schlichtherle.io.ArchiveException;
import de.schlichtherle.io.File;
import de.schlichtherle.io.FileOutputStream;

/**
 * PackageBuilder class implements a package builder in order to build both
 * runtime and deployment JARs. <br>
 * For deployment JARs there are options to sing and/or obfuscate. To enable
 * these options set the builder's command attributes.
 * 
 * @author David Marques
 */
public class PackageBuilder extends MTJIncrementalProjectBuilder {

    public static final String ARG_DO_OBFUSCATION = "_do_obfuscation"; //$NON-NLS-1$
    public static final String ARG_DO_PACKAGE = "_do_package"; //$NON-NLS-1$
    public static final String ARG_UPDATE_VERSION = "_update_version"; //$NON-NLS-1$

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#doBuild
     * (int, java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
     */
    protected IProject[] doBuild(int kind, Map<?, ?> args,
            IProgressMonitor monitor) throws CoreException {
        IMTJProject mtjProject = getMTJProject();
        if (mtjProject instanceof IMidletSuiteProject) {
            IMidletSuiteProject suiteProject = (IMidletSuiteProject) mtjProject;
            updatePackage(suiteProject, kind, args, monitor);
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#doClean
     * (int, org.eclipse.core.runtime.IProgressMonitor)
     */
    protected void doClean(int kind, IProgressMonitor monitor)
            throws CoreException {
        monitor.subTask(BuilderMessages.PackageBuilder_cleaningRuntimeFolder);
        IProject project = getProject();
        IFolder folder = getRuntimeFolder(project);
        if (folder.exists()) {
            IResource[] children = folder.members();
            for (IResource resource : children) {
                resource.delete(true, monitor);
            }
            folder.refreshLocal(IResource.DEPTH_ZERO, monitor);
        }

        IFolder deployed = getProject().getFolder("deployed"); //$NON-NLS-1$
        if (deployed.exists()) {
            IResource[] children = deployed.members();
            monitor.subTask(BuilderMessages.PackageBuilder_cleaningDeployedFolder);
            for (IResource resource : children) {
                if (resource.getType() == IResource.FOLDER) {
                    if (kind == CLEAN_BUILD) {
                        resource.delete(true, monitor);
                    }
                } else {
                    resource.delete(true, monitor);
                }
            }
            deployed.refreshLocal(IResource.DEPTH_ZERO, monitor);
        }
    }

    /**
     * Updates/Creates the runtime JAR content.
     * 
     * @param suiteProject target {@link IMidletSuiteProject} instance
     * @param kind build kind
     * @param args build arguments
     * @param monitor progress monitor.
     * @throws CoreException Any build error occurs.
     */
    private void updatePackage(IMidletSuiteProject suiteProject, int kind,
            Map<?, ?> args, IProgressMonitor monitor) throws CoreException {
        IProject project = suiteProject.getProject();
        IFolder runtime = getRuntimeFolder(project);
        // Remove configuration old deployment folder.
        if (!runtime.exists()) {
            runtime.create(true, true, monitor);
        }

        try {
            // Update the manifest version if specified
            boolean updateVersionForProject = PreferenceAccessor.instance
                    .getAutoversionPackage(suiteProject.getProject())
                    && isBuildArgumentTrue(args, ARG_UPDATE_VERSION);
            if (updateVersionForProject) {
                monitor.subTask(BuilderMessages.PackageBuilder_updatingJadVersion);
                IFile jad = suiteProject.getApplicationDescriptorFile();
                this.updateJADVersion(jad, monitor);
            }
        } catch (IOException e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, 999, e);
        }

        IFile jarFie = runtime.getFile(suiteProject.getJarFilename());
        File zipFile = new File(jarFie.getLocation().toOSString());

        monitor.subTask(BuilderMessages.PackageBuilder_collectingSources);
        PackageBuilderVisitor visitor = new PackageBuilderVisitor(suiteProject,
                zipFile);
        IResourceDelta delta = getDelta(getProject());
        if (delta == null) {
            IFolder outputFolder = getOutputFolder(suiteProject
                    .getJavaProject());
            if (outputFolder.exists()) {
                outputFolder.accept(visitor);
            }
        } else {
            delta.accept((IResourceDeltaVisitor) visitor);
        }

        monitor.subTask(BuilderMessages.PackageBuilder_packagingResources);
        for (IFile resource : visitor.getResourcesToPackage()) {
            this.addFileToJAR(zipFile, resource, getResourceZipPath(
                    suiteProject, resource));
        }

        monitor.subTask(BuilderMessages.PackageBuilder_packagingClasses);
        for (IFile clazz : visitor.getClassesToPackage()) {
            this.addFileToJAR(zipFile, clazz, getClassZipPath(suiteProject,
                    clazz));
        }

        monitor.subTask(BuilderMessages.PackageBuilder_packagingLibraries);
        this.addPreverifiedLibraries(suiteProject, zipFile, monitor);

        monitor.subTask(BuilderMessages.PackageBuilder_packagingDependencies);
        this.addRequiredProjectsClasses(suiteProject, zipFile, monitor);
        this.addRequiredProjectsLibs(suiteProject, zipFile, monitor);
        this.addRequiredProjectsResources(suiteProject, zipFile, monitor);

        // Create MANIFEST.MF at last because other JAR files may have their
        // manifests copied into the app JAR.
        this.createManifestFile(suiteProject, zipFile, monitor);
        try {
            File.umount();

            java.io.File jar = new java.io.File(zipFile.getAbsolutePath());
            if (isBuildArgumentTrue(args, ARG_DO_OBFUSCATION)) {
                monitor.subTask(BuilderMessages.PackageBuilder_obfuscating);
                doObfuscation(suiteProject, jar, monitor);
            }

            DeployedJADWriter writer = new DeployedJADWriter(suiteProject,
                    runtime, jar);
            writer.writeDeployedJAD(true, monitor);
            if (isBuildArgumentTrue(args, ARG_DO_PACKAGE)) {
                monitor.subTask(BuilderMessages.PackageBuilder_generatingDeploymentPackage);
                createDeployedPackage(suiteProject, zipFile, args, monitor);
            }
        } catch (ArchiveException e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, 999, e);
        } catch (IOException e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, 999, e);
        } finally {
            runtime.refreshLocal(IResource.DEPTH_ONE, monitor);
        }
    }

    /**
     * Adds all resources from the required projects output
     * folder.
     * 
     * @param suiteProject target suite project
     * @param zipFile runtime JAR file
     * @param monitor progress monitor
     * @throws CoreException Any core error occurs
     */
    private void addRequiredProjectsResources(IMidletSuiteProject suiteProject,
            File zipFile, IProgressMonitor monitor) throws CoreException {

        List<IFile> resources = new ArrayList<IFile>();
        RequiredProjectsCPEntryVisitor visitor = new RequiredProjectsCPEntryVisitor();
        visitor.getRunner().run(suiteProject.getJavaProject(), visitor, monitor);
        List<IJavaProject> required = visitor.getRequiredProjects();
        for (IJavaProject javaProject : required) {
            IProject project = javaProject.getProject();
            IPath binaryPath = javaProject.getOutputLocation()
                    .removeFirstSegments(project.getFullPath().segmentCount());
            IFolder folder = project.getFolder(binaryPath);
            if (folder.exists()) {
                ResourcesCollector collector = new ResourcesCollector(
                        javaProject);
                folder.accept(collector);
                resources.addAll(collector.getResources());
            }
        }

        for (IFile resource : resources) {
            this.addRequiredResource(resource, zipFile);
        }
    }

    /**
     * Adds the required resource into the runtime JAR file.
     * 
     * @param resource target resource
     * @param zipFile runtime JAR.
     * @throws CoreException Any core error occurs.
     */
    private void addRequiredResource(IFile resource, File zipFile)
            throws CoreException {
        IJavaProject javaProject = JavaCore.create(resource.getProject());
        IPath binaryPath = javaProject.getOutputLocation();
        if (binaryPath.isPrefixOf(resource.getFullPath())) {
            IPath zipPath = resource.getFullPath().removeFirstSegments(
                    binaryPath.segmentCount());
            if (!zipPath.isEmpty()) {
                this.addResourceToJAR(zipFile, resource, zipPath);
            }
        }
    }

    /**
     * Adds all class files from the required projects into
     * the runtime JAR file.
     * 
     * @param suiteProject target suite project
     * @param zipFile runtime JAR file
     * @param monitor progress monitor
     * @throws CoreException Any core error occurs
     */
    private void addRequiredProjectsClasses(IMidletSuiteProject suiteProject,
            File zipFile, IProgressMonitor monitor) throws CoreException {
        IFolder requiredClassesFolder = suiteProject.getVerifiedOutputFolder(
                monitor).getFolder(IMTJCoreConstants.REQUIRED_CLASSES_FOLDER); //$NON-NLS-1$
        if (requiredClassesFolder.exists()) {
            for (IResource resource : requiredClassesFolder.members()) {
                this.addResourceToJAR(zipFile, resource,
                        getRequiredClassesZipPath(suiteProject, resource));
            }
        }
    }

    /**
     * Adds all libraries from the required projects.
     * 
     * @param suiteProject target suite project
     * @param zipFile runtime JAR file
     * @param monitor progress monitor
     * @throws CoreException Any core error occurs
     */
    private void addRequiredProjectsLibs(IMidletSuiteProject suiteProject,
            File zipFile, IProgressMonitor monitor) throws CoreException {
        IFolder requiredLibsFolder = suiteProject.getVerifiedOutputFolder(
                monitor).getFolder(IMTJCoreConstants.REQUIRED_LIBS_FOLDER); //$NON-NLS-1$
        if (requiredLibsFolder.exists()) {
            for (IResource resource : requiredLibsFolder.members()) {
                if (!resource.getName().endsWith(".jar")) { //$NON-NLS-1$
                    continue;
                }
                zipFile.archiveCopyAllFrom(new File(resource.getLocation()
                        .toOSString()));
            }
        }
    }

    /**
     * Creates both JAD/JAR files on the deployment package.
     * 
     * @param suiteProject target {@link IMidletSuiteProject} instance
     * @param jar
     * @param args build arguments
     * @param monitor progress monitor.
     * @throws CoreException Any build error occurs.
     */
    private void createDeployedPackage(IMidletSuiteProject suiteProject,
            File jar, Map<?, ?> args, IProgressMonitor monitor)
            throws CoreException {
        MTJRuntimeList runtime = suiteProject.getRuntimeList();
        MTJRuntime active = runtime.getActiveMTJRuntime();
        if (active == null) {
            return;
        }

        IFolder deployed = getProject().getFolder("deployed"); //$NON-NLS-1$
        if (!deployed.exists()) {
            deployed.create(true, true, monitor);
        }

        IFolder target = deployed.getFolder(active.getName());
        if (target.exists()) {
            target.delete(true, monitor);
        }
        target.create(true, true, monitor);

        try {
            File deployedJar = new File(target.getLocation().toFile(), jar
                    .getName());
            jar.archiveCopyAllTo(deployedJar);
            File.umount();

            DeployedJADWriter writer = new DeployedJADWriter(suiteProject,
                    target, new java.io.File(deployedJar.getAbsolutePath()));
            writer.writeDeployedJAD(false, monitor);
        } catch (IOException e) {
            MTJStatusHandler.throwCoreException(Status.ERROR, 999, e);
        } finally {
            deployed.refreshLocal(IResource.DEPTH_INFINITE, monitor);
        }
    }

    /**
     * Checks whether the argument is true.
     * 
     * @param args arguments map.
     * @param argument target argument.
     * @return true or false.
     */
    private boolean isBuildArgumentTrue(Map<?, ?> args, String argument) {
        String value = (String) args.get(argument);
        if (value != null && Boolean.valueOf(value)) {
            return true;
        }
        return false;
    }

    /**
     * Update the JAD version in the manifest properties.
     * 
     * @throws IOException
     * @throws CoreException
     */
    private void updateJADVersion(IFile jad, IProgressMonitor monitor)
            throws CoreException, IOException {
        // Read the source jad file and update the jar file
        // length property.
        IApplicationDescriptor appDescriptor = new ApplicationDescriptor(jad
                .getLocation().toFile());
        ColonDelimitedProperties jadProperties = (ColonDelimitedProperties) appDescriptor
                .getManifestProperties();

        // Calculate the updated version string
        String versionString = jadProperties.getProperty(
                IJADConstants.JAD_MIDLET_VERSION, "0.0.0"); //$NON-NLS-1$
        Version version = new Version(versionString);

        int major = version.getMajor();
        int minor = version.getMinor();
        int secondary = version.getMicro();

        if (secondary >= 99) {
            secondary = 0;
            minor++;
        } else {
            secondary++;
        }

        StringBuffer newVersion = new StringBuffer();
        newVersion.append(major).append(".").append(minor).append(".").append( //$NON-NLS-1$ //$NON-NLS-2$
                secondary);

        // Update the JAD
        jadProperties.setProperty(IJADConstants.JAD_MIDLET_VERSION, newVersion
                .toString());

        try {
            appDescriptor.store();
        } catch (IOException e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, 999, e);
        } finally {
            jad.refreshLocal(IResource.DEPTH_ZERO, monitor);
        }
    }

    /**
     * Adds the specified {@link IMidletSuiteProject} instance's verified
     * libraries into the JAR file.
     * 
     * @param suiteProject {@link IMidletSuiteProject} instance.
     * @param zipFile target JAR file.
     * @param monitor progress monitor.
     * @throws CoreException Any build error occurs.
     */
    private void addPreverifiedLibraries(IMidletSuiteProject suiteProject,
            File zipFile, IProgressMonitor monitor) throws CoreException {
        IFolder folder = suiteProject.getVerifiedLibrariesOutputFolder(monitor);
        if (!folder.exists()) {
            return;
        }
        IResource[] libraries = folder.members();
        for (IResource library : libraries) {
            if (!library.getName().endsWith(".jar")) { //$NON-NLS-1$
                continue;
            }
            zipFile.archiveCopyAllFrom(new File(library.getLocation()
                    .toOSString()));
        }
    }

    /**
     * Generate a MANIFEST.MF file into the deployed folder based on the current
     * information in the JAD file.
     * 
     * @param midletSuite
     * @param monitor progress monitor.
     * @throws CoreException Any build error occurs.
     */
    private void createManifestFile(IMidletSuiteProject midletSuite, File jar,
            IProgressMonitor monitor) throws CoreException {

        IProject project = midletSuite.getProject();
        if (!midletSuite.getApplicationDescriptorFile().exists()) {
            return;
        }

        IApplicationDescriptor applicationDescriptor = midletSuite
                .getApplicationDescriptor();
        Properties manifestProperties = applicationDescriptor
                .getManifestProperties();

        String[] excluded = PreferenceAccessor.instance
                .getExcludedManifestProperties(project);
        for (String excludedName : excluded) {
            if (manifestProperties.containsKey(excludedName)) {
                manifestProperties.remove(excludedName);
            }
        }

        Manifest jarManifest = new Manifest();
        Attributes mainAttributes = jarManifest.getMainAttributes();
        mainAttributes.putValue(Attributes.Name.MANIFEST_VERSION.toString(),
                BuilderMessages.PreverificationBuilder_13);

        Iterator<Map.Entry<Object, Object>> iterator = manifestProperties
                .entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Object, Object> entry = iterator.next();
            try {
                mainAttributes.putValue((String) entry.getKey(), (String) entry
                        .getValue());
            } catch (IllegalArgumentException e) {
                MTJStatusHandler.throwCoreException(IStatus.ERROR, -999, e);
            }
        }

        Iterator<IMidletDefinition> iter = applicationDescriptor
                .getMidletDefinitions().iterator();
        while (iter.hasNext()) {
            IMidletDefinition def = iter.next();

            String key = ApplicationDescriptor.MIDLET_PREFIX + def.getNumber();
            String value = def.toString();
            mainAttributes.putValue(key, value);
        }

        FileOutputStream fos = null;
        File manifestFolder = new File(jar, "META-INF"); //$NON-NLS-1$
        if (!manifestFolder.exists()) {
            manifestFolder.mkdir();
        }
        try {
            fos = new FileOutputStream(new File(manifestFolder, "MANIFEST.MF")); //$NON-NLS-1$
            jarManifest.write(fos);
        } catch (IOException e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, -999, e);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * @seeorg.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#
     * getEnterState()
     */
    protected MTJBuildState getEnterState() {
        return MTJBuildState.PRE_PACKAGING;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#getExitState
     * ()
     */
    protected MTJBuildState getExitState() {
        return MTJBuildState.POST_PACKAGING;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#getBuilderId
     * ()
     */
    protected String getBuilderId() {
        return IMTJCoreConstants.PACKAGE_BUILDER_ID;
    }

    /**
     * Do the work to obfuscate the jar file.
     * 
     * @param deployedJarFile
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    private void doObfuscation(IMidletSuiteProject suiteProject,
            java.io.File jar, IProgressMonitor monitor) throws CoreException {
        final StringBuffer errorText = new StringBuffer();

        BuildStateMachine stateMachine = BuildStateMachine
                .getInstance(getMTJProject());
        stateMachine.changeState(MTJBuildState.PRE_OBFUSCATION, monitor);

        // Calculate the name of the obfuscated jar file name...
        Path jarPath = new Path(jar.getAbsolutePath());
        String basename = jarPath.lastSegment();
        basename = basename.substring(0, basename.length() - 4);
        // Making an assumption of .jar file extension

        IFolder runtimeFolder = getRuntimeFolder(suiteProject.getProject());
        IFile obfuscatedJarFile = runtimeFolder.getFile(basename + "_obf.jar"); //$NON-NLS-1$
        IFile runtimeJarFile = runtimeFolder.getFile(jarPath.lastSegment());

        ObfuscatorTool obfuscator = new ObfuscatorTool(suiteProject, jar,
                obfuscatedJarFile.getLocation().toFile());
        ILaunch launch = obfuscator.launch(monitor);

        // Snag any error output that might occur
        final StringBuffer stdoutBuffer = new StringBuffer();
        IProcess[] processes = launch.getProcesses();
        if ((processes != null) && (processes.length > 0)) {
            IProcess process = processes[0];
            IStreamsProxy proxy = process.getStreamsProxy();
            proxy.getErrorStreamMonitor().addListener(new IStreamListener() {
                public void streamAppended(String text, IStreamMonitor monitor) {
                    errorText.append(text);
                }
            });

            // Wait until completion
            while ((!monitor.isCanceled()) && (!process.isTerminated())) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                ;
            }

            // Log the stdout if requested
            if (stdoutBuffer.length() > 0) {
                MTJLogger.log(IStatus.INFO, stdoutBuffer.toString());
            }

            // Let the user know that something went wrong if necessary
            boolean doFinalPreverify = true;
            if (errorText.length() > 0) {
                String text = errorText.toString();
                IStatus status = new Status(IStatus.ERROR,
                        IMTJCoreConstants.PLUGIN_ID,
                        IMTJCoreConstants.ERR_OBFUSCATION_ERRORS, text, null);

                Boolean response = (Boolean) MTJStatusHandler.statusPrompt(
                        status, this);
                doFinalPreverify = (response != null) ? response.booleanValue()
                        : false;
            }

            if (doFinalPreverify) {
                try {
                    doPostObfuscationPreverification(suiteProject,
                            obfuscatedJarFile, runtimeJarFile, monitor);
                } catch (PreverifierNotFoundException e) {
                    MTJStatusHandler.throwCoreException(IStatus.ERROR, 999, e);
                }
            }
        }
        stateMachine.changeState(MTJBuildState.POST_OBFUSCATION, monitor);
    }

    private IFolder getRuntimeFolder(IProject project) {
        return project.getFolder(".mtj.tmp/emulation"); //$NON-NLS-1$
    }

    /**
     * Do the preverification necessary after obfuscation occurs.
     * 
     * @param obfuscatedJar
     * @param deployedJarFile
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     * @throws IOException
     */
    private void doPostObfuscationPreverification(
            IMidletSuiteProject suiteProject, IFile obfuscatedJar,
            IFile deployedJarFile, IProgressMonitor monitor)
            throws CoreException, PreverifierNotFoundException {

        try {
            // Create a temporary directory to handle the preverification
            // output
            IFolder deployFolder = (IFolder) obfuscatedJar.getParent();
            IFolder tempFolder = deployFolder.getFolder("temp"); //$NON-NLS-1$
            if (!tempFolder.exists()) {
                tempFolder.create(true, true, monitor);
            }

            // Preverify the jar file into the temp directory
            File jarFile = new File(obfuscatedJar.getLocation().toFile());
            IPreverificationError[] errors = suiteProject.preverifyJarFile(
                    jarFile, tempFolder, monitor);
            tempFolder.refreshLocal(IResource.DEPTH_ONE, monitor);

            // Check for errors
            if (errors.length > 0) {
                handlePreverificationErrors(errors);
            }

            // Copy the result back into the deployment directory
            IFile preverified = tempFolder.getFile(obfuscatedJar.getName());
            InputStream stream = new FileInputStream(preverified.getLocation()
                    .toFile());
            deployedJarFile.create(stream, true, monitor);
            preverified.delete(true, monitor);
            tempFolder.delete(true, monitor);
        } catch (IOException e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, 999, e);
        }
    }

    /**
     * Handle preverification errors that were encountered while obfuscating.
     * 
     * @param errors
     * @throws CoreException
     */
    private void handlePreverificationErrors(IPreverificationError[] errors)
            throws CoreException {
        StringBuffer sb = new StringBuffer(
                BuilderMessages.PreverificationBuilder_45);
        for (int i = 0; i < errors.length; i++) {
            if (i != 0) {
                sb.append("\n"); //$NON-NLS-1$
            }
            sb.append(PreverificationUtils.getErrorText(errors[i]));
        }

        MTJStatusHandler.throwCoreException(IStatus.ERROR, -999, sb.toString());
    }

    /**
     * Adds the specified resource to the JAR file into the specified
     * path.
     * 
     * @param jar target JAR file
     * @param resource resource
     * @param zipPath jar path
     */
    private void addResourceToJAR(File jar, IResource resource, IPath zipPath) {
        IPath location = resource.getLocation();
        File zipFile = new File(jar, zipPath.toString());
        zipFile.copyAllFrom(location.toFile());
    }

    /**
     * Adds the specified file to the JAR file into the specified
     * path.
     * 
     * @param jar target JAR file
     * @param file file
     * @param zipPath jar path
     */
    private void addFileToJAR(File jar, IFile file, IPath zipPath) {
        IPath location = file.getLocation();
        File zipFile = new File(jar, zipPath.toString());
        zipFile.copyFrom(location.toFile());
    }

    /**
     * Return the File instance for the runtime jar file in the specified
     * project.
     * 
     * @param project
     * @return
     */
    public static File getRuntimeJar(IProject project, IProgressMonitor monitor)
            throws CoreException {

        IFolder tempFolder = project
                .getFolder(IMTJCoreConstants.TEMP_FOLDER_NAME);
        IFolder runtimeFolder = tempFolder
                .getFolder(IMTJCoreConstants.EMULATION_FOLDER_NAME);
        createFolders(runtimeFolder, monitor);

        IJavaProject javaProject = JavaCore.create(project);
        IMidletSuiteProject midletSuite = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);

        File runtimeJar = new File(runtimeFolder.getLocation().toFile(),
                midletSuite.getJarFilename());
        runtimeJar.mkdir();
        runtimeFolder.refreshLocal(IResource.DEPTH_ONE, monitor);
        return runtimeJar;
    }

    /**
     * Create the specified folder and all parent folders as necessary.
     * 
     * @param folder
     * @param monitor
     * @throws CoreException
     */
    public static void createFolders(IFolder folder, IProgressMonitor monitor)
            throws CoreException {
        while (!folder.exists()) {
            if (folder.getParent().getType() == IResource.FOLDER) {
                createFolders((IFolder) folder.getParent(), monitor);
            }

            folder.create(true, true, monitor);
            folder.setDerived(true);
        }
    }

    /**
     * Gets the {@link IJavaProject} instance's output folder.
     * 
     * @param javaProject target project.
     * @return the output folder instance.
     * @throws JavaModelException if this element does not exist
     */
    static IFolder getOutputFolder(IJavaProject javaProject)
            throws JavaModelException {
        IWorkspaceRoot root = MTJCore.getWorkspace().getRoot();
        IPath path = javaProject.getOutputLocation();
        return root.getFolder(path);
    }

    /**
     * Gets the resource's path to be written inside the JAR file. It is the
     * same path as the output folder relative path.
     * 
     * @param suiteProject target {@link IMidletSuiteProject} instance.
     * @param file resource file.
     * @return the path on the JAR file.
     * @throws CoreException If output folder does not exist.
     */
    static IPath getResourceZipPath(IMidletSuiteProject suiteProject, IFile file)
            throws CoreException {
        IFolder outputFolder = PackageBuilder.getOutputFolder(suiteProject
                .getJavaProject());
        return file.getFullPath().removeFirstSegments(
                outputFolder.getFullPath().segmentCount());
    }

    /**
     * Gets the class' path to be written inside the JAR file. It is the same
     * path as the verified output folder relative path.
     * 
     * @param suiteProject target {@link IMidletSuiteProject} instance.
     * @param file resource file.
     * @return the path on the JAR file.
     * @throws CoreException If verified classes output folder does not exist.
     */
    static IPath getClassZipPath(IMidletSuiteProject suiteProject, IFile file)
            throws CoreException {
        IFolder verifiedFolder = suiteProject
                .getVerifiedClassesOutputFolder(new NullProgressMonitor());
        return file.getFullPath().removeFirstSegments(
                verifiedFolder.getFullPath().segmentCount());
    }

    /**
     * Gets the required-class folder relative path for the
     * specified resource.
     * 
     * @param suiteProject target project.
     * @param resource resource
     * @return the relative path.
     * @throws CoreException Any core error occurs.
     */
    static IPath getRequiredClassesZipPath(IMidletSuiteProject suiteProject,
			IResource resource) throws CoreException {
		IFolder verifiedFolder = suiteProject.getVerifiedOutputFolder(
				new NullProgressMonitor()).getFolder(
				IMTJCoreConstants.REQUIRED_CLASSES_FOLDER); //$NON-NLS-1$
		return resource.getFullPath().removeFirstSegments(
				verifiedFolder.getFullPath().segmentCount());
	}
}
