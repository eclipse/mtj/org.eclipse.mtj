/**
 * 
 */
package org.eclipse.mtj.internal.core.symbol;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;

/**
 * @author wgp010
 * 
 */
public class SymbolSet implements ISymbolSet, ISymbolSetConstants {

    private String name = "";
    private HashMap<String, ISymbol> symbols = new HashMap<String, ISymbol>();

    public SymbolSet() {
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#add(java.lang.String)
     */
    public ISymbol add(String identifier) {
        ISymbol s = new Symbol(identifier, "true");
        symbols.put(identifier, s);
        return s;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#add(java.lang.String, java.lang.String)
     */
    public ISymbol add(String identifier, String value) {
        ISymbol s = new Symbol(identifier, value);
        symbols.put(identifier, s);
        return s;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#add(java.lang.String, java.lang.String, int)
     */
    public ISymbol add(String identifier, String value, int type) {
        ISymbol s = new Symbol(identifier, value, type);
        symbols.put(identifier, s);
        return s;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#contains(java.lang.String)
     */
    public boolean contains(String identifier) {
        return symbols.containsKey(identifier);
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return (obj instanceof SymbolSet) && equals((SymbolSet) obj);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#equals(org.eclipse.mtj.core.build.preprocess.SymbolDefinitionSet)
     */
    public boolean equals(ISymbolSet definitions) {
        SymbolSet ss = (SymbolSet) definitions;

        if (!name.equals(ss.getName())) {
            return false;
        }

        for (Iterator<String> iterator = symbols.keySet().iterator(); iterator
                .hasNext();) {
            String type = iterator.next();

            if (!ss.symbols.containsKey(type)) {
                return false;
            }
        }

        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getSymbolSetString();
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return safeHash(name) ^ safeHash(symbols);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#getSymbolSetName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#getSymbolSetString()
     */
    public String getSymbolSetString() {
        Collection<ISymbol> c = symbols.values();
        StringBuffer sb = new StringBuffer();
        for (ISymbol s : c) {
            sb.append(s.getName()).append("=").append(s.getSafeValue()).append( //$NON-NLS-1$
                    ","); //$NON-NLS-1$
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#getSymbolValue(java.lang.String)
     */
    public String getSymbolValue(String identifier) {
        return symbols.get(identifier).getValue();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#getSymbols()
     */
    public Collection<ISymbol> getSymbols() {
        return symbols.values();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#remove(java.lang.String)
     */
    public void remove(String identifier) {
        symbols.remove(identifier);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#setName(java.lang.String)
     */
    public void setName(String name) {
        this.name = name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#setSymbols(java.util.Map)
     */
    public void setSymbols(Map<String, String> definitions) {
        symbols.clear();
        Iterator<String> it = definitions.keySet().iterator();

        while (it.hasNext()) {
            String identifier = it.next();
            this.add(identifier, definitions.get(identifier));
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#size()
     */
    public int size() {
        return symbols.size();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.symbol.ISymbolSet#toArray(T[])
     */
    public <T> T[] toArray(T[] a) {
        Collection<ISymbol> c = symbols.values();
        return c.toArray(a);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // Load the name of this definitions object
        name = persistenceProvider.loadString(ELEMENT_NAME_KEY);

        symbols.clear();
        String keyString = persistenceProvider.loadString(ELEMENT_KEYS_KEY
                + getStorableName());
        if ((keyString != null) && (keyString.length() > 0)) {
            String[] keys = keyString.split(","); //$NON-NLS-1$
            for (String key : keys) {
                String value = persistenceProvider.loadString(key);
                this.add(key, value);
            }
        }

        Properties properties = persistenceProvider
                .loadProperties(ELEMENT_KEYS_KEY);
        if (properties != null) {
            Iterator<?> iterator = properties.keySet().iterator();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                String value = properties.getProperty(key);
                this.add(key, value);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // Store off the name of this definitions object
        persistenceProvider.storeString(ELEMENT_NAME_KEY, name);

        // Store off the entries in the object
        Iterator<Map.Entry<String, ISymbol>> entries = symbols.entrySet()
                .iterator();
        Properties properties = new Properties();
        while (entries.hasNext()) {
            Map.Entry<String, ISymbol> entry = (Map.Entry<String, ISymbol>) entries
                    .next();

            properties.put(entry.getKey(), entry.getValue().getValue());
        }

        persistenceProvider.storeProperties(ELEMENT_KEYS_KEY, properties);
    }

    /**
     * Return the name which may be used for storage.
     * 
     * @return
     */
    String getStorableName() {
        return name.replaceAll("\\ ", "_"); //$NON-NLS-1$ //$NON-NLS-2$
    }

    /**
     * Safely gather the hashcode of the specified object.
     * 
     * @param object
     * @return
     */
    private int safeHash(Object object) {
        return (object != null) ? object.hashCode() : 0;
    }

    public void add(Collection<ISymbol> c) {
        Iterator<ISymbol> it = c.iterator();
        while (it.hasNext()) {
            ISymbol s = it.next();
            this.symbols.put(s.getName(), s);
        }
    }

    public void add(ISymbol s) {
        this.symbols.put(s.getName(), s);
    }

    public void remove(Collection<ISymbol> c) {
        Iterator<ISymbol> it = c.iterator();
        while (it.hasNext()) {
            ISymbol s = it.next();
            this.symbols.remove(s.getName());
        }

    }
}
