/**
 * Copyright (c) 2003, 2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards. Change bundleMappings values
 *                                from "eclipseme.toolkit.X" to 
 *                                "org.eclipse.mtj.toolkit.X" on
 *                                XMLPersistenceProvider constructor.
 *     Diego Sandin (Motorola)  - Fixed inconsistencies with documentation and 
 *                                enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.internal.core.persistence;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.IBundleReferencePersistable;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Bundle;
import org.osgi.framework.Version;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * A persistence provider implementation that loads and stores persistence
 * information to/from an XML file.
 * <p>
 * This is the only persistence provider available on MTJ.
 * </p>
 * 
 * @author Craig Setera
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @noinstantiate This class is not intended to be instantiated by clients.
 * 
 * @since 1.0
 */
public class XMLPersistenceProvider implements IPersistenceProvider {

    /**
     * Return a new root element for use in persistence. Use the specified
     * element name for the root element.
     * 
     * @param elementName the name to be used in the root element.
     * @return a new root element for use in persistence named as the
     *         elementName and with a version attribute retrieved from
     *         {@link MTJCore#getMTJCoreVersion()}.
     * @throws PersistenceException if fails to create the root element.
     */
    private static Element createRootElement(String elementName)
            throws PersistenceException {

        String pluginVersion = MTJCore.getMTJCoreVersion();
        Version version = new Version(pluginVersion);

        Element element = null;
        try {
            element = XMLUtils.createRootElement(elementName, version);
        } catch (ParserConfigurationException e) {
            throw new PersistenceException(e.getMessage(), e);
        }

        return element;
    }

    /**
     * Mapping from old bundle names to the bundle that now can handle those
     * instances.
     */
    private Map<String, String> bundleMappings;

    /**
     * Bundle reference stack
     */
    private Stack<Bundle> bundleStack;

    /**
     * The root element of the persisted data.
     */
    private Element element;

    /**
     * Numerical identifier for a {@link IPersistable} object.
     */
    private int identifier;

    /**
     * Map for objects that may be referenced using the
     * {@link #storeReference(String, Object)} method.
     */
    private Map<Object, Object> referenceMap;

    /**
     * Construct a new persistence provider instance given the persisted data in
     * the specified document.
     * 
     * @param document the document from which we'll extract the the root
     *            element to be used while persisting objects.
     */
    public XMLPersistenceProvider(Document document) {
        this(document.getDocumentElement());
    }

    /**
     * Construct a new persistence provider instance given the persisted data in
     * the specified root element.
     * 
     * @param rootElement the root element to be used while persisting objects.
     */
    public XMLPersistenceProvider(Element rootElement) {
        element = rootElement;
        bundleStack = new Stack<Bundle>();
        referenceMap = new HashMap<Object, Object>();

        // Set up a mapping from old bundle names to
        // the bundle that now can handle those instances
        bundleMappings = new HashMap<String, String>();
        bundleMappings.put("org.eclipse.mtj.toolkit.nokia", //$NON-NLS-1$
                "org.eclipse.mtj.toolkit.uei"); //$NON-NLS-1$
        bundleMappings.put("org.eclipse.mtj.toolkit.sonyericsson", //$NON-NLS-1$
                "org.eclipse.mtj.toolkit.uei"); //$NON-NLS-1$
        bundleMappings.put("org.eclipse.mtj.toolkit.sprint", //$NON-NLS-1$
                "org.eclipse.mtj.toolkit.uei"); //$NON-NLS-1$
        bundleMappings.put("org.eclipse.mtj.toolkit.sun", //$NON-NLS-1$
                "org.eclipse.mtj.toolkit.uei"); //$NON-NLS-1$
    }

    /**
     * Construct a new persistence provider instance that contains no persisted
     * data. Use the specified root element name for the resulting output.
     * 
     * @param rootElementName the root element to be used while persisting
     *            objects.
     * @throws PersistenceException if fails to create the root element.
     */
    public XMLPersistenceProvider(String rootElementName)
            throws PersistenceException {
        this(createRootElement(rootElementName));
    }

    /**
     * Return the document to which the persistence is being output.
     * 
     * @return the root element.
     */
    public Document getDocument() {
        return element.getOwnerDocument();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#loadBoolean(java.lang.String)
     */
    public boolean loadBoolean(String name) throws PersistenceException {
        String value = loadString(name);
        return (value == null) ? false : Boolean.TRUE.toString()
                .equalsIgnoreCase(value);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#loadInteger(java.lang.String)
     */
    public int loadInteger(String name) throws PersistenceException {
        String value = loadString(name);
        int integer = 0;
        try {
            integer = (value == null) ? 0 : Integer.parseInt(value);
        } catch (NumberFormatException e) {
            // nothing to be done, we'll return the default value anyway.
        }
        return integer;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#loadPersistable(java.lang.String)
     */
    public IPersistable loadPersistable(String name)
            throws PersistenceException {

        IPersistable persistable = null;
        Element persistableElement = getNamedElement(name);

        if (persistableElement != null) {

            boolean pushedNewBundle = pushNewBundle(persistableElement);
            persistable = createPersistableInstance(persistableElement);

            if (persistable != null) {
                element = persistableElement;

                if (persistable instanceof IBundleReferencePersistable) {
                    Bundle bundle = getCurrentBundle();
                    if (bundle != null) {
                        IBundleReferencePersistable bundlePersistable = (IBundleReferencePersistable) persistable;
                        bundlePersistable.setBundle(bundle.getSymbolicName());
                    }
                }

                persistable.loadUsing(this);

                // Make sure that the object can be picked up by reference
                String idString = persistableElement
                        .getAttribute(IPersistable.ID_PERSISTABLE_ATTRIBUTE);

                Integer id = 0;

                if (!idString.equals(Utils.EMPTY_STRING)) {

                    try {
                        id = Integer.valueOf(idString);
                    } catch (NumberFormatException nfe) {
                        MTJLogger.log(IStatus.WARNING, NLS.bind(
                                Messages.XMLPersistenceProvider_8, name), nfe);

                        Random random = new Random(System.currentTimeMillis());
                        id = random.nextInt();
                    }
                } else {
                    Random random = new Random(System.currentTimeMillis());
                    id = random.nextInt();
                }

                referenceMap.put(id, persistable);
                element = (Element) element.getParentNode();
            }

            if (pushedNewBundle) {
                if (!bundleStack.isEmpty()) {
                    bundleStack.pop();
                }
            }
        }

        return persistable;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#loadProperties(java.lang.String)
     */
    public Properties loadProperties(String name) throws PersistenceException {
        Properties properties = null;

        Element namedElement = getNamedElement(name);

        if (namedElement != null) {

            properties = new Properties();

            NodeList propertyElements = namedElement
                    .getElementsByTagName(IPersistable.PROPERTY_PERSISTABLE_ELEMENT);
            for (int i = 0; i < propertyElements.getLength(); i++) {
                Element propertyElement = (Element) propertyElements.item(i);
                String key = propertyElement
                        .getAttribute(IPersistable.KEY_PERSISTABLE_ATTRIBUTE);
                String value = propertyElement
                        .getAttribute(IPersistable.VALUE_PERSISTABLE_ATTRIBUTE);
                properties.setProperty(key, value);
            }
        }

        return properties;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#loadReference(java.lang.String)
     */
    public Object loadReference(String name) throws PersistenceException {
        Object object = null;

        Element namedElement = getNamedElement(name);
        if (namedElement != null) {
            try {
                String refidString = namedElement
                        .getAttribute(IPersistable.REFID_PERSISTABLE_ATTRIBUTE);
                Integer refid = Integer.valueOf(refidString);
                object = referenceMap.get(refid);
            } catch (NumberFormatException e) {
                // nothing to be done, we'll return the default value.
            }
        }

        return object;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#loadString(java.lang.String)
     */
    public String loadString(String name) throws PersistenceException {
        Element namedElement = getNamedElement(name);
        return (namedElement == null) ? null : namedElement
                .getAttribute(IPersistable.VALUE_PERSISTABLE_ATTRIBUTE);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#storeBoolean(java.lang.String, boolean)
     */
    public void storeBoolean(String name, boolean value)
            throws PersistenceException {
        storeString(name, Boolean.toString(value));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#storeInteger(java.lang.String, int)
     */
    public void storeInteger(String name, int value)
            throws PersistenceException {
        storeString(name, Integer.toString(value));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#storePersistable(java.lang.String, org.eclipse.mtj.core.persistence.IPersistable)
     */
    public void storePersistable(String name, IPersistable value)
            throws PersistenceException {

        if (value != null) {
            element = XMLUtils.createChild(element, name);

            if (value instanceof IBundleReferencePersistable) {
                String bundle = ((IBundleReferencePersistable) value)
                        .getBundle();
                if (bundle != null) {
                    element
                            .setAttribute(
                                    IBundleReferencePersistable.BUNDLE_PERSISTABLE_ATTRIBUTE,
                                    bundle);
                }
            }
            element.setAttribute(IPersistable.CLASS_PERSISTABLE_ATTRIBUTE,
                    value.getClass().getName());

            // Store material for making a reference
            Integer id = Integer.valueOf(identifier++);
            element.setAttribute(IPersistable.ID_PERSISTABLE_ATTRIBUTE, id
                    .toString());
            referenceMap.put(value, id);

            value.storeUsing(this);
            element = (Element) element.getParentNode();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#storeProperties(java.lang.String, java.util.Properties)
     */
    public void storeProperties(String name, Properties properties)
            throws PersistenceException {

        if (properties != null) {

            Element propertiesElement = XMLUtils.createChild(element, name);

            Iterator<Map.Entry<Object, Object>> entries = properties.entrySet()
                    .iterator();
            while (entries.hasNext()) {
                Element propertyElement = XMLUtils.createChild(
                        propertiesElement,
                        IPersistable.PROPERTY_PERSISTABLE_ELEMENT);

                Map.Entry<Object, Object> entry = entries.next();
                propertyElement.setAttribute(
                        IPersistable.KEY_PERSISTABLE_ATTRIBUTE, entry.getKey()
                                .toString());
                propertyElement.setAttribute(
                        IPersistable.VALUE_PERSISTABLE_ATTRIBUTE, entry
                                .getValue().toString());
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#storeReference(java.lang.String, java.lang.Object)
     */
    public void storeReference(String name, Object referenceObject)
            throws PersistenceException {
        Integer refid = (Integer) referenceMap.get(referenceObject);
        if (refid != null) {
            Element newElement = XMLUtils.createChild(element, name);
            newElement.setAttribute(IPersistable.REFID_PERSISTABLE_ATTRIBUTE,
                    refid.toString());
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistenceProvider#storeString(java.lang.String, java.lang.String)
     */
    public void storeString(String name, String value)
            throws PersistenceException {

        if (value != null) {
            try {
                Element newElement = XMLUtils.createChild(element, name);
                if (newElement != null) {
                    newElement.setAttribute(
                            IPersistable.VALUE_PERSISTABLE_ATTRIBUTE, value);
                } else {
                    throw new PersistenceException(
                            Messages.XMLPersistenceProvider_1);
                }
            } catch (DOMException e) {
                throw new PersistenceException(
                        Messages.XMLPersistenceProvider_0, e);
            }
        }
    }

    /**
     * Create a new persistable instance based on the information in the
     * specified element.
     * 
     * @param persistableElement the element containing the information
     *            necessary to load a {@link IPersistable} object.
     * @return a newly allocated instance of the class represented by the
     *         persistableElement.
     * @throws PersistenceException if this method fails. Reasons include:
     *             <ul>
     *             <li>if we try to create an instance of the persisted class
     *             using the newInstance method in class Class, but the
     *             specified class object cannot be instantiated because it is
     *             an interface or is an abstract class.</li>
     *             <li>If the class constructor is not accessible.</li>
     *             <li>If the class for the persisted element could not be
     *             found.</li>
     *             <li>If for any reason a new instance for the persisted
     *             element could not be created.</li>
     *             </ul>
     */
    private IPersistable createPersistableInstance(Element persistableElement)
            throws PersistenceException {
        IPersistable persistable = null;

        // Handle bundle references
        String className = persistableElement
                .getAttribute(IPersistable.CLASS_PERSISTABLE_ATTRIBUTE);
        String bunbleName = persistableElement
                .getAttribute(IBundleReferencePersistable.BUNDLE_PERSISTABLE_ATTRIBUTE);

        try {
            Class<?> clazz = loadClass(bunbleName, className);
            if (clazz != null) {
                persistable = (IPersistable) clazz.newInstance();
            }
        } catch (InstantiationException e) {
            throw new PersistenceException("InstantiationException: " //$NON-NLS-1$
                    + e.getMessage(), e);
        } catch (IllegalAccessException e) {
            throw new PersistenceException("IllegalAccessException: " //$NON-NLS-1$
                    + e.getMessage(), e);
        } catch (ClassNotFoundException e) {
            throw new PersistenceException("ClassNotFoundException: " //$NON-NLS-1$
                    + e.getMessage(), e);
        } catch (Exception e) {
            throw new PersistenceException("Exception: " + e.getMessage(), e); //$NON-NLS-1$
        }

        return persistable;
    }

    /**
     * Return the current bundle in effect for class loading purposes.
     * 
     * @return the object at the top of bundle stack without removing it from
     *         the stack.
     */
    private Bundle getCurrentBundle() {
        Bundle bundle = null;

        if (!bundleStack.isEmpty()) {
            bundle = bundleStack.peek();
        }

        return bundle;
    }

    /**
     * Return the element with the given named, within the current context.
     * <p>
     * If no element whit the given name could be found this method returns the
     * <code>null</code> value.
     * </p>
     * 
     * @param name the name of the element to be found.
     * @return the element whit the given name or <code>null</code> if no
     *         element could be found.
     */
    private Element getNamedElement(String name) {
        Element namedElement = null;

        NodeList nodes = element.getElementsByTagName(name);

        // Find an element with the current element as the parent node to avoid
        // picking up nodes in objects held within fields.
        for (int i = 0; (namedElement == null) && (i < nodes.getLength()); i++) {
            Element elem = (Element) nodes.item(i);
            if (elem != null) {
                Node parentNone = elem.getParentNode();
                if (parentNone != null) {
                    if (parentNone.equals(element)) {
                        namedElement = elem;
                    }
                }
            }
        }

        return namedElement;
    }

    /**
     * Load the specified class by finding the correct bundle.
     * 
     * @param bundleId he symbolic name of the bundle from where we can use the
     *            classloader to load the given class based on it's className.
     * @param className the name of the class to be loaded.
     * @return The Class object for the requested class.
     * @throws ClassNotFoundException If the class was not found.
     */
    private Class<?> loadClass(String bundlename, String className)
            throws ClassNotFoundException {
        Class<?> clazz = null;

        Bundle bundle = Platform.getBundle(bundlename);

        if (bundle != null) {
            clazz = bundle.loadClass(className);
        } else {
            clazz = getClass().getClassLoader().loadClass(className);
        }

        return clazz;
    }

    /**
     * Push a new bundle reference on the stack if the persistable element
     * references a bundle identifier. Return a boolean indicating whether or
     * not a bundle was actually pushed.
     * 
     * @param persistableElement the persistable element to be pushed in the
     *            bundle stack.
     * @return <code>true</code> if the element was correctly pushed in the
     *         bundle stack or <code>false</code> otherwise.
     */
    private boolean pushNewBundle(Element persistableElement) {
        boolean pushed = false;

        String bundleId = persistableElement
                .getAttribute(IBundleReferencePersistable.BUNDLE_PERSISTABLE_ATTRIBUTE);

        if ((bundleId != null) && (bundleId.length() > 0)) {
            // Handle mapping one bundle reference to a different bundle. This
            // is done to handle changes in bundle references that may occur
            // between versions.
            String bundleMapping = bundleMappings.get(bundleId);
            if (bundleMapping != null) {
                bundleId = bundleMapping;
            }

            // Lookup a new bundle as necessary
            Bundle bundle = Platform.getBundle(bundleId);
            if (bundle != null) {
                bundleStack.push(bundle);
                pushed = true;
            }
        }

        return pushed;
    }
}
