/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 *     David Marques (Motorola) - Implementing validation.
 *     David Marques (Motorola) - Adding default locale.                     
 */
package org.eclipse.mtj.internal.core.text.l10n;

import org.eclipse.mtj.internal.core.text.IDocumentElementNode;

/**
 * @since 0.9.1
 */
public class L10nLocales extends L10nObject {

    private static final long serialVersionUID = 1L;
    private L10nLocale defaultLocale;
    
    /**
     * @param model
     */
    public L10nLocales(L10nModel model) {
        super(model, ELEMENT_LOCALES);
        setInTheModel(true);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#addChild(org.eclipse.mtj.internal.core.text.l10n.L10nObject)
     */
    @Override
    public void addChild(L10nObject child) {
        addChildNode(child, true);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#addChild(org.eclipse.mtj.internal.core.text.l10n.L10nObject, org.eclipse.mtj.internal.core.text.l10n.L10nObject, boolean)
     */
    @Override
    public void addChild(L10nObject child, L10nObject sibling,
            boolean insertBefore) {
        int currentIndex = indexOf(sibling);
        if (!insertBefore) {
            currentIndex++;
        }

        addChildNode(child, currentIndex, true);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#canBeParent()
     */
    @Override
    public boolean canBeParent() {
        return true;
    }

    /**
     * @return
     */
    public String getDestination() {
        return getXMLAttributeValue(ATTRIBUTE_DESTINATION);
    }

    /**
     * Gets the default locale instance.
     * 
     * @return default locale.
     */
    public L10nLocale getDefaultLocale() {
    	String name = getXMLAttributeValue(ATTRIBUTE_DEFAULT_LOCALE);
    	IDocumentElementNode[] localeNodes = this.getChildNodes();
        for (IDocumentElementNode localeNode : localeNodes) {
            L10nLocale locale = (L10nLocale) localeNode;
            if (locale.getName().equals(name)) {
				defaultLocale = locale;
				break;
			}
        }
    	return defaultLocale;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getName()
     */
    @Override
    public String getName() {
        return "Locales";
    }

    /**
     * @return
     */
    public String getPackage() {
        String pack = getXMLAttributeValue(ATTRIBUTE_PACKAGE);
        return pack == null ? "" : pack;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getType()
     */
    @Override
    public int getType() {
        return TYPE_LOCALES;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.DocumentElementNode#isRoot()
     */
    @Override
    public boolean isRoot() {
        return true;
    }

    /**
     * @param l10nObject
     * @param newRelativeIndex
     */
    public void moveChild(L10nObject l10nObject, int newRelativeIndex) {
        moveChildNode(l10nObject, newRelativeIndex, true);
    }

    /**
     * @param l10nObject
     */
    public void removeChild(L10nObject l10nObject) {
        removeChildNode(l10nObject, true);
    }

    /**
     * @param name
     */
    public void setDestination(String name) {
        setXMLAttribute(ATTRIBUTE_DESTINATION, name);
    }

    /**
     * @param value
     */
    public void setPackage(String value) {
        setXMLAttribute(ATTRIBUTE_PACKAGE, value);
    }

    /**
     * Sets the default locale.
     * 
     * @param locale default locale instance.
     */
    public void setDefaultLocale(L10nLocale locale) {
    	String value = null;
    	if (locale != null) {
			value = locale.getName();
		}
    	setXMLAttribute(ATTRIBUTE_DEFAULT_LOCALE, value);
    	this.defaultLocale = locale;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#validate()
     */
    @Override
    public void validate() {
        IDocumentElementNode[] localeNodes = this.getChildNodes();
        for (IDocumentElementNode localeNode : localeNodes) {
            L10nLocale locale = (L10nLocale) localeNode;
            locale.validate();
        }
    }

}
