/**
 * Copyright (c) 2003, 2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.persistence;

import org.osgi.framework.Bundle;

/**
 * Classes that implement the IBundleReferencePersistable interface are standard
 * IPersistable classes that must also be able to provide a reference to the
 * bundle from which they may be loaded.
 * 
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface IBundleReferencePersistable extends IPersistable {

    /**
     * The persistable attribute id for storing the bundle identifier.
     */
    public static final String BUNDLE_PERSISTABLE_ATTRIBUTE = "bundle"; //$NON-NLS-1$

    /**
     * Return the bundle identifier that should be used to load this
     * persistable. The bundle identifier must be the symbolic name of that
     * bundle as specified by its <code>Bundle-SymbolicName</code> manifest
     * header.
     * <p>
     * Clients must always return a valid bundle identifier in order to MTJ
     * correctly load that bundle.
     * </p>
     * <p>
     * The symbolic name of a bundle may be retrieved using the
     * {@link Bundle#getSymbolicName()} method.
     * </p>
     * 
     * @return the symbolic name of the bundle that should be used to load this
     *         persistable.
     */
    String getBundle();

    /**
     * Set the bundle identifier that is used to load this persistable. The
     * bundle identifier must be the symbolic name of that bundle as specified
     * by its <code>Bundle-SymbolicName</code> manifest header.
     * <p>
     * The symbolic name of a bundle may be retrieved using the
     * {@link Bundle#getSymbolicName()} method.
     * </p>
     * 
     * @param bundle a valid bundle identifier.
     */
    void setBundle(String bundle);
}
