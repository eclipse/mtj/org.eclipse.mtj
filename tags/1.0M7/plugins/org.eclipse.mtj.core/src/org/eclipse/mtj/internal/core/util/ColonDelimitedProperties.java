/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID.
 */
package org.eclipse.mtj.internal.core.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 * A special Properties subclass that reads and writes using ':' instead of '='
 * for the key/value separator.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @since 1.0
 */
public class ColonDelimitedProperties extends Properties {

    /**
     * Default serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Creates an empty property list with no default values.
     */
    public ColonDelimitedProperties() {
        super();
    }

    /**
     * Creates a property list with the specified defaults.
     * 
     * @param defaults the defaults.
     */
    public ColonDelimitedProperties(Properties defaults) {
        super(defaults);
    }

    /* (non-Javadoc)
     * @see java.util.Properties#load(java.io.InputStream)
     */
    @Override
    public synchronized void load(InputStream inStream) throws IOException {
        Reader dataReader = new InputStreamReader(inStream, "UTF-8"); //$NON-NLS-1$
        load(dataReader);
    }

    /**
     * Load the colon-delimited properties from the specified Reader.
     * 
     * @param reader the reader containing the properties
     * @throws IOException if an error occurs loading the properties
     */
    public synchronized void load(Reader reader) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(reader);

        try {
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                // Find the first colon and break the string up
                line = line.trim();
                if ((line.length() > 0) && !line.startsWith("#")) { //$NON-NLS-1$
                    int colonIndex = line.indexOf(':');
                    if (colonIndex != -1) {
                        String name = line.substring(0, colonIndex);
                        if (colonIndex + 1 < line.length()) {
                            String value = line.substring(colonIndex + 1)
                                    .trim();
                            setProperty(name, value);
                        }
                    }
                }
            }
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
            }
        }
    }

    /* (non-Javadoc)
     * @see java.util.Properties#store(java.io.OutputStream, java.lang.String)
     */
    @Override
    @SuppressWarnings("unchecked")
    public synchronized void store(OutputStream out, String header)
            throws IOException {
        OutputStreamWriter osw = new OutputStreamWriter(out, "UTF-8"); //$NON-NLS-1$
        PrintWriter writer = new PrintWriter(osw);

        // Maintain the JAD file in sorted by keys order
        List keys = new ArrayList(keySet());
        Collections.sort(keys);

        Iterator<?> iter = keys.iterator();
        while (iter.hasNext()) {
            String key = (String) iter.next();
            String value = getProperty(key);
            writer.println(key + ": " + value); //$NON-NLS-1$
        }

        writer.flush();
    }
}
