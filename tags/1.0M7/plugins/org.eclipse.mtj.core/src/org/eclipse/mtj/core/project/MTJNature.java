/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.project;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;

/**
 * This abstract {@link IProjectNature Nature} implementation must be the
 * superclass of all natures that may be created to be added to a
 * {@link IMTJProject} project.
 * <p>
 * All clients interested in adding a new nature to MTJ <strong>must</strong>
 * extend this class.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public abstract class MTJNature implements IProjectNature {

    /**
     * The project to which this nature applies
     */
    private IProject project;

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IProjectNature#configure()
     */
    public void configure() throws CoreException {
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IProjectNature#deconfigure()
     */
    public void deconfigure() throws CoreException {
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IProjectNature#getProject()
     */
    public IProject getProject() {
        return project;
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IProjectNature#setProject(org.eclipse.core.resources.IProject)
     */
    public void setProject(IProject project) {
        this.project = project;
    }

    /**
     * Adds the specified nature in the given project.
     * <p>
     * The nature will be append in the end of the list of natures associated
     * with the given project.
     * </p>
     * 
     * @param project the project in which we'll associate the given nature.
     * @param natureId the fully qualified nature extension identifier, formed
     *            by combining the nature extension id with the id of the
     *            declaring plug-in. (e.g. "org.eclipse.mtj.core.l10nNature")
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @throws CoreException if this method fails. Reasons include:
     *             <ul>
     *             <li>The project does not exist in the workspace.</li>
     *             <li>The project is not open.</li>
     *             <li>The location in the local file system corresponding to
     *             the project description file is occupied by a directory.</li>
     *             <li>The workspace is out of sync with the project description
     *             file in the local file system .</li>
     *             <li>The file modification validator disallowed the change.</li>
     *             </ul>
     * @throws OperationCanceledException if the operation is canceled.
     *             Cancellation can occur even if no progress monitor is
     *             provided.
     */
    public static void addNatureToProject(IProject project, String natureId,
            IProgressMonitor monitor) throws CoreException,
            OperationCanceledException {

        IProjectDescription description = project.getDescription();
        ArrayList<String> natures = new ArrayList<String>(Arrays
                .asList(description.getNatureIds()));
        natures.add(natureId);
        String[] natureArray = natures.toArray(new String[natures.size()]);
        description.setNatureIds(natureArray);
        project.setDescription(description, monitor);

    }

    /**
     * Removes the specified nature from the given project.
     * <p>
     * After removal of specified nature, the order of the remaining natures
     * will be maintained
     * </p>
     * 
     * @param project the project from which we'll remove the association to the
     *            given nature.
     * @param natureId the fully qualified nature extension identifier, formed
     *            by combining the nature extension id with the id of the
     *            declaring plug-in. (e.g. "org.eclipse.mtj.core.l10nNature")
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @throws CoreException if this method fails. Reasons include:
     *             <ul>
     *             <li>The project does not exist in the workspace.</li>
     *             <li>The project is not open.</li>
     *             <li>The location in the local file system corresponding to
     *             the project description file is occupied by a directory.</li>
     *             <li>The workspace is out of sync with the project description
     *             file in the local file system .</li>
     *             <li>The file modification validator disallowed the change.</li>
     *             </ul>
     * @throws OperationCanceledException if the operation is canceled.
     *             Cancellation can occur even if no progress monitor is
     *             provided.
     */
    public static void removeNatureFromProject(IProject project,
            String natureId, IProgressMonitor monitor) throws CoreException {

        IProjectDescription description = project.getDescription();
        ArrayList<String> natures = new ArrayList<String>(Arrays
                .asList(description.getNatureIds()));
        natures.remove(natureId);
        String[] natureArray = natures.toArray(new String[natures.size()]);
        description.setNatureIds(natureArray);
        project.setDescription(description, monitor);

    }
}
