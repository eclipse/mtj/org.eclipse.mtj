/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.core.build.preprocessor;

/**
 * Helper for preprocessor
 * 
 * @author gma
 */
public class PreprocessorHelper {
    public static final String J2ME_PREPROCESS_DEBUG = "debug"; //$NON-NLS-1$
    public static final String J2ME_PREPROCESS_ERROR = "error"; //$NON-NLS-1$
    public static final String J2ME_PREPROCESS_FATAL = "fatal"; //$NON-NLS-1$
    public static final String J2ME_PREPROCESS_INFO = "info"; //$NON-NLS-1$
    public static final String J2ME_PREPROCESS_WARN = "warn"; //$NON-NLS-1$

    public static final String J2ME_PREPROCESS_DEBUG_LEVEL_KEY = "DEBUG"; //$NON-NLS-1$

    public static final String[] debuglevels = new String[] {
            J2ME_PREPROCESS_DEBUG, J2ME_PREPROCESS_INFO, J2ME_PREPROCESS_WARN,
            J2ME_PREPROCESS_ERROR, J2ME_PREPROCESS_FATAL,

    };

    public static String[] getSupportDebugLevels() {
        return debuglevels;
    }

    /**
     * judge whether the debug level is supported.
     * 
     * @param debuglevel
     * @return
     */
    public static boolean isLegalDebuglevel(String debuglevel) {
        for (String level : getSupportDebugLevels()) {
            if (level.equals(debuglevel))
                return true;
        }
        return false;
    }

}
