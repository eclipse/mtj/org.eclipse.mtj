/**
 * Copyright (c) 2003, 2009 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     David Marques (Motorola) - Adding process type.
 *     Diego Sandin (Motorola)  - Adopt ICU4J into MTJ
 *     Diego Sandin (Motorola)  - Enhance javadoc
 */
package org.eclipse.mtj.internal.core.launching.midp;

import java.io.File;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Preferences;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.jdi.Bootstrap;
import org.eclipse.jdt.debug.core.JDIDebugModel;
import org.eclipse.jdt.launching.AbstractVMRunner;
import org.eclipse.jdt.launching.ExecutionArguments;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.launching.SocketUtil;
import org.eclipse.jdt.launching.VMRunnerConfiguration;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.launching.LaunchEnvironment;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.osgi.util.NLS;

import com.ibm.icu.text.DateFormat;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.connect.AttachingConnector;
import com.sun.jdi.connect.Connector;
import com.sun.jdi.connect.IllegalConnectorArgumentsException;
import com.sun.jdi.connect.ListeningConnector;

/**
 * A VMRunner implementation that debugs against a wireless toolkit emulator.
 * Places the standard VM debug arguments as program arguments when in debug
 * mode. Otherwise, does not mess with that when not in debug mode.
 * 
 * @author Craig Setera
 */
@SuppressWarnings("unchecked")
public class EmulatorRunner extends AbstractVMRunner {

    /**
     * Used to attach to a VM in a separate thread, to allow for cancellation
     * and detect that the associated System process died before the connect
     * occurred.
     */
    static class ConnectRunnable implements Runnable {

        /**
         * The argument map to be used in launching the VM.
         */
        private Map fConnectionMap = null;

        /**
         * The {@link ListeningConnector} that will provide the connection
         * between a debugger and a target VM.
         */
        private ListeningConnector fConnector = null;

        /**
         * An exception that occurred while attaching to the target VM.
         */
        private Exception fException = null;

        /**
         * the VM that was attached to
         */
        private VirtualMachine fVirtualMachine = null;

        /**
         * Constructs a runnable to connect to a VM via the given connector with
         * the given connection arguments.
         * 
         * @param connector the {@link ListeningConnector} that will provide the
         *            connection between a debugger and a target VM.
         * @param vmArgMap the argument map to be used in launching the VM.
         */
        public ConnectRunnable(ListeningConnector connector, Map vmArgMap) {
            fConnector = connector;
            fConnectionMap = vmArgMap;
        }

        /**
         * Returns any exception that occurred while attaching, or
         * <code>null</code>.
         * 
         * @return IOException when failed to attach to the target VM,
         *         IllegalConnectorArgumentsException when one of the connector
         *         arguments is invalid or <code>null</code> if no exception was
         *         thrown.
         */
        public Exception getException() {
            return fException;
        }

        /**
         * Returns the VM that was attached to, or <code>null</code> if none.
         * 
         * @return the VM that was attached to, or <code>null</code> if none
         */
        public VirtualMachine getVirtualMachine() {
            return fVirtualMachine;
        }

        /* (non-Javadoc)
         * @see java.lang.Runnable#run()
         */
        public void run() {
            try {
                fVirtualMachine = fConnector.accept(fConnectionMap);
            } catch (IOException e) {
                fException = e;
            } catch (IllegalConnectorArgumentsException e) {
                fException = e;
            }
        }
    }

    /**
     * The port VM argument
     */
    private static final String PORT_VM_ARG = "port"; //$NON-NLS-1$

    /**
     * The short identifier for the SocketAttach connector.
     */
    private static final String SOCKET_ATTACHING_CONNECTOR_NAME = "com.sun.jdi.SocketAttach"; //$NON-NLS-1$

    /**
     * The short identifier for the SocketListen connector.
     */
    private static final String SOCKET_LISTENING_CONNECTOR_NAME = "com.sun.jdi.SocketListen"; //$NON-NLS-1$

    /**
     * The timeout VM argument
     */
    private static final String TIMEOUT_VM_ARG = "timeout"; //$NON-NLS-1$

    /**
     * Flag for indicating if the launch should be made in debug mode.
     */
    private boolean debugMode;

    /**
     * The device implementation to be launched.
     */
    private IDevice device;

    /**
     * The project that contains the resources that may be launched.
     */
    private IMidletSuiteProject midletSuiteProject;

    /**
     * Construct an VM runner instance for an executable emulator.
     * 
     * @param midletSuiteProject project that contains the resources that may be
     *            launched.
     * @param device the device implementation to be launched.
     * @param mode the launch mode. For launching in a normal, non-debug
     *            mode(value <code>"run"</code>) or for launching in a special
     *            debug mode (value <code>"debug"</code>).
     */
    public EmulatorRunner(IMidletSuiteProject midletSuiteProject,
            IDevice device, String mode) {
        this.midletSuiteProject = midletSuiteProject;
        this.device = device;
        debugMode = ILaunchManager.DEBUG_MODE.equals(mode);
    }

    /**
     * Render the process label string in the following format:
     * 
     * <pre>
     * &lt;commandLine[0]&gt; (&lt;time stamp&gt;) 
     * </pre>
     * 
     * @param commandLine the command line as an array of arguments.
     * @return the rendered process label string.
     */
    public String renderProcessLabel(String[] commandLine) {
        String timestamp = DateFormat.getInstance().format(
                new Date(System.currentTimeMillis()));
        return NLS.bind(Messages.debugvmrunner_process_label_string,
                new String[] { commandLine[0], timestamp });
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.launching.IVMRunner#run(org.eclipse.jdt.launching.VMRunnerConfiguration, org.eclipse.debug.core.ILaunch, org.eclipse.core.runtime.IProgressMonitor)
     */
    public void run(VMRunnerConfiguration configuration, ILaunch launch,
            IProgressMonitor monitor) throws CoreException {
        // Method provided to meet the superclass requirement. Is not called.
    }

    /**
     * Launches a Java VM as specified in the given configuration, contributing
     * results (debug targets and processes), to the given launch.
     * 
     * @param vmRunnerConfig the configuration settings for this run
     * @param launchConfig The launchConfiguration to set
     * @param launch the launch to contribute to
     * @param monitor progress monitor or <code>null</code>. A cancelable
     *            progress monitor is provided by the Job framework. It should
     *            be noted that the setCanceled(boolean) method should never be
     *            called on the provided monitor or the monitor passed to any
     *            delegates from this method; due to a limitation in the
     *            progress monitor framework using the setCanceled method can
     *            cause entire workspace batch jobs to be canceled, as the
     *            canceled flag is propagated up the top-level parent monitor.
     *            The provided monitor is not guaranteed to have been started.
     * @exception CoreException if an exception occurs while launching
     * @see org.eclipse.jdt.launching.IVMRunner#run(org.eclipse.jdt.launching.VMRunnerConfiguration,
     *      org.eclipse.debug.core.ILaunch,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public void run(VMRunnerConfiguration vmRunnerConfig,
            ILaunchConfiguration launchConfig, ILaunch launch,
            IProgressMonitor monitor) throws CoreException {
        if (debugMode) {
            runInDebug(vmRunnerConfig, launchConfig, launch, monitor);
        } else {
            runWithoutDebug(vmRunnerConfig, launchConfig, launch, monitor);
        }
    }

    /**
     * Run the emulator with debugging.
     * 
     * @param vmRunnerConfig the configuration settings for this run
     * @param launchConfig The launchConfiguration to set
     * @param launch the launch to contribute to
     * @param monitor progress monitor or <code>null</code>. A cancelable
     *            progress monitor is provided by the Job framework. It should
     *            be noted that the setCanceled(boolean) method should never be
     *            called on the provided monitor or the monitor passed to any
     *            delegates from this method; due to a limitation in the
     *            progress monitor framework using the setCanceled method can
     *            cause entire workspace batch jobs to be canceled, as the
     *            canceled flag is propagated up the top-level parent monitor.
     *            The provided monitor is not guaranteed to have been started.
     * @exception CoreException if an exception occurs while launching
     */
    public void runInDebug(VMRunnerConfiguration vmRunnerConfig,
            ILaunchConfiguration launchConfig, ILaunch launch,
            IProgressMonitor monitor) throws CoreException {

        if (monitor == null) {
            monitor = new NullProgressMonitor();
        }

        IProgressMonitor subMonitor = new SubProgressMonitor(monitor, 1);
        subMonitor.beginTask(Messages.debugvmrunner_launching_vm, 4);
        subMonitor.subTask(Messages.debugvmrunner_finding_free_socket);

        int port = SocketUtil.findFreePort();
        if (port == -1) {
            abort(Messages.debugvmrunner_no_free_socket, null,
                    IJavaLaunchConfigurationConstants.ERR_NO_SOCKET_AVAILABLE);
        }

        subMonitor.worked(1);

        // check for cancellation
        if (monitor.isCanceled()) {
            return;
        }

        subMonitor.subTask(Messages.debugvmrunner_constructing_cmd_line);

        String[] cmdLine = getCommandLine(launchConfig, port, monitor);
        Utils.dumpCommandLine(cmdLine);

        // check for cancellation
        if (monitor.isCanceled()) {
            return;
        }

        subMonitor.worked(1);
        subMonitor.subTask(Messages.debugvmrunner_starting_VM);

        Connector vmConnector = getVMConnector();
        if (vmConnector == null) {
            abort(
                    Messages.debugvmrunner_no_connector,
                    null,
                    IJavaLaunchConfigurationConstants.ERR_CONNECTOR_NOT_AVAILABLE);
        }

        Map vmArgsMap = vmConnector.defaultArguments();
        specifyArguments(vmArgsMap, port);

        Process emulatorProcess = null;
        try {
            try {
                // check for cancellation
                if (monitor.isCanceled()) {
                    return;
                }

                if (!device.isDebugServer()) {
                    ((ListeningConnector) vmConnector)
                            .startListening(vmArgsMap);
                }

                File workingDir = getWorkingDir(vmRunnerConfig);
                emulatorProcess = exec(cmdLine, workingDir);
                if (emulatorProcess == null) {
                    return;
                }

                // check for cancellation
                if (monitor.isCanceled()) {
                    emulatorProcess.destroy();
                    return;
                }

                Map<String, String> defaultMap = getDefaultProcessMap();
                defaultMap.put(IProcess.ATTR_PROCESS_TYPE,
                        IMTJCoreConstants.MTJ_PROCESS_TYPE);

                IProcess newEmulatorProcess = DebugPlugin.newProcess(launch,
                        emulatorProcess, renderProcessLabel(cmdLine),
                        defaultMap);
                newEmulatorProcess.setAttribute(IProcess.ATTR_CMDLINE,
                        renderCommandLine(cmdLine));

                subMonitor.worked(1);
                subMonitor
                        .subTask(Messages.debugvmrunner_establishing_debug_conn);

                VirtualMachine vm = createVirtualMachine(vmConnector,
                        vmArgsMap, emulatorProcess, newEmulatorProcess, monitor);

                JDIDebugModel.newDebugTarget(launch, vm, renderDebugTarget(
                        vmRunnerConfig.getClassToLaunch(), port),
                        newEmulatorProcess, true, false);
                subMonitor.worked(1);
                subMonitor.done();
                return;

            } finally {
                if (!device.isDebugServer()) {
                    ((ListeningConnector) vmConnector).stopListening(vmArgsMap);
                }
            }
        } catch (IOException e) {
            abort(Messages.debugvmrunner_couldnt_connect_to_vm, e,
                    IJavaLaunchConfigurationConstants.ERR_CONNECTION_FAILED);
        } catch (IllegalConnectorArgumentsException e) {
            abort(Messages.debugvmrunner_couldnt_connect_to_vm, e,
                    IJavaLaunchConfigurationConstants.ERR_CONNECTION_FAILED);
        }

        if (emulatorProcess != null) {
            emulatorProcess.destroy();
        }
    }

    /**
     * Run the emulator without debugging.
     * 
     * @param vmRunnerConfig the configuration settings for this run
     * @param launchConfig The launchConfiguration to set
     * @param launch the launch to contribute to
     * @param monitor progress monitor or <code>null</code> A cancelable
     *            progress monitor is provided by the Job framework. It should
     *            be noted that the setCanceled(boolean) method should never be
     *            called on the provided monitor or the monitor passed to any
     *            delegates from this method; due to a limitation in the
     *            progress monitor framework using the setCanceled method can
     *            cause entire workspace batch jobs to be canceled, as the
     *            canceled flag is propagated up the top-level parent monitor.
     *            The provided monitor is not guaranteed to have been started.
     * @exception CoreException if an exception occurs while launching
     */
    public void runWithoutDebug(VMRunnerConfiguration vmRunnerConfig,
            ILaunchConfiguration launchConfig, ILaunch launch,
            IProgressMonitor monitor) throws CoreException {

        if (monitor == null) {
            monitor = new NullProgressMonitor();
        }

        IProgressMonitor subMonitor = new SubProgressMonitor(monitor, 1);
        subMonitor.beginTask(Messages.debugvmrunner_launching_vm, 3);

        // check for cancellation
        if (monitor.isCanceled()) {
            return;
        }

        subMonitor.subTask(Messages.debugvmrunner_constructing_cmd_line);

        String[] cmdLine = getCommandLine(launchConfig, -1, monitor);
        Utils.dumpCommandLine(cmdLine);

        // check for cancellation
        if (monitor.isCanceled()) {
            return;
        }

        subMonitor.worked(1);
        subMonitor.subTask(Messages.debugvmrunner_starting_VM);

        // check for cancellation
        if (monitor.isCanceled()) {
            return;
        }

        File workingDir = getWorkingDir(vmRunnerConfig);
        Process p = exec(cmdLine, workingDir);
        if (p == null) {
            return;
        }

        // check for cancellation
        if (monitor.isCanceled()) {
            p.destroy();
            return;
        }

        Map<String, String> defaultMap = getDefaultProcessMap();
        defaultMap.put(IProcess.ATTR_PROCESS_TYPE,
                IMTJCoreConstants.MTJ_PROCESS_TYPE);
        IProcess process = DebugPlugin.newProcess(launch, p,
                renderProcessLabel(cmdLine), defaultMap);
        process.setAttribute(IProcess.ATTR_CMDLINE, renderCommandLine(cmdLine));

        subMonitor.worked(1);
    }

    /**
     * Create a new VirtualMachine instances for the specified Connector and
     * associated information.
     * 
     * @param vmConnector the {@link Connector} implementation that will provide
     *            the connection between a debugger and a target VM.
     * @param vmArgMap the argument map to be used in launching the VM.
     * @param emulatorProcessRaw the emulator process.
     * @param emulatorProcessIlaunchBased the emulator process launched using an
     *            {@link ILaunch} implementation.
     * @param monitor progress monitor or <code>null</code>.
     * @return a new VirtualMachine instance for the specified vmConnector and
     *         associated information.
     * @throws IOException if an error was found while trying to attach to the
     *             debugger.
     * @throws IllegalConnectorArgumentsException when one of the connector
     *             arguments is invalid.
     * @throws CoreException if an error was found while trying to attach to the
     *             debugger.
     */
    private VirtualMachine createVirtualMachine(Connector vmConnector,
            Map vmArgMap, Process emulatorProcess,
            IProcess emulatorProcessIlaunchBased, IProgressMonitor monitor)
            throws IOException, IllegalConnectorArgumentsException,
            CoreException {
        VirtualMachine vm = (device.isDebugServer()) ? (waitForRemoteDebugger(
                (AttachingConnector) vmConnector, vmArgMap))
                : waitForDebuggerConnection((ListeningConnector) vmConnector,
                        emulatorProcess, emulatorProcessIlaunchBased, vmArgMap,
                        monitor);
        return vm;
    }

    /**
     * Get the appropriate JDI AttachingConnector instance.
     * 
     * @return a SocketAttach connector instance.
     */
    private AttachingConnector getAttachingConnector() {
        AttachingConnector connector = null;

        List connectors = Bootstrap.virtualMachineManager()
                .attachingConnectors();

        for (int i = 0; i < connectors.size(); i++) {
            AttachingConnector c = (AttachingConnector) connectors.get(i);
            if (SOCKET_ATTACHING_CONNECTOR_NAME.equals(c.name())) {
                connector = c;
            }
        }

        return connector;
    }

    /**
     * Get the appropriate JDI ListenerConnector instance.
     * 
     * @return a SocketListen connector instance.
     */
    private ListeningConnector getListeningConnector() {
        ListeningConnector connector = null;

        List connectors = Bootstrap.virtualMachineManager()
                .listeningConnectors();

        for (int i = 0; i < connectors.size(); i++) {
            ListeningConnector c = (ListeningConnector) connectors.get(i);
            if (SOCKET_LISTENING_CONNECTOR_NAME.equals(c.name())) { //$NON-NLS-1$
                connector = c;
            }
        }

        return connector;
    }

    /**
     * Get the appropriate {@link Connector} depending if the {@link IDevice}
     * implementation is a debug server or not.
     * 
     * @return an {@link AttachingConnector} if the
     *         {@link IDevice#isDebugServer()} method returns <code>true</code>
     *         or a {@link ListeningConnector} otherwise.
     */
    private Connector getVMConnector() {
        Connector connector = (device.isDebugServer()) ? (Connector) getAttachingConnector()
                : (Connector) getListeningConnector();
        return connector;
    }

    /**
     * Returns the working directory to use for the launched VM, or
     * <code>null</code> if the working directory is to be inherited from the
     * current process.
     * 
     * @return the working directory to use
     * @exception CoreException if the working directory specified by the
     *                configuration does not exist or is not a directory
     */
    private File getWorkingDir(VMRunnerConfiguration config)
            throws CoreException {
        File dir = null;

        String path = null;

        if (device instanceof IMIDPDevice) {
            File deviceWorkingDirectory = ((IMIDPDevice) device)
                    .getWorkingDirectory();
            if ((deviceWorkingDirectory != null)
                    && deviceWorkingDirectory.exists()) {
                path = deviceWorkingDirectory.getPath();
            }
        }

        if (path == null) {
            path = config.getWorkingDirectory();
        }

        if (path != null) {
            dir = new File(path);
            if (!dir.isDirectory()) {
                abort(
                        NLS.bind(Messages.debugvmrunner_workingdir_not_dir,
                                new String[] { path }),
                        null,
                        IJavaLaunchConfigurationConstants.ERR_WORKING_DIRECTORY_DOES_NOT_EXIST);
            }
        }

        return dir;
    }

    /**
     * Render the command line string.
     * 
     * 
     * @param commandLineArgs the command line as an array of arguments.
     * @return the command line string.
     */
    private String renderCommandLine(String[] commandLineArgs) {
        StringBuffer buf = new StringBuffer();

        if (commandLineArgs.length > 1) {
            for (int i = 0; i < commandLineArgs.length; i++) {
                if (i > 0) {
                    buf.append(' ');
                }
                buf.append(commandLineArgs[i]);
            }
        }

        return buf.toString();
    }

    /**
     * Render the debug target string in the following format:
     * 
     * <pre>
     * &lt;classToRun&gt; at localhost &lt;debugPort&gt;
     * </pre>
     * 
     * @param classToRun
     * @param debugPort the port to be listened for debugging.
     * @return the rendered debug target string.
     */
    private String renderDebugTarget(String classToRun, int debugPort) {
        return NLS.bind(Messages.debugvmrunner_debug_target_string,
                new String[] { classToRun, String.valueOf(debugPort) });
    }

    /**
     * Specify new connector arguments to the JDI connector.
     * 
     * @param args the argument map to be used in launching the VM.
     * @param portNumber the port to be listened for debugging.
     */
    private void specifyArguments(Map args, int portNumber) {

        Connector.IntegerArgument port = (Connector.IntegerArgument) args
                .get(PORT_VM_ARG);
        port.setValue(portNumber);

        Connector.IntegerArgument timeoutArg = (Connector.IntegerArgument) args
                .get(TIMEOUT_VM_ARG);
        if (timeoutArg != null) {
            int timeout = JavaRuntime.getPreferences().getInt(
                    JavaRuntime.PREF_CONNECT_TIMEOUT);
            timeoutArg.setValue(timeout);
        }
    }

    /**
     * Wait for the debugger to connect to our connector and return the new
     * VirtualMachine.
     * 
     * @param vmConnector the {@link Connector} implementation that will provide
     *            the connection between a debugger and a target VM.
     * @param emulatorProcessRaw the emulator process.
     * @param emulatorProcessIlaunchBased the emulator process launched using an
     *            {@link ILaunch} implementation.
     * @param vmArgMap the argument map to be used in launching the VM.
     * @param monitor progress monitor or <code>null</code>.
     * @return the VM that was attached to, or <code>null</code> if none.
     * @throws CoreException if an error was found while trying to attach to the
     *             debugger.
     * @throws IOException when unable to stop listening to the connection.
     * @throws IllegalConnectorArgumentsException when one of the connector
     *             arguments is invalid.
     */
    private VirtualMachine waitForDebuggerConnection(
            ListeningConnector vmConnector, Process emulatorProcessRaw,
            IProcess emulatorProcessIlaunchBased, Map vmArgMap,
            IProgressMonitor monitor) throws CoreException, IOException,
            IllegalConnectorArgumentsException {
        VirtualMachine vm = null;

        boolean retry = false;
        do {
            try {
                ConnectRunnable runnable = new ConnectRunnable(vmConnector,
                        vmArgMap);
                Thread connectThread = new Thread(runnable,
                        Messages.EmulatorRunner_4);
                connectThread.start();

                while (connectThread.isAlive()) {
                    if (monitor.isCanceled()) {
                        vmConnector.stopListening(vmArgMap);
                        emulatorProcessRaw.destroy();

                        break;
                    }
                    try {
                        emulatorProcessRaw.exitValue();
                        // process has terminated - stop waiting for a
                        // connection
                        try {
                            vmConnector.stopListening(vmArgMap);
                        } catch (IOException e) {
                            // expected
                        }
                        checkErrorMessage(emulatorProcessIlaunchBased);
                    } catch (IllegalThreadStateException e) {
                        // expected while process is alive
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }

                Exception ex = runnable.getException();

                if (ex instanceof IllegalConnectorArgumentsException) {
                    throw (IllegalConnectorArgumentsException) ex;
                }
                if (ex instanceof InterruptedIOException) {
                    throw (InterruptedIOException) ex;
                }
                if (ex instanceof IOException) {
                    throw (IOException) ex;
                }

                vm = runnable.getVirtualMachine();

                break;
            } catch (InterruptedIOException e) {

                checkErrorMessage(emulatorProcessIlaunchBased);

                // timeout, consult status handler if there is one
                IStatus status = new Status(
                        IStatus.ERROR,
                        IMTJCoreConstants.PLUGIN_ID,
                        IJavaLaunchConfigurationConstants.ERR_VM_CONNECT_TIMEOUT,
                        Utils.EMPTY_STRING, e);
                IStatusHandler handler = DebugPlugin.getDefault()
                        .getStatusHandler(status);

                retry = false;
                if (handler == null) {
                    // if there is no handler, throw the exception
                    throw new CoreException(status);
                } else {
                    Object result = handler.handleStatus(status, this);
                    if (result instanceof Boolean) {
                        retry = ((Boolean) result).booleanValue();
                    }
                }
            }
        } while (retry);

        return vm;
    }

    /**
     * Connect to the remote VM debugger and return a new VirtualMachine. Will
     * retry until launch timeout is reached.
     * 
     * @param vmConnector the connector that will be used for attaching to a
     *            remote VM debugger and return a mirror of its VM.
     * @param vmArgMap the argument map to be used in launching the VM.
     * @return a mirror of the attached remote VM.
     * @throws IOException when unable to attach to the remote application.
     * @throws IllegalConnectorArgumentsException when one of the connector
     *             arguments is invalid.
     */
    private VirtualMachine waitForRemoteDebugger(
            AttachingConnector vmConnector, Map vmArgMap) throws IOException,
            IllegalConnectorArgumentsException {
        Preferences preferences = MTJCore.getMTJCore().getPluginPreferences();

        VirtualMachine vm = null;
        int launchTimeout = preferences
                .getInt(IMTJCoreConstants.PREF_RMTDBG_TIMEOUT);
        int launchRetryInterval = preferences
                .getInt(IMTJCoreConstants.PREF_RMTDBG_INTERVAL);
        long launchEndTime = System.currentTimeMillis() + launchTimeout;

        boolean retry = true;
        do {
            try {
                vm = vmConnector.attach(vmArgMap);
                retry = false;
            } catch (IOException e) {
                if (System.currentTimeMillis() > launchEndTime) {
                    throw new IOException(Messages.EmulatorRunner_6);
                } else {
                    try {
                        Thread.sleep(launchRetryInterval);
                    } catch (InterruptedException ex) {
                        // No action, re-try immediately in this case.
                    }
                }
            }
        } while (retry);

        return vm;
    }

    /**
     * Check for an error message and throw an exception as necessary.
     * 
     * @param process the process to be checked against error messages.
     * @throws CoreException if an error was found.
     */
    protected void checkErrorMessage(IProcess process) throws CoreException {
        String errorMessage = process.getStreamsProxy().getErrorStreamMonitor()
                .getContents();

        if (errorMessage.length() == 0) {
            errorMessage = process.getStreamsProxy().getOutputStreamMonitor()
                    .getContents();
        }

        if (errorMessage.length() != 0) {
            abort(errorMessage, null,
                    IJavaLaunchConfigurationConstants.ERR_VM_LAUNCH_ERROR);
        }
    }

    /**
     * Return the command-line arguments for launching the {@link IDevice}
     * implementation (received in the {@link EmulatorRunner} constructor) as an
     * array of individual arguments.
     * 
     * @param config the launch environment instance that provides the necessary
     *            information to a IDevice implementation for determining the
     *            correct command-line for execution of an emulator.
     * @param port the port to be listened for debugging.
     * @param monitor progress monitor or <code>null</code>. A cancelable
     *            progress monitor is provided by the Job framework. It should
     *            be noted that the setCanceled(boolean) method should never be
     *            called on the provided monitor or the monitor passed to any
     *            delegates from this method; due to a limitation in the
     *            progress monitor framework using the setCanceled method can
     *            cause entire workspace batch jobs to be canceled, as the
     *            canceled flag is propagated up the top-level parent monitor.
     *            The provided monitor is not guaranteed to have been started.
     * @return the command line arguments as an array of individual arguments
     * @throws CoreException if fails to get the command line from the
     *             {@link IDevice} implementation that will be used for
     *             launching.
     */
    protected String[] getCommandLine(ILaunchConfiguration config, int port,
            IProgressMonitor monitor) throws CoreException {

        LaunchEnvironment launchEnvironment = new LaunchEnvironment();
        launchEnvironment.setDebugLaunch(debugMode);
        launchEnvironment.setDebugListenerPort(port);
        launchEnvironment.setLaunchConfiguration(config);
        launchEnvironment.setProject(midletSuiteProject);

        String commandLineString = device.getLaunchCommand(launchEnvironment,
                monitor);
        ExecutionArguments execArgs = new ExecutionArguments(
                Utils.EMPTY_STRING, commandLineString);
        String[] cmdLine = execArgs.getProgramArgumentsArray();

        return cmdLine;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.launching.AbstractVMRunner#getPluginIdentifier()
     */
    @Override
    protected String getPluginIdentifier() {
        return IMTJCoreConstants.PLUGIN_ID;
    }
}
