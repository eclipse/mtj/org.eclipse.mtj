<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.eclipse.mtj.core" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appInfo>
         <meta.schema plugin="org.eclipse.mtj.core" id="mtjbuildhook" name="Build Hook"/>
      </appInfo>
      <documentation>
         This extension point provides a way for third party components to hook themselves to the MTJ project&apos;s build process.
&lt;p&gt;A hook can perform pre and pos build operations based on the several states available in the build process.&lt;br&gt;
Currently, the ordered list of possible states are:
&lt;ol&gt;
 &lt;li&gt;&lt;b&gt;PRE_BUILD&lt;/b&gt; [Before build process starts]&lt;/li&gt;
 &lt;li&gt;&lt;b&gt;PRE_PREPROCESS&lt;/b&gt; [Before preprocessing]&lt;/li&gt;
 &lt;li&gt;&lt;b&gt;POST_PREPROCESS&lt;/b&gt; [After preprocessing]&lt;/li&gt;
 &lt;li&gt;&lt;b&gt;PRE_COMPILE&lt;/b&gt; [Before JDT builder starts]&lt;/li&gt;
 &lt;li&gt;&lt;b&gt;POST_COMPILE&lt;/b&gt; [After JDT builder ends]&lt;/li&gt;
 &lt;li&gt;&lt;b&gt;PRE_LOCALIZATION&lt;/b&gt; [Before localization]&lt;/li&gt;
 &lt;li&gt;&lt;b&gt;POST_LOCALIZATION&lt;/b&gt; [After localization]&lt;/li&gt;
 &lt;li&gt;&lt;b&gt;PRE_PREVERIFICATION&lt;/b&gt; [Before preverifying]&lt;/li&gt;
 &lt;li&gt;&lt;b&gt;POST_PREVERIFICATION&lt;/b&gt; [After preverifying]&lt;/li&gt;
 &lt;li&gt;&lt;b&gt;POST_BUILD&lt;/b&gt; [After build process ends]&lt;/li&gt;
&lt;/ol&gt;
&lt;/p&gt;
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appInfo>
            <meta.element />
         </appInfo>
      </annotation>
      <complexType>
         <sequence minOccurs="1" maxOccurs="unbounded">
            <element ref="build-hook"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="build-hook">
      <annotation>
         <documentation>
            Adds a new build hook to the MTJ project&apos;s builder.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="hook" type="string" use="required">
            <annotation>
               <documentation>
                  The IMTJBuildHook implementation. This class represents the build hook and will be called during the build process.
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.mtj.core.build.IMTJBuildHook"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         1.0
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         Example of a declaration of a &lt;code&gt;Build Hook&lt;/code&gt;:
&lt;pre&gt;
 &lt;extension point=&quot;org.eclipse.mtj.core.mtjbuildhook&quot;&gt;
    &lt;build-hook
          hook=&quot;org.eclipse.mtj.core.build.MotoBuildHook&quot;&gt;
    &lt;/build-hook&gt;
 &lt;/extension&gt;
&lt;/pre&gt;
      </documentation>
   </annotation>


   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         The MTJ itself does not have any hooks predefined.
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="copyright"/>
      </appInfo>
      <documentation>
         Copyright (c) 2009 Motorola. &lt;br&gt;
All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at &lt;a 
href=&quot;http://www.eclipse.org/legal/epl-v10.html&quot;&gt;http://www.eclipse.org/legal/epl-v10.html&lt;/a&gt;
      </documentation>
   </annotation>

</schema>
