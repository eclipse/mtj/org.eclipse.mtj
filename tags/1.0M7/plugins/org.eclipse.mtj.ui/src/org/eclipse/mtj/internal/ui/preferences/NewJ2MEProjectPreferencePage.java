/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Change references from "J2ME projects" to 
 *                                "MIDlet projects"
 *     Diego Sandin (Motorola)  - Fixed incorrect behavior using the 
 *                                "Restore Defaults" button
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *                                
 */
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.util.ValueChangeTrackingBooleanFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

/**
 * Preference page implementation for setting new J2ME project preferences.
 * 
 * @author Craig Setera
 */
public class NewJ2MEProjectPreferencePage extends FieldEditorPreferencePage
        implements IWorkbenchPreferencePage, IMTJCoreConstants {

    /**
     * Default constructor.
     */
    public NewJ2MEProjectPreferencePage() {
        super(GRID);
        setPreferenceStore(MTJUIPlugin.getDefault().getCorePreferenceStore());
        setDescription(MTJUIMessages.NewJ2MEProjectPreferencePage_description);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
     */
    @Override
    public void createFieldEditors() {
        Composite parent = getFieldEditorParent();

        addField(new BooleanFieldEditor(
                PREF_FORCE_JAVA11,
                MTJUIMessages.NewJ2MEProjectPreferencePage_compliance_label_text,
                parent));

        ValueChangeTrackingBooleanFieldEditor useResourcesDirEditor = new ValueChangeTrackingBooleanFieldEditor(
                PREF_USE_RESOURCES_DIR,
                MTJUIMessages.NewJ2MEProjectPreferencePage_resource_dir_label_text,
                parent);
        StringFieldEditor resDirectoryEditor = new StringFieldEditor(
                PREF_RESOURCES_DIR,
                MTJUIMessages.NewJ2MEProjectPreferencePage_resource_dir_field_lable_text,
                parent);

        useResourcesDirEditor.setFieldEditor(resDirectoryEditor);

        addField(useResourcesDirEditor);
        addField(resDirectoryEditor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }

    /**
     * Overridden so that we get the help context where it belongs so that it
     * works when the focus is in the left pane.
     * 
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_J2MEPreferencePage"); //$NON-NLS-1$
        return (super.createContents(parent));
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performDefaults()
     */
    @Override
    protected void performDefaults() {
        super.performDefaults();
    }

}
