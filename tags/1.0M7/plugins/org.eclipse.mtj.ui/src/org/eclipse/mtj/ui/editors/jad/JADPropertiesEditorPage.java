/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Refactoring the page to support extensibility
 */
package org.eclipse.mtj.ui.editors.jad;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.mtj.core.project.midp.DescriptorPropertyDescription;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.editors.FormLayoutFactory;
import org.eclipse.mtj.internal.ui.editors.jad.form.JADAttributesRegistry;
import org.eclipse.mtj.internal.ui.editors.jad.form.JADFormEditor;
import org.eclipse.mtj.internal.ui.preferences.ExtendedStringFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;

/**
 * An editor part page that may be added to the JAD editor.
 * 
 * @since 1.0
 */
public abstract class JADPropertiesEditorPage extends AbstractJADEditorPage
        implements IPropertyChangeListener {

    /**
     * 
     */
    private Constructor<?> comboEditorConstructor;

    /**
     * To handle the change from the internal ComboFieldEditor used prior to
     * 3.3. This probably should have been better handled in the first place,
     * but now that it is moving to an externally available class, it isn't
     * worth replicating
     */
    private Class<?> comboFieldEditorClass;

    /** The descriptors being edited */
    private DescriptorPropertyDescription[] descriptors;

    /** The field editors in use */
    protected FieldEditor[] fieldEditors;

    /**
     * A constructor that creates the JAD Properties EditorPage and initializes
     * it with the editor.
     * 
     * @param editor the parent editor
     * @param id the unique identifier
     * @param title the page title
     * @noreference This constructor is not intended to be referenced by clients.
     */
    public JADPropertiesEditorPage(JADFormEditor editor, String id, String title) {
        super(editor, id, title);
    }

    /**
     * A constructor that creates the JAD Properties EditorPage. The parent
     * editor need to be passed in the <code>initialize</code> method if this
     * constructor is used.
     * 
     * @param id a unique page identifier
     * @param title a user-friendly page title
     */
    public JADPropertiesEditorPage(String id, String title) {
        super(id, title);
    }

    /**
     * Saves the contents of this part by storing all FieldEditor's values.
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor monitor) {
        monitor.setTaskName(getTitle());

        for (FieldEditor element : fieldEditors) {
            element.store();
        }
        setDirty(false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#editorInputChanged()
     */
    @Override
    public void editorInputChanged() {
        updateEditComponents();
    }

    /**
     * Get all application descriptor property description for the current page
     * {@link FormPage#getId() ID}.
     * 
     * @return the list of property descriptors for the current page ID
     */
    public DescriptorPropertyDescription[] getDescriptors() {
        if (descriptors == null) {
            descriptors = doGetDescriptors();
        }
        return descriptors;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#isManagingProperty(java.lang.String)
     */
    @Override
    public boolean isManagingProperty(String property) {
        boolean manages = false;
        for (int i = 0; i < getDescriptors().length; i++) {
            DescriptorPropertyDescription desc = getDescriptors()[i];
            if (property.equals(desc.getPropertyName())) {
                manages = true;
                break;
            }
        }
        return manages;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.util.IPropertyChangeListener#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
     */
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getProperty().equals(FieldEditor.VALUE)) {
            setDirty(true);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#setFocus()
     */
    @Override
    public void setFocus() {
        if (fieldEditors.length > 0) {
            fieldEditors[0].setFocus();
        }
    }

    /**
     * Create a new combo field editor.
     * 
     * @param toolkit
     * @param composite
     * @param description
     * @return
     */
    private FieldEditor createComboFieldEditor(FormToolkit toolkit,
            Composite composite, DescriptorPropertyDescription description) {

        if (comboFieldEditorClass == null) {
            initializeComboFieldEditorSupport();
        }

        ListDescriptorPropertyDescription listDescription = (ListDescriptorPropertyDescription) description;

        FieldEditor editor = null;
        try {
            editor = (FieldEditor) comboEditorConstructor
                    .newInstance(new Object[] {
                            listDescription.getPropertyName(),
                            listDescription.getDisplayName(),
                            listDescription.getNamesAndValues(), composite });

        } catch (IllegalArgumentException e) {
            MTJLogger.log(IStatus.ERROR, e);
        } catch (InstantiationException e) {
            MTJLogger.log(IStatus.ERROR, e);
        } catch (IllegalAccessException e) {
            MTJLogger.log(IStatus.ERROR, e);
        } catch (InvocationTargetException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }

        return editor;
    }

    /**
     * Create a new integer field editor.
     * 
     * @param toolkit
     * @param parent
     * @param descriptor
     * @return
     */
    private IntegerFieldEditor createIntegerFieldEditor(FormToolkit toolkit,
            Composite parent, DescriptorPropertyDescription descriptor) {

        IntegerFieldEditor integerEditor = new IntegerFieldEditor(descriptor
                .getPropertyName(), descriptor.getDisplayName(), parent);
        toolkit.adapt(integerEditor.getTextControl(parent), true, true);

        return integerEditor;
    }

    /**
     * Create a new String field editor.
     * 
     * @param toolkit
     * @param parent
     * @param descriptor
     * @return
     */
    private ExtendedStringFieldEditor createStringFieldEditor(FormToolkit toolkit,
            Composite parent, DescriptorPropertyDescription descriptor) {
        ExtendedStringFieldEditor editor = new ExtendedStringFieldEditor(descriptor
                .getPropertyName(), descriptor.getDisplayName(), parent);
        toolkit.adapt(editor.getTextControl(parent), true, true);

        return editor;
    }

    /**
     * Resolve the appropriate combo field editor class to be used dependent on
     * the version.
     */
    private void initializeComboFieldEditorSupport() {
        String[] names = new String[] {
                "org.eclipse.jdt.internal.debug.ui.launcher.ComboFieldEditor",
                "org.eclipse.jface.preference.ComboFieldEditor", };

        for (int i = 0; (comboFieldEditorClass == null) && (i < names.length); i++) {
            String name = names[i];
            try {
                comboFieldEditorClass = Class.forName(name);
                comboEditorConstructor = comboFieldEditorClass
                        .getConstructors()[0];
            } catch (ClassNotFoundException e) {
            } catch (SecurityException e) {
                MTJLogger.log(IStatus.ERROR, e);
            }
        }
    }

    /**
     * Update the application descriptor the components are handling
     */
    private void updateEditComponents() {
        if (fieldEditors != null) {
            IPreferenceStore store = (IPreferenceStore) getPreferenceStore();
            for (FieldEditor fieldEditor : fieldEditors) {
                fieldEditor.setPreferenceStore(store);
                fieldEditor.load();
            }
        }
    }

    /**
     * Sets the help context on the given control.
     * 
     * @param control the control on which to register the context help
     */
    protected abstract void addContextHelp(Composite control);

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createFormContent(IManagedForm managedForm) {
        super.createFormContent(managedForm);

        FormToolkit toolkit = managedForm.getToolkit();

        Composite body = managedForm.getForm().getBody();
        body.setLayout(FormLayoutFactory.createFormTableWrapLayout(true, 1));

        Composite center = toolkit.createComposite(body);
        center.setLayout(FormLayoutFactory.createFormPaneTableWrapLayout(false,
                1));
        center.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB));

        Section section = createStaticBasicSection(toolkit, center,
                getSectionTitle(), getSectionDescription());

        Composite sectionClient = createStaticSectionClient(toolkit, section);
        sectionClient.setLayout(FormLayoutFactory
                .createSectionClientTableWrapLayout(false, 1));

        createSectionContent(managedForm, sectionClient, this);
        section.setClient(sectionClient);
    }

    /**
     * Create a Section Client Content for the descriptors available in the
     * page.
     * 
     * @param managedForm
     * @param composite
     * @param propertyChangeListener
     */
    protected void createSectionContent(IManagedForm managedForm,
            Composite composite, IPropertyChangeListener propertyChangeListener) {

        FormToolkit toolkit = managedForm.getToolkit();

        composite.setLayout(FormLayoutFactory.createSectionClientGridLayout(
                false, 2));

        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);

        DescriptorPropertyDescription[] theDescriptors = getDescriptors();
        fieldEditors = new FieldEditor[theDescriptors.length];

        for (int i = 0; i < theDescriptors.length; i++) {

            switch (theDescriptors[i].getDataType()) {

            case DescriptorPropertyDescription.DATATYPE_INT:
                fieldEditors[i] = createIntegerFieldEditor(toolkit, composite,
                        theDescriptors[i]);
                break;

            case DescriptorPropertyDescription.DATATYPE_LIST:
                fieldEditors[i] = createComboFieldEditor(toolkit, composite,
                        theDescriptors[i]);
                break;

            case DescriptorPropertyDescription.DATATYPE_URL:
            case DescriptorPropertyDescription.DATATYPE_STRING:
            default:
                fieldEditors[i] = createStringFieldEditor(toolkit, composite,
                        theDescriptors[i]);
                break;
            }

            Label label = fieldEditors[i].getLabelControl(composite);
            toolkit.adapt(label, false, false);

            // Listen for property change events on the editor
            fieldEditors[i].setPropertyChangeListener(propertyChangeListener);
        }

        // Adapt the Combo instances...
        Control[] children = composite.getChildren();
        for (Control control : children) {
            if (control instanceof Combo) {
                toolkit.adapt(control, false, false);
            }
        }

        updateEditComponents();
        addContextHelp(composite);
    }

    /**
     * sub class may override this method to provide its own descriptor provide
     * strategy
     * 
     * @return
     */
    protected DescriptorPropertyDescription[] doGetDescriptors() {
        return JADAttributesRegistry.getJADAttrDescriptorsByPage(getId());
    }

    /**
     * @return
     */
    protected abstract String getSectionDescription();

    /**
     * @return
     */
    protected abstract String getSectionTitle();

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
     */
    @Override
    protected void setInput(IEditorInput input) {
        super.setInput(input);
        updateEditComponents();
        setDirty(false);
    }

}
