/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Always enable the preverifier browse button
 *     Hugo Raniere (Motorola)  - Adding message to indicate default preverifier 
 *                                usage
 *     Diego Sandin (Motorola)   - Use Eclipse Message Bundles [Bug 255874]
 *     Gustavo de Paula (Motorola)   - Preverifier api refactoring     
 */
package org.eclipse.mtj.internal.ui.editors.device.pages;

import java.io.File;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * A composite implementation for the editing of the basic properties of a
 * device.
 * 
 * @author Craig Setera
 */
public class DeviceBasicEditorPage extends AbstractDeviceEditorPage {

    /**
     * Content provider for boolean types
     */
    private static class BooleanContentProvider implements
            IStructuredContentProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            return BOOLEAN_VALUES;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    private static class BooleanLabelProvider extends LabelProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
         */
        @Override
        public String getText(Object element) {
            Boolean bool = (Boolean) element;
            return bool.booleanValue() ? MTJUIMessages.DeviceBasicEditorPage_yes_label
                    : MTJUIMessages.DeviceBasicEditorPage_no_label;
        }
    }

    private static final Boolean[] BOOLEAN_VALUES = new Boolean[] {
            Boolean.TRUE, Boolean.FALSE };

    private ComboViewer debugServerCombo;
    private Text descriptionText;
    private Button executableBrowseButton;
    private Text executableText;
    private Text groupText;
    private boolean javaExecutableDevice;
    private Text launchCommandText;

    // The widgets
    private Text nameText;
    private boolean preverifierEnabled;
    private Button preverifyBrowseButton;
    private Text preverifyExecutableText;

    /**
     * Construct a new instance of the page.
     * 
     * @param parent
     * @param style
     */
    public DeviceBasicEditorPage(Composite parent,
            boolean javaExecutableDevice, int style) {
        super(parent, style);
        this.javaExecutableDevice = javaExecutableDevice;
        preverifierEnabled = true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#commitDeviceChanges()
     */
    @Override
    public void commitDeviceChanges() throws CoreException {
        editDevice.setName(nameText.getText());
        editDevice.setGroupName(groupText.getText());
        editDevice.setDescription(descriptionText.getText());

        if (!javaExecutableDevice) {
            editDevice.setExecutable(new File(executableText.getText()));
        }

        editDevice.setDebugServer(getBooleanSelection(debugServerCombo));
        editDevice.setLaunchCommandTemplate(launchCommandText.getText());

        if (preverifierEnabled) {
            File exe = new File(preverifyExecutableText.getText());
            if (exe.exists()) {
            	IPreverifier standard = MTJCore.createPreverifier(IPreverifier.PREVERIFIER_STANDARD, exe);
                editDevice.setPreverifier(standard);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#getDescription()
     */
    @Override
    public String getDescription() {
        return MTJUIMessages.DeviceBasicEditorPage_description;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#getTitle()
     */
    @Override
    public String getTitle() {
        return MTJUIMessages.DeviceBasicEditorPage_title;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#setDevice(org.eclipse.mtj.core.model.device.IDevice)
     */
    @Override
    public void setDevice(IMIDPDevice device) {
        super.setDevice(device);
        initializeDeviceState();
    }

    /**
     * Create a new viewer for boolean value selection.
     * 
     * @param parent
     * @return
     */
    private ComboViewer createBooleanComboViewer(Composite parent) {
        ComboViewer viewer = new ComboViewer(parent, SWT.READ_ONLY);
        viewer.setContentProvider(new BooleanContentProvider());
        viewer.setLabelProvider(new BooleanLabelProvider());
        viewer.setInput(new Object());

        return viewer;
    }

    /**
     * Get the boolean combo viewer selection as a boolean value.
     * 
     * @param viewer
     * @return
     */
    private boolean getBooleanSelection(ComboViewer viewer) {
        IStructuredSelection selection = (IStructuredSelection) viewer
                .getSelection();
        Boolean value = (Boolean) selection.getFirstElement();
        return value.booleanValue();
    }

    /**
     * Initialize the state of the widgets based on the state of the device
     */
    private void initializeDeviceState() {
        if ((nameText != null) && (editDevice != null)) {
            nameText.setText(editDevice.getName());
            groupText.setText(editDevice.getSDKName());
            descriptionText.setText(editDevice.getDescription());

            String exeText = javaExecutableDevice ? "Java" : editDevice //$NON-NLS-1$
                    .getExecutable().toString();
            executableText.setText(exeText);
            executableText.setEnabled(!javaExecutableDevice);
            executableBrowseButton.setEnabled(!javaExecutableDevice);

            setBooleanSelection(debugServerCombo, editDevice.isDebugServer());
            launchCommandText.setText(editDevice.getLaunchCommandTemplate());

            preverifierEnabled = true;
            IPreverifier preverifier = editDevice.getPreverifier();

            preverifierEnabled = true;

            preverifyExecutableText
                    .setText(preverifier != null ? preverifier
                            .getPreverifierExecutable().toString()
                            : Utils.EMPTY_STRING);

            preverifyBrowseButton.setEnabled(preverifierEnabled);
            preverifyExecutableText.setEnabled(preverifierEnabled);
        }
    }

    /**
     * Set the boolean combo viewer selection to the specified value.
     * 
     * @param viewer
     * @param value
     */
    private void setBooleanSelection(ComboViewer viewer, boolean value) {
        StructuredSelection selection = new StructuredSelection(Boolean
                .valueOf(value));
        viewer.setSelection(selection, true);
    }

    /**
     * Update the state of this page.
     */
    private void updateState() {
        String errorMessage = null;

        if (nameText.getText().length() == 0) {
            errorMessage = MTJUIMessages.DeviceBasicEditorPage_error_no_device_name;
        } else if (groupText.getText().length() == 0) {
            errorMessage = MTJUIMessages.DeviceBasicEditorPage_error_no_device_group;
        } else if (launchCommandText.getText().length() == 0) {
            errorMessage = MTJUIMessages.DeviceBasicEditorPage_error_no_launch_command;
        } else {
            File executable = new File(executableText.getText());
            if (!javaExecutableDevice && !executable.exists()) {
                errorMessage = MTJUIMessages.DeviceBasicEditorPage_error_invalid_executable;
            } else {
                executable = new File(preverifyExecutableText.getText());
                if (!executable.exists()) {
                    executable = new File(preverifyExecutableText.getText()
                            + ".exe"); //$NON-NLS-1$
                    if (!executable.exists()) {
                        errorMessage = MTJUIMessages.DeviceBasicEditorPage_error_invalid_preverifier;
                    }
                }
            }
        }

        setErrorMessage(errorMessage);
        setValid(errorMessage == null);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#addPageControls(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void addPageControls(Composite parent) {
        ModifyListener modifyListener = new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                updateState();
            }
        };

        parent.setLayoutData(new GridData(GridData.FILL_BOTH));
        parent.setLayout(new GridLayout(3, false));

        (new Label(parent, SWT.NONE))
                .setText(MTJUIMessages.DeviceBasicEditorPage_name_label);
        nameText = new Text(parent, SWT.BORDER);
        nameText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        nameText.addModifyListener(modifyListener);
        new Label(parent, SWT.NONE);

        (new Label(parent, SWT.NONE))
                .setText(MTJUIMessages.DeviceBasicEditorPage_group_label);
        groupText = new Text(parent, SWT.BORDER);
        groupText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        groupText.addModifyListener(modifyListener);
        new Label(parent, SWT.NONE);

        (new Label(parent, SWT.NONE))
                .setText(MTJUIMessages.DeviceBasicEditorPage_description_label);
        descriptionText = new Text(parent, SWT.BORDER);
        descriptionText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        new Label(parent, SWT.NONE);

        (new Label(parent, SWT.NONE))
                .setText(MTJUIMessages.DeviceBasicEditorPage_executable_label);
        executableText = new Text(parent, SWT.BORDER);
        executableText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        executableText.addModifyListener(modifyListener);

        executableBrowseButton = new Button(parent, SWT.PUSH);
        executableBrowseButton
                .setText(MTJUIMessages.DeviceBasicEditorPage_executableBrowseButton_label);
        executableBrowseButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                browseForExecutable(executableText);
            }
        });

        (new Label(parent, SWT.NONE))
                .setText(MTJUIMessages.DeviceBasicEditorPage_preverifier_label);
        preverifyExecutableText = new Text(parent, SWT.BORDER);
        preverifyExecutableText.setLayoutData(new GridData(
                GridData.FILL_HORIZONTAL));
        preverifyExecutableText.addModifyListener(modifyListener);

        preverifyBrowseButton = new Button(parent, SWT.PUSH);
        preverifyBrowseButton
                .setText(MTJUIMessages.DeviceBasicEditorPage_preverifyBrowseButton_label);
        preverifyBrowseButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                browseForExecutable(preverifyExecutableText);
            }
        });

        new Label(parent, SWT.NONE); // blank label on 1st cell.
        Label preverifierTip = new Label(parent, SWT.WRAP);
        preverifierTip
                .setText(MTJUIMessages.DeviceBasicEditorPage_preverifierTip);
        GridData gd = new GridData();
        gd.horizontalSpan = 2;
        preverifierTip.setLayoutData(gd);

        Label blankLabel1 = new Label(parent, SWT.NONE);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 3;
        blankLabel1.setLayoutData(gd);

        (new Label(parent, SWT.NONE))
                .setText(MTJUIMessages.DeviceBasicEditorPage_debug_server_label);
        debugServerCombo = createBooleanComboViewer(parent);
        debugServerCombo.getCombo().setLayoutData(
                new GridData(GridData.FILL_HORIZONTAL));
        new Label(parent, SWT.NONE);

        Label blankLabel2 = new Label(parent, SWT.NONE);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 3;
        blankLabel2.setLayoutData(gd);

        Label launchCommandLabel = new Label(parent, SWT.NONE);
        launchCommandLabel
                .setText(MTJUIMessages.DeviceBasicEditorPage_launchCommand_label);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 3;
        launchCommandLabel.setLayoutData(gd);

        launchCommandText = new Text(parent, SWT.MULTI | SWT.WRAP | SWT.BORDER);
        launchCommandText.setTextLimit(1000);
        launchCommandText.addModifyListener(modifyListener);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 3;
        gd.widthHint = 500;
        gd.heightHint = 300;
        launchCommandText.setLayoutData(gd);

        initializeDeviceState();
    }

    /**
     * Browse the file system for an executable. Use the value in the specified
     * text field to set a starting point for the browser. Store the result in
     * the specified text field.
     * 
     * @param textField
     */
    protected void browseForExecutable(Text textField) {
        FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);
        dialog
                .setText(MTJUIMessages.DeviceBasicEditorPage_browseForExecutable_dialog_title);

        File currentFile = new File(textField.getText());
        while (currentFile != null) {
            if (currentFile.exists()) {
                dialog.setFileName(currentFile.toString());
                break;
            } else {
                currentFile = currentFile.getParentFile();
            }
        }

        String filename = dialog.open();
        if (filename != null) {
            textField.setText(filename);
        }
    }
}
