/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     David Marques (Motorola) - Synchronizing key states with outline. 
 *     David Marques (Motorola) - Adding call to updateMessageManager method.
 */
package org.eclipse.mtj.internal.ui.editors.l10n.details;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.mtj.internal.core.text.l10n.L10nEntry;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.editor.FormEntryAdapter;
import org.eclipse.mtj.internal.ui.editors.l10n.L10nInputContext;
import org.eclipse.mtj.internal.ui.editors.l10n.LocalesTreeSection;
import org.eclipse.mtj.internal.ui.editors.l10n.LocalizationDataEditor;
import org.eclipse.mtj.internal.ui.forms.parts.FormEntry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.IFormPart;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.events.HyperlinkEvent;

public class L10nEntryDetails extends L10nAbstractDetails {

    private FormEntry keyEntry;

    private L10nEntry localeEntry;
    private FormEntry valueEntry;

    /**
     * @param masterSection
     */
    public L10nEntryDetails(LocalesTreeSection masterSection) {
        super(masterSection, L10nInputContext.CONTEXT_ID);
        localeEntry = null;

        keyEntry = null;
        valueEntry = null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.AbstractFormPart#commit(boolean)
     */
    @Override
    public void commit(boolean onSave) {
        super.commit(onSave);
        // Only required for form entries
        keyEntry.commit();
        valueEntry.commit();

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#createFields(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createFields(Composite parent) {
        createKeyWidget(parent);
        createSpace(parent);
        createValueWidget(parent);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#hookListeners()
     */
    @Override
    public void hookListeners() {
        createLocaleEntryListeners();
        createPageEntryListeners();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#selectionChanged(org.eclipse.ui.forms.IFormPart, org.eclipse.jface.viewers.ISelection)
     */
    @Override
    public void selectionChanged(IFormPart part, ISelection selection) {
        // Get the first selected object
        Object object = getFirstSelectedObject(selection);
        // Ensure we have the right type
        if ((object != null) && (object instanceof L10nEntry)) {
            // Set data
            setData((L10nEntry) object);
            // Update the UI given the new data
            updateFields();
        }
    }

    /**
     * @param object
     */
    public void setData(L10nEntry object) {
        // Set data
        localeEntry = object;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#updateFields()
     */
    @Override
    public void updateFields() {
        // Ensure data object is defined
        if (localeEntry != null) { // Update name entry
            updateKeyEntry(isEditableElement());
            updateValueEntry(isEditableElement());
        }
    }

    /**
     * 
     */
    private void createLocaleEntryListeners() {
        keyEntry.setFormEntryListener(new FormEntryAdapter(this) {
            
            public void textValueChanged(FormEntry entry) {
                // Ensure data object is defined
                if (localeEntry != null) {
                    {
                        localeEntry.setKey(keyEntry.getValue());
                        localeEntry.getParent().validate();
                        
                        FormEditor editor = getPage().getEditor();
                        if (editor instanceof LocalizationDataEditor) {
                            ((LocalizationDataEditor) editor).updateMessageManager(localeEntry.getLocales());
                        }
                    }
                }
            }
        });
    }

    /**
     * @param parent
     */
    private void createKeyWidget(Composite parent) {
        createLabel(parent, getManagedForm().getToolkit(), MTJUIMessages.L10nEntryDetails_keyWidgetLabel);

        keyEntry = new FormEntry(parent, getManagedForm().getToolkit(),
                MTJUIMessages.L10nEntryDetails_keyEntry_label, SWT.NONE);
    }

    /**
     * 
     */
    private void createPageEntryListeners() {
        valueEntry.setFormEntryListener(new FormEntryAdapter(this) {

            /* (non-Javadoc)
             * @see org.eclipse.mtj.internal.ui.editor.FormEntryAdapter#browseButtonSelected(org.eclipse.mtj.internal.ui.forms.parts.FormEntry)
             */
            @Override
            public void browseButtonSelected(FormEntry entry) {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.internal.ui.editor.FormEntryAdapter#linkActivated(org.eclipse.ui.forms.events.HyperlinkEvent)
             */
            @Override
            public void linkActivated(HyperlinkEvent e) {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.internal.ui.editor.FormEntryAdapter#textValueChanged(org.eclipse.mtj.internal.ui.forms.parts.FormEntry)
             */
            @Override
            public void textValueChanged(FormEntry entry) { // Ensure data
                // object is defined
                if (localeEntry != null) {
                    localeEntry.setValue(valueEntry.getValue());
                }
            }
        });
    }

    /**
     * @param parent
     */
    private void createValueWidget(Composite parent) {
        createLabel(parent, getManagedForm().getToolkit(), MTJUIMessages.L10nEntryDetails_valueWidget_label);

        valueEntry = new FormEntry(parent, getManagedForm().getToolkit(),
                MTJUIMessages.L10nEntryDetails_valueEntry_label, SWT.NONE);
    }

    /**
     * @param editable
     */
    private void updateKeyEntry(boolean editable) {
        keyEntry.setValue(localeEntry.getKey(), true);
        keyEntry.setEditable(editable);
    }

    /**
     * @param editable
     */
    private void updateValueEntry(boolean editable) {
        valueEntry.setValue(localeEntry.getValue(), true);
        valueEntry.setEditable(editable);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#getDetailsDescription()
     */
    @Override
    protected String getDetailsDescription() {
        return MTJUIMessages.L10nEntryDetails_detailsDescription;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#getDetailsTitle()
     */
    @Override
    protected String getDetailsTitle() {
        return MTJUIMessages.L10nEntryDetails_detailsTitle;
    }

}
