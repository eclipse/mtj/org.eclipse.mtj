/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.wizards.importer.eclipseme;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

public class ImportEclipseMEProjectWizard extends Wizard implements
		IImportWizard {
	private static final String EclipseME_PROJECT_IMPORT_SECTION = "EclipseMEProjectImportWizard";//$NON-NLS-1$
	private ImportEclipseMEProjectWizardPage mainPage;

	public ImportEclipseMEProjectWizard() {
		super();
		setNeedsProgressMonitor(true);
		IDialogSettings workbenchSettings = MTJUIPlugin.getDefault()
				.getDialogSettings();

		IDialogSettings wizardSettings = workbenchSettings
				.getSection(EclipseME_PROJECT_IMPORT_SECTION);
		if (wizardSettings == null) {
			wizardSettings = workbenchSettings
					.addNewSection(EclipseME_PROJECT_IMPORT_SECTION);
		}
		setDialogSettings(wizardSettings);
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle(ProjectImporterMessage.WizardProjectsImportPage_ImportProjectsTitle);
	}

	@Override
	public void addPages() {
		super.addPages();
		mainPage = new ImportEclipseMEProjectWizardPage();
		addPage(mainPage);
	}

	@Override
	public boolean performCancel() {
		mainPage.performCancel();
		return true;
	}

	@Override
	public boolean performFinish() {
		return mainPage.createProjects();
	}

}
