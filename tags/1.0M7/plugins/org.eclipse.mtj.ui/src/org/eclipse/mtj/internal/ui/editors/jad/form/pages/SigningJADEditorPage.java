/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Fixing page scrolling.
 *     David Marques (Motorola) - Updating to support signing enhancements.
 *     David Marques (Motorola) - Updating to reload aliases after external changes.
 *     David Marques (Motorola) - Avoiding disabling the permissions list upon disabling
 *                                signing.
 *     David Marques (Motorola) - Adding support for certificates.
 */
package org.eclipse.mtj.internal.ui.editors.jad.form.pages;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProjectListener;
import org.eclipse.mtj.internal.core.build.sign.SignatureProperties;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.sign.DefaultKeyStoreManager;
import org.eclipse.mtj.internal.core.sign.IKeyStoreManager;
import org.eclipse.mtj.internal.core.sign.KeyStoreEntry;
import org.eclipse.mtj.internal.core.sign.KeyStoreManagerException;
import org.eclipse.mtj.internal.core.sign.PermissionsGroup;
import org.eclipse.mtj.internal.core.sign.SecurityPermissionsScanner;
import org.eclipse.mtj.internal.core.sign.KeyStoreEntry.Type;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.dialog.SecurityPermissionsDialog;
import org.eclipse.mtj.internal.ui.dialog.SigningPasswordDialog;
import org.eclipse.mtj.internal.ui.dialog.SecurityPermissionsDialog.PermissionNode;
import org.eclipse.mtj.internal.ui.editors.jad.form.JADFormEditor;
import org.eclipse.mtj.internal.ui.forms.blocks.ButtonBarBlock;
import org.eclipse.mtj.internal.ui.forms.blocks.GenericListBlock;
import org.eclipse.mtj.internal.ui.forms.blocks.GenericListBlockItem;
import org.eclipse.mtj.internal.ui.forms.blocks.GenericListBlockListener;
import org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.dialogs.CheckedTreeSelectionDialog;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

/**
 * SigningJADEditorPage class adds a signing page to the JAD editor.
 * 
 * @author David Marques
 * @since 1.0
 */
public class SigningJADEditorPage extends AbstractJADEditorPage {

    private static enum ListId {
        Optional, Required
    }

    private class SecurityPermissionListItem implements GenericListBlockItem {

        private String permission;

        /**
         * Creates a new instance of SecurityPermissionListItem.
         * 
         * @param _permission
         */
        public SecurityPermissionListItem(String _permission) {
            this.permission = _permission;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        public boolean equals(Object obj) {
            if (obj instanceof SecurityPermissionListItem) {
                SecurityPermissionListItem other = (SecurityPermissionListItem) obj;
                return this.getText().equals(other.getText());
            } else {
                return super.equals(obj);
            }
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.forms.blocks.GenericListBlockItem#getImage()
         */
        public Image getImage() {
            return MTJUIPluginImages.DESC_PERMISSION_OBJ.createImage();
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.forms.blocks.GenericListBlockItem#getText()
         */
        public String getText() {
            return this.permission;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        public int hashCode() {
            String text = this.getText();
            int hash = 0x00;
            for (int i = 0; i < text.length(); i++) {
                hash += text.charAt(i);
            }
            return hash;
        }
    }

    public static final String ID = "signing"; //$NON-NLS-1$
    private static final String COMMA = ","; //$NON-NLS-1$

    private static final String TITLE = MTJUIMessages.PermissionsPage_title;
    private Combo combo;
    private Button enableButton;
    private GenericListBlock<SecurityPermissionListItem> optPermissionBlock;
    private List<SecurityPermissionListItem> optPermissions;
    private IMidletSuiteProjectListener projectListener;

    private GenericListBlock<SecurityPermissionListItem> reqPermissionBlock;

    private List<SecurityPermissionListItem> reqPermissions;

    /**
     * Creates a SigningJADEditorPage instance.
     * 
     * @param editor the parent editor
     */
    public SigningJADEditorPage(JADFormEditor editor) {
        super(editor, ID, TITLE);

        reqPermissions = new LinkedList<SecurityPermissionListItem>();
        optPermissions = new LinkedList<SecurityPermissionListItem>();
        listenForProjectSignatureUpdates();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#dispose()
     */
    public void dispose() {
        super.dispose();
        IMidletSuiteProject midletSuiteProject = MidletSuiteFactory
                .getMidletSuiteProject(getJavaProject());
        if (midletSuiteProject != null) {
            midletSuiteProject.removeMTJProjectListener(this.projectListener);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    public void doSave(IProgressMonitor monitor) {
        IPreferenceStore store = getPreferenceStore();

        String requiredPermissions = getPermissionsListAsString(ListId.Required);
        store.setValue(IJADConstants.JAD_MIDLET_PERMISSIONS,
                requiredPermissions);

        String optionalPermissions = getPermissionsListAsString(ListId.Optional);
        store.setValue(IJADConstants.JAD_MIDLET_PERMISSIONS_OPTIONAL,
                optionalPermissions);

        this.updateProjectSigningState();
        setDirty(false);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#editorInputChanged()
     */
    public void editorInputChanged() {
        IPreferenceStore store = getPreferenceStore();
        this.reqPermissions.clear();
        this.optPermissions.clear();

        String reqPermissions = store
                .getString(IJADConstants.JAD_MIDLET_PERMISSIONS);
        if (reqPermissions.length() > 0x00) {
            String[] permissions = reqPermissions.split(COMMA);
            for (String permission : permissions) {
                this.reqPermissions.add(new SecurityPermissionListItem(
                        permission));
            }
        }

        String optPermissions = store
                .getString(IJADConstants.JAD_MIDLET_PERMISSIONS_OPTIONAL);
        if (optPermissions.length() > 0x00) {
            String[] permissions = optPermissions.split(COMMA);
            for (String permission : permissions) {
                this.optPermissions.add(new SecurityPermissionListItem(
                        permission));
            }
        }
        if (this.reqPermissionBlock != null) {
            this.reqPermissionBlock.update();
        }
        if (this.optPermissionBlock != null) {
            this.optPermissionBlock.update();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getTitle()
     */
    public String getTitle() {
        return TITLE;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#isManagingProperty(java.lang.String)
     */
    public boolean isManagingProperty(String property) {
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormPage#setActive(boolean)
     */
    public void setActive(boolean active) {
        super.setActive(active);
        if (active) {
            this.getErrorMessageManager().removeAllMessages();
        }
    }

    /**
     * Opens a SecurityPermissionsDialog and adds the selected permissions to
     * the list with the specified id.
     * 
     * @param listId if of the list.
     */
    private void addPermissions(ListId listId) {
        CheckedTreeSelectionDialog dialog = SecurityPermissionsDialog
                .createDialog(getPartControl().getShell());
        if (dialog.open() == Dialog.OK) {
            Object[] result = dialog.getResult();

            GenericListBlock<SecurityPermissionListItem> block = null;
            List<SecurityPermissionListItem> list = null;
            switch (listId) {
                case Required:
                    block = reqPermissionBlock;
                    list = reqPermissions;
                    break;
                case Optional:
                    block = optPermissionBlock;
                    list = optPermissions;
                    break;
            }

            if (list != null) {
                addPermissionsToList(list, result);
                block.update();
            }
        }
    }

    /**
     * Adds the permission to the specified list.
     * 
     * @param permissionsList target list.
     * @param result selected items.
     */
    private void addPermissionsToList(
            List<SecurityPermissionListItem> permissionsList, Object[] result) {
        for (Object object : result) {
            if (object instanceof PermissionNode) {
                boolean isNew = true;
                String permission = ((PermissionNode) object).getPermission();
                for (SecurityPermissionListItem item : permissionsList) {
                    if (item.getText().equals(permission)) {
                        isNew = false;
                        break;
                    }
                }
                if (isNew) {
                    permissionsList.add(new SecurityPermissionListItem(
                            permission));
                }
            }
        }
        setDirty(true);
    }

    /**
     * Creates the section for the alias selection.
     * 
     * @param managedForm parent form.
     * @param body parent composite.
     */
    private void createAliasSection(IManagedForm managedForm, Composite body) {
        FormToolkit toolkit = managedForm.getToolkit();

        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gridData.horizontalSpan = 2;

        Composite sectionBody = createSection(managedForm, body,
                MTJUIMessages.PermissionsPage_signProperties, null, gridData);
        sectionBody.setLayout(new GridLayout(2, false));

        toolkit
                .createLabel(sectionBody,
                        MTJUIMessages.PermissionsPage_keyAlias);
        combo = new Combo(sectionBody, SWT.DROP_DOWN | SWT.READ_ONLY);
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        combo.setLayoutData(gridData);
        combo.setFocus();
        combo.addSelectionListener(new SelectionListener() {

            public void widgetDefaultSelected(SelectionEvent e) {
            }

            public void widgetSelected(SelectionEvent e) {
                setDirty(true);
            }
        });
    }

    /**
     * Creates the section for the permissions lists.
     * 
     * @param managedForm parent form.
     * @param body parent composite.
     */
    private void createListsSection(IManagedForm managedForm, Composite body) {
        Composite reqSectionBody = createSection(managedForm, body,
                MTJUIMessages.PermissionsPage_requiredPermissionsTitle,
                MTJUIMessages.PermissionsPage_requiredPermissionsMessage,
                new GridData(SWT.FILL, SWT.FILL, true, true));
        reqSectionBody.setLayout(new GridLayout(0x01, false));

        reqPermissionBlock = new GenericListBlock<SecurityPermissionListItem>(
                reqSectionBody, SWT.BORDER | SWT.MULTI,
                ButtonBarBlock.BUTTON_ALL, reqPermissions);
        reqPermissionBlock
                .addGenericListBlockListener(new GenericListBlockListener<SecurityPermissionListItem>() {

                    public void addButtonPressed() {
                        addPermissions(ListId.Required);
                    }

                    public void downButtonPressed() {
                        setDirty(true);
                    }

                    public void itemsRemoved(
                            List<SecurityPermissionListItem> _items) {
                        setDirty(true);
                    }

                    public void scan() {
                        scanProjectClasses(ListId.Required);
                    }

                    public void upButtonPressed() {
                        setDirty(true);
                    }
                });

        Composite optSectionBody = createSection(managedForm, body,
                MTJUIMessages.PermissionsPage_optionalPermissionsTitle,
                MTJUIMessages.PermissionsPage_optionalPermissionsMessage,
                new GridData(SWT.FILL, SWT.FILL, true, true));
        optSectionBody.setLayout(new GridLayout(0x01, false));

        optPermissionBlock = new GenericListBlock<SecurityPermissionListItem>(
                optSectionBody, SWT.BORDER | SWT.MULTI,
                ButtonBarBlock.BUTTON_ALL, optPermissions);
        optPermissionBlock
                .addGenericListBlockListener(new GenericListBlockListener<SecurityPermissionListItem>() {

                    public void addButtonPressed() {
                        addPermissions(ListId.Optional);
                    }

                    public void downButtonPressed() {
                        setDirty(true);
                    }

                    public void itemsRemoved(
                            List<SecurityPermissionListItem> _items) {
                        setDirty(true);
                    }

                    public void scan() {
                        scanProjectClasses(ListId.Optional);
                    }

                    public void upButtonPressed() {
                        setDirty(true);
                    }
                });

    }

    /**
     * Creates a generic section.
     * 
     * @param managedForm parent form.
     * @param body parent composite.
     */
    private Composite createSection(IManagedForm managedForm,
            Composite listsParent, String text, String description,
            GridData gridData) {
        FormToolkit toolkit = managedForm.getToolkit();

        Section section = toolkit.createSection(listsParent,
                Section.DESCRIPTION | ExpandableComposite.TITLE_BAR);
        if (text != null) {
            section.setText(text);
        }
        if (description != null) {
            section.setDescription(description);
        }
        section.setLayoutData(gridData);
        section.setLayout(new GridLayout(0x01, false));

        Composite client = toolkit.createComposite(section, SWT.NONE);
        client.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        section.setClient(client);
        return client;
    }

    /**
     * Creates the section for the top section.
     * 
     * @param managedForm parent form.
     * @param body parent composite.
     */
    private void createTopSection(IManagedForm managedForm, Composite body) {
        FormToolkit toolkit = managedForm.getToolkit();
        enableButton = toolkit.createButton(body,
                MTJUIMessages.PermissionsPage_signPackage, SWT.CHECK);
        enableButton.addSelectionListener(new SelectionListener() {

            public void widgetDefaultSelected(SelectionEvent e) {
            }

            public void widgetSelected(SelectionEvent e) {
                updateWidgetsStates(enableButton.getSelection());
                setDirty(true);
            }
        });

        ISignatureProperties signProperties = getProjectSignatureProperties();
        if (signProperties != null) {
            enableButton.setSelection(signProperties.isSignProject());
        }

        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        gridData.horizontalSpan = 2;
        enableButton.setLayoutData(gridData);
    }

    /**
     * Gets the permissions from the list as String.
     * 
     * @param listId id of the target list.
     * @return permissions list String.
     */
    private String getPermissionsListAsString(ListId listId) {
        StringBuffer buffer = new StringBuffer();

        List<SecurityPermissionListItem> list = null;
        switch (listId) {
            case Required:
                list = reqPermissions;
                break;
            case Optional:
                list = optPermissions;
                break;
        }

        if (list != null) {
            for (SecurityPermissionListItem item : list) {
                if (buffer.length() > 0) {
                    buffer.append(COMMA);
                }

                buffer.append(item.getText().trim());
            }
        }

        return buffer.toString();
    }

    /**
     * Gets the signature properties for the project.
     * 
     * @return ISignatureProperties instance.
     */
    private ISignatureProperties getProjectSignatureProperties() {
        ISignatureProperties signProperties = null;
        IMidletSuiteProject midletProject = null;

        try {
            midletProject = MidletSuiteFactory
                    .getMidletSuiteProject(getJavaProject());
            signProperties = midletProject.getSignatureProperties();
        } catch (CoreException e) {
            String message = NLS.bind(
                    "Unable to get project signing properties: {0}", e
                            .getMessage());
            MTJLogger.log(IStatus.ERROR, message);
            getErrorMessageManager().addMessage(message, message, null,
                    IMessageProvider.ERROR);
        }
        return signProperties;
    }

    /**
     * List all aliases into the combo box.
     * 
     * @param combo target combo.
     * @throws KeyStoreManagerException
     */
    private void listAliases(final Combo combo) {
        Display.getDefault().asyncExec(new Runnable() {
            public void run() {
                ISignatureProperties properties = null;
                String password = null;
                String path = null;
                File file = null;

                combo.removeAll();
                properties = getProjectSignatureProperties();
                if (properties == null) {
                    return;
                }

                try {
                    path = properties.getAbsoluteKeyStorePath(getJavaProject()
                            .getProject());
                    if (path == null) {
                        return;
                    }
                } catch (CoreException e) {
                    MTJLogger
                            .log(IStatus.ERROR, NLS.bind(
                                    "Unable to get keystore path: {0}", e
                                            .getMessage()));
                }

                file = new File(path);
                if (!file.exists() || !file.isFile()) {
                    return;
                }

                if (properties.getPasswordStorageMethod() == SignatureProperties.PASSMETHOD_PROMPT) {
                    SigningPasswordDialog dialog = new SigningPasswordDialog(
                            getPartControl().getShell(), false);
                    dialog.setTitle("KeyStore Password");
                    if (dialog.open() == Dialog.OK) {
                        password = dialog.getPassword();
                    } else {
                        return;
                    }
                } else {
                    password = properties.getKeyStorePassword();
                }

                try {
                    IKeyStoreManager manager = new DefaultKeyStoreManager(file,
                            password);
                    manager.setProvider(properties.getKeyStoreProvider());
                    manager.setKeystoreType(properties.getKeyStoreType());

                    List<KeyStoreEntry> entries = manager.getEntries();
                    for (KeyStoreEntry entry : entries) {
                    	if (entry.getType() == Type.KEY_PAIR) {							
                    		combo.add(entry.getAlias());
						}
                    }
                } catch (KeyStoreManagerException e) {
                    String message = NLS.bind(
                            "Unable to get keystore aliases: {0}", e
                                    .getMessage());
                    MTJLogger.log(IStatus.ERROR, message);
                    getErrorMessageManager().addMessage(message, message, null,
                            IMessageProvider.ERROR);
                } catch (Exception e) {
                    String message = e.getMessage();
                    MTJLogger.log(IStatus.ERROR, message);
                    getErrorMessageManager().addMessage(message, message, null,
                            IMessageProvider.ERROR);
                }
                String alias = properties.getKeyAlias();
                // In case no alias is selected yet return;
                if (alias == null) {
                    return;
                }
                for (int i = 0; i < combo.getItemCount(); i++) {
                    if (alias.equals(combo.getItem(i))) {
                        combo.select(i);
                    }
                }
            }
        });
    }

    /**
     * Adds a listener for the suite to update aliases upon external changes.
     */
    private void listenForProjectSignatureUpdates() {
        this.projectListener = new IMidletSuiteProjectListener() {

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.IMTJProjectListener#classpathRefreshed()
             */
            public void classpathRefreshed() {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProjectListener#jadFileNameChanged()
             */
            public void jadFileNameChanged() {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.IMTJProjectListener#metaDataSaved()
             */
            public void metaDataSaved() {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.IMTJProjectListener#packageCreated()
             */
            public void packageCreated() {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.IMTJProjectListener#signaturePropertiesChanged()
             */
            public void signaturePropertiesChanged() {
                if (enableButton != null && !enableButton.isDisposed()
                        && enableButton.getSelection()) {
                    listAliases(combo);
                }
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProjectListener#tempKeyPasswordChanged()
             */
            public void tempKeyPasswordChanged() {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProjectListener#tempKeystorePasswordChanged()
             */
            public void tempKeystorePasswordChanged() {
            }
        };
        IMidletSuiteProject midletSuiteProject = MidletSuiteFactory
                .getMidletSuiteProject(getJavaProject());
        if (midletSuiteProject != null) {
            midletSuiteProject.addMTJProjectListener(this.projectListener);
        }
    }

    /**
     * Scans the project classes for occurrences of the classes requiring
     * permissions and opens a permissions dialog with all permissions found
     * selected.
     * 
     * @param listId id of the target list.
     */
    private void scanProjectClasses(ListId listId) {
        IMidletSuiteProject midletSuiteProject = MidletSuiteFactory
                .getMidletSuiteProject(getJavaProject());
        if (midletSuiteProject == null) {
            return;
        }

        CheckedTreeSelectionDialog dialog = SecurityPermissionsDialog
                .createDialog(getPartControl().getShell());
        SecurityPermissionsScanner scanner = new SecurityPermissionsScanner(
                midletSuiteProject.getVerifiedClassesOutputFolder(null));
        List<PermissionsGroup> permissionsGroups = scanner
                .getRequiredPermissions();
        dialog.setInitialSelections(permissionsGroups.toArray());
        if (dialog.open() == Dialog.OK) {
            Object[] result = dialog.getResult();

            GenericListBlock<SecurityPermissionListItem> block = null;
            List<SecurityPermissionListItem> list = null;
            switch (listId) {
                case Required:
                    block = reqPermissionBlock;
                    list = reqPermissions;
                    break;
                case Optional:
                    block = optPermissionBlock;
                    list = optPermissions;
                    break;
            }

            if (list != null) {
                addPermissionsToList(list, result);
                block.update();
            }
        }
    }

    /**
     * Updates the project's signature properties.
     */
    private void updateProjectSigningState() {
        ISignatureProperties signProperties = null;
        IMidletSuiteProject midletProject = null;

        midletProject = MidletSuiteFactory
                .getMidletSuiteProject(getJavaProject());
        signProperties = getProjectSignatureProperties();
        if (signProperties == null) {
            return;
        }

        try {
            signProperties.setSignProject(this.enableButton.getSelection());
            int selectedIndex = this.combo.getSelectionIndex();
            if (selectedIndex >= 0x00) {
                signProperties.setKeyAlias(this.combo.getItem(selectedIndex));
            }
            midletProject.setSignatureProperties(signProperties);
            midletProject.saveMetaData();
        } catch (CoreException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    protected void createFormContent(IManagedForm managedForm) {
        FormToolkit toolkit = managedForm.getToolkit();
        ScrolledForm form = managedForm.getForm();
        form.setExpandHorizontal(true);
        form.setExpandVertical(true);

        toolkit.decorateFormHeading(form.getForm());
        createErrorMessageHandler(managedForm);
        form.setText(getTitle());

        Composite body = form.getBody();
        body.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        GridLayout layout = new GridLayout(2, true);
        layout.horizontalSpacing = 10;
        layout.verticalSpacing = 20;
        body.setLayout(layout);
        this.createTopSection(managedForm, body);
        this.createAliasSection(managedForm, body);
        this.createListsSection(managedForm, body);
        updateWidgetsStates(enableButton.getSelection());
        form.reflow(true);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
     */
    protected String getHelpResource() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
     */
    protected void setInput(IEditorInput input) {
        super.setInput(input);
        editorInputChanged();
    }

    /**
     * Updates the widgets enabled state.
     * 
     * @param state true to enable false to disable.
     */
    protected void updateWidgetsStates(boolean state) {
        this.combo.setEnabled(state);
        if (state) {
            this.listAliases(this.combo);
        }
    }
}
