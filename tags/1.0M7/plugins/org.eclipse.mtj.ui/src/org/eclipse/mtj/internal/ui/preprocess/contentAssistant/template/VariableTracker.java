/*******************************************************************************
 * Copyright (c) 2000, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/*******************************************************************************
 * Copyright (c) 2008 Sybase Corporation.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma (Sybase) - Copy from 
 *              org.eclipse.jdt.internal.corext.template.java.JavaFormatter$VariableTracker,
 *              which located in org.eclipse.jdt.ui plug-in. Remove unused methods.
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant.template;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BadPositionCategoryException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.TypedPosition;
import org.eclipse.jface.text.templates.TemplateBuffer;
import org.eclipse.jface.text.templates.TemplateVariable;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.RangeMarker;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEdit;

/**
 * Wraps a {@link TemplateBuffer} and tracks the variable offsets while changes
 * to the buffer occur. Whitespace variables are also tracked.
 */
@SuppressWarnings("unchecked")
public class VariableTracker {
    private static final String CATEGORY = "__template_variables"; //$NON-NLS-1$
    private static final String COMMENT_START = "/*-"; //$NON-NLS-1$
    private static final String COMMENT_END = "*/"; //$NON-NLS-1$
    private Document fDocument;
    private final TemplateBuffer fBuffer;

    private List fPositions;

    /**
     * Creates a new tracker.
     * 
     * @param buffer the buffer to track
     * @throws MalformedTreeException
     * @throws BadLocationException
     */
    public VariableTracker(TemplateBuffer buffer)
            throws MalformedTreeException, BadLocationException {
        Assert.isLegal(buffer != null);
        fBuffer = buffer;
        fDocument = new Document(fBuffer.getString());
        // installJavaStuff(textDocument);
        fDocument.addPositionCategory(CATEGORY);
        fDocument.addPositionUpdater(new ExclusivePositionUpdater(CATEGORY));
        fPositions = createRangeMarkers(fBuffer.getVariables(), fDocument);
    }

    private void checkState() {
        if (fDocument == null) {
            throw new IllegalStateException();
        }
    }

    private List createRangeMarkers(TemplateVariable[] variables,
            IDocument document) throws MalformedTreeException,
            BadLocationException {
        Map markerToOriginal = new HashMap();

        MultiTextEdit root = new MultiTextEdit(0, document.getLength());
        List edits = new ArrayList();
        boolean hasModifications = false;
        for (int i = 0; i != variables.length; i++) {
            final TemplateVariable variable = variables[i];
            int[] offsets = variable.getOffsets();

            String value = variable.getDefaultValue();
            if (isWhitespaceVariable(value)) {
                // replace whitespace positions with unformattable comments
                String placeholder = COMMENT_START + value + COMMENT_END;
                for (int j = 0; j != offsets.length; j++) {
                    ReplaceEdit replace = new ReplaceEdit(offsets[j], value
                            .length(), placeholder);
                    root.addChild(replace);
                    hasModifications = true;
                    markerToOriginal.put(replace, value);
                    edits.add(replace);
                }
            } else {
                for (int j = 0; j != offsets.length; j++) {
                    RangeMarker marker = new RangeMarker(offsets[j], value
                            .length());
                    root.addChild(marker);
                    edits.add(marker);
                }
            }
        }

        if (hasModifications) {
            // update the document and convert the replaces to markers
            root.apply(document, TextEdit.UPDATE_REGIONS);
        }

        List positions = new ArrayList();
        for (Iterator it = edits.iterator(); it.hasNext();) {
            TextEdit edit = (TextEdit) it.next();
            try {
                // abuse TypedPosition to piggy back the original contents
                // of the position
                final TypedPosition pos = new TypedPosition(edit.getOffset(),
                        edit.getLength(), (String) markerToOriginal.get(edit));
                document.addPosition(CATEGORY, pos);
                positions.add(pos);
            } catch (BadPositionCategoryException x) {
                Assert.isTrue(false);
            }
        }

        return positions;
    }

    // /**
    // * Installs a java partitioner with <code>document</code>.
    // *
    // * @param document the document
    // */
    // private static void installJavaStuff(Document document) {
    // String[] types= new String[] {
    // IJavaPartitions.JAVA_DOC,
    // IJavaPartitions.JAVA_MULTI_LINE_COMMENT,
    // IJavaPartitions.JAVA_SINGLE_LINE_COMMENT,
    // IJavaPartitions.JAVA_STRING,
    // IJavaPartitions.JAVA_CHARACTER,
    // IDocument.DEFAULT_CONTENT_TYPE
    // };
    // FastPartitioner partitioner= new FastPartitioner(new
    // FastJavaPartitionScanner(), types);
    // partitioner.connect(document);
    // document.setDocumentPartitioner(IJavaPartitions.JAVA_PARTITIONING,
    // partitioner);
    // }
    //    
    /**
     * Returns the document with the buffer contents. Whitespace variables are
     * decorated with comments.
     * 
     * @return the buffer document
     */
    public IDocument getDocument() {
        checkState();
        return fDocument;
    }

    private boolean isWhitespaceVariable(String value) {
        int length = value.length();
        return (length == 0) || Character.isWhitespace(value.charAt(0))
                || Character.isWhitespace(value.charAt(length - 1));
    }

    private void removeRangeMarkers(List positions, IDocument document,
            TemplateVariable[] variables) throws MalformedTreeException,
            BadLocationException, BadPositionCategoryException {

        // revert previous changes
        for (Iterator it = positions.iterator(); it.hasNext();) {
            TypedPosition position = (TypedPosition) it.next();
            // remove and re-add in order to not confuse
            // ExclusivePositionUpdater
            document.removePosition(CATEGORY, position);
            final String original = position.getType();
            if (original != null) {
                document.replace(position.getOffset(), position.getLength(),
                        original);
                position.setLength(original.length());
            }
            document.addPosition(position);
        }

        Iterator it = positions.iterator();
        for (int i = 0; i != variables.length; i++) {
            TemplateVariable variable = variables[i];

            int[] offsets = new int[variable.getOffsets().length];
            for (int j = 0; j != offsets.length; j++) {
                offsets[j] = ((Position) it.next()).getOffset();
            }

            variable.setOffsets(offsets);
        }

    }

    /**
     * Restores any decorated regions and updates the buffer's variable offsets.
     * 
     * @return the buffer.
     * @throws MalformedTreeException
     * @throws BadLocationException
     */
    public TemplateBuffer updateBuffer() throws MalformedTreeException,
            BadLocationException {
        checkState();
        TemplateVariable[] variables = fBuffer.getVariables();
        try {
            removeRangeMarkers(fPositions, fDocument, variables);
        } catch (BadPositionCategoryException x) {
            Assert.isTrue(false);
        }
        fBuffer.setContent(fDocument.get(), variables);
        fDocument = null;

        return fBuffer;
    }
}
