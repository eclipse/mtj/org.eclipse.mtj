/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma (Sybase)        - Initial implementation
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874] 
 */
package org.eclipse.mtj.internal.ui.editors.jad.form.pages;

import org.eclipse.mtj.core.project.midp.DescriptorPropertyDescription;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.sdk.device.midp.Configuration;
import org.eclipse.mtj.internal.core.sdk.device.midp.Profile;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider;
import org.eclipse.mtj.ui.editors.jad.ListDescriptorPropertyDescription;

/**
 * provide required JAD attributes descriptors
 * 
 * @author Gang Ma
 */
public class RequiredJADDesciptorsProvider implements IJADDescriptorsProvider {

    /**
     * Required property descriptors.
     */
    private static final DescriptorPropertyDescription[] REQUIRED_DESCRIPTORS = new DescriptorPropertyDescription[] {
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_JAR_URL,
                    MTJUIMessages.RequiredJADDesciptorsProvider_midlet_jar_url,
                    DescriptorPropertyDescription.DATATYPE_URL),
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_NAME,
                    MTJUIMessages.RequiredJADDesciptorsProvider_midlet_name,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_VENDOR,
                    MTJUIMessages.RequiredJADDesciptorsProvider_midlet_vendor,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_VERSION,
                    MTJUIMessages.RequiredJADDesciptorsProvider_midlet_version,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new ListDescriptorPropertyDescription(
                    IJADConstants.JAD_MICROEDITION_CONFIG,
                    MTJUIMessages.RequiredJADDesciptorsProvider_microedition_configuration,
                    getConfigurationNamesAndValues()),
            new ListDescriptorPropertyDescription(
                    IJADConstants.JAD_MICROEDITION_PROFILE,
                    MTJUIMessages.RequiredJADDesciptorsProvider_microedition_profile,
                    getProfileNamesAndValues()), };

    /**
     * Return the J2ME configuration names and values.
     * 
     * @return
     */
    private static String[][] getConfigurationNamesAndValues() {
        String[][] namesAndValues = null;

        IAPI[] specifications = Configuration.values();

        namesAndValues = new String[specifications.length][];
        for (int i = 0; i < specifications.length; i++) {
            IAPI spec = specifications[i];
            namesAndValues[i] = new String[2];
            namesAndValues[i][0] = spec.getName();
            namesAndValues[i][1] = spec.toString();
        }

        return namesAndValues;
    }

    /**
     * Return the J2ME profile names and values.
     * 
     * @return
     */
    private static String[][] getProfileNamesAndValues() {
        String[][] namesAndValues = null;

        IAPI[] specifications = Profile.values();

        namesAndValues = new String[specifications.length][];
        for (int i = 0; i < specifications.length; i++) {
            IAPI spec = specifications[i];
            namesAndValues[i] = new String[2];
            namesAndValues[i][0] = spec.getName();
            namesAndValues[i][1] = spec.toString();
        }

        return namesAndValues;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider#getDescriptorPropertyDescriptions()
     */
    public DescriptorPropertyDescription[] getDescriptorPropertyDescriptions() {
        return REQUIRED_DESCRIPTORS;
    }

}
