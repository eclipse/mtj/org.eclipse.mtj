/**
 * Copyright (c) 2005, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.jface.preference.ColorSelector;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.editor.text.ChangeAwareSourceViewerConfiguration;
import org.eclipse.mtj.internal.ui.editor.text.IColorManager;
import org.eclipse.mtj.internal.ui.editor.text.IMTJColorConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

/**
 * @since 0.9.1
 */
public abstract class SyntaxColorTab {

    /**
     * @since 0.9.1
     */
    class ColorElement {

        private boolean fBold;
        private String fColorKey;
        private RGB fColorValue;
        private String fDisplayName;
        private boolean fItalic;

        /**
         * @param displayName
         * @param colorKey
         * @param colorValue
         * @param bold
         * @param italic
         */
        public ColorElement(String displayName, String colorKey,
                RGB colorValue, boolean bold, boolean italic) {
            fDisplayName = displayName;
            fColorKey = colorKey;
            fColorValue = colorValue;
            fBold = bold;
            fItalic = italic;
        }

        /**
         * @param event
         */
        public void firePropertyChange(PropertyChangeEvent event) {
            if (fSourceViewerConfiguration != null) {
                fSourceViewerConfiguration.adaptToPreferenceChange(event);
                fPreviewViewer.invalidateTextPresentation();
            }
        }

        /**
         * @return
         */
        public String getColorKey() {
            return fColorKey;
        }

        /**
         * @return
         */
        public RGB getColorValue() {
            return fColorValue;
        }

        /**
         * @return
         */
        public String getDisplayName() {
            return fDisplayName;
        }

        /**
         * @return
         */
        public boolean isBold() {
            return fBold;
        }

        /**
         * @return
         */
        public boolean isItalic() {
            return fItalic;
        }

        /**
         * @param bold
         */
        public void setBold(boolean bold) {
            if (bold == fBold) {
                return;
            }
            Boolean oldValue = Boolean.valueOf(fBold);
            fBold = bold;
            Boolean newValue = Boolean.valueOf(bold);
            String property = fColorKey + IMTJColorConstants.P_BOLD_SUFFIX;
            firePropertyChange(new PropertyChangeEvent(this, property,
                    oldValue, newValue));
        }

        /**
         * @param rgb
         */
        public void setColorValue(RGB rgb) {
            if (fColorValue.equals(rgb)) {
                return;
            }
            RGB oldrgb = fColorValue;
            fColorValue = rgb;
            firePropertyChange(new PropertyChangeEvent(this, fColorKey, oldrgb,
                    rgb));
        }

        /**
         * @param italic
         */
        public void setItalic(boolean italic) {
            if (italic == fItalic) {
                return;
            }
            Boolean oldValue = Boolean.valueOf(fItalic);
            fItalic = italic;
            Boolean newValue = Boolean.valueOf(italic);
            String property = fColorKey + IMTJColorConstants.P_ITALIC_SUFFIX;
            firePropertyChange(new PropertyChangeEvent(this, property,
                    oldValue, newValue));
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return getDisplayName();
        }
    }

    private Button fBoldButton;
    private TableViewer fElementViewer;
    private Button fItalicButton;
    private SourceViewer fPreviewViewer;
    private ChangeAwareSourceViewerConfiguration fSourceViewerConfiguration;

    protected IColorManager fColorManager;

    /**
     * @param manager
     */
    public SyntaxColorTab(IColorManager manager) {
        fColorManager = manager;
    }

    /**
     * @param parent
     * @return
     */
    public Control createContents(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);
        container.setLayout(new GridLayout());
        container.setLayoutData(new GridData(GridData.FILL_BOTH));
        createElementTable(container);
        createPreviewer(container);
        return container;
    }

    /**
     * 
     */
    public void dispose() {
        fSourceViewerConfiguration.dispose();
    }

    /**
     * 
     */
    public void performDefaults() {
        IPreferenceStore store = MTJUIPlugin.getDefault().getPreferenceStore();
        int count = fElementViewer.getTable().getItemCount();
        for (int i = 0; i < count; i++) {
            ColorElement item = (ColorElement) fElementViewer.getElementAt(i);
            RGB rgb = PreferenceConverter.getDefaultColor(store, item
                    .getColorKey());
            item.setColorValue(rgb);
            item.setBold(store.getDefaultBoolean(item.getColorKey()
                    + IMTJColorConstants.P_BOLD_SUFFIX));
            item.setItalic(store.getDefaultBoolean(item.getColorKey()
                    + IMTJColorConstants.P_ITALIC_SUFFIX));
        }
        ColorElement element = getColorElement(fElementViewer);
        fBoldButton.setSelection(element.isBold());
        fItalicButton.setSelection(element.isItalic());
    }

    /**
     * 
     */
    public void performOk() {
        IPreferenceStore store = MTJUIPlugin.getDefault().getPreferenceStore();
        int count = fElementViewer.getTable().getItemCount();
        for (int i = 0; i < count; i++) {
            ColorElement item = (ColorElement) fElementViewer.getElementAt(i);
            PreferenceConverter.setValue(store, item.getColorKey(), item
                    .getColorValue());
            store.setValue(item.getColorKey()
                    + IMTJColorConstants.P_BOLD_SUFFIX, item.isBold());
            store.setValue(item.getColorKey()
                    + IMTJColorConstants.P_ITALIC_SUFFIX, item.isItalic());
        }
    }

    /**
     * @param parent
     */
    private void createElementTable(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout(2, true);
        layout.marginWidth = layout.marginHeight = 0;
        container.setLayout(layout);
        container.setLayoutData(new GridData(GridData.FILL_BOTH));

        Label label = new Label(container, SWT.LEFT);
        label.setText(MTJUIMessages.SyntaxColorTab_elements_label);
        GridData gd = new GridData();
        gd.horizontalSpan = 2;
        label.setLayoutData(gd);

        fElementViewer = new TableViewer(container, SWT.SINGLE | SWT.V_SCROLL
                | SWT.BORDER);
        fElementViewer.setLabelProvider(new LabelProvider());
        fElementViewer.setContentProvider(new ArrayContentProvider());
        fElementViewer.getControl().setLayoutData(
                new GridData(GridData.FILL_BOTH));

        Composite colorComposite = new Composite(container, SWT.NONE);
        colorComposite.setLayout(new GridLayout(2, false));
        colorComposite.setLayoutData(new GridData(GridData.FILL_BOTH));

        label = new Label(colorComposite, SWT.LEFT);
        label.setText(MTJUIMessages.SyntaxColorTab_color_label);

        final ColorSelector colorSelector = new ColorSelector(colorComposite);
        Button colorButton = colorSelector.getButton();
        colorButton.setLayoutData(new GridData(GridData.BEGINNING));

        colorButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                ColorElement item = getColorElement(fElementViewer);
                item.setColorValue(colorSelector.getColorValue());
            }
        });

        fBoldButton = new Button(colorComposite, SWT.CHECK);
        gd = new GridData();
        gd.horizontalSpan = 2;
        fBoldButton.setLayoutData(gd);
        fBoldButton.setText(MTJUIMessages.SyntaxColorTab_bold_label);
        fBoldButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                ColorElement item = getColorElement(fElementViewer);
                item.setBold(fBoldButton.getSelection());
            }
        });

        fItalicButton = new Button(colorComposite, SWT.CHECK);
        gd = new GridData();
        gd.horizontalSpan = 2;
        fItalicButton.setLayoutData(gd);
        fItalicButton.setText(MTJUIMessages.SyntaxColorTab_italic_label);
        fItalicButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                ColorElement item = getColorElement(fElementViewer);
                item.setItalic(fItalicButton.getSelection());
            }
        });

        fElementViewer
                .addSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        ColorElement item = getColorElement(fElementViewer);
                        colorSelector.setColorValue(item.getColorValue());
                        fBoldButton.setSelection(item.isBold());
                        fItalicButton.setSelection(item.isItalic());
                    }
                });
        fElementViewer.setInput(getColorData());
        fElementViewer.setComparator(new ViewerComparator());
        fElementViewer.setSelection(new StructuredSelection(fElementViewer
                .getElementAt(0)));
    }

    /**
     * @param parent
     */
    private void createPreviewer(Composite parent) {
        Composite previewComp = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.marginHeight = layout.marginWidth = 0;
        previewComp.setLayout(layout);
        previewComp.setLayoutData(new GridData(GridData.FILL_BOTH));

        Label label = new Label(previewComp, SWT.NONE);
        label.setText(MTJUIMessages.SyntaxColorTab_preview_label);
        label.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        fPreviewViewer = new SourceViewer(previewComp, null, SWT.BORDER
                | SWT.V_SCROLL | SWT.H_SCROLL);
        fSourceViewerConfiguration = getSourceViewerConfiguration();

        if (fSourceViewerConfiguration != null) {
            fPreviewViewer.configure(fSourceViewerConfiguration);
        }

        fPreviewViewer.setEditable(false);
        fPreviewViewer.getTextWidget().setFont(
                JFaceResources.getFont(JFaceResources.TEXT_FONT));
        fPreviewViewer.setDocument(getDocument());

        Control control = fPreviewViewer.getControl();
        control.setLayoutData(new GridData(GridData.FILL_BOTH));
    }

    /**
     * @return
     */
    private ColorElement[] getColorData() {
        String[][] colors = getColorStrings();
        IPreferenceStore store = MTJUIPlugin.getDefault().getPreferenceStore();
        ColorElement[] list = new ColorElement[colors.length];
        for (int i = 0; i < colors.length; i++) {
            String displayName = colors[i][0];
            String key = colors[i][1];
            RGB setting = PreferenceConverter.getColor(store, key);
            boolean bold = store.getBoolean(key
                    + IMTJColorConstants.P_BOLD_SUFFIX);
            boolean italic = store.getBoolean(key
                    + IMTJColorConstants.P_ITALIC_SUFFIX);
            list[i] = new ColorElement(displayName, key, setting, bold, italic);
        }
        return list;
    }

    /**
     * @param viewer
     * @return
     */
    private ColorElement getColorElement(TableViewer viewer) {
        IStructuredSelection selection = (IStructuredSelection) viewer
                .getSelection();
        return (ColorElement) selection.getFirstElement();
    }

    /**
     * @return
     */
    protected abstract String[][] getColorStrings();

    /**
     * @return
     */
    protected abstract IDocument getDocument();

    /**
     * @return
     */
    protected abstract ChangeAwareSourceViewerConfiguration getSourceViewerConfiguration();

}
