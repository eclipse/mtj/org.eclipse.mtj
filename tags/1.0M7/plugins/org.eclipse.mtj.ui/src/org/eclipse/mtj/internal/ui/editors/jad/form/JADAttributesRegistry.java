/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma 	(Sybase)       - Initial implementation
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.internal.ui.editors.jad.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.core.project.midp.DescriptorPropertyDescription;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider;

/**
 * Provide registry functionality for getting JAD attributes according to the
 * specify device and editor page.
 * 
 * @author Gang Ma
 */
public class JADAttributesRegistry {

    /**
     * class wrapped IConfigurationElement instance for jadAttributes
     * extension-point extensions
     * 
     * @author gma
     */
    static class JADAttributesConfigElement {

        private static final String JAD_ATTR_SHOW_PAGE = "pageID"; //$NON-NLS-1$
        private static final String JAD_DESCRIPTOR_PROVIDER_CLASS = "class"; //$NON-NLS-1$
        private static final String JAD_DESCRIPTOR_PROVIDER_ELEMENT = "jadDescriptorsProvider"; //$NON-NLS-1$
        private static final String VENDOR_SPEC_ATTR = "vendorSpec"; //$NON-NLS-1$

        private IConfigurationElement element;

        private IJADDescriptorsProvider jadDescriptorsProvider;

        /**
         * @param jadAttrElement
         */
        public JADAttributesConfigElement(IConfigurationElement jadAttrElement) {
            element = jadAttrElement;
        }

        /**
         * @return
         */
        public String getAttributesShowPage() {
            return element.getAttribute(JAD_ATTR_SHOW_PAGE);
        }

        /**
         * @return
         * @throws CoreException
         */
        public IJADDescriptorsProvider getJadDescriptorsProvider()
                throws CoreException {
            if (jadDescriptorsProvider == null) {
                IConfigurationElement[] providers = element
                        .getChildren(JAD_DESCRIPTOR_PROVIDER_ELEMENT);
                if ((providers != null) && (providers.length > 0)) {
                    jadDescriptorsProvider = (IJADDescriptorsProvider) providers[0]
                            .createExecutableExtension(JAD_DESCRIPTOR_PROVIDER_CLASS);
                }
            }
            return jadDescriptorsProvider;
        }

        public boolean isVendorSpec() {
            String value = element.getAttribute(VENDOR_SPEC_ATTR);
            return value == null ? false : "true".equalsIgnoreCase(value); //$NON-NLS-1$
        }

    }

    /**
     * The JAD attributes extension point
     */
    public static final String JAD_ATTRIBUTE_EXTENSION = "jadattributes"; //$NON-NLS-1$

    /**
     * all JAD attributes configElements
     */
    private static JADAttributesConfigElement[] allJADAttrElements;

    /**
     * use to store generic JAD attributes(not vendor specific) for each page.
     */
    private static Map<String, JADAttributesConfigElement[]> genericPageJADAttrMap = new HashMap<String, JADAttributesConfigElement[]>();

    private static IJADDescriptorsProvider[] JADDescriptorproviders;

    public static IJADDescriptorsProvider[] getAllJADDescriptorProviders() {
        JADAttributesConfigElement[] configs = getAllJADAttributeElements();

        if (JADDescriptorproviders == null) {
            JADDescriptorproviders = new IJADDescriptorsProvider[configs.length];
            try {
                for (int i = 0; i < configs.length; i++) {
                    JADDescriptorproviders[i] = configs[i]
                            .getJadDescriptorsProvider();
                }
            } catch (CoreException ex) {
                MTJUIPlugin.getDefault().getLog().log(
                        new Status(IStatus.WARNING, MTJUIPlugin.getPluginId(),
                                "Unable to read the JAD descriptor", ex)); //$NON-NLS-1$
            }
        }
        return JADDescriptorproviders;
    }

    /**
     * @param pageID the target page's ID
     * @param device the device user used
     * @return the vendor specific JAD descriptors
     */
    public static DescriptorPropertyDescription[] getJADAttrDescriptorsByPage(
            String pageID) {
        JADAttributesConfigElement[] relatedElements = getRelatedAttrElements(pageID);
        DescriptorPropertyDescription[] resultAttributes = getDescriptorsFromElements(relatedElements);
        return resultAttributes;
    }

    /**
     * @param elements
     * @param device
     * @param pageID
     * @param excludeVendorSpec
     * @return
     */
    private static JADAttributesConfigElement[] filterElements(
            JADAttributesConfigElement[] elements, IMIDPDevice device,
            String pageID, boolean excludeVendorSpec) {
        ArrayList<JADAttributesConfigElement> resultDescriptorList = new ArrayList<JADAttributesConfigElement>();
        for (JADAttributesConfigElement element : elements) {
            boolean satisfied = true;
            if (device != null) {
                satisfied &= element.isVendorSpec()
                        && isDeviceMatchVendor(device, element);
            }
            if (pageID != null) {
                satisfied &= element.getAttributesShowPage().equalsIgnoreCase(
                        pageID);
            }
            if (excludeVendorSpec) {
                satisfied &= !element.isVendorSpec();
            }

            if (satisfied) {
                resultDescriptorList.add(element);
            }

        }
        return resultDescriptorList.toArray(new JADAttributesConfigElement[0]);
    }

    /**
     * @param elements
     * @param pageID
     * @param excludeVendorSpec
     * @return
     */
    private static JADAttributesConfigElement[] filterElementsByPageAndVendorSpec(
            JADAttributesConfigElement[] elements, String pageID,
            boolean excludeVendorSpec) {
        return filterElements(elements, null, pageID, excludeVendorSpec);
    }

    /**
     * Get all the JAD attributes registered. This includes elements that are
     * vendor specific as well as those coming from specifications.
     * 
     * @return array of JADAttributesConfigElement
     */
    private static JADAttributesConfigElement[] getAllJADAttributeElements() {
        if (allJADAttrElements == null) {
            allJADAttrElements = readAllJADAttributes();
        }

        return allJADAttrElements;
    }

    /**
     * @param elements config elements
     * @param pageID editor page ID
     * @param device target device
     * @return DescriptorPropertyDescription array
     */
    private static DescriptorPropertyDescription[] getDescriptorsFromElements(
            JADAttributesConfigElement[] elements) {
        ArrayList<DescriptorPropertyDescription> descriptorList = new ArrayList<DescriptorPropertyDescription>();
        for (JADAttributesConfigElement element : elements) {
            try {
                IJADDescriptorsProvider provider = element
                        .getJadDescriptorsProvider();
                DescriptorPropertyDescription[] descriptorArray = provider
                        .getDescriptorPropertyDescriptions();
                descriptorList.addAll(Arrays.asList(descriptorArray));

            } catch (Exception e) {
                MTJLogger
                        .log(
                                IStatus.WARNING,
                                MTJUIMessages.JADAttributesRegistry_error_getDescriptorsFromElements,
                                e);
            }
        }

        return descriptorList
                .toArray(new DescriptorPropertyDescription[descriptorList
                        .size()]);
    }

    /**
     * @param pageID the JAD Editor page's ID
     * @return the generic jadAttributes configElements for specific page
     */
    private static JADAttributesConfigElement[] getGenericElements(String pageID) {
        JADAttributesConfigElement[] elements = getAllJADAttributeElements();
        return filterElementsByPageAndVendorSpec(elements, pageID, true);

    }

    /**
     * return the related jadAttributes configElements
     */
    private static JADAttributesConfigElement[] getRelatedAttrElements(
            String pageID) {
        JADAttributesConfigElement[] genericElements = null;

        // get the page's generic JAD attribute descriptors
        if (!genericPageJADAttrMap.containsKey(pageID)) {
            genericPageJADAttrMap.put(pageID, getGenericElements(pageID));
        }
        genericElements = genericPageJADAttrMap.get(pageID);

        return genericElements;
    }

    /**
     * judge whether the configuration element is for the device
     * 
     * @param device
     * @param element
     * @return if matched return true else false
     */
    private static boolean isDeviceMatchVendor(IMIDPDevice device,
            JADAttributesConfigElement element) {

        return true;
    }

    /**
     * @return
     */
    private static JADAttributesConfigElement[] readAllJADAttributes() {
        String plugin = MTJUIPlugin.getDefault().getBundle().getSymbolicName();
        IConfigurationElement[] configElements = Platform
                .getExtensionRegistry().getConfigurationElementsFor(plugin,
                        JAD_ATTRIBUTE_EXTENSION);

        JADAttributesConfigElement[] elements = new JADAttributesConfigElement[configElements.length];
        for (int i = 0; i < configElements.length; i++) {
            elements[i] = new JADAttributesConfigElement(configElements[i]);
        }

        return elements;
    }
}
