/**
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.viewers.IPostSelectionProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.mtj.internal.core.IBaseModel;
import org.eclipse.mtj.internal.core.IWritableDelimiter;
import org.eclipse.mtj.internal.core.text.IWritable;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.editor.context.IInputContextListener;
import org.eclipse.mtj.internal.ui.editor.context.InputContext;
import org.eclipse.mtj.internal.ui.editor.context.InputContextManager;
import org.eclipse.mtj.internal.ui.editors.l10n.L10nSourceOutlinePage;
import org.eclipse.search.ui.text.ISearchEditorAccess;
import org.eclipse.search.ui.text.Match;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IEditorActionBarContributor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IStorageEditorInput;
import org.eclipse.ui.IURIEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.IFormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.ide.IGotoMarker;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.part.MultiPageEditorSite;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

/**
 * A simple multi-page form editor that uses Eclipse Forms support.
 * 
 * @since 0.9.1
 */
public abstract class MTJFormEditor extends FormEditor implements
        IInputContextListener, IGotoMarker, ISearchEditorAccess {

    /**
     * Updates the OutlinePage selection.
     * 
     * @since 0.9.1
     */
    public class MTJFormEditorChangeListener implements
            ISelectionChangedListener {

        /**
         * Installs this selection changed listener with the given selection
         * provider. If the selection provider is a post selection provider,
         * post selection changed events are the preferred choice, otherwise
         * normal selection changed events are requested.
         * 
         * @param selectionProvider
         */
        public void install(ISelectionProvider selectionProvider) {
            if (selectionProvider == null) {
                return;
            }

            if (selectionProvider instanceof IPostSelectionProvider) {
                IPostSelectionProvider provider = (IPostSelectionProvider) selectionProvider;
                provider.addPostSelectionChangedListener(this);
            } else {
                selectionProvider.addSelectionChangedListener(this);
            }
        }

        /*
         * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
         */
        public void selectionChanged(SelectionChangedEvent event) {
            if (MTJUIPlugin.getDefault().getPreferenceStore().getBoolean(
                    "ToggleLinkWithEditorAction.isChecked")) { //$NON-NLS-1$
                if (getFormOutline() != null) {
                    getFormOutline().setSelection(event.getSelection());
                }
            }
        }

        /**
         * Removes this selection changed listener from the given selection
         * provider.
         * 
         * @param selectionProviderstyle
         */
        public void uninstall(ISelectionProvider selectionProvider) {
            if (selectionProvider == null) {
                return;
            }

            if (selectionProvider instanceof IPostSelectionProvider) {
                IPostSelectionProvider provider = (IPostSelectionProvider) selectionProvider;
                provider.removePostSelectionChangedListener(this);
            } else {
                selectionProvider.removeSelectionChangedListener(this);
            }
        }

    }

    /**
     * @since 0.9.1
     */
    private static class MTJMultiPageEditorSite extends MultiPageEditorSite {

        /**
         * @param multiPageEditor
         * @param editor
         */
        public MTJMultiPageEditorSite(MultiPageEditorPart multiPageEditor,
                IEditorPart editor) {
            super(multiPageEditor, editor);
        }

        /* (non-Javadoc)
         * @see org.eclipse.ui.part.MultiPageEditorSite#getActionBarContributor()
         */
        @Override
        public IEditorActionBarContributor getActionBarContributor() {
            MTJFormEditor editor = (MTJFormEditor) getMultiPageEditor();
            MTJFormEditorContributor contributor = editor.getContributor();
            return contributor.getSourceContributor();
        }

        /* (non-Javadoc)
         * @see org.eclipse.ui.part.MultiPageEditorSite#getPart()
         */
        @Override
        public IWorkbenchPart getPart() {
            return getMultiPageEditor();
        }
    }

    private static final String F_DIALOG_EDITOR_SECTION_KEY = "mtj-form-editor"; //$NON-NLS-1$
    private Clipboard clipboard;
    private MTJMultiPageContentOutline contentOutline;
    private Menu contextMenu;

    /**
     * The editor selection changed listener.
     * 
     * @since 3.0
     */
    private MTJFormEditorChangeListener editorSelectionChangedListener;
    private boolean error;
    private ISortableContentOutlinePage formOutline;
    private String lastActivePageId;
    private boolean lastDirtyState;

    protected InputContextManager inputContextManager;

    public MTJFormEditor() {

        MTJUIPlugin.getDefault().getLabelProvider().connect(this);
        inputContextManager = createInputContextManager();
    }

    /**
     * @param selection
     * @return
     */
    public boolean canCopy(ISelection selection) {
        if (selection == null) {
            return false;
        }
        if (selection instanceof IStructuredSelection) {
            return !selection.isEmpty();
        }
        if (selection instanceof ITextSelection) {
            ITextSelection textSelection = (ITextSelection) selection;
            return textSelection.getLength() > 0;
        }
        return false;
    }

    /**
     * @param selection
     * @return
     */
    public boolean canCut(ISelection selection) {
        return canCopy(selection);
    }

    /**
     * @return
     */
    public boolean canPasteFromClipboard() {
        IFormPage page = getActivePageInstance();
        if (page instanceof MTJFormPage) {
            return ((MTJFormPage) page).canPaste(getClipboard());
        }
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.IInputContextListener#contextAdded(org.eclipse.mtj.internal.ui.editor.context.InputContext)
     */
    public final void contextAdded(InputContext context) {
        if (error) {
            removePage(0);
            addPages();
            if (!error) {
                // FIXME
                // setActivePage(OverviewPage.PAGE_ID);
            }
        } else {
            editorContextAdded(context);
        }
    }

    /**
     * @param manager
     */
    public void contributeToToolbar(IToolBarManager manager) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormEditor#dispose()
     */
    @Override
    public void dispose() {
        storeDefaultPage();
        if (editorSelectionChangedListener != null) {
            editorSelectionChangedListener.uninstall(getSite()
                    .getSelectionProvider());
            editorSelectionChangedListener = null;
        }
        setSelection(new StructuredSelection());

        MTJUIPlugin.getDefault().getLabelProvider().disconnect(this);

        if (clipboard != null) {
            clipboard.dispose();
            clipboard = null;
        }
        super.dispose();
        inputContextManager.dispose();
        inputContextManager = null;
    }

    /**
     * 
     */
    public void doRevert() {
        IFormPage formPage = getActivePageInstance();
        // If the active page is a form page, commit all of its dirty field
        // values to the model
        if ((formPage != null) && (formPage instanceof MTJFormPage)) {
            formPage.getManagedForm().commit(true);
        }
        // If the editor has source pages, revert them
        // Reverting the source page fires events to the associated form pages
        // which will cause all their values to be updated
        boolean reverted = doRevertSourcePages();
        // If the editor does not have any source pages, revert the form pages
        // by directly reloading the underlying model.
        // Reloading the model fires a world changed event to all form pages
        // causing them to update their values
        if (reverted == false) {
            reverted = doRevertFormPage();
        }
        // If the revert operation was performed disable the revert action and
        // fire the dirty event
        if (reverted) {
            editorDirtyStateChanged();
        }
    }

    /**
     * @param input
     */
    public void doRevert(IEditorInput input) {
        IFormPage currentPage = getActivePageInstance();
        if ((currentPage != null) && (currentPage instanceof MTJFormPage)) {
            ((MTJFormPage) currentPage).cancelEdit();
        }
        InputContext context = inputContextManager.getContext(input);
        IFormPage page = findPage(context.getId());
        if ((page != null) && (page instanceof MTJSourcePage)) {
            MTJSourcePage spage = (MTJSourcePage) page;
            spage.doRevertToSaved();
        }
        editorDirtyStateChanged();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor monitor) {
        commitPages(true);
        inputContextManager.save(monitor);
        editorDirtyStateChanged();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorPart#doSaveAs()
     */
    @Override
    public void doSaveAs() {
        try {
            // Get the context for which the save as operation should be
            // performed
            String contextID = getContextIDForSaveAs();
            // Perform the same as operation
            getContextManager().saveAs(getProgressMonitor(), contextID);
            // Get the new editor input
            IEditorInput input = getContextManager().findContext(contextID)
                    .getInput();
            // Store the new editor input
            setInputWithNotify(input);
            // Update the title of the editor using the name of the new editor
            // input
            setPartName(input.getName());
            // Fire a property change accordingly
            firePropertyChange(PROP_DIRTY);
        } catch (InterruptedException e) {
            // Ignore
        } catch (Exception e) {
            String message = MTJUIMessages.MTJFormEditor_doSaveAs_failed;
            if (e.getMessage() != null) {
                message = message + ' ' + e.getMessage();
            }
            MTJLogger.log(IStatus.ERROR, message, e);
        }
    }

    public abstract void editorContextAdded(InputContext context);

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormEditor#editorDirtyStateChanged()
     */
    @Override
    public void editorDirtyStateChanged() {
        super.editorDirtyStateChanged();
        MTJFormEditorContributor contributor = getContributor();
        if (contributor != null) {
            contributor.updateActions();
        }
    }

    /**
     * @param input
     * @param notify
     */
    public void fireSaveNeeded(IEditorInput input, boolean notify) {
        if (notify) {
            editorDirtyStateChanged();
        }
        if (isDirty()) {
            validateEdit(input);
        }
    }

    /**
     * @param contextId
     * @param notify
     */
    public void fireSaveNeeded(String contextId, boolean notify) {
        if (contextId == null) {
            return;
        }
        InputContext context = inputContextManager.findContext(contextId);
        if (context != null) {
            fireSaveNeeded(context.getInput(), notify);
        }
    }

    /**
     * 
     */
    public void flushEdits() {
        IFormPage[] pages = getPages();
        IManagedForm mForm = pages[getActivePage()].getManagedForm();
        if (mForm != null) {
            mForm.commit(false);
        }
        for (IFormPage page : pages) {
            if (page instanceof MTJSourcePage) {
                MTJSourcePage sourcePage = (MTJSourcePage) page;
                sourcePage.getInputContext().flushEditorInput();
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.MultiPageEditorPart#getAdapter(java.lang.Class)
     */
    @SuppressWarnings("unchecked")
    @Override
    public Object getAdapter(Class key) {
        if (key.equals(IContentOutlinePage.class)) {
            return getContentOutline();
        }
        if (key.equals(IGotoMarker.class)) {
            return this;
        }
        if (key.equals(ISearchEditorAccess.class)) {
            return this;
        }
        return super.getAdapter(key);
    }

    /**
     * @return
     */
    public IBaseModel getAggregateModel() {
        if (inputContextManager != null) {
            return inputContextManager.getAggregateModel();
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.search.ui.text.ISearchEditorAccess#getAnnotationModel(org.eclipse.search.ui.text.Match)
     */
    public IAnnotationModel getAnnotationModel(Match match) {
        InputContext context = getInputContext(match.getElement());
        return context == null ? null : context.getDocumentProvider()
                .getAnnotationModel(context.getInput());
    }

    /**
     * @return
     */
    public Clipboard getClipboard() {
        return clipboard;
    }

    /**
     * @return
     */
    public IProject getCommonProject() {
        return inputContextManager.getCommonProject();
    }

    /**
     * @return
     */
    public MTJMultiPageContentOutline getContentOutline() {
        if ((contentOutline == null) || contentOutline.isDisposed()) {
            contentOutline = new MTJMultiPageContentOutline(this);
            updateContentOutline(getActivePageInstance());
        }
        return contentOutline;
    }

    /**
     * @return
     */
    public String getContextIDForSaveAs() {
        // Sub-classes must override this method and the isSaveAsAllowed
        // method to perform save as operations
        return null;
    }

    /**
     * @return
     */
    public InputContextManager getContextManager() {
        return inputContextManager;
    }

    /**
     * @return
     */
    public Menu getContextMenu() {
        return contextMenu;
    }

    /**
     * @return
     */
    public MTJFormEditorContributor getContributor() {
        return (MTJFormEditorContributor) getEditorSite()
                .getActionBarContributor();
    }

    /* (non-Javadoc)
     * @see org.eclipse.search.ui.text.ISearchEditorAccess#getDocument(org.eclipse.search.ui.text.Match)
     */
    public IDocument getDocument(Match match) {
        InputContext context = getInputContext(match.getElement());
        return context == null ? null : context.getDocumentProvider()
                .getDocument(context.getInput());
    }

    /**
     * @return
     */
    public boolean getLastDirtyState() {
        return lastDirtyState;
    }

    /**
     * @return
     */
    public ISelection getSelection() {
        return getSite().getSelectionProvider().getSelection();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.WorkbenchPart#getTitle()
     */
    @Override
    public String getTitle() {
        if (inputContextManager == null) {
            return super.getTitle();
        }
        InputContext context = inputContextManager.getPrimaryContext();
        if (context == null) {
            return super.getTitle();
        }
        return context.getInput().getName();
    }

    /**
     * @return
     */
    public String getTitleProperty() {
        return Utils.EMPTY_STRING;
    }

    public void gotoMarker(IMarker marker) {
        IResource resource = marker.getResource();
        InputContext context = inputContextManager.findContext(resource);
        if (context == null) {
            return;
        }
        IFormPage page = getActivePageInstance();
        if (!context.getId().equals(page.getId())) {
            page = setActivePage(context.getId());
        }
        IDE.gotoMarker(page, marker);
    }

    /**
     * Tests whether this editor has a context with a provided id. The test can
     * be used to check whether to add certain pages.
     * 
     * @param contextId
     * @return <code>true</code> if provided context is present,
     *         <code>false</code> otherwise.
     */
    public boolean hasInputContext(String contextId) {
        return inputContextManager.hasContext(contextId);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormEditor#isDirty()
     */
    @Override
    public boolean isDirty() {
        lastDirtyState = computeDirtyState();
        return lastDirtyState;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
     */
    @Override
    public boolean isSaveAsAllowed() {
        return false;
    }

    /**
     * @param object
     * @param offset
     * @param length
     */
    public void openToSourcePage(Object object, int offset, int length) {
        InputContext context = null;
        if (object instanceof InputContext) {
            context = (InputContext) object;
        } else {
            context = getInputContext(object);
        }
        if (context != null) {
            MTJSourcePage page = (MTJSourcePage) setActivePage(context.getId());
            if (page != null) {
                page.selectAndReveal(offset, length);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormEditor#setActivePage(java.lang.String)
     */
    @Override
    public IFormPage setActivePage(String pageId) {
        IFormPage page = super.setActivePage(pageId);
        if (page != null) {
            updateContentOutline(page);
        }
        return page;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.MultiPageEditorPart#setFocus()
     */
    @Override
    public void setFocus() {
        super.setFocus();
        IFormPage page = getActivePageInstance();
        // Could be done on setActive in MTJFormPage;
        // but setActive only handles page switches and not focus events
        if ((page != null) && (page instanceof MTJFormPage)) {
            ((MTJFormPage) page).updateFormSelection();
        }
    }

    public void setSelection(ISelection selection) {
        getSite().getSelectionProvider().setSelection(selection);
        getContributor().updateSelectableActions(selection);
    }

    /**
     * Triggered by toggling the 'Link with Editor' button in the outline view
     */
    public void synchronizeOutlinePage() {
        // Get current page
        IFormPage page = getActivePageInstance();

        if (page instanceof MTJSourcePage) {
            // Synchronize with current source page
            ((MTJSourcePage) page).synchronizeOutlinePage();
        } else {
            // Synchronize with current form page
            // This currently does not work
            // TODO: Fix 'Link with Editor' functionality for form pages
            if (getFormOutline() != null) {
                getFormOutline().setSelection(getSelection());
            }
        }
    }

    /**
     * 
     */
    public void updateTitle() {
        firePropertyChange(IWorkbenchPart.PROP_TITLE);
    }

    /**
     * @return
     */
    private boolean computeDirtyState() {
        IFormPage page = getActivePageInstance();
        if (((page != null) && page.isDirty())
                || ((inputContextManager != null) && inputContextManager
                        .isDirty())) {
            return true;
        }
        return super.isDirty();
    }

    /**
     * @param selection
     */
    @SuppressWarnings("unchecked")
    private void copyToClipboard(ISelection selection) {
        Object[] objects = null;
        String textVersion = null;
        if (selection instanceof IStructuredSelection) {
            IStructuredSelection ssel = (IStructuredSelection) selection;
            if ((ssel == null) || (ssel.size() == 0)) {
                return;
            }
            objects = ssel.toArray();
            StringWriter writer = new StringWriter();
            PrintWriter pwriter = new PrintWriter(writer);
            Class objClass = null;
            for (int i = 0; i < objects.length; i++) {
                Object obj = objects[i];
                if (objClass == null) {
                    objClass = obj.getClass();
                } else if (objClass.equals(obj.getClass()) == false) {
                    return;
                }
                if (obj instanceof IWritable) {
                    // Add a customized delimiter in between all serialized
                    // objects to format the text representation
                    if ((i != 0) && (obj instanceof IWritableDelimiter)) {
                        ((IWritableDelimiter) obj).writeDelimeter(pwriter);
                    }
                    ((IWritable) obj).write(Utils.EMPTY_STRING, pwriter);
                } else if (obj instanceof String) {
                    // Delimiter is always a newline
                    pwriter.println((String) obj);
                }
            }
            pwriter.flush();
            textVersion = writer.toString();
            try {
                pwriter.close();
                writer.close();
            } catch (IOException e) {
            }
        } else if (selection instanceof ITextSelection) {
            textVersion = ((ITextSelection) selection).getText();
        }
        if (((textVersion == null) || (textVersion.length() == 0))
                && (objects == null)) {
            return;
        }
        // set the clipboard contents
        Object[] o = null;
        Transfer[] t = null;
        if (objects == null) {
            o = new Object[] { textVersion };
            t = new Transfer[] { TextTransfer.getInstance() };
        } else if ((textVersion == null) || (textVersion.length() == 0)) {
            o = new Object[] { objects };
            t = new Transfer[] { ModelDataTransfer.getInstance() };
        } else {
            o = new Object[] { objects, textVersion };
            t = new Transfer[] { ModelDataTransfer.getInstance(),
                    TextTransfer.getInstance() };
        }
        clipboard.setContents(o, t);
    }

    /**
     * @return
     */
    private boolean doRevertFormPage() {
        return false;
    }

    /**
     * @return
     */
    private boolean doRevertSourcePages() {
        boolean reverted = false;
        IFormPage[] pages = getPages();
        for (IFormPage page : pages) {
            if (page instanceof MTJSourcePage) {
                MTJSourcePage sourcePage = (MTJSourcePage) page;
                // Flush any pending editor operations into the document
                // so that the revert operation executes (revert operation is
                // aborted if the current document has not changed)
                // This happens when a form page field is modified.
                // The source page is not updated until a source operation is
                // performed or the source page is made active and possibly
                // in some other cases.
                sourcePage.getInputContext().flushEditorInput();
                // Revert the source page to the contents of the last save to
                // file
                sourcePage.doRevertToSaved();
                reverted = true;
            }
        }
        return reverted;
    }

    /**
     * @return
     */
    private String getFirstInvalidContextId() {
        InputContext[] invalidContexts = inputContextManager
                .getInvalidContexts();
        if (invalidContexts.length == 0) {
            return null;
        }
        // If primary context is among the invalid ones, return that.
        for (InputContext invalidContext : invalidContexts) {
            if (invalidContext.isPrimary()) {
                return invalidContext.getId();
            }
        }
        // Return the first one
        return invalidContexts[0].getId();
    }

    /**
     * @return
     */
    private IDialogSettings getSettingsSection() {
        // Store global settings that will persist when the editor is closed
        // in the dialog settings (This is cheating)
        // Get the dialog settings
        IDialogSettings root = MTJUIPlugin.getDefault().getDialogSettings();
        // Get the dialog section reserved for MTJ form editors
        IDialogSettings section = root.getSection(F_DIALOG_EDITOR_SECTION_KEY);
        // If the section is not defined, define it
        if (section == null) {
            section = root.addNewSection(F_DIALOG_EDITOR_SECTION_KEY);
        }
        return section;
    }

    /**
     * @return
     */
    private String loadDefaultPage() {
        IEditorInput input = getEditorInput();
        if (input instanceof IFileEditorInput) {
            // Triggered by opening a file in the workspace
            // e.g. From the Package Explorer View
            return getPropertyEditorPageKey((IFileEditorInput) input);
        } else if (input instanceof IStorageEditorInput) {
            // Triggered by opening a file NOT in the workspace
            // e.g. From the Plug-in View
            return getDialogEditorPageKey();
        }
        return null;
    }

    /**
     * 
     */
    private void storeDefaultPage() {
        IEditorInput input = getEditorInput();
        String pageId = lastActivePageId;
        if (pageId == null) {
            return;
        }
        if (input instanceof IFileEditorInput) {
            // Triggered by opening a file in the workspace
            // e.g. From the Package Explorer View
            setPropertyEditorPageKey((IFileEditorInput) input, pageId);
        } else if (input instanceof IStorageEditorInput) {
            // Triggered by opening a file NOT in the workspace
            // e.g. From the Plug-in View
            setDialogEditorPageKey(pageId);
        }
    }

    /**
     * @param page
     */
    private void updateContentOutline(IFormPage page) {
        if (contentOutline == null) {
            return;
        }
        ISortableContentOutlinePage outline = null;
        if (page instanceof MTJSourcePage) {
            outline = ((MTJSourcePage) page).getContentOutline();
            if ((outline != null) && (outline instanceof L10nSourceOutlinePage)) {
                ((L10nSourceOutlinePage) outline).refresh();
            }
        } else {
            outline = getFormOutline();
            if ((outline != null) && (outline instanceof FormOutlinePage)) {
                ((FormOutlinePage) outline).refresh();
            }
        }
        contentOutline.setPageActive(outline);
    }

    /**
     * @param input
     */
    private void validateEdit(IEditorInput input) {
        final InputContext context = inputContextManager.getContext(input);
        if (!context.validateEdit()) {
            getSite().getShell().getDisplay().asyncExec(new Runnable() {
                public void run() {
                    doRevert(context.getInput());
                    context.setValidated(false);
                }
            });
        }
    }

    protected abstract void addEditorPages();

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
     */
    @Override
    protected final void addPages() {
        error = getAggregateModel() == null;
        if (error) {
            try {
                addPage(new MissingResourcePage(this));
            } catch (PartInitException e) {
                MTJLogger.log(IStatus.ERROR, e);
            }
        } else {
            addEditorPages();
        }
    }

    /**
     * @return
     */
    protected String computeInitialPageId() {
        String firstPageId = null;
        String storedFirstPageId = loadDefaultPage();
        if (storedFirstPageId != null) {
            firstPageId = storedFirstPageId;
        }
        // Regardless what is the stored value,
        // use source page if model is not valid
        String invalidContextId = getFirstInvalidContextId();
        if (invalidContextId != null) {
            return invalidContextId;
        }
        return firstPageId;
    }

    /**
     * @param manager
     */
    protected void contextMenuAboutToShow(IMenuManager manager) {
        MTJFormEditorContributor contributor = getContributor();
        IFormPage page = getActivePageInstance();
        if (page instanceof MTJFormPage) {
            ((MTJFormPage) page).contextMenuAboutToShow(manager);
        }
        if (contributor != null) {
            contributor.contextMenuAboutToShow(manager);
        }
    }

    abstract protected ISortableContentOutlinePage createContentOutline();

    protected abstract InputContextManager createInputContextManager();

    /**
     * @param contextManager
     */
    protected void createInputContexts(InputContextManager contextManager) {
        IEditorInput input = getEditorInput();
        if (input instanceof IFileEditorInput) {
            // resource - find the project
            createResourceContexts(contextManager, (IFileEditorInput) input);
        } else if (input instanceof SystemFileEditorInput) {
            // system file - find the file system folder
            createSystemFileContexts(contextManager,
                    (SystemFileEditorInput) input);
        } else if (input instanceof IStorageEditorInput) {
            createStorageContexts(contextManager, (IStorageEditorInput) input);
        } else if (input instanceof IURIEditorInput) {
            IURIEditorInput uriEditorInput = (IURIEditorInput) input;
            try {
                IFileStore store = EFS.getStore(uriEditorInput.getURI());
                if (!EFS.SCHEME_FILE.equals(store.getFileSystem().getScheme())) {
                    return;
                }
            } catch (CoreException e) {
                return;
            }
            File file = new File(uriEditorInput.getURI());
            SystemFileEditorInput sinput = new SystemFileEditorInput(file);
            createSystemFileContexts(contextManager, sinput);
        }
    }

    /**
     * When sub-classed, don't forget to call <code>super</code>
     * 
     * @see org.eclipse.ui.forms.editor.FormEditor#createPages()
     */
    @Override
    protected void createPages() {
        clipboard = new Clipboard(getContainer().getDisplay());
        MenuManager manager = new MenuManager();
        IMenuListener listener = new IMenuListener() {
            public void menuAboutToShow(IMenuManager manager) {
                contextMenuAboutToShow(manager);
            }
        };
        manager.setRemoveAllWhenShown(true);
        manager.addMenuListener(listener);
        contextMenu = manager.createContextMenu(getContainer());
        getContainer().setMenu(contextMenu);
        createInputContexts(inputContextManager);
        super.createPages();
        inputContextManager.addInputContextListener(this);
        String pageToShow = computeInitialPageId();
        if (pageToShow != null) {
            setActivePage(pageToShow);
        }
        updateTitle();
    }

    /**
     * @param contexts
     * @param input
     */
    protected abstract void createResourceContexts(
            InputContextManager contexts, IFileEditorInput input);

    /**
     * We must override nested site creation so that we properly pass the source
     * editor contributor when asked.
     * 
     * @see org.eclipse.ui.part.MultiPageEditorPart#createSite(org.eclipse.ui.IEditorPart)
     */
    @Override
    protected IEditorSite createSite(IEditorPart editor) {
        return new MTJMultiPageEditorSite(this, editor);
    }

    /**
     * @param contexts
     * @param input
     */
    protected abstract void createStorageContexts(InputContextManager contexts,
            IStorageEditorInput input);

    /**
     * @param contexts
     * @param input
     */
    protected abstract void createSystemFileContexts(
            InputContextManager contexts, SystemFileEditorInput input);

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormEditor#createToolkit(org.eclipse.swt.widgets.Display)
     */
    @Override
    protected FormToolkit createToolkit(Display display) {
        // Create a toolkit that shares colors between editors.
        return new FormToolkit(MTJUIPlugin.getDefault().getFormColors(display));
    }

    /**
     * Use one global setting for all files belonging to a given editor type.
     * Use the editor ID as the key. Could use the storage editor input to get
     * the underlying file and use it as a unique key; but, the dialog settings
     * file will grow out of control and we do not need that level of
     * granularity
     * 
     * @return
     */
    protected String getDialogEditorPageKey() {
        IDialogSettings section = getSettingsSection();
        return section.get(getEditorID());
    }

    /**
     * @return
     */
    protected abstract String getEditorID();

    /**
     * @return outline page or null
     */
    protected ISortableContentOutlinePage getFormOutline() {
        if (formOutline == null) {
            formOutline = createContentOutline();
            if (formOutline != null) {
                editorSelectionChangedListener = new MTJFormEditorChangeListener();
                editorSelectionChangedListener.install(getSite()
                        .getSelectionProvider());
            }
        }
        return formOutline;
    }

    protected abstract InputContext getInputContext(Object object);

    /**
     * @return
     */
    protected IProgressMonitor getProgressMonitor() {
        IProgressMonitor monitor = null;
        IStatusLineManager manager = getStatusLineManager();
        if (manager != null) {
            monitor = manager.getProgressMonitor();
        }
        if (monitor == null) {
            monitor = new NullProgressMonitor();
        }
        return monitor;
    }

    /**
     * @param input
     * @return
     */
    protected String getPropertyEditorPageKey(IFileEditorInput input) {
        // We are using the file itself to persist the editor page key property
        // The value persists even after the editor is closed
        IFile file = input.getFile();
        // Get the persistent editor page key from the file
        try {
            return file
                    .getPersistentProperty(IMTJUIConstants.PROPERTY_EDITOR_PAGE_KEY);
        } catch (CoreException e) {
            return null;
        }
    }

    /**
     * @return
     */
    protected IStatusLineManager getStatusLineManager() {
        return getEditorSite().getActionBars().getStatusLineManager();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormEditor#pageChange(int)
     */
    @Override
    protected void pageChange(int newPageIndex) {
        super.pageChange(newPageIndex);
        IFormPage page = getActivePageInstance();
        updateContentOutline(page);
        if (page != null) {
            lastActivePageId = page.getId();
        }
    }

    /**
     * @param id
     */
    protected void performGlobalAction(String id) {
        // preserve selection
        ISelection selection = getSelection();
        boolean handled = ((MTJFormPage) getActivePageInstance())
                .performGlobalAction(id);
        if (!handled) {
            IFormPage page = getActivePageInstance();
            if (page instanceof MTJFormPage) {
                if (id.equals(ActionFactory.UNDO.getId())) {
                    inputContextManager.undo();
                    return;
                }
                if (id.equals(ActionFactory.REDO.getId())) {
                    inputContextManager.redo();
                    return;
                }
                if (id.equals(ActionFactory.CUT.getId())
                        || id.equals(ActionFactory.COPY.getId())) {
                    copyToClipboard(selection);
                    return;
                }
            }
        }
    }

    /**
     * @param pageID
     */
    protected void setDialogEditorPageKey(String pageID) {
        // Use one global setting for all files belonging to a given editor
        // type. Use the editor ID as the key.
        // Could use the storage editor input to get the underlying file
        // and use it as a unique key; but, the dialog settings file will
        // grow out of control and we do not need that level of granularity
        IDialogSettings section = getSettingsSection();
        section.put(getEditorID(), pageID);
    }

    /**
     * @param input
     * @param pageId
     */
    protected void setPropertyEditorPageKey(IFileEditorInput input,
            String pageId) {
        // We are using the file itself to persist the editor page key property
        // The value persists even after the editor is closed
        IFile file = input.getFile();
        try {
            // Set the editor page ID as a persistent property on the file
            file.setPersistentProperty(
                    IMTJUIConstants.PROPERTY_EDITOR_PAGE_KEY, pageId);
        } catch (CoreException e) {
            // Ignore
        }
    }

    /* package */IFormPage[] getPages() {
        ArrayList<IFormPage> formPages = new ArrayList<IFormPage>();
        for (int i = 0; i < pages.size(); i++) {
            Object page = pages.get(i);
            if (page instanceof IFormPage) {
                formPages.add((IFormPage) page);
            }
        }
        return formPages.toArray(new IFormPage[formPages.size()]);
    }

    void updateUndo(IAction undoAction, IAction redoAction) {
        IModelUndoManager undoManager = inputContextManager.getUndoManager();
        if (undoManager != null) {
            undoManager.setActions(undoAction, redoAction);
        }
    }

}
