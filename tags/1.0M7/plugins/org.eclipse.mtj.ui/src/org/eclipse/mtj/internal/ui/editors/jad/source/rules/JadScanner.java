/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.editors.jad.source.rules;

import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IWhitespaceDetector;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.mtj.core.project.midp.DescriptorPropertyDescription;
import org.eclipse.mtj.internal.ui.editors.jad.form.JADAttributesRegistry;
import org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;

/**
 * A scanner programmed with a sequence of rules specific to the JAD file.
 * 
 * @author Diego Madruga Sandin
 */
public class JadScanner extends RuleBasedScanner {

	/**
	 * Symbolic name for the keyword colors
	 */
	private static final String KEYWORD_COLOR= "mtj.jad.keyword.color"; //$NON-NLS-1$
	/**
	 * Symbolic name for value colors
	 */
    private static final String VALUE_COLOR= "mtj.jad.value.color"; //$NON-NLS-1$

    
    // This is probably not the best way to handle the colors for this scanner
    // it will do until the colors are integrated with preferences.
    private static final ColorRegistry colorRegistry = new ColorRegistry();
    static{
    	colorRegistry.put(KEYWORD_COLOR, new RGB(127,0, 85));
    	colorRegistry.put(VALUE_COLOR, new RGB(0,0, 0));
    }
    
    /**
     * Creates a new Scanner
     */
    public JadScanner() {

    	IWordDetector detector = new IWordDetector() {
            public boolean isWordPart(char c) {
                if (c == ':') {
                    return false;
                } 
                if (c == '-' ){
                	return true;
                }
                return Character.isJavaIdentifierPart(c);
            }
            public boolean isWordStart(char c) {
                return Character.isJavaIdentifierStart(c);
            }
        };
        
        final Token keyword = new Token(new TextAttribute(colorRegistry.get(KEYWORD_COLOR), null, SWT.BOLD ));
        final Token key = new Token(new TextAttribute(colorRegistry.get(KEYWORD_COLOR), null, SWT.NONE));
        
        WordRule rule = new WordRule(detector,key);

//      add tokens for each reserved word 
       IJADDescriptorsProvider[] providers = JADAttributesRegistry.getAllJADDescriptorProviders();
       for (int i = 0; i < providers.length; i++) {
    	   DescriptorPropertyDescription[] descriptions = providers[i].getDescriptorPropertyDescriptions();
    	   for (int j = 0; j < descriptions.length; j++) {
    		   rule.addWord( descriptions[j].getPropertyName(), keyword);
    	   }
       }

		Token string = new Token(new TextAttribute(colorRegistry.get(VALUE_COLOR)));

		// Rule for Values  
		SingleLineRule singleLineRule = new SingleLineRule(":", null, string, '\\'); //$NON-NLS-1$
    	// Add generic whitespace rule.
        WhitespaceRule whiteSpaceRule = new WhitespaceRule(new IWhitespaceDetector() {
            public boolean isWhitespace(char c) {
                return Character.isWhitespace(c);
            }
        });
        
        setRules(new IRule[] { rule, singleLineRule, whiteSpaceRule });
    }
}
