/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version.
 */
package org.eclipse.mtj.ui.templates;

import java.util.Map;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.swt.widgets.Composite;

/**
 * AbstractTemplateWizardPage class is intended to be extended by template 
 * implementations to add a custom template page to the New MIDlet Wizard. 
 * The custom template page contains fields for template customization that 
 * will setup some template specific values.
 * 
 * @since 1.0
 */
public abstract class AbstractTemplateWizardPage extends WizardPage {

    // Constants -----------------------------------------------------

    // Attributes ----------------------------------------------------

    // Static --------------------------------------------------------

    // Constructors --------------------------------------------------

    /**
     * Creates an AbstractTemplateWizardPage instance. This abstract
     * page is intended to be extended by template providers in order
     * to add template specific fields.
     * 
     * @param name page name.
     */
    protected AbstractTemplateWizardPage(String name) {
        super(name);
    }

    /**
     * Creates an AbstractTemplateWizardPage instance. This abstract
     * page is intended to be extended by template providers in order
     * to add template specific fields.
     */
    protected AbstractTemplateWizardPage() {
        super(Utils.EMPTY_STRING);
    }
    
    // Public --------------------------------------------------------

    /**
     * Gets the template dictionary of tags
     * and it's corresponding values according
     * to the page's custom fields.
     * 
     * @return template's dictionary.
     */
    public abstract Map<String, String> getDictionary();

    // X implementation ----------------------------------------------

    /**
     * Creates the Wizard Template custom page under the given 
     * parent composite.
     *
     * @param parent the parent composite.
     */
    public abstract void createControl(Composite parent);

    /**
     * Returns whether this page is complete or not.
     * <p>
     * This information is typically used by the wizard to decide
     * when it is okay to finish.
     * </p>
     *
     * @return <code>true</code> if this page is complete, and
     *  <code>false</code> otherwise
     */
    public abstract boolean isPageComplete();
    
    // Y overrides ---------------------------------------------------

    // Package protected ---------------------------------------------

    // Protected -----------------------------------------------------

    // Private -------------------------------------------------------
}
