/*******************************************************************************
 * Copyright (c) 2007, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.mtj.internal.ui.editors.l10n.details;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.dialog.SrcFolderFilter;
import org.eclipse.mtj.internal.ui.editor.FormEntryAdapter;
import org.eclipse.mtj.internal.ui.editors.l10n.L10nInputContext;
import org.eclipse.mtj.internal.ui.editors.l10n.LocalesTreeSection;
import org.eclipse.mtj.internal.ui.forms.parts.FormEntry;
import org.eclipse.mtj.internal.ui.util.FolderValidator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.SelectionDialog;
import org.eclipse.ui.forms.IFormPart;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

/**
 * @since 0.9.1
 */
public class L10nLocalesDetails extends L10nAbstractDetails {

    private L10nLocales locales;

    private FormEntry locationEntry;
    private FormEntry packageEntry;

    /**
     * @param masterSection
     */
    public L10nLocalesDetails(LocalesTreeSection masterSection) {
        super(masterSection, L10nInputContext.CONTEXT_ID);
        locales = null;

        packageEntry = null;
        locationEntry = null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.AbstractFormPart#commit(boolean)
     */
    @Override
    public void commit(boolean onSave) {
        super.commit(onSave);
        // Only required for form entries
        packageEntry.commit();
        locationEntry.commit();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#createFields(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createFields(Composite parent) {
        createPackageWidget(parent);
        createSpace(parent);
        createLocationWidget(parent);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#hookListeners()
     */
    @Override
    public void hookListeners() {
        createPackageEntryListeners();
        createLocationEntryListeners();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#selectionChanged(org.eclipse.ui.forms.IFormPart, org.eclipse.jface.viewers.ISelection)
     */
    @Override
    public void selectionChanged(IFormPart part, ISelection selection) {
        // Get the first selected object
        Object object = getFirstSelectedObject(selection);
        // Ensure we have the right type
        if ((object != null) && (object instanceof L10nLocales)) {
            // Set data
            setData((L10nLocales) object);
            // Update the UI given the new data
            updateFields();
        }
    }

    /**
     * @param object
     */
    public void setData(L10nLocales object) {
        // Set data
        locales = object;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#updateFields()
     */
    @Override
    public void updateFields() {
        // Ensure data object is defined
        if (locales != null) { // Update name entry
            updatePackageEntry(isEditableElement());
            updateDestinationEntry(isEditableElement());
        }
    }

    /**
     * Creates the FormEntryListener for the {@link #locationEntry} FormEntry
     */
    private void createLocationEntryListeners() {

        locationEntry.setFormEntryListener(new FormEntryAdapter(this) {
            /**
             * @param entry
             */
            @Override
            public void browseButtonSelected(FormEntry entry) {
                handleLocationEntryBrowse();
            }

            /**
             * @param entry
             */
            @Override
            public void textValueChanged(FormEntry entry) {
                // Ensure data locales object was defined
                if (locales != null) {
                    locales.setDestination(locationEntry.getValue());
                }
            }
        });
    }

    /**
     * @param parent
     */
    private void createLocationWidget(Composite parent) {
        createLabel(parent, getManagedForm().getToolkit(),
                MTJUIMessages.L10nLocalesDetails_locationWidget_label);

        locationEntry = new FormEntry(parent, getManagedForm().getToolkit(),
                MTJUIMessages.L10nLocalesDetails_locationEntry_label, MTJUIMessages.L10nLocalesDetails_browse_label, false);
    }

    /**
     * Creates the FormEntryListener for the {@link #packageEntry} FormEntry
     */
    private void createPackageEntryListeners() {
        packageEntry.setFormEntryListener(new FormEntryAdapter(this) {

            /**
             * @param entry
             */
            @Override
            public void browseButtonSelected(FormEntry entry) {
                handlePackageEntryBrowse();
            }

            /**
             * @param entry
             */
            @Override
            public void textValueChanged(FormEntry entry) {
                // Ensure data object is defined
                if (locales != null) {
                    {
                        locales.setPackage(packageEntry.getValue());
                    }
                }
            }
        });
    }

    private void handlePackageEntryBrowse() {
        try {
            IResource file = locales.getModel().getUnderlyingResource();
            IJavaProject project = JavaCore.create(file.getProject());

            SelectionDialog dialog = JavaUI.createPackageDialog(getPage()
                    .getSite().getShell(), project, SWT.NONE);
            dialog
                    .setMessage(MTJUIMessages.L10nLocalesDetails_packageEntryBrowseDialog_message);
            dialog.setTitle(MTJUIMessages.L10nLocalesDetails_packageEntryBrowseDialog_title);
            if (dialog.open() == Window.OK) {
                Object result[] = dialog.getResult();
                if (result.length > 0) {
                    // Ensure data object is defined
                    if (locales != null) {

                        packageEntry.setValue(((IPackageFragment) dialog
                                .getResult()[0]).getElementName());

                    }
                }
            }
        } catch (JavaModelException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }

    }

    /**
     * @param parent
     */
    private void createPackageWidget(Composite parent) {
        createLabel(parent, getManagedForm().getToolkit(),
                MTJUIMessages.L10nLocalesDetails_packageWidget_label);

        packageEntry = new FormEntry(parent, getManagedForm().getToolkit(),
                MTJUIMessages.L10nLocalesDetails_packageEntry_label, MTJUIMessages.L10nLocalesDetails_browse_label, false);

    }

    /**
     * Handle the selection of a folder to be used as output for .properties
     * resources
     */
    private void handleLocationEntryBrowse() {
        ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
                getPage().getSite().getShell(), new WorkbenchLabelProvider(),
                new WorkbenchContentProvider());

        IResource file = locales.getModel().getUnderlyingResource();
        IJavaProject project = JavaCore.create(file.getProject());

        dialog.setValidator(new FolderValidator());
        dialog.setAllowMultiple(false);
        dialog.setTitle(MTJUIMessages.L10nLocalesDetails_locationEntryBrowseDialog_title);
        dialog
                .setMessage(MTJUIMessages.L10nLocalesDetails_locationEntryBrowseDialog_message);

        try {
            dialog.addFilter(new SrcFolderFilter(project.getProject(), Utils
                    .getJavaProjectSourceDirectories(project)));
        } catch (CoreException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }

        dialog.setInput(MTJUIPlugin.getWorkspace().getRoot());

        if (dialog.open() == Window.OK) {
            IFolder folder = (IFolder) dialog.getFirstResult();
            locationEntry.setValue(folder.getProjectRelativePath().toString());
        }
    }

    /**
     * @param editable
     */
    private void updateDestinationEntry(boolean editable) {
        locationEntry.setValue(locales.getDestination(), true);
        locationEntry.setEditable(editable);
    }

    /**
     * @param editable
     */
    private void updatePackageEntry(boolean editable) {
        packageEntry.setValue(locales.getPackage(), true);
        packageEntry.setEditable(editable);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#getDetailsDescription()
     */
    @Override
    protected String getDetailsDescription() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#getDetailsTitle()
     */
    @Override
    protected String getDetailsTitle() {
        return MTJUIMessages.L10nLocalesDetails_detailsTitle;
    }
}
