/*******************************************************************************
 * Copyright (c) 2000, 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Adapted code available in JDT
 */
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.SharedScrolledComposite;

/**
 * This class is used to provide common scrolling services to SWT controls.
 * 
 * @since 0.9.1
 */
public class ScrolledPageContent extends SharedScrolledComposite {

    /**
     * The toolkit responsible for creating SWT controls adapted to work in
     * Eclipse forms.
     */
    private FormToolkit fToolkit;

    /**
     * Creates a new ScrolledPageContent with {@link SWT#V_SCROLL} |
     * {@link SWT#H_SCROLL} as style.
     * 
     * @param parent the parent composite
     */
    public ScrolledPageContent(Composite parent) {
        this(parent, SWT.V_SCROLL | SWT.H_SCROLL);
    }

    /**
     * Creates a new ScrolledPageContent.
     * 
     * @param parent the parent composite
     * @param style the parent composite
     */
    public ScrolledPageContent(Composite parent, int style) {
        super(parent, style);

        setFont(parent.getFont());

        fToolkit = MTJUIPlugin.getDefault().getDialogsFormToolkit();

        setExpandHorizontal(true);
        setExpandVertical(true);

        Composite body = new Composite(this, SWT.NONE);
        body.setFont(parent.getFont());
        setContent(body);
    }

    /**
     * Adapts a control to be used in a form that is associated with this
     * toolkit. This involves adjusting colors and optionally adding handlers to
     * ensure focus tracking and keyboard management.
     * 
     * @param control a control to adapt
     */
    public void adaptChild(Control childControl) {
        fToolkit.adapt(childControl, true, true);
    }

    /**
     * Get the content that is being scrolled.
     * 
     * @return the control displayed in the content area
     */
    public Composite getBody() {
        return (Composite) getContent();
    }

}
