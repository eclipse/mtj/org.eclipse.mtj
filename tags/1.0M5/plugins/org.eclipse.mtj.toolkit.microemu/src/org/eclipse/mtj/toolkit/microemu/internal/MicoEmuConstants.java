/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 */
package org.eclipse.mtj.toolkit.microemu.internal;

/**
 * This Class holds the constant values to be used by the
 * <code>org.eclipse.mtj.toolkit.microemu</code> bundle
 * 
 * @author Diego Madruga Sandin
 */
public final class MicoEmuConstants {

    /* Persistence keys */

    public static final String TOOLKITROOT_PERSISTENCE_KEY = "toolkitRoot";

    public static final String SKIN_JAR_PERSISTENCE_KEY = "skinJar";

    public static final String SKIN_PATH_PERSISTENCE_KEY = "skinPathInJar";

    public static final String SKIN_NAME_PERSISTENCE_KEY = "skinName";

    /* MicroEmulator Device Skins */

    // The standard skin
    public static final String EMULATOR_STANDARDSKIN_JAR = "microemulator.jar";

    public static final String EMULATOR_STANDARDSKIN_PATH = "org/microemu/device/default/device.xml";

    public static final String EMULATOR_STANDARDSKIN_NAME = "Default device";

    // The large Skin
    public static final String EMULATOR_LARGESKIN_JAR = "devices/microemu-device-large.jar";

    public static final String EMULATOR_LARGESKIN_PATH = "org/microemu/device/large/device.xml";

    public static final String EMULATOR_LARGESKIN_NAME = "Large Device";

    // The minimum Skin
    public static final String EMULATOR_MINIMUNSKIN_JAR = "devices/microemu-device-minimum.jar";

    public static final String EMULATOR_MINIMUNSKIN_PATH = "org/microemu/device/minimum/device.xml";

    public static final String EMULATOR_MINIMUNSKIN_NAME = "Minimum device";

}
