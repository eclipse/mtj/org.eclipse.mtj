/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.core.library.classpath;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IAccessRule;

/**
 * Describes an access rule to class files on a classpath entry. An access rule
 * is composed of a file pattern and a kind (accessible, non accessible, or
 * discouraged).
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class ClasspathAccessRule implements IAccessRule {

    private static final String ACCESSIBLE = "ACCESSIBLE"; //$NON-NLS-1$
    private static final String DISCOURAGED = "DISCOURAGED"; //$NON-NLS-1$
    private static final String NON_ACCESSIBLE = "NON_ACCESSIBLE"; //$NON-NLS-1$

    /**
     * {@link IAccessRule#K_ACCESSIBLE}, {@link IAccessRule#K_DISCOURAGED} or
     * {@link IAccessRule#K_NON_ACCESSIBLE}
     */
    private int kind;

    /**
     * The access rule file pattern.
     */
    private IPath pattern;

    /**
     * Creates a new ClasspathAccessRule
     * 
     * @param pattern An access rule file pattern.
     * @param kind An access rule kind (accessible, non accessible, or
     *            discouraged).<br>
     *            <code>ACCESSIBLE</code>: indicates that files matching the
     *            rule's pattern are accessible.<br>
     *            <code>NON_ACCESSIBLE</code>: indicates that files matching the
     *            rule's pattern are non accessible.<br>
     *            <code>DISCOURAGED</code>: indicates that access to the files
     *            matching the rule's pattern is discouraged.<br>
     */
    public ClasspathAccessRule(IPath pattern, String kind) {
        this.pattern = pattern;
        this.kind = kindStringToProblemId(kind);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.IAccessRule#getKind()
     */
    public int getKind() {
        return kind;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.IAccessRule#getPattern()
     */
    public IPath getPattern() {
        return pattern;
    }

    /**
     * Should return whether a type matching this rule should be ignored if a
     * type with the same qualified name can be found on a later classpath entry
     * with a better accessibility.
     * 
     * @return <code>false</code>
     * @see org.eclipse.jdt.core.IAccessRule#ignoreIfBetter()
     */
    public boolean ignoreIfBetter() {
        return false;
    }

    /**
     * Converts an String that describes an access rule kind to its match
     * {@link IAccessRule} constant.
     * 
     * @param kind An access rule kind (accessible, non accessible, or
     *            discouraged).<br>
     *            <code>ACCESSIBLE</code>: indicates that files matching the
     *            rule's pattern are accessible.<br>
     *            <code>NON_ACCESSIBLE</code>: indicates that files matching the
     *            rule's pattern are non accessible.<br>
     *            <code>DISCOURAGED</code>: indicates that access to the files
     *            matching the rule's pattern is discouraged.<br>
     * @return If the informed kind is <code>ACCESSIBLE</code>:
     *         {@link IAccessRule#K_ACCESSIBLE} <br>
     *         If the informed kind is <code>DISCOURAGED</code>:
     *         {@link IAccessRule#K_DISCOURAGED} <br>
     *         If the informed kind is <code>NON_ACCESSIBLE</code>:
     *         {@link IAccessRule#K_NON_ACCESSIBLE}<br>
     *         Any other value : {@link IAccessRule#K_ACCESSIBLE}
     */
    private int kindStringToProblemId(String kind) {
        int problemId = 0;

        if (kind.equals(ACCESSIBLE)) {
            problemId = IAccessRule.K_ACCESSIBLE;
        } else if (kind.equals(NON_ACCESSIBLE)) {
            problemId = IAccessRule.K_NON_ACCESSIBLE;
        } else if (kind.equals(DISCOURAGED)) {
            problemId = IAccessRule.K_DISCOURAGED;
        }

        return problemId;
    }

}
