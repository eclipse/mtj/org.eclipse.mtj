/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.core.library;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 * 
 * @since 0.9.1
 */
public class LibraryPlugin extends Plugin {

    /**
     * The plug-in ID
     */
    public static final String PLUGIN_ID = "org.eclipse.mtj.core.library";

    // The shared instance
    private static LibraryPlugin plugin;

    /**
     * Returns the shared instance
     * 
     * @return the shared instance
     */
    public static LibraryPlugin getDefault() {
        return plugin;
    }

    /**
     * The constructor
     */
    public LibraryPlugin() {
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * @param error
     * @param string
     * @param object
     */
    public static void log(int error, String message, Throwable throwable) {
        String id = PLUGIN_ID;
        Status status = new Status(error, id, IStatus.OK, message, throwable);
        plugin.getLog().log(status);
    }

}
