/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.core.library.classpath;

import org.eclipse.jdt.core.IClasspathAttribute;

/**
 * A javadoc location classpath attribute that can be persisted with a classpath
 * entry.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class JavadocClasspathAttribute implements IClasspathAttribute {

    /**
     * The javadoc location
     */
    private String location;

    /**
     * Creates a new JavadocClasspathAttribute.
     * 
     * @param location the javadoc location
     */
    public JavadocClasspathAttribute(String location) {
        this.location = location;
    }

    /**
     * Returns the name of this javadoc location classpath attribute.
     * 
     * @return always returns
     *         {@link IClasspathAttribute#JAVADOC_LOCATION_ATTRIBUTE_NAME}.
     * @see org.eclipse.jdt.core.IClasspathAttribute#getName()
     */
    public String getName() {
        return JAVADOC_LOCATION_ATTRIBUTE_NAME;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.core.IClasspathAttribute#getValue()
     */
    public String getValue() {
        return location;
    }

}
