/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.text;

import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.IAnnotationModelFactory;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.mtj.internal.ui.editor.SystemFileMarkerAnnotationModel;

/**
 * Factory for text file buffer annotation models. Used by the text file buffer
 * manager to create the annotation model for a new text file buffer.
 * 
 * @since 0.9.1
 */
public class AnnotationModelFactory implements IAnnotationModelFactory {

    /* (non-Javadoc)
     * @see org.eclipse.core.filebuffers.IAnnotationModelFactory#createAnnotationModel(org.eclipse.core.runtime.IPath)
     */
    public IAnnotationModel createAnnotationModel(IPath location) {

        IFile file = FileBuffers.getWorkspaceFileAtLocation(location);
        if (file == null) {
            return new SystemFileMarkerAnnotationModel();
        }

        return new MTJMarkerAnnotationModel(file);
    }

}
