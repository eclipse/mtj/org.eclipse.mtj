/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.model.configuration.Configuration;
import org.eclipse.mtj.core.model.configuration.Configurations;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;
import org.eclipse.mtj.core.model.preprocessor.symbol.Symbol;
import org.eclipse.mtj.core.model.preprocessor.symbol.SymbolSet;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;

/**
 * This class is used to manage the preprocess symbols
 * 
 * @author gma
 * @since 0.9.1
 */
public class PreprocessSymbolManager {
    private static void createSymbol(HashSet<PreprocessSymbol> symbols,
            String symbolName, int symbolType, String value,
            String providerName, boolean isActive) {
        PreprocessSymbol symbol = new PreprocessSymbol(symbolName, symbolType);
        PreprocessSymbolProviderInfo provider = new PreprocessSymbolProviderInfo(
                providerName, value, isActive);
        if (symbols.contains(symbol)) {
            Iterator<PreprocessSymbol> it = symbols.iterator();
            while (it.hasNext()) {
                PreprocessSymbol aSymbol = it.next();
                // find the symbol
                if (symbol.equals(aSymbol)) {
                    aSymbol.addProvider(provider);
                }
            }
        } else {
            symbol.addProvider(provider);
            symbols.add(symbol);
        }
    }

    /**
     * Retrieve all the symbols according to the specific project
     * 
     * @param project
     * @return List of <code>PreprocessSymbol</code>
     */
    public static List<PreprocessSymbol> getSymbols(IProject project) {
        HashSet<PreprocessSymbol> symbolmap = new HashSet<PreprocessSymbol>();

        IMidletSuiteProject midletSuite = MidletSuiteFactory
                .getMidletSuiteProject(JavaCore.create(project));
        // add the symbols provided by configurations associated to the project
        Configurations configurations = midletSuite.getConfigurations();
        for (Configuration configuration : configurations) {
            SymbolSet symbolSet = configuration.getSymbolSetForPreprocessing();
            boolean isActive = configuration.isActive();
            Iterator<Symbol> it = symbolSet.iterator();
            while (it.hasNext()) {
                Symbol symbol = it.next();
                createSymbol(symbolmap, symbol.getName(), symbol.getType(),
                        symbol.getValue(), configuration.getName(), isActive);
            }
            // add the symbols from workspace symbol set definition
            List<SymbolDefinitionSet> workspaceSymbolSets = configuration
                    .getWorkspaceScopeSymbolSets();

            for (SymbolDefinitionSet wsymbolset : workspaceSymbolSets) {
                Set<Entry<String, String>> symbols = wsymbolset
                        .getDefinedSymbols().entrySet();
                for (Entry<String, String> e : symbols) {
                    String key = e.getKey();
                    String value = e.getValue();
                    createSymbol(symbolmap, key, 0, value,
                            wsymbolset.getName(), isActive);
                }
            }
        }

        return new ArrayList<PreprocessSymbol>(symbolmap);
    }
}
