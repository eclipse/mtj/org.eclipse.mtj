/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.editors.jad.source.contentassist;

import org.eclipse.mtj.core.model.jad.IJADConstants;

public class JADAttributeHeadersProvider {
    private static final String[] fHeader = {
        IJADConstants.JAD_MIDLET_JAR_SIZE,
        IJADConstants.JAD_MIDLET_JAR_URL, IJADConstants.JAD_MIDLET_NAME,
        IJADConstants.JAD_MIDLET_VENDOR, IJADConstants.JAD_MIDLET_VERSION,
        IJADConstants.JAD_MICROEDITION_CONFIG,
        IJADConstants.JAD_MICROEDITION_PROFILE,
        IJADConstants.JAD_MIDLET_DATA_SIZE,
        IJADConstants.JAD_MIDLET_DELETE_CONFIRM,
        IJADConstants.JAD_MIDLET_DELETE_NOTIFY,
        IJADConstants.JAD_MIDLET_DESCRIPTION,
        IJADConstants.JAD_MIDLET_ICON, IJADConstants.JAD_MIDLET_INFO_URL,
        IJADConstants.JAD_MIDLET_INSTALL_NOTIFY,
        IJADConstants.JAD_MIDLET_PERMISSIONS,
        IJADConstants.JAD_MIDLET_PERMISSIONS_OPTIONAL,
        IJADConstants.JAD_MIDLET_JAR_RSA_SHA1 };
    
    public static String[] getSupportJADAttrHeaders(){
        return fHeader;
    }
}
