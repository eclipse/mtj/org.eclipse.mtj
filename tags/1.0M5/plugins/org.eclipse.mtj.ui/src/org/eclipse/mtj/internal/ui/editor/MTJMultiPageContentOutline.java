/**
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDEMultiPageContentOutline
 */
package org.eclipse.mtj.internal.ui.editor;

import java.util.ArrayList;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.mtj.internal.ui.IPreferenceConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.part.Page;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;

public class MTJMultiPageContentOutline extends Page implements
        IContentOutlinePage, ISelectionProvider, ISelectionChangedListener,
        IPreferenceConstants {

    class SortingAction extends Action {

        public SortingAction() {
            super();

            setText(MTJUIMessages.MTJMultiPageContentOutline_sortingAction_text);
            setImageDescriptor(MTJUIPluginImages.DESC_ALPHAB_SORT_CO);
            setDisabledImageDescriptor(MTJUIPluginImages.DESC_ALPHAB_SORT_CO_DISABLED);
            setToolTipText(MTJUIMessages.MTJMultiPageContentOutline_toolTipText);
            setDescription(MTJUIMessages.MTJMultiPageContentOutline_description);
            setChecked(sortingOn);
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.action.Action#run()
         */
        @Override
        public void run() {
            setChecked(isChecked());
            valueChanged(isChecked());
        }

        /**
         * @param on
         */
        private void valueChanged(final boolean on) {
            sortingOn = on;
            if (currentPage != null) {
                currentPage.sort(on);
            }
            MTJUIPlugin.getDefault().getPreferenceStore().setValue(
                    "MTJMultiPageContentOutline.SortingAction.isChecked", on); //$NON-NLS-1$
        }

    }

    private IActionBars actionBars;
    private ISortableContentOutlinePage currentPage;
    private MTJFormEditor editor;
    private ISortableContentOutlinePage emptyPage;
    private ArrayList<ISelectionChangedListener> listeners;
    private PageBook pagebook;
    private ISelection selection;

    private boolean sortingOn;

    public MTJMultiPageContentOutline(MTJFormEditor editor) {
        this.editor = editor;
        listeners = new ArrayList<ISelectionChangedListener>();
        sortingOn = MTJUIPlugin.getDefault().getPreferenceStore().getBoolean(
                "PDEMultiPageContentOutline.SortingAction.isChecked"); //$NON-NLS-1$

    }

    public void addFocusListener(FocusListener listener) {
    }

    public void addSelectionChangedListener(ISelectionChangedListener listener) {
        listeners.add(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.Page#createControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createControl(Composite parent) {
        pagebook = new PageBook(parent, SWT.NONE);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.Page#dispose()
     */
    @Override
    public void dispose() {
        if ((pagebook != null) && !pagebook.isDisposed()) {
            pagebook.dispose();
        }
        if (emptyPage != null) {
            emptyPage.dispose();
            emptyPage = null;
        }
        pagebook = null;
        listeners = null;
    }

    public IActionBars getActionBars() {
        return actionBars;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.Page#getControl()
     */
    @Override
    public Control getControl() {
        return pagebook;
    }

    public PageBook getPagebook() {
        return pagebook;
    }

    public ISelection getSelection() {
        return selection;
    }

    public boolean isDisposed() {
        return listeners == null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.Page#makeContributions(org.eclipse.jface.action.IMenuManager, org.eclipse.jface.action.IToolBarManager, org.eclipse.jface.action.IStatusLineManager)
     */
    @Override
    public void makeContributions(IMenuManager menuManager,
            IToolBarManager toolBarManager, IStatusLineManager statusLineManager) {
    }

    public void removeFocusListener(FocusListener listener) {
    }

    public void removeSelectionChangedListener(
            ISelectionChangedListener listener) {
        listeners.remove(listener);
    }

    public void selectionChanged(SelectionChangedEvent event) {
        setSelection(event.getSelection());
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.Page#setActionBars(org.eclipse.ui.IActionBars)
     */
    @Override
    public void setActionBars(IActionBars actionBars) {
        this.actionBars = actionBars;
        registerToolbarActions(actionBars);
        if (currentPage != null) {
            setPageActive(currentPage);
        }

    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.Page#setFocus()
     */
    @Override
    public void setFocus() {
        if (currentPage != null) {
            currentPage.setFocus();
        }
    }

    /**
     * @param page
     */
    public void setPageActive(ISortableContentOutlinePage page) {
        if (page == null) {
            page = getEmptyPage();
        }
        if (currentPage != null) {
            currentPage.removeSelectionChangedListener(this);
        }
        // page.init(getSite());
        page.sort(sortingOn);
        page.addSelectionChangedListener(this);
        this.currentPage = page;
        if (pagebook == null) {
            // still not being made
            return;
        }
        Control control = page.getControl();
        if ((control == null) || control.isDisposed()) {
            // first time
            page.createControl(pagebook);
            page.setActionBars(getActionBars());
            control = page.getControl();
        }
        pagebook.showPage(control);
        this.currentPage = page;
    }

    /**
     * Set the selection.
     */
    public void setSelection(ISelection selection) {
        this.selection = selection;
        if (listeners == null) {
            return;
        }
        SelectionChangedEvent e = new SelectionChangedEvent(this, selection);
        for (int i = 0; i < listeners.size(); i++) {
            (listeners.get(i)).selectionChanged(e);
        }
    }

    private ISortableContentOutlinePage getEmptyPage() {
        if (emptyPage == null) {
            emptyPage = new EmptyOutlinePage();
        }
        return emptyPage;
    }

    private void registerToolbarActions(IActionBars actionBars) {

        IToolBarManager toolBarManager = actionBars.getToolBarManager();
        if (toolBarManager != null) {
            toolBarManager.add(new ToggleLinkWithEditorAction(editor));
            toolBarManager.add(new SortingAction());
        }
    }
}