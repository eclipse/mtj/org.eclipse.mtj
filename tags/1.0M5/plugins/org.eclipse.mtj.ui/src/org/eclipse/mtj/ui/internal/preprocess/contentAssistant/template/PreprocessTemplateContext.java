/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant.template;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.templates.DocumentTemplateContext;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateBuffer;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.jface.text.templates.TemplateException;
import org.eclipse.jface.text.templates.TemplateTranslator;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.preprocess.contentAssistant.PreprocessContext;

/**
 * @author gma
 */
public class PreprocessTemplateContext extends DocumentTemplateContext {
    int ppDirectiveBeginOffset = 0;
    PreprocessContext ppContext;

    public PreprocessTemplateContext(TemplateContextType type,
            IDocument document, Position position, PreprocessContext ppContext) {
        super(type, document, position);
        this.ppDirectiveBeginOffset = ppContext.getStatementStartOffset();
        this.ppContext = ppContext;
    }

    @Override
    public boolean canEvaluate(Template template) {
        String key = getKey();
        return template.matches(key, getContextType().getId())
                && template.getName().toLowerCase().startsWith(
                        key.toLowerCase());
    }

    /*
     * @see org.eclipse.jface.text.templates.TemplateContext#evaluate(org.eclipse.jface.text.templates.Template)
     */
    @Override
    public TemplateBuffer evaluate(Template template)
            throws BadLocationException, TemplateException {

        TemplateBuffer buffer = evaluateWithoutFormatter(template);
        if (buffer == null) {
            return null;
        }

        IPreferenceStore prefs = MTJUIPlugin.getDefault().getPreferenceStore();
        boolean useCodeFormatter = prefs
                .getBoolean(IMTJUIConstants.TEMPLATES_USE_CODEFORMATTER);
        if (useCodeFormatter) {
            IJavaProject project = JavaCore.create(ppContext
                    .getContainedProject());
            PreprocessTemplateFormatter formatter = new PreprocessTemplateFormatter();
            formatter.format(buffer, this, project);

        }

        // JavaFormatter formatter2= new
        // JavaFormatter(TextUtilities.getDefaultLineDelimiter(getDocument()),
        // 2, false, project);
        // formatter2.format(buffer, this);

        return buffer;

    }

    public TemplateBuffer evaluateWithoutFormatter(Template template)
            throws BadLocationException, TemplateException {
        if (!canEvaluate(template)) {
            return null;
        }

        TemplateTranslator translator = new TemplateTranslator();
        TemplateBuffer buffer = translator.translate(template);

        getContextType().resolve(buffer, this);

        return buffer;
    }

    @Override
    public String getKey() {
        int offset = getPpDirectiveBeginOffset();
        int length = getEnd() - offset;
        try {
            return getDocument().get(offset, length);
        } catch (BadLocationException e) {
            return ""; //$NON-NLS-1$
        }
    }

    public int getPpDirectiveBeginOffset() {
        return ppDirectiveBeginOffset;
    }

}
