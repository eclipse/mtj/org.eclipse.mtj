/**
 * Copyright (c) 2008 Motorola Inc.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola) - Initial implementation                            
 */
package org.eclipse.mtj.ui.internal.wizards.importer.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;


/**
 * This class contains the data used to create a MTJ project
 */
public class ProjectRecord {
	
	/**
	 * Gets the provider used to access project contents.
	 * Returns null if the project is not stored inside a compressed file.
	 * @return
	 */
	public ILeveledImportStructureProvider getProvider() {
		return provider;
	}

	private String projectRoot;
	
	private String projectName;

	private ILeveledImportStructureProvider provider = null;
	
	public ProjectRecord(String projectName,
			String projectRoot) {
		this.projectName = projectName;
		this.projectRoot = projectRoot;
	}
	
	public ProjectRecord() {
	}

	public String getProjectRoot() {
		return projectRoot;
	}

	public void setProjectRoot(String root) {
		this.projectRoot = root;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * Get the name of the project
	 * 
	 * @return String
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * Gets the label to be used when rendering this project record in the
	 * UI.
	 * 
	 * @return String the label
	 * @since 3.4
	 */
	public String getProjectLabel() {
		return projectName + " (" + projectRoot + ")"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Sets the provider to be used to access the project contents.
	 * If a provider is set, the project is stored inside a compressed file.
	 * @param provider
	 */
	public void setProvider(ILeveledImportStructureProvider provider) {
		this.provider  = provider;
	}
	
	public InputStream getResourceContents(String file) throws FileNotFoundException {
		if (provider != null) {
			Object parentElement = ArchiveUtils.getChild(provider, provider.getRoot(), projectRoot);
			Object fileElement = ArchiveUtils.getChild(provider, parentElement, file);
			return provider.getContents(fileElement);
		} else {
			return new FileInputStream(new File(projectRoot, file));
		}
	}
	
}