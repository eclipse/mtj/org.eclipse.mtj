/*******************************************************************************
 * Copyright (c) 2005, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * IBM - Initial API and implementation
 * Red Hat, Inc - WizardProjectsImportPage[_ArchiveSelectTitle,
 * 										   _SelectArchiveDialogTitle]
 *******************************************************************************/
/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Copy from 
 *     		org.eclipse.ui.internal.wizards.datatransfer.DataTransferMessages,
 *     		which located in org.eclipse.ui.ide plug-in. And Modify it to suit for
 *          EclipseME project importing.
 *	   Hugo Raniere (Motorola) - Moved to commons package to be reused by
 *			other importers.
 *******************************************************************************/
package org.eclipse.mtj.ui.internal.wizards.importer.common;

import org.eclipse.osgi.util.NLS;

public class ProjectImporterMessage extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.mtj.ui.internal.wizards.importer.common.messages";//$NON-NLS-1$

	public static String DataTransfer_browse;
	public static String DataTransfer_selectAll;
	public static String DataTransfer_deselectAll;
	public static String DataTransfer_refresh;

	public static String ImportProjectWizardPage_ProjectAlreadyInWorkpace;

	public static String ProjectImporter_DeviceNotFound;
	public static String WizardImportPage_internalErrorTitle;
	public static String WizardDataTransfer_existsQuestion;
	public static String WizardDataTransfer_overwriteNameAndPathQuestion;
	public static String Question;
	public static String ZipImport_couldNotClose;
	public static String WizardProjectsImportPage_ImportProjectsTitle;
	public static String WizardProjectsImportPage_ImportProjectsDescription;
	public static String WizardProjectsImportPage_ProjectsListTitle;
	public static String WizardProjectsImportPage_ArchiveSelectTitle;
	public static String WizardProjectsImportPage_RootSelectTitle;
	public static String WizardProjectsImportPage_SearchingMessage;
	public static String WizardProjectsImportPage_ProcessingMessage;
	public static String ZipImport_badFormat;
	public static String ZipImport_couldNotRead;
	public static String TarImport_badFormat;
	public static String TarImport_invalid_tar_format;
	public static String WizardProjectsImportPage_CheckingMessage;
	public static String WizardProjectsImportPage_SelectDialogTitle;
	public static String WizardProjectsImportPage_SelectArchiveDialogTitle;
	public static String WizardExternalProjectImportPage_errorMessage;
	public static String WizardProjectsImportPage_CreateProjectTask;

	static {
		// load message values from bundle file
		NLS.initializeMessages(BUNDLE_NAME, ProjectImporterMessage.class);
	}
}
