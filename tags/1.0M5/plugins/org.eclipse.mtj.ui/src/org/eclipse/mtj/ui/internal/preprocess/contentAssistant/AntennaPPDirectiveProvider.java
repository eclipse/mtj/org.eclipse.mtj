/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant;

/**
 * privide the Antenna preprocess directives information
 * 
 * @author gma
 * @since 0.9.1
 */
public class AntennaPPDirectiveProvider implements IPreprocessDirectiveProvider {
    // TODO maybe a better way is to provide directives via .xml file
    private static final PreprocessDirective[] directives = new PreprocessDirective[] {
            new PreprocessDirective("ifdef",
                    PreprocessContentAssistMessages.ifdef_desc),
            new PreprocessDirective("ifndef",
                    PreprocessContentAssistMessages.ifndef_desc),
            new PreprocessDirective("elifdef",
                    PreprocessContentAssistMessages.elifdef_desc),
            new PreprocessDirective("elifndef",
                    PreprocessContentAssistMessages.elifndef_desc),
            new PreprocessDirective("if",
                    PreprocessContentAssistMessages.if_desc),
            new PreprocessDirective("elif",
                    PreprocessContentAssistMessages.elif_desc),
            new PreprocessDirective("else",
                    PreprocessContentAssistMessages.else_desc),
            new PreprocessDirective("endif",
                    PreprocessContentAssistMessages.endif_desc),
            new PreprocessDirective("condition",
                    PreprocessContentAssistMessages.condition_desc),
            new PreprocessDirective("debug",
                    PreprocessContentAssistMessages.debug_desc),
            new PreprocessDirective("mdebug",
                    PreprocessContentAssistMessages.mdebug_desc),
            new PreprocessDirective("enddebug",
                    PreprocessContentAssistMessages.enddebug_desc),
            new PreprocessDirective("define",
                    PreprocessContentAssistMessages.define_desc),
            new PreprocessDirective("undefine",
                    PreprocessContentAssistMessages.undefine_desc),
            new PreprocessDirective("expand",
                    PreprocessContentAssistMessages.expand_desc) };

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.preprocess.contentAssistant.IPreprocessDirectiveProvider#getAllDirectives()
     */
    public PreprocessDirective[] getAllDirectives() {
        return directives;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.preprocess.contentAssistant.IPreprocessDirectiveProvider#getDirective(java.lang.String)
     */
    public PreprocessDirective getDirective(String directiveName) {
        for (PreprocessDirective element : directives) {
            if (element.getName().equalsIgnoreCase(directiveName)) {
                return element;
            }
        }
        return null;
    }

}
