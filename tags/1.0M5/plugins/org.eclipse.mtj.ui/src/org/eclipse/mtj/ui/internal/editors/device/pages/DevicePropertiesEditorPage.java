/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.editors.device.pages;

import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.ui.viewers.TableViewerConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;

/**
 * Device editor for the device properties.
 * 
 * @author Craig Setera
 */
public class DevicePropertiesEditorPage extends AbstractDeviceEditorPage {

    // Content provider for a device's classpath entries
    private static class DevicePropertiesContentProvider implements
            IStructuredContentProvider {
        public void dispose() {
        }

        public Object[] getElements(Object inputElement) {
            Object[] elements = NO_ELEMENTS;

            if (inputElement instanceof IDevice) {
                IDevice device = (IDevice) inputElement;
                Properties deviceProperties = device.getDeviceProperties();
                if (deviceProperties != null) {
                    Set<Entry<Object, Object>> entrySet = deviceProperties
                            .entrySet();
                    elements = entrySet.toArray(new Map.Entry[entrySet.size()]);
                }
            }

            return elements;
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }
    }

    // Label provider for Library instances
    private static class PropertyLabelProvider extends LabelProvider implements
            ITableLabelProvider {

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
         */
        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
         */
        @SuppressWarnings("unchecked")
        public String getColumnText(Object element, int columnIndex) {
            Map.Entry entry = (Entry) element;
            String text = ""; //$NON-NLS-1$

            switch (columnIndex) {
            case 0:
                text = entry.getKey().toString();
                break;

            case 1:
                text = entry.getValue().toString();
                break;
            }

            return text;
        }
    }

    private static final TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
            new TableColumnInfo(
                    MTJUIMessages.DevicePropertiesEditorPage_property_columnInfo,
                    40f, null),
            new TableColumnInfo(
                    MTJUIMessages.DevicePropertiesEditorPage_value_columnInfo,
                    60f, null), };

    // Column information structure
    private static final int DEFAULT_TABLE_WIDTH = 650;
    private static final Object[] NO_ELEMENTS = new Object[0];

    // Widgets
    private TableViewer viewer;

    /**
     * Construct the editor page.
     * 
     * @param parent
     * @param style
     */
    public DevicePropertiesEditorPage(Composite parent, int style) {
        super(parent, style);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#commitDeviceChanges()
     */
    @Override
    public void commitDeviceChanges() {
        // TODO Not allowing changes in this editor yet
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#getDescription()
     */
    @Override
    public String getDescription() {
        return MTJUIMessages.DevicePropertiesEditorPage_description;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#getTitle()
     */
    @Override
    public String getTitle() {
        return MTJUIMessages.DevicePropertiesEditorPage_title;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#setDevice(org.eclipse.mtj.core.model.device.IDevice)
     */
    @Override
    public void setDevice(IDevice device) {
        super.setDevice(device);
        viewer.setInput(device);
    }

    /**
     * Create the devices table viewer.
     * 
     * @param parent
     */
    private TableViewer createTableViewer(Composite composite) {
        int styles = SWT.SINGLE | SWT.V_SCROLL | SWT.BORDER
                | SWT.FULL_SELECTION;
        Table table = new Table(composite, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        // Wire up the viewer
        TableViewer viewer = new TableViewer(table);
        viewer.setContentProvider(new DevicePropertiesContentProvider());
        viewer.setLabelProvider(new PropertyLabelProvider());

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings("devicePropertiesViewerSettings"); //$NON-NLS-1$
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 0);
        viewerConfiguration.configure(viewer);

        return viewer;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.device.AbstractDeviceEditorPage#addPageControls(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void addPageControls(Composite parent) {
        parent.setLayoutData(new GridData(GridData.FILL_BOTH));
        parent.setLayout(new GridLayout(2, false));

        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.minimumWidth = DEFAULT_TABLE_WIDTH;
        gridData.heightHint = 400;
        viewer = createTableViewer(parent);
        viewer.getTable().setLayoutData(gridData);
    }
}
