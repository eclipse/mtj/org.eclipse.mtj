/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDEQuickAssistAssistant
 */
package org.eclipse.mtj.internal.ui.editor.text;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jface.text.AbstractReusableInformationControlCreator;
import org.eclipse.jface.text.DefaultInformationControl;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.DefaultInformationControl.IInformationPresenter;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension3;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.quickassist.QuickAssistAssistant;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IMarkerResolution;
import org.eclipse.ui.IMarkerResolution2;

/**
 * This class adds support for quick fixes and quick assists. The quick assist
 * assistant is a ISourceViewer add-on. Its propose, display, and insert quick
 * assists and quick fixes available at the current source viewer's quick assist
 * invocation context.
 * 
 * @since 0.9.1
 */
public class MTJQuickAssistAssistant extends QuickAssistAssistant {

    class MTJCompletionProposal implements ICompletionProposal,
            ICompletionProposalExtension3 {

        Position fPosition;
        IMarkerResolution fResolution;
        IMarker fMarker;

        /**
         * @param resolution
         * @param pos
         * @param marker
         */
        public MTJCompletionProposal(IMarkerResolution resolution,
                Position pos, IMarker marker) {
            fPosition = pos;
            fResolution = resolution;
            fMarker = marker;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.text.contentassist.ICompletionProposal#apply(org.eclipse.jface.text.IDocument)
         */
        public void apply(IDocument document) {
            fResolution.run(fMarker);
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getSelection(org.eclipse.jface.text.IDocument)
         */
        public Point getSelection(IDocument document) {
            return new Point(fPosition.offset, 0);
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getAdditionalProposalInfo()
         */
        public String getAdditionalProposalInfo() {
            if (fResolution instanceof IMarkerResolution2)
                return ((IMarkerResolution2) fResolution).getDescription();
            return null;
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getDisplayString()
         */
        public String getDisplayString() {
            return fResolution.getLabel();
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.text.contentassist.ICompletionProposal#getImage()
         */
        public Image getImage() {
            if (fResolution instanceof IMarkerResolution2) {
                IMarkerResolution2 resolution = (IMarkerResolution2) fResolution;
                return resolution.getImage();
            }
            return null;
        }

        public IContextInformation getContextInformation() {
            return null;
        }

        public IInformationControlCreator getInformationControlCreator() {
            return null;
        }

        public int getPrefixCompletionStart(IDocument document,
                int completionOffset) {
            return 0;
        }

        public CharSequence getPrefixCompletionText(IDocument document,
                int completionOffset) {
            return null;
        }

    }

    /**
     * 
     */
    public MTJQuickAssistAssistant() {
        // FIXME
        // setQuickAssistProcessor(new MTJQuickAssistProcessor());

        setInformationControlCreator(new AbstractReusableInformationControlCreator() {
            public IInformationControl doCreateInformationControl(Shell parent) {
                return new DefaultInformationControl(parent,
                        (IInformationPresenter) null);
            }
        });
    }

    /**
     * 
     */
    public void dispose() {

    }
}
