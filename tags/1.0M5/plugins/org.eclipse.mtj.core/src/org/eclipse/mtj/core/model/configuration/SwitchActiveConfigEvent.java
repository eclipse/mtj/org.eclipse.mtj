/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.model.configuration;

import java.util.EventObject;

/**
 * SwitchActiveConfigEvent is used to notify that active Configuration for a
 * Midlet project switched.
 * 
 * @author wangf
 */
public class SwitchActiveConfigEvent extends EventObject {

    private static final long serialVersionUID = 1L;

    private Configuration oldActiveConfig;
    private Configuration newActiveConfig;

    /**
     * Constructs a SwitchActiveConfigEvent object.
     * 
     * @param source - Configurations instance of the MTJ Midlet project.
     * @param oldActiveConfig - the old active configuration.
     * @param newActiveConfig - the new active configuration.
     */
    public SwitchActiveConfigEvent(Configurations source,
            Configuration oldActiveConfig, Configuration newActiveConfig) {
        super(source);
        this.oldActiveConfig = oldActiveConfig;
        this.newActiveConfig = newActiveConfig;
    }

    public Configuration getNewActiveConfig() {
        return newActiveConfig;
    }

    public Configuration getOldActiveConfig() {
        return oldActiveConfig;
    }

    public void setNewActiveConfig(Configuration newActiveConfig) {
        this.newActiveConfig = newActiveConfig;
    }

    public void setOldActiveConfig(Configuration oldActiveConfig) {
        this.oldActiveConfig = oldActiveConfig;
    }

}
