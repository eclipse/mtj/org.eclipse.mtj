/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.core.model.configuration;

import java.util.EventObject;

/**
 * RemoveConfigEvent is used to notify that a Configuration is removed from
 * Configurations.
 * 
 * @author wangf
 */
public class RemoveConfigEvent extends EventObject {
    private static final long serialVersionUID = 1L;

    private Configuration removedConfig;

    /**
     * Constructs a AddConfigEvent object.
     * 
     * @param source - Configurations instance of the MTJ Midlet project.
     * @param removedConfig - the removed configuration.
     */
    public RemoveConfigEvent(Configurations source, Configuration removedConfig) {
        super(source);
        this.removedConfig = removedConfig;
    }

    public Configuration getRemovedConfig() {
        return removedConfig;
    }

    public void setRemovedConfig(Configuration removedConfig) {
        this.removedConfig = removedConfig;
    }

}
