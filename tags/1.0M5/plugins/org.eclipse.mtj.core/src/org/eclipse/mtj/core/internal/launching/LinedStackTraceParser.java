/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.core.internal.launching;

import java.io.File;
import java.util.Stack;

import org.eclipse.jdt.core.IJavaProject;

/**
 * LinedStackTraceParser class parses stack traces for an IJavaProject. Parsing
 * a lined stack trace means resolving the source files to link with since the
 * line numbers are already resolved.
 * 
 * @author David Marques
 */
public class LinedStackTraceParser extends StackTraceParser {

    /**
     * Creates a LinedStackTraceParser for the specified file.
     * 
     * @param _project project instance.
     */
    public LinedStackTraceParser(IJavaProject _project) {
        super(_project);
    }

    /**
     * Parses the specified stack into a new stack with lines entries. </p> The
     * process for parsing the stack trace is only finding the source code since
     * the line number is already resolved.
     * 
     * @param _stack stack to parse.
     * @return a new stack parsed.
     * @throws StackTraceParserException if any error occurs.
     */
    public Stack<StackTraceEntry> parseStackTrace(Stack<StackTraceEntry> _stack)
            throws StackTraceParserException {
        Stack<StackTraceEntry> linedStack = new Stack<StackTraceEntry>();
        Stack<StackTraceEntry> stack = new Stack<StackTraceEntry>();
        stack.addAll(_stack);

        StackTraceEntry caller = null;
        while (!stack.isEmpty()) {
            caller = stack.pop();
            File source = getSourceFile(caller.getClassName());
            if (source != null) {                
                caller.setSourcePath(source);
                linedStack.push(caller);
            }
        }
        return linedStack;
    }
}
