/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.model.configuration;

import java.util.EventObject;

/**
 * AddConfigEvent is used to notify that a Configuration is added to
 * Configurations.
 * 
 * @author wangf
 */
public class AddConfigEvent extends EventObject {
    private static final long serialVersionUID = 1L;
    private Configuration addedConfig;

    /**
     * Constructs a AddConfigEvent object.
     * 
     * @param source - Configurations instance of the MTJ Midlet project.
     * @param addedConfig - the added configuration.
     */
    public AddConfigEvent(Configurations source, Configuration addedConfig) {
        super(source);
        this.addedConfig = addedConfig;
    }

    public Configuration getAddedConfig() {
        return addedConfig;
    }

    public void setAddedConfig(Configuration addedConfig) {
        this.addedConfig = addedConfig;
    }

}
