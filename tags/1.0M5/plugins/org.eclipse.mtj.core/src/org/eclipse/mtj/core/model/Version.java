/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model;

import java.util.StringTokenizer;

/**
 * Represents the version of a component. Understands a string of the form
 * "major.minor.secondary_patch".
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */
public class Version implements Comparable<Object> {

    private String major;
    private String minor;
    private String secondary;
    private String patch;
    private String versionString;

    /**
     * Construct a new version instance based on the specified version string.
     */
    public Version(String versionString) {
        super();

        this.versionString = versionString;
        major = "";
        minor = "";
        secondary = "";
        patch = "";

        parseVersionString(versionString);
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Object o) {
        return compareTo((Version) o);
    }

    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Version version) {
        int value = major.compareTo(version.major);
        if (value != 0)
            return value;

        value = minor.compareTo(version.minor);
        if (value != 0)
            return value;

        value = secondary.compareTo(version.secondary);
        if (value != 0)
            return value;

        value = patch.compareTo(version.patch);
        return value;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object object) {
        return (object instanceof Version) && equals((Version) object);
    }

    /**
     * Return a boolean indicating the equality of the specified version to this
     * version.
     * 
     * @param version
     * @return
     */
    public boolean equals(Version version) {
        return (compareTo(version) == 0);
    }

    /**
     * Get the major version number.
     * 
     * @return the major version string
     */
    public String getMajor() {
        return major;
    }

    /**
     * Get the minor version number or <code>null</code> if not specified.
     * 
     * @return the minor version string
     */
    public String getMinor() {
        return minor;
    }

    /**
     * Get the patch version number or <code>null</code> if not specified.
     * 
     * @return the patch version string
     */
    public String getPatch() {
        return patch;
    }

    /**
     * Get the secondary version number or <code>null</code> if not specified.
     * 
     * @return the secondary version string.
     */
    public String getSecondary() {
        return secondary;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return versionString;
    }

    /**
     * Parse the specified version string into the constituent parts.
     * 
     * @param versionString
     */
    private void parseVersionString(String vString) {
        // Peel off the patch level first, if it exists
        int index = vString.indexOf('_');
        if (index != -1) {
            patch = vString.substring(index + 1);
            vString = vString.substring(0, index);
        }

        StringTokenizer st = new StringTokenizer(vString, ".");
        if (st.hasMoreTokens())
            major = st.nextToken();
        if (st.hasMoreTokens())
            minor = st.nextToken();
        if (st.hasMoreTokens())
            secondary = st.nextToken();
    }
}
