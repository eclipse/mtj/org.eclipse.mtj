/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.core.model.preprocessor.symbol;

import java.util.HashSet;

public class SymbolSet extends HashSet<Symbol> {
    private static final long serialVersionUID = 1L;

    public SymbolSet() {
    }

    /**
     * Return a String representation of the SymbolSet. This String will be used
     * by the preprocessor.
     * 
     * @return
     */
    public String getSymbolSetString() {
        StringBuffer sb = new StringBuffer();
        for (Symbol s : this) {
            sb.append(s.getName()).append("=").append(s.getSafeValue()).append(
                    ",");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
