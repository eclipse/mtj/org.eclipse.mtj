<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.eclipse.mtj.core">
<annotation>
      <appInfo>
         <meta.schema plugin="org.eclipse.mtj.core" id="deviceImporter" name="Device importer registering"/>
      </appInfo>
      <documentation>
         The device importer extension point provides a means for registering new types of device importers.  Devices are fully-formed at import time by the appropriate instance of IDeviceImporter.  These device instances are then stored and loaded automatically by the system as necessary.  Implementors of this extension point must provide instances of the org.eclipse.mtj.core.importer.IDeviceImporter interface.
      </documentation>
   </annotation>

   <element name="extension">
      <complexType>
         <sequence>
            <element ref="importer" minOccurs="1" maxOccurs="unbounded"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  a fully qualified identifier of the target extension point
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  an optional identifier of the extension instance
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  an optional name of the extension instance
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="importer">
      <complexType>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  a required fully-qualified identifier for this particular device importer extension
               </documentation>
            </annotation>
         </attribute>
         <attribute name="priority" type="string" use="required">
            <annotation>
               <documentation>
                  the priority order for this importer.  The priorty may range from 1 to 99.  The importers will be consulted in priority order starting with the lower numbered priority and working toward the highest numbered priority.  The first device importer that requires a non-null result for a particular folder will &quot;win&quot; and no further importer instances will be consulted.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  an optional displayable name for this particular device importer extension
               </documentation>
            </annotation>
         </attribute>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  the required implementation class for the &lt;code&gt;org.eclipse.mtj.core.importer.IDeviceImporter&lt;/code&gt; interface
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.mtj.core.importer.IDeviceImporter"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         [Enter the first release in which this extension point appears.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         [Enter extension point usage example here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="apiInfo"/>
      </appInfo>
      <documentation>
         [Enter API information here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         [Enter information about supplied implementation of this extension point.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="copyright"/>
      </appInfo>
      <documentation>
         Copyright (c) 2003,2008 Craig Setera and others.
 
All rights reserved. This program and the accompanying materials 
are made available under the terms of the Eclipse Public License v1.0 
which accompanies this distribution, and is available at 
http://www.eclipse.org/legal/epl-v10.html

Contributors:
    Craig Setera (EclipseME) - Initial implementation;
    Diego Sandin (Motorola)  - Refactoring extension name to follow eclipse 
                               standards
      </documentation>
   </annotation>

</schema>
