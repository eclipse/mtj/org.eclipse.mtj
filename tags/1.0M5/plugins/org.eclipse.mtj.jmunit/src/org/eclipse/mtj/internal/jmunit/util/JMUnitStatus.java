/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.jdt.junit/JUnitStatus
 */
package org.eclipse.mtj.internal.jmunit.util;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.jmunit.JMUnitPlugin;

/**
 * An auxiliary implementation of IStatus.
 * 
 * @since 0.9.1
 */
public class JMUnitStatus implements IStatus {

    /**
     * Creates a new JMUnitStatus with {@link IStatus#ERROR} severity and the
     * given message.
     * 
     * @param message the status message.
     * @return a new instance of JMUnitStatus with {@link IStatus#ERROR}
     *         severity.
     */
    public static IStatus createError(String message) {
        return new JMUnitStatus(IStatus.ERROR, message);
    }

    /**
     * Creates a new JMUnitStatus with {@link IStatus#INFO} severity and the
     * given message.
     * 
     * @param message the status message.
     * @return a new instance of JMUnitStatus with {@link IStatus#INFO}
     *         severity.
     */
    public static IStatus createInfo(String message) {
        return new JMUnitStatus(IStatus.INFO, message);
    }

    /**
     * Creates a new JMUnitStatus with {@link IStatus#WARNING} severity and the
     * given message.
     * 
     * @param message the status message.
     * @return a new instance of JMUnitStatus with {@link IStatus#WARNING}
     *         severity.
     */
    public static IStatus createWarning(String message) {
        return new JMUnitStatus(IStatus.WARNING, message);
    }

    private String fStatusMessage;

    private int fSeverity;

    /**
     * Creates a status set to OK (no message)
     */
    public JMUnitStatus() {
        this(OK, null);
    }

    /**
     * Creates a status .
     * 
     * @param severity The status severity: ERROR, WARNING, INFO and OK.
     * @param message The message of the status. Applies only for ERROR, WARNING
     *            and INFO.
     */
    public JMUnitStatus(int severity, String message) {
        fStatusMessage = message;
        fSeverity = severity;
    }

    /**
     * Returns always <code>null</code>.
     * 
     * @see IStatus#getChildren()
     */
    public IStatus[] getChildren() {
        return new IStatus[0];
    }

    /**
     * Returns always the error severity.
     * 
     * @see IStatus#getCode()
     */
    public int getCode() {
        return fSeverity;
    }

    /**
     * Returns always <code>null</code>.
     * 
     * @see org.eclipse.core.runtime.IStatus#getException()
     */
    public Throwable getException() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.runtime.IStatus#getMessage()
     */
    public String getMessage() {
        return fStatusMessage;
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.runtime.IStatus#getPlugin()
     */
    public String getPlugin() {
        return JMUnitPlugin.getPluginId();
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.runtime.IStatus#getSeverity()
     */
    public int getSeverity() {
        return fSeverity;
    }

    /**
     * Returns if the status' severity is ERROR.
     */
    public boolean isError() {
        return fSeverity == IStatus.ERROR;
    }

    /**
     * Returns if the status' severity is INFO.
     */
    public boolean isInfo() {
        return fSeverity == IStatus.INFO;
    }

    /**
     * Returns always <code>false</code>.
     * 
     * @see IStatus#isMultiStatus()
     */
    public boolean isMultiStatus() {
        return false;
    }

    /**
     * Returns if the status' severity is OK.
     */
    public boolean isOK() {
        return fSeverity == IStatus.OK;
    }

    /**
     * Returns if the status' severity is WARNING.
     */
    public boolean isWarning() {
        return fSeverity == IStatus.WARNING;
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.runtime.IStatus#matches(int)
     */
    public boolean matches(int severityMask) {
        return (fSeverity & severityMask) != 0;
    }

    /**
     * Sets the status to ERROR.
     * 
     * @param errorMessage the error message (can be empty, but not null)
     */
    public void setError(String errorMessage) {
        Assert.isNotNull(errorMessage);
        fStatusMessage = errorMessage;
        fSeverity = IStatus.ERROR;
    }

    /**
     * Sets the status to INFO.
     * 
     * @param infoMessage the info message (can be empty, but not null)
     */
    public void setInfo(String infoMessage) {
        Assert.isNotNull(infoMessage);
        fStatusMessage = infoMessage;
        fSeverity = IStatus.INFO;
    }

    /**
     * Sets the status to OK.
     */
    public void setOK() {
        fStatusMessage = null;
        fSeverity = IStatus.OK;
    }

    /**
     * Sets the status to WARNING.
     * 
     * @param warningMessage the warning message (can be empty, but not null)
     */
    public void setWarning(String warningMessage) {
        Assert.isNotNull(warningMessage);
        fStatusMessage = warningMessage;
        fSeverity = IStatus.WARNING;
    }

}
