/**
 * Copyright (c) 2006,2008 Nokia and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nokia                    - Initial version
 *     Diego Madruga (Motorola) - Refactored some parts of code to follow MTJ 
 *                                standards
 */
package org.eclipse.mtj.internal.jmunit.ui.actions;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.jmunit.JMUnitPlugin;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class has been added as part of a work in
 * progress. There is no guarantee that this API will work or that it will
 * remain the same. Please do not use this API without consulting with the MTJ
 * team.
 * </p>
 * 
 * @author Gorkem Ercan
 */
public class TestMethodsContentProvider implements IStructuredContentProvider {

    public Object[] getElements(Object inputElement) {
        if (!(inputElement instanceof ICompilationUnit))
            return new Object[0];
        ICompilationUnit cu = (ICompilationUnit) inputElement;
        if (!cu.exists())
            return new Object[0];

        try {
            IJavaElement[] jeArray = cu.getChildren();

            List<IMethod> methodsList = new ArrayList<IMethod>();
            for (int i = 0; i < jeArray.length; i++) {
                IJavaElement je = jeArray[i];
                if (je.getElementType() == IJavaElement.TYPE) {
                    IType type = (IType) jeArray[i];
                    IMethod[] methods = type.getMethods();
                    for (int k = 0; k < methods.length; k++) {
                        if (!methods[k].isConstructor()) {
                            methodsList.add(methods[k]);
                        }
                    }
                }

            }
            return methodsList.toArray();
        } catch (JavaModelException e) {
            JMUnitPlugin.log(e.getStatus());
            return new Object[0];
        }
    }

    public void dispose() {
        // TODO Auto-generated method stub

    }

    public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        // TODO Auto-generated method stub

    }

}
