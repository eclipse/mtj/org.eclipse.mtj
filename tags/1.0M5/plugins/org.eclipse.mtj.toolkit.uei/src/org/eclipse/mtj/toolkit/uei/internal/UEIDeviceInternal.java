/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.toolkit.uei.internal;

import java.io.File;
import java.util.Properties;

import org.eclipse.mtj.core.importer.JavadocDetector;
import org.eclipse.mtj.core.importer.LibraryImporter;
import org.eclipse.mtj.core.importer.JavadocDetector.GenericLocalFSSearch;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.model.Classpath;
import org.eclipse.mtj.core.model.preverifier.IPreverifier;
import org.eclipse.mtj.toolkit.uei.Messages;
import org.eclipse.mtj.toolkit.uei.UEIDevice;
import org.eclipse.mtj.toolkit.uei.UeiPlugin;
import org.eclipse.mtj.toolkit.uei.internal.properties.UEIDeviceDefinition;
import org.eclipse.mtj.toolkit.uei.model.EmulatorInfoArgs;
import org.eclipse.mtj.toolkit.uei.model.properties.DeviceSpecificProperties;

/**
 * Unified Emulator Interface (UEI) internal implementation.
 * 
 * @author Diego Madruga Sandin
 */
public final class UEIDeviceInternal extends UEIDevice {

    /**
     * Create a new instance of UEIDeviceInternal based on the properties for
     * that device found with the
     * <code>emulator {@link EmulatorInfoArgs#XQUERY -Xquery}</code> command.
     * 
     * @param name the device name.
     * @param groupName the device group name.
     * @param description the device description.
     * @param properties the properties for the device.
     * @param definition information on how the UEI emulator will be launched.
     * @param emulatorExecutable the emulator application.
     * @param preverifier the preverifier application.
     * @throws IllegalArgumentException if the properties or definition are
     *             <code>null</code>.
     */
    public UEIDeviceInternal(String name, String groupName, String description,
            Properties properties, UEIDeviceDefinition definition,
            File emulatorExecutable, IPreverifier preverifier)
            throws IllegalArgumentException {

        if (properties == null) {
            throw new IllegalArgumentException(
                    Messages.UEIDevice_null_properties);
        } else if (definition == null) {
            throw new IllegalArgumentException(
                    Messages.UEIDevice_null_definition);
        }

        this.setName(name);
        this.setDescription(description);
        this.setGroupName(groupName);

        this.setDeviceProperties(properties);
        this.setClasspath(getBootClasspath(properties));
        this.setProtectionDomains(getProtectionDomains(properties));

        this.setExecutable(emulatorExecutable);
        this.setPreverifier(preverifier);

        this.setDebugServer(definition.isDebugServer());
        this.setLaunchCommandTemplate(definition.getLaunchTemplate());

        this.setBundle(UeiPlugin.getDefault().getBundle().getSymbolicName());
    }

    /**
     * Import the device's classpath and return it.
     * 
     * @param deviceProperties
     * @return
     */
    private Classpath getBootClasspath(Properties deviceProperties) {

        Classpath classpath = new Classpath();
        String classpathString = deviceProperties.getProperty(
                DeviceSpecificProperties.BOOTCLASSPATH.toString(),
                Utils.EMPTY_STRING);

        String[] classpathEntries = classpathString.split(","); //$NON-NLS-1$

        // create a new javadoc detector which will search the
        // javadoc of the library automatically
        JavadocDetector javadocDetector = new JavadocDetector()
                .addJavadocSearchStrategy(new GenericLocalFSSearch());
        LibraryImporter libraryImporter = new LibraryImporter();
        libraryImporter.setJavadocDetector(javadocDetector);

        for (String element : classpathEntries) {
            File libraryFile = new File(element);
            if (libraryFile.exists()) {
                classpath.addEntry(libraryImporter
                        .createLibraryFor(libraryFile));
            }
        }

        return classpath;
    }

    /**
     * Return the protection domains specified for this device.
     * 
     * @param deviceProperties
     * @return
     */
    private String[] getProtectionDomains(Properties deviceProperties) {
        String domainsString = deviceProperties.getProperty(
                DeviceSpecificProperties.SECURITY_DOMAINS.toString(),
                Utils.EMPTY_STRING);
        return domainsString.split(","); //$NON-NLS-1$
    }
}
