-quiet
-encoding "iso-8859-1"
-charset "iso-8859-1"
-sourcepath "../org.eclipse.mtj.core/src
;../org.eclipse.mtj.core.hooks/src
;../org.eclipse.mtj.core.library/src
;../org.eclipse.mtj.core.preverifier/src
;../org.eclipse.mtj.jmunit/src
;../org.eclipse.mtj.toolkit.uei/src
;../org.eclipse.mtj.ui/src"
-d html/reference/api
-classpath @rt@
;../com.ibm.icu_4.0.0.v20081201.jar
;../org.eclipse.jdt.apt.core/mirrorapi.jar
;../org.apache.ant/lib/ant.jar
;../org.eclipse.ant.core/@dot
;../org.eclipse.compare/@dot
;../org.eclipse.compare.core/@dot
;../org.eclipse.core.commands/@dot
;../org.eclipse.core.contenttype/@dot
;../org.eclipse.core.expressions/@dot
;../org.eclipse.core.filebuffers/@dot
;../org.eclipse.core.filesystem/@dot
;../org.eclipse.core.jobs/@dot
;../org.eclipse.core.resources/@dot
;../org.eclipse.core.runtime/@dot
;../org.eclipse.core.runtime.content/@dot
;../org.eclipse.core.runtime.jobs/@dot
;../org.eclipse.core.runtime.preferences/@dot
;../org.eclipse.core.variables/@dot
;../org.eclipse.debug.core/@dot
;../org.eclipse.debug.ui/@dot
;../org.eclipse.equinox.app/@dot
;../org.eclipse.equinox.common/@dot
;../org.eclipse.equinox.preferences/@dot
;../org.eclipse.equinox.registry/@dot
;../org.eclipse.equinox.supplement/@dot
;../org.eclipse.help/@dot
;../org.eclipse.help.ui/@dot
;../org.eclipse.jdt.core.manipulation/@dot
;../org.eclipse.jdt.debug/jdi.jar
;../org.eclipse.jdt.junit/@dot
;../org.eclipse.jface/@dot
;../org.eclipse.jface.text/@dot
;../org.eclipse.ltk.core.refactoring/@dot
;../org.eclipse.ltk.ui.refactoring/@dot
;../org.eclipse.osgi.services/@dot
;../org.eclipse.osgi.util/@dot
;../org.eclipse.osgi/@dot
;../org.eclipse.search/@dot
;../org.eclipse.swt.win32.win32.x86/@dot
;../org.eclipse.team.core/@dot
;../org.eclipse.team.ui/@dot
;../org.eclipse.text/@dot
;../org.eclipse.ui/@dot
;../org.eclipse.ui.console/@dot
;../org.eclipse.ui.editors/@dot
;../org.eclipse.ui.externaltools/@dot
;../org.eclipse.ui.forms/@dot
;../org.eclipse.ui.ide/@dot
;../org.eclipse.ui.navigator/@dot
;../org.eclipse.ui.views/@dot
;../org.eclipse.ui.workbench.texteditor/@dot
;../org.eclipse.ui.workbench/@dot
;../org.junit/junit.jar
-breakiterator
-use
-splitIndex
-windowtitle "Eclipse MTJ API Specification"
-doctitle "Eclipse MTJ API Specification"
-header "<b>Mobile Tools for Java</b><br>Release 1.0"
-group "MTJ core plug-in packages" "org.eclipse.mtj.core
;org.eclipse.mtj.core.*"
-group "MTJ UI plug-in packages" "org.eclipse.mtj.ui
;org.eclipse.mtj.ui.*"
-link http://java.sun.com/j2se/1.5/docs/api
-linkoffline ./../../../org.eclipse.platform.doc.isv/reference/api ../org.eclipse.platform.doc.isv/reference/api
-link http://bundles.osgi.org/javadoc/r4
-tag 'category:X'
-tag 'noimplement:a:Restriction:'
-tag 'noextend:a:Restriction:'
-tag 'noreference:a:Restriction:'
-tag 'noinstantiate:a:Restriction:'
-tag 'nooverride:a:Restriction:'
org.eclipse.mtj.core
org.eclipse.mtj.core.console
org.eclipse.mtj.core.converter
org.eclipse.mtj.core.importer
org.eclipse.mtj.core.importer.impl
org.eclipse.mtj.core.importer.properties
org.eclipse.mtj.core.launching
org.eclipse.mtj.core.model
org.eclipse.mtj.core.model.configuration
org.eclipse.mtj.core.model.device
org.eclipse.mtj.core.model.device.impl
org.eclipse.mtj.core.model.device.launch.properties
org.eclipse.mtj.core.model.device.preprocess
org.eclipse.mtj.core.model.impl
org.eclipse.mtj.core.model.jad
org.eclipse.mtj.core.model.library
org.eclipse.mtj.core.model.library.api
org.eclipse.mtj.core.model.library.impl
org.eclipse.mtj.core.model.preprocessor
org.eclipse.mtj.core.model.preprocessor.symbol
org.eclipse.mtj.core.model.preverifier
org.eclipse.mtj.core.model.preverifier.impl
org.eclipse.mtj.core.model.project
org.eclipse.mtj.core.model.project.impl
org.eclipse.mtj.core.model.sign
org.eclipse.mtj.core.nature
org.eclipse.mtj.core.persistence
org.eclipse.mtj.core.signing
org.eclipse.mtj.core.hook.sourceMapper
org.eclipse.mtj.core.hooks
org.eclipse.mtj.core.library
org.eclipse.mtj.core.library.classpath
org.eclipse.mtj.core.library.manager
org.eclipse.mtj.core.library.model
org.eclipse.mtj.core.library.model.impl
org.eclipse.mtj.core.library.model.licence
org.eclipse.mtj.core.library.model.security
org.eclipse.mtj.core.preverifier
org.eclipse.mtj.preverifier.results
org.eclipse.mtj.preverifier
org.eclipse.mtj.toolkit.uei
org.eclipse.mtj.toolkit.uei.model
org.eclipse.mtj.toolkit.uei.model.properties
org.eclipse.mtj.ui
org.eclipse.mtj.ui.configurations
org.eclipse.mtj.ui.console
org.eclipse.mtj.ui.devices
org.eclipse.mtj.ui.editors.device
org.eclipse.mtj.ui.editors.jad
org.eclipse.mtj.ui.editors.models
org.eclipse.mtj.ui.viewers