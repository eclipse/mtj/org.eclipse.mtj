/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.examples.toolkits.me4se;

import org.eclipse.osgi.util.NLS;

/**
 * Convenient class for manipulating messages.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class Messages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.internal.toolkit.me4se.messages"; //$NON-NLS-1$

    public static String ME4SEDeviceImporter_importing_error;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
