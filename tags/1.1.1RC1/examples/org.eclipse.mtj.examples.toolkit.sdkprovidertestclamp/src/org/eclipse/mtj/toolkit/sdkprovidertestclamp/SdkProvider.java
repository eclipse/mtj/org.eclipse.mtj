/**
 * Copyright (c) 2009 Research In Motion Limited.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial implementation.
 */
package org.eclipse.mtj.toolkit.sdkprovidertestclamp;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.internal.core.sdk.AbstractSDKProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

/**
 * Implementation of a simple AbstractSDKProvider for use in an SDK provider test clamp. 
 * 
 * @author jdearden
 */
public class SdkProvider extends AbstractSDKProvider {
    
    String identifier;
    ArrayList<ISDK> mySdks = new ArrayList<ISDK>();
    
    public SdkProvider() {
        Properties p = Plugin.getDefault().getProperties();
        name = p.getProperty("provider.name"); //$NON-NLS-1$
        identifier = p.getProperty("provider.id"); //$NON-NLS-1$
        String[] sdkKeys = p.getProperty("provider.sdks").split(","); //$NON-NLS-1$ $NON-NLS-2$
        mySdks = new ArrayList<ISDK>(sdkKeys.length);
        for (String key : sdkKeys) {
            Sdk sdk = new Sdk(this, "sdk." + key); //$NON-NLS-1$
            mySdks.add(sdk);
        }
    }
    
    public String getDescription() {
        return name;
    }

    public String getIdentifier() {
        return identifier;
    }

    public int getSDKCount() {
        return mySdks.size();
    }

    public List<ISDK> getSDKs() {
        return mySdks;
    }
    
    public Image getLogo() {
        Display display = Display.getCurrent();
        if (display == null) {
            display = Display.getDefault();
        }
        return new Image(display, SdkProvider.class
                .getResourceAsStream("sample_logo2.png")); //$NON-NLS-1$
    }
    
}
