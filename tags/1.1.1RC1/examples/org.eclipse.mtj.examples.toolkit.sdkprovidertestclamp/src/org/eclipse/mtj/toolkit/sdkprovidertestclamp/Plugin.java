/**
 * Copyright (c) 2009 Research In Motion Limited.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial implementation.
 */
package org.eclipse.mtj.toolkit.sdkprovidertestclamp;


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * Implementation of a simple AbstractUIPlugin for use in an SDK provider test clamp. 
 * 
 * @author jdearden
 */
public final class Plugin extends AbstractUIPlugin {
    
    // The plug-in ID
    public static final String PLUGIN_ID = "org.eclipse.mtj.toolkit.sdkprovidertestclamp"; //$NON-NLS-1$

    // The shared instance
    private static Plugin plugin;
    
    private static Properties properties;

    public static Plugin getDefault() {
        return plugin;
    }
    
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;
    }

    public void stop(BundleContext context) throws Exception {
        super.stop(context);
        plugin = null;
    }
    
    Properties getProperties() {
        if (properties == null) {
            properties = new Properties();
            InputStream propertiesStream = null;
            try {
                URL propsFileURL = getBundle().getEntry( "properties.properties" ); //$NON-NLS-1$
                propertiesStream = propsFileURL.openStream();
                properties.load( propertiesStream );
            } catch( IOException e ) {
                properties = null;
            } finally {
                if( propertiesStream != null ) {
                    try {
                        propertiesStream.close();
                    } catch( IOException ignore ) {
                    }
                }
            }
        }
        return properties;
    }
}
