/**
 * Copyright (c) 2009 Research In Motion Limited.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial implementation.
 */
package org.eclipse.mtj.toolkit.sdkprovidertestclamp;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.sdk.ISDKProvider;
import org.eclipse.mtj.core.sdk.ManagedSDK;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IManagedDevice;
import org.osgi.framework.Version;

/**
 * Implementation of a fake ManagedSDK for use in an SDK provider test clamp. 
 * 
 * @author jdearden
 */
public class Sdk extends ManagedSDK {
    
    private ISDKProvider provider;
    private String name;
    private String description;
    private String identifier;
    private Version version;
    
    private ArrayList<IManagedDevice> myDevices;

    public Sdk(ISDKProvider sdkProvider, String propertyKey) {
        Properties p = Plugin.getDefault().getProperties();
        provider = sdkProvider;
        name = p.getProperty(propertyKey + ".name"); //$NON-NLS-1$
        description = p.getProperty(propertyKey + ".description"); //$NON-NLS-1$
        identifier = p.getProperty(propertyKey + ".id"); //$NON-NLS-1$
        version = new Version(p.getProperty(propertyKey + ".version")); //$NON-NLS-1$
        String[] deviceKeys = p.getProperty(propertyKey + ".devices").split(","); //$NON-NLS-1$ $NON-NLS-2$
        myDevices = new ArrayList<IManagedDevice>(deviceKeys.length);
        for (String key : deviceKeys) {
            IManagedDevice device = new Device(this, "device." + key); //$NON-NLS-1$
            myDevices.add(device);
        }
    }

    @Override
    protected List<IManagedDevice> getProvidedDeviceList()
            throws CoreException {
        return myDevices;
    }

    @Override
    public ISDKProvider getSDKProvider() {
        return provider;
    }
    
    // << ISDK >>
    public String getName() {
        return name;
    }

    // << ISDK >>
    public String getDescription() {
        return description;
    }

    // << ISDK >>
    public String getIdentifier() {
        return identifier;
    }

    // << ISDK >>
    public Version getVersion() {
        return version;
    }
    
    @Override
    public void deleteDuplicateDevice(IManagedDevice device) {
        if (device.isDuplicate()) {
            myDevices.remove(device);
        }
    }

    @SuppressWarnings("restriction")
    @Override
    public IDevice duplicateDevice(IManagedDevice device, String newName) {
        // XXX Note that this test clamp does not persist duplicated devices
        // between MTJ sessions
        Device duplicate = null;
        if (device instanceof Device) {
            try {
                duplicate = (Device) ((Device) device).clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                return null;
            }
        }
        duplicate.setAsDuplicate();
        duplicate.setName(newName);
        myDevices.add(duplicate);
        return duplicate;
    }

}
