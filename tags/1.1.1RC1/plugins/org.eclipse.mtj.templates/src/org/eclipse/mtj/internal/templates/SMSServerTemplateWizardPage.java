/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.templates;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * SMSServerTemplateWizardPage provides a template for creating 
 * an application that supports receiving SMS messages.
 * 
 * @author David Marques
 * @since 1.0
 */
public class SMSServerTemplateWizardPage extends AbstractTemplateWizardPage {

    private Text portText;

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        GridData data = null;
        parent.setLayout(new GridLayout(0x01, false));
        
        Group group = new Group(parent, SWT.NONE);
        group.setText(Messages.SMSTemplateProvider_grouptext);
        
        data = new GridData(SWT.FILL, SWT.FILL, true, false);
        group.setLayoutData(data);
        group.setLayout(new GridLayout(2, false));
        
        Label portLabel = new Label(group, SWT.NONE);
        portLabel.setText(Messages.SMSTemplateProvider_server_port);
        
        data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        portLabel.setLayoutData(data);
        
        portText = new Text(group, SWT.BORDER);
        portText.setText("1234"); //$NON-NLS-1$
        
        portText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        data = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
        data.grabExcessHorizontalSpace = true;
        portText.setLayoutData(data);   
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.ITemplateProvider#getDictionary()
     */
    public Map<String, String> getDictionary() {
        Map<String, String> dictionary = new HashMap<String, String>();
        dictionary.put("$server-port$", this.portText.getText()); //$NON-NLS-1$
        return dictionary;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.templates.ITemplateProvider#isPageComplete()
     */
    public boolean isPageComplete() {
        boolean result = Pattern.matches("\\d+", this.portText.getText()); //$NON-NLS-1$
        return result;
    }
}
