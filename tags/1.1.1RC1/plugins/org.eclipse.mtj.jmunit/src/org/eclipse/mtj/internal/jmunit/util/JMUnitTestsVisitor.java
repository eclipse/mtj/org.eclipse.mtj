/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Eric S. Dias  (Motorola) - Initial version
 *     
 * @since 1.0
 */
package org.eclipse.mtj.internal.jmunit.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.internal.core.util.IJavaProjectVisitor;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.jmunit.IJMUnitContants;

/**
 * JMUnitTestsVisitor class implements an IJavaProjectVisitor in order to visit
 * an {@link IJavaProject} instance and collect all classes extending JMUnit
 * test classes.
 * 
 * @author Eric S. Dias
 * @since 1.0
 */
public class JMUnitTestsVisitor implements IJavaProjectVisitor {

    private static final String[] JMUNIT_TESTS = {
            IJMUnitContants.JMUNIT_TESTCASE_CLDC11,
            IJMUnitContants.JMUNIT_TESTSUITE_CLDC11 };

    private List<IResource>       excludeds    = new ArrayList<IResource>();

    private IJavaProject          javaProject;

    /**
     * Creates a JMUnitTestsVisitor for the specified
     * {@link IJavaProject} inatance.
     * 
     * @param javaProject target java project.
     */
    public JMUnitTestsVisitor(IJavaProject javaProject) {
        this.javaProject = javaProject;
    }

    /**
     * Visits the ICompilationUnit.
     * 
     * @param compilationUnit target ICompilationUnit.
     */
    public void visitCompilatioUnit(ICompilationUnit compilationUnit) {
        IType type = compilationUnit.findPrimaryType();
        try {
            ITypeHierarchy hierarchy = type
                    .newSupertypeHierarchy(new NullProgressMonitor());
            IType[] types = hierarchy.getAllClasses();
            for (IType iType : types) {
                String superClass = iType.getFullyQualifiedName('.');
                for (String jmunitTestName : JMUNIT_TESTS) {
                    if (superClass.equals(jmunitTestName)) {
                        IResource resource = compilationUnit
                                .getCorrespondingResource();
                        if (resource != null) {
                            excludeds.add(resource);
                        }
                    }

                }
            }
        } catch (JavaModelException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }

    }

    
    /**
     * Gets all test classes within the java project.
     * 
     * @return array of test classes.
     */
    public IPath[] getJMunitTests() {
        List<IPath> paths = new ArrayList<IPath>();
        IResource[] sources = Utils.getSourceFolders(javaProject);
        for (IResource excluded : excludeds) {
            IPath path = excluded.getProjectRelativePath();
            for (IResource source : sources) {
                IPath sourcePath = source.getProjectRelativePath();
                if (sourcePath.isPrefixOf(path)) {
                    paths.add(path.removeFirstSegments(sourcePath
                            .segmentCount()));
                    break;
                }
            }
        }
        return paths.toArray(new IPath[0]);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.util.IJavaProjectVisitor#visitPackageFragment(org.eclipse.jdt.core.IPackageFragment)
     */
    public void visitPackageFragment(IPackageFragment fragment) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.util.IJavaProjectVisitor#visitPackageFragmentRoot(org.eclipse.jdt.core.IPackageFragmentRoot)
     */
    public void visitPackageFragmentRoot(
            IPackageFragmentRoot packageFragmentRoot) {
    }

}
