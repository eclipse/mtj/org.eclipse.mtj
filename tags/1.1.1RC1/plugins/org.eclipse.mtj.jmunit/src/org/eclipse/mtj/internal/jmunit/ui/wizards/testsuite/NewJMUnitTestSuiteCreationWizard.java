/**
 * Copyright (c) 2006,2008 Nokia and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nokia                    - Initial version
 *     Diego Madruga (Motorola) - Refactored some parts of code to follow MTJ 
 *                                standards
 *     Diego Madruga (Motorola) - Refactored code and created new superclass   
 */
package org.eclipse.mtj.internal.jmunit.ui.wizards.testsuite;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.mtj.internal.jmunit.JMUnitMessages;
import org.eclipse.mtj.internal.jmunit.JMUnitPluginImages;
import org.eclipse.mtj.internal.jmunit.ui.wizards.JMUnitWizard;
import org.eclipse.ui.IEditorPart;

/**
 * A wizard for creating test suites.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class has been added as part of a work in
 * progress. There is no guarantee that this API will work or that it will
 * remain the same. Please do not use this API without consulting with the MTJ
 * team.
 * </p>
 * 
 * @author Gorkem Ercan
 * @since 0.9.1
 */
public class NewJMUnitTestSuiteCreationWizard extends JMUnitWizard {

    /**
     * 
     */
    private NewJMUnitTestSuiteWizardPageOne fPage;

    /**
     * 
     */
    public NewJMUnitTestSuiteCreationWizard() {
        super();
        setWindowTitle(JMUnitMessages.NewJMUnitTestSuiteCreationWizard_title);
        initDialogSettings();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#addPages()
     */
    @Override
    public void addPages() {
        super.addPages();
        fPage = new NewJMUnitTestSuiteWizardPageOne();
        addPage(fPage);
        fPage.init(getSelection());
    }

    /**
     * @param cu_ep
     * @return
     */
    public IRunnableWithProgress getRunnableSave(final IEditorPart cu_ep) {
        return new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor)
                    throws InvocationTargetException, InterruptedException {
                if (monitor == null) {
                    monitor = new NullProgressMonitor();
                }
                cu_ep.doSave(monitor);
            }
        };
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.jmunit.ui.wizards.JMUnitWizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        if (finishPage(fPage.getRunnable())) {
            postCreatingType();
            return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.jmunit.ui.wizards.JMUnitWizard#initializeDefaultPageImageDescriptor()
     */
    @Override
    protected void initializeDefaultPageImageDescriptor() {
        setDefaultPageImageDescriptor(JMUnitPluginImages.DESC_NEW_TEST_SUITE_WIZ);
    }

    protected void postCreatingType() {
        IType newClass = fPage.getCreatedType();
        if (newClass == null) {
            return;
        }
        ICompilationUnit cu = newClass.getCompilationUnit();
        IResource resource = cu.getResource();
        if (resource != null) {
            selectAndReveal(resource);
            openResource(resource);
        }
    }
}
