/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.forms.blocks;

import org.eclipse.mtj.internal.ui.editors.IModel;
import org.eclipse.mtj.internal.ui.editors.IModelListener;

/**
 *
 */
public class NamedObject {

    /**
     * 
     */
    private String name;

    /**
     * 
     */
    protected IModel model;

    /**
     * @param name
     */
    public NamedObject(String name) {
        this.name = name;
    }

    /**
     * @param model
     */
    public void setModel(IModel model) {
        this.model = model;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return getName();
    }

    /**
     * @param name
     */
    public void setName(String name, boolean notify) {
        this.name = name;
        if (notify) {
            model.fireModelChanged(new Object[] { this },
                    IModelListener.CHANGED, "");
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((model == null) ? 0 : model.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        NamedObject other = (NamedObject) obj;
        if (model == null) {
            if (other.model != null) {
                return false;
            }
        } else if (!model.equals(other.model)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }
    
    
}
