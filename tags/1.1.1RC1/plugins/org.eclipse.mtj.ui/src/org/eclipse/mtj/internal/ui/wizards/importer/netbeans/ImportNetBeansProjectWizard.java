/**
 * Copyright (c) 2008 Motorola Inc.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola) - Initial implementation                            
 */
package org.eclipse.mtj.internal.ui.wizards.importer.netbeans;

import org.eclipse.mtj.internal.ui.wizards.importer.common.ImportProjectWizard;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ImportProjectWizardPage;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ProjectImporter;

/**
 * Wizard class to import projects from Netbeans Mobility Pack
 * @author wha006
 *
 */
public class ImportNetBeansProjectWizard extends ImportProjectWizard {
	
	private static final String NB_PRODUCT_NAME = "NetBeans"; //$NON-NLS-1$
	
	private ProjectImporter importer = new NetBeansProjectImporter();

	@Override
	protected ImportProjectWizardPage createImportProjectWizardPage() {
		return new ImportProjectWizardPage(importer, NB_PRODUCT_NAME);
	}

	@Override
	protected String getProductNameToImportFrom() {
		return NB_PRODUCT_NAME;
	}

}
