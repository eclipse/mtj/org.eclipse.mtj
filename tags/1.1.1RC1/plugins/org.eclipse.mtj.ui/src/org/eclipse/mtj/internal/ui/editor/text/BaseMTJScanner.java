/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/BasePDEScanner
 */
package org.eclipse.mtj.internal.ui.editor.text;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.BufferedRuleBasedScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;

/**
 * A buffered rule based scanner. The buffer always contains a section of a
 * fixed size of the document to be scanned.
 * 
 * @since 0.9.1
 */
public abstract class BaseMTJScanner extends BufferedRuleBasedScanner {

    /**
     * @param manager
     * @param property
     * @return
     */
    public static TextAttribute createTextAttribute(IColorManager manager,
            String property) {
        Color color = manager.getColor(property);
        int style = SWT.NORMAL;
        IPreferenceStore store = MTJUIPlugin.getDefault().getPreferenceStore();
        if (store.getBoolean(property + IMTJColorConstants.P_BOLD_SUFFIX)) {
            style |= SWT.BOLD;
        }
        if (store.getBoolean(property + IMTJColorConstants.P_ITALIC_SUFFIX)) {
            style |= SWT.ITALIC;
        }
        return new TextAttribute(color, null, style);
    }

    /**
     * 
     */
    private IColorManager fColorManager;

    /**
     * @param manager
     */
    public BaseMTJScanner(IColorManager manager) {
        fColorManager = manager;
        initialize();
    }

    /**
     * 
     */
    protected BaseMTJScanner() {
    }

    /**
     * @param event
     */
    public void adaptToPreferenceChange(PropertyChangeEvent event) {
        String property = event.getProperty();
        if (affectsTextPresentation(property)) {
            Token token = getTokenAffected(event);
            if (property.endsWith(IMTJColorConstants.P_BOLD_SUFFIX)) {
                adaptToStyleChange(event, token, SWT.BOLD);
            } else if (property.endsWith(IMTJColorConstants.P_ITALIC_SUFFIX)) {
                adaptToStyleChange(event, token, SWT.ITALIC);
            } else {
                adaptToColorChange(event, token);
            }
        }
    }

    /**
     * @param property
     * @return
     */
    public abstract boolean affectsTextPresentation(String property);

    /**
     * @param manager
     */
    public void setColorManager(IColorManager manager) {
        fColorManager = manager;
    }

    /**
     * @param event
     * @param token
     */
    protected void adaptToColorChange(PropertyChangeEvent event, Token token) {
        TextAttribute attr = (TextAttribute) token.getData();
        token.setData(new TextAttribute(fColorManager.getColor(event
                .getProperty()), attr.getBackground(), attr.getStyle()));
    }

    /**
     * @param event
     * @param token
     * @param styleAttribute
     */
    protected void adaptToStyleChange(PropertyChangeEvent event, Token token,
            int styleAttribute) {
        if (token == null) {
            return;
        }

        boolean eventValue = false;
        Object value = event.getNewValue();
        if (value instanceof Boolean) {
            eventValue = ((Boolean) value).booleanValue();
        }

        TextAttribute attr = (TextAttribute) token.getData();
        boolean activeValue = (attr.getStyle() & styleAttribute) == styleAttribute;
        if (activeValue != eventValue) {
            Color foreground = attr.getForeground();
            Color background = attr.getBackground();
            int style = eventValue ? attr.getStyle() | styleAttribute : attr
                    .getStyle()
                    & ~styleAttribute;
            token.setData(new TextAttribute(foreground, background, style));
        }
    }

    /**
     * @param property
     * @return
     */
    protected TextAttribute createTextAttribute(String property) {
        return createTextAttribute(fColorManager, property);
    }

    /**
     * @param event
     * @return
     */
    protected abstract Token getTokenAffected(PropertyChangeEvent event);

    /**
     * 
     */
    protected abstract void initialize();

}
