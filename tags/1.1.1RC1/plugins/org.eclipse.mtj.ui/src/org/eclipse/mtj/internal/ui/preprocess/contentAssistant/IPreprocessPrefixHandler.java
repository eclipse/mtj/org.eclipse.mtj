/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant;

/**
 * Interface that handles preprocess prefix operation
 * 
 * @author gma
 * @since 0.9.1
 */
public interface IPreprocessPrefixHandler {
    /**
     * @param line the preprocessing line
     * @return the preprocessing line's prefix
     */
    String getCurrPrefix(String line);

    /**
     * @param line the preprocessing line
     * @return if the preprocessing line has legal prefix return true, else
     *         return false
     */
    boolean hasLegalPrefix(String line);
}
