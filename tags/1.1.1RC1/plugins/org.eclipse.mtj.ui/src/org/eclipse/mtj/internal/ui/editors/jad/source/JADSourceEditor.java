/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 *     Gang  Ma     (Sybase)   - Add content assist support
 */
package org.eclipse.mtj.internal.ui.editors.jad.source;

import java.util.ResourceBundle;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.texteditor.ContentAssistAction;
import org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds;

/**
 * The standard text editor for Java Application Descriptors
 * 
 * @author Diego Madruga Sandin
 */
public class JADSourceEditor extends TextEditor {

    /**
     * The location of the resource bundle containing the ContentAssist keys.
     */
    private static final String RES_BUNDLE_LOCATION = "org.eclipse.mtj.internal.ui.editors.jad.source.contentassist.JADSourceEditorMessages"; //$NON-NLS-1$
    
    /**
     * The resource bundle containing the ContentAssist keys.
     */
    private static ResourceBundle bundleForConstructedKeys = ResourceBundle
            .getBundle(RES_BUNDLE_LOCATION);


    /**
     * Returns the resource bundle containing the ContentAssist keys.
     * 
     * @return the resource bundle
     */
    public static ResourceBundle getBundleForConstructedKeys() {
        return bundleForConstructedKeys;
    }

    /**
     * Creates a new Application Descriptor editor
     */
    public JADSourceEditor() {
        setSourceViewerConfiguration(new JADSourceViewerConfiguration(this));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.editors.text.TextEditor#createActions()
     */
    @Override
    protected void createActions() {
        super.createActions();

        /* Add action to allow the user to invoke Content Assist */
        Action action = new ContentAssistAction(getBundleForConstructedKeys(),
                "ContentAssistProposal.", this); //$NON-NLS-1$
        String id = ITextEditorActionDefinitionIds.CONTENT_ASSIST_PROPOSALS;
        action.setActionDefinitionId(id);
        setAction("ContentAssistProposal", action); //$NON-NLS-1$
        markAsStateDependentAction("ContentAssistProposal", true); //$NON-NLS-1$
    }
    
    public IJavaProject getJavaProject(){
    	IProject project = null;

        IEditorInput input = getEditorInput();
        if (input instanceof IFileEditorInput) {
            IFile file = ((IFileEditorInput) input).getFile();
            project = file.getProject();
        }
        
        return (project == null) ? null : JavaCore.create(project);
	}
    
}
