/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 *     David Marques (Motorola) - Changing several attributes access.
 */
package org.eclipse.mtj.internal.ui.editor;

import java.util.ArrayList;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.mtj.internal.core.text.IEditingModel;
import org.eclipse.mtj.internal.core.text.IReconcilingParticipant;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.part.IPageSite;

/**
 * Content outline page for the XML editor.
 * 
 * @since 0.9.1
 */
public class SourceOutlinePage extends MTJOutlinePage implements
        IReconcilingParticipant, ISortableContentOutlinePage {

    private IContentProvider contentProvider;
    private ViewerComparator defaultComparator;
    private IBaseLabelProvider labelProvider;

    /**
     * This list is redundant; but, required because we can't access
     * 
     * <code>org.eclipse.ui.views.contentoutline.ContentOutlinePage.selectionChangedListeners</code>
     * from our parent.
     */
    private ArrayList<ISelectionChangedListener> listenerList;
    protected IEditingModel model;
    private boolean sorted;
    private ViewerComparator viewerComparator;
    TreeViewer viewer;

    /**
     * Creates a new content outline page for the XML editor.
     * 
     * @param editor
     * @param model
     * @param lProvider
     * @param cProvider
     * @param defaultComparator
     * @param comparator
     */
    public SourceOutlinePage(MTJFormEditor editor, IEditingModel model,
            IBaseLabelProvider lProvider, IContentProvider cProvider,
            ViewerComparator defaultComparator, ViewerComparator comparator) {

        super(editor);
        this.model = model;
        labelProvider = lProvider;
        contentProvider = cProvider;
        this.defaultComparator = defaultComparator;
        viewerComparator = comparator;
        listenerList = new ArrayList<ISelectionChangedListener>();
    }

    /**
     * Used for restoration after temporary removal. Uses listeners cached.
     * <p>
     * Re-add the tree listener added by our parent for our parent:
     * <code>org.eclipse.ui.views.contentoutline.ContentOutlinePage</code>
     * </p>
     */
    public void addAllSelectionChangedListeners() {
        if (isViewerDefined()) {
            viewer.addSelectionChangedListener(this);
        }
        // Add all current listeners
        for (int i = 0; i < listenerList.size(); i++) {
            super.addSelectionChangedListener(listenerList.get(i));
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#addSelectionChangedListener(org.eclipse.jface.viewers.ISelectionChangedListener)
     */
    @Override
    public void addSelectionChangedListener(ISelectionChangedListener listener) {
        // Add the listener to our private list
        listenerList.add(listener);
        super.addSelectionChangedListener(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#createControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createControl(Composite parent) {
        super.createControl(parent);
        viewer = getTreeViewer();
        viewer.setContentProvider(contentProvider);
        viewer.setLabelProvider(labelProvider);
        if (sorted) {
            viewer.setComparator(viewerComparator);
        } else {
            viewer.setComparator(defaultComparator);
        }
        viewer.setInput(model);
        viewer.expandAll();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#init(org.eclipse.ui.part.IPageSite)
     */
    @Override
    public void init(IPageSite pageSite) {
        super.init(pageSite);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.IReconcilingParticipant#reconciled(org.eclipse.jface.text.IDocument)
     */
    public void reconciled(IDocument document) {
        final Control control = getControl();
        if ((control == null) || control.isDisposed()) {
            return;
        }
        control.getDisplay().asyncExec(new Runnable() {
            public void run() {
                if (control.isDisposed()) {
                    return;
                }
                control.setRedraw(false);
                // Temporarily remove all selection listeners from the tree
                // viewer. This is required because the refresh fires a
                // selection event back to the source page (observered in
                // the bundle source page) when typing
                removeAllSelectionChangedListeners();
                getTreeViewer().refresh();
                addAllSelectionChangedListeners();
                getTreeViewer().expandAll();
                control.setRedraw(true);
            }
        });
    }

    /**
     * Used for temporary removal. Listeners cached.
     */
    public void removeAllSelectionChangedListeners() {
        // Remove the tree listener added by our parent for our parent:
        // org.eclipse.ui.views.contentoutline.ContentOutlinePage
        if (isViewerDefined()) {
            viewer.removeSelectionChangedListener(this);
        }
        // Remove all current listeners
        for (int i = 0; i < listenerList.size(); i++) {
            super.removeSelectionChangedListener(listenerList.get(i));
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#removeSelectionChangedListener(org.eclipse.jface.viewers.ISelectionChangedListener)
     */
    @Override
    public void removeSelectionChangedListener(
            ISelectionChangedListener listener) {
        // Remove the listener from our private list
        listenerList.remove(listener);
        super.removeSelectionChangedListener(listener);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.ISortableContentOutlinePage#sort(boolean)
     */
    public void sort(boolean sorting) {
        sorted = sorting;
        if (isViewerDefined()) {
            if (sorting) {
                viewer.setComparator(viewerComparator);
            } else {
                viewer.setComparator(defaultComparator);
            }
        }
    }

    /**
     * @return
     */
    private boolean isViewerDefined() {
        if (viewer == null) {
            return false;
        } else if (viewer.getTree().isDisposed()) {
            return false;
        }
        return true;
    }
}
