/**
 * Copyright (c) 2007,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

/**
 * @since 0.9.1
 */
public interface IDocumentNodeFactory {

    /**
     * @param name
     * @param value
     * @param enclosingElement
     * @return
     */
    public IDocumentAttributeNode createAttribute(String name, String value,
            IDocumentElementNode enclosingElement);

    /**
     * @param name
     * @param parent
     * @return
     */
    public IDocumentElementNode createDocumentNode(String name,
            IDocumentElementNode parent);

    /**
     * @param content
     * @param parent
     * @return
     */
    public IDocumentTextNode createDocumentTextNode(String content,
            IDocumentElementNode parent);

}
