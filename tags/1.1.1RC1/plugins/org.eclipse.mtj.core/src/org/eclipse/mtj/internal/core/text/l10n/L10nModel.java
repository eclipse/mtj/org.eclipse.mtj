/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 *     Renato Franca (Motorola) - Fixing bug 257367. Error catching improvement
 */
package org.eclipse.mtj.internal.core.text.l10n;

import java.io.InputStream;

import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.internal.core.IModel;
import org.eclipse.mtj.internal.core.text.IWritable;
import org.eclipse.mtj.internal.core.text.XMLEditingModel;
import org.xml.sax.helpers.DefaultHandler;

/**
 * L10nModel
 * 
 * @since 0.9.1
 */
public class L10nModel extends XMLEditingModel {

    private L10nDocumentFactory factory;

    private L10nDocumentHandler handler;

    private L10nLocales l10nLocales;

    /**
     * @param document
     * @param isReconciling
     */
    public L10nModel(IDocument document, boolean isReconciling) {
        super(document, isReconciling);

        handler = null;
        factory = new L10nDocumentFactory(this);
        l10nLocales = null;
    }

    /**
     * @return
     */
    public L10nDocumentFactory getFactory() {
        return factory;
    }

    /**
     * @return
     */
    public L10nLocales getLocales() {
        if (l10nLocales == null) {
            l10nLocales = getFactory().createL10nLocales();
        }
        return l10nLocales;
    }

    /**
     * Validate the model element
     */
    public void validate() {
        if(l10nLocales != null) {
            l10nLocales.validate();
        }
            
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.XMLEditingModel#createDocumentHandler(org.eclipse.mtj.core.model.IModel, boolean)
     */
    @Override
    protected DefaultHandler createDocumentHandler(IModel model,
            boolean reconciling) {

        if (handler == null) {
            handler = new L10nDocumentHandler(this, reconciling);
        }
        return handler;
    }

    public L10nDocumentHandler getHandler() {
        return handler;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.XMLEditingModel#getRoot()
     */
    @Override
    protected IWritable getRoot() {
        return getLocales();
    }
    
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModel#load(java.io.InputStream, boolean)
     */
    public void load(InputStream source, boolean outOfSync) {
        
        super.load(source, outOfSync);
        
    }
    

}
