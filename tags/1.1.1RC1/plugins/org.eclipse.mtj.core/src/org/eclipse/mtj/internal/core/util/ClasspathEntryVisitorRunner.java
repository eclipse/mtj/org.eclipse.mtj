/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.util;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.MTJCore;

/**
 * Instances of this class operate on instances of IClasspathEntryVisitor to
 * walk through the tree of classpath entries.
 * 
 * @author Craig Setera
 */
public class ClasspathEntryVisitorRunner {
    private boolean breadthFirst;

    /**
     * Construct a new visitor runner, assuming a depth-first traversal of the
     * classpath entries.
     */
    public ClasspathEntryVisitorRunner() {
        this(false);
    }

    /**
     * Construct a new visitor runner, specifying whether or not to do a
     * breadth-first traversal and assuming we aren't ignoring things that
     * aren't exported.
     * 
     * @param breadthFirst
     */
    public ClasspathEntryVisitorRunner(boolean breadthFirst) {
        super();
        this.breadthFirst = breadthFirst;
    }

    /**
     * Run the visitor across the classpath entries of the specified
     * IJavaProject.
     * 
     * @param javaProject
     * @param entryVisitor
     * @param monitor
     * @throws CoreException
     */
    public void run(IJavaProject javaProject,
            IClasspathEntryVisitor entryVisitor, IProgressMonitor monitor)
            throws CoreException {
        if (javaProject != null) {
            // Store up the projects to be traversed during a
            // breadth-first traversal
            ArrayList<IJavaProject> breadthFirstProjects = new ArrayList<IJavaProject>();

            // Walk through the classpath entries in this project
            IClasspathEntry[] entries = javaProject.getRawClasspath();
            for (IClasspathEntry entry : entries) {
                visitEntry(javaProject, entry, entryVisitor,
                        breadthFirstProjects, monitor);
            }

            // Handle any projects that still need to be traversed in
            // breadth-first
            // fashion.
            Iterator<IJavaProject> iterator = breadthFirstProjects.iterator();
            while (iterator.hasNext()) {
                IJavaProject jProject = iterator.next();
                run(jProject, entryVisitor, monitor);
            }
        }
    }

    /**
     * Visit the specified classpath container entry.
     * 
     * @param javaProject
     * @param entry
     * @param entryVisitor
     * @param breadthFirstProjects
     * @param monitor
     * @throws CoreException
     */
    private void visitContainerEntry(IJavaProject javaProject,
            IClasspathEntry entry, IClasspathEntryVisitor entryVisitor,
            ArrayList<IJavaProject> breadthFirstProjects,
            IProgressMonitor monitor) throws CoreException {
        IPath containerPath = entry.getPath();

        // Consult the visitor about whether to keep traversing
        if (entryVisitor.visitContainerBegin(entry, javaProject, containerPath,
                monitor)) {
            IClasspathContainer cpContainer = JavaCore.getClasspathContainer(
                    containerPath, javaProject);
            IClasspathEntry[] entries = cpContainer.getClasspathEntries();

            for (IClasspathEntry entry2 : entries) {
                visitEntry(javaProject, entry2, entryVisitor,
                        breadthFirstProjects, monitor);
            }
            entryVisitor.visitContainerEnd(entry, javaProject, containerPath,
                    monitor);
        }
    }

    /**
     * Visit the specified IClasspathEntry via the IClasspathEntryVisitor.
     * 
     * @param javaProject
     * @param entry
     * @param entryVisitor
     * @param breadthFirstProjects
     * @param monitor
     * @throws CoreException
     */
    private void visitEntry(IJavaProject javaProject, IClasspathEntry entry,
            IClasspathEntryVisitor entryVisitor,
            ArrayList<IJavaProject> breadthFirstProjects,
            IProgressMonitor monitor) throws CoreException {
        switch (entry.getEntryKind()) {
            case IClasspathEntry.CPE_CONTAINER:
                visitContainerEntry(javaProject, entry, entryVisitor,
                        breadthFirstProjects, monitor);
                break;

            case IClasspathEntry.CPE_LIBRARY:
                visitLibraryEntry(javaProject, entry, entryVisitor, monitor);
                break;

            case IClasspathEntry.CPE_PROJECT:
                visitProjectEntry(javaProject, entry, entryVisitor,
                        breadthFirstProjects, monitor);
                break;

            case IClasspathEntry.CPE_SOURCE:
                visitSourceEntry(javaProject, entry, entryVisitor, monitor);
                break;

            case IClasspathEntry.CPE_VARIABLE:
                visitVariableEntry(javaProject, entry, entryVisitor,
                        breadthFirstProjects, monitor);
                break;
        }
    }

    /**
     * Visit the specified library classpath entry.
     * 
     * @param javaProject
     * @param entry
     * @param entryVisitor
     * @param monitor
     * @throws CoreException
     */
    private void visitLibraryEntry(IJavaProject javaProject,
            IClasspathEntry entry, IClasspathEntryVisitor entryVisitor,
            IProgressMonitor monitor) throws CoreException {
        entryVisitor.visitLibraryEntry(entry, javaProject, monitor);
    }

    /**
     * Visit the specified project classpath entry.
     * 
     * @param javaProject
     * @param entry
     * @param entryVisitor
     * @param monitor
     * @throws CoreException
     */
    private void visitProjectEntry(IJavaProject javaProject,
            IClasspathEntry entry, IClasspathEntryVisitor entryVisitor,
            ArrayList<IJavaProject> breadthFirstProjects,
            IProgressMonitor monitor) throws CoreException {
        IPath projectPath = entry.getPath();
        IProject project = (IProject) MTJCore.getWorkspace().getRoot()
                .findMember(projectPath);
        IJavaProject jProject = JavaCore.create(project);

        if (entryVisitor.visitProject(entry, javaProject, jProject, monitor)) {
            if (breadthFirst) {
                breadthFirstProjects.add(jProject);
            } else {
                run(jProject, entryVisitor, monitor);
            }
        }
    }

    /**
     * Visit the specified source classpath entry.
     * 
     * @param javaProject
     * @param entry
     * @param entryVisitor
     * @param monitor
     * @throws CoreException
     */
    private void visitSourceEntry(IJavaProject javaProject,
            IClasspathEntry entry, IClasspathEntryVisitor entryVisitor,
            IProgressMonitor monitor) throws CoreException {
        entryVisitor.visitSourceEntry(entry, javaProject, monitor);
    }

    /**
     * Visit the specified variable classpath entry.
     * 
     * @param javaProject
     * @param entry
     * @param entryVisitor
     * @param breadthFirstProjects
     * @param monitor
     * @throws CoreException
     */
    private void visitVariableEntry(IJavaProject javaProject,
            IClasspathEntry entry, IClasspathEntryVisitor entryVisitor,
            ArrayList<IJavaProject> breadthFirstProjects,
            IProgressMonitor monitor) throws CoreException {
        String variableName = entry.getPath().lastSegment();
        if (entryVisitor.visitVariable(entry, javaProject, variableName,
                monitor)) {
            IClasspathEntry entry2 = JavaCore.getResolvedClasspathEntry(entry);
            visitEntry(javaProject, entry2, entryVisitor, breadthFirstProjects,
                    monitor);
        }
    }
}
