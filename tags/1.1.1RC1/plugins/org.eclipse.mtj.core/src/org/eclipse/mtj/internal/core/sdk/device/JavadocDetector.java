/**
 * Copyright (c) 2003,2010 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma (Sybase) - Initial implementation
 *     
 */
package org.eclipse.mtj.internal.core.sdk.device;

import static org.eclipse.mtj.internal.core.Trace.trace;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.jar.JarFile;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.device.ILibrary;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary;
import org.eclipse.mtj.internal.core.Trace;

/**
 * Helper class to detect the javadoc location
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @since 1.0
 */
public class JavadocDetector {
   
	private static final String PACKAGE_LIST = "package-list"; //$NON-NLS-1$


	public URL detectJavadoc(ILibrary library) {
		boolean trace = Trace.isOptionEnabled(Trace.JAVADOC_DETECT_PERF);
		long enterTime=0;
		if (trace){
				 enterTime = System.currentTimeMillis();
		}
		
		URL retURL  = searchForJavaDoc(library);

		if (trace){
			long elapsed = System.currentTimeMillis() - enterTime;
			trace(Trace.JAVADOC_DETECT_PERF, "Detected javadoc for ", //$NON-NLS-1$
				library.toString(), " in ", elapsed, "ms"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return retURL;
	}


    
    private URL searchForJavaDoc(ILibrary library) {
        URL foundURL = null;

        File[] rootDirs = getDefaultDocRootDirectories(library);
        // a javadoc root directory exist
        if (rootDirs != null) {
            for (File docDir : rootDirs) {
                foundURL = searchForLibraryDoc(docDir, library);
                if (foundURL != null)
                    break;
            }
        }
        return foundURL;
    }
    
    


    // get the default javadoc directory for the library
    private File[] getDefaultDocRootDirectories(ILibrary library) {
        if (library == null)
            return null;

        ArrayList<File> docfiles = new ArrayList<File>();
        File parentDir = library.toFile().getParentFile();
        for (int i = 0; i < 5 && parentDir != null; i++) {
            File[] tmpDocfiles = parentDir.listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    // find the directory whose name contains "doc" string
                    return pathname.isDirectory()
                            && pathname.getName().indexOf("doc") != -1; //$NON-NLS-1$
                }
            });
            for (File file : tmpDocfiles) {
                docfiles.add(file);
            }
            parentDir = parentDir.getParentFile();

        }

        if (docfiles.size() > 0)
            return docfiles.toArray(new File[0]);
        else
            return null;

    }
    
    
    private URL searchForLibraryDoc(File parentDir, final ILibrary library) {
		if (library instanceof IMIDPLibrary) {
			// get the directories with javadoc
			File[] javadocDirs = parentDir.listFiles(new FileFilter() {
				public boolean accept(File folder) {
					if (folder.isDirectory()) {
						File indexFile = new File(folder, "index.html"); //$NON-NLS-1$
						if (indexFile.isFile()) {
							File packageList = new File(folder, PACKAGE_LIST);
							if (packageList.isFile()) {
								return true;
							}
						}
					}
					return false;
				}
			});

			if (javadocDirs.length > 0) {

				try {
					JarFile jar = new JarFile(library.toFile());
					boolean found = false;
					IMIDPLibrary midpLib = (IMIDPLibrary)library;
					boolean mustHaveJavaLang = midpLib.hasProfile() || midpLib.hasConfiguration();
					
					trace(Trace.JAVADOC_DETECT_DEBUG, "Searching javadocs for ",midpLib); //$NON-NLS-1$
					for (int i = 0; i < javadocDirs.length; i++) {
						File pList = new File(javadocDirs[i], PACKAGE_LIST);
						boolean foundJavaLang=false;
						BufferedReader reader = new BufferedReader(
								new FileReader(pList));
						String s = reader.readLine();
						while (s != null) {
							if (s.startsWith("#")) { //$NON-NLS-1$
								s = reader.readLine();
								continue;
							}
							// try to workaround the jsr classes packaged into CLDC & MIDP libraries.
							// very often SDKs pack the jsr classes to these libraries and without 
							// this guard the jsr javadocs are mapped to these libraries
							if(  mustHaveJavaLang && !foundJavaLang && "java.lang".equals(s.trim()) ){ //$NON-NLS-1$
								trace(Trace.JAVADOC_DETECT_DEBUG, "java.lang entry found in line ",s, " package-list is ",pList);  //$NON-NLS-1$//$NON-NLS-2$
								foundJavaLang =true;
							}
							if (jar.getEntry(s.replace('.', '/')) == null) {
								found = false;
								break;
							}
							s = reader.readLine();
							found = true ;
							if(mustHaveJavaLang && !foundJavaLang ){
								found = false;
							}
						}
						if (found) {
							trace(Trace.JAVADOC_DETECT_DEBUG, "Found javadocs for ",midpLib, " in ",javadocDirs[i]);  //$NON-NLS-1$//$NON-NLS-2$
							return javadocDirs[i].toURI().toURL();
						}
						// Could not find do a last attempt to match the directory names.
						String libFileName = library.toFile().getName();
						int dotIdx = libFileName.lastIndexOf('.');
						final String libName;
						if (dotIdx > -1)
							libName = libFileName.substring(0, dotIdx);
						else
							libName = libFileName;

						String pathname = javadocDirs[i].getName()
								.toLowerCase();
						String libraryName = libName.toLowerCase();
						if( pathname.startsWith(libraryName)
								|| libraryName.startsWith(pathname))
						{
							return javadocDirs[i].toURI().toURL();
						}
						
					}
				} catch (IOException e) {
					MTJCore.getMTJCore().getLog().log(
							new Status(IStatus.WARNING, MTJCore.getPluginId(), 
									"Error while matchin javadocs",e)); //$NON-NLS-1$
				}

			}

			// Could not detect
			// Now recurse to sub directories
			File[] subdirectories = parentDir.listFiles(new FileFilter() {
				public boolean accept(File pathname) {
					return pathname.isDirectory();
				}
			});
			URL url = null;
			for (int i = 0; i < subdirectories.length && url == null; i++) {
				url = searchForLibraryDoc(subdirectories[i], library);
			}
			return url;
		}
        return null;
    }
}
