/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.statemachine;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mtj.internal.core.Messages;

/**
 * AbstractState class implements an abstract state of the 
 * {@link StateMachine} class. States can have sub-states 
 * in order to support advanced state machines.
 * <br>
 * Use the {@link #setInitialState(AbstractState)} method
 * in order to set the initial sub-state, so when the parent
 * state is entered the initial child sub-state can be entered
 * as well.
 * <br>
 * State transitions are supported by adding and/or removing
 * {@link AbstractStateTransition} instances to a state.
 * 
 * @see AbstractStateTransition
 * 
 * @author David Marques
 */
public abstract class AbstractState {

	private List<AbstractStateTransition> transitions;
	private List<AbstractState>      childStates;
	
	private AbstractState parent;
	private AbstractState intialChild;
	
	/**
	 * Creates an {@link AbstractState} instance with the
	 * specified parent.
	 * 
	 * @param _parent parent state or null.
	 */
	public AbstractState(AbstractState _parent) {
		this.transitions = new ArrayList<AbstractStateTransition>();
		this.parent      = _parent;
		
		if (parent != null) {			
			this.parent.addChildState(this);
		}
	}
	
	/**
	 * Adds the specified {@link AbstractStateTransition} transition
	 * to the state.
	 * 
	 * @param _transition transition instance.
	 */
	public final void addTransition(AbstractStateTransition _transition) {
		if (_transition == null) {
			throw new IllegalArgumentException(Messages.AbstractState_transitionNotNull);
		}
		
		if (_transition.getSource() == null) {
			throw new IllegalArgumentException(Messages.AbstractState_noSourceState);
		}
		
		if (_transition.getTarget() == null) {
			throw new IllegalArgumentException(Messages.AbstractState_noTargetState);
		}
		
		if (_transition.getSource() != this) {
			throw new IllegalArgumentException(Messages.AbstractState_invalidSourceState);
		}
		
		if (!this.transitions.contains(_transition)) {
			this.transitions.add(_transition);
		}
	}
	
	/**
	 * Removes the specified {@link AbstractStateTransition} transition
	 * from the state.
	 * 
	 * @param _transition transition instance.
	 */
	public final void removeTransition(AbstractStateTransition _transition) {
		this.transitions.remove(_transition);
	}
	
	/**
	 * Sets the initial sub-state so when the state is entered
	 * it's initial sub-state is entered as well.
	 * 
	 * @param _state initial sub-state.
	 */
	public final void setInitialState(AbstractState _state) {
		if (_state == null) {
			throw new IllegalArgumentException(Messages.AbstractState_stateNotNull);
		}
		
		if (_state.getParent() != this) {
			throw new IllegalArgumentException(Messages.AbstractState_invalidInitialState);
		}
		this.intialChild = _state;
	}
	
	/**
	 * Gets this state's transitions.
	 * 
	 * @return transitions array.
	 */
	public final AbstractStateTransition[] getTransitions() {
		return this.transitions.toArray(new AbstractStateTransition[this.transitions.size()]);
	}
	
	/**
	 * Called when this state is entered. 
	 */
	protected abstract void onEnter();

	/**
	 * Called when this state is exited. 
	 */
	protected abstract void onExit();
	
	/**
	 * Adds the specified state as a child state.
	 * 
	 * @param _state child state.
	 */
	final synchronized void addChildState(AbstractState _state) {
		if (this.childStates == null) {
			this.childStates = new ArrayList<AbstractState>();
		}
		this.childStates.add(_state);
	}
	
	/**
	 * Gets the parent state.
	 * 
	 * @return parent state.
	 */
	final AbstractState getParent() {
		return this.parent;
	}

	/**
	 * Post the specified {@link AbstractStateMachineEvent} to this state
	 * in order to verify if any transition from this state is triggered
	 * by this event.
	 * 
	 * @param _event target event.
	 * @return new state to enter.
	 */
	final synchronized AbstractState postEvent(AbstractStateMachineEvent _event) {
		AbstractState nextState = null;
		
		List<AbstractStateTransition> candidates = new ArrayList<AbstractStateTransition>();
		AbstractStateTransition[] transitions = this.getTransitions();
		for (AbstractStateTransition transition : transitions) {
			if (transition.isTransitionReady(_event)) {
				candidates.add(transition);
			}
		}
		
		if (!candidates.isEmpty()) {			
			if (candidates.size() > 0x01) {
				throw new IllegalStateException(Messages.AbstractState_conflictingTransitions);
			} else {
				AbstractStateTransition transition = candidates.get(0); 
				nextState = transition.getTarget();
				transition.onTransition();
			}
		}
		return nextState;
	}
	
	/**
	 * Called when this state is entered.
	 */
	final void onEnterState() {
		this.onEnter();
		if (this.intialChild != null) {
			this.intialChild.onEnterState();
		}
	}
	
	/**
	 * Called when this state is exited.
	 */
	final void onExitState() {
		this.onExit();
		if (this.intialChild != null) {
			this.intialChild.onExitState();
		}
	}
}
