/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 *     David Marques (Motorola) - Implementing validation.
 *     David Marques (Motorola) - Adding default locale.
 *     Fernando Rocha(Motorola) - Validate duplicated locales.      
 *     David Marques (Motorola) - Adding getLocale(String localeName).               
 *     Fernando Rocha(Motorola) - Validate duplicated locales.
 *     Renato Franca (Motorola) - Fixing bug 257367. Error catching improvement                     
 *     Renato Franca (Motorola) - Implementing validate method [Bug 285589].
 */
package org.eclipse.mtj.internal.core.text.l10n;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.MTJPluginSchemas;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.xml.sax.SAXParseException;

/**
 * @since 0.9.1
 */
public class L10nLocales extends L10nObject {

    private static final long serialVersionUID = 1L;
    private L10nLocale defaultLocale;
    private Schema schema;
    private Vector<L10nLocale> localeList;

    /**
     * @param model
     */
    public L10nLocales(L10nModel model) {
        super(model, ELEMENT_LOCALES);
        setInTheModel(true);
        localeList = new Vector<L10nLocale>();
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.core.text.l10n.L10nObject#addChild(org.eclipse
     * .mtj.internal.core.text.l10n.L10nObject)
     */
    @Override
    public void addChild(L10nObject child) {
        addChildNode(child, true);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.core.text.l10n.L10nObject#addChild(org.eclipse
     * .mtj.internal.core.text.l10n.L10nObject,
     * org.eclipse.mtj.internal.core.text.l10n.L10nObject, boolean)
     */
    @Override
    public void addChild(L10nObject child, L10nObject sibling,
            boolean insertBefore) {
        int currentIndex = indexOf(sibling);
        if (!insertBefore) {
            currentIndex++;
        }

        addChildNode(child, currentIndex, true);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#canBeParent()
     */
    @Override
    public boolean canBeParent() {
        return true;
    }

    /**
     * @return
     */
    public String getDestination() {
        return getXMLAttributeValue(ATTRIBUTE_DESTINATION);
    }

    /**
     * Gets the default locale instance.
     * 
     * @return default locale.
     */
    public L10nLocale getDefaultLocale() {

        String name = getXMLAttributeValue(ATTRIBUTE_DEFAULT_LOCALE);

        if (name == null || name.length() == 0) {
            defaultLocale = null;

        } else {

            IDocumentElementNode[] localeNodes = this.getChildNodes();
            boolean found = false;
            for (IDocumentElementNode localeNode : localeNodes) {
                L10nLocale locale;

                if (localeNode instanceof L10nLocale) {

                    locale = (L10nLocale) localeNode;
                } else {
                    continue;
                }

                if (locale.getName().equals(name)) {
                    defaultLocale = locale;
                    found = true;
                    break;
                }
            }

            if (!found) {
                defaultLocale = null;
                setXMLAttribute(ATTRIBUTE_DEFAULT_LOCALE, ""); //$NON-NLS-1$
            }
        }
        return defaultLocale;
    }

    public L10nLocale createlLocale() {
        L10nLocale locale = null;

        locale = new L10nLocale(getModel());

        localeList.add(locale);
        return locale;
    }

    public L10nLocale getLocale(IDocumentElementNode loc) {
        L10nLocale locale = null;

        for (int i = 0; i < localeList.size(); i++) {
            if (localeList.get(i).equals(loc)) {
                locale = localeList.get(i);
            }
        }

        return locale;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getName()
     */
    @Override
    public String getName() {
        return "Locales"; //$NON-NLS-1$
    }

    /**
     * @return
     */
    public String getPackage() {
        String pack = getXMLAttributeValue(ATTRIBUTE_PACKAGE);
        return pack == null ? "" : pack; //$NON-NLS-1$
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getType()
     */
    @Override
    public int getType() {
        return TYPE_LOCALES;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.DocumentElementNode#isRoot()
     */
    @Override
    public boolean isRoot() {
        return true;
    }

    /**
     * @param l10nObject
     * @param newRelativeIndex
     */
    public void moveChild(L10nObject l10nObject, int newRelativeIndex) {
        moveChildNode(l10nObject, newRelativeIndex, true);
    }

    /**
     * @param l10nObject
     */
    public void removeChild(L10nObject l10nObject) {
        removeChildNode(l10nObject, true);
    }

    /**
     * @param name
     */
    public void setDestination(String name) {
        setXMLAttribute(ATTRIBUTE_DESTINATION, name);
    }

    /**
     * @param value
     */
    public void setPackage(String value) {
        setXMLAttribute(ATTRIBUTE_PACKAGE, value);
    }

    /**
     * Sets the default locale.
     * 
     * @param locale default locale instance.
     */
    public void setDefaultLocale(L10nLocale locale) {
        String value = null;
        if (locale != null) {
            value = locale.getName();
        }
        setXMLAttribute(ATTRIBUTE_DEFAULT_LOCALE, value);
        this.defaultLocale = locale;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#validate()
     */
    @Override
    public void validate() {

        // If Locales's schema isn't ok
        if (!validateLocalesSchema()) {

            IDocumentElementNode[] localeList = this.getChildNodes();
            for (int i = 0; i < localeList.length; i++) {
                L10nLocale locale;
                try {
                    locale = (L10nLocale) localeList[i];
                } catch (Exception e) {
                    // ClasCastException can be thrown
                    continue;
                }
                locale.validate();
            }
        } else {

            // check for duplicated tags
            IDocumentElementNode[] localeNodes = this.getChildNodes();
            List<L10nLocale> duplicates = new ArrayList<L10nLocale>();

            // Validate the Locales and the find duplicated Locales
            for (int i = 0; i < localeNodes.length; i++) {
                L10nLocale locale;

                try {
                    locale = (L10nLocale) localeNodes[i];
                } catch (Exception e) {
                    // ClasCastException can be thrown
                    continue;
                }

                // Validate the locale
                locale.validate();

                // Find duplicated locale
                String localeName = locale.getName();
                if (!duplicates.contains(locale)) {
                    boolean hasDuplicates = false;
                    for (int j = 0; j < localeNodes.length; j++) {
                        L10nLocale candidate;
                        
                        if(localeNodes[j] instanceof L10nLocale) {
                            
                            candidate = (L10nLocale) localeNodes[j];
                        } else {
                            continue;
                        }
                        
                        String candidateName = candidate.getLocaleName();
                        if (j != i
                                && localeName.equalsIgnoreCase(candidateName)) {
                            duplicates.add(candidate);
                            hasDuplicates = true;
                        }
                    }
                    if (hasDuplicates) {
                        duplicates.add(locale);
                    }
                }
            }
            // Mark the locales that are duplicated
            for (L10nLocale duplicate : duplicates) {
                duplicate.setStatus(new Status(IStatus.ERROR,
                        IMTJCoreConstants.PLUGIN_ID,
                        Messages.L10nLocales_duplicatedLocale));
            }

        }
    }

    /**
     * @return A vector with all the errors found
     */
    public Vector<L10nMarkerError> getMarkerErrors() {

        IDocumentElementNode[] localeNodes = this.getChildNodes();
        Vector<L10nMarkerError> markerErrors = new Vector<L10nMarkerError>();

        if (!this.getStatus().isOK()) {
                
                if(getStatus().getException() instanceof SAXParseException) {
                    
                    SAXParseException ex = (SAXParseException)getStatus().getException();
                    L10nMarkerError marker = new L10nMarkerError(ex.getMessage(), ex.getLineNumber());
                    markerErrors.add(marker);
                    
                } else {
                    
                    int errorLine = -1;
                    try {
                        int offSet = getModel().getDocument().getLineOfOffset(
                                this.getOffset());
                        int length = this.getLength();
                        errorLine = offSet
                                + getModel().getDocument().getNumberOfLines(this.getOffset(),
                                        length);
                   
                    } catch (BadLocationException e) {
                        return new Vector<L10nMarkerError>();
                    }
                    
                    L10nMarkerError marker = new L10nMarkerError(this.getStatus()
                            .getMessage(), errorLine);
                    markerErrors.add(marker);
                }
            
        } else {

            //check for internal errors
            for (int i = 0; i < localeNodes.length; i++) {

                try {
                    L10nLocale locale;

                    try {
                        locale = (L10nLocale) localeNodes[i];
                    } catch (Exception e) {
                        // ClasCastException can be thrown
                        continue;
                    }

                    if (locale.getStatus().isOK()) {
                        
                        //check for internal errors
                        Vector<L10nMarkerError> entryErrors = locale
                                .getMarkerErrors();

                        if (entryErrors.size() > 0) {
                            return entryErrors;
                        }

                    } else {

                        //return the exception found
                        int errorLine = -1;
                        try {
                            int offSet = getModel().getDocument()
                                    .getLineOfOffset(locale.getOffset());
                            int length = locale.getLength();
                            errorLine = offSet
                            + getModel().getDocument()
                                    .getNumberOfLines(locale.getOffset(), length);

                            L10nMarkerError marker = new L10nMarkerError(locale
                                    .getStatus().getMessage(), errorLine);
                            markerErrors.add(marker);
                        } catch (BadLocationException e) {
                            continue;
                        }

                    }
                } catch (Exception e) {
                    continue;
                }
            }
        }

        return markerErrors;
    }

    /**
     * @return The line of a given offSet
     */
    public int getLocalesLine() {
        int line = -1;

        try {
            line = getModel().getDocument().getLineOfOffset(this.getOffset());
        } catch (BadLocationException e) {
            return line;
        }

        return line;

    }

    /*
     * Validate the Locales tag using a specific schema
     */
    private boolean validateLocalesSchema() {

        boolean isOK = true;
        IDocument doc = getModel().getDocument();
        InputStream is = null;

        try {
            is = new BufferedInputStream(new ByteArrayInputStream(doc.get()
                    .getBytes("UTF-8"))); //$NON-NLS-1$
        } catch (UnsupportedEncodingException e) {
            isOK = false;
        }

        try {
            String schemaLang = "http://www.w3.org/2001/XMLSchema";//$NON-NLS-1$

            SchemaFactory factory = SchemaFactory.newInstance(schemaLang);

            if (schema == null) {
                schema = factory.newSchema(MTJPluginSchemas.getInstance()
                        .create(MTJPluginSchemas.LOCALES_SCHEMA));
            }

            Validator validator = schema.newValidator();

            validator.validate(new StreamSource(is));

            Status status = new Status(IStatus.OK, IMTJCoreConstants.PLUGIN_ID,
                    null);
            this.setStatus(status);
            
        } catch (Exception ex) {
            Status status = new Status(IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID, ex.getMessage(), ex);
            this.setStatus(status);
            isOK = false;
        }

        return isOK;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.core.text.DocumentObject#removeChildNode(org
     * .eclipse.mtj.internal.core.text.IDocumentElementNode, boolean)
     */
    public IDocumentElementNode removeChildNode(IDocumentElementNode child,
            boolean fireEvent) {

        localeList.remove(child);

        return super.removeChildNode(child, fireEvent);

    }

    /**
     * Gets the {@link L10nLocale} instance
     * with the specified locale name.
     * 
     * @param localeName target locale's name.
     * @return the child locale or null if none.
     */
    public L10nLocale getLocale(String localeName) {
        L10nLocale result = null;
        IDocumentElementNode[] localeNodes = this.getChildNodes();
        for (IDocumentElementNode node : localeNodes) {
            if (!(node instanceof L10nLocale)) {
                continue;
            }
            L10nLocale locale = (L10nLocale) node;
            if (locale.getLocaleName().equals(localeName)) {
               result = locale;
               break;
            }
        }
        return result;
    }

}
