/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)          - Initial implementation
 *     Gustavo de Paula (Motorola) - Configuration refactory [Bug 265522]
 *     Diego Sandin (Motorola)     - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.project.runtime.event;

import java.util.EventObject;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;

/**
 * RemoveMTJRuntimeEvent is used to notify that a MTJRuntime is removed from
 * MTJRuntimeList.
 * <p>
 * All Events are constructed with a reference to the object, the "source", that
 * is logically deemed to be the object upon which the Event in question
 * initially occurred upon.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 * 
 * @since 1.0
 */
public class RemoveMTJRuntimeEvent extends EventObject {

    /**
     * The default serial version UID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The removed MTJRuntime.
     */
    private MTJRuntime removedRuntime;

    /**
     * Creates a new instance of RemoveMTJRuntimeEvent.
     * 
     * @param source The MTJRuntimeList from where the MTJRuntime was removed.
     * @param removedRuntime the removed MTJRuntime.
     */
    public RemoveMTJRuntimeEvent(MTJRuntimeList source,
            MTJRuntime removedRuntime) {
        super(source);
        this.removedRuntime = removedRuntime;
    }

    /**
     * Return the removed MTJRuntime.
     * 
     * @return the removed MTJRuntime.
     */
    public MTJRuntime getRemovedMTJRuntime() {
        return removedRuntime;
    }

    /**
     * Set the removed MTJRuntime.
     * 
     * @param removedRuntime the removed MTJRuntime.
     */
    public void setRemovedMTJRuntime(MTJRuntime removedRuntime) {
        this.removedRuntime = removedRuntime;
    }

}
