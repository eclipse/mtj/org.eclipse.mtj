/**
 * Copyright (c) 2009 Research In Motion Limited and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial Version. This code borrows heavily 
 *                                        from the original DeviceRegistry code.
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerException;

import org.eclipse.core.runtime.IPath;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.ILibrary;
import org.eclipse.mtj.core.sdk.device.IManagedDevice;
import org.eclipse.mtj.internal.core.persistence.XMLPersistenceProvider;
import org.eclipse.mtj.internal.core.sdk.BasicSDK;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;

/**
 * ImportedDeviceRegistry is contained by DeviceRegistry. ImportedDeviceRegistry
 * provides a place to store and retrieve imported devices by name. It is
 * implemented as a multi-level registry in which the top-level device groups
 * reference lower-level devices. ImportedDeviceRegistry uses the persistence
 * mechanism provided by DeviceRegistry.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * 
 * @since 1.1
 * @noextend This class is not intended to be subclassed by clients.
 */
class ImportedDeviceRegistry {

    // Map of device group names that maps to a lower-level
    // map of device instances by name
    private Map<String, Map<String, IDevice>> deviceGroupsMap;
      
    private Map<String, Map<String, IDevice>> invalidDeviceGroupsMap;

    ImportedDeviceRegistry() {
        invalidDeviceGroupsMap = new HashMap<String, Map<String, IDevice>>();
    }

    void addDevice(IDevice device) throws IllegalArgumentException,
            PersistenceException {
        if ((device.getSDKName() == null) || (device.getName() == null)) {
            throw new IllegalArgumentException();
        }
        Map<String, IDevice> groupMap = getDeviceGroupMap(device.getSDKName(), true);
        groupMap.put(device.getName(), device);
    }

    void clear() throws PersistenceException {
        getDeviceGroupsMap().clear();
    }

    List<IDevice> getAllDevices() throws PersistenceException {
        return getAllDevicesInternal(false, null);
    }

    IDevice getDevice(String groupName, String deviceName)
            throws PersistenceException {
        IDevice device = null;
        Map<String, IDevice> groupMap = getDeviceGroupMap(groupName, false);
        if (groupMap != null) {
            device = groupMap.get(deviceName);
        }
        return device;
    }

     int getDeviceCount() throws PersistenceException {
        int deviceCount = 0;
        Iterator<Map<String, IDevice>> groupMaps = getDeviceGroupsMap()
                .values().iterator();
        while (groupMaps.hasNext()) {
            Map<String, IDevice> groupMap = groupMaps.next();
            deviceCount += groupMap.size();
        }
        return deviceCount;
    }

    List<IDevice> getDevices(String groupName)
            throws PersistenceException {
        List<IDevice> devices = getAllDevicesInternal(true, groupName);
        return (devices.isEmpty() ? null : devices);
    }

    List<String> getSDKNames() throws PersistenceException {
        return new ArrayList<String>(getDeviceGroupsMap().keySet());
    }

    void load() throws PersistenceException {
        XMLPersistenceProvider persistenceProvider 
            = DeviceRegistry.createPersistenceProvider();
        if (persistenceProvider != null) {
            loadUsing(persistenceProvider, false);
        }
    }
    
    /**
     * Similar to loadUsing
     * 
     * @param persistenceProvider the IPersistenceProvider implementation that
     *            provides the facilities for storing and retrieving persistable
     *            objects.
     * @param loadOnlyValidDevices - false to load all devices, true to load
     *            only valid devices
     * @throws PersistenceException
     */
    void loadUsing(IPersistenceProvider persistenceProvider,
            boolean loadOnlyValidDevices) throws PersistenceException {

        getDeviceGroupsMap().clear();
        invalidDeviceGroupsMap.clear();

        int deviceCount = persistenceProvider.loadInteger("deviceCount"); //$NON-NLS-1$

        for (int i = 0; i < deviceCount; i++) {
            IDevice device = (IDevice) persistenceProvider
            .loadPersistable("device" + i); //$NON-NLS-1$

            List<ILibrary> entries = device.getClasspath().getEntries();
            boolean isValidDevice = true;

            for (ILibrary library : entries) {
                File file = library.toFile();
                if (!file.exists()) {
                    isValidDevice = false;
                    break;
                }
            }

            if (!loadOnlyValidDevices) {
                addDevice(device);
                if (!isValidDevice) {
                    Map<String, IDevice> sdkMap = invalidDeviceGroupsMap
                    .get(device.getSDKName());
                    if (sdkMap == null) {
                        sdkMap = new HashMap<String, IDevice>();
                        invalidDeviceGroupsMap.put(device.getSDKName(),
                                sdkMap);
                    }
                    if (!sdkMap.containsKey(device.getName())) {
                        sdkMap.put(device.getName(), device);
                    }
                }
            } else if (isValidDevice) {
                addDevice(device);
            }
        }
    }

    synchronized void removeDevice(IDevice device) throws PersistenceException {
        
        // Only imported devices may be removed
        if (device instanceof IManagedDevice)
            return;
            
        Map<String, IDevice> groupMap = getDeviceGroupMap(device.getSDKName(), false);
        if (groupMap != null) {
            String key = getDeviceKey(groupMap, device);
            if (key != null) {
                groupMap.remove(key);
            }

            // Remove empty groups
            if (groupMap.isEmpty()) {
                deviceGroupsMap.remove(device.getSDKName());
            }
        }
        
        // Remove the device from a BasicSDK to keep the BasicSDK's deviceList in sync
        // with this regsitry.
        ISDK sdk = device.getSDK();
        if (sdk instanceof BasicSDK) {
            ((BasicSDK) sdk).deleteDevice(device);
        }
    }

    /**
     * Gets the key in the device group map for the device.
     * 
     * @param groupMap target group map.
     * @param device target device.
     * @return the device's key in the map.
     */
    private String getDeviceKey(Map<String, IDevice> groupMap, IDevice device) {
        String key = null;
        Iterator<Map.Entry<String, IDevice>> keyValuePairs 
            = groupMap.entrySet().iterator();
        while (keyValuePairs.hasNext()) {
            Map.Entry<String, IDevice> entry = keyValuePairs.next();
            IDevice currentDevice = entry.getValue();
            if (currentDevice == device) {
                key = entry.getKey();
                break;
            }
        }
        return key;
    }

    void store() throws PersistenceException, TransformerException,
            IOException {
        XMLPersistenceProvider provider = new XMLPersistenceProvider(
                "deviceRegistry"); //$NON-NLS-1$
        storeUsing(provider);
        XMLUtils.writeDocument(getComponentStoreFile(), provider.getDocument());
    }

    void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        List<IDevice> allDevices = getAllDevices();
        int deviceCount = 0;
        
        for (IDevice device : allDevices) {
            // Do not store managed devices
            if (!(device instanceof IManagedDevice)) {
                persistenceProvider.storePersistable(
                        "device" + deviceCount++, device); //$NON-NLS-1$
            }
        }
        
        persistenceProvider.storeInteger("deviceCount", deviceCount); //$NON-NLS-1$
    }

    /**
     * Do the initial device group maps creation and loading.
     * 
     * @throws PersistenceException
     */
    protected void createAndLoadDeviceGroups() throws PersistenceException {
        deviceGroupsMap = new HashMap<String, Map<String, IDevice>>();
        load();
    }

    /**
     * Get the file for the device store file.
     * 
     * @return
     */
    private File getComponentStoreFile() {
        IPath pluginStatePath = MTJCore.getMTJCore().getStateLocation();
        IPath storePath = pluginStatePath
                .append(IDeviceRegistryConstants.DEVICES_FILENAME);

        return storePath.toFile();
    }

    /**
     * Return the map of devices for the specified group. If the map is null and
     * createIfNull is specified, a new map will be created.
     * 
     * @param groupName
     * @param createIfNull
     * @return
     * @throws PersistenceException if there is a problem doing the initial
     *             registry load
     */
    protected Map<String, IDevice> getDeviceGroupMap(String groupName,
            boolean createIfNull) throws PersistenceException {
        Map<String, Map<String, IDevice>> groupsMap = getDeviceGroupsMap();
        Map<String, IDevice> deviceGroupMap = groupsMap.get(groupName);

        if ((deviceGroupMap == null) && createIfNull) {
            deviceGroupMap = new HashMap<String, IDevice>();
            groupsMap.put(groupName, deviceGroupMap);
        }

        return deviceGroupMap;
    }

    /**
     * Return the device groups map.
     * 
     * @return
     * @throws PersistenceException
     */
    private synchronized Map<String, Map<String, IDevice>> getDeviceGroupsMap()
            throws PersistenceException {
        if (deviceGroupsMap == null) {
            createAndLoadDeviceGroups();
        }

        return deviceGroupsMap;
    }
    
    /**
     * Return a list of names of SDKs containing invalid devices.  In general,
     * this is due to SDKs which have been removed from the file system.
     * 
     * @return list of SDK names which have invalid devices.
     */
    List<String> getInvalidSDKNames() {
        return new ArrayList<String>(invalidDeviceGroupsMap.keySet());
    }
    
    /**
     * @return An empty list if no matching devices are found.
     */
    private List<IDevice> getAllDevicesInternal(boolean filterByGroupName,
            String groupName) throws PersistenceException {
        
        // If filterByGroupName is false, a non-null groupName is likely an error
        if (!filterByGroupName && groupName != null)
            throw new IllegalArgumentException("If filterByGroupName is false, groupName should be null");
        
        if (filterByGroupName && groupName == null)
            throw new IllegalArgumentException("If filterByGroupName is true, groupName cannot be null");
     
        
        ArrayList<IDevice> allDevices = new ArrayList<IDevice>();

        Iterator<Map<String, IDevice>> groups = getDeviceGroupsMap().values().iterator();
        
        while (groups.hasNext()) {
            Map<String, IDevice> deviceMap = groups.next();
            allDevices.addAll(deviceMap.values());
        }
        
        if (filterByGroupName) {
            ArrayList<IDevice> filteredDevices = new ArrayList<IDevice>();
            for (IDevice importedDevice : allDevices) {
                if (importedDevice.getSDKName().equals(groupName)) {
                    filteredDevices.add(importedDevice);
                }
            } return filteredDevices;
        } 

        return allDevices;
    }
    
}
