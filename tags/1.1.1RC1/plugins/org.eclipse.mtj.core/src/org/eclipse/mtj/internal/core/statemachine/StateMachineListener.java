/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.statemachine;

/**
 * StateMachineListener interface is intended to be 
 * implemented by classes that want to be notified
 * of the state machine life cycle changes.
 * 
 * @author David Marques
 */
public interface StateMachineListener {

	/**
	 * Called when the state machine starts.
	 */
	public void started();
	
	
	/**
	 * Called when the state machine stops.
	 */
	public void stopped();
	
	/**
	 * Called when the state machine reaches
	 * a final state.
	 */
	public void finished();
	
}
