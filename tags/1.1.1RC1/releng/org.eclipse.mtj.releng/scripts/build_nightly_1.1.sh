#!/bin/bash

# *****************************************************************************************************************************************
# Copyright (c) 2008 Motorola.
# 
# All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 
# which accompanies this distribution, and is available at: http://www.eclipse.org/legal/epl-v10.html 
# 
# Contributors:
#     Motorola - initial version
# *****************************************************************************************************************************************
#         										Mobile Tools for Java nightly build script
# *****************************************************************************************************************************************

# To enable analysis add: -analysis "/opt/public/dsdp/mtj/baselines/1.0/dsdp-mtj-SDK-1.0_base.zip"

. build.sh -publish -notify -metadata -updateSite -updateSiteDirectory "/home/data/httpd/download.eclipse.org/dsdp/mtj/updates/1.1.0/NightlyBuilds/" N