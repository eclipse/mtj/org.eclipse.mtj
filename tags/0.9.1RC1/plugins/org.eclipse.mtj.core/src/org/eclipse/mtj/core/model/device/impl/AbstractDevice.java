/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Adapting code to use Jetty version 5.1.11
 *                                
 */
package org.eclipse.mtj.core.model.device.impl;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.launching.LaunchingUtils;
import org.eclipse.mtj.core.internal.overtheair.OTAServer;
import org.eclipse.mtj.core.internal.preverification.builder.PreverificationBuilder;
import org.eclipse.mtj.core.internal.utils.TemporaryFileManager;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.launching.ILaunchConstants;
import org.eclipse.mtj.core.model.Classpath;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.library.api.APIType;
import org.eclipse.mtj.core.model.preverifier.IPreverifier;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.persistence.IBundleReferencePersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * Abstract superclass of the various device implementations.
 * 
 * @author Craig Setera
 */
public abstract class AbstractDevice implements IDevice,
        IBundleReferencePersistable {

    // Variables that define this device
    protected String bundle;
    protected Classpath classpath;
    protected boolean debugServer;
    protected String description;
    protected Properties deviceProperties;
    protected File executable;
    protected String name;
    protected String[] protectionDomains;
    protected String groupName;
    protected IPreverifier preverifier;
    protected String launchCommandTemplate;

    /**
     * Test equality on a AbstractDevice and return a boolean indicating
     * equality.
     * 
     * @param device
     * @return
     */
    public boolean equals(AbstractDevice device) {
        return classpath.equals(device.classpath)
                && executable.equals(device.executable)
                && name.equals(device.name)
                && groupName.equals(device.groupName);
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IBundleReferencePersistable#getBundle()
     */
    public String getBundle() {
        return bundle;
    }

    /**
     * @see org.eclipse.mtj.core.model.device.IDevice#getClasspath()
     */
    public Classpath getClasspath() {
        return classpath;
    }

    /**
     * @see org.eclipse.mtj.core.model.device.IDevice#getConfigurationLibrary()
     */
    public ILibrary getConfigurationLibrary() {
        return getLibraryWithType(APIType.CONFIGURATION);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.device.IDevice#getDescription()
     */
    public String getDescription() {
        return description;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.device.IDevice#getDeviceProperties()
     */
    public Properties getDeviceProperties() {
        return deviceProperties;
    }

    /**
     * @return Returns the executable.
     */
    public File getExecutable() {
        return executable;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.device.IDevice#getName()
     */
    public String getName() {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.device.IDevice#getPreverifier()
     */
    public IPreverifier getPreverifier() {
        return preverifier;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.device.IDevice#getProfileLibrary()
     */
    public ILibrary getProfileLibrary() {
        return getLibraryWithType(APIType.PROFILE);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.device.IDevice#getProtectionDomains()
     */
    public String[] getProtectionDomains() {
        return protectionDomains;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.device.IDevice#getGroupName()
     */
    public String getGroupName() {
        return groupName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.device.IDevice#isDebugServer()
     */
    public boolean isDebugServer() {
        return debugServer;
    }

    /**
     * @see org.eclipse.mtj.core.model.device.IDevice#isPredeploymentRequired()
     * @deprecated this method is no longer called during launching
     */
    public boolean isPredeploymentRequired() {
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        classpath = (Classpath) persistenceProvider
                .loadPersistable("classpath");
        debugServer = persistenceProvider.loadBoolean("debugServer");
        description = persistenceProvider.loadString("description");
        deviceProperties = persistenceProvider
                .loadProperties("deviceProperties");
        name = persistenceProvider.loadString("name");

        protectionDomains = new String[persistenceProvider
                .loadInteger("protectionDomainsCount")];
        for (int i = 0; i < protectionDomains.length; i++) {
            protectionDomains[i] = persistenceProvider.loadString(
                    "protectionDomain" + i).trim();
        }

        groupName = persistenceProvider.loadString("groupName");
        preverifier = (IPreverifier) persistenceProvider
                .loadPersistable("preverifier");
        launchCommandTemplate = persistenceProvider
                .loadString("rawLaunchCommand");

        String executableString = persistenceProvider.loadString("executable");
        if ((executableString != null)
                && (executableString.trim().length() > 0)) {
            executable = new File(executableString);
        }
    }

    /**
     * @param classpath The classpath to set.
     */
    public void setClasspath(Classpath classpath) {
        this.classpath = classpath;
    }

    /**
     * @param debugServer The debugServer to set.
     */
    public void setDebugServer(boolean debugServer) {
        this.debugServer = debugServer;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param deviceProperties The deviceProperties to set.
     */
    public void setDeviceProperties(Properties deviceProperties) {
        this.deviceProperties = deviceProperties;
    }

    /**
     * @param executable The executable to set.
     */
    public void setExecutable(File executable) {
        this.executable = executable;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param predeploymentRequired The predeploymentRequired to set.
     * @deprecated
     */
    public void setPredeploymentRequired(boolean predeploymentRequired) {
    }

    /**
     * @param preverifier The preverifier to set.
     */
    public void setPreverifier(IPreverifier preverifier) {
        this.preverifier = preverifier;
    }

    /**
     * @param protectionDomains The protectionDomains to set.
     */
    public void setProtectionDomains(String[] protectionDomains) {
        this.protectionDomains = protectionDomains;

        // Trim any spaces that could cause problems
        for (int i = 0; i < protectionDomains.length; i++) {
            protectionDomains[i] = protectionDomains[i].trim();
        }
    }

    /**
     * @param groupName The groupName to set.
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        persistenceProvider.storePersistable("classpath", classpath);
        persistenceProvider.storeBoolean("debugServer", debugServer);
        persistenceProvider.storeString("description", description);
        persistenceProvider.storeProperties("deviceProperties",
                deviceProperties);
        persistenceProvider.storeString("name", name);
        persistenceProvider.storeInteger("protectionDomainsCount",
                protectionDomains.length);
        for (int i = 0; i < protectionDomains.length; i++) {
            persistenceProvider.storeString("protectionDomain" + i,
                    protectionDomains[i]);
        }
        persistenceProvider.storeString("groupName", groupName);
        persistenceProvider.storePersistable("preverifier", preverifier);
        persistenceProvider.storeString("rawLaunchCommand",
                launchCommandTemplate);

        if (executable != null) {
            persistenceProvider
                    .storeString("executable", executable.toString());
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        StringBuffer sb = new StringBuffer(getGroupName());
        sb.append("/").append(getName());

        return sb.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.persistence.IBundleReferencePersistable#setBundle(java.lang.String)
     */
    public void setBundle(String bundle) {
        this.bundle = bundle;
    }

    /**
     * @return Returns the launchCommandTemplate.
     */
    public String getLaunchCommandTemplate() {
        return launchCommandTemplate;
    }

    /**
     * @param launchCommandTemplate The launchCommandTemplate to set.
     */
    public void setLaunchCommandTemplate(String launchCommandTemplate) {
        this.launchCommandTemplate = launchCommandTemplate;
    }

    /**
     * Add a value from the launch configuration to the execution properties
     * map.
     * 
     * @param executionProperties
     * @param propertyName
     * @param launchConfiguration
     * @param launchConstant
     * @throws CoreException
     */
    protected void addLaunchConfigurationValue(Map<String, String> executionProperties,
            String propertyName, ILaunchConfiguration launchConfiguration,
            String launchConstant) throws CoreException {
        String configValue = launchConfiguration.getAttribute(launchConstant,
                (String) null);
        if (configValue != null) {
            executionProperties.put(propertyName, configValue);
        }
    }

	/**
	 * Copy the deployed jar and jad for use during launching. Return the folder
	 * into which the copy has been made.
	 * 
	 * @param suite
	 * @param monitor
	 * @param launchFromJad
	 *            - If launch from a JAD file.
	 * @return
	 * @throws IOException
	 */
	protected File copyForLaunch(IMidletSuiteProject suite,
			IProgressMonitor monitor, boolean launchFromJad)
			throws CoreException {
		try {
			// If launch from JAD, will copy the runtime JAD and JAR to JAD
			// Launching Base Folder. This folder is located in .mtj.tmp folder.
			if (launchFromJad) {
				IFolder emulationFolder = LaunchingUtils
						.getEmulationFolder(suite);
				// jadLaunchDir is the directory containing JAD and JAR for
				// launching
				File jadLaunchDir = LaunchingUtils.makeJadLaunchBaseDir(suite);
				// Get JAD and JAR for copy.
				File runtimeJad = emulationFolder.getFile(
						suite.getJadFileName()).getLocation().toFile();
				File runtimeJar = emulationFolder.getFile(
						suite.getJarFilename()).getLocation().toFile();
				File emulateJad = new File(jadLaunchDir, suite.getJadFileName());
				File emulateJar = new File(jadLaunchDir, suite.getJarFilename());
				// Do file copy.
				Utils.copyFile(runtimeJad, emulateJad, null);
				Utils.copyFile(runtimeJar, emulateJar, null);
				// Refresh local.
				LaunchingUtils.getProjectTempFolder(suite).refreshLocal(
						IResource.DEPTH_INFINITE, monitor);
				return jadLaunchDir;
			}
			// If launch from Midlet, will copy the runtime JAD and JAR to temp
			// folder. The folder is located in system temp directory.
			else {
				File tempFolder = null;
				IFolder emulationFolder = LaunchingUtils
						.getEmulationFolder(suite);
				File emulationDirectory = emulationFolder.getLocation()
						.toFile();
				String folderName = suite.getProject().getName().replace(' ',
						'_');
				tempFolder = TemporaryFileManager.instance.createTempDirectory(
						folderName, ".launch");
				Utils.copyDirectory(emulationDirectory, tempFolder, null);
				return tempFolder;
			}
		} catch (IOException e) {
			MTJCorePlugin.throwCoreException(IStatus.ERROR, -999, e);
			return null;
		}

	}

    /**
     * Return the JAD file to use for launching from the specified temporary
     * directory.
     * 
     * @param midletSuite
     * @param temporaryDirectory
     * @param monitor
     * @return
     */
    protected File getJadForLaunch(IMidletSuiteProject midletSuite,
            File temporaryDirectory, IProgressMonitor monitor) {
        return new File(temporaryDirectory, midletSuite.getJadFileName());
    }

    /**
     * Return the url specified by the user for direct launching or
     * <code>null</code> if none was specified.
     * 
     * @param launchConfiguration
     * @return
     * @throws CoreException
     */
    protected String getSpecifiedJadURL(ILaunchConfiguration launchConfiguration)
            throws CoreException {
        return launchConfiguration.getAttribute(
                ILaunchConstants.SPECIFIED_JAD_URL, (String) null);
    }

    /**
     * Return a boolean indicating whether or not the emulation should just
     * directly launch a specified JAD URL without the other things that
     * normally happen during launching.
     * 
     * @param configuration
     * @return
     * @throws CoreException
     */
    protected boolean shouldDirectLaunchJAD(ILaunchConfiguration configuration)
            throws CoreException {
        return configuration
                .getAttribute(ILaunchConstants.DO_JAD_LAUNCH, false);
    }

    /**
     * Return a boolean indicating whether or not the emulation should be
     * launched as OTA.
     * 
     * @param configuration
     * @return
     * @throws CoreException
     */
    protected boolean shouldDoOTA(ILaunchConfiguration configuration)
            throws CoreException {
        return configuration.getAttribute(ILaunchConstants.DO_OTA, true);
    }

    /**
     * Encode the specified information for a URL.
     * 
     * @param value
     * @return
     */
    protected String urlEncoded(String value) {
        String encoded = null;

        try {
            encoded = URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            // Should never happen
        }

        return encoded;
    }

    /**
     * Look for the library with the specified type and return it. If it cannot
     * be found, return <code>null</code>
     * 
     * @param type
     * @return
     */
    private ILibrary getLibraryWithType(APIType type) {
        ILibrary library = null;

        Classpath classpath = getClasspath();
        ILibrary[] libraries = classpath.getEntries();
        for (int i = 0; i < libraries.length; i++) {
            if (libraries[i].getAPI(type) != null) {
                library = libraries[i];
                break;
            }
        }

        return library;
    }

    /**
     * Return the Over the Air URL for accessing the JAD file via the built-in
     * OTA HTTP server.
     * 
     * @param launchConfig
     * @return
     * @throws CoreException
     */
    protected String getOTAURL(ILaunchConfiguration launchConfig,
            IMidletSuiteProject midletSuite) throws CoreException {
        // If we are doing OTA launching, make sure that the
        // OTA server has been started
        try {
            OTAServer.instance.start();
        } catch (Exception e) {
            MTJCorePlugin.log(IStatus.WARNING, "launch", e);
        }

        String projectName = midletSuite.getProject().getName();
        String jadName = midletSuite.getJadFileName();

        StringBuffer sb = new StringBuffer();
        sb.append("http://localhost:").append(OTAServer.getPort()).append(
                "/ota/").append(urlEncoded(projectName)).append('/').append(
                urlEncoded(jadName));

        return sb.toString();
    }

    /**
     * Return the classpath string to be used when launching the specified
     * project.
     * 
     * @param midletSuite
     * @return
     * @throws CoreException
     */
    protected String getProjectClasspathString(IMidletSuiteProject midletSuite,
            File temporaryDirectory, IProgressMonitor monitor)
            throws CoreException {
        de.schlichtherle.io.File deployedJar = PreverificationBuilder
                .getRuntimeJar(midletSuite.getProject(), monitor);
        return new File(temporaryDirectory, deployedJar.getName())
                .getAbsolutePath();
    }
}
