/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version                     
 */
package org.eclipse.mtj.internal.core.l10n;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.nature.AbstractNature;
import org.eclipse.mtj.core.nature.BuildSpecManipulator;

/**
 * L10nNature Class provides a localization nature to a MTJ project.
 * 
 * @author David Marques
 * @since 0.9.1
 */
public class L10nNature extends AbstractNature {

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.nature.AbstractNature#configure()
     */
    public void configure() throws CoreException {
        BuildSpecManipulator manipulator = new BuildSpecManipulator(
                getProject());
        manipulator.addBuilderBefore(IMTJCoreConstants.J2ME_PREPROCESSOR_ID,
                IMTJCoreConstants.L10N_BUILDER_ID, null);
        manipulator.commitChanges(new NullProgressMonitor());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.nature.AbstractNature#deconfigure()
     */
    public void deconfigure() throws CoreException {
        BuildSpecManipulator manipulator = new BuildSpecManipulator(
                getProject());
        manipulator.removeBuilder(IMTJCoreConstants.L10N_BUILDER_ID);
        manipulator.commitChanges(new NullProgressMonitor());
    }

}
