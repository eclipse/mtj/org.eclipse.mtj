/*******************************************************************************
 * Copyright (c) 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.mtj.internal.core.text.l10n;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.mtj.core.model.IModel;
import org.eclipse.mtj.internal.core.text.DocumentObject;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;

/**
 * L10nObject - All objects modeled in a Table of Contents subclass L10nObject
 * This class contains functionality common to all L10n elements.
 */
public abstract class L10nObject extends DocumentObject implements
        IL10nConstants, Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Constructs the L10nObject and initializes its attributes.
     * 
     * @param model The model associated with this L10nObject.
     * @param parent The parent of this L10nObject.
     */
    public L10nObject(L10nModel model, String tagName) {
        super(model, tagName);
    }

    /**
     * Add a TocObject child to this topic and signal the model if necessary.
     * 
     * @param child The child to add to the TocObject
     */
    public void addChild(L10nObject child) {
        addChildNode(child, true);
    }

    /**
     * Add a TocObject child to this topic beside a specified sibling and signal
     * the model if necessary.
     * 
     * @param child The child to add to the TocObject
     * @param sibling The object that will become the child's direct sibling
     * @param insertBefore If the object should be inserted before the sibling
     */
    public void addChild(L10nObject child, L10nObject sibling,
            boolean insertBefore) {
        int currentIndex = indexOf(sibling);
        if (!insertBefore) {
            currentIndex++;
        }

        addChildNode(child, currentIndex, true);
    }

    /**
     * @return true if this L10n object is capable of containing children.
     */
    public abstract boolean canBeParent();

    /**
     * @return true if a child object can be removed
     */
    public boolean canBeRemoved() {

        if (getType() == TYPE_LOCALES) {
            // Semantic Rule: The TOC root element can never be removed
            return false;
        }

        L10nObject parent = getParent();
        if (parent != null) {
            return true;
        }

        return false;
    }

    /**
     * Check if the object is a direct or indirect descendant of the object
     * parameter.
     * 
     * @param obj The L10n object to find in this object's ancestry
     * @return true if obj is an ancestor of this TOC object
     */
    public boolean descendsFrom(L10nObject obj) {
        if (this.equals(obj)) {
            return true;
        }

        if ((getParent() != null) && obj.canBeParent()) {
            return getParent().descendsFrom(obj);
        }

        return false;
    }

    /**
     * @return the children of the object or an empty List if none exist.
     */
    public List<IDocumentElementNode> getChildren() { // Create a copy of the
        // child list instead of
        // returning the list itself. That way, our list
        // of children cannot be altered from outside
        ArrayList<IDocumentElementNode> list = new ArrayList<IDocumentElementNode>();

        // Add children of this topic
        IDocumentElementNode[] childNodes = getChildNodes();
        if (childNodes.length > 0) {
            for (int i = 0; i < childNodes.length; ++i) {
                if (childNodes[i] instanceof L10nObject) {
                    list.add(childNodes[i]);
                }
            }
        }

        return list;
    }

    /**
     * @return the root L10n element that is an ancestor to this L10nObject.
     */
    public L10nModel getModel() {
        final IModel sharedModel = getSharedModel();
        if (sharedModel instanceof L10nModel) {
            return (L10nModel) sharedModel;
        }

        return null;
    }

    /**
     * @param l10nObject the child used to locate a sibling
     * @return the L10nObject proceeding the specified one in the list of
     *         children
     */
    public L10nObject getNextSibling(L10nObject l10nObject) {
        return (L10nObject) getNextSibling(l10nObject, L10nObject.class);
    }

    /**
     * @return the parent of this L10nObject, or <br />
     *         <code>null</code> if the L10nObject has no parent.
     */
    public L10nObject getParent() {
        IDocumentElementNode parent = getParentNode();
        return parent instanceof L10nObject ? (L10nObject) parent : null;
    }

    /**
     * @param l10nObject the child used to locate a sibling
     * @return the L10nObject preceding the specified one in the list of
     *         children
     */
    public L10nObject getPreviousSibling(L10nObject l10nObject) {
        return (L10nObject) getPreviousSibling(l10nObject, L10nObject.class);
    }

    /**
     * @return
     */
    public L10nLocales getLocales() {
        final L10nModel model = getModel();

        if (model != null) {
            return model.getLocales();
        }

        return null;
    }

    /**
     * Get the concrete type of this L10nObject.
     */
    public abstract int getType();

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.DocumentElementNode#isLeafNode()
     */
    @Override
    public boolean isLeafNode() {
        return !canBeParent();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.DocumentElementNode#getAttributeIndent()
     */
    @Override
    protected String getAttributeIndent() {
        return " "; //$NON-NLS-1$
    }

    public abstract String getName();
}
