/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.IModel;
import org.eclipse.mtj.core.model.IWritable;
import org.eclipse.mtj.internal.core.util.SAXParserWrapper;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @since 0.9.1
 */
public abstract class XMLEditingModel extends AbstractEditingModel {

    private IStatus status;

    /**
     * @param document
     * @param isReconciling
     */
    public XMLEditingModel(IDocument document, boolean isReconciling) {
        super(document, isReconciling);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.AbstractEditingModel#adjustOffsets(org.eclipse.jface.text.IDocument)
     */
    @Override
    public void adjustOffsets(IDocument document) {
        try {
            SAXParserWrapper parser = new SAXParserWrapper();
            parser.parse(getInputStream(document), createDocumentHandler(this,
                    false));
        } catch (SAXException e) {
        } catch (IOException e) {
        } catch (ParserConfigurationException e) {
        } catch (FactoryConfigurationError e) {
        }
    }

    /**
     * @return
     */
    public String getContents() {
        StringWriter swriter = new StringWriter();
        PrintWriter writer = new PrintWriter(swriter);
        setLoaded(true);
        save(writer);
        writer.flush();
        try {
            swriter.close();
        } catch (IOException e) {
        }
        return swriter.toString();
    }

    /**
     * @return
     */
    public IStatus getStatus() {
        return status;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.IModel#load(java.io.InputStream, boolean)
     */
    public void load(InputStream source, boolean outOfSync) {
        try {
            fLoaded = true;
            status = new Status(IStatus.OK, IMTJCoreConstants.PLUGIN_ID, null);

            SAXParserWrapper parser = new SAXParserWrapper();
            parser.parse(source, createDocumentHandler(this, true));
        } catch (SAXException e) {
            fLoaded = false;
            status = new Status(IStatus.ERROR, IMTJCoreConstants.PLUGIN_ID, e
                    .getMessage(), e);
        } catch (IOException e) {
            fLoaded = false;
        } catch (ParserConfigurationException e) {
            fLoaded = false;
        } catch (FactoryConfigurationError e) {
            fLoaded = false;
        }
    }

    /**
     * 
     */
    public void reload() {
        if (isResourceFile() == false) {
            return;
        }
        IFile file = (IFile) getUnderlyingResource();
        // Underlying file has to exist in order to reload the model
        if (file.exists()) {
            InputStream stream = null;
            try {
                // Get the file contents
                stream = new BufferedInputStream(file.getContents(true));
                // Load the model using the last saved file contents
                reload(stream, false);
                // Remove the dirty (*) indicator from the editor window
                setDirty(false);
            } catch (CoreException e) {
                // Ignore
            }
        }
    }

    /**
     * @param document
     */
    public void reload(IDocument document) {
        // Get the document's text
        String text = document.get();
        InputStream stream = null;

        try {
            // Turn the document's text into a stream
            stream = new ByteArrayInputStream(text.getBytes("UTF8")); //$NON-NLS-1$
            // Reload the model using the stream
            reload(stream, false);
            // Remove the dirty (*) indicator from the editor window
            setDirty(false);
        } catch (UnsupportedEncodingException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        } catch (CoreException e) {
            // Ignore
        }
    }

    /**
     * 
     */
    public void save() {
        if (isResourceFile() == false) {
            return;
        }
        try {
            IFile file = (IFile) getUnderlyingResource();
            String contents = getContents();
            ByteArrayInputStream stream = new ByteArrayInputStream(contents
                    .getBytes("UTF8")); //$NON-NLS-1$
            if (file.exists()) {
                file.setContents(stream, false, false, null);
            } else {
                file.create(stream, false, null);
            }
            stream.close();
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        } catch (IOException e) {
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.AbstractEditingModel#save(java.io.PrintWriter)
     */
    @Override
    public void save(PrintWriter writer) {
        if (isLoaded()) {
            getRoot().write("", writer); //$NON-NLS-1$
        }
        setDirty(false);
    }

    /**
     * @return
     */
    private boolean isResourceFile() {
        if (getUnderlyingResource() == null) {
            return false;
        } else if ((getUnderlyingResource() instanceof IFile) == false) {
            return false;
        }
        return true;
    }

    /**
     * @param model
     * @param reconciling
     * @return
     */
    protected abstract DefaultHandler createDocumentHandler(IModel model,
            boolean reconciling);

    /**
     * @return
     */
    protected abstract IWritable getRoot();
}