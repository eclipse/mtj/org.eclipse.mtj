/**
 * Copyright (c) 2006,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

import org.eclipse.mtj.core.model.IWritable;

/**
 * @since 0.9.1
 */
public interface IDocumentKey extends IWritable, IDocumentRange {
    
    /**
     * @param name
     */
    void setName(String name);

    /**
     * @return
     */
    String getName();

    /**
     * @param offset
     */
    void setOffset(int offset);

    /**
     * @param length
     */
    void setLength(int length);

    /**
     * @return
     */
    String write();

}
