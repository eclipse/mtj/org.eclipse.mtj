/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.core.model.configuration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.mtj.core.persistence.PersistenceException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class is used to maintains configurations for Midlet project. Each
 * Midlet project have one instance of this class. We retrieve the instance by
 * IMidletSuiteProject#getConfigurations().
 * 
 * @author wangf
 */
public class Configurations extends ArrayList<Configuration> {

    private static final long serialVersionUID = -8304504334314517586L;
    /**
     * The configuration element name the meta data xml file.
     */
    public static final String ELEM_CONFIGURATION = "configuration";
    /**
     * The listeners listen to Configurations change event.<br>
     * <b>Note:</b>Since instance of Configuration have a long life cycle (as
     * long as the Midlet project), so we should remove listener manually when
     * it no longer used.
     */
    private List<IConfigurationsChangeListener> listeners = new ArrayList<IConfigurationsChangeListener>();

    /**
     * The constructor used for create Configurations for a Midlet project that
     * have not yet configurations.
     */
    public Configurations() {
    }

    /**
     * Construct Configurations from meta data XML file.
     * 
     * @param configsElement - The &#60configurations&#62 xml element.
     * @throws PersistenceException
     */
    public Configurations(Element configsElement) throws PersistenceException {
        populateConfigurations(configsElement);
    }

    @Override
    public boolean add(Configuration config) {
        boolean result = super.add(config);
        if (result == false) {
            return result;
        }
        // notify event listener
        AddConfigEvent event = new AddConfigEvent(this, config);
        for (IConfigurationsChangeListener listener : listeners) {
            listener.configurationAdded(event);
        }
        return result;
    }

    @Override
    public boolean addAll(Collection<? extends Configuration> configs) {
        boolean modified = false;
        for (Configuration c : configs) {
            modified = this.add(c);
        }
        return modified;
    }

    public void addConfigsChangeListener(IConfigurationsChangeListener listener) {
        listeners.add(listener);
    }

    /**
     * Get the active configuration. If no active one, will return null.
     * 
     * @return
     */
    public Configuration getActiveConfiguration() {
        for (Configuration c : this) {
            if (c.isActive()) {
                return c;
            }
        }
        return null;
    }

    /**
     * Read configurations from xml and put them into Configurations object.
     * 
     * @param configsElement
     * @throws PersistenceException
     */
    private void populateConfigurations(Element configsElement)
            throws PersistenceException {
        NodeList configs = configsElement
                .getElementsByTagName(ELEM_CONFIGURATION);
        for (int i = 0; i < configs.getLength(); i++) {
            Configuration config = new Configuration((Element) configs.item(i));
            add(config);
        }
    }

    @Override
    public boolean remove(Object o) {
        boolean result = super.remove(o);
        if (result == false) {
            return result;
        }
        if (o instanceof Configuration) {
            Configuration removedConfig = (Configuration) o;
            RemoveConfigEvent event = new RemoveConfigEvent(this, removedConfig);
            for (IConfigurationsChangeListener listener : listeners) {
                listener.configurationRemoved(event);
            }
        }
        return result;
    }

    @Override
    public boolean removeAll(Collection<?> configs) {
        boolean modified = false;
        for (Object o : configs) {
            modified = this.remove(o);
        }
        return modified;
    }

    /**
     * <b>Note:</b>Since instance of Configurations have a long life cycle (as
     * long as the Midlet project), so we should remove listener manually when
     * it no longer used.
     * 
     * @param listener
     */
    public void removeConfigsChangeListener(
            IConfigurationsChangeListener listener) {
        listeners.remove(listener);
    }

    /**
     * Switch the current active configuration.
     * 
     * @param configuration - configuration to be set as active one.
     */
    public void switchActiveConfiguration(Configuration configuration) {
        Configuration oldActiveConfig = getActiveConfiguration();
        if (oldActiveConfig != null) {
            getActiveConfiguration().setActive(false);
        }
        configuration.setActive(true);
        // notify event listener
        SwitchActiveConfigEvent event = new SwitchActiveConfigEvent(this,
                oldActiveConfig, configuration);
        for (IConfigurationsChangeListener listener : listeners) {
            listener.activeConfigSwitched(event);
        }
    }
}
