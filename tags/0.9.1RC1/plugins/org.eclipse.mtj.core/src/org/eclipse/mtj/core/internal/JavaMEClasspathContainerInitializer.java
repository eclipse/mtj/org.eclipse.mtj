/**
 * Copyright (c) 2008 Hugo Raniere.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Hugo Raniere (Motorola)  - Initial implementation
 */
package org.eclipse.mtj.core.internal;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.ClasspathContainerInitializer;
import org.eclipse.jdt.core.IClasspathContainer;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * Initializes JavaMEClasspathContainer, acording to project's selected device.
 * This class caches one classpath container per device, avoiding multiple
 * objects creation with same content. When this class is called, through
 * org.eclipse.jdt.core.classpathContainerInitializer" extension point, it set
 * in the project, the cached ClasspathContainer associated with the project's
 * device.
 * 
 * @author Hugo Raniere
 * @since MTJ 0.9
 * @see org.eclipse.jdt.core.ClasspathContainerInitializer,
 *      JavaMEClasspathContainer
 */
public class JavaMEClasspathContainerInitializer extends
        ClasspathContainerInitializer {

    // Caches one JavaMEClasspathContainer per Device registered in
    // DeviceRegistry
    private static Map<IDevice, JavaMEClasspathContainer> containers = new HashMap<IDevice, JavaMEClasspathContainer>();

    /**
     * Sets to the project a JavaMEClasspathContainer corresponding to the
     * project's device.
     * 
     * @see org.eclipse.jdt.core.ClasspathContainerInitializer#initialize(org.eclipse.core.runtime.IPath,
     *      org.eclipse.jdt.core.IJavaProject)
     */
    @Override
    public void initialize(IPath containerPath, IJavaProject project)
            throws CoreException {

        String deviceGroup = containerPath.segment(1);
        String deviceName = containerPath.segment(2);
        try {
            IDevice device = DeviceRegistry.singleton.getDevice(deviceGroup,
                    deviceName);
            JavaMEClasspathContainer container = containers.get(device);

            if (device != null) {
                if (container == null) {
                    container = new JavaMEClasspathContainer(containerPath,
                            device);
                    containers.put(device, container);
                }
                JavaCore.setClasspathContainer(containerPath,
                        new IJavaProject[] { project },
                        new IClasspathContainer[] { container }, null);

            } else {

                JavaCore.setClasspathContainer(containerPath,
                        new IJavaProject[] { project },
                        new IClasspathContainer[] { null }, null);
            }
        } catch (PersistenceException e) {

            MTJCorePlugin.log(IStatus.ERROR,
                    "Could not load JavaMEClasspath, device " + deviceGroup
                            + "/" + deviceName + "not found.", e);
        }
    }
}
