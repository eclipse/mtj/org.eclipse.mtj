/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.core.model.configuration;

/**
 * The instance of this interface observe Configurations change.
 * 
 * @author wangf
 */
public interface IConfigurationsChangeListener {

    void activeConfigSwitched(SwitchActiveConfigEvent event);

    void configurationAdded(AddConfigEvent event);

    void configurationRemoved(RemoveConfigEvent event);
}
