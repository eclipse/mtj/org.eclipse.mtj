/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.library.api;

/**
 * An enumeration type that represents the type of an API.
 * 
 * @author Craig Setera
 */
public class APIType {

    /**
     * Static constant representing a API that acts as a configuration
     */
    public static final int CONFIGURATION_CODE = 1;

    /**
     * An API that acts as a configuration
     */
    public static final APIType CONFIGURATION = new APIType(CONFIGURATION_CODE);

    /** Static constant representing an API that acts as a profile */
    public static final int PROFILE_CODE = 2;

    /**
     * An API that acts as a profile
     */
    public static final APIType PROFILE = new APIType(PROFILE_CODE);

    /**
     * Static constant representing an API that offers optional functionality
     */
    public static final int OPTIONAL_CODE = 3;

    /**
     * An API that is optional
     */
    public static final APIType OPTIONAL = new APIType(OPTIONAL_CODE);

    /**
     * Static constant representing an API that is of an unknown type
     */
    public static final int UNKNOWN_CODE = 0;

    /**
     * An API that is an unknown type
     */
    public static final APIType UNKNOWN = new APIType(UNKNOWN_CODE);

    // Strings values for the various types
    public static final String[] TYPE_STRINGS = new String[] { "Unknown",
            "Configuration", "Profile", "Optional", };

    // The type instances in order
    private static final APIType[] types = new APIType[] { UNKNOWN,
            CONFIGURATION, PROFILE, OPTIONAL, };

    /**
     * Return the type associated with the specified code.
     * 
     * @param typeCode
     * @return
     */
    public static APIType typeForCode(int typeCode) {
        APIType type = UNKNOWN;

        if ((typeCode >= 1) && (typeCode <= 3)) {
            type = types[typeCode];
        }

        return type;
    }

    private int typeCode;

    private APIType(int typeCode) {
        this.typeCode = typeCode;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return (this == obj);
    }

    /**
     * Return the type code for this particular type.
     * 
     * @return
     */
    public int getTypeCode() {
        return typeCode;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return typeCode;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return TYPE_STRINGS[typeCode];
    }
}
