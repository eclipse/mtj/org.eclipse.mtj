/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.internal.core.text.l10n;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public interface IL10nConstants {

    // elements
    public static final String ELEMENT_LOCALES = "locales";
    public static final String ELEMENT_LOCALE = "locale";
    public static final String ELEMENT_ENTRY = "entry";

    // Attributes
    public static final String ATTRIBUTE_PACKAGE = "package";
    public static final String ATTRIBUTE_DESTINATION = "destination";
    public static final String ATTRIBUTE_NAME = "name";
    public static final String ATTRIBUTE_KEY = "key";
    public static final String ATTRIBUTE_VALUE = "value";

    // Types
    public static final int TYPE_LOCALES = 0;
    public static final int TYPE_LOCALE = 1;
    public static final int TYPE_ENTRY = 2;

}
