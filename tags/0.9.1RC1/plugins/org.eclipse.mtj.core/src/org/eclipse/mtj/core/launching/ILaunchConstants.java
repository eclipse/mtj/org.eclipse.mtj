/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards, renamed Launch Constants values
 *                                from eclipseme.* to mtj.*
 *     Feng Wang (Sybase) - Remove constant EMULATED_CLASS, use 
 *                          IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME
 *                          instead, we do this to take advantage of JDT launch
 *                          configuration refactoring participates.
 */
package org.eclipse.mtj.core.launching;

/**
 * Constant definitions for launching support.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Craig Setera
 */
public interface ILaunchConstants {

    /**
     * The launch configuration type for the wireless emulator
     */
    public static final String LAUNCH_CONFIG_TYPE = "MTJ.emulatorLaunchConfigurationType";

    /**
     * Whether we are emulating a Midlet or doing OTA
     */
    public static final String DO_OTA = "mtj.do_ota";

    /**
     * Whether to use the specified JAD URL to do the launch, bypassing JAD
     * creation and OTA use
     */
    public static final String DO_JAD_LAUNCH = "mtj.do_jad_launch";

    /**
     * The URL specified by the user for launching when DO_JAD_LAUNCH has been
     * set true
     */
    public static final String SPECIFIED_JAD_URL = "mtj.specified_jad_url";

    /**
     * The emulated classpath for the emulated class
     */
    public static final String EMULATED_CLASSPATH = "mtj.emulated_classpath";

    /**
     * The verbosity options
     */
    public static final String VERBOSITY_OPTIONS = "mtj.verbosity_options";

    /**
     * Whether or not to use the project's device for emulation
     */
    public static final String USE_PROJECT_DEVICE = "mtj.use_project_device";

    /** The device to be emulated */
    public static final String EMULATED_DEVICE = "mtj.emulated_device";

    /**
     * The name of the group for the device to be emulated
     */
    public static final String EMULATED_DEVICE_GROUP = "mtj.emulated_device_group";

    /**
     * The application descriptor to be executed
     */
    public static final String APP_DESCRIPTOR = "mtj.app_descriptor";

    /**
     * The heap size to be used by the emulator
     */
    public static final String HEAP_SIZE = "mtj.heap_size";

    /**
     * The security domain to execute the emulation under
     */
    public static final String SECURITY_DOMAIN = "mtj.security_domain";

    /**
     * The security domain setting that tells not to add the security domain
     */
    public static final String NO_SECURITY_DOMAIN = "None";

    /**
     * Extra device launch parameters
     */
    public static final String LAUNCH_PARAMS = "mtj.launch_params";
}
