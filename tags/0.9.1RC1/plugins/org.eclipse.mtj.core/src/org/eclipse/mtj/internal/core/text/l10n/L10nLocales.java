/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.internal.core.text.l10n;

/**
 * @since 0.9.1
 */
public class L10nLocales extends L10nObject {

    private static final long serialVersionUID = 1L;

    /**
     * @param model
     */
    public L10nLocales(L10nModel model) {
        super(model, ELEMENT_LOCALES);
        setInTheModel(true);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.DocumentElementNode#isRoot()
     */
    public boolean isRoot() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.TocTopic#getType()
     */
    public int getType() {
        return TYPE_LOCALES;
    }

    /**
     * @return
     */
    public String getPackage() {
        String pack = getXMLAttributeValue(ATTRIBUTE_PACKAGE);
        return pack == null ? "" : pack;
    }

    /**
     * @param value
     */
    public void setPackage(String value) {
        setXMLAttribute(ATTRIBUTE_PACKAGE, value);
    }

    /**
     * @return
     */
    public String getDestination() {
        return getXMLAttributeValue(ATTRIBUTE_DESTINATION);
    }

    /**
     * @param name
     */
    public void setDestination(String name) {
        setXMLAttribute(ATTRIBUTE_DESTINATION, name);
    }

    /**
     * Add a L10nObject child to this topic and signal the model if necessary.
     * 
     * @param child The child to add to the L10nObject
     */
    public void addChild(L10nObject child) {
        addChildNode(child, true);
    }

    /**
     * Add a L10nObject child to this topic beside a specified sibling and
     * signal the model if necessary.
     * 
     * @param child The child to add to the L10nObject
     * @param sibling The object that will become the child's direct sibling
     * @param insertBefore If the object should be inserted before the sibling
     */
    public void addChild(L10nObject child, L10nObject sibling,
            boolean insertBefore) {
        int currentIndex = indexOf(sibling);
        if (!insertBefore) {
            currentIndex++;
        }

        addChildNode(child, currentIndex, true);
    }

    /**
     * @param l10nObject
     * @param newRelativeIndex
     */
    public void moveChild(L10nObject l10nObject, int newRelativeIndex) {
        moveChildNode(l10nObject, newRelativeIndex, true);
    }

    /**
     * @param l10nObject
     */
    public void removeChild(L10nObject l10nObject) {
        removeChildNode(l10nObject, true);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#canBeParent()
     */
    @Override
    public boolean canBeParent() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getName()
     */
    @Override
    public String getName() {
        return "Locales";
    }

}
