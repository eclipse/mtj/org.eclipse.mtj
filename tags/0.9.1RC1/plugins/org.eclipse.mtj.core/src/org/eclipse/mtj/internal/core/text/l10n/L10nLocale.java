/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.text.l10n;

public class L10nLocale extends L10nObject {

    private static final long serialVersionUID = 1L;

    /**
     * @param model
     */
    public L10nLocale(L10nModel model) {
        super(model, ELEMENT_LOCALE);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#canBeParent()
     */
    public boolean canBeParent() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getType()
     */
    public int getType() {
        return TYPE_LOCALE;
    }

    /**
     * @return
     */
    public String getLocaleName() {
        return getXMLAttributeValue(ATTRIBUTE_NAME);
    }

    /**
     * @param name
     */
    public void setLocaleName(String name) {
        setXMLAttribute(ATTRIBUTE_NAME, name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getName()
     */
    @Override
    public String getName() {
        return getXMLAttributeValue(ATTRIBUTE_NAME);
    }
}
