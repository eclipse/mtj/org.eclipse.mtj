/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.text.l10n;

/**
 * @since 0.9.1
 */
public class L10nEntry extends L10nObject {

    private static final long serialVersionUID = 1L;

    public L10nEntry(L10nModel model) {
        super(model, ELEMENT_ENTRY);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#canBeParent()
     */
    @Override
    public boolean canBeParent() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getType()
     */
    @Override
    public int getType() {
        return TYPE_ENTRY;
    }

    /**
     * @return
     */
    public String getKey() {
        return getXMLAttributeValue(ATTRIBUTE_KEY);
    }

    /**
     * @param Key
     */
    public void setKey(String Key) {
        setXMLAttribute(ATTRIBUTE_KEY, Key);
    }

    /**
     * @return
     */
    public String getValue() {
        return getXMLAttributeValue(ATTRIBUTE_VALUE);
    }

    /**
     * @param value
     */
    public void setValue(String value) {
        setXMLAttribute(ATTRIBUTE_VALUE, value);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.l10n.L10nObject#getName()
     */
    @Override
    public String getName() {
        return getXMLAttributeValue(ATTRIBUTE_KEY);
    }
}
