/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.text.l10n;

import org.eclipse.mtj.internal.core.text.DocumentNodeFactory;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.IDocumentNodeFactory;

/**
 * @since 0.9.1
 */
public class L10nDocumentFactory extends DocumentNodeFactory implements
        IDocumentNodeFactory {

    private L10nModel fModel;

    /**
     * @param model
     */
    public L10nDocumentFactory(L10nModel model) {
        fModel = model;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.DocumentNodeFactory#createDocumentNode(java.lang.String, org.eclipse.mtj.internal.core.text.IDocumentElementNode)
     */
    @Override
    public IDocumentElementNode createDocumentNode(String name,
            IDocumentElementNode parent) {

        if (isLocales(name)) { // L10nLocales Root
            return createL10nLocales();
        }

        if (isEntry(name)) { // Entry
            return createL10nEntry();
        }

        if (isLocale(name)) { // Locale
            return createL10nLocale();
        }

        return super.createDocumentNode(name, parent);
    }

    /**
     * @param parent
     * @return
     */
    public L10nEntry createL10nEntry() {
        return new L10nEntry(fModel);
    }

    /**
     * @return
     */
    public L10nLocale createL10nLocale() {
        return new L10nLocale(fModel);
    }

    /**
     * @return
     */
    public L10nLocales createL10nLocales() {
        return new L10nLocales(fModel);
    }

    /**
     * @param name
     * @return
     */
    private boolean isEntry(String name) {
        return isL10nElement(name, IL10nConstants.ELEMENT_ENTRY);
    }

    /**
     * @param name
     * @param elementName
     * @return
     */
    private boolean isL10nElement(String name, String elementName) {
        if (name.equals(elementName)) {
            return true;
        }
        return false;
    }

    /**
     * @param name
     * @return
     */
    private boolean isLocale(String name) {
        return isL10nElement(name, IL10nConstants.ELEMENT_LOCALE);
    }

    /**
     * @param name
     * @return
     */
    private boolean isLocales(String name) {
        return isL10nElement(name, IL10nConstants.ELEMENT_LOCALES);
    }
}
