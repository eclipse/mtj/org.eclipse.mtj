/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.TreeMap;

import org.eclipse.mtj.core.model.IModel;

/**
 * @since 0.9.1
 */
public interface IDocumentElementNode extends Serializable, IDocumentRange,
        IDocumentXMLNode {

    public static final String F_PROPERTY_CHANGE_TYPE_SWAP = "type_swap"; //$NON-NLS-1$

    // Not used by text edit operations
    public boolean canTerminateStartTag();

    // Not used by text edit operations
    public int getChildCount();

    // Not used by text edit operations
    public ArrayList<IDocumentElementNode> getChildNodesList();

    // Not used by text edit operations
    public String getIndent();

    // Not used by text edit operations
    public int getNodeAttributesCount();

    // Not used by text edit operations
    public TreeMap<String, IDocumentAttributeNode> getNodeAttributesMap();

    // Not used by text edit operations
    public String getXMLContent();

    // Not used by text edit operations
    public boolean hasXMLAttributes();

    // Not used by text edit operations
    public boolean hasXMLChildren();

    // Not used by text edit operations
    public boolean hasXMLContent();

    // Not used by text edit operations
    public boolean isContentCollapsed();

    // Not used by text edit operations
    public boolean isLeafNode();

    // Not used by text edit operations
    public void reconnect(IDocumentElementNode parent, IModel model);

    // Not used by text edit operations
    public boolean setXMLAttribute(String name, String value);

    /**
     * @param text String already trimmed and formatted
     * @return
     */
    public boolean setXMLContent(String text);

    void addChildNode(IDocumentElementNode child);

    void addChildNode(IDocumentElementNode child, int position);

    void addTextNode(IDocumentTextNode textNode);

    IDocumentElementNode getChildAt(int index);

    IDocumentElementNode[] getChildNodes();

    IDocumentAttributeNode getDocumentAttribute(String name);

    int getLineIndent();

    IDocumentAttributeNode[] getNodeAttributes();

    IDocumentElementNode getParentNode();

    IDocumentElementNode getPreviousSibling();

    IDocumentTextNode getTextNode();

    // Not used by text edit operations
    String getXMLAttributeValue(String name);

    String getXMLTagName();

    // Not used by text edit operations
    int indexOf(IDocumentElementNode child);

    boolean isErrorNode();

    boolean isRoot();

    IDocumentElementNode removeChildNode(IDocumentElementNode child);

    // Not used by text edit operations
    IDocumentElementNode removeChildNode(int index);

    void removeDocumentAttribute(IDocumentAttributeNode attr);

    void removeTextNode();

    void setIsErrorNode(boolean isErrorNode);

    void setLength(int length);

    void setLineIndent(int indent);

    void setOffset(int offset);

    void setParentNode(IDocumentElementNode node);

    void setPreviousSibling(IDocumentElementNode sibling);

    void setXMLAttribute(IDocumentAttributeNode attribute);

    void setXMLTagName(String tag);

    // Not used by text edit operations
    void swap(IDocumentElementNode child1, IDocumentElementNode child2);

    String write(boolean indent);

    String writeShallow(boolean terminate);

}
