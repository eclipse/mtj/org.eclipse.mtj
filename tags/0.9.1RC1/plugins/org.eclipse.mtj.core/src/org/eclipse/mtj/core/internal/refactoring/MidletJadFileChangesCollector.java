/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.internal.refactoring;

import java.util.regex.Matcher;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.utils.AbstractClasspathEntryVisitor;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;

/**
 * Type description
 * 
 * @author Craig Setera
 */
class MidletJadFileChangesCollector extends AbstractClasspathEntryVisitor {
    static final int MODE_RENAME = 1;
    static final int MODE_DELETE = 2;

    /**
     * Create the CompositeChange that holds the JAD file changes.
     * 
     * @param mode
     * @param type
     * @param newName
     * @param monitor
     * @return
     * @throws CoreException
     */
    public static CompositeChange collectChange(int mode, IType type,
            String newName, IProgressMonitor monitor) throws CoreException {
        IJavaProject javaProject = (IJavaProject) type
                .getAncestor(IJavaElement.JAVA_PROJECT);

        MidletJadFileChangesCollector collector = new MidletJadFileChangesCollector(
                mode, javaProject, type, newName);

        collector.addJavaProjectChanges(javaProject, monitor);
        collector.getRunner().run(javaProject, collector, monitor);

        return collector.getChange();
    }

    private int mode;
    private String oldName;
    private String newName;
    private CompositeChange change;

    /**
     * Construct a new changes collector.
     * 
     * @param javaProject
     * @param change
     */
    public MidletJadFileChangesCollector(int mode, IJavaProject javaProject,
            IType type, String newName) {
        this.mode = mode;
        this.newName = newName;
        this.oldName = type.getFullyQualifiedName();

        change = new CompositeChange(
                "Update Application Descriptor Midlet References");
    }

    /**
     * Return the composite change created by this collector.
     * 
     * @return
     */
    public CompositeChange getChange() {
        return change;
    }

    /**
     * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitProject(org.eclipse.jdt.core.IClasspathEntry,
     *      org.eclipse.jdt.core.IJavaProject,
     *      org.eclipse.jdt.core.IJavaProject,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public boolean visitProject(IClasspathEntry entry,
            IJavaProject javaProject, IJavaProject classpathProject,
            IProgressMonitor monitor) throws CoreException {
        addJavaProjectChanges(javaProject, monitor);
        return true;
    }

    /**
     * Add any JAD file updates found in the specified project.
     * 
     * @param jadFile
     * @param monitor
     * @throws CoreException
     */
    private void addJadFileChanges(IFile jadFile, IProgressMonitor monitor)
            throws CoreException {

        MidletJadTextFileMidletChange jadChange = new MidletJadTextFileMidletChange(
                mode, jadFile.getName(), jadFile, oldName, newName);

        // Only add changes that actually match
        SubProgressMonitor subProgressMonitor = new SubProgressMonitor(monitor,
                3);
        Matcher matcher = jadChange.getMatcher(monitor);
        if (matcher.find()) {
            jadChange.initializeTextEdit(monitor);
            change.add(jadChange);
        }

        subProgressMonitor.done();
    }

    /**
     * Handle visitation of a java project.
     * 
     * @param javaProject
     * @param monitor
     * @throws CoreException
     */
    private void addJavaProjectChanges(IJavaProject javaProject,
            IProgressMonitor monitor) throws CoreException {
        monitor.setTaskName("Creating Application Descriptor for "
                + javaProject.getElementName());

        if (isMidletSuite(javaProject)) {
            IMidletSuiteProject midletSuite = MidletSuiteFactory
                    .getMidletSuiteProject(javaProject);

            if (midletSuite != null) {
                IFile jadFile = midletSuite.getApplicationDescriptorFile();
                if (jadFile.exists()) {
                    addJadFileChanges(jadFile, monitor);
                }
            }
        }

        monitor.worked(1);
    }

    /**
     * Return a boolean describing whether the specified java project is a
     * midlet suite project.
     * 
     * @param javaProject
     * @return
     * @throws CoreException
     */
    private boolean isMidletSuite(IJavaProject javaProject)
            throws CoreException {
        return javaProject.getProject().hasNature(
                IMTJCoreConstants.MTJ_NATURE_ID);
    }
}