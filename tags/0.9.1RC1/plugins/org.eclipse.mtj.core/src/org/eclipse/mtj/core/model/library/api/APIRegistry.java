/**
 * Copyright (c) 2007,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Changed the getAPIs to first try to extract 
 *                                API info from MANIFEST
 *     Gustavo de Paula (Motorola)  - Fix getTypeFromManifest NPE  
 *                                                                    
 *                              
 */
package org.eclipse.mtj.core.model.library.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.model.Version;

/**
 * Provides access to the API definitions that have been registered via the API
 * extension point.
 * 
 * @author Craig Setera
 */
public class APIRegistry {

    // The API definitions keyed by their identifier
    private static Map<String, List<API>> apisByIdentifier;

    // The extension point
    private static final String EXT_API = "api"; //$NON-NLS-1$

    // The API definitions ordered as they are found in the registry
    private static List<API> orderedApis;

    /**
     * Return the API with the specified identifier and version.
     * 
     * @param identifier
     * @param version
     * @return
     */
    public static API getAPI(String identifier, Version version) {
        API api = null;

        try {
            Map<?, ?> byId = getAPIsByIdentifier();
            List<?> apiList = (List<?>) byId.get(identifier);
            Iterator<?> iterator = apiList.iterator();
            while (iterator.hasNext()) {
                API apiTemp = (API) iterator.next();
                if (version.equals(apiTemp.getVersion())) {
                    api = apiTemp;
                    break;
                }
            }
        } catch (IOException e) {
            MTJCorePlugin.log(IStatus.WARNING, e);
        }

        return api;
    }

    /**
     * Return the {@link API} instances in the order that they were found within
     * the registry.
     * 
     * @return
     * @throws IOException
     */
    public static List<API> getAPIDefinitions() throws IOException {
        if (orderedApis == null) {
            orderedApis = readAPIDefinitions();
        }

        return orderedApis;
    }

    /**
     * Return the list of API's present in specified JAR file.
     * 
     * @param jarFile
     * @return
     * @throws IOException
     */
    public static API[] getAPIs(File jarFile) {
        Map<String, API> apis = new HashMap<String, API>();
        JarFile jar = null;

        try {
            jar = new JarFile(jarFile);

            if (jar != null) {

                API api = createAPIFromManifest(jar);
                if (api != null) {
                    apis.put(api.getIdentifier(), api);
                } else {
                    // Could not find the API information in the MANIFEST, now
                    // we try all
                    // registered APIs that match the jar file by primary class
                    // or identifier.
                    addRegisteredAPIs(apis, jar);
                }
            }
        } catch (IOException e) {

        } finally {
            if (jar != null) {
                try {
                    jar.close();
                } catch (IOException e) {
                }
            }
        }

        // Final fall back position
        if (apis.size() == 0) {
            API unknown = createUnknownAPI(jarFile);
            apis.put(unknown.getIdentifier(), unknown);
        }

        // Convert to the final array
        Collection<API> values = apis.values();
        return values.toArray(new API[values.size()]);
    }

    /**
     * Add all registered APIs that match the jar file by primary class or
     * identifier.
     * 
     * @param apis
     * @param jar
     * @throws IOException
     */
    private static void addRegisteredAPIs(Map<String, API> apis, JarFile jar)
            throws IOException {
        String jarApiIdentifier = Utils.EMPTY_STRING;

        Manifest manifest = jar.getManifest();
        if (manifest != null) {
            Attributes attributes = manifest.getMainAttributes();
            jarApiIdentifier = attributes.getValue(APIManifestAttributes.API
                    .toString());
        }

        if (jarApiIdentifier != null) {
            Iterator<?> apiIter = getAPIDefinitions().iterator();
            while (apiIter.hasNext()) {
                API api = (API) apiIter.next();

                // Try to match based on the classes in the jar file
                String primaryClass = api.getPrimaryMatchableClass();
                if (primaryClass != null) {
                    primaryClass = primaryClass.replace('.', '/') + ".class"; //$NON-NLS-1$
                    if (jar.getEntry(primaryClass) != null) {
                        addVersionFilteredAPI(apis, api);
                    }
                }

                // Also look at a match based on API identifier
                if (jarApiIdentifier.equals(api.getIdentifier())) {
                    addVersionFilteredAPI(apis, api);
                }
            }
        }
    }

    /**
     * Add the specified API into the map of API's appropriately handling
     * versioning. When done, we should only have a single API for each
     * identifier and it should be the greatest version number match.
     * 
     * @param apis
     * @param api
     */
    private static void addVersionFilteredAPI(Map<String, API> apis, API api) {
        API currentAPI = apis.get(api.getIdentifier());
        if (currentAPI != null) {
            // We already have this API in our list. If the version
            // being added is a higher version, replace the current.
            if (api.getVersion().compareTo(currentAPI.getVersion()) > 0) {
                apis.put(api.getIdentifier(), api);
            }
        } else {
            // Let's handle MIDP/IMP as a special-case here...
            if (api.getIdentifier().equals("IMP")) { //$NON-NLS-1$
                if (apis.get("MIDP") == null) { //$NON-NLS-1$
                    // Go ahead and add this...
                    apis.put(api.getIdentifier(), api);
                }
            } else if (api.getIdentifier().equals("MIDP")) { //$NON-NLS-1$
                if (apis.get("IMP") != null) { //$NON-NLS-1$
                    apis.remove("IMP"); //$NON-NLS-1$
                }

                apis.put(api.getIdentifier(), api);
            } else {
                apis.put(api.getIdentifier(), api);
            }
        }
    }

    /**
     * Attempt to return an API definition for the information in the manifest.
     * 
     * @param attributes
     * @return
     * @throws IOException
     */
    private static API createAPIFromManifest(JarFile jarFile)
            throws IOException {
        API api = null;

        Manifest manifest = jarFile.getManifest();
        if (manifest != null) {
            Attributes attributes = manifest.getMainAttributes();

            String identifier = attributes.getValue(APIManifestAttributes.API
                    .toString());
            if (identifier != null) {
                api = new API();
                api.setIdentifier(identifier);
                api.setName(attributes.getValue(APIManifestAttributes.API_NAME
                        .toString()));
                api.setType(getTypeFromManifest(attributes));
                api.setVersion(getVersionFromManifest(attributes));
            }
        }

        return api;
    }

    /**
     * Return a new unknown API instance.
     * 
     * @param libraryFile
     * @return
     */
    private static API createUnknownAPI(File libraryFile) {
        API api = new API();
        api.setIdentifier(libraryFile.getName());
        api.setName("Unknown Library"); //$NON-NLS-1$
        api.setType(APIType.UNKNOWN);
        api.setVersion(new Version("1.0")); //$NON-NLS-1$

        return api;
    }

    /**
     * Return the API's keyed by identifier.
     * 
     * @return
     * @throws IOException
     */
    private static Map<String, List<API>> getAPIsByIdentifier()
            throws IOException {
        if (apisByIdentifier == null) {
            apisByIdentifier = new HashMap<String, List<API>>();

            Iterator<?> iterator = getAPIDefinitions().iterator();
            while (iterator.hasNext()) {
                API api = (API) iterator.next();

                List<API> apis = apisByIdentifier.get(api.getIdentifier());
                if (apis == null) {
                    apis = new ArrayList<API>();
                    apisByIdentifier.put(api.getIdentifier(), apis);
                }

                apis.add(api);
            }
        }

        return apisByIdentifier;
    }

    /**
     * Return the integer type of the value specified in the manifest.
     * 
     * @param attributes
     * @return
     */
    private static APIType getTypeFromManifest(Attributes attributes) {
        APIType type = APIType.UNKNOWN;

        String typeString = attributes.getValue(APIManifestAttributes.API_TYPE
                .toString());
        if (typeString == null || typeString.equalsIgnoreCase(APIType.OPTIONAL.toString())) {
            type = APIType.OPTIONAL;
        } else if (typeString.equalsIgnoreCase(APIType.PROFILE.toString())) {
            type = APIType.PROFILE;
        } else if (typeString
                .equalsIgnoreCase(APIType.CONFIGURATION.toString())) {
            type = APIType.CONFIGURATION;
        }

        return type;
    }

    /**
     * Return the version of the API as found in the attributes.
     * 
     * @param attributes
     * @return
     */
    private static Version getVersionFromManifest(Attributes attributes) {
        Version version = null;

        String versionString = attributes
                .getValue(APIManifestAttributes.API_VERSION.toString());
        if (versionString != null) {
            version = new Version(versionString);
        } else {
            version = new Version("0.0.0"); //$NON-NLS-1$
        }

        return version;
    }

    /**
     * Read in the API definitions from the registry.
     * 
     * @return
     * @throws IOException
     */
    private static List<API> readAPIDefinitions() throws IOException {
        List<API> apis = new ArrayList<API>();

        String pluginId = MTJCorePlugin.getDefault().getBundle()
                .getSymbolicName();
        IExtensionRegistry registry = Platform.getExtensionRegistry();

        IConfigurationElement[] elements = registry
                .getConfigurationElementsFor(pluginId, EXT_API);

        for (IConfigurationElement element : elements) {
            apis.add(new API(element));
        }

        return Collections.unmodifiableList(apis);
    }

    // Private constructor for static access.
    private APIRegistry() {
    }
}
