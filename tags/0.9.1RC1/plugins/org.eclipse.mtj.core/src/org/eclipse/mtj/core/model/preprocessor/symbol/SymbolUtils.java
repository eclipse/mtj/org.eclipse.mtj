package org.eclipse.mtj.core.model.preprocessor.symbol;

public class SymbolUtils {
    /**
     * Get symbol value in safe format for preprocessor.
     * 
     * @return
     */
    public static String getSafeSymbolValue(String value) {
        if (value == null || value.trim().length() == 0) {
            value = Boolean.TRUE.toString();
        }
        if (!value.startsWith("\"") && !value.startsWith("'")) {
            value = "'" + value + "'";
        }
        return value;
    }
}
