/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.jmunit;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public interface IJMUnitContants {

    /** JMUnit for CLDC 1.1 Test Case Superclass */
    public static final String JMUNIT_TESTCASE_CLDC11 = "jmunit.framework.cldc11.TestCase"; //$NON-NLS-1$

    /** JMUnit for CLDC 1.0 Test Case Superclass */
    public static final String JMUNIT_TESTCASE_CLDC10 = "jmunit.framework.cldc10.TestCase"; //$NON-NLS-1$

    /** JMUnit for CLDC 1.1 Test Case Superclass */
    public static final String JMUNIT_TESTSUITE_CLDC11 = "jmunit.framework.cldc11.TestSuite"; //$NON-NLS-1$

    /** JMUnit for CLDC 1.0 Test Case Superclass */
    public static final String JMUNIT_TESTSUITE_CLDC10 = "jmunit.framework.cldc10.TestSuite"; //$NON-NLS-1$
    
    public static final String COMMENT_START = "//"; //$NON-NLS-1$

    /** JMUnit Test Suite <b>begin</b> marker */
    public static final String NON_COMMENT_START_MARKER = "$JMUnit-BEGIN$"; //$NON-NLS-1$

    /** JMUnit Test Suite <b>end</b> marker */
    public static final String NON_COMMENT_END_MARKER = "$JMUnit-END$"; //$NON-NLS-1$

    /** The string used to mark the end of the generated test suite code */
    public static final String END_MARKER = COMMENT_START  + NON_COMMENT_END_MARKER;

    /** The string used to mark the beginning of the generated test suite code */
    public static final String START_MARKER = COMMENT_START + NON_COMMENT_START_MARKER;

}
