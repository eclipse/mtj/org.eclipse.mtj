/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.jmunit.core.api;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.internal.jmunit.IJMUnitContants;
import org.eclipse.osgi.util.NLS;

/**
 * TestSuiteWriter Class writes test suites.
 */
public class TestSuiteWriter extends AbstractTestWriter {

    private String suiteName;

    /**
     * Creates a test suite writer.
     * 
     * @param _type target type.
     * @param _name suite name.
     * @throws JavaModelException Any java model error.
     */
    public TestSuiteWriter(IType _type, String _name) throws JavaModelException {
        super(_type);
        this.suiteName = _name;
    }

    /**
     * Writes the test suite code adding the specified classes to the suite.
     * 
     * @param _testCases test case classes.
     * @param _monitor active monitor.
     * @throws JavaModelException Any java model error.
     */
    public void writeCode(String[] _testCases, IProgressMonitor _monitor)
            throws JavaModelException {
        this.writeConstructor(_monitor);
        this.writeSuiteSetup(_testCases, _monitor);
    }

    /**
     * Writes the TestSuite constructor.
     * 
     * @param _monitor active monitor.
     * @return the constructor method.
     * @throws JavaModelException Any java model error.
     */
    private IMethod writeConstructor(IProgressMonitor _monitor)
            throws JavaModelException {
        StringBuffer buffer = new StringBuffer();

        String[] comment = { "TestSuite Class constructor initializes the test suite." }; //$NON-NLS-1$

        this.writeMethodComment(buffer, comment, delimiter);

        String declaration = NLS.bind("public {0}()", type.getElementName()); //$NON-NLS-1$
        String[] body = { NLS.bind("super(\"{0}\");", this.suiteName), //$NON-NLS-1$
                "this.setupSuite();" }; //$NON-NLS-1$

        this.writeMethodDeclaration(buffer, declaration, body, delimiter);

        return this.type.createMethod(buffer.toString(), null, false, _monitor);
    }

    /**
     * Writes the setup method adding the specified classes to the suite.
     * 
     * @param _testCases TestCase class names.
     * @param _monitor active monitor.
     * @return the constructor method.
     * @throws JavaModelException Any java model error.
     */
    private IMethod writeSuiteSetup(String[] _testCases,
            IProgressMonitor _monitor) throws JavaModelException {
        StringBuffer buffer = new StringBuffer();

        String[] comment = { "This method adds all suite test cases to be run." }; //$NON-NLS-1$

        this.writeMethodComment(buffer, comment, delimiter);

        String declaration = "private void setupSuite()"; //$NON-NLS-1$

        List<String> body = new LinkedList<String>();

        body.add(IJMUnitContants.START_MARKER);
        for (String testCase : _testCases) {
            body.add(NLS.bind("add(new {0}());", testCase)); //$NON-NLS-1$
        }
        body.add(IJMUnitContants.END_MARKER);
        this.writeMethodDeclaration(buffer, declaration, body
                .toArray(new String[body.size()]), delimiter);

        return this.type.createMethod(buffer.toString(), null, false, _monitor);
    }

}
