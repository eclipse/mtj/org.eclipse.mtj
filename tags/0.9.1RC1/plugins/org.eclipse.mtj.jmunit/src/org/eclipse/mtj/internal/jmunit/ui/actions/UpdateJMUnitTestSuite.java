package org.eclipse.mtj.internal.jmunit.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class has been added as part of a work in
 * progress. There is no guarantee that this API will work or that it will
 * remain the same. Please do not use this API without consulting with the MTJ
 * team.
 * </p>
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class UpdateJMUnitTestSuite implements IObjectActionDelegate {

    public UpdateJMUnitTestSuite() {
        super();
    }

    public void setActivePart(IAction action, IWorkbenchPart targetPart) {
        // TODO Auto-generated method stub

    }

    public void run(IAction action) {
        // TODO Auto-generated method stub

    }

    public void selectionChanged(IAction action, ISelection selection) {
        // TODO Auto-generated method stub

    }

}
