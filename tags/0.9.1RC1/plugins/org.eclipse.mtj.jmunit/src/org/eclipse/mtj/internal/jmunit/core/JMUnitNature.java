/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     
 * @since 0.9.1
 */
package org.eclipse.mtj.internal.jmunit.core;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.nature.AbstractNature;

/**
 * JMUnitNature class defines a nature behavior of having a JMUnit library on
 * the class path and the ability of exporting the library dependencies into the
 * application.
 * 
 * @author David Marques
 * @since 0.9.1
 */
public class JMUnitNature extends AbstractNature {

    private static final String JMUNIT = "JMUnit";

    /**
     * @see org.eclipse.core.resources.IProjectNature#configure()
     */
    public void configure() throws CoreException {
        IJavaProject javaProject = JavaCore.create(getProject());
        List<IClasspathEntry> oldEntries = new LinkedList<IClasspathEntry>(
                Arrays.asList(javaProject.getRawClasspath()));
        List<IClasspathEntry> newEntries = new LinkedList<IClasspathEntry>();

        for (IClasspathEntry entry : oldEntries) {
            boolean changed = false;
            switch (entry.getEntryKind()) {
                case IClasspathEntry.CPE_CONTAINER:
                    if (entry.getPath().toString().contains(JMUNIT)) {
                        newEntries.add(JavaCore.newContainerEntry(entry
                                .getPath(), true));
                        changed = true;
                    }
                    break;
                case IClasspathEntry.CPE_SOURCE:
                    newEntries.add(JavaCore.newSourceEntry(entry.getPath()));
                    changed = true;
                    break;
            }
            if (!changed) {
                newEntries.add(entry);
            }
        }

        IClasspathEntry[] array = newEntries
                .toArray(new IClasspathEntry[oldEntries.size()]);
        if (!javaProject.hasClasspathCycle(array)) {
            javaProject.setRawClasspath(null, new NullProgressMonitor());
            javaProject.setRawClasspath(array, new NullProgressMonitor());
        }
    }

    /**
     * @see org.eclipse.core.resources.IProjectNature#deconfigure()
     */
    public void deconfigure() throws CoreException {
        IJavaProject javaProject = JavaCore.create(getProject());
        List<IClasspathEntry> oldEntries = new LinkedList<IClasspathEntry>(
                Arrays.asList(javaProject.getRawClasspath()));
        List<IClasspathEntry> newEntries = new LinkedList<IClasspathEntry>();

        for (IClasspathEntry entry : oldEntries) {
            boolean changed = false;
            switch (entry.getEntryKind()) {
                case IClasspathEntry.CPE_CONTAINER:
                    if (entry.getPath().toString().contains(JMUNIT)) {
                        newEntries.add(JavaCore.newContainerEntry(entry
                                .getPath(), false));
                        changed = true;
                    }
                    break;
                case IClasspathEntry.CPE_SOURCE:
                    newEntries.add(JavaCore.newSourceEntry(entry.getPath(),
                            new IPath[] { new Path("**/*Test.java"),
                                    new Path("**/*TestSuite.java") }));
                    changed = true;
                    break;
            }
            if (!changed) {
                newEntries.add(entry);
            }
        }

        IClasspathEntry[] array = newEntries
                .toArray(new IClasspathEntry[oldEntries.size()]);
        if (!javaProject.hasClasspathCycle(array)) {
            javaProject.setRawClasspath(null, new NullProgressMonitor());
            javaProject.setRawClasspath(array, new NullProgressMonitor());
        }
    }

}
