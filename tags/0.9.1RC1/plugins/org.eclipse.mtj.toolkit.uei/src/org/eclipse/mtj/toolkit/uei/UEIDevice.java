/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Feng Wang (Sybase) - 1. Modify to fit for AbstractDevice#copyForLaunch(...)
 *                                   signature changing.
 *                          2. Replace ILaunchConstants.EMULATED_CLASS with
 *                             IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME
 *                             , to take advantage of JDT launch configuration 
 *                             refactoring participates.
 *     Diego Sandin (Motorola)  - Added a new constructor.
 */
package org.eclipse.mtj.toolkit.uei;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.mtj.core.importer.JavadocDetector;
import org.eclipse.mtj.core.importer.LibraryImporter;
import org.eclipse.mtj.core.importer.JavadocDetector.GenericLocalFSSearch;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.launching.ILaunchConstants;
import org.eclipse.mtj.core.model.Classpath;
import org.eclipse.mtj.core.model.LaunchEnvironment;
import org.eclipse.mtj.core.model.ReplaceableParametersProcessor;
import org.eclipse.mtj.core.model.device.IDevice2;
import org.eclipse.mtj.core.model.device.impl.AbstractDevice;
import org.eclipse.mtj.core.model.device.launch.properties.LaunchTemplateProperties;
import org.eclipse.mtj.core.model.preverifier.IPreverifier;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.toolkit.uei.internal.properties.UEIDeviceDefinition;
import org.eclipse.mtj.toolkit.uei.model.EmulatorInfoArgs;
import org.eclipse.mtj.toolkit.uei.model.properties.DeviceSpecificProperties;

/**
 * Unified Emulator Interface (UEI) implementation of the IDevice interface.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class has been added as part of a work in
 * progress. There is no guarantee that this API will work or that it will
 * remain the same. Please do not use this API without consulting with the MTJ
 * team.
 * </p>
 * 
 * @author Craig Setera
 */
public class UEIDevice extends AbstractDevice implements IDevice2 {

    /**
     * Creates a new UEIDevice
     */
    public UEIDevice() {
    }

    /**
     * Create a new instance of UEIDevice based on the properties for that
     * device found with the
     * <code>emulator {@link EmulatorInfoArgs#XQUERY -Xquery}</code> command.
     * 
     * @param name the device name.
     * @param groupName the device group name.
     * @param description the device description.
     * @param properties the properties for the device.
     * @param definition information on how the UEI emulator will be launched.
     * @param emulatorExecutable the emulator application.
     * @param preverifier the preverifier application.
     * @throws IllegalArgumentException if the properties or definition are
     *             <code>null</code>.
     */
    public UEIDevice(String name, String groupName, String description,
            Properties properties, UEIDeviceDefinition definition,
            File emulatorExecutable, IPreverifier preverifier)
            throws IllegalArgumentException {

        if (properties == null) {
            throw new IllegalArgumentException(
                    Messages.UEIDevice_null_properties);
        } else if (definition == null) {
            throw new IllegalArgumentException(
                    Messages.UEIDevice_null_definition);
        }

        this.setName(name);
        this.setDescription(description);
        this.setGroupName(groupName);

        this.setDeviceProperties(properties);
        this.setClasspath(getBootClasspath(properties));
        this.setProtectionDomains(getProtectionDomains(properties));

        this.setExecutable(emulatorExecutable);
        this.setPreverifier(preverifier);

        this.setDebugServer(definition.isDebugServer());
        this.setLaunchCommandTemplate(definition.getLaunchTemplate());

        this.setBundle(UeiPlugin.getDefault().getBundle().getSymbolicName());
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals = false;

        if (obj instanceof UEIDevice) {
            equals = equals((UEIDevice) obj);
        }

        return equals;
    }

    /**
     * Test equality on a UEI device and return a boolean indicating equality.
     * 
     * @param device the reference device with which to compare.
     * @return <code>true</code> if this object is the same as the device
     *         argument; <code>false</code> otherwise.
     */
    public boolean equals(UEIDevice device) {
        return super.equals(device)
                && launchCommandTemplate.equals(device.launchCommandTemplate);
    }

    /**
     * Import the device's classpath and return it.
     * 
     * @param deviceProperties
     * @return
     */
    private Classpath getBootClasspath(Properties deviceProperties) {

        Classpath classpath = new Classpath();
        String classpathString = deviceProperties.getProperty(
                DeviceSpecificProperties.BOOTCLASSPATH.toString(),
                Utils.EMPTY_STRING);

        String[] classpathEntries = classpathString.split(","); //$NON-NLS-1$

        // create a new javadoc detector which will search the
        // javadoc of the library automatically
        JavadocDetector javadocDetector = new JavadocDetector()
                .addJavadocSearchStrategy(new GenericLocalFSSearch());
        LibraryImporter libraryImporter = new LibraryImporter();
        libraryImporter.setJavadocDetector(javadocDetector);

        for (String element : classpathEntries) {
            File libraryFile = new File(element);
            if (libraryFile.exists()) {
                classpath.addEntry(libraryImporter
                        .createLibraryFor(libraryFile));
            }
        }

        return classpath;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.core.model.device.IDevice#getLaunchCommand(org.eclipse
     * .mtj.core.model.LaunchEnvironment,
     * org.eclipse.core.runtime.IProgressMonitor)
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException {

        boolean isWin32 = Platform.getOS().equals(Platform.OS_WIN32);

        IMidletSuiteProject midletSuite = launchEnvironment.getMidletSuite();

        ILaunchConfiguration launchConfiguration = launchEnvironment
                .getLaunchConfiguration();

        boolean launchFromJAD = shouldDirectLaunchJAD(launchConfiguration);

        // Copy the things we need for launch to a temporary location to avoid
        // locking files
        File deployedTemp = copyForLaunch(midletSuite, monitor, launchFromJAD);

        // Fill in our properties so we can use them for resolution
        // against the raw executable command line
        Map<String, String> executionProperties = new HashMap<String, String>();
        executionProperties.put(LaunchTemplateProperties.EXECUTABLE.toString(),
                isWin32 ? executable.getName() : executable.getAbsolutePath());

        executionProperties.put(LaunchTemplateProperties.DEVICE.toString(),
                getName());

        // Debug information
        if (launchEnvironment.isDebugLaunch()) {
            executionProperties.put(LaunchTemplateProperties.DEBUGPORT
                    .toString(), Integer.toString(launchEnvironment
                    .getDebugListenerPort()));
        }

        // Classpath
        if (!launchFromJAD) {
            String classpathString = getProjectClasspathString(
                    launchEnvironment.getMidletSuite(), deployedTemp, monitor);
            executionProperties.put(LaunchTemplateProperties.CLASSPATH
                    .toString(), classpathString);
        }

        // Add launch configuration values
        addLaunchConfigurationValue(executionProperties,
                LaunchTemplateProperties.VERBOSE.toString(),
                launchConfiguration, ILaunchConstants.VERBOSITY_OPTIONS);

        addLaunchConfigurationValue(executionProperties,
                LaunchTemplateProperties.HEAPSIZE.toString(),
                launchConfiguration, ILaunchConstants.HEAP_SIZE);

        String securityDomainName = launchConfiguration.getAttribute(
                ILaunchConstants.SECURITY_DOMAIN,
                ILaunchConstants.NO_SECURITY_DOMAIN);
        if (!securityDomainName.equals(ILaunchConstants.NO_SECURITY_DOMAIN)) {
            executionProperties.put(LaunchTemplateProperties.SECURITYDOMAIN
                    .toString(), securityDomainName);
        }

        String extraArguments = launchConfiguration.getAttribute(
                ILaunchConstants.LAUNCH_PARAMS, Utils.EMPTY_STRING);
        executionProperties.put(LaunchTemplateProperties.USERSPECIFIEDARGUMENTS
                .toString(), extraArguments);

        if (launchFromJAD) {
            executionProperties.put(
                    LaunchTemplateProperties.JADFILE.toString(),
                    getSpecifiedJadURL(launchConfiguration));
        } else if (shouldDoOTA(launchConfiguration)) {
            String url = getOTAURL(launchConfiguration, midletSuite);
            executionProperties.put(LaunchTemplateProperties.OTAURL.toString(),
                    url);
        } else {
            File jadFile = getJadForLaunch(midletSuite, deployedTemp, monitor);
            if (jadFile.exists()) {
                executionProperties.put(LaunchTemplateProperties.JADFILE
                        .toString(), jadFile.toString());
            }

            addLaunchConfigurationValue(executionProperties,
                    LaunchTemplateProperties.TARGET.toString(),
                    launchConfiguration,
                    IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME);
        }

        // Do the property resolution given the previous information
        String baseCommand = ReplaceableParametersProcessor
                .processReplaceableValues(launchCommandTemplate,
                        executionProperties);

        return isWin32 ? "cmd /c " + baseCommand : baseCommand; //$NON-NLS-1$
    }

    /**
     * Return the protection domains specified for this device.
     * 
     * @param deviceProperties
     * @return
     */
    private String[] getProtectionDomains(Properties deviceProperties) {
        String domainsString = deviceProperties.getProperty(
                DeviceSpecificProperties.SECURITY_DOMAINS.toString(),
                Utils.EMPTY_STRING);
        return domainsString.split(","); //$NON-NLS-1$
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.core.model.device.IDevice2#getWorkingDirectory()
     */
    public File getWorkingDirectory() {
        return executable.getParentFile();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return classpath.hashCode() ^ executable.hashCode() ^ name.hashCode()
                ^ launchCommandTemplate.hashCode() ^ groupName.hashCode();
    }
}
