/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.toolkit.uei;

import org.eclipse.osgi.util.NLS;

/**
 * Provides convenience methods for manipulating messages.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class Messages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.toolkit.uei.messages"; //$NON-NLS-1$

    public static String DeviceDefinitionManager_failed_reading_file;

    public static String DeviceDefinitionManager_malformed_unicode_esc_seq;

    public static String DeviceDefinitionManager_readDeviceDefinitions_closeStream_fail;

    public static String DeviceDefinitionManager_readDeviceDefinitions_invalid_file;

    public static String UEIDevice_null_definition;

    public static String UEIDevice_null_properties;

    public static String UEIDeviceDefinition_deviceName_null;

    public static String UEIDeviceDefinition_invalid_match_expression;

    public static String UEIDeviceDefinition_properties_null;

    public static String UEIDeviceImporter_addUEIDevices_begin;

    public static String UEIDeviceImporter_addUEIDevices_devices;

    public static String UEIDeviceImporter_addUEIDevices_end;

    public static String UEIDeviceImporter_createDevice;

    public static String UEIDeviceImporter_getMatchingDevices_begin;

    public static String UEIDeviceImporter_getMatchingDevices_device_def;

    public static String UEIDeviceImporter_getMatchingDevices_emulator;

    public static String UEIDeviceImporter_getMatchingDevices_end;

    public static String UEIDeviceImporter_getMatchingDevices_failed;

    public static String UEIDeviceImporter_getMatchingDevices_invalid_props;

    public static String UEIDeviceImporter_getUEIDevicesPropertiesStream_IllegalState;

    public static String UEIDeviceImporter_getUEIDevicesPropertiesStream_file_not_found;

    public static String UEIDeviceImporter_getUEIDevicesPropertiesStream_IOError;

    public static String UEIDeviceImporter_readAccessDenied;

    public static String UeiPlugin_start_message;

    public static String UeiPlugin_ueiDebug_tag;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
