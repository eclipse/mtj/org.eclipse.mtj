/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.toolkit.uei.model;

/**
 * The emulator command support several arguments that provide information about
 * the emulator itself.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public enum EmulatorInfoArgs {

    /**
     * Display version information about the emulator. The first line is the
     * emulator name and version. Subsequent lines are key and value pairs
     * separated by a single colon and a single space. The valid keys and their
     * values are as follows:
     * <ul>
     * <li><b>Configuration</b>: Configuration-Name[-Version]</li>
     * <li><b>Profile</b>: Profile-Name[-Version]</li>
     * <li><b>Optional</b>: Optional-API-Names</li>
     * </ul>
     */
    VERSION("-version"), //$NON-NLS-1$

    /**
     * Print emulator device information to the standard output and immediately
     * exit. Printed information includes, but is not limited to, device names,
     * device screen size, and other device capabilities.
     */
    XQUERY("-Xquery"); //$NON-NLS-1$

    /**
     * The command string represented by this enum constant.
     */
    private String key;

    /**
     * Creates a new EmulatorInfoArgs.
     * 
     * @param key the command string to be represented by this enum constant.
     */
    EmulatorInfoArgs(String key) {
        this.key = key;
    }

    /**
     * Returns the command string represented by this enum constant.
     * 
     * @return the command represented by this enum constant
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return key;
    }

}
