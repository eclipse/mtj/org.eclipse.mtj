/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.toolkit.uei.model.properties;

/**
 * When the emulator runs with the -Xquery option, information describing the
 * emulator and its capabilities are sent to standard output. The general format
 * is that of a properties file.
 * <p>
 * The properties describing <i>information</i> for a single devices are
 * represented by this enum.
 * </p>
 * <p>
 * The device-specific properties start with the device name. The device name
 * must be one of the values from the {@link DevicesProperties#DEVICE_LIST
 * device.list} property.
 * </p>
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public enum DeviceSpecificProperties {

    /**
     * <b>Property Key</b>: <code>apis</code><br>
     * <b>Value</b>: Contains the same information, in the same format, as
     * {@link #BOOTCLASSPATH device-name.bootclasspath} and also includes any
     * earlier versions of APIs supported by this device. This enables an IDE to
     * select which versions of the API to use to build an application. If this
     * property is not present, its value is the same as {@link #BOOTCLASSPATH
     * device-name.bootclasspath}.
     */
    APIS("apis", UEIImplementationReq.OPTIONAL), //$NON-NLS-1$

    /**
     * <b>Property Key</b>: <code>bootclasspath</code><br>
     * <b>Value</b>: A list of API files that must be used to build a MIDlet
     * that runs on this device.
     */
    BOOTCLASSPATH("bootclasspath", UEIImplementationReq.REQUIRED), //$NON-NLS-1$

    /**
     * <b>Property Key</b>: <code>description</code><br>
     * <b>Value</b>: The device description.
     */
    DESCRIPTION("description", UEIImplementationReq.OPTIONAL), //$NON-NLS-1$

    /**
     * <b>Property Key</b>: <code>screen.bitDepth</code><br>
     * <b>Value</b>: The number of bits that describe a pixel's color. For
     * grayscale screens, this property describes how many bits are used to
     * determine the level of gray.
     */
    SCREEN_BITDEPTH("screen.bitDepth", UEIImplementationReq.REQUIRED), //$NON-NLS-1$

    /**
     * <b>Property Key</b>: <code>screen.height</code><br>
     * <b>Value</b>: The height of the device screen in pixels.
     */
    SCREEN_HEIGHT("screen.height", UEIImplementationReq.REQUIRED), //$NON-NLS-1$

    /**
     * <b>Property Key</b>: <code>screen.isColor</code><br>
     * <b>Value</b>: {@code true} for color screens, {@code false} for
     * grayscale.
     */
    SCREEN_ISCOLOR("screen.isColor", UEIImplementationReq.REQUIRED), //$NON-NLS-1$

    /**
     * <b>Property Key</b>: <code>screen.isTouch</code><br>
     * <b>Value</b>: {@code true} for screens that support pointer events,
     * {@code false} otherwise.
     */
    SCREEN_ISTOUCH("screen.isTouch", UEIImplementationReq.REQUIRED), //$NON-NLS-1$

    /**
     * <b>Property Key</b>: <code>screen.width</code><br>
     * <b>Value</b>: The width of the device screen in pixels.
     */
    SCREEN_WIDTH("screen.width", UEIImplementationReq.REQUIRED), //$NON-NLS-1$

    /**
     * <b>Property Key</b>: <code>security.domains</code><br>
     * <b>Value</b>: Has the same meaning and syntax as
     * {@link DevicesProperties#SECURITY_DOMAINS security.domains}, but it
     * applies to a specific device and overrides the value of
     * {@link DevicesProperties#SECURITY_DOMAINS security.domains}.
     */
    SECURITY_DOMAINS("security.domains", UEIImplementationReq.OPTIONAL), //$NON-NLS-1$

    /**
     * <b>Property Key</b>: <code>uei.arguments</code><br>
     * <b>Value</b>: Has the same meaning and syntax as
     * {@link DevicesProperties#UEI_ARGUMENTS uei.arguments}, but it applies to
     * a specific device and overrides the value of
     * {@link DevicesProperties#UEI_ARGUMENTS uei.arguments}.
     */
    UEI_ARGUMENTS("uei.arguments", UEIImplementationReq.OPTIONAL), //$NON-NLS-1$

    /**
     * <b>Property Key</b>: <code>version.configuration</code><br>
     * <b>Value</b>: The configuration supported by this device. A device can
     * support only one configuration. Emulators that support multiple
     * configurations can do so by providing separate devices for each.
     * Acceptable configuration names are:
     * <ul>
     * <li>CDLC-1.0</li>
     * <li>CDLC-1.1</li>
     * <li>CDC-1.0</li>
     * <li>CDC-1.1</li>
     * </ul>
     */
    VERSION_CONFIGURATION("version.configuration", //$NON-NLS-1$
            UEIImplementationReq.OPTIONAL),

    /**
     * <b>Property Key</b>: <code>version.profile</code><br>
     * <b>Value</b>: A list of one or more profiles supported by this device.
     * Multiple items are separated by commas. Acceptable profile names are:
     * <ul>
     * <li>MIDP-1.0</li>
     * <li>MIDP-2.0</li>
     * <li>IMP-1.0</li>
     * <li>IMP-NG</li>
     * </ul>
     */
    VERSION_PROFILE("version.profile", UEIImplementationReq.OPTIONAL); //$NON-NLS-1$

    /**
     * Defines the implementation Requirement for each property.
     */
    private UEIImplementationReq implementationRequirement;

    /**
     * A property key.
     */
    private String propertyKey;

    /**
     * Creates a new DeviceSpecificProperties instance.
     * 
     * @param propertyKey the property key
     * @param impl the implementation requirement for this property.
     */
    private DeviceSpecificProperties(String propertyKey,
            UEIImplementationReq req) {
        this.propertyKey = propertyKey;
        this.implementationRequirement = req;
    }

    /**
     * Return the device-specific properties starting with the device name.
     * 
     * @param deviceName the device name. The device name must be one of the
     *            values from the {@link DevicesProperties#DEVICE_LIST
     *            device.list} property.
     * @return the device-specific properties.
     */
    public String getDeviceSpecificProperties(final String deviceName) {
        if (deviceName == null) {
            throw new IllegalArgumentException("Device name must not be null."); //$NON-NLS-1$
        }
        return deviceName + "." + propertyKey; //$NON-NLS-1$
    }

    /**
     * Return the implementation requirement for this property.
     * 
     * @return {@link UEIImplementationReq#REQUIRED} if this property must be
     *         implemented or {@link UEIImplementationReq#OPTIONAL} if not.
     */
    public UEIImplementationReq getImplementationRequirement() {
        return implementationRequirement;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return propertyKey;
    }
}
