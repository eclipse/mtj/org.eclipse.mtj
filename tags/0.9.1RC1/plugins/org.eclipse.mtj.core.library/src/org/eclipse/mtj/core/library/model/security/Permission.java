/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.core.library.model.security;

/**
 * This Class represents a named permission defined by an API or function to
 * prevent it from being used without authorization
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class Permission {

    /**
     * named permission defined by an API or function
     */
    private final String permissionID;

    /**
     * Creates a new Permission.
     * 
     * @param permission a named permission defined by an API or function.
     */
    public Permission(final String permission) {
        this.permissionID = permission;
    }

    /**
     * Returns the named permission for this instance.
     * 
     * @return the permissionID the named permission.
     */
    public String getPermissionID() {
        return permissionID;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((permissionID == null) ? 0 : permissionID.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Permission)) {
            return false;
        }
        Permission other = (Permission) obj;
        if (permissionID == null) {
            if (other.permissionID != null) {
                return false;
            }
        } else if (!permissionID.equals(other.permissionID)) {
            return false;
        }
        return true;
    }

}
