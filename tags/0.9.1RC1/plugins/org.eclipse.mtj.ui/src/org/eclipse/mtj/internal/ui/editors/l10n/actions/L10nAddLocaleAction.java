package org.eclipse.mtj.internal.ui.editors.l10n.actions;

import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.util.MTJLabelUtility;

/**
 * An add Locale Action.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class L10nAddLocaleAction extends L10nAddObjectAction {

    /**
     * Creates a new L10n add locale action.
     */
    public L10nAddLocaleAction() {
        setText(MTJUIMessages.L10nAddLocaleAction_text);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {

        if (parentObject != null) {

            // Create a new locale object
            L10nLocale locale = parentObject.getModel().getFactory()
                    .createL10nLocale();

            // Generate the name for the locale
            String localeName = MTJLabelUtility.generateName(getChildNames(),
                    "lc-C"); //$NON-NLS-1$

            locale.setLocaleName(localeName);

            // Add the new locale to the parent Locales object
            addChild(locale);
        }
    }
}
