/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 */
package org.eclipse.mtj.internal.ui;

import org.eclipse.osgi.util.NLS;

/**
 * MTJUIMessages contains all messages that can be localized babel.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class MTJUIMessages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.internal.ui.messages"; //$NON-NLS-1$

    public static String EditorPreferencePage_description;
    public static String EditorPreferencePage_folding_label;
    public static String EditorPreferencePage_generalTextEditor_link;
    public static String EditorPreferencePage_localization_tab_title;
    public static String EnableLocalizationAction_dialog_title;
    public static String FormatAction_text;
    public static String HyperlinkAction_text;
    public static String InputContext_doSaveAs_fileNotExist;
    public static String InputContext_saveAs_noLocation;
    public static String InputContextManager_saveAsInputContextNull;
    public static String InputContextManager_updateInputContextNull;
    public static String MissingResourcePage_editor_failed;
    public static String MissingResourcePage_message;
    public static String MissingResourcePage_resource_unavailable;
    public static String MissingResourcePage_title;
    public static String MTJFormEditor_doSaveAs_failed;
    public static String MTJFormEditorContributor_copyAction_text;
    public static String MTJFormEditorContributor_cutAction_text;
    public static String MTJFormEditorContributor_pasteAction_text;
    public static String MTJFormEditorContributor_revertAction_text;
    public static String MTJFormEditorContributor_saveAction_text;
    public static String MTJFormPage_detailsSection_title;
    public static String MTJFormPage_helpAction_toolTipText;
    public static String MTJFormPage_messageSection_title;
    public static String MTJMultiPageContentOutline_description;
    public static String MTJMultiPageContentOutline_sortingAction_text;
    public static String MTJMultiPageContentOutline_toolTipText;
    public static String MTJSourcePage_quickOutlineAction_text;
    public static String ToggleLinkWithEditorAction_description;
    public static String ToggleLinkWithEditorAction_name;
    public static String ToggleLinkWithEditorAction_toolTipText;
    public static String XMLSourcePage_cantLeaveThePage;

    public static String XMLSyntaxColorTab_comments_label;
    public static String XMLSyntaxColorTab_constants_label;
    public static String XMLSyntaxColorTab_process_label;
    public static String XMLSyntaxColorTab_tags_label;
    public static String XMLSyntaxColorTab_text_label;
    public static String L10nAddLocaleAction_text;
    public static String L10nAddLocaleEntryAction_initialKey;
    public static String L10nAddLocaleEntryAction_initialValue;
    public static String L10nAddLocaleEntryAction_text;
    public static String L10nEntryDetails_detailsDescription;
    public static String L10nEntryDetails_detailsTitle;
    public static String L10nEntryDetails_keyEntry_label;
    public static String L10nEntryDetails_keyWidgetLabel;
    public static String L10nEntryDetails_valueEntry_label;
    public static String L10nEntryDetails_valueWidget_label;
    public static String L10nLocaleDetails_detailsDescription;
    public static String L10nLocaleDetails_detailsTitle;
    public static String L10nLocaleDetails_localeNameEntry_label;
    public static String L10nLocalesDetails_browse_label;
    public static String L10nLocalesDetails_detailsTitle;
    public static String L10nLocalesDetails_locationEntry_label;
    public static String L10nLocalesDetails_locationEntryBrowseDialog_message;
    public static String L10nLocalesDetails_locationEntryBrowseDialog_title;
    public static String L10nLocalesDetails_locationWidget_label;
    public static String L10nLocalesDetails_packageEntry_label;
    public static String L10nLocalesDetails_packageEntryBrowseDialog_message;
    public static String L10nLocalesDetails_packageEntryBrowseDialog_title;
    public static String L10nLocalesDetails_packageWidget_label;
    public static String L10nRemoveObjectAction_text;
    public static String L10nSourcePage_source_partName;
    public static String LocalesTreeSection_addEntry_button_label;
    public static String LocalesTreeSection_addLocale_button_label;
    public static String LocalesTreeSection_collapseAllAction_Text;
    public static String LocalesTreeSection_description;
    public static String LocalesTreeSection_down_button_label;
    public static String LocalesTreeSection_new_submenu_text;
    public static String LocalesTreeSection_remove_button_label;
    public static String LocalesTreeSection_showInLabel;
    public static String LocalesTreeSection_title;
    public static String LocalesTreeSection_up_button_label;
    public static String LocalizationBlock_Browse;
    public static String LocalizationBlock_DestinationFolder;
    public static String LocalizationBlock_DestinationFolderMessage;
    public static String LocalizationBlock_DestinationFolderTitle;
    public static String LocalizationBlock_PackageDialogMessage;
    public static String LocalizationBlock_PackageDialogTitle;
    public static String LocalizationBlock_PackageName;
    public static String LocalizationPage_formErrorContent_message;
    public static String LocalizationPage_formErrorContent_title;
    public static String LocalizationPage_LocalizationPage;
    public static String LocalizationPage_PageDescription;
    public static String LocalizationPage_PageTitle;

    public static String LocalizationPage_text;
    public static String LocalizationPage_title;
    public static String LocalizationWizard_failed_createLocalizationFile;

    public static String LocalizationWizard_window_title;
    public static String QuickOutlinePopupDialog_infoText;
    public static String QuickOutlinePopupDialog_sortActionText;

    public static String SyntaxColorTab_bold_label;
    public static String SyntaxColorTab_color_label;
    public static String SyntaxColorTab_elements_label;
    public static String SyntaxColorTab_italic_label;
    public static String SyntaxColorTab_preview_label;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, MTJUIMessages.class);
    }

    private MTJUIMessages() {
    }
}
