/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.ui.internal.wizards.projects;

import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.ui.internal.wizards.libraries.LibrarySelectionBlock;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * An implementation of a wizard page responsible for selection of MIDlet Suite
 * libraries.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class NewMidletProjectWizardPageLibrary extends WizardPage {

    LibrarySelectionBlock block;

    /**
     * Indicates if the page is displayable
     */
    private boolean pageDisplayable = false;

    /**
     * The name of the page.
     */
    private static final String PAGE_NAME = "NewMidletProjectWizardPageLibrary"; //$NON-NLS-1$

    /**
     * Creates a new NewMidletProjectWizardPageLibrary page
     */
    public NewMidletProjectWizardPageLibrary() {
        super(PAGE_NAME);

        setTitle(Messages.NewMidletProjectWizardPageLibrary_title);
        setDescription(Messages.NewMidletProjectWizardPageLibrary_description);

        block = new LibrarySelectionBlock();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setFont(parent.getFont());
        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        Control control = block.createControl(composite);
        control.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        Dialog.applyDialogFont(composite);
        setControl(composite);
    }

    /**
     * Indicates if the NewMidletProjectWizardPageLibrary page may be displayed
     * in the new project wizard.
     * 
     * @return <code>true</code> if at least one MIDlet Suit Library is
     *         available or <code>false</code> otherwise.
     */
    public boolean isPageDisplayable() {

        if (block != null) {
            pageDisplayable = block.isLibrariesAvailable();
        }
        return pageDisplayable;
    }

    /**
     * @return
     */
    public List<IPath> getSelectedLibraries() {
        return block.getSelectedLibrariesPathEntries();
    }
}
