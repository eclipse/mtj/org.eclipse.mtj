/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;

/**
 * Provides context for preprocessing
 * 
 * @author gma
 * @since 0.9.1
 */
public class PreprocessContext {
    private static final IPreprocessPrefixHandler DEFAULT_PREFIX_HANDLER = new AntennaPPPrefixHandler();
    private static final IPreprocessDirectiveProvider DEFAULT_DIRECTIVE_PROVIDER = new AntennaPPDirectiveProvider();
    public static final char COMMON_SEPARATOR = ' ';
    private static final int STATAMENT_DIRECTIVE_UNCOMPLETE = 0;
    private static final int STATAMENT_DIRECTIVE_COMPLETE = 1;

    private IPreprocessPrefixHandler prefixHandler = DEFAULT_PREFIX_HANDLER;
    private IPreprocessDirectiveProvider directiveProvider = DEFAULT_DIRECTIVE_PROVIDER;

    private IDocument document;
    private int fOffset;

    private IProject containedProject;

    private boolean validEnv = false;
    private int fLineStartOffset = -1;
    private int fStatementStartOffset = -1;
    private int fCompletionOffset = -1;

    private int currStatementState;

    /**
     * @param document the processing document
     * @param offset content assist invocation offset
     * @param containedProject the project the processing file contains
     */
    public PreprocessContext(IDocument document, int offset,
            IProject containedProject) {
        this.document = document;
        this.fOffset = offset;
        this.containedProject = containedProject;
        initialize();
    }

    /**
     * @param dLevel the preprocess debug level
     * @return if the debug level can be accepted in the context, return ture,
     *         else return false.
     */
    public boolean accept(PreprocessDebugLevel dLevel) {
        return prefixMatch(dLevel);
    }

    /**
     * @param directive the preprocess directive
     * @return if the directive can be accepted in the context, return ture,
     *         else return false.
     */
    public boolean accept(PreprocessDirective directive) {
        if(!prefixMatch(directive))
            return false;
        return sytaxAcceptable(directive);
    }

    

    /**
     * @param symbol the preprocess symbol
     * @return if the symbol can be accepted in the context, return ture, else
     *         return false.
     */
    public boolean accept(PreprocessSymbol symbol) {
        return prefixMatch(symbol);
    }
    // judge whether the directive can be accepted in syntax environment
    private boolean sytaxAcceptable(PreprocessDirective directive) {
        if(directive.getName().equalsIgnoreCase("condition")){
            try {
                int lineNum = document.getLineOfOffset(fOffset);
                if(lineNum!=0)return false;
            } catch (BadLocationException e) {
            }
        }
        return true;
    }
    public int getCompletionOffset() {
        if (fCompletionOffset == -1) {
            if (currStatementState == STATAMENT_DIRECTIVE_UNCOMPLETE) {
                fCompletionOffset = getStatementStartOffset();
            } else if (currStatementState == STATAMENT_DIRECTIVE_COMPLETE) {
                fCompletionOffset = getWordBeginingPos(COMMON_SEPARATOR);
            }
        }
        return fCompletionOffset;
    }

    public IProject getContainedProject() {
        return containedProject;
    }

    private String getCurrDirectiveName() {
        String statement = getPreprocessStatement();
        String directiveName = statement.substring(0, statement
                .indexOf(COMMON_SEPARATOR));
        return directiveName;

    }

    public IPreprocessDirectiveProvider getDirectiveProvider() {
        return directiveProvider;
    }

    public IDocument getDocument() {
        return document;
    }

    private String getDocumentSegment(int startPos, int length) {
        String value = "";
        try {
            value = document.get(startPos, length);
        } catch (BadLocationException e) {

        }
        return value;
    }

    /**
     * @return the document's current line start position
     */
    public int getLineStartOffset() {
        if (fLineStartOffset == -1) {
            try {
                int lineNum = document.getLineOfOffset(fOffset);
                fLineStartOffset = document.getLineOffset(lineNum);
            } catch (BadLocationException e) {
                fLineStartOffset = 0;
            }
        }

        return fLineStartOffset;
    }

    public int getOffset() {
        return fOffset;
    }

    /**
     * calculate the possible proposal types according to the context
     * 
     * @return a list of possible <code>ProposalType</code>
     */
    public List<ProposalType> getPossibleProposalTypes() {
        List<ProposalType> types = new ArrayList<ProposalType>();
        // if the environment is not satisfied for proposal,
        // return an empty list
        if (!validEnv) {
            return types;
        }
        // preprocess statement has uncomplete directive
        if (currStatementState == STATAMENT_DIRECTIVE_UNCOMPLETE) {
            types.add(ProposalType.TEMPLATE);
            types.add(ProposalType.DIRECTIVE);
            return types;
        }
        // preprocess statement has completed directive
        // need to complete the expression
        if (currStatementState == STATAMENT_DIRECTIVE_COMPLETE) {
            if (hasLegalDirective()) {
                if (needSymbolProposal()) {
                    types.add(ProposalType.SYMBOL);
                    return types;
                }

                if (needDebugLevelProposal()) {
                    types.add(ProposalType.DEBUG_LEVER);
                    return types;
                }
            }
        }

        return types;
    }

    /**
     * @return the preprocessing line where the content assist invoked, the line
     *         begins with the line start and end with the content assist
     *         invocation offset.
     */
    public String getPreprocessingLine() {
        int start = getLineStartOffset();
        int length = getOffset() - start;
        return getDocumentSegment(start, length);
    }

    /**
     * @return the preprocess prefix
     */
    public String getPreprocessingLinePrefix() {
        return prefixHandler.getCurrPrefix(getPreprocessingLine());

    }

    /**
     * @return current preprocessing statment(without prefix).
     */
    public String getPreprocessStatement() {
        int start = getStatementStartOffset();
        int length = getOffset() - start;
        return getDocumentSegment(start, length);
    }

    /**
     * @return the statement's begin position
     */
    public int getStatementStartOffset() {
        if (fStatementStartOffset == -1) {
            fStatementStartOffset = getLineStartOffset()
                    + getPreprocessingLinePrefix().length();
        }
        return fStatementStartOffset;
    }

    private String getUncompletedElementName() {
        int elementBeginPos = getCompletionOffset();
        String unCompleteElementName = "";
        try {
            unCompleteElementName = document.get(elementBeginPos, getOffset()
                    - elementBeginPos);
        } catch (BadLocationException e) {
        }
        return unCompleteElementName;
    }

    private int getWordBeginingPos(char seperator) {
        int wordBeginPos = getOffset();
        try {
            while (document.getChar(wordBeginPos) != seperator) {
                wordBeginPos--;
            }
        } catch (BadLocationException e) {
        }
        return wordBeginPos + 1;
    }

    private boolean hasLegalDirective() {
        if (!isLegalDirective(getCurrDirectiveName())) {
            return false;
        }
        return true;
    }

    private void initialize() {
        validEnv = validatePPEnvironment();
        if (validEnv) {
            String statement = getPreprocessStatement();
            if (statement.indexOf(COMMON_SEPARATOR) >= 0) {
                currStatementState = STATAMENT_DIRECTIVE_COMPLETE;
            }
        }

    }

    /**
     * @param project
     * @return if the contained project is a preprocessing project return true
     */
    private boolean isInRightProject(IProject project) {
        boolean preprocessing = false;
        try {
            preprocessing = project
                    .hasNature(IMTJCoreConstants.J2ME_PREPROCESSING_NATURE_ID);
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.WARNING, e);
        }
        return preprocessing;
    }

    private boolean isLegalDirective(String directiveName) {
        if (getDirectiveProvider() != null) {
            return getDirectiveProvider().getDirective(directiveName) != null;
        }
        return false;
    }

    private boolean needDebugLevelProposal() {
        String directiveName = getCurrDirectiveName();
        if (directiveName.equalsIgnoreCase("debug")
                || directiveName.equalsIgnoreCase("mdebug")) {
            return true;
        }

        return false;
    }

    private boolean needSymbolProposal() {
        String directiveName = getCurrDirectiveName();
        if (directiveName.equalsIgnoreCase("else")
                || directiveName.equalsIgnoreCase("debug")
                || directiveName.equalsIgnoreCase("mdebug")
                || directiveName.equalsIgnoreCase("endif")
                || directiveName.equalsIgnoreCase("enddebug")) {
            return false;
        }

        return true;
    }

    private boolean prefixMatch(IPreprocessContentAssistModel model) {
        String unCompleteSymbolName = getUncompletedElementName();
        return model.getName().regionMatches(true, 0, unCompleteSymbolName, 0,
                unCompleteSymbolName.length());

    }

    public void setDirectiveProvider(
            IPreprocessDirectiveProvider directiveProvider) {
        this.directiveProvider = directiveProvider;
    }

    public void setPrefixHandler(IPreprocessPrefixHandler prefixHandler) {
        this.prefixHandler = prefixHandler;
    }

    private boolean validatePPEnvironment() {
        return isInRightProject(containedProject)
                && prefixHandler.hasLegalPrefix(getPreprocessingLine());
    }

}

enum ProposalType {
    DIRECTIVE, SYMBOL, DEBUG_LEVER, TEMPLATE;
}
