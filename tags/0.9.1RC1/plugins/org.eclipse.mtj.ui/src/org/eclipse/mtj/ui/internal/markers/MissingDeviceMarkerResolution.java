/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.ui.internal.markers;

import org.eclipse.core.resources.IMarker;
import org.eclipse.mtj.ui.internal.properties.J2MEProjectPropertiesPage;
import org.eclipse.ui.IMarkerResolution;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.dialogs.PropertyDialog;

/**
 * Resolution for the "org.eclipse.mtj.core.device.missing" marker.
 * 
 * @author Diego Madruga Sandin
 */
@SuppressWarnings("restriction")
public class MissingDeviceMarkerResolution implements IMarkerResolution {

    /**
     * Creates a new MissingDeviceMarkerResolution.
     */
    public MissingDeviceMarkerResolution() {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IMarkerResolution#getLabel()
     */
    public String getLabel() {
        return Messages.MissingDeviceMarkerResolution_fix_device_definition;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IMarkerResolution#run(org.eclipse.core.resources.IMarker)
     */
    public void run(final IMarker marker) {

        PropertyDialog dialog = PropertyDialog.createDialogOn(PlatformUI
                .getWorkbench().getActiveWorkbenchWindow().getShell(),
                J2MEProjectPropertiesPage.PAGEID, marker.getResource()
                        .getProject());

        if (dialog != null) {
            dialog.open();
        }
    }
}
