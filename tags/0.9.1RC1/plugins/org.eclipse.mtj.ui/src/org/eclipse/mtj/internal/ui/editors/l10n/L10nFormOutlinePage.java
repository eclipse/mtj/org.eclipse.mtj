/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n;

import java.util.List;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nObject;
import org.eclipse.mtj.internal.ui.editor.FormOutlinePage;
import org.eclipse.mtj.internal.ui.editor.MTJFormEditor;
import org.eclipse.mtj.internal.ui.editors.l10n.pages.LocalizationPage;
import org.eclipse.mtj.ui.IMTJUIConstants;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class L10nFormOutlinePage extends FormOutlinePage {

    /**
     * TocLabelProvider
     */
    private class L10nLabelProvider extends BasicLabelProvider {

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.editor.FormOutlinePage.BasicLabelProvider#getText(java.lang.Object)
         */
        @Override
        public String getText(Object obj) {
            if (obj instanceof L10nObject) {
                return ((L10nObject) obj).getName();
            }
            return super.getText(obj);
        }
    }

    /**
     * @param editor
     */
    public L10nFormOutlinePage(MTJFormEditor editor) {
        super(editor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.FormOutlinePage#createLabelProvider()
     */
    @Override
    public ILabelProvider createLabelProvider() {
        return new L10nLabelProvider();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.FormOutlinePage#getParentPageId(java.lang.Object)
     */
    @Override
    protected String getParentPageId(Object item) {
        return IMTJUIConstants.LOCALIZATION_DATA_EDITOR;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.FormOutlinePage#getChildren(java.lang.Object)
     */
    protected Object[] getChildren(Object parent) {
        if (parent instanceof LocalizationPage) {
            L10nModel l10nModel = (L10nModel) fEditor.getAggregateModel();
            if (l10nModel != null && l10nModel.isLoaded()) {
                Object[] list = new Object[1];
                list[0] = l10nModel.getLocales();
                return list;
            }
        } else if (parent instanceof L10nObject) {
            List<IDocumentElementNode> list = ((L10nObject) parent)
                    .getChildren();
            // List is never null
            if (list.size() > 0) {
                return list.toArray();
            }
        }

        return super.getChildren(parent);
    }
}
