/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.ui.internal.configurations;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.core.model.configuration.Configuration;
import org.eclipse.mtj.core.model.configuration.Configurations;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;

/**
 * Wizard for Configuration add and edit
 * 
 * @author wangf
 */
public class ConfigAddAndEditWizard extends Wizard {

    private ConfigAddAndEditWizardPage wizardPage;
    private Configurations configurations;
    private Configuration currentConfig;

    public ConfigAddAndEditWizard(Configurations configurations,
            Configuration currentConfig) {
        this.configurations = configurations;
        init(currentConfig);
    }

    @Override
    public void addPages() {
        wizardPage = new ConfigAddAndEditWizardPage(configurations,
                currentConfig);
        addPage(wizardPage);
    }

    public Configuration getConfiguration() {
        return wizardPage.getConfiguration();
    }

    private void init(Configuration currentConfig) {
        this.currentConfig = currentConfig;
        setNeedsProgressMonitor(true);
        if (currentConfig == null) {
            setWindowTitle(ConfigurationMessages.AddConfiguration);
        } else {
            setWindowTitle(ConfigurationMessages.EditConfiguration);
        }
    }

    @Override
    public boolean performCancel() {
        wizardPage.performCancel();
        return true;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.wizard.Wizard#performFinish()
     */
    @Override
    public boolean performFinish() {
        wizardPage.performFinish();
        return true;
    }

    public void setMidletSuiteProject(IMidletSuiteProject midletSuiteProject) {
        wizardPage.setMidletSuiteProject(midletSuiteProject);
    }
}
