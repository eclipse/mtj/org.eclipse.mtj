/**
 * Copyright (c) 2008 Ales Milan and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Ales Milan - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preferences;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.ui.internal.preferences.messages"; //$NON-NLS-1$
    public static String SymbolDefinitionsImportWizardPage_title;
    protected static String SymbolDefinitionsImportWizardPage_error_errorDuringImportSymbolDefinitionSet;
    public static String SymbolDefinitionsImportWizardPage_error;
    public static String SymbolDefinitionsImportWizardPage_description;
    public static String SymbolDefinitionsImportWizardPage_from;
    public static String SymbolDefinitionsImportWizardPage_importFromAntennaJarFile;
    public static String SymbolDefinitionsImportWizardPage_importFromXMLFiles;
    public static String SymbolDefinitionsImportWizardPage_specifyDirectory;
    public static String SymbolDefinitionsImportWizardPage_browse;
    public static String SymbolDefinitionsImportWizardPage_error_antennaLibraryIsNotSpecified;
    public static String SymbolDefinitionsImportWizardPage_error_directoryDoesNotContaintFiles;
    public static String SymbolDefinitionsPreferencePage_ImportButton;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
