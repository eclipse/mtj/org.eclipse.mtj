/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.ui.internal.configurations;

import org.eclipse.osgi.util.NLS;

/**
 * Provides convenience methods for manipulating messages.
 */
public class ConfigurationMessages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.ui.internal.configurations.messages"; //$NON-NLS-1$
    public static String Configuration;
    public static String Configurations;
    public static String Active;
    public static String Add;
    public static String Edit;
    public static String Remove;
    public static String AddConfiguration;
    public static String ConfigAddAndEditWizardPage_AddConfigDescription;
    public static String ConfigurationAddWizardPage_NewConfigurationName;
    public static String Symbols;
    public static String EditConfiguration;
    public static String ConfigAddAndEditWizardPage_EditConfigDescription;
    public static String ErrorMessage_Title;
    public static String ErrorMessage_MustHaveAtLeastOneConfig;
    public static String ErrorMessage_InvalidConfigName;
    public static String ErrorMessage_NoDeviceSelected;
    public static String QuestionMessage_ReexportAntennaBuildFiles_Title;
    public static String QuestionMessage_ReexportAntennaBuildFiles_Message;
    public static String ErrorMessage_ReexportAntennaBuildFilesFailed;
    public static String ErrorMessage_NoDeviceAvailable;
    public static String WarningMessage_ConfigurationsDirty_Title;
    public static String WarningMessage_ConfigurationsDirty_Message;
    public static String QuestionMessage_PackageAllConfigs_Title;
    public static String QuestionMessage_PackageAllConfigs_Message;
    public static String WorkspaceSymbolSetViewer_ColumnTitle_Choose;
    public static String WorkspaceSymbolSetViewer_ColumnTitle_SymbolSet;
    public static String WorkspaceSymbolSetViewer_GroupText;
    public static String WorkspaceSymbolSetViewer_ManageButton;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, ConfigurationMessages.class);
    }

    private ConfigurationMessages() {
    }
}
