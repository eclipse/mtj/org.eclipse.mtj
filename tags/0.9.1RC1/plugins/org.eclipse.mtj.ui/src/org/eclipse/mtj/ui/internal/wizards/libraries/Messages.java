/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.ui.internal.wizards.libraries;

import org.eclipse.osgi.util.NLS;

/**
 * @author Diego Madruga Sandin
 */
public class Messages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.ui.internal.wizards.libraries.messages"; //$NON-NLS-1$

    public static String LibraryInfoBlock_libraryLicenseURI_href;
    public static String LibraryInfoBlock_libraryDescriptionLabel;
    public static String LibraryInfoBlock_libraryLicenceLabel;
    public static String LibraryInfoBlock_libraryLicenseURILabel;
    public static String LibraryInfoBlock_libraryNameLabel;
    public static String LibraryInfoBlock_libraryVersionLabel;

    public static String LibraryInfoBlock_permissions_label;

    public static String LibraryInfoBlock_protection_domain_label;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
