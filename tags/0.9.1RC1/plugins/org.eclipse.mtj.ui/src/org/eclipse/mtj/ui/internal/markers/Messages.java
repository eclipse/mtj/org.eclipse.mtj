/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.ui.internal.markers;

import org.eclipse.osgi.util.NLS;

/**
 * @author Diego Madruga Sandin
 */
public class Messages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.ui.internal.markers.messages"; //$NON-NLS-1$

    public static String MissingDeviceMarkerResolution_fix_device_definition;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
