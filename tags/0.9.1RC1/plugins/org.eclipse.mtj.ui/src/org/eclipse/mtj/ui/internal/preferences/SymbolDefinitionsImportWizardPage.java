/**
 * Copyright (c) 2008 Ales Milan and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Ales Milan - Initial implementation
 */

package org.eclipse.mtj.ui.internal.preferences;

import java.io.File;
import java.text.MessageFormat;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.model.device.preprocess.DeviceSymbolDefinitionSetFactory;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * Wizard for importing SymbolDefinitionSets J2ME Polish.
 * 
 * @author Ales "afro" Milan
 */
public class SymbolDefinitionsImportWizardPage extends WizardPage {

    public static final int IMPORT_FROM_ANTENNA_JAR = 0;
    public static final int IMPORT_J2MEPOLISH_FORMAT = 1;
    public static final int IMPORT_MTJ_FORMAT = 2;

    public static final String NAME = "symbolDefinitionsImportPage";

    private static final String KEY_WIDTH = "dialogWidth";
    private static final String KEY_HEIGHT = "dialogHeight";

    private Button[] importTypeRadios;
    private Label importFileLabel;
    private Text importDirectory;
    private Button importFileBrowseButton;
    private String antennaFile;

    /**
     * Constructor
     */
    public SymbolDefinitionsImportWizardPage() {

        super(NAME, Messages.SymbolDefinitionsImportWizardPage_title, null);
        setPageComplete(true);
        setDescription(Messages.SymbolDefinitionsImportWizardPage_description);
    }

    private void addChooseFileControl(Composite parent) {

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(3, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        importFileLabel = new Label(composite, SWT.NONE);
        importFileLabel
                .setText(Messages.SymbolDefinitionsImportWizardPage_specifyDirectory);

        importDirectory = new Text(composite, SWT.BORDER);
        importDirectory.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        importDirectory.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                updateFinishButton();
            }
        });

        importFileBrowseButton = new Button(composite, SWT.PUSH);
        importFileBrowseButton
                .setText(Messages.SymbolDefinitionsImportWizardPage_browse);
        importFileBrowseButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleBrowseButton();
            }
        });

    }

    /**
     * Add the controls that allow the user to select the type of import.
     * 
     * @param parent
     */
    private void addImportSelectorControls(Composite parent) {

        Group importTypeGroup = new Group(parent, SWT.NONE);
        importTypeGroup
                .setText(Messages.SymbolDefinitionsImportWizardPage_from);
        importTypeGroup.setLayout(new GridLayout(1, false));
        importTypeGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        importTypeRadios = new Button[2];

        importTypeRadios[0] = new Button(importTypeGroup, SWT.RADIO);
        importTypeRadios[0]
                .setText(Messages.SymbolDefinitionsImportWizardPage_importFromAntennaJarFile);

        importTypeRadios[1] = new Button(importTypeGroup, SWT.RADIO);
        importTypeRadios[1]
                .setText(Messages.SymbolDefinitionsImportWizardPage_importFromXMLFiles);
        // set default selection
        importTypeRadios[0].setSelection(true);

        importTypeRadios[1].addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                if (((Button) e.getSource()).getSelection()) {
                    updateControlEnable(true);
                } else {
                    updateControlEnable(false);
                }
                updateFinishButton();
            }
        });

        addChooseFileControl(importTypeGroup);
        updateControlEnable(false);
        updateFinishButton();

    }

    public void createControl(Composite parent) {

        parent.setLayoutData(new GridData(GridData.FILL_BOTH));

        final Composite composite = new Composite(parent, SWT.NONE);

        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));
        composite.addControlListener(new ControlAdapter() {
            @Override
            public void controlResized(ControlEvent e) {
                storeSize();
            }
        });
        setControl(composite);

        IPreferenceStore preferenceStore = MTJUIPlugin.getDefault()
                .getCorePreferenceStore();
        antennaFile = preferenceStore
                .getString(IMTJCoreConstants.PREF_ANTENNA_JAR);

        addImportSelectorControls(composite);

        // Set the size if previously stored away

        Point size = retrieveSize();
        if (size != null) {
            composite.setSize(size);
        }

    }

    public String getDirectory() {
        return importDirectory.getText();
    }

    public int getImportType() {
        if (importTypeRadios[0].getSelection()) {
            return IMPORT_FROM_ANTENNA_JAR;
        } else if (importTypeRadios[1].getSelection()) {
            return IMPORT_J2MEPOLISH_FORMAT;
        }
        return IMPORT_MTJ_FORMAT;
    }

    private void handleBrowseButton() {

        DirectoryDialog dialog = new DirectoryDialog(getShell());
        String directory = dialog.open();
        if (directory != null) {
            importDirectory.setText(directory);
            updateFinishButton();
        }

    }

    /**
     * Retrieve the previously stored size or <code>null</code> if not found.
     * 
     * @return
     */
    private Point retrieveSize() {
        Point size = null;
        IDialogSettings settings = getDialogSettings();
        if ((settings != null) && (settings.get(KEY_WIDTH) != null)) {
            size = new Point(settings.getInt(KEY_WIDTH), settings
                    .getInt(KEY_HEIGHT));
        }
        return size;
    }

    /**
     * Store off the size of the control in the dialog settings.
     */
    private void storeSize() {
        IDialogSettings settings = getDialogSettings();
        Point size = getControl().getSize();
        if (settings != null) {
            settings.put(KEY_WIDTH, size.x);
            settings.put(KEY_HEIGHT, size.y);
        }
    }

    private void updateControlEnable(boolean value) {

        importFileLabel.setEnabled(value);
        importFileBrowseButton.setEnabled(value);
        importDirectory.setEnabled(value);

    }

    public void updateFinishButton() {

        if (importTypeRadios[0].getSelection()) {
            if (!new File(antennaFile).exists()) {
                setPageComplete(false);
                setErrorMessage(Messages.SymbolDefinitionsImportWizardPage_error_antennaLibraryIsNotSpecified);
                return;
            }
        } else if (importTypeRadios[1].getSelection()) {
            String directory = importDirectory.getText().trim();
            if (!new File(
                    directory
                            + File.separator
                            + DeviceSymbolDefinitionSetFactory.J2MEPOLISH_FILENAME_XML_DEVICES)
                    .exists()
                    || !new File(
                            directory
                                    + File.separator
                                    + DeviceSymbolDefinitionSetFactory.J2MEPOLISH_FILENAME_XML_GROUPS)
                            .exists()) {
                setPageComplete(false);
                setErrorMessage(MessageFormat
                        .format(
                                Messages.SymbolDefinitionsImportWizardPage_error_directoryDoesNotContaintFiles,
                                new Object[] {
                                        DeviceSymbolDefinitionSetFactory.J2MEPOLISH_FILENAME_XML_DEVICES,
                                        DeviceSymbolDefinitionSetFactory.J2MEPOLISH_FILENAME_XML_GROUPS }));
                return;
            }
        }
        setErrorMessage(null);
        setPageComplete(true);
    }

}
