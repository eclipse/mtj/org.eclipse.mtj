/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant.template;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.preprocess.contentAssistant.PreprocessContext;

/**
 * Responsible for the completion of preprocess template proposal
 * 
 * @author gma
 * @since 0.9.1
 */
public class PreprocessTemplateEngine {

    private static final int DEFAULT_TEMPLATE_PROPOSAL_SERVERITY = 1;
    private PreprocessTemplateContextType ppTemplateContextType;

    public PreprocessTemplateEngine(
            PreprocessTemplateContextType ppTemplateContextType) {
        this.ppTemplateContextType = ppTemplateContextType;
    }

    /**
     * @param ppContext
     * @return List of computed <code>PreprocessTemplateProposal</code>
     */
    public List<PreprocessTemplateProposal> complete(PreprocessContext ppContext) {
        List<PreprocessTemplateProposal> result = new ArrayList<PreprocessTemplateProposal>();

        Template[] templates = PreprocessTemplateAccess.getDefault()
                .getTemplateStore().getTemplates();
        Position position = getReplacePosition(ppContext);

        PreprocessTemplateContext pTemplateContext = ppTemplateContextType
                .createContext(ppContext.getDocument(), position, ppContext);

        IRegion region = new Region(position.offset, position.length);
        for (Template element : templates) {
            if (pTemplateContext.canEvaluate(element)) {
                PreprocessTemplateProposal proposal = constructTemplateProposal(
                        element, pTemplateContext, region);
                result.add(proposal);
            }
        }
        return result;
    }

    private PreprocessTemplateProposal constructTemplateProposal(
            Template template, PreprocessTemplateContext ppTemplateContext,
            IRegion region) {
        PreprocessTemplateProposal proposal = new PreprocessTemplateProposal(
                template, ppTemplateContext, region, MTJUIPlugin
                        .getImageFromCache("preprocess_template_proposal.gif"),
                DEFAULT_TEMPLATE_PROPOSAL_SERVERITY);
        proposal
                .setInformationControlCreator(new PreprocessTemplateInformationControlCreator());
        return proposal;
    }

    private Position getReplacePosition(PreprocessContext ppContext) {
        int offset = ppContext.getOffset();
        IDocument doc = ppContext.getDocument();
        int lineStart = ppContext.getLineStartOffset();
        String prefix = "";
        try {
            prefix = doc.get(lineStart, offset - lineStart).trim();
        } catch (BadLocationException e) {
        }
        Position position = new Position(offset - prefix.length(), prefix
                .length());
        return position;
    }
}
