/*******************************************************************************
 * Copyright (c) 2007 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/

package org.eclipse.mtj.internal.ui.editors.l10n.details;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nObject;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.editor.FormEntryAdapter;
import org.eclipse.mtj.internal.ui.editors.l10n.L10nInputContext;
import org.eclipse.mtj.internal.ui.editors.l10n.LocalesTreeSection;
import org.eclipse.mtj.ui.internal.forms.parts.FormEntry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.IFormPart;

public class L10nLocaleDetails extends L10nAbstractDetails {

    private L10nLocale locale;

    private FormEntry localeNameEntry;

    /**
     * @param masterSection
     */
    public L10nLocaleDetails(LocalesTreeSection masterSection) {
        super(masterSection, L10nInputContext.CONTEXT_ID);
        locale = null;

        localeNameEntry = null;
    }

    /**
     * @param object
     */
    public void setData(L10nLocale object) {
        // Set data
        locale = object;
    }

    protected L10nObject getDataObject() {
        return locale;
    }

    /* (non-Javadoc)
     * @see org.eclipse.pde.internal.ui.editor.cheatsheet.CSAbstractDetails#createDetails(org.eclipse.swt.widgets.Composite)
     */
    public void createFields(Composite parent) {
        createAnchorIdWidget(parent);
    }

    /**
     * @param parent
     */
    private void createAnchorIdWidget(Composite parent) {
        localeNameEntry = new FormEntry(parent, getManagedForm().getToolkit(),
                MTJUIMessages.L10nLocaleDetails_localeNameEntry_label, SWT.NONE);
        // Ensure that the text field has proper width
        localeNameEntry.getText().setLayoutData(
                new GridData(SWT.FILL, SWT.CENTER, true, false));
    }

    protected String getDetailsTitle() {
        return MTJUIMessages.L10nLocaleDetails_detailsTitle;
    }

    protected String getDetailsDescription() {
        return MTJUIMessages.L10nLocaleDetails_detailsDescription;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#hookListeners()
     */
    public void hookListeners() {
        createLocaleNameEntryListeners();
    }

    /**
     * 
     */
    private void createLocaleNameEntryListeners() {
        localeNameEntry.setFormEntryListener(new FormEntryAdapter(this) {
            public void textValueChanged(FormEntry entry) {
                // Ensure data object is defined
                if (locale != null) {
                    {
                        locale.setLocaleName(localeNameEntry.getValue());
                    }
                }
            }
        });
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#updateFields()
     */
    public void updateFields() {
        // Ensure data object is defined
        if (locale != null) { // Update name entry
            updateAnchorIdEntry(isEditableElement());
        }
    }

    /**
     * @param editable
     */
    private void updateAnchorIdEntry(boolean editable) {
        localeNameEntry.setValue(locale.getLocaleName(), true);
        localeNameEntry.setEditable(editable);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.AbstractFormPart#commit(boolean)
     */
    public void commit(boolean onSave) {
        super.commit(onSave);
        // Only required for form entries
        localeNameEntry.commit();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.IPartSelectionListener#selectionChanged(org.eclipse.ui.forms.IFormPart, org.eclipse.jface.viewers.ISelection)
     */
    public void selectionChanged(IFormPart part, ISelection selection) {
        // Get the first selected object
        Object object = getFirstSelectedObject(selection);
        // Ensure we have the right type
        if (object != null && object instanceof L10nLocale) {
            // Set data
            setData((L10nLocale) object);
            // Update the UI given the new data
            updateFields();
        }
    }
}
