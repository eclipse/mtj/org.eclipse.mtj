/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang  Ma     (Sybase)    - Set default TEMPLATES_USE_CODEFORMATTER to true
 */
package org.eclipse.mtj.ui.internal;

import org.eclipse.core.runtime.Preferences;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.mtj.core.internal.MTJCorePreferenceInitializer;
import org.eclipse.mtj.internal.ui.IPreferenceConstants;
import org.eclipse.mtj.internal.ui.editor.text.ColorManager;
import org.eclipse.mtj.ui.IMTJUIConstants;

/**
 * Preference initializer for default MTJ UI preferences.
 * 
 * @author Craig Setera
 */
public class MTJUIPreferenceInitializer extends AbstractPreferenceInitializer {

    public static final boolean PREF_DEF_TEMPLATES_USE_CODEFORMATTER = true;

    /**
     * Construct a new Initializer instance.
     */
    public MTJUIPreferenceInitializer() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
     */
    public void initializeDefaultPreferences() {

        // Synchronize the preferences between the core and UI plug-ins.
        Preferences prefs = MTJUIPlugin.getDefault().getPluginPreferences();
        initializeDefaultPreferences(prefs);
        MTJCorePreferenceInitializer.initializeDefaultPreferences(prefs);

        IPreferenceStore store = MTJUIPlugin.getDefault().getPreferenceStore();

        /* Initialize some editor preferences */
        ColorManager.initializeDefaults(store);
        store.setDefault(IPreferenceConstants.EDITOR_FOLDING_ENABLED, false);
    }

    /**
     * @param prefs
     */
    private void initializeDefaultPreferences(Preferences prefs) {
        prefs.setDefault(IMTJUIConstants.TEMPLATES_USE_CODEFORMATTER,
                PREF_DEF_TEMPLATES_USE_CODEFORMATTER);
    }
}
