/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.utils.ValueChangeTrackingBooleanFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

/**
 * Preference page implementation for setting Java ME Over the Air preferences.
 * 
 * @author Craig Setera
 */
public class J2MEOTAPreferencePage extends FieldEditorPreferencePage implements
        IWorkbenchPreferencePage, IMTJCoreConstants {
    /**
     * Default constructor.
     */
    public J2MEOTAPreferencePage() {
        super(GRID);
        setPreferenceStore(MTJUIPlugin.getDefault().getCorePreferenceStore());
        setDescription(MTJUIStrings.getString("pref.description"));
    }

    /*
     * Overridden so that we get the help context where it belongs so that it
     * works when the focus is in the left pane.
     */
    protected Control createContents(Composite parent) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_J2MEOTAPreferencePage");

        return (super.createContents(parent));
    }

    /**
     * Creates the field editors.
     */
    public void createFieldEditors() {
        final Composite parent = getFieldEditorParent();

        addField(new BooleanFieldEditor(PREF_OTA_SERVER_START_AT_START,
                "Start Listening at Startup", parent));

        // Create the field editors to be added to the page
        final ValueChangeTrackingBooleanFieldEditor definedPortEditor = new ValueChangeTrackingBooleanFieldEditor(
                PREF_OTA_PORT_DEFINED, "Use Specified Port", parent);
        final IntegerFieldEditor portEditor = new IntegerFieldEditor(
                PREF_OTA_PORT, "Specified Port", parent);

        // Add the field editors
        definedPortEditor.setFieldEditor(portEditor);
        addField(definedPortEditor);
        addField(portEditor);

        addField(new BooleanFieldEditor(PREF_OTA_AUTODEPLOY,
                "Automatically deploy prior to launch", parent));
    }

    /**
     * Initialize the preference page as necessary
     * 
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }
}
