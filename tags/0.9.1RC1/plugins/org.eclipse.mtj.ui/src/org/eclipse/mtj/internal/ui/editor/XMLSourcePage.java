/**
 * Copyright (c) 2003, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.texteditor.DefaultRangeIndicator;

public abstract class XMLSourcePage extends MTJProjectionSourcePage {

    /**
     * @param editor
     * @param id
     * @param title
     */
    public XMLSourcePage(MTJFormEditor editor, String id, String title) {
        super(editor, id, title);
        setRangeIndicator(new DefaultRangeIndicator());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#canLeaveThePage()
     */
    public boolean canLeaveThePage() {
        boolean cleanModel = getInputContext().isModelCorrect();
        if (!cleanModel) {
            Display.getCurrent().beep();
            String title = getEditor().getSite().getRegisteredName();
            MessageDialog
                    .openError(
                            MTJUIPlugin.getActiveWorkbenchShell(),
                            title,
                            MTJUIMessages.XMLSourcePage_cantLeaveThePage);
        }
        return cleanModel;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#collectContextMenuPreferencePages()
     */
    protected String[] collectContextMenuPreferencePages() {
        String[] ids = super.collectContextMenuPreferencePages();
        String[] more = new String[ids.length + 1];
        more[0] = "org.eclipse.mtj.ui.EditorPreferencePage"; //$NON-NLS-1$
        System.arraycopy(ids, 0, more, 1, ids.length);
        return more;
    }

}
