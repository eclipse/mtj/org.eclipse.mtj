/**
 * Copyright (c) 2003, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDESourcePage
 */
package org.eclipse.mtj.internal.ui.editor;

import java.util.ResourceBundle;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IPostSelectionProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.mtj.internal.core.text.IDocumentAttributeNode;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.IDocumentRange;
import org.eclipse.mtj.internal.core.text.IDocumentTextNode;
import org.eclipse.mtj.internal.core.text.IEditingModel;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.editor.actions.FormatAction;
import org.eclipse.mtj.internal.ui.editor.actions.HyperlinkAction;
import org.eclipse.mtj.internal.ui.editor.actions.MTJActionConstants;
import org.eclipse.mtj.internal.ui.editor.context.InputContext;
import org.eclipse.mtj.internal.ui.editor.outline.IOutlineContentCreator;
import org.eclipse.mtj.internal.ui.editor.outline.IOutlineSelectionHandler;
import org.eclipse.mtj.internal.ui.editor.text.MTJSelectAnnotationRulerAction;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.IFormPage;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.ide.IGotoMarker;
import org.eclipse.ui.part.IShowInTargetList;
import org.eclipse.ui.texteditor.ChainedPreferenceStore;
import org.eclipse.ui.texteditor.ContentAssistAction;
import org.eclipse.ui.texteditor.DefaultRangeIndicator;
import org.eclipse.ui.texteditor.ITextEditorActionConstants;
import org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds;
import org.eclipse.ui.texteditor.ResourceAction;
import org.eclipse.ui.texteditor.TextOperationAction;

/**
 * @since 0.9.1
 */
public abstract class MTJSourcePage extends TextEditor implements IFormPage,
        IGotoMarker, ISelectionChangedListener, IOutlineContentCreator,
        IOutlineSelectionHandler {

    /**
     * Updates the OutlinePage selection and this editor's range indicator.
     * 
     * @since 0.9.1
     */
    private class MTJSourcePageChangedListener implements
            ISelectionChangedListener {

        /**
         * Installs this selection changed listener with the given selection
         * provider. If the selection provider is a post selection provider,
         * post selection changed events are the preferred choice, otherwise
         * normal selection changed events are requested.
         * 
         * @param selectionProvider
         */
        public void install(ISelectionProvider selectionProvider) {
            if (selectionProvider != null) {
                if (selectionProvider instanceof IPostSelectionProvider) {
                    IPostSelectionProvider provider = (IPostSelectionProvider) selectionProvider;
                    provider.addPostSelectionChangedListener(this);
                } else {
                    selectionProvider.addSelectionChangedListener(this);
                }
            }
        }

        /* (non-Javadoc)
         * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
         */
        public void selectionChanged(SelectionChangedEvent event) {
            handleSelectionChangedSourcePage(event);
        }

        /**
         * Removes this selection changed listener from the given selection
         * provider.
         * 
         * @param selectionProviderstyle
         */
        public void uninstall(ISelectionProvider selectionProvider) {
            if (selectionProvider != null) {
                if (selectionProvider instanceof IPostSelectionProvider) {
                    IPostSelectionProvider provider = (IPostSelectionProvider) selectionProvider;
                    provider.removePostSelectionChangedListener(this);
                } else {
                    selectionProvider.removeSelectionChangedListener(this);
                }
            }
        }

    }

    private static String RES_BUNDLE_LOCATION = "org.eclipse.mtj.internal.ui.editor.text.ConstructedMTJEditorMessages"; //$NON-NLS-1$

    private static ResourceBundle fgBundleForConstructedKeys = ResourceBundle
            .getBundle(RES_BUNDLE_LOCATION);

    /**
     * @return
     */
    public static ResourceBundle getBundleForConstructedKeys() {
        return fgBundleForConstructedKeys;
    }

    private Control fControl;

    private MTJFormEditor fEditor;
    /**
     * The editor selection changed listener.
     */
    private MTJSourcePageChangedListener fEditorSelectionChangedListener;
    private String fId;
    private int fIndex;
    private InputContext fInputContext;
    private ISortableContentOutlinePage fOutlinePage;
    private Object fSelection;
    private ISelectionChangedListener fOutlineSelectionChangedListener;

    /**
     * @param editor
     * @param id
     * @param title
     */
    public MTJSourcePage(MTJFormEditor editor, String id, String title) {
        fId = id;
        initialize(editor);
        IPreferenceStore[] stores = new IPreferenceStore[2];
        stores[0] = MTJUIPlugin.getDefault().getPreferenceStore();
        stores[1] = EditorsUI.getPreferenceStore();
        setPreferenceStore(new ChainedPreferenceStore(stores));
        setRangeIndicator(new DefaultRangeIndicator());
        if (isSelectionListener()) {
            getEditor().getSite().getSelectionProvider()
                    .addSelectionChangedListener(this);
        }
    }

    /**
     * @param range
     */
    public IDocumentRange adaptRange(IDocumentRange range) {
        // Subclasses to override
        return range;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#canLeaveThePage()
     */
    public boolean canLeaveThePage() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.outline.IOutlineContentCreator#createDefaultOutlineComparator()
     */
    public ViewerComparator createDefaultOutlineComparator() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.outline.IOutlineContentCreator#createOutlineComparator()
     */
    public abstract ViewerComparator createOutlineComparator();

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.outline.IOutlineContentCreator#createOutlineContentProvider()
     */
    public abstract ITreeContentProvider createOutlineContentProvider();

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.outline.IOutlineContentCreator#createOutlineLabelProvider()
     */
    public abstract ILabelProvider createOutlineLabelProvider();

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createPartControl(Composite parent) {
        super.createPartControl(parent);
        Control[] children = parent.getChildren();
        fControl = children[children.length - 1];
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.editors.text.TextEditor#dispose()
     */
    @Override
    public void dispose() {
        if (fEditorSelectionChangedListener != null) {
            fEditorSelectionChangedListener.uninstall(getSelectionProvider());
            fEditorSelectionChangedListener = null;
        }
        if (fOutlinePage != null) {
            fOutlinePage.dispose();
            fOutlinePage = null;
        }
        if (isSelectionListener()) {
            getEditor().getSite().getSelectionProvider()
                    .removeSelectionChangedListener(this);
        }
        super.dispose();
    }

    /**
     * Override the getAdapter function to return a list of targets for the
     * "Show In >" action in the context menu.
     * 
     * @param adapter
     * @return A list of targets (IShowInTargetList) for the "Show In >"
     *         sub-menu if the appropriate adapter is passed in and the editor
     *         is not read-only. Returns <code>super.getAdapter(adapter)</code>
     *         otherwise.
     * @see org.eclipse.ui.editors.text.TextEditor#getAdapter(java.lang.Class)
     */
    @Override
    @SuppressWarnings("unchecked")
    public Object getAdapter(Class adapter) {
        if ((adapter == IShowInTargetList.class) && (fEditor != null)
                && (fEditor.getEditorInput() instanceof IFileEditorInput)) {
            return getShowInTargetList();
        }
        return super.getAdapter(adapter);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.outline.IOutlineSelectionHandler#getContentOutline()
     */
    public ISortableContentOutlinePage getContentOutline() {
        if (fOutlinePage == null) {
            fOutlinePage = createOutlinePage();
        }
        return fOutlinePage;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#getEditor()
     */
    public FormEditor getEditor() {
        return fEditor;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#getId()
     */
    public String getId() {
        return fId;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#getIndex()
     */
    public int getIndex() {
        return fIndex;
    }

    /**
     * @return Returns the inputContext.
     */
    public InputContext getInputContext() {
        return fInputContext;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#getManagedForm()
     */
    public IManagedForm getManagedForm() {
        // not a form page
        return null;
    }

    @Override
    public int getOrientation() {
        return SWT.LEFT_TO_RIGHT;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.outline.IOutlineContentCreator#getOutlineInput()
     */
    public Object getOutlineInput() {
        return getInputContext().getModel();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#getPartControl()
     */
    public Control getPartControl() {
        return fControl;
    }

    /**
     * @param offset
     * @param searchChildren
     * @return
     */
    public IDocumentRange getRangeElement(int offset, boolean searchChildren) {
        return null;
    }

    /**
     * @return
     */
    public Object getSelection() {
        return fSelection;
    }

    /**
     * @return
     */
    public ISourceViewer getViewer() {
        return getSourceViewer();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#initialize(org.eclipse.ui.forms.editor.FormEditor)
     */
    public void initialize(FormEditor editor) {
        fEditor = (MTJFormEditor) editor;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#isActive()
     */
    public boolean isActive() {
        return this.equals(fEditor.getActivePageInstance());
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#isSource()
     */
    public boolean isEditor() {
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent)
     */
    public final void selectionChanged(SelectionChangedEvent event) {
        if (event.getSource() == getSelectionProvider()) {
            return;
        }
        ISelection sel = event.getSelection();
        if (sel instanceof IStructuredSelection) {

            IStructuredSelection structuredSel = (IStructuredSelection) sel;

            // Store the selected object to save us having to do this again.
            setSelectedObject(structuredSel.getFirstElement());

        } else if (sel instanceof ITextSelection) {

            ITextSelection textSel = (ITextSelection) sel;

            setSelectedObject(getRangeElement(textSel.getOffset(), false));

        } else {
            fSelection = null;
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#focusOn(java.lang.Object)
     */
    public boolean selectReveal(Object object) {
        if (object instanceof IMarker) {
            IDE.gotoMarker(this, (IMarker) object);
            return true;
        }
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#setActive(boolean)
     */
    public void setActive(boolean active) {
        fInputContext.setSourceEditingMode(active);
    }

    /**
     * @param range
     * @param moveCursor
     */
    public void setHighlightRange(IDocumentRange range, boolean moveCursor) {
        int offset = range.getOffset();
        if (offset == -1) {
            resetHighlightRange();
            return;
        }

        ISourceViewer sourceViewer = getSourceViewer();
        if (sourceViewer == null) {
            return;
        }

        IDocument document = sourceViewer.getDocument();
        if (document == null) {
            return;
        }

        int length = range.getLength();
        setHighlightRange(offset, length == -1 ? 1 : length, moveCursor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.IFormPage#setIndex(int)
     */
    public void setIndex(int index) {
        fIndex = index;
    }

    /**
     * @param inputContext The inputContext to set.
     */
    public void setInputContext(InputContext inputContext) {
        fInputContext = inputContext;
        setDocumentProvider(inputContext.getDocumentProvider());
    }

    /**
     * @param range
     * @param fullNodeSelection
     */
    public void setSelectedRange(IDocumentRange range, boolean fullNodeSelection) {
        ISourceViewer sourceViewer = getSourceViewer();
        if (sourceViewer == null) {
            return;
        }

        IDocument document = sourceViewer.getDocument();
        if (document == null) {
            return;
        }

        int offset;
        int length;
        if ((range instanceof IDocumentElementNode) && !fullNodeSelection) {
            length = ((IDocumentElementNode) range).getXMLTagName().length();
            offset = range.getOffset() + 1;
        } else {
            length = range.getLength();
            offset = range.getOffset();
        }
        sourceViewer.setSelectedRange(offset, length);
    }

    /**
     * Triggered by toggling the 'Link with Editor' button in the outline view
     * 
     * @param offset
     */
    public void synchronizeOutlinePage() {
        // Get the current position of the cursor in this page
        int current_offset = getSourceViewer().getSelectedRange().x;
        synchronizeOutlinePage(current_offset);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.outline.IOutlineSelectionHandler#updateSelection(java.lang.Object)
     */
    public abstract void updateSelection(Object object);

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.outline.IOutlineSelectionHandler#updateSelection(org.eclipse.jface.viewers.SelectionChangedEvent)
     */
    public void updateSelection(SelectionChangedEvent event) {
        ISelection sel = event.getSelection();
        if (sel instanceof IStructuredSelection) {
            IStructuredSelection structuredSelection = (IStructuredSelection) sel;
            updateSelection(structuredSelection.getFirstElement());
        }
    }

    private void createContentAssistAction() {
        IAction contentAssist = new ContentAssistAction(
                getBundleForConstructedKeys(), "ContentAssistProposal.", this); //$NON-NLS-1$
        contentAssist
                .setActionDefinitionId(ITextEditorActionDefinitionIds.CONTENT_ASSIST_PROPOSALS);
        setAction("ContentAssist", contentAssist); //$NON-NLS-1$
        markAsStateDependentAction("ContentAssist", true); //$NON-NLS-1$

        // FIXME
        // ISourceViewer sourceViewer = getSourceViewer();
        // if (sourceViewer instanceof ISourceViewerExtension4) {
        // ContentAssistantFacade contentAssistantFacade =
        // ((ISourceViewerExtension4) sourceViewer)
        // .getContentAssistantFacade();
        // if (contentAssistantFacade != null)
        // fKeyBindingSupportForAssistant = new KeyBindingSupportForAssistant(
        // contentAssistantFacade);
        // }
    }

    /**
     * 
     */
    private void createQuickOutlineAction() {
        // Quick Outline Action
        ResourceAction action = new TextOperationAction(
                getBundleForConstructedKeys(), "QuickOutline.", this, //$NON-NLS-1$
                MTJProjectionViewer.QUICK_OUTLINE, true);
        action
                .setActionDefinitionId(MTJActionConstants.COMMAND_ID_QUICK_OUTLINE);
        action.setText(MTJUIMessages.MTJSourcePage_quickOutlineAction_text);
        action.setId(MTJActionConstants.COMMAND_ID_QUICK_OUTLINE);
        action.setImageDescriptor(MTJUIPluginImages.DESC_OVERVIEW_OBJ);
        setAction(MTJActionConstants.COMMAND_ID_QUICK_OUTLINE, action);
    }

    /**
     * Synchronize the outline page to show the specified element
     * 
     * @param rangeElement The element to show in the outline page
     */
    private void synchronizeOutlinePage(IDocumentRange rangeElement) {
        updateHighlightRange(rangeElement);
        updateOutlinePageSelection(rangeElement);
    }

    @Override
    protected void createActions() {
        super.createActions();
        MTJSelectAnnotationRulerAction action = new MTJSelectAnnotationRulerAction(
                getBundleForConstructedKeys(),
                "MTJSelectAnnotationRulerAction.", this, getVerticalRuler()); //$NON-NLS-1$
        setAction(ITextEditorActionConstants.RULER_CLICK, action);
        MTJFormEditorContributor contributor = fEditor == null ? null : fEditor
                .getContributor();
        if (contributor instanceof MTJFormTextEditorContributor) {
            MTJFormTextEditorContributor textContributor = (MTJFormTextEditorContributor) contributor;
            setAction(MTJActionConstants.OPEN, textContributor
                    .getHyperlinkAction());
            setAction(MTJActionConstants.FORMAT, textContributor
                    .getFormatAction());
            if (textContributor.supportsContentAssist()) {
                createContentAssistAction();
            }
        }

        // Create the quick outline action
        createQuickOutlineAction();
    }

    /**
     * @return
     */
    protected ISortableContentOutlinePage createOutlinePage() {
        SourceOutlinePage sourceOutlinePage = new SourceOutlinePage(
                (MTJFormEditor) getEditor(), (IEditingModel) getInputContext()
                        .getModel(), createOutlineLabelProvider(),
                createOutlineContentProvider(),
                createDefaultOutlineComparator(), createOutlineComparator());

        fOutlinePage = sourceOutlinePage;
        fOutlineSelectionChangedListener = new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                updateSelection(event);
            }
        };
        fOutlinePage
                .addSelectionChangedListener(fOutlineSelectionChangedListener);
        getSelectionProvider().addSelectionChangedListener(sourceOutlinePage);
        fEditorSelectionChangedListener = new MTJSourcePageChangedListener();
        fEditorSelectionChangedListener.install(getSelectionProvider());
        return fOutlinePage;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.editors.text.TextEditor#editorContextMenuAboutToShow(org.eclipse.jface.action.IMenuManager)
     */
    @Override
    protected void editorContextMenuAboutToShow(IMenuManager menu) {
        MTJFormEditorContributor contributor = fEditor == null ? null : fEditor
                .getContributor();

        if (contributor instanceof MTJFormTextEditorContributor) {
            MTJFormTextEditorContributor textContributor = (MTJFormTextEditorContributor) contributor;
            HyperlinkAction action = textContributor.getHyperlinkAction();

            menu.add(action);

            FormatAction formatManifestAction = textContributor
                    .getFormatAction();
            if (isEditable() && (formatManifestAction != null)
                    && formatManifestAction.isEnabled()) {
                menu.add(formatManifestAction);
            }
        }
        super.editorContextMenuAboutToShow(menu);
    }

    /**
     * Utility method for getRangeElement(int, boolean)
     * 
     * @param node
     * @param offset
     * @param searchChildren
     * @see org.eclipse.pde.internal.ui.editor.PDESourcePage#findNode(Object[],
     *      int, boolean)
     */
    protected IDocumentRange findNode(IDocumentElementNode node, int offset,
            boolean searchChildren) {
        return findNode(new Object[] { node }, offset, searchChildren);
    }

    /**
     * Utility method for getRangeElement(int, boolean)
     * 
     * @param nodes All entries should be instances of IDocumentElementNode
     * @param offset The offset the cursor is currently on in the document
     * @param searchChildren <code>true</code> to search child nodes;
     *            <code>false</code> otherwise.
     * @return Node the offset is in
     */
    protected IDocumentRange findNode(Object[] nodes, int offset,
            boolean searchChildren) {
        for (Object node2 : nodes) {
            IDocumentElementNode node = (IDocumentElementNode) node2;
            if ((node.getOffset() <= offset)
                    && (offset < node.getOffset() + node.getLength())) {

                if (!searchChildren) {
                    return node;
                }

                if ((node.getOffset() < offset)
                        && (offset <= node.getOffset()
                                + node.getXMLTagName().length() + 1)) {
                    return node;
                }

                IDocumentAttributeNode[] attrs = node.getNodeAttributes();
                if (attrs != null) {
                    for (IDocumentAttributeNode attr : attrs) {
                        if ((attr.getNameOffset() <= offset)
                                && (offset <= attr.getValueOffset()
                                        + attr.getValueLength())) {
                            return attr;
                        }
                    }
                }

                IDocumentTextNode textNode = node.getTextNode();
                if ((textNode != null)
                        && (textNode.getOffset() <= offset)
                        && (offset < textNode.getOffset()
                                + textNode.getLength())) {
                    return textNode;
                }

                IDocumentElementNode[] children = node.getChildNodes();
                if (children != null) {
                    for (IDocumentElementNode element : children) {
                        if ((element.getOffset() <= offset)
                                && (offset < element.getOffset()
                                        + element.getLength())) {
                            return findNode(element, offset, searchChildren);
                        }
                    }
                }

                // not contained inside any sub elements, must be inside node
                return node;
            }
        }
        return null;
    }


    /**
     * Locate an IDocumentRange, subclasses that want to highlight text
     * components based on site selection should override this method.
     */
    protected IDocumentRange findRange() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractTextEditor#firePropertyChange(int)
     */
    @Override
    protected void firePropertyChange(int type) {
        if (type == PROP_DIRTY) {
            fEditor.fireSaveNeeded(getEditorInput(), true);
        }
        super.firePropertyChange(type);
    }

    /**
     * Returns the <code>IShowInTargetList</code> for this view.
     * 
     * @return the <code>IShowInTargetList</code>
     */
    protected IShowInTargetList getShowInTargetList() {
        return new IShowInTargetList() {
            public String[] getShowInTargetIds() {
                return new String[] { JavaUI.ID_PACKAGES,
                        IPageLayout.ID_RES_NAV };
            }
        };
    }

    /**
     * Handles selection events generated from the source page
     * 
     * @param event The selection changed event
     */
    protected void handleSelectionChangedSourcePage(SelectionChangedEvent event) {

        ISelection selection = event.getSelection();
        if (!selection.isEmpty() && (selection instanceof ITextSelection)) {

            ITextSelection textSel = (ITextSelection) selection;

            // Get the current element from the offset
            int offset = textSel.getOffset();
            IDocumentRange rangeElement;
            rangeElement = this.getRangeElement(offset, false);

            // store this as the current selection
            setSelectedObject(rangeElement);

            // Synchronize the outline page
            synchronizeOutlinePage(rangeElement);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#initializeKeyBindingScopes()
     */
    @Override
    protected void initializeKeyBindingScopes() {
        setKeyBindingScopes(new String[] { "org.eclipse.mtj.ui.mtjEditorContext" }); //$NON-NLS-1$
    }

    /**
     * Subclasses that wish to provide MTJFormPage -> MTJSourcePage selection
     * persistence should override this and return true.
     */
    protected boolean isSelectionListener() {
        return false;
    }

    /**
     * Allow for programatic selection of the currently selected object by
     * subclasses
     * 
     * @param selectedObject
     */
    protected void setSelectedObject(Object selectedObject) {
        fSelection = selectedObject;
    }

    /**
     * Synchronize the outline page to show a relevant element given the current
     * offset.
     * 
     * @param offset The current offset within the source page
     */
    protected void synchronizeOutlinePage(int offset) {
        IDocumentRange rangeElement = getRangeElement(offset, false);

        synchronizeOutlinePage(rangeElement);
    }

    /**
     * @param rangeElement
     */
    protected void updateHighlightRange(IDocumentRange rangeElement) {
        if (rangeElement != null) {
            setHighlightRange(rangeElement, false);
        } else {
            resetHighlightRange();
        }
    }

    /**
     * @param rangeElement
     */
    protected void updateOutlinePageSelection(Object rangeElement) {
        // Set selection in source outline page if the 'Link with Editor'
        // feature is on
        if (MTJUIPlugin.getDefault().getPreferenceStore().getBoolean(
                "ToggleLinkWithEditorAction.isChecked")) { //$NON-NLS-1$
            // Ensure we have a source outline page
            if ((fOutlinePage instanceof SourceOutlinePage) == false) {
                return;
            }
            SourceOutlinePage outlinePage = (SourceOutlinePage) fOutlinePage;
            // Temporarily remove the listener to prevent a selection event
            // being fired back at this page
            outlinePage.removeAllSelectionChangedListeners();
            if (rangeElement != null) {
                outlinePage.setSelection(new StructuredSelection(rangeElement));
            } else {
                outlinePage.setSelection(StructuredSelection.EMPTY);
            }
            outlinePage.addAllSelectionChangedListeners();
        }
    }
}
