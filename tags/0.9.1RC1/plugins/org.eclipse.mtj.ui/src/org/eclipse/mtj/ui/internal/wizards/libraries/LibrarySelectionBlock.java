/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.ui.internal.wizards.libraries;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.library.manager.LibraryManager;
import org.eclipse.mtj.core.library.model.ILibrary;
import org.eclipse.mtj.core.library.model.Visibility;
import org.eclipse.mtj.internal.core.library.MIDletLibraryClasspathContainer;
import org.eclipse.mtj.ui.internal.preferences.ScrolledPageContent;
import org.eclipse.mtj.ui.internal.utils.PixelConverter;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.CheckedListDialogField;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.DialogField;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.IDialogFieldListener;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.LayoutUtil;
import org.eclipse.mtj.ui.internal.wizards.projects.Messages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;

/**
 * Configures the libraries to be included in the project classpath and exported
 * in the deployable JAR file.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class LibrarySelectionBlock {

    private final static int IDX_SELECT_ALL = 0;

    private final static int IDX_UNSELECT_ALL = 1;

    private static final String PATH_SEPARATOR = "/";

    private final List<String> publicLibsNames;

    private boolean librariesAvailable = false;

    private LibraryInfoBlock libraryInfoBlock;

    private CheckedListDialogField libraryList;

    private Composite LibrarySelectionComposite;

    private Composite parentComposite;

    /**
     * Creates a new LibrarySelectionBlock.
     * <p>
     * <b>Note:</b> Constructing a new block will initialize the internal
     * elements, but will not create the block {@link Control}.
     * </p>
     * <p>
     * Client must call {@link #createControl(Composite)} before start using
     * this block methods.
     * </p>
     */
    public LibrarySelectionBlock() {

        String[] buttonLabels = new String[] {
                /* IDX_SELECT_ALL */Messages.NewMidletProjectWizardPageLibrary_libraryList_checkall_button,
                /* IDX_UNSELECT_ALL */Messages.NewMidletProjectWizardPageLibrary_libraryList_uncheckall_button };

        libraryList = new CheckedListDialogField(null, buttonLabels,
                new LibraryListLabelProvider());
        libraryList
                .setLabelText(Messages.NewMidletProjectWizardPageLibrary_libraryList_label);
        libraryList.setCheckAllButtonIndex(IDX_SELECT_ALL);
        libraryList.setUncheckAllButtonIndex(IDX_UNSELECT_ALL);

        libraryInfoBlock = new LibraryInfoBlock();

        publicLibsNames = new ArrayList<String>();

        /* Retrieve the list of all public libraries available */
        String[] tempLibs = LibraryManager.getInstance()
                .getMidletLibraryNames();

        for (String libname : tempLibs) {
            ILibrary library = LibraryManager.getInstance().getMidletLibrary(
                    libname);

            if ((library != null)
                    && (library.getVisibility() != Visibility.INTERNAL)
                    && (library.getVisibility() != Visibility.INVALID)) {
                publicLibsNames.add(libname);
            }
        }

        if (!publicLibsNames.isEmpty()) {
            librariesAvailable = true;
        }

    }

    /**
     * Sets the check state of all elements
     * 
     * @param state
     */
    public void checkAll(boolean state) {
        libraryList.checkAll(state);
    }

    /**
     * Creates the SWT control for this Block under the given parent control.
     * <p>
     * Clients should call this method.
     * </p>
     * 
     * @param parent the parent control
     * @return the composite were the libraries are displayed.
     */
    public Control createControl(Composite parent) {

        parentComposite = parent;
        PixelConverter converter = new PixelConverter(parentComposite);
        final int[] sashWeight = { 60 };

        LibrarySelectionComposite = createMainComposite(parentComposite);

        Composite scrolledPagecomposite = createScrolledPageContent(LibrarySelectionComposite);

        SashForm sashForm = createSashForm(scrolledPagecomposite);

        createLibraryListComposite(sashForm, converter
                .convertWidthInCharsToPixels(24));

        createExpandableComposite(sashForm, sashWeight);

        sashForm.setWeights(new int[] { 50, 50 });
        adjustSashForm(sashWeight, sashForm, false);

        GridData gd = new GridData(SWT.FILL, SWT.FILL, true, true);
        gd.heightHint = converter.convertHeightInCharsToPixels(20);
        sashForm.setLayoutData(gd);

        parent.layout(true);
        libraryList.getTableViewer().addSelectionChangedListener(
                libraryInfoBlock);
        return LibrarySelectionComposite;
    }

    /**
     * Returns the SWT control for this page.
     * 
     * @return the SWT control for this page, or <code>null</code> if this page
     *         does not have a control yet.
     * @see #createControl(Composite)
     */
    public Control getControl() {
        return LibrarySelectionComposite;
    }

    /**
     * Return a list of paths to the available libraries containers
     * 
     * @return the list of paths to the library containers
     */
    public List<IPath> getLibrariesPathEntries() {

        List<IPath> entryPath = new ArrayList<IPath>();

        List<Object> libs = libraryList.getElements();

        for (Object object : libs) {
            ILibrary type = (ILibrary) object;

            entryPath.add(new Path(
                    MIDletLibraryClasspathContainer.MIDLET_LIBRARY_CONTAINER_ID
                            + PATH_SEPARATOR + type.getName()));
        }
        return entryPath;
    }

    /**
     * Return the list of selected libraries in the selection block
     * 
     * @return the selected libraries.
     */
    public List<Object> getSelectedLibraries() {
        return libraryList.getCheckedElements();
    }

    /**
     * Return a list of paths to the available libraries containers
     * 
     * @return the list of paths to the library containers
     */
    public List<IPath> getSelectedLibrariesPathEntries() {

        List<IPath> entryPath = new ArrayList<IPath>();

        List<Object> libs = getSelectedLibraries();

        for (Object object : libs) {
            ILibrary type = (ILibrary) object;

            entryPath.add(new Path(
                    MIDletLibraryClasspathContainer.MIDLET_LIBRARY_CONTAINER_ID
                            + PATH_SEPARATOR + type.getName()));
        }
        return entryPath;
    }

    /**
     * @return the librariesAvailable
     */
    public boolean isLibrariesAvailable() {
        return librariesAvailable;
    }

    /**
     * Sets the checked state of an element.
     * 
     * @param object
     * @param state
     */
    public void setChecked(Object object, boolean state) {
        libraryList.setCheckedWithoutUpdate(object, state);
    }

    /**
     * Defines the listener for Library Selection dialog field.
     * 
     * @param listener the listener to be set
     */
    public void setLibrarySelectionDialogFieldListener(
            IDialogFieldListener listener) {

        libraryList.setDialogFieldListener(listener);

    }

    /**
     * Adjust the size of the sash form.
     * 
     * @param sashWeight the weight to be read or written
     * @param sashForm the sash form to apply the new weights to
     * @param isExpanded <code>true</code> if the expandable composite is
     *            expanded, <code>false</code> otherwise
     */
    private void adjustSashForm(int[] sashWeight, SashForm sashForm,
            boolean isExpanded) {
        if (isExpanded) {
            int upperWeight = sashWeight[0];
            sashForm.setWeights(new int[] { upperWeight, 100 - upperWeight });
        } else {
            sashWeight[0] = sashForm.getWeights()[0] / 10;
            sashForm.setWeights(new int[] { 95, 5 });
        }
        sashForm.layout(true);
    }

    /**
     * @param parent
     * @param sashWeight
     * @return
     */
    private Composite createExpandableComposite(final SashForm parent,
            final int[] sashWeight) {
        final ExpandableComposite excomposite = new ExpandableComposite(parent,
                SWT.NONE, ExpandableComposite.TWISTIE
                        | ExpandableComposite.CLIENT_INDENT);

        excomposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        excomposite.setFont(parent.getFont());
        excomposite
                .setText(Messages.NewMidletProjectWizardPageLibrary_HintTextGroup_title);
        excomposite.setExpanded(false);

        excomposite.addExpansionListener(new ExpansionAdapter() {

            /* (non-Javadoc)
             * @see org.eclipse.ui.forms.events.ExpansionAdapter#expansionStateChanged(org.eclipse.ui.forms.events.ExpansionEvent)
             */
            @Override
            public void expansionStateChanged(ExpansionEvent e) {

                ScrolledPageContent parentScrolledComposite = getParentScrolledComposite(excomposite);
                if (parentScrolledComposite != null) {
                    boolean expanded = excomposite.isExpanded();
                    parentScrolledComposite.reflow(true);
                    adjustSashForm(sashWeight, parent, expanded);
                }
            }
        });

        excomposite.setClient(libraryInfoBlock.createControl(excomposite));

        return excomposite;
    }

    /**
     * @param parent
     * @param buttonBarWidth
     * @return
     */
    private Composite createLibraryListComposite(Composite parent,
            int buttonBarWidth) {

        Composite libraryListComposite = new Composite(parent, SWT.NONE);

        GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        layout.marginWidth = 0;
        layout.marginHeight = 0;
        libraryListComposite.setLayout(layout);

        libraryListComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
                true, true));

        LayoutUtil.doDefaultLayout(libraryListComposite,
                new DialogField[] { libraryList }, true, SWT.DEFAULT,
                SWT.DEFAULT);

        LayoutUtil.setHorizontalGrabbing(libraryList.getListControl(null));

        libraryList.setButtonsMinWidth(buttonBarWidth);

        initialize();
        return libraryListComposite;
    }

    /**
     * @param parent
     * @return
     */
    private Composite createMainComposite(Composite parent) {
        // Create the main Composite.
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setFont(parent.getFont());

        GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        layout.marginWidth = 0;
        layout.marginHeight = 0;
        composite.setLayout(layout);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        return composite;
    }

    /**
     * @param parent
     * @return
     */
    private SashForm createSashForm(Composite parent) {
        final SashForm sashForm = new SashForm(parent, SWT.VERTICAL | SWT.NONE);
        sashForm.setFont(sashForm.getFont());
        sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        return sashForm;
    }

    /**
     * Create the ScrolledPageContent that is needed for resizing on expand the
     * expandable composite.
     * 
     * @return the ScrolledPageContent
     */
    private Composite createScrolledPageContent(Composite parent) {

        ScrolledPageContent scrolledContent = new ScrolledPageContent(parent);
        scrolledContent.setLayout(new GridLayout());
        scrolledContent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                true));

        Composite body = scrolledContent.getBody();
        body.setLayout(new GridLayout());
        body.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        return body;
    }

    /**
     * Get the scrolled page content of the given control by traversing the
     * parents.
     * 
     * @param control the control to get the scrolled page content for
     * @return the scrolled page content or <code>null</code> if none found
     */
    private ScrolledPageContent getParentScrolledComposite(Control control) {
        Control parent = control.getParent();
        while (!(parent instanceof ScrolledPageContent)) {
            parent = parent.getParent();
        }
        if (parent instanceof ScrolledPageContent) {
            return (ScrolledPageContent) parent;
        }
        return null;
    }

    /**
     * Initialize the CheckedListDialogField with the libraries available
     */
    private void initialize() {

        if (librariesAvailable) {
            for (String libName : publicLibsNames) {
                try {
                    libraryList.addElement(LibraryManager.getInstance()
                            .getMidletLibrary(libName));
                } catch (Exception e) {
                    MTJCorePlugin.log(IStatus.ERROR, "Failed to load \""
                            + libName + "\" Library", e);
                }
            }
        }
    }

    /**
     * 
     */
    protected void doUpdateUI() {
        libraryList.refresh();
    }

    /**
     * 
     */
    protected void updateUI() {

        if ((parentComposite == null) || parentComposite.isDisposed()) {
            return;
        }
        if (Display.getCurrent() != null) {
            doUpdateUI();
        } else {
            Display.getDefault().asyncExec(new Runnable() {
                public void run() {
                    if ((parentComposite == null)
                            || parentComposite.isDisposed()) {
                        return;
                    }
                    doUpdateUI();
                }
            });
        }
    }
}
