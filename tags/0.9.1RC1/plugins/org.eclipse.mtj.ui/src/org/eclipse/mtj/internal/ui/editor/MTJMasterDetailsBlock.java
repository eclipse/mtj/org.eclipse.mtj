/**
 * Copyright (c) 2003, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDEMasterDetailsBlock
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.mtj.ui.internal.editors.FormLayoutFactory;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.MasterDetailsBlock;
import org.eclipse.ui.forms.widgets.Section;

public abstract class MTJMasterDetailsBlock extends MasterDetailsBlock {
    private MTJFormPage fPage;
    private MTJSection fSection;

    public MTJMasterDetailsBlock(MTJFormPage page) {
        fPage = page;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.MasterDetailsBlock#createContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    public void createContent(IManagedForm managedForm) {
        super.createContent(managedForm);
        managedForm.getForm().getBody().setLayout(
                FormLayoutFactory.createFormGridLayout(false, 1));
    }

    /**
     * @return
     */
    public DetailsPart getDetailsPart() {
        return detailsPart;
    }

    /**
     * @return
     */
    public MTJFormPage getPage() {
        return fPage;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.MasterDetailsBlock#createMasterPart(org.eclipse.ui.forms.IManagedForm, org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createMasterPart(final IManagedForm managedForm,
            Composite parent) {
        Composite container = managedForm.getToolkit().createComposite(parent);
        container.setLayout(FormLayoutFactory.createMasterGridLayout(false, 1));
        container.setLayoutData(new GridData(GridData.FILL_BOTH));
        fSection = createMasterSection(managedForm, container);
        managedForm.addPart(fSection);
        Section section = fSection.getSection();
        section.setLayout(FormLayoutFactory.createClearGridLayout(false, 1));
        section.setLayoutData(new GridData(GridData.FILL_BOTH));
    }

    protected abstract MTJSection createMasterSection(IManagedForm managedForm,
            Composite parent);

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.MasterDetailsBlock#createToolBarActions(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createToolBarActions(IManagedForm managedForm) {
    }

}
