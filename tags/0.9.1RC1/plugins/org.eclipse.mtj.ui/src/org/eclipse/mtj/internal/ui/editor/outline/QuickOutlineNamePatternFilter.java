/**
 * Copyright (c) 2006,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.outline;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.mtj.internal.ui.util.StringMatcher;

/**
 * @since 0.9.1
 */
public class QuickOutlineNamePatternFilter extends ViewerFilter {

    private StringMatcher stringMatcher;

    /**
     * Creates a new QuickOutlineNamePatternFilter with an empty StringMatcher.
     */
    public QuickOutlineNamePatternFilter() {
        stringMatcher = null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
     */
    @Override
    public boolean select(Viewer viewer, Object parentElement, Object element) {
        // Element passes the filter if the string matcher is undefined or the
        // viewer is not a tree viewer
        if ((stringMatcher == null)
                || ((viewer instanceof TreeViewer) == false)) {
            return true;
        }
        TreeViewer treeViewer = (TreeViewer) viewer;
        // Match the pattern against the label of the given element
        String matchName = ((ILabelProvider) treeViewer.getLabelProvider())
                .getText(element);
        // Element passes the filter if it matches the pattern
        if ((matchName != null) && stringMatcher.match(matchName)) {
            return true;
        }
        // Determine whether the element has children that pass the filter
        return hasUnfilteredChild(treeViewer, element);
    }

    /**
     * @param stringMatcher
     */
    public void setStringMatcher(StringMatcher stringMatcher) {
        this.stringMatcher = stringMatcher;
    }

    /**
     * @param viewer
     * @param element
     * @return
     */
    private boolean hasUnfilteredChild(TreeViewer viewer, Object element) {
        // No point calling hasChildren() because the operation is the same cost
        // as getting the children
        // If the element has a child that passes the filter, then we want to
        // keep the parent around - even if it does not pass the filter itself
        Object[] children = ((ITreeContentProvider) viewer.getContentProvider())
                .getChildren(element);
        for (Object element2 : children) {
            if (select(viewer, element, element2)) {
                return true;
            }
        }
        // Element does not pass the filter
        return false;
    }

}
