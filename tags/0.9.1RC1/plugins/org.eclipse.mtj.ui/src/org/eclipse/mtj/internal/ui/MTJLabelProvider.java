/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui;

import org.eclipse.mtj.internal.core.text.l10n.L10nEntry;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;
import org.eclipse.mtj.internal.core.text.l10n.L10nObject;
import org.eclipse.mtj.internal.ui.util.SharedLabelProvider;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.graphics.Image;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class MTJLabelProvider extends SharedLabelProvider {

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
     */
    @Override
    public Image getImage(Object obj) {

        if (obj instanceof L10nObject) {
            return getObjectImage((L10nObject) obj);
        }
        return super.getImage(obj);
    }

    /**
     * @param obj
     * @return
     */
    public String getObjectText(L10nObject obj) {
        return obj.getName();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
     */
    @Override
    public String getText(Object obj) {

        if (obj instanceof L10nObject) {
            return getObjectText((L10nObject) obj);
        }

        return super.getText(obj);
    }

    /**
     * @return
     */
    public boolean isFullNameModeEnabled() {
        return MTJUIPlugin.isFullNameModeEnabled();
    }

    /**
     * @param object
     * @return
     */
    private Image getObjectImage(L10nObject object) {

        if (object instanceof L10nLocales) {
            return get(MTJUIPluginImages.DESC_LOCALES_OBJ);
        } else if (object instanceof L10nLocale) {
            return get(MTJUIPluginImages.DESC_LOCALE_OBJ);
        } else if (object instanceof L10nEntry) {
            return get(MTJUIPluginImages.DESC_LOCALE_ENTRY_OBJ);
        } else {
            return get(MTJUIPluginImages.DESC_PAGE_OBJ);
        }
    }

}
