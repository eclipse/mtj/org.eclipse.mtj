/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)  - Initial implementation
 *     Kevin Hunter (Individual) - Added signature support
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse
 *                                 standards
 *     Hugo Raniere (Motorola)  - Removing Preprocessor code
 *     Gang Ma      (Sybase)	- Add jar/jad names configuration support
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code
 *     Feng Wang (Sybase) - 1. Add configurations management UI section for
 *                             multi-configs support.
 *                          2. Remove SymbolDefinitionSet select UI, for we
 *                             select it in ConfigAddAndEditWizardPage.
 */
package org.eclipse.mtj.ui.internal.properties;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.preverification.builder.PreverificationBuilder;
import org.eclipse.mtj.core.internal.utils.ColonDelimitedProperties;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.jad.ApplicationDescriptor;
import org.eclipse.mtj.core.model.jad.IJADConstants;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.core.nature.J2MENature;
import org.eclipse.mtj.ui.MTJUIErrors;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.mtj.ui.configurations.ConfigManageComponent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPropertyPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PropertyPage;

/**
 * Property page implementation for Java ME properties associated with the
 * project.
 * 
 * @author Craig Setera
 * @see PropertyPage
 */
public class J2MEProjectPropertiesPage extends PropertyPage implements
        IWorkbenchPropertyPage {

    public static final String PAGEID = "org.eclipse.mtj.project.J2MEPropertiesPage";

    private Text jadFileNameText;
    private Text jarFileNameText;

    /**
     * Valid the jad file name and jar file name
     */
    private boolean fileNameValid = true;

    private ConfigManageComponent configManager;

    /**
     * add jad/jar filename configuration control to the page
     * 
     * @param parent
     */
    private void addJadJarConfigure(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        composite.setLayout(new GridLayout(2, false));

        IMidletSuiteProject midletProject = getMidletSuiteProject();

        new Label(composite, SWT.NONE).setText(MTJUIStrings
                .getString("J2MEProjectPropertiesPage.JADFileName"));
        jadFileNameText = new Text(composite, SWT.BORDER);
        jadFileNameText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        jadFileNameText.setText(midletProject.getJadFileName());
        jadFileNameText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validatePage();
            }
        });

        new Label(composite, SWT.NONE).setText(MTJUIStrings
                .getString("J2MEProjectPropertiesPage.JarFileName"));
        jarFileNameText = new Text(composite, SWT.BORDER);
        jarFileNameText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        jarFileNameText.setText(midletProject.getJarFilename());
        jarFileNameText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validatePage();
            }
        });

    }

    /**
     * Add multi-configs management section on page.
     * 
     * @param parent
     */
    private void addMultiConfigSection(Composite parent) {
        configManager = new ConfigManageComponent(getMidletSuiteProject());
        configManager.createContents(parent);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse
     * .swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        Control control = null;

        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_J2MEProjectPropertiesPage");

        IProject project = getProject();

        if (isJ2MEProject(project)) {
            Composite composite = new Composite(parent, SWT.NONE);
            composite.setLayout(new GridLayout(1, true));

            addMultiConfigSection(composite);

            // add jad/jar file name configuration control
            addJadJarConfigure(composite);

            control = composite;

        } else {
            Label lbl = new Label(parent, SWT.NONE);
            lbl
                    .setText(MTJUIStrings
                            .getString("J2MEProjectPropertiesPage.NotMidletSuiteProject")); //$NON-NLS-1$
            control = lbl;
        }

        return control;
    }

    @Override
    public void dispose() {
        super.dispose();
        // must call ConfigManageComponent#dispose() when page disposed
        configManager.dispose();
    }

    /**
     * Get the MIDlet suite project for this project.
     * 
     * @return
     */
    private IMidletSuiteProject getMidletSuiteProject() {
        IJavaProject javaProject = JavaCore.create(getProject());
        IMidletSuiteProject midletProject = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);

        return midletProject;
    }

    /**
     * Get the selected project or <code>null</code> if a project is not
     * selected.
     * 
     * @return
     */
    private IProject getProject() {
        IProject project = null;
        IAdaptable adaptable = getElement();

        if (adaptable instanceof IProject) {
            project = (IProject) adaptable;
        } else if (adaptable instanceof IJavaProject) {
            project = ((IJavaProject) adaptable).getProject();
        }

        return project;
    }

    /**
     * Return a boolean indicating whether the selected element is a J2ME
     * project.
     * 
     * @param project
     * @return
     */
    private boolean isJ2MEProject(IProject project) {
        boolean j2meProject = false;

        if (project != null) {
            try {
                j2meProject = project
                        .hasNature(IMTJCoreConstants.MTJ_NATURE_ID);
            } catch (CoreException e) {
            }
        }

        return j2meProject;
    }

    /**
     * Returns <code>true</code> if the page data is currently valid.
     * 
     * @see org.eclipse.jface.preference.PreferencePage#isValid()
     */
    @Override
    public boolean isValid() {
        return (configManager.getActiveConfiguration() != null)
                && fileNameValid;
    }

    @Override
    public boolean performCancel() {
        configManager.performCancel();
        return super.performCancel();
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        boolean succeeded = false;
        IProject project = getProject();

        try {
            if (J2MENature.hasMtjCoreNature(project)) {
                ProgressMonitorDialog dialog = new ProgressMonitorDialog(
                        getShell());
                dialog.setCancelable(false);
                dialog.setOpenOnRun(true);

                try {
                    final IDevice device = configManager
                            .getActiveConfiguration().getDevice();
                    final IMidletSuiteProject midletProject = getMidletSuiteProject();
                    final String newJadFileName = jadFileNameText.getText();
                    final String newJarFileName = jarFileNameText.getText();
                    dialog.run(true, false, new IRunnableWithProgress() {

                        /*
                         * (non-Javadoc)
                         * @see
                         * org.eclipse.jface.operation.IRunnableWithProgress
                         * #run(org.eclipse.core.runtime.IProgressMonitor)
                         */
                        public void run(IProgressMonitor monitor)
                                throws InvocationTargetException,
                                InterruptedException {

                            try {
                                midletProject.setDevice(device, monitor);
                                boolean needCleanProject = false;
                                // the jad file name has been changed
                                if (!midletProject.getJadFileName()
                                        .equalsIgnoreCase(newJadFileName)) {

                                    // must set the new jad file's location
                                    midletProject
                                            .setJadFileName(newJadFileName);
                                }
                                // the jar file name has been changed
                                if (!midletProject.getJarFilename()
                                        .equalsIgnoreCase(newJarFileName)) {
                                    ApplicationDescriptor appDescriptor = midletProject
                                            .getApplicationDescriptor();
                                    ColonDelimitedProperties jadProperties = appDescriptor
                                            .getManifestProperties();
                                    // Update the jar file URL
                                    jadProperties.setProperty(
                                            IJADConstants.JAD_MIDLET_JAR_URL,
                                            newJarFileName);

                                    try {
                                        appDescriptor.store();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    needCleanProject = true;
                                }
                                configManager.performFinish();
                                midletProject.saveMetaData();
                                // need to clean the project
                                if (needCleanProject) {
                                    PreverificationBuilder.cleanProject(
                                            midletProject.getProject(), true,
                                            monitor);
                                }
                                midletProject.getProject().build(
                                        IncrementalProjectBuilder.FULL_BUILD,
                                        monitor);

                            } catch (CoreException e) {
                                throw new InvocationTargetException(e);
                            }
                        }
                    });

                    succeeded = true;

                } catch (InvocationTargetException e) {
                    MTJUIErrors.displayError(getShell(),
                            "MTJUiError.Exception", //$NON-NLS-1$
                            "MTJUiError.SetPlatformFailed", //$NON-NLS-1$
                            e);
                } catch (InterruptedException e) {
                    // Ignore this
                } finally {
                    dialog.close();
                }
            }
        } catch (CoreException e) {
            MTJUIErrors.displayError(getShell(), "MTJUiError.Exception", //$NON-NLS-1$
                    "MTJUiError.SetPlatformFailed", //$NON-NLS-1$
                    e);
        }

        return succeeded;
    }

    /**
     * check whether the page is valid
     */
    private void validatePage() {
        String message = null;

        IWorkspace workspace = ResourcesPlugin.getWorkspace();
        IStatus result;
        String jadFileName = jadFileNameText.getText();
        result = workspace.validateName(jadFileName, IResource.FILE);
        if (!result.isOK()) {
            message = result.getMessage();
        } else if (!jadFileName.endsWith(".jad")) {
            message = "JAD file name must end with .jad";
        }

        String jarFileName = jarFileNameText.getText();
        result = workspace.validateName(jarFileName, IResource.FILE);
        if (!result.isOK()) {
            message = result.getMessage();
            ;
        } else if (!jarFileName.endsWith(".jar")) {
            message = "Jar file name must end with .jar";
        }

        setErrorMessage(message);
        if (message != null) {
            fileNameValid = false;
        } else {
            fileNameValid = true;
        }
        updateApplyButton();
        getContainer().updateButtons();
    }

}
