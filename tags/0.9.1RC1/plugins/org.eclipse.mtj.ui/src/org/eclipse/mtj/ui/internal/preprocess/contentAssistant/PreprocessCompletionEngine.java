/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mtj.core.internal.preprocessor.PreprocessorHelper;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.graphics.Image;

/**
 * this class is responsible for preprocess directive and symbol completion.
 * 
 * @author gma
 * @since 0.9.1
 */
public class PreprocessCompletionEngine {
    /**
     * @param ppContext the preprocess context
     * @return list of acceptable <code>PreprocessCompletionProposal</code>
     */
    public List<PreprocessCompletionProposal> completeDebugLevel(
            PreprocessContext ppContext) {
        List<PreprocessCompletionProposal> result = new ArrayList<PreprocessCompletionProposal>();
        
        PreprocessDebugLevel[] levels = getSupportDebuglevels();
        
        for (PreprocessDebugLevel level : levels) {
            if (ppContext.accept(level)) {
                result
                        .add(constructPreprocessProposal(
                                ppContext,
                                level,
                                MTJUIPlugin
                                        .getImageFromCache("preprocess_symbol_assist.gif")));
            }
        }
        return result;
    }

    private PreprocessDebugLevel[] getSupportDebuglevels() {
        PreprocessDebugLevel[] levels = new PreprocessDebugLevel[] {
                new PreprocessDebugLevel(PreprocessorHelper.J2ME_PREPROCESS_DEBUG,
                        PreprocessContentAssistMessages.dl_debug_desc),
                new PreprocessDebugLevel(PreprocessorHelper.J2ME_PREPROCESS_ERROR,
                        PreprocessContentAssistMessages.dl_error_desc),
                new PreprocessDebugLevel(PreprocessorHelper.J2ME_PREPROCESS_FATAL,
                        PreprocessContentAssistMessages.dl_fatal_desc),
                new PreprocessDebugLevel(PreprocessorHelper.J2ME_PREPROCESS_INFO,
                        PreprocessContentAssistMessages.dl_info_desc),
                new PreprocessDebugLevel(PreprocessorHelper.J2ME_PREPROCESS_WARN,
                        PreprocessContentAssistMessages.dl_warn_desc), };
        return levels;
    }

    /**
     * @param ppContext the preprocess context
     * @return list of acceptable <code>PreprocessCompletionProposal</code>
     */
    public List<PreprocessCompletionProposal> completeDirective(
            PreprocessContext ppContext) {
        List<PreprocessCompletionProposal> result = new ArrayList<PreprocessCompletionProposal>();

        IPreprocessDirectiveProvider directiveProvider = ppContext
                .getDirectiveProvider();
        PreprocessDirective[] directives = directiveProvider.getAllDirectives();

        for (PreprocessDirective directive : directives) {
            if (ppContext.accept(directive)) {
                result
                        .add(constructPreprocessProposal(
                                ppContext,
                                directive,
                                MTJUIPlugin
                                        .getImageFromCache("preprocess_directive_assist.gif")));
            }
        }
        return result;

    }

    /**
     * @param ppContext the preprocess context
     * @return list of acceptable <code>PreprocessCompletionProposal</code>
     */
    public List<PreprocessCompletionProposal> completeSymbol(
            PreprocessContext ppContext) {
        List<PreprocessCompletionProposal> result = new ArrayList<PreprocessCompletionProposal>();

        List<PreprocessSymbol> symbols = PreprocessSymbolManager
                .getSymbols(ppContext.getContainedProject());
        for (PreprocessSymbol symbol : symbols) {
            if (ppContext.accept(symbol)) {
                result
                        .add(constructPreprocessProposal(
                                ppContext,
                                symbol,
                                MTJUIPlugin
                                        .getImageFromCache("preprocess_symbol_assist.gif")));
            }
        }
        return result;
    }

    /**
     * construct the PreprocessCompletionProposal
     * 
     * @param ppContext
     * @param model
     * @param image
     * @return
     */
    private PreprocessCompletionProposal constructPreprocessProposal(
            PreprocessContext ppContext, IPreprocessContentAssistModel model,
            Image image) {

        return new PreprocessCompletionProposal(model, ppContext, image);

    }

}
