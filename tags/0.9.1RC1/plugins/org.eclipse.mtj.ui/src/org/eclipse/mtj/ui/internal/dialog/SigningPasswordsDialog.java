/**
 * Copyright (c) 2004,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 */
package org.eclipse.mtj.ui.internal.dialog;

import java.text.MessageFormat;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Dialog for obtaining keystore and key passwords from the user.
 * 
 * @author Kevin Hunter
 */
public class SigningPasswordsDialog extends TitleAreaDialog {
    private static final String DIALOG_TITLE = "Enter passwords";
    private static final String DIALOG_MESSAGE1 = "Enter passwords for project \"{0}\"";
    private static final String DIALOG_MESSAGE2 = "Enter passwords for project";
    private static final String KEYSTORE_PWD_LABEL = "Keystore password:";
    private static final String KEY_PWD_LABEL = "Key password:";
    private Text keystorePasswordText;

    private Text keyPasswordText;

    private String keystorePassword;

    private String keyPassword;

    private String dialogMessage;

    private Button okButton;

    /**
     * @param parentShell
     */
    public SigningPasswordsDialog(Shell parentShell, IProject project) {
        super(parentShell);

        try {
            IProjectDescription description = project.getDescription();
            String projectName = description.getName();
            String[] args = new String[] { projectName };
            dialogMessage = MessageFormat.format(DIALOG_MESSAGE1,
                    (Object[]) args);
        } catch (CoreException e) {
            dialogMessage = DIALOG_MESSAGE2;
        }
    }

    /**
     * @return
     */
    public String getKeyPassword() {
        return (keyPassword);
    }

    /**
     * @return
     */
    public String getKeystorePassword() {
        return (keystorePassword);
    }

    /**
     * @param value
     */
    public void setKeyPassword(String value) {
        keyPassword = value;
    }

    /**
     * @param value
     */
    public void setKeystorePassword(String value) {
        keystorePassword = value;
    }

    /**
     * @param nFill
     * @param bGrab
     * @param nSpan
     * @return
     */
    private GridData buildGridData(int nFill, boolean bGrab, int nSpan) {
        GridData gd = new GridData();

        gd.horizontalAlignment = nFill;
        gd.grabExcessHorizontalSpace = bGrab;
        gd.horizontalSpan = nSpan;

        return (gd);
    }

    /**
     * 
     */
    private void updateButtons() {
        if ((okButton == null) || (keystorePasswordText == null)
                || (keyPasswordText == null)) {
            return;
        }

        String value = keystorePasswordText.getText();
        if (value == null) {
            okButton.setEnabled(false);
            return;
        }
        if (value.length() == 0) {
            okButton.setEnabled(false);
            return;
        }
        value = keyPasswordText.getText();
        if (value == null) {
            okButton.setEnabled(false);
            return;
        }
        if (value.length() == 0) {
            okButton.setEnabled(false);
            return;
        }
        okButton.setEnabled(true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        okButton = createButton(parent, IDialogConstants.OK_ID,
                IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID,
                IDialogConstants.CANCEL_LABEL, false);
        updateButtons();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.TitleAreaDialog#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        Control control = super.createContents(parent);
        setTitle(DIALOG_TITLE);
        setMessage(dialogMessage, IMessageProvider.INFORMATION);
        return control;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.TitleAreaDialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createDialogArea(Composite parent) {
        Composite dialogArea = new Composite((Composite) super
                .createDialogArea(parent), SWT.NONE);
        dialogArea.setLayoutData(new GridData(GridData.FILL_BOTH));

        GridLayout layout = new GridLayout(2, false);
        dialogArea.setLayout(layout);

        Label label = new Label(dialogArea, SWT.NONE);
        label.setText(KEYSTORE_PWD_LABEL);

        keystorePasswordText = new Text(dialogArea, SWT.SINGLE | SWT.BORDER
                | SWT.PASSWORD);
        keystorePasswordText.setLayoutData(buildGridData(SWT.FILL, true, 1));
        if (keystorePassword != null) {
            keystorePasswordText.setText(keystorePassword);
        }

        label = new Label(dialogArea, SWT.NONE);
        label.setText(KEY_PWD_LABEL);

        keyPasswordText = new Text(dialogArea, SWT.SINGLE | SWT.BORDER
                | SWT.PASSWORD);
        keyPasswordText.setLayoutData(buildGridData(SWT.FILL, true, 1));
        if (keyPassword != null) {
            keyPasswordText.setText(keyPassword);
        }

        keystorePasswordText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                updateButtons();
            }
        });

        keyPasswordText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                updateButtons();
            }
        });

        updateButtons();

        return (dialogArea);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.Dialog#okPressed()
     */
    @Override
    protected void okPressed() {
        keystorePassword = keystorePasswordText.getText();
        keyPassword = keyPasswordText.getText();

        super.okPressed();
    }
}
