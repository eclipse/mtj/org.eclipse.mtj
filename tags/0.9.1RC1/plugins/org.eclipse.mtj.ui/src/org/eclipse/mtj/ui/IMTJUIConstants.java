/**
 * Copyright (c) 2004,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 *     Diego Sandin (Motorola)   - Added new constants 
 *     Gang  Ma     (Sybase)     - Added new constants                             
 */
package org.eclipse.mtj.ui;

import org.eclipse.core.runtime.QualifiedName;

/**
 * @author Kevin Hunter
 */
public interface IMTJUIConstants {

    /**
     * The <code>org.eclipse.mtj.ui</code> plug-in ID
     */
    public static final String PLUGIN_ID = "org.eclipse.mtj.ui";

    QualifiedName PROPERTY_EDITOR_PAGE_KEY = new QualifiedName(PLUGIN_ID,
            "editor-page-key"); //$NON-NLS-1$

    /**
     * 
     */
    public static final String PLUGIN_ROOT = "/";

    /**
     * The help icon filename
     */
    public static final String IMG_LINKTOHELP = "linkto_help.gif";

    public static final String IMG_DEBUG_MIDLET = "debug_exc.gif";
    public static final String IMG_DEBUG_JAD = "debug_exc.gif";
    public static final String IMG_DEBUG_OTA = "debug_exc.gif";

    public static final String IMG_ANT = "ant.gif";

    public static final String IMG_PACKAGE = "library.gif";

    public static final String IMG_PACKAGE_OBFUSCATED = "library.gif";

    public static final String IMG_RUN_MIDLET = "run_exc.gif";
    public static final String IMG_RUN_JAD = "run_exc.gif";
    public static final String IMG_RUN_OTA = "run_exc.gif";
    /**
     * Icons resource folder name
     */
    public static final String ICONS_RES_FOLDER = "icons";

    public static final String IMG_HORIZONTAL = "th_horizontal.gif";

    public static final String IMG_VERTICAL = "th_vertical.gif";

    public static final String IMG_MIDLET_ICON = "class_obj_green.gif";

    public static final String IMG_LIBRARY = "full/obj16/library_obj.gif";

    /**
     * A named preference that controls if templates are formatted when applied.
     * <p>
     * Value is of type <code>Boolean</code>.
     * </p>
     * 
     * @since 0.9.1
     */
    public static final String TEMPLATES_USE_CODEFORMATTER = "org.eclipse.mtj.ui.template.format";

    /**
     * @since 0.9.1
     */
    public static final String LOCALIZATION_DATA_EDITOR = "org.eclipse.mtj.ui.editor.LocalizationDataEditor";

}
