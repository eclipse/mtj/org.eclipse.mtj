/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.mtj.internal.ui.IPreferenceConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.editor.text.ColorManager;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.dialogs.PreferencesUtil;

/**
 * @since 0.9.1
 */
public class EditorPreferencePage extends PreferencePage implements
        IWorkbenchPreferencePage, IPreferenceConstants {

    private ColorManager fColorManager;
    private XMLSyntaxColorTab fXMLTab;

    /**
     * Create the EditorPreferencePage.
     */
    public EditorPreferencePage() {
        setDescription(MTJUIMessages.EditorPreferencePage_description);
        fColorManager = new ColorManager();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.DialogPage#dispose()
     */
    @Override
    public void dispose() {
        fColorManager.disposeColors(false);
        fXMLTab.dispose();
        super.dispose();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        fXMLTab.performOk();
        MTJUIPlugin.getDefault().savePluginPreferences();
        return super.performOk();
    }

    /**
     * @param folder
     */
    private void createXMLTab(TabFolder folder) {
        fXMLTab = new XMLSyntaxColorTab(fColorManager);
        TabItem item = new TabItem(folder, SWT.NONE);
        item.setText(MTJUIMessages.EditorPreferencePage_localization_tab_title);
        item.setControl(fXMLTab.createContents(folder));
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        final Link link = new Link(parent, SWT.NONE);
        final String target = "org.eclipse.ui.preferencePages.GeneralTextEditor"; //$NON-NLS-1$
        link
                .setText(MTJUIMessages.EditorPreferencePage_generalTextEditor_link);
        link.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                PreferencesUtil.createPreferenceDialogOn(link.getShell(),
                        target, null, null);
            }
        });

        GridData gd = new GridData(GridData.HORIZONTAL_ALIGN_BEGINNING);

        Button foldingButton = new Button(parent, SWT.CHECK | SWT.LEFT
                | SWT.WRAP);
        foldingButton.setText(MTJUIMessages.EditorPreferencePage_folding_label);
        foldingButton.setLayoutData(gd);
        foldingButton.setSelection(MTJUIPlugin.getDefault()
                .getPreferenceStore().getBoolean(
                        IPreferenceConstants.EDITOR_FOLDING_ENABLED));
        foldingButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                IPreferenceStore store = MTJUIPlugin.getDefault()
                        .getPreferenceStore();
                store.setValue(IPreferenceConstants.EDITOR_FOLDING_ENABLED,
                        ((Button) e.getSource()).getSelection());
            }
        });

        TabFolder folder = new TabFolder(parent, SWT.NONE);
        folder.setLayout(new TabFolderLayout());
        folder.setLayoutData(new GridData(GridData.FILL_BOTH));

        createXMLTab(folder);

        Dialog.applyDialogFont(getControl());

        return parent;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
     */
    @Override
    protected void performDefaults() {
        fXMLTab.performDefaults();
        super.performDefaults();
    }
}
