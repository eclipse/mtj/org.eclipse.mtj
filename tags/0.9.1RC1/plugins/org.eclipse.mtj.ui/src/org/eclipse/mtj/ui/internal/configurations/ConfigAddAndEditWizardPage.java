/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.ui.internal.configurations;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICellEditorListener;
import org.eclipse.jface.viewers.ICellEditorValidator;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.configuration.Configuration;
import org.eclipse.mtj.core.model.configuration.Configurations;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSetRegistry;
import org.eclipse.mtj.core.model.preprocessor.symbol.Symbol;
import org.eclipse.mtj.core.model.preprocessor.symbol.SymbolSet;
import org.eclipse.mtj.core.model.preprocessor.symbol.SymbolSetFactory;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MetaData;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.ui.devices.DeviceSelector;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.preferences.SymbolDefinitionsPreferencePage;
import org.eclipse.mtj.ui.viewers.TableColumnInfo;
import org.eclipse.mtj.ui.viewers.TableViewerConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

/**
 * Wizard Page for Configuration add and edit.
 * 
 * @author wangf
 */
public class ConfigAddAndEditWizardPage extends WizardPage {
    /**
     * A cell modifier implementation for the device libraries editor
     */
    private class CellModifier implements ICellModifier {

        private Symbol symbol;

        public boolean canModify(Object element, String property) {
            return true;
        }

        public Object getValue(Object element, String property) {
            String value = null;

            Symbol symbol = (Symbol) element;
            this.symbol = symbol;
            if (property.equals(PROP_SYMBOL)) {
                value = symbol.getName();
            } else {
                value = symbol.getValue();
            }
            return value;
        }

        public void modify(Object element, String property, Object value) {

            if (symbol != null) {
                if (property.equals(PROP_SYMBOL)) {
                    String newSymbolName = (String) value;
                    if (symbol.getName().equals(newSymbolName)) {
                        return;
                    }
                    if (isValidSymbol(newSymbolName)) {
                        // 1. We must remove symbol from symbolSet first
                        symbolSet.remove(symbol);
                        // 2. Then we change name of the symbol
                        symbol.setName(newSymbolName);
                        // 3. Finally we add the symbol into symbolSet
                        symbolSet.add(symbol);
                        // If we don't do step 1&3, symbolSet.contains(symbol)
                        // will return false, so we will cannot remove the
                        // symbol
                        // 4. mark symbolSet changed
                        symbolSetChanged = true;
                    }
                } else {
                    if (symbol.getValue().equals(value)) {
                        return;
                    }
                    symbol.setValue(String.valueOf(value));
                    // mark symbolSet changed
                    symbolSetChanged = true;
                }

                symbolsTableViewer.refresh();
            }
        }
    }

    /**
     * Cell editor that includes validation of the Symbol name
     */
    private class SymbolNameCellEditor extends TextCellEditor {

        /**
         * @param parent
         */
        public SymbolNameCellEditor(Composite parent) {
            super(parent);
            setValidator(new SymbolNameCellEditorValidator());
            addListener(new ICellEditorListener() {
                public void applyEditorValue() {
                }

                public void cancelEditor() {
                }

                public void editorValueChanged(boolean oldValidState,
                        boolean newValidState) {
                    if (!newValidState) {
                        setErrorMessage(getErrorMessage());
                    } else {
                        setErrorMessage(null);
                    }
                }
            });
        }

        @Override
        protected void doSetValue(Object value) {
            if (isValueValid()) {
                super.doSetValue(value);
            }
        }
    }

    /**
     * Validates that the value for the symbol is a valid value
     */
    private class SymbolNameCellEditorValidator implements ICellEditorValidator {
        public String isValid(Object value) {
            String symbol = (String) value;
            return isValidSymbol(symbol) ? null
                    : "Whitespace not allowed in symbol names.";
        }
    }

    private class SymbolsLabelProvider extends LabelProvider implements
            ITableLabelProvider {

        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        public String getColumnText(Object element, int columnIndex) {
            Symbol def = (Symbol) element;
            switch (columnIndex) {
            case 0:
                return def.getName();
            case 1:
                return def.getValue();
            default:
                return "";
            }
        }
    }

    private class SymbolsTableContentProvider implements
            IStructuredContentProvider {

        public void dispose() {
        }

        public Object[] getElements(Object inputElement) {
            SymbolSet symbolSet = (SymbolSet) inputElement;
            return symbolSet.toArray(new Symbol[symbolSet.size()]);
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

    }

    private class WorkSpaceSymbolSetTableContentProvider implements
            IStructuredContentProvider {

        public void dispose() {
        }

        public Object[] getElements(Object inputElement) {
            return (SymbolDefinitionSet[]) inputElement;
        }

        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        }

    }

    private class WorkSpaceSymbolSetTableLabelProvider extends LabelProvider
            implements ITableLabelProvider {

        private static final int COL_CHECKBOX = 0;
        private static final int COL_STMBOLSET_NAME = 1;

        public Image getColumnImage(Object element, int columnIndex) {
            return null;
        }

        public String getColumnText(Object element, int columnIndex) {
            SymbolDefinitionSet symbolSet = (SymbolDefinitionSet) element;

            switch (columnIndex) {
            case COL_CHECKBOX:
                return "";
            case COL_STMBOLSET_NAME:
                return symbolSet.getName();
            default:
                return "";
            }
        }

    }

    // private static final String SYMBOL_DEFS_VIEWER_SETTINGS =
    // "symbolDefsViewerSettings";
    private static final String CONFIG_WIZARD_SETTINGS = "ConfigWizardSettings";

    public static final String NAME = "configurationAddPage";
    private IMidletSuiteProject midletSuiteProject;
    private Configurations configurations;
    private Configuration configuration;
    private SymbolSet symbolSet;

    private List<Symbol> addedSymbols;
    private List<Symbol> removedSymbols;
    private boolean inEditMode;
    private boolean needChangeConfigName = true;
    private boolean symbolSetChanged;

    private DeviceSelector deviceSelector;

    private Text configNameText;

    private TableViewer symbolsTableViewer;

    private Button addSymbolButton;

    private Button removeSymbolButton;
    private CheckboxTableViewer workSpaceSymbolSetViewer;

    // Column property names
    private static final String PROP_SYMBOL = "symbol";

    private static final String PROP_VALUE = "value";

    // All of the properties in order
    private static final String[] PROPERTIES = new String[] { PROP_SYMBOL,
            PROP_VALUE };

    public ConfigAddAndEditWizardPage(Configurations configurations,
            Configuration currentConfig) {
        super(NAME);
        this.configurations = configurations;
        init(currentConfig);
    }

    /**
     * Add configuration name UI section to wizard page.
     * 
     * @param composite
     */
    private void addConfigurationNameSection(Composite composite) {
        Label label = new Label(composite, SWT.NONE);
        label
                .setText(ConfigurationMessages.ConfigurationAddWizardPage_NewConfigurationName);
        configNameText = new Text(composite, SWT.BORDER);
        configNameText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        configNameText.addModifyListener(new ModifyListener() {

            public void modifyText(ModifyEvent e) {
                // If user change config name manually, config name will not
                // change automatically according device name.
                if (needChangeConfigName) {
                    needChangeConfigName = false;
                }
                validateControls();
            }

        });
    }

    /**
     * Add Device Selector UI section to wizard page.
     * 
     * @param composite
     */
    private void addDeviceSelectorSection(Composite composite) {
        deviceSelector = new DeviceSelector();
        deviceSelector.createContents(composite, true, true);
        deviceSelector
                .setSelectionChangedListener(new ISelectionChangedListener() {

                    public void selectionChanged(SelectionChangedEvent event) {
                        if (inEditMode) {
                            validateControls();
                            return;
                        }
                        IDevice device = deviceSelector.getSelectedDevice();
                        if (device == null) {
                            return;
                        }
                        if (needChangeConfigName) {
                            configNameText.setText(device.getName());
                            needChangeConfigName = true;
                        }
                        symbolSet = SymbolSetFactory.createSymbolSet(device);
                        symbolsTableViewer.setInput(symbolSet);
                        validateControls();
                    }

                });
    }

    /**
     * The add symbol button has been selected.
     */
    private void addSymbol() {
        if (symbolSet == null) {
            return;
        }
        // Find a new symbol name that doesn't already exist
        // in the list
        String symbolName = "NewSymbol";
        Symbol symbol = null;
        for (int i = 1; i < 100; i++) {
            symbolName = "NewSymbol" + i;
            symbol = new Symbol(symbolName, "true");
            if (!symbolSet.contains(symbol)) {
                symbolSet.add(symbol);
                recordAddedSymbols(symbol);
                break;
            }
        }
        symbolsTableViewer.refresh();
        // set the added symbols selected
        makeAddedSymbolSelected(symbol);
        // mark the symbolSet changed
        symbolSetChanged = true;
    }

    /**
     * Add symbol manipulate buttons on UI
     * 
     * @param parent
     */
    private void addSymbolButtons(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, true));

        addSymbolButton = new Button(composite, SWT.PUSH);
        addSymbolButton.setText("Add");
        addSymbolButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        addSymbolButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                addSymbol();
            }
        });

        removeSymbolButton = new Button(composite, SWT.PUSH);
        removeSymbolButton.setText("Remove");
        removeSymbolButton
                .setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        removeSymbolButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                removeSymbol();
            }
        });
    }

    /**
     * Add SymbolSet table viewer UI section.
     * 
     * @param composite
     */
    private void addSymbolSetSection(Composite composite) {
        Group symbolSetGroup = new Group(composite, SWT.FILL);
        symbolSetGroup.setLayout(new GridLayout(2, false));
        GridData gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 2;
        symbolSetGroup.setLayoutData(gd);
        symbolSetGroup.setText(ConfigurationMessages.Symbols);

        GridData gridData = new GridData(GridData.FILL_BOTH);
        gridData.minimumWidth = 400;
        gridData.heightHint = 300;
        symbolsTableViewer = createSymbolSetTableViewer(symbolSetGroup);
        symbolsTableViewer.getTable().setLayoutData(gridData);

        addSymbolButtons(symbolSetGroup);
    }

    private void addWorkSpaceScopeSymbolSetChooseSection(Composite composite) {
        Group group = new Group(composite, SWT.FILL);
        group.setLayout(new GridLayout(2, false));
        GridData gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 2;
        group.setLayoutData(gd);
        group.setText(ConfigurationMessages.WorkspaceSymbolSetViewer_GroupText);

        final int DEFAULT_TABLE_WIDTH = 400;
        final int DEFAULT_TABLE_HIGHT = 100;

        gd = new GridData();

        int styles = SWT.MULTI | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION
                | SWT.CHECK;
        Table table = new Table(group, styles);
        gd = new GridData(GridData.FILL_BOTH);
        gd.heightHint = DEFAULT_TABLE_HIGHT;
        gd.widthHint = DEFAULT_TABLE_WIDTH;
        table.setLayoutData(gd);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        workSpaceSymbolSetViewer = new CheckboxTableViewer(table);
        workSpaceSymbolSetViewer
                .setContentProvider(new WorkSpaceSymbolSetTableContentProvider());
        workSpaceSymbolSetViewer
                .setLabelProvider(new WorkSpaceSymbolSetTableLabelProvider());

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings(CONFIG_WIZARD_SETTINGS);
        TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
                new TableColumnInfo(
                        ConfigurationMessages.WorkspaceSymbolSetViewer_ColumnTitle_Choose,
                        20f, null),
                new TableColumnInfo(
                        ConfigurationMessages.WorkspaceSymbolSetViewer_ColumnTitle_SymbolSet,
                        80f, null) };
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 1);
        viewerConfiguration.configure(workSpaceSymbolSetViewer);

        Button workspaceSymbolSetManageButton = new Button(group, SWT.PUSH);
        workspaceSymbolSetManageButton
                .setText(ConfigurationMessages.WorkspaceSymbolSetViewer_ManageButton);
        workspaceSymbolSetManageButton
                .addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        Shell shell = e.widget.getDisplay().getActiveShell();
                        PreferenceManager manager = PlatformUI.getWorkbench()
                                .getPreferenceManager();
                        PreferenceDialog dialog = new PreferenceDialog(shell,
                                manager);
                        dialog
                                .setSelectedNode(SymbolDefinitionsPreferencePage.ID);
                        if (dialog.open() == Window.OK) {
                            initWorkspaceSymbolSetViewer();
                        }
                    }
                });
    }

    public void createControl(Composite parent) {
        parent.setLayoutData(new GridData(GridData.FILL_BOTH));
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(2, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        addConfigurationNameSection(composite);

        addDeviceSelectorSection(composite);

        addSymbolSetSection(composite);

        addWorkSpaceScopeSymbolSetChooseSection(composite);

        initializeControls();

        setControl(composite);
    }

    /**
     * Create SymbolSet table viewer.
     * 
     * @param composite
     * @return
     */
    private TableViewer createSymbolSetTableViewer(Composite composite) {
        int styles = SWT.MULTI | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION;

        final Table table = new Table(composite, styles);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        // Wire up the viewer
        TableViewer viewer = new TableViewer(table);
        viewer.setContentProvider(new SymbolsTableContentProvider());
        viewer.setLabelProvider(new SymbolsLabelProvider());
        viewer.setSorter(new ViewerSorter());

        IDialogSettings viewerSettings = MTJUIPlugin
                .getDialogSettings(CONFIG_WIZARD_SETTINGS);
        int DEFAULT_TABLE_WIDTH = 650;
        TableColumnInfo[] COLUMN_INFO = new TableColumnInfo[] {
                new TableColumnInfo("Symbol", 40f, null),
                new TableColumnInfo("Value", 60f, null) };
        TableViewerConfiguration viewerConfiguration = new TableViewerConfiguration(
                viewerSettings, DEFAULT_TABLE_WIDTH, COLUMN_INFO, 0);
        viewerConfiguration.configure(viewer);

        // Wire up the cell modification handling
        viewer.setCellModifier(new CellModifier());
        viewer.setColumnProperties(PROPERTIES);
        viewer.setCellEditors(new CellEditor[] {
                new SymbolNameCellEditor(table), new TextCellEditor(table) });

        viewer.addSelectionChangedListener(new ISelectionChangedListener() {

            public void selectionChanged(SelectionChangedEvent event) {
                validateControls();
            }

        });

        return viewer;
    }

    /**
     * Get the Configuration in edit/add.
     * 
     * @return
     */
    public Configuration getConfiguration() {
        return configuration;
    }

    /**
     * Return the number of devices available.
     * 
     * @return the number of devices available.
     */
    private int getDeviceCount() {
        int count = 0;

        /* Check the number of devices already registered */
        DeviceRegistry registry = DeviceRegistry.singleton;

        try {
            count = registry.getDeviceCount();
        } catch (PersistenceException e) {
            MTJCorePlugin.log(IStatus.WARNING, "Error retrieving device count", //$NON-NLS-1$
                    e);
        }
        return count;
    }

    private void init(Configuration currentConfig) {
        this.configuration = currentConfig;
        if (currentConfig != null) {
            inEditMode = true;
        }
        if (inEditMode) {
            setTitle(ConfigurationMessages.EditConfiguration);
            setDescription(ConfigurationMessages.ConfigAddAndEditWizardPage_EditConfigDescription);

        } else {
            setTitle(ConfigurationMessages.AddConfiguration);
            setDescription(ConfigurationMessages.ConfigAddAndEditWizardPage_AddConfigDescription);
        }
    }

    /**
     * Initialize controls according model data.
     */
    private void initializeControls() {
        if (configuration == null) {
            deviceSelector.setSelectedDevice(null);
        } else {
            deviceSelector.setSelectedDevice(configuration.getDevice());
            configNameText.setText(configuration.getName());
            symbolSet = configuration.getSymbolSet();
            symbolsTableViewer.setInput(symbolSet);
        }
        initWorkspaceSymbolSetViewer();
        validateControls();
        // Clear error message after the wizard launching
        setErrorMessage(null);
    }

    private void initWorkspaceSymbolSetViewer() {
        try {
            workSpaceSymbolSetViewer
                    .setInput(SymbolDefinitionSetRegistry.singleton
                            .getAllSetDefinitions());
        } catch (PersistenceException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
            return;
        }
        if (configuration != null) {
            List<SymbolDefinitionSet> symbolSets = configuration
                    .getWorkspaceScopeSymbolSets();
            workSpaceSymbolSetViewer.setCheckedElements(symbolSets.toArray());
        }
    }

    /**
     * Return a boolean indicating whether the specified symbol is valid.
     * 
     * @param symbol
     * @return
     */
    private boolean isValidSymbol(String symbol) {
        boolean valid = false;
        Pattern WHITESPACE_PATTERN = Pattern.compile(".*\\s+.*");
        if (symbol != null) {
            Matcher matcher = WHITESPACE_PATTERN.matcher(symbol);
            valid = !matcher.matches();
        }

        return valid;
    }

    /**
     * Set a symbol as selected in SymbolSet table viewer.
     * 
     * @param addedSymbol
     */
    private void makeAddedSymbolSelected(Symbol addedSymbol) {
        TableItem[] items = symbolsTableViewer.getTable().getItems();
        int pos = 0;
        for (; pos < items.length; pos++) {
            if (addedSymbol.equals(items[pos].getData())) {
                break;
            }
        }
        symbolsTableViewer.getTable().select(pos);
        symbolsTableViewer.getTable().forceFocus();
    }

    /**
     * Should be called by parent wizard's performCancel() method. To do restore
     * work.
     */
    public void performCancel() {
        restoreSymbolSet();
    }

    /**
     * Should be called by parent wizard's performFinish() method. To modify
     * Configurations.
     */
    public void performFinish() {
        String configName = configNameText.getText().trim();
        if (inEditMode) {
            configuration.setName(configName);
            configuration.setDevice(deviceSelector.getSelectedDevice());
        } else {
            configuration = new Configuration(configName);
            configuration.setDevice(deviceSelector.getSelectedDevice());
            configuration.setSymbolSet(symbolSet);
            configurations.add(configuration);
        }
        setWorkspaceSymbolSets();
        // Notify IConfigurationChangeListener that symbolSet changed
        if (symbolSetChanged) {
            configuration.fireSymbolSetChanged();
        }
    }

    /**
     * Record added symbols. May be used for restore symbolSet when perform
     * cancel.
     * 
     * @param symbol
     */
    private void recordAddedSymbols(Symbol symbol) {
        if (addedSymbols == null) {
            addedSymbols = new ArrayList<Symbol>();
        }
        addedSymbols.add(symbol);
    }

    /**
     * Record removed symbols. May be used for restore symbolSet when perform
     * cancel.
     * 
     * @param symbol
     */
    private void recordRemovedSymbols(Symbol symbol) {
        if (removedSymbols == null) {
            removedSymbols = new ArrayList<Symbol>();
        }
        removedSymbols.add(symbol);
    }

    /**
     * The remove symbol button has been selected.
     */
    private void removeSymbol() {
        if (symbolSet == null) {
            return;
        }
        Table table = symbolsTableViewer.getTable();
        TableItem[] selected = table.getSelection();
        for (TableItem tableItem : selected) {
            Symbol symbol = (Symbol) tableItem.getData();
            symbolSet.remove(symbol);
            recordRemovedSymbols(symbol);
        }
        symbolsTableViewer.refresh();

        symbolSetChanged = true;
    }

    /**
     * Restore symbolSet when perform cancel.
     */
    private void restoreSymbolSet() {
        if (!inEditMode) {
            return;
        }
        if (!symbolSetChanged) {
            return;
        }
        // if midletSuiteProject != null, we restore symbolSet from meta data
        // file. All changes can restore.
        if (midletSuiteProject != null) {
            Configurations configsInMetadataFile = new MetaData(
                    midletSuiteProject.getProject()).getConfigurations();
            Configuration toRestore = configsInMetadataFile
                    .get(configsInMetadataFile.indexOf(configuration));
            configurations.remove(configuration);
            configurations.add(toRestore);
            if (configuration.isActive()) {
                // To fire a SwithActiveConfigEvent,tell listeners active config
                // switched.
                configurations.switchActiveConfiguration(toRestore);
            }
            return;
        }
        // if midletSuiteProject == null, we just restore added and removed
        // symbols, symbol name and value changes cannot restore.

        // MUST 1:add all removedSymbols, then 2:remove all addedSymbols, should
        // not convert the sequence
        // 1. add all removedSymbols
        if (removedSymbols != null) {
            symbolSet.addAll(removedSymbols);
            removedSymbols.clear();
        }
        // 2. remove all addedSymbols
        if (addedSymbols != null) {
            symbolSet.removeAll(addedSymbols);
            addedSymbols.clear();
        }
    }

    /**
     * If midletSuiteProject been set, will use it to retrieve configuration
     * from meta data file to do restore symbolSet(if user click Cancel on the
     * wizard).
     * 
     * @param midletSuiteProject
     */
    public void setMidletSuiteProject(IMidletSuiteProject midletSuiteProject) {
        this.midletSuiteProject = midletSuiteProject;
    }

    private void setWorkspaceSymbolSets() {
        Object[] objects = workSpaceSymbolSetViewer.getCheckedElements();
        List<SymbolDefinitionSet> symbolSets = new ArrayList<SymbolDefinitionSet>();
        for (Object o : objects) {
            symbolSets.add((SymbolDefinitionSet) o);
        }
        configuration.setWorkspaceScopeSymbolSets(symbolSets);
    }

    /**
     * Validate controls on the UI, to determine if controls can be enabled and
     * to set/remove error message.
     */
    private void validateControls() {
        // validate removeSymbolButton
        validateRemoveSymbolButton();
        // validate finish button
        boolean canFinish = validConfigName() && validDevice();
        setPageComplete(canFinish);
    }

    /**
     * Determine if removeSymbolButton and addSymbolButton can be enabled.
     */
    private void validateRemoveSymbolButton() {
        boolean isRemoveSymbolButtonEnable = symbolsTableViewer.getTable()
                .getSelection().length > 0;
        removeSymbolButton.setEnabled(isRemoveSymbolButtonEnable);
        addSymbolButton.setEnabled(symbolSet != null);
    }

    /**
     * Determine if config name are valid.
     * 
     * @return
     */
    private boolean validConfigName() {
        String newConfigName = configNameText.getText();
        boolean uniqueConfigName = !configurations.contains(new Configuration(
                newConfigName));
        boolean ifValidConfigName;
        IStatus result = ResourcesPlugin.getWorkspace().validateName(
                newConfigName, IResource.FILE);
        if (!result.isOK()) {
            ifValidConfigName = false;
        } else if (inEditMode) {
            if (configuration.getName().equals(newConfigName)) {
                ifValidConfigName = true;
            } else {
                ifValidConfigName = uniqueConfigName;
            }
        } else {
            ifValidConfigName = uniqueConfigName;
        }
        if (!ifValidConfigName) {
            setErrorMessage(ConfigurationMessages.ErrorMessage_InvalidConfigName);
        } else {
            setErrorMessage(null);
        }
        return ifValidConfigName;
    }

    /**
     * Determine if a device are selected.
     * 
     * @return
     */
    private boolean validDevice() {
        IDevice device = deviceSelector.getSelectedDevice();
        if (getDeviceCount() <= 0) {
            setErrorMessage(ConfigurationMessages.ErrorMessage_NoDeviceAvailable);
            return false;
        } else if (device == null) {
            setErrorMessage(ConfigurationMessages.ErrorMessage_NoDeviceSelected);
            return false;
        } else {
            setErrorMessage(null);
            return true;
        }
    }
}
