/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n.actions;

import org.eclipse.mtj.internal.core.text.l10n.L10nEntry;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.util.MTJLabelUtility;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class L10nAddLocaleEntryAction extends L10nAddObjectAction {

    /**
     * 
     */
    public L10nAddLocaleEntryAction() {
        setText(MTJUIMessages.L10nAddLocaleEntryAction_text);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public void run() {
        if (parentObject != null) {

            // Create a new entry object
            L10nEntry entry = parentObject.getModel().getFactory()
                    .createL10nEntry();

            // Generate the key and value for the entry
            String key = MTJLabelUtility.generateName(getChildNames(), MTJUIMessages.L10nAddLocaleEntryAction_initialKey);
            String value = MTJLabelUtility.generateName(getChildNames(),
                    MTJUIMessages.L10nAddLocaleEntryAction_initialValue);

            entry.setKey(key);
            entry.setValue(value);

            // Add the new Entry to the parent Locale object
            addChild(entry);
        }
    }
}
