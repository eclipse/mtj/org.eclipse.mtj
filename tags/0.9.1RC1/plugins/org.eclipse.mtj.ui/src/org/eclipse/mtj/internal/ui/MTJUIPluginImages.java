/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 */
package org.eclipse.mtj.internal.ui;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.graphics.Image;

/**
 * Bundle of all images used by the MTJ UI plug-in.
 * 
 * @since 0.9.1
 */
public class MTJUIPluginImages {

    public final static String ICONS_PATH = "icons/"; //$NON-NLS-1$
    public final static String ICONS_NEW_PATH = "icons/full/"; //$NON-NLS-1$

    private static ImageRegistry PLUGIN_REGISTRY;

    /* The standard icon folders */
    public static final String PATH_LCL = ICONS_NEW_PATH + "elcl16/"; //$NON-NLS-1$
    public static final String PATH_LCL_DISABLED = ICONS_NEW_PATH + "dlcl16/"; //$NON-NLS-1$
    public static final String PATH_OBJ = ICONS_NEW_PATH + "obj16/"; //$NON-NLS-1$
    public static final String PATH_OVR = ICONS_NEW_PATH + "ovr16/"; //$NON-NLS-1$
    public static final String PATH_TOOL = ICONS_NEW_PATH + "etool16/"; //$NON-NLS-1$
    public static final String PATH_VIEW = ICONS_NEW_PATH + "view16/"; //$NON-NLS-1$
    public static final String PATH_WIZBAN = ICONS_NEW_PATH + "wizban/"; //$NON-NLS-1$

    /* Image Descriptors */
    public static final ImageDescriptor DESC_LINK_WITH_EDITOR = create(
            PATH_LCL, "synced.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_ALPHAB_SORT_CO = create(PATH_LCL,
            "alphab_sort_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_HELP = create(PATH_LCL, "help.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_PAGE_OBJ = create(PATH_OBJ,
            "page_obj.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_ALPHAB_SORT_CO_DISABLED = create(
            PATH_LCL_DISABLED, "alphab_sort_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_LINK_WITH_EDITOR_DISABLED = create(
            PATH_LCL_DISABLED, "synced.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_LOCALES_OBJ = create(PATH_OBJ,
            "locales_obj.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_LOCALE_OBJ = create(PATH_OBJ,
    "locale_obj.gif"); //$NON-NLS-1$
    
    public static final ImageDescriptor DESC_LOCALE_ENTRY_OBJ = create(PATH_OBJ,
    "locale_entry_obj.gif"); //$NON-NLS-1$
    
    public static final ImageDescriptor DESC_LINK_OBJ = create(PATH_OBJ,
            "link_obj.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_OVERVIEW_OBJ = create(PATH_OBJ,
            "overview_obj.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_ERROR_CO = create(PATH_OVR,
            "error_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_WARNING_CO = create(PATH_OVR,
            "warning_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_EXPORT_CO = create(PATH_OVR,
            "export_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_EXTERNAL_CO = create(PATH_OVR,
            "external_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_BINARY_CO = create(PATH_OVR,
            "binary_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_JAVA_CO = create(PATH_OVR,
            "java_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_JAR_CO = create(PATH_OVR,
            "jar_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_PROJECT_CO = create(PATH_OVR,
            "project_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_OPTIONAL_CO = create(PATH_OVR,
            "optional_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_INTERNAL_CO = create(PATH_OVR,
            "internal_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_FRIEND_CO = create(PATH_OVR,
            "friend_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_DOC_CO = create(PATH_OVR,
            "doc_co.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_COLLAPSE_ALL = create(PATH_LCL,
            "collapseall.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_ADD_L10N = create(PATH_WIZBAN,
            "addL10n_wiz.png"); //$NON-NLS-1$

    
    /**
     * @param key
     * @return
     */
    public static Image get(String key) {
        if (PLUGIN_REGISTRY == null) {
            initialize();
        }
        return PLUGIN_REGISTRY.get(key);
    }

    /**
     * @param key
     * @param desc
     * @return
     */
    public static Image manage(String key, ImageDescriptor desc) {
        Image image = desc.createImage();
        PLUGIN_REGISTRY.put(key, image);
        return image;
    }

    /**
     * @param prefix
     * @param name
     * @return
     */
    private static ImageDescriptor create(String prefix, String name) {
        return ImageDescriptor.createFromURL(makeImageURL(prefix, name));
    }

    /**
     * 
     */
    private static final void initialize() {
        PLUGIN_REGISTRY = new ImageRegistry();
    }

    /**
     * @param prefix
     * @param name
     * @return
     */
    private static URL makeImageURL(String prefix, String name) {
        String path = "$nl$/" + prefix + name; //$NON-NLS-1$
        return FileLocator.find(MTJUIPlugin.getDefault().getBundle(), new Path(
                path), null);
    }
}
