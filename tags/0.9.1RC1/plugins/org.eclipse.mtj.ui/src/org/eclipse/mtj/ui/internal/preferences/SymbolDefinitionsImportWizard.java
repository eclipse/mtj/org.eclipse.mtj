/**
 * Copyright (c) 2008 Ales Milan and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Ales Milan - Initial implementation
 */

package org.eclipse.mtj.ui.internal.preferences;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.device.preprocess.DeviceSymbolDefinitionSetFactory;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;

/**
 * Wizard for importing SymbolDefinitionSets from J2ME Polish.
 * 
 * @author Ales "afro" Milan
 */
public class SymbolDefinitionsImportWizard extends Wizard {

    private SymbolDefinitionsImportWizardPage wizardPage;

    private String importDirectory = null;

    public SymbolDefinitionsImportWizard() {

        super();
        setNeedsProgressMonitor(true);
        setWindowTitle(Messages.SymbolDefinitionsImportWizardPage_title);

    }

    @Override
    public void addPages() {
        wizardPage = new SymbolDefinitionsImportWizardPage();
        addPage(wizardPage);
    }

    private IRunnableWithProgress getProcessImportFromAntennaJarFile() {

        // The runnable to do import definitions
        return new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor) {
                JarFile antennaFileJar = null;
                try {
                    IPreferenceStore preferenceStore = MTJUIPlugin.getDefault()
                            .getCorePreferenceStore();
                    String antennaFile = preferenceStore
                            .getString(IMTJCoreConstants.PREF_ANTENNA_JAR);

                    // search devices.xml & groups.xml from Antenna Jar file
                    InputStream devicesInputStream = null;
                    InputStream groupsInputStream = null;
                    antennaFileJar = new JarFile(new File(antennaFile));
                    Enumeration<JarEntry> e = antennaFileJar.entries();
                    while (e.hasMoreElements()) {
                        JarEntry entry = e.nextElement();
                        if (entry
                                .getName()
                                .toLowerCase()
                                .equals(
                                        DeviceSymbolDefinitionSetFactory.J2MEPOLISH_FILENAME_XML_DEVICES)) {
                            devicesInputStream = antennaFileJar
                                    .getInputStream(entry);
                        }
                        if (entry
                                .getName()
                                .toLowerCase()
                                .equals(
                                        DeviceSymbolDefinitionSetFactory.J2MEPOLISH_FILENAME_XML_GROUPS)) {
                            groupsInputStream = antennaFileJar
                                    .getInputStream(entry);
                        }
                    }
                    if ((devicesInputStream != null)
                            && (groupsInputStream != null)) {
                        try {
                            DeviceSymbolDefinitionSetFactory
                                    .importFromJ2MEPolishFormat(monitor,
                                            devicesInputStream,
                                            groupsInputStream);
                        } catch (Exception ex) {
                            handleException(
                                    Messages.SymbolDefinitionsImportWizardPage_error_errorDuringImportSymbolDefinitionSet,
                                    ex);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        antennaFileJar.close();
                    } catch (Exception e) {
                    }
                }

            }
        };

    }

    private IRunnableWithProgress getProcessImportFromXMLFiles() {

        // The runnable to do import definitions
        return new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor) {
                InputStream devicesInputStream = null;
                InputStream groupsInputStream = null;
                try {
                    // search devices.xml & groups.xml from Antenna Jar file
                    devicesInputStream = new FileInputStream(
                            new File(
                                    importDirectory
                                            + File.separator
                                            + DeviceSymbolDefinitionSetFactory.J2MEPOLISH_FILENAME_XML_DEVICES));
                    groupsInputStream = new FileInputStream(
                            new File(
                                    importDirectory
                                            + File.separator
                                            + DeviceSymbolDefinitionSetFactory.J2MEPOLISH_FILENAME_XML_GROUPS));
                    if ((devicesInputStream != null)
                            && (groupsInputStream != null)) {
                        try {
                            DeviceSymbolDefinitionSetFactory
                                    .importFromJ2MEPolishFormat(monitor,
                                            devicesInputStream,
                                            groupsInputStream);
                        } catch (Exception ex) {
                            handleException(
                                    Messages.SymbolDefinitionsImportWizardPage_error_errorDuringImportSymbolDefinitionSet,
                                    ex);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        devicesInputStream.close();
                    } catch (Exception e) {
                    }
                    try {
                        groupsInputStream.close();
                    } catch (Exception e) {
                    }
                }

            }
        };

    }

    /**
     * An exception has occurred. Handle it appropriately.
     * 
     * @param t
     */
    private void handleException(String message, Throwable t) {
        MTJCorePlugin.log(IStatus.WARNING, message, t);
        MessageDialog.openError(getShell(),
                Messages.SymbolDefinitionsImportWizardPage_error, message);
    }

    @Override
    public boolean performFinish() {

        switch (wizardPage.getImportType()) {
        case SymbolDefinitionsImportWizardPage.IMPORT_FROM_ANTENNA_JAR:
            try {
                getContainer().run(true, true,
                        getProcessImportFromAntennaJarFile());
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            break;

        case SymbolDefinitionsImportWizardPage.IMPORT_J2MEPOLISH_FORMAT:
            importDirectory = wizardPage.getDirectory();
            try {
                getContainer().run(true, true, getProcessImportFromXMLFiles());
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            break;
        case SymbolDefinitionsImportWizardPage.IMPORT_MTJ_FORMAT:

            break;
        }
        return true;
    }

}
