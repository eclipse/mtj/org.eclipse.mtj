/**
 * Copyright (c) 2003, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.model.IBaseModel;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.ui.internal.editors.FormLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.dialogs.FilteredTree;
import org.eclipse.ui.forms.AbstractFormPart;
import org.eclipse.ui.forms.IFormPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Hyperlink;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

/**
 * @since 0.9.1
 */
public abstract class MTJFormPage extends FormPage {

    /**
     * 
     */
    private Control lastFocusControl;

    /**
     * 
     */
    private boolean newStyleHeader = true;

    /**
     * 
     */
    private boolean stale;

    /**
     * Creates a new MTJFormPage
     * 
     * @param editor
     * @param id
     * @param title
     */
    public MTJFormPage(FormEditor editor, String id, String title) {
        super(editor, id, title);
        lastFocusControl = null;
        stale = false;
    }

    /**
     * Creates a new MTJFormPage
     * 
     * @param editor
     * @param id
     * @param title
     * @param newStyleHeader
     */
    public MTJFormPage(FormEditor editor, String id, String title,
            boolean newStyleHeader) {
        this(editor, id, title);
        this.newStyleHeader = newStyleHeader;
    }

    /**
     * Programatically and recursively add focus listeners to the specified
     * composite and its children that track the last control to have focus
     * before a page change or the editor lost focus
     * 
     * @param composite
     */
    public void addLastFocusListeners(Composite composite) {
        Control[] controls = composite.getChildren();

        for (Control control : controls) {
            // Add a focus listener if the control is any one of the below types
            // Note that the controls listed below represent all the controls
            // currently in use by all form pages in MTJ. In the future,
            // more controls will have to be added.
            // Could not add super class categories of controls because it
            // would include things like tool bars that we don't want to track
            // focus for.
            if ((control instanceof Text) || (control instanceof Button)
                    || (control instanceof Combo)
                    || (control instanceof CCombo) || (control instanceof Tree)
                    || (control instanceof Table)
                    || (control instanceof Spinner)
                    || (control instanceof Link) || (control instanceof List)
                    || (control instanceof TabFolder)
                    || (control instanceof CTabFolder)
                    || (control instanceof Hyperlink)
                    || (control instanceof FilteredTree)) {
                addLastFocusListener(control);
            }
            if (control instanceof Composite) {
                // Recursively add focus listeners to this composites children
                addLastFocusListeners((Composite) control);
            }
        }
    }

    /**
     * Used to align the section client / descriptions of two section headers
     * horizontally adjacent to each other. The misalignment is caused by one
     * section header containing toolbar icons and the other not.
     * 
     * @param masterSection
     * @param detailsSection
     */
    public void alignSectionHeaders(Section masterSection,
            Section detailsSection) {
        detailsSection.descriptionVerticalSpacing += masterSection
                .getTextClientHeightDifference();
    }

    /**
     * 
     */
    public void cancelEdit() {
        IFormPart[] parts = getManagedForm().getParts();
        for (IFormPart part : parts) {
            if (part instanceof IContextPart) {
                ((IContextPart) part).cancelEdit();
            }
        }
    }

    /**
     * @param selection
     * @return
     */
    public boolean canCopy(ISelection selection) {
        AbstractFormPart focusPart = getFocusSection();
        if (focusPart != null) {
            if (focusPart instanceof MTJSection) {
                return ((MTJSection) focusPart).canCopy(selection);
            }
            if (focusPart instanceof MTJDetails) {
                return ((MTJDetails) focusPart).canCopy(selection);
            }
        }
        return false;
    }

    /**
     * @param selection
     * @return
     */
    public boolean canCut(ISelection selection) {
        AbstractFormPart focusPart = getFocusSection();
        if (focusPart != null) {
            if (focusPart instanceof MTJSection) {
                return ((MTJSection) focusPart).canCut(selection);
            }
            if (focusPart instanceof MTJDetails) {
                return ((MTJDetails) focusPart).canCut(selection);
            }
        }
        return false;
    }

    /**
     * @param clipboard
     * @return
     */
    public boolean canPaste(Clipboard clipboard) {
        AbstractFormPart focusPart = getFocusSection();
        if (focusPart != null) {
            if (focusPart instanceof MTJSection) {
                return ((MTJSection) focusPart).canPaste(clipboard);
            }
            if (focusPart instanceof MTJDetails) {
                return ((MTJDetails) focusPart).canPaste(clipboard);
            }
        }
        return false;
    }

    /**
     * @param menu
     */
    public void contextMenuAboutToShow(IMenuManager menu) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormPage#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createPartControl(Composite parent) {
        super.createPartControl(parent);
        // Dynamically add focus listeners to all the forms children in order
        // to track the last focus control
        IManagedForm managedForm = getManagedForm();
        if (managedForm != null) {
            addLastFocusListeners(managedForm.getForm());
        }
    }

    /**
     * @param parent
     * @param text
     * @param description
     * @param style
     * @return
     */
    public Section createUISection(Composite parent, String text,
            String description, int style) {
        Section section = getManagedForm().getToolkit().createSection(parent,
                style);
        section.clientVerticalSpacing = FormLayoutFactory.SECTION_HEADER_VERTICAL_SPACING;
        section.setLayout(FormLayoutFactory.createClearGridLayout(false, 1));
        section.setText(text);
        section.setDescription(description);
        GridData data = new GridData(GridData.FILL_HORIZONTAL);
        section.setLayoutData(data);
        return section;
    }

    /**
     * @param parent
     * @param columns
     * @return
     */
    public Composite createUISectionContainer(Composite parent, int columns) {
        Composite container = getManagedForm().getToolkit().createComposite(
                parent);
        container.setLayout(FormLayoutFactory.createSectionClientGridLayout(
                false, columns));
        return container;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormPage#dispose()
     */
    @Override
    public void dispose() {
        Control c = getPartControl();
        if ((c != null) && !c.isDisposed()) {
            Menu menu = c.getMenu();
            if (menu != null) {
                resetMenu(menu, c);
            }
        }
        super.dispose();
    }

    /**
     * @return
     */
    public Control getLastFocusControl() {
        return lastFocusControl;
    }

    /**
     * @return
     */
    public IBaseModel getModel() {
        return getMTJEditor().getAggregateModel();
    }

    /**
     * @return
     */
    public MTJFormEditor getMTJEditor() {
        return (MTJFormEditor) getEditor();
    }

    /**
     * @param throwable
     * @return
     */
    public String getStackTrace(Throwable throwable) {
        StringWriter swriter = new StringWriter();
        PrintWriter pwriter = new PrintWriter(swriter);
        throwable.printStackTrace(pwriter);
        pwriter.flush();
        pwriter.close();
        return swriter.toString();
    }

    /**
     * @param actionId
     * @return
     */
    public boolean performGlobalAction(String actionId) {
        Control focusControl = getFocusControl();
        if (focusControl == null) {
            return false;
        }

        if (canPerformDirectly(actionId, focusControl)) {
            return true;
        }
        AbstractFormPart focusPart = getFocusSection();
        if (focusPart != null) {
            if (focusPart instanceof MTJSection) {
                return ((MTJSection) focusPart).doGlobalAction(actionId);
            }
            if (focusPart instanceof MTJDetails) {
                return ((MTJDetails) focusPart).doGlobalAction(actionId);
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormPage#setActive(boolean)
     */
    @Override
    public void setActive(boolean active) {
        super.setActive(active);
        if (active && isStale()) {
            refresh();
        }
    }

    /**
     * @param control
     */
    public void setLastFocusControl(Control control) {
        lastFocusControl = control;
    }

    /**
     * Set the focus on the last control to have focus before a page change or
     * the editor lost focus.
     */
    public void updateFormSelection() {
        if ((lastFocusControl != null)
                && (lastFocusControl.isDisposed() == false)) {
            // Set focus on the control
            lastFocusControl.setFocus();
            // If the control is a Text widget, select its contents
            if (lastFocusControl instanceof Text) {
                Text text = (Text) lastFocusControl;
                text.setSelection(0, text.getText().length());
            }
        } else {
            // No focus control set
            // Fallback on managed form selection mechanism by setting the
            // focus on this page itself.
            // The managed form will set focus on the first managed part.
            // Most likely this will turn out to be a section.
            // In order for this to work properly, we must override the
            // sections setFocus() method and set focus on a child control
            // (preferably first) that can practically take focus.
            setFocus();
        }
    }

    /**
     * Add a focus listener to the specified control that tracks the last
     * control to have focus on this page. When focus is gained by this control,
     * it registers itself as the last control to have focus. The last control
     * to have focus is stored in order to be restored after a page change or
     * editor loses focus.
     * 
     * @param control
     */
    private void addLastFocusListener(final Control control) {
        control.addFocusListener(new FocusListener() {
            public void focusGained(FocusEvent e) {
                // NO-OP
            }

            public void focusLost(FocusEvent e) {
                lastFocusControl = control;
            }
        });
    }

    /**
     * @return
     */
    private AbstractFormPart getFocusSection() {
        Control focusControl = getFocusControl();
        if (focusControl == null) {
            return null;
        }
        Composite parent = focusControl.getParent();
        AbstractFormPart targetPart = null;
        while (parent != null) {
            Object data = parent.getData("part"); //$NON-NLS-1$
            if ((data != null) && (data instanceof AbstractFormPart)) {
                targetPart = (AbstractFormPart) data;
                break;
            }
            parent = parent.getParent();
        }
        return targetPart;
    }

    /**
     * @param menu
     * @param c
     */
    private void resetMenu(Menu menu, Control c) {
        if (c instanceof Composite) {
            Composite comp = (Composite) c;
            Control[] children = comp.getChildren();
            for (Control element : children) {
                resetMenu(menu, element);
            }
        }
        Menu cmenu = c.getMenu();
        if ((cmenu != null) && cmenu.equals(menu)) {
            c.setMenu(null);
        }
    }

    /**
     * @param id
     * @param control
     * @return
     */
    protected boolean canPerformDirectly(String id, Control control) {
        if (control instanceof Text) {
            Text text = (Text) control;
            if (id.equals(ActionFactory.CUT.getId())) {
                text.cut();
                return true;
            }
            if (id.equals(ActionFactory.COPY.getId())) {
                text.copy();
                return true;
            }
            if (id.equals(ActionFactory.PASTE.getId())) {
                text.paste();
                return true;
            }
            if (id.equals(ActionFactory.SELECT_ALL.getId())) {
                text.selectAll();
                return true;
            }
            if (id.equals(ActionFactory.DELETE.getId())) {
                int count = text.getSelectionCount();
                if (count == 0) {
                    int caretPos = text.getCaretPosition();
                    text.setSelection(caretPos, caretPos + 1);
                }
                text.insert(Utils.EMPTY_STRING);
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createFormContent(IManagedForm managedForm) {
        final ScrolledForm form = managedForm.getForm();
        FormToolkit toolkit = managedForm.getToolkit();

        if (newStyleHeader) {
            toolkit.decorateFormHeading(form.getForm());
        }

        IToolBarManager manager = form.getToolBarManager();

        getMTJEditor().contributeToToolbar(manager);

        final String href = getHelpResource();
        if (href != null) {
            Action helpAction = new Action("help") { //$NON-NLS-1$
                @Override
                public void run() {
                    BusyIndicator.showWhile(form.getDisplay(), new Runnable() {
                        public void run() {
                            PlatformUI.getWorkbench().getHelpSystem()
                                    .displayHelpResource(href);
                        }
                    });
                }
            };
            helpAction.setToolTipText(MTJUIMessages.MTJFormPage_helpAction_toolTipText);
            helpAction.setImageDescriptor(MTJUIPluginImages.DESC_HELP);
            manager.add(helpAction);
        }
        // check to see if our form parts are contributing actions
        IFormPart[] parts = managedForm.getParts();
        for (IFormPart part : parts) {
            if (part instanceof IAdaptable) {
                IAdaptable adapter = (IAdaptable) part;
                IAction[] actions = (IAction[]) adapter
                        .getAdapter(IAction[].class);
                if (actions != null) {
                    for (IAction action : actions) {
                        form.getToolBarManager().add(action);
                    }
                }
            }
        }
        form.updateToolBar();
    }

    /**
     * @param managedForm
     * @param errorTitle
     * @param errorMessage
     */
    protected void createFormErrorContent(IManagedForm managedForm,
            String errorTitle, String errorMessage) {
        createFormErrorContent(managedForm, errorTitle, errorMessage, null);
    }

    /**
     * @param managedForm
     * @param errorTitle
     * @param errorMessage
     * @param e
     */
    protected void createFormErrorContent(IManagedForm managedForm,
            String errorTitle, String errorMessage, Exception e) {

        ScrolledForm form = managedForm.getForm();
        FormToolkit toolkit = managedForm.getToolkit();
        // FormColors colors = toolkit.getColors();
        // form.getForm().setSeparatorColor(colors.getColor(FormColors.TB_BORDER));
        if (newStyleHeader) {
            // createNewStyleHeader(form, colors);
            toolkit.decorateFormHeading(form.getForm());
        }

        Composite parent = form.getBody();
        GridLayout layout = new GridLayout();
        GridData data2 = new GridData(GridData.FILL_BOTH);
        layout.marginWidth = 7;
        layout.marginHeight = 7;
        parent.setLayout(layout);
        parent.setLayoutData(data2);
        // Set the title and image of the form
        form.setText(errorTitle);
        form.setImage(JFaceResources.getImage(Dialog.DLG_IMG_MESSAGE_ERROR));

        int sectionStyle = Section.DESCRIPTION | ExpandableComposite.TITLE_BAR;
        // Create the message section
        Section messageSection = createUISection(parent, MTJUIMessages.MTJFormPage_messageSection_title,
                errorMessage, sectionStyle);
        Composite messageClient = createUISectionContainer(messageSection, 1);
        // Bind the widgets
        toolkit.paintBordersFor(messageClient);
        messageSection.setClient(messageClient);
        // Ensure the exception was defined
        if (e == null) {
            return;
        }
        // Create the details section
        Section detailsSection = createUISection(parent, MTJUIMessages.MTJFormPage_detailsSection_title, e
                .getMessage(), sectionStyle);
        Composite detailsClient = createUISectionContainer(detailsSection, 1);
        // Create text widget holding the exception trace
        int style = SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL | SWT.READ_ONLY;
        Text text = toolkit.createText(detailsClient, getStackTrace(e), style);
        GridData data = new GridData(GridData.FILL_HORIZONTAL);
        data.heightHint = 160;
        data.widthHint = 200;
        text.setLayoutData(data);
        // Bind the widgets
        toolkit.paintBordersFor(detailsClient);
        detailsSection.setClient(detailsClient);
        // Note: The vertical scrollbar fails to appear when text widget is
        // not entirely shown
    }

    /**
     * @return
     */
    protected Control getFocusControl() {
        IManagedForm form = getManagedForm();
        if (form == null) {
            return null;
        }
        Control control = form.getForm();
        if ((control == null) || control.isDisposed()) {
            return null;
        }
        Display display = control.getDisplay();
        Control focusControl = display.getFocusControl();
        if ((focusControl == null) || focusControl.isDisposed()) {
            return null;
        }
        return focusControl;
    }

    /**
     * @return
     */
    protected String getHelpResource() {
        return null;
    }

    /**
     * @return
     */
    protected boolean isStale() {
        return stale;
    }

    /**
     * 
     */
    protected void markStale() {
        stale = true;
    }

    /**
     * 
     */
    protected void refresh() {
        stale = false;
    }
}
