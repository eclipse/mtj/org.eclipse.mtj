/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.ui.internal.launching;

import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.ui.console.IConsole;
import org.eclipse.debug.ui.console.IConsoleLineTracker;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IRegion;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.launching.StackTraceEntry;
import org.eclipse.mtj.core.internal.launching.StackTraceParser;
import org.eclipse.mtj.core.internal.utils.Utils;

/**
 *  MTJConsoleLineTracker caches stack trace lines since the same stack
 *  entry can occur more that once in stack traces. Fore every new stack
 *  line appended to the console a link is created.
 */
public class MTJConsoleLineTracker implements IConsoleLineTracker {

    private Map<String, StackTraceEntry> cachedTrace;
    private IJavaProject project;
    private IConsole     console;
    
    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.console.IConsoleLineTracker#init(org.eclipse.debug.ui.console.IConsole)
     */
    public void init(IConsole console) {
        this.cachedTrace = new Hashtable<String, StackTraceEntry>();
        this.console     = console;
        
        try {
            ILaunchConfiguration configuration = console.getProcess().getLaunch().getLaunchConfiguration();
            String projectName = configuration.getAttribute("org.eclipse.jdt.launching.PROJECT_ATTR", Utils.EMPTY_STRING);
            if (projectName.length() > 0x00) {
                IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
                if (project.isOpen() && project.hasNature(JavaCore.NATURE_ID)){
                    this.project = JavaCore.create(project);
                    StackTraceParser parser = new StackTraceParser(this.project);
                    MTJStackTraceMatcher matcher = new MTJStackTraceMatcher(this, parser);
                    this.console.addPatternMatchListener(matcher);
                }
            }
        } catch (CoreException e) {
        	MTJCorePlugin.log(IStatus.ERROR, e.getMessage(), e);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.console.IConsoleLineTracker#lineAppended(org.eclipse.jface.text.IRegion)
     */
    public void lineAppended(IRegion region) {
        try {
            String trace = this.console.getDocument().get(region.getOffset(),
                    region.getLength());
            if (Pattern.matches("(\tat .+[(][+]\\d+[)])", trace)) {
                StackTraceEntry element = null;
                synchronized (cachedTrace) {
                    element = this.cachedTrace.get(trace);
                    if (element == null) {
                        element = new StackTraceEntry(trace);
                        this.cachedTrace.put(trace, element);
                    }
                }
                element.addRegion(region);
            }
        } catch (BadLocationException e) {
        	MTJCorePlugin.log(IStatus.ERROR, e.getMessage(), e);
        }
    }

    /**
     * Gets the cached stack trace entries.
     * 
     * @return cached entries.
     */
    public Map<String, StackTraceEntry> getCachedTrace(){
        return this.cachedTrace;
    }

    /**
     * Gets the attached console.
     * 
     * @return console.
     */
    public IConsole getConsole(){
        return this.console;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.debug.ui.console.IConsoleLineTracker#dispose()
     */
    public void dispose() {
        this.console = null;
    }

}
