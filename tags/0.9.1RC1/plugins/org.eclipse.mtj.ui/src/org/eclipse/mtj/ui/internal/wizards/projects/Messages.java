/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.ui.internal.wizards.projects;

import org.eclipse.osgi.util.NLS;

/**
 * Provides convenience methods for manipulating messages.
 * 
 * @author Diego Madruga Sandin
 */
public class Messages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.ui.internal.wizards.projects.messages"; //$NON-NLS-1$

    public static String NewMidletProjectWizard_wizard_title;
    public static String NewMidletProjectWizard_image_descriptor;
    public static String NewMidletProjectWizard_error_create_project_window_message;
    public static String NewMidletProjectWizard_error_create_project_window_title;
    public static String NewMidletProjectWizard_error_open_jad_file;

    public static String NewMidletProjectWizardPageOne_description;
    public static String NewMidletProjectWizardPageOne_deviceGroup;
    public static String NewMidletProjectWizardPageOne_deviceGroup_coment;
    public static String NewMidletProjectWizardPageOne_validate_devicecount_error;
    public static String NewMidletProjectWizardPageOne_validate_jadname_error_emptyname;
    public static String NewMidletProjectWizardPageOne_validate_jadname_error_extension;
    public static String NewMidletProjectWizardPageOne_preprocessor;
    public static String NewMidletProjectWizardPageOne_preprocessorGroup;
    public static String NewMidletProjectWizardPageOne_projectNameGroup;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_alreadyExists;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_emptyName;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_failedCreateContents;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_invalidDirectory;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_missingLocation;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_workspace1;
    public static String NewMidletProjectWizardPageOne_projectNameGroup_update_error_workspace2;
    public static String NewMidletProjectWizardPageOne_jad_groupname;
    public static String NewMidletProjectWizardPageOne_jad_label;
    public static String NewMidletProjectWizardPageOne_jadFileNameField;
    public static String NewMidletProjectWizardPageOne_jadNameGroup_projectBasedRadio;
    public static String NewMidletProjectWizardPageOne_jadNameGroup_userDefinedRadio;
    public static String NewMidletProjectWizardPageOne_locationGroup_browse_button;
    public static String NewMidletProjectWizardPageOne_locationGroup_changeControlPressed_dialogMessage;
    public static String NewMidletProjectWizardPageOne_locationGroup_contents;
    public static String NewMidletProjectWizardPageOne_locationGroup_externalLocationRadio;
    public static String NewMidletProjectWizardPageOne_locationGroup_projectLocation;
    public static String NewMidletProjectWizardPageOne_locationGroup_workspaceLocationRadio;
    public static String NewMidletProjectWizardPageOne_title;

    public static String NewMidletProjectWizardPageTwo_changeToNewProject_errordialog_message;
    public static String NewMidletProjectWizardPageTwo_changeToNewProject_errordialog_title;
    public static String NewMidletProjectWizardPageTwo_createBackup_error_1;
    public static String NewMidletProjectWizardPageTwo_createBackup_error_2;
    public static String NewMidletProjectWizardPageTwo_doRemoveProject_taskname;
    public static String NewMidletProjectWizardPageTwo_performFinish_monitor_taskname;
    public static String NewMidletProjectWizardPageTwo_rememberExisitingFolders_errordialog_message;
    public static String NewMidletProjectWizardPageTwo_rememberExisitingFolders_errordialog_title;
    public static String NewMidletProjectWizardPageTwo_restoreExistingFiles_problem_restoring_dotclasspath;
    public static String NewMidletProjectWizardPageTwo_restoreExistingFiles_problem_restoring_dotproject;
    public static String NewMidletProjectWizardPageTwo_updateProject_errordialog_message;
    public static String NewMidletProjectWizardPageTwo_updateProject_errordialog_title;
    public static String NewMidletProjectWizardPageTwo_updateProject_fail_read_metadata;
    public static String NewMidletProjectWizardPageTwo_updateProject_monitor_buildpath_name;
    public static String NewMidletProjectWizardPageTwo_updateProject_taskname;

    public static String NewMidletProjectWizardPageLibrary_title;
    public static String NewMidletProjectWizardPageLibrary_description;
    public static String NewMidletProjectWizardPageLibrary_libraryList_label;
    public static String NewMidletProjectWizardPageLibrary_libraryList_up_button;
    public static String NewMidletProjectWizardPageLibrary_libraryList_down_button;
    public static String NewMidletProjectWizardPageLibrary_libraryList_top_button;
    public static String NewMidletProjectWizardPageLibrary_libraryList_bottom_button;
    public static String NewMidletProjectWizardPageLibrary_libraryList_checkall_button;
    public static String NewMidletProjectWizardPageLibrary_libraryList_uncheckall_button;

    public static String NewMidletProjectWizardPageLibrary_HintTextGroup_title;

    public static String ConfigurationSection_Description;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
