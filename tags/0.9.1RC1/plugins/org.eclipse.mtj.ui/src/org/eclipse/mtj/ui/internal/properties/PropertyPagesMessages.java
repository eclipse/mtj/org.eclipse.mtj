/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.ui.internal.properties;

import org.eclipse.osgi.util.NLS;

/**
 * Common class for all messages on property pages. Provides convenience methods
 * for manipulating messages.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class PropertyPagesMessages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.ui.internal.properties.messages"; //$NON-NLS-1$

    public static String LibraryPropertyPage_description;

    public static String LibraryPropertyPage_unsavedchanges_button_discard;
    public static String LibraryPropertyPage_unsavedchanges_button_ignore;
    public static String LibraryPropertyPage_unsavedchanges_button_save;
    public static String LibraryPropertyPage_unsavedchanges_message;
    public static String LibraryPropertyPage_unsavedchanges_title;

    public static String LibraryPropertyPage_error_title;
    public static String LibraryPropertyPage_error_message;

    public static String LibraryPropertyPage_restore_error_title;
    public static String LibraryPropertyPage_restore_error_message;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, PropertyPagesMessages.class);
    }

    private PropertyPagesMessages() {
    }
}
