/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.toolkit.uei.jadEditor;

import org.eclipse.osgi.util.NLS;

/**
 * Provides convenience methods for manipulating messages.
 * 
 * @author Diego Madruga Sandin
 */
public class JadUIContent extends NLS {

    
    public static String motoSpecPropertiesEditorPage_page_title;
    public static String motoSpecPropertiesEditorPage_section_title;
    public static String motoSpecPropertiesEditorPage_section_description;
        
    public static String nokiaSpecPropertiesEditorPage_page_title;
    public static String nokiaSpecPropertiesEditorPage_section_title;
    public static String nokiaSpecPropertiesEditorPage_section_description;
    
    /**
     * The base name of the MTJ resource bundle
     */
    private static final String BUNDLE_NAME = "org.eclipse.mtj.toolkit.uei.jadEditor."
            + "JadUIContent";

    static {
        // load message values from bundle file
        NLS.initializeMessages(BUNDLE_NAME, JadUIContent.class);
    }

}
