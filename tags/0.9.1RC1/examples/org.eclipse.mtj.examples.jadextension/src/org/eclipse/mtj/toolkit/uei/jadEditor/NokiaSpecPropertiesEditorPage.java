/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma (Sybase)         - Initial implementation
 *     Diego Madruga (Motorola) - Refactored class after API updates.   
 */
package org.eclipse.mtj.toolkit.uei.jadEditor;

import org.eclipse.mtj.core.model.jad.DescriptorPropertyDescription;
import org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;

/**
 * Nokia S60 specific JAD editor page.
 * 
 * @author Gang Ma
 */
public class NokiaSpecPropertiesEditorPage extends JADPropertiesEditorPage {

    public static final String NOKIA_PAGEID = "nokiaSpecific";

    private static final String NOKIA_S60_CATEGORY = "Nokia-MIDlet-Category";
    private static final String NOKIA_S60_CATEGORY_LBL = "Nokia-MIDlet-Category:";
    private static final String NOKIA_S60_ORIGIN_DISPLAY_SIZE = "Nokia-MIDlet-Original-Display-Size";
    private static final String NOKIA_S60_ORIGIN_DISPLAY_SIZE_LBL = "Nokia-MIDlet-Original-Display-Size:";
    private static final String NOKIA_S60_TARGET_DISPLAY_SIZE = "Nokia-MIDlet-Target-Display-Size";

    private static final String NOKIA_S60_TARGET_DISPLAY_SIZE_LBL = "Nokia-MIDlet-Target-Display-Size:";

    /**
     * List of application descriptor property descriptions
     */
    private static final DescriptorPropertyDescription[] NOKIA_S60_JAD_DESCRIPTORS = new DescriptorPropertyDescription[] {
            new DescriptorPropertyDescription(NOKIA_S60_CATEGORY,
                    NOKIA_S60_CATEGORY_LBL,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(NOKIA_S60_TARGET_DISPLAY_SIZE,
                    NOKIA_S60_ORIGIN_DISPLAY_SIZE_LBL,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(NOKIA_S60_ORIGIN_DISPLAY_SIZE,
                    NOKIA_S60_TARGET_DISPLAY_SIZE_LBL,
                    DescriptorPropertyDescription.DATATYPE_STRING) };

    /**
     * Creates a new NokiaSpecPropertiesEditorPage.
     */
    public NokiaSpecPropertiesEditorPage() {
        super(NOKIA_PAGEID,
                JadUIContent.nokiaSpecPropertiesEditorPage_page_title);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getTitle()
     */
    @Override
    public String getTitle() {

        return JadUIContent.nokiaSpecPropertiesEditorPage_page_title;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#addContextHelp(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void addContextHelp(Composite c) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(c,
                "org.eclipse.mtj.ui.help_NokiaJADPropertiesEditorPage"); //$NON-NLS-1$
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#doGetDescriptors()
     */
    @Override
    protected DescriptorPropertyDescription[] doGetDescriptors() {

        return NOKIA_S60_JAD_DESCRIPTORS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
     */
    @Override
    protected String getHelpResource() {
        return "/org.eclipse.mtj.docs/docs/jadeditor.html"; //$NON-NLS-1$
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionDescription()
     */
    @Override
    protected String getSectionDescription() {
        return JadUIContent.nokiaSpecPropertiesEditorPage_section_description;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionTitle()
     */
    @Override
    protected String getSectionTitle() {
        return JadUIContent.nokiaSpecPropertiesEditorPage_section_title;
    }
}
