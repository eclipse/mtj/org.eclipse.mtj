package org.eclipse.mtj.examples.templates;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;


public class ExampleTemplatePage extends AbstractTemplateWizardPage {

    // Constants -----------------------------------------------------

    private static final String COLOR_REG_EXP = "(0x)([a-fA-F0-9]){6}";
    
    // Attributes ----------------------------------------------------

    private Text bgColorText;
    private Text fgColorText;
    private Button faceSystemBtn;
    private Button faceMonospaceBtn;
    private Button faceProportionalBtn;
    private Button styleBoldBtn;
    private Button styleItalicBtn;
    private Button styleUnderlineBtn;
    private Button sizeSmallBtn;
    private Button sizeMediumBtn;
    private Button sizeLargeBtn;
    private Text msgText;
    
    // Static --------------------------------------------------------

    // Constructors --------------------------------------------------

    // Public --------------------------------------------------------

    // X implementation ----------------------------------------------

    public Map<String, String> getDictionary() {
        Map<String, String> dict = new HashMap<String, String>();
        
        dict.put("$message$", this.msgText.getText());
        dict.put("$bg$", this.bgColorText.getText());
        dict.put("$fg$", this.fgColorText.getText());
        
        dict.put("$font-face$", getFontFace());
        dict.put("$font-style$", getFontStyle());
        dict.put("$font-size$", getFontSize());

        return dict;
    }

    public boolean isPageComplete() {
        boolean result = true;
        result &= Pattern.matches(COLOR_REG_EXP, this.bgColorText.getText());
        result &= Pattern.matches(COLOR_REG_EXP, this.fgColorText.getText());
        result &= this.msgText.getText().length() > 0x00;
        return result;
    }

    public void createControl(Composite parent) {
        GridData gridData = null;
        
        parent.setLayout(new GridLayout(0x01, false));
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        composite.setLayout(new GridLayout(0x01, false));
        
        Group group = new Group(composite, SWT.NONE);
        group.setText("Text Design:");
        group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        group.setLayout(new GridLayout(0x02, false));
        
        Label textLabel = new Label(group, SWT.NONE);
        textLabel.setText("Text Message:");
        gridData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        textLabel.setLayoutData(gridData);
        
        msgText = new Text(group, SWT.BORDER);
        msgText.setText("MTJ Rocks...");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        msgText.setLayoutData(gridData);
        
        msgText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        // COLOR DESIGN
        
        group = new Group(composite, SWT.NONE);
        group.setText("Color Design:");
        group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        group.setLayout(new GridLayout(0x02, false));
        
        Label bgColorLabel = new Label(group, SWT.NONE);
        bgColorLabel.setText("Background Color (0xXXXXXX):");
        gridData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        bgColorLabel.setLayoutData(gridData);
        
        bgColorText = new Text(group, SWT.BORDER);
        bgColorText.setText("0xFFFFFF");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        bgColorText.setLayoutData(gridData);
        
        bgColorText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        Label fgColorLabel = new Label(group, SWT.NONE);
        fgColorLabel.setText("Text Color (0xXXXXXX):");
        gridData = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        fgColorLabel.setLayoutData(gridData);
        
        fgColorText = new Text(group, SWT.BORDER);
        fgColorText.setText("0x000000");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        fgColorText.setLayoutData(gridData);
        
        fgColorText.addModifyListener(new ModifyListener(){
            public void modifyText(ModifyEvent e) {
                getWizard().getContainer().updateButtons();
            }
        });
        
        // FONT DESIGN
        
        Group fontGroup = new Group(composite, SWT.NONE);
        fontGroup.setText("Font Design:");
        fontGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        fontGroup.setLayout(new GridLayout(0x01, false));
        
        // FONT FACE
        
        group = new Group(fontGroup, SWT.NONE);
        group.setText("Font Face:");
        group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        group.setLayout(new GridLayout(0x03, false));
        
        faceSystemBtn = new Button(group, SWT.RADIO);
        faceSystemBtn.setText("System");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        faceSystemBtn.setLayoutData(gridData);
        faceSystemBtn.setSelection(true);
        
        faceMonospaceBtn = new Button(group, SWT.RADIO);
        faceMonospaceBtn.setText("Monospace");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        faceMonospaceBtn.setLayoutData(gridData);
        
        faceProportionalBtn = new Button(group, SWT.RADIO);
        faceProportionalBtn.setText("Proportional");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        faceProportionalBtn.setLayoutData(gridData);
        
        // FONT STYLE
        
        group = new Group(fontGroup, SWT.NONE);
        group.setText("Font Style:");
        group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        group.setLayout(new GridLayout(0x03, false));
        
        styleBoldBtn = new Button(group, SWT.CHECK);
        styleBoldBtn.setText("Bold");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        styleBoldBtn.setLayoutData(gridData);
        styleBoldBtn.setSelection(true);
        
        styleItalicBtn = new Button(group, SWT.CHECK);
        styleItalicBtn.setText("Italic");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        styleItalicBtn.setLayoutData(gridData);
        styleItalicBtn.setSelection(true);
        
        styleUnderlineBtn = new Button(group, SWT.CHECK);
        styleUnderlineBtn.setText("Underline");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        styleUnderlineBtn.setLayoutData(gridData);
        styleUnderlineBtn.setSelection(true);
        
        // FONT SIZE
        
        group = new Group(fontGroup, SWT.NONE);
        group.setText("Font Size:");
        group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        group.setLayout(new GridLayout(0x03, false));
        
        sizeSmallBtn = new Button(group, SWT.RADIO);
        sizeSmallBtn.setText("Small");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        sizeSmallBtn.setLayoutData(gridData);
        
        sizeMediumBtn = new Button(group, SWT.RADIO);
        sizeMediumBtn.setText("Medium");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        sizeMediumBtn.setLayoutData(gridData);
        
        sizeLargeBtn = new Button(group, SWT.RADIO);
        sizeLargeBtn.setText("Large");
        gridData = new GridData(GridData.FILL_HORIZONTAL);
        sizeLargeBtn.setLayoutData(gridData);
        sizeLargeBtn.setSelection(true);
        
        this.setControl(composite);
    }
    
    // Y overrides ---------------------------------------------------

    // Package protected ---------------------------------------------

    // Protected -----------------------------------------------------

    // Private -------------------------------------------------------
    
    private String getFontSize() {
        StringBuffer buffer = new StringBuffer();
        if (this.sizeSmallBtn.getSelection()) {
            buffer.append("Font.SIZE_SMALL");
        } else
        if (this.sizeMediumBtn.getSelection()) {
            buffer.append("Font.SIZE_MEDIUM");
        } else
        if (this.sizeLargeBtn.getSelection()) {
            buffer.append("Font.SIZE_LARGE");
        }
        return buffer.toString();
    }

    private String getFontStyle() {
        StringBuffer buffer = new StringBuffer();
        if (this.styleBoldBtn.getSelection()) {
            buffer.append("Font.STYLE_BOLD");
        }
        
        if (this.styleItalicBtn.getSelection()) {
            if (buffer.length() > 0x00) {
                buffer.append(" | ");
            }
            buffer.append("Font.STYLE_ITALIC");
        }
        
        if (this.styleUnderlineBtn.getSelection()) {
            if (buffer.length() > 0x00) {
                buffer.append(" | ");
            }
            buffer.append("Font.STYLE_UNDERLINED");
        }
        
        if (buffer.length() == 0x00) {
            buffer.append("Font.STYLE_PLAIN");
        }
        
        return buffer.toString();
    }

    private String getFontFace() {
        StringBuffer buffer = new StringBuffer();
        if (this.faceSystemBtn.getSelection()) {
            buffer.append("Font.FACE_SYSTEM");
        } else
        if (this.faceMonospaceBtn.getSelection()) {
            buffer.append("Font.FACE_MONOSPACE");
        } else
        if (this.faceProportionalBtn.getSelection()) {
            buffer.append("Font.FACE_PROPORTIONAL");
        }
        return buffer.toString();
    }
}
