<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.eclipse.mtj.core" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appInfo>
         <meta.schema plugin="org.eclipse.mtj.core" id="securitypermission" name="Security Permission"/>
      </appInfo>
      <documentation>
         This extension point allows third party plugins to add security permissions for third party APIs to the MTJ security permissions set.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appInfo>
            <meta.element />
         </appInfo>
      </annotation>
      <complexType>
         <sequence minOccurs="1" maxOccurs="unbounded">
            <element ref="class"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="class">
      <annotation>
         <documentation>
            A class may require several security permissions. There must be one permission entry for each required permission.
         </documentation>
      </annotation>
      <complexType>
         <sequence minOccurs="1" maxOccurs="unbounded">
            <element ref="permission"/>
         </sequence>
         <attribute name="name" type="string" use="required">
            <annotation>
               <documentation>
                  The name of the class requiring security permissions.
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="permission">
      <annotation>
         <documentation>
            The permission entry for each required permission.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="name" type="string" use="required">
            <annotation>
               <documentation>
                  The name of the permission following the specific name format.
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         1.0
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         &lt;extension
       id=&quot;jsr120permissions&quot; name=&quot;JSR120 Permissions&quot; point=&quot;org.eclipse.mtj.core.securitypermission&quot;&gt;
    &lt;class name=&quot;javax.wireless.messaging.MessageConnection&quot;&gt;
       &lt;permission name=&quot;javax.microedition.io.Connector.sms&quot;&gt;
       &lt;/permission&gt;
       &lt;permission name=&quot;javax.microedition.io.Connector.cbs&quot;&gt;
       &lt;/permission&gt;
       &lt;permission name=&quot;javax.microedition.io.Connector.mms&quot;&gt;
       &lt;/permission&gt;
       &lt;permission name=&quot;javax.wireless.messaging.sms.send&quot;&gt;
       &lt;/permission&gt;
       &lt;permission name=&quot;javax.wireless.messaging.sms.receive&quot;&gt;
       &lt;/permission&gt;
       &lt;permission name=&quot;javax.wireless.messaging.mms.send&quot;&gt;
       &lt;/permission&gt;
       &lt;permission name=&quot;javax.wireless.messaging.mms.receive&quot;&gt;
       &lt;/permission&gt;
    &lt;/class&gt;
 &lt;/extension&gt;
      </documentation>
   </annotation>


   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         [Enter information about supplied implementation of this extension point.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="copyright"/>
      </appInfo>
      <documentation>
         Copyright (c) 2009 Motorola. &lt;br&gt;
All rights reserved. This program and the accompanying materials are made available under the terms of the Eclipse Public License v1.0 which accompanies this distribution, and is available at &lt;a 
href=&quot;http://www.eclipse.org/legal/epl-v10.html&quot;&gt;http://www.eclipse.org/legal/epl-v10.html&lt;/a&gt;
      </documentation>
   </annotation>

</schema>
