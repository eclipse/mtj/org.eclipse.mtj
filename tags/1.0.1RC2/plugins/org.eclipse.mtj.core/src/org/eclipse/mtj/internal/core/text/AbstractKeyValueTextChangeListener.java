/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde
 */
package org.eclipse.mtj.internal.core.text;

import java.util.HashMap;

import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.internal.core.util.PropertiesUtil;
import org.eclipse.text.edits.DeleteEdit;
import org.eclipse.text.edits.InsertEdit;
import org.eclipse.text.edits.ReplaceEdit;
import org.eclipse.text.edits.TextEdit;

/**
 * @since 0.9.1
 */
public abstract class AbstractKeyValueTextChangeListener extends
        AbstractTextChangeListener {

    protected HashMap<TextEdit, String> fReadableNames = null;

    /**
     * @param document
     * @param generateReadableNames
     */
    public AbstractKeyValueTextChangeListener(IDocument document,
            boolean generateReadableNames) {
        super(document);
        if (generateReadableNames) {
            fReadableNames = new HashMap<TextEdit, String>();
        }
    }

    /**
     * @param edit
     * @return
     */
    public String getReadableName(TextEdit edit) {
        if ((fReadableNames != null) && fReadableNames.containsKey(edit)) {
            return fReadableNames.get(edit);
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.text.IModelTextChangeListener#getTextOperations()
     */
    public TextEdit[] getTextOperations() {
        if (operationTable.size() == 0) {
            return new TextEdit[0];
        }
        return operationTable.values().toArray(
                new TextEdit[operationTable.size()]);
    }

    /**
     * @param key
     * @param name
     */
    protected void deleteKey(IDocumentKey key, String name) {
        if (key.getOffset() >= 0) {
            DeleteEdit edit = new DeleteEdit(key.getOffset(), key.getLength());
            operationTable.put(key, edit);
            if (fReadableNames != null) {
                fReadableNames.put(edit, name);
            }
        }
    }

    /**
     * @param key
     * @param name
     */
    protected void insertKey(IDocumentKey key, String name) {
        int offset = PropertiesUtil.getInsertOffset(textDocument);
        InsertEdit edit = new InsertEdit(offset, key.write());
        operationTable.put(key, edit);
        if (fReadableNames != null) {
            fReadableNames.put(edit, name);
        }
    }

    /**
     * @param key
     * @param name
     */
    protected void modifyKey(IDocumentKey key, String name) {
        if (key.getOffset() == -1) {
            insertKey(key, name);
        } else {
            ReplaceEdit edit = new ReplaceEdit(key.getOffset(),
                    key.getLength(), key.write());
            operationTable.put(key, edit);
            if (fReadableNames != null) {
                fReadableNames.put(edit, name);
            }
        }
    }
}
