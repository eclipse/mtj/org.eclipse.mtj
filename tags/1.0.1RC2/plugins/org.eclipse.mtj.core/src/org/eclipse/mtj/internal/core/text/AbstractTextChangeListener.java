/**
 * Copyright (c) 2006,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

import java.util.HashMap;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.text.edits.TextEdit;

/**
 * @since 0.9.1
 */
public abstract class AbstractTextChangeListener implements
        IModelTextChangeListener {

    protected HashMap<IDocumentKey, TextEdit> operationTable = new HashMap<IDocumentKey, TextEdit>();
    protected String separator;
    protected IDocument textDocument;

    /**
     * @param document
     */
    public AbstractTextChangeListener(IDocument document) {
        textDocument = document;
        separator = TextUtilities.getDefaultLineDelimiter(textDocument);
    }

}
