/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.build.export.states;

import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.internal.core.build.export.AntennaBuildExport;
import org.eclipse.mtj.internal.core.build.export.AntennaExportException;
import org.eclipse.mtj.internal.core.statemachine.StateMachine;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * CreateAntCollectResourcesState collects all resources from the required and
 * target projects in order to include it on package.
 * 
 * @author David Marques
 * @since 1.0
 */
public class CreateAntCollectResourcesState extends AbstractCreateAntTaskState {

	/**
	 * Creates a {@link CreateAntCollectResourcesState} instance bound to the
	 * specified state machine in order to create collect-resources target for
	 * the specified project.
	 * 
	 * @param machine
	 *            bound {@link StateMachine} instance.
	 * @param project
	 *            target {@link IMidletSuiteProject} instance.
	 * @param _document
	 *            target {@link Document}.
	 */
	public CreateAntCollectResourcesState(StateMachine machine,
			IMidletSuiteProject project, Document _document) {
		super(machine, project, _document);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mtj.internal.core.build.export.states.AbstractCreateAntTaskState
	 * #onEnter(org.eclipse.mtj.core.project.runtime.MTJRuntime)
	 */
	protected void onEnter(MTJRuntime runtime) throws AntennaExportException {
		IProject project = getMidletSuiteProject().getProject();
		Document document = getDocument();
		Element root = document.getDocumentElement();
		String configName = getFormatedName(runtime.getName());

		Element collect = XMLUtils
				.createTargetElement(
						document,
						root,
						NLS.bind("collect-resources-{0}", configName), getDependencies(runtime)); //$NON-NLS-1$

		IFile file = getMidletSuiteProject().getApplicationDescriptorFile();
		if (file != null && file.exists()) {
			Element copy = document.createElement("copy"); //$NON-NLS-1$
			copy.setAttribute("tofile", NLS.bind( //$NON-NLS-1$
					"deployed/{0}/{1}", new String[] { configName,
							getMidletSuiteProject().getJadFileName() })); //$NON-NLS-1$
			copy.setAttribute("file", file.getProjectRelativePath().toString()); //$NON-NLS-1$
			collect.appendChild(copy);
		}

		Set<IProject> requiredProjects = getRequiredProjects(project);
		requiredProjects.add(project);
		for (IProject required : requiredProjects) {
			Element copy = document.createElement("copy"); //$NON-NLS-1$
			copy.setAttribute("todir", NLS.bind( //$NON-NLS-1$
					"{0}/{1}/{2}/resources/", new String[] {
							AntennaBuildExport.BUILD_FOLDER, configName, //$NON-NLS-1$
							getFormatedName(required.getName()) }));
			collect.appendChild(copy);
			IResource[] sources = Utils.getSourceFolders(JavaCore
					.create(required));
			for (IResource source : sources) {
				Element fileset = document.createElement("fileset"); //$NON-NLS-1$
				fileset.setAttribute("dir", NLS.bind("..{0}", source //$NON-NLS-1$ //$NON-NLS-2$
						.getFullPath()));
				fileset.setAttribute("excludes", "**/**.java"); //$NON-NLS-1$ //$NON-NLS-2$
				copy.appendChild(fileset);
			}
		}
	}

}
