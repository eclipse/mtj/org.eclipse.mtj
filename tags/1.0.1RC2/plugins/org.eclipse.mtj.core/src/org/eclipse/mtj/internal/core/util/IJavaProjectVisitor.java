/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     Eric S. Dias  (Motorola) - Fixing disable JMUnit causes confusion
 */
package org.eclipse.mtj.internal.core.util;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;

/**
 * IJavaProjectVisitor defines methods for visiting an IJavaProject instance.
 * 
 * @see Visitor Pattern.
 * @author David Marques
 * @since 1.0
 */
public interface IJavaProjectVisitor {

    // Constants -----------------------------------------------------

    // Attributes ----------------------------------------------------

    // Static --------------------------------------------------------

    // Constructors --------------------------------------------------

    // Public --------------------------------------------------------

    /**
     * Visits the IPackageFragmentRoot.
     * 
     * @param packageFragmentRoot target IPackageFragmentRoot.
     */
    public void visitPackageFragmentRoot(
            IPackageFragmentRoot packageFragmentRoot);

    /**
     * Visits the IPackageFragment.
     * 
     * @param fragment target IPackageFragment.
     */
    public void visitPackageFragment(IPackageFragment fragment);

    /**
     * Visits the ICompilationUnit.
     * 
     * @param compilationUnit target ICompilationUnit.
     */
    public void visitCompilatioUnit(ICompilationUnit compilationUnit);

    // X implementation ----------------------------------------------

    // Y overrides ---------------------------------------------------

    // Package protected ---------------------------------------------

    // Protected -----------------------------------------------------

    // Private -------------------------------------------------------

}
