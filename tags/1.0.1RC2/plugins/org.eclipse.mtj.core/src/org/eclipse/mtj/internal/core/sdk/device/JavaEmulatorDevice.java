/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.io.File;

import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;

/**
 * An abstract superclass that can be used to help implement an emulator that is
 * launched via Java.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @author Craig Setera
 */
public abstract class JavaEmulatorDevice extends AbstractMIDPDevice {

    /**
     * The list of locations in which to look for the java executable in
     * candidate VM install locations, relative to the VM install location.
     */
    private static final String[] CANDIDATE_JAVA_LOCATIONS = {

            "bin" + File.separatorChar + "javaw", //$NON-NLS-2$ //$NON-NLS-1$
            "bin" + File.separatorChar + "javaw.exe", //$NON-NLS-2$ //$NON-NLS-1$
            "jre" + File.separatorChar + "bin" + File.separatorChar + "javaw", //$NON-NLS-3$ //$NON-NLS-2$ //$NON-NLS-1$
            "jre"   + File.separatorChar + "bin" + File.separatorChar + "javaw.exe", //$NON-NLS-3$ //$NON-NLS-2$ //$NON-NLS-1$									
            "bin" + File.separatorChar + "java", //$NON-NLS-2$ //$NON-NLS-1$
            "bin" + File.separatorChar + "java.exe", //$NON-NLS-2$ //$NON-NLS-1$
            "jre" + File.separatorChar + "bin" + File.separatorChar + "java", //$NON-NLS-3$ //$NON-NLS-2$ //$NON-NLS-1$
            "jre"   + File.separatorChar + "bin" + File.separatorChar + "java.exe" }; //$NON-NLS-3$ //$NON-NLS-2$ //$NON-NLS-1$							

    /**
     * Return the Java executable to be used for launching this device.
     * 
     * @return
     */
    protected File getJavaExecutable() {
        File executable = null;

        IVMInstall vmInstall = JavaRuntime.getDefaultVMInstall();
        File installLocation = vmInstall.getInstallLocation();

        for (int i = 0; i < CANDIDATE_JAVA_LOCATIONS.length; i++) {
            String javaLocation = CANDIDATE_JAVA_LOCATIONS[i];
            File javaExecutable = new File(installLocation, javaLocation);
            if (javaExecutable.exists()) {
                executable = javaExecutable;
                break;
            }
        }

        return executable;
    }
}
