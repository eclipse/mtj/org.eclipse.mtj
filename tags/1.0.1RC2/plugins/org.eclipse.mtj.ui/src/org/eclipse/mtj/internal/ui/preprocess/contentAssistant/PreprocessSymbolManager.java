/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;

/**
 * This class is used to manage the preprocess symbols
 * 
 * @author gma
 * @since 0.9.1
 */
public class PreprocessSymbolManager {
    
    /**
     * @param symbols
     * @param symbolName
     * @param symbolType
     * @param value
     * @param providerName
     * @param isActive
     */
    private static void createSymbol(HashSet<PreprocessSymbol> symbols,
            String symbolName, int symbolType, String value,
            String providerName, boolean isActive) {
        PreprocessSymbol symbol = new PreprocessSymbol(symbolName, symbolType);
        PreprocessSymbolProviderInfo provider = new PreprocessSymbolProviderInfo(
                providerName, value, isActive);
        if (symbols.contains(symbol)) {
            Iterator<PreprocessSymbol> it = symbols.iterator();
            while (it.hasNext()) {
                PreprocessSymbol aSymbol = it.next();
                // find the symbol
                if (symbol.equals(aSymbol)) {
                    aSymbol.addProvider(provider);
                }
            }
        } else {
            symbol.addProvider(provider);
            symbols.add(symbol);
        }
    }

    /**
     * Retrieve all the symbols according to the specific project
     * 
     * @param project
     * @return List of <code>PreprocessSymbol</code>
     */
    public static List<PreprocessSymbol> getSymbols(IProject project) {
        HashSet<PreprocessSymbol> symbolmap = new HashSet<PreprocessSymbol>();

        IMTJProject midletSuite = MidletSuiteFactory
                .getMidletSuiteProject(JavaCore.create(project));
        // add the symbols provided by configurations associated to the project
        MTJRuntimeList configurations = midletSuite.getRuntimeList();
        for (MTJRuntime configuration : configurations) {
            ISymbolSet symbolSet = configuration.getSymbolSetForPreprocessing();
            boolean isActive = configuration.isActive();
            Iterator<ISymbol> it = symbolSet.getSymbols().iterator();
            while (it.hasNext()) {
                ISymbol symbol = it.next();
                createSymbol(symbolmap, symbol.getName(), symbol.getType(),
                        symbol.getValue(), configuration.getName(), isActive);
            }
            // add the symbols from workspace symbol set definition
            List<ISymbolSet> workspaceSymbolSets = configuration
                    .getWorkspaceScopeSymbolSets();

            for (ISymbolSet wsymbolset : workspaceSymbolSets) {
            	Collection<ISymbol> c = wsymbolset.getSymbols(); 
                for (ISymbol s : c) {
                    String key = s.getName();
                    String value = s.getValue();
                    createSymbol(symbolmap, key, 0, value,
                            wsymbolset.getName(), isActive);
                }
            }
        }

        return new ArrayList<PreprocessSymbol>(symbolmap);
    }
}
