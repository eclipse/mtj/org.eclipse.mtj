/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial implementation
 */
package org.eclipse.mtj.internal.ui.editors.jad.form.pages;

import java.util.ArrayList;

import org.eclipse.mtj.internal.ui.editors.IModelListener;
import org.eclipse.mtj.internal.ui.forms.blocks.NamedObject;

/**
 * @author Diego Madruga Sandin
 */
public class MidletType extends NamedObject {

    /**
     * 
     */
    private static final String P_CLASSNAME = "className";

    /**
     * 
     */
    private static final String P_ICON = "icon";

    /**
     * 
     */
    private static final String P_MIDLETNAME = "midletName";

    /**
     * 
     */
    private String midletName;

    /**
     * 
     */
    private String icon;

    /**
     * 
     */
    private String className;

    /**
     * @param id
     * @param definitionString
     */
    MidletType(final String id, final String definitionString) {
        super(id);

        String fields[] = new String[3];
        String[] tokens = tokenize(definitionString, ',');

        for (int i = 0; i < 3; i++) {
            fields[i] = (i > tokens.length) ? "" : tokens[i]; //$NON-NLS-1$
        }
        this.midletName = fields[0];
        this.icon = fields[1];
        this.className = fields[2];
    }

    /**
     * @param id
     * @param name
     * @param icon
     * @param className
     */
    MidletType(final String id, String name, String icon, String className) {
        super(id);

        this.midletName = name;
        this.icon = icon;
        this.className = className;
    }

    /**
     * @return
     */
    public String getClassName() {
        return className;
    }

    /**
     * @return
     */
    public String getIcon() {
        return icon;
    }

    /**
     * @return
     */
    public String getMidletName() {
        return midletName;
    }

    /**
     * @param className
     */
    public void setClassName(String className) {
        this.className = className;
        model.fireModelChanged(new Object[] { this }, IModelListener.CHANGED,
                P_CLASSNAME);
    }

    /**
     * @param icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
        model.fireModelChanged(new Object[] { this }, IModelListener.CHANGED,
                P_ICON);
    }

    /**
     * @param name
     */
    public void setMidletName(String name) {
        this.midletName = name;
        model.fireModelChanged(new Object[] { this }, IModelListener.CHANGED,
                P_MIDLETNAME);
    }

    /**
     * Tokenize the specified string, returning the strings within that string.
     * 
     * @param definition
     * @return
     */
    private String[] tokenize(String string, char delimiter) {
        ArrayList<String> tokens = new ArrayList<String>();

        int offset = 0;
        int delimiterIndex = 0;

        do {
            delimiterIndex = string.indexOf(delimiter, offset);
            if (delimiterIndex == -1) {
                tokens.add(string.substring(offset));
            } else {
                tokens.add(string.substring(offset, delimiterIndex));
                offset = delimiterIndex + 1;
            }
        } while (delimiterIndex != -1);

        return tokens.toArray(new String[tokens.size()]);
    }

    /**
     * Returns a string representation of the MidletType with the "<code>&lt;name&gt;,&lt;icon&gt;,&lt;class&gt;</code>"
     * pattern.
     * 
     * @see org.eclipse.mtj.internal.ui.forms.blocks.NamedObject#toString()
     */
    @Override
    public String toString() {
        return midletName + "," + icon + "," + className;
    }
}
