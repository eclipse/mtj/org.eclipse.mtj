/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Added source view page
 *     Gang Ma      (Sybase)	- Refactoring the editor to support expansibility
 *     Diego Sandin (Motorola)  - Editor title now shows project name to avoid 
 *                                confusion when opening multiple application 
 *                                descriptors
 *     Diego Sandin (Motorola)  - The changes made "Application Descriptor" tab 
 *                                will now reflect on the other tabs
 *     Diego Sandin (Motorola)  - Fix NullPointerException while closing the 
 *                                editor after deleting the MIDlet project
 *     Hugo Raniere (Motorola)  - Listening to changes in project (rename, 
 *				  close, ...) and closing editor
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     David Marques (Motorola) - Adding signing page.
 *     David Marques (Motorola) - Renaming PermissionsPage to SigningJADEditorPage.
 *     David Marques (Motorola) - Adding build properties page.
 *     David Marques (Motorola) - Refactoring build.properties
 *     Rafael Amaral (Motorola) - Changing to make MTJ extension point handle 
 *                                dynamic add/remove of plugins
 */
package org.eclipse.mtj.internal.ui.editors.jad.form;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionDelta;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IRegistryChangeEvent;
import org.eclipse.core.runtime.IRegistryChangeListener;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.build.MTJBuildProperties;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.editors.build.pages.BuildPropertiesPage;
import org.eclipse.mtj.internal.ui.editors.build.pages.BuildPropertiesSourcePage;
import org.eclipse.mtj.internal.ui.editors.jad.form.pages.JADSourceEditorPage;
import org.eclipse.mtj.internal.ui.editors.jad.form.pages.OverviewEditorPage;
import org.eclipse.mtj.internal.ui.editors.jad.form.pages.SigningJADEditorPage;
import org.eclipse.mtj.internal.ui.util.ManifestPreferenceStore;
import org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IInPlaceEditor;
import org.eclipse.ui.IInPlaceEditorInput;
import org.eclipse.ui.IStorageEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.IFormPage;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.FileInPlaceEditorInput;

/**
 * Editor for specifying the properties of a MIDP application descriptor.
 * 
 * @author Craig Setera
 */
public class JADFormEditor extends FormEditor implements IInPlaceEditor,
        IRegistryChangeListener {

    //Instance to listen registry changes
    private static JADFormEditor registryChangeListener;
    
    // all JAD editor page configuration elements
    private static JADEditorPageConfigElement[] jadEditorPageConfigElements;

    // Whether to do a clean just after a save
    private boolean cleanRequired;

    // the JAD editor pages defined as extension
    private List<AbstractJADEditorPage> configPages = new ArrayList<AbstractJADEditorPage>();

    // The underlying file being operated on
    private IFile jadFile;

    // The most recent timestamp of the JAD file
    private long modificationStamp;

    // The preference store we are using to
    // represent the edit state
    private ManifestPreferenceStore preferenceStore;

    // The source editor page in the editor
    private JADSourceEditorPage sourceEditor;

    private IFormPage[] buildPropertiesPages;
    
    private static final String EXT_JAD_EDITOR_PAGE = "jadeditorpage";

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor monitor) {
        monitor.beginTask(MTJUIMessages.JADFormEditor_save_jad_task_name,
                getPageCount() + 1);

        boolean sourceEditorDirty = sourceEditor.isDirty();

        int i = 0;
        Iterator<AbstractJADEditorPage> pageIter = configPages.iterator();

        while (pageIter.hasNext()) {
            Object part = pageIter.next();

            if (part instanceof IFormPage) {

                AbstractJADEditorPage formPage = (AbstractJADEditorPage) part;

                // If the control hasn't been created yet, it can't be dirty yet
                if ((formPage != null) && (formPage.getPartControl() != null)) {
                    if ((formPage.isDirty()) && (!sourceEditorDirty)) {
                        formPage.doSave(monitor);
                        monitor
                                .subTask(NLS
                                        .bind(
                                                MTJUIMessages.JADFormEditor_save_jad_subtask_name,
                                                formPage.getTitle()));
                    } else {
                        monitor
                                .subTask(NLS
                                        .bind(
                                                MTJUIMessages.JADFormEditor_ignore_save_jad_subtask_name,
                                                formPage.getTitle()));
                        formPage.setDirty(false);
                    }
                }
            }
            monitor.worked(i + 1);
        }

        if (this.buildPropertiesPages != null) {
			for (IFormPage page : this.buildPropertiesPages) {
				page.doSave(monitor);
			}
		}
        
        if (sourceEditorDirty) {
            sourceEditor.doSave(monitor);
            updateEditorInput();
            monitor.subTask(NLS.bind(
                    MTJUIMessages.JADFormEditor_save_jad_subtask_name,
                    sourceEditor.getTitle()));
            monitor.worked(i + 1);
        }

        try {
            // Attempt to make the file read/write as necessary, using
            // the resource API so that Team providers can get involved.
            if (jadFile.exists() && jadFile.isReadOnly()) {
                ResourceAttributes attributes = jadFile.getResourceAttributes();
                attributes.setReadOnly(false);
                jadFile.setResourceAttributes(attributes);
            }

            preferenceStore.save();

            if ((jadFile != null) && (jadFile.exists())) {
                jadFile.refreshLocal(IResource.DEPTH_ZERO, monitor);
            }

            // Do a clean if requested
            if ((jadFile != null) && cleanRequired) {
                jadFile.getProject().build(
                        IncrementalProjectBuilder.CLEAN_BUILD, monitor);
            }
        } catch (IOException e) {
            MTJLogger.log(IStatus.ERROR, e);
        } catch (CoreException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }

        monitor.done();
        editorDirtyStateChanged();
        reloadLocalFile();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorPart#doSaveAs()
     */
    @Override
    public void doSaveAs() {
        // Not allowed...
    }

    /**
     * Return the JAD file (if set)
     * 
     * @return
     */
    public IFile getJadFile() {
        return jadFile;
    }

    /**
     * Get the IPreferenceStore in use for the edit function.
     * 
     * @return
     */
    public ManifestPreferenceStore getPreferenceStore() {
        return preferenceStore;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormEditor#isDirty()
     */
    @Override
    @SuppressWarnings("unchecked")
    public boolean isDirty() {
        boolean dirty = false;

        Iterator<Object> pageIter = pages.iterator();
        while (pageIter.hasNext()) {

            Object part = pageIter.next();
            if (part instanceof IFormPage) {

                IFormPage formPage = (IFormPage) part;

                // If the control hasn't been created yet, it can't be dirty yet
                if ((formPage != null) && (formPage.getPartControl() != null)) {
                    if (formPage.isDirty()) {
                        dirty = true;
                        break;
                    }
                }
            }
        }

        return dirty;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
     */
    @Override
    public boolean isSaveAsAllowed() {
        // Don't allow "Save As..."
        return false;
    }

    /**
     * Return a boolean indicating whether the specified property key is an
     * allowable user-defined property.
     * 
     * @param key
     * @return
     */
    public boolean isUserDefinedPropertyKey(String key) {
        Iterator<AbstractJADEditorPage> pageIter = configPages.iterator();
        while (pageIter.hasNext()) {
            AbstractJADEditorPage jadEditorPage = pageIter.next();
            if (jadEditorPage != null) {
                if (jadEditorPage.isManagingProperty(key)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Set a boolean indicating whether a clean is required when the save is
     * complete.
     * 
     * @param value
     */
    public void setCleanRequired(boolean value) {
        cleanRequired = value;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.MultiPageEditorPart#setFocus()
     */
    @Override
    public void setFocus() {
        File localFile = getLocalFile();

        if ((localFile != null)
                && (localFile.lastModified() > modificationStamp)) {
            if (shouldReloadLocalFile()) {
                reloadLocalFile();
            }
        }

        super.setFocus();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IInPlaceEditor#sourceChanged(org.eclipse.ui.IInPlaceEditorInput)
     */
    public void sourceChanged(IInPlaceEditorInput input) {
        getSite().getPage().closeEditor(this, true);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IInPlaceEditor#sourceDeleted()
     */
    public void sourceDeleted() {
        getSite().getPage().closeEditor(this, false);
    }

    /**
     * Return all Config Elements
     * 
     * @return the list of Config Elements
     */
    private JADEditorPageConfigElement[] getAllEditorPageConfigElements() {
        if (jadEditorPageConfigElements == null) {
            jadEditorPageConfigElements = readAllVendorSpecJADAttributes();
            registryChangeListener = new JADFormEditor();
            Platform.getExtensionRegistry().addRegistryChangeListener(registryChangeListener);
        }
        return jadEditorPageConfigElements;
    }

    /**
     * Return the jad file as a local File instance.
     * 
     * @return
     */
    private File getLocalFile() {

        File localfile = null;

        try {
            localfile = jadFile.getLocation().toFile();
        } catch (Exception e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
        return localfile;
    }

    private JADEditorPageConfigElement[] readAllVendorSpecJADAttributes() {
        String plugin = MTJUIPlugin.getDefault().getBundle().getSymbolicName();
        IConfigurationElement[] configElements = Platform
                .getExtensionRegistry().getConfigurationElementsFor(plugin,
                        EXT_JAD_EDITOR_PAGE); //$NON-NLS-1$
        JADEditorPageConfigElement[] resultElements = new JADEditorPageConfigElement[configElements.length];
        for (int i = 0; i < configElements.length; i++) {
            resultElements[i] = new JADEditorPageConfigElement(
                    configElements[i]);
        }
        return resultElements;
    }

    /**
     * 
     */
    private void reloadLocalFile() {
        updateEditorInput();

        Iterator<AbstractJADEditorPage> pageIter = configPages.iterator();
        while (pageIter.hasNext()) {
            AbstractJADEditorPage jadEditorPage = pageIter.next();
            if (jadEditorPage != null) {
                jadEditorPage.editorInputChanged();
            }
        }
    }

    /**
     * Return a boolean indicating whether the user wants to reload the updated
     * file.
     * 
     * @return <code>true</code> if the user presses the OK button,
     *         <code>false</code> otherwise
     */
    private boolean shouldReloadLocalFile() {
        return MessageDialog
                .openQuestion(
                        getSite().getShell(),
                        MTJUIMessages.JADFormEditor_shouldReloadLocalFile_dialod_title,
                        MTJUIMessages.JADFormEditor_shouldReloadLocalFile_dialog_message);
    }

    /**
     * Update the editor input.
     * 
     * @throws IOException
     */
    private void updateEditorInput() {
        File localFile = getLocalFile();
        modificationStamp = localFile.lastModified();
        String filename = localFile.toString();
        preferenceStore = new ManifestPreferenceStore(filename);

        try {
            preferenceStore.load();
        } catch (IOException e) {
            MTJLogger.log(IStatus.WARNING, e);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
     */
    @Override
    protected void addPages() {

        super.setPartName(jadFile.getProject().getName());

        JADEditorPageConfigElement[] pageElements = getAllEditorPageConfigElements();

        // Sort the page elements according to the page element's priority
        Arrays.sort(pageElements, new Comparator<JADEditorPageConfigElement>() {
            public int compare(JADEditorPageConfigElement o1,
                    JADEditorPageConfigElement o2) {
                return o1.getPriority() - o2.getPriority();
            }
        });

        /* Add the overview page */
        try {
            AbstractJADEditorPage page = new OverviewEditorPage(this);
            addPage(page);
            configPages.add(page);
        } catch (PartInitException e1) {
            e1.printStackTrace();
        }

        /* Add the property editor pages */
        for (JADEditorPageConfigElement element : pageElements) {
            try {
                AbstractJADEditorPage page = element.getJADEditorPage();
                page.initialize(this);
                addPage(page);
                configPages.add(page);
            } catch (Exception e) {
                MTJLogger.log(IStatus.ERROR, e);
            }
        }

        try {
        	SigningJADEditorPage signingJADEditorPage = new SigningJADEditorPage(this);
			addPage(signingJADEditorPage);
			configPages.add(signingJADEditorPage);
		} catch (PartInitException e) {
            MTJLogger.log(IStatus.ERROR, e);
		}
        
		try {
			IJavaProject javaProject = JavaCore.create(jadFile.getProject());
			if (javaProject != null) {
				IMidletSuiteProject suiteProject = MidletSuiteFactory.getMidletSuiteProject(javaProject);
				MTJBuildProperties  properties   = MTJBuildProperties.getBuildProperties(suiteProject);
				if (properties.getBuildPropertyFile().exists()) {					
					this.buildPropertiesPages = new IFormPage[] {
							new BuildPropertiesPage(this, properties),
							new BuildPropertiesSourcePage(this, properties)
					};
					for (IFormPage page : this.buildPropertiesPages) {
						this.addPage(page);
					}
				}
			}
		} catch (PartInitException e) {
            MTJLogger.log(IStatus.ERROR, e);
		}
		
        /* Add the source editor page */
        sourceEditor = new JADSourceEditorPage(this);
        try {
            addPage(sourceEditor, super.getEditorInput());
        } catch (PartInitException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
     */
    @Override
    protected void setInput(IEditorInput input) {
        if (input instanceof FileEditorInput) {
            FileInPlaceEditorInput fipei = new FileInPlaceEditorInput(
                    ((FileEditorInput) input).getFile());
            fipei.setInPlaceEditor(this);
            super.setInput(fipei);
        } else {
            super.setInput(input);
        }

        // Update the application descriptor instance
        if (input instanceof IStorageEditorInput) {
            IStorageEditorInput storageInput = (IStorageEditorInput) input;
            try {
                // Read the storage from the file system as
                // a preference store
                IPath storagePath = storageInput.getStorage().getFullPath();
                if (storagePath != null) {
                    IWorkspaceRoot root = MTJCore.getWorkspace()
                            .getRoot();
                    jadFile = root.getFile(storagePath.makeAbsolute());

                    if ((jadFile != null) && (jadFile.exists())) {
                        updateEditorInput();
                    }
                }

            } catch (Exception e) {
                MTJLogger.log(IStatus.WARNING, e);
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.core.runtime.IRegistryChangeListener#registryChanged(org.
     * eclipse.core.runtime.IRegistryChangeEvent)
     */
    public void registryChanged(IRegistryChangeEvent event) {
        
        String plugin = MTJUIPlugin.getDefault().getBundle().getSymbolicName();
        IExtensionDelta[] deltas = event.getExtensionDeltas(plugin,
                EXT_JAD_EDITOR_PAGE);
        
        if (deltas.length > 0) {
            jadEditorPageConfigElements = readAllVendorSpecJADAttributes();
        }
    }

}
