/**
 * Copyright (c) 2004, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Red Hat, Inc - extensive changes to allow importing of Archive Files
 *     Philippe Ombredanne (pombredanne@nexb.com)
 *     		- Bug 101180 [Import/Export] Import Existing Project into Workspace default widget is back button , should be text field
 *     Martin Oberhuber (martin.oberhuber@windriver.com)
 *     		- Bug 187318[Wizards] "Import Existing Project" loops forever with cyclic symbolic links
 *     Remy Chi Jian Suen  (remy.suen@gmail.com)
 *     		- Bug 210568 [Import/Export] [Import/Export] - Refresh button does not update list of projects
 *     Feng Wang (Sybase) - Copy from 
 *     		org.eclipse.ui.internal.wizards.datatransfer.WizardProjectsImportPage,
 *     		which located in org.eclipse.ui.ide plug-in. And Modify it to suit for
 *          EclipseME project importing.
 *	   Hugo Raniere - Moved from
 *			org.eclipse.mtj.internal.ui.wizards.importer.eclipse and modified to
 *			be used by other project importers
 */
package org.eclipse.mtj.internal.ui.wizards.importer.common;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.ui.wizards.ClassPathDetector;
import org.eclipse.jdt.ui.wizards.JavaCapabilityConfigurationPage;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.DialogPage;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteProject;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory.MidletSuiteCreationRunnable;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.IOverwriteQuery;
import org.eclipse.ui.wizards.datatransfer.FileSystemStructureProvider;
import org.eclipse.ui.wizards.datatransfer.ImportOperation;

/**
 * The WizardProjectsImportPage is the page that allows the user to import
 * projects from a particular location.
 */
public class ImportProjectWizardPage extends WizardPage implements
        IOverwriteQuery {

    // constant from WizardArchiveFileResourceImportPage1
    private static final String[] FILE_IMPORT_MASK = {
            "*.jar;*.zip;*.tar;*.tar.gz;*.tgz", "*.*" }; //$NON-NLS-1$ //$NON-NLS-2$

    // Keep track of the archive that we browsed to last time
    // the wizard was invoked.
    private static String previouslyBrowsedArchive = ""; //$NON-NLS-1$

    // Keep track of the directory that we browsed to last time
    // the wizard was invoked.
    private static String previouslyBrowsedDirectory = ""; //$NON-NLS-1$

    private final static String STORE_ARCHIVE_SELECTED = "ProjectImportWizard.STORE_ARCHIVE_SELECTED"; //$NON-NLS-1$

    private Text archivePathField;

    private Button browseArchivesButton;

    private Button browseDirectoriesButton;

    private Text directoryPathField;

    private ProjectRecord[] foundProjects = new ProjectRecord[0];

    private ProjectImporter importer;

    // The last time that the file or folder at the selected path was modified
    // to minimize searches
    private long lastModified;

    // The last selected path to minimize searches
    private String lastPath;

    private String productName;

    private Button projectFromArchiveRadio;
    private Button projectFromDirectoryRadio;

    private CheckboxTreeViewer projectsList;

    /**
     * The import structure provider.
     * 
     * @since 3.4
     */
    private ILeveledImportStructureProvider structureProvider;

    private IProject[] wsProjects;

    /**
     * Creates a new project creation wizard page.
     */
    public ImportProjectWizardPage(ProjectImporter importer, String productName) {
        this("wizardExternalProjectsPage", importer, productName); //$NON-NLS-1$
    }

    /**
     * Create a new instance of the receiver.
     * 
     * @param pageName
     */
    public ImportProjectWizardPage(String pageName, ProjectImporter importer,
            String productName) {
        super(pageName);
        this.importer = importer;
        this.productName = productName;
        setPageComplete(false);
        setTitle(NLS
                .bind(
                        ProjectImporterMessage.WizardProjectsImportPage_ImportProjectsTitle,
                        productName));
        setDescription(NLS
                .bind(
                        ProjectImporterMessage.WizardProjectsImportPage_ImportProjectsDescription,
                        productName));
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets
     * .Composite)
     */
    public void createControl(Composite parent) {

        initializeDialogUnits(parent);

        Composite workArea = new Composite(parent, SWT.NONE);
        setControl(workArea);

        workArea.setLayout(new GridLayout());
        workArea.setLayoutData(new GridData(GridData.FILL_BOTH
                | GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL));

        createProjectsRoot(workArea);
        createProjectsList(workArea);
        restoreWidgetValues();
        Dialog.applyDialogFont(workArea);

    }

    /**
     * Create the selected projects
     * 
     * @return boolean <code>true</code> if all project creations were
     *         successful.
     */
    public boolean createProjects() {
        saveWidgetValues();
        final Object[] selected = projectsList.getCheckedElements();
        WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
            protected void execute(IProgressMonitor monitor)
                    throws InvocationTargetException, InterruptedException {
                try {
                    monitor.beginTask("", selected.length); //$NON-NLS-1$
                    if (monitor.isCanceled()) {
                        throw new OperationCanceledException();
                    }
                    for (int i = 0; i < selected.length; i++) {
                        createExistingProject((ProjectRecord) selected[i],
                                new SubProgressMonitor(monitor, 1));
                    }
                } finally {
                    monitor.done();
                }
            }
        };
        // run the new project creation operation
        try {
            getContainer().run(true, true, op);
        } catch (InterruptedException e) {
            return false;
        } catch (InvocationTargetException e) {
            // one of the steps resulted in a core exception
            Throwable t = e.getTargetException();
            String message = ProjectImporterMessage.WizardExternalProjectImportPage_errorMessage;
            IStatus status;
            if (t instanceof CoreException) {
                status = ((CoreException) t).getStatus();
            } else {
                status = new Status(IStatus.ERROR, IMTJUIConstants.PLUGIN_ID,
                        1, message, t);
            }
            ErrorDialog.openError(getShell(), message, null, status);
            return false;
        }
        ArchiveFileManipulations.closeStructureProvider(structureProvider,
                getShell());
        return true;
    }

    public ProjectRecord[] getFoundProjects() {
        return foundProjects;
    }

    /**
     * Method used for test suite.
     * 
     * @return Button the Import from Directory RadioButton
     */
    public Button getProjectFromDirectoryRadio() {
        return projectFromDirectoryRadio;
    }

    /**
     * Method used for test suite.
     * 
     * @return CheckboxTreeViewer the viewer containing all the projects found
     */
    public CheckboxTreeViewer getProjectsList() {
        return projectsList;
    }

    /**
     * Performs clean-up if the user cancels the wizard without doing anything
     */
    public void performCancel() {
        ArchiveFileManipulations.closeStructureProvider(structureProvider,
                getShell());
    }

    /**
     * The <code>WizardDataTransfer</code> implementation of this
     * <code>IOverwriteQuery</code> method asks the user whether the existing
     * resource at the given path should be overwritten.
     * 
     * @param pathString
     * @return the user's reply: one of <code>"YES"</code>, <code>"NO"</code>,
     *         <code>"ALL"</code>, or <code>"CANCEL"</code>
     */
    public String queryOverwrite(String pathString) {

        Path path = new Path(pathString);

        String messageString;
        // Break the message up if there is a file name and a directory
        // and there are at least 2 segments.
        if (path.getFileExtension() == null || path.segmentCount() < 2) {
            messageString = NLS.bind(
                    ProjectImporterMessage.WizardDataTransfer_existsQuestion,
                    pathString);
        } else {
            messageString = NLS
                    .bind(
                            ProjectImporterMessage.WizardDataTransfer_overwriteNameAndPathQuestion,
                            path.lastSegment(), path.removeLastSegments(1)
                                    .toOSString());
        }

        final MessageDialog dialog = new MessageDialog(getContainer()
                .getShell(), ProjectImporterMessage.Question, null,
                messageString, MessageDialog.QUESTION, new String[] {
                        IDialogConstants.YES_LABEL,
                        IDialogConstants.YES_TO_ALL_LABEL,
                        IDialogConstants.NO_LABEL,
                        IDialogConstants.NO_TO_ALL_LABEL,
                        IDialogConstants.CANCEL_LABEL }, 0);
        String[] response = new String[] { YES, ALL, NO, NO_ALL, CANCEL };
        // run in syncExec because callback is from an operation,
        // which is probably not running in the UI thread.
        getControl().getDisplay().syncExec(new Runnable() {
            public void run() {
                dialog.open();
            }
        });
        return dialog.getReturnCode() < 0 ? CANCEL : response[dialog
                .getReturnCode()];
    }

    /**
     * Use the dialog store to restore widget values to the values that they
     * held last time this wizard was used to completion. Method declared public
     * only for use of tests.
     */
    public void restoreWidgetValues() {
        IDialogSettings settings = getDialogSettings();
        if (settings != null) {

            // radio selection
            boolean archiveSelected = settings
                    .getBoolean(STORE_ARCHIVE_SELECTED);
            projectFromDirectoryRadio.setSelection(!archiveSelected);
            projectFromArchiveRadio.setSelection(archiveSelected);
            if (archiveSelected) {
                archiveRadioSelected();
            } else {
                directoryRadioSelected();
            }
        }
    }

    /**
     * Since Finish was pressed, write widget values to the dialog store so that
     * they will persist into the next invocation of this wizard page. Method
     * declared public only for use of tests.
     */
    public void saveWidgetValues() {
        IDialogSettings settings = getDialogSettings();
        if (settings != null) {
            settings.put(STORE_ARCHIVE_SELECTED, projectFromArchiveRadio
                    .getSelection());
        }
    }

    /*
     * (non-Javadoc) Method declared on IDialogPage. Set the focus on path
     * fields when page becomes visible.
     */
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (visible && this.projectFromDirectoryRadio.getSelection()) {
            this.directoryPathField.setFocus();
        }
        if (visible && this.projectFromArchiveRadio.getSelection()) {
            this.archivePathField.setFocus();
        }
    }

    /**
     * Update the list of projects based on path. Method declared public only
     * for test suite.
     * 
     * @param path
     */
    public void updateProjectsList(final String path) {
        // on an empty path empty selectedProjects
        if (path == null || path.length() == 0) {
            foundProjects = new ProjectRecord[0];
            projectsList.refresh(true);
            projectsList.setCheckedElements(foundProjects);
            checkPageComplete();
            lastPath = path;
            return;
        }

        final File directory = new File(path);
        long modified = directory.lastModified();
        if (path.equals(lastPath) && lastModified == modified) {
            // since the file/folder was not modified and the path did not
            // change, no refreshing is required
            return;
        }

        lastPath = path;
        lastModified = modified;

        // We can't access the radio button from the inner class so get the
        // status beforehand
        final boolean dirSelected = this.projectFromDirectoryRadio
                .getSelection();
        try {
            getContainer().run(true, true, new IRunnableWithProgress() {

                /*
                 * (non-Javadoc)
                 * 
                 * @see
                 * org.eclipse.jface.operation.IRunnableWithProgress#run(org
                 * .eclipse.core.runtime.IProgressMonitor)
                 */
                public void run(IProgressMonitor monitor) {

                    monitor
                            .beginTask(
                                    ProjectImporterMessage.WizardProjectsImportPage_SearchingMessage,
                                    100);
                    foundProjects = new ProjectRecord[0];
                    if (!dirSelected) {
                        if (ArchiveFileManipulations.isTarFile(path)) {
                            TarFile sourceTarFile = getSpecifiedTarSourceFile(path);
                            if (sourceTarFile == null) {
                                return;
                            }
                            structureProvider = new TarLeveledStructureProvider(
                                    sourceTarFile);
                        } else if (ArchiveFileManipulations.isZipFile(path)) {
                            ZipFile sourceFile = getSpecifiedZipSourceFile(path);
                            if (sourceFile == null) {
                                return;
                            }
                            structureProvider = new ZipLeveledStructureProvider(
                                    sourceFile);
                        }
                        foundProjects = importer.searchProjectsFromProvider(
                                structureProvider, SubMonitor.convert(monitor,
                                        100).newChild(100));
                    }

                    else if (dirSelected && directory.isDirectory()) {
                        foundProjects = importer.searchProjectsFromDirectory(
                                directory, SubMonitor.convert(monitor, 100)
                                        .newChild(100));
                    }
                    monitor.done();
                }

            });
        } catch (InvocationTargetException e) {
            MTJLogger.log(IStatus.ERROR, e.getMessage(), e);
        } catch (InterruptedException e) {
            // Nothing to do if the user interrupts.
        }

        projectsList.refresh(true);
        projectsList.setCheckedElements(foundProjects);
        checkPageComplete();
    }

    private void archiveRadioSelected() {
        if (projectFromArchiveRadio.getSelection()) {
            directoryPathField.setEnabled(false);
            browseDirectoriesButton.setEnabled(false);
            archivePathField.setEnabled(true);
            browseArchivesButton.setEnabled(true);
            updateProjectsList(archivePathField.getText());
            archivePathField.setFocus();
        }
    }

    private void checkPageComplete() {

        for (Object element : projectsList.getCheckedElements()) {
            ProjectRecord project = (ProjectRecord) element;
            if (isProjectInWorkspace(project.getProjectName())) {
                setMessage(
                        NLS
                                .bind(
                                        ProjectImporterMessage.ImportProjectWizardPage_ProjectAlreadyInWorkpace,
                                        project.getProjectName()),
                        DialogPage.ERROR);
                setPageComplete(false);
                return;
            }
        }
        setMessage(NLS
                .bind(
                        ProjectImporterMessage.WizardProjectsImportPage_ImportProjectsDescription,
                        productName));
        setPageComplete(projectsList.getCheckedElements().length > 0);
    }

    /**
     * Create the project described in record. If it is successful return true.
     * 
     * @param record
     * @return boolean <code>true</code> if successful
     * @throws InterruptedException
     */
    @SuppressWarnings("unchecked")
    private boolean createExistingProject(final ProjectRecord record,
            IProgressMonitor monitor) throws InvocationTargetException,
            InterruptedException {
        String projectName = record.getProjectName();
        final IWorkspace workspace = MTJCore.getWorkspace();
        final IProject project = workspace.getRoot().getProject(projectName);

        IProjectDescription newProjectDescription = workspace
                .newProjectDescription(projectName);

        // HOOK: Call any pre-project creation process here.

        try {
            monitor
                    .beginTask(
                            NLS
                                    .bind(
                                            ProjectImporterMessage.WizardProjectsImportPage_CreateProjectTask,
                                            record.getProjectName()), 100);
            JavaCapabilityConfigurationPage.createProject(project,
                    newProjectDescription.getLocationURI(),
                    new SubProgressMonitor(monitor, 20));

            if (record.getProvider() == null) { // import from file system
                IPath locationPath = new Path(new File(record.getProjectRoot())
                        .getAbsolutePath());
                File importSource = locationPath.toFile();
                // If it is not under the root, copy files
                if (!Platform.getLocation().isPrefixOf(locationPath)) {
                    List<File> filesToImport = FileSystemStructureProvider.INSTANCE
                            .getChildren(importSource);
                    ImportOperation operation = new ImportOperation(project
                            .getFullPath(), importSource,
                            FileSystemStructureProvider.INSTANCE, this,
                            filesToImport);
                    operation.setContext(getShell());
                    operation.setOverwriteResources(true); // may need to
                    // overwrite
                    // .project, .classpath files
                    operation.setCreateContainerStructure(false);
                    operation.run(new SubProgressMonitor(monitor, 20));

                    monitor.worked(50);
                    importer.projectCreated(record, project,
                            new SubProgressMonitor(monitor, 20));
                }
            } else { // import from archive
                Object root = ArchiveUtils
                        .getChild(record.getProvider(), record.getProvider()
                                .getRoot(), record.getProjectRoot());
                List<File> fileSystemObjects = record.getProvider()
                        .getChildren(root);

                // guess strip level
                int stripLevel = ArchiveUtils.guessStripLevel(record
                        .getProjectRoot());
                record.getProvider().setStrip(stripLevel);
                ImportOperation operation = new ImportOperation(project
                        .getFullPath(), record.getProvider().getRoot(), record
                        .getProvider(), this, fileSystemObjects);
                operation.setContext(getShell());
                operation.run(monitor);
                importer.projectCreated(record, project,
                        new SubProgressMonitor(monitor, 20));
            }

            monitor
                    .setTaskName(ProjectImporterMessage.WizardProjectsImportPage_ProcessingMessage);

            // Refreshes the project's folder in order for the ClassPathDetector
            // to work properly
            project.refreshLocal(IResource.DEPTH_INFINITE, null);

            ClassPathDetector detector = new ClassPathDetector(project, null);
            JavaCapabilityConfigurationPage javaPropPage = new JavaCapabilityConfigurationPage();
            javaPropPage.init(JavaCore.create(project), detector
                    .getOutputLocation(), detector.getClasspath(), false);
            javaPropPage.configureJavaProject(null);

            removeJ2SELibraries(javaPropPage.getJavaProject(), monitor);

            IDevice device = importer.getProjectDevice(record);

            MidletSuiteCreationRunnable builder = MidletSuiteFactory
                    .getMidletSuiteCreationRunnable(project, javaPropPage
                            .getJavaProject(), (IMIDPDevice) device,
                            MidletSuiteProject.getDefaultJadFileName(project));
            builder.run(monitor);

            // Build imported project
            project.build(IncrementalProjectBuilder.FULL_BUILD, monitor);

            monitor.done();

        } catch (CoreException e) {
            try {
                // rollback
                project.delete(true, monitor);
            } catch (CoreException ignored) {
            }
            throw new InvocationTargetException(e);
        }
        return true;
    }

    /**
     * Create the checkbox list for the found projects.
     * 
     * @param workArea
     */
    private void createProjectsList(Composite workArea) {

        Label title = new Label(workArea, SWT.NONE);
        title
                .setText(ProjectImporterMessage.WizardProjectsImportPage_ProjectsListTitle);

        Composite listComposite = new Composite(workArea, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.marginWidth = 0;
        layout.makeColumnsEqualWidth = false;
        listComposite.setLayout(layout);

        listComposite.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
                | GridData.GRAB_VERTICAL | GridData.FILL_BOTH));

        projectsList = new CheckboxTreeViewer(listComposite, SWT.BORDER);
        GridData listData = new GridData(GridData.GRAB_HORIZONTAL
                | GridData.GRAB_VERTICAL | GridData.FILL_BOTH);
        projectsList.getControl().setLayoutData(listData);

        projectsList.setContentProvider(new ITreeContentProvider() {

            /*
             * (non-Javadoc)
             * 
             * @see org.eclipse.jface.viewers.IContentProvider#dispose()
             */
            public void dispose() {

            }

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java
             * .lang.Object)
             */
            public Object[] getChildren(Object parentElement) {
                return null;
            }

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.IStructuredContentProvider#getElements
             * (java.lang.Object)
             */
            public Object[] getElements(Object inputElement) {
                return getFoundProjects();
            }

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.ITreeContentProvider#getParent(java
             * .lang.Object)
             */
            public Object getParent(Object element) {
                return null;
            }

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java
             * .lang.Object)
             */
            public boolean hasChildren(Object element) {
                return false;
            }

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse
             * .jface.viewers.Viewer, java.lang.Object, java.lang.Object)
             */
            public void inputChanged(Viewer viewer, Object oldInput,
                    Object newInput) {
            }

        });

        projectsList.setLabelProvider(new LabelProvider() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
             */
            public String getText(Object element) {
                return ((ProjectRecord) element).getProjectLabel();
            }
        });

        projectsList.addCheckStateListener(new ICheckStateListener() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.ICheckStateListener#checkStateChanged
             * (org.eclipse.jface.viewers.CheckStateChangedEvent)
             */
            public void checkStateChanged(CheckStateChangedEvent event) {
                checkPageComplete();
            }
        });

        projectsList.setInput(this);
        projectsList.setComparator(new ViewerComparator());
        createSelectionButtons(listComposite);
    }

    /**
     * Create the area where you select the root directory for the projects.
     * 
     * @param workArea Composite
     */
    private void createProjectsRoot(Composite workArea) {

        // project specification group
        Composite projectGroup = new Composite(workArea, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 3;
        layout.makeColumnsEqualWidth = false;
        layout.marginWidth = 0;
        projectGroup.setLayout(layout);
        projectGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        // new project from directory radio button
        projectFromDirectoryRadio = new Button(projectGroup, SWT.RADIO);
        projectFromDirectoryRadio
                .setText(ProjectImporterMessage.WizardProjectsImportPage_RootSelectTitle);

        // project location entry field
        this.directoryPathField = new Text(projectGroup, SWT.BORDER);

        this.directoryPathField.setLayoutData(new GridData(
                GridData.FILL_HORIZONTAL | GridData.GRAB_HORIZONTAL));

        // browse button
        browseDirectoriesButton = new Button(projectGroup, SWT.PUSH);
        browseDirectoriesButton
                .setText(ProjectImporterMessage.DataTransfer_browse);
        setButtonLayoutData(browseDirectoriesButton);

        // new project from archive radio button
        projectFromArchiveRadio = new Button(projectGroup, SWT.RADIO);
        projectFromArchiveRadio
                .setText(ProjectImporterMessage.WizardProjectsImportPage_ArchiveSelectTitle);

        // project location entry field
        archivePathField = new Text(projectGroup, SWT.BORDER);

        archivePathField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL
                | GridData.GRAB_HORIZONTAL));
        // browse button
        browseArchivesButton = new Button(projectGroup, SWT.PUSH);
        browseArchivesButton
                .setText(ProjectImporterMessage.DataTransfer_browse);
        setButtonLayoutData(browseArchivesButton);

        projectFromDirectoryRadio.setSelection(true);
        archivePathField.setEnabled(false);
        browseArchivesButton.setEnabled(false);

        browseDirectoriesButton.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see org.eclipse.swt.events.SelectionAdapter#widgetS
             * elected(org.eclipse.swt.events.SelectionEvent)
             */
            public void widgetSelected(SelectionEvent e) {
                handleLocationDirectoryButtonPressed();
            }

        });

        browseArchivesButton.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            public void widgetSelected(SelectionEvent e) {
                handleLocationArchiveButtonPressed();
            }

        });

        directoryPathField.addTraverseListener(new TraverseListener() {

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.TraverseListener#keyTraversed(org.eclipse
             * .swt.events.TraverseEvent)
             */
            public void keyTraversed(TraverseEvent e) {
                if (e.detail == SWT.TRAVERSE_RETURN) {
                    e.doit = false;
                    updateProjectsList(directoryPathField.getText().trim());
                }
            }

        });

        directoryPathField.addFocusListener(new FocusAdapter() {

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.FocusListener#focusLost(org.eclipse.swt
             * .events.FocusEvent)
             */
            public void focusLost(org.eclipse.swt.events.FocusEvent e) {
                updateProjectsList(directoryPathField.getText().trim());
            }

        });

        archivePathField.addTraverseListener(new TraverseListener() {

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.TraverseListener#keyTraversed(org.eclipse
             * .swt.events.TraverseEvent)
             */
            public void keyTraversed(TraverseEvent e) {
                if (e.detail == SWT.TRAVERSE_RETURN) {
                    e.doit = false;
                    updateProjectsList(archivePathField.getText().trim());
                }
            }

        });

        archivePathField.addFocusListener(new FocusAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.FocusListener#focusLost(org.eclipse.swt
             * .events.FocusEvent)
             */
            public void focusLost(org.eclipse.swt.events.FocusEvent e) {
                updateProjectsList(archivePathField.getText().trim());
            }
        });

        projectFromDirectoryRadio.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            public void widgetSelected(SelectionEvent e) {
                directoryRadioSelected();
            }
        });

        projectFromArchiveRadio.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            public void widgetSelected(SelectionEvent e) {
                archiveRadioSelected();
            }
        });
    }

    /**
     * Create the selection buttons in the listComposite.
     * 
     * @param listComposite
     */
    private void createSelectionButtons(Composite listComposite) {
        Composite buttonsComposite = new Composite(listComposite, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.marginWidth = 0;
        layout.marginHeight = 0;
        buttonsComposite.setLayout(layout);

        buttonsComposite.setLayoutData(new GridData(
                GridData.VERTICAL_ALIGN_BEGINNING));

        Button selectAll = new Button(buttonsComposite, SWT.PUSH);
        selectAll.setText(ProjectImporterMessage.DataTransfer_selectAll);
        selectAll.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                projectsList.setCheckedElements(foundProjects);
                checkPageComplete();
            }
        });
        Dialog.applyDialogFont(selectAll);
        setButtonLayoutData(selectAll);

        Button deselectAll = new Button(buttonsComposite, SWT.PUSH);
        deselectAll.setText(ProjectImporterMessage.DataTransfer_deselectAll);
        deselectAll.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            public void widgetSelected(SelectionEvent e) {

                projectsList.setCheckedElements(new Object[0]);
                checkPageComplete();
            }
        });
        Dialog.applyDialogFont(deselectAll);
        setButtonLayoutData(deselectAll);

        Button refresh = new Button(buttonsComposite, SWT.PUSH);
        refresh.setText(ProjectImporterMessage.DataTransfer_refresh);
        refresh.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            public void widgetSelected(SelectionEvent e) {
                if (projectFromDirectoryRadio.getSelection()) {
                    updateProjectsList(directoryPathField.getText().trim());
                } else {
                    updateProjectsList(archivePathField.getText().trim());
                }
            }
        });
        Dialog.applyDialogFont(refresh);
        setButtonLayoutData(refresh);
    }

    private void directoryRadioSelected() {
        if (projectFromDirectoryRadio.getSelection()) {
            directoryPathField.setEnabled(true);
            browseDirectoriesButton.setEnabled(true);
            archivePathField.setEnabled(false);
            browseArchivesButton.setEnabled(false);
            updateProjectsList(directoryPathField.getText());
            directoryPathField.setFocus();
        }
    }

    /**
     * display a error message dialog
     * 
     * @param title
     * @param message
     */
    private void displayErrorMessageDialog(final String title,
            final String message) {
        Runnable runnable = new Runnable() {

            public void run() {
                MessageDialog.openError(getShell(), title, message);
            }

        };
        getShell().getDisplay().syncExec(runnable);
    }

    /**
     * Retrieve all the projects in the current workspace.
     * 
     * @return IProject[] array of IProject in the current workspace
     */
    private IProject[] getProjectsInWorkspace() {
        if (wsProjects == null) {
            wsProjects = MTJCore.getWorkspace().getRoot().getProjects();
        }
        return wsProjects;
    }

    /**
     * Answer a handle to the zip file currently specified as being the source.
     * Return null if this file does not exist or is not of valid format.
     */
    private TarFile getSpecifiedTarSourceFile(String fileName) {
        if (fileName.length() == 0) {
            return null;
        }

        try {
            return new TarFile(fileName);
        } catch (TarException e) {
            displayErrorMessageDialog(
                    ProjectImporterMessage.WizardImportPage_internalErrorTitle,
                    ProjectImporterMessage.TarImport_badFormat);
        } catch (IOException e) {
            displayErrorMessageDialog(
                    ProjectImporterMessage.WizardImportPage_internalErrorTitle,
                    ProjectImporterMessage.ZipImport_couldNotRead);
        }

        archivePathField.setFocus();
        return null;
    }

    /**
     * Answer a handle to the zip file currently specified as being the source.
     * Return null if this file does not exist or is not of valid format.
     */
    private ZipFile getSpecifiedZipSourceFile(String fileName) {
        if (fileName.length() == 0) {
            return null;
        }

        try {
            return new ZipFile(fileName);
        } catch (ZipException e) {
            displayErrorMessageDialog(
                    ProjectImporterMessage.WizardImportPage_internalErrorTitle,
                    ProjectImporterMessage.ZipImport_badFormat);
        } catch (IOException e) {
            displayErrorMessageDialog(
                    ProjectImporterMessage.WizardImportPage_internalErrorTitle,
                    ProjectImporterMessage.ZipImport_couldNotRead);
        }

        archivePathField.setFocus();
        return null;
    }

    /**
     * Return a boolean indicating whether or not the specified class path entry
     * represents the standard J2SE libraries.
     * 
     * @param entry
     * @return
     */
    private boolean isJ2SELibraryEntry(IClasspathEntry entry) {
        boolean isJ2SEEntry = false;

        if (entry.getEntryKind() == IClasspathEntry.CPE_LIBRARY) {
            if (entry.getPath().lastSegment().equals("JRE_LIB")) { //$NON-NLS-1$
                isJ2SEEntry = true;
            }
        } else if (entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER) {
            if (entry.getPath().lastSegment().equals(
                    "org.eclipse.jdt.launching.JRE_CONTAINER")) { //$NON-NLS-1$
                isJ2SEEntry = true;
            }
        }
        return isJ2SEEntry;
    }

    /**
     * Determine if the project with the given name is in the current workspace.
     * 
     * @param projectName String the project name to check
     * @return boolean true if the project with the given name is in this
     *         workspace
     */
    private boolean isProjectInWorkspace(String projectName) {
        if (projectName == null) {
            return false;
        }
        IProject[] workspaceProjects = getProjectsInWorkspace();
        for (int i = 0; i < workspaceProjects.length; i++) {
            if (projectName.equals(workspaceProjects[i].getName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Remove the Java SE standard libraries from the specified IJavaProject
     * instance.
     * 
     * @param javaProject
     */
    private void removeJ2SELibraries(IJavaProject javaProject,
            IProgressMonitor monitor) throws JavaModelException {
        IClasspathEntry[] entries = javaProject.getRawClasspath();
        ArrayList<IClasspathEntry> list = new ArrayList<IClasspathEntry>();

        for (int i = 0; i < entries.length; i++) {
            if (!isJ2SELibraryEntry(entries[i])) {
                list.add(entries[i]);
            }
        }

        entries = list.toArray(new IClasspathEntry[list.size()]);
        javaProject.setRawClasspath(entries, monitor);
    }

    /**
     * The browse button has been selected. Select the location.
     */
    protected void handleLocationArchiveButtonPressed() {

        FileDialog dialog = new FileDialog(archivePathField.getShell());
        dialog.setFilterExtensions(FILE_IMPORT_MASK);
        dialog
                .setText(ProjectImporterMessage.WizardProjectsImportPage_SelectArchiveDialogTitle);

        String fileName = archivePathField.getText().trim();
        if (fileName.length() == 0) {
            fileName = previouslyBrowsedArchive;
        }

        if (fileName.length() == 0) {
            dialog.setFilterPath(MTJCore.getWorkspace().getRoot().getLocation()
                    .toOSString());
        } else {
            File path = new File(fileName).getParentFile();
            if (path != null && path.exists()) {
                dialog.setFilterPath(path.toString());
            }
        }

        String selectedArchive = dialog.open();
        if (selectedArchive != null) {
            previouslyBrowsedArchive = selectedArchive;
            archivePathField.setText(previouslyBrowsedArchive);
            updateProjectsList(selectedArchive);
        }

    }

    /**
     * The browse button has been selected. Select the location.
     */
    protected void handleLocationDirectoryButtonPressed() {

        DirectoryDialog dialog = new DirectoryDialog(directoryPathField
                .getShell());
        dialog
                .setMessage(ProjectImporterMessage.WizardProjectsImportPage_SelectDialogTitle);

        String dirName = directoryPathField.getText().trim();
        if (dirName.length() == 0) {
            dirName = previouslyBrowsedDirectory;
        }

        if (dirName.length() == 0) {
            dialog.setFilterPath(MTJCore.getWorkspace().getRoot().getLocation()
                    .toOSString());
        } else {
            File path = new File(dirName);
            if (path.exists()) {
                dialog.setFilterPath(new Path(dirName).toOSString());
            }
        }

        String selectedDirectory = dialog.open();
        if (selectedDirectory != null) {
            previouslyBrowsedDirectory = selectedDirectory;
            directoryPathField.setText(previouslyBrowsedDirectory);
            updateProjectsList(selectedDirectory);
        }

    }
}
