/**
 * Copyright (c) 2004, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     Red Hat, Inc - extensive changes to allow importing of Archive Files
 *     Philippe Ombredanne (pombredanne@nexb.com)
 *     		- Bug 101180 [Import/Export] Import Existing Project into Workspace default widget is back button , should be text field
 *     Martin Oberhuber (martin.oberhuber@windriver.com)
 *     		- Bug 187318[Wizards] "Import Existing Project" loops forever with cyclic symbolic links
 *     Remy Chi Jian Suen  (remy.suen@gmail.com)
 *     		- Bug 210568 [Import/Export] [Import/Export] - Refresh button does not update list of projects
 *     Feng Wang (Sybase) - Copy from 
 *     		org.eclipse.ui.internal.wizards.datatransfer.WizardProjectsImportPage,
 *     		which located in org.eclipse.ui.ide plug-in. And Modify it to suit for
 *          EclipseME project importing.
 *     Hugo Raniere (Motorola) - Refactoring to reuse code on "import project from
 *     		NetBeans"
 *     Feng Wang (Sybase) - Fix bug251675, ensure that EclipseME library container
 *     						will be deleted
 *     Gustavo de Paula (Motorola) - Refactor mtj project converter
 */
package org.eclipse.mtj.internal.ui.wizards.importer.eclipseme;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.midp.ProjectConvertionException;
import org.eclipse.mtj.internal.core.project.midp.MTJProjectConverter;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ArchiveFileManipulations;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ILeveledImportStructureProvider;
import org.eclipse.mtj.internal.ui.wizards.importer.common.StatusUtil;
import org.eclipse.mtj.internal.ui.wizards.importer.common.TarEntry;
import org.eclipse.mtj.internal.ui.wizards.importer.common.TarException;
import org.eclipse.mtj.internal.ui.wizards.importer.common.TarFile;
import org.eclipse.mtj.internal.ui.wizards.importer.common.TarLeveledStructureProvider;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ZipLeveledStructureProvider;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.IOverwriteQuery;
import org.eclipse.ui.statushandlers.StatusManager;
import org.eclipse.ui.wizards.datatransfer.FileSystemStructureProvider;
import org.eclipse.ui.wizards.datatransfer.ImportOperation;

/**
 * The WizardProjectsImportPage is the page that allows the user to import
 * projects from a particular location.
 */
public class ImportEclipseMEProjectWizardPage extends WizardPage implements
        IOverwriteQuery {

    /**
     * Class declared public only for test suite.
     */
    public class ProjectRecord {
        IProjectDescription description;

        int level;

        Object parent;

        Object projectArchiveFile;

        String projectName;

        File projectSystemFile;

        /**
         * Create a record for a project based on the info in the file.
         * 
         * @param file
         */
        ProjectRecord(File file) {
            projectSystemFile = file;
            setProjectName();
        }

        /**
         * @param file The Object representing the .project file
         * @param parent The parent folder of the .project file
         * @param level The number of levels deep in the provider the file is
         */
        ProjectRecord(Object file, Object parent, int level) {
            this.projectArchiveFile = file;
            this.parent = parent;
            this.level = level;
            setProjectName();
        }

        /**
         * Gets the label to be used when rendering this project record in the
         * UI.
         * 
         * @return String the label
         * @since 3.4
         */
        public String getProjectLabel() {
            if (description == null) {
                return projectName;
            }

            String path = projectSystemFile == null ? structureProvider
                    .getLabel(parent) : projectSystemFile.getParent();

            return NLS
                    .bind(
                            ProjectImporterMessage.WizardProjectsImportPage_projectLabel,
                            projectName, path);
        }

        /**
         * Get the name of the project
         * 
         * @return String
         */
        public String getProjectName() {
            return projectName;
        }

        /**
         * Returns whether the given project description file path is in the
         * default location for a project
         * 
         * @param path The path to examine
         * @return Whether the given path is the default location for a project
         */
        private boolean isDefaultLocation(IPath path) {
            // The project description file must at least be within the project,
            // which is within the workspace location
            if (path.segmentCount() < 2) {
                return false;
            }
            return path.removeLastSegments(2).toFile().equals(
                    Platform.getLocation().toFile());
        }

        /**
         * Set the name of the project based on the projectFile.
         */
        private void setProjectName() {
            try {
                if (projectArchiveFile != null) {
                    InputStream stream = structureProvider
                            .getContents(projectArchiveFile);

                    // If we can get a description pull the name from there
                    if (stream == null) {
                        if (projectArchiveFile instanceof ZipEntry) {
                            IPath path = new Path(
                                    ((ZipEntry) projectArchiveFile).getName());
                            projectName = path.segment(path.segmentCount() - 2);
                        } else if (projectArchiveFile instanceof TarEntry) {
                            IPath path = new Path(
                                    ((TarEntry) projectArchiveFile).getName());
                            projectName = path.segment(path.segmentCount() - 2);
                        }
                    } else {
                        description = MTJCore.getWorkspace()
                                .loadProjectDescription(stream);
                        stream.close();
                        projectName = description.getName();
                    }

                }

                // If we don't have the project name try again
                if (projectName == null) {
                    IPath path = new Path(projectSystemFile.getPath());
                    // if the file is in the default location, use the directory
                    // name as the project name
                    if (isDefaultLocation(path)) {
                        projectName = path.segment(path.segmentCount() - 2);
                        description = MTJCore.getWorkspace()
                                .newProjectDescription(projectName);
                    } else {
                        description = MTJCore.getWorkspace()
                                .loadProjectDescription(path);
                        projectName = description.getName();
                    }

                }
            } catch (CoreException e) {
                // no good couldn't get the name
            } catch (IOException e) {
                // no good couldn't get the name
            }
        }
    }

    private static final String EclipseME_NATURE = "eclipseme.core.nature";
    // constant from WizardArchiveFileResourceImportPage1
    private static final String[] FILE_IMPORT_MASK = {
            "*.jar;*.zip;*.tar;*.tar.gz;*.tgz", "*.*" }; //$NON-NLS-1$ //$NON-NLS-2$
    /**
     * The name of the folder containing metadata information for the workspace.
     */
    private static final String METADATA_FOLDER = ".metadata"; //$NON-NLS-1$
    // dialog store id constants

    // Keep track of the archive that we browsed to last time
    // the wizard was invoked.
    private static String previouslyBrowsedArchive = ""; //$NON-NLS-1$

    // Keep track of the directory that we browsed to last time
    // the wizard was invoked.
    private static String previouslyBrowsedDirectory = ""; //$NON-NLS-1$

    private final static String STORE_ARCHIVE_SELECTED = "EclipseMEProjectImportWizard.STORE_ARCHIVE_SELECTED"; //$NON-NLS-1$

    private Text archivePathField;

    private Button browseArchivesButton;

    private Button browseDirectoriesButton;

    private Text directoryPathField;

    // The last time that the file or folder at the selected path was modified
    // to mimize searches
    private long lastModified;

    // The last selected path to minimize searches
    private String lastPath;

    private Button projectFromArchiveRadio;

    private Button projectFromDirectoryRadio;

    private CheckboxTreeViewer projectsList;

    private ProjectRecord[] selectedProjects = new ProjectRecord[0];
    private boolean showProjectInWorkspaceWorning = false;

    /**
     * The import structure provider.
     * 
     * @since 3.4
     */
    private ILeveledImportStructureProvider structureProvider;

    private IProject[] wsProjects;

    /**
     * Creates a new project creation wizard page.
     */
    public ImportEclipseMEProjectWizardPage() {
        this("wizardExternalProjectsPage"); //$NON-NLS-1$
    }

    /**
     * Create a new instance of the receiver.
     * 
     * @param pageName
     */
    public ImportEclipseMEProjectWizardPage(String pageName) {
        super(pageName);
        setPageComplete(false);
        setTitle(ProjectImporterMessage.WizardProjectsImportPage_ImportProjectsTitle);
        setDescription(ProjectImporterMessage.WizardProjectsImportPage_ImportProjectsDescription);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets
     * .Composite)
     */
    public void createControl(Composite parent) {

        initializeDialogUnits(parent);

        Composite workArea = new Composite(parent, SWT.NONE);
        setControl(workArea);

        workArea.setLayout(new GridLayout());
        workArea.setLayoutData(new GridData(GridData.FILL_BOTH
                | GridData.GRAB_HORIZONTAL | GridData.GRAB_VERTICAL));

        createProjectsRoot(workArea);
        createProjectsList(workArea);
        createOptionsArea(workArea);
        restoreWidgetValues();
        Dialog.applyDialogFont(workArea);

    }

    /**
     * Create the selected projects
     * 
     * @return boolean <code>true</code> if all project creations were
     *         successful.
     */
    public boolean createProjects() {
        saveWidgetValues();
        final Object[] selected = projectsList.getCheckedElements();
        WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
            @Override
            protected void execute(IProgressMonitor monitor)
                    throws InvocationTargetException, InterruptedException {
                try {
                    monitor.beginTask("", selected.length); //$NON-NLS-1$
                    if (monitor.isCanceled()) {
                        throw new OperationCanceledException();
                    }
                    for (Object element : selected) {
                        createExistingProject((ProjectRecord) element,
                                new SubProgressMonitor(monitor, 1));
                    }
                } finally {
                    monitor.done();
                }
            }
        };
        // run the new project creation operation
        try {
            getContainer().run(true, true, op);
        } catch (InterruptedException e) {
            return false;
        } catch (InvocationTargetException e) {
            // one of the steps resulted in a core exception
            Throwable t = e.getTargetException();
            String message = ProjectImporterMessage.WizardExternalProjectImportPage_errorMessage;
            IStatus status;
            if (t instanceof CoreException) {
                status = ((CoreException) t).getStatus();
            } else {
                status = new Status(IStatus.ERROR, IMTJUIConstants.PLUGIN_ID,
                        1, message, t);
            }
            ErrorDialog.openError(getShell(), message, null, status);
            return false;
        }
        ArchiveFileManipulations.closeStructureProvider(structureProvider,
                getShell());
        return true;
    }

    /**
     * Method used for test suite.
     * 
     * @return Button the Import from Directory RadioButton
     */
    public Button getProjectFromDirectoryRadio() {
        return projectFromDirectoryRadio;
    }

    /**
     * Method used for test suite.
     * 
     * @return CheckboxTreeViewer the viewer containing all the projects found
     */
    public CheckboxTreeViewer getProjectsList() {
        return projectsList;
    }

    /**
     * Get the array of valid project records that can be imported from the
     * source workspace or archive, selected by the user. If a project with the
     * same name exists in both the source workspace and the current workspace,
     * it will not appear in the list of projects to import and thus cannot be
     * selected for import. Method declared public for test suite.
     * 
     * @return ProjectRecord[] array of projects that can be imported into the
     *         workspace
     */
    public ProjectRecord[] getValidProjects() {
        List<ProjectRecord> validProjects = new ArrayList<ProjectRecord>();
        for (int i = 0; i < selectedProjects.length; i++) {
            boolean projectNotInWorkspace = !isProjectInWorkspace(selectedProjects[i]
                    .getProjectName());
            boolean isEclipseMEProject = isEclipseMEProject(selectedProjects[i]);
            if (projectNotInWorkspace && isEclipseMEProject) {
                validProjects.add(selectedProjects[i]);
            }
            boolean projectInWorkspace = !projectNotInWorkspace;
            if (projectInWorkspace && isEclipseMEProject) {
                showProjectInWorkspaceWorning = true;
            }
        }
        return validProjects.toArray(new ProjectRecord[validProjects.size()]);
    }

    /**
     * Performs clean-up if the user cancels the wizard without doing anything
     */
    public void performCancel() {
        ArchiveFileManipulations.closeStructureProvider(structureProvider,
                getShell());
    }

    /**
     * The <code>WizardDataTransfer</code> implementation of this
     * <code>IOverwriteQuery</code> method asks the user whether the existing
     * resource at the given path should be overwritten.
     * 
     * @param pathString
     * @return the user's reply: one of <code>"YES"</code>, <code>"NO"</code>,
     *         <code>"ALL"</code>, or <code>"CANCEL"</code>
     */
    public String queryOverwrite(String pathString) {

        Path path = new Path(pathString);

        String messageString;
        // Break the message up if there is a file name and a directory
        // and there are at least 2 segments.
        if ((path.getFileExtension() == null) || (path.segmentCount() < 2)) {
            messageString = NLS.bind(
                    ProjectImporterMessage.WizardDataTransfer_existsQuestion,
                    pathString);
        } else {
            messageString = NLS
                    .bind(
                            ProjectImporterMessage.WizardDataTransfer_overwriteNameAndPathQuestion,
                            path.lastSegment(), path.removeLastSegments(1)
                                    .toOSString());
        }

        final MessageDialog dialog = new MessageDialog(getContainer()
                .getShell(), ProjectImporterMessage.Question, null,
                messageString, MessageDialog.QUESTION, new String[] {
                        IDialogConstants.YES_LABEL,
                        IDialogConstants.YES_TO_ALL_LABEL,
                        IDialogConstants.NO_LABEL,
                        IDialogConstants.NO_TO_ALL_LABEL,
                        IDialogConstants.CANCEL_LABEL }, 0);
        String[] response = new String[] { YES, ALL, NO, NO_ALL, CANCEL };
        // run in syncExec because callback is from an operation,
        // which is probably not running in the UI thread.
        getControl().getDisplay().syncExec(new Runnable() {
            public void run() {
                dialog.open();
            }
        });
        return dialog.getReturnCode() < 0 ? CANCEL : response[dialog
                .getReturnCode()];
    }

    /**
     * Use the dialog store to restore widget values to the values that they
     * held last time this wizard was used to completion. Method declared public
     * only for use of tests.
     */
    public void restoreWidgetValues() {
        IDialogSettings settings = getDialogSettings();
        if (settings != null) {

            // radio selection
            boolean archiveSelected = settings
                    .getBoolean(STORE_ARCHIVE_SELECTED);
            projectFromDirectoryRadio.setSelection(!archiveSelected);
            projectFromArchiveRadio.setSelection(archiveSelected);
            if (archiveSelected) {
                archiveRadioSelected();
            } else {
                directoryRadioSelected();
            }
        }
    }

    /**
     * Since Finish was pressed, write widget values to the dialog store so that
     * they will persist into the next invocation of this wizard page. Method
     * declared public only for use of tests.
     */
    public void saveWidgetValues() {
        IDialogSettings settings = getDialogSettings();
        if (settings != null) {
            settings.put(STORE_ARCHIVE_SELECTED, projectFromArchiveRadio
                    .getSelection());
        }
    }

    /*
     * (non-Javadoc) Method declared on IDialogPage. Set the focus on path
     * fields when page becomes visible.
     */
    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (visible && this.projectFromDirectoryRadio.getSelection()) {
            this.directoryPathField.setFocus();
        }
        if (visible && this.projectFromArchiveRadio.getSelection()) {
            this.archivePathField.setFocus();
        }
    }

    /**
     * Update the list of projects based on path. Method declared public only
     * for test suite.
     * 
     * @param path
     */
    public void updateProjectsList(final String path) {
        // on an empty path empty selectedProjects
        if ((path == null) || (path.length() == 0)) {
            setMessage(ProjectImporterMessage.WizardProjectsImportPage_ImportProjectsDescription);
            selectedProjects = new ProjectRecord[0];
            projectsList.refresh(true);
            projectsList.setCheckedElements(selectedProjects);
            setPageComplete(projectsList.getCheckedElements().length > 0);
            lastPath = path;
            return;
        }

        final File directory = new File(path);
        long modified = directory.lastModified();
        if (path.equals(lastPath) && (lastModified == modified)) {
            // since the file/folder was not modified and the path did not
            // change, no refreshing is required
            return;
        }

        lastPath = path;
        lastModified = modified;

        // We can't access the radio button from the inner class so get the
        // status beforehand
        final boolean dirSelected = this.projectFromDirectoryRadio
                .getSelection();
        try {
            getContainer().run(true, true, new IRunnableWithProgress() {

                /*
                 * (non-Javadoc)
                 * 
                 * @see
                 * org.eclipse.jface.operation.IRunnableWithProgress#run(org
                 * .eclipse.core.runtime.IProgressMonitor)
                 */
                public void run(IProgressMonitor monitor) {

                    monitor
                            .beginTask(
                                    ProjectImporterMessage.WizardProjectsImportPage_SearchingMessage,
                                    100);
                    selectedProjects = new ProjectRecord[0];
                    Collection<ProjectRecord> projectRecords = new ArrayList<ProjectRecord>();
                    monitor.worked(10);
                    if (!dirSelected
                            && ArchiveFileManipulations.isTarFile(path)) {
                        TarFile sourceTarFile = getSpecifiedTarSourceFile(path);
                        if (sourceTarFile == null) {
                            return;
                        }

                        structureProvider = new TarLeveledStructureProvider(
                                sourceTarFile);
                        Object child = structureProvider.getRoot();

                        if (!collectProjectFilesFromProvider(projectRecords,
                                child, 0, monitor)) {
                            return;
                        }
                        Iterator<ProjectRecord> filesIterator = projectRecords
                                .iterator();
                        selectedProjects = new ProjectRecord[projectRecords
                                .size()];
                        int index = 0;
                        monitor.worked(50);
                        monitor
                                .subTask(ProjectImporterMessage.WizardProjectsImportPage_ProcessingMessage);
                        while (filesIterator.hasNext()) {
                            selectedProjects[index++] = filesIterator.next();
                        }
                    } else if (!dirSelected
                            && ArchiveFileManipulations.isZipFile(path)) {
                        ZipFile sourceFile = getSpecifiedZipSourceFile(path);
                        if (sourceFile == null) {
                            return;
                        }
                        structureProvider = new ZipLeveledStructureProvider(
                                sourceFile);
                        Object child = structureProvider.getRoot();

                        if (!collectProjectFilesFromProvider(projectRecords,
                                child, 0, monitor)) {
                            return;
                        }
                        Iterator<ProjectRecord> filesIterator = projectRecords
                                .iterator();
                        selectedProjects = new ProjectRecord[projectRecords
                                .size()];
                        int index = 0;
                        monitor.worked(50);
                        monitor
                                .subTask(ProjectImporterMessage.WizardProjectsImportPage_ProcessingMessage);
                        while (filesIterator.hasNext()) {
                            selectedProjects[index++] = filesIterator.next();
                        }
                    }

                    else if (dirSelected && directory.isDirectory()) {
                        List<File> files = new ArrayList<File>();
                        if (!collectProjectFilesFromDirectory(files, directory,
                                null, monitor)) {
                            return;
                        }
                        Iterator<File> filesIterator = files.iterator();
                        selectedProjects = new ProjectRecord[files.size()];
                        int index = 0;
                        monitor.worked(50);
                        monitor
                                .subTask(ProjectImporterMessage.WizardProjectsImportPage_ProcessingMessage);
                        while (filesIterator.hasNext()) {
                            File file = filesIterator.next();
                            selectedProjects[index] = new ProjectRecord(file);
                            index++;
                        }
                    } else {
                        monitor.worked(60);
                    }
                    monitor.done();
                }

            });
        } catch (InvocationTargetException e) {
            MTJLogger.log(IStatus.ERROR, e.getMessage(), e);
        } catch (InterruptedException e) {
            // Nothing to do if the user interrupts.
        }

        projectsList.refresh(true);
        projectsList.setCheckedElements(getValidProjects());
        if (showProjectInWorkspaceWorning) {
            setMessage(
                    ProjectImporterMessage.WizardProjectsImportPage_projectsInWorkspace,
                    WARNING);
        } else {
            setMessage(ProjectImporterMessage.WizardProjectsImportPage_ImportProjectsDescription);
        }
        setPageComplete(projectsList.getCheckedElements().length > 0);
    }

    private void archiveRadioSelected() {
        if (projectFromArchiveRadio.getSelection()) {
            directoryPathField.setEnabled(false);
            browseDirectoriesButton.setEnabled(false);
            archivePathField.setEnabled(true);
            browseArchivesButton.setEnabled(true);
            updateProjectsList(archivePathField.getText());
            archivePathField.setFocus();
        }
    }

    /**
     * Collect the list of .project files that are under directory into files.
     * 
     * @param files
     * @param directory
     * @param directoriesVisited Set of canonical paths of directories, used as
     *            recursion guard
     * @param monitor The monitor to report to
     * @return boolean <code>true</code> if the operation was completed.
     */
    private boolean collectProjectFilesFromDirectory(Collection<File> files,
            File directory, Set<String> directoriesVisited,
            IProgressMonitor monitor) {

        if (monitor.isCanceled()) {
            return false;
        }
        monitor
                .subTask(NLS
                        .bind(
                                ProjectImporterMessage.WizardProjectsImportPage_CheckingMessage,
                                directory.getPath()));
        File[] contents = directory.listFiles();
        if (contents == null) {
            return false;
        }

        // Initialize recursion guard for recursive symbolic links
        if (directoriesVisited == null) {
            directoriesVisited = new HashSet<String>();
            try {
                directoriesVisited.add(directory.getCanonicalPath());
            } catch (IOException exception) {
                StatusManager.getManager().handle(
                        StatusUtil.newStatus(IStatus.ERROR, exception
                                .getLocalizedMessage(), exception));
            }
        }

        // first look for project description files
        final String dotProject = IProjectDescription.DESCRIPTION_FILE_NAME;
        for (File file : contents) {
            if (file.isFile() && file.getName().equals(dotProject)) {
                files.add(file);
                // don't search sub-directories since we can't have nested
                // projects
                return true;
            }
        }
        // no project description found, so recurse into sub-directories
        for (int i = 0; i < contents.length; i++) {
            if (contents[i].isDirectory()) {
                if (!contents[i].getName().equals(METADATA_FOLDER)) {
                    try {
                        String canonicalPath = contents[i].getCanonicalPath();
                        if (!directoriesVisited.add(canonicalPath)) {
                            // already been here --> do not recurse
                            continue;
                        }
                    } catch (IOException exception) {
                        StatusManager.getManager().handle(
                                StatusUtil.newStatus(IStatus.ERROR, exception
                                        .getLocalizedMessage(), exception));

                    }
                    collectProjectFilesFromDirectory(files, contents[i],
                            directoriesVisited, monitor);
                }
            }
        }
        return true;
    }

    /**
     * Collect the list of .project files that are under directory into files.
     * 
     * @param files
     * @param monitor The monitor to report to
     * @return boolean <code>true</code> if the operation was completed.
     */
    @SuppressWarnings("unchecked")
    private boolean collectProjectFilesFromProvider(
            Collection<ProjectRecord> files, Object entry, int level,
            IProgressMonitor monitor) {

        if (monitor.isCanceled()) {
            return false;
        }
        monitor
                .subTask(NLS
                        .bind(
                                ProjectImporterMessage.WizardProjectsImportPage_CheckingMessage,
                                structureProvider.getLabel(entry)));
        List<File> children = structureProvider.getChildren(entry);
        if (children == null) {
            children = new ArrayList<File>(1);
        }
        Iterator<File> childrenEnum = children.iterator();
        while (childrenEnum.hasNext()) {
            Object child = childrenEnum.next();
            if (structureProvider.isFolder(child)) {
                collectProjectFilesFromProvider(files, child, level + 1,
                        monitor);
            }
            String elementLabel = structureProvider.getLabel(child);
            if (elementLabel.equals(IProjectDescription.DESCRIPTION_FILE_NAME)) {
                files.add(new ProjectRecord(child, entry, level));
            }
        }
        return true;
    }

    /**
     * Create the project described in record. If it is successful return true.
     * 
     * @param record
     * @return boolean <code>true</code> if successful
     * @throws InterruptedException
     */
    @SuppressWarnings("unchecked")
    private boolean createExistingProject(final ProjectRecord record,
            IProgressMonitor monitor) throws InvocationTargetException,
            InterruptedException {
        String projectName = record.getProjectName();
        final IWorkspace workspace = MTJCore.getWorkspace();
        final IProject project = workspace.getRoot().getProject(projectName);
        if (!isEclipseMEProject(record)) {
            displayErrorMessageDialog(
                    ProjectImporterMessage.WizardProjectsImportPage_NotEclipseMEProject_Title,
                    NLS
                            .bind(
                                    ProjectImporterMessage.WizardProjectsImportPage_NotEclipseMEProject_Message,
                                    projectName));
            return false;
        }
        if (record.description == null) {
            // error case
            record.description = workspace.newProjectDescription(projectName);
            IPath locationPath = new Path(record.projectSystemFile
                    .getAbsolutePath());

            // If it is under the root use the default location
            if (Platform.getLocation().isPrefixOf(locationPath)) {
                record.description.setLocation(null);
            } else {
                record.description.setLocation(locationPath);
            }
        } else {
            record.description.setName(projectName);
        }
        if (record.projectArchiveFile != null) {
            // import from archive
            List<File> fileSystemObjects = structureProvider
                    .getChildren(record.parent);
            structureProvider.setStrip(record.level);
            ImportOperation operation = new ImportOperation(project
                    .getFullPath(), structureProvider.getRoot(),
                    structureProvider, this, fileSystemObjects);
            operation.setContext(getShell());
            operation.run(monitor);
            try {
                // migrate EclipseME project to MTJ project
                MTJProjectConverter.getInstance().convertEclipseMeProject(
                        project, new SubProgressMonitor(monitor, 50));

                return true;
            } catch (ProjectConvertionException e) {
                throw new InvocationTargetException(e);
            }
        }
        // import from file system
        File importSource = null;
        // import project from location copying files - use default project
        // location for this workspace
        URI locationURI = record.description.getLocationURI();
        // if location is null, project already exists in this location or
        // some error condition occurred.
        if (locationURI != null) {
            importSource = new File(locationURI);
            IProjectDescription desc = workspace
                    .newProjectDescription(projectName);
            desc.setBuildSpec(record.description.getBuildSpec());
            desc.setComment(record.description.getComment());
            desc
                    .setDynamicReferences(record.description
                            .getDynamicReferences());
            desc.setNatureIds(record.description.getNatureIds());
            desc.setReferencedProjects(record.description
                    .getReferencedProjects());
            record.description = desc;
        }

        try {
            monitor
                    .beginTask(
                            ProjectImporterMessage.WizardProjectsImportPage_CreateProjectsTask,
                            100);
            project.create(record.description, new SubProgressMonitor(monitor,
                    10));
            project.open(IResource.BACKGROUND_REFRESH, new SubProgressMonitor(
                    monitor, 20));
        } catch (CoreException e) {
            throw new InvocationTargetException(e);
        }

        // import operation to import project files
        if (importSource != null) {
            List<File> filesToImport = FileSystemStructureProvider.INSTANCE
                    .getChildren(importSource);
            ImportOperation operation = new ImportOperation(project
                    .getFullPath(), importSource,
                    FileSystemStructureProvider.INSTANCE, this, filesToImport);
            operation.setContext(getShell());
            operation.setOverwriteResources(true); // need to overwrite
            // .project, .classpath
            // files
            operation.setCreateContainerStructure(false);
            operation.run(new SubProgressMonitor(monitor, 20));

            try {
                // migrate EclipseME project to MTJ project
                MTJProjectConverter.getInstance().convertEclipseMeProject(
                        project, new SubProgressMonitor(monitor, 50));
            } catch (ProjectConvertionException e) {
                throw new InvocationTargetException(e);
            }
        }

        return true;
    }

    /**
     * Create the area with the extra options.
     * 
     * @param workArea
     */
    private void createOptionsArea(Composite workArea) {
        Composite optionsGroup = new Composite(workArea, SWT.NONE);
        optionsGroup.setLayout(new GridLayout());
        optionsGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
    }

    /**
     * Create the checkbox list for the found projects.
     * 
     * @param workArea
     */
    private void createProjectsList(Composite workArea) {

        Label title = new Label(workArea, SWT.NONE);
        title
                .setText(ProjectImporterMessage.WizardProjectsImportPage_ProjectsListTitle);

        Composite listComposite = new Composite(workArea, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 2;
        layout.marginWidth = 0;
        layout.makeColumnsEqualWidth = false;
        listComposite.setLayout(layout);

        listComposite.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
                | GridData.GRAB_VERTICAL | GridData.FILL_BOTH));

        projectsList = new CheckboxTreeViewer(listComposite, SWT.BORDER);
        GridData listData = new GridData(GridData.GRAB_HORIZONTAL
                | GridData.GRAB_VERTICAL | GridData.FILL_BOTH);
        projectsList.getControl().setLayoutData(listData);

        projectsList.setContentProvider(new ITreeContentProvider() {

            /*
             * (non-Javadoc)
             * 
             * @see org.eclipse.jface.viewers.IContentProvider#dispose()
             */
            public void dispose() {

            }

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java
             * .lang.Object)
             */
            public Object[] getChildren(Object parentElement) {
                return null;
            }

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.IStructuredContentProvider#getElements
             * (java.lang.Object)
             */
            public Object[] getElements(Object inputElement) {
                return getValidProjects();
            }

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.ITreeContentProvider#getParent(java
             * .lang.Object)
             */
            public Object getParent(Object element) {
                return null;
            }

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java
             * .lang.Object)
             */
            public boolean hasChildren(Object element) {
                return false;
            }

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse
             * .jface.viewers.Viewer, java.lang.Object, java.lang.Object)
             */
            public void inputChanged(Viewer viewer, Object oldInput,
                    Object newInput) {
            }

        });

        projectsList.setLabelProvider(new LabelProvider() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
             */
            @Override
            public String getText(Object element) {
                return ((ProjectRecord) element).getProjectLabel();
            }
        });

        projectsList.addCheckStateListener(new ICheckStateListener() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.jface.viewers.ICheckStateListener#checkStateChanged
             * (org.eclipse.jface.viewers.CheckStateChangedEvent)
             */
            public void checkStateChanged(CheckStateChangedEvent event) {
                setPageComplete(projectsList.getCheckedElements().length > 0);
            }
        });

        projectsList.setInput(this);
        projectsList.setComparator(new ViewerComparator());
        createSelectionButtons(listComposite);
    }

    /**
     * Create the area where you select the root directory for the projects.
     * 
     * @param workArea Composite
     */
    private void createProjectsRoot(Composite workArea) {

        // project specification group
        Composite projectGroup = new Composite(workArea, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 3;
        layout.makeColumnsEqualWidth = false;
        layout.marginWidth = 0;
        projectGroup.setLayout(layout);
        projectGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        // new project from directory radio button
        projectFromDirectoryRadio = new Button(projectGroup, SWT.RADIO);
        projectFromDirectoryRadio
                .setText(ProjectImporterMessage.WizardProjectsImportPage_RootSelectTitle);

        // project location entry field
        this.directoryPathField = new Text(projectGroup, SWT.BORDER);

        this.directoryPathField.setLayoutData(new GridData(
                GridData.FILL_HORIZONTAL | GridData.GRAB_HORIZONTAL));

        // browse button
        browseDirectoriesButton = new Button(projectGroup, SWT.PUSH);
        browseDirectoriesButton
                .setText(ProjectImporterMessage.DataTransfer_browse);
        setButtonLayoutData(browseDirectoriesButton);

        // new project from archive radio button
        projectFromArchiveRadio = new Button(projectGroup, SWT.RADIO);
        projectFromArchiveRadio
                .setText(ProjectImporterMessage.WizardProjectsImportPage_ArchiveSelectTitle);

        // project location entry field
        archivePathField = new Text(projectGroup, SWT.BORDER);

        archivePathField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL
                | GridData.GRAB_HORIZONTAL));
        // browse button
        browseArchivesButton = new Button(projectGroup, SWT.PUSH);
        browseArchivesButton
                .setText(ProjectImporterMessage.DataTransfer_browse);
        setButtonLayoutData(browseArchivesButton);

        projectFromDirectoryRadio.setSelection(true);
        archivePathField.setEnabled(false);
        browseArchivesButton.setEnabled(false);

        browseDirectoriesButton.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see org.eclipse.swt.events.SelectionAdapter#widgetS
             * elected(org.eclipse.swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleLocationDirectoryButtonPressed();
            }

        });

        browseArchivesButton.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleLocationArchiveButtonPressed();
            }

        });

        directoryPathField.addTraverseListener(new TraverseListener() {

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.TraverseListener#keyTraversed(org.eclipse
             * .swt.events.TraverseEvent)
             */
            public void keyTraversed(TraverseEvent e) {
                if (e.detail == SWT.TRAVERSE_RETURN) {
                    e.doit = false;
                    updateProjectsList(directoryPathField.getText().trim());
                }
            }

        });

        directoryPathField.addFocusListener(new FocusAdapter() {

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.FocusListener#focusLost(org.eclipse.swt
             * .events.FocusEvent)
             */
            @Override
            public void focusLost(org.eclipse.swt.events.FocusEvent e) {
                updateProjectsList(directoryPathField.getText().trim());
            }

        });

        archivePathField.addTraverseListener(new TraverseListener() {

            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.TraverseListener#keyTraversed(org.eclipse
             * .swt.events.TraverseEvent)
             */
            public void keyTraversed(TraverseEvent e) {
                if (e.detail == SWT.TRAVERSE_RETURN) {
                    e.doit = false;
                    updateProjectsList(archivePathField.getText().trim());
                }
            }

        });

        archivePathField.addFocusListener(new FocusAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.FocusListener#focusLost(org.eclipse.swt
             * .events.FocusEvent)
             */
            @Override
            public void focusLost(org.eclipse.swt.events.FocusEvent e) {
                updateProjectsList(archivePathField.getText().trim());
            }
        });

        projectFromDirectoryRadio.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                directoryRadioSelected();
            }
        });

        projectFromArchiveRadio.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                archiveRadioSelected();
            }
        });
    }

    /**
     * Create the selection buttons in the listComposite.
     * 
     * @param listComposite
     */
    private void createSelectionButtons(Composite listComposite) {
        Composite buttonsComposite = new Composite(listComposite, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.marginWidth = 0;
        layout.marginHeight = 0;
        buttonsComposite.setLayout(layout);

        buttonsComposite.setLayoutData(new GridData(
                GridData.VERTICAL_ALIGN_BEGINNING));

        Button selectAll = new Button(buttonsComposite, SWT.PUSH);
        selectAll.setText(ProjectImporterMessage.DataTransfer_selectAll);
        selectAll.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                projectsList.setCheckedElements(selectedProjects);
                setPageComplete(projectsList.getCheckedElements().length > 0);
            }
        });
        Dialog.applyDialogFont(selectAll);
        setButtonLayoutData(selectAll);

        Button deselectAll = new Button(buttonsComposite, SWT.PUSH);
        deselectAll.setText(ProjectImporterMessage.DataTransfer_deselectAll);
        deselectAll.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {

                projectsList.setCheckedElements(new Object[0]);
                setPageComplete(false);
            }
        });
        Dialog.applyDialogFont(deselectAll);
        setButtonLayoutData(deselectAll);

        Button refresh = new Button(buttonsComposite, SWT.PUSH);
        refresh.setText(ProjectImporterMessage.DataTransfer_refresh);
        refresh.addSelectionListener(new SelectionAdapter() {
            /*
             * (non-Javadoc)
             * 
             * @see
             * org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                if (projectFromDirectoryRadio.getSelection()) {
                    updateProjectsList(directoryPathField.getText().trim());
                } else {
                    updateProjectsList(archivePathField.getText().trim());
                }
            }
        });
        Dialog.applyDialogFont(refresh);
        setButtonLayoutData(refresh);
    }

    private void directoryRadioSelected() {
        if (projectFromDirectoryRadio.getSelection()) {
            directoryPathField.setEnabled(true);
            browseDirectoriesButton.setEnabled(true);
            archivePathField.setEnabled(false);
            browseArchivesButton.setEnabled(false);
            updateProjectsList(directoryPathField.getText());
            directoryPathField.setFocus();
        }
    }

    /**
     * desplay a erroe message dialog
     * 
     * @param title
     * @param message
     */
    private void displayErrorMessageDialog(final String title,
            final String message) {
        Runnable runnable = new Runnable() {

            public void run() {
                MessageDialog.openError(getShell(), title, message);
            }

        };
        getShell().getDisplay().syncExec(runnable);
    }

    /**
     * Retrieve all the projects in the current workspace.
     * 
     * @return IProject[] array of IProject in the current workspace
     */
    private IProject[] getProjectsInWorkspace() {
        if (wsProjects == null) {
            wsProjects = MTJCore.getWorkspace().getRoot().getProjects();
        }
        return wsProjects;
    }

    /**
     * Answer a handle to the zip file currently specified as being the source.
     * Return null if this file does not exist or is not of valid format.
     */
    private TarFile getSpecifiedTarSourceFile(String fileName) {
        if (fileName.length() == 0) {
            return null;
        }

        try {
            return new TarFile(fileName);
        } catch (TarException e) {
            displayErrorMessageDialog(
                    ProjectImporterMessage.WizardImportPage_internalErrorTitle,
                    ProjectImporterMessage.TarImport_badFormat);
        } catch (IOException e) {
            displayErrorMessageDialog(
                    ProjectImporterMessage.WizardImportPage_internalErrorTitle,
                    ProjectImporterMessage.ZipImport_couldNotRead);
        }

        archivePathField.setFocus();
        return null;
    }

    /**
     * Answer a handle to the zip file currently specified as being the source.
     * Return null if this file does not exist or is not of valid format.
     */
    private ZipFile getSpecifiedZipSourceFile(String fileName) {
        if (fileName.length() == 0) {
            return null;
        }

        try {
            return new ZipFile(fileName);
        } catch (ZipException e) {
            displayErrorMessageDialog(
                    ProjectImporterMessage.WizardImportPage_internalErrorTitle,
                    ProjectImporterMessage.ZipImport_badFormat);
        } catch (IOException e) {
            displayErrorMessageDialog(
                    ProjectImporterMessage.WizardImportPage_internalErrorTitle,
                    ProjectImporterMessage.ZipImport_couldNotRead);
        }

        archivePathField.setFocus();
        return null;
    }

    private boolean isEclipseMEProject(ProjectRecord record) {
        if (record.description.hasNature((EclipseME_NATURE))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determine if the project with the given name is in the current workspace.
     * 
     * @param projectName String the project name to check
     * @return boolean true if the project with the given name is in this
     *         workspace
     */
    private boolean isProjectInWorkspace(String projectName) {
        if (projectName == null) {
            return false;
        }
        IProject[] workspaceProjects = getProjectsInWorkspace();
        for (IProject element : workspaceProjects) {
            if (projectName.equals(element.getName())) {
                return true;
            }
        }
        return false;
    }

    protected void handleLocationArchiveButtonPressed() {

        FileDialog dialog = new FileDialog(archivePathField.getShell());
        dialog.setFilterExtensions(FILE_IMPORT_MASK);
        dialog
                .setText(ProjectImporterMessage.WizardProjectsImportPage_SelectArchiveDialogTitle);

        String fileName = archivePathField.getText().trim();
        if (fileName.length() == 0) {
            fileName = previouslyBrowsedArchive;
        }

        if (fileName.length() == 0) {
            dialog.setFilterPath(MTJCore.getWorkspace().getRoot()
                    .getLocation().toOSString());
        } else {
            File path = new File(fileName).getParentFile();
            if ((path != null) && path.exists()) {
                dialog.setFilterPath(path.toString());
            }
        }

        String selectedArchive = dialog.open();
        if (selectedArchive != null) {
            previouslyBrowsedArchive = selectedArchive;
            archivePathField.setText(previouslyBrowsedArchive);
            updateProjectsList(selectedArchive);
        }

    }

    /**
     * The browse button has been selected. Select the location.
     */
    protected void handleLocationDirectoryButtonPressed() {

        DirectoryDialog dialog = new DirectoryDialog(directoryPathField
                .getShell());
        dialog
                .setMessage(ProjectImporterMessage.WizardProjectsImportPage_SelectDialogTitle);

        String dirName = directoryPathField.getText().trim();
        if (dirName.length() == 0) {
            dirName = previouslyBrowsedDirectory;
        }

        if (dirName.length() == 0) {
            dialog.setFilterPath(MTJCore.getWorkspace().getRoot()
                    .getLocation().toOSString());
        } else {
            File path = new File(dirName);
            if (path.exists()) {
                dialog.setFilterPath(new Path(dirName).toOSString());
            }
        }

        String selectedDirectory = dialog.open();
        if (selectedDirectory != null) {
            previouslyBrowsedDirectory = selectedDirectory;
            directoryPathField.setText(previouslyBrowsedDirectory);
            updateProjectsList(selectedDirectory);
        }

    }
}
