/**
 * Copyright (c) 2008, 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Renato Franca (Motorola) - Adding marker support
 *     Renato Franca (Motorola) - Fixing bug 257367. Save improvement
 */
package org.eclipse.mtj.internal.ui.editors.l10n;

import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.internal.core.IBaseModel;
import org.eclipse.mtj.internal.core.text.AbstractEditingModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nMarkerError;
import org.eclipse.mtj.internal.core.text.l10n.L10nMarkerManager;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.editor.MTJFormEditor;
import org.eclipse.mtj.internal.ui.editor.SystemFileEditorInput;
import org.eclipse.mtj.internal.ui.editor.context.XMLInputContext;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IStorageEditorInput;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class L10nInputContext extends XMLInputContext {

    public static final String CONTEXT_ID = "l10n-context"; //$NON-NLS-1$ 
    private L10nMarkerManager markerManager;

    /**
     * @param editor
     * @param input
     * @param primary
     */
    public L10nInputContext(MTJFormEditor editor, IEditorInput input,
            boolean primary) {
        super(editor, input, primary);
        markerManager = new L10nMarkerManager();
        create();
        updateMarkersFromModel((IFileEditorInput) super.getInput(),
                ((L10nModel) getModel()));
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.InputContext#doRevert()
     */
    public void doRevert() {
        fEditOperations.clear();
        fOperationTable.clear();
        fMoveOperations.clear();
        AbstractEditingModel model = (AbstractEditingModel) getModel();
        model.reconciled(model.getDocument());
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.context.InputContext#doSave(org.eclipse
     * .core.runtime.IProgressMonitor)
     */
    public void doSave(IProgressMonitor monitor) {

        super.doSave(monitor);

        L10nModel fModel = (L10nModel) getModel();

        // to be sure that the errors are updated.
        fModel.getLocales().validate();

        if (validateModel(fModel)) {
            return;
        }

        validateDefaultLocale(fModel);
    }

    private void validateDefaultLocale(L10nModel fModel) {
        int line = -1;
        String warningMessage = MTJUIMessages.L10nLocaleDetails_noDefaultLocale;
        if (fModel.getLocales().getDefaultLocale() == null) {

            line = fModel.getLocales().getLocalesLine() + 1;
            addWarningMarker((IFileEditorInput) super.getInput(), line,
                    warningMessage);
        } else {
            cleanMarkers((IFileEditorInput) super.getInput(),
                    IMarker.SEVERITY_WARNING);
        }

    }

    public boolean validateModel(L10nModel model) {

        Vector<L10nMarkerError> markerErrors = model.getLocales()
                .getMarkerErrors();

        cleanMarkers((IFileEditorInput) super.getInput(),
                IMarker.SEVERITY_ERROR);
        if (markerErrors.size() > 0) {

            for (int i = 0; i < markerErrors.size(); i++) {
                updateMarkersFromLines((IFileEditorInput) super.getInput(),
                        markerErrors.get(i).getLine(), markerErrors.get(i)
                                .getErrorMessage());
            }
            return true;

        } else {
            return false;
        }

    }


    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.context.InputContext#createModel(org
     * .eclipse.ui.IEditorInput)
     */
    @Override
    protected IBaseModel createModel(IEditorInput input) throws CoreException {

        if (input instanceof IStorageEditorInput) {
            boolean isReconciling = input instanceof IFileEditorInput;
            IDocument document = getDocumentProvider().getDocument(input);

            L10nModel model = new L10nModel(document, isReconciling);

            if (input instanceof IFileEditorInput) {
                IFile file = ((IFileEditorInput) input).getFile();
                model.setUnderlyingResource(file);
                model.setCharset(file.getCharset());
            } else if (input instanceof SystemFileEditorInput) {
                File file = (File) ((SystemFileEditorInput) input)
                        .getAdapter(File.class);
                model.setInstallLocation(file.getParent());
                model.setCharset(getDefaultCharset());
            } else {
                model.setCharset(getDefaultCharset());
            }

            model.load();

            return model;
        }

        return null;

    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.context.InputContext#getDefaultCharset
     * ()
     */
    @Override
    protected String getDefaultCharset() {
        return "ISO-8859-1"; //$NON-NLS-1$;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.InputContext#getId()
     */
    @Override
    public String getId() {
        return CONTEXT_ID;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.context.InputContext#getPartitionName
     * ()
     */
    @Override
    protected String getPartitionName() {
        return "___l10n_partition"; //$NON-NLS-1$
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.internal.ui.editor.context.XMLInputContext#reorderInsertEdits
     * (java.util.ArrayList)
     */
    @Override
    protected void reorderInsertEdits(ArrayList<TextEdit> ops) {

    }

    private void updateMarkersFromModel(IFileEditorInput editor, L10nModel model) {

        try {
            markerManager.createMarker(editor.getFile(), model);
        } catch (CoreException e) {
            // NOT-TO-DO
        }
    }


    private void updateMarkersFromLines(IFileEditorInput editor, int line,
            String errorMessage) {

        try {
            markerManager.createMarker(editor.getFile(), line, errorMessage,
                    false);
        } catch (CoreException e) {
            // NOT-TO-DO
        }
    }

    private void cleanMarkers(IFileEditorInput editor, int severity) {

        try {
            markerManager.clearMarkers(editor.getFile(), severity);
        } catch (CoreException e) {
            // NOT-TO-DO
        }
    }

    private void addWarningMarker(IFileEditorInput editor, int line,
            String warningMessage) {

        try {
            markerManager.addWarningMarker(editor.getFile(), line,
                    warningMessage);
        } catch (CoreException e) {
            // NOT-TO-DO
        }
    }

}
