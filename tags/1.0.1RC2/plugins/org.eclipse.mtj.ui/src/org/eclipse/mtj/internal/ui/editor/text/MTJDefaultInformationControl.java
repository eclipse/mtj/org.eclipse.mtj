/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editor.text;

import org.eclipse.jface.text.DefaultInformationControl;
import org.eclipse.swt.widgets.Shell;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class MTJDefaultInformationControl extends DefaultInformationControl {

    /**
     * 
     */
    private boolean disposed = false;

    /**
     * Creates a new MTJDefaultInformationControl.
     * 
     * @param parent
     * @param tooltipAffordanceString
     */
    public MTJDefaultInformationControl(Shell parent,
            String tooltipAffordanceString) {
        super(parent);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.DefaultInformationControl#dispose()
     */
    public void dispose() {
        disposed = true;
        super.dispose();
    }

    /**
     * @return
     */
    public boolean isDisposed() {
        return disposed;
    }

}
