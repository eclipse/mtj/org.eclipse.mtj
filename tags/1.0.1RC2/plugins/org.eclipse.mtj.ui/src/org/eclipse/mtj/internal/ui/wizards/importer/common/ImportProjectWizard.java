/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *	   Hugo Raniere - Moved from
 *			org.eclipse.mtj.internal.ui.wizards.importer.eclipse and modified to
 *			be used by other project importers
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.wizards.importer.common;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;

/**
 * Base for import project wizards. New import project wizards in MTJ should extend this class
 * 
 * @author Hugo Raniere
 *
 */
public abstract class ImportProjectWizard extends Wizard implements IImportWizard {

	private static final String PROJECT_IMPORT_SECTION_NAME = "ProjectImportWizard"; //$NON-NLS-1$
	
	private ImportProjectWizardPage mainPage;
	
	public ImportProjectWizard() {
		super();
		setNeedsProgressMonitor(true);
		IDialogSettings workbenchSettings = MTJUIPlugin.getDefault()
				.getDialogSettings();

		String projectImportSectionName = getProductNameToImportFrom() + PROJECT_IMPORT_SECTION_NAME;
		IDialogSettings wizardSettings = workbenchSettings
				.getSection(projectImportSectionName);
		if (wizardSettings == null) {
			wizardSettings = workbenchSettings
					.addNewSection(projectImportSectionName);
		}
		setDialogSettings(wizardSettings);
	}

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle(NLS.bind(ProjectImporterMessage.WizardProjectsImportPage_ImportProjectsTitle, getProductNameToImportFrom()));
	}

	/**
	 * Returns the product name that the project will be imported from
	 * @return
	 */
	protected abstract String getProductNameToImportFrom();

	@Override
	public void addPages() {
		super.addPages();
		mainPage = createImportProjectWizardPage();
		addPage(mainPage);
	}
	
	/**
	 * Creates the main page for this wizard
	 * @return a subclass from ImportProjectWizardPage
	 */
	protected abstract ImportProjectWizardPage createImportProjectWizardPage();
	
	@Override
	public boolean performCancel() {
		mainPage.performCancel();
		return true;
	}

	@Override
	public boolean performFinish() {
		return mainPage.createProjects();
	}

}
