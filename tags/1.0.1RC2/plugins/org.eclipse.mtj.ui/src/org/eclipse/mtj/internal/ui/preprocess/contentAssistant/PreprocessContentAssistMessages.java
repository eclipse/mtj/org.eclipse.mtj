/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma (Sybase)        - Initial implementation
 *     Diego Sandin (Motorola) - Adopt ICU4J into MTJ
 */
package org.eclipse.mtj.internal.ui.preprocess.contentAssistant;

import java.lang.reflect.Field;

import org.eclipse.osgi.util.NLS;

/**
 * @author gma
 * @since 0.9.1
 */
public class PreprocessContentAssistMessages extends NLS {
    private static final String BUNDLE_NAME = PreprocessContentAssistMessages.class
            .getName();

    public static String PreprocessProposal_displayString;

    public static String condition_desc;
    public static String debug_desc;
    public static String define_desc;
    public static String elif_desc;
    public static String elifdef_desc;
    public static String elifndef_desc;
    public static String else_desc;
    public static String enddebug_desc;
    public static String endif_desc;
    public static String if_desc;
    public static String ifdef_desc;
    public static String ifndef_desc;
    public static String mdebug_desc;
    public static String undefine_desc;
    public static String expand_desc;
    public static String condition_javadoc;

    public static String debug_javadoc;
    public static String define_javadoc;
    public static String elif_javadoc;
    public static String elifdef_javadoc;
    public static String elifndef_javadoc;
    public static String else_javadoc;
    public static String enddebug_javadoc;
    public static String endif_javadoc;
    public static String if_javadoc;
    public static String ifdef_javadoc;
    public static String ifndef_javadoc;
    public static String mdebug_javadoc;
    public static String undefine_javadoc;
    public static String expand_javadoc;

    public static String dl_debug_desc;
    public static String dl_error_desc;
    public static String dl_fatal_desc;
    public static String dl_info_desc;
    public static String dl_warn_desc;

    public static String symbol_active;
    public static String symbol_inactive;
    public static String symbol_configuration;

    static {
        NLS.initializeMessages(BUNDLE_NAME,
                PreprocessContentAssistMessages.class);
    }

    public static String getFormattedString(String key, Object[] args) {
        return NLS.bind(key, args);
    }

    public static String getString(String key) {
        String value = null;
        try {
            Field field = PreprocessContentAssistMessages.class
                    .getDeclaredField(key);
            value = field.get(null).toString();
        } catch (Exception e1) {

        }
        return value;
    }

    private PreprocessContentAssistMessages() {
        // Do not instantiate
    }
}
