/**
 * Copyright (c) 2006,2008 Nokia and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nokia                    - Initial version
 *     Diego Madruga (Motorola) - Refactored some parts of code to follow MTJ 
 *                                standards
 */
package org.eclipse.mtj.internal.jmunit.ui.actions;

/**
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class has been added as part of a work in
 * progress. There is no guarantee that this API will work or that it will
 * remain the same. Please do not use this API without consulting with the MTJ
 * team.
 * </p>
 * 
 * @author Gorkem Ercan
 */
public class TestSuiteClassListRange {

    private final int fStart;
    private final int fEnd;
    private final boolean fInitialUpdate;

    public TestSuiteClassListRange(int start, int end, boolean initialUpdate) {
        fStart = start;
        fEnd = end;
        fInitialUpdate = initialUpdate;
    }

    public int getEnd() {
        return fEnd;
    }

    public int getStart() {
        return fStart;
    }

    public boolean isInitialUpdate() {
        return fInitialUpdate;
    }

}
