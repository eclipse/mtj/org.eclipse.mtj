<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta
    name="copyright"
    content="Copyright (c) Motorola and others 2008. This page is made available under license. For full details see the LEGAL in the documentation book that contains this page."
>
<meta
    http-equiv="Content-Type"
    content="text/html; charset=utf-8"
>
<link
    rel="stylesheet"
    type="text/css"
    href="../style/book.css"
/>
<title>Signing MIDlet suites</title>
</head>

<body>
<h1>Signing MIDlet suites</h1>
<p>MTJ supports cryptographic signing of MIDlet suites. This page provides background material
on the signing process and provides an overview of how you go about signing a MIDlet Suite using MTJ.</p>
<ol>
    <li class="TOC"><a
        class="TOC"
        href="#background"
    >Background</a></li>
    <li class="TOC"><a
        class="TOC"
        href="#basicSteps"
    >Basic steps</a></li>
    <li class="TOC"><a
        class="TOC"
        href="#MTJSigning"
    >Signing using MTJ</a></li>
</ol>
<hr>
<h2 id="background">Background</h2>
<p>Under the MIDP 1.0 specification, all MIDlets operated using a "sandbox" security model.
Essentially, a MIDlet could only access API's that were specifically included as part of the MIDP
1.0 specification, or to libraries that were bundled as part of the same MIDlet suite.</p>
<p>MIDP 2.0 (<a href="#refs">JSR-118</a>) introduced the concept of "trusted" and "untrusted"
MIDlet suites. An "untrusted" suite operates under the same restrictions as in MIDP 1.0 - the only
API's that can be accessed are those that are part of the MIDP specification. "Trusted" suites can
be granted access to a wider selection of API's on the device. In addition, as part of the MIDP 2.0
specification, certain "standard" API's were provided that required authorization to use. For
example, an untrusted MIDlet suite must be able to access the javax.microedition.io.HttpConnection
API, but the system is required to make the user confirm that it is OK for the MIDlet suite to use
this API.</p>
<p>Trusted MIDlet suites can, on the other hand, be granted access to API's without explicit
user authorization, and may have access to API's that an untrusted MIDlet is not allowed to access.
Details of all of this are spelled out in JSR-118, and so will not be repeated here.</p>
<h2 id="basicSteps">Basic Steps in Signing a MIDlet Suite</h2>
<p>The basic steps in signing a MIDlet suite are:</p>
<ol>
    <li><a href="#permissions">Include the list of permissions your MIDlet suite is
    requesting in the JAD file and manifest</a></li>
    <li><a href="#getKey">Obtain an appropriate cryptographic key/certificate pair to do
    the signing</a></li>
    <li><a href="#sign">Sign the MIDlet suite as the final step in deploying it</a></li>
</ol>
<h3 id="permissions">Including the list of permissions your MIDlet suite is requesting in the
JAD file and manifest</h3>
<p>A MIDlet requests permissions by declaring them in the application descriptor and JAR
manifest, using either <strong>MIDlet-Permissions</strong> or <strong>MIDlet-Permissions-Opt</strong>
tag. Multiple permissions can be specified with either tag, and are separated by commas. During the
installation process, the device is required to look at the requested permissions. If a permission
is declared in <strong>MIDlet-Permissions</strong>, then the device must either be prepared to
install the MIDlet in a protection domain that grants access to these features, or the device must
abort the installation. However, if the permission is declared in <strong>MIDlet-Permissions-Opt</strong>,
and the MIDlet suite fails to gain access to an appropriate protection domain, the installation can
continue but the suite will not have access to the requested permission. So, if your MIDlet suite
depends on having a permission, and cannot operate without it, this permission should be requested
in <strong>MIDlet-Permissions</strong>.</p>
<p>If your application can live without a specific permission, but would make use of the feature
if access is granted, request the permission using <strong>MIDlet-Permissions-Opt</strong>. If your
MIDlet is not granted permission to the feature, you should get a SecurityException when you try to
access it. Note that for HTTP in particular, certain device providers violate the specification just
slightly and throw an IOException, instead of a SecurityException, in an odd attempt to be
compatible with MIDP 1.0. Check your toolkit provider's documentation on this.</p>
<p>The following is an example of requested permissions in a JAD file. The MIDlet suite is
indicating it must have access to HTTP and HTTPS and would like to have access to the <strong>PushRegistry,
VideoControl.getSnapshot</strong>, and SMS. If the latter three are denied, the suite can adjust and still
provide value to the user.</p>
<pre class="codeindent">MIDlet-Permissions: javax.microedition.io.Connector.http, javax.microedition.io.Connector.https
MIDlet-Permissions-Opt: javax.microedition.io.PushRegistry, javax.microedition.media.control.VideoControl.getSnapshot, javax.wireless.messaging.sms.receive, javax.wireless.messaging.sms.send
</pre>
<p>In this example, HTTP and HTTPS are not requested as optional. Because the application has
indicated it cannot function without these resources, if the device is unable to grant these
permissions on installation, the installation will abort. Whether or not the device will be able to
grant permission is dependent on its own security policies, and whether or not the MIDlet is
trusted. In order to be trusted, the MIDlet must be signed.</p>
<p><h3>MTJ provides a signing page on the JAD Editor in order to manage the suite permissions.</h3>
The signing page allows developers to:
<ul>
<li>Add/Remove Permissions</li>
<li>Order the permissions in the JAD</li>
<li><strong>Scan the project's code for permissions</strong></li>
</ul> 
</p>
<image src="./images/signing/jadeditor-page.png"/>

<h3 id="getKey">Obtaining an appropriate cryptographic key/certificate pair to do the signing</h3>
<p>MIDlet security is based around public key cryptography. The basic steps are:</p>
<ol>
    <li>You create a public key/private key pair.</li>
    <li>You use the private key to digitally "sign" the MIDlet suite.</li>
    <li>You then include the public key as part of the MIDlet suite so that anybody else can
    validate the signed information.</li>
</ol>
<p>Since only your private key can generate information that your public key can decrypt, so as
long as you keep your private key a secret, the world can be sure that anything that can be verified
by your public key was prepared by you.</p>
<p>Of course, this still begs the question of how someone knows that a particular public key
really came from you. What is to prevent me from claiming to be Bill Gates, and digitally signing
documents accordingly? Instead of just including your public key in the MIDlet suite, you include a
certificate that, in turn, includes the public key. The certificate is issued by a trusted third
party such as Verisign, and essentially promises that this public key really did come from you.</p>
<p>How does the device know to trust the Verisign certificate? Devices are programmed with a set
of "root certificate authorities" that they will always trust. Certificates are themselves signed,
using information that is pre-programmed into the phones. Thus, the device can verify that your
certificate was actually issued by Verisign by using its pre-programmed information. Alternately,
your certificate may be signed by an "intermediate" key, which is itself, in turn, signed by a
trusted key. This is called a "certificate chain." Chains can actually be any number of keys in
length, although it's rare that they contain more than two or three links.</p>
<p>As a result, the process for getting your keys set up typically works like this:</p>
<ul>
    <li>You generate a public/private key pair using a convenient tool.</li>
    <li>You generate a "certificate request" that contains your public key, and send it to a
    certificate authority.</li>
    <li>The certificate authority does a little checking to make sure that you are who you say
    you are. What kind of checking is done varies from authority to authority.</li>
    <li>Once the certificate authority is satisfied that you are who you say you are (and you
    pay them their fee) the certificate authority wraps your public key in a certificate, and sends
    it back to you.</li>
    <li>You import the certificate back into your keystore so that you will have it when you
    need to sign something.</li>
</ul>
<h3 id="MTJSigning">Sign Using MTJ</h3>
<p>In order to sign the suite using MTJ the developer will need to setup the signing settings in one of two ways:
<ol>
<li><strong>Workspace Preferences</strong> signing page for default project signing settings</li>
<li><strong>Project Properties</strong> signing page for specific project signing settings</li>
</ol>
</p>
<p><h3>1. Signing Preferences Page</h3></p>
<p>
On this page the user can select a keystore file to be used as the default keystore. By pressing the
<strong>External...</strong> button the user will select the default keystore.
<p/>
<image src="./images/signing/preferences-page.png"/> 
<p/>
After selecting the file the user will be prompted for the keystore password in order to list all private key aliases:
<p/>
<image src="./images/signing/kspassword-prompt.png"/>
<p>
<strong>NOTE:</strong> The default settings for keystore's <strong>Provider</strong> and <strong>Type</strong> are:
<table class="datatable">
	<tr>
		<th>Option</th>
		<th>Value</th>
	</tr>
	<tr>
		<td align="left" valign="top">Provider</td>
		<td align="left" valign="top">SUN</td>
	</tr>
	<tr>
		<td align="left" valign="top">Type</td>
		<td align="left" valign="top">JKS</td>
	</tr>
</table>
<p/>
In case your keystore has either a different Provider or Type they must be set in the <strong>Advanced Settings</strong>
section before opening the external file:
<p/>
<image src="./images/signing/advanced-section.png"/>
</p>
The keystore password prompt allows the user to save the keystore password in the workspace keyring in order
to avoid having to enter the password when needed.
<br>
Once the correct password is entered the list of available aliases will be listed as bellow:
<p/>
<image src="./images/signing/preferences-page-with-aliases.png"/>
</p>
<p><h3>2. Project Properties Page</h3>
<p>
In case a project needs signing specific settings other than the default set in the workspace
preferences, the developer can setup project specific signing settings by opening the signing
page on the project properties:
<p/>
<image src="./images/signing/properties-page.png"/>
<p/>
The only difference between the project and the preferences signing pages are the password management
as show bellow:
<p/>
<image src="./images/signing/keys-management.png"/>
<p/>
Both keystore and key passwords can be managed in one of the following forms:
<ul>
<li>Prompt for password when required</li>
<li>Save password in workspace keyring</li>
<li>Save password as part of project</li>
</ul>
</p>
</p>
<p>
Once all signing configuration is set, open the signing page under the JAD Editor. Make sure the sign generated
packages button is selected in order to sign your application.<br>
<p/>
<image src="./images/signing/sign-project.png"/>
<p/>
On the alias section there should be listed all available aliases from the configured keystore, select one of the
available aliases and MTJ will sign your application with it.
<p/>
<image src="./images/signing/jadeditor-aliases.png"/>
</p>
<h3><strong>NOTE: MTJ only signs the package generated under the <strong>deployed</strong> folder, it does
not sign any intermediate package since it is a time consuming task for incremental builds.</strong></h3>
</body>
</html>