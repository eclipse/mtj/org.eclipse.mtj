/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version                     
 */
package org.eclipse.mtj.internal.core.l10n;

import org.eclipse.osgi.util.NLS;

/**
 * Class with methods for manipulating the L10nBuilder messages.
 * 
 * @author David Marques
 * @since 0.9.1
 */
public class Messages extends NLS {

    public static String L10nBuilder_BuildingLocalizationData;
    public static String L10nBuilder_clean_CleaningOldProperties;
    public static String L10nBuilder_InvalidModel;
    public static String L10nBuilder_ErrorParsingLocalizationData;
    public static String L10nBuilder_ErrorWritingProperty;
    public static String L10nBuilder_loadL10nModel_CanNotRealizeLocation;
    public static String L10nBuilder_LocalizationDataDoesNotExist;
    public static String L10nBuilder_PackageFolderNotFound;
    public static String L10nBuilder_ProcessingLocalizationData;
    public static String L10nBuilder_SourceFolderNotFound;

    private static final String BUNDLE_NAME = "org.eclipse.mtj.internal.core.l10n.messages"; //$NON-NLS-1$

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
