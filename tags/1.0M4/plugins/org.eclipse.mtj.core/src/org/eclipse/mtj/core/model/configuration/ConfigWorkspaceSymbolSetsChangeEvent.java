/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.model.configuration;

import java.util.EventObject;
import java.util.List;

import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;

/**
 * ConfigWorkspaceSymbolSetsChangeEvent is used to notify that the
 * workspaceScopeSymbolSets of the configuration changed.
 * 
 * @author wangf
 */
public class ConfigWorkspaceSymbolSetsChangeEvent extends EventObject {

    private static final long serialVersionUID = 1L;
    private List<SymbolDefinitionSet> oldSymbolDefinitionSets;
    private List<SymbolDefinitionSet> newSymbolDefinitionSets;

    public ConfigWorkspaceSymbolSetsChangeEvent(Object source,
            List<SymbolDefinitionSet> oldSets, List<SymbolDefinitionSet> newSets) {
        super(source);
        this.oldSymbolDefinitionSets = oldSets;
        this.newSymbolDefinitionSets = newSets;
    }

    public List<SymbolDefinitionSet> getNewSymbolDefinitionSets() {
        return newSymbolDefinitionSets;
    }

    public List<SymbolDefinitionSet> getOldSymbolDefinitionSets() {
        return oldSymbolDefinitionSets;
    }

    public void setNewSymbolDefinitionSets(
            List<SymbolDefinitionSet> newSymbolDefinitionSets) {
        this.newSymbolDefinitionSets = newSymbolDefinitionSets;
    }

    public void setOldSymbolDefinitionSets(
            List<SymbolDefinitionSet> oldSymbolDefinitionSets) {
        this.oldSymbolDefinitionSets = oldSymbolDefinitionSets;
    }

}
