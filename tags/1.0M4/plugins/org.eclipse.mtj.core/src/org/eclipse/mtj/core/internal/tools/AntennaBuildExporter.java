/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Feng Wang (Sybase) - Enhance exported Antenna build files to support
 *                          build for multi configurations.
 */
package org.eclipse.mtj.core.internal.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Preferences;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.PreferenceAccessor;
import org.eclipse.mtj.core.internal.utils.FilteringClasspathEntryVisitor;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.internal.utils.XMLUtils;
import org.eclipse.mtj.core.model.configuration.Configuration;
import org.eclipse.mtj.core.model.jad.ApplicationDescriptor;
import org.eclipse.mtj.core.model.jad.IJADConstants;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;
import org.eclipse.mtj.core.model.preprocessor.symbol.Symbol;
import org.eclipse.mtj.core.model.preprocessor.symbol.SymbolUtils;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Tool for exporting an Antenna build file.
 * 
 * @author Craig Setera
 */
public class AntennaBuildExporter {
    // Classpath entry visitor for helping build the build.xml file
    private class BuildClasspathEntryVisitor extends
            FilteringClasspathEntryVisitor {
        private Map<IJavaProject, ProjectInfo> projectInfoMap;

        /**
         * Construct a new visitor that will be responsible for creating the
         * necessary elements.
         */
        public BuildClasspathEntryVisitor(IJavaProject rootProject) {
            projectInfoMap = new LinkedHashMap<IJavaProject, ProjectInfo>();
            createProjectInfo(rootProject, true);
        }

        /**
         * Add the inclusion and exclusion elements as necessary.
         * 
         * @param entry
         * @param element
         */
        private void addIncludesAndExcludes(IClasspathEntry entry,
                Element element) {
            for (int i = 0; i < entry.getExclusionPatterns().length; i++) {
                IPath pattern = entry.getExclusionPatterns()[i];
                Element exclusionElement = newChildElement(element, "exclude");
                exclusionElement.setAttribute(ATTR_NAME, pattern.toString());
            }

            for (int i = 0; i < entry.getInclusionPatterns().length; i++) {
                IPath pattern = entry.getInclusionPatterns()[i];
                Element inclusionElement = newChildElement(element, "include");
                inclusionElement.setAttribute(ATTR_NAME, pattern.toString());
            }
        }

        /**
         * Create a new project information structure.
         * 
         * @param javaProject
         * @param exported
         */
        private void createProjectInfo(IJavaProject javaProject,
                boolean exported) {
            ProjectInfo info = new ProjectInfo(javaProject, exported);
            projectInfoMap.put(javaProject, info);
        }

        /**
         * Get the file system location for the specified classpath entry.
         * 
         * @param entry
         * @return
         * @throws CoreException
         */
        private IPath getLibraryLocation(IClasspathEntry entry)
                throws CoreException {
            IPath libLocation = null;

            Object resolved = Utils.getResolvedClasspathEntry(entry);
            if (resolved instanceof IResource) {
                IResource libResource = (IResource) resolved;
                libLocation = libResource.getLocation();
            } else if (resolved instanceof File) {
                libLocation = entry.getPath();
            }

            return libLocation;
        }

        /**
         * Get the holder of information for the specified project.
         * 
         * @param javaProject
         * @return
         */
        private ProjectInfo getProjectInfo(IJavaProject javaProject) {
            return (ProjectInfo) projectInfoMap.get(javaProject);
        }

        /**
         * Return the map of project information.
         * 
         * @return
         */
        public Map<IJavaProject, ProjectInfo> getProjectInfoMap() {
            return projectInfoMap;
        }

        /**
         * Return the project relative path.
         * 
         * @param javaProject
         * @param projectInfo
         * @param location
         * @return
         * @throws CoreException
         */
        private String getProjectRelativeValue(IJavaProject javaProject,
                ProjectInfo projectInfo, IPath location) throws CoreException {
            StringBuffer sb = new StringBuffer();
            IPath projectPath = javaProject.getProject().getLocation();

            String relativePath = getRelativePath(projectPath, location);
            if (relativePath == null) {
                // Can't get relation to projectPath, use location as it is
                sb.append(location.toString());
            } else {
                sb.append("${").append(projectInfo.getAntProjectPropertyName())
                        .append("}").append(relativePath);
            }

            return sb.toString();
        }

        /**
         * Get the file system location for the specified classpath entry.
         * 
         * @param entry
         * @return
         * @throws CoreException
         */
        private IPath getSourceLocation(IClasspathEntry entry)
                throws CoreException {
            IPath sourceLocation = null;

            Object resolved = Utils.getResolvedClasspathEntry(entry);
            if (resolved instanceof IResource) {
                IResource srcResource = (IResource) resolved;
                sourceLocation = srcResource.getLocation();
            }

            return sourceLocation;
        }

        /**
         * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitLibraryEntry(org.eclipse.jdt.core.IClasspathEntry,
         *      org.eclipse.jdt.core.IJavaProject,
         *      org.eclipse.core.runtime.IProgressMonitor)
         */
        public void visitLibraryEntry(IClasspathEntry entry,
                IJavaProject javaProject, IProgressMonitor monitor)
                throws CoreException {
            ProjectInfo projectInfo = getProjectInfo(javaProject);
            Element classpathElement = projectInfo.getClasspathElement();

            IPath libLocation = getLibraryLocation(entry);
            if (libLocation != null) {
                File libFile = libLocation.toFile();

                Element pathElement = newChildElement(classpathElement,
                        ELEM_PATH);
                String relativePath = getProjectRelativeValue(javaProject,
                        projectInfo, libLocation);
                pathElement.setAttribute(ATTR_LOCATION, relativePath);
                addIncludesAndExcludes(entry, pathElement);

                if (isLibraryExported(entry)) {
                    if (libFile.isDirectory()) {
                        // Create a new zip fileset for the packaging
                        Element filesetElement = mtjBuildXmlDocument
                                .createElement(ELEM_FILESET);
                        projectInfo.addPackageFilesetElement(filesetElement);
                        filesetElement.setAttribute(ATTR_DIR, relativePath);
                    } else {
                        // Create a new zip fileset for the packaging
                        Element zipFilesetElement = mtjBuildXmlDocument
                                .createElement("zipfileset");
                        projectInfo.addPackageFilesetElement(zipFilesetElement);
                        zipFilesetElement.setAttribute("src", relativePath);
                        addIncludesAndExcludes(entry, zipFilesetElement);
                    }
                }
            }
        }

        /**
         * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitProject(IClasspathEntry,
         *      org.eclipse.jdt.core.IJavaProject,
         *      org.eclipse.jdt.core.IJavaProject,
         *      org.eclipse.core.runtime.IProgressMonitor)
         */
        public boolean visitProject(IClasspathEntry entry,
                IJavaProject javaProject, IJavaProject classpathProject,
                IProgressMonitor monitor) throws CoreException {
            // Force the project info into the map so that it
            // is held in the correct order
            boolean exported = entry.isExported();
            createProjectInfo(classpathProject, exported);

            return super.visitProject(entry, javaProject, classpathProject,
                    monitor);
        }

        /**
         * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitSourceEntry(org.eclipse.jdt.core.IClasspathEntry,
         *      org.eclipse.jdt.core.IJavaProject,
         *      org.eclipse.core.runtime.IProgressMonitor)
         */
        public void visitSourceEntry(IClasspathEntry entry,
                IJavaProject javaProject, IProgressMonitor monitor)
                throws CoreException {
            IPath srcLocation = getSourceLocation(entry);

            if (srcLocation != null) {
                ProjectInfo projectInfo = getProjectInfo(javaProject);

                // Create the wtkbuild task call
                String buildTask = projectInfo.isExported() ? ELEM_WTKBUILD
                        : "javac";
                Element buildElement = mtjBuildXmlDocument
                        .createElement(buildTask);
                projectInfo.addWtkBuildElement(entry, buildElement);
                buildElement.setAttribute("destdir", projectInfo
                        .getBuildDestination());
                buildElement.setAttribute("sourcepath", "");
                buildElement.setAttribute("encoding", "${src.encoding}");
                buildElement.setAttribute("source", "1.3");

                // Calculate the src directory relative to the specified
                // project.
                String relativePath = getProjectRelativeValue(javaProject,
                        projectInfo, srcLocation);
                buildElement.setAttribute(ATTR_SRCDIR, relativePath);

                // Add the inclusion and exclusion patterns as necessary
                addIncludesAndExcludes(entry, buildElement);

                // Add the classpath reference
                Element classpathElement = newChildElement(buildElement,
                        ELEM_CLASSPATH);
                classpathElement.setAttribute(ATTR_REFID, projectInfo
                        .getClasspathElementId());

                // Make sure to copy the non-Java resources into the package
                Element srcPackageElement = mtjBuildXmlDocument
                        .createElement(ELEM_FILESET);
                srcPackageElement.setAttribute(ATTR_DIR, relativePath);

                Element javaExclude = newChildElement(srcPackageElement,
                        "exclude");
                javaExclude.setAttribute(ATTR_NAME, "**/*.java");
                Element buildExclude = newChildElement(srcPackageElement,
                        "exclude");
                buildExclude.setAttribute(ATTR_NAME, "build/");
                addIncludesAndExcludes(entry, srcPackageElement);
                projectInfo.addPackageFilesetElement(srcPackageElement);
            } else {
                MTJCorePlugin.log(IStatus.WARNING,
                        "Skipping unresolvable classpath entry " + entry);
            }
        }
    }

    /**
     * Holder for project-specific information built during the visitation of
     * the classpath
     */
    private class ProjectInfo {
        private String safeProjectName;
        private IJavaProject javaProject;
        private boolean exported;
        private Element classpathElement;
        private Map<IClasspathEntry, Element> wtkBuildElements;
        private List<Element> packageFilesetElements;

        /**
         * Construct a new project information instance for the specified java
         * project.
         * 
         * @param javaProject
         */
        ProjectInfo(IJavaProject javaProject, boolean exported) {
            this.javaProject = javaProject;
            this.exported = exported;

            wtkBuildElements = new HashMap<IClasspathEntry, Element>();
            packageFilesetElements = new ArrayList<Element>();

            // Calculate a "safe" project name
            safeProjectName = javaProject.getElementName();
            safeProjectName = safeProjectName.replace(' ', '_');
        }

        /**
         * Add a new fileset element to the list.
         * 
         * @param element
         */
        public void addPackageFilesetElement(Element element) {
            packageFilesetElements.add(element);
        }

        /**
         * Add a new wtkbuild element to the list.
         * 
         * @param srcEntry
         * @param element
         */
        public void addWtkBuildElement(IClasspathEntry srcEntry, Element element) {
            if (!wtkBuildElements.containsKey(srcEntry)) {
                wtkBuildElements.put(srcEntry, element);
            }
        }

        /**
         * Return the Ant property name to be used for this project.
         */
        public String getAntProjectPropertyName() {
            return "project.root." + safeProjectName;
        }

        /**
         * Return the Ant project property value.
         * 
         * @return
         */
        public String getAntProjectPropertyValue() {
            String relativePath = null;

            // Figure out the property value relative to the base directory
            IPath projectPath = javaProject.getProject().getLocation();
            relativePath = getRelativePath(basedirPath, projectPath);
            if (relativePath == null) {
                relativePath = projectPath.toString();
            }

            return relativePath;
        }

        /**
         * Return the destination directory to be used for class build output.
         * 
         * @return
         */
        public String getBuildDestination() {
            return exported ? PATH_BUILD_CLASSES : PATH_BUILD_CLASSES_NO_EXPORT;
        }

        /**
         * Return the classpath element.
         * 
         * @return
         */
        public Element getClasspathElement() {
            if (classpathElement == null) {
                classpathElement = mtjBuildXmlDocument.createElement(ELEM_PATH);
                classpathElement.setAttribute(ATTR_ID, getClasspathElementId());

                Element pathElement = newChildElement(classpathElement,
                        ELEM_PATH);
                pathElement.setAttribute(ATTR_LOCATION, PATH_BUILD_CLASSES);

                pathElement = newChildElement(classpathElement, ELEM_PATH);
                pathElement.setAttribute(ATTR_LOCATION,
                        PATH_BUILD_CLASSES_NO_EXPORT);
            }

            return classpathElement;
        }

        /**
         * Return the identifier of the classpath path element.
         * 
         * @return
         */
        public String getClasspathElementId() {
            return "classpath." + safeProjectName;
        }

        /**
         * @return Returns the packageFilesetElements.
         */
        public List<Element> getPackageFilesetElements() {
            return packageFilesetElements;
        }

        /**
         * @return Returns the wtkBuildElements.
         */
        public Collection<Element> getWtkBuildElements() {
            return wtkBuildElements.values();
        }

        /**
         * @return Returns the exported flag indication
         */
        public boolean isExported() {
            return exported;
        }
    }

    public static final String ATTR_MESSAGE = "message";
    public static final String ELEM_ECHO = "echo";

    public static final String MTJ_BUILD_XML_FILE_NAME = "mtj-build.xml";
    public static final String BUILD_XML_FILE_NAME = "build.xml";
    public static final String ATTR_PRCFILE = "prcfile";
    public static final String ELEM_WTKMAKEPRC = "wtkmakeprc";
    public static final String ELEM_WTKRUN = "wtkrun";
    public static final String ELEM_MKDIR = "mkdir";
    public static final String ELEM_WTKPREVERIFY = "wtkpreverify";
    public static final String ELEM_WTKOBFUSCATE = "wtkobfuscate";
    public static final String ATTR_TARGET = "target";
    public static final String ATTR_JARFILE = "jarfile";
    public static final String ATTR_JADFILE = "jadfile";
    public static final String ELEM_WTKPACKAGE = "wtkpackage";
    public static final String ATTR_TOFILE = "tofile";
    public static final String ELEM_COPY = "copy";
    public static final String ATTR_DIR = "dir";
    public static final String ELEM_FILESET = "fileset";
    public static final String ELEM_ANTCALL = "antcall";
    public static final String TASK_MTJ_BUILD = "-mtj-build";
    public static final String ATTR_REFID = "refid";
    public static final String ELEM_CLASSPATH = "classpath";
    public static final String ATTR_BOOTCLASSPATH = "bootclasspath";
    public static final String ELEM_WTKBUILD = "wtkbuild";
    public static final String ATTR_VERBOSE = "verbose";
    public static final String ELEM_WTKPREPROCESS = "wtkpreprocess";
    public static final String ATTR_SRCDIR = "srcdir";
    public static final String ATTR_DESTDIR = "destdir";
    public static final String ATTR_NAME = "name";
    public static final String TASK_MTJ_INITIALIZE = "-mtj-initialize";
    public static final String ATTR_DEPENDS = "depends";
    public static final String ELEM_TARGET = ATTR_TARGET;
    public static final String ATTR_LOCATION = "location";

    public static final String ELEM_PATH = "path";
    public static final String ATTR_ID = "id";
    private static final String NO_EXPORT = "_no_export";

    private static final String PATH_BUILD_CLASSES_NO_EXPORT = "${path.build.classes}/"
            + NO_EXPORT;

    private static final String PATH_BUILD_CLASSES = "${path.build.classes}";

    private static final Pattern SUBSTITUTION_PATTERN = Pattern
            .compile("\\@\\{(.+?)\\}");

    private IMidletSuiteProject midletSuite;
    private IJavaProject javaProject;
    private IPath basedirPath;
    private String projectName;
    private Properties buildProperties;
    private Document mtjBuildXmlDocument;

    /**
     * Construct a new exporter for the specified midlet suite project.
     * 
     * @param midletSuite
     */
    public AntennaBuildExporter(IMidletSuiteProject midletSuite) {
        super();
        this.midletSuite = midletSuite;
        this.javaProject = midletSuite.getJavaProject();

        basedirPath = javaProject.getProject().getLocation();
        projectName = midletSuite.getProject().getName();
    }

    /**
     * Add the obfuscation-related properties.
     * 
     * @param props
     */
    private void addObfuscationProperties(Properties props) {
        File proguardFile = MTJCorePlugin.getProguardJarFile();
        boolean obfuscate = ((proguardFile != null) && proguardFile.exists());

        props.setProperty("flag.should.obfuscate", Boolean.toString(obfuscate));

        if (obfuscate) {
            File proguardHome = proguardFile.getParentFile().getParentFile();
            props.setProperty("wtk.proguard.home", proguardHome.toString());
        }
    }

    /**
     * Add the appropriate J2ME version properties to match the JAD versions.
     * 
     * @param props
     */
    private void addVersionProperties(Properties props) {
        ApplicationDescriptor descriptor = midletSuite
                .getApplicationDescriptor();
        Properties manifestProperties = descriptor.getManifestProperties();

        String versionString = manifestProperties
                .getProperty(IJADConstants.JAD_MICROEDITION_CONFIG);
        if (versionString != null) {
            int index = versionString.indexOf('-');
            versionString = (index == -1) ? versionString : versionString
                    .substring(index + 1);
            props.setProperty("wtk.cldc.version", versionString);
        }

        versionString = manifestProperties
                .getProperty(IJADConstants.JAD_MICROEDITION_PROFILE);
        if (versionString != null) {
            int index = versionString.indexOf('-');
            versionString = (index == -1) ? versionString : versionString
                    .substring(index + 1);
            props.setProperty("wtk.midp.version", versionString);
        }
    }

    /**
     * Add a new argument to the WTK obfuscate element.
     * 
     * @param wktobfuscateElement
     * @param argument
     */
    private void addWtkObfuscateArgument(Element wktobfuscateElement,
            String argument) {
        Element argumentElement = newChildElement(wktobfuscateElement,
                "argument");
        argumentElement.setAttribute("value", argument);
    }

    /**
     * Create the initial properties object.
     * 
     * @return
     */
    private Properties createInitialProperties() {
        Properties props = new Properties();
        props.setProperty("midlet.name", replaceSpace(javaProject.getProject()
                .getName()));
        props.setProperty("jad.name",
                replaceSpace(midletSuite.getJadFileName()));
        props.setProperty("descriptor.name", midletSuite
                .getApplicationDescriptorFile().getName());
        props.setProperty("path.build", "${basedir}/build");
        props.setProperty("path.build.classes", "${basedir}/build/classes");
        props.setProperty("path.preprocess.output",
                "${basedir}/preprocessOutput");

        setClassesFolderForConfigs(props);

        setPreprocessOutputFolderForConfigs(props);

        setDeployFolderForConfigs(props);

        // Antenna stuff
        Preferences prefs = MTJCorePlugin.getDefault().getPluginPreferences();
        props.setProperty("wtk.home", prefs
                .getString(IMTJCoreConstants.PREF_WTK_ROOT));
        props.setProperty("path.antenna.jar", prefs
                .getString(IMTJCoreConstants.PREF_ANTENNA_JAR));
        boolean autoVersion = PreferenceAccessor.instance
                .getAutoversionPackage(midletSuite.getProject());
        props.setProperty("flag.autoversion", Boolean.toString(autoVersion));
        props.setProperty("flag.preverify", "true");

        // Set default encoding to UTF-8
        props.setProperty("src.encoding", "UTF-8");

        setBootclasspathForConfigs(props);

        // Properties necessary for running in the emulator
        props.setProperty("run.device.name", "DefaultColorPhone");
        props.setProperty("run.trace.options", "");

        // Add some default properties based on various things
        addObfuscationProperties(props);
        addVersionProperties(props);

        return props;
    }

    /**
     * Do the export for the midlet suite.
     * 
     * @throws CoreException
     */
    public void doExport(IProgressMonitor monitor) throws CoreException,
            AntennaExportException {
        // Validate the environment is ok.
        validateEnvironment(monitor);

        try {
            // Read in the templates
            buildProperties = createInitialProperties();
            mtjBuildXmlDocument = readMTJBuildTemplate();

            // Traverse the classpath and update the results along the way
            BuildClasspathEntryVisitor visitor = traverseClasspath(monitor);
            updateMTJBuildXml(visitor.getProjectInfoMap());

            // Write out the results
            exportBuildXml(monitor);
            writeBuildProperties(monitor);
            writeMTJBuildXml(monitor);
            exportSymbolFileForConfigs(monitor);

            // Refresh so these files show in the workbench.
            javaProject.getProject().refreshLocal(IResource.DEPTH_ONE, monitor);

        } catch (Exception e) {
            if (e instanceof CoreException) {
                throw (CoreException) e;
            } else {
                MTJCorePlugin.throwCoreException(IStatus.ERROR, -999, e);
            }
        }
    }

    /**
     * Export the build.xml file.
     * 
     * @param monitor
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws TransformerException
     * @throws CoreException
     */
    private void exportBuildXml(IProgressMonitor monitor)
            throws ParserConfigurationException, SAXException,
            TransformerException, IOException, CoreException {
        // Check to make sure that we don't overwrite a user's build.xml file
        // with our template file.
        // TODO What do we do, if anything, when the build.xml file already
        // exists?
        File buildXmlFile = getMidletSuiteFile("build.xml", monitor);
        if (!buildXmlFile.exists()) {
            // Template values
            Properties props = new Properties();
            props.setProperty("mtj.version", MTJCorePlugin.getPluginVersion());
            props.setProperty("date", (new Date()).toString());
            props.setProperty("project.name", projectName);

            InputStream is = getClass()
                    .getResourceAsStream("buildtemplate.xml");
            if (is != null) {
                // Read the template and do the replacements
                StringBuffer sb = readBuildXmlTemplate(is);
                replaceTemplateValues(sb, props);

                // Write the results
                FileOutputStream fos = new FileOutputStream(buildXmlFile);
                OutputStreamWriter writer = new OutputStreamWriter(fos, "UTF-8");
                writer.write(sb.toString());
                writer.close();
            }
        }
    }

    /**
     * Antenna wtkpreprocess task need a symbol file(which contains symbols) to
     * do preprocess for each configuration.<br>
     * Note: Should not use
     * {@link Properties#store(java.io.OutputStream, String)} to write symbol
     * file. Because any line start by "#" will cause error for Antenna
     * preprocessor parse symbols.
     * 
     * @throws IOException
     */
    private void exportSymbolFileForConfigs(IProgressMonitor monitor)
            throws IOException {
        for (Configuration config : midletSuite.getConfigurations()) {

            String safeConfigName = replaceSpace(config.getName());
            File symbolFile = getMidletSuiteFile(safeConfigName + ".symbols",
                    monitor);
            FileOutputStream fos = new FileOutputStream(symbolFile);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos,
                    "8859_1"));
            // write symbols contained by configuration
            for (Symbol s : config.getSymbolSetForPreprocessing()) {
                bw.write(s.getName() + "=" + s.getSafeValue());
                bw.newLine();
            }
            // write symbols from EnabledSymbolDefinitionSet which come from
            // workspace scope
            try {
                List<SymbolDefinitionSet> sets = config
                        .getWorkspaceScopeSymbolSets();
                for (SymbolDefinitionSet symbols : sets) {
                    Set<Entry<String, String>> entries = symbols
                            .getDefinedSymbols().entrySet();
                    for (Entry<String, String> s : entries) {
                        bw.write(s.getKey() + "="
                                + SymbolUtils.getSafeSymbolValue(s.getValue()));
                        bw.newLine();
                    }
                }
            } catch (Exception e) {
                MTJCorePlugin.log(IStatus.ERROR, e);
            }

            bw.flush();
            fos.close();
        }
    }

    /**
     * Return the specified element within the specified parent. If more
     * elements exist with that name, the first will be returned. If not found,
     * <code>null</code> will be returned.
     * 
     * @param parentElement
     * @param elementName
     * @return
     */
    private Element findElement(Element parentElement, String elementName) {
        Element element = null;

        NodeList elements = parentElement.getElementsByTagName(elementName);
        if (elements.getLength() > 0) {
            element = (Element) elements.item(0);
        }

        return element;
    }

    private String getCompileDestDirForConfig(String destDir, String configName) {
        return destDir.replaceFirst("}", "." + configName + "}");
    }

    /**
     * Return a File reference to a file with the specified name in the midlet
     * suite project.
     * 
     * @param filename
     * @param monitor
     * @return
     * @throws CoreException
     */
    private File getMidletSuiteFile(String filename, IProgressMonitor monitor) {
        IFile file = midletSuite.getProject().getFile(filename);
        return file.getLocation().toFile();
    }

    /**
     * Get the options to be specified when calling Proguard for obfuscation.
     * 
     * @return
     */
    private String getProguardOptions() {
        IProject project = midletSuite.getProject();
        PreferenceAccessor obfuscationPrefs = PreferenceAccessor.instance;
        String specifiedOptions = obfuscationPrefs
                .getSpecifiedProguardOptions(project);
        boolean useSpecified = obfuscationPrefs
                .isUseSpecifiedProguardOptions(project);

        return useSpecified ? specifiedOptions : obfuscationPrefs
                .getDefaultProguardOptions();
    }

    /**
     * Return the relative path.
     * 
     * @param basePath the path that acts as the base for comparison
     * @param tgtPath the path being compared to the base path
     * @return relative path, or null if relation not possible
     */
    private String getRelativePath(IPath basePath, IPath tgtPath) {
        String path = null;

        // Find the common path prefix
        int matchingSegments = tgtPath.matchingFirstSegments(basePath);

        String baseDevice = basePath.getDevice();
        String tgtDevice = tgtPath.getDevice();
        boolean bothNull = ((baseDevice == null) && (tgtDevice == null));

        if (bothNull || baseDevice.equals(tgtDevice)) {
            // Step up the directory tree
            StringBuffer relativePath = new StringBuffer();
            int upSteps = basedirPath.segmentCount() - matchingSegments;
            for (int i = 0; i < upSteps; i++) {
                relativePath.append("/..");
            }

            // Tack on the src folder segments
            tgtPath = tgtPath.removeFirstSegments(matchingSegments);
            String[] segments = tgtPath.segments();
            for (String segment : segments) {
                relativePath.append("/").append(segment);
            }

            path = relativePath.toString();
        }

        return path;
    }

    /**
     * Return the root of the first located Sun wireless toolkit for use in
     * setting the appropriate Antenna property or <code>null</code> if one
     * cannot be found.
     * 
     * @return
     */
    private File getWTKRoot() {
        Preferences prefs = MTJCorePlugin.getDefault().getPluginPreferences();
        return new File(prefs.getString(IMTJCoreConstants.PREF_WTK_ROOT));
    }

    /**
     * insert build tasks into mtj build xml doc.
     * 
     * @param projectInfoMap
     */
    private void insertBuildTasks(Map<IJavaProject, ProjectInfo> projectInfoMap) {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        Element insertBuildBeforeThisElement = findElement(documentElement,
                "insert_build_task_for_configs");
        NodeList tasks = mtjBuildXmlDocument.getElementsByTagName(ELEM_TARGET);
        Element mtjBuildElem = null;
        for (int i = 0; i < tasks.getLength(); i++) {
            Element task = (Element) tasks.item(i);
            if (TASK_MTJ_BUILD.equals(task.getAttribute(ATTR_NAME))) {
                mtjBuildElem = task;
                break;
            }
        }
        for (Configuration config : midletSuite.getConfigurations()) {
            String configName = replaceSpace(config.getName());
            Element buildConfigTask = mtjBuildXmlDocument
                    .createElement(ELEM_TARGET);
            buildConfigTask.setAttribute(ATTR_DEPENDS, TASK_MTJ_INITIALIZE);
            buildConfigTask.setAttribute(ATTR_NAME, "-mtj-build-" + configName);
            Element echo = XMLUtils.createChild(buildConfigTask, ELEM_ECHO);
            echo.setAttribute(ATTR_MESSAGE, NLS.bind(
                    Messages.Message_BuildConfiguration, configName));
            Iterator<Map.Entry<IJavaProject, ProjectInfo>> iterator = projectInfoMap
                    .entrySet().iterator();
            String preprocessOutput = "${path.preprocess.output." + configName
                    + "}";
            // String destDir = "${path.build.classes." + configName + "}";
            while (iterator.hasNext()) {
                Map.Entry<IJavaProject, ProjectInfo> entry = (Map.Entry<IJavaProject, ProjectInfo>) iterator
                        .next();
                ProjectInfo info = (ProjectInfo) entry.getValue();
                String destDirWihoutConfigName = "";
                if (info.javaProject == this.javaProject) {
                    for (Element element : info.getWtkBuildElements()) {
                        // Element element = (Element) elements.next();
                        String srcDir = element.getAttribute(ATTR_SRCDIR);
                        Element preprocessTask = XMLUtils.createChild(
                                buildConfigTask, ELEM_WTKPREPROCESS);
                        preprocessTask.setAttribute(ATTR_SRCDIR, srcDir);
                        preprocessTask.setAttribute(ATTR_VERBOSE, "true");
                        preprocessTask.setAttribute(ATTR_DESTDIR,
                                preprocessOutput);
                        preprocessTask.setAttribute("printsymbols", "true");
                        preprocessTask.setAttribute("savesymbols",
                                preprocessOutput + "/usedSymbols.txt");
                        preprocessTask.setAttribute("debuglevel", "info");

                        Element symbolFile = XMLUtils.createChild(
                                preprocessTask, "symbols_file");
                        symbolFile.setAttribute(ATTR_NAME, "${project.root."
                                + replaceSpace(midletSuite.getProject()
                                        .getName()) + "}/" + configName
                                + ".symbols");
                        destDirWihoutConfigName = element
                                .getAttribute(ATTR_DESTDIR);

                    }
                    Element wtkBuildRootProject = XMLUtils.createChild(
                            buildConfigTask, ELEM_WTKBUILD);
                    wtkBuildRootProject.setAttribute(ATTR_BOOTCLASSPATH,
                            "${bootclasspath." + configName + "}");
                    wtkBuildRootProject.setAttribute(ATTR_SRCDIR,
                            preprocessOutput);
                    wtkBuildRootProject.setAttribute(ATTR_DESTDIR,
                            getCompileDestDirForConfig(destDirWihoutConfigName,
                                    configName));
                    wtkBuildRootProject.setAttribute("encoding",
                            "${src.encoding}");
                    wtkBuildRootProject.setAttribute("source", "1.3");
                    wtkBuildRootProject.setAttribute("sourcepath", "");

                    Element classpath = XMLUtils.createChild(
                            wtkBuildRootProject, ELEM_CLASSPATH);
                    classpath.setAttribute(ATTR_REFID, info
                            .getClasspathElementId()
                            + "." + configName);
                } else {
                    for (Element element : info.getWtkBuildElements()) {
                        Element clonedElement = (Element) element
                                .cloneNode(true);
                        destDirWihoutConfigName = clonedElement
                                .getAttribute(ATTR_DESTDIR);
                        clonedElement.setAttribute(ATTR_DESTDIR,
                                getCompileDestDirForConfig(
                                        destDirWihoutConfigName, configName));
                        Element classpath = XMLUtils
                                .getFirstElementWithTagName(clonedElement,
                                        ELEM_CLASSPATH);
                        classpath.setAttribute(ATTR_REFID, info
                                .getClasspathElementId()
                                + "." + configName);
                        buildConfigTask.insertBefore(clonedElement, echo
                                .getNextSibling());
                    }
                }

            }
            insertBuildBeforeThisElement.getParentNode().insertBefore(
                    buildConfigTask, insertBuildBeforeThisElement);

            if (mtjBuildElem != null) {
                Element antCall = newChildElement(mtjBuildElem, ELEM_ANTCALL);
                antCall.setAttribute(ATTR_TARGET, buildConfigTask
                        .getAttribute(ATTR_NAME));
            }
        }

        insertBuildBeforeThisElement.getParentNode().removeChild(
                insertBuildBeforeThisElement);
    }

    /**
     * Insert the comment text before the specified element.
     * 
     * @param element
     * @param commentString
     */
    private void insertCommentBefore(Element element, String commentString) {
        Document document = element.getOwnerDocument();

        Comment comment = document.createComment(commentString);
        document.insertBefore(comment, element);
    }

    /**
     * insert wtkpackage tasks into mtj build xml doc.
     * 
     * @param projectInfoMap
     */
    private void insertPackageTasks(
            Map<IJavaProject, ProjectInfo> projectInfoMap) {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        Element wtkPackageElement = findElement(documentElement,
                ELEM_WTKPACKAGE);

        Iterator<Map.Entry<IJavaProject, ProjectInfo>> iterator = projectInfoMap
                .entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<IJavaProject, ProjectInfo> entry = (Map.Entry<IJavaProject, ProjectInfo>) iterator
                    .next();
            ProjectInfo info = (ProjectInfo) entry.getValue();
            // Insert the fileset definitions for packaging from this project
            if (info.isExported()) {
                // Add the classpath reference
                Element classpathElement = newChildElement(wtkPackageElement,
                        ELEM_CLASSPATH);
                classpathElement.setAttribute(ATTR_REFID, info
                        .getClasspathElementId());

                Iterator<Element> filesetElements = info
                        .getPackageFilesetElements().iterator();
                while (filesetElements.hasNext()) {
                    Element element = (Element) filesetElements.next();
                    wtkPackageElement.appendChild(element);
                }
            }
        }
        Element packageTask = (Element) wtkPackageElement.getParentNode();
        for (Configuration config : midletSuite.getConfigurations()) {
            String configName = replaceSpace(config.getName());
            String buildTaskName = "-mtj-build-" + configName;
            Element packConfigElem = (Element) packageTask.cloneNode(true);
            // change attributes
            packConfigElem.setAttribute(ATTR_DEPENDS, buildTaskName);
            packConfigElem.setAttribute(ATTR_NAME, "-mtj-internal-package-"
                    + configName);
            // change fileset with dir = ${path.build.classes}
            NodeList filesets = packConfigElem
                    .getElementsByTagName(ELEM_FILESET);
            for (int i = 0; i < filesets.getLength(); i++) {
                Element fileset = (Element) filesets.item(i);
                if ("${path.build.classes}".equals(fileset
                        .getAttribute(ATTR_DIR))) {
                    fileset.setAttribute(ATTR_DIR, "${path.build.classes."
                            + configName + "}");
                    break;
                }
            }
            // change classpath
            NodeList classPathes = packConfigElem
                    .getElementsByTagName(ELEM_CLASSPATH);
            for (int i = 0; i < classPathes.getLength(); i++) {
                Element classpath = (Element) classPathes.item(i);
                String oldRefid = classpath.getAttribute(ATTR_REFID);
                String newRefid = oldRefid + "." + configName;
                classpath.setAttribute(ATTR_REFID, newRefid);
            }
            // change copy task
            Element copyTask = XMLUtils.getFirstElementWithTagName(
                    packConfigElem, ELEM_COPY);
            String tofile = "${path.build.output." + configName
                    + "}/${jad.name}";
            copyTask.setAttribute(ATTR_TOFILE, tofile);
            // change wtkpackage task
            Element wtkpackage = XMLUtils.getFirstElementWithTagName(
                    packConfigElem, ELEM_WTKPACKAGE);
            String jadfile = "${path.build.output." + configName
                    + "}/${midlet.name}.jad";
            String jarfile = "${path.build.output." + configName
                    + "}/${midlet.name}.jar";
            wtkpackage.setAttribute(ATTR_JADFILE, jadfile);
            wtkpackage.setAttribute(ATTR_JARFILE, jarfile);
            // change antcall task
            Element antcall = XMLUtils.getFirstElementWithTagName(
                    packConfigElem, ELEM_ANTCALL);
            antcall.setAttribute(ATTR_TARGET, "-mtj-obfuscate-" + configName);

            // appent packConfigElem to mtjBuildXmlDocument
            packageTask.getParentNode().insertBefore(packConfigElem,
                    packageTask.getNextSibling());
        }
        // add package antcall to root package task
        Element newRootPackageTask = (Element) packageTask.cloneNode(false);
        for (Configuration config : midletSuite.getConfigurations()) {

            String configName = replaceSpace(config.getName());
            String packageConfigTaskName = "-mtj-internal-package-"
                    + configName;
            Element antcallPack = newChildElement(newRootPackageTask,
                    ELEM_ANTCALL);
            antcallPack.setAttribute(ATTR_TARGET, packageConfigTaskName);
        }
        // replace root package task
        packageTask.getParentNode().replaceChild(newRootPackageTask,
                packageTask);

    }

    /**
     * Insert path elements into the exported mtj-build xml document.
     * 
     * @param pathElement
     * @param info
     */
    private void insertPathElements(
            Map<IJavaProject, ProjectInfo> projectInfoMap) {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        // must retrieve path element before insert any new path element.
        Element pathElement = findElement(documentElement, ELEM_PATH);

        Iterator<Map.Entry<IJavaProject, ProjectInfo>> iterator = projectInfoMap
                .entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<IJavaProject, ProjectInfo> entry = (Map.Entry<IJavaProject, ProjectInfo>) iterator
                    .next();
            ProjectInfo info = (ProjectInfo) entry.getValue();
            String pathID = info.getClasspathElementId();
            for (Configuration config : midletSuite.getConfigurations()) {
                String configName = replaceSpace(config.getName());
                Element classPathElem = info.getClasspathElement();
                Element cpElemForConfig = (Element) classPathElem
                        .cloneNode(true);
                cpElemForConfig
                        .setAttribute(ATTR_ID, pathID + "." + configName);

                String buildPathForConfig = "${path.build.classes."
                        + configName + "}";
                NodeList pathes = cpElemForConfig
                        .getElementsByTagName(ELEM_PATH);
                for (int i = 0; i < pathes.getLength(); i++) {
                    Element currentPathElem = (Element) pathes.item(i);
                    if (PATH_BUILD_CLASSES.equals(currentPathElem
                            .getAttribute(ATTR_LOCATION))) {
                        currentPathElem.setAttribute(ATTR_LOCATION,
                                buildPathForConfig);
                    } else if (PATH_BUILD_CLASSES_NO_EXPORT
                            .equals(currentPathElem.getAttribute(ATTR_LOCATION))) {
                        currentPathElem.setAttribute(ATTR_LOCATION,
                                buildPathForConfig + "/_no_export");
                    }
                }
                pathElement.getParentNode().insertBefore(cpElemForConfig,
                        pathElement);
            }
        }

        pathElement.getParentNode().removeChild(pathElement);
    }

    /**
     * Insert wtkobfuscate tasks into mtj build xml doc.
     */
    private void insertWtkobfuscateTasks() {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        Element wtkobfuscateElement = findElement(documentElement,
                ELEM_WTKOBFUSCATE);
        updateWtkObfuscateElement(wtkobfuscateElement);
        Element rootObfuscateElement = (Element) wtkobfuscateElement
                .getParentNode();
        for (Configuration config : midletSuite.getConfigurations()) {
            String configName = replaceSpace(config.getName());
            Element obfuscateConfigElem = (Element) rootObfuscateElement
                    .cloneNode(true);
            obfuscateConfigElem.setAttribute(ATTR_NAME, "-mtj-obfuscate-"
                    + configName);

            Element wtkobfuscate = XMLUtils.getFirstElementWithTagName(
                    obfuscateConfigElem, ELEM_WTKOBFUSCATE);
            String jadfile = "${path.build.output." + configName
                    + "}/${midlet.name}.jad";
            String jarfile = "${path.build.output." + configName
                    + "}/${midlet.name}.jar";
            wtkobfuscate.setAttribute(ATTR_JADFILE, jadfile);
            wtkobfuscate.setAttribute(ATTR_JARFILE, jarfile);

            Element wtkpreverify = XMLUtils.getFirstElementWithTagName(
                    obfuscateConfigElem, ELEM_WTKPREVERIFY);
            wtkpreverify.setAttribute(ATTR_JADFILE, jadfile);
            wtkpreverify.setAttribute(ATTR_JARFILE, jarfile);

            // append obfuscateConfigElem to mtjBuildXmlDocument
            rootObfuscateElement.getParentNode().insertBefore(
                    obfuscateConfigElem, rootObfuscateElement.getNextSibling());
        }
        // add obfuscate allcall to rootObfuscateElement
        Element newRootObfuscateElement = (Element) rootObfuscateElement
                .cloneNode(false);
        for (Configuration config : midletSuite.getConfigurations()) {
            String configName = replaceSpace(config.getName());
            String obfuscateConfigTaksName = "-mtj-obfuscate-" + configName;

            Element antcallObfuscate = newChildElement(newRootObfuscateElement,
                    ELEM_ANTCALL);
            antcallObfuscate.setAttribute(ATTR_TARGET, obfuscateConfigTaksName);
        }
        // replace root package task
        rootObfuscateElement.getParentNode().replaceChild(
                newRootObfuscateElement, rootObfuscateElement);
    }

    /**
     * Create and return a new child element under the specified parent element.
     * 
     * @param parentElement
     * @param name
     * @return
     */
    private Element newChildElement(Element parentElement, String name) {
        Element element = parentElement.getOwnerDocument().createElement(name);
        parentElement.appendChild(element);

        return element;
    }

    /**
     * Read the build.xml template file.
     * 
     * @param is
     * @return
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    private StringBuffer readBuildXmlTemplate(InputStream is)
            throws UnsupportedEncodingException, IOException {
        // Read in the template
        int charsRead;
        char[] buffer = new char[1024];

        Reader reader = new InputStreamReader(is, "UTF-8");
        StringBuffer sb = new StringBuffer();
        while ((charsRead = reader.read(buffer)) != -1) {
            sb.append(buffer, 0, charsRead);
        }

        is.close();
        return sb;
    }

    /**
     * Read the template MTJ build file.
     * 
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    private Document readMTJBuildTemplate()
            throws ParserConfigurationException, SAXException, IOException {
        Document document = null;

        InputStream is = getClass().getResourceAsStream("mtj-build.xml");
        if (is == null) {
            throw new IOException("build.xml template not found");
        } else {
            // Read the document
            document = XMLUtils.readDocument(is);

            // Alter the "project" element's name attribute
            Element projectElement = document.getDocumentElement();
            projectElement.setAttribute(ATTR_NAME, "mtj-" + projectName);

            // Add some warning comments
            StringBuffer comment = new StringBuffer();
            comment.append("\n\tAutomatically generated by MTJ on ").append(
                    new Date()).append(
                    "\n\tDO NOT ALTER THIS FILE.  IT WILL "
                            + "BE OVERWRITTEN\n\n").append(
                    "\tChanges may be made to build.xml and "
                            + "user-build.properties\n");
            insertCommentBefore(projectElement, comment.toString());
        }

        return document;
    }

    /**
     * Replace space with underscore.
     * 
     * @param string
     * @return
     */
    private String replaceSpace(String string) {
        if (string == null) {
            return "";
        }
        return string.replace(' ', '_');
    }

    /**
     * Replace the build xml template values.
     * 
     * @param sb
     * @param props
     */
    private void replaceTemplateValues(StringBuffer sb, Properties props) {
        // Replace template values
        int offset = 0;
        Matcher matcher = SUBSTITUTION_PATTERN.matcher(sb);

        while (matcher.find(offset)) {
            String referencedValue = matcher.group(1);
            String resolvedValue = props.getProperty(referencedValue,
                    referencedValue);
            sb.replace(matcher.start(), matcher.end(), resolvedValue);

            // Figure out the new offset, based on the replaced
            // string length
            offset = matcher.start() + resolvedValue.length();
        }
    }

    /**
     * Each configuration should have its own bootclasspath.
     * 
     * @param props
     */
    private void setBootclasspathForConfigs(Properties props) {
        for (Configuration config : midletSuite.getConfigurations()) {
            props.setProperty(
                    "bootclasspath." + replaceSpace(config.getName()), config
                            .getDevice().getClasspath().toString());
        }
    }

    /**
     * Each configuration should have its own build output folder.
     * 
     * @param props
     */
    private void setClassesFolderForConfigs(Properties props) {
        for (Configuration config : midletSuite.getConfigurations()) {
            String safeConfigName = replaceSpace(config.getName());
            props.setProperty("path.build.classes." + safeConfigName,
                    "${basedir}/build/classes/" + safeConfigName);
        }
    }

    /**
     * Each configuration should have its own deployment folder.
     * 
     * @param props
     */
    private void setDeployFolderForConfigs(Properties props) {
        for (Configuration config : midletSuite.getConfigurations()) {
            String safeConfigName = replaceSpace(config.getName());
            props.setProperty("path.build.output." + safeConfigName,
                    "${basedir}/" + MTJCorePlugin.getDeploymentDirectoryName()
                            + "/" + safeConfigName);
        }
    }

    /**
     * Each configuration should have its own prepross output folder.
     * 
     * @param props
     */
    private void setPreprocessOutputFolderForConfigs(Properties props) {
        for (Configuration config : midletSuite.getConfigurations()) {
            String safeConfigName = replaceSpace(config.getName());
            props.setProperty("path.preprocess.output." + safeConfigName,
                    "${basedir}/preprocessOutput/" + safeConfigName);
        }
    }

    /**
     * Traverse the classpath and update the build information along the way.
     * 
     * @throws CoreException
     */
    private BuildClasspathEntryVisitor traverseClasspath(
            IProgressMonitor monitor) throws CoreException {
        // Use a classpath visitor to build up the build information
        BuildClasspathEntryVisitor visitor = new BuildClasspathEntryVisitor(
                javaProject);
        visitor.getRunner(true).run(javaProject, visitor, monitor);

        return visitor;
    }

    /**
     * Insert project.root.&lt;PROJECT_NAME&gt; property entries into build
     * properties.
     * 
     * @param projectInfoMap
     */
    private void updateBuildProperties(
            Map<IJavaProject, ProjectInfo> projectInfoMap) {
        Iterator<Map.Entry<IJavaProject, ProjectInfo>> iterator = projectInfoMap
                .entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<IJavaProject, ProjectInfo> entry = (Map.Entry<IJavaProject, ProjectInfo>) iterator
                    .next();
            ProjectInfo info = (ProjectInfo) entry.getValue();

            // Set the property for the root of the project
            String propertyValue = info.getAntProjectPropertyValue();
            propertyValue = (propertyValue.length() == 0) ? "${basedir}"
                    : "${basedir}" + propertyValue;
            buildProperties.setProperty(info.getAntProjectPropertyName(),
                    propertyValue);
        }
    }

    /**
     * Update the mtj build file based on the classpath information collected
     * during traversal.
     * 
     * @param projectInfoMap
     */
    private void updateMTJBuildXml(Map<IJavaProject, ProjectInfo> projectInfoMap) {

        // updata "-mtj-initialize" target
        updateMtjInitializeTarget();
        // update wtkmakeprc target
        updateWtkmakeprcTarget();
        // Insert the project's wtkbuild calls
        insertBuildTasks(projectInfoMap);
        // Insert package tasks
        insertPackageTasks(projectInfoMap);
        // Insert this project's classpath path definition
        insertPathElements(projectInfoMap);
        // update build properties
        updateBuildProperties(projectInfoMap);
        // Update the wtkobfuscate parameters
        insertWtkobfuscateTasks();
        // update wtkrun target
        updateWtkrunTarget();

    }

    /**
     * Update "-mtj-initialize" target in mtj build xml doc.
     */
    private void updateMtjInitializeTarget() {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        NodeList targets = documentElement.getElementsByTagName(ELEM_TARGET);
        for (int i = 0; i < targets.getLength(); i++) {
            Element element = (Element) targets.item(i);
            if ("-mtj-initialize".equals(element.getAttribute(ATTR_NAME))) {
                for (Configuration config : midletSuite.getConfigurations()) {
                    String configName = replaceSpace(config.getName());
                    Element mkdir = newChildElement(element, ELEM_MKDIR);
                    mkdir.setAttribute(ATTR_DIR, "${path.build.classes."
                            + configName + "}/_no_export");
                }
                break;
            }
        }
    }

    /**
     * Update wtkmakeprc target in mtj build xml doc.
     */
    private void updateWtkmakeprcTarget() {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        NodeList targets = documentElement.getElementsByTagName(ELEM_TARGET);
        for (int i = 0; i < targets.getLength(); i++) {
            Element element = (Element) targets.item(i);
            if ("-mtj-makeprc".equals(element.getAttribute(ATTR_NAME))) {
                for (Configuration config : midletSuite.getConfigurations()) {
                    String configName = replaceSpace(config.getName());
                    String jadfile = "${path.build.output." + configName
                            + "}/${midlet.name}.jad";
                    String prcfile = "${path.build.output." + configName
                            + "}/${midlet.name}.prc";
                    Element wtkmakeprc = XMLUtils.createChild(element,
                            ELEM_WTKMAKEPRC);
                    wtkmakeprc.setAttribute(ATTR_JADFILE, jadfile);
                    wtkmakeprc.setAttribute(ATTR_PRCFILE, prcfile);
                }
                break;
            }
        }
    }

    /**
     * Update the WTK obfuscate element to include the parameters.
     * 
     * @param wktobfuscateElement
     */
    private void updateWtkObfuscateElement(Element wktobfuscateElement) {
        String[] keepExpressions = PreferenceAccessor.instance
                .getProguardKeepExpressions(midletSuite.getProject());

        for (String keepExpression : keepExpressions) {
            StringBuffer sb = new StringBuffer("'-keep ");
            sb.append(keepExpression).append("'");

            addWtkObfuscateArgument(wktobfuscateElement, sb.toString());
        }

        addWtkObfuscateArgument(wktobfuscateElement, getProguardOptions());
    }

    /**
     * Update wtkrun target in mtj build xml doc.
     */
    private void updateWtkrunTarget() {
        Element documentElement = mtjBuildXmlDocument.getDocumentElement();
        Element runElement = findElement(documentElement, ELEM_WTKRUN);
        String activeConfigName = replaceSpace(midletSuite.getConfigurations()
                .getActiveConfiguration().getName());
        String jadfile = "${path.build.output." + activeConfigName
                + "}/${midlet.name}.jad";
        runElement.setAttribute(ATTR_JADFILE, jadfile);
    }

    /**
     * Validate that the antenna property is valid.
     * 
     * @param monitor
     * @throws AntennaExportException
     */
    private void validateAntenna(IProgressMonitor monitor)
            throws AntennaExportException {
        boolean valid = false;

        Preferences prefs = MTJCorePlugin.getDefault().getPluginPreferences();
        String antennaPref = prefs
                .getString(IMTJCoreConstants.PREF_ANTENNA_JAR);
        File antennaFile = new File(antennaPref);
        if (antennaFile.exists()) {
            try {
                ZipFile zipFile = new ZipFile(antennaFile);
                ZipEntry entry = zipFile.getEntry("antenna.properties");
                valid = (entry != null);
                zipFile.close();
            } catch (IOException e) {
                // Assume these cases mean the file is invalid
                MTJCorePlugin.log(IStatus.WARNING,
                        "Error testing Antenna settings", e);
            }
        }

        if (!valid) {
            throw new AntennaExportException(
                    "Antenna library not found or not valid.\nCheck the "
                            + "preference settings.");
        }
    }

    /**
     * Validate the environment prior to the export. Throw an exception if the
     * environment is not valid.
     * 
     * @param monitor
     * @throws AntennaExportException if the environment is not valid
     */
    private void validateEnvironment(IProgressMonitor monitor)
            throws AntennaExportException {
        validateAntenna(monitor);
        validateWTK(monitor);
    }

    /**
     * Validate that a Sun WTK can be found.
     * 
     * @param monitor
     * @throws AntennaExportException
     */
    private void validateWTK(IProgressMonitor monitor)
            throws AntennaExportException {
        File wtkRoot = getWTKRoot();
        if ((wtkRoot == null) || !wtkRoot.exists()) {
            throw new AntennaExportException(
                    "Sun WTK not found.\nCheck the platform definition settings.");
        }
    }

    /**
     * Write out the build properties file.
     * 
     * @param monitor
     * @throws IOException
     * @throws CoreException
     */
    private void writeBuildProperties(IProgressMonitor monitor)
            throws IOException, CoreException {
        File buildPropsFile = getMidletSuiteFile("mtj-build.properties",
                monitor);
        FileOutputStream fos = new FileOutputStream(buildPropsFile);
        buildProperties.store(fos,
                " MTJ Build Properties - DO NOT ALTER THIS FILE - "
                        + "Make changes in user-build.properties");
        fos.close();
    }

    /**
     * Write the MTJ build.xml file.
     * 
     * @throws TransformerException
     * @throws IOException
     * @throws CoreException
     */
    private void writeMTJBuildXml(IProgressMonitor monitor)
            throws TransformerException, IOException, CoreException {
        // Write the output
        File buildXmlFile = getMidletSuiteFile("mtj-build.xml", monitor);
        XMLUtils.writeDocument(buildXmlFile, mtjBuildXmlDocument);
    }
}