/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.core.model.configuration;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mtj.core.internal.utils.XMLUtils;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSetRegistry;
import org.eclipse.mtj.core.model.preprocessor.symbol.Symbol;
import org.eclipse.mtj.core.model.preprocessor.symbol.SymbolSet;
import org.eclipse.mtj.core.model.preprocessor.symbol.SymbolSetFactory;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class contains configuration information for multi-configuration
 * support. Now it contains "device" and "symbol set" information, and a boolean
 * "active" to indicate if this configuration is the current active one. This
 * class may contains more information if need in future.
 * 
 * @author wangf
 */
public class Configuration {
    // The elements and attributes in the meta data xml file.
    public static final String ATTR_NAME = "name";
    public static final String ATTR_ACTIVE = "active";
    public static final String ELEM_DEVICE = "device";
    public static final String ATTR_DEVICEGROUP = "group";
    public static final String ATTR_DEVICENAME = "name";
    public static final String ELEM_SYMBOL_SET = "symbolSet";
    public static final String ELEM_WORKSPACE_SYMBOLSET = "workspaceSymbolSet";
    public static final String ELEM_SYMBOL = "symbol";
    public static final String ATTR_VALUE = "value";
    /**
     * The name of the configuration, should be unique in project scope.
     */
    private String name;
    /**
     * The boolean value to indicate if this configuration is the current active
     * one.
     */
    private boolean active;
    /**
     * The device of the configuration.
     */
    private IDevice device;
    /**
     * The symbols of the configuration. For preprocessing support.
     */
    private SymbolSet symbolSet;
    /**
     * SymbolSets from workspace scope.
     */
    private List<SymbolDefinitionSet> workspaceScopeSymbolSets;
    /**
     * The listeners listen to this configuration properties change event. <br>
     * <b>Note:</b>Since instance of configuration have a long life cycle (as
     * long as the Midlet project), so we should remove listener manually when
     * it no longer used.
     */
    private List<IConfigurationChangeListener> listeners = new ArrayList<IConfigurationChangeListener>();

    /**
     * Construct Configuration from meta data XML file.
     * 
     * @param configElement - The &#60configuration&#62 xml element.
     * @throws PersistenceException
     */
    public Configuration(Element configElement) throws PersistenceException {
        name = configElement.getAttribute(ATTR_NAME);
        active = Boolean.valueOf(configElement.getAttribute(ATTR_ACTIVE));
        loadDevice(configElement);
        loadSymbolSet(configElement);
        loadWorkspaceSymbolSets(configElement);
    }

    public Configuration(String name) {
        this.name = name;
    }

    public void addConfigChangeListener(IConfigurationChangeListener listener) {
        listeners.add(listener);
    }

    /**
     * If name equals, then configuration equals.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Configuration other = (Configuration) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    public void fireSymbolSetChanged() {
        for (IConfigurationChangeListener listener : listeners) {
            listener.symbolSetChanged();
        }
    }

    public IDevice getDevice() {
        return device;
    }

    public String getName() {
        return name;
    }

    /**
     * All preprocessing related code should NOT use this method to get
     * SymbolSet, instead, getSymbolSetForPreprocessing() should be used for
     * preprocessing purpose.
     * 
     * @return
     */
    public SymbolSet getSymbolSet() {
        if (symbolSet == null) {
            symbolSet = new SymbolSet();
        }
        return symbolSet;
    }

    /**
     * All preprocessing related function should use this method to get
     * SymbolSet. This method return a SymbolSet for preprocessing using. The
     * returned SymbolSet contains one more Symbol (whose key =
     * this.SYMBOLKEY_CONFIG_NAME, value=this.getName()) than SymbolSet returned
     * by this.getSymbolSet().
     * 
     * @return
     */
    public SymbolSet getSymbolSetForPreprocessing() {
        SymbolSet symbolSetForPreprocessing = new SymbolSet();
        symbolSetForPreprocessing.addAll(symbolSet);
        String configName = getName().replace(' ', '_');
        configName = SymbolSetFactory.replaceFirstNonLetterChar(configName);
        Symbol configSymbol = new Symbol(configName, "true");
        configSymbol.setType(Symbol.TYPE_CONFIG);
        symbolSetForPreprocessing.add(configSymbol);
        return symbolSetForPreprocessing;
    }

    public List<SymbolDefinitionSet> getWorkspaceScopeSymbolSets() {
        if (workspaceScopeSymbolSets == null) {
            workspaceScopeSymbolSets = new ArrayList<SymbolDefinitionSet>();
        }
        return workspaceScopeSymbolSets;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public boolean isActive() {
        return active;
    }

    /**
     * Load device from xml.
     * 
     * @param configElement
     * @throws PersistenceException
     */
    private void loadDevice(Element configElement) throws PersistenceException {
        Element deviceElement = XMLUtils.getFirstElementWithTagName(
                configElement, ELEM_DEVICE);
        if (deviceElement != null) {
            String deviceGroup = deviceElement.getAttribute(ATTR_DEVICEGROUP);
            String deviceName = deviceElement.getAttribute(ATTR_DEVICENAME);
            device = DeviceRegistry.singleton
                    .getDevice(deviceGroup, deviceName);
        }
    }

    /**
     * Load symbols from xml.
     * 
     * @param configElement
     */
    private void loadSymbolSet(Element configElement) {
        symbolSet = getSymbolSet();
        Element symbolSetElement = XMLUtils.getFirstElementWithTagName(
                configElement, ELEM_SYMBOL_SET);
        NodeList symbols = symbolSetElement.getElementsByTagName(ELEM_SYMBOL);
        for (int i = 0; i < symbols.getLength(); i++) {
            Element symbolElement = (Element) symbols.item(i);
            String symbolName = symbolElement.getAttribute(ATTR_NAME);
            String symbolValue = symbolElement.getAttribute(ATTR_VALUE);
            Symbol symbol = new Symbol(symbolName, symbolValue);
            symbolSet.add(symbol);
        }
    }

    private void loadWorkspaceSymbolSets(Element configElement)
            throws PersistenceException {
        NodeList symbolSets = configElement
                .getElementsByTagName(ELEM_WORKSPACE_SYMBOLSET);
        if ((symbolSets == null) || (symbolSets.getLength() == 0)) {
            return;
        }
        workspaceScopeSymbolSets = new ArrayList<SymbolDefinitionSet>();
        for (int i = 0; i < symbolSets.getLength(); i++) {
            Element symbolSet = (Element) symbolSets.item(i);
            String symbolSetName = symbolSet.getAttribute(ATTR_NAME);
            SymbolDefinitionSet definitionSet = SymbolDefinitionSetRegistry.singleton
                    .getSymbolDefinitionSet(symbolSetName);
            if (definitionSet != null) {
                workspaceScopeSymbolSets.add(definitionSet);
            }
        }
    }

    /**
     * <b>Note:</b>Since instance of configuration have a long life cycle (as
     * long as the Midlet project), so we should remove listener manually when
     * it no longer used.
     * 
     * @param listener
     */
    public void removeConfigChangeListener(IConfigurationChangeListener listener) {
        listeners.remove(listener);
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setDevice(IDevice device) {
        // If device is the same object, just return
        if (this.device == device) {
            return;
        }
        IDevice oldDevice = this.device;
        this.device = device;
        // notify listeners
        ConfigDeviceChangeEvent event = new ConfigDeviceChangeEvent(this,
                oldDevice, device);
        for (IConfigurationChangeListener listener : listeners) {
            listener.deviceChanged(event);
        }

    }

    public void setName(String name) {
        if (this.name.equals(name)) {
            return;
        }
        String oldName = this.name;
        this.name = name;
        // notify listeners
        ConfigNameChangeEvent event = new ConfigNameChangeEvent(this, oldName,
                name);
        for (IConfigurationChangeListener listener : listeners) {
            listener.nameChanged(event);
        }

    }

    public void setSymbolSet(SymbolSet symbolSet) {
        this.symbolSet = symbolSet;
    }

    public void setWorkspaceScopeSymbolSets(List<SymbolDefinitionSet> symbolSets) {
        if (ConfigurationsUtils.workspaceSymbolsetsEquals(
                workspaceScopeSymbolSets, symbolSets)) {
            return;
        }
        List<SymbolDefinitionSet> oldSets = this.workspaceScopeSymbolSets;
        this.workspaceScopeSymbolSets = symbolSets;
        // notify listeners
        ConfigWorkspaceSymbolSetsChangeEvent event = new ConfigWorkspaceSymbolSetsChangeEvent(
                this, oldSets, symbolSets);
        for (IConfigurationChangeListener listener : listeners) {
            listener.workspaceScopeSymbolSetsChanged(event);
        }

    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer().append("name=").append(name)
                .append("|active=").append(active).append("|device=").append(
                        device).append("|symbolSet=").append(symbolSet);
        return sb.toString();
    }
}
