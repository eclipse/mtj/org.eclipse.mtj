/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.core.model.configuration;

import java.util.EventObject;

/**
 * ConfigNameChangeEvent is used to notify that the name of the configuration
 * changed.
 * 
 * @author wangf
 */
public class ConfigNameChangeEvent extends EventObject {
    private static final long serialVersionUID = 1L;
    private String oldConfigName;
    private String newCongigName;

    /**
     * Constructor.
     * 
     * @param source - The configuration has been changed.
     * @param oldConfigName - the old configuration name.
     * @param newCongigName - the new configuration name.
     */
    public ConfigNameChangeEvent(Configuration source, String oldConfigName,
            String newCongigName) {
        super(source);
        this.oldConfigName = oldConfigName;
        this.newCongigName = newCongigName;
    }

    public String getNewCongigName() {
        return newCongigName;
    }

    public String getOldConfigName() {
        return oldConfigName;
    }

    public void setNewCongigName(String newCongigName) {
        this.newCongigName = newCongigName;
    }

    public void setOldConfigName(String oldConfigName) {
        this.oldConfigName = oldConfigName;
    }

}
