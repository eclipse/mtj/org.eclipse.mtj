/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.internal.tools;

import org.eclipse.osgi.util.NLS;

/**
 * Provides convenience methods for manipulating messages.
 * 
 * @author Diego Madruga Sandin
 */
public class Messages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.core.internal.tools.messages"; //$NON-NLS-1$
    public static String Message_BuildConfiguration;

    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
