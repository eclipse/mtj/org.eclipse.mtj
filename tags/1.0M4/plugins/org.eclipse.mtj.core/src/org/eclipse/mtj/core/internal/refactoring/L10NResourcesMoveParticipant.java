/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Refreshing content upon move.                     
 */
package org.eclipse.mtj.core.internal.refactoring;

import java.io.IOException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.MoveArguments;
import org.eclipse.ltk.core.refactoring.participants.MoveParticipant;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.internal.core.l10n.L10nApi;
import org.eclipse.mtj.internal.core.l10n.Messages;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * L10NResourcesMoveParticipant identifies changes on the L10nResources class
 * file and updates the package name into the Localization Data file.
 * 
 * @since 0.9.1
 */
public class L10NResourcesMoveParticipant extends MoveParticipant {

    private IJavaElement oldParent;
    private IJavaProject jProject;

    /* (non-Javadoc)
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#initialize(java.lang.Object)
     */
    protected boolean initialize(Object element) {
        if (element instanceof ICompilationUnit) {
            ICompilationUnit compilationUnit = (ICompilationUnit) element;
            if (compilationUnit.getElementName().equals(L10nApi.L10N_RESOURCES_CLASS)) {
                this.jProject  = compilationUnit.getJavaProject();
                this.oldParent = compilationUnit.getParent();
                return true;
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#checkConditions(org.eclipse.core.runtime.IProgressMonitor, org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext)
     */
    public RefactoringStatus checkConditions(IProgressMonitor pm,
            CheckConditionsContext context) throws OperationCanceledException {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#createChange(org.eclipse.core.runtime.IProgressMonitor)
     */
    public Change createChange(IProgressMonitor pm) throws CoreException,
            OperationCanceledException {
        MoveArguments arguments = this.getArguments();
        Object destination = arguments.getDestination();
        if (destination instanceof IPackageFragment) {
            IPackageFragment packageFragment = (IPackageFragment) destination;
            IProject project = this.jProject.getProject();
            IFile locationsXml = project
                    .getFile(L10nApi.LOCALIZATION_DATA_FILE);
            if (locationsXml.exists()) {
                try {
                    IPath xmlPath = locationsXml.getLocation();
                    Document document = Utils.getXmlDocument(xmlPath);
                    Element root = document.getDocumentElement();
                    root.setAttribute("package", packageFragment
                            .getElementName());

                    TransformerFactory factory = TransformerFactory
                            .newInstance();
                    Transformer transformer = factory.newTransformer();
                    transformer.transform(new DOMSource(document),
                            new StreamResult(xmlPath.toFile()));
                } catch (IOException e) {
                    MTJCorePlugin.log(IStatus.ERROR,
                            Messages.L10nBuilder_ErrorParsingLocalizationData);
                } catch (Exception e) {
                    MTJCorePlugin.log(IStatus.ERROR, e);
                } finally {
                	this.jProject.getProject().refreshLocal(IProject.DEPTH_INFINITE, pm);
                }
            }
        }
        
        if (this.oldParent != null && this.oldParent instanceof IPackageFragment) {
            IPackageFragment pack = (IPackageFragment) this.oldParent;
            ICompilationUnit compilationUnit = pack.getCompilationUnit(L10nApi.L10N_CONSTANTS_CLASS);
            if (compilationUnit != null && compilationUnit.exists()) {
                compilationUnit.delete(true, pm);
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant#getName()
     */
    public String getName() {
        return "Localization Data Participant";
    }
}
