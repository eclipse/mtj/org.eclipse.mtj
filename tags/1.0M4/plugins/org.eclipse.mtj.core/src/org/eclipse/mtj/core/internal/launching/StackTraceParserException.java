/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.core.internal.launching;

/**
 * StackTraceParserException class wraps errors relative to
 * Stack Trace Parsing.
 */
public class StackTraceParserException extends Exception {

    private static final long serialVersionUID = -5729087182933761135L;

    /**
     * Creates a StackTraceParserException with the specified message.
     * 
     * @param message the error message.
     */
    public StackTraceParserException(String message) {
        super(message);
    }

    /**
     * Creates a StackTraceParserException with the specified throwable.
     * 
     * @param throwable instance.
     */
    public StackTraceParserException(Throwable throwable) {
        super(throwable);
    }
}
