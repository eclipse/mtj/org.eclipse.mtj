/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.core.model.configuration;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;
import org.eclipse.mtj.core.model.preprocessor.symbol.Symbol;
import org.eclipse.mtj.core.model.preprocessor.symbol.SymbolSet;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MetaData;

/**
 * Provides Configuration related utils.
 * 
 * @author wangf
 */
public class ConfigurationsUtils {
    /**
     * The same as
     * {@link ConfigurationsUtils#configurationsEuqals(Configurations, Configurations)}
     * , except ignore active configuration. This means, if two Configurations
     * are same except that active configuration are different, also return
     * true.
     * 
     * @param cs1
     * @param cs2
     * @return
     */
    public static boolean configsContentsEquals(Configurations cs1,
            Configurations cs2) {
        if (cs1.size() != cs2.size()) {
            return false;
        }
        for (Configuration cInCs1 : cs1) {
            int index = cs2.indexOf(cInCs1);
            if (index < 0) {
                return false;
            }
            Configuration cInCs2 = cs2.get(index);
            if (!deviceEquals(cInCs1, cInCs2)) {
                return false;
            }
            if (!workspaceSymbolsetsEquals(
                    cInCs1.getWorkspaceScopeSymbolSets(), cInCs2
                            .getWorkspaceScopeSymbolSets())) {
                return false;
            }
            if (!symbolSetEquals(cInCs1, cInCs2)) {
                return false;
            }

        }
        return true;
    }

    /**
     * Determine if two Configurations are totally equal.<br>
     * If<br>
     * 1, two Configurations have same size and<br>
     * 2, both contains configuration with same name and<br>
     * 3, configuration with the same name have same device and same symbols,<br>
     * this method will return true and<br>
     * 4, two Configurations have same active configuration.<br>
     * In which same symbols means two SymbolSet have same size, and both have
     * symbols with the same name, and symbols with the same name have same
     * value.
     * 
     * @param cs1
     * @param cs2
     * @return
     */
    public static boolean configurationsEuqals(Configurations cs1,
            Configurations cs2) {
        if (!configsContentsEquals(cs1, cs2)) {
            return false;
        }
        if (!cs1.getActiveConfiguration().equals(cs2.getActiveConfiguration())) {
            return false;
        }
        return true;
    }

    /**
     * Determine if devices in two Configurations is the same instance.
     * 
     * @param cInCs1
     * @param cInCs2
     * @return
     */
    private static boolean deviceEquals(Configuration cInCs1,
            Configuration cInCs2) {
        if (cInCs1.getDevice() == cInCs2.getDevice()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * If Configurations have any modification that not save to metadata file,
     * return true.
     * 
     * @param midletProject
     * @return
     */
    public static boolean isConfigsDirty(IMidletSuiteProject midletProject) {
        Configurations configsInMemory = midletProject.getConfigurations();
        Configurations configsInMetadataFile = new MetaData(midletProject
                .getProject()).getConfigurations();
        boolean isDirty = !configurationsEuqals(configsInMemory,
                configsInMetadataFile);
        return isDirty;
    }

    /**
     * If you only change active configuration(any other contents not change)
     * and not save it to metadata file, return true.
     * 
     * @param midletProject
     * @return
     */
    public static boolean isOnlyActiveConfigDirty(
            IMidletSuiteProject midletProject) {
        Configurations configsInMemory = midletProject.getConfigurations();
        Configurations configsInMetadataFile = new MetaData(midletProject
                .getProject()).getConfigurations();
        boolean same = configsContentsEquals(configsInMemory,
                configsInMetadataFile);
        return same;
    }

    /**
     * If<br>
     * 1, two SymbolSet have the same size and<br>
     * 2, both SymbolSet contains the symbol with a certain name and<br>
     * 3, two symbol with the same name have the same value,<br>
     * return true.
     * 
     * @param cInCs1
     * @param cInCs2
     * @return
     */
    private static boolean symbolSetEquals(Configuration cInCs1,
            Configuration cInCs2) {
        SymbolSet symbolSet1 = cInCs1.getSymbolSet();
        SymbolSet symbolSet2 = cInCs2.getSymbolSet();
        if (!symbolSet1.equals(symbolSet2)) {
            return false;
        }
        List<Symbol> symbolList1 = new ArrayList<Symbol>(symbolSet1);
        List<Symbol> symbolList2 = new ArrayList<Symbol>(symbolSet2);
        for (Symbol sInList1 : symbolList1) {
            int index = symbolList2.indexOf(sInList1);
            if (index < 0) {
                return false;
            }
            Symbol sInList2 = symbolList2.get(index);
            if (!sInList1.getValue().equals(sInList2.getValue())) {
                return false;
            }
        }
        return true;
    }

    public static boolean workspaceSymbolsetsEquals(
            List<SymbolDefinitionSet> sets1, List<SymbolDefinitionSet> sets2) {
        if ((sets1 == null) && (sets2 == null)) {
            return true;
        }
        if ((sets1 == null) && (sets2 != null)) {
            return false;
        }
        if ((sets1 != null) && (sets2 == null)) {
            return false;
        }
        if (sets1.size() != sets2.size()) {
            return false;
        }
        for (SymbolDefinitionSet s : sets1) {
            if (!sets2.contains(s)) {
                return false;
            }
        }
        return true;
    }
}
