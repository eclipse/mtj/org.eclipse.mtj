/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Handling the case that there is no valid preverifier                           
 */
package org.eclipse.mtj.core.internal.preverification.builder;

import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaModelMarker;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.BuildLoggingConfiguration;
import org.eclipse.mtj.core.console.BuildConsoleProxy;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.preverifier.PreverificationUtils;
import org.eclipse.mtj.core.internal.utils.AbstractClasspathEntryVisitor;
import org.eclipse.mtj.core.internal.utils.FilteringClasspathEntryVisitor;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.model.preverifier.PreverifierNotFoundException;
import org.eclipse.mtj.preverifier.results.IClassErrorInformation;
import org.eclipse.mtj.preverifier.results.IFieldErrorInformation;
import org.eclipse.mtj.preverifier.results.IMethodErrorInformation;
import org.eclipse.mtj.preverifier.results.PreverificationError;
import org.eclipse.mtj.preverifier.results.PreverificationErrorLocationType;
import org.objectweb.asm.Type;

import de.schlichtherle.io.File;

/**
 * A build helper that builds an individual project's resource delta.
 * 
 * @author Craig Setera
 */

class ResourceDeltaBuilder {
    private static final String MANIFEST_FILE_NAME = "META-INF"
	    + java.io.File.separator + "MANIFEST.MF";

    /**
     * Implementation of a resource delta visitor for the preverification
     * builder.
     */
    private class ResourceDeltaVisitor implements IResourceDeltaVisitor {
	private IProgressMonitor monitor;
	private IFile jadFile;
	private IPath[] libraryPaths;
	private List<IResource> addedOrChangedClasses;

	/**
	 * Constructor
	 * 
	 * @param monitor
	 * @throws CoreException
	 */
	ResourceDeltaVisitor(IProgressMonitor monitor) throws CoreException {
	    this.monitor = monitor;
	    addedOrChangedClasses = new ArrayList<IResource>();
	    removedClasses = new ArrayList<IResource>();

	    jadFile = buildInfo.getMidletSuite().getApplicationDescriptorFile();
	    libraryPaths = collectLibraryFolderPaths(monitor);
	}

	/**
	 * @see org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.core.resources.IResourceDelta)
	 */
	public boolean visit(IResourceDelta delta) throws CoreException {
	    IResource resource = delta.getResource();
	    if (resource.getType() == IResource.FILE) {
		if (resource.getName().equals(".classpath")) {
		    buildInfo.setClasspathChanged(true);
		} else if (resource.equals(jadFile)) {
		    PreverificationBuilder.generateDeployedManifest(buildInfo
			    .getMidletSuite(), monitor);
		    buildInfo.setPackageDirty(true);
		} else {
		    String fileExtension = resource.getFileExtension();

		    if ((fileExtension != null)
			    && fileExtension.equals("class")) {
			handleClassDelta(delta, resource);
		    } else {
			handleNonClassResource(delta, resource);
		    }
		}
	    }

	    return true;
	}

	/**
	 * Get the list of added or changed classes that were collected.
	 * 
	 * @return
	 */
	public List<IResource> getAddedOrChangedClasses() {
	    return addedOrChangedClasses;
	}

	/**
	 * Get the list of removed classes that were collected.
	 * 
	 * @return
	 */
	public List<IResource> getRemovedClasses() {
	    return removedClasses;
	}

	/**
	 * Handle a change in a class.
	 * 
	 * @param delta
	 * @param resource
	 */
	private void handleClassDelta(IResourceDelta delta, IResource resource) {
	    // If this is a library class, it will be handled elsewhere
	    if (!isLibraryResource(libraryPaths, resource)) {
		switch (delta.getKind()) {
		case IResourceDelta.ADDED:
		case IResourceDelta.CHANGED:
		    if (buildInfo.isOutputResource(resource)) {
			// Batch up class updates so they can be verified
			// in one go
			addedOrChangedClasses.add(resource);
		    }
		    break;

		case IResourceDelta.REMOVED:
		    removedClasses.add(resource);
		    break;
		}
	    }
	}

	/**
	 * Handle a change in a resource.
	 * 
	 * @param delta
	 * @param resource
	 * @throws JavaModelException
	 * @throws CoreException
	 */
	private void handleNonClassResource(IResourceDelta delta,
		IResource resource) throws CoreException {
	    if (resource.getType() == IResource.FILE) {
		IFile file = (IFile) resource;

		switch (delta.getKind()) {
		case IResourceDelta.ADDED:
		case IResourceDelta.CHANGED:
		    handleNonClassAddOrChange(file, monitor);
		    break;

		case IResourceDelta.REMOVED: {
		    IPath relativePath = Utils
			    .extractsSourceFolderRelativePath(buildInfo
				    .getCurrentJavaProject(), resource);
		    removeFileFromRuntimeJar(relativePath, monitor);

		}
		    removeVerifiedResource(buildInfo.getCurrentJavaProject(),
			    buildInfo.getVerifiedClassesFolder(monitor), file,
			    monitor);
		    break;
		}
	    }
	}
    }

    /**
     * Classpath entry visitor to collect up the libraries.
     */
    private static class LibraryCollectionVisitor extends
	    FilteringClasspathEntryVisitor {
	private ArrayList<IClasspathEntry> libraryEntries;

	/** Construct a new instance */
	private LibraryCollectionVisitor() {
	    libraryEntries = new ArrayList<IClasspathEntry>();
	}

	/**
	 * @return Returns the libraryEntries.
	 */
	public ArrayList<IClasspathEntry> getLibraryEntries() {
	    return libraryEntries;
	}

	/**
	 * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitLibraryEntry(org.eclipse.jdt.core.IClasspathEntry,
	 *      org.eclipse.jdt.core.IJavaProject,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void visitLibraryEntry(IClasspathEntry entry,
		IJavaProject javaProject, IProgressMonitor monitor)
		throws CoreException {
	    if (isLibraryExported(entry)) {
		libraryEntries.add(entry);
	    }
	}
    }

    /**
     * Classpath entry visitor to collect up the output locations in a java
     * project.
     */
    private static class OutputLocationsCollectionVisitor extends
	    AbstractClasspathEntryVisitor {
	private Set<IPath> outputLocations;

	private OutputLocationsCollectionVisitor() {
	    outputLocations = new HashSet<IPath>();
	}

	/**
	 * @return Returns the outputLocations.
	 */
	public Set<IPath> getOutputLocations() {
	    return outputLocations;
	}

	/**
	 * @see org.eclipse.mtj.core.internal.utils.IClasspathEntryVisitor#visitSourceEntry(org.eclipse.jdt.core.IClasspathEntry,
	 *      org.eclipse.jdt.core.IJavaProject,
	 *      org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void visitSourceEntry(IClasspathEntry entry,
		IJavaProject javaProject, IProgressMonitor monitor)
		throws CoreException {
	    IPath outputLocation = entry.getOutputLocation();
	    if (outputLocation == null) {
		outputLocation = javaProject.getOutputLocation();
	    }

	    outputLocations.add(outputLocation);
	}
    }

    private BuildLoggingConfiguration buildLoggingConfig;

    // Build information
    private BuildInfo buildInfo;

    // Collections captured due to build
    private List<IResource> removedClasses;

    // Shortcuts to resources
    private IWorkspaceRoot workspaceRoot;

    /**
     * Construct a new Resource Delta Builder instance.
     * 
     * @param buildInfo
     */
    public ResourceDeltaBuilder(BuildInfo buildInfo) {
	this.buildInfo = buildInfo;
	buildLoggingConfig = BuildLoggingConfiguration.instance;
	workspaceRoot = MTJCorePlugin.getWorkspace().getRoot();
    }

    /**
     * Do the build for the project and resource delta specified in the build
     * info.
     * 
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    void build(IProgressMonitor monitor) throws CoreException,
	    PreverifierNotFoundException {
	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance.traceln("> ResourceDeltaBuilder.build");
	}

	// If this is a full build, start by clearing the output
	// directory
	if (buildInfo.getBuildKind() == IncrementalProjectBuilder.FULL_BUILD) {
	    // Only clear for the midlet suite project and not prereqs
	    if (buildInfo.isCurrentProjectMidletSuite()) {
		if (buildInfo.areClassesPreverified()) {
		    if (buildLoggingConfig.isPreverifierTraceEnabled()) {
			BuildConsoleProxy.instance
				.traceln("> ResourceDeltaBuilder.build; clearing "
					+ buildInfo
						.getVerifiedClassesFolder(monitor));
		    }

		    clearContainer(buildInfo, buildInfo
			    .getVerifiedClassesFolder(monitor), monitor);
		}

		if (buildInfo.areLibrariesPreverified()) {
		    if (buildLoggingConfig.isPreverifierTraceEnabled()) {
			BuildConsoleProxy.instance
				.traceln("> ResourceDeltaBuilder.build; clearing "
					+ buildInfo
						.getVerifiedLibsFolder(monitor));
		    }

		    clearContainer(buildInfo, buildInfo
			    .getVerifiedLibsFolder(monitor), monitor);
		}
	    }
	}

	// Handle the actual resource changes
	IResourceDelta resourceDelta = buildInfo.getCurrentResourceDelta();
	if (resourceDelta == null) {
	    // Since we don't know... Assume the classpath has changed
	    // when no resource delta exists
	    buildInfo.setClasspathChanged(true);
	    handleNullDelta(monitor);
	} else {
	    handleResourceDelta(monitor);
	}

	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance.traceln("< ResourceDeltaBuilder.build");
	}
    }

    /**
     * Preverify any libraries that exist in the build path that may be out of
     * date.
     * 
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    void preverifyLibraries(IProgressMonitor monitor) throws CoreException,
	    PreverifierNotFoundException {
	IFolder verifiedClassesFolder = buildInfo
		.getVerifiedClassesFolder(monitor);
	IFolder verifiedLibsFolder = buildInfo.getVerifiedLibsFolder(monitor);

	// Walk through the classpath, looking for jars that need to
	// be preverified
	LibraryCollectionVisitor visitor = new LibraryCollectionVisitor();
	visitor.getRunner().run(buildInfo.getCurrentJavaProject(), visitor,
		monitor);
	List<IClasspathEntry> resolvedEntries = visitor.getLibraryEntries();

	Iterator<IClasspathEntry> iter = resolvedEntries.iterator();
	while (iter.hasNext()) {
	    IClasspathEntry entry = (IClasspathEntry) iter.next();

	    // Figure out the source resource. If this is outside
	    // the workspace, set it to the project so we have
	    // something to attach an error marker to.
	    Object resolvedEntry = Utils.getResolvedClasspathEntry(entry);
	    IResource srcResource = (resolvedEntry instanceof IResource) ? (IResource) resolvedEntry
		    : buildInfo.getCurrentJavaProject().getProject();

	    // Convert to a java.io.File
	    IPath srcPath = entry.getPath().makeAbsolute();
	    File srcFile = new File(Utils.getResolvedClasspathEntryFile(entry));

	    // Check to see if there has been an attempt to preverify the file
	    if ((srcFile != null)
		    && !buildInfo.hasLibraryBeenPreverified(srcFile)) {
		// Log that we tried to preverify this file
		buildInfo.addPreverifiedLibrary(srcFile);

		// Look up the appropriate target folder and resource
		IResource target = null;
		IFolder targetFolder = null;
		if (srcFile.isDirectory() && !srcFile.isArchive()) {
		    target = verifiedClassesFolder;
		    targetFolder = verifiedClassesFolder;
		} else {
		    target = verifiedLibsFolder.getFile(srcPath.lastSegment());
		    targetFolder = verifiedLibsFolder;
		}

		File tgtFile = new File(target.getLocation().toFile());

		// Determine whether or not the library needs preverification
		boolean requiresPreverification = false;
		if (srcFile.isDirectory()) {
		    requiresPreverification = doClassesRequirePreverification(
			    srcFile, tgtFile);
		} else {
		    requiresPreverification = isSourceNewerThanTarget(srcFile,
			    tgtFile);
		}

		if (requiresPreverification) {
		    attemptLibraryPreverification(srcResource, srcFile,
			    targetFolder, monitor);
		}
	    }
	}
    }

    /**
     * Return a boolean indicating whether the specified resource is being held
     * within a library path.
     * 
     * @param resource
     * @return
     */
    private boolean isLibraryResource(IPath[] libraryPaths, IResource resource) {
	boolean isLibraryClass = false;

	for (int i = 0; i < libraryPaths.length; i++) {
	    IPath libraryPath = libraryPaths[i];
	    if (libraryPath.isPrefixOf(resource.getFullPath())) {
		isLibraryClass = true;
		break;
	    }
	}
	return isLibraryClass;
    }

    /**
     * Return a boolean indicating whether the source file is newer than the
     * target file.
     * 
     * @param srcFile
     * @param tgtFile
     * @return
     */
    private boolean isSourceNewerThanTarget(File srcFile, File tgtFile) {
	return !tgtFile.exists()
		|| (tgtFile.lastModified() < srcFile.lastModified());
    }

    /**
     * Add the specified file contents to the deployed jar file with the
     * relative path.
     * 
     * @param relativePath
     * @param contents
     * @throws CoreException
     */
    private void addFileToRuntimeJar(IPath relativePath, IFile contents,
	    IProgressMonitor monitor) throws CoreException {
	File deployedFile = new File(buildInfo.getRuntimeJarFile(monitor),
		relativePath.toString());
	deployedFile.copyFrom(contents.getLocation().toFile());
    }

    /**
     * Attempt to preverify the specified library.
     * 
     * @param srcResource
     * @param srcFile
     * @param verifiedLibsFolder
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    private void attemptLibraryPreverification(IResource srcResource,
	    File srcFile, IFolder verifiedLibsFolder, IProgressMonitor monitor)
	    throws CoreException, PreverifierNotFoundException {
	buildInfo.setPackageDirty(true);
	try {
	    // Run the preverifier
	    PreverificationError[] errors = buildInfo.getMidletSuite()
		    .preverifyJarFile(srcFile, verifiedLibsFolder, monitor);

	    // Handle errors that may have occurred.
	    // Should this actually be bubbled up as an exception?
	    if (errors.length > 0) {
		createJarErrorMarker(srcResource, srcFile, errors);
	    } else {
		File tgtFile = new File(verifiedLibsFolder.getLocation()
			.toString());

		// Deploy the preverified library contents into the deployed jar
		if (srcFile.isFile() || srcFile.isArchive()) {
		    // An archive file
		    tgtFile = new File(tgtFile, srcFile.getName());
		}

		// An actual directory of classes
		FileFilter filter = new FileFilter() {
		    public boolean accept(java.io.File pathname) {
			String path = pathname.getPath();
			return !path.toUpperCase().endsWith(MANIFEST_FILE_NAME);
		    }
		};
		copyAll(filter, tgtFile, buildInfo.getRuntimeJarFile(monitor));
		File.umount(tgtFile);
	    }
	} catch (IOException e) {
	    MTJCorePlugin.throwCoreException(IStatus.ERROR, -999, e);
	}
    }

    /**
     * Copy all of the files from the source to the target recursively. Use the
     * provided filter to determine whether the source file should be copied.
     * 
     * @param filter
     * @param srcFile
     * @param tgtFile
     * @throws IOException
     */
    private void copyAll(FileFilter filter, File srcFile, File tgtFile)
	    throws IOException {
	if (srcFile.isFile() && !srcFile.isArchive()) {
	    tgtFile.copyFrom(srcFile);
	} else {
	    java.io.File[] listFiles = srcFile.listFiles(filter);
	    for (int i = 0; i < listFiles.length; i++) {
		copyAll(filter, new File(listFiles[i]), new File(tgtFile,
			listFiles[i].getName()));
	    }
	}
    }

    /**
     * Log any errors that occurred during preverification.
     * 
     * @param srcResource
     * @param srcFile
     * @param errors
     * @throws CoreException
     */
    private void createJarErrorMarker(IResource srcResource, File srcFile,
	    PreverificationError[] errors) throws CoreException {
	StringBuffer sb = new StringBuffer("Preverification errors:\n");

	for (int i = 0; i < errors.length; i++) {
	    sb.append(PreverificationUtils.getErrorText(errors[i]))
		    .append("\n");
	}

	IMarker marker = srcResource
		.createMarker(IJavaModelMarker.JAVA_MODEL_PROBLEM_MARKER);
	marker.setAttribute(IMarker.MESSAGE, sb.toString());
	marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
    }

    /**
     * Clear the specified container of all resources recursively.
     * 
     * @param buildInfo
     * @param container
     * @param monitor
     * @throws CoreException
     */
    private void clearContainer(BuildInfo buildInfo, IContainer container,
	    IProgressMonitor monitor) throws CoreException {
	Utils.clearContainer(container, monitor);
	buildInfo.setPackageDirty(true);
    }

    /**
     * Add the class file resources inside this resource and all subfolders.
     * 
     * @param classes
     * @param resources
     * @param resource
     * @param libraryFolderPaths
     */
    private void collectClassesAndResources(List<IResource> classes,
	    List<IResource> resources, IResource resource,
	    IPath[] libraryFolderPaths) throws CoreException {
	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("> ResourceDeltaBuilder.collectClassesAndResources; resource = "
			    + resource);
	}

	if (resource.getType() == IResource.FILE) {
	    if (!isLibraryResource(libraryFolderPaths, resource)) {
		IFile file = (IFile) resource;
		if ("class".equals(file.getFileExtension())) {
		    classes.add(resource);
		} else {
		    resources.add(resource);
		}
	    }
	} else {
	    IContainer container = (IContainer) resource;
	    IResource[] members = container.members();
	    for (int i = 0; i < members.length; i++) {
		collectClassesAndResources(classes, resources, members[i],
			libraryFolderPaths);
	    }
	}

	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("< ResourceDeltaBuilder.collectClassesAndResources");
	}
    }

    /**
     * Collect and return the paths of the libraries that are folders rather
     * than archives.
     * 
     * @param monitor
     * @throws CoreException
     */
    private IPath[] collectLibraryFolderPaths(IProgressMonitor monitor)
	    throws CoreException {
	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("> ResourceDeltaBuilder.collectLibraryFolderPaths");
	}

	LibraryCollectionVisitor visitor = new LibraryCollectionVisitor();
	visitor.getRunner().run(buildInfo.getCurrentJavaProject(), visitor,
		monitor);

	ArrayList<IPath> libraryFolders = new ArrayList<IPath>();
	Iterator<IClasspathEntry> collectedLibraries = visitor
		.getLibraryEntries().iterator();
	while (collectedLibraries.hasNext()) {
	    IClasspathEntry entry = (IClasspathEntry) collectedLibraries.next();
	    File entryFile = new File(Utils
		    .getResolvedClasspathEntryFile(entry));
	    if (entryFile.isDirectory()) {
		libraryFolders.add(entry.getPath().makeAbsolute());
	    }
	}

	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("< ResourceDeltaBuilder.collectLibraryFolderPaths");
	}
	return (IPath[]) libraryFolders
		.toArray(new IPath[libraryFolders.size()]);
    }

    /**
     * Create an error marker for the specific type with the specified error
     * message.
     * 
     * @param error
     */
    private void createErrorMarkerFor(PreverificationError error)
	    throws JavaModelException, CoreException {
	IMarker marker = null;

	// Calculate the resource
	IClassErrorInformation classInfo = error.getLocation()
		.getClassInformation();
	String typeName = (classInfo == null) ? "" : classInfo.getName()
		.replace('/', '.');
	String message = PreverificationUtils.getErrorText(error);

	IType type = buildInfo.getCurrentJavaProject().findType(typeName);
	if (type != null) {
	    IResource resource = type.getResource();

	    // Sometimes the resource doesn't come back... This is supposed
	    // to be only when the resource is in an external archive.
	    if (resource != null) {
		// Create the marker and set the attributes
		marker = resource
			.createMarker(IJavaModelMarker.JAVA_MODEL_PROBLEM_MARKER);
		marker.setAttribute(IMarker.MESSAGE, message);
		marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);

		int lineNumber = error.getLocation().getLineNumber();
		if (lineNumber != -1) {
		    marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
		}

		setMarkerRangeAttributes(marker, error, type);
	    }
	}

	// Fallback position if nothing specific is possible
	if (marker == null) {
	    createProjectLevelPreverifyMarker(typeName, message);
	}
    }

    /**
     * Create an error marker on the project about a type verification. This
     * will happen when an error occurs trying to preverify and yet the type
     * resource cannot be located for some reason.
     * 
     * @param typeName
     * @param message
     * @throws CoreException
     */
    private void createProjectLevelPreverifyMarker(String typeName,
	    String message) throws CoreException {
	StringBuffer sb = new StringBuffer("Type ");
	sb.append(typeName).append(" ").append(message);

	IProject project = buildInfo.getCurrentJavaProject().getProject();
	IMarker marker = project
		.createMarker(IJavaModelMarker.JAVA_MODEL_PROBLEM_MARKER);
	marker.setAttribute(IMarker.MESSAGE, sb.toString());
	marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
    }

    /**
     * Return a boolean indicating whether or not the classes in the specified
     * directory and subdirectories need preverification.
     * 
     * @param srcFile
     * @param tgtFile
     * @return
     */
    private boolean doClassesRequirePreverification(File srcFile, File tgtFile) {
	boolean requirePreverify = false;

	if (srcFile.isDirectory()) {
	    if (!tgtFile.exists()) {
		requirePreverify = true;
	    } else {
		java.io.File[] classesAndDirectories = srcFile
			.listFiles(new FileFilter() {
			    public boolean accept(java.io.File pathname) {
				return pathname.isDirectory()
					|| pathname.getName()
						.endsWith(".class");
			    }
			});

		for (int i = 0; i < classesAndDirectories.length; i++) {
		    File srcFile2 = new File(classesAndDirectories[i]);
		    File tgtFile2 = new File(tgtFile, classesAndDirectories[i]
			    .getName());
		    requirePreverify = doClassesRequirePreverification(
			    srcFile2, tgtFile2);
		    if (requirePreverify)
			break;
		}
	    }
	} else {
	    requirePreverify = isSourceNewerThanTarget(srcFile, tgtFile);
	}

	return requirePreverify;
    }

    /**
     * Get the output locations (IPath instances) for the specified java
     * project.
     * 
     * @param javaProject
     * @param monitor
     * @return
     * @throws CoreException
     */
    private IPath[] getOutputLocations(IJavaProject javaProject,
	    IProgressMonitor monitor) throws CoreException {
	OutputLocationsCollectionVisitor visitor = new OutputLocationsCollectionVisitor();
	visitor.getRunner().run(javaProject, visitor, monitor);

	// Collect the unique output locations
	Set<IPath> outputLocations = visitor.getOutputLocations();
	return (IPath[]) outputLocations.toArray(new IPath[outputLocations
		.size()]);
    }

    /**
     * Get the method based on the method information.
     * 
     * @param type
     * @param methodInfo
     * @return
     */
    private IMethod getTypeMethod(IType type, IMethodErrorInformation methodInfo) {
	Type[] argTypes = Type
		.getArgumentTypes(methodInfo.getTypeDescription());
	String[] argTypeStrings = new String[argTypes.length];
	for (int i = 0; i < argTypes.length; i++) {
	    argTypeStrings[i] = argTypes[i].getInternalName();
	}

	return type.getMethod(methodInfo.getName(), argTypeStrings);
    }

    /**
     * Handle class file additions and changes by preverifying the new and
     * changed classes.
     * 
     * @param classFiles
     * @param monitor
     * @throws PreverifierNotFoundException
     */
    private void handleClassAddsAndChanges(List<IResource> classFiles,
	    IProgressMonitor monitor) throws CoreException,
	    PreverifierNotFoundException {
	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("> ResourceDeltaBuilder.handleClassAddsAndChanges; classFiles count = "
			    + classFiles.size());
	}

	if (classFiles.size() > 0) {
	    IResource[] resources = (IResource[]) classFiles
		    .toArray(new IResource[classFiles.size()]);

	    try {
		// Run the preverification
		IFolder outputFolder = buildInfo
			.getVerifiedClassesFolder(monitor);
		PreverificationError[] errors = buildInfo.getMidletSuite()
			.preverify(resources, outputFolder, monitor);
		outputFolder.refreshLocal(IResource.DEPTH_INFINITE, monitor);

		for (int i = 0; i < errors.length; i++) {
		    PreverificationError error = errors[i];
		    createErrorMarkerFor(error);
		}

		// Now, place the results into the deployed jar
		for (int i = 0; i < resources.length; i++) {
		    IFile file = (IFile) resources[i];

		    IPath relativePath = Utils
			    .extractsSourceFolderRelativePath(buildInfo
				    .getCurrentJavaProject(), file);
		    IFile outputFile = outputFolder.getFile(relativePath);
		    if (outputFile.exists()) {
			addFileToRuntimeJar(relativePath, outputFile, monitor);
		    }
		}

		buildInfo.setPackageDirty(true);

	    } catch (IOException e) {
		MTJCorePlugin.throwCoreException(IStatus.ERROR, -999, e);
	    }
	}

	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("< ResourceDeltaBuilder.handleClassAddsAndChanges");
	}
    }

    /**
     * Handle resource file additions and changes by copying the new and changed
     * classes.
     * 
     * @param resourceFiles
     * @param monitor
     */
    private void handleNonClassAddsAndChanges(List<IResource> resourceFiles,
	    IProgressMonitor monitor) throws CoreException {
	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("> ResourceDeltaBuilder.handleResourceAddsAndChanges; resource count = "
			    + resourceFiles.size());
	}

	if (resourceFiles.size() > 0) {
	    Iterator<IResource> iter = resourceFiles.iterator();
	    while (iter.hasNext()) {
		IFile file = (IFile) iter.next();
		handleNonClassAddOrChange(file, monitor);
	    }
	}

	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("< ResourceDeltaBuilder.handleResourceAddsAndChanges");
	}
    }

    /**
     * Handle the addition of a single resource to the deployed jar file.
     * 
     * @param file
     * @throws CoreException
     */
    private void handleNonClassAddOrChange(IFile file, IProgressMonitor monitor)
	    throws CoreException {
	if (file.exists()
		&& buildInfo.getResourceFilter().shouldBeIncluded(file)) {
	    // Calculate the path
	    IPath relativePath = Utils.extractsSourceFolderRelativePath(
		    buildInfo.getCurrentJavaProject(), file);

	    if (relativePath != null) {
		addFileToRuntimeJar(relativePath, file, monitor);
		buildInfo.setPackageDirty(true);
	    }
	}
    }

    /**
     * Handle a null delta for the specified project.
     * 
     * @param monitor
     * @throws PreverifierNotFoundException
     */
    private void handleNullDelta(IProgressMonitor monitor)
	    throws CoreException, PreverifierNotFoundException {
	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("> ResourceDeltaBuilder.handleNullDelta");
	}

	// Collect the unique output locations
	IPath[] outputLocations = getOutputLocations(buildInfo
		.getCurrentJavaProject(), monitor);

	// We don't want stuff from the library folders at this point
	IPath[] libraryFolderPaths = collectLibraryFolderPaths(monitor);

	// Now collect up the classes in the output locations
	List<IResource> classes = new ArrayList<IResource>();
	List<IResource> resources = new ArrayList<IResource>();

	for (int i = 0; i < outputLocations.length; i++) {
	    IPath path = outputLocations[i];

	    IResource resource = workspaceRoot.findMember(path.makeAbsolute());
	    if (resource != null) {
		collectClassesAndResources(classes, resources, resource,
			libraryFolderPaths);
	    }
	}

	// Do the adds
	handleClassAddsAndChanges(classes, monitor);
	handleNonClassAddsAndChanges(resources, monitor);

	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("< ResourceDeltaBuilder.handleNullDelta");
	}
    }

    /**
     * Handle class file removals by removing them from the preverified
     * directory.
     * 
     * @param list
     * @param monitor
     */
    private void handleRemoves(List<IResource> removals,
	    IProgressMonitor monitor) throws CoreException {
	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("> ResourceDeltaBuilder.handleRemoves; resource count = "
			    + removals.size());
	}

	// Only do this if there is really anything to do
	if (removals.size() > 0) {
	    IFolder outputFolder = buildInfo.getVerifiedClassesFolder(monitor);

	    // Remove one by one...
	    Iterator<IResource> iter = removals.iterator();
	    while (iter.hasNext()) {
		// Find the verified class for the specified
		// resource and delete it if found.
		IResource resource = (IResource) iter.next();
		removeVerifiedResource(buildInfo.getCurrentJavaProject(),
			outputFolder, resource, monitor);
	    }
	}

	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("< ResourceDeltaBuilder.handleRemoves");
	}
    }

    /**
     * Handle a non-null resource delta build.
     * 
     * @param project
     * @param delta
     * @param monitor
     * @return
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    private void handleResourceDelta(IProgressMonitor monitor)
	    throws CoreException, PreverifierNotFoundException {
	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("> ResourceDeltaBuilder.handleResourceDelta");
	}

	// Collect class file deltas
	ResourceDeltaVisitor visitor = new ResourceDeltaVisitor(monitor);
	buildInfo.getCurrentResourceDelta().accept(visitor, false);

	if (buildInfo.areClassesPreverified()) {
	    // Handle the class deltas
	    handleRemoves(visitor.getRemovedClasses(), monitor);
	    handleClassAddsAndChanges(visitor.getAddedOrChangedClasses(),
		    monitor);
	}

	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("< ResourceDeltaBuilder.handleResourceDelta");
	}
    }

    /**
     * Remove the specified relative path from the deployed jar file.
     * 
     * @param relativePath
     * @throws CoreException
     */
    private void removeFileFromRuntimeJar(IPath relativePath,
	    IProgressMonitor monitor) throws CoreException {
	if (relativePath != null) {
	    File deployedFile = new File(buildInfo.getRuntimeJarFile(monitor),
		    relativePath.toString());
	    deployedFile.delete();
	}
    }

    /**
     * Remove the specified verified resource.
     * 
     * @param javaProject
     * @param outputFolder
     * @param resource
     * @param monitor
     * @throws CoreException
     */
    private void removeVerifiedResource(IJavaProject javaProject,
	    IFolder outputFolder, IResource resource, IProgressMonitor monitor)
	    throws CoreException {
	IPath pathToResource = Utils.extractsSourceFolderRelativePath(
		javaProject, resource);

	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("> ResourceDeltaBuilder.deleteVerifiedResource; resource = "
			    + resource + "; pathToResource = " + pathToResource);
	}

	if (pathToResource != null) {
	    IResource verifiedResource = outputFolder
		    .findMember(pathToResource);

	    if ((verifiedResource != null) && verifiedResource.exists()) {
		verifiedResource.delete(true, monitor);
	    }

	    removeFileFromRuntimeJar(pathToResource, monitor);
	    buildInfo.setPackageDirty(true);
	}

	if (buildLoggingConfig.isPreverifierTraceEnabled()) {
	    BuildConsoleProxy.instance
		    .traceln("< ResourceDeltaBuilder.deleteVerifiedResource");
	}
    }

    /**
     * Set the attributes for character range on the marker.
     * 
     * @param marker
     * @param error
     * @param type
     * @throws CoreException
     */
    private void setMarkerRangeAttributes(IMarker marker,
	    PreverificationError error, IType type) throws CoreException {
	int start = 1;
	int end = 1;

	IMethodErrorInformation methodInfo = error.getLocation()
		.getMethodInformation();
	IFieldErrorInformation fieldInfo = error.getLocation()
		.getFieldInformation();

	switch (error.getLocation().getLocationType().getTypeCode()) {
	case PreverificationErrorLocationType.CLASS_DEFINITION_CODE: {
	    ISourceRange sourceRange = type.getNameRange();
	    start = sourceRange.getOffset();
	    end = start + sourceRange.getLength();
	}
	    break;

	case PreverificationErrorLocationType.CLASS_FIELD_CODE: {
	    IField field = type.getField(fieldInfo.getName());
	    if ((field != null) && (field.exists())) {
		ISourceRange sourceRange = field.getNameRange();
		start = sourceRange.getOffset();
		end = start + sourceRange.getLength();
	    }
	}
	    break;

	case PreverificationErrorLocationType.METHOD_SIGNATURE_CODE:
	case PreverificationErrorLocationType.METHOD_FIELD_CODE:
	case PreverificationErrorLocationType.METHOD_INSTRUCTION_CODE: {
	    IMethod method = getTypeMethod(type, methodInfo);
	    if ((method != null) && (method.exists())) {
		ISourceRange sourceRange = method.getNameRange();
		start = sourceRange.getOffset();
		end = start + sourceRange.getLength();
	    }
	}
	    break;
	}

	// Final fallback...
	marker.setAttribute(IMarker.CHAR_START, start);
	marker.setAttribute(IMarker.CHAR_END, end);
    }
}
