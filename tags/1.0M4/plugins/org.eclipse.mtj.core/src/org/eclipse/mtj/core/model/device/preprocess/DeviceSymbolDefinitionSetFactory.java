/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 *     Feng Wang (Sybase) - Remove method createSymbolDefinitionSet(IDevice),
 *                          for it's no longer used.
 *     Ales Milan - Add import symbol sets from J2ME Polish devices.xml and 
 *                  groups.xml files support.
 */
package org.eclipse.mtj.core.model.device.preprocess;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSetRegistry;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This class is responsible for generate {@link SymbolDefinitionSet}'s based on
 * a device information.
 * 
 * @author Diego Madruga Sandin
 */
public class DeviceSymbolDefinitionSetFactory {

    private static final String J2MEPOLISH_CAPABILITY_SEPARATOR = ",";

    public static final String J2MEPOLISH_FILENAME_XML_DEVICES = "devices.xml";
    public static final String J2MEPOLISH_FILENAME_XML_GROUPS = "groups.xml";

    /**
     * Copy capabilities from Map to other Map.
     * 
     * @param capabilities dest map
     * @param parentCapabilities source map, records from this map will be
     *            copied to dest map
     */
    private static void copyCapabilities(HashMap<String, String> capabilities,
            HashMap<String, String> parentCapabilities) {
        Iterator<String> i = parentCapabilities.keySet().iterator();
        while (i.hasNext()) {
            String capability = i.next();
            copyCapability(capabilities, capability, parentCapabilities
                    .get(capability));
        }
    }

    /**
     * Copy capability. If exist in dest map capability with same key, then new
     * capability value will be merged to exist capability.
     * 
     * @param capabilities dest map of capabilities
     * @param name key of capability
     * @param capability new capability, this capability value is added or
     *            merged to map
     */
    private static void copyCapability(HashMap<String, String> capabilities,
            String name, String capability) {

        String currentCapabilityValue = capabilities.get(name);
        if ((currentCapabilityValue == null)
                || currentCapabilityValue.equals("")) {
            capabilities.put(name, capability);
        } else {
            String mergedCapabilities = mergeCapabilities(
                    currentCapabilityValue, capability);
            capabilities.put(name, mergedCapabilities);
        }

    }

    /**
     * Create SymbolDefinitionSet from Map of properties.
     * 
     * @param name
     * @param symbols
     * @author Ales "afro" Milan
     * @return
     */
    public static SymbolDefinitionSet createSymbolDefinitionSet(String name,
            HashMap<String, String> symbols) {

        SymbolDefinitionSet definitionSet = new SymbolDefinitionSet(name);
        definitionSet.define(name);

        for (String key : symbols.keySet()) {
            try {
                String value = symbols.get(key);
                definitionSet.define(key, value);
            } catch (RuntimeException e) {
                MTJCorePlugin.log(IStatus.ERROR, e);
            }
        }

        return definitionSet;
    }

    /**
     * Return capabilities for specified group.
     * 
     * @param groups map of groups XML elements
     * @param name group name
     * @return map of capabilities, key/value
     */
    private static HashMap<String, String> getGroupCapabilities(
            HashMap<String, Element> groups, String name) {

        Element groupElement = groups.get(name);

        HashMap<String, String> result = new HashMap<String, String>();

        NodeList nl = groupElement.getElementsByTagName("parent");
        if ((nl != null) && (nl.getLength() > 0)) {
            Element parent = (Element) nl.item(0);
            String parentValue = parent.getTextContent();
            if ((parentValue != null) && !parent.getTextContent().equals("")) {
                HashMap<String, String> parentCapabilities = getGroupCapabilities(
                        groups, parent.getTextContent());
                copyCapabilities(result, parentCapabilities);
            }
        }

        nl = groupElement.getElementsByTagName("capability");
        for (int i = 0; i < nl.getLength(); i++) {
            Element capabilityElement = (Element) nl.item(i);
            result.put(capabilityElement.getAttribute("name"),
                    capabilityElement.getAttribute("value"));
        }

        return result;

    }

    /**
     * Import and create array of SymbosDefinitionSet from J2ME polish XML files
     * (devices.xml & groups.xml).
     * 
     * @param monitor Progress monitor, inform user about process of import.
     *            Null is allowed.
     * @param devicesInputStream Input stream of device.xml file.
     * @param groupsInputStream Input stream of groups.xml file.
     * @return
     * @throws PersistenceException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @author Ales "afro" Milan
     */
    public static SymbolDefinitionSet[] importFromJ2MEPolishFormat(
            IProgressMonitor monitor, InputStream devicesInputStream,
            InputStream groupsInputStream) throws PersistenceException,
            ParserConfigurationException, SAXException, IOException {

        Vector<SymbolDefinitionSet> result = new Vector<SymbolDefinitionSet>();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();

        Document devicesDOM = db.parse(devicesInputStream);
        Document groupsDOM = db.parse(groupsInputStream);

        HashMap<String, Element> groups = prepareGroups(groupsDOM);

        NodeList nl = devicesDOM.getElementsByTagName("device");
        if (monitor != null) {
            monitor.beginTask("Importing...", nl.getLength());
        }
        for (int i = 0; i < nl.getLength(); i++) {

            HashMap<String, String> capabilities = new HashMap<String, String>();
            HashMap<String, String> groupCapabilities;
            String features;

            Element deviceElement = (Element) nl.item(i);
            String name = ((Element) deviceElement.getElementsByTagName(
                    "identifier").item(0)).getTextContent();

            Element featuresElement = (Element) deviceElement
                    .getElementsByTagName("features").item(0);
            if (featuresElement != null) {
                features = featuresElement.getTextContent();
                capabilities.put("features", features);
            }

            Element groupElement = (Element) deviceElement
                    .getElementsByTagName("groups").item(0);
            if (groupElement != null) {
                StringTokenizer st = new StringTokenizer(groupElement
                        .getTextContent(), J2MEPOLISH_CAPABILITY_SEPARATOR);
                while (st.hasMoreTokens()) {
                    String group = st.nextToken().trim();
                    groupCapabilities = getGroupCapabilities(groups, group);
                    copyCapabilities(capabilities, groupCapabilities);
                }
                capabilities.put("groups", groupElement.getTextContent()
                        .toLowerCase());
            }

            NodeList capabilitiesNL = deviceElement
                    .getElementsByTagName("capability");
            for (int j = 0; j < capabilitiesNL.getLength(); j++) {
                Element capabilityElement = (Element) capabilitiesNL.item(j);
                copyCapability(capabilities, capabilityElement
                        .getAttribute("name"), capabilityElement
                        .getAttribute("value"));
            }

            capabilities.put(name, "true");

            StringTokenizer st = new StringTokenizer(name,
                    J2MEPOLISH_CAPABILITY_SEPARATOR);
            while (st.hasMoreTokens()) {
                String device = st.nextToken().trim();
                device = device.replace('/', '_');
                device = device.replace('(', '_');
                device = device.replace(')', '_');

                SymbolDefinitionSet definitionSet = DeviceSymbolDefinitionSetFactory
                        .createSymbolDefinitionSet(device, capabilities);
                SymbolDefinitionSetRegistry.singleton
                        .addDefinitionSet(definitionSet);
            }
            if (monitor != null) {
                monitor.worked(1);
            }
        }
        if (monitor != null) {
            monitor.done();
        }

        if (result.size() != 0) {
            SymbolDefinitionSet[] res = new SymbolDefinitionSet[result.size()];
            result.copyInto(res);
            return res;
        }
        return null;
    }

    /**
     * Merge two capabilities to one string. Function ignore duplicity
     * 
     * @param currentCapability
     * @param newCapability
     * @return merged capabilities
     */
    private static String mergeCapabilities(String currentCapability,
            String newCapability) {

        Vector<String> v = new Vector<String>();
        StringTokenizer st = new StringTokenizer(currentCapability,
                J2MEPOLISH_CAPABILITY_SEPARATOR);
        while (st.hasMoreTokens()) {
            v.add(st.nextToken().trim());
        }
        st = new StringTokenizer(newCapability, J2MEPOLISH_CAPABILITY_SEPARATOR);
        while (st.hasMoreTokens()) {
            String capability = st.nextToken().trim();
            if (v.indexOf(capability) == -1) {
                v.add(capability);
            }
        }
        String result = "";
        for (int i = 0; i < v.size(); i++) {
            if (i > 0) {
                result = result.concat(",");
            }
            result = result.concat(v.elementAt(i));
        }

        return result;
    }

    /**
     * Prepare device groups elements to Map.
     * 
     * @param groupsDOM XML document which contain Device Groups
     *            <p>
     *            Example of XML document: <code>
     * <groups>
     *   <group>
     *     <name>name of group</name>
     *     <description>...</description>
     *     <features>feature1, feature2, ...</features>
     *     <parent>nameOfParrentGroup</parent>
     *     <capability name="capabilityA" value="valueA" />
     *     <capability name="capabilityB" value="valueB" />
     *     ...
     *   </group>
     *   <group>
     *     ...
     *   </group>
     * </groups>
     * </code>
     *            </p>
     * @return return map of XML elements
     */
    private static HashMap<String, Element> prepareGroups(Document groupsDOM) {

        HashMap<String, Element> result = new HashMap<String, Element>();

        NodeList nl = groupsDOM.getElementsByTagName("group");
        for (int i = 0; i < nl.getLength(); i++) {
            Element groupElement = (Element) nl.item(i);
            String name = ((Element) groupElement.getElementsByTagName("name")
                    .item(0)).getTextContent();
            result.put(name, groupElement);
        }

        return result;
    }

}
