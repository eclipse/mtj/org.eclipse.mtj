/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 *     David Marques (Motorola) - Adding resolved callers 
 *     with unresolved calles
 *     David Marques (Motorola) - Extracting specific methods
 *     to subclasses.
 */
package org.eclipse.mtj.core.internal.launching;

import java.io.File;
import java.util.Stack;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;

/**
 * StackTraceParser class parses stack traces for an IJavaProject. Parsing a
 * stack trace means resolving line numbers and source files for the stack trace
 * lines.
 * 
 * @author David Marques
 */
public abstract class StackTraceParser {

    protected IJavaProject project;

    /**
     * Creates a StackTraceParser for the specified file.
     * 
     * @param _project project instance.
     */
    public StackTraceParser(IJavaProject _project) {
        this.project = _project;
    }

    /**
     * Parses the specified stack into a new stack with lines entries. The process
     * of line number resolution and source linkage is specific for each stack trace
     * type.
     * 
     * @param _stack stack to parse.
     * @return a new stack parsed.
     * @throws StackTraceParserException if any error occurs.
     */
    public abstract Stack<StackTraceEntry> parseStackTrace(Stack<StackTraceEntry> _stack)
            throws StackTraceParserException;

    /**
     * Finds the source file inside a project.
     * 
     * @param className name of class to search.
     * @return the source file or null of not found.
     * @throws StackTraceParserException if any error occurs.
     */
    protected File getSourceFile(String className)
            throws StackTraceParserException {
        File classFile = null;
        try {
            String[] paths = Utils
                    .getJavaProjectSourceDirectories(this.project);
            for (int i = 0; i < paths.length; i++) {
                File srcFolder = project.getProject().getLocation().append(
                        paths[i]).toFile();
                classFile = findClassFile(srcFolder, className);
                if (classFile != null) {
                    break;
                }
            }
        } catch (Exception e) {
            MTJCorePlugin.log(IStatus.ERROR, e.getMessage(), e);
        }
        return classFile;
    }

    /**
     * Searched for a class inside a source folder.
     * 
     * @param srcFolder folder name.
     * @param className class name.
     * @return the class file or null if not found.
     */
    private File findClassFile(File srcFolder, String className) {
        File tempFile = null;
        /* Remove $ from inner classes */
        int index = className.indexOf("$"); //$NON-NLS-1$
        if (index >= 0x00) {
            className = className.substring(0x00, index);
        }

        index = className.lastIndexOf("."); //$NON-NLS-1$
        if (index >= 0x00) {
            String packageName = className.substring(0x00, index);
            String packages[] = packageName.split("."); //$NON-NLS-1$
            for (int i = 0; i < packages.length; i++) {
                tempFile = new File(srcFolder, packages[i]);
                if (tempFile.exists()) {
                    srcFolder = tempFile;
                }
            }
        } else {
            tempFile = new File(srcFolder, className + ".java"); //$NON-NLS-1$
        }

        if (tempFile != null && !tempFile.exists()) {
            tempFile = null;
        }

        return tempFile;
    }
}
