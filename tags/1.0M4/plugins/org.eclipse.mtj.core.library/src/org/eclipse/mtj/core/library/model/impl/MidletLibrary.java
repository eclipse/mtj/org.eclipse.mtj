/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.core.library.model.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.jdt.core.IClasspathAttribute;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.library.LibraryPlugin;
import org.eclipse.mtj.core.library.classpath.ClasspathAccessRule;
import org.eclipse.mtj.core.library.classpath.JavadocClasspathAttribute;
import org.eclipse.mtj.core.library.model.ILibrary;
import org.eclipse.mtj.core.library.model.Visibility;
import org.eclipse.mtj.core.library.model.licence.LicenceInfo;
import org.eclipse.mtj.core.library.model.security.PermissionList;
import org.eclipse.mtj.core.library.model.security.ProtectionDomain;
import org.eclipse.mtj.core.library.model.security.SecurityInfo;
import org.eclipse.mtj.core.library.model.security.ProtectionDomain.ProtectionDomainType;
import org.osgi.framework.Version;

/**
 * Representation of the MIDlet libraries that are provided by 3rd party
 * vendors.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public final class MidletLibrary implements ILibrary {

    /* Extension Point Attributes */
    private static final String EP_ATTRIB_ACCESSRULE_KIND = "kind"; //$NON-NLS-1$
    private static final String EP_ATTRIB_ACCESSRULE_PATTERN = "pattern"; //$NON-NLS-1$
    private static final String EP_ATTRIB_CLASSPATHENTRY_JARFILE = "jarFile"; //$NON-NLS-1$
    private static final String EP_ATTRIB_CLASSPATHENTRY_JAVADOCPATH = "javadocPath"; //$NON-NLS-1$
    private static final String EP_ATTRIB_CLASSPATHENTRY_SRCPATH = "srcPath"; //$NON-NLS-1$
    private static final String EP_ATTRIB_LIBRARY_DESCRIPTION = "description"; //$NON-NLS-1$
    private static final String EP_ATTRIB_LIBRARY_NAME = "name"; //$NON-NLS-1$
    private static final String EP_ATTRIB_LIBRARY_VERSION = "version"; //$NON-NLS-1$
    private static final String EP_ATTRIB_LIBRARY_VISIBILITY = "visibility"; //$NON-NLS-1$
    private static final String EP_ATTRIB_LICENSE_NAME = "name"; //$NON-NLS-1$
    private static final String EP_ATTRIB_LICENSE_URL = "url"; //$NON-NLS-1$
    private static final String EP_ATTRIB_SECURITY_PD = "ProtectionDomain"; //$NON-NLS-1$
    private static final String EP_ATTRIB_SECURITY_PERMISSIONS = "Premissions"; //$NON-NLS-1$

    /* Extension Point Elements */
    private static final String EP_ELEMENT_ACCESSRULE = "AccessRule"; //$NON-NLS-1$
    private static final String EP_ELEMENT_CLASSPATHENTRY = "ClasspathEntry"; //$NON-NLS-1$
    private static final String EP_ELEMENT_LICENSE = "License"; //$NON-NLS-1$
    private static final String EP_ELEMENT_SECURITY = "Security"; //$NON-NLS-1$

    private static final String FILE_PROTOCOL = "file:/"; //$NON-NLS-1$

    /**
     * The list of ClasspathEntries available in this library
     */
    private List<IClasspathEntry> classpathEntryList;

    /**
     * The Library description
     */
    private String description;

    /**
     * The Library identifier
     */
    private String identifier;

    /**
     * Licensing information
     */
    private LicenceInfo licence;

    /**
     * The Library name
     */
    private String name;

    /**
     * Security information
     */
    private SecurityInfo security;

    /**
     * The Library version
     */
    private Version version;

    /**
     * The library visibility
     */
    private Visibility visibility;

    /**
     * Creates a new configurationElement based on the informed
     * {@link IConfigurationElement}.
     * 
     * @param configurationElement
     * @throws IllegalArgumentException
     */
    public MidletLibrary(IConfigurationElement configurationElement)
            throws IllegalArgumentException {

        String extImplAbsolutePath = getBundleAbsolutePath(configurationElement
                .getNamespaceIdentifier());

        classpathEntryList = new ArrayList<IClasspathEntry>();

        /* Get the library name [required] */
        name = configurationElement.getAttribute(EP_ATTRIB_LIBRARY_NAME);

        if (name == null) {
            throw new IllegalArgumentException("No library name defined.");
        }

        /* Get the library name [required] */
        visibility = Visibility.getFromString(configurationElement
                .getAttribute(EP_ATTRIB_LIBRARY_VISIBILITY));

        if (visibility == Visibility.INVALID) {
            visibility = Visibility.PUBLIC;
        }

        /* Get the library version [required] */
        String versionString = configurationElement
                .getAttribute(EP_ATTRIB_LIBRARY_VERSION);
        try {
            version = new Version(versionString);
        } catch (Exception e) {
            throw new IllegalArgumentException("The version for " + name
                    + " is improperly formatted.", e);
        }

        /* Get the library version [optional] */
        description = configurationElement
                .getAttribute(EP_ATTRIB_LIBRARY_DESCRIPTION);

        /* Set the library identifier [required] */
        identifier = name.replace(" ", "_") + "_" + version.toString();

        /* Get the library ClasspathEntries [required] */
        IConfigurationElement[] ClasspathEntries = configurationElement
                .getChildren(EP_ELEMENT_CLASSPATHENTRY);

        for (IConfigurationElement classpathEntry : ClasspathEntries) {

            /* Get the library jar file path [required] */
            IPath jarPath = new Path(extImplAbsolutePath
                    + File.separator
                    + classpathEntry
                            .getAttribute(EP_ATTRIB_CLASSPATHENTRY_JARFILE));

            /* Get the library source folder path [optional] */
            String sourceFolder = classpathEntry
                    .getAttribute(EP_ATTRIB_CLASSPATHENTRY_SRCPATH);

            IPath srcPath = null;

            if (sourceFolder != null) {
                srcPath = new Path(extImplAbsolutePath + File.separator
                        + sourceFolder);
            }

            /* Get the library javadoc folder path [optional] */
            String javadocFolder = classpathEntry
                    .getAttribute(EP_ATTRIB_CLASSPATHENTRY_JAVADOCPATH);

            IClasspathAttribute[] extraAttributes = null;

            if (javadocFolder != null) {
                IPath javadocPath = new Path(extImplAbsolutePath
                        + File.separator + javadocFolder);

                extraAttributes = new IClasspathAttribute[1];
                extraAttributes[0] = new JavadocClasspathAttribute(javadocPath
                        .toOSString());
            } else {
                extraAttributes = new IClasspathAttribute[0];
            }

            /* Get the library access rules [optional] */
            IConfigurationElement[] accessRulesElements = classpathEntry
                    .getChildren(EP_ELEMENT_ACCESSRULE);

            List<IAccessRule> accessRules = new ArrayList<IAccessRule>();
            for (IConfigurationElement accessRule : accessRulesElements) {

                ClasspathAccessRule rule = new ClasspathAccessRule(new Path(
                        accessRule.getAttribute(EP_ATTRIB_ACCESSRULE_PATTERN)),
                        accessRule.getAttribute(EP_ATTRIB_ACCESSRULE_KIND));

                accessRules.add(JavaCore.newAccessRule(rule.getPattern(), rule
                        .getKind()));
            }

            /* Create a new classpath entry */
            IClasspathEntry entry = JavaCore.newLibraryEntry(jarPath, srcPath,
                    null, accessRules.toArray(new IAccessRule[0]),
                    extraAttributes, true);

            classpathEntryList.add(entry);
        }

        IConfigurationElement[] licenceCE = configurationElement
                .getChildren(EP_ELEMENT_LICENSE);

        try {
            URI uri = null;

            try {
                uri = URI.create(licenceCE[0]
                        .getAttribute(EP_ATTRIB_LICENSE_URL));
            } catch (Throwable e) {
                /* No need to log */
            }

            licence = new LicenceInfo(licenceCE[0]
                    .getAttribute(EP_ATTRIB_LICENSE_NAME), uri);
        } catch (Throwable e) {
            LibraryPlugin.log(IStatus.WARNING,
                    "No Lincense information available for " + name, e);
        }

        /* Get the library Security info [optional] */
        IConfigurationElement[] securityCE = configurationElement
                .getChildren(EP_ELEMENT_SECURITY);

        ProtectionDomain domain = null;

        try {

            domain = new ProtectionDomain(securityCE[0]
                    .getAttribute(EP_ATTRIB_SECURITY_PD));

        } catch (Throwable e) {
            domain = new ProtectionDomain(ProtectionDomainType.UNTRUSTED);

            LibraryPlugin.log(IStatus.WARNING,
                    "No Protection Domain information available for " + name
                            + ". the Untrusted domain will be used.", e);
        }

        PermissionList permissionList = null;

        try {
            permissionList = new PermissionList(securityCE[0]
                    .getAttribute(EP_ATTRIB_SECURITY_PERMISSIONS));
        } catch (Throwable e) {
            permissionList = PermissionList.EMPTY;
            LibraryPlugin.log(IStatus.WARNING,
                    "No permission information available for " + name, null);
        }

        security = new SecurityInfo(domain, permissionList);

    }

    /**
     * Creates a new MidletLibrary
     * 
     * @param identifier
     * @param name
     * @param description
     * @param version
     * @param licence
     * @throws IllegalArgumentException
     */
    public MidletLibrary(String identifier, String name, String description,
            Visibility visibility, Version version, LicenceInfo licence,
            SecurityInfo security) throws IllegalArgumentException {
        super();

        if (name == null) {
            throw new IllegalArgumentException("No library name defined.");
        }

        if (version == null) {
            throw new IllegalArgumentException("No version was provided for "
                    + name);
        }

        if (visibility == null) {
            throw new IllegalArgumentException(
                    "No visibility was provided for " + name);
        }

        this.name = name;
        this.visibility = visibility;
        this.version = version;
        this.identifier = identifier;
        this.description = description;
        this.licence = licence;
        this.classpathEntryList = new ArrayList<IClasspathEntry>();
        this.security = security;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#addLibraryItem(org.eclipse.mtj.core.library.model.ILibraryItem)
     */
    public void addClasspathEntry(IClasspathEntry libraryItem) {
        this.classpathEntryList.add(libraryItem);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MidletLibrary other = (MidletLibrary) obj;
        if (classpathEntryList == null) {
            if (other.classpathEntryList != null) {
                return false;
            }
        } else if (!classpathEntryList.equals(other.classpathEntryList)) {
            return false;
        }
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        if (identifier == null) {
            if (other.identifier != null) {
                return false;
            }
        } else if (!identifier.equals(other.identifier)) {
            return false;
        }
        if (licence == null) {
            if (other.licence != null) {
                return false;
            }
        } else if (!licence.equals(other.licence)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (security == null) {
            if (other.security != null) {
                return false;
            }
        } else if (!security.equals(other.security)) {
            return false;
        }
        if (version == null) {
            if (other.version != null) {
                return false;
            }
        } else if (!version.equals(other.version)) {
            return false;
        }
        if (visibility == null) {
            if (other.visibility != null) {
                return false;
            }
        } else if (!visibility.equals(other.visibility)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#getLibraryItemList()
     */
    public List<IClasspathEntry> getClasspathEntryList() {
        return Collections.unmodifiableList(classpathEntryList);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#getDescription()
     */
    public String getDescription() {
        return description;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#getIdentifier()
     */
    public String getIdentifier() {
        return identifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#getLicence()
     */
    public LicenceInfo getLicence() {
        return licence;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#getSecurity()
     */
    public SecurityInfo getSecurity() {
        return security;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#getVersion()
     */
    public Version getVersion() {
        return version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#getVisibility()
     */
    public Visibility getVisibility() {
        return visibility;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((classpathEntryList == null) ? 0 : classpathEntryList
                        .hashCode());
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result
                + ((identifier == null) ? 0 : identifier.hashCode());
        result = prime * result + ((licence == null) ? 0 : licence.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result
                + ((security == null) ? 0 : security.hashCode());
        result = prime * result + ((version == null) ? 0 : version.hashCode());
        result = prime * result
                + ((visibility == null) ? 0 : visibility.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#removeLibraryItem(org.eclipse.mtj.core.library.model.ILibraryItem)
     */
    public void removeClasspathEntry(IClasspathEntry libraryItem) {
        this.classpathEntryList.remove(libraryItem);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#setLibraryItemList(java.util.List)
     */
    public void setClasspathEntryList(List<IClasspathEntry> libraryItemList) {
        this.classpathEntryList = libraryItemList;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#setDescription(java.lang.String)
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#setIdentifier(java.lang.String)
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#setName(java.lang.String)
     */
    public void setName(String name) {
        this.name = name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#setVersion(org.osgi.framework.Version)
     */
    public void setVersion(Version version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.library.model.ILibrary#setVisibility(org.eclipse.mtj.core.library.model.Visibility)
     */
    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;

    }

    /**
     * @param bundleNamespaceIdentifier
     * @return
     */
    private String getBundleAbsolutePath(String bundleNamespaceIdentifier) {

        String absolutePath = new String();
        URL entry = Platform.getBundle(bundleNamespaceIdentifier).getEntry("/");
        String bundleLocation = "";
        try {
            bundleLocation = FileLocator.toFileURL(entry).getPath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        URL bundleUrl = null;
        try {
            bundleUrl = new File(bundleLocation).toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (bundleUrl != null) {
            absolutePath = bundleUrl.getFile();
            if (absolutePath.startsWith(FILE_PROTOCOL)) {
                absolutePath = absolutePath
                        .substring(FILE_PROTOCOL.length() + 1);
            }
        }

        return absolutePath;
    }
}
