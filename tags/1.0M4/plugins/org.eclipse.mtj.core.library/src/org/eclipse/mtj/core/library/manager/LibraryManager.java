/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola)  - Initial version
 *     David Marques (Motorola) - Added new methods to add and remove
 *                                libraries from MIDlet projects
 */
package org.eclipse.mtj.core.library.manager;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.library.LibraryPlugin;
import org.eclipse.mtj.core.library.model.ILibrary;
import org.eclipse.mtj.core.library.model.impl.MidletLibrary;
import org.eclipse.mtj.internal.core.library.MIDletLibraryClasspathContainer;
import org.eclipse.osgi.util.NLS;

/**
 * Library Manager responsible for supply the libraries available in MTJ.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class LibraryManager {

    /**
     * Library Extension point ID
     */
    private static final String EXT_MIDLETLIBRARY = "MIDletLibrary"; //$NON-NLS-1$

    /**
     * The unique instance of the LibraryManager
     */
    private static LibraryManager LibraryManagerInstance;

    /**
     * Get the LibraryManager instance.
     * 
     * @return the unique instance of the LibraryManager
     */
    public synchronized static LibraryManager getInstance() {
        if (LibraryManagerInstance == null) {
            LibraryManagerInstance = new LibraryManager();
        }

        return LibraryManagerInstance;
    }

    private Map<String, MidletLibrary> midletLibraries;

    /**
     * Creates a new LibraryManager and initialize the list of available
     * libraries.
     */
    private LibraryManager() {
        initialize();
    }

    /**
     * Adds a library to a MIDlet project.
     * 
     * @param project target project.
     * @param libName target library name.
     * @throws CoreException If the project does not have a
     *             org.eclipse.mtj.core.nature or if the specified library does
     *             not exist in the library manager.
     */
    public void addLibraryToMidletProject(IProject project, String libName)
            throws CoreException {
        if (!project.hasNature(IMTJCoreConstants.MTJ_NATURE_ID)) {
            String message = NLS.bind("Project {0} must have a {1} nature.",
                    new String[] { project.getName(),
                            IMTJCoreConstants.MTJ_NATURE_ID });
            MTJCorePlugin.throwCoreException(IStatus.ERROR, -999, message);
        }

        ILibrary library = this.getMidletLibrary(libName);
        if (library == null) {
            String message = NLS.bind(
                    "LibraryManager does not contain a {0} library.", libName);
            MTJCorePlugin.throwCoreException(IStatus.ERROR, -999, message);
        }

        IJavaProject javaProject = JavaCore.create(project);

        List<IClasspathEntry> entriesList = new LinkedList<IClasspathEntry>();
        entriesList.addAll(Arrays.asList(javaProject.getRawClasspath()));
        
        try {
            IPath path = new Path(
                    MIDletLibraryClasspathContainer.MIDLET_LIBRARY_CONTAINER_ID
                            + File.separator + library.getName());
            entriesList.add(JavaCore.newContainerEntry(path, true));
        } catch (Exception e) {
            e.printStackTrace();
        }

        javaProject.setRawClasspath(null, new NullProgressMonitor());

        javaProject.setRawClasspath(entriesList
                .toArray(new IClasspathEntry[entriesList.size()]),
                new NullProgressMonitor());
        
        Utils.addNatureToProject(javaProject.getProject(),
                IMTJCoreConstants.JMUNIT_NATURE_ID, new NullProgressMonitor());
    }

    /**
     * Gets corresponding library for the given name.
     * 
     * @param libName the library name
     * @return the library for a given name or <code>null</code> if no such
     *         library exists.
     */
    public synchronized ILibrary getMidletLibrary(String libName) {
        return this.midletLibraries.get(libName);

    }

    /**
     * Returns the names of all defined MIDlet libraries. The corresponding
     * classpath container path is the name appended to the CONTAINER_ID.
     * 
     * @return the names of all defined MIDlet libraries
     */
    public synchronized String[] getMidletLibraryNames() {
        Set<String> set = this.midletLibraries.keySet();
        return set.toArray(new String[set.size()]);
    }

    /**
     * Removes a library from a MIDlet project.
     * 
     * @param project target project.
     * @param libName target library name.
     * @throws CoreException If the project does not have a
     *             org.eclipse.mtj.core.nature or if the specified library does
     *             not exist in the library manager.
     */
    public void removeLibraryFromMidletProject(IProject project, String libName)
            throws CoreException {
        if (!project.hasNature(IMTJCoreConstants.MTJ_NATURE_ID)) {
            String message = NLS.bind("Project {0} must have a {1} nature.",
                    new String[] { project.getName(),
                            IMTJCoreConstants.MTJ_NATURE_ID });
            MTJCorePlugin.throwCoreException(IStatus.ERROR, -999, message);
        }

        ILibrary library = this.getMidletLibrary(libName);
        if (library == null) {
            String message = NLS.bind(
                    "LibraryManager does not contain a {0} library.", libName);
            MTJCorePlugin.throwCoreException(IStatus.ERROR, -999, message);
        }

        IJavaProject javaProject = JavaCore.create(project);
        IPath path = new Path(
                MIDletLibraryClasspathContainer.MIDLET_LIBRARY_CONTAINER_ID
                        + File.separator + library.getName());

        List<IClasspathEntry> entriesList = Arrays.asList();
        for (IClasspathEntry classpathEntry : entriesList) {
            if (classpathEntry.getPath().toString().equals(path.toString())) {
                entriesList.remove(classpathEntry);
            }
        }

        javaProject.setRawClasspath(entriesList
                .toArray(new IClasspathEntry[entriesList.size()]),
                new NullProgressMonitor());
    }

    /**
     * Initialize the list of available libraries.
     */
    private void initialize() {
        this.midletLibraries = new HashMap<String, MidletLibrary>();

        IExtensionRegistry registry = Platform.getExtensionRegistry();

        IConfigurationElement[] elements = registry
                .getConfigurationElementsFor(LibraryPlugin.PLUGIN_ID,
                        EXT_MIDLETLIBRARY);

        for (IConfigurationElement element : elements) {
            MidletLibrary midletLibrary = null;
            try {
                midletLibrary = new MidletLibrary(element);
            } catch (Throwable e) {
                LibraryPlugin.log(IStatus.ERROR,
                        "Failed to load MidletLibrary", e); //$NON-NLS-1$
            }
            if (midletLibrary != null) {
                midletLibraries.put(midletLibrary.getName(), midletLibrary);
            }
        }
    }
}
