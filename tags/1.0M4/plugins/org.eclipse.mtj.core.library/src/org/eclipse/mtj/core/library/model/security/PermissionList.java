/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.core.library.model.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

/**
 * A list of {@link Permission permissions}.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class PermissionList {

    /**
     * An empty PermissionList instance;
     */
    public static final PermissionList EMPTY = new PermissionList();

    /**
     * The list of permissions.
     */
    private List<Permission> permissionList;

    /**
     * Creates a new PermissionList from a comma separated list with named
     * permissions.
     * <p>
     * <code>permission1,permission2,...,permissionN</code>
     * </p>
     * 
     * @param permissions comma separated list with named permissions.
     */
    public PermissionList(final String permissions) {
        permissionList = new ArrayList<Permission>();

        /* Remove white spaces before StringTokenizer */
        String permissionsInput = permissions.replace(" ", "");

        StringTokenizer permissionTokenizer = new StringTokenizer(
                permissionsInput, ",");

        while (permissionTokenizer.hasMoreTokens()) {
            try {
                permissionList.add(new Permission((String) permissionTokenizer
                        .nextElement()));
            } catch (Exception e) {

            }
        }
    }

    /**
     * Creates a new PermissionList;
     */
    private PermissionList() {
        permissionList = new ArrayList<Permission>();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof PermissionList)) {
            return false;
        }
        PermissionList other = (PermissionList) obj;
        if (permissionList == null) {
            if (other.permissionList != null) {
                return false;
            }
        } else if (!permissionList.equals(other.permissionList)) {
            return false;
        }
        return true;
    }

    /**
     * @return the permissionList
     */
    public List<Permission> getPermissionList() {
        return Collections.unmodifiableList(permissionList);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((permissionList == null) ? 0 : permissionList.hashCode());
        return result;
    }

    /* Specified 
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        if (!permissionList.isEmpty()) {
            for (Iterator<Permission> iterator = permissionList.iterator(); iterator
                    .hasNext();) {
                Permission permission = iterator.next();
                stringBuilder.append(permission.getPermissionID());

                if (iterator.hasNext()) {
                    stringBuilder.append(",");
                }
            }
        } else {
            stringBuilder.append("No permissions specified");
        }

        return stringBuilder.toString();
    }
}
