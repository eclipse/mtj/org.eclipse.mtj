/**
 * Copyright (c) 2003,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.mtj.ui.internal.forms.parts.FormEntry;
import org.eclipse.mtj.ui.internal.forms.parts.IFormEntryListener;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.forms.events.HyperlinkEvent;

/**
 * Implements the listener for FormEntry component events.
 * 
 * @since 0.9.1
 */
public class FormEntryAdapter implements IFormEntryListener {

    private IContextPart contextPart;
    protected IActionBars actionBars;

    /**
     * @param contextPart
     */
    public FormEntryAdapter(IContextPart contextPart) {
        this(contextPart, null);
    }

    /**
     * @param contextPart
     * @param actionBars
     */
    public FormEntryAdapter(IContextPart contextPart, IActionBars actionBars) {
        this.contextPart = contextPart;
        this.actionBars = actionBars;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.forms.parts.IFormEntryListener#browseButtonSelected(org.eclipse.mtj.ui.internal.forms.parts.FormEntry)
     */
    public void browseButtonSelected(FormEntry entry) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.forms.parts.IFormEntryListener#focusGained(org.eclipse.mtj.ui.internal.forms.parts.FormEntry)
     */
    public void focusGained(FormEntry entry) {
        ITextSelection selection = new TextSelection(1, 1);
        contextPart.getPage().getMTJEditor().getContributor()
                .updateSelectableActions(selection);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.events.HyperlinkListener#linkActivated(org.eclipse.ui.forms.events.HyperlinkEvent)
     */
    public void linkActivated(HyperlinkEvent e) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.events.IHyperlinkListener#linkEntered(org.eclipse.ui.forms.events.HyperlinkEvent)
     */
    public void linkEntered(HyperlinkEvent e) {
        if (actionBars == null) {
            return;
        }
        IStatusLineManager mng = actionBars.getStatusLineManager();
        mng.setMessage(e.getLabel());
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.events.IHyperlinkListener#linkExited(org.eclipse.ui.forms.events.HyperlinkEvent)
     */
    public void linkExited(HyperlinkEvent e) {
        if (actionBars == null) {
            return;
        }
        IStatusLineManager mng = actionBars.getStatusLineManager();
        mng.setMessage(null);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.forms.parts.IFormEntryListener#selectionChanged(org.eclipse.mtj.ui.internal.forms.parts.FormEntry)
     */
    public void selectionChanged(FormEntry entry) {
        ITextSelection selection = new TextSelection(1, 1);
        contextPart.getPage().getMTJEditor().getContributor()
                .updateSelectableActions(selection);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.forms.parts.IFormEntryListener#textDirty(org.eclipse.mtj.ui.internal.forms.parts.FormEntry)
     */
    public void textDirty(FormEntry entry) {
        contextPart.fireSaveNeeded();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.forms.parts.IFormEntryListener#textValueChanged(org.eclipse.mtj.ui.internal.forms.parts.FormEntry)
     */
    public void textValueChanged(FormEntry entry) {
    }
}
