/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * @since 0.9.1
 */
public abstract class FormatAction extends Action {

    protected ITextEditor textEditor;

    /**
     * 
     */
    public FormatAction() {
        setText(MTJUIMessages.FormatAction_text);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.action.Action#run()
     */
    @Override
    public abstract void run();

    /* (non-Javadoc)
     * @see org.eclipse.jface.action.Action#runWithEvent(org.eclipse.swt.widgets.Event)
     */
    @Override
    public void runWithEvent(Event event) {
        run();
    }

    /**
     * @param textEditor
     */
    public void setTextEditor(ITextEditor textEditor) {
        this.textEditor = textEditor;
    }

}
