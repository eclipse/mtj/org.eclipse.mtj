/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.properties;

import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.ui.internal.preferences.PreprocessPreferencePage;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPropertyPage;
/**
 *  Property page implementation for J2ME properties associated with
 *  preprocessor settings of a project.
 * @author gma
 *
 */
public class PreprocessorPropertyPage extends PropertyAndPreferencePage implements
        IWorkbenchPropertyPage {

    public PreprocessorPropertyPage() {
        
    }

    /**
     * Embed a preference page into the parent composite, setting things up
     * correctly as we go along.
     * 
     * @param composite
     */
    @Override
    protected void embedPreferencePage(Composite composite) {
        preferencePage = new PreprocessPreferencePage(true, getPreferenceStore());
        preferencePage.createControl(composite);
        preferencePage.getControl().setLayoutData(
                new GridData(GridData.FILL_BOTH));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.properties.PropertyAndPreferencePage#getProjectSpecificSettingsKey()
     */
    @Override
    protected String getProjectSpecificSettingsKey() {
        return IMTJCoreConstants.PREF_PREPROCESS_USE_PROJECT;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.internal.properties.PropertyAndPreferencePage#isReadOnly()
     */
    @Override
    protected boolean isReadOnly() {
        return isPreprocessedOutputProject();
    }

}
