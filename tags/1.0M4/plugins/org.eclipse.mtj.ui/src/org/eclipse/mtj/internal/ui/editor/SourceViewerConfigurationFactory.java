/**
 * Copyright (c) 2006, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.mtj.internal.ui.editor.text.ChangeAwareSourceViewerConfiguration;
import org.eclipse.mtj.internal.ui.editor.text.IColorManager;
import org.eclipse.mtj.internal.ui.editors.l10n.pages.L10nSourcePage;

public class SourceViewerConfigurationFactory {

    public static ChangeAwareSourceViewerConfiguration createSourceViewerConfiguration(
            MTJSourcePage sourcePage, IColorManager colorManager) {

        /* If the editor is a Localization Data source page */
        if (sourcePage instanceof L10nSourcePage) {
            return new XMLConfiguration(colorManager, sourcePage);
        }

        return null;
    }

}
