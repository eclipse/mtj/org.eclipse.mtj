/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     David Marques (Motorola) - Synchronizing locale states with outline.
 */
package org.eclipse.mtj.internal.ui.editors.l10n.details;

import java.util.Locale;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nObject;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.editor.FormEntryAdapter;
import org.eclipse.mtj.internal.ui.editors.l10n.L10nInputContext;
import org.eclipse.mtj.internal.ui.editors.l10n.LocalesTreeSection;
import org.eclipse.mtj.ui.internal.forms.parts.FormEntry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;
import org.eclipse.ui.forms.IFormPart;

/**
 * The details page for the locale definition.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class L10nLocaleDetails extends L10nAbstractDetails {

    private L10nLocale locale;

    /**
     * Country code, a pair of uppercase letters that conforms to ISO-3166. A
     * copy of ISO-3166 can be found <a
     * href="http://www.chemie.fu-berlin.de/diverse/doc/ISO_3166.html">here</a>.
     */
    private FormEntry localeCountryCodeEntry;

    /**
     * Language code, a pair of lowercase letters that conforms to ISO-639. You
     * can find a full list of the ISO-639 codes <a
     * href="http://www.ics.uci.edu/pub/ietf/http/related/iso639.txt">here</a>.
     */
    private FormEntry localeLanguageCodeEntry;

    /**
     * @param masterSection
     */
    public L10nLocaleDetails(LocalesTreeSection masterSection) {
        super(masterSection, L10nInputContext.CONTEXT_ID);
        locale = null;

        localeCountryCodeEntry = null;
        localeLanguageCodeEntry = null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.AbstractFormPart#commit(boolean)
     */
    @Override
    public void commit(boolean onSave) {
        super.commit(onSave);
        // Only required for form entries
        localeCountryCodeEntry.commit();
        localeLanguageCodeEntry.commit();
    }

    /* (non-Javadoc)
     * @see org.eclipse.pde.internal.ui.editor.cheatsheet.CSAbstractDetails#createDetails(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createFields(Composite parent) {
        createLanguageCodeWidget(parent);
        createSpace(parent);
        createCountryCodeWidget(parent);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#hookListeners()
     */
    @Override
    public void hookListeners() {
        createLocaleCountryCodeEntryListeners();
        createLocaleLanguageCodeEntryListeners();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.IPartSelectionListener#selectionChanged(org.eclipse.ui.forms.IFormPart, org.eclipse.jface.viewers.ISelection)
     */
    @Override
    public void selectionChanged(IFormPart part, ISelection selection) {
        // Get the first selected object
        Object object = getFirstSelectedObject(selection);
        // Ensure we have the right type
        if ((object != null) && (object instanceof L10nLocale)) {
            // Set data
            setData((L10nLocale) object);
            // Update the UI given the new data
            updateFields();
        }
    }

    /**
     * @param object
     */
    public void setData(L10nLocale object) {
        // Set data
        locale = object;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#updateFields()
     */
    @Override
    public void updateFields() {
        // Ensure data object is defined
        if (locale != null) {
            updateLocaleCountryCodeEntry(isEditableElement());
            updateLocaleLanguageCodeEntry(isEditableElement());
        }
    }

    /**
     * @param parent
     */
    private void createCountryCodeWidget(Composite parent) {
        localeCountryCodeEntry = new FormEntry(parent, getManagedForm()
                .getToolkit(),
                MTJUIMessages.L10nLocaleDetails_countrycode_label,
                MTJUIMessages.L10nLocaleDetails_choose_btn_label, false);
        // Ensure that the text field has proper width
        localeCountryCodeEntry.getText().setLayoutData(
                new GridData(SWT.FILL, SWT.CENTER, true, false));
    }

    /**
     * @param parent
     */
    private void createLanguageCodeWidget(Composite parent) {
        localeLanguageCodeEntry = new FormEntry(parent, getManagedForm()
                .getToolkit(),
                MTJUIMessages.L10nLocaleDetails_languagecode_label,
                MTJUIMessages.L10nLocaleDetails_choose_btn_label, false);

        // Ensure that the text field has proper width
        localeLanguageCodeEntry.getText().setLayoutData(
                new GridData(SWT.FILL, SWT.CENTER, true, false));
    }

    /**
     * 
     */
    private void createLocaleCountryCodeEntryListeners() {
        localeCountryCodeEntry.setFormEntryListener(new FormEntryAdapter(this) {

            /* (non-Javadoc)
             * @see org.eclipse.mtj.internal.ui.editor.FormEntryAdapter#browseButtonSelected(org.eclipse.mtj.ui.internal.forms.parts.FormEntry)
             */
            @Override
            public void browseButtonSelected(FormEntry entry) {
                handleContryCodeEntryBrowse();
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.internal.ui.editor.FormEntryAdapter#textValueChanged(org.eclipse.mtj.ui.internal.forms.parts.FormEntry)
             */
            @Override
            public void textValueChanged(FormEntry entry) {
                // Ensure data object is defined
                if (locale != null) {
                    {
                        locale
                                .setCountryCode(localeCountryCodeEntry
                                        .getValue());
                        locale.validate();
                    }
                }
            }
        });
    }

    /**
     * 
     */
    private void createLocaleLanguageCodeEntryListeners() {
        localeLanguageCodeEntry
                .setFormEntryListener(new FormEntryAdapter(this) {

                    /* (non-Javadoc)
                     * @see org.eclipse.mtj.internal.ui.editor.FormEntryAdapter#browseButtonSelected(org.eclipse.mtj.ui.internal.forms.parts.FormEntry)
                     */
                    @Override
                    public void browseButtonSelected(FormEntry entry) {
                        handleLanguageCodeEntryBrowse();
                    }

                    /* (non-Javadoc)
                     * @see org.eclipse.mtj.internal.ui.editor.FormEntryAdapter#textValueChanged(org.eclipse.mtj.ui.internal.forms.parts.FormEntry)
                     */
                    @Override
                    public void textValueChanged(FormEntry entry) {
                        // Ensure data object is defined
                        if (locale != null) {
                            {
                                locale.setLanguageCode(localeLanguageCodeEntry
                                        .getValue());
                                locale.validate();
                            }
                        }
                    }
                });
    }

    /**
     * Handle the selection of a folder to be used as output for .properties
     * resources
     */
    private void handleContryCodeEntryBrowse() {
        ElementListSelectionDialog dialog = new ElementListSelectionDialog(
                getPage().getSite().getShell(), new LabelProvider());

        dialog
                .setTitle(MTJUIMessages.L10nLocalesDetails_locationCountryBrowseDialog_title);
        dialog
                .setMessage(MTJUIMessages.L10nLocalesDetails_locationCountryBrowseDialog_message);

        dialog.setElements(Locale.getISOCountries());

        if (dialog.open() == Window.OK) {
            String folder = (String) dialog.getFirstResult();
            localeCountryCodeEntry.setValue(folder);
        }
    }

    /**
     * Handle the selection of a folder to be used as output for .properties
     * resources
     */
    private void handleLanguageCodeEntryBrowse() {
        ElementListSelectionDialog dialog = new ElementListSelectionDialog(
                getPage().getSite().getShell(), new LabelProvider());

        dialog
                .setTitle(MTJUIMessages.L10nLocalesDetails_locationLanguageBrowseDialog_title);
        dialog
                .setMessage(MTJUIMessages.L10nLocalesDetails_locationLanguageBrowseDialog_message);

        dialog.setElements(Locale.getISOLanguages());

        if (dialog.open() == Window.OK) {
            String folder = (String) dialog.getFirstResult();
            localeLanguageCodeEntry.setValue(folder);
        }
    }

    /**
     * @param editable
     */
    private void updateLocaleCountryCodeEntry(boolean editable) {

        localeCountryCodeEntry.setValue(locale.getCountryCode(), true);
        localeCountryCodeEntry.setEditable(editable);
    }

    /**
     * @param editable
     */
    private void updateLocaleLanguageCodeEntry(boolean editable) {
        localeLanguageCodeEntry.setValue(locale.getLanguageCode(), true);
        localeLanguageCodeEntry.setEditable(editable);
    }

    /**
     * @return
     */
    protected L10nObject getDataObject() {
        return locale;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#getDetailsDescription()
     */
    @Override
    protected String getDetailsDescription() {
        return MTJUIMessages.L10nLocaleDetails_detailsDescription;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editors.l10n.details.L10nAbstractDetails#getDetailsTitle()
     */
    @Override
    protected String getDetailsTitle() {
        return MTJUIMessages.L10nLocaleDetails_detailsTitle;
    }
}
