/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant.template;

import java.io.IOException;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.templates.ContextTypeRegistry;
import org.eclipse.jface.text.templates.persistence.TemplateStore;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.ui.editors.text.templates.ContributionContextTypeRegistry;
import org.eclipse.ui.editors.text.templates.ContributionTemplateStore;

/**
 * @author gma
 */
public class PreprocessTemplateAccess {
    /** Key to store custom templates. */
    private static final String CUSTOM_PP_TEMPLATES_KEY = "org.eclipse.mtj.ui.customtemplate"; //$NON-NLS-1$

    private static PreprocessTemplateAccess fInstance;

    /**
     * Returns the shared instance.
     * 
     * @return the shared instance
     */
    public static PreprocessTemplateAccess getDefault() {
        if (fInstance == null) {
            fInstance = new PreprocessTemplateAccess();
        }
        return fInstance;
    }

    private TemplateStore fTemplateStore;

    private ContextTypeRegistry fContextTypeRegistry;

    private PreprocessTemplateAccess() {
    }

    public ContextTypeRegistry getTemplateContextRegistry() {
        if (fContextTypeRegistry == null) {
            ContributionContextTypeRegistry registry = new ContributionContextTypeRegistry();
            registry
                    .addContextType(PreprocessTemplateContextType.PREPROCESS_CONTEXTTYPE);

            fContextTypeRegistry = registry;
        }

        return fContextTypeRegistry;
    }

    /**
     * @return the template store for the preprocess templates
     */
    public TemplateStore getTemplateStore() {
        if (fTemplateStore == null) {
            final IPreferenceStore store = MTJUIPlugin.getDefault()
                    .getPreferenceStore();

            fTemplateStore = new ContributionTemplateStore(
                    getTemplateContextRegistry(), store,
                    CUSTOM_PP_TEMPLATES_KEY);
            try {
                fTemplateStore.load();
            } catch (IOException e) {

            }
            fTemplateStore.startListeningForPreferenceChanges();
        }

        return fTemplateStore;
    }
}
