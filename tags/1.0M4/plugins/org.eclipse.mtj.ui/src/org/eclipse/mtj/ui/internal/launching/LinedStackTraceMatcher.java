/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.ui.internal.launching;

import org.eclipse.mtj.core.internal.launching.StackTraceParser;
import org.eclipse.ui.console.IPatternMatchListener;

/**
 * LinedStackTraceMatcher class finds matches on consoles 
 * for stack traces. Every time it matches a stack trace
 * it parses the stack into a lined stack.
 */
public class LinedStackTraceMatcher extends StackTraceMatcher implements IPatternMatchListener{

    /**
     * Creates a LinedStackTraceMatcher instance associated to
     * the specified line tracker.
     * 
     * @param _tracker MTJConsoleLineTracker instance.
     * @param stackTraceParser 
     */
    public LinedStackTraceMatcher(MTJConsoleLineTracker _tracker, StackTraceParser _parser) {
        super(_tracker, _parser);
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.ui.console.IPatternMatchListener#getPattern()
     */
    public String getPattern() {
        return "(\tat .+[(].*[.java][:]\\d+[)]"+System.getProperty("line.separator")+")+"; //$NON-NLS-2$
    }

}
