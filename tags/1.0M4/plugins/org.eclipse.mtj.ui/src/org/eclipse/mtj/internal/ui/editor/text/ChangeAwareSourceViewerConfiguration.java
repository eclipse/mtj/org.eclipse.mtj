/**
 * Copyright (c) 2005,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.text;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.AbstractInformationControlManager;
import org.eclipse.jface.text.DefaultInformationControl;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.hyperlink.IHyperlinkDetector;
import org.eclipse.jface.text.information.IInformationPresenter;
import org.eclipse.jface.text.information.IInformationProvider;
import org.eclipse.jface.text.information.InformationPresenter;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.reconciler.MonoReconciler;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.mtj.core.model.IBaseModel;
import org.eclipse.mtj.internal.core.text.IReconcilingParticipant;
import org.eclipse.mtj.internal.ui.editor.ISortableContentOutlinePage;
import org.eclipse.mtj.internal.ui.editor.MTJSourcePage;
import org.eclipse.mtj.internal.ui.editor.actions.MTJActionConstants;
import org.eclipse.mtj.internal.ui.editor.outline.QuickOutlinePopupDialog;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.editors.text.TextSourceViewerConfiguration;
import org.eclipse.ui.texteditor.ChainedPreferenceStore;

/**
 * Source viewer configuration for the text editor.
 * 
 * @since 0.9.1
 */
public abstract class ChangeAwareSourceViewerConfiguration extends
        TextSourceViewerConfiguration {

    private InformationPresenter fInfoPresenter;
    private InformationPresenter fOutlinePresenter;
    private MonoReconciler fReconciler;
    protected IColorManager fColorManager;
    protected MTJSourcePage sourcePage;

    /**
     * Creates a new ChangeAwareSourceViewerConfiguration.
     * 
     * @param page
     * @param manager
     */
    public ChangeAwareSourceViewerConfiguration(MTJSourcePage page,
            IColorManager manager) {
        this(page, manager, new ChainedPreferenceStore(new IPreferenceStore[] {
                MTJUIPlugin.getDefault().getPreferenceStore(),
                EditorsUI.getPreferenceStore() // general text editor store
                }));
    }

    /**
     * @param page
     * @param manager - an IColorManager, clients must dispose this themselves.
     * @param store
     */
    public ChangeAwareSourceViewerConfiguration(MTJSourcePage page,
            IColorManager manager, IPreferenceStore store) {
        super(store);
        fColorManager = manager;
        sourcePage = page;
    }

    public abstract void adaptToPreferenceChange(PropertyChangeEvent event);

    public abstract boolean affectsColorPresentation(PropertyChangeEvent event);

    public abstract boolean affectsTextPresentation(PropertyChangeEvent event);

    public abstract void dispose();

    /* (non-Javadoc)
     * @see org.eclipse.ui.editors.text.TextSourceViewerConfiguration#getHyperlinkDetectors(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IHyperlinkDetector[] getHyperlinkDetectors(ISourceViewer sourceViewer) {
        IHyperlinkDetector[] registeredDetectors = super
                .getHyperlinkDetectors(sourceViewer);
        if (registeredDetectors == null) {
            return null;
        }

        if (sourcePage == null) {
            return registeredDetectors;
        }

        IHyperlinkDetector additionalDetector = (IHyperlinkDetector) sourcePage
                .getAdapter(IHyperlinkDetector.class);
        if (additionalDetector == null) {
            return registeredDetectors;
        }

        IHyperlinkDetector[] allDetectors = new IHyperlinkDetector[registeredDetectors.length + 1];
        System.arraycopy(registeredDetectors, 0, allDetectors, 0,
                registeredDetectors.length);
        allDetectors[registeredDetectors.length] = additionalDetector;
        return allDetectors;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.SourceViewerConfiguration#getInformationPresenter(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IInformationPresenter getInformationPresenter(
            ISourceViewer sourceViewer) {
        if (sourcePage == null) {
            return null;
        }
        if ((fInfoPresenter == null)
                && (getInfoImplementationType() != SourceInformationProvider.NO_IMP)) {
            IInformationControlCreator icc = getInformationControlCreator(false);
            fInfoPresenter = new InformationPresenter(icc);
            fInfoPresenter
                    .setDocumentPartitioning(getConfiguredDocumentPartitioning(sourceViewer));

            // Register information provider
            IInformationProvider provider = new SourceInformationProvider(
                    sourcePage, icc, getInfoImplementationType());
            String[] contentTypes = getConfiguredContentTypes(sourceViewer);
            for (String contentType : contentTypes) {
                fInfoPresenter.setInformationProvider(provider, contentType);
            }

            fInfoPresenter.setSizeConstraints(60, 10, true, true);
        }
        return fInfoPresenter;
    }

    /**
     * @param sourceViewer
     * @return
     */
    public IInformationPresenter getOutlinePresenter(ISourceViewer sourceViewer) {
        // Ensure the source page is defined
        if (sourcePage == null) {
            return null;
        }
        // Reuse the old outline presenter
        if (fOutlinePresenter != null) {
            return fOutlinePresenter;
        }
        // Define a new outline presenter
        fOutlinePresenter = new InformationPresenter(
                getOutlinePresenterControlCreator(sourceViewer,
                        MTJActionConstants.COMMAND_ID_QUICK_OUTLINE));
        fOutlinePresenter
                .setDocumentPartitioning(getConfiguredDocumentPartitioning(sourceViewer));
        fOutlinePresenter
                .setAnchor(AbstractInformationControlManager.ANCHOR_GLOBAL);
        // Define a new outline provider
        IInformationProvider provider = new MTJSourceInfoProvider(sourcePage);
        // Set the provider on all defined content types
        String[] contentTypes = getConfiguredContentTypes(sourceViewer);
        for (String contentType : contentTypes) {
            fOutlinePresenter.setInformationProvider(provider, contentType);
        }
        // Set the presenter size constraints
        fOutlinePresenter.setSizeConstraints(50, 20, true, false);

        return fOutlinePresenter;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.editors.text.TextSourceViewerConfiguration#getReconciler(org.eclipse.jface.text.source.ISourceViewer)
     */
    @Override
    public IReconciler getReconciler(ISourceViewer sourceViewer) {
        if ((sourcePage != null) && (fReconciler == null)) {
            IBaseModel model = sourcePage.getInputContext().getModel();
            if (model instanceof IReconcilingParticipant) {
                ReconcilingStrategy strategy = new ReconcilingStrategy();
                strategy.addParticipant((IReconcilingParticipant) model);
                ISortableContentOutlinePage outline = sourcePage
                        .getContentOutline();
                if (outline instanceof IReconcilingParticipant) {
                    strategy.addParticipant((IReconcilingParticipant) outline);
                }
                fReconciler = new MonoReconciler(strategy, false);
                fReconciler.setDelay(500);
            }
        }
        return fReconciler;
    }

    /**
     * Returns the outline presenter control creator. The creator is a factory
     * creating outline presenter controls for the given source viewer.
     * 
     * @param sourceViewer the source viewer to be configured by this
     *            configuration
     * @param commandId the ID of the command that opens this control
     * @return an information control creator
     */
    private IInformationControlCreator getOutlinePresenterControlCreator(
            ISourceViewer sourceViewer, final String commandId) {
        return new IInformationControlCreator() {
            public IInformationControl createInformationControl(Shell parent) {
                int shellStyle = SWT.RESIZE;
                QuickOutlinePopupDialog dialog = new QuickOutlinePopupDialog(
                        parent, shellStyle, sourcePage, sourcePage);
                return dialog;
            }
        };
    }

    /**
     * @return
     */
    protected int getInfoImplementationType() {
        return SourceInformationProvider.NO_IMP;
    }

    /**
     * @param cutDown
     * @return
     */
    protected IInformationControlCreator getInformationControlCreator(
            final boolean cutDown) {
        return new IInformationControlCreator() {
            public IInformationControl createInformationControl(Shell parent) {
                return new DefaultInformationControl(parent);
            }
        };
    }

}
