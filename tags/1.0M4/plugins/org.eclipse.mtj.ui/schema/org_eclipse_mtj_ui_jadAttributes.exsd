<?xml version='1.0' encoding='UTF-8'?>
<!-- Schema file written by PDE -->
<schema targetNamespace="org.eclipse.mtj.ui">
<annotation>
      <appInfo>
         <meta.schema plugin="org.eclipse.mtj.ui" id="venderSpecJADAttributes" name="Vender Specific JAD Attributes"/>
      </appInfo>
      <documentation>
         Provides an extension point to add the some JAD attributes to a specific JAD editor page.
      </documentation>
   </annotation>

   <element name="extension">
      <complexType>
         <sequence>
            <element ref="jadAttributes"/>
         </sequence>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  a fully qualified identifier of the target extension point
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  an optional identifier of the extension instance
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  an optional name of the extension instance
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="jadAttributes">
      <complexType>
         <sequence minOccurs="1" maxOccurs="unbounded">
            <element ref="jadDescriptorsProvider"/>
         </sequence>
         <attribute name="pageID" type="string" use="required">
            <annotation>
               <documentation>
                  indicate which JAD editor page the attributes will be shown.
currently, there are 3 pages you can add:
&lt;p&gt;
Page name:Required, pageID:required
&lt;p&gt;
Page name:Optional, pageID:optional
&lt;p&gt;
Page name:Over the Air, pageID:ota
               </documentation>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="jadDescriptorsProvider">
      <complexType>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  the required implementation class for the &lt;code&gt;org.eclipse.mtj.ui.jadEditor.IJADDescriptorsProvider&lt;/code&gt; interface that will be used to provide JAD Attributes descriptors.
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.mtj.ui.jadEditor.IJADDescriptorsProvider"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         [Enter the first release in which this extension point appears.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="examples"/>
      </appInfo>
      <documentation>
         an extension example that add some JAD attributes to the required page:
   &lt;p&gt;
&lt;pre&gt; 
   &lt;extension
          point=&quot;org.eclipse.mtj.ui.jadAttributes&quot;&gt;
       &lt;jadAttributes pageID=&quot;required&quot;&gt;
         &lt;jadDescriptorsProvider
          class=&quot;org.eclipse.mtj.ui.internal.editor.jad.RequiredJADDesciptorsProvider&quot;/&gt;
       &lt;/jadAttributes&gt;
    &lt;/extension&gt;
   
   &lt;/pre&gt;
&lt;/p&gt;
   an extension example that add some motorola specific JAD attributes to the OTA page:
  
  &lt;p&gt;
&lt;pre&gt; 
   &lt;extension
          point=&quot;org.eclipse.mtj.ui.jadAttributes&quot;&gt;
       &lt;jadAttributes 
         pageID=&quot;OTA&quot;
         vendorSpec=&quot;true&quot;
         vendorSpecTester=&quot;org.eclipse.mtj.toolkit.uei.jadEditor.MotoSpecTester&quot;&gt;
         &lt;jadDescriptorsProvider
          class=&quot;org.eclipse.mtj.toolkit.uei.jadEditor.MotoJADDesciptorsProvider&quot;/&gt;
       &lt;/jadAttributes&gt;
    &lt;/extension&gt;
   
   &lt;/pre&gt;
&lt;/p&gt;
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="apiInfo"/>
      </appInfo>
      <documentation>
         [Enter API information here.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="implementation"/>
      </appInfo>
      <documentation>
         [Enter information about supplied implementation of this extension point.]
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="copyright"/>
      </appInfo>
      <documentation>
         
      </documentation>
   </annotation>

</schema>
