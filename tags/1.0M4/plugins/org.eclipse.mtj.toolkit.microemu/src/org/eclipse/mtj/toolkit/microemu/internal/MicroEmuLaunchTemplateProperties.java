/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.toolkit.microemu.internal;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public enum MicroEmuLaunchTemplateProperties {

    /**
     * 
     */
    TOOLKITROOT("toolkitroot"),

    /**
     * 
     */
    SKINJARFILE("skinJarFile"),

    /**
     * 
     */
    SKINPATHINJAR("skinPathInJar"),

    /**
     * 
     */
    PATHSEPARATOR("pathSeparator");

    /**
     * 
     */
    private String property;

    /**
     * @param property
     */
    MicroEmuLaunchTemplateProperties(final String property) {
        this.property = property;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return property;
    }
}
