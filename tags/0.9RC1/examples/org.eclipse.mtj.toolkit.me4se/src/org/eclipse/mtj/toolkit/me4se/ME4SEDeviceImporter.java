/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.toolkit.me4se;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.importer.IDeviceImporter;
import org.eclipse.mtj.core.importer.LibraryImporter;
import org.eclipse.mtj.core.importer.impl.JavaEmulatorDeviceImporter;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.Classpath;
import org.eclipse.mtj.core.model.ReplaceableParametersProcessor;
import org.eclipse.mtj.core.model.Version;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.impl.VMInstallClasspath;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.library.api.API;
import org.eclipse.mtj.core.model.library.api.APIType;
import org.eclipse.mtj.toolkit.me4se.internal.Activator;

/**
 * An {@link IDeviceImporter} implementation that recognizes and imports ME4SE
 * toolkit instances.
 * 
 * @author Craig Setera
 */
public class ME4SEDeviceImporter extends JavaEmulatorDeviceImporter {
    // Properties file holding emulator/device information
    private static final String PROPS_FILE = "me4se.properties";

    // Various pieces of static information
    private static final String EMULATOR_JAR_PREFIX = "me4se";
    private static final String EMULATOR_JAR_SUFFIX = ".jar";

    /**
     * @see org.eclipse.mtj.core.importer.IDeviceImporter#getMatchingDevices(java.io.File,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public IDevice[] getMatchingDevices(File directory, IProgressMonitor monitor) {
        IDevice[] devices = null;

        try {
            File[] files = directory.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.startsWith(EMULATOR_JAR_PREFIX)
                            && name.endsWith(EMULATOR_JAR_SUFFIX);
                }
            });

            // We should probably look more deeply into the jar, but this should
            // do for now.
            if (files.length > 0) {
                IDevice device = createDevice(files[0]);
                devices = new IDevice[] { device };
            }
        } catch (Exception e) {
            MTJCorePlugin.log(IStatus.WARNING,
                    "Error importing Microemu device", e);
        }

        return devices;
    }

    /**
     * Get the device classpath based on the specified player.jar file.
     * 
     * @param jarFile
     * @return
     */
    private Classpath getDeviceClasspath(File jarFile) {
        VMInstallClasspath classpath = new VMInstallClasspath();
        classpath.setPrependVMLibraries(false);

        addDeviceLibraries(jarFile, classpath, new LibraryImporter());

        return classpath;
    }

    /**
     * @see org.eclipse.mtj.core.importer.impl.JavaEmulatorDeviceImporter#getDevicePropertiesURL()
     */
    protected URL getDevicePropertiesURL() {
        return Activator.getDefault().getBundle().getEntry(PROPS_FILE);
    }

    /**
     * Add the device libraries.
     * 
     * @param jarFile
     * @param classpath
     * @param importer
     */
    private void addDeviceLibraries(File jarFile, Classpath classpath,
            LibraryImporter importer) {
        // Now add the player libraries
        String classpathString = getDeviceProperties().getProperty("classpath",
                "");

        Map<String, File> replaceableParameters = new HashMap<String, File>();
        replaceableParameters.put("me4sejar", jarFile);

        classpathString = ReplaceableParametersProcessor
                .processReplaceableValues(classpathString,
                        replaceableParameters);
        String[] entries = classpathString.split(";");

        for (int i = 0; i < entries.length; i++) {
            ILibrary library = importer.createLibraryFor(new File(entries[i]));

            // Because of the structure of the libraries,
            // we need to hardcode the CLDC and MIDP libraries
            API api = library.getAPI(APIType.CONFIGURATION);
            if (api != null) {
                api.setVersion(new Version("1.1"));
            }
            api = library.getAPI(APIType.PROFILE);
            if (api != null) {
                api.setVersion(new Version("2.0"));
            }

            classpath.addEntry(library);
        }
    }

    /**
     * Create the new device instance for the specified Microemulator jar file.
     * 
     * @param jarFile
     * @return
     */
    private IDevice createDevice(File jarFile) {
        ME4SEDevice device = new ME4SEDevice();

        device.setBundle(Activator.getDefault().getBundle().getSymbolicName());
        device.setClasspath(getDeviceClasspath(jarFile));
        device.setDebugServer(isDebugServer());
        device.setDescription("ME4SE Device");
        device.setDeviceProperties(new Properties());
        device.setGroupName("ME4SE");
        device.setName("ME4SE");
        device.setPreverifier(getPreverifier(jarFile));
        device.setProtectionDomains(new String[0]);
        device.setLaunchCommandTemplate(getLaunchCommand());
        device.setJarFile(jarFile);

        return device;
    }
}
