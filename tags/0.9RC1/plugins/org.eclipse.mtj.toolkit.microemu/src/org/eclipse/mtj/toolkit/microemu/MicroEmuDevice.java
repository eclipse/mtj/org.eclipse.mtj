/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Added skin atribute for device  
 *     Feng Wang (Sybase) - Modify to fit for AbstractDevice#copyForLaunch(...)
 *                          signature changing.                       
 */
package org.eclipse.mtj.toolkit.microemu;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.launching.ILaunchConstants;
import org.eclipse.mtj.core.model.LaunchEnvironment;
import org.eclipse.mtj.core.model.ReplaceableParametersProcessor;
import org.eclipse.mtj.core.model.device.impl.JavaEmulatorDevice;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.toolkit.microemu.internal.MicoEmuConstants;
import org.eclipse.mtj.toolkit.microemu.internal.MicroEmuDeviceSkin;

/**
 * Device implementation for the MicroEmulator SDK devices.
 * 
 * @author Craig Setera
 */
public class MicroEmuDevice extends JavaEmulatorDevice {

    /**
     * The SDK root folder
     */
    private File root;

    /**
     * The device Skin
     */
    private MicroEmuDeviceSkin skin;

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.device.IDevice#getLaunchCommand(org.eclipse.mtj.core.model.LaunchEnvironment,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException {
        IMidletSuiteProject midletSuite = launchEnvironment.getMidletSuite();
        ILaunchConfiguration launchConfiguration = launchEnvironment
        		.getLaunchConfiguration();
        boolean launchFromJAD = shouldDirectLaunchJAD(launchConfiguration);
        
        File tempDeployed = copyForLaunch(midletSuite, monitor, launchFromJAD);

        // Fill in our properties so we can use them for resolution
        // against the raw executable command line
        Map<String, Comparable<?>> executionProperties = new HashMap<String, Comparable<?>>();

        // Adding launcher information
        executionProperties.put(MicoEmuConstants.EXECUTABLE_PROPERTY_KEY,
                getJavaExecutable());

        // Adding sdk root folder information
        executionProperties
                .put(MicoEmuConstants.TOOLKITROOT_PROPERTY_KEY, root);

        // Adding skin information
        executionProperties.put(MicoEmuConstants.SKINJARFILE_PROPERTY_KEY, skin
                .getJarFile());

        executionProperties.put(MicoEmuConstants.SKINPATHINJAR_PROPERTY_KEY,
                skin.getPath());

        // Adding java path separator information
        executionProperties.put(MicoEmuConstants.PATHSEPARATOR_PROPERTY_KEY,
                System.getProperty("path.separator"));

        // Adding debug information
        if (launchEnvironment.isDebugLaunch()) {
            executionProperties.put(MicoEmuConstants.DEBUG_PORT_PROPERTY_KEY,
                    new Integer(launchEnvironment.getDebugListenerPort()));
        }

        // Adding launch configuration values
        executionProperties.put(
                MicoEmuConstants.USER_SPECIFIED_ARGUMENTS_PROPERTY_KEY,
                launchConfiguration.getAttribute(
                        ILaunchConstants.LAUNCH_PARAMS, ""));

        if (launchFromJAD) {
            executionProperties.put(MicoEmuConstants.JADFILE_PROPERTY_KEY,
                    getSpecifiedJadURL(launchConfiguration));
        } else {
            File jadFile = getJadForLaunch(midletSuite, tempDeployed, monitor);
            if (jadFile.exists()) {
                executionProperties.put(MicoEmuConstants.JADFILE_PROPERTY_KEY,
                        jadFile.toString());
            }
        }

        // Do the property resolution given the previous information
        return ReplaceableParametersProcessor.processReplaceableValues(
                launchCommandTemplate, executionProperties);
    }

    /**
     * Return the SDK root directory.
     * 
     * @return Returns the root.
     */
    public File getRoot() {
        return root;
    }

    /**
     * Return the device skin
     * 
     * @return the skin
     */
    public MicroEmuDeviceSkin getSkin() {
        return skin;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.impl.AbstractDevice#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.loadUsing(persistenceProvider);

        /* Looding the sdk root directory */
        String rootString = persistenceProvider
                .loadString(MicoEmuConstants.TOOLKITROOT_PERSISTENCE_KEY);
        root = new File(rootString);

        /* Loading the skin information */
        String skinJarFile = persistenceProvider
                .loadString(MicoEmuConstants.SKIN_JAR_PERSISTENCE_KEY);
        String skinPathInFile = persistenceProvider
                .loadString(MicoEmuConstants.SKIN_PATH_PERSISTENCE_KEY);
        String skinName = persistenceProvider
                .loadString(MicoEmuConstants.SKIN_NAME_PERSISTENCE_KEY);

        skin = new MicroEmuDeviceSkin(skinJarFile, skinPathInFile, skinName);

    }

    /**
     * Set the SDK root directory
     * 
     * @param mppRoot The mppRoot to set.
     */
    public void setRoot(File mppRoot) {
        this.root = mppRoot;
    }

    /**
     * Set the device skin
     * 
     * @param skin the skin to set
     */
    public void setSkin(MicroEmuDeviceSkin skin) {
        this.skin = skin;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.model.impl.AbstractDevice#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.storeUsing(persistenceProvider);

        /* Saving the sdk root directory */
        persistenceProvider.storeString(
                MicoEmuConstants.TOOLKITROOT_PERSISTENCE_KEY, root.toString());

        /* Saving skin information */
        persistenceProvider.storeString(
                MicoEmuConstants.SKIN_JAR_PERSISTENCE_KEY, skin.getJarFile());
        persistenceProvider.storeString(
                MicoEmuConstants.SKIN_PATH_PERSISTENCE_KEY, skin.getPath());
        persistenceProvider.storeString(
                MicoEmuConstants.SKIN_NAME_PERSISTENCE_KEY, skin.getName());

    }
}
