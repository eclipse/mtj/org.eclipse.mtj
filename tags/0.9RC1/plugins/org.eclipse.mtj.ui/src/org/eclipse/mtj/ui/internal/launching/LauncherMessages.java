/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/
package org.eclipse.mtj.ui.internal.launching;

import org.eclipse.osgi.util.NLS;

public class LauncherMessages extends NLS {

    private static final String BUNDLE_NAME = "org.eclipse.mtj.ui.internal.launching.messages";//$NON-NLS-1$

    public static String launch_configSelection_title;
    public static String launch_configSelection_message;
    public static String MidletLaunching_EditorContainsNoMidlet;
    public static String MidletLaunching_SelectionContainsNoMidlet;
    public static String MidletLaunching_SelectionDialogTitle;
    public static String JavaLaunchShortcut_0;
    public static String JavaLaunchShortcut_1;
    public static String JavaLaunchShortcut_2;
    public static String JavaLaunchShortcut_3;
    public static String MainMethodLabelProvider_0;
    public static String JavaMainTab_Choose_a_main__type_to_launch__12;

    static {
        // load message values from bundle file
        NLS.initializeMessages(BUNDLE_NAME, LauncherMessages.class);
    }
}
