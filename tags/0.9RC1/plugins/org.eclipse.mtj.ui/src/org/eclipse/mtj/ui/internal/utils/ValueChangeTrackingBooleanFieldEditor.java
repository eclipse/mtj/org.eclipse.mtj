/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Fixed problems with the doLoadDefault() method                         
 */
package org.eclipse.mtj.ui.internal.utils;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.widgets.Composite;

/**
 * A BooleanFieldEditor subclass that enables and disables another
 * StringFieldEditor based on the state of the boolean field.
 * 
 * @author Craig Setera
 */
public class ValueChangeTrackingBooleanFieldEditor extends BooleanFieldEditor {

    private Composite parent;
    private StringFieldEditor fieldEditor;

    /**
     * Construct a new tracking field editor.
     * 
     * @param name the name of the preference this field editor works on
     * @param label the label text of the field editor
     * @param parent the parent of the field editor's control
     */
    public ValueChangeTrackingBooleanFieldEditor(String name, String label,
            Composite parent) {
        super(name, label, parent);
        this.parent = parent;
    }

    /**
     * Get the field editor
     * 
     * @return the field editor for a string type preference.
     */
    public StringFieldEditor getFieldEditor() {
        return fieldEditor;
    }

    /**
     * Set the field editor
     * 
     * @param editor A field editor for a string type preference.
     */
    public void setFieldEditor(StringFieldEditor editor) {
        fieldEditor = editor;
    }

    /**
     * Update the enablement of the field editor controls based on the current
     * state of the boolean values.
     */
    private void updateEditorEnablement() {
        if (fieldEditor != null) {
            boolean value = getBooleanValue();
            fieldEditor.getLabelControl(parent).setEnabled(value);
            fieldEditor.getTextControl(parent).setEnabled(value);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.BooleanFieldEditor#doLoad()
     */
    @Override
    protected void doLoad() {
        super.doLoad();
        updateEditorEnablement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.BooleanFieldEditor#doLoadDefault()
     */
    @Override
    protected void doLoadDefault() {
        super.doLoadDefault();
        updateEditorEnablement();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.BooleanFieldEditor#valueChanged(boolean,
     *      boolean)
     */
    @Override
    protected void valueChanged(boolean oldValue, boolean newValue) {
        super.valueChanged(oldValue, newValue);
        updateEditorEnablement();
    }
}
