/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Hugo Raniere (Motorola)  - Wizard class must be aware of device changes 
 *                                to handle the classpath correctly
 *     Diego Sandin (Motorola)  - Fixed invalid JAD file name checking (Bug#244543).                                  
 */
package org.eclipse.mtj.ui.internal.wizards.project.page;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.project.impl.MidletSuiteProject;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.ui.IMTJUIConstants;
import org.eclipse.mtj.ui.devices.DeviceSelector;
import org.eclipse.mtj.ui.internal.wizards.project.NewMIDLetProjectWizard;
import org.eclipse.mtj.ui.internal.wizards.project.NewMidletProjectWizardMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * Wizard page used to define the initial properties for the created MIDlet
 * project.
 * 
 * @author Craig Setera
 */
public class NewMidletProjectPropertiesPage extends WizardPage {

    /**
     * The key used to register this page in the wizard
     */
    public static final String PAGE_NAME = "NewMidletProjectProperties"; //$NON-NLS-1$

    // Widget variables
    private DeviceSelector deviceSelector;
    private Text jadFileNameText;
    private boolean jadNameInitialized;

    private int deviceCount = 0;

    /**
     * Construct a NewMidletProjectPropertiesPage instance.
     */
    public NewMidletProjectPropertiesPage() {
        super(PAGE_NAME);

        /* Setting the page title and description */
        setTitle(NewMidletProjectWizardMessages.NewMidletProjectPropertiesPage_title);
        setDescription(NewMidletProjectWizardMessages.NewMidletProjectPropertiesPage_description);

        deviceCount = getDeviceCount();

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        Font font = parent.getFont();

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setFont(font);
        setControl(composite);
        initializeDialogUnits(parent);

        composite.setLayout(new GridLayout(1, false));
        composite.setLayoutData(new GridData(GridData.FILL_BOTH));

        // Create the device selection controls
        deviceSelector = new DeviceSelector();
        deviceSelector.createContents(composite, true, true);
        deviceSelector
                .setSelectionChangedListener(new ISelectionChangedListener() {
                    public void selectionChanged(SelectionChangedEvent event) {
                        updateStatus();
                    }
                });

        Group group = new Group(composite, SWT.FILL);
        group
                .setText(NewMidletProjectWizardMessages.NewMidletProjectPropertiesPage_jad_groupname);

        group.setLayout(new GridLayout(3, false));
        group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        new Label(group, SWT.NONE);
        (new Label(group, SWT.NONE))
                .setText(NewMidletProjectWizardMessages.NewMidletProjectPropertiesPage_jad_label);
        new Label(group, SWT.NONE);
        new Label(group, SWT.NONE);
        jadFileNameText = new Text(group, SWT.BORDER);
        jadFileNameText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        jadFileNameText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                updateStatus();
            }
        });
        new Label(group, SWT.NONE);
        updateStatus();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.DialogPage#dispose()
     */
    @Override
    public void dispose() {
        deviceSelector.dispose();
        super.dispose();
    }

    /**
     * Return the JAD file name specified by the user.
     * 
     * @return
     */
    public String getJadFileName() {
        return jadFileNameText.getText();
    }

    /**
     * Return the device selected by the user.
     * 
     * @return
     */
    public IDevice getSelectedDevice() {
        return deviceSelector.getSelectedDevice();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.wizard.WizardPage#isPageComplete()
     */
    @Override
    public boolean isPageComplete() {

        return ((isValidDeviceCount().isOK())
                && (isValidDeviceSelection().isOK()) && (isValidJadName()
                .isOK()));

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.dialogs.DialogPage#setVisible(boolean)
     */
    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            if (!jadNameInitialized) {
                jadFileNameText.setText(getDefaultJadName());
                jadNameInitialized = true;
            }
        }

        super.setVisible(visible);
    }

    /**
     * Return the default JAD file name, based in the project name.
     * 
     * @return
     */
    private String getDefaultJadName() {
        IProject project = ((NewMIDLetProjectWizard) getWizard())
                .getCreatedProjectHandle();
        return MidletSuiteProject.getDefaultJadFileName(project);
    }

    /**
     * Return the number of devices available.
     * 
     * @return the number of devices available.
     */
    private int getDeviceCount() {
        int count = 0;

        /* Check the number of devices already registered */
        DeviceRegistry registry = DeviceRegistry.singleton;

        try {
            count = registry.getDeviceCount();
        } catch (PersistenceException e) {
            MTJCorePlugin.log(IStatus.WARNING, "Error retrieving device count", //$NON-NLS-1$
                    e);
        }
        return count;
    }

    /**
     * Check if at least one device was previously imported by user.
     * 
     * @return a status object with code IStatus.OK if at least one device is
     *         available, otherwise a status object indicating that no device is
     *         was previously imported.
     */
    private IStatus isValidDeviceCount() {
        IStatus status = null;

        if (deviceCount <= 0) {
            status = new Status(
                    IStatus.ERROR,
                    IMTJUIConstants.PLUGIN_ID,
                    NewMidletProjectWizardMessages.NewMidletProjectPropertiesPage_validate_devicecount_error);

        } else {
            status = new Status(IStatus.OK, IMTJUIConstants.PLUGIN_ID, "");
        }

        return status;
    }

    /**
     * Check if the user selected a device.
     * 
     * @return a status object with code IStatus.OK if one device is selected ,
     *         otherwise a status object indicating that no device is selected.
     */
    private IStatus isValidDeviceSelection() {
        IStatus status = null;

        if (getSelectedDevice() == null) {
            status = new Status(
                    IStatus.ERROR,
                    IMTJUIConstants.PLUGIN_ID,
                    NewMidletProjectWizardMessages.NewMidletProjectPropertiesPage_validate_devicecount_error);
        } else {
            status = new Status(IStatus.OK, IMTJUIConstants.PLUGIN_ID, "");
        }

        return status;
    }

    /**
     * Validates the JAD file name.
     * 
     * @return a status object with code IStatus.OK if the JAD file name is
     *         valid , otherwise a status object indicating what is wrong with
     *         the file name
     * @see IWorkspace#validateName(String, int)
     */
    private IStatus isValidJadName() {

        final String jadFileName = getJadFileName();

        IWorkspace workspace = ResourcesPlugin.getWorkspace();

        IStatus result = new Status(
                IStatus.ERROR,
                IMTJUIConstants.PLUGIN_ID,
                NewMidletProjectWizardMessages.NewMidletProjectPropertiesPage_validate_jadname_error_invalidname);

        if (jadFileName != null) {
            result = workspace.validateName(jadFileName, IResource.FILE);

            if (result.isOK()) {
                /* File name must end with the .jad file extension */
                if (!jadFileName.endsWith(".jad")) {
                    result = new Status(
                            IStatus.ERROR,
                            IMTJUIConstants.PLUGIN_ID,
                            NewMidletProjectWizardMessages.NewMidletProjectPropertiesPage_validate_jadname_error_extension);
                    /* File name must not be only the .jad file extension */
                } else if (jadFileName.equals(".jad")) {
                    result = new Status(
                            IStatus.ERROR,
                            IMTJUIConstants.PLUGIN_ID,
                            NewMidletProjectWizardMessages.NewMidletProjectPropertiesPage_validate_jadname_error_emptyname);
                }
            }
        }
        return result;
    }

    /**
     * Set the status based on the current values on the page.
     */
    private void updateStatus() {
        IStatus status;

        /* Update the device count */
        deviceCount = getDeviceCount();

        status = isValidDeviceCount();

        if (status.isOK()) {
            status = isValidDeviceSelection();
            if (status.isOK()) {
                status = isValidJadName();
            }
        }

        if (status.isOK()) {
            setErrorMessage(null);
            setPageComplete(true);
            ((NewMIDLetProjectWizard) getWizard())
                    .setProjectDevice(getSelectedDevice());
        } else {
            setErrorMessage(status.getMessage());
            setPageComplete(false);
        }

    }

}
