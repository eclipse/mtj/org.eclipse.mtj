/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.ui.internal.editors;

import org.eclipse.osgi.util.NLS;

/**
 * Provides convenience methods for manipulating messages.
 * 
 * @author Diego Madruga Sandin
 */
public class EditorsUIContent extends NLS {

    public static String overviewPage_requiredsection_title;
    public static String overviewPage_requiredsection_description;

    public static String overviewPage_runtimesection_title;
    public static String overviewPage_runtimesection_description;

    public static String overviewPage_deploying;
    public static String overviewPage_exporting;

    public static String overviewPage_launchsection_runlinks;
    public static String overviewPage_launchsection_debuglinks;

    public static String buttonBarBlock_button_add;
    public static String buttonBarBlock_button_remove;
    public static String buttonBarBlock_button_up;
    public static String buttonBarBlock_button_down;

    /**
     * The base name of the MTJ resource bundle
     */
    private static final String BUNDLE_NAME = "org.eclipse.mtj.ui.internal.editors."
            + "EditorsUIContent";

    static {
        // load message values from bundle file
        NLS.initializeMessages(BUNDLE_NAME, EditorsUIContent.class);
    }

}
