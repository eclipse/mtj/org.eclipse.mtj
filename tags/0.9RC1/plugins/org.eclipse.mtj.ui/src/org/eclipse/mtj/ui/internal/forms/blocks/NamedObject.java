/*******************************************************************************
 * Copyright (c) 2000, 2004 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.mtj.ui.internal.forms.blocks;

import org.eclipse.mtj.ui.editors.models.IModel;
import org.eclipse.mtj.ui.editors.models.IModelListener;

/**
 *
 */
public class NamedObject {

    /**
     * 
     */
    private String name;

    /**
     * 
     */
    protected IModel model;

    /**
     * @param name
     */
    public NamedObject(String name) {
        this.name = name;
    }

    /**
     * @param model
     */
    public void setModel(IModel model) {
        this.model = model;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return getName();
    }

    /**
     * @param name
     */
    public void setName(String name, boolean notify) {
        this.name = name;
        if (notify) {
            model.fireModelChanged(new Object[] { this },
                    IModelListener.CHANGED, "");
        }
    }
}
