/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)  - Initial implementation
 *     Kevin Hunter (Individual) - Added signature support
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse
 *                                 standards
 *     Gang  Ma	(Sybase)         - Change page validation to follow eclipse 
 *     				   UI guide
 */
package org.eclipse.mtj.ui.internal.properties;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.signing.SignatureUtils;
import org.eclipse.mtj.core.model.jad.IJadSignature;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.core.model.sign.ISignatureProperties;
import org.eclipse.mtj.core.nature.J2MENature;
import org.eclipse.mtj.core.signing.SignatureProperties;
import org.eclipse.mtj.ui.IMTJUIConstants;
import org.eclipse.mtj.ui.MTJUIErrors;
import org.eclipse.mtj.ui.MTJUIStrings;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPropertyPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.dialogs.PropertyPage;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

/**
 * Property page implementation for Java ME properties associated with signing
 * the project.
 * 
 * @author Craig Setera
 * @author Kevin Hunter
 * @see PropertyPage
 */
public class J2MESigningPropertiesPage extends PropertyPage implements
        IWorkbenchPropertyPage {

    private static final String SIGNING_GROUP_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.SigningProperties"); //$NON-NLS-1$

    private static final String SIGN_PROJECT_CHECK_BUTTON_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.SignProject"); //$NON-NLS-1$
    private static final String KEYSTORE_LABEL_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.KeystoreFile"); //$NON-NLS-1$
    private static final String EXTERNAL_BROWSE_BUTTON_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.BrowseExternal"); //$NON-NLS-1$
    private static final String INTERNAL_BROWSE_BUTTON_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.BrowseInternal"); //$NON-NLS-1$
    private static final String ALIAS_LABEL_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.KeyAlias"); //$NON-NLS-1$

    private static final String PROMPT_FOR_PWD_LABEL_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.PromptForPasswords"); //$NON-NLS-1$
    private static final String STORE_PWD_KEYRING_LABEL_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.SavePasswordsInKeyring"); //$NON-NLS-1$

    private static final String STORE_PWD_PROJECT_LABEL_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.SavePasswordsInProject"); //$NON-NLS-1$
    private static final String KEYSTORE_PWD_LABEL_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.KeystorePassword"); //$NON-NLS-1$
    private static final String KEY_PWD_LABEL_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.KeyPassword"); //$NON-NLS-1$

    private static final String TEST_BUTTON_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.VerifySettings"); //$NON-NLS-1$
    private static final String ADVANCED_GROUP_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.AdvancedSettings"); //$NON-NLS-1$

    private static final String ADVANCED_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.LeaveEmptyForDefaults"); //$NON-NLS-1$
    private static final String PROVIDER_LABEL_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.CryptoProvider"); //$NON-NLS-1$

    private static final String KEYSTORE_TYPE_LABEL_TEXT = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.KeystoreType"); //$NON-NLS-1$
    private static final String ERROR_MISSING_KEYSTORE = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.ErrKeystoreFileRequired"); //$NON-NLS-1$
    private static final String ERROR_NOSUCH_KEYSTORE = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.ErrKeystoreDoesntExist"); //$NON-NLS-1$
    private static final String ERROR_INVALID_KEYSTORE = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.ErrInvalidKeystore"); //$NON-NLS-1$
    private static final String ERROR_MISSING_ALIAS = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.ErrAliasRequired"); //$NON-NLS-1$
    private static final String ERROR_MISSING_STOREPASS = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.KeystorePassRequired"); //$NON-NLS-1$

    private static final String ERROR_MISSING_KEYPASS = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.KeyPassRequired"); //$NON-NLS-1$

    private static final String TEST_SUCCESS = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.SettingsVerified"); //$NON-NLS-1$

    private static final String BROWSE_DIALOG_TITLE = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.BrowseForKeystore"); //$NON-NLS-1$
    private static final String BROWSE_DIALOG_MESSAGE = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.BrowseForKeystoreMsg"); //$NON-NLS-1$

    // currently valid

    private static final String BROWSE_NEED_FILE = MTJUIStrings
            .getString("J2MEProjectPropertiesPage.BrowseForKeystoreNeedFile"); //$NON-NLS-1$

    private static final String[] BROWSE_FILTER_NAMES = {
            MTJUIStrings
                    .getString("J2MEProjectPropertiesPage.FilterNameKeystores"), //$NON-NLS-1$
            MTJUIStrings
                    .getString("J2MEProjectPropertiesPage.FilterNameAllFiles") //$NON-NLS-1$
    };

    private static final String[] BROWSE_FILTER_EXTENSIONS = { "*.ks", //$NON-NLS-1$
            "*.*" //$NON-NLS-1$
    };

    private Button signProjectCheckButton; // "Sign project?" checkbox

    private Label keyfilePathLabel;

    private Label keyfilePath; // path to keyfile label

    private Button keyfileExternalBrowseButton;// "External" browse button for

    // keyfile text box
    private Button keyfileInternalBrowseButton;// "Internal" browse button for

    // keyfile text box
    private boolean isKeyfileExternal;

    private Label aliasLabel;

    private Text aliasText; // key alias text box

    private Button promptForPasswordRadio; // "prompt" radio button

    private Button savePasswordsInKeyringRadio;// "save" radio button

    private Button savePasswordsInProjectRadio;// "save" radio button

    private Label keystorePassLabel;

    private Text keystorePassText; // keystore password text box

    private Label keyPassLabel;

    private Text keyPassText; // key password text box

    private Group advancedGroup; // "advanced" group

    private Label advancedInstructions;

    private Label providerLabel;

    private Text providerText; // crypto provider text box

    private Label keystoreTypeLabel;
    private Text keystoreTypeText; // keystore type text box
    private Button testButton; // "test settings" button
    private SignatureProperties sigProps; // signature properties object
    private boolean bLoading = false; // true while loading to defer
    // validation
    private boolean bSigningDataValid = false; // true if signing data is

    /**
     * Default constructor.
     */
    public J2MESigningPropertiesPage() {
    }

    /**
     * Utility routine to convert an empty string to a null value when unloading
     * Text widgets.
     * 
     * @param s
     * @return
     */
    public String convertEmptyToNull(String s) {
        if (s == null) {
            return (null);
        }

        if (s.length() == 0) {
            return (null);
        }

        return (s);
    }

    /**
     * Returns <code>true</code> if the page data is currently valid.
     * 
     * @see org.eclipse.jface.preference.PreferencePage#isValid()
     */
    @Override
    public boolean isValid() {
        return bSigningDataValid;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        IProject project = getProject();

        boolean isJ2meProject = false;
        try {
            isJ2meProject = J2MENature.hasJ2MENature(project);
        } catch (CoreException e) {
        }

        if (!isJ2meProject) {
            return (true);
        }

        boolean succeeded = false;

        try {
            IMidletSuiteProject midletProject = getMidletSuiteProject();
            midletProject.setSignatureProperties(sigProps);
            midletProject.saveMetaData();

            succeeded = true;
        } catch (Exception ex) {
            MTJUIErrors.displayError(getShell(), "MTJUiError.Exception", //$NON-NLS-1$
                    "MTJUiError.SetPlatformFailed", //$NON-NLS-1$
                    ex);
        }

        return succeeded;
    }

    /**
     * This routine is called in response to the "Browse" button to allow the
     * user to choose a keystore file using a File Open dialog.
     */
    private void browseExternalForKeystore() {
        FileDialog dlg = new FileDialog(getShell(), SWT.OPEN);

        dlg.setText(BROWSE_DIALOG_TITLE);
        dlg.setFilterNames(BROWSE_FILTER_NAMES);
        dlg.setFilterExtensions(BROWSE_FILTER_EXTENSIONS);

        String path = dlg.open();
        if (path == null) {
            return;
        }

        keyfilePath.setText(path);
        isKeyfileExternal = true;
    }

    /**
     * This routine is called in response to the "Browse" button to allow the
     * user to choose a keystore file using a File Open dialog.
     */
    private void browseProjectForKeystore() {
        ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
                getShell(), new WorkbenchLabelProvider(),
                new WorkbenchContentProvider());
        dialog.setTitle(BROWSE_DIALOG_TITLE);
        dialog.setMessage(BROWSE_DIALOG_MESSAGE);
        dialog.setInput(getProject());
        dialog.setAllowMultiple(false);
        dialog.setEmptyListMessage(BROWSE_NEED_FILE);
        dialog.setStatusLineAboveButtons(true);
        dialog.setValidator(new ISelectionStatusValidator() {
            public IStatus validate(Object[] selection) {
                if (selection.length == 0) {
                    return new Status(IStatus.ERROR, IMTJUIConstants.PLUGIN_ID,
                            1, BROWSE_NEED_FILE, null);
                }

                if (!(selection[0] instanceof IFile)) {
                    return new Status(IStatus.ERROR, IMTJUIConstants.PLUGIN_ID,
                            1, BROWSE_NEED_FILE, null);
                }

                return new Status(IStatus.OK, IMTJUIConstants.PLUGIN_ID,
                        IStatus.OK, ((IFile) selection[0]).getName(), null);
            }
        });

        if (dialog.open() != Window.OK) {
            return;
        }

        Object[] result = dialog.getResult();
        if ((result.length == 0) || !(result[0] instanceof IFile)) {
            return;
        }

        IFile theFile = (IFile) result[0];
        IPath fullPath = theFile.getFullPath();
        /*
         * The full path comes back with the name of the project in the first
         * segment. Removing the first segment gives us a path that's really
         * (from a file system point of view) relative to the project home
         * directory.
         */
        IPath relativePath = fullPath.removeFirstSegments(1);
        String filePath = relativePath.toString();

        /*
         * "toRelative" in this sense means prepending the prefix to put the
         * string into its "relative display form".
         */
        String displayPath = SignatureProperties.toRelative(filePath);

        keyfilePath.setText(displayPath);
        isKeyfileExternal = false;
    }

    /**
     * Utility routine to build a GridData item
     */
    private GridData buildGridData(int nFill, boolean bGrab, int nSpan) {
        GridData gd = new GridData();

        gd.horizontalAlignment = nFill;
        gd.grabExcessHorizontalSpace = bGrab;
        gd.horizontalSpan = nSpan;

        return (gd);
    }

    /**
     * Utility routine to convert a null value to an empty string when loading
     * Text widgets.
     * 
     * @param s
     * @return
     */
    private String convertNullToEmpty(String s) {
        if (s == null) {
            return (""); //$NON-NLS-1$
        }

        return (s);
    }

    /**
     * Get the MIDlet suite project for this project.
     * 
     * @return
     */
    private IMidletSuiteProject getMidletSuiteProject() {
        IJavaProject javaProject = JavaCore.create(getProject());
        IMidletSuiteProject midletProject = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);

        return midletProject;
    }

    /**
     * Get the selected project or <code>null</code> if a project is not
     * selected.
     * 
     * @return
     */
    private IProject getProject() {
        IProject project = null;
        IAdaptable adaptable = getElement();

        if (adaptable instanceof IProject) {
            project = (IProject) adaptable;
        } else if (adaptable instanceof IJavaProject) {
            project = ((IJavaProject) adaptable).getProject();
        }

        return project;
    }

    /**
     * Utility routine to insert a label with the specified text onto the
     * specified parent
     */
    private Label insertLabel(Composite parent, String text) {
        Label lbl = new Label(parent, SWT.LEFT);
        lbl.setText(text);
        return (lbl);
    }

    /**
     * Return a boolean indicating whether the selected element is a J2ME
     * project.
     * 
     * @param project
     * @return
     */
    private boolean isJ2MEProject(IProject project) {
        boolean j2meProject = false;

        if (project != null) {
            try {
                IProjectNature nature = project
                        .getNature(IMTJCoreConstants.J2ME_NATURE_ID);
                j2meProject = (nature != null);
            } catch (CoreException e) {
            }
        }

        return j2meProject;
    }

    /**
     * This routine checks to see if the path specified for the keystore exists,
     * and is a file (not a directory). Beyond this, we can't do more checking
     * until they actually test the settings.
     */
    private boolean isKeystorePathValid() {
        String value;

        value = convertEmptyToNull(keyfilePath.getText());
        if (value == null) {
            setErrorMessage(ERROR_MISSING_KEYSTORE);
            return (false);
        }

        if (!isKeyfileExternal) {
            try {
                value = SignatureProperties.toAbsolute(value, getProject());
            } catch (CoreException e) {
                MTJCorePlugin.log(IStatus.ERROR, e);
                return false;
            }
        }

        File f = new File(value);
        if (!f.exists()) {
            setErrorMessage(ERROR_NOSUCH_KEYSTORE);
            return (false);
        }

        if (!f.isFile()) {
            setErrorMessage(ERROR_INVALID_KEYSTORE);
            return (false);
        }

        return (true);
    }

    /**
     * Return a boolean indicating whether the controls should be read only for
     * the specified project.
     * 
     * @param project
     * @return
     */
    private boolean isReadOnly(IProject project) {
        boolean readOnly = false;

        try {
            readOnly = project
                    .hasNature(IMTJCoreConstants.J2ME_PREPROCESSED_NATURE_ID);
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.WARNING, e);
        }

        return readOnly;
    }

    /**
     * This routine is responsible for initializing all the controls on the page
     * to the current values in the MIDlet Suite project.
     */
    private void loadPageData() {
        // Get the associated MIDlet suite project
        IMidletSuiteProject midletProject = getMidletSuiteProject();

        ISignatureProperties props = null;
        try {
            props = midletProject.getSignatureProperties();
        } catch (CoreException e) {
        }

        if (props != null) {
            sigProps.copy(props);
        } else {
            sigProps.clear();
        }

        loadSigningData();
    }

    /**
     * This routine loads the controls that are part of the Signing Properties
     * Group. It is separated from loadPageData() because we also need to do
     * this in response to the "Defaults" button being pressed.
     */
    private void loadSigningData() {
        bLoading = true;

        signProjectCheckButton.setSelection(sigProps.getSignProject());
        keyfilePath.setText(convertNullToEmpty(sigProps
                .getKeyStoreDisplayPath()));
        isKeyfileExternal = sigProps.isKeyStorePathExternal();
        aliasText.setText(convertNullToEmpty(sigProps.getKeyAlias()));

        switch (sigProps.getPasswordStorageMethod()) {
        case ISignatureProperties.PASSMETHOD_IN_PROJECT:
            promptForPasswordRadio.setSelection(false);
            savePasswordsInKeyringRadio.setSelection(false);
            savePasswordsInProjectRadio.setSelection(true);
            keystorePassText.setText(convertNullToEmpty(sigProps
                    .getKeyStorePassword()));
            keyPassText.setText(convertNullToEmpty(sigProps.getKeyPassword()));
            break;

        case ISignatureProperties.PASSMETHOD_IN_KEYRING:
            promptForPasswordRadio.setSelection(false);
            savePasswordsInKeyringRadio.setSelection(true);
            savePasswordsInProjectRadio.setSelection(false);
            keystorePassText.setText(convertNullToEmpty(sigProps
                    .getKeyStorePassword()));
            keyPassText.setText(convertNullToEmpty(sigProps.getKeyPassword()));
            break;

        case ISignatureProperties.PASSMETHOD_PROMPT:
        default:
            promptForPasswordRadio.setSelection(true);
            savePasswordsInKeyringRadio.setSelection(false);
            savePasswordsInProjectRadio.setSelection(false);
            break;
        }

        providerText
                .setText(convertNullToEmpty(sigProps.getKeyStoreProvider()));
        keystoreTypeText
                .setText(convertNullToEmpty(sigProps.getKeyStoreType()));

        bLoading = false;

        validatePage();
    }

    /**
     * This routine builds all the controls in the "Signing Properties" Group
     */
    private void populateSigningGroup(Group signingGroup) {
        signingGroup.setLayout(new GridLayout(4, false));

        signProjectCheckButton = new Button(signingGroup, SWT.CHECK);
        signProjectCheckButton.setText(SIGN_PROJECT_CHECK_BUTTON_TEXT);
        signProjectCheckButton.setLayoutData(buildGridData(SWT.BEGINNING,
                false, 4));
        signProjectCheckButton.setEnabled(!isReadOnly(getProject()));

        keyfilePathLabel = insertLabel(signingGroup, KEYSTORE_LABEL_TEXT);

        keyfilePath = new Label(signingGroup, SWT.LEFT | SWT.BORDER);
        keyfilePath.setLayoutData(buildGridData(SWT.FILL, true, 1));

        keyfileInternalBrowseButton = new Button(signingGroup, SWT.PUSH);
        keyfileInternalBrowseButton.setText(INTERNAL_BROWSE_BUTTON_TEXT);
        keyfileInternalBrowseButton.setLayoutData(buildGridData(SWT.END, false,
                1));

        keyfileExternalBrowseButton = new Button(signingGroup, SWT.PUSH);
        keyfileExternalBrowseButton.setText(EXTERNAL_BROWSE_BUTTON_TEXT);
        keyfileExternalBrowseButton.setLayoutData(buildGridData(SWT.END, false,
                1));

        aliasLabel = insertLabel(signingGroup, ALIAS_LABEL_TEXT);

        aliasText = new Text(signingGroup, SWT.SINGLE | SWT.BORDER);
        aliasText.setLayoutData(buildGridData(SWT.FILL, false, 3));

        new Label(signingGroup, SWT.SEPARATOR | SWT.HORIZONTAL)
                .setLayoutData(buildGridData(SWT.FILL, false, 4));

        promptForPasswordRadio = new Button(signingGroup, SWT.RADIO);
        promptForPasswordRadio.setText(PROMPT_FOR_PWD_LABEL_TEXT);
        promptForPasswordRadio.setLayoutData(buildGridData(SWT.BEGINNING,
                false, 4));

        savePasswordsInKeyringRadio = new Button(signingGroup, SWT.RADIO);
        savePasswordsInKeyringRadio.setText(STORE_PWD_KEYRING_LABEL_TEXT);
        savePasswordsInKeyringRadio.setLayoutData(buildGridData(SWT.BEGINNING,
                false, 4));

        savePasswordsInProjectRadio = new Button(signingGroup, SWT.RADIO);
        savePasswordsInProjectRadio.setText(STORE_PWD_PROJECT_LABEL_TEXT);
        savePasswordsInProjectRadio.setLayoutData(buildGridData(SWT.BEGINNING,
                false, 4));

        keystorePassLabel = insertLabel(signingGroup, KEYSTORE_PWD_LABEL_TEXT);

        keystorePassText = new Text(signingGroup, SWT.SINGLE | SWT.BORDER
                | SWT.PASSWORD);
        keystorePassText.setLayoutData(buildGridData(SWT.FILL, false, 3));

        keyPassLabel = insertLabel(signingGroup, KEY_PWD_LABEL_TEXT);

        keyPassText = new Text(signingGroup, SWT.SINGLE | SWT.BORDER
                | SWT.PASSWORD);
        keyPassText.setLayoutData(buildGridData(SWT.FILL, false, 3));

        advancedGroup = new Group(signingGroup, SWT.NONE);
        advancedGroup.setLayout(new GridLayout(2, false));
        advancedGroup.setText(ADVANCED_GROUP_TEXT);
        advancedGroup.setLayoutData(buildGridData(SWT.FILL, false, 4));

        advancedInstructions = new Label(advancedGroup, SWT.LEFT);
        advancedInstructions.setText(ADVANCED_TEXT);
        advancedInstructions.setLayoutData(buildGridData(SWT.BEGINNING, false,
                3));

        providerLabel = insertLabel(advancedGroup, PROVIDER_LABEL_TEXT);
        providerText = new Text(advancedGroup, SWT.SINGLE | SWT.BORDER);
        providerText.setLayoutData(buildGridData(SWT.FILL, true, 2));

        keystoreTypeLabel = insertLabel(advancedGroup, KEYSTORE_TYPE_LABEL_TEXT);
        keystoreTypeText = new Text(advancedGroup, SWT.SINGLE | SWT.BORDER);
        keystoreTypeText.setLayoutData(buildGridData(SWT.FILL, true, 2));

        testButton = new Button(signingGroup, SWT.PUSH);
        testButton.setText(TEST_BUTTON_TEXT);
        testButton.setLayoutData(buildGridData(SWT.BEGINNING, false, 1));

        signProjectCheckButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                validatePage();
            }
        });

        promptForPasswordRadio.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                Button b = (Button) e.widget;
                if (b.getSelection()) {
                    validatePage();
                }
            }
        });

        savePasswordsInKeyringRadio
                .addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        Button b = (Button) e.widget;
                        if (b.getSelection()) {
                            validatePage();
                        }
                    }
                });

        savePasswordsInProjectRadio
                .addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        Button b = (Button) e.widget;
                        if (b.getSelection()) {
                            validatePage();
                        }
                    }
                });

        keyfileInternalBrowseButton
                .addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        browseProjectForKeystore();
                        validatePage();
                    }
                });

        keyfileExternalBrowseButton
                .addSelectionListener(new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        browseExternalForKeystore();
                        validatePage();
                    }
                });

        aliasText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validatePage();
            }
        });

        keystorePassText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validatePage();
            }
        });

        keyPassText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                validatePage();
            }
        });

        testButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                testSettings();
            }
        });
    }

    /**
     * This routine is called in response to the user pushing the "test
     * settings" button. It tries to load up the key from the keystore and check
     * to make sure it's acceptable to the JadSignature class. If necessary, it
     * will prompt for the passwords.
     */
    private void testSettings() {
        if (!updateSigningFields()) {
            return;
        }

        IMidletSuiteProject project = getMidletSuiteProject();

        try {
            IJadSignature sig = SignatureUtils.getSignatureObject(project,
                    sigProps);

            if (sig != null) {
                MessageDialog.openInformation(getShell(), MTJUIStrings
                        .getString("common.success"), //$NON-NLS-1$
                        TEST_SUCCESS);
            }
        } catch (CoreException ce) {
            MTJUIErrors.displayError(getShell(), null, // use default
                    "J2MEProjectPropertiesPage.VerificationFailed", //$NON-NLS-1$
                    ce.getStatus());
        }
    }

    /**
     * This routine updates the state of the various Signing Properties fields.
     * Depending on the various settings, this involves enabling and disabling
     * some of the fields. Values are also checked for validity, and the error
     * label updated appropriately. Returns true if all the fields are in a
     * self-consistent and valid state, false otherwise.
     */
    private boolean updateSigningFields() {

        // Don't run the tests while loading. Event listeners may
        // be triggered while the form isn't self-consistent during
        // the load process.
        if (bLoading) {
            return (true);
        }

        // The "sign it" checkbox enables or disables everything

        boolean bSign = signProjectCheckButton.getSelection();

        keyfilePathLabel.setEnabled(bSign);
        keyfilePath.setEnabled(bSign);
        keyfileExternalBrowseButton.setEnabled(bSign);
        aliasLabel.setEnabled(bSign);
        aliasText.setEnabled(bSign);
        promptForPasswordRadio.setEnabled(bSign);
        savePasswordsInProjectRadio.setEnabled(bSign);
        savePasswordsInKeyringRadio.setEnabled(bSign);
        advancedGroup.setEnabled(bSign);
        advancedInstructions.setEnabled(bSign);
        providerLabel.setEnabled(bSign);
        providerText.setEnabled(bSign);
        keystoreTypeLabel.setEnabled(bSign);
        keystoreTypeText.setEnabled(bSign);

        if (!bSign) {
            keystorePassLabel.setEnabled(false);
            keystorePassText.setEnabled(false);
            keyPassLabel.setEnabled(false);
            keyPassText.setEnabled(false);
            sigProps.clear();
            testButton.setEnabled(false);
            return (true);
        }

        testButton.setEnabled(false); // assume for now, check later

        boolean bSave = !promptForPasswordRadio.getSelection();
        keystorePassLabel.setEnabled(bSave);
        keystorePassText.setEnabled(bSave);
        keyPassLabel.setEnabled(bSave);
        keyPassText.setEnabled(bSave);

        sigProps.setSignProject(true);

        // pull out the various text fields, validating them as we go

        if (!isKeystorePathValid()) {
            return (false);
        }

        sigProps.setKeyStoreDisplayPath(convertEmptyToNull(keyfilePath
                .getText()));

        String value = convertEmptyToNull(aliasText.getText());
        if (value == null) {
            setErrorMessage(ERROR_MISSING_ALIAS);
            return (false);
        }

        sigProps.setKeyAlias(value);

        if (savePasswordsInKeyringRadio.getSelection()) {
            sigProps
                    .setPasswordStorageMethod(ISignatureProperties.PASSMETHOD_IN_KEYRING);

            value = convertEmptyToNull(keystorePassText.getText());
            if (value == null) {
                setErrorMessage(ERROR_MISSING_STOREPASS);
                return (false);
            }
            sigProps.setKeyStorePassword(value);

            value = convertEmptyToNull(keyPassText.getText());
            if (value == null) {
                setErrorMessage(ERROR_MISSING_KEYPASS);
                return (false);
            }
            sigProps.setKeyPassword(value);
        } else if (savePasswordsInProjectRadio.getSelection()) {
            sigProps
                    .setPasswordStorageMethod(ISignatureProperties.PASSMETHOD_IN_PROJECT);

            value = convertEmptyToNull(keystorePassText.getText());
            if (value == null) {
                setErrorMessage(ERROR_MISSING_STOREPASS);
                return (false);
            }
            sigProps.setKeyStorePassword(value);

            value = convertEmptyToNull(keyPassText.getText());
            if (value == null) {
                setErrorMessage(ERROR_MISSING_KEYPASS);
                return (false);
            }
            sigProps.setKeyPassword(value);
        } else {
            sigProps
                    .setPasswordStorageMethod(ISignatureProperties.PASSMETHOD_PROMPT);
            sigProps.setKeyStorePassword(null);
            sigProps.setKeyPassword(null);
        }

        sigProps
                .setKeyStoreProvider(convertEmptyToNull(providerText.getText()));
        sigProps
                .setKeyStoreType(convertEmptyToNull(keystoreTypeText.getText()));

        setErrorMessage(null);
        testButton.setEnabled(true);
        return (true);
    }

    /**
     * This routine validates the data on the page, updating the buttons on the
     * form at the same time.
     */
    private void validatePage() {
        bSigningDataValid = updateSigningFields();
        updateApplyButton();
        getContainer().updateButtons();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_J2MEProjectPropertiesPage");

        IProject project = getProject();

        if (!isJ2MEProject(project)) {
            Label lbl = new Label(parent, SWT.NONE);
            lbl
                    .setText(MTJUIStrings
                            .getString("J2MEProjectPropertiesPage.NotMidletSuiteProject")); //$NON-NLS-1$
            return (lbl);
        }

        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, true));

        Group signingGroup = new Group(composite, SWT.NONE);
        signingGroup.setText(SIGNING_GROUP_TEXT);
        signingGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        populateSigningGroup(signingGroup);

        sigProps = new SignatureProperties();

        loadPageData();

        return composite;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
     */
    @Override
    protected void performDefaults() {
        super.performDefaults();

        setErrorMessage(null);
        sigProps.clear(); // restore signing settings to defaults
        loadSigningData(); // reload the page data
    }
}
