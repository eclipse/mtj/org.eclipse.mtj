/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *******************************************************************************/

package org.eclipse.mtj.ui.internal.launching;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.launching.ILaunchConstants;

/**
 * Launch shortcut for MIDlet.
 * 
 * @author Feng Wang
 */
public class MidletLaunchShortcut extends JavaLaunchShortcut {

    /*
     * (non-Javadoc)
     * @seeorg.eclipse.jdt.debug.ui.launchConfigurations.JavaLaunchShortcut#
     * createConfiguration(org.eclipse.jdt.core.IType)
     */
    @Override
    protected ILaunchConfiguration createConfiguration(IType type) {
        ILaunchConfiguration config = null;
        try {
            ILaunchConfigurationType configType = getConfigurationType();

            String launchConfigName = DebugPlugin.getDefault()
                    .getLaunchManager()
                    .generateUniqueLaunchConfigurationNameFrom(
                            type.getElementName());
            ILaunchConfigurationWorkingCopy wc = configType.newInstance(null,
                    launchConfigName);

            wc.setAttribute(ILaunchConstants.EMULATED_CLASS, Utils
                    .getQualifiedClassName(type));
            wc.setAttribute(
                    IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, type
                            .getJavaProject().getElementName());
            wc.setAttribute(ILaunchConstants.DO_OTA, false);

            DebugUITools.setLaunchPerspective(configType,
                    ILaunchManager.RUN_MODE,
                    IDebugUIConstants.PERSPECTIVE_DEFAULT);
            DebugUITools.setLaunchPerspective(configType,
                    ILaunchManager.DEBUG_MODE,
                    IDebugUIConstants.PERSPECTIVE_DEFAULT);

            config = wc.doSave();

        } catch (CoreException ce) {
            MTJCorePlugin.log(IStatus.WARNING, "createConfiguration", ce);
        }

        return config;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jdt.debug.ui.launchConfigurations.JavaLaunchShortcut#findTypes
     *      (java.lang.Object[], org.eclipse.jface.operation.IRunnableContext)
     */
    @Override
    protected IType[] findTypes(Object[] elements, IRunnableContext context)
            throws InterruptedException, CoreException {

        try {
            return MidletLaunchConfigUtils.findMidlets(context, elements);
        } catch (InvocationTargetException e) {
            throw (CoreException) e.getTargetException();
        }

    }

    /*
     * (non-Javadoc)
     * @seeorg.eclipse.jdt.debug.ui.launchConfigurations.JavaLaunchShortcut#
     * getConfigurationType()
     */
    @Override
    protected ILaunchConfigurationType getConfigurationType() {
        ILaunchManager lm = DebugPlugin.getDefault().getLaunchManager();
        ILaunchConfigurationType configType = lm
                .getLaunchConfigurationType(ILaunchConstants.LAUNCH_CONFIG_TYPE);
        return configType;
    }

    /*
     * (non-Javadoc)
     * @seeorg.eclipse.jdt.debug.ui.launchConfigurations.JavaLaunchShortcut#
     * getEditorEmptyMessage()
     */
    @Override
    protected String getEditorEmptyMessage() {
        return LauncherMessages.MidletLaunching_EditorContainsNoMidlet;
    }

    /*
     * (non-Javadoc)
     * @seeorg.eclipse.jdt.debug.ui.launchConfigurations.JavaLaunchShortcut#
     * getSelectionEmptyMessage()
     */
    @Override
    protected String getSelectionEmptyMessage() {
        return LauncherMessages.MidletLaunching_SelectionContainsNoMidlet;
    }

    /*
     * (non-Javadoc)
     * @seeorg.eclipse.jdt.debug.ui.launchConfigurations.JavaLaunchShortcut#
     * getTypeSelectionTitle()
     */
    @Override
    protected String getTypeSelectionTitle() {
        return LauncherMessages.MidletLaunching_SelectionDialogTitle;
    }

}
