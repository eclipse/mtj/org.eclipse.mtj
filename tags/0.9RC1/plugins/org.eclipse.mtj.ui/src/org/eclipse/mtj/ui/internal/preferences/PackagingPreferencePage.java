/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.IEmbeddableWorkbenchPreferencePage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

/**
 * Preference page implementation for setting J2ME Over the Air preferences.
 * 
 * @author Craig Setera
 */
public class PackagingPreferencePage extends FieldEditorPreferencePage
        implements IEmbeddableWorkbenchPreferencePage, IMTJCoreConstants {

    private boolean embeddedInProperties;

    /**
     * Default constructor.
     */
    public PackagingPreferencePage() {
        this(false, MTJUIPlugin.getDefault().getCorePreferenceStore());
    }

    /**
     * Constructor for use when embedding the preference page within a
     * properties page.
     * 
     * @param embeddedInProperties
     * @param preferenceStore
     */
    public PackagingPreferencePage(boolean embeddedInProperties,
            IPreferenceStore preferenceStore) {
        super(GRID);

        this.embeddedInProperties = embeddedInProperties;
        setPreferenceStore(preferenceStore);

        if (!embeddedInProperties) {
            setDescription("Packaging Preferences");
        }
    }

    /*
     * Overridden so that we get the help context where it belongs so that it
     * works when the focus is in the left pane.
     */
    protected Control createContents(Composite parent) {
        if (embeddedInProperties) {
            noDefaultAndApplyButton();
        }

        PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
                "org.eclipse.mtj.ui.help_PackagingPreferencePage");
        return super.createContents(parent);
    }

    /**
     * Creates the field editors.
     */
    public void createFieldEditors() {
        final Composite parent = getFieldEditorParent();

        addField(new BooleanFieldEditor(PREF_PKG_AUTOVERSION,
                "Increment Version Automatically", parent));

        addField(new MultiValuedTableFieldEditor(PREF_PKG_EXCLUDED_PROPS,
                "Excluded Manifest Entries", parent));
    }

    /**
     * Initialize the preference page as necessary
     * 
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench) {
    }

    /**
     * @see org.eclipse.jface.preference.FieldEditorPreferencePage#performDefaults()
     */
    public void performDefaults() {
        super.performDefaults();
    }

    /**
     * @see org.eclipse.jface.preference.PreferencePage#performApply()
     */
    public void performApply() {
        super.performApply();
    }
}
