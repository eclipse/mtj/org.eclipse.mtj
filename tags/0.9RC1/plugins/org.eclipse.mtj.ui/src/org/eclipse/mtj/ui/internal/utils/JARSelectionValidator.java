/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.utils;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.ui.IMTJUIConstants;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

/**
 * The selection validator for the jar file dialog.
 * 
 * @author Craig Setera
 */
public class JARSelectionValidator implements ISelectionStatusValidator {
    private IStatus okStatus = new Status(IStatus.OK,
            IMTJUIConstants.PLUGIN_ID, 0, "", null);

    private IStatus errorStatus = new Status(IStatus.ERROR,
            IMTJUIConstants.PLUGIN_ID, 0, "", null);

    /**
     * @see org.eclipse.ui.dialogs.ISelectionStatusValidator#validate(java.lang.Object[])
     */
    public IStatus validate(Object[] selection) {
        return isValid(selection) ? okStatus : errorStatus;
    }

    /**
     * Return a boolean indicating whether the specified selection is valid.
     * 
     * @param selection
     * @return
     */
    protected boolean isValid(Object[] selection) {
        boolean valid = true;

        if (selection.length > 0) {
            for (int i = 0; i < selection.length; i++) {
                Object object = selection[i];
                if (!IFile.class.isInstance(object)) {
                    valid = false;
                    break;
                }
            }
        } else {
            valid = false;
        }

        return valid;
    }
}
