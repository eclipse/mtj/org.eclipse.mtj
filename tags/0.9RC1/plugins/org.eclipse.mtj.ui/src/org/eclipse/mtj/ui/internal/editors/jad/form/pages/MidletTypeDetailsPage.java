/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola)  - Initial implementation
 *     Diego Sandin (Motorola)  - Fixed returned path in the Icon FormEntry
 */
package org.eclipse.mtj.ui.internal.editors.jad.form.pages;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.ui.internal.forms.parts.FormEntry;
import org.eclipse.mtj.ui.internal.forms.parts.IFormEntryListener;
import org.eclipse.mtj.ui.internal.utils.ImageSelectionDialogCreator;
import org.eclipse.mtj.ui.internal.utils.MidletSelectionDialogCreator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.dialogs.SelectionDialog;
import org.eclipse.ui.forms.IDetailsPage;
import org.eclipse.ui.forms.IFormPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

/**
 * @author Diego Madruga Sandin
 */
public class MidletTypeDetailsPage implements IDetailsPage {

    /**
     * @author Diego Madruga Sandin
     */
    class MIDletsFormEntryAdapter implements IFormEntryListener {

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.IFormEntryListener#browseButtonSelected(org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.FormEntry)
         */
        public void browseButtonSelected(FormEntry entry) {

            if (entry == classEntry) {
                SelectionDialog dialog;

                try {
                    dialog = MidletSelectionDialogCreator
                            .createMidletSelectionDialog(entry.getButton()
                                    .getShell(), null, javaProject, false);
                    if (dialog.open() == Window.OK) {
                        Object[] results = dialog.getResult();
                        if ((results != null) && (results.length > 0)) {
                            IType type = (IType) results[0];
                            if (type != null) {
                                String className = type.getFullyQualifiedName();
                                classEntry.setValue(className);
                                input.setClassName(className);
                            }
                        }
                    }
                } catch (JavaModelException e) {
                    MTJCorePlugin.log(IStatus.ERROR,
                            "browseButtonSelected - MIDlet", e); //$NON-NLS-1$
                }
            } else if (entry == iconEntry) {
                SelectionDialog dialog;

                try {
                    dialog = ImageSelectionDialogCreator
                            .createImageSelectionDialog(entry.getButton()
                                    .getShell(), javaProject);
                    if (dialog.open() == Window.OK) {
                        Object[] results = dialog.getResult();
                        if ((results != null) && (results.length > 0)) {
                            IResource type = (IResource) results[0];
                            if (type != null) {
                                String imagePath = "/"
                                        + type.getProjectRelativePath()
                                                .removeFirstSegments(1)
                                                .toString();

                                iconEntry.setValue(imagePath);
                                input.setIcon(imagePath);
                            }
                        }
                    }
                } catch (Throwable e) {
                    MTJCorePlugin.log(IStatus.ERROR,
                            "browseButtonSelected - Image", e); //$NON-NLS-1$
                }
            }
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.IFormEntryListener#focusGained(org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.FormEntry)
         */
        public void focusGained(FormEntry entry) {
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.ui.forms.events.IHyperlinkListener#linkActivated(org.eclipse.ui.forms.events.HyperlinkEvent)
         */
        public void linkActivated(HyperlinkEvent e) {

        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.ui.forms.events.IHyperlinkListener#linkEntered(org.eclipse.ui.forms.events.HyperlinkEvent)
         */
        public void linkEntered(HyperlinkEvent e) {

        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.ui.forms.events.IHyperlinkListener#linkExited(org.eclipse.ui.forms.events.HyperlinkEvent)
         */
        public void linkExited(HyperlinkEvent e) {

        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.IFormEntryListener#selectionChanged(org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.FormEntry)
         */
        public void selectionChanged(FormEntry entry) {

        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.IFormEntryListener#textDirty(org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.FormEntry)
         */
        public void textDirty(FormEntry entry) {
            textValueChanged(entry);
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.IFormEntryListener#textValueChanged(org.eclipse.mtj.ui.internal.editors.jad.form.pages.block.FormEntry)
         */
        public void textValueChanged(FormEntry entry) {
            if (entry == classEntry) {
                input.setClassName(entry.getValue());
            } else if (entry == iconEntry) {
                input.setIcon(entry.getValue());
            } else if (entry == nameEntry) {
                input.setName(entry.getValue(), true);
                input.setMidletName(entry.getValue());
            }
        }

    }

    /**
     * The text used in the browse button
     */
    private static final String BROWSE_BUTTON_LABEL_TEXT = "browse";

    /**
     * The FormEntry MIDlet name label text
     */
    private static final String MIDLET_NAME_LABEL_TEXT = "Name";

    /**
     * The FormEntry MIDlet icon label text
     */
    private static final String MIDLET_ICON_LABEL_TEXT = "Icon";

    /**
     * The FormEntry MIDlet class label text
     */
    private static final String MIDLET_CLASS_LABEL_TEXT = "Class";

    /**
     * The managed form that manages this part
     */
    private IManagedForm mform;

    /**
     * The FormEntry for the MIDlet name
     */
    private FormEntry nameEntry;

    /**
     * The FormEntry for the MIDlet class name
     */
    private FormEntry classEntry;

    /**
     * The FormEntry for the MIDlet icon path
     */
    private FormEntry iconEntry;

    /**
     * The MIDlet type to be edited or displayed
     */
    private MidletType input;

    /**
     * The project where the MIDlets are available
     */
    private IJavaProject javaProject;

    /**
     * Creates a new MidletTypeDetailsPage.
     * 
     * @param javaProject the current MIDlet project
     */
    public MidletTypeDetailsPage(IJavaProject javaProject) {
        this.javaProject = javaProject;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.IDetailsPage#commit()
     */
    public void commit(boolean onSave) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.IDetailsPage#createContents(org.eclipse.swt.widgets.Composite)
     */
    public void createContents(Composite parent) {

        TableWrapLayout layout = new TableWrapLayout();
        layout.topMargin = 5;
        layout.leftMargin = 5;
        layout.rightMargin = 2;
        layout.bottomMargin = 2;
        parent.setLayout(layout);

        FormToolkit toolkit = mform.getToolkit();

        Section s1 = toolkit.createSection(parent, Section.DESCRIPTION
                | ExpandableComposite.TITLE_BAR);
        s1.marginWidth = 10;

        s1.setText("MIDlet Details");
        s1.setDescription("Set the properties of the selected MIDlet.");

        TableWrapData td = new TableWrapData(TableWrapData.FILL,
                TableWrapData.TOP);
        td.grabHorizontal = true;
        s1.setLayoutData(td);

        GridLayout glayout = new GridLayout();
        glayout.marginWidth = glayout.marginHeight = toolkit.getBorderStyle() == SWT.BORDER ? 0
                : 2;
        glayout.numColumns = 3;

        Composite client = toolkit.createComposite(s1);

        createSpacer(client, toolkit, 3);
        client.setLayout(glayout);

        createNameEntry(client, toolkit, new MIDletsFormEntryAdapter());
        createIconEntry(client, toolkit, new MIDletsFormEntryAdapter());
        createClassEntry(client, toolkit, new MIDletsFormEntryAdapter());

        toolkit.paintBordersFor(client);
        s1.setClient(client);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.IDetailsPage#dispose()
     */
    public void dispose() {
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.IDetailsPage#initialize(org.eclipse.ui.forms.IManagedForm)
     */
    public void initialize(IManagedForm mform) {
        this.mform = mform;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.IDetailsPage#isDirty()
     */
    public boolean isDirty() {
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.IFormPart#isStale()
     */
    public boolean isStale() {
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.IDetailsPage#refresh()
     */
    public void refresh() {
        update();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.IDetailsPage#inputChanged(org.eclipse.jface.viewers.IStructuredSelection)
     */
    public void selectionChanged(IFormPart part, ISelection selection) {
        IStructuredSelection ssel = (IStructuredSelection) selection;
        if (ssel.size() == 1) {
            input = (MidletType) ssel.getFirstElement();
        } else {
            input = null;
        }
        update();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.IDetailsPage#setFocus()
     */
    public void setFocus() {
        nameEntry.getText().setFocus();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.IFormPart#setFormInput(java.lang.Object)
     */
    public boolean setFormInput(Object input) {
        return false;
    }

    /**
     * Create a new FormEntry for the MIDlet class.
     * 
     * @param parent the parent composite
     * @param toolkit the default FormToolkit
     * @param listener the event listener for this FormEntry
     */
    private void createClassEntry(Composite parent, FormToolkit toolkit,
            IFormEntryListener listener) {
        classEntry = new FormEntry(parent, toolkit, MIDLET_CLASS_LABEL_TEXT,
                BROWSE_BUTTON_LABEL_TEXT, false);
        classEntry.setFormEntryListener(listener);
        classEntry.setEditable(true);

    }

    /**
     * Create a new FormEntry for the Icon resource.
     * 
     * @param parent the parent composite
     * @param toolkit the default FormToolkit
     * @param listener the event listener for this FormEntry
     */
    private void createIconEntry(Composite parent, FormToolkit toolkit,
            IFormEntryListener listener) {
        iconEntry = new FormEntry(parent, toolkit, MIDLET_ICON_LABEL_TEXT,
                BROWSE_BUTTON_LABEL_TEXT, false);
        iconEntry.setFormEntryListener(listener);
        iconEntry.setEditable(true);

    }

    /**
     * Create a new FormEntry for the MIDlet name.
     * 
     * @param parent the parent composite
     * @param toolkit the default FormToolkit
     * @param listener the event listener for this FormEntry
     */
    private void createNameEntry(Composite parent, FormToolkit toolkit,
            IFormEntryListener listener) {
        nameEntry = new FormEntry(parent, toolkit, MIDLET_NAME_LABEL_TEXT,
                SWT.SINGLE);
        nameEntry.setFormEntryListener(listener);
        nameEntry.setEditable(true);

    }

    /**
     * Creates an spacer.
     * 
     * @param parent the spacer parent composite
     * @param toolkit the default FormToolkit
     * @param span specifies the number of column cells that the control will
     *                take up.
     */
    private void createSpacer(Composite parent, FormToolkit toolkit, int span) {
        Label spacer = toolkit.createLabel(parent, "");
        GridData gd = new GridData();
        gd.horizontalSpan = span;
        spacer.setLayoutData(gd);
    }

    /**
     * Update the values of the {@link #nameEntry MIDlet name},
     * {@link #classEntry MIDlet class} and {@link #iconEntry Icon}.
     */
    private void update() {

        nameEntry.setValue(
                (input != null) && (input.getMidletName() != null) ? input
                        .getMidletName() : "", true);
        iconEntry.setValue((input != null) && (input.getIcon() != null) ? input
                .getIcon() : "", true);
        classEntry.setValue(
                (input != null) && (input.getClassName() != null) ? input
                        .getClassName() : "", true);
    }

}
