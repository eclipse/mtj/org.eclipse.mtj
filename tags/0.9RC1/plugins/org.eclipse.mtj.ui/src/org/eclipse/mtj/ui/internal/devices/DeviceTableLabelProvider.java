/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.devices;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.library.api.API;
import org.eclipse.swt.graphics.Image;

/**
 * Label provider implementation that provides label information for an instance
 * of IDevice.
 * 
 * @author Craig Setera
 */
public class DeviceTableLabelProvider extends LabelProvider implements
        ITableLabelProvider {

    private static final String TEXT_UNKNOWN = "";
    private static final int COL_CHECKBOX = 0;
    private static final int COL_GROUP_NAME = 1;
    private static final int COL_NAME = 2;
    private static final int COL_CONFIGURATION = 3;
    private static final int COL_PROFILE = 4;

    /**
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object,
     *      int)
     */
    public Image getColumnImage(Object element, int columnIndex) {
        return null;
    }

    /**
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object,
     *      int)
     */
    public String getColumnText(Object element, int columnIndex) {
        String text = TEXT_UNKNOWN;
        IDevice device = (IDevice) element;

        switch (columnIndex) {
        case COL_CHECKBOX:
            text = "";
            break;

        case COL_NAME:
            text = device.getName();
            break;

        case COL_GROUP_NAME:
            text = device.getGroupName();
            break;

        case COL_CONFIGURATION:
            text = getConfigurationText(device);
            break;

        case COL_PROFILE:
            text = getProfileText(device);
            break;
        }

        return text;
    }

    /**
     * Return the text for the device's configuration library.
     * 
     * @param device
     * @return
     */
    private String getConfigurationText(IDevice device) {
        String text = TEXT_UNKNOWN;

        ILibrary library = device.getConfigurationLibrary();
        if (library != null) {
            API api = library.getConfiguration();
            if (api != null) {
                text = api.toString();
            }
        }

        return text;
    }

    /**
     * Return the text for the device's profile library.
     * 
     * @param device
     * @return
     */
    private String getProfileText(IDevice device) {
        String text = TEXT_UNKNOWN;

        ILibrary library = device.getProfileLibrary();
        if (library != null) {
            API api = library.getProfile();
            if (api != null) {
                text = api.toString();
            }
        }

        return text;
    }
}
