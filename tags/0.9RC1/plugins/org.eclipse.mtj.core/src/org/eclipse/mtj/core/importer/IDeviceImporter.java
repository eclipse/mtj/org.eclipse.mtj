/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 */
package org.eclipse.mtj.core.importer;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.model.device.IDevice;

/**
 * Instances of IDeviceImporter are presented with a set of directories for
 * which the instance may return one or more device instances. IDeviceImporters
 * are provided to the system via the <code>deviceImporter</code> extension
 * point.
 * 
 * @author Craig Setera
 */
public interface IDeviceImporter {
    /**
     * Return the fully configured device instances found in the specified
     * directory or <code>null</code> if no devices are recognized by this
     * importer in this directory.
     * 
     * @param directory
     * @param monitor
     * @return
     */
    public IDevice[] getMatchingDevices(File directory, IProgressMonitor monitor);
}
