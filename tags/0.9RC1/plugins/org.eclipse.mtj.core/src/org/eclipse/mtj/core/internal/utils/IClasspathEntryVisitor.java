/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID.
 */
package org.eclipse.mtj.core.internal.utils;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;

/**
 * Instances of this interface are called at appropriate times to visit the
 * classpath entries as they are traversed. At any time, each method may return
 * <code>false</code> to stop traversing that part of the classpath entry
 * tree.
 * 
 * @author Craig Setera
 */
public interface IClasspathEntryVisitor {

    /**
     * Visit a classpath entry container beginning. Return a boolean value
     * specifying whether to resolve the entry and recursively visit the
     * resolved container's contents.
     * 
     * @param entry the classpath entry representing this container
     * @param javaProject
     * @param containerPath
     * @param monitor
     * 
     * @return
     * @throws CoreException
     */
    public boolean visitContainerBegin(IClasspathEntry entry,
            IJavaProject javaProject, IPath containerPath,
            IProgressMonitor monitor) throws CoreException;

    /**
     * Visit a classpath entry container ending.
     * 
     * @param entry the classpath entry representing this container
     * @param javaProject
     * @param containerPath
     * @param monitor
     * 
     * @return
     * @throws CoreException
     */
    public void visitContainerEnd(IClasspathEntry entry,
            IJavaProject javaProject, IPath containerPath,
            IProgressMonitor monitor) throws CoreException;

    /**
     * Visit a classpath library entry.
     * 
     * @param entry the classpath entry representing this container
     * @param javaProject
     * @param monitor
     * 
     * @throws CoreException
     */
    public void visitLibraryEntry(IClasspathEntry entry,
            IJavaProject javaProject, IProgressMonitor monitor)
            throws CoreException;

    /**
     * Visit a classpath project entry. Return a boolean value specifying
     * whether to resolve the entry and recursively visit the resolved entry.
     * 
     * @param entry the classpath entry representing this container
     * @param javaProject
     * @param classpathProject
     * @param monitor
     * 
     * @return
     * @throws CoreException
     */
    public boolean visitProject(IClasspathEntry entry,
            IJavaProject javaProject, IJavaProject classpathProject,
            IProgressMonitor monitor) throws CoreException;

    /**
     * Visit a classpath source entry.
     * 
     * @param entry the classpath entry representing this container
     * @param javaProject
     * @param monitor
     * 
     * @throws CoreException
     */
    public void visitSourceEntry(IClasspathEntry entry,
            IJavaProject javaProject, IProgressMonitor monitor)
            throws CoreException;

    /**
     * Visit a classpath variable name entry. Return a boolean value specifying
     * whether to resolve the entry and recursively visit the resolved entry.
     * 
     * @param entry the classpath entry representing this container
     * @param javaProject
     * @param variableName
     * @param monitor
     * 
     * @return
     * @throws CoreException
     */
    public boolean visitVariable(IClasspathEntry entry,
            IJavaProject javaProject, String variableName,
            IProgressMonitor monitor) throws CoreException;
}
