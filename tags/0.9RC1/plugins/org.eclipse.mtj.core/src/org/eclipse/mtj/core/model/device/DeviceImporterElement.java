/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.device;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.mtj.core.importer.IDeviceImporter;

/**
 * Wraps an IConfigurationElement for use as a device importer.
 * 
 * @author Craig Setera
 */
public class DeviceImporterElement {

    private static final String ATTR_ID = "id";
    private static final String ATTR_NAME = "name";
    private static final String ATTR_CLASS = "class";
    private static final String ATTR_PRIORITY = "priority";

    private IConfigurationElement configurationElement;

    /**
     * Construct a new wrapper around the specified element.
     * 
     * @param element
     */
    public DeviceImporterElement(IConfigurationElement element) {
        super();
        configurationElement = element;
    }

    /**
     * Get the device importer for the configuration element.
     * 
     * @return
     * @throws CoreException
     */
    public IDeviceImporter getDeviceImporter() throws CoreException {
        return (IDeviceImporter) configurationElement
                .createExecutableExtension(ATTR_CLASS);
    }

    /**
     * Return the id of this extension element.
     * 
     * @return
     */
    public String getId() {
        return configurationElement.getAttribute(ATTR_ID);
    }

    /**
     * Return the name of this extension element.
     * 
     * @return
     */
    public String getName() {
        return configurationElement.getAttribute(ATTR_NAME);
    }

    /**
     * Return the priority of this element.
     * 
     * @return
     */
    public int getPriority() {
        int priority = 50;

        try {
            priority = Integer.parseInt(configurationElement
                    .getAttribute(ATTR_PRIORITY));
        } catch (NumberFormatException e) {
        }

        return priority;
    }
}
