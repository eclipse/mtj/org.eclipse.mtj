/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code                           
 */
package org.eclipse.mtj.core.internal.preprocessor;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.nature.AbstractNature;
import org.eclipse.mtj.core.nature.BuildSpecManipulator;

/**
 * <p>
 * Project nature for J2ME projects that preproces their resources.
 * </p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * 
 * @author Craig Setera
 * @see IProjectNature
 */
public class PreprocessingNature extends AbstractNature {
    /**
     * Configure the builders on the specified project for preprocessing.
     * 
     * @param project
     * @param monitor
     * @throws CoreException
     */
    public static void configureBuilders(IProject project,
            IProgressMonitor monitor) throws CoreException {
        BuildSpecManipulator manipulator = new BuildSpecManipulator(project);
        manipulator.addBuilderBefore(JavaCore.BUILDER_ID,
                IMTJCoreConstants.J2ME_PREPROCESSOR_ID, null);
        manipulator.commitChanges(monitor);
    }

    /**
     * Deconfigure the builders on the specified project for preprocessing.
     * 
     * @param project
     * @param monitor
     * @throws CoreException
     */
    public static void deconfigureBuilders(IProject project,
            IProgressMonitor monitor) throws CoreException {
        BuildSpecManipulator manipulator = new BuildSpecManipulator(project);
        manipulator.removeBuilder(IMTJCoreConstants.J2ME_PREPROCESSOR_ID);
        manipulator.commitChanges(monitor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.core.nature.AbstractNature#configure()
     */
    @Override
    public void configure() throws CoreException {
        configureBuilders(getProject(), new NullProgressMonitor());
    }
}
