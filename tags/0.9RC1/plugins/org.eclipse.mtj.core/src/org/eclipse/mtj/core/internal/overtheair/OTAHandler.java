/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards. Added serialVersionUID.
 *     Diego Sandin (Motorola)  - Adapting code to use Jetty version 5.1.11                                
 */
package org.eclipse.mtj.core.internal.overtheair;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.mortbay.http.HttpException;
import org.mortbay.http.HttpRequest;
import org.mortbay.http.HttpResponse;
import org.mortbay.http.handler.AbstractHttpHandler;

/**
 * Jetty HttpHandler implementation for the OTA implementation.
 * 
 * @author Craig Setera
 */
public class OTAHandler extends AbstractHttpHandler {

    /**
     * Default serial version UID
     */
    private static final long serialVersionUID = 1L;

    // Internal map of the known MIME types
    private static final String[] KNOWN_MIME_TYPES = new String[] { "jad",
            "text/vnd.sun.j2me.app-descriptor", "jar",
            "application/java-archive" };

    /**
     * Internal map of file extensions to MIME types.
     */
    private static Map<String, String> mimeTypeMap;

    /**
     * @uml.property name="mimeTypeMap"
     */
    private static Map<String, String> getMimeTypeMap() {
        if (mimeTypeMap == null) {
            mimeTypeMap = new HashMap<String, String>();

            for (int i = 0; i < KNOWN_MIME_TYPES.length;) {
                mimeTypeMap.put(KNOWN_MIME_TYPES[i++], KNOWN_MIME_TYPES[i++]);
            }
        }

        return mimeTypeMap;
    }

    /**
     * Default constructor
     */
    public OTAHandler() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.mortbay.http.handler.AbstractHttpHandler#getName()
     */
    public String getName() {
        return "MTJ Over the Air Handler";
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.mortbay.http.HttpHandler#handle(java.lang.String,
     *      java.lang.String, org.mortbay.http.HttpRequest,
     *      org.mortbay.http.HttpResponse)
     */
    public void handle(String pathInContext, String pathParams,
            HttpRequest request, HttpResponse response) throws HttpException,
            IOException {
        String project = null;
        String file = null;

        StringTokenizer st = new StringTokenizer(pathInContext, "/");
        if (st.hasMoreTokens())
            project = st.nextToken();
        if (st.hasMoreTokens())
            file = st.nextToken();

        if ((project != null) && (file != null)) {
            try {
                handle(project, file, pathParams, request, response);
            } catch (CoreException e) {
                throw new IOException(e.getMessage());
            }
        }
    }

    /**
     * Get the appropriate content encoding for the specified file.
     * 
     * @param requestedFile
     * @return
     */
    private String getContentEncoding(IFile requestedFile) {
        return (requestedFile.getFileExtension().equals("jad")) ? "UTF-8"
                : null;
    }

    /**
     * @param requestedFile
     * @return
     */
    private String getContentType(IFile requestedFile) {
        String contentType = (String) getMimeTypeMap().get(
                requestedFile.getFileExtension());

        return (contentType != null) ? contentType : "application/octet-stream";
    }

    /**
     * Return the IJavaProject for the specified name or <code>null</code> if
     * no such project is found.
     * 
     * @param projectName
     * @return
     */
    private IJavaProject getJavaProject(String projectName) {
        IJavaProject javaProject = null;

        IWorkspaceRoot root = MTJCorePlugin.getWorkspace().getRoot();
        IProject project = root.getProject(projectName);

        if (project != null) {
            javaProject = JavaCore.create(project);
        }

        return javaProject;
    }

    /**
     * Handle sending the specified file in the specified Eclipse project.
     * 
     * @param project
     * @param file
     * @param pathParams
     * @param request
     * @param response
     * @throws CoreException
     * @throws IOException
     */
    private void handle(String project, String file, String pathParams,
            HttpRequest request, HttpResponse response) throws CoreException,
            IOException {
        IJavaProject javaProject = getJavaProject(project);
        if (javaProject != null) {
            handle(javaProject, file, pathParams, request, response);
        }
    }

    /**
     * Handle sending the specified file in the specified Eclipse Java Project.
     * 
     * @param javaProject
     * @param file
     * @param pathParams
     * @param request
     * @param response
     * @throws CoreException
     * @throws IOException
     */
    private void handle(IJavaProject javaProject, String file,
            String pathParams, HttpRequest request, HttpResponse response)
            throws CoreException, IOException {
        String deploymentFolderName = MTJCorePlugin
                .getDeploymentDirectoryName();
        IFolder deployedFolder = javaProject.getProject().getFolder(
                deploymentFolderName);

        if (deployedFolder.exists()) {
            IFile requestedFile = deployedFolder.getFile(file);

            if (requestedFile.exists()) {
                sendFile(requestedFile, response);
            }
        }
    }

    /**
     * Send the specified file back to the caller.
     * 
     * @param requestedFile
     * @param response
     * @throws CoreException
     * @throws IOException
     */
    private void sendFile(IFile requestedFile, HttpResponse response)
            throws CoreException, IOException {
        String contentType = getContentType(requestedFile);
        String contentEncoding = getContentEncoding(requestedFile);

        File javaFile = requestedFile.getLocation().toFile();

        response.setCharacterEncoding(contentEncoding, true);
        response.setContentType(contentType);
        response.setContentLength((int) javaFile.length());

        InputStream is = requestedFile.getContents();
        OutputStream os = response.getOutputStream();
        Utils.copyInputToOutput(is, os);
        os.close();
        is.close();

        response.commit();
    }
}
