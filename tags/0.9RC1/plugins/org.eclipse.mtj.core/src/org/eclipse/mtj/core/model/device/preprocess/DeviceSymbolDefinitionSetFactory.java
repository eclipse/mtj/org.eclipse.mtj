/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.core.model.device.preprocess;

import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.preprocessor.SymbolDefinitionSet;

/**
 * This class is responsible for generate {@link SymbolDefinitionSet}'s based
 * on a device information.
 * 
 * @author Diego Madruga Sandin
 */
public class DeviceSymbolDefinitionSetFactory {

    /**
     * Generate {@link SymbolDefinitionSet}'s based on the device information.
     * <p>
     * The {@link SymbolDefinitionSet} will be constructed with the device's
     * name. All properties and APIs retrieved using the methods
     * {@link IDevice#getDeviceProperties()} and {@link IDevice#getClasspath()}
     * will be included.
     * </p>
     * <p>
     * Spaces will be replaced by "_" on symbol identifiers.
     * </p>
     * 
     * @param device the device used as base
     * @return the {@link SymbolDefinitionSet} based on the device information.
     */
    public static SymbolDefinitionSet createSymbolDefinitionSet(IDevice device) {

        SymbolDefinitionSet definitionSet = new SymbolDefinitionSet(device
                .getName());

        definitionSet
                .define((device.getName().toUpperCase()).replace(' ', '_'));

        Properties properties = device.getDeviceProperties();

        if (properties != null) {
            for (Iterator<Map.Entry<Object, Object>> iterator = properties
                    .entrySet().iterator(); iterator.hasNext();) {
                Map.Entry<Object, Object> type = (Map.Entry<Object, Object>) iterator
                        .next();

                try {
                    String key = (type.getKey().toString().toUpperCase())
                            .replace(' ', '_');
                    String value = type.getValue().toString();

                    definitionSet.define(key, value);
                } catch (RuntimeException e) {
                    MTJCorePlugin.log(IStatus.ERROR, e);
                }
            }
        }

        ILibrary[] libraries = device.getClasspath().getEntries();

        if (libraries != null) {
            for (int i = 0; i < libraries.length; i++) {

                try {
                    String key = (libraries[i].getAPIs()[0].getIdentifier())
                            .replace(' ', '_');
                    String value = libraries[i].getAPIs()[0].getVersion()
                            .toString();

                    definitionSet.define(key, value);
                } catch (RuntimeException e) {
                    MTJCorePlugin.log(IStatus.ERROR, e);
                }
            }
        }

        return definitionSet;
    }
}
