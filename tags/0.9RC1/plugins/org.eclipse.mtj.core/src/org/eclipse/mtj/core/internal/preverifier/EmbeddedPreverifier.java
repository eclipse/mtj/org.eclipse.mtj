/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.internal.preverifier;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.PreferenceAccessor;
import org.eclipse.mtj.core.model.Version;
import org.eclipse.mtj.core.model.preverifier.IPreverifier;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.preverifier.ArchivePreverifier;
import org.eclipse.mtj.preverifier.CLDC1_0PreverificationPolicy;
import org.eclipse.mtj.preverifier.CLDC1_1PreverificationPolicy;
import org.eclipse.mtj.preverifier.ClassPreverifier;
import org.eclipse.mtj.preverifier.IArchivePreverificationListener;
import org.eclipse.mtj.preverifier.IPreverificationPolicy;
import org.eclipse.mtj.preverifier.results.PreverificationError;
import org.eclipse.mtj.preverifier.results.PreverificationResults;
import org.osgi.framework.Bundle;

/**
 * A preverifier implementation that is embedded within MTJ and not requiring an
 * executable call.
 * 
 * @author Craig Setera
 */
public class EmbeddedPreverifier implements IPreverifier {
    private static class PreverificationParameters {
        private IMidletSuiteProject midletSuite;
        private IPreverificationPolicy preverificationPolicy;
        private ClassPreverifier classPreverifier;

        PreverificationParameters(IMidletSuiteProject midletSuite) {
            this.midletSuite = midletSuite;
        }

        /**
         * Return an archive preverifier for the specified progress monitor.
         * 
         * @param monitor
         * @return
         */
        protected ArchivePreverifier getArchivePreverifier(
                IProgressMonitor monitor) {
            IArchivePreverificationListener listener = new ProgressMonitorPreverificationListener(
                    monitor);
            ArchivePreverifier preverifier = new ArchivePreverifier(
                    getPreverificationPolicy(), listener);

            return preverifier;
        }

        /**
         * @return Returns the midletSuite.
         */
        protected IMidletSuiteProject getMidletSuite() {
            return midletSuite;
        }

        /**
         * @return Returns the preverificationPolicy.
         */
        protected IPreverificationPolicy getPreverificationPolicy() {
            if (preverificationPolicy == null) {
                IProject project = midletSuite.getProject();

                Version configVersion = null;
                try {
                    configVersion = PreferenceAccessor.instance
                            .getPreverificationConfigurationVersion(project);
                } catch (CoreException e) {
                    MTJCorePlugin.log(IStatus.ERROR, e.getMessage(), e);
                }

                Version version = new Version("1.1");
                preverificationPolicy = ((configVersion != null) && configVersion
                        .equals(version)) ? (IPreverificationPolicy) new CLDC1_1PreverificationPolicy()
                        : (IPreverificationPolicy) new CLDC1_0PreverificationPolicy();
            }

            return preverificationPolicy;
        }

        /**
         * @return Returns the classPreverifier.
         */
        protected ClassPreverifier getClassPreverifier() {
            if (classPreverifier == null) {
                classPreverifier = new ClassPreverifier(
                        getPreverificationPolicy());
            }

            return classPreverifier;
        }

        /**
         * Return the classpath for the specified midlet project.
         * 
         * @return
         * @throws CoreException
         */
        protected URL[] getClasspath() throws CoreException, IOException {
            IJavaProject javaProject = midletSuite.getJavaProject();
            String[] entries = JavaRuntime
                    .computeDefaultRuntimeClassPath(javaProject);
            URL[] classpath = new URL[entries.length + 1];

            classpath[0] = getEmptyAPI();
            for (int i = 0; i < entries.length; i++) {
                File file = new File(entries[i]);
                classpath[i + 1] = file.toURI().toURL();
            }

            return classpath;
        }

        /**
         * Return a reference to the empty API used during preverification.
         * 
         * @return
         * @throws IOException
         */
        protected URL getEmptyAPI() throws IOException {
            Bundle bundle = MTJCorePlugin.getDefault().getBundle();
            URL emptyapi = bundle.getEntry("lib/emptyapi.zip");
            return FileLocator.resolve(emptyapi);
        }
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // There is no data to be stored for this preverifier
    }

    /**
     * @see org.eclipse.mtj.core.model.preverifier.IPreverifier#preverify(org.eclipse.mtj.core.model.project.IMidletSuiteProject,
     *      org.eclipse.core.resources.IResource[],
     *      org.eclipse.core.resources.IFolder,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public PreverificationError[] preverify(IMidletSuiteProject midletProject,
            IResource[] toVerify, IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException {
        PreverificationParameters params = new PreverificationParameters(
                midletProject);
        List<PreverificationError> errorList = new ArrayList<PreverificationError>();

        for (int i = 0; i < toVerify.length; i++) {
            IResource resource = toVerify[i];
            if (resource instanceof IContainer) {
                preverify((IContainer) resource, outputFolder, params,
                        errorList, monitor);
            } else {
                preverify((IFile) resource, outputFolder, params, errorList,
                        monitor);
            }
        }

        return (PreverificationError[]) errorList
                .toArray(new PreverificationError[errorList.size()]);
    }

    /**
     * @see org.eclipse.mtj.core.model.preverifier.IPreverifier#preverifyJarFile(org.eclipse.mtj.core.model.project.IMidletSuiteProject,
     *      java.io.File, org.eclipse.core.resources.IFolder,
     *      org.eclipse.core.runtime.IProgressMonitor)
     */
    public PreverificationError[] preverifyJarFile(
            IMidletSuiteProject midletProject, File jarFile,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, IOException {
        PreverificationParameters params = new PreverificationParameters(
                midletProject);

        ProgressMonitorPreverificationListener listener = new ProgressMonitorPreverificationListener(
                monitor);
        ArchivePreverifier preverifier = new ArchivePreverifier(params
                .getPreverificationPolicy(), listener);

        IFile outputFile = outputFolder.getFile(jarFile.getName());
        preverifier.preverify(jarFile, outputFile.getLocation().toFile(),
                params.getClasspath());

        List<?> errorList = listener.getErrorList();
        return (PreverificationError[]) errorList
                .toArray(new PreverificationError[errorList.size()]);
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // There is no data to be stored for this preverifier
    }

    /**
     * Preverify (recursively) the contents of the specified container.
     * 
     * @param container
     * @param outputFolder
     * @param params
     * @param monitor
     * @throws CoreException
     * @throws IOException
     */
    private void preverify(IContainer container, IFolder outputFolder,
            PreverificationParameters params,
            List<PreverificationError> errorList, IProgressMonitor monitor)
            throws CoreException, IOException {
        IResource[] resources = container.members();
        for (int i = 0; i < resources.length; i++) {
            IResource resource = resources[i];

            if (resource.getType() == IResource.FOLDER) {
                IFolder folder = (IFolder) resource;
                IFolder subFolder = folder.getFolder(folder.getName());
                preverify(folder, subFolder, params, errorList, monitor);
            } else {
                IFile file = (IFile) resource;
                preverify(file, outputFolder, params, errorList, monitor);
            }
        }
    }

    /**
     * Preverify the specified file.
     * 
     * @param file
     * @param outputFolder
     * @param params
     * @param monitor
     * @throws CoreException
     * @throws IOException
     */
    private void preverify(IFile file, IFolder outputFolder,
            PreverificationParameters params,
            List<PreverificationError> errorList, IProgressMonitor monitor)
            throws CoreException, IOException {
        // Preverify the file
        ClassPreverifier classPreverifier = params.getClassPreverifier();
        InputStream contentStream = file.getContents();
        URL[] classpath = params.getClasspath();
        PreverificationResults results = classPreverifier.preverify(
                contentStream, classpath);

        // Write the output if not an errorf
        if (results.isErrorResult()) {
            errorList.addAll(Arrays.asList(results.getErrors()));
        } else {
            InputStream is = new ByteArrayInputStream(results
                    .getPreverifiedClassBytes());

            IFolder packageFolder = createPackageFolder(outputFolder, results,
                    monitor);
            IFile outputFile = packageFolder.getFile(file.getName());
            if (!outputFile.exists()) {
                outputFile.create(is, true, monitor);
            } else {
                outputFile.setContents(is, true, false, monitor);
            }
        }
    }

    /**
     * Create a folder matching the package name.
     * 
     * @param outputFolder
     * @param results
     * @return
     * @throws CoreException
     */
    private IFolder createPackageFolder(IFolder outputFolder,
            PreverificationResults results, IProgressMonitor monitor)
            throws CoreException {
        String typeName = results.getPreverifiedClassNode().name;
        String[] components = typeName.split("/");
        IFolder currentFolder = outputFolder;

        for (int i = 0; i < components.length - 1; i++) {
            String component = components[i];
            currentFolder = currentFolder.getFolder(component);
            if (!currentFolder.exists()) {
                currentFolder.create(true, true, monitor);
            }
        }

        return currentFolder;
    }
}
