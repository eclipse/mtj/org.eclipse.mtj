/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.preprocessor;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * A set of symbol names to true/false definition status.
 * 
 * @author Craig Setera
 */
public class SymbolDefinitionSet implements IPersistable {

    private static final String NAME_KEY = "mtjName";
    private static final String KEYS_KEY = "mtjKeys";

    // A parent definition set to be chained...
    // private SymbolDefinitionSet parentSet;

    private String name;
    private HashMap<String, String> map;

    /**
     * Construct a new symbol definition set without a name
     */
    public SymbolDefinitionSet() {
        this((String) null);
    }

    /**
     * Construct a new symbol definition set with the specified name.
     * 
     * @param name
     */
    public SymbolDefinitionSet(String name) {
        super();
        this.name = name;
        map = new HashMap<String, String>();
    }

    /**
     * Define the specified identifier.
     * 
     * @param identifier
     */
    public void define(String identifier) {
        define(identifier, "true");
    }

    /**
     * Define the specified identifier to be the specified value.
     * 
     * @param identifier
     * @param value
     */
    public void define(String identifier, String value) {
        map.put(identifier, value);
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return (obj instanceof SymbolDefinitionSet)
                && equals((SymbolDefinitionSet) obj);
    }

    /**
     * Return a boolean indicating whether the specified SymbolDefinitions
     * object is equal to this object.
     * 
     * @param definitions
     * @return
     */
    public boolean equals(SymbolDefinitionSet definitions) {
        return safeEquals(name, definitions.name)
                && safeEquals(map, definitions.map);
    }

    /**
     * Return the symbols that are defined as true in the list of symbol
     * definitions. The list of symbols returned by this method does not include
     * symbols from parent symbol definition sets.
     * 
     * @return
     */
    public Map<String, String> getDefinedSymbols() {
        return new HashMap<String, String>(map);
    }

    /**
     * Return the name of this set of symbol definitions.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Return the (possibly null) value of the specified identifier.
     * 
     * @param identifier
     * @return
     */
    public String getValue(String identifier) {
        return map.get(identifier);
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return safeHash(name) ^ safeHash(map);
    }

    /**
     * Return a boolean indicating whether the specified identifier is defined.
     * 
     * @param identifier
     * @return
     */
    public boolean isDefined(String identifier) {
        return map.containsKey(identifier);
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // Load the name of this definitions object
        name = persistenceProvider.loadString(NAME_KEY);

        map.clear();
        String keyString = persistenceProvider.loadString(KEYS_KEY
                + getStorableName());
        if ((keyString != null) && (keyString.length() > 0)) {
            String[] keys = keyString.split(",");
            for (int i = 0; i < keys.length; i++) {
                String key = keys[i];
                String value = persistenceProvider.loadString(key);
                map.put(key, value);
            }
        }
    }

    /**
     * Set the definitions set to contain the specified definitions.
     * 
     * @param definitions
     */
    public void setDefinitions(Map<String, String> definitions) {
        map = new HashMap<String, String>(definitions);
    }

    /**
     * Set the name of this set of symbol definitions.
     * 
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // Store off the name of this definitions object
        persistenceProvider.storeString(NAME_KEY, name);

        // Store off the entries in the object
        StringBuffer sb = new StringBuffer();
        Iterator<?> entries = map.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<?,?> entry = (Map.Entry<?,?>) entries.next();

            // Add the key to the list of keys
            String key = (String) entry.getKey();
            sb.append(key);
            if (entries.hasNext()) {
                sb.append(",");
            }

            // Add the entry to the object
            persistenceProvider.storeString(key, entry.getValue().toString());
        }

        // Store the list of the keys that can be used to reconstruct
        // this object as a comma-separated list
        persistenceProvider.storeString(KEYS_KEY + getStorableName(), sb
                .toString());
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();

        Iterator<?> entries = map.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<?,?> entry = (Map.Entry<?,?>) entries.next();
            sb.append(entry.getKey()).append("=").append(entry.getValue());

            if (entries.hasNext()) {
                sb.append(",");
            }
        }

        return sb.toString();
    }

    /**
     * Undefine the specified identifier.
     * 
     * @param identifier
     */
    public void undefine(String identifier) {
        map.remove(identifier);
    }

    /**
     * Return the name which may be used for storage.
     * 
     * @return
     */
    String getStorableName() {
        return name.replaceAll("\\ ", "_");
    }

    /**
     * Safely test equality of the two objects.
     * 
     * @param object1
     * @param object2
     * @return
     */
    private boolean safeEquals(Object object1, Object object2) {
        boolean equals = false;

        if ((object1 == null) && (object2 == null)) {
            equals = true;
        } else if ((object1 != null) && (object2 != null)) {
            equals = object1.equals(object2);
        }

        return equals;
    }

    /**
     * Safely gather the hashcode of the specified object.
     * 
     * @param object
     * @return
     */
    private int safeHash(Object object) {
        return (object != null) ? object.hashCode() : 0;
    }
}
