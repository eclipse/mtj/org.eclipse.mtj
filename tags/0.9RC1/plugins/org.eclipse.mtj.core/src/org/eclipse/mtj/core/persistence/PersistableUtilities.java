/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.persistence;

import org.eclipse.mtj.core.internal.persistence.XMLPersistenceProvider;

/**
 * Utility methods related to the persistence mechanism.
 * 
 * @author Craig Setera
 */
public class PersistableUtilities {

    /**
     * Clone the specified persistable using the underlying persistence
     * functionality.
     * 
     * @param persistable
     * @return
     * @throws PersistenceException
     */
    public static IPersistable clonePersistable(IPersistable persistable)
            throws PersistenceException {
        XMLPersistenceProvider writeProvider = new XMLPersistenceProvider(
                "clone");
        writeProvider.storePersistable("persistable", persistable);
        XMLPersistenceProvider readProvider = new XMLPersistenceProvider(
                writeProvider.getDocument());

        return readProvider.loadPersistable("persistable");
    }

    /**
     * Static-only access
     */
    private PersistableUtilities() {
        super();
    }
}
