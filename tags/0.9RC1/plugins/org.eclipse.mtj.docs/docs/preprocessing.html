<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
	<title>MTJ - Preprocessing</title>
	<link rel="stylesheet" href="docs.css" type="text/css">
</head>
<body>
<p class="PageTitle">Preprocessing</p>
<p>
Device fragmentation is a difficult issue in the Micro Edition world.  Devices are 
constrained in a variety of different and inconsistent ways.  While there are many
techniques that can be used to deal with these inconsistencies, the limited memory
and processing power of many mobile devices points to a solution that occurs off-device
rather than something that happens at runtime.
</p>
<p>
MTJ provides some help in the area of device fragmentation through the inclusion
of a source code "preprocessor".  Using the limited processing power of the 
preprocessor, it is possible to do some build time source code transformations.
</p>
<ul>
	<li class="TOC"><a class="TOC" href="#enablement">Enabling and Disabling Preprocessing</a></li>
	<li class="TOC"><a class="TOC" href="#compilation">Controlling Compilation</a></li>
	<li class="TOC"><a class="TOC" href="#symbols">Defining Symbols</a></li>
</ul>
<hr>

<h1 id="enablement">Enabling and Disabling Preprocessing</h1>
<p>
Use of the preprocessing functionality is an optional part of the MTJ
functionality.  Preprocessing is enabled or disabled on a project-level basis, allowing
significant flexibility in the structure of workspace projects.
</p>
<h2>Enabling Preprocessing</h2>
<p>
Enabling preprocessing functionality can be accomplished in two different ways.  During
the creation of a new MIDlet Suite, preprocessing may be enabled on the first page of
the wizard.
</p>
<img src="img/newProjectWizard2.png" alt="screenshot" />
<p>
After a project has been created preprocessing may be enabled via the Java ME project context 
menu option.
</p>
<img src="img/crtpkg.png" />
<h2>Disabling Preprocessing</h2>
<p>
Projects with preprocessing functionality enabled, may also disable the functionality
if it is no longer necessary or useful.  Disabling the preprocessor is done via the 
project context menu, just as enablement is done.  For projects with processing
enabled, there will be a "Disable Preprocessing" menu item rather than an item to enable
processing.
</p>
<h1 id="compilation">Controlling Compilation</h1>
<p>
Preprocessing controls the final compiled code using a set of preprocessor directives.
The preprocessor supports the following directives inside a Java
		source file. All directives must follow a "//" comment that
		starts at the beginning of a line (whitespace is allowed left of them, but
		no Java code). That way, they don't interfere with normal Java compilation.
		Directives must not span multiple lines of code.
		
		<p />

		<table class="color" border="1">
			<tr>
				<th width="30%">
		  			Directive
		  		</th>

				<th>
		  			Decription
		  		</th>
			</tr>

			<tr>
				<td>
		  			#define &lt;identifier&gt;

		  		</td>

				<td>
		  			Defines an identifier, thus making its value "true" when it is
		  			referenced in further expressions.
		  		</td>
			</tr>

			<tr>
				<td>
		  			#undefine &lt;identifier&gt;

		  		</td>

				<td>
		  			Undefines an identifier, thus making its value "false" when it is
		  			referenced in further expressions.
		  		</td>
			</tr>

			<tr>
				<td>
		  			#ifdef &lt;identifier&gt;

		  		</td>

				<td rowspan="8">
		  			The following lines are compiled only if the given identifier
		  			is defined (or undefined, in the case of an "#ifndef"
		  			directive). "#else" does exactly what your think it does. Each
		  			directive must be ultimately closed by an "#endif" directive.
		  			The "#elifdef" and "#elifndef" directives help to specify longer conditional
		  			cascades without having to nest each level.
		  			
		  			<p />
		  			
		  			The "#if" and "#elif" directives even allow to use complex expressions.
		  			These expressions are very much like Java boolean expressions. They
		  			may consist of identifiers, parentheses and the usual "&amp;&amp;", "||",
		  			"^", and "!" operators.
		  			
		  			<p />
		  			
		  			Please note that "#ifdef" and "#ifndef" don't
		  			support complex expressions. They only expect a single argument - the
		  			symbol to be checked.
		  		</td>
			</tr>

			<tr>
				<td>
		  			#ifndef &lt;identifier&gt;
		  		</td>
			</tr>


			<tr>

				<td>
		  			#else
		  		</td>
			</tr>

			<tr>
				<td>
		  			#endif
		  		</td>
			</tr>

			<tr>
				<td>
		  			#elifdef &lt;identifier&gt;
		  		</td>
			</tr>

			<tr>
				<td>

		  			#elifndef &lt;identifier&gt;
		  		</td>
			</tr>

			<tr>
				<td>
		  			#if &lt;expression&gt;

		  		</td>
			</tr>

			<tr>
				<td>
		  			#elif &lt;expression&gt;
		  		</td>
			</tr>

			<tr>
				<td>
		  			#include &lt;filename&gt;
		  		</td>

				<td rowspan="2">
		  			Includes the given source file at the current position. Must
		  			be terminated by "#endinclude" for technical reasons.  Note
		  			that relative file names are interpreted as relative to the
		  			project root directory.
		  		</td>

			</tr>

			<tr>
				<td>
		  			#endinclude
		  		</td>
			</tr>

		</table>
<p />
<h1 id="symbols">Defining Symbols</h1>
<p>
The compilation directive expressions are dependent on the definition of a set of
"symbols".  These symbols are either defined/true or undefined/false.  It
is possible to control whether a particular symbol is defined or undefined directly
via preprocessing directives or indirectly through the definition of symbol definition
sets.
</p>
<h2>Direct Definitions</h2>
<p>
The <code>#define</code> and <code>#undefine</code> directives directly control whether
a particular symbol is currently defined or undefined.  Using these directives, it is possible
to override the current state of a symbol from a previous setting.
</p>
<h2>Symbol Definition Sets</h2>
<p>
Symbol definition sets provide a means to define a group of related symbols with a 
particular name to reference that group.  A definition set may then be specified for
a project via the user interface to control the compilation.
</p>
<p>
Symbol definition sets are most useful for controlling definitions without the need to
alter source code.  For example, symbol definition sets can be used to remove debugging
information to create a production build.  Start by defining a "Debug" symbol definition
set in which the symbol "DEBUG" has been defined.  Wrap the debugging code in your
project with "#ifdef DEBUG" directives.  When building a production version of the code,
use a different symbol definition set that does not have DEBUG defined and the debugging
code will be removed.
</p>
<pre>
...
// #ifdef DEBUG
System.out.println("Some debug output");
// #endif
...
</pre>
<h3>Defining Symbol Definition Sets</h3>
<p>
Symbol set definitions are accessed via the MTJ preferences.  From the Window
menu, choose the <i>Preferences...</i> menu item.  Expand the Mobiles Tools for Java category and
select the <i>Symbol Set Definitions</i> category.
</p>
<img src="img/symbols.png" />
<h4>Creating a New Set</h4>
<p>
Creating a new symbol definition set is accomplished by specifying the name of
the definition set and selecting the <i>Add</i> button.  An empty set will be created.
</p>
<h4>Defining Symbols</h4>
<p>
To define a new symbol:
</p>
<ol>
<li>Select the set via the definition set drop-down.</li>
<li>Press the <i>Add</i> button to the right of the symbols list.</li>
<li>Select the newly created default symbol name and edit the name within the cell.</li>
</ol>
<h4>Removing Symbols</h4>
<p>
To remove a symbol from the set:
<ol>
<li>Select the set via the definition set drop-down.</li>
<li>Select symbol definition to be removed.</li>
<li>Press the <i>Remove</i> button to the right of the symbols list.</li>
</ol>
</p>
<h4>Removing a Set</h4>
<p>
If a symbol definition set is no longer necessary, it can be removed:
<ol>
<li>Select the set via the definition set drop-down.</li>
<li>Press the <i>Remove</i> button to the right of the definition set drop-down.</li>
</ol>
</p>
<h3>Referencing Symbol Definition Sets</h3>
<p>
Once a Symbol Definition Set has been defined, it may be referenced by a 
project.  This is accomplished from the Mobile Tools for Java category of the project
properties dialog.
</p>
<img src="img/projectsymbols.png" />
<p>
The symbols that are defined in the set will then be used by the preprocessor
directives in processing the source code.
</p>
</body>
</html>
