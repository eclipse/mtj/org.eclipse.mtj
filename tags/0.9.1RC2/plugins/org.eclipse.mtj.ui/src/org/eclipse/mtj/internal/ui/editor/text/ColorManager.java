/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.text;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jdt.ui.PreferenceConstants;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

/**
 * @since 0.9.1
 */
public class ColorManager implements IColorManager, IMTJColorConstants {

    private static int counter = 0;
    private static ColorManager fColorManager;

    /**
     * @return
     */
    public static IColorManager getDefault() {
        if (fColorManager == null) {
            fColorManager = new ColorManager();
        }

        counter += 1;
        return fColorManager;
    }

    /**
     * @param store
     */
    public static void initializeDefaults(IPreferenceStore store) {
        boolean highContrast = false;
        try {
            highContrast = Display.getDefault().getHighContrast();
        } catch (SWTException e) { // keep highContrast = false
        }

        PreferenceConverter.setDefault(store, P_DEFAULT,
                highContrast ? DEFAULT_HIGH_CONTRAST : DEFAULT);
        PreferenceConverter.setDefault(store, P_PROC_INSTR, PROC_INSTR);
        PreferenceConverter.setDefault(store, P_STRING, STRING);
        PreferenceConverter.setDefault(store, P_TAG, TAG);
        PreferenceConverter.setDefault(store, P_XML_COMMENT, XML_COMMENT);
    }

    private Map<String, Color> fColorTable = new HashMap<String, Color>(5);

    /**
     * 
     */
    public ColorManager() {
        initialize();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.text.IColorManager#dispose()
     */
    public void dispose() {
        counter--;
        if (counter == 0) {
            disposeColors(true);
        }
    }

    /**
     * @param resetSingleton
     */
    public void disposeColors(boolean resetSingleton) {
        Iterator<Color> e = fColorTable.values().iterator();
        while (e.hasNext()) {
            (e.next()).dispose();
        }
        if (resetSingleton) {
            fColorManager = null;
        }

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.text.IColorManager#getColor(java.lang.String)
     */
    public Color getColor(String key) {
        Color color = fColorTable.get(key);
        if (color == null) {
            color = Display.getCurrent().getSystemColor(
                    SWT.COLOR_LIST_FOREGROUND);
        }
        return color;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.text.IColorManager#handlePropertyChangeEvent(org.eclipse.jface.util.PropertyChangeEvent)
     */
    public void handlePropertyChangeEvent(PropertyChangeEvent event) {
        Object color = event.getNewValue();
        if (color instanceof RGB) {
            putColor(event.getProperty(), (RGB) color);
        } else {
            putColor(event.getProperty(), StringConverter.asRGB(color
                    .toString()));
        }
    }

    /**
     * 
     */
    private void initialize() {
        IPreferenceStore pstore = MTJUIPlugin.getDefault().getPreferenceStore();
        putColor(pstore, P_DEFAULT);
        putColor(pstore, P_PROC_INSTR);
        putColor(pstore, P_STRING);
        putColor(pstore, P_TAG);
        putColor(pstore, P_XML_COMMENT);
        pstore = PreferenceConstants.getPreferenceStore();
        for (String element : IColorManager.PROPERTIES_COLORS) {
            putColor(pstore, element);
        }
    }

    /**
     * @param pstore
     * @param property
     */
    private void putColor(IPreferenceStore pstore, String property) {
        putColor(property, PreferenceConverter.getColor(pstore, property));
    }

    /**
     * @param property
     * @param setting
     */
    private void putColor(String property, RGB setting) {
        Color oldColor = fColorTable.get(property);
        if (oldColor != null) {
            if (oldColor.getRGB().equals(setting)) {
                return;
            }
            oldColor.dispose();
        }
        fColorTable.put(property, new Color(Display.getCurrent(), setting));
    }
}
