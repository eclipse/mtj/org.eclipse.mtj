/*******************************************************************************
 * Copyright (c) 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.editor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.projection.ProjectionAnnotation;
import org.eclipse.jface.text.source.projection.ProjectionAnnotationModel;
import org.eclipse.mtj.core.model.IModelChangedEvent;
import org.eclipse.mtj.internal.core.IModelChangedListener;
import org.eclipse.mtj.internal.core.text.IEditingModel;

public abstract class AbstractFoldingStructureProvider implements
        IFoldingStructureProvider, IModelChangedListener {

    private MTJSourcePage fEditor;
    private IEditingModel fModel;

    public AbstractFoldingStructureProvider(MTJSourcePage editor,
            IEditingModel model) {
        this.fEditor = editor;
        this.fModel = model;
    }

    public void initialize() {
        update();
    }

    public void modelChanged(IModelChangedEvent event) {
        update();
    }

    public void reconciled(IDocument document) {
        update();
    }

    public void update() {
        ProjectionAnnotationModel annotationModel = (ProjectionAnnotationModel) fEditor
                .getAdapter(ProjectionAnnotationModel.class);
        if (annotationModel == null) {
            return;
        }

        Set<Position> currentRegions = new HashSet<Position>();
        try {
            addFoldingRegions(currentRegions, fModel);
            updateFoldingRegions(annotationModel, currentRegions);
        } catch (BadLocationException e) {
        }
    }

    public void updateFoldingRegions(ProjectionAnnotationModel model,
            Set<Position> currentRegions) {
        Annotation[] deletions = computeDifferences(model, currentRegions);

        Map<ProjectionAnnotation, Object> additionsMap = new HashMap<ProjectionAnnotation, Object>();
        for (Iterator<Position> iter = currentRegions.iterator(); iter
                .hasNext();) {
            Object position = iter.next();
            additionsMap.put(new ProjectionAnnotation(false), position);
        }

        if (((deletions.length != 0) || (additionsMap.size() != 0))) {
            model.modifyAnnotations(deletions, additionsMap,
                    new Annotation[] {});
        }
    }

    private Annotation[] computeDifferences(ProjectionAnnotationModel model,
            Set<Position> additions) {
        List<Object> deletions = new ArrayList<Object>();
        for (Iterator<?> iter = model.getAnnotationIterator(); iter.hasNext();) {
            Object annotation = iter.next();
            if (annotation instanceof ProjectionAnnotation) {
                Position position = model.getPosition((Annotation) annotation);
                if (additions.contains(position)) {
                    additions.remove(position);
                } else {
                    deletions.add(annotation);
                }
            }
        }
        return (Annotation[]) deletions
                .toArray(new Annotation[deletions.size()]);
    }

}
