/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.ui.internal.wizards.libraries;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.mtj.core.library.model.ILibrary;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.swt.graphics.Image;

/**
 * Provide the library text and image for each column of the given element.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class LibraryListLabelProvider extends LabelProvider implements
        ITableLabelProvider {

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
     */
    public Image getColumnImage(Object element, int columnIndex) {

        Image image = null;

        if ((element instanceof ILibrary) && (columnIndex == 0)) {
            image = MTJUIPluginImages.DESC_LIBRARY.createImage();
        } else {
            image = super.getImage(element);
        }
        return image;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
     */
    public String getColumnText(Object element, int columnIndex) {
        String text = null;
        if ((element instanceof ILibrary) && (columnIndex == 0)) {
            text = ((ILibrary) element).getName();
        } else if ((element instanceof ILibrary) && (columnIndex == 1)) {
            text = ((ILibrary) element).getVersion().toString();
        } else {
            text = super.getText(element);
        }
        return text;
    }

}
