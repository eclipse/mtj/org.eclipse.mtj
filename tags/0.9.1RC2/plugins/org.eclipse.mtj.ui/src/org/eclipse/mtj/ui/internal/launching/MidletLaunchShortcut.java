/**
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - 1. initial implementation
 *                          2. Fix a bug that always create a new launch 
 *                             configuration when launching.
 *                          3. Replace ILaunchConstants.EMULATED_CLASS with
 *                             IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
 *                             to take advantage of JDT launch configuration 
 *                             refactoring participates
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874]
 */

package org.eclipse.mtj.ui.internal.launching;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.debug.ui.IDebugUIConstants;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.launching.ILaunchConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;

/**
 * Launch shortcut for MIDlet.
 * 
 * @author Feng Wang
 */
public class MidletLaunchShortcut extends JavaLaunchShortcut {

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.launching.JavaLaunchShortcut#createConfiguration(org.eclipse.jdt.core.IType)
     */
    @Override
    protected ILaunchConfiguration createConfiguration(IType type) {
        ILaunchConfiguration config = null;
        try {
            ILaunchConfigurationType configType = getConfigurationType();

            String launchConfigName = DebugPlugin.getDefault()
                    .getLaunchManager()
                    .generateUniqueLaunchConfigurationNameFrom(
                            type.getElementName());
            ILaunchConfigurationWorkingCopy wc = configType.newInstance(null,
                    launchConfigName);

            wc.setAttribute(
                    IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
                    Utils.getQualifiedClassName(type));
            wc.setAttribute(
                    IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME, type
                            .getJavaProject().getElementName());
            wc.setAttribute(ILaunchConstants.DO_OTA, false);

            DebugUITools.setLaunchPerspective(configType,
                    ILaunchManager.RUN_MODE,
                    IDebugUIConstants.PERSPECTIVE_DEFAULT);
            DebugUITools.setLaunchPerspective(configType,
                    ILaunchManager.DEBUG_MODE,
                    IDebugUIConstants.PERSPECTIVE_DEFAULT);

            config = wc.doSave();

        } catch (CoreException ce) {
            MTJCorePlugin.log(IStatus.WARNING, "createConfiguration", ce); //$NON-NLS-1$
        }

        return config;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.launching.JavaLaunchShortcut#findTypes(java.lang.Object[], org.eclipse.jface.operation.IRunnableContext)
     */
    @Override
    protected IType[] findTypes(Object[] elements, IRunnableContext context)
            throws InterruptedException, CoreException {

        try {
            return MidletLaunchConfigUtils.findMidlets(context, elements);
        } catch (InvocationTargetException e) {
            throw (CoreException) e.getTargetException();
        }

    }

    /**
     * Get all ILaunchConfiguration meet these conditions:
     * <ol>
     * <li>Project Name matching.</li>
     * <li>doJadLaunch==false</li>
     * <li>mtj.emulated_class matching</li>
     * </ol>
     * 
     * @see org.eclipse.mtj.ui.internal.launching.JavaLaunchShortcut#getCandidateConfigs(org.eclipse.jdt.core.IType,
     *      org.eclipse.debug.core.ILaunchConfigurationType)
     */
    @Override
    protected List<ILaunchConfiguration> getCandidateConfigs(IType type,
            ILaunchConfigurationType configType) {
        List<ILaunchConfiguration> candidateConfigs = Collections.emptyList();
        try {
            ILaunchConfiguration[] configs = DebugPlugin.getDefault()
                    .getLaunchManager().getLaunchConfigurations(configType);
            candidateConfigs = new ArrayList<ILaunchConfiguration>(
                    configs.length);
            for (ILaunchConfiguration config : configs) {
                boolean doJadLaunch = config.getAttribute(
                        ILaunchConstants.DO_JAD_LAUNCH, false);
                String projectName = config
                        .getAttribute(
                                IJavaLaunchConfigurationConstants.ATTR_PROJECT_NAME,
                                ""); //$NON-NLS-1$
                String midletName = config.getAttribute(
                        IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
                        ""); //$NON-NLS-1$
                // If doJadLaunch && project name is matching && jadUrl contains
                // JAD name, then the launch config is a candidate.
                if (!doJadLaunch
                        && projectName.equals(type.getJavaProject()
                                .getElementName())
                        && midletName.equals(type.getFullyQualifiedName())) {
                    candidateConfigs.add(config);
                }
            }
        } catch (CoreException e) {
            MTJCorePlugin.log(IStatus.WARNING, "getCandidateConfigs", e); //$NON-NLS-1$
        }
        return candidateConfigs;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.launching.JavaLaunchShortcut#getConfigurationType()
     */
    @Override
    protected ILaunchConfigurationType getConfigurationType() {
        ILaunchManager lm = DebugPlugin.getDefault().getLaunchManager();
        ILaunchConfigurationType configType = lm
                .getLaunchConfigurationType(ILaunchConstants.LAUNCH_CONFIG_TYPE);
        return configType;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.launching.JavaLaunchShortcut#getEditorEmptyMessage()
     */
    @Override
    protected String getEditorEmptyMessage() {
        return MTJUIMessages.MidletLaunching_EditorContainsNoMidlet;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.launching.JavaLaunchShortcut#getSelectionEmptyMessage()
     */
    @Override
    protected String getSelectionEmptyMessage() {
        return MTJUIMessages.MidletLaunching_SelectionContainsNoMidlet;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.internal.launching.JavaLaunchShortcut#getTypeSelectionTitle()
     */
    @Override
    protected String getTypeSelectionTitle() {
        return MTJUIMessages.MidletLaunching_SelectionDialogTitle;
    }

}
