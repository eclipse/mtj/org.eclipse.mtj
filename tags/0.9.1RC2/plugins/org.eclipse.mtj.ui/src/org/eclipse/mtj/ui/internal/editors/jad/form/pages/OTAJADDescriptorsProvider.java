/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma (Sybase)        - Initial implementation
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874] 
 */
package org.eclipse.mtj.ui.internal.editors.jad.form.pages;

import org.eclipse.mtj.core.model.jad.DescriptorPropertyDescription;
import org.eclipse.mtj.core.model.jad.IJADConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider;

/**
 * Provide OTA JAD attributes descriptors
 * 
 * @author Gang Ma
 */
public class OTAJADDescriptorsProvider implements IJADDescriptorsProvider {

    /**
     * OTA property descriptors.
     */
    private static final DescriptorPropertyDescription[] OTA_DESCRIPTORS = new DescriptorPropertyDescription[] {
            new DescriptorPropertyDescription(
                    IJADConstants.JAD_MIDLET_DELETE_CONFIRM,
                    MTJUIMessages.OTAJADDescriptorsProvider_midlet_del_confirm,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(
                    IJADConstants.JAD_MIDLET_DELETE_NOTIFY,
                    MTJUIMessages.OTAJADDescriptorsProvider_midlet_del_notify,
                    DescriptorPropertyDescription.DATATYPE_URL),
            new DescriptorPropertyDescription(
                    IJADConstants.JAD_MIDLET_INSTALL_NOTIFY,
                    MTJUIMessages.OTAJADDescriptorsProvider_midlet_install_notify,
                    DescriptorPropertyDescription.DATATYPE_URL), };

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider#getDescriptorPropertyDescriptions()
     */
    public DescriptorPropertyDescription[] getDescriptorPropertyDescriptions() {
        return OTA_DESCRIPTORS;
    }

}
