/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version                     
 */
package org.eclipse.mtj.internal.ui.actions.l10n;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.wizards.l10n.LocalizationWizard;
import org.eclipse.mtj.ui.internal.actions.AbstractJavaProjectAction;

/**
 * EnableLocalizationAction class enables localization feature into MTJ
 * projects.
 * 
 * @author David Marques
 * @since 0.9.1
 */
public class EnableLocalizationAction extends AbstractJavaProjectAction {

    /**
     * @param action
     */
    public void run(IAction action) {
        if ((selection != null) && !selection.isEmpty()) {
            IJavaProject javaProject = getJavaProject(selection
                    .getFirstElement());
            if (javaProject == null) {
                return;
            }
            LocalizationWizard wizard = new LocalizationWizard(javaProject);
            WizardDialog dialog = new WizardDialog(getShell(), wizard);
            dialog.setTitle(MTJUIMessages.EnableLocalizationAction_dialog_title);
            dialog.create();
            if (dialog.open() == Dialog.OK) {
                try {
                    Utils.addNatureToProject(javaProject.getProject(),
                            IMTJCoreConstants.L10N_NATURE_ID,
                            new NullProgressMonitor());
                } catch (CoreException e) {
                    MTJCorePlugin.log(IStatus.ERROR, e);
                }
            }
        }
    }
}
