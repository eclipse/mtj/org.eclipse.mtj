/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.editors.jad.source;

import java.util.ResourceBundle;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.editors.text.TextEditorActionContributor;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.RetargetTextEditorAction;
/**
 * JAD source editor action bar contributor 
 * @author gma
 *
 */
public class JADSourceEditorActionContributor extends TextEditorActionContributor {
	protected RetargetTextEditorAction fContentAssist;
	
	public JADSourceEditorActionContributor(){
		ResourceBundle bundle = JADSourceEditor.getBundleForConstructedKeys();
		fContentAssist = new RetargetTextEditorAction( bundle, "ContentAssistProposal." ); // $NON-NLS-1$
	}
	public void contributeToMenu(IMenuManager mm) {
		super.contributeToMenu(mm);
		IMenuManager editMenu = mm.findMenuUsingPath( IWorkbenchActionConstants.M_EDIT );
        if (editMenu != null) {
            editMenu.add( new Separator() );
            editMenu.add( fContentAssist );
        }
	}
	

	public void setActiveEditor(IEditorPart part) {
		super.setActiveEditor(part);
		IActionBars actionBars = getActionBars();
		IStatusLineManager manager = actionBars.getStatusLineManager();
		manager.setMessage(null);
		manager.setErrorMessage(null);

		ITextEditor textEditor = (part instanceof ITextEditor) ? (ITextEditor) part : null;
		
		if (fContentAssist != null)
			fContentAssist.setAction(getAction(textEditor, "ContentAssistProposal")); //$NON-NLS-1$
	}
}
