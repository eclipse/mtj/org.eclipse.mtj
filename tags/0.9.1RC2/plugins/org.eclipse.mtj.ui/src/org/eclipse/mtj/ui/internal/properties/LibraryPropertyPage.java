/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 *     Diego Sandin (Motorola) - Fix bug 257850
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.ui.internal.properties;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.mtj.core.library.manager.LibraryManager;
import org.eclipse.mtj.core.library.model.ILibrary;
import org.eclipse.mtj.core.library.model.Visibility;
import org.eclipse.mtj.internal.core.library.MIDletLibraryClasspathContainer;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.ui.internal.actions.WorkbenchRunnableAdapter;
import org.eclipse.mtj.ui.internal.utils.ExceptionHandler;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.DialogField;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.IDialogFieldListener;
import org.eclipse.mtj.ui.internal.wizards.libraries.LibrarySelectionBlock;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbenchPropertyPage;
import org.eclipse.ui.dialogs.PropertyPage;

/**
 * The property page responsible for managing libraries to be included in the
 * project classpath and exported in the deployable JAR file.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class LibraryPropertyPage extends PropertyPage implements
        IWorkbenchPropertyPage {

    /**
     * Listener used to listen if a library checked state changed.
     * 
     * @author Diego Madruga Sandin
     * @since 0.9.1
     */
    public class LibraryAdapter implements IDialogFieldListener {

        /* (non-Javadoc)
         * @see org.eclipse.mtj.ui.internal.wizards.dialogfields.IDialogFieldListener#dialogFieldChanged(org.eclipse.mtj.ui.internal.wizards.dialogfields.DialogField)
         */
        public void dialogFieldChanged(DialogField field) {
            if (isControlCreated()) {
                dirty = true;
            }
        }
    }

    /**
     * The source folders and the device libraries container
     */
    private List<IClasspathEntry> basicCPEntries = null;

    private boolean dirty = false;

    /**
     * The original classpath entries for the project. This is used to restore
     * the project classpath in case of errors.
     */
    private IClasspathEntry[] origCPEntries = null;

    /**
     * The instance of the block that manages libraries.
     */
    private LibrarySelectionBlock selectionBlock;

    /**
     * Creates a new LibraryPropertyPage.
     */
    public LibraryPropertyPage() {
        selectionBlock = new LibrarySelectionBlock();
        basicCPEntries = new ArrayList<IClasspathEntry>();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#createControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createControl(Composite parent) {
        noDefaultAndApplyButton();
        super.createControl(parent);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.DialogPage#getDescription()
     */
    @Override
    public String getDescription() {
        return MTJUIMessages.LibraryPropertyPage_description;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        if (selectionBlock != null) {
            // if (selectionBlock.getSelectedLibraries().equals(null)) {

            final IJavaProject javaProject = JavaCore
                    .create(getCurrentProject());

            final List<IClasspathEntry> newCPEntries = new ArrayList<IClasspathEntry>();

            newCPEntries.clear();

            IWorkspaceRunnable runnable = new IWorkspaceRunnable() {

                /* (non-Javadoc)
                 * @see org.eclipse.core.resources.IWorkspaceRunnable#run(org.eclipse.core.runtime.IProgressMonitor)
                 */
                public void run(IProgressMonitor monitor) throws CoreException,
                        OperationCanceledException {

                    for (IClasspathEntry entry : basicCPEntries) {
                        newCPEntries.add(entry);
                    }

                    for (IPath path : selectionBlock
                            .getSelectedLibrariesPathEntries()) {
                        newCPEntries
                                .add(JavaCore.newContainerEntry(path, true));
                    }

                    javaProject.setRawClasspath(null, new SubProgressMonitor(
                            monitor, 2));

                    javaProject.setRawClasspath(newCPEntries
                            .toArray(new IClasspathEntry[0]),
                            new SubProgressMonitor(monitor, 2));

                }
            };

            WorkbenchRunnableAdapter op = new WorkbenchRunnableAdapter(runnable);

            try {
                new ProgressMonitorDialog(getShell()).run(true, false, op);
            } catch (InvocationTargetException e) {
                ExceptionHandler.handle(e, getShell(),
                        MTJUIMessages.LibraryPropertyPage_error_title,
                        MTJUIMessages.LibraryPropertyPage_error_message);

                try {
                    javaProject.setRawClasspath(origCPEntries, null);
                } catch (JavaModelException e1) {
                    ExceptionHandler
                            .handle(
                                    e,
                                    getShell(),
                                    MTJUIMessages.LibraryPropertyPage_restore_error_title,
                                    MTJUIMessages.LibraryPropertyPage_restore_error_message);
                }

                /* Reinitialize the block with the projects current classpath */
                initBlockValues();

                return false;
            } catch (InterruptedException e) {
                return false;
            }
            // }
        }
        dirty = false;
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.IDialogPage#setVisible(boolean)
     */
    @Override
    public void setVisible(boolean visible) {
        if (selectionBlock != null) {
            if (!visible) {
                if (dirty) {
                    String title = MTJUIMessages.LibraryPropertyPage_unsavedchanges_title;
                    String message = MTJUIMessages.LibraryPropertyPage_unsavedchanges_message;
                    String[] buttonLabels = new String[] {
                            MTJUIMessages.LibraryPropertyPage_unsavedchanges_button_save,
                            MTJUIMessages.LibraryPropertyPage_unsavedchanges_button_discard,
                            MTJUIMessages.LibraryPropertyPage_unsavedchanges_button_ignore };
                    MessageDialog dialog = new MessageDialog(getShell(), title,
                            null, message, MessageDialog.QUESTION,
                            buttonLabels, 0);
                    int res = dialog.open();
                    if (res == 0) {
                        /* Apply changes */
                        performOk();
                    } else if (res == 1) {
                        /* Discard changes */
                        initBlockValues();
                    } else {
                        /* keep unsaved */
                    }
                }
            }
        }
        super.setVisible(visible);
    }

    /**
     * Get the current selected project.
     * 
     * @return the selected project or <code>null</code> if no project is not
     *         selected.
     */
    private IProject getCurrentProject() {
        IProject project = null;
        IAdaptable adaptable = getElement();

        if (adaptable instanceof IProject) {
            project = (IProject) adaptable;
        } else if (adaptable instanceof IJavaProject) {
            project = ((IJavaProject) adaptable).getProject();
        }

        return project;
    }

    /**
     * Initialize the state of the LibrarySelectionBlock.
     * <p>
     * This method retrieves the Raw Classpath from the current project and
     * devices it in two groups;
     * <ul>
     * <li><b>Minimum CP entries</b> : the source folders and the device
     * container</li>
     * <li><b>Library CP entries</b> : the library containers</li>
     * </ul>
     * The <i>Minimum CP entries</i> are stored in the {@link #origCPEntries}
     * list which later will be used to reset the project classpath entries. <br>
     * The <i>Library CP entries</i> are used to determined which elements are
     * checked in the {@link #selectionBlock library selection block}
     * </p>
     */
    private void initBlockValues() {

        IJavaProject javaProject = JavaCore.create(getCurrentProject());

        /* clear the list of minimum CP entries for the current project */
        basicCPEntries.clear();

        /* reset the block selection */
        selectionBlock.checkAll(false);

        try {
            /* Retrieve the current project classpath */
            origCPEntries = javaProject.getRawClasspath();
        } catch (JavaModelException e1) {
            e1.printStackTrace();
        }

        if (origCPEntries != null) {
            for (IClasspathEntry entry : origCPEntries) {

                try {
                    /* Check if the entry is an library container */
                    if ((entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER)
                            && (entry.getPath().segment(0)
                                    .equals(MIDletLibraryClasspathContainer.MIDLET_LIBRARY_CONTAINER_ID))) {

                        ILibrary library = LibraryManager.getInstance()
                                .getMidletLibrary(entry.getPath().segment(1));

                        /* Check if the library is valid */
                        if ((library != null)
                                && (library.getVisibility() != Visibility.INVALID)) {

                            if ((library.getVisibility() != Visibility.INTERNAL)) {
                                /* If the library is public select it no the library block */
                                selectionBlock.setChecked(library, true);
                            } else {
                                /* if a internal library container, store it as minimum CP entry */
                                basicCPEntries.add(entry);
                            }

                        }

                    } else {
                        /* if not a library container, store it as minimum CP entry */
                        basicCPEntries.add(entry);
                    }
                } catch (Exception e) {
                    basicCPEntries.add(entry);
                }
            }
        }
        dirty = false;
    }

    /**
     * Creates and returns the SWT control for the {@link LibrarySelectionBlock}
     * instance .
     * <p>
     * Set a listener to be used to listen if a library checked state changed
     * and initializes the {@link LibrarySelectionBlock} according to the
     * libraries available in the project classpath.
     * </p>
     * 
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     * @see org.eclipse.mtj.ui.internal.wizards.libraries.LibrarySelectionBlock
     */
    @Override
    protected Control createContents(Composite parent) {
        selectionBlock.createControl(parent);
        LibraryAdapter adapter = new LibraryAdapter();
        selectionBlock.setLibrarySelectionDialogFieldListener(adapter);
        initBlockValues();
        return selectionBlock.getControl();
    }
}
