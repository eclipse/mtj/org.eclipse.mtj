/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.ui.internal.statusHandlers;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

/**
 * Status handler for the case when no MIDlets are defined and the user chooses
 * to launch in OTA mode.
 * 
 * @author Craig Setera
 */
public class OTANoMidletsHandler implements IStatusHandler {

    /* (non-Javadoc)
     * @see org.eclipse.debug.core.IStatusHandler#handleStatus(org.eclipse.core.runtime.IStatus, java.lang.Object)
     */
    public Object handleStatus(IStatus status, Object source)
            throws CoreException {
        Display display = Display.getCurrent();
        Shell shell = display.getActiveShell();
        if (shell == null) {
            shell = new Shell(display, SWT.NONE);
        }

        final Shell finalShell = shell;
        final boolean[] result = new boolean[1];

        display.syncExec(new Runnable() {
            public void run() {
                String title = MTJUIMessages.OTANoMidletsHandler_MessageDialog_title;
                String message = MTJUIMessages.OTANoMidletsHandler_MessageDialog_message;
                result[0] = (MessageDialog.openQuestion(finalShell, title,
                        message));
            }
        });

        return Boolean.valueOf(result[0]);
    }
}
