/**
 * Copyright (c) 2006, 2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui/PDEProjectionSourcePage
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.reconciler.IReconcilingStrategy;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.jface.text.source.projection.IProjectionListener;
import org.eclipse.jface.text.source.projection.ProjectionSupport;
import org.eclipse.jface.text.source.projection.ProjectionViewer;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.mtj.core.model.IBaseModel;
import org.eclipse.mtj.internal.core.text.IEditingModel;
import org.eclipse.mtj.internal.ui.IPreferenceConstants;
import org.eclipse.mtj.internal.ui.editor.actions.MTJActionConstants;
import org.eclipse.mtj.internal.ui.editor.text.ChangeAwareSourceViewerConfiguration;
import org.eclipse.mtj.internal.ui.editor.text.ColorManager;
import org.eclipse.mtj.internal.ui.editor.text.IColorManager;
import org.eclipse.mtj.internal.ui.editor.text.ReconcilingStrategy;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.swt.widgets.Composite;

/**
 *
 */
public abstract class MTJProjectionSourcePage extends MTJSourcePage implements
        IProjectionListener {

    private IColorManager fColorManager;
    private ChangeAwareSourceViewerConfiguration fConfiguration;
    private IFoldingStructureProvider fFoldingStructureProvider;
    private ProjectionSupport fProjectionSupport;

    /**
     * @param editor
     * @param id
     * @param title
     */
    public MTJProjectionSourcePage(MTJFormEditor editor, String id, String title) {
        super(editor, id, title);
        fColorManager = ColorManager.getDefault();
        fConfiguration = SourceViewerConfigurationFactory
                .createSourceViewerConfiguration(this, fColorManager);
        if (fConfiguration != null) {
            setSourceViewerConfiguration(fConfiguration);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createPartControl(Composite parent) {
        super.createPartControl(parent);

        ProjectionViewer projectionViewer = (ProjectionViewer) getSourceViewer();
        createFoldingSupport(projectionViewer);

        if (isFoldingEnabled()) {
            projectionViewer.doOperation(ProjectionViewer.TOGGLE);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#dispose()
     */
    @Override
    public void dispose() {
        ((ProjectionViewer) getSourceViewer()).removeProjectionListener(this);
        if (fProjectionSupport != null) {
            fProjectionSupport.dispose();
            fProjectionSupport = null;
        }
        fColorManager.dispose();
        if (fConfiguration != null) {
            fConfiguration.dispose();
        }
        super.dispose();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#getAdapter(java.lang.Class)
     */
    @Override
    @SuppressWarnings("unchecked")
    public Object getAdapter(Class key) {
        if (fProjectionSupport != null) {
            Object adapter = fProjectionSupport.getAdapter(getSourceViewer(),
                    key);
            if (adapter != null) {
                return adapter;
            }
        }
        return super.getAdapter(key);
    }

    /**
     * @return
     */
    public abstract boolean isQuickOutlineEnabled();

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.projection.IProjectionListener#projectionDisabled()
     */
    public void projectionDisabled() {
        fFoldingStructureProvider = null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.text.source.projection.IProjectionListener#projectionEnabled()
     */
    public void projectionEnabled() {
        IBaseModel model = getInputContext().getModel();
        if (model instanceof IEditingModel) {
            fFoldingStructureProvider = FoldingStructureProviderFactory
                    .createProvider(this, (IEditingModel) model);
            if (fFoldingStructureProvider != null) {
                fFoldingStructureProvider.initialize();
                IReconciler rec = getSourceViewerConfiguration().getReconciler(
                        getSourceViewer());
                IReconcilingStrategy startegy = rec
                        .getReconcilingStrategy(new String());
                if (startegy instanceof ReconcilingStrategy) {
                    ((ReconcilingStrategy) startegy)
                            .addParticipant(fFoldingStructureProvider);
                }
            }
        }
    }

    /**
     * @param menu
     */
    private void addQuickOutlineMenuEntry(IMenuManager menu) {
        // Only add the action if the source page supports it
        if (isQuickOutlineEnabled() == false) {
            return;
        }
        // Get the appropriate quick outline action associated with the active
        // source page
        IAction quickOutlineAction = getAction(MTJActionConstants.COMMAND_ID_QUICK_OUTLINE);
        // Ensure it is defined
        if (quickOutlineAction == null) {
            return;
        }
        // Insert the quick outline action after the "Show In" menu contributed
        menu.add(quickOutlineAction);
    }

    /**
     * @param projectionViewer
     */
    private void createFoldingSupport(ProjectionViewer projectionViewer) {
        fProjectionSupport = new ProjectionSupport(projectionViewer,
                getAnnotationAccess(), getSharedColors());

        fProjectionSupport.install();
        ((ProjectionViewer) getSourceViewer()).addProjectionListener(this);

    }

    /**
     * @return
     */
    private boolean isFoldingEnabled() {
        IPreferenceStore store = MTJUIPlugin.getDefault().getPreferenceStore();
        return store.getBoolean(IPreferenceConstants.EDITOR_FOLDING_ENABLED);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractTextEditor#affectsTextPresentation(org.eclipse.jface.util.PropertyChangeEvent)
     */
    @Override
    protected boolean affectsTextPresentation(PropertyChangeEvent event) {
        if (fConfiguration == null) {
            return false;
        }
        return fConfiguration.affectsTextPresentation(event)
                || super.affectsTextPresentation(event);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#createSourceViewer(org.eclipse.swt.widgets.Composite, org.eclipse.jface.text.source.IVerticalRuler, int)
     */
    @Override
    protected ISourceViewer createSourceViewer(Composite parent,
            IVerticalRuler ruler, int styles) {
        ISourceViewer viewer = new MTJProjectionViewer(parent, ruler,
                getOverviewRuler(), isOverviewRulerVisible(), styles,
                isQuickOutlineEnabled());
        getSourceViewerDecorationSupport(viewer);
        return viewer;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#editorContextMenuAboutToShow(org.eclipse.jface.action.IMenuManager)
     */
    @Override
    protected void editorContextMenuAboutToShow(IMenuManager menu) {
        // Add the quick outline menu entry to the context menu
        addQuickOutlineMenuEntry(menu);
        // Add the rest
        super.editorContextMenuAboutToShow(menu);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.editors.text.TextEditor#handlePreferenceStoreChanged(org.eclipse.jface.util.PropertyChangeEvent)
     */
    @Override
    protected void handlePreferenceStoreChanged(PropertyChangeEvent event) {
        try {
            if (fConfiguration != null) {
                ISourceViewer sourceViewer = getSourceViewer();
                if (sourceViewer != null) {
                    fConfiguration.adaptToPreferenceChange(event);
                }
            }
        } finally {
            super.handlePreferenceStoreChanged(event);
        }
    }

}
