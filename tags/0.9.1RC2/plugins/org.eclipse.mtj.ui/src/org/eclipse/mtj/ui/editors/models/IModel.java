/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version                     
 */
package org.eclipse.mtj.ui.editors.models;

import org.eclipse.mtj.ui.internal.forms.blocks.NamedObject;

/**
 * @author Diego Madruga Sandin
 */
public interface IModel {

    /**
     * Add the elements to the model.
     * 
     * @param objs objects to be added to the model
     * @param notify flag to indicate if the ModelListeners must be notified
     *            about the inclusion action.
     */
    public abstract void add(NamedObject[] objs, boolean notify);

    /**
     * Add a listener to model modifications.
     * 
     * @param listener the listener to be added.
     */
    public abstract void addModelListener(IModelListener listener);

    /**
     * Remove all elements from the model.
     */
    public void clear();

    /**
     * @param objects
     * @param type
     * @param property
     */
    public abstract void fireModelChanged(Object[] objects, String type,
            String property);

    /**
     * Return all elements on the model
     * 
     * @return the elements on the model
     */
    public abstract Object[] getContents();

    /**
     * Remove elements from the model.
     * 
     * @param objs objects to be removed from the model
     * @param notify flag to indicate if the ModelListeners must be notified
     *            about the removal action.
     */
    public abstract void remove(NamedObject[] objs, boolean notify);

    /**
     * Remove the specified model listener
     * 
     * @param listener the listener to be removed
     */
    public abstract void removeModelListener(IModelListener listener);

}