/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.ui.internal.devices;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.ui.IActionDelegate;

/**
 * Wrapper around an IConfigurationElement instance for a deviceEditor extension
 * point extension.
 * 
 * @author Craig Setera
 */
public class DeviceEditorConfigElement {
    private IConfigurationElement element;
    private IActionDelegate delegate;

    /**
     * Construct a new wrapper instance.
     * 
     * @param editorElement
     */
    public DeviceEditorConfigElement(IConfigurationElement editorElement) {
        super();
        element = editorElement;
    }

    /**
     * Return an instance of the action delegate for this device editor.
     * 
     * @return
     * @throws CoreException
     */
    public IActionDelegate getActionDelegate() throws CoreException {
        if (delegate == null) {
            delegate = (IActionDelegate) element
                    .createExecutableExtension("class");
        }

        return delegate;
    }

    /**
     * Return the identifier of this extension.
     * 
     * @return
     */
    public String getIdentifier() {
        return element.getAttribute("id");
    }

    /**
     * Return the name of this extension.
     * 
     * @return
     */
    public String getName() {
        return element.getAttribute("name");
    }

    /**
     * Return the class of the device for which this editor applies.
     * 
     * @return
     */
    public String getDeviceClass() {
        return element.getAttribute("deviceClass");
    }
}
