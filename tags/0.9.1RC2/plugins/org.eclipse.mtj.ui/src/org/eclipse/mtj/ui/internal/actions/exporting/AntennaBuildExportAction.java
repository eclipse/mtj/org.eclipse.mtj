/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards, changed Platform#run to 
 *                                SafeRunner#run in run method
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874] 
 */
package org.eclipse.mtj.ui.internal.actions.exporting;

import java.io.File;
import java.util.Iterator;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Preferences;
import org.eclipse.core.runtime.SafeRunner;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.tools.AntennaBuildExporter;
import org.eclipse.mtj.core.model.project.IMidletSuiteProject;
import org.eclipse.mtj.core.model.project.MidletSuiteFactory;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.ui.internal.actions.AbstractJavaProjectAction;
import org.eclipse.mtj.ui.internal.actions.ConfigurationErrorDialog;
import org.eclipse.mtj.ui.internal.preferences.J2MEPreferencePage;
import org.eclipse.mtj.ui.internal.utils.LogAndDisplaySafeRunnable;
import org.eclipse.ui.IWorkbenchPart;

/**
 * Action delegate implementation that provides a means for the user to create
 * Ant build files for Antenna.
 * 
 * @author Craig Setera
 */
public class AntennaBuildExportAction extends AbstractJavaProjectAction {

    /* (non-Javadoc)
     * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
     */
    public void run(IAction action) {
        if ((selection != null) && !selection.isEmpty()) {
            if (configurationIsValid()) {
                // Setup the progress monitoring
                ProgressMonitorDialog dialog = new ProgressMonitorDialog(
                        workbenchPart.getSite().getShell());
                dialog.open();

                final IProgressMonitor monitor = dialog.getProgressMonitor();
                monitor
                        .beginTask(
                                MTJUIMessages.AntennaBuildExportAction_export_task_name,
                                3);

                // Create the packages
                Iterator<?> iter = selection.iterator();
                IMidletSuiteProject suite = getMidletSuite(iter.next());
                if (suite != null) {
                    final AntennaBuildExporter exporter = new AntennaBuildExporter(
                            suite);
                    SafeRunner
                            .run(new LogAndDisplaySafeRunnable(
                                    workbenchPart.getSite().getShell(),
                                    MTJUIMessages.AntennaBuildExportAction_LogAndDisplaySafeRunnable_action_name) {
                                public void run() throws Exception {
                                    exporter.doExport(monitor);
                                }
                            });
                }

                // All done
                monitor.done();
                dialog.close();
            } else {
                warnUserAboutConfigurationError();
            }
        }
    }

    /**
     * @param project
     * @param workbenchPart
     */
    public void run(IJavaProject project, IWorkbenchPart workbenchPart) {

        if (configurationIsValid()) {
            // Setup the progress monitoring
            ProgressMonitorDialog dialog = new ProgressMonitorDialog(
                    workbenchPart.getSite().getShell());
            dialog.open();

            final IProgressMonitor monitor = dialog.getProgressMonitor();
            monitor.beginTask(
                    MTJUIMessages.AntennaBuildExportAction_export_task_name, 3);

            IMidletSuiteProject suite = getMidletSuite(project);
            if (suite != null) {
                final AntennaBuildExporter exporter = new AntennaBuildExporter(
                        suite);
                SafeRunner
                        .run(new LogAndDisplaySafeRunnable(
                                workbenchPart.getSite().getShell(),
                                MTJUIMessages.AntennaBuildExportAction_LogAndDisplaySafeRunnable_action_name) {
                            public void run() throws Exception {
                                exporter.doExport(monitor);
                            }
                        });
            }

            // All done
            monitor.done();
            dialog.close();
        } else {
            warnUserAboutConfigurationError();
        }

    }

    /**
     * Return a boolean indicating whether or not the configuration is valid for
     * this operation.
     * 
     * @return
     */
    private boolean configurationIsValid() {
        // A simplistic view of whether or not this is a valid configuration
        Preferences prefs = MTJCorePlugin.getDefault().getPluginPreferences();

        File antennaJar = new File(prefs
                .getString(IMTJCoreConstants.PREF_ANTENNA_JAR));
        File wtkRoot = new File(prefs
                .getString(IMTJCoreConstants.PREF_WTK_ROOT));

        return antennaJar.exists() && antennaJar.isFile() && wtkRoot.exists()
                && wtkRoot.isDirectory();
    }

    /**
     * Return the MIDlet suite project associated with the selected object.
     * 
     * @param selected
     * @return
     */
    private IMidletSuiteProject getMidletSuite(Object selected) {
        IMidletSuiteProject suite = null;

        IJavaProject project = getJavaProject(selected);
        if (project != null) {
            suite = MidletSuiteFactory.getMidletSuiteProject(project);
        }

        return suite;
    }

    /**
     * Warn the user about the antenna configuration errors. Allow the user to
     * open the configuration.
     */
    private void warnUserAboutConfigurationError() {
        String message = MTJUIMessages.AntennaBuildExportAction_warnUserAboutConfigurationError_message;

        ConfigurationErrorDialog dialog = new ConfigurationErrorDialog(
                getShell(),
                J2MEPreferencePage.ID,
                MTJUIMessages.AntennaBuildExportAction_warnUserAboutConfigurationError_title,
                message,
                MTJUIMessages.AntennaBuildExportAction_warnUserAboutConfigurationError_btn_labl_text);

        dialog.open();
    }
}
