/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 *     Feng Wang (Sybase)      - Replace Device select section with Configuration
 *                               management section, for multi-configs support. 
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874]
 */
package org.eclipse.mtj.ui.internal.wizards.projects;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.core.filesystem.URIUtil;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.ui.PreferenceConstants;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.core.internal.JavaMEClasspathContainer;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.model.configuration.AddConfigEvent;
import org.eclipse.mtj.core.model.configuration.Configuration;
import org.eclipse.mtj.core.model.configuration.Configurations;
import org.eclipse.mtj.core.model.configuration.IConfigurationsChangeListener;
import org.eclipse.mtj.core.model.configuration.RemoveConfigEvent;
import org.eclipse.mtj.core.model.configuration.SwitchActiveConfigEvent;
import org.eclipse.mtj.core.model.device.DeviceRegistry;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.preprocessor.symbol.SymbolSetFactory;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.ui.configurations.ConfigManageComponent;
import org.eclipse.mtj.ui.internal.MTJUIPlugin;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.DialogField;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.IDialogFieldListener;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.IStringButtonAdapter;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.LayoutUtil;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.SelectionButtonDialogField;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.StringButtonDialogField;
import org.eclipse.mtj.ui.internal.wizards.dialogfields.StringDialogField;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbenchPart;

/**
 * Main page for the wizard that creates the new MIDlet project.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9
 */
public class NewMidletProjectWizardPageOne extends WizardPage {

    private final class ConfigurationGroup extends Observable {
        private ConfigManageComponent configManager;
        private IConfigurationsChangeListener configurationsChangeListener;

        public Control createControl(Composite parent) {
            Composite configComposite = new Composite(parent, SWT.NONE);
            configComposite.setFont(parent.getFont());
            configComposite.setLayout(initGridLayout(new GridLayout(1, false),
                    false));

            configManager = new ConfigManageComponent();
            initConfigManager(configComposite);
            configurationsChangeListener = new IConfigurationsChangeListener() {

                public void activeConfigSwitched(SwitchActiveConfigEvent event) {
                }

                /**
                 * If add the first configuration, invoke the validator.
                 */
                public void configurationAdded(AddConfigEvent event) {
                    Configurations configurations = (Configurations) event
                            .getSource();
                    if (configurations.size() == 1) {
                        fireEvent();
                    }
                }

                /**
                 * If all configs been removed, invoke the validator.
                 */
                public void configurationRemoved(RemoveConfigEvent event) {
                    Configurations configurations = (Configurations) event
                            .getSource();
                    if (configurations.isEmpty()) {
                        fireEvent();
                    }
                }

            };
            configManager
                    .setConfigurationsChangeListener(configurationsChangeListener);

            return configComposite;
        }

        private void initConfigManager(Composite configComposite) {
            configManager
                    .setDescription(MTJUIMessages.ConfigurationSection_Description);
            // create controls
            configManager.createContents(configComposite);
            // set default configuration
            IDevice defaultDevice = DeviceRegistry.singleton.getDefaultDevice();
            if (defaultDevice == null) {
                return;
            }
            Configuration defaultConfig = new Configuration(defaultDevice
                    .getName());
            defaultConfig.setDevice(defaultDevice);
            defaultConfig.setSymbolSet(SymbolSetFactory
                    .createSymbolSet(defaultDevice));
            configManager.getConfigurations().add(defaultConfig);
            configManager.getConfigurations().switchActiveConfiguration(
                    defaultConfig);
        }

        /**
         * Must be called when the wizard page disposed, to remove listeners,
         * avoid memory leak.
         */
        public void dispose() {
            configManager.dispose();
        }

        protected void fireEvent() {
            setChanged();
            notifyObservers();
        }

        public Configurations getConfigurations() {
            return configManager.getConfigurations();
        }

        public IDevice getSelectedDevice() {
            return configManager.getActiveConfiguration().getDevice();
        }
    }

    private final class JadNameGroup extends Observable implements Observer,
            IDialogFieldListener {

        private String previousJadName;
        protected final StringDialogField jadFileNameField;
        protected final SelectionButtonDialogField projectBasedRadio;
        protected final SelectionButtonDialogField userDefinedRadio;

        /**
         * 
         */
        public JadNameGroup() {

            projectBasedRadio = new SelectionButtonDialogField(SWT.RADIO);
            projectBasedRadio.setDialogFieldListener(this);
            projectBasedRadio
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageOne_jadNameGroup_projectBasedRadio);

            userDefinedRadio = new SelectionButtonDialogField(SWT.RADIO);
            userDefinedRadio
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageOne_jadNameGroup_userDefinedRadio);

            // text field for project name
            jadFileNameField = new StringDialogField();
            jadFileNameField
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageOne_jadFileNameField);
            jadFileNameField.setDialogFieldListener(this);

            userDefinedRadio.attachDialogField(jadFileNameField);

            projectBasedRadio.setSelection(true);
            userDefinedRadio.setSelection(false);

        }

        /**
         * @param composite
         * @return
         */
        public Control createControl(Composite parent) {

            final int numColumns = 2;

            final Group group = new Group(parent, SWT.NONE);
            group.setLayout(initGridLayout(new GridLayout(numColumns, false),
                    true));
            group
                    .setText(MTJUIMessages.NewMidletProjectWizardPageOne_jad_groupname);

            (new Label(group, SWT.NULL))
                    .setText(MTJUIMessages.NewMidletProjectWizardPageOne_jad_label);

            projectBasedRadio.doFillIntoGrid(group, numColumns);
            userDefinedRadio.doFillIntoGrid(group, numColumns);
            jadFileNameField.doFillIntoGrid(group, 3);

            LayoutUtil.setHorizontalGrabbing(jadFileNameField
                    .getTextControl(null));

            return group;
        }

        /*
         * (non-Javadoc)
         * @seeorg.eclipse.mtj.ui.internal.wizards.testing.dialogfields.
         * IDialogFieldListener
         * #dialogFieldChanged(org.eclipse.mtj.ui.internal.wizards
         * .testing.dialogfields.DialogField)
         */
        public void dialogFieldChanged(DialogField field) {

            if (field == projectBasedRadio) {
                final boolean checked = projectBasedRadio.isSelected();
                if (checked) {
                    previousJadName = jadFileNameField.getText();
                    jadFileNameField.setText(projectNameGroup.getName()
                            + ".jad"); //$NON-NLS-1$
                } else {
                    jadFileNameField.setText(previousJadName);
                }
            }

            fireEvent();
        }

        protected void fireEvent() {
            setChanged();
            notifyObservers();
        }

        /**
         * Return the JAD file name specified by the user.
         * 
         * @return
         */
        public String getJadFileName() {
            return jadFileNameField.getText();
        }

        /**
         * @return
         */
        public boolean isProjectBasedRadioSelected() {
            return projectBasedRadio.isSelected();
        }

        public void postSetFocus() {
            jadFileNameField.postSetFocusOnDialogField(getShell().getDisplay());
        }

        /**
         * Return the JAD file name specified by the user.
         * 
         * @return
         */
        public void setJadFileName(String name) {
            jadFileNameField.setText(name);
        }

        /*
         * (non-Javadoc)
         * @see java.util.Observer#update(java.util.Observable,
         * java.lang.Object)
         */
        public void update(Observable o, Object arg) {
            if (isProjectBasedRadioSelected()) {
                setJadFileName(projectNameGroup.getName() + ".jad"); //$NON-NLS-1$
            }
            fireEvent();
        }

    }

    /**
     * Request a location. Fires an event whenever the checkbox or the location
     * field is changed, regardless of whether the change originates from the
     * user or has been invoked programmatically.
     */
    private final class LocationGroup extends Observable implements Observer,
            IStringButtonAdapter, IDialogFieldListener {

        private static final String DIALOGSTORE_LAST_EXTERNAL_LOC = IMTJUIConstants.PLUGIN_ID
                + ".last.external.project"; //$NON-NLS-1$

        private String fPreviousExternalLocation;
        protected final SelectionButtonDialogField externalLocationRadio;

        protected final StringButtonDialogField projectLocation;

        protected final SelectionButtonDialogField workspaceLocationRadio;

        public LocationGroup() {

            workspaceLocationRadio = new SelectionButtonDialogField(SWT.RADIO);
            workspaceLocationRadio.setDialogFieldListener(this);
            workspaceLocationRadio
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageOne_locationGroup_workspaceLocationRadio);

            externalLocationRadio = new SelectionButtonDialogField(SWT.RADIO);
            externalLocationRadio
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageOne_locationGroup_externalLocationRadio);

            projectLocation = new StringButtonDialogField(this);
            projectLocation.setDialogFieldListener(this);
            projectLocation
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageOne_locationGroup_projectLocation);
            projectLocation
                    .setButtonLabel(MTJUIMessages.NewMidletProjectWizardPageOne_locationGroup_browse_button);

            externalLocationRadio.attachDialogField(projectLocation);

            workspaceLocationRadio.setSelection(true);
            externalLocationRadio.setSelection(false);

            fPreviousExternalLocation = ""; //$NON-NLS-1$
        }

        /*
         * (non-Javadoc)
         * @see
         * org.eclipse.mtj.ui.internal.wizards.dialogfields.IStringButtonAdapter
         * #
         * changeControlPressed(org.eclipse.mtj.ui.internal.wizards.dialogfields
         * .DialogField)
         */
        public void changeControlPressed(DialogField field) {

            final DirectoryDialog dialog = new DirectoryDialog(getShell());

            dialog
                    .setMessage(MTJUIMessages.NewMidletProjectWizardPageOne_locationGroup_changeControlPressed_dialogMessage);
            String directoryName = projectLocation.getText().trim();
            if (directoryName.length() == 0) {
                String prevLocation = MTJUIPlugin.getDefault()
                        .getDialogSettings().get(DIALOGSTORE_LAST_EXTERNAL_LOC);
                if (prevLocation != null) {
                    directoryName = prevLocation;
                }
            }

            if (directoryName.length() > 0) {
                final File path = new File(directoryName);
                if (path.exists()) {
                    dialog.setFilterPath(directoryName);
                }
            }
            final String selectedDirectory = dialog.open();
            if (selectedDirectory != null) {
                projectLocation.setText(selectedDirectory);
                MTJUIPlugin.getDefault().getDialogSettings().put(
                        DIALOGSTORE_LAST_EXTERNAL_LOC, selectedDirectory);
            }
        }

        /**
         * @param composite
         * @return
         */
        public Control createControl(Composite composite) {
            final int numColumns = 3;

            final Group group = new Group(composite, SWT.NONE);
            group.setLayout(initGridLayout(new GridLayout(numColumns, false),
                    true));
            group
                    .setText(MTJUIMessages.NewMidletProjectWizardPageOne_locationGroup_contents);

            workspaceLocationRadio.doFillIntoGrid(group, numColumns);
            externalLocationRadio.doFillIntoGrid(group, numColumns);
            projectLocation.doFillIntoGrid(group, numColumns);
            LayoutUtil.setHorizontalGrabbing(projectLocation
                    .getTextControl(null));

            return group;
        }

        /*
         * (non-Javadoc)
         * @seeorg.eclipse.mtj.ui.internal.wizards.testing.dialogfields.
         * IDialogFieldListener
         * #dialogFieldChanged(org.eclipse.mtj.ui.internal.wizards
         * .testing.dialogfields.DialogField)
         */
        public void dialogFieldChanged(DialogField field) {

            if (field == workspaceLocationRadio) {
                final boolean checked = workspaceLocationRadio.isSelected();
                if (checked) {
                    fPreviousExternalLocation = projectLocation.getText();
                    projectLocation.setText(getDefaultPath(projectNameGroup
                            .getName()));
                } else {
                    projectLocation.setText(fPreviousExternalLocation);
                }
            }
            fireEvent();
        }

        protected void fireEvent() {
            setChanged();
            notifyObservers();
        }

        protected String getDefaultPath(String name) {
            final IPath path = Platform.getLocation().append(name);
            return path.toOSString();
        }

        public IPath getLocation() {

            if (isWorkspaceRadioSelected()) {
                return Platform.getLocation();
            }
            return Path.fromOSString(projectLocation.getText().trim());
        }

        /**
         * @return <code>true</code> if the location is in the workspace
         */
        public boolean isLocationInWorkspace() {

            final String location = projectLocationGroup.getLocation()
                    .toOSString();
            IPath projectPath = Path.fromOSString(location);
            return Platform.getLocation().isPrefixOf(projectPath);
        }

        public boolean isWorkspaceRadioSelected() {
            return workspaceLocationRadio.isSelected();
        }

        public void setLocation(IPath path) {
            workspaceLocationRadio.setSelection(path == null);
            if (path != null) {
                projectLocation.setText(path.toOSString());
            } else {
                projectLocation.setText(getDefaultPath(projectNameGroup
                        .getName()));
            }
            fireEvent();
        }

        /*
         * (non-Javadoc)
         * @see java.util.Observer#update(java.util.Observable,
         * java.lang.Object)
         */
        public void update(Observable o, Object arg) {
            if (isWorkspaceRadioSelected()) {
                projectLocation.setText(getDefaultPath(projectNameGroup
                        .getName()));
            }
            fireEvent();
        }
    }

    /**
     * Request a project name. Fires an event whenever the text field is
     * changed, regardless of its content.
     */
    private final class ProjectNameGroup extends Observable implements
            IDialogFieldListener {

        protected final StringDialogField projNameField;

        public ProjectNameGroup() {
            projNameField = new StringDialogField();
            projNameField
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageOne_projectNameGroup);
            projNameField.setDialogFieldListener(this);

        }

        public Control createControl(Composite composite) {
            Composite nameComposite = new Composite(composite, SWT.NONE);
            nameComposite.setFont(composite.getFont());
            nameComposite.setLayout(initGridLayout(new GridLayout(2, false),
                    false));

            projNameField.doFillIntoGrid(nameComposite, 2);
            LayoutUtil
                    .setHorizontalGrabbing(projNameField.getTextControl(null));

            return nameComposite;
        }

        /*
         * (non-Javadoc)
         * @see
         * org.eclipse.mtj.ui.internal.wizards.dialogfields.IDialogFieldListener
         * #dialogFieldChanged(org.eclipse.mtj.ui.internal.wizards.dialogfields.
         * DialogField)
         */
        public void dialogFieldChanged(DialogField field) {
            fireEvent();
        }

        protected void fireEvent() {
            setChanged();
            notifyObservers();
        }

        public String getName() {
            return projNameField.getText().trim();
        }

        public void postSetFocus() {
            projNameField.postSetFocusOnDialogField(getShell().getDisplay());
        }

        public void setName(String name) {
            projNameField.setText(name);
        }
    }

    /**
     * Validate this page and show appropriate warnings and error
     * NewWizardMessages.
     */
    private final class Validator implements Observer {

        private boolean canCreate(File file) {
            while (!file.exists()) {
                file = file.getParentFile();
                if (file == null) {
                    return false;
                }
            }

            return file.canWrite();
        }

        /**
         * Check if the user add at least one configuration.
         * 
         * @return a status object with code IStatus.OK if one or more
         *         configurations is added , otherwise a status object
         *         indicating that no configuration is added.
         */
        private IStatus isValidConfigurations() {
            IStatus status = null;

            if (configurationGroup.getConfigurations().isEmpty()) {
                status = new Status(
                        IStatus.ERROR,
                        IMTJUIConstants.PLUGIN_ID,
                        MTJUIMessages.NewMidletProjectWizardPageOne_validate_devicecount_error);
            } else {
                status = new Status(IStatus.OK, IMTJUIConstants.PLUGIN_ID, ""); //$NON-NLS-1$
            }

            return status;
        }

        /**
         * Validates the JAD file name.
         * 
         * @return a status object with code IStatus.OK if the JAD file name is
         *         valid , otherwise a status object indicating what is wrong
         *         with the file name
         * @see IWorkspace#validateName(String, int)
         */
        private IStatus isValidJadName(final String jadName) {

            final String jadFileName = jadName;

            IWorkspace workspace = ResourcesPlugin.getWorkspace();

            IStatus result = new Status(
                    IStatus.ERROR,
                    IMTJUIConstants.PLUGIN_ID,
                    MTJUIMessages.NewMidletProjectWizardPageOne_validate_jadname_error_emptyname);

            if ((jadFileName != null) && (jadFileName != "")) { //$NON-NLS-1$
                result = workspace.validateName(jadFileName, IResource.FILE);

                if (result.isOK()) {
                    /* File name must end with the .jad file extension */
                    if (!jadFileName.endsWith(".jad")) { //$NON-NLS-1$
                        result = new Status(
                                IStatus.ERROR,
                                IMTJUIConstants.PLUGIN_ID,
                                MTJUIMessages.NewMidletProjectWizardPageOne_validate_jadname_error_extension);
                        /* File name must not be only the .jad file extension */
                    } else if (jadFileName.equals(".jad")) { //$NON-NLS-1$
                        result = new Status(
                                IStatus.ERROR,
                                IMTJUIConstants.PLUGIN_ID,
                                MTJUIMessages.NewMidletProjectWizardPageOne_validate_jadname_error_emptyname);
                    }
                }
            }
            return result;
        }

        public void update(Observable o, Object arg) {

            final IWorkspace workspace = MTJUIPlugin.getWorkspace();

            final String name = projectNameGroup.getName();

            // check whether the project name field is empty
            if (name.length() == 0) {
                setErrorMessage(null);
                setMessage(MTJUIMessages.NewMidletProjectWizardPageOne_projectNameGroup_update_error_emptyName);
                setPageComplete(false);
                return;
            }

            // check whether the project name is valid
            final IStatus nameStatus = workspace.validateName(name,
                    IResource.PROJECT);
            if (!nameStatus.isOK()) {
                setErrorMessage(nameStatus.getMessage());
                setPageComplete(false);
                return;
            }

            // check whether project already exists
            final IProject handle = workspace.getRoot().getProject(name);
            if (handle.exists()) {
                setErrorMessage(MTJUIMessages.NewMidletProjectWizardPageOne_projectNameGroup_update_error_alreadyExists);
                setPageComplete(false);
                return;
            }

            IPath projectLocation = ResourcesPlugin.getWorkspace().getRoot()
                    .getLocation().append(name);
            if (projectLocation.toFile().exists()) {
                try {
                    // correct casing
                    String canonicalPath = projectLocation.toFile()
                            .getCanonicalPath();
                    projectLocation = new Path(canonicalPath);
                } catch (IOException e) {
                    MTJCorePlugin.log(IStatus.ERROR, e);
                }

                String existingName = projectLocation.lastSegment();
                if (!existingName.equals(projectNameGroup.getName())) {
                    setErrorMessage(MTJUIMessages.NewMidletProjectWizardPageOne_projectNameGroup_update_error_alreadyExists);
                    setPageComplete(false);
                    return;
                }

            }

            final String location = projectLocationGroup.getLocation()
                    .toOSString();

            // check whether location is empty
            if (location.length() == 0) {
                setErrorMessage(null);
                setMessage(MTJUIMessages.NewMidletProjectWizardPageOne_projectNameGroup_update_error_missingLocation);
                setPageComplete(false);
                return;
            }

            // check whether the location is a syntactically correct path
            if (!Path.EMPTY.isValidPath(location)) {
                setErrorMessage(MTJUIMessages.NewMidletProjectWizardPageOne_projectNameGroup_update_error_invalidDirectory);
                setPageComplete(false);
                return;
            }

            IPath projectPath = Path.fromOSString(location);

            if (projectLocationGroup.isWorkspaceRadioSelected()) {
                projectPath = projectPath.append(projectNameGroup.getName());
            }

            if (projectPath.toFile().exists()) {// create from existing source
                if (Platform.getLocation().isPrefixOf(projectPath)) { // create
                    // from existing source in workspace
                    if (!Platform.getLocation().equals(
                            projectPath.removeLastSegments(1))) {
                        setErrorMessage(MTJUIMessages.NewMidletProjectWizardPageOne_projectNameGroup_update_error_workspace1);
                        setPageComplete(false);
                        return;
                    }

                    if (!projectPath.toFile().exists()) {
                        setErrorMessage(MTJUIMessages.NewMidletProjectWizardPageOne_projectNameGroup_update_error_workspace2);
                        setPageComplete(false);
                        return;
                    }
                }
            } else if (!projectLocationGroup.isWorkspaceRadioSelected()) {// create
                // at
                // non existing external location
                if (!canCreate(projectPath.toFile())) {
                    setErrorMessage(MTJUIMessages.NewMidletProjectWizardPageOne_projectNameGroup_update_error_failedCreateContents);
                    setPageComplete(false);
                    return;
                }

                // If we do not place the contents in the workspace validate the
                // location.
                final IStatus locationStatus = workspace
                        .validateProjectLocation(handle, projectPath);
                if (!locationStatus.isOK()) {
                    setErrorMessage(locationStatus.getMessage());
                    setPageComplete(false);
                    return;
                }
            }

            IStatus status;

            status = isValidJadName(jadNameGroup.getJadFileName());
            if (!status.isOK()) {
                setErrorMessage(status.getMessage());
                setPageComplete(false);
                return;
            }

            // Validate if there is at least one configuration added
            status = isValidConfigurations();
            if (!status.isOK()) {
                setErrorMessage(status.getMessage());
                setPageComplete(false);
                return;
            }

            setPageComplete(true);
            setErrorMessage(null);
            setMessage(null);
        }
    }

    private static final String PAGE_NAME = "NewJavaProjectWizardPageOne"; //$NON-NLS-1$

    private final ConfigurationGroup configurationGroup;

    private final JadNameGroup jadNameGroup;

    private Button preprocessedButton;

    private boolean preprocessingEnabled;
    private final LocationGroup projectLocationGroup;

    private final ProjectNameGroup projectNameGroup;

    private final Validator wizardPageValidator;

    /**
     * Creates a new NewMidletProjectWizardPageOne
     */
    public NewMidletProjectWizardPageOne() {
        super(PAGE_NAME);
        setPageComplete(false);
        setTitle(MTJUIMessages.NewMidletProjectWizardPageOne_title);
        setDescription(MTJUIMessages.NewMidletProjectWizardPageOne_description);

        projectNameGroup = new ProjectNameGroup();
        jadNameGroup = new JadNameGroup();
        projectLocationGroup = new LocationGroup();
        configurationGroup = new ConfigurationGroup();

        // establish connections
        projectNameGroup.addObserver(projectLocationGroup);
        projectNameGroup.addObserver(jadNameGroup);

        // initialize all elements
        projectNameGroup.notifyObservers();

        // create and connect validator
        wizardPageValidator = new Validator();
        projectNameGroup.addObserver(wizardPageValidator);
        configurationGroup.addObserver(wizardPageValidator);
        projectLocationGroup.addObserver(wizardPageValidator);
        jadNameGroup.addObserver(wizardPageValidator);

        // initialize defaults
        setProjectName(""); //$NON-NLS-1$
        setProjectLocationURI(null);

        JavaRuntime.getDefaultVMInstall();
    }

    /**
     * Creates the controls for the configuration management.
     * 
     * @param composite the parent composite
     * @return the created control
     */
    protected Control createConfigGroupControl(Composite composite) {
        return configurationGroup.createControl(composite);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets
     * .Composite)
     */
    public void createControl(Composite parent) {
        initializeDialogUnits(parent);

        final Composite composite = new Composite(parent, SWT.NULL);
        composite.setFont(parent.getFont());
        composite.setLayout(initGridLayout(new GridLayout(1, false), true));
        composite.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));

        // create UI elements
        Control nameControl = createNameControl(composite);
        nameControl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Control jadControl = createJadNameControl(composite);
        jadControl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Control locationControl = createLocationControl(composite);
        locationControl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Control configControl = createConfigGroupControl(composite);
        configControl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Control preprocessorControl = createPreprocessorGroupSelectionControl(composite);
        preprocessorControl
                .setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        setControl(composite);
    }

    /**
     * @param composite
     * @return
     */
    protected Control createJadNameControl(Composite composite) {
        return jadNameGroup.createControl(composite);
    }

    /**
     * Creates the controls for the location field.
     * 
     * @param composite the parent composite
     * @return the created control
     */
    protected Control createLocationControl(Composite composite) {
        return projectLocationGroup.createControl(composite);
    }

    /**
     * Creates the controls for the name field.
     * 
     * @param composite the parent composite
     * @return the created control
     */
    protected Control createNameControl(Composite composite) {
        return projectNameGroup.createControl(composite);
    }

    /**
     * @param composite
     * @return
     */
    protected Control createPreprocessorGroupSelectionControl(
            Composite composite) {

        // Add an extra composite to get the layout to match up the
        // components vertically
        Group preprocessorGroup = new Group(composite, SWT.NONE);
        preprocessorGroup.setLayout(new GridLayout(1, true));
        preprocessorGroup.setLayoutData(new GridData(GridData.FILL_BOTH));
        preprocessorGroup
                .setText(MTJUIMessages.NewMidletProjectWizardPageOne_preprocessorGroup);

        preprocessedButton = new Button(preprocessorGroup, SWT.CHECK);
        preprocessedButton
                .setText(MTJUIMessages.NewMidletProjectWizardPageOne_preprocessor);
        preprocessedButton
                .setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        preprocessedButton.addSelectionListener(new SelectionAdapter() {

            /*
             * (non-Javadoc)
             * @see
             * org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse
             * .swt.events.SelectionEvent)
             */
            @Override
            public void widgetSelected(SelectionEvent e) {
                preprocessingEnabled = preprocessedButton.getSelection();
            }
        });
        return preprocessorGroup;
    }

    @Override
    public void dispose() {
        super.dispose();
        configurationGroup.dispose();
    }

    public Configurations getConfigurations() {
        return configurationGroup.getConfigurations();
    }

    /**
     * Returns the default class path entries to be added on new projects. By
     * default this is the JRE container as selected by the user.
     * 
     * @return returns the default class path entries
     */
    public IClasspathEntry[] getDefaultClasspathEntries() {
        IPath entryPath = new Path(JavaMEClasspathContainer.JAVAME_CONTAINER
                + "/" + getSelectedDevice()); //$NON-NLS-1$

        return new IClasspathEntry[] { JavaCore.newContainerEntry(entryPath) };
    }

    /**
     * @return
     */
    public String getJadFileName() {
        return jadNameGroup.getJadFileName();
    }

    /**
     * Returns the source class path entries to be added on new projects. The
     * underlying resource may not exist.
     * 
     * @return returns the default class path entries
     */
    public IPath getOutputLocation() {
        IPath outputLocationPath = new Path(getProjectName()).makeAbsolute();

        IPath binPath = new Path(PreferenceConstants.getPreferenceStore()
                .getString(PreferenceConstants.SRCBIN_BINNAME));
        if (binPath.segmentCount() > 0) {
            outputLocationPath = outputLocationPath.append(binPath);
        }

        return outputLocationPath;
    }

    /**
     * Returns the current project location path as entered by the user, or
     * <code>null</code> if the project should be created in the workspace.
     * 
     * @return the project location path or its anticipated initial value.
     */
    public URI getProjectLocationURI() {
        if (projectLocationGroup.isLocationInWorkspace()) {
            return null;
        }
        return URIUtil.toURI(projectLocationGroup.getLocation());
    }

    /**
     * Gets a project name for the new project.
     * 
     * @return the new project resource handle
     */
    public String getProjectName() {
        return projectNameGroup.getName();
    }

    /**
     * Returns the compiler compliance to be used for the project, or
     * <code>null</code> to use the workspace compiler compliance.
     * 
     * @return compiler compliance to be used for the project or
     *         <code>null</code>
     */
    public IDevice getSelectedDevice() {
        return configurationGroup.getSelectedDevice();
    }

    /**
     * Returns the source class path entries to be added on new projects. The
     * underlying resources may not exist. All entries that are returned must be
     * of kind {@link IClasspathEntry#CPE_SOURCE}.
     * 
     * @return returns the source class path entries for the new project
     */
    public IClasspathEntry[] getSourceClasspathEntries() {
        IPath sourceFolderPath = new Path(getProjectName()).makeAbsolute();

        IPath srcPath = new Path(PreferenceConstants.getPreferenceStore()
                .getString(PreferenceConstants.SRCBIN_SRCNAME));
        if (srcPath.segmentCount() > 0) {
            sourceFolderPath = sourceFolderPath.append(srcPath);
        }

        return new IClasspathEntry[] { JavaCore
                .newSourceEntry(sourceFolderPath) };
    }

    /**
     * The wizard owning this page can call this method to initialize the fields
     * from the current selection and active part.
     * 
     * @param selection used to initialize the fields
     * @param activePart the (typically active) part to initialize the fields or
     *            <code>null</code>
     */
    public void init(IStructuredSelection selection, IWorkbenchPart activePart) {
    }

    private GridLayout initGridLayout(GridLayout layout, boolean margins) {
        layout.horizontalSpacing = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
        layout.verticalSpacing = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_SPACING);
        if (margins) {
            layout.marginWidth = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_MARGIN);
            layout.marginHeight = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_MARGIN);
        } else {
            layout.marginWidth = 0;
            layout.marginHeight = 0;
        }
        return layout;
    }

    /**
     * Return a boolean concerning whether preprocessing is enabled for the
     * newly created project.
     * 
     * @return the preprocessingEnabled
     */
    public boolean isPreprocessingEnabled() {
        return preprocessingEnabled;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.jface.dialogs.DialogPage#setControl(org.eclipse.swt.widgets
     * .Control)
     */
    @Override
    protected void setControl(Control newControl) {
        Dialog.applyDialogFont(newControl);
        super.setControl(newControl);
    }

    /**
     * Sets the project location of the new project or <code>null</code> if the
     * project should be created in the workspace
     * 
     * @param uri the new project location
     */
    public void setProjectLocationURI(URI uri) {
        IPath path = uri != null ? URIUtil.toPath(uri) : null;
        projectLocationGroup.setLocation(path);
    }

    /**
     * Sets the name of the new project
     * 
     * @param name the new name
     */
    public void setProjectName(String name) {
        if (name == null) {
            throw new IllegalArgumentException();
        }

        projectNameGroup.setName(name);
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.jface.dialogs.DialogPage#setVisible(boolean)
     */
    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (visible) {
            projectNameGroup.postSetFocus();
        }
    }
}
