/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.core.model.IBaseModel;
import org.eclipse.mtj.internal.core.text.AbstractEditingModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.ui.editor.MTJFormEditor;
import org.eclipse.mtj.internal.ui.editor.SystemFileEditorInput;
import org.eclipse.mtj.internal.ui.editor.context.XMLInputContext;
import org.eclipse.text.edits.TextEdit;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IStorageEditorInput;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class L10nInputContext extends XMLInputContext {

    public static final String CONTEXT_ID = "l10n-context"; //$NON-NLS-1$ 

    /**
     * @param editor
     * @param input
     * @param primary
     */
    public L10nInputContext(MTJFormEditor editor, IEditorInput input,
            boolean primary) {
        super(editor, input, primary);
        create();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.InputContext#doRevert()
     */
    public void doRevert() {
        fEditOperations.clear();
        fOperationTable.clear();
        fMoveOperations.clear();
        AbstractEditingModel model = (AbstractEditingModel) getModel();
        model.reconciled(model.getDocument());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.InputContext#createModel(org.eclipse.ui.IEditorInput)
     */
    @Override
    protected IBaseModel createModel(IEditorInput input) throws CoreException {

        if (input instanceof IStorageEditorInput) {
            boolean isReconciling = input instanceof IFileEditorInput;
            IDocument document = getDocumentProvider().getDocument(input);

            L10nModel model = new L10nModel(document, isReconciling);

            if (input instanceof IFileEditorInput) {
                IFile file = ((IFileEditorInput) input).getFile();
                model.setUnderlyingResource(file);
                model.setCharset(file.getCharset());
            } else if (input instanceof SystemFileEditorInput) {
                File file = (File) ((SystemFileEditorInput) input)
                        .getAdapter(File.class);
                model.setInstallLocation(file.getParent());
                model.setCharset(getDefaultCharset());
            } else {
                model.setCharset(getDefaultCharset());
            }

            model.load();

            return model;
        }

        return null;

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.InputContext#getDefaultCharset()
     */
    @Override
    protected String getDefaultCharset() {
        return "ISO-8859-1"; //$NON-NLS-1$;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.InputContext#getId()
     */
    @Override
    public String getId() {
        return CONTEXT_ID;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.InputContext#getPartitionName()
     */
    @Override
    protected String getPartitionName() {
        return "___l10n_partition"; //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.XMLInputContext#reorderInsertEdits(java.util.ArrayList)
     */
    @Override
    protected void reorderInsertEdits(ArrayList<TextEdit> ops) {

    }

}
