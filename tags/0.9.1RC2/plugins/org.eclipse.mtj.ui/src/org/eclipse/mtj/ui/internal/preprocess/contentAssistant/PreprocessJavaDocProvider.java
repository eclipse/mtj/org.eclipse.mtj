/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma          (Sybase) - Initial implementation
 */
package org.eclipse.mtj.ui.internal.preprocess.contentAssistant;

/**
 * provide java doc for preprocess elements
 * 
 * @author gma
 */
public class PreprocessJavaDocProvider {
    private static PreprocessJavaDocProvider fDefault;

    private static final String NEW_LINE_HTML = "<BR>";

    public static PreprocessJavaDocProvider getDefault() {
        if (fDefault == null) {
            fDefault = new PreprocessJavaDocProvider();
        }
        return fDefault;
    }

    private PreprocessJavaDocProvider() {

    }

    private String getDirectiveDocKey(PreprocessDirective directive) {
        return directive.getName() + "_javadoc";
    }

    /**
     * @param directive
     * @return the directive's java doc
     */
    public String getDirectiveJavaDoc(PreprocessDirective directive) {
        String key = getDirectiveDocKey(directive);
        return PreprocessContentAssistMessages.getString(key);
    }

    /**
     * @param symbol
     * @return the symbol's java doc
     */
    public String getSymbolJavaDoc(PreprocessSymbol symbol) {
        StringBuffer sb = new StringBuffer();
        sb.append("Followings are the symbol provider's information:" + NEW_LINE_HTML
                + NEW_LINE_HTML);

        for (PreprocessSymbolProviderInfo provider : symbol.getProviderInfos()) {
            showProviderInfo(provider, sb);
            sb.append(NEW_LINE_HTML);
        }
        return sb.toString();
    }

    private void showProviderInfo(PreprocessSymbolProviderInfo provider,
            StringBuffer sb) {
        if (provider.isActive) {
            sb.append("<B>");
        }
        sb.append("    ");
        sb.append(provider.getProviderName());
        sb.append(" - ");
        sb.append(provider.getProvidedSymbolValue());
        if (provider.isActive) {
            sb.append("</B>");
        }
    }
}
