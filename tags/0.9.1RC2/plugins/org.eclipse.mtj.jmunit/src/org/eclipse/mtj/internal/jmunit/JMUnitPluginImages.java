/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.jmunit;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.mtj.jmunit.JMUnitPlugin;
import org.eclipse.swt.graphics.Image;

/**
 * Bundle of all images used by the JMUnit plug-in.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class JMUnitPluginImages {

    public final static String ICONS_PATH = "icons/full/"; //$NON-NLS-1$

    private static ImageRegistry PLUGIN_REGISTRY;

    /* The standard icon folders */
    public static final String PATH_TOOL = ICONS_PATH + "etool16/"; //$NON-NLS-1$
    public static final String PATH_WIZBAN = ICONS_PATH + "wizban/"; //$NON-NLS-1$

    /* Image Descriptors */
    public static final ImageDescriptor DESC_NEW_TEST_CASE = create(PATH_TOOL,
            "new_testcase.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_NEW_TEST_SUITE = create(PATH_TOOL,
            "new_testsuite.gif"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_NEW_TEST_CASE_WIZ = create(
            PATH_WIZBAN, "newtest_wiz.png"); //$NON-NLS-1$

    public static final ImageDescriptor DESC_NEW_TEST_SUITE_WIZ = create(
            PATH_WIZBAN, "newsuite_wiz.png"); //$NON-NLS-1$

    /**
     * @param key
     * @return
     */
    public static Image get(String key) {
        if (PLUGIN_REGISTRY == null) {
            initialize();
        }
        return PLUGIN_REGISTRY.get(key);
    }

    /**
     * @param key
     * @param desc
     * @return
     */
    public static Image manage(String key, ImageDescriptor desc) {
        Image image = desc.createImage();
        PLUGIN_REGISTRY.put(key, image);
        return image;
    }

    /**
     * @param prefix
     * @param name
     * @return
     */
    private static ImageDescriptor create(String prefix, String name) {
        return ImageDescriptor.createFromURL(makeImageURL(prefix, name));
    }

    /**
     * 
     */
    private static final void initialize() {
        PLUGIN_REGISTRY = new ImageRegistry();
    }

    /**
     * @param prefix
     * @param name
     * @return
     */
    private static URL makeImageURL(String prefix, String name) {
        String path = "$nl$/" + prefix + name; //$NON-NLS-1$
        return FileLocator.find(JMUnitPlugin.getDefault().getBundle(), new Path(
                path), null);
    }
}
