/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version
 *     David Marques (Motorola) - Implementing updateSetupSuiteMethod method.
 *     David Marques (Motorola) - Fixing tag formatting behavior.
 */
package org.eclipse.mtj.internal.jmunit.core.api;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IBuffer;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.formatter.CodeFormatter;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.internal.jmunit.IJMUnitContants;
import org.eclipse.osgi.util.NLS;

/**
 * TestSuiteWriter Class writes test suites.
 */
public class TestSuiteWriter extends AbstractTestWriter {

    private String suiteName;

    /**
     * Creates a test suite writer.
     * 
     * @param _type target type.
     * @param _name suite name.
     * @throws JavaModelException Any java model error.
     */
    public TestSuiteWriter(IType _type, String _name) throws JavaModelException {
        super(_type);
        this.suiteName = _name;
    }

    /**
     * Updates the setup method adding the specified classes to the suite.
     * 
     * @param _testCases TestCase class names.
     * @param _monitor active monitor.
     * @throws BadLocationException Attempt to access a non-existing position
     * @throws JavaModelException Any java model error.
     */
    public void updateSetupSuiteMethod(String[] _testClasses,
            IProgressMonitor _monitor) throws JavaModelException {
        ICompilationUnit compilationUnit = this.type.getCompilationUnit();
        IDocument document = new Document(compilationUnit.getBuffer()
                .getContents());
        IMethod method = this.type.getMethod("setupSuite", new String[0]);
        try {
            if (method != null) {
                ISourceRange methodRange = method.getSourceRange();
                StringBuffer methodBuffer = new StringBuffer(document.get(
                        methodRange.getOffset(), methodRange.getLength()));
                StringBuffer adds = new StringBuffer();
                adds.append(delimiter);
                for (String test : _testClasses) {
                    adds.append((NLS.bind("add(new {0}());", test)));
                    adds.append(delimiter);
                }

                int start = methodBuffer
                        .indexOf(IJMUnitContants.NON_COMMENT_START_MARKER)
                        + IJMUnitContants.NON_COMMENT_START_MARKER.length();
                int end = methodBuffer.indexOf(IJMUnitContants.COMMENT_START,
                        start);
                methodBuffer.replace(start, end, adds.toString());

                document.replace(methodRange.getOffset(), methodRange
                        .getLength(), methodBuffer.toString());

                String formatted = Utils.codeFormat(compilationUnit
                        .getJavaProject(), document.get(),
                        CodeFormatter.K_COMPILATION_UNIT, 0, delimiter);

                IBuffer buf = compilationUnit.getBuffer();
                buf.replace(0, buf.getLength(), formatted);
                compilationUnit.save(new NullProgressMonitor(), true);
            }
        } catch (BadLocationException e) {
            MTJCorePlugin.log(IStatus.ERROR, e);
        }
    }

    /**
     * Writes the test suite code adding the specified classes to the suite.
     * 
     * @param _testCases test case classes.
     * @param _monitor active monitor.
     * @throws JavaModelException Any java model error.
     */
    public void writeCode(String[] _testCases, IProgressMonitor _monitor)
            throws JavaModelException {
        this.writeConstructor(_monitor);
        this.writeSuiteSetup(_testCases, _monitor);
    }

    /**
     * Writes the TestSuite constructor.
     * 
     * @param _monitor active monitor.
     * @return the constructor method.
     * @throws JavaModelException Any java model error.
     */
    private IMethod writeConstructor(IProgressMonitor _monitor)
            throws JavaModelException {
        StringBuffer buffer = new StringBuffer();

        String[] comment = { "TestSuite Class constructor initializes the test suite." }; //$NON-NLS-1$

        this.writeMethodComment(buffer, comment, delimiter);

        String declaration = NLS.bind("public {0}()", type.getElementName()); //$NON-NLS-1$
        String[] body = { NLS.bind("super(\"{0}\");", this.suiteName), //$NON-NLS-1$
                "this.setupSuite();" }; //$NON-NLS-1$

        this.writeMethodDeclaration(buffer, declaration, body, delimiter);

        return this.type.createMethod(buffer.toString(), null, false, _monitor);
    }

    /**
     * Writes the setup method adding the specified classes to the suite.
     * 
     * @param _testCases TestCase class names.
     * @param _monitor active monitor.
     * @return the constructor method.
     * @throws JavaModelException Any java model error.
     */
    private IMethod writeSuiteSetup(String[] _testCases,
            IProgressMonitor _monitor) throws JavaModelException {
        StringBuffer buffer = new StringBuffer();

        String[] comment = { "This method adds all suite test cases to be run." }; //$NON-NLS-1$

        this.writeMethodComment(buffer, comment, delimiter);

        String declaration = "private void setupSuite()"; //$NON-NLS-1$

        List<String> body = new LinkedList<String>();

        body.add(IJMUnitContants.START_MARKER);
        for (String testCase : _testCases) {
            body.add(NLS.bind("add(new {0}());", testCase)); //$NON-NLS-1$
        }
        body.add(IJMUnitContants.END_MARKER);
        this.writeMethodDeclaration(buffer, declaration, body
                .toArray(new String[body.size()]), delimiter);

        return this.type.createMethod(buffer.toString(), null, false, _monitor);
    }

}
