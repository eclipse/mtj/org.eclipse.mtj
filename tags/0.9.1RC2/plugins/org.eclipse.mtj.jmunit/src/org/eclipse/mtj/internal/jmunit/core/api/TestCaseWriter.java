/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.jmunit.core.api;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.osgi.util.NLS;

/**
 * TestCaseWriter Class writes JMUnit TestCase class source code based on a
 * SourceType and several methods to run tests on.
 * 
 * @author David Marques
 */
public class TestCaseWriter extends AbstractTestWriter {

    private Hashtable<String, Integer> overrides;
    private String test;

    /**
     * Creates a TestCaseWriter instance for the specified type.
     * 
     * @param _type target type.
     * @param _name TestCase name.
     * @throws JavaModelException Any java model error.
     */
    public TestCaseWriter(IType _type, String _name) throws JavaModelException {
        super(_type);
        this.overrides = new Hashtable<String, Integer>();
        this.test = _name;
    }

    /**
     * Writes a TestCase code.
     * 
     * @param _methods test methods.
     * @param _setup flag to enable/disable setup creation.
     * @param _teardown flag to enable/disable tearDown creation.
     * @param _comment flag to enable/disable comment creation.
     * @param _stub flag to enable/disable task creation.
     * @param _final flag to set test methods as final.
     * @param _monitor active monitor.
     * @throws JavaModelException Any java model error.
     */
    public void writeCode(IMethod[] _methods, boolean _setup,
            boolean _teardown, boolean _comment, boolean _stub, boolean _final,
            IProgressMonitor _monitor) throws JavaModelException {
        this.delimiter = this.type.getTypeRoot().findRecommendedLineSeparator();

        writeConstructor(_methods.length, _monitor);
        List<String> methodNames = writeTestMethods(_methods, _comment, _stub,
                _final, _monitor);

        if (_setup) {
            writeSetup(_monitor);
        }

        if (_teardown) {
            writeTearDown(_monitor);
        }
        writeTestMethod(methodNames, _monitor);
    }

    /**
     * Writes the TestCase constructor.
     * 
     * @param testNumber Number of tests.
     * @param _monitor Active monitor.
     * @return the created method.
     * @throws JavaModelException Any java model error.
     */
    private IMethod writeConstructor(int testNumber, IProgressMonitor _monitor)
            throws JavaModelException {
        StringBuffer buffer = new StringBuffer();

        String comment[] = new String[] {
                "The default constructor. It just transmits the necessary informations to", //$NON-NLS-1$
                "the superclass.", //$NON-NLS-1$
                "", //$NON-NLS-1$
                "@param totalOfTests the total of test methods present in the class.", //$NON-NLS-1$
                "@param name this testcase's name." }; //$NON-NLS-1$
        this.writeMethodComment(buffer, comment, delimiter);

        String declaration = NLS.bind("public {0}()", this.type //$NON-NLS-1$
                .getElementName());
        String body[] = new String[] { NLS.bind("super({0}, \"{1}\");", //$NON-NLS-1$
                new String[] { String.valueOf(testNumber), this.test }) };
        this.writeMethodDeclaration(buffer, declaration, body, delimiter);

        return this.type.createMethod(buffer.toString(), null, false, _monitor);
    }

    /**
     * Writes a setup method.
     * 
     * @param _monitor Active monitor.
     * @return the created method.
     * @throws JavaModelException Any java model error.
     */
    private IMethod writeSetup(IProgressMonitor _monitor)
            throws JavaModelException {
        StringBuffer buffer = new StringBuffer();

        String comment[] = new String[] {
                "A empty method used by the framework to initialize the tests. If there's", //$NON-NLS-1$
                "5 test methods, the setUp is called 5 times, one for each method. The", //$NON-NLS-1$
                "setUp occurs before the method's execution, so the developer can use it", //$NON-NLS-1$
                "to any necessary initialization. It's necessary to override it, however.", //$NON-NLS-1$
                "", //$NON-NLS-1$
                "@throws Throwable anything that the initialization can throw.", }; //$NON-NLS-1$
        this.writeMethodComment(buffer, comment, delimiter);

        String declaration = "public void setUp() throws Throwable"; //$NON-NLS-1$

        this.writeMethodDeclaration(buffer, declaration, null, delimiter);

        return this.type.createMethod(buffer.toString(), null, false, _monitor);
    }

    /**
     * Writes a tearDown method.
     * 
     * @param _monitor Active monitor.
     * @return the created method.
     * @throws JavaModelException Any java model error.
     */
    private IMethod writeTearDown(IProgressMonitor _monitor)
            throws JavaModelException {
        StringBuffer buffer = new StringBuffer();

        String comment[] = new String[] {
                "A empty mehod used by the framework to release resources used by the", //$NON-NLS-1$
                "tests. If there's 5 test methods, the tearDown is called 5 times, one for", //$NON-NLS-1$
                "each method. The tearDown occurs after the method's execution, so the", //$NON-NLS-1$
                "developer can use it to close something used in the test, like a", //$NON-NLS-1$
                "nputStream or the RMS. It's necessary to override it, however." }; //$NON-NLS-1$
        this.writeMethodComment(buffer, comment, delimiter);

        String declaration = "public void tearDown()"; //$NON-NLS-1$

        this.writeMethodDeclaration(buffer, declaration, null, delimiter);

        return this.type.createMethod(buffer.toString(), null, false, _monitor);
    }

    /**
     * Creates the the test method.
     * 
     * @param methodNames the test method names.
     * @param _monitor Active monitor.
     * @return the created method.
     * @throws JavaModelException Any java model error.
     */
    private IMethod writeTestMethod(List<String> methodNames,
            IProgressMonitor _monitor) throws JavaModelException {
        StringBuffer buffer = new StringBuffer();

        String comment[] = new String[] {
                "This method stores all the test methods invocation. The developer must", //$NON-NLS-1$
                "implement this method with a switch-case. The cases must start from 0 and", //$NON-NLS-1$
                "increase in steps of one until the number declared as the total of tests", //$NON-NLS-1$
                "in the constructor, exclusive. For example, if the total is 3, the cases", //$NON-NLS-1$
                "must be 0, 1 and 2. In each case, there must be a test method invocation.", //$NON-NLS-1$
                "", "@param testNumber the test to be executed.", //$NON-NLS-1$ //$NON-NLS-2$
                "@throws Throwable anything that the executed test can throw." }; //$NON-NLS-1$
        this.writeMethodComment(buffer, comment, delimiter);

        List<String> body = new LinkedList<String>();

        int index = 0x00;
        body.add("switch(testNumber) {"); //$NON-NLS-1$
        for (String methodName : methodNames) {
            body.add(NLS.bind("case {0} :", String.valueOf(index++))); //$NON-NLS-1$
            body.add(NLS.bind("{0}();", methodName)); //$NON-NLS-1$
            body.add("break;"); //$NON-NLS-1$
        }
        body.add("}"); //$NON-NLS-1$

        String declaration = "public void test(int testNumber) throws Throwable"; //$NON-NLS-1$
        this.writeMethodDeclaration(buffer, declaration, body
                .toArray(new String[body.size()]), delimiter);

        return this.type.createMethod(buffer.toString(), null, false, _monitor);
    }

    /**
     * Writes all specified test methods.
     * 
     * @param _methods test methods.
     * @param _comment flag to enable/disable comment creation.
     * @param _stub flag to enable/disable task creation.
     * @param _final flag to set test methods as final.
     * @param _monitor Active monitor.
     * @return a list of method names created.
     * @throws JavaModelException Any java model error.
     */
    private List<String> writeTestMethods(IMethod[] _methods, boolean _comment,
            boolean _stub, boolean _final, IProgressMonitor _monitor)
            throws JavaModelException {
        List<String> result = new LinkedList<String>();
        for (IMethod method : _methods) {
            String testMethodName = resolveMethodName(method.getElementName());
            writeTestMethod(method, testMethodName, _comment, _stub, _final,
                    _monitor);
            result.add(testMethodName);
        }
        return result;
    }

    /**
     * Wites a test method code.
     * 
     * @param _method target method instance.
     * @param _testMethodName test method name.
     * @param _comment flag to enable/disable comment creation.
     * @param _stub flag to enable/disable task creation.
     * @param _final flag to set test methods as final.
     * @param _monitor Active monitor.
     * @return the created method.
     * @throws JavaModelException Any java model error.
     */
    private IMethod writeTestMethod(IMethod _method, String _testMethodName,
            boolean _comment, boolean _stub, boolean _final,
            IProgressMonitor _monitor) throws JavaModelException {
        StringBuffer buffer = new StringBuffer();

        if (_comment) {
            writeMethodComment(buffer, _method);
        }

        String declaration = NLS.bind("public {0} void {1}()", new String[] { //$NON-NLS-1$
                _final ? "final" : "", _testMethodName }); //$NON-NLS-1$ //$NON-NLS-2$
        String[] body = null;

        if (_stub) {
            body = new String[] { "fail(\"Not Yet Implemented.\"); // TODO" }; //$NON-NLS-1$
        } else {
            body = new String[] { "fail(\"Not Yet Implemented.\");" }; //$NON-NLS-1$
        }
        this.writeMethodDeclaration(buffer, declaration, body, delimiter);

        return this.type.createMethod(buffer.toString(), null, false, _monitor);
    }

    /**
     * Creates a method comment.
     * 
     * @param buffer target buffer.
     * @param _method method instance.
     * @throws JavaModelException Any java model error.
     */
    private void writeMethodComment(StringBuffer buffer, IMethod _method)
            throws JavaModelException {

        String[] comments = {
                NLS.bind("Test for method: {0}.", Signature.toString(_method //$NON-NLS-1$
                        .getSignature(), _method.getElementName(), _method
                        .getParameterNames(), true, true)),
                "", //$NON-NLS-1$
                NLS.bind("@see {0}#{1}", new String[] { //$NON-NLS-1$
                        _method.getDeclaringType().getFullyQualifiedName(),
                        Signature.toString(_method.getSignature(), _method
                                .getElementName(), _method.getParameterNames(),
                                true, false) })

        };
        
        /*Replace slashes by dots on class full names.*/
        for (int i = 0; i < comments.length; i++) {
            comments[i] = comments[i].replace("/", "."); //$NON-NLS-1$ //$NON-NLS-2$
        }
        
        this.writeMethodComment(buffer, comments, delimiter);
    }

    /**
     * Resolves overwritten methods name.
     * 
     * @param _name target method name.
     * @return the test method name.
     */
    private String resolveMethodName(String _name) {
        boolean isOverloaded = false;
        int sequenceNum = 0x01;
        for (String name : this.overrides.keySet()) {
            if (name.equals(_name)) {
                sequenceNum += this.overrides.get(name);
                this.overrides.put(name, sequenceNum);
                isOverloaded = true;
            }
        }

        if (!isOverloaded) {
            this.overrides.put(_name, sequenceNum);
        }

        return NLS.bind("{0}{1}Test", new String[] { _name, //$NON-NLS-1$
                String.valueOf(sequenceNum) });
    }
}
