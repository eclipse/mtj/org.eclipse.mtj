/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.hook.sourceMapper;

import org.eclipse.core.resources.IFile;
import org.eclipse.mtj.core.hooks.Debug;

/**
 * Provides a static registration location for the currently registered
 * {@link SourceMapper} instance.
 * 
 * @author Craig Setera
 */
public class SourceMapperAccess {

    // The currently registered source mapper or null if none
    // is currently registered.
    private static SourceMapper sourceMapper;

    /**
     * Return a mapped source file for the specified file or <code>null</code>
     * if the file is not mapped. This method will check for a currently
     * registered {@link SourceMapper} instance and use that for the mapping if
     * it has been set.
     * 
     * @param file
     * @return
     */
    public static IFile getMappedSourceFile(IFile file) {
        IFile mapped = null;

        if (sourceMapper != null) {
            mapped = sourceMapper.getMappedResource(file);
        }

        return mapped;
    }

    /**
     * Return a boolean indicating whether the hook code was properly installed.
     * 
     * @return
     */
    public static boolean isHookCodeInstalled() {
        // NOTE: Don't change this in the source code.
        // The hook implementation will rewrite this
        // class to return true to this call. This is
        // necessary since the hook is loaded from a different
        // classloader, thus static variables and methods
        // are not shared with the callers.
        return false;
    }

    /**
     * Set the current {@link SourceMapper} instance used to map SourceFile
     * {@link IFile} instances to a different IFile instance.
     * 
     * @param newSourceMapper
     */
    public static void setSourceMapper(SourceMapper newSourceMapper) {
        if (Debug.DEBUG_GENERAL) {
            System.out.println("Setting the source mapper to: "
                    + newSourceMapper);
        }

        sourceMapper = newSourceMapper;
    }

    // Private constructor for static access
    private SourceMapperAccess() {
    }
}
