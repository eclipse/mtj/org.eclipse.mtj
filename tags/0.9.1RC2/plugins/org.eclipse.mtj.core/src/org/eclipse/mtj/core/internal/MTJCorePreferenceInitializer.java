/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Hugo Raniere (Motorola)  - Default value for default preverifier
 *     Gang Ma      (Sybase)    - Default value for preprocess debug level
 */
package org.eclipse.mtj.core.internal;

import org.eclipse.core.runtime.Preferences;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.preprocessor.PreprocessorHelper;

/**
 * Preference initializer for default MTJ preferences.
 * 
 * @author Craig Setera
 */
public class MTJCorePreferenceInitializer extends AbstractPreferenceInitializer
        implements IMTJCoreConstants {

    // Default values
    public static final String PREF_DEF_DEPLOYMENT_DIR = "deployed";
    public static final String PREF_DEF_VERIFIED_DIR = "verified";

    public static final boolean PREF_DEF_USE_RESOURCES_DIR = true;
    public static final String PREF_DEF_RESOURCES_DIR = "res";
    public static final boolean PREF_DEF_FORCE_JAVA11 = true;

    public static final boolean PREF_DEF_OTA_SERVER_START_AT_START = false;
    public static final boolean PREF_DEF_OTA_PORT_DEFINED = false;
    public static final int PREF_DEF_OTA_PORT = 0;
    public static final boolean PREF_DEF_OTA_AUTODEPLOY = true;

    public static final boolean PREF_DEF_OBFUSCATION_USE_PROJECT = false;
    public static final String PREF_DEF_PROGUARD_DIR = "";
    public static final boolean PREF_DEF_PROGUARD_USE_SPECIFIED = false;
    public static final String PREF_DEF_PROGUARD_OPTIONS = "-dontusemixedcaseclassnames -dontnote -defaultpackage \'\'";
    public static final String PREF_DEF_PROGUARD_KEEP = "public class * extends javax.microedition.midlet.MIDlet";

    public static final boolean PREF_DEF_PKG_USE_PROJECT = false;
    public static final boolean PREF_DEF_PKG_AUTOVERSION = false;
    public static final String PREF_DEF_PKG_EXCLUDED_PROPS = "MIDlet-Jar-URL|MIDlet-Jar-Size";

    public static final boolean PREF_DEF_PREVERIFY_USE_PROJECT = false;
    public static final String PREF_DEF_PREVERIFY_CONFIG_LOCATION = PREF_PREVERIFY_CONFIG_LOCATION_PLATFORM;
    public static final String PREF_DEF_PREVERIFY_CONFIG_VALUE = "";
    public static final String PREF_DEF_DEFAULT_PREVERIFIER = "";

    public static final String PREF_DEF_WTK_ROOT = "";
    public static final String PREF_DEF_ANTENNA_JAR = "";

    public static final int PREF_DEF_RMTDBG_DELAY = 60000;
    public static final int PREF_DEF_RMTDBG_INTERVAL = 500;
    public static final boolean PREF_DEF_AUTO_LAUNCH_MIGRATION = true;
    
    public static final String PREF_DEF_PREPROCESS_DEBUGLEVEL = PreprocessorHelper.J2ME_PREPROCESS_DEBUG;

    /**
     * Set up the default preferences in the specified preference store.
     * 
     * @param prefs
     */
    public static void initializeDefaultPreferences(Preferences prefs) {

        prefs.setDefault(PREF_DEPLOYMENT_DIR, PREF_DEF_DEPLOYMENT_DIR);
        prefs.setDefault(PREF_RESOURCES_DIR, PREF_DEF_RESOURCES_DIR);

        prefs.setDefault(PREF_USE_RESOURCES_DIR, PREF_DEF_USE_RESOURCES_DIR);
        prefs.setDefault(PREF_VERIFIED_DIR, PREF_DEF_VERIFIED_DIR);
        prefs.setDefault(PREF_FORCE_JAVA11, PREF_DEF_FORCE_JAVA11);

        prefs.setDefault(PREF_OTA_SERVER_START_AT_START,
                PREF_DEF_OTA_SERVER_START_AT_START);
        prefs.setDefault(PREF_OTA_PORT_DEFINED, PREF_DEF_OTA_PORT_DEFINED);
        prefs.setDefault(PREF_OTA_PORT, PREF_DEF_OTA_PORT);
        prefs.setDefault(PREF_OTA_AUTODEPLOY, PREF_DEF_OTA_AUTODEPLOY);

        prefs.setDefault(PREF_OBFUSCATION_USE_PROJECT,
                PREF_DEF_OBFUSCATION_USE_PROJECT);
        prefs.setDefault(PREF_PROGUARD_DIR, PREF_DEF_PROGUARD_DIR);
        prefs.setDefault(PREF_PROGUARD_USE_SPECIFIED,
                PREF_DEF_PROGUARD_USE_SPECIFIED);
        prefs.setDefault(PREF_PROGUARD_OPTIONS, PREF_DEF_PROGUARD_OPTIONS);
        prefs.setDefault(PREF_PROGUARD_KEEP, PREF_DEF_PROGUARD_KEEP);

        prefs.setDefault(PREF_PKG_USE_PROJECT, PREF_DEF_PKG_USE_PROJECT);
        prefs.setDefault(PREF_PKG_AUTOVERSION, PREF_DEF_PKG_AUTOVERSION);
        prefs.setDefault(PREF_PKG_EXCLUDED_PROPS, PREF_DEF_PKG_EXCLUDED_PROPS);

        prefs.setDefault(PREF_PREVERIFY_USE_PROJECT,
                PREF_DEF_PREVERIFY_USE_PROJECT);
        prefs.setDefault(PREF_PREVERIFY_CONFIG_LOCATION,
                PREF_DEF_PREVERIFY_CONFIG_LOCATION);
        prefs.setDefault(PREF_PREVERIFY_CONFIG_VALUE,
                PREF_DEF_PREVERIFY_CONFIG_VALUE);
        prefs.setDefault(PREF_DEFAULT_PREVERIFIER,
        	PREF_DEF_DEFAULT_PREVERIFIER);

        prefs.setDefault(PREF_ANTENNA_JAR, PREF_DEF_ANTENNA_JAR);
        prefs.setDefault(PREF_WTK_ROOT, PREF_DEF_WTK_ROOT);

        prefs.setDefault(PREF_RMTDBG_TIMEOUT, PREF_DEF_RMTDBG_DELAY);
        prefs.setDefault(PREF_RMTDBG_INTERVAL, PREF_DEF_RMTDBG_INTERVAL);
        prefs.setDefault(PREF_AUTO_LAUNCH_MIGRATION,
                PREF_DEF_AUTO_LAUNCH_MIGRATION);
        
        prefs.setDefault(PREF_PREPROCESS_DEBUG_LEVEL, PREF_DEF_PREPROCESS_DEBUGLEVEL);
    }

    /**
     * Constructor
     */
    public MTJCorePreferenceInitializer() {
        super();
    }

    /**
     * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
     */
    public void initializeDefaultPreferences() {
        Preferences prefs = MTJCorePlugin.getDefault().getPluginPreferences();
        initializeDefaultPreferences(prefs);
    }
}
