/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.model.impl;

import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.launching.LibraryLocation;
import org.eclipse.mtj.core.importer.LibraryImporter;
import org.eclipse.mtj.core.model.Classpath;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;

/**
 * A special version of the Classpath class that is capable of appending or
 * prepending the primary {@link IVMInstall} classpath libraries to this
 * classpath dynamically.
 * 
 * @author Craig Setera
 */
public class VMInstallClasspath extends Classpath {

    private boolean prependVMLibraries;
    private ILibrary[] entriesCache;

    /**
     * Construct a new classpath with the libraries prepended or appended
     * according to the provided boolean.
     * 
     * @param prependVMLibraries
     */
    public VMInstallClasspath() {
        super();
    }

    /**
     * @see org.eclipse.mtj.core.model.Classpath#getEntries()
     */
    public ILibrary[] getEntries() {
        if (entriesCache == null) {
            entriesCache = calculateEntries();
        }

        return entriesCache;
    }

    public boolean isPrependVMLibraries() {
        return prependVMLibraries;
    }

    /**
     * @see org.eclipse.mtj.core.model.Classpath#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.loadUsing(persistenceProvider);
        persistenceProvider.loadBoolean("prependVMLibraries");
    }

    public void setPrependVMLibraries(boolean prependVMLibraries) {
        this.prependVMLibraries = prependVMLibraries;
    }

    /**
     * @see org.eclipse.mtj.core.model.Classpath#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        super.storeUsing(persistenceProvider);
        persistenceProvider.storeBoolean("prependVMLibraries",
                prependVMLibraries);
    }

    /**
     * Calculate the entries for this classpath based on the current default VM
     * installation.
     * 
     * @return
     */
    private ILibrary[] calculateEntries() {
        ILibrary[] userLibs = super.getEntries();

        // Retrieve the libraries from the default VM Install
        LibraryLocation[] libraryLocations = JavaRuntime
                .getLibraryLocations(JavaRuntime.getDefaultVMInstall());

        // Build our complete list of entries
        int vmLibIndex = prependVMLibraries ? 0 : userLibs.length;
        int userLibIndex = prependVMLibraries ? userLibs.length : 0;

        ILibrary[] allLibraries = new ILibrary[userLibs.length
                + libraryLocations.length];
        System.arraycopy(userLibs, 0, allLibraries, userLibIndex,
                userLibs.length);

        LibraryImporter importer = new LibraryImporter();
        for (int i = 0; i < libraryLocations.length; i++) {
            allLibraries[vmLibIndex++] = importer
                    .createLibraryFor(libraryLocations[i]
                            .getSystemLibraryPath().toFile());
        }

        return allLibraries;
    }
}
