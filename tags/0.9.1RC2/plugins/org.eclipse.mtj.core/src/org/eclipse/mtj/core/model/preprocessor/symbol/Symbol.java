/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/

package org.eclipse.mtj.core.model.preprocessor.symbol;

/**
 * The model of preprocess symbol. The instance of this class contains the name
 * and value of a preprocess symbol.
 * 
 * @author wangf
 */
public class Symbol {
    public static final int TYPE_ABILITY = 0;
    public static final int TYPE_CONFIG = 1;

    private String name;
    private String value;
    private int type;

    /**
     * The constructor.
     * 
     * @param name - The name of the symbol, should be unique in a SymbolSet.
     * @param value - The value of the symbol.
     */
    public Symbol(String name, String value) {
        this.name = name;
        this.value = value;
        // this.type = type;
    }

    /**
     * If name equals, then symbol equals.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Symbol other = (Symbol) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    public String getName() {
        return name;
    }

    /**
     * Get value in safe format for preprocessor.
     * 
     * @return
     */
    public String getSafeValue() {
        return SymbolUtils.getSafeSymbolValue(value);
    }

    public int getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    public void setName(String identifier) {
        this.name = identifier;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + name + "=" + value;
    }

}
