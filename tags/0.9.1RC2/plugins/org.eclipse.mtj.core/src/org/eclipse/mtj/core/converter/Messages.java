/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.converter;

import org.eclipse.osgi.util.NLS;

/**
 *  Convenience methods for manipulating messages. 
 * 
 * @author David Marques
 */
public class Messages extends NLS {
    private static final String BUNDLE_NAME = "org.eclipse.mtj.core.converter.messages"; //$NON-NLS-1$
    public static String MTJProjectConverter_convert_taskname;
    public static String MTJProjectConverter_convertProject_convertion_error;
    public static String MTJProjectConverter_convertProject_device_unavailable;
    public static String MTJProjectConverter_modifyMetadata_exception;
    public static String MTJProjectConverter_modifyMetadata_taskname;
    public static String MTJProjectConverter_removeBuilderAndNature_taskname;
    public static String MTJProjectConverter_renameEclipseMETmpFolder_taskname;
    public static String MTJProjectConverter_renamePreferenceStoreFile_taskname;
    static {
        // initialize resource bundle
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
    }
}
