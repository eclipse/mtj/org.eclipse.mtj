/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.internal.preverifier;

import org.eclipse.mtj.preverifier.results.IClassErrorInformation;
import org.eclipse.mtj.preverifier.results.IFieldErrorInformation;
import org.eclipse.mtj.preverifier.results.IMethodErrorInformation;
import org.eclipse.mtj.preverifier.results.PreverificationError;
import org.eclipse.mtj.preverifier.results.PreverificationErrorLocationType;
import org.eclipse.mtj.preverifier.results.PreverificationErrorType;

/**
 * Some preverification utility functions until a better place comes along to
 * place them.
 * 
 * @author Craig Setera
 */
public class PreverificationUtils {
    /**
     * Return the error text for the specified preverification error.
     * 
     * @param error
     * @return
     */
    public static String getErrorText(PreverificationError error) {
        StringBuffer errorText = new StringBuffer();

        // If details were passed in, we just use that outright
        String detail = error.getDetail();
        if ((detail != null) && (detail.length() > 0)) {
            errorText.append(detail);
        } else {
            appendErrorInformation(errorText, error);
        }

        return errorText.toString();
    }

    /**
     * Append the error information specific to the preverification error.
     * 
     * @param errorText
     * @param error
     */
    private static void appendErrorInformation(StringBuffer errorText,
            PreverificationError error) {
        appendErrorTypeInformation(errorText, error);

        if (error.getType() != PreverificationErrorType.FINALIZERS) {
            appendErrorLocationInformation(errorText, error);
        }
    }

    /**
     * Append the error type information to the text.
     * 
     * @param errorText
     * @param error
     */
    private static void appendErrorLocationInformation(StringBuffer errorText,
            PreverificationError error) {
        IClassErrorInformation classInfo = error.getLocation()
                .getClassInformation();
        IMethodErrorInformation methodInfo = error.getLocation()
                .getMethodInformation();
        IFieldErrorInformation fieldInfo = error.getLocation()
                .getFieldInformation();

        switch (error.getLocation().getLocationType().getTypeCode()) {
        case PreverificationErrorLocationType.CLASS_DEFINITION_CODE:
            errorText.append(" in class ").append(classInfo.getName()).append(
                    " or superclass");
            break;

        case PreverificationErrorLocationType.CLASS_FIELD_CODE:
            errorText.append(" for class field ").append(fieldInfo.getName());
            break;

        case PreverificationErrorLocationType.METHOD_SIGNATURE_CODE:
            errorText.append(" in method declaration for ").append(
                    methodInfo.getName());
            break;

        case PreverificationErrorLocationType.METHOD_FIELD_CODE:
            errorText.append(" in method ").append(methodInfo.getName())
                    .append(" of class ").append(classInfo.getName());
            break;

        case PreverificationErrorLocationType.METHOD_INSTRUCTION_CODE:
            errorText.append(" in method definition for ").append(
                    methodInfo.getName());
            break;
        }
    }

    /**
     * Append the error type information to the text.
     * 
     * @param errorText
     * @param error
     */
    private static void appendErrorTypeInformation(StringBuffer errorText,
            PreverificationError error) {
        // No detailed message was provided, so we will build one
        // up
        switch (error.getType().getErrorCode()) {
        case PreverificationErrorType.FINALIZERS_CODE:
            errorText.append("Finalizers not allowed");
            break;

        case PreverificationErrorType.FLOATING_POINT_CODE:
            errorText.append("Floating point not allowed");
            break;

        case PreverificationErrorType.MISSING_TYPE_CODE:
            errorText.append("Missing type");
            break;

        case PreverificationErrorType.NATIVE_CODE:
            errorText.append("Native method implementations not allowed");
            break;

        case PreverificationErrorType.UNKNOWN_ERROR_CODE:
            errorText.append("Unknown error during preverification");
            break;
        }
    }

    private PreverificationUtils() {
        super();
    }
}
