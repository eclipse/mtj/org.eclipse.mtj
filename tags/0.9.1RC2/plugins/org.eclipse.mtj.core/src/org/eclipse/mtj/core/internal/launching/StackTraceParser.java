/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 *     David Marques (Motorola) - Adding resolved callers 
 *     with unresolved calles
 */
package org.eclipse.mtj.core.internal.launching;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Stack;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.LineNumberNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

/**
 * StackTraceParser class parses stack traces for an IJavaProject. Parsing a
 * stack trace means resolving line numbers and source files for the stack trace
 * lines.
 * 
 * @author David Marques
 */
public class StackTraceParser {

    private IJavaProject project;

    /**
     * Creates a StackTraceParser for the specified file.
     * 
     * @param _project project instance.
     */
    public StackTraceParser(IJavaProject _project) {
        this.project = _project;
    }

    /**
     * Parses the specified stack into a new stack with lines entries. </p> The
     * process for parsing the stack trace starts by trying to resolve a method
     * from the stack base up. The base method must not be overloaded since the
     * stack trace does not inform the method descriptor only name. <br>
     * After having resolved the base method the algorithm follows the stack
     * calls until it reaches the top of the stack. In case in the middle of the
     * stack of not being able to resolve a stack entry, it tries to get a new
     * stack base and follows all way up again.
     * 
     * @param _stack stack to parse.
     * @return a new stack parsed.
     * @throws StackTraceParserException if any error occurs.
     */
    public Stack<StackTraceEntry> parseStackTrace(Stack<StackTraceEntry> _stack)
            throws StackTraceParserException {
        Stack<StackTraceEntry> linedStack = new Stack<StackTraceEntry>();
        Stack<StackTraceEntry> stack = new Stack<StackTraceEntry>();
        stack.addAll(_stack);

        StackTraceEntry caller = null;
        StackTraceEntry callee = null;
        MethodNode method = null;

        /* Try to find a valid entry from the stack base up. */
        while (!stack.isEmpty() && method == null) {
            caller = stack.pop();
            method = this.getStackBaseMethod(caller);
            /* If the caller can not be a stack base method */
            if (method == null) {
                continue;
            }

            /*
             * Walks through the method calls and resolves line number and
             * source files.
             */
            while (!stack.isEmpty()) {
                try {
                    callee = stack.pop();
                    method = this.getCalleeMethodNode(method, caller, callee);
                    linedStack.push(caller);
                    caller = callee;
                } catch (StackTraceParserException e) {
                    /* Check if the line was still resolved */
                    if (caller.getLine() != Integer.MIN_VALUE) {
                        linedStack.push(caller);
                    }
                    stack.push(callee);
                    method = null;
                    break;
                }
            }
        }

        /* Resolves the line and source of the method on top of stack */
        if (caller != null && method != null) {
            int line = getStackTopCalleeLine(method, caller);

            caller.setSourcePath(getSourceFile(caller.getClassName()));
            caller.setLine(line);
            linedStack.push(caller);
        }

        return linedStack;
    }

    /**
     * Finds the source file inside a project.
     * 
     * @param className name of class to search.
     * @return the source file or null of not found.
     * @throws StackTraceParserException if any error occurs.
     */
    private File getSourceFile(String className)
            throws StackTraceParserException {
        File classFile = null;
        try {
            String[] paths = Utils
                    .getJavaProjectSourceDirectories(this.project);
            for (int i = 0; i < paths.length; i++) {
                File srcFolder = project.getProject().getLocation().append(
                        paths[i]).toFile();
                classFile = findClassFile(srcFolder, className);
                if (classFile != null) {
                    break;
                }
            }
        } catch (Exception e) {
            MTJCorePlugin.log(IStatus.ERROR, e.getMessage(), e);
        }
        return classFile;
    }

    /**
     * Searched for a class inside a source folder.
     * 
     * @param srcFolder folder name.
     * @param className class name.
     * @return the class file or null if not found.
     */
    private File findClassFile(File srcFolder, String className) {
        File tempFile = null;
        /* Remove $ from inner classes */
        int index = className.indexOf("$"); //$NON-NLS-1$
        if (index >= 0x00) {
            className = className.substring(0x00, index);
        }

        index = className.lastIndexOf("."); //$NON-NLS-1$
        if (index >= 0x00) {
            String packageName = className.substring(0x00, index);
            String packages[] = packageName.split("."); //$NON-NLS-1$
            for (int i = 0; i < packages.length; i++) {
                tempFile = new File(srcFolder, packages[i]);
                if (tempFile.exists()) {
                    srcFolder = tempFile;
                }
            }
        } else {
            tempFile = new File(srcFolder, className + ".java"); //$NON-NLS-1$
        }

        if (tempFile != null && !tempFile.exists()) {
            tempFile = null;
        }

        return tempFile;
    }

    /**
     * Finds the line of the method on top of the stack by navigating through
     * the line nodes in the method node.
     * 
     * @param callerMethod stack top method node.
     * @param caller stack trace entry.
     * @return the line number.
     */
    private int getStackTopCalleeLine(MethodNode callerMethod,
            StackTraceEntry caller) {
        int line = Integer.MIN_VALUE;

        AbstractInsnNode[] instructions = callerMethod.instructions.toArray();
        for (int i = 0; i < instructions.length; i++) {
            if (instructions[i] instanceof LineNumberNode) {
                LineNumberNode lineNumberNode = (LineNumberNode) instructions[i];
                int offset = lineNumberNode.start.getLabel().getOffset();
                if (offset >= caller.getOffset()) {
                    break;
                } else {
                    line = lineNumberNode.line;
                }
            }
        }
        return line;
    }

    /**
     * Finds a method call inside a caller method referencing a callee entry.
     * After resolving the call updates the line number and source file.
     * 
     * @param callerMethod Method calling calle entry.
     * @param caller Caller entry.
     * @param callee Calle entry.
     * @return the callee method node.
     * @throws StackTraceParserException if any error occurs.
     */
    private MethodNode getCalleeMethodNode(MethodNode callerMethod,
            StackTraceEntry caller, StackTraceEntry callee)
            throws StackTraceParserException {
        MethodInsnNode targetMethod = null;
        int line = Integer.MIN_VALUE;

        AbstractInsnNode[] instructions = callerMethod.instructions.toArray();
        for (int i = 0; i < instructions.length; i++) {
            if (instructions[i] instanceof LineNumberNode) {
                LineNumberNode lineNumberNode = (LineNumberNode) instructions[i];
                int offset = lineNumberNode.start.getLabel().getOffset();
                if (offset >= caller.getOffset()) {
                    break;
                } else {
                    line = lineNumberNode.line;
                }
            } else if (instructions[i] instanceof MethodInsnNode) {
                MethodInsnNode methodInsnNode = (MethodInsnNode) instructions[i];
                if (methodInsnNode.owner.equals(callee.getClassName())
                        && methodInsnNode.name.equals(callee.getMethodName())) {
                    targetMethod = methodInsnNode;
                }
            }
        }
        caller.setSourcePath(getSourceFile(caller.getClassName()));
        caller.setLine(line);

        if (targetMethod == null) {
            throw new StackTraceParserException(
                    "Unable to realize target method.");
        }

        MethodNode result = getMethodNode(targetMethod);
        if (result == null) {
            throw new StackTraceParserException(
                    "Unable to realize method node.");
        }
        return result;
    }

    /**
     * Gets the method node of the target method in a method instruction.
     * 
     * @param methodInsnNode method call instruction.
     * @return the target method node.
     * @throws StackTraceParserException if any error occurs.
     */
    private MethodNode getMethodNode(MethodInsnNode methodInsnNode)
            throws StackTraceParserException {
        MethodNode methodNode = null;

        ClassNode classNode = getClassNode(methodInsnNode.owner);
        List<MethodNode> methods = classNode.methods;
        for (MethodNode method : methods) {
            if (method.name.equals(methodInsnNode.name)
                    && method.desc.equals(methodInsnNode.desc)) {
                methodNode = method;
                break;
            }
        }

        return methodNode;
    }

    /**
     * Gets the first method node matching the stack entry method.
     * 
     * @param entry stack entry.
     * @return the method node or null if either the class can not be resolved
     *         or if the method is overloaded.
     * @throws StackTraceParserException If the entry is an overloaded method.
     *             It can not be overloaded because it is the entry in a method
     *             trace search.
     */
    private MethodNode getStackBaseMethod(StackTraceEntry entry)
            throws StackTraceParserException {
        MethodNode result = null;
        ClassNode clazz = null;

        try {
            clazz = getClassNode(entry.getClassName());
        } catch (StackTraceParserException e) {
            return null;
        }

        List<MethodNode> methods = clazz.methods;
        for (MethodNode methodNode : methods) {
            if (methodNode.name.equals(entry.getMethodName())) {
                if (result == null) {
                    result = methodNode;
                } else {
                    return null; /* The method is overloaded try next. */
                }
            }
        }

        if (result == null) {
            throw new StackTraceParserException(
                    Messages
                            .bind(
                                    Messages.StackTraceParser_getStackBaseMethod_method_not_found,
                                    new String[] { entry.getClassName(),
                                            entry.getMethodName() }));
        }

        return result;
    }

    /**
     * Gets a class node for the specified class by searching on the projects
     * binary folder.
     * 
     * @param className name of class.
     * @return a class node.
     * @throws StackTraceParserException if any error occurs.
     */
    private ClassNode getClassNode(String className)
            throws StackTraceParserException {
        ClassNode classNode = new ClassNode();
        try {
            IPath binPath = project.getProject().getLocation().append(
                    project.getOutputLocation().removeFirstSegments(0x01));

            FileInputStream in = new FileInputStream(new File(binPath.toFile()
                    .getAbsolutePath()
                    + File.separator + className.replace(".", "/") + ".class")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$

            ClassReader classReader = new ClassReader(in);
            classReader.accept(classNode, 0x00);

            ClassWriter classWriter = new ClassWriter(0x00);
            classNode.accept(classWriter);
        } catch (IOException e) {
            throw new StackTraceParserException(e);
        } catch (JavaModelException e) {
            throw new StackTraceParserException(e);
        }
        return classNode;
    }

}
