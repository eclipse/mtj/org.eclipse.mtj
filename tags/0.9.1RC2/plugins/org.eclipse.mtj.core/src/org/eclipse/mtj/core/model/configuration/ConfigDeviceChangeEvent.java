/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - Initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.model.configuration;

import java.util.EventObject;

import org.eclipse.mtj.core.model.device.IDevice;

/**
 * ConfigDeviceChangeEvent is used to notify that the device of the
 * configuration changed.
 * 
 * @author wangf
 */
public class ConfigDeviceChangeEvent extends EventObject {

    private static final long serialVersionUID = 1L;
    private IDevice oldDevice;
    private IDevice newDevice;

    /**
     * Constructor.
     * 
     * @param source - the configuration has been changed.
     * @param oldDevice - the device before change.
     * @param newDevice - the device after change.
     */
    public ConfigDeviceChangeEvent(Configuration source, IDevice oldDevice,
            IDevice newDevice) {
        super(source);
        this.oldDevice = oldDevice;
        this.newDevice = newDevice;
    }

    public IDevice getNewDevice() {
        return newDevice;
    }

    public IDevice getOldDevice() {
        return oldDevice;
    }

    public void setNewDevice(IDevice newDevice) {
        this.newDevice = newDevice;
    }

    public void setOldDevice(IDevice oldDevice) {
        this.oldDevice = oldDevice;
    }

}
