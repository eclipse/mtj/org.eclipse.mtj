/**
 * Copyright (c) 2004,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID.
 */
package org.eclipse.mtj.core.internal.utils;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;

/**
 * JarOutputStream subclass capable of tracking and checking existence of
 * entries that have been made.
 * 
 * @author Craig Setera
 */

public class EntryTrackingJarOutputStream extends JarOutputStream {

    /**
     * 
     * @uml.property name="addedEntryNames"
     * @uml.associationEnd
     * @uml.property name="addedEntryNames" multiplicity="(0 -1)"
     *               elementType="java.lang.String"
     */
    private Set<String> addedEntryNames = new HashSet<String>();

    /**
     * Create a new instance.
     * 
     * @param out
     * @throws java.io.IOException
     */
    public EntryTrackingJarOutputStream(OutputStream out) throws IOException {
        super(out);
    }

    /**
     * Create a new instance.
     * 
     * @param out
     * @param man
     * @throws java.io.IOException
     */
    public EntryTrackingJarOutputStream(OutputStream out, Manifest man)
            throws IOException {
        super(out);
        addManifest(man);
    }

    /**
     * Return a boolean indicating whether the specified entry has already been
     * added to the output stream.
     * 
     * @param ze
     * @return
     */
    public boolean alreadyAdded(ZipEntry ze) {
        return addedEntryNames.contains(ze.getName());
    }

    /**
     * @see java.util.zip.ZipOutputStream#putNextEntry(java.util.zip.ZipEntry)
     */
    public void putNextEntry(ZipEntry ze) throws IOException {
        super.putNextEntry(ze);
        addedEntryNames.add(ze.getName());
    }

    /**
     * Add the manifest to the output stream.
     * 
     * @param man
     * @throws IOException
     */
    private void addManifest(Manifest man) throws IOException {
        if (man == null) {
            throw new NullPointerException("man");
        }

        ZipEntry e = new ZipEntry(JarFile.MANIFEST_NAME);
        putNextEntry(e);
        man.write(new BufferedOutputStream(this));
        closeEntry();
    }
}
