/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 */
package org.eclipse.mtj.core.console;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.eclipse.debug.core.IStreamListener;
import org.eclipse.debug.core.model.IStreamMonitor;

/**
 * The proxied implementation of {@link IBuildConsoleProxy}.
 * 
 * @author Craig Setera
 */
public class BuildConsoleProxy implements IBuildConsoleProxy {
    public static final BuildConsoleProxy instance = new BuildConsoleProxy();

    private static NullOutputStream nullStream = new NullOutputStream();

    private static class NullOutputStream extends OutputStream {
        public void write(int arg0) throws IOException {
        }
    }

    private IBuildConsoleProxy proxy;
    private PrintWriter traceWriter;

    private BuildConsoleProxy() {
    }

    /**
     * Add a new listener to the specified console stream.
     * 
     * @param id
     * @param monitor
     */
    public void addConsoleStreamListener(String id, IStreamMonitor monitor) {
        final PrintWriter writer = getConsoleWriter(id);
        monitor.addListener(new IStreamListener() {
            public void streamAppended(String text, IStreamMonitor monitor) {
                writer.print(text);
                writer.flush();
            }
        });

        writer.print(monitor.getContents());
        writer.flush();
    }

    /**
     * @see org.eclipse.mtj.core.console.IBuildConsoleProxy#getConsoleWriter(java.lang.String)
     */
    public PrintWriter getConsoleWriter(String id) {
        return (proxy == null) ? new PrintWriter(nullStream) : proxy
                .getConsoleWriter(id);
    }

    /**
     * Print the specified information to the log output stream.
     * 
     * @param text
     */
    public void trace(String text) {
        if (traceWriter == null) {
            traceWriter = getConsoleWriter(ID_TRACE_STREAM);
        }

        traceWriter.print(text);
        traceWriter.flush();
    }

    /**
     * Print the specified information to the log output stream.
     * 
     * @param text
     */
    public void traceln(String text) {
        trace(text + "\n");
    }

    /**
     * Set the proxy being wrapped by this proxy.
     * 
     * @param proxy
     */
    public void setProxy(IBuildConsoleProxy proxy) {
        this.proxy = proxy;
        traceWriter = null;
    }
}
