/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID.
 */
package org.eclipse.mtj.core.internal.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.runtime.Platform;

/**
 * Wrapper around the environment variables queried from the underlying
 * operating system.
 * 
 * @author Craig Setera
 */

public class EnvironmentVariables {
    // The properties instance used to track the values
    private Properties variables;

    // The map of upper-cased property name to property name as found
    private Map<String, String> nameMap;

    /**
     * Construct a new instance.
     * 
     * @throws IOException
     */
    public EnvironmentVariables() throws IOException {
        super();
        variables = new Properties();
        nameMap = new HashMap<String, String>();
        retrieveEnvironmentVariables();
    }

    /**
     * Return a boolean indicating whether or not the environment variables
     * contains a variable of the specified name.
     * 
     * @param name
     * @return
     */
    public boolean containsVariable(String name) {
        return getVariable(name) != null;
    }

    /**
     * Convert the environment variables into an array of Strings.
     * 
     * @return
     */
    public String[] convertToStrings() {
        String[] strings = new String[variables.size()];

        int index = 0;
        Iterator<Map.Entry<Object, Object>> entries = variables.entrySet()
                .iterator();
        while (entries.hasNext()) {
            Map.Entry<Object, Object> entry = (Map.Entry<Object, Object>) entries
                    .next();
            strings[index++] = entry.getKey() + "=" + entry.getValue();
        }

        return strings;
    }

    /**
     * Return the variable value or <code>null</code> searching in a
     * case-insensitive way.
     * 
     * @param name
     * @return
     */
    public String getVariable(String name) {
        // Handle things in a case insensitive manner...
        String caseSpecificName = (String) nameMap.get(name.toUpperCase());
        return (caseSpecificName == null) ? null : (String) variables
                .get(caseSpecificName);
    }

    /**
     * Set the specified environment variable to the specified value. This does
     * not set the variable into the operating system, only into this holder
     * class.
     * 
     * @param name
     * @param value
     */
    public void setVariable(String name, String value) {
        // Handle things in a case insensitive manner...
        String caseSpecificName = (String) nameMap.get(name.toUpperCase());
        if (caseSpecificName == null) {
            caseSpecificName = name;
            nameMap.put(caseSpecificName, caseSpecificName);
        }

        variables.setProperty(caseSpecificName, value);
    }

    /**
     * Retrieve the environment variables.
     * 
     * @return
     * @throws IOException
     */
    private void retrieveEnvironmentVariables() throws IOException {
        // Launch a command to gather the environment variables
        String envVarsCommand = getEnvironmentVariablesCommand();
        Process process = Runtime.getRuntime().exec(envVarsCommand);
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(process
                    .getInputStream()));
            // Parse the resulting properties
            String line = null;
            while ((line = br.readLine()) != null) {
                int equalsIndex = line.indexOf('=');
                if (equalsIndex != -1) {
                    String key = line.substring(0, equalsIndex);
                    String value = line.substring(equalsIndex + 1);
                    variables.setProperty(key, value);
                    nameMap.put(key.toUpperCase(), key);
                }
            }
        } finally {
            br.close();
        }
    }

    /**
     * Get the command to be executed to retrieve the enviroment variables.
     * 
     * @return
     */
    private String getEnvironmentVariablesCommand() {
        String command = null;

        // NOTE: I'm assuming that we don't support Win95 or Win98 anymore...
        // if (os.indexOf("windows 9") > -1) {
        // command = "command.com /c set";

        if (Platform.getOS().equals(Platform.OS_WIN32)) {
            command = "cmd.exe /c set";
        } else {
            command = "env";
        }

        return command;
    }
}
