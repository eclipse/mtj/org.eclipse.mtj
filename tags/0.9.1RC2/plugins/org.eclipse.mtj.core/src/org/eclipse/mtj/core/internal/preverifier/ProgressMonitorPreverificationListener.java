/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.internal.preverifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.mtj.preverifier.IArchivePreverificationListener;
import org.eclipse.mtj.preverifier.results.PreverificationError;
import org.eclipse.mtj.preverifier.results.PreverificationResults;


/**
 * Listener for archive preverification events that wraps a progress monitor for
 * feedback.
 * 
 * @author Craig Setera
 */
public class ProgressMonitorPreverificationListener implements
        IArchivePreverificationListener {
    private IProgressMonitor subMonitor;
    private ArrayList<PreverificationError> errorList;

    /**
     * Construct a new listener wrapping the specified progress monitor.
     * 
     * @param monitor
     */
    public ProgressMonitorPreverificationListener(IProgressMonitor monitor) {
        super();
        errorList = new ArrayList<PreverificationError>();
        subMonitor = new SubProgressMonitor(monitor, IProgressMonitor.UNKNOWN);
    }

    /**
     * @see org.eclipse.mtj.preverifier.IArchivePreverificationListener#fileBegin(java.util.zip.ZipFile)
     */
    public void fileBegin(ZipFile archive) {
        subMonitor.beginTask("Beginning preverification of "
                + archive.getName(), IProgressMonitor.UNKNOWN);
    }

    /**
     * @see org.eclipse.mtj.preverifier.IArchivePreverificationListener#classBegin(java.util.zip.ZipFile,
     *      java.util.zip.ZipEntry)
     */
    public boolean classBegin(ZipFile archive, ZipEntry classEntry) {
        String name = classEntry.getName().replace('/', '.');
        subMonitor.subTask("Preverifying " + name + "...");

        return !subMonitor.isCanceled();
    }

    /**
     * @see org.eclipse.mtj.preverifier.IArchivePreverificationListener#classEnd(java.util.zip.ZipFile,
     *      java.util.zip.ZipEntry,
     *      org.eclipse.mtj.preverifier.results.PreverificationResults)
     */
    public boolean classEnd(ZipFile archive, ZipEntry classEntry,
            PreverificationResults results) {
        subMonitor.worked(1);
        errorList.addAll(Arrays.asList(results.getErrors()));
        return !subMonitor.isCanceled();
    }

    /**
     * @see org.eclipse.mtj.preverifier.IArchivePreverificationListener#fileEnd(java.util.zip.ZipFile)
     */
    public void fileEnd(ZipFile archive) {
        subMonitor.done();
    }

    /**
     * Return the errors collected during the preverify processing.
     * 
     * @return
     */
    public List<PreverificationError> getErrorList() {
        return errorList;
    }
}
