/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Diego Sandin (Motorola)  - Adapting code to use Jetty version 5.1.11                                
 */
package org.eclipse.mtj.core.internal.overtheair;

import java.io.File;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Preferences;
import org.eclipse.mtj.core.IMTJCoreConstants;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.mortbay.http.HttpContext;
import org.mortbay.http.HttpListener;
import org.mortbay.http.HttpServer;
import org.mortbay.http.SocketListener;
import org.mortbay.http.handler.NotFoundHandler;
import org.mortbay.log.LogImpl;
import org.mortbay.log.OutputStreamLogSink;

/**
 * Singleton management of the Over the Air HTTP Server.
 * 
 * @author Craig Setera
 */
public class OTAServer {

    /**
     * Singleton instance of the OTAServer
     */
    public static final OTAServer instance = new OTAServer();

    /**
     * Instance of Log
     */
    public static LogImpl logImpl = new LogImpl();

    /**
     * Return the port to be listened on for OTA connections.
     * 
     * @return
     */
    public static int getPort() {
        HttpListener listener = instance.getHttpServer().getListeners()[0];
        return listener.getPort();
    }

    /**
     * @uml.property name="httpServer"
     * @uml.associationEnd
     * @uml.property name="httpServer" multiplicity="(1 1)"
     */
    // The HttpServer instance in use for serving the content
    private HttpServer httpServer;

    /**
     * Private constructor
     */
    private OTAServer() {
        super();
        //logImpl = new LogImpl();

    }

    /**
     * Start the HttpServer if it has not already been started.
     * 
     * @throws Exception
     */
    HttpServer server = getHttpServer();

    public void start() throws Exception {
        if (!server.isStarted()) {
            server.start();
        }
    }

    /**
     * Stop the HttpServer if it is currently running.
     * 
     * @throws InterruptedException
     */
    public void stop() throws InterruptedException {
        HttpServer server = getHttpServer();
        if (server.isStarted()) {
            server.stop();
        }
    }

    /**
     * Configure the context(s) for the HttpServer.
     * 
     * @param server
     */
    private void configureContext(HttpServer server) {
        // Configure the context
        HttpContext context = new HttpContext();
        context.setContextPath("/ota/*");
        server.addContext(context);

        // Configure the handlers in the context
        context.addHandler(new OTAHandler());
        context.addHandler(new NotFoundHandler());
    }

    /**
     * Configure the HttpServer listener.
     * 
     * @param server
     */
    private void configureListener(HttpServer server) {
        SocketListener listener = new SocketListener();
        listener.setMinThreads(1);
        listener.setMaxThreads(2);
        listener.setMaxIdleTimeMs(10000);
        listener.setDaemon(true);

        Preferences prefs = MTJCorePlugin.getDefault().getPluginPreferences();
        if (prefs.getBoolean(IMTJCoreConstants.PREF_OTA_PORT_DEFINED)) {
            int specifiedPortNumber = prefs
                    .getInt(IMTJCoreConstants.PREF_OTA_PORT);
            listener.setPort(specifiedPortNumber);
        }

        server.addListener(listener);
    }

    /**
     * Configure the Jetty logging support so that it writes out to our metadata
     * directory.
     * 
     * @throws Exception
     */
    private void configureLogging() throws Exception {
        // Set some System properties so that they are available
        // when creating the logging
        System.setProperty("LOG_FILE_RETAIN_DAYS", "5");

        // Calculate the name of the file to be used for logging
        IPath stateLocation = MTJCorePlugin.getDefault().getStateLocation();
        IPath logDirectory = stateLocation.append("jetty");

        File logDirectoryFile = logDirectory.toFile();
        if (!logDirectoryFile.exists()) {
            logDirectoryFile.mkdir();
        }

        File logFile = new File(logDirectoryFile, "logfile");
        String filename = logFile + "yyyy_mm_dd.txt";

        // Create the output sink and add it to the logging class
        OutputStreamLogSink sink = new OutputStreamLogSink(filename);

        sink.start();

        logImpl.add(sink);
    }

    /**
     * Create and configure the HTTP server instance.
     * 
     * @return
     */
    private HttpServer createHttpServer() {
        HttpServer server = new HttpServer();
        configureListener(server);
        configureContext(server);
        return server;
    }

    /**
     * Get the HttpServer instance.
     * 
     * @return
     */
    private HttpServer getHttpServer() {
        if (httpServer == null) {
            httpServer = createHttpServer();
            try {
                configureLogging();
            } catch (Exception e) {
                MTJCorePlugin.log(IStatus.WARNING, "configureLogging", e);
            }
        }

        return httpServer;
    }
}
