/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Added support to MPowerPlayer embedded 
 *                                preverifier on Mac OS X
 *     Diego Sandin (Motorola)  - Hard coded the CLDC and MIDP libraries 
 *     Diego Sandin (Motorola)  - Use LaunchTemplateProperties enum instead of 
 *                                hard-coded property strings  
 */
package org.eclipse.mtj.toolkit.mpowerplayer;

import java.io.File;
import java.io.FileFilter;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.importer.LibraryImporter;
import org.eclipse.mtj.core.importer.impl.JavaEmulatorDeviceImporter;
import org.eclipse.mtj.core.importer.properties.JavaEmulatorDeviceProperties;
import org.eclipse.mtj.core.internal.MTJCorePlugin;
import org.eclipse.mtj.core.internal.utils.Utils;
import org.eclipse.mtj.core.model.Classpath;
import org.eclipse.mtj.core.model.ReplaceableParametersProcessor;
import org.eclipse.mtj.core.model.Version;
import org.eclipse.mtj.core.model.device.IDevice;
import org.eclipse.mtj.core.model.library.ILibrary;
import org.eclipse.mtj.core.model.library.api.API;
import org.eclipse.mtj.core.model.library.api.APIType;
import org.eclipse.mtj.core.model.preverifier.IPreverifier;
import org.eclipse.mtj.core.model.preverifier.StandardPreverifierFactory;

/**
 * Implementor of the IDeviceImporter interface that imports a device reference
 * to the <a href="http://www.mpowerplayer.com/">MPowerPlayer</a> device.
 * 
 * @author Craig Setera
 */
public class MpowerplayerDeviceImporter extends JavaEmulatorDeviceImporter {

    /* OS related static information */
    private static final String MACOSX_OS_NAME = "Mac OS X"; //$NON-NLS-1$
    private static String osName = System.getProperty("os.name"); //$NON-NLS-1$

    // Various pieces of static information
    private static final String PLAYER_JAR_NAME = "player.jar"; //$NON-NLS-1$
    private static final String PLAYER_MACOSX_PREVERIFIER = "/osx/preverify"; //$NON-NLS-1$

    /**
     * MPowerPlayer main class
     */
    private static final String PLAYER_MAIN_CLASS = "com.mpp.player.PowerPlayerApp"; //$NON-NLS-1$

    /**
     * Properties file holding emulator/device information
     */
    private static final String PROPS_FILE = "mpowerplayer.properties"; //$NON-NLS-1$

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.importer.IDeviceImporter#getMatchingDevices(java.io.File, org.eclipse.core.runtime.IProgressMonitor)
     */
    public IDevice[] getMatchingDevices(File directory, IProgressMonitor monitor) {
        IDevice[] devices = null;

        try {
            File jarFile = new File(directory, PLAYER_JAR_NAME);
            if (jarFile.exists()
                    && hasMainClassAttribute(jarFile, PLAYER_MAIN_CLASS)) {
                IDevice device = createDevice(jarFile);
                devices = new IDevice[] { device };
            }
        } catch (Exception e) {
            MTJCorePlugin.log(IStatus.WARNING,
                    Messages.MpowerplayerDeviceImporter_import_error, e);
        }

        return devices;
    }

    /**
     * Add the MPowerPlayer device libraries.
     * 
     * @param jarFile
     * @param classpath
     * @param importer
     */
    private void addMplayerDeviceLibraries(File jarFile, Classpath classpath,
            LibraryImporter importer) {

        // Now add the player libraries
        String classpathString = getDeviceProperties().getProperty(
                JavaEmulatorDeviceProperties.CLASSPATH.toString(),
                Utils.EMPTY_STRING);

        Map<String, String> replaceableParameters = new HashMap<String, String>();
        replaceableParameters.put("mpproot", jarFile.getParent()); //$NON-NLS-1$

        classpathString = ReplaceableParametersProcessor
                .processReplaceableValues(classpathString,
                        replaceableParameters);
        String[] entries = classpathString.split(";"); //$NON-NLS-1$

        for (String entrie : entries) {
            ILibrary library = importer.createLibraryFor(new File(entrie));

            // Because of the structure of the MPowerPlayer libraries,
            // we need to hard code the CLDC and MIDP libraries
            API api = library.getAPI(APIType.UNKNOWN);
            if (api != null) {
                if (api.getIdentifier().equalsIgnoreCase("cldc-1.1.jar")) { //$NON-NLS-1$
                    api.setIdentifier("CLDC"); //$NON-NLS-1$
                    api.setType(APIType.CONFIGURATION);
                    api.setName("Connected Limited Device Configuration"); //$NON-NLS-1$
                    api.setVersion(new Version("1.1")); //$NON-NLS-1$
                } else if (api.getIdentifier().equalsIgnoreCase("midp-2.0.jar")) { //$NON-NLS-1$
                    api.setIdentifier("MIDP"); //$NON-NLS-1$
                    api.setType(APIType.PROFILE);
                    api.setName("Mobile Information Device Profile"); //$NON-NLS-1$
                    api.setVersion(new Version("2.0")); //$NON-NLS-1$

                }
            }
            classpath.addEntry(library);
        }
    }

    /**
     * Create a device instance based on the specified player jar file.
     * 
     * @param jarFile
     * @return
     */
    private IDevice createDevice(File jarFile) {
        MpowerplayerDevice device = new MpowerplayerDevice();

        device.setBundle(MpowerplayerPlugin.getDefault().getBundle()
                .getSymbolicName());
        device.setClasspath(getDeviceClasspath(jarFile));
        device.setDebugServer(isDebugServer());
        device.setDescription("Mpowerplayer Device"); //$NON-NLS-1$
        device.setDeviceProperties(new Properties());
        device.setGroupName("Mpowerplayer"); //$NON-NLS-1$
        device.setName("Mpowerplayer"); //$NON-NLS-1$
        device.setPreverifier(getPreverifier(jarFile));
        device.setProtectionDomains(new String[0]);
        device.setLaunchCommandTemplate(getLaunchCommand());
        device.setMppRoot(jarFile.getParentFile());

        return device;
    }

    /**
     * Find a preverify executable in the specified directory or subdirectories.
     * 
     * @param directory
     * @return
     */
    private File findEmbeddedPreverifyExtecutable(File directory) {
        File[] files = directory.listFiles(new FileFilter() {

            /*
             * (non-Javadoc)
             * 
             * @see java.io.FileFilter#accept(java.io.File)
             */
            public boolean accept(File pathname) {
                String name = pathname.getName();
                return pathname.isDirectory() || (name.equals("preverify")); //$NON-NLS-1$
            }
        });

        File executable = null;
        for (File file : files) {
            if (file.isDirectory()) {
                executable = findEmbeddedPreverifyExtecutable(file);
            } else {
                executable = file;
                break;
            }
        }

        return executable;
    }

    /**
     * Get the device classpath based on the specified player.jar file.
     * 
     * @param jarFile
     * @return
     */
    private Classpath getDeviceClasspath(File jarFile) {
        Classpath classpath = new Classpath();
        LibraryImporter importer = new LibraryImporter();

        addMplayerDeviceLibraries(jarFile, classpath, importer);

        return classpath;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.importer.impl.JavaEmulatorDeviceImporter#getDevicePropertiesURL()
     */
    @Override
    protected URL getDevicePropertiesURL() {
        return MpowerplayerPlugin.getDefault().getBundle().getEntry(PROPS_FILE);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.importer.impl.JavaEmulatorDeviceImporter#getPreverifier(java.io.File)
     */
    @Override
    protected IPreverifier getPreverifier(File jarFile) {

        IPreverifier preverifier = super.getPreverifier(jarFile);

        if (osName.equals(MACOSX_OS_NAME)) {

            File macOsPreverifierPath = new File(jarFile.getParentFile()
                    .getPath()
                    + PLAYER_MACOSX_PREVERIFIER);
            File preverifyExecutable = findEmbeddedPreverifyExtecutable(macOsPreverifierPath);

            if (preverifyExecutable != null) {
                try {
                    preverifier = StandardPreverifierFactory
                            .createPreverifier(preverifyExecutable);
                } catch (CoreException e) {
                    MTJCorePlugin
                            .log(
                                    IStatus.WARNING,
                                    Messages.MpowerplayerDeviceImporter_preverifier_import_error,
                                    e);
                }
            }
        }
        return preverifier;
    }
}
