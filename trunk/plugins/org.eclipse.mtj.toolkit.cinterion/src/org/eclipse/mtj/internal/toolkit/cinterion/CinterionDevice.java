/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Gustavo de Paula (Individual) - adapt to use with cinterion 
 */
package org.eclipse.mtj.internal.toolkit.cinterion;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.project.ProjectType;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.ILibrary;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary;
import org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.toolkit.uei.EmulatorInfoArgs;
import org.eclipse.mtj.internal.toolkit.uei.UEIDeviceInternal;
import org.eclipse.mtj.internal.toolkit.uei.properties.DeviceSpecificProperties;
import org.eclipse.mtj.internal.toolkit.uei.properties.UEIDeviceDefinition;
import org.osgi.framework.Version;

/**
 * @author Gustavo de Paula
 * @since 1.1
 */
public class CinterionDevice extends UEIDeviceInternal {

    /**
     * Creates a new instance of CinterionDevice.
     */
    public CinterionDevice() {

    }

    /**
     * Create a new instance of CinterionDevice based on the properties for
     * that device found with the
     * <code>emulator {@link EmulatorInfoArgs#XQUERY -Xquery}</code> command.
     * 
     * @param name the device name.
     * @param groupName the device group name.
     * @param description the device description.
     * @param properties the properties for the device.
     * @param definition information on how the UEI emulator will be launched.
     * @param emulatorExecutable the emulator application.
     * @param preverifier the preverifier application.
     * @throws IllegalArgumentException if the properties or definition are
     *             <code>null</code>.
     */
    public CinterionDevice(String name, String groupName,
            String description, Properties properties,
            UEIDeviceDefinition definition, File emulatorExecutable,
            IPreverifier preverifier) throws IllegalArgumentException {
        super(name, groupName, description, properties, definition,
                emulatorExecutable, preverifier);
        setBundle(CinterionCore.getDefault().getBundle().getSymbolicName());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.toolkit.uei.UEIDeviceInternal#getBootClasspath(java.util.Properties)
     */
    @Override
    protected IDeviceClasspath getBootClasspath(Properties deviceProperties) {

        IDeviceClasspath deviceClasspath = MTJCore.createNewDeviceClasspath();
        String classpathString = deviceProperties.getProperty(
                DeviceSpecificProperties.BOOTCLASSPATH.toString(),
                Utils.EMPTY_STRING);

        deviceClasspath.addEntry(createLibraries(classpathString));
        return deviceClasspath;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.sdk.device.AbstractMIDPDevice#getBundle()
     */
    @Override
    public String getBundle() {
        return CinterionCore.getDefault().getBundle().getSymbolicName();
    }

    /**
     * Create the APIs for: <br>
     * Profile: IMP-NG<br>
     * Configuration: CLDC-1.1<br>
     * Optional:
     * <ul>
     * <li>SIEMENS ICM</li>
     * </ul>
     * 
     * @param classpath
     * @return
     */
    private ILibrary createLibraries(String classpath) {

        List<IMIDPAPI> apis = new ArrayList<IMIDPAPI>();

        IMIDPAPI midp = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        midp.setIdentifier("IMP-NG"); //$NON-NLS-1$
        midp.setType(MIDPAPIType.PROFILE);
        midp.setName("Information Module Profile"); //$NON-NLS-1$
        midp.setVersion(new Version("1.0")); //$NON-NLS-1$
        apis.add(midp);

        IMIDPAPI cldc = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        cldc.setIdentifier("CLDC"); //$NON-NLS-1$
        cldc.setType(MIDPAPIType.CONFIGURATION);
        cldc.setName("Connected Limited Device Configuration"); //$NON-NLS-1$
        cldc.setVersion(new Version("1.1")); //$NON-NLS-1$
        apis.add(cldc);

        IMIDPAPI icmAPI = (IMIDPAPI) MTJCore
                .createNewAPI(ProjectType.MIDLET_SUITE);
        icmAPI.setIdentifier("SIEMENS-ICM"); //$NON-NLS-1$
        icmAPI.setType(MIDPAPIType.OPTIONAL);
        icmAPI.setName("Siemens Icm"); //$NON-NLS-1$
        icmAPI.setVersion(new Version("1.0")); //$NON-NLS-1$
        apis.add(icmAPI);

        IMIDPLibrary library = (IMIDPLibrary) MTJCore
                .createNewLibrary(ProjectType.MIDLET_SUITE);
        library.setLibraryFile(new File(classpath));
        library.setApis(apis);

        return library;
    }
}
