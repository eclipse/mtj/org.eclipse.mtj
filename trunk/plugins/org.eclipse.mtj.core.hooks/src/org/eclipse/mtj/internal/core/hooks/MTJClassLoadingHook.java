/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Fix errors after updating ASM library form 
 *                                version 2.2.2 to 3.0.0
 */
package org.eclipse.mtj.internal.core.hooks;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;

import org.eclipse.osgi.internal.framework.EquinoxConfiguration;
import org.eclipse.osgi.internal.hookregistry.ClassLoaderHook;
import org.eclipse.osgi.internal.loader.BundleLoader;
import org.eclipse.osgi.internal.loader.ModuleClassLoader;
import org.eclipse.osgi.internal.loader.classpath.ClasspathEntry;
import org.eclipse.osgi.internal.loader.classpath.ClasspathManager;
import org.eclipse.osgi.storage.BundleInfo.Generation;
import org.eclipse.osgi.storage.bundlefile.BundleEntry;
import org.eclipse.osgi.util.ManifestElement;
import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodAdapter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.util.TraceClassVisitor;
import org.osgi.framework.BundleException;

/**
 * Hooks the classloading functionality for MTJ functionality.
 * 
 * @author Craig Setera
 */
@SuppressWarnings("restriction")
public class MTJClassLoadingHook extends ClassLoaderHook {

    /**
     * MethodVisitor implementation to rewrite the getContents method of the
     * SourceFile class.
     * 
     * @author Craig Setera
     */
    class GetContentsMethodVisitor extends MethodAdapter {
        boolean foundReturn = false;

        public GetContentsMethodVisitor(MethodVisitor mv) {
            super(mv);
        }

        /* (non-Javadoc)
         * @see org.objectweb.asm.MethodAdapter#visitFieldInsn(int, java.lang.String, java.lang.String, java.lang.String)
         */
        @Override
        public void visitFieldInsn(int opcode, String owner, String name,
                String desc) {
            if ((opcode == Opcodes.GETFIELD) && (name.equals("resource"))
                    && !foundReturn) {
                insertMappedResourceCode();
            } else {
                super.visitFieldInsn(opcode, owner, name, desc);
            }
        }

        /* (non-Javadoc)
         * @see org.objectweb.asm.MethodAdapter#visitInsn(int)
         */
        @Override
        public void visitInsn(int opcode) {
            if (!foundReturn && (opcode == Opcodes.ARETURN)) {
                if (Debug.DEBUG_GENERAL) {
                    System.out.println("Found ARETURN in method.");
                }

                foundReturn = true;
            }

            super.visitInsn(opcode);
        }

        /**
         * Write the code that will write a debug message.
         * 
         * @param message
         */
        private void insertDebugMessageCode(String message) {
            if (Debug.DEBUG_GENERAL) {
                super.visitFieldInsn(Opcodes.GETSTATIC, "java/lang/System",
                        "out", "Ljava/io/PrintStream;");
                super.visitLdcInsn(message);
                super.visitMethodInsn(Opcodes.INVOKEVIRTUAL,
                        "java/io/PrintStream", "println",
                        "(Ljava/lang/String;)V");
            }
        }

        /**
         * Insert the code that will attempt to request the mapped resource from
         * the {@link SourceMapperTracker}.
         */
        private void insertMappedResourceCode() {
            if (Debug.DEBUG_GENERAL) {
                System.out.println("Inserting mapped "
                        + "resource lookup code into method.");
            }

            // We want to rewrite the first GETFIELD call for the resource
            // object as a call to the mapping function
            Label endLabel = new Label();

            // Call through the mapper accessor to see if we
            // get a mapped resource
            insertDebugMessageCode("Rewritten SourceFile");
            super.visitInsn(Opcodes.POP);
            super.visitVarInsn(Opcodes.ALOAD, 0); // Load "this"
            super.visitFieldInsn(Opcodes.GETFIELD, SourceFile_Type
                    .getInternalName(), "resource", IFile_Type.getDescriptor());
            super
                    .visitMethodInsn(Opcodes.INVOKESTATIC,
                            SourceMapperAccess_Type.getInternalName(),
                            "getMappedSourceFile", Type.getMethodDescriptor(
                                    IFile_Type, new Type[] { IFile_Type }));

            // If the result was non-null, we are done... otherwise we need to
            // clear the extra copy and load the raw resource
            super.visitInsn(Opcodes.DUP);
            super.visitJumpInsn(Opcodes.IFNONNULL, endLabel);
            insertDebugMessageCode("Mapped resource was null");
            super.visitInsn(Opcodes.POP);

            // Load the local (raw) resource
            insertDebugMessageCode("Using raw resource");
            super.visitVarInsn(Opcodes.ALOAD, 0); // Load "this"
            super.visitFieldInsn(Opcodes.GETFIELD, SourceFile_Type
                    .getInternalName(), "resource", IFile_Type.getDescriptor());

            // All done with the rewrite
            visitLabel(endLabel);
            insertDebugMessageCode("Finished generated code");
        }
    }

    /**
     * MethodVisitor implementation to rewrite the isHookInstalled method of the
     * SourceMapperAccess class.
     * 
     * @author Craig Setera
     */
    class IsHookInstalledMethodVisitor extends MethodAdapter {

        /**
         * Construct a new MethodAdapter
         * 
         * @param mv
         */
        public IsHookInstalledMethodVisitor(MethodVisitor mv) {
            super(mv);
        }

        /* (non-Javadoc)
         * @see org.objectweb.asm.MethodAdapter#visitInsn(int)
         */
        @Override
        public void visitInsn(int opcode) {
            if (opcode == Opcodes.ICONST_0) {
                opcode = Opcodes.ICONST_1;
            }

            super.visitInsn(opcode);
        }
    }

    /**
     * Class adapter for rewriting the get contents method.
     * 
     * @author Craig Setera
     */
    class SourceFileClassAdapter extends ClassAdapter {

        /**
         * Construct a new adapter instance.
         * 
         * @param cv
         */
        public SourceFileClassAdapter(ClassVisitor cv) {
            super(cv);
        }

        /* (non-Javadoc)
         * @see org.objectweb.asm.ClassAdapter#visitMethod(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String[])
         */
        @Override
        public MethodVisitor visitMethod(int access, String name, String desc,
                String signature, String[] exceptions) {
            MethodVisitor methodVisitor = super.visitMethod(access, name, desc,
                    signature, exceptions);

            if (name.equals("getContents")) {
                if (Debug.DEBUG_GENERAL) {
                    System.out.println("SourceFile#"
                            + "getContents spotted.  Rewriting method."); //$NON-NLS-1$
                }

                methodVisitor = new GetContentsMethodVisitor(methodVisitor);
            }

            return methodVisitor;
        }
    }

    /**
     * Class adapter for rewriting the SourceMapperAccess class
     * 
     * @author Craig Setera
     */
    class SourceMapperAccessClassAdapter extends ClassAdapter {

        /**
         * Construct a new adapter instance.
         * 
         * @param cv
         */
        public SourceMapperAccessClassAdapter(ClassVisitor cv) {
            super(cv);
        }

        /* (non-Javadoc)
         * @see org.objectweb.asm.ClassAdapter#visitMethod(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String[])
         */
        @Override
        public MethodVisitor visitMethod(int access, String name, String desc,
                String signature, String[] exceptions) {
            MethodVisitor methodVisitor = super.visitMethod(access, name, desc,
                    signature, exceptions);

            if (name.equals("isHookCodeInstalled")) {
                if (Debug.DEBUG_GENERAL) {
                    System.out
                            .println("SourceMapperAccess#isHookInstalled spotted.  Rewriting method.");
                }

                methodVisitor = new IsHookInstalledMethodVisitor(methodVisitor);
            }

            return methodVisitor;
        }

    }

    /**
     * Type reference to the IFile interface
     */
    private static Type IFile_Type = Type
            .getType("Lorg/eclipse/core/resources/IFile;"); //$NON-NLS-1$

    private static final String SOURCE_FILE_CLASS = "org.eclipse.jdt.internal.core.builder.SourceFile"; //$NON-NLS-1$

    /**
     * Source mapper class
     */
    private static String SOURCE_MAPPER_CLASS = "org.eclipse.mtj.internal.core.hook.sourceMapper.SourceMapperAccess"; //$NON-NLS-1$

    /**
     * Type reference to the JDT SourceFile class
     */
    private static Type SourceFile_Type = Type
            .getType("Lorg/eclipse/jdt/internal/core/builder/SourceFile;"); //$NON-NLS-1$

    /**
     * Type reference to the SourceMapperAccess type
     */
    private static Type SourceMapperAccess_Type = Type
            .getType("Lorg/eclipse/mtj/internal/core/hook/sourceMapper/SourceMapperAccess;"); //$NON-NLS-1$

    private static ManifestElement[] sourceMapperManifestElements;

    /**
     * Retrieve the dynamic manifest elements for use in linking in the source
     * mapper functionality.
     * 
     * @return
     */
    private static ManifestElement[] getSourceMapperManifestElements() {
        if (sourceMapperManifestElements == null) {
            try {
                sourceMapperManifestElements = ManifestElement.parseHeader(
                        "DynamicImport-Package",
                        "org.eclipse.mtj.internal.core.hook.sourceMapper");
            } catch (BundleException e) {
                if (Debug.DEBUG_GENERAL) {
                    e.printStackTrace();
                }
            }
        }

        return sourceMapperManifestElements;
    }
    
    

	@Override
	public ModuleClassLoader createClassLoader(ClassLoader parent,
			EquinoxConfiguration configuration, BundleLoader delegate,
			Generation generation) {
        // Make sure the class is within the JDT core plugin
        if ("org.eclipse.jdt.core".equals(generation.getRevision().getSymbolicName())) {

            // CAS - The BundleLoader class has been refactored in the 3.5
            // (Galileo)
            // release to a new package. To avoid issues with class loading, the
            // following drops into reflection to do the work.
            ManifestElement[] dynamicElements = getSourceMapperManifestElements();
            Class<? extends BundleLoader> clazz = delegate.getClass();
            Method dynamicImportMethod = null;

            try {
                dynamicImportMethod = clazz.getMethod(
                        "addDynamicImportPackage", dynamicElements.getClass());
            } catch (NoSuchMethodException e) {
                // Do nothing...
            }

            if (dynamicImportMethod != null) {
                // We have an appropriate method to invoke... go ahead and
                // invoke the method.
                if (Debug.DEBUG_GENERAL) {
                    System.out
                            .println("Adding dynamic import into JDT Core bundle");
                }

                try {
                    dynamicImportMethod.invoke(delegate,
                            new Object[] { dynamicElements });
                } catch (Exception e) {
                    if (Debug.DEBUG_GENERAL) {
                        e.printStackTrace();
                    }
                }
            }
        }

        // Let the framework know that we did not create the classloader
        return null;
    }

	/* (non-Javadoc)
     * @see org.eclipse.osgi.baseadaptor.hooks.ClassLoadingHook#processClass(java.lang.String, byte[], org.eclipse.osgi.baseadaptor.loader.ClasspathEntry, org.eclipse.osgi.baseadaptor.bundlefile.BundleEntry, org.eclipse.osgi.baseadaptor.loader.ClasspathManager)
     */
    public byte[] processClass(String name, byte[] classbytes,
            ClasspathEntry classpathEntry, BundleEntry entry,
            ClasspathManager manager) {
        byte[] processed = null;

        if (SOURCE_FILE_CLASS.equals(name) || SOURCE_MAPPER_CLASS.equals(name)) {
            processed = rewriteSourceFileClass(name, classbytes);
        }

        return processed;
    }

    /**
     * Rewrite the SourceFile class given the specified class bytes.
     * 
     * @param classBytes
     * @return
     */
    private byte[] rewriteSourceFileClass(String name, byte[] classBytes) {
        byte[] rewritten = classBytes;

        if (Debug.DEBUG_GENERAL) {
            System.out.println(name + " located.  Rewriting class bytes.");
        }

        // Use ASM to rewrite the SourceFile.getMethods call
        ClassReader classReader = new ClassReader(classBytes);
        ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS);

        ClassAdapter adapter = null;
        if (SOURCE_FILE_CLASS.equals(name)) {
            adapter = new SourceFileClassAdapter(classWriter);
        } else {
            adapter = new SourceMapperAccessClassAdapter(classWriter);
        }

        classReader.accept(adapter, ClassReader.EXPAND_FRAMES);

        rewritten = classWriter.toByteArray();

        // Dump the rewritten class file if debug is enabled
        if (Debug.DEBUG_GENERAL) {
            StringWriter stringWriter = new StringWriter();
            ClassReader reader = new ClassReader(rewritten);
            TraceClassVisitor writer = new TraceClassVisitor(new PrintWriter(
                    stringWriter));

            // Skip the debug information in the class
            reader.accept(writer, ClassReader.SKIP_DEBUG);

            System.out.println(stringWriter);
        }

        return rewritten;
    }
}
