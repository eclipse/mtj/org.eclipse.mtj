This bundle provides a class load hook for rewriting the
JDT core SourceFile class to call out for a source file
mapping.

Development
===========
To run under development:
1) Import the org.eclipse.osgi plugin into the workspace with source folders
2) Set the VM argument "-Dosgi.framework.extensions=org.eclipse.mtj.core.hooks"
3) Disable the platform org.eclipse.osgi and use the imported workspace version

Production
==========
Add the following property to the /eclipse/configuration/config.ini file:
osgi.framework.extensions=org.eclipse.mtj.core.hooks

*MUST* be installed into the base Eclipse plugins directory
