/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)    - Initial implementation
 *     Diego Sandin (Motorola)     - Refactoring package name to follow eclipse 
 *                                   standards and removed setPredeploymentRequired
 *                                   invocation
 *     Gang Ma (Sybase)            - Added javadoc detector
 *     Diego Sandin (Motorola)     - Refactored code and added workaround to avoid 
 *                                   importing CDC devices
 *     Gustavo de Paula (Motorola) - Preverifier API refactoring
 *     Diego Sandin (Motorola)     - IDeviceImporter API refactoring  
 *     Diego Sandin (Motorola)     - Fix for Bug 278171    
 */
package org.eclipse.mtj.internal.toolkit.uei;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.*;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceImporter;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.toolkit.uei.properties.DeviceSpecificProperties;
import org.eclipse.mtj.internal.toolkit.uei.properties.DevicesProperties;
import org.eclipse.mtj.internal.toolkit.uei.properties.UEIDeviceDefinition;
import org.eclipse.osgi.util.NLS;

/**
 * A device importer implementation that imports device information from a UEI
 * device emulator.
 * 
 * @author Craig Setera
 */
public class UEIDeviceImporter implements IDeviceImporter {

    /**
     * A non-standard property name in the device properties to hold the
     * emulator version
     */
    public static final String PROP_TOOLKIT_NAME = "org.eclipse.mtj.toolkit.name"; //$NON-NLS-1$

    /**
     * Temporary workaround constant used to avoid importing CDC devices.
     */
    private static final String UNSUPORTED_CONFIGURATION = "CDC"; //$NON-NLS-1$

    public List<IDevice> importDevices(File directory, IProgressMonitor monitor) throws CoreException,
            InterruptedException {
        return importDevices(directory, null, monitor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceImporter#importDevices(java.io.File, org.eclipse.core.runtime.IProgressMonitor)
     */
    public List<IDevice> importDevices(File directory, Map<Class<? extends IDeviceImporter>, Map<File, Object>> importersCollectedData, IProgressMonitor monitor)
            throws CoreException, InterruptedException {

        ArrayList<IDevice> deviceList = null;
        
        UEIImporterCollectedData collectedData = null;
        
        if (importersCollectedData != null) {
            Map<File, Object> collectedDataByDirs = importersCollectedData.get(UEIDeviceImporter.class);
            
            if (collectedDataByDirs == null) {
                collectedDataByDirs = new HashMap<File, Object>();
                importersCollectedData.put(UEIDeviceImporter.class, collectedDataByDirs);
            }
            
            Object data = collectedDataByDirs.get(directory);
            
            if (data == null) {
                collectedData = new UEIImporterCollectedData();
                collectedDataByDirs.put(directory, collectedData);
            } else if (data instanceof UEIImporterCollectedData) {
                collectedData = (UEIImporterCollectedData) data;
            }
        }

        try {
            monitor.beginTask(Messages.UEIDeviceImporter_0, 4);
            UeiPlugin.debugLog(NLS.bind(
                    Messages.UEIDeviceImporter_getMatchingDevices_begin,
                    directory));

            /* Try to find the emulator executable */
            File emulator = getEmulatorInDirectory(directory, collectedData);

            monitor.worked(1);

            if (emulator != null) {

                UeiPlugin.debugLog(NLS.bind(
                        Messages.UEIDeviceImporter_getMatchingDevices_emulator,
                        emulator));

                try {

                    Properties ueiProperties = getUEIEmulatorProperties(emulator, collectedData);
                    monitor.worked(1);

                    if (isValidUEIProperties(ueiProperties)) {
                        UeiPlugin
                                .debugLog(Messages.UEIDeviceImporter_getMatchingDevices_invalid_props);

                        UEIDeviceDefinition definition = getDeviceDefinition(ueiProperties, collectedData);
                        monitor.worked(1);

                        UeiPlugin
                                .debugLog(NLS
                                        .bind(
                                                Messages.UEIDeviceImporter_getMatchingDevices_device_def,
                                                definition));

                        if (definition != null) {
                            deviceList = new ArrayList<IDevice>();
                            addUEIDevices(deviceList, emulator, definition,
                                    ueiProperties, new SubProgressMonitor(
                                            monitor, 1));
                            monitor.worked(1);
                        }
                    }
                } catch (CoreException e) {
                    MTJLogger
                            .log(
                                    IStatus.ERROR,
                                    Messages.UEIDeviceImporter_getMatchingDevices_failed,
                                    e);
                } catch (IOException e) {
                    MTJLogger
                            .log(
                                    IStatus.ERROR,
                                    Messages.UEIDeviceImporter_getMatchingDevices_failed,
                                    e);
                }
            }

            UeiPlugin
                    .debugLog(Messages.UEIDeviceImporter_getMatchingDevices_end
                            + directory);

        } finally {
            monitor.done();
        }

        return deviceList;
    }

    /**
     * Add all of the devices found for the specified emulator with the
     * specified UEI properties.
     * 
     * @param devices
     * @param emulator
     * @param definition
     * @param ueiProperties
     * @return
     * @throws CoreException when fail to create a device
     * @throws InterruptedException
     */
    private void addUEIDevices(ArrayList<IDevice> devices, File emulator,
            UEIDeviceDefinition definition, Properties ueiProperties,
            IProgressMonitor monitor) throws CoreException,
            InterruptedException {

        UeiPlugin.debugLog(Messages.UEIDeviceImporter_addUEIDevices_begin);

        String devicesProp = ueiProperties
                .getProperty(DevicesProperties.DEVICE_LIST.toString());

        UeiPlugin.debugLog(NLS.bind(
                Messages.UEIDeviceImporter_addUEIDevices_devices, devicesProp));

        if (devicesProp != null) {

            StringTokenizer st = new StringTokenizer(devicesProp, ","); //$NON-NLS-1$

            monitor.beginTask(Messages.UEIDeviceImporter_1, st.countTokens());

            while (st.hasMoreTokens()) {

                // Give the user the chance to bail out during the search
                if (monitor.isCanceled()) {
                    throw new InterruptedException();
                }
                try {
                    String deviceName = st.nextToken().trim();
                    IDevice device = createDevice(emulator, definition,
                            ueiProperties, deviceName);

                    if (device != null) {

                        /* Workaround to avoid importing CDC devices */
                        IMIDPAPI api = ((IMIDPDevice) device).getCLDCAPI();
                        if (api != null) {
                            if (!api.getIdentifier().equals(
                                    UNSUPORTED_CONFIGURATION)) {
                                monitor.subTask(NLS.bind(
                                        Messages.UEIDeviceImporter_2,
                                        new String[] { device.getName(),
                                                device.getSDKName() }));
                                devices.add(device);
                                monitor.worked(1);
                            }
                        }
                    }
                } finally {
                    monitor.done();
                }
            }
        }

        UeiPlugin.debugLog(Messages.UEIDeviceImporter_addUEIDevices_end);
    }

    /**
     * Create and return a new IDevice to match the provided information.
     * 
     * @param emulatorExecutable the emulator application.
     * @param definition information on how the UEI emulator will be launched.
     * @param ueiProperties device-specified properties from the complete list
     *            of the emulator's UEI properties.
     * @param deviceName the device name
     * @throws CoreException when fail to determine the parameters available for
     *             the preverifier.
     */
    protected IMIDPDevice createDevice(File emulatorExecutable,
            UEIDeviceDefinition definition, Properties ueiProperties,
            String deviceName) throws CoreException {

        UeiPlugin.debugLog(NLS.bind(Messages.UEIDeviceImporter_createDevice,
                deviceName));

        Properties deviceProperties = filterDeviceProperties(ueiProperties,
                deviceName);

        UEIDeviceInternal device = new UEIDeviceInternal(
                deviceName,
                ueiProperties.getProperty(UEIDeviceImporter.PROP_TOOLKIT_NAME,
                        "Unknown"), //$NON-NLS-1$
                getDeviceDescription(deviceProperties, deviceName),
                deviceProperties, definition, emulatorExecutable,
                getPreverifier(emulatorExecutable));

        return device;
    }

    /**
     * Filter out the device-specified properties from the complete list of the
     * emulator's UEI properties.
     * 
     * @param ueiProperties
     * @param deviceName
     * @return
     */
    protected Properties filterDeviceProperties(Properties ueiProperties,
            String deviceName) {

        Properties deviceProperties = new Properties();

        String prefix = deviceName + "."; //$NON-NLS-1$
        int substringStart = prefix.length();

        Iterator<Object> keys = ueiProperties.keySet().iterator();

        while (keys.hasNext()) {
            String key = (String) keys.next();
            if (key.startsWith(prefix)) {
                String propName = key.substring(substringStart);
                String value = ueiProperties.getProperty(key);
                deviceProperties.setProperty(propName, value);
            }
        }

        return deviceProperties;
    }

    /**
     * Return the device definition associated with the specified UEI
     * properties.
     * 
     * @param ueiProperties
     * @return
     * @throws IOException
     */
    private UEIDeviceDefinition getDeviceDefinition(Properties ueiProperties, UEIImporterCollectedData collectedData)
            throws IOException {
        UEIDeviceDefinition definition = collectedData == null ? null : collectedData.getDefinition();
        
        if (definition == null) {
            String toolkitName = ueiProperties.getProperty(UEIDeviceImporter.PROP_TOOLKIT_NAME, Utils.EMPTY_STRING);
            
            definition = DeviceDefinitionManager.getInstance().getDeviceDefinition(toolkitName);
            
            if (collectedData != null) {
                collectedData.setDefinition(definition);
            }
        }

        return definition;
    }

    /**
     * @param deviceProperties
     * @param deviceName
     * @return
     */
    protected String getDeviceDescription(Properties deviceProperties,
            String deviceName) {
        return deviceProperties.getProperty(
                DeviceSpecificProperties.DESCRIPTION.toString(), deviceName)
                .trim();
    }

    /**
     * Try to get the emulator file in the specified directory.
     * <p>
     * This method try to locate the <code>emulator.exe</code> or
     * <code>emulator</code> files in the specified directory.
     * </p>
     * 
     * @param directory the directory to be searched.
     * @return the emulator file or <code>null</code> if no emulator was found.
     */
    private File getEmulatorInDirectory(File directory, UEIImporterCollectedData collectedData) {
        File emulator = collectedData == null ? null : collectedData.getEmulator();
        
        if (emulator == null) {
            File[] files = null;
    
            try {
                files = directory.listFiles(new FileFilter() {
    
                    /* (non-Javadoc)
                     * @see java.io.FileFilter#accept(java.io.File)
                     */
                    public boolean accept(File pathname) {
                        return pathname.getName().equalsIgnoreCase("emulator.exe") //$NON-NLS-1$
                                || pathname.getName().equals("emulator"); //$NON-NLS-1$
                    }
                });
            } catch (SecurityException se) {
                MTJLogger
                        .log(IStatus.ERROR, NLS.bind(
                                Messages.UEIDeviceImporter_readAccessDenied,
                                directory), se);
            }
    
            if ((files != null) && (files.length > 0)) {
                emulator = files[0];
                
                if (collectedData != null) {
                    collectedData.setEmulator(emulator);
                }
            }
        }

        return emulator;
    }

    /**
     * Try to create a preverify instance.
     * 
     * @param emulatorExecutable a parent abstract pathname.
     * @return A standard preverifier instance.
     * @throws CoreException when fail to determine the parameters available for
     *             the preverifier.
     */
    protected IPreverifier getPreverifier(File emulatorExecutable)
            throws CoreException {

        File preverifierExecutable = new File(emulatorExecutable
                .getParentFile(), "preverify"); //$NON-NLS-1$
        IPreverifier preverifier = MTJCore.createPreverifier(
                IPreverifier.PREVERIFIER_STANDARD, preverifierExecutable);

        return preverifier;
    }

    /**
     * Return the UEI emulator properties.
     * 
     * @param emulator the emulator executable to query
     * @return the UEI properties.
     * @throws CoreException
     */
    private Properties getUEIEmulatorProperties(File emulator, UEIImporterCollectedData collectedData)
            throws CoreException {
        Properties result = collectedData == null ? null : collectedData.getEmulatorProperties();
        
        if (result == null) {
            result = UEIPropertiesReader.getInstance().getUEIProperties(emulator);
            
            if (collectedData != null) {
                collectedData.setEmulatorProperties(result);
            }
        }
        
        return result;
    }

    /**
     * Return a boolean indicating whether the specified properties appears to
     * be valid UEI properties.
     * 
     * @param ueiProperties
     * @return
     */
    private boolean isValidUEIProperties(Properties ueiProperties) {
        return (ueiProperties != null)
                && ueiProperties.containsKey(DevicesProperties.DEVICE_LIST
                        .toString());
    }


    private static class UEIImporterCollectedData {
        private File emulator;
        private Properties emulatorProperties;
        private UEIDeviceDefinition definition;

        public File getEmulator() {
            return emulator;
        }

        public void setEmulator(File emulator) {
            this.emulator = emulator;
        }

        public Properties getEmulatorProperties() {
            return emulatorProperties;
        }

        public void setEmulatorProperties(Properties emulatorProperties) {
            this.emulatorProperties = emulatorProperties;
        }

        public UEIDeviceDefinition getDefinition() {
            return definition;
        }

        public void setDefinition(UEIDeviceDefinition definition) {
            this.definition = definition;
        }
    }
}
