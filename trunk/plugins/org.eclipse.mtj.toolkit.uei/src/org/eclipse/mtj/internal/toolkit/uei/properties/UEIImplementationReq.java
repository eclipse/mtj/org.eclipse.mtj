/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.toolkit.uei.properties;

/**
 * This enum represents when a feature must be implemented or not.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public enum UEIImplementationReq {

    /**
     * Means that the feature must be implemented.
     */
    REQUIRED,

    /**
     * Means that the feature is not required to be implemented. However, if it
     * is implemented, the implementation must fully meet the specification of
     * the feature.
     */
    OPTIONAL
}
