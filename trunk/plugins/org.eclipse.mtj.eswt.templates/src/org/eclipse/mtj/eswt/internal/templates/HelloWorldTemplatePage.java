/*******************************************************************************
 * Copyright (c) 2009 Nokia Corporation and/or its subsidiary(-ies).
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nokia Corporation - initial implementation 
 *******************************************************************************/
package org.eclipse.mtj.eswt.internal.templates;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.internal.ui.wizards.midlet.page.NewMidletWizardPage;
import org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
/**
 * HelloWorld template for a MIDlet using eSWT. 
 *
 */
public class HelloWorldTemplatePage extends AbstractTemplateWizardPage {

	private static final String THREADSUPPORT_JAR_PATH = "/lib/eswtuithreadsupport.jar"; //$NON-NLS-1$
	private static final String UITHREAD_CLASS_NAME = "org.eclipse.ercp.swt.midp.UIThreadSupport"; //$NON-NLS-1$
	private Text messageTxt;
	
	public Map<String, String> getDictionary() {
        Map<String, String> dictionary = new HashMap<String, String>();
        dictionary.put("$message$",  messageTxt.getText()); //$NON-NLS-1$
        correctClasspath();
        return dictionary;
	}
	
	private void correctClasspath(){
		IJavaProject project = ((NewMidletWizardPage)getWizard().getPage(NewMidletWizardPage.PAGE_NAME)).getJavaProject();
		try {
			if(project.findType(UITHREAD_CLASS_NAME) != null ) return; //UIThread support is already available
			Path jarpath = new Path(THREADSUPPORT_JAR_PATH);
			URL url = FileLocator.find(ESWTTemplatesPlugin.getDefault().getBundle(),jarpath,null);
			url = FileLocator.toFileURL(url);
			
			IClasspathEntry uithreadsupport = JavaCore.newLibraryEntry(new Path(url.getPath()), null, null);
			IClasspathEntry[] oldentries = project.getRawClasspath();
			IClasspathEntry[] newentries = new IClasspathEntry[oldentries.length+1];
			System.arraycopy(oldentries, 0, newentries, 0, oldentries.length);
 			newentries[newentries.length-1] = uithreadsupport;
			project.setRawClasspath(newentries, new NullProgressMonitor());
			
		} catch (JavaModelException e) {
			// ignore
		} catch (IOException e) {
			// ignore
		}
	}

	public boolean isPageComplete() {
		
		return (messageTxt != null && 
		messageTxt.getText()!= null && 
		messageTxt.getText().length() > 0);
	}
	
	@Override
	public void createControl(Composite parent) {
		parent.setLayout(new GridLayout(1, false));
		Composite composite = new Composite(parent,SWT.NONE);
		composite.setLayout(new GridLayout(2,false));
		composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		Label messageLbl = new Label(composite, SWT.NONE);
		messageLbl.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false,
				false));
		messageLbl.setText(TemplateMessages.HelloWorldTemplateMessage);
		
		messageTxt = new Text(composite, SWT.SINGLE | SWT.LEAD | SWT.BORDER);
		messageTxt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		messageTxt.setText(TemplateMessages.HelloWorldTemplateMessageHelloWorld);	
	}

}
