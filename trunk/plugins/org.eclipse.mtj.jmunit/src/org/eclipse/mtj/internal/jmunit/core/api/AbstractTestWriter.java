/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.jmunit.core.api;

import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

/**
 * AbstractTestWriter Class provides basic functions for TestCase and TestSuite
 * Writers.
 */
public abstract class AbstractTestWriter {

    protected String delimiter;
    protected IType type;
    
    public AbstractTestWriter(IType _type) throws JavaModelException {
        this.delimiter = _type.getTypeRoot().findRecommendedLineSeparator();
        this.type = _type;
    }
    
    /**
     * Writes a method declaration into the specified buffer.
     * 
     * @param _buffer target buffer.
     * @param _declaration method declaration.
     * @param _body method body.
     * @param _delimiter line delimiter.
     */
    public void writeMethodDeclaration(StringBuffer _buffer,
            String _declaration, String[] _body, String _delimiter) {
        _buffer.append(_declaration).append(" {").append(_delimiter); //$NON-NLS-1$
        if (_body != null) {
            for (String line : _body) {
                _buffer.append(line).append(_delimiter);
            }
        }
        _buffer.append("}").append(_delimiter); //$NON-NLS-1$
    }

    /**
     * Writes method comments into the specified buffer.
     * 
     * @param _buffer target buffer.
     * @param _lines comment lines.
     * @param _delimiter line delimiter.
     */
    public void writeMethodComment(StringBuffer _buffer, String[] _lines,
            String _delimiter) {
        if (_lines != null) {
            _buffer.append("/**").append(_delimiter); //$NON-NLS-1$
            for (String line : _lines) {
                _buffer.append(" * ").append(line).append(_delimiter); //$NON-NLS-1$
            }
            _buffer.append(" */").append(_delimiter); //$NON-NLS-1$
        }
    }

}
