/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Arag�o  (Motorola) - Added method to add  JMUnit for CLDC 1.1 library
 *     
 * @since 0.9.1
 */
package org.eclipse.mtj.internal.jmunit.ui.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.core.project.MTJNature;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.externallibrary.manager.ExternalLibraryManager;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.sdk.device.midp.Configuration;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.actions.AbstractJavaProjectAction;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * EnableJMUnitAction Class adds the JMUnit Nature from a project.
 * 
 * @author David Marques
 */
public class EnableJMUnitAction extends AbstractHandler {
    public Object execute(ExecutionEvent event) throws ExecutionException {
        ISelection selection = HandlerUtil.getCurrentSelection(event);

        if (selection instanceof IStructuredSelection && !selection.isEmpty()) {
            IJavaProject javaProject = AbstractJavaProjectAction.getJavaProject(((IStructuredSelection) selection).getFirstElement());

            if (javaProject != null) {
                try {
                    MTJNature.addNatureToProject(javaProject.getProject(), IMTJCoreConstants.JMUNIT_NATURE_ID,
                            new NullProgressMonitor());

                    IMidletSuiteProject mtjProject = MidletSuiteFactory.getMidletSuiteProject(javaProject);
                    String jmunitLibraryName;

                    if (mtjProject.getApplicationDescriptor()
                            .getConfigurationSpecificationVersion()
                            .compareTo(Configuration.CLDC_10.getVersion()) == 0) {
                        jmunitLibraryName = IMTJCoreConstants.JMUNIT_LIBRARY_CLDC10;
                    } else {
                        jmunitLibraryName = IMTJCoreConstants.JMUNIT_LIBRARY_CLDC11;
                    }

                    ExternalLibraryManager.getInstance().addLibraryToMidletProject(javaProject.getProject(),
                            jmunitLibraryName);
                } catch (CoreException e) {
                    MTJLogger.log(IStatus.ERROR, e);
                }
            }
        }

        return null;
    }
}
