/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.ui.editors.jad;

/**
 * This interface can be used by custom JAD property editors to perform filtering of their 
 * contents depending on the value of some property.
 */
public interface IFilteredJADPropertyEditor {
    /**
     * Property name constant (value <code>"mtj_update_jad_filter"</code>)
     * to signal a change in the value of a field editor that should triggers filtration.
     */
    final String UPDATE_FILTER_NOTIFICATION = "mtj_update_jad_filter";
    
	/**
	 * @return the array of names of properties that require filtering of content of this editor.
	 */
	String[] getFilterPropertyNames();
	
	/**
	 * Performs filtering depending of the value of the given property 
	 *  
	 * @param filterPropertyName filter property name
	 * @param filterPropertyValue filter property value
	 */
	void performFiltering(String filterPropertyName, Object filterPropertyValue);
	
	/**
	 * Triggers event to update filters in dependent editors.
	 */
	void updateDependencies();
}

