/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)         - Initial implementation
 *     Diego Sandin (Motorola)          - Refactoring package name to follow eclipse
 *                                        standards
 *     Gang Ma (Sybase)                 - Refactoring the page to support extensibility
 *     Jon Dearden (Research In Motion) - Added support for applying 
 *                                        DescriptorPropertyDescription(s) to specific
 *                                        page sections [Bug 284452]     
 */
package org.eclipse.mtj.ui.editors.jad;

import java.util.*;
import java.util.Map.Entry;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.mtj.core.project.midp.DescriptorPropertyDescription;
import org.eclipse.mtj.internal.core.project.midp.JADAttributesRegistry;
import org.eclipse.mtj.internal.ui.editors.FormLayoutFactory;
import org.eclipse.mtj.internal.ui.editors.jad.form.JADFormEditor;
import org.eclipse.mtj.internal.ui.preferences.ExtendedStringFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;

/**
 * An editor part page that may be added to the JAD editor.
 * 
 * @since 1.0
 */
public abstract class JADPropertiesEditorPage extends AbstractJADEditorPage
        implements IPropertyChangeListener {

    /** The descriptors being edited */
    private DescriptorPropertyDescription[] descriptors;

    /** The field editors in use */
    protected FieldEditor[] fieldEditors;

    /**
     * A constructor that creates the JAD Properties EditorPage and initializes
     * it with the editor.
     * 
     * @param editor the parent editor
     * @param id the unique identifier
     * @param title the page title
     * @noreference This constructor is not intended to be referenced by clients.
     */
    public JADPropertiesEditorPage(JADFormEditor editor, String id, String title) {
        super(editor, id, title);
    }

    /**
     * A constructor that creates the JAD Properties EditorPage. The parent
     * editor need to be passed in the <code>initialize</code> method if this
     * constructor is used.
     * 
     * @param id a unique page identifier
     * @param title a user-friendly page title
     */
    public JADPropertiesEditorPage(String id, String title) {
        super(id, title);
    }

    /**
     * Saves the contents of this part by storing all FieldEditor's values.
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor monitor) {
        monitor.setTaskName(getTitle());
        for (FieldEditor element : fieldEditors) {
            element.store();
        }
        setDirty(false);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#editorInputChanged()
     */
    @Override
    public void editorInputChanged() {
        updateEditComponents();
    }

    /**
     * Get all application descriptor property description for the current page
     * {@link FormPage#getId() ID}.
     * 
     * @return the list of property descriptors for the current page ID
     */
    public DescriptorPropertyDescription[] getDescriptors() {
        if (descriptors == null) {
            descriptors = doGetDescriptors();
        }
        return descriptors;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#isManagingProperty(java.lang.String)
     */
    @Override
    public boolean isManagingProperty(String property) {
        for (int i = 0; i < getDescriptors().length; i++) {
            DescriptorPropertyDescription desc = getDescriptors()[i];
            if (property.equals(desc.getPropertyName())) {
                return true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.util.IPropertyChangeListener#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
     */
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getProperty().equals(FieldEditor.VALUE)) {
            setDirty(true);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#setFocus()
     */
    @Override
    public void setFocus() {
        if (fieldEditors.length > 0) {
            fieldEditors[0].setFocus();
        }
    }

    /**
     * Create a new combo field editor.
     * 
     * @param toolkit
     * @param composite
     * @param description
     * @return
     */
    protected FieldEditor createComboFieldEditor(FormToolkit toolkit,
            Composite composite, DescriptorPropertyDescription description) {
        ListDescriptorPropertyDescription listDescription = (ListDescriptorPropertyDescription) description;

        return new ComboJADPropertyEditor(listDescription.getPropertyName(), listDescription.getDisplayName(),
                listDescription.getNamesAndValues(), listDescription.getFilter(), composite);
    }

    /**
     * Create a new integer field editor.
     * 
     * @param toolkit
     * @param parent
     * @param descriptor
     * @return
     */
    protected IntegerFieldEditor createIntegerFieldEditor(FormToolkit toolkit,
            Composite parent, DescriptorPropertyDescription descriptor) {

        IntegerFieldEditor integerEditor = new IntegerFieldEditor(descriptor
                .getPropertyName(), descriptor.getDisplayName(), parent);
        toolkit.adapt(integerEditor.getTextControl(parent), true, true);

        return integerEditor;
    }

    /**
     * Create a new String field editor.
     * 
     * @param toolkit
     * @param parent
     * @param descriptor
     * @return
     */
    protected StringFieldEditor createStringFieldEditor(FormToolkit toolkit,
            Composite parent, DescriptorPropertyDescription descriptor) {
        ExtendedStringFieldEditor editor = new ExtendedStringFieldEditor(descriptor
                .getPropertyName(), descriptor.getDisplayName(), parent);
        toolkit.adapt(editor.getTextControl(parent), true, true);

        return editor;
    }

    /**
     * Update the application descriptor the components are handling
     */
    protected void updateEditComponents() {
        if (fieldEditors != null) {
            IPreferenceStore store = (IPreferenceStore) getPreferenceStore();
            for (FieldEditor fieldEditor : fieldEditors) {
                fieldEditor.setPreferenceStore(store);
                fieldEditor.load();             
            }
            
            for (FieldEditor fieldEditor : fieldEditors) {
            	if (fieldEditor instanceof IFilteredJADPropertyEditor) {
                    ((IFilteredJADPropertyEditor) fieldEditor).updateDependencies();
                }
            }
        }        
    }

    /**
     * Sets the help context on the given control.
     * 
     * @param control the control on which to register the context help
     */
    protected abstract void addContextHelp(Composite control);

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createFormContent(IManagedForm managedForm) {
        super.createFormContent(managedForm);

        FormToolkit toolkit = managedForm.getToolkit();

        Composite body = managedForm.getForm().getBody();
        body.setLayout(FormLayoutFactory.createFormTableWrapLayout(true, 1));

        Composite center = toolkit.createComposite(body);
        center.setLayout(FormLayoutFactory.createFormPaneTableWrapLayout(false,
                1));
        center.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB));

        Section section = createStaticBasicSection(toolkit, center,
                getSectionTitle(), getSectionDescription());

        Composite sectionClient = createStaticSectionClient(toolkit, section, null);
        sectionClient.setLayout(FormLayoutFactory
                .createSectionClientTableWrapLayout(false, 1));

        createSectionContent(managedForm, sectionClient, this);
        section.setClient(sectionClient);
    }

    /**
     * Create a Section Client Content for the descriptors available in the
     * page.
     * 
     * @param managedForm
     * @param composite
     * @param propertyChangeListener
     */
    protected void createSectionContent(IManagedForm managedForm,
            Composite composite, final IPropertyChangeListener propertyChangeListener) {

        FormToolkit toolkit = managedForm.getToolkit();

        composite.setLayout(FormLayoutFactory.createSectionClientGridLayout(
                false, 2));

        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);

        DescriptorPropertyDescription[] theDescriptors = getDescriptors();
        Map<String, FieldEditor> fieldEditorsByPropertyNames = new HashMap<String, FieldEditor>();
        Map<String, List<IFilteredJADPropertyEditor>> dependentEditors = new HashMap<String, List<IFilteredJADPropertyEditor>>();
        fieldEditors = new FieldEditor[theDescriptors.length];

        for (int i = 0; i < theDescriptors.length; i++) {

            switch (theDescriptors[i].getDataType()) {

            case DescriptorPropertyDescription.DATATYPE_INT:
                fieldEditors[i] = createIntegerFieldEditor(toolkit, composite,
                        theDescriptors[i]);
                break;

            case DescriptorPropertyDescription.DATATYPE_LIST:
                fieldEditors[i] = createComboFieldEditor(toolkit, composite,
                        theDescriptors[i]);
                break;

            case DescriptorPropertyDescription.DATATYPE_URL:
            case DescriptorPropertyDescription.DATATYPE_STRING:
            default:
                fieldEditors[i] = createStringFieldEditor(toolkit, composite,
                        theDescriptors[i]);
                break;
            }
            
            fieldEditorsByPropertyNames.put(theDescriptors[i].getPropertyName(), fieldEditors[i]);
            
            if (fieldEditors[i] instanceof IFilteredJADPropertyEditor) {
            	IFilteredJADPropertyEditor filteredEditor = (IFilteredJADPropertyEditor) fieldEditors[i];
            	String[] filterPropertyNames = filteredEditor.getFilterPropertyNames();
            	
            	if (filterPropertyNames != null) {
            		for (String filterPropertyName : filterPropertyNames) {
						List<IFilteredJADPropertyEditor> propertyDependentEditors = dependentEditors.get(filterPropertyName);
						
						if (propertyDependentEditors == null) {
							propertyDependentEditors = new ArrayList<IFilteredJADPropertyEditor>();
							dependentEditors.put(filterPropertyName, propertyDependentEditors);
						}
						
						propertyDependentEditors.add(filteredEditor);
					}
            	}
            }

            Label label = fieldEditors[i].getLabelControl(composite);
            toolkit.adapt(label, false, false);

            // Listen for property change events on the editor
            fieldEditors[i].setPropertyChangeListener(propertyChangeListener);
        }
        
        for (Entry<String, List<IFilteredJADPropertyEditor>> dependentEditorsEntry : dependentEditors.entrySet()) {
			final String masterEditorPropertyName = dependentEditorsEntry.getKey();
			FieldEditor masterEditor = fieldEditorsByPropertyNames.get(masterEditorPropertyName);
			
			if (masterEditor != null) {
				final List<IFilteredJADPropertyEditor> propertyDependentEditors = dependentEditorsEntry.getValue();
				masterEditor.setPropertyChangeListener(new IPropertyChangeListener() {
					public void propertyChange(final PropertyChangeEvent event) {
						propertyChangeListener.propertyChange(event);
						
						if (IFilteredJADPropertyEditor.UPDATE_FILTER_NOTIFICATION.equals(event.getProperty())) {
    						for (final IFilteredJADPropertyEditor propertyDependentEditor : propertyDependentEditors) {  							
    						    propertyDependentEditor.performFiltering(masterEditorPropertyName, event.getNewValue());   								
    						}
						}
					}
				});
			}
		}

        // Adapt the Combo instances...
        Control[] children = composite.getChildren();
        for (Control control : children) {
            if (control instanceof Combo) {
                toolkit.adapt(control, false, false);
            }
        }

        updateEditComponents();
        addContextHelp(composite);
    }

    /**
     * sub class may override this method to provide its own descriptor provide
     * strategy
     * 
     * @return
     */
    protected DescriptorPropertyDescription[] doGetDescriptors() {
        return JADAttributesRegistry.getJADAttrDescriptorsByPage(getId());
    }

    /**
     * @return
     */
    protected abstract String getSectionDescription();

    /**
     * @return
     */
    protected abstract String getSectionTitle();

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
     */
    @Override
    protected void setInput(IEditorInput input) {
        super.setInput(input);
        updateEditComponents();
        setDirty(false);
    }

}
