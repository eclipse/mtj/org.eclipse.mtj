/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.ui.editors.jad;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.mtj.ui.editors.jad.ListDescriptorPropertyDescription.IListDescriptorPropertyFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

public class ComboJADPropertyEditor extends FieldEditor implements IFilteredJADPropertyEditor {
    private static final String UPDATE_FILTER_NOTIFICATION = "mtj_update_jad_filter";

    private Combo combo;

    private String currentValue;

    private String[][] namesAndValues;

    private IListDescriptorPropertyFilter filter;

    private Map<String, Object> filterProperties = new HashMap<String, Object>();

    /**
     * Create the filtered combo box field editor.
     * 
     * @param name the name of the preference this field editor works on
     * @param labelText the label text of the field editor
     * @param entryNamesAndValues the names (labels) and underlying values to populate the combo widget.  These should be
     * arranged as: { {name1, value1}, {name2, value2}, ...}
     * @param filter filter
     * @param parent the parent composite
     */
    public ComboJADPropertyEditor(String name, String labelText, String[][] namesAndValues,
            IListDescriptorPropertyFilter filter, Composite parent) {
        init(name, labelText);

        Assert.isTrue(checkArray(namesAndValues));

        this.namesAndValues = namesAndValues;
        this.filter = filter;

        createControl(parent);
    }

    private boolean checkArray(String[][] table) {
        if (table == null) {
            return false;
        }
        for (int i = 0; i < table.length; i++) {
            String[] array = table[i];
            if (array == null || array.length != 2) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void adjustForNumColumns(int numColumns) {
        if (numColumns > 1) {
            Control control = getLabelControl();
            int left = numColumns;
            if (control != null) {
                ((GridData) control.getLayoutData()).horizontalSpan = 1;
                left = left - 1;
            }
            ((GridData) combo.getLayoutData()).horizontalSpan = left;
        } else {
            Control control = getLabelControl();
            if (control != null) {
                ((GridData) control.getLayoutData()).horizontalSpan = 1;
            }
            ((GridData) combo.getLayoutData()).horizontalSpan = 1;
        }
    }

    @Override
    protected void doFillIntoGrid(Composite parent, int numColumns) {
        int comboC = 1;
        if (numColumns > 1) {
            comboC = numColumns - 1;
        }
        Control control = getLabelControl(parent);
        GridData gd = new GridData();
        gd.horizontalSpan = 1;
        control.setLayoutData(gd);
        control = getComboBoxControl(parent);
        gd = new GridData();
        gd.horizontalSpan = comboC;
        gd.horizontalAlignment = GridData.FILL;
        control.setLayoutData(gd);
        control.setFont(parent.getFont());
    }

    @Override
    protected void doLoad() {
        updateComboForValue(getPreferenceStore().getString(getPreferenceName()), false);
    }

    @Override
    protected void doLoadDefault() {
        updateComboForValue(getPreferenceStore().getDefaultString(getPreferenceName()), false);
    }
    
    public void updateDependencies() {
        fireValueChanged(UPDATE_FILTER_NOTIFICATION, null, currentValue);
    }

    @Override
    protected void doStore() {
        if (currentValue == null) {
            getPreferenceStore().setToDefault(getPreferenceName());
            return;
        }
        getPreferenceStore().setValue(getPreferenceName(), currentValue);
    }

    @Override
    public int getNumberOfControls() {
        return 2;
    }

    private Combo getComboBoxControl(Composite parent) {
        if (combo == null) {
            combo = new Combo(parent, SWT.READ_ONLY);
            combo.setFont(parent.getFont());

            String[][] filteredNamesAndValues = getFilteredNamesAndValues();

            for (int i = 0; i < filteredNamesAndValues.length; i++) {
                combo.add(filteredNamesAndValues[i][0], i);
            }

            combo.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent evt) {
                    String oldValue = currentValue;
                    String name = combo.getText();
                    currentValue = getValueForName(name);
                    setPresentsDefaultValue(false);
                    fireValueChanged(VALUE, oldValue, currentValue);
                    fireValueChanged(UPDATE_FILTER_NOTIFICATION, oldValue, currentValue);
                }
            });
        }

        return combo;
    }

    private String getValueForName(String name) {
        for (int i = 0; i < namesAndValues.length; i++) {
            String[] entry = namesAndValues[i];
            if (name.equals(entry[0])) {
                return entry[1];
            }
        }
        return namesAndValues[0][1];
    }

    private void updateComboForValue(String value, boolean considerFiltration) {
        String oldValue = currentValue;
        currentValue = value;

        String[][] filteredNamesAndValues = considerFiltration ? getFilteredNamesAndValues() : namesAndValues;

        for (int i = 0; i < filteredNamesAndValues.length; i++) {
            if (value.equals(filteredNamesAndValues[i][1])) {
                combo.setText(filteredNamesAndValues[i][0]);
                return;
            }
        }
        if (filteredNamesAndValues.length > 0) {
            currentValue = filteredNamesAndValues[0][1];
            combo.setText(filteredNamesAndValues[0][0]);
            fireValueChanged(VALUE, oldValue, currentValue);
            
            if (considerFiltration) {
                fireValueChanged(UPDATE_FILTER_NOTIFICATION, oldValue, currentValue);
            }
        }
    }

    @Override
    public void setEnabled(boolean enabled, Composite parent) {
        super.setEnabled(enabled, parent);
        getComboBoxControl(parent).setEnabled(enabled);
    }

    public void setFilter(IListDescriptorPropertyFilter filter) {
        this.filter = filter;
    }

    public String[] getFilterPropertyNames() {
        return filter == null ? null : filter.getFilterPropertyNames();
    }

    public void performFiltering(String filterPropertyName, Object filterPropertyValue) {
        if (filter == null) {
            return;
        }

        filterProperties.put(filterPropertyName, filterPropertyValue);

        String[][] filteredNamesAndValues = getFilteredNamesAndValues();

        combo.removeAll();

        for (int i = 0; i < filteredNamesAndValues.length; i++) {
            combo.add(filteredNamesAndValues[i][0], i);
        }

        updateComboForValue(currentValue, true);
    }

    private String[][] getFilteredNamesAndValues() {
        if (filter == null) {
            return namesAndValues;
        }

        return filter.filterNamesAndValues(filterProperties, namesAndValues);
    }
}
