/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)	- Move the class to the new package so that 
 *     							  it can be used in other plug-ins
 */
package org.eclipse.mtj.ui.editors.jad;

import java.util.Map;

import org.eclipse.mtj.core.project.midp.DescriptorPropertyDescription;

/**
 * Application descriptor property description holding a list of strings.
 * 
 * @since 1.0
 */
public class ListDescriptorPropertyDescription extends
        DescriptorPropertyDescription {

    // The list of names and values
    private String[][] namesAndValues;
    
    private IListDescriptorPropertyFilter filter;
    
    /**
     * Constructor
     * 
     * @param propertyName
     * @param displayName
     * @param namesAndValues
     */
    public ListDescriptorPropertyDescription(String propertyName,
            String displayName, String[][] namesAndValues) {
        super(propertyName, displayName, DATATYPE_LIST);
        this.namesAndValues = namesAndValues;
    }

    /**
     * @return Returns the namesAndValues.
     */
    public String[][] getNamesAndValues() {
        return namesAndValues;
    }

    /**
     * Sets the list content filter.
     */
    public void setFilter(IListDescriptorPropertyFilter filter) {
		this.filter = filter;
	}
    
    /**
     * @return the list content filter.
     */
	public IListDescriptorPropertyFilter getFilter() {
		return filter;
	}

	
	/**
     * IListDescriptorPropertyFilter allows to filter the list property contents. 
     */
	public interface IListDescriptorPropertyFilter {
		/**
		 * @return the array of names of properties that require filtering of content of the bound list descriptor property.
		 */
    	String[] getFilterPropertyNames();
    	
    	/**
    	 * Performs filtering. Usually used when the list content should be filtered depending on the values of other properties.
    	 *  
    	 * @param filterProperties map with filter property values, where map keys are filter property names 
    	 * @param namesAndValues content to filter
    	 * @return filtered content
    	 */
    	String[][] filterNamesAndValues(Map<String, Object> filterProperties, String[][] namesAndValues);
    }
}
