/*
* Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
* All rights reserved.
* This component and the accompanying materials are made available
* under the terms of "Eclipse Public License v1.0"
* which accompanies this distribution, and is available
* at the URL "http://www.eclipse.org/legal/epl-v10.html".
*
* Initial Contributors:
* 		Nokia Corporation - initial contribution.
*
*/
package org.eclipse.mtj.internal.ui.wizards.export.packaging;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
/**
 * Content provider that takes IJavaProject as input and provides the 
 * MTJRuntime contents.
 * @author Gorkem Ercan
 *
 */
public class MTJRuntimeListContentProvider implements
		IStructuredContentProvider {

	public void dispose() {

	}


	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {

	}

	public Object[] getElements(Object inputElement) {
		IJavaProject javaProject = (IJavaProject) inputElement;
		IMidletSuiteProject project = MidletSuiteFactory.getMidletSuiteProject(javaProject);
		if (project == null ){
			return new Object[0];
		}
		MTJRuntimeList list = project.getRuntimeList();
		List<Object> result = new ArrayList<Object>();
		result.addAll(list);
		return result.toArray();
	}

}
