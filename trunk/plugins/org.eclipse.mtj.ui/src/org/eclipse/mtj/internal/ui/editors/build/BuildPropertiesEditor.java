/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Refactoring build.properties
 */
package org.eclipse.mtj.internal.ui.editors.build;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.internal.core.build.MTJBuildProperties;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.editors.build.pages.BuildPropertiesPage;
import org.eclipse.mtj.internal.ui.editors.build.pages.BuildPropertiesSourcePage;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.IFormPage;

/**
 * BuildPropertiesEditor class implements a {@link FormEditor}
 * for the build.properties files.
 * 
 * @author David Marques
 */
public class BuildPropertiesEditor extends FormEditor {

	private IMTJProject mtjProject;
	private List<IFormPage> pages;
	private IFile file;

	/**
	 * Creates a {@link BuildPropertiesEditor} instance.
	 */
	public BuildPropertiesEditor() {
		this.pages = new ArrayList<IFormPage>();
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.forms.editor.FormEditor#init(org.eclipse.ui.IEditorSite, org.eclipse.ui.IEditorInput)
	 */
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);
		if (input instanceof IFileEditorInput) {
			file = ((IFileEditorInput) input).getFile();
			IJavaProject javaProject = JavaCore.create(file.getProject());
			if (javaProject != null) {				
				mtjProject = MidletSuiteFactory.getMidletSuiteProject(javaProject);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
	 */
	protected void addPages() {
		if (this.mtjProject != null) {
			try {				
				MTJBuildProperties buildProperties = MTJBuildProperties.getBuildProperties(this.mtjProject);
				
				BuildPropertiesPage buildPropertiesPage = new BuildPropertiesPage(this, buildProperties);
				this.addPage(buildPropertiesPage);
				
				BuildPropertiesSourcePage sourcePage = new BuildPropertiesSourcePage(this, buildProperties);
				this.addPage(sourcePage);
				
				this.pages.add(buildPropertiesPage);
				this.pages.add(sourcePage);
			} catch (PartInitException e) {
				MTJLogger.log(IStatus.ERROR, e);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	public void doSave(IProgressMonitor monitor) {
		for (IFormPage page : this.pages) {
			page.doSave(monitor);
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	public void doSaveAs() {
		for (IFormPage page : this.pages) {
			page.doSaveAs();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	public boolean isSaveAsAllowed() {
		return false;
	}

}
