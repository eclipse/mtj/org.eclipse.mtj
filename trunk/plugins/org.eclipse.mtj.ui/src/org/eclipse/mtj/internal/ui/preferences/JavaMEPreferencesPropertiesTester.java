/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.preferences;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.mtj.internal.core.build.CodeValidationBuilder;

public class JavaMEPreferencesPropertiesTester extends PropertyTester {
    private static final String PROPERTY_CONFIGURE_VALIDATION = "configureCodeValidation"; //$NON-NLS-1$

    public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
        if (PROPERTY_CONFIGURE_VALIDATION.equals(property)) {
            return !CodeValidationBuilder.getCodeValidators(null).isEmpty();
        }

        return false;
    }

}
