/*
* Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
* All rights reserved.
* This component and the accompanying materials are made available
* under the terms of "Eclipse Public License v1.0"
* which accompanies this distribution, and is available
* at the URL "http://www.eclipse.org/legal/epl-v10.html".
*
* Initial Contributors:
* 		Nokia Corporation - initial contribution.
*
*
*/
package org.eclipse.mtj.internal.ui.wizards.export.antenna;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PlatformUI;
/**
 * An action for invoking the {@link AntennaBuildFileExportWizard} 
 * @author Gorkem Ercan
 *
 */
public class AntennaExportAction extends Action {
	private IJavaProject project;
	
	public AntennaExportAction(IJavaProject project){
		this.project = project;
	}
	
	
	@Override
	public void run() {
		StructuredSelection selection = null;
		if (project!= null && MidletSuiteFactory.isMidletSuiteProject(project.getProject())){
			selection = new StructuredSelection(project);
		}
		IWorkbenchWizard wizard = new AntennaBuildFileExportWizard();
		wizard.init(PlatformUI.getWorkbench(), selection);
		
		WizardDialog wd = new WizardDialog(MTJUIPlugin.getActiveWorkbenchShell(), wizard);
		wd.create();

		int result = wd.open();
		notifyResult(result == Window.OK);
	}
}
