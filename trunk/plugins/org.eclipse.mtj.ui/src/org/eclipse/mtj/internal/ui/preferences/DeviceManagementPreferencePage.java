/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang Ma      (Sybase)    - Add the support to group the devices by SDK
 *     Hugo Raniere (Motorola)  - Removing default and apply buttons per
 *                                discussion on bugzilla
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     Jon Dearden  (Research In Motion) - Major changes to allow for SDK providers
 *                                         and a new tree-based user interface [Bug 264736].
 */
package org.eclipse.mtj.internal.ui.preferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.core.sdk.ISDKProvider;
import org.eclipse.mtj.core.sdk.ManagedSDK;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IManagedDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.sdk.ImportedSDKProvider;
import org.eclipse.mtj.internal.core.sdk.SDKProviderRegistry;
import org.eclipse.mtj.internal.core.sdk.device.AbstractMIDPDevice;
import org.eclipse.mtj.internal.core.sdk.device.DeviceRegistry;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.devices.DeviceEditorConfigElement;
import org.eclipse.mtj.internal.ui.devices.DeviceEditorRegistry;
import org.eclipse.mtj.internal.ui.devices.DeviceImportWizard;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.TreeEvent;
import org.eclipse.swt.events.TreeListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

/**
 * Implements the preference page for managing the registered devices.
 *
 * @author Craig Setera
 */
public class DeviceManagementPreferencePage extends PreferencePage implements
        IWorkbenchPreferencePage {

    /** Identifier of this preference page */
    public static final String ID = "org.eclipse.mtj.ui.preferences.deviceManagementPreferencePage"; //$NON-NLS-1$

    private static final int DEFAULT_COLUMN_IDX = 1;
    private static final String DEFAULT_MARKER = "\u25cf\u25cf"; //$NON-NLS-1$

    private Tree deviceTree;
    private Button deleteButton;
    private Button duplicateButton;
    private Button editButton;
    private Button setDefaultButton;
    private Button importButton;
    private IAction editAction;
    private IActionDelegate editDelegate;
    private IWorkbench workbench;
    private Font boldFont;

    private Image IMG_FOLDER;
    private Image IMG_CELLPHONE;

    /**
     * Construct a new preference page instance
     */
    public DeviceManagementPreferencePage() {
        super(MTJUIMessages.DeviceManagementPreferencePage_title);
        setDescription(MTJUIMessages.DeviceManagementPreferencePage_description + "\n\n" //$NON-NLS-1$
                + MTJUIMessages.DeviceManagementPreferencePage_instaled_sdks_label);
        setPreferenceStore(MTJUIPlugin.getDefault().getCorePreferenceStore());
        noDefaultAndApplyButton();
        editAction = new Action() {};
        ISharedImages images = PlatformUI.getWorkbench().getSharedImages();
        IMG_FOLDER = images.getImage(ISharedImages.IMG_OBJ_FOLDER);
        IMG_CELLPHONE = MTJUIPluginImages.DESC_CELLPHONE_OBJ.createImage();
    }

    // << IWorkbenchPreferencePage >>
    public void init(IWorkbench workbench) {
        this.workbench = workbench;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createContents(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        layout.makeColumnsEqualWidth = true;
        container.setLayout(layout);
        GridData gd = new GridData(SWT.FILL, SWT.FILL, true, true);
        container.setLayoutData(gd);
        container.setFont(parent.getFont());
        try {
            DeviceRegistry.getInstance().load();
        } catch (Exception e) {
            MTJLogger.log(IStatus.ERROR, e);
            createErrorContents(container, e);
            return container;
        }
        try {
            createDeviceContents(container);
        } catch (Exception e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
        PlatformUI.getWorkbench().getHelpSystem().setHelp(container,
                "org.eclipse.mtj.ui.help_DeviceManagementPage"); //$NON-NLS-1$
        return container;
    }

    /**
     * Create the UI elements for the page
     */
    private Control createDeviceContents(Composite parent) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(2, false));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        gridData.minimumWidth = 475;
        gridData.heightHint = 400;

        deviceTree = new Tree(composite, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL);
        deviceTree.setHeaderVisible(true);
        deviceTree.setLayoutData(gridData);
        deviceTree.setLinesVisible(true);
        deviceTree.addListener(SWT.MouseDoubleClick, new Listener() {
            public void handleEvent(Event event) {
                handleTreeDoubleClick();
            }
        });
        deviceTree.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleTreeSelection();
            }
        });
        deviceTree.addTreeListener(new TreeListener() {
            public void treeCollapsed(TreeEvent e) {
                adjustDefaultMarker();
            }
            public void treeExpanded(TreeEvent e) {
                adjustDefaultMarker();
            }
        });

        TreeColumn deviceNameColumn = new TreeColumn(deviceTree, SWT.LEFT);
        deviceNameColumn.setText(MTJUIMessages.DeviceManagementPreferencePage_name_columnInfo);
        deviceNameColumn.setWidth(315);
        deviceNameColumn.setAlignment(SWT.LEFT);

        TreeColumn defaultColumn = new TreeColumn(deviceTree, SWT.CENTER, DEFAULT_COLUMN_IDX);
        defaultColumn.setText(MTJUIMessages.DeviceManagementPreferencePage_default_columnInfo);
        defaultColumn.setWidth(55);

        TreeColumn cldcColumn = new TreeColumn(deviceTree, SWT.LEFT);
        cldcColumn.setText(MTJUIMessages.DeviceManagementPreferencePage_configuration_columnInfo);
        cldcColumn.setWidth(80);

        TreeColumn midpColumn = new TreeColumn(deviceTree, SWT.LEFT);
        midpColumn.setText(MTJUIMessages.DeviceManagementPreferencePage_profile_columnInfo);
        midpColumn.setWidth(60);

        Composite buttonComposite = new Composite(composite, SWT.NONE);
        buttonComposite.setLayout(new GridLayout(1, true));
        buttonComposite.setLayoutData(new GridData(GridData.FILL_VERTICAL));

        editButton = new Button(buttonComposite, SWT.PUSH);
        editButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        editButton.setText(MTJUIMessages.DeviceManagementPreferencePage_editButton_lable_text);
        editButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                updateButtonEnablement();
                updateEditDelegate();
                handleEditDevice();
            }
        });

        duplicateButton = new Button(buttonComposite, SWT.PUSH);
        duplicateButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        duplicateButton.setText(MTJUIMessages.DeviceManagementPreferencePage_duplicateButton_label_text);
        duplicateButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleDuplicate();
            }
        });

        deleteButton = new Button(buttonComposite, SWT.PUSH);
        deleteButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        deleteButton.setText(MTJUIMessages.DeviceManagementPreferencePage_deleteButton_label_text);
        deleteButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleDelete();
            }
        });

        setDefaultButton = new Button(buttonComposite, SWT.PUSH);
        setDefaultButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        setDefaultButton.setText(MTJUIMessages.DeviceManagementPreferencePage_setDefaultButton_label_text);
        setDefaultButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleSetDefault();
            }
        });

        new Label(buttonComposite, SWT.NONE);

        importButton = new Button(buttonComposite, SWT.PUSH);
        importButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        importButton.setText(MTJUIMessages.DeviceManagementPreferencePage_importButton_label_text);
        importButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                handleImport();
            }
        });

        FontData[] fontData = deviceTree.getFont().getFontData();
        for (FontData fontDatum : fontData) {
            fontDatum.setStyle(fontDatum.getStyle() | SWT.BOLD);
        }
        Display display = Display.getDefault();
        boldFont = new Font(display, fontData);

        refreshDeviceTree();
        doDefaultTreeExpand();
        updateEditDelegate();
        updateButtonEnablement();
        selectStartingDefaultDevice();
        return composite;
    }

    /**
     * Create the contents for a device registry exception.
     */
    private Control createErrorContents(Composite parent, Exception e) {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new GridLayout(1, true));
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        String text = NLS.bind(
                MTJUIMessages.DeviceManagementPreferencePage_error_reading_device_registry,
                new String[] { e.getClass().getName(), e.getMessage() });
        Text errorText = new Text(composite, SWT.BORDER | SWT.MULTI | SWT.READ_ONLY | SWT.WRAP);
        errorText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        errorText.setText(text);
        return composite;
    }

    /**
     * (Re)populate the device tree.
     */
    private void refreshDeviceTree() {
        deviceTree.setRedraw(false);
        deviceTree.removeAll();
        IDevice defaultDevice = DeviceRegistry.getInstance().getDefaultDevice();
        List<ISDKProvider> providers = SDKProviderRegistry.getInstance()
        .getSDKProviders();
        Collections.sort(providers);
        // Insert imported devices at top of tree
        providers.add(0, ImportedSDKProvider.getInstance());

        // For each SDK provider...
        for (ISDKProvider provider : providers) {
            List<ISDK> sdks = provider.getSDKs();
            if (sdks.size() < 1)
                continue;
            TreeItem providerTreeItem = new TreeItem(deviceTree, SWT.NONE);
            providerTreeItem.setData(new MTJObject(provider));
            providerTreeItem.setText(provider.getName());
            Image providerLogo = provider.getLogo();
            if (provider.getLogo() != null) {
                providerTreeItem.setImage(providerLogo);
            } else {
                providerTreeItem.setImage(IMG_FOLDER);
            }
            providerTreeItem.setFont(boldFont);
            if (sdks.size() > 1) {
                // ISDKs are not Comparable, so use SortedMap to do the sort
                SortedMap<String, ISDK> sortedSdks = new TreeMap<String, ISDK>();
                for (ISDK sdk : provider.getSDKs()) {
                    sortedSdks.put(sdk.getName(), sdk);
                }
                sdks.clear();
                sdks.addAll(sortedSdks.values());
            }

            // For each SDK...
            for (ISDK sdk : sdks) {
                List<IDevice> devices;
                try {
                    devices = sdk.getDeviceList();
                } catch (CoreException e) {
                    e.printStackTrace();
                    continue;
                }
                if (devices.size() < 1)
                    continue;
                TreeItem sdkTreeItem = new TreeItem(providerTreeItem, SWT.NONE);
                sdkTreeItem.setData(new MTJObject(sdk));
                sdkTreeItem.setText(sdk.getName());
                if (sdk instanceof ManagedSDK) {
                    sdkTreeItem.setImage(MTJUIPluginImages.DESC_IU_OBJ.createImage());
                } else {
                    sdkTreeItem.setImage(IMG_FOLDER);
                }
                if (devices.size() >= 1) {
                    // Do sort - IDevices are not Comparable, so use SortedMap
                    SortedMap<String, IDevice> sortedDevices = new TreeMap<String, IDevice>();
                    for (IDevice device : devices) {
                        sortedDevices.put(device.getName(), device);
                    }
                    devices.clear();
                    devices.addAll(sortedDevices.values());
                }
                // For each device...
                for (IDevice device : devices) {
                    TreeItem deviceTreeItem = new TreeItem(sdkTreeItem, SWT.NONE);
                    deviceTreeItem.setData(new MTJObject(device));
                    String displayedName = device.getName();
                    String defaultString = Utils.EMPTY_STRING;
                    if (defaultDevice != null
                            && device.getName().equals(defaultDevice.getName())
                            && device.getSDKName().equals(defaultDevice.getSDKName())) {
                        defaultString = DEFAULT_MARKER;
                    }
                    String configuration = Utils.EMPTY_STRING;
                    String profile = Utils.EMPTY_STRING;
                    if (device instanceof IMIDPDevice) {
                        IAPI cldcApi = ((IMIDPDevice) device).getCLDCAPI();
                        if (cldcApi != null) {
                            configuration = cldcApi.toString();
                        }
                        IAPI midpApi = ((IMIDPDevice) device).getMIDPAPI();
                        if (midpApi != null) {
                            profile = midpApi.toString();
                        }
                    }
                    deviceTreeItem.setText(
                            new String[] { displayedName, defaultString, configuration, profile });
                    deviceTreeItem.setImage(IMG_CELLPHONE);
                }
            }
        }
        deviceTree.setRedraw(true);
    }

    // -----------------------
    // Tree expansion handling
    // -----------------------

    /**
     * Expand entire device tree.
     */
    private void doDefaultTreeExpand() {
        TreeItem[] providerItems = deviceTree.getItems();
        for (TreeItem providerItem : providerItems) {
            providerItem.setExpanded(true);
            TreeItem[] sdkItems = providerItem.getItems();
            for (TreeItem sdkItem : sdkItems) {
                sdkItem.setExpanded(true);
            }
        }
    }

    /**
     * Upon a double click, toggle the expansion state of the selected SDK
     * provider item or SDDK item. A double click on a device results in
     * an edit action for that device.
     */
    private void toggleExpansionOnSelectedItem() {
        TreeItem[] treeItems = deviceTree.getSelection();
        if (treeItems.length == 1) {
            treeItems[0].setExpanded(!treeItems[0].getExpanded());
            adjustDefaultMarker();
        }
    }

    /**
     * Used by the duplicate device feature so it can return the tree items to
     * their former expansion state after the duplication and refresh. Returns a
     * (possibly empty) list of only those items that are expanded.
     */
    private List<String> recordProviderAndSdkExpansionStates() {
        ArrayList<String> list = new ArrayList<String>();
        TreeItem[] providerItems = deviceTree.getItems();
        for (TreeItem providerItem : providerItems) {
            if (providerItem.getExpanded()) {
                MTJObject providerObject = (MTJObject) providerItem.getData();
                list.add(providerObject.getSdkProvider().getName());
                TreeItem[] sdkItems = providerItem.getItems();
                for (TreeItem sdkItem : sdkItems) {
                    if (sdkItem.getExpanded()) {
                        MTJObject sdkObject = (MTJObject) sdkItem.getData();
                        if (sdkObject != null) {
                            list.add(sdkObject.getSdk().getName());
                        }
                    }
                }
            }
        }
        return list;
    }

    /**
     * Returns the tree expansion state to a former recorded state.
     */
    private void replayItemExpansionStates(List<String> expansionSnapshot) {
        TreeItem[] providerItems = deviceTree.getItems();
        for (TreeItem providerItem : providerItems) {
            if (expansionSnapshot.contains(providerItem.getText())) {
                providerItem.setExpanded(true);
                TreeItem[] sdkItems = providerItem.getItems();
                for (TreeItem sdkItem : sdkItems) {
                    sdkItem.setExpanded(expansionSnapshot.contains(sdkItem.getText()));
                }
            }
        }
        adjustDefaultMarker();
    }

    /**
     * If an SDK provider item or SDK item contains the default device and the
     * tree item is collapsed, this method will copy the default device marker
     * (**) to a parent tree item so that it can be seen.
     */
    private void adjustDefaultMarker() {
        TreeItem defaultTreeItem = null;
        TreeItem sdkTreeItem = null;
        TreeItem providerTreeItem = null;
        TreeItem[] providerItems = deviceTree.getItems();

        for (TreeItem providerItem : providerItems) {
            TreeItem[] sdkItems = providerItem.getItems();
            for (TreeItem sdkItem : sdkItems) {
                TreeItem[] deviceItems = sdkItem.getItems();
                for (TreeItem deviceItem : deviceItems) {
                    MTJObject mtjObject = (MTJObject) deviceItem.getData();
                    IDevice thisDevice = mtjObject.getDevice();
                    if (isDefaultDevice(thisDevice)) {
                        providerTreeItem = providerItem;
                        sdkTreeItem = sdkItem;
                        defaultTreeItem = deviceItem;
                        break;
                    }
                }
            }
        }

        if (defaultTreeItem != null) {
            final TreeItem finalSdkTreeItem = sdkTreeItem;
            final TreeItem finalProviderTreeItem = providerTreeItem;
            Runnable treeRefresh = new Runnable() {
                public void run() {
                    finalSdkTreeItem.setText(DEFAULT_COLUMN_IDX, Utils.EMPTY_STRING);
                    finalProviderTreeItem.setText(DEFAULT_COLUMN_IDX, Utils.EMPTY_STRING);

                    if (finalProviderTreeItem.getExpanded() == false) {
                        finalProviderTreeItem.setText(DEFAULT_COLUMN_IDX, DEFAULT_MARKER);
                    } else if (finalSdkTreeItem.getExpanded() == false) {
                        finalSdkTreeItem.setText(DEFAULT_COLUMN_IDX, DEFAULT_MARKER);
                    }
                }
            };
            deviceTree.getDisplay().asyncExec(treeRefresh);
        }
    }

    // -----------------------
    // Item selection handling
    // -----------------------

    /**
     * Return a single ItemData of the selected item if there is only one
     * selection, otherwise <code>null</code>.
     */
    private MTJObject getSelectedMTJObject() {
        TreeItem[] treeItems = deviceTree.getSelection();
        if (treeItems.length == 1) {
            return (MTJObject) treeItems[0].getData();
        }
        return null;
    }

    /**
     * Return a (possibly empty) list of all ItemData of the selected items.
     */
    private List<MTJObject> getSelectedMTJObjects() {
        List<MTJObject> list = new ArrayList<MTJObject>();
        TreeItem[] treeItems = deviceTree.getSelection();
        for (TreeItem treeItem : treeItems) {
            list.add((MTJObject) treeItem.getData());

        }
        return list;
    }

    /**
     * Return a device if there is only one selection and that selection is an
     * IDevice, otherwise return null.
     */
    private IDevice getSelectedDevice() {
        MTJObject mtjObject = getSelectedMTJObject();
        if (mtjObject != null && mtjObject.isDevice)
            return mtjObject.getDevice();
        return null;
    }

    /**
     * Set the tree selection to the given device.
     */
    private void setSelectedDevice(IDevice device) {
        deviceTree.redraw();
        TreeItem[] providerItems = deviceTree.getItems();
        for (TreeItem providerItem : providerItems) {
            TreeItem[] sdkItems = providerItem.getItems();
            for (TreeItem sdkItem : sdkItems) {
                TreeItem[] deviceItems = sdkItem.getItems();
                for (TreeItem deviceItem : deviceItems) {
                    MTJObject mtjObject = (MTJObject) deviceItem.getData();
                    IDevice thisDevice = mtjObject.getDevice();
                    if (device == thisDevice) {
                        deviceTree.setSelection(deviceItem);
                        deviceTree.setFocus();
                        return;
                    }
                }
            }
        }
    }

    /**
     * Set the tree selection to the given devices.
     */
    private void setSelectedDevices(List<IDevice> devices) {
        TreeItem[] treeItems = new TreeItem[devices.size()];
        int idx = 0;
        deviceTree.redraw();
        TreeItem[] providerItems = deviceTree.getItems();
        for (TreeItem providerItem : providerItems) {
            TreeItem[] sdkItems = providerItem.getItems();
            for (TreeItem sdkItem : sdkItems) {
                TreeItem[] deviceItems = sdkItem.getItems();
                for (TreeItem deviceItem : deviceItems) {
                    MTJObject mtjObject = (MTJObject) deviceItem.getData();
                    if (devices.contains(mtjObject.getDevice())) {
                        treeItems[idx] = deviceItem;
                        idx++;
                    }
                }
            }
        }
        deviceTree.setSelection(treeItems);
        deviceTree.setFocus();
    }

    /**
     * Return a TreeItem for the given MTJObject or <code>null</code> if no such
     * match is found.
     */
    private TreeItem getTreeItem(MTJObject mtjObject) {
        TreeItem[] treeItems = deviceTree.getSelection();
        for (TreeItem treeItem : treeItems) {
            MTJObject object = (MTJObject)treeItem.getData();
            if (object == mtjObject)
                return treeItem;
        }
        return null;
    }

    // ---------------
    // Action handlers
    // ---------------

    /**
     * Set the default device.
     */
    private void handleSetDefault() {
        IDevice selectedDevice = getSelectedDevice();
        if (selectedDevice != null) {
            DeviceRegistry.getInstance().setDefaultDevice(selectedDevice);
            List<String> expandedTreeItems = recordProviderAndSdkExpansionStates();
            deviceTree.setRedraw(false);
            refreshDeviceTree();
            replayItemExpansionStates(expandedTreeItems);
            setSelectedDevice(selectedDevice);
            deviceTree.setRedraw(true);
            updateButtonEnablement();
        }
    }

    /**
     * Delete the currently selected device(s). The ImportedSdkProvider and any
     * manually imported SDKs or devices may be deleted. Managed SDKs or devices
     * (unless a duplicate) may not be deleted.
     */
    private void handleDelete() {

        // This method relies on canDoDelete() to ensure that delete is allowed
        // only when an item may be deleted.

        List<IDevice> selectedDevices = new ArrayList<IDevice>();
        List<MTJObject> mtjObjects = getSelectedMTJObjects();
        MTJObject sampleMtjObject = mtjObjects.get(0);

        // Record current state
        List<String> expandedSnapshot = recordProviderAndSdkExpansionStates();

        if (sampleMtjObject.isDevice) {
            for (MTJObject deviceObject : mtjObjects) {
                selectedDevices.add(deviceObject.getDevice());
            }
        } else if (sampleMtjObject.isSdk) {
            for (MTJObject sdkObject : mtjObjects) {
                // Visually expose items to be deleted
                TreeItem sdkItem = getTreeItem(sdkObject);
                if (sdkItem != null) {
                    sdkItem.setExpanded(true);
                }
                ISDK sdk = sdkObject.getSdk();
                try {
                    selectedDevices.addAll(sdk.getDeviceList());
                } catch (CoreException e) {
                    e.printStackTrace();
                }
            }
        } else if (sampleMtjObject.isSdkProvider) {
            for (MTJObject providerObject : mtjObjects) {
                // Visually expose items to be deleted
                TreeItem providerItem = getTreeItem(providerObject);
                if (providerItem != null) {
                    providerItem.setExpanded(true);
                    TreeItem[] sdkItems = providerItem.getItems();
                    for (TreeItem sdkItem : sdkItems) {
                        sdkItem.setExpanded(true);
                    }
                }
                ISDKProvider provider = providerObject.getSdkProvider();
                List<ISDK> sdks = provider.getSDKs();
                for (ISDK sdk : sdks) {
                    try {
                        selectedDevices.addAll(sdk.getDeviceList());
                    } catch (CoreException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        if (selectedDevices.isEmpty())
            return;

        // Highlight the devices the user has chosen to delete
        setSelectedDevices(selectedDevices);

        int count = selectedDevices.size();
        String title = MTJUIMessages.DeviceManagementPreferencePage_confirm_delete_dialog_title;
        String message = NLS.bind(
                        MTJUIMessages.DeviceManagementPreferencePage_confirm_delet_dialog_message,
                        count);

        if (MessageDialog.openConfirm(getShell(), title, message)) {

            for (IDevice device : selectedDevices) {
                if (device instanceof IManagedDevice) {
                    ISDK sdk = device.getSDK();
                    ManagedSDK managedSDK = (ManagedSDK) sdk;
                    managedSDK.deleteDuplicateDevice((IManagedDevice) device);
                } else {
                    try {
                        if (!ImportedSDKProvider.getInstance().removeDevice(
                                device)) {
                            throw new PersistenceException();
                        }
                    } catch (PersistenceException e) {
                        handleException(
                                MTJUIMessages.DeviceManagementPreferencePage_error_remove_device,
                                e);
                    }
                }
            }

            refreshDeviceTree();
            updateButtonEnablement();
            selectStartingDefaultDevice();
        }

        replayItemExpansionStates(expandedSnapshot);
        updateButtonEnablement();
    }

    /**
     * Duplicate a device. Managed devices may be duplicated and it is the
     * responsiblity of the SDK provider to persist the duplicated device.
     * Duplicate imported devices are stored by the DeviceRegistry.
     */
    private void handleDuplicate() {

        IDevice selectedDevice = getSelectedDevice();
        if (selectedDevice == null)
            return;
        ISDK sdk = selectedDevice.getSDK();
        IDevice duplicate = null;
        String uniqueName = findUniqueName(selectedDevice);

        if (sdk instanceof ManagedSDK) {
            duplicate = ((ManagedSDK) sdk).duplicateDevice(
                    (IManagedDevice) selectedDevice, uniqueName);
        } else {
            duplicate = ImportedSDKProvider.getInstance().duplicateDevice(
                    selectedDevice, uniqueName);
        }

        try {
            if (duplicate == null)
                throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
            handleException(
                    MTJUIMessages.DeviceManagementPreferencePage_error_duplicated_device,
                    e);
        }

        List<String> expandedTreeItems = recordProviderAndSdkExpansionStates();
        refreshDeviceTree();
        replayItemExpansionStates(expandedTreeItems);
        setSelectedDevice(duplicate);
        updateButtonEnablement();

    }

    /**
     * Launch the device editor dialog.
     */
    private void handleEditDevice() {
        IDevice selectedDevice = getSelectedDevice();
        if (selectedDevice instanceof IMIDPDevice) {
            if (editDelegate != null) {
                editDelegate.run(editAction);
            }
        }
    }

    /**
     * Prompt the user to import some devices using the import device wizard.
     */
    private void handleImport() {
        DeviceImportWizard wizard = new DeviceImportWizard();
        WizardDialog dialog = new WizardDialog(getShell(), wizard);
        if (dialog.open() == Window.OK) {
            refreshDeviceTree();
            updateButtonEnablement();
            doDefaultTreeExpand();
            selectStartingDefaultDevice();
        }
    }

    /**
     * A double click on a device launches the device editor dialog.
     * A doubleclick on other items toggles their expansion state.
     */
    private void handleTreeDoubleClick() {
        MTJObject mtjObject = getSelectedMTJObject();
        if (mtjObject != null) {
            if (mtjObject.canExpand) {
                // Expand or collapse selection
                toggleExpansionOnSelectedItem();
            } else if (mtjObject.canEdit) {
                // Edit selected device
                updateButtonEnablement();
                updateEditDelegate();
                handleEditDevice();
            }
        }
    }

    /**
     * Update button states upon a selection on the tree.
     */
    private void handleTreeSelection() {
        updateEditDelegate();
        updateButtonEnablement();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#performOk()
     */
    @Override
    public boolean performOk() {
        boolean wasOK = false;
        try {
            DeviceRegistry.getInstance().store();
            wasOK = true;
        } catch (Exception e) {
            MTJUIPlugin.displayError(getShell(), IStatus.ERROR, -999,
                    MTJUIMessages.DeviceManagementPreferencePage_error_storing_devises_dialog_title,
                    MTJUIMessages.DeviceManagementPreferencePage_error_storing_devises_dialog_message,
                    e);
            MTJLogger.log(IStatus.ERROR, NLS .bind(
                    MTJUIMessages.DeviceManagementPreferencePage_error_storing_devises_log_msg,
                    new String[] { e.getClass().getName(), e.getMessage() }), e);
        }

        return wasOK;
    }

    // --------------------
    // ButtonState handling
    // --------------------

    /**
     * Update the edit delegate.
     */
    private void updateEditDelegate() {
        editDelegate = null;
        IDevice device = getSelectedDevice();
        if (device instanceof AbstractMIDPDevice) {
            AbstractMIDPDevice midpDevice = (AbstractMIDPDevice) device;
            TreePath tp = new TreePath(new AbstractMIDPDevice[] { midpDevice });
            IStructuredSelection selection = new TreeSelection(tp);
            editDelegate = findActionDelegate(device);
            if (editDelegate != null) {
                editDelegate.selectionChanged(editAction, selection);
            }
        }
    }

    /**
     * Update button states.
     */
    private void updateButtonEnablement() {
        List<MTJObject> mtjObjects = getSelectedMTJObjects();
        if (mtjObjects.size() == 1) {
            MTJObject mtjObject = mtjObjects.get(0);
            duplicateButton.setEnabled(mtjObject.canDuplicate);
            editButton.setEnabled(mtjObject.canEdit && editDelegate != null
                    && editAction.isEnabled());
            IDevice selectedDevice = mtjObject.getDevice();
            setDefaultButton.setEnabled(mtjObject.canSetDefault
                    && !(isDefaultDevice(selectedDevice)));
        } else {
            duplicateButton.setEnabled(false);
            editButton.setEnabled(false);
            setDefaultButton.setEnabled(false);
        }
        deleteButton.setEnabled(canDoDelete(mtjObjects));
    }

    /**
     * Used for delete button state determination.
     */
    private boolean canDoDelete(List<MTJObject> selectedMtjObjects) {
        // Rule: There must be at least one selected item
        if (selectedMtjObjects.isEmpty())
            return false;
        int providerCount = 0;
        int sdkCount = 0;
        int deviceCount = 0;
        for (MTJObject thisMtjObject : selectedMtjObjects) {
            // Rule: All selected items must be deleteable
            if (!thisMtjObject.canDelete) {
                return false;
            }
            if (thisMtjObject.isSdkProvider) {
                providerCount++;
            } else if (thisMtjObject.isSdk) {
                sdkCount++;
            } else if (thisMtjObject.isDevice) {
                deviceCount++;
            }
        }
        // Rule: Multiple selections may be deleted if they are all devices,
        // or all SDKs, or all providers, but not a mix. This is to prevent
        // ambiguity.
        if (providerCount > 0 && (sdkCount > 0 || deviceCount > 0)
                || sdkCount > 0 && (providerCount > 0 || deviceCount > 0)
                || deviceCount > 0 && (sdkCount > 0 || providerCount > 0)) {
            return false;
        }
        return true;
    }

    // -----
    // Other
    // -----

    /**
     * Select a starting default device from the currently available devices.
     */
    private void selectStartingDefaultDevice() {
        if (DeviceRegistry.getInstance().getDefaultDevice() == null) {
            TreeItem[] providerItems = deviceTree.getItems();
            for (TreeItem providerItem : providerItems) {
                TreeItem[] sdkItems = providerItem.getItems();
                for (TreeItem sdkItem : sdkItems) {
                    TreeItem[] deviceItems = sdkItem.getItems();
                    for (TreeItem deviceItem : deviceItems) {
                        MTJObject mtjObject = (MTJObject) deviceItem.getData();
                        if (mtjObject.isDevice) {
                            IDevice device = mtjObject.getDevice();
                            DeviceRegistry.getInstance().setDefaultDevice(device);
                            List<String> expandedTreeItems = recordProviderAndSdkExpansionStates();
                            refreshDeviceTree();
                            replayItemExpansionStates(expandedTreeItems);
                            return;
                        }
                    }
                }
            }
        }
    }

    private boolean isDefaultDevice(IDevice device) {
        if (device == null)
            return false;
        String deviceName = device.getName();
        if (deviceName == null)
            return false;
        String deviceSdkName = device.getSDKName();
        if (deviceSdkName == null)
            return false;
        IDevice defaultDevice = DeviceRegistry.getInstance().getDefaultDevice();
        if (defaultDevice == null)
            return false;
        String defaultDeviceName = defaultDevice.getName();
        if (defaultDeviceName == null)
            return false;
        String defaultDeviceSdkName = defaultDevice.getSDKName();
        if (defaultDeviceSdkName == null)
            return false;
        return (defaultDeviceName.equals(deviceName)
                && defaultDeviceSdkName.equals(deviceSdkName));
    }

    /**
     * Return the action delegate for the specified device or <code>null</code>
     * if no delegate can be found.
     */
    private IActionDelegate findActionDelegate(IDevice device) {
        IActionDelegate delegate = null;
        DeviceEditorConfigElement element = DeviceEditorRegistry.findEditorElement(device);
        if (element != null) {
            try {
                delegate = element.getActionDelegate();
            } catch (CoreException e) {
                MTJLogger.log(IStatus.WARNING,
                        MTJUIMessages.DeviceManagementPreferencePage_error_findActionDelegate,
                        e);
            }
        }

        return delegate;
    }

    /**
     * Handle the specified exception by displaying it to the user and logging.
     */
    private void handleException(String message, Throwable throwable) {
        MTJLogger.log(IStatus.WARNING,
                MTJUIMessages.DeviceManagementPreferencePage_error_device_registry_exception,
                throwable);
        MTJUIPlugin.displayError(getShell(), IStatus.WARNING, -999,
                MTJUIMessages.DeviceManagementPreferencePage_error_device_registry_error,
                message, throwable);
    }

    /**
     * Find a new name that is unique using the specified device's name as a
     * base.
     */
    private String findUniqueName(IDevice device) {
        // A pattern for locating previously unique-ified names.
        final Pattern UNIQUE_NAME_PATTERN = Pattern.compile("^.+\\((\\d+)\\)$"); //$NON-NLS-1$
        String uniqueName = null;
        String groupName = device.getSDKName();
        String baseName = device.getName();

        for (int i = 1; i <= 100; i++) {
            StringBuffer deviceNameBuffer = new StringBuffer(baseName);
            Matcher matcher = UNIQUE_NAME_PATTERN.matcher(deviceNameBuffer);
            if (matcher.find()) {
                int matchStart = matcher.start(1);
                int matchEnd = matcher.end(1);
                deviceNameBuffer.replace(matchStart, matchEnd, Integer.toString(i));
            } else {
                deviceNameBuffer.append(" (").append(i).append(")"); //$NON-NLS-1$ //$NON-NLS-2$
            }
            uniqueName = deviceNameBuffer.toString();
            try {
                IMIDPDevice foundDevice = (IMIDPDevice) DeviceRegistry
                        .getInstance().getDevice(groupName, uniqueName);
                if (foundDevice == null) {
                    break;
                }
            } catch (PersistenceException e) {
                MTJLogger.log(IStatus.ERROR,
                        MTJUIMessages.DeviceManagementPreferencePage_error_find_device, e);
            }
        }
        return uniqueName;
    }

    /**
     * MTJObject is a simple container to hold an arbitrary object to be
     * associated with a tree item along with allowable actions for that object.
     * Used primarily for button state handling. MTJObjects are stored in
     * TreeItem's application defined widget data.
     */
    static private class MTJObject {

        private Object  payload; // IDevice, ISDK, or ISDKProvider
        boolean isDevice, isSdk, isSdkProvider;
        boolean canEdit, canDuplicate, canSetDefault, canExpand, canDelete;

        MTJObject(Object payload) {
            this.payload = payload;
            if (payload instanceof IDevice) {
                isDevice = true;
                canEdit = true;
                canDuplicate = true;
                if (payload instanceof IManagedDevice) {
                    // An ISDKProvider device may be deleted if it is a duplicate
                    canDelete = ((IManagedDevice) payload).isDuplicate();
                } else {
                    // Imported devices may always be deleted
                    canDelete = true;
                }
                canSetDefault = true;
            } else if (payload instanceof ISDK) {
                isSdk = true;
                canExpand = true;
                canDelete = !(payload instanceof ManagedSDK);
            } else if (payload instanceof ISDKProvider) {
                isSdkProvider = true;
                canExpand = true;
                canDelete = (payload instanceof ImportedSDKProvider);
            } else {
                throw new IllegalArgumentException();
            }
        }

        IDevice getDevice() {
            return (isDevice ? (IDevice) payload : null);
        }

        ISDK getSdk() {
            return (isSdk ? (ISDK) payload : null);
        }

        ISDKProvider getSdkProvider() {
            return (isSdkProvider ? (ISDKProvider) payload : null);
        }
    }

}
