/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version                     
 */
package org.eclipse.mtj.internal.ui.actions.l10n;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.core.project.MTJNature;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.actions.AbstractJavaProjectAction;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * DisableLocalizationAction class disables localization feature into MTJ
 * projects.
 * 
 * @author David Marques
 * @since 0.9.1
 */
public class DisableLocalizationAction extends AbstractHandler {
    public Object execute(ExecutionEvent event) throws ExecutionException {
        ISelection selection = HandlerUtil.getCurrentSelection(event);

        if (selection instanceof IStructuredSelection && !selection.isEmpty()) {
            IJavaProject javaProject = AbstractJavaProjectAction.getJavaProject(((IStructuredSelection) selection).getFirstElement());

            if (javaProject != null) {
                try {
                    MTJNature.removeNatureFromProject(javaProject.getProject(),
                            IMTJCoreConstants.L10N_NATURE_ID,
                            new NullProgressMonitor());
                } catch (CoreException e) {
                    MTJLogger.log(IStatus.ERROR, e);
                }
            }
        }

        return null;
    }
}
