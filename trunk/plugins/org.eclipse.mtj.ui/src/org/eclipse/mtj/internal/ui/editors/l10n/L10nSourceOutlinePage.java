/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version 
 */
package org.eclipse.mtj.internal.ui.editors.l10n;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.mtj.internal.core.text.IEditingModel;
import org.eclipse.mtj.internal.core.text.l10n.L10nModel;
import org.eclipse.mtj.internal.ui.editor.MTJFormEditor;
import org.eclipse.mtj.internal.ui.editor.SourceOutlinePage;
import org.eclipse.swt.widgets.Control;

public class L10nSourceOutlinePage extends SourceOutlinePage {

    /**
     * Creates a new content outline page for the XML editor.
     * 
     * @param editor
     * @param model
     * @param lProvider
     * @param cProvider
     * @param defaultComparator
     * @param comparator
     */
    public L10nSourceOutlinePage(MTJFormEditor editor, IEditingModel model,
            IBaseLabelProvider provider, IContentProvider provider2,
            ViewerComparator defaultComparator, ViewerComparator comparator) {
        super(editor, model, provider, provider2, defaultComparator, comparator);
    }
    
    @Override
    public void reconciled(IDocument document) {
        
        final Control control = getControl();
        if ((control == null) || control.isDisposed()) {
            return;
        }
        
        control.getDisplay().asyncExec(new Runnable() {
            public void run() {
                if (control.isDisposed()) {
                    return;
                }                
                if (model instanceof L10nModel) {            
                    ((L10nModel) model).getLocales().validate();
                }
            }
        });
        super.reconciled(document);
    }

    /**
     * Validates the model.
     */
    public void refresh() {
        if (model instanceof L10nModel) {            
            ((L10nModel) model).getLocales().validate();
        }
    }
}
