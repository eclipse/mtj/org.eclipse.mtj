/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.properties;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.ui.preferences.CodeValidationPreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

/**
 * Property page with code validation settings.
 */
public class CodeValidationPropertyPage extends PropertyAndPreferencePage {
    @Override
    protected void embedPreferencePage(Composite composite) {
        preferencePage = new CodeValidationPreferencePage(getPreferenceStore(), JavaCore.create(getProject()));
        preferencePage.createControl(composite);
        
        GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(preferencePage.getControl());

    }

    @Override
    protected String getProjectSpecificSettingsKey() {
        return IMTJCoreConstants.PREF_CODE_VALIDATION_USE_PROJECT;
    }

    @Override
    protected boolean isReadOnly() {
        return false;
    }
}
