package org.eclipse.mtj.internal.ui.wizards.export.antenna;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.build.export.AntennaBuildExport;
import org.eclipse.mtj.internal.core.build.export.AntennaExportException;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;

public class AntennaBuildFileExportWizard extends Wizard implements
		IExportWizard {

	private IStructuredSelection fSelection;
	private AntennaBuildFileExportPage page;
	public void init(IWorkbench workbench, IStructuredSelection selection) {
        setWindowTitle(MTJUIMessages.AntennaBuildFileExportWizard_windowTitle);
    	setDefaultPageImageDescriptor(MTJUIPluginImages.DESC_ANTENNA_EXPORT_WIZ);
    	setNeedsProgressMonitor(true);
        fSelection= selection;
	}
	
    @Override
	@SuppressWarnings("unchecked")
	public void addPages()
    {
        page = new AntennaBuildFileExportPage();
        @SuppressWarnings("rawtypes")
		List projects = fSelection.toList();
        page.setSelectedProjects(projects);
        addPage(page);
    }

	@Override
	public boolean performFinish() {
		final IMidletSuiteProject midletProject = page.getMidletProject();
		final String buildFile = page.getBuildFileName();
		final String buildDirectory = page.getBuildDirectory();
		if (midletProject == null )
			return false;
		try {
			this.getContainer().run(false, true, new IRunnableWithProgress() {
				
				public void run(IProgressMonitor monitor) throws InvocationTargetException,
						InterruptedException {
					AntennaBuildExport export = new AntennaBuildExport(midletProject,buildFile, buildDirectory);
					 try {
						export.doExport(monitor);
					} catch (AntennaExportException e) {
						MTJUIPlugin.getDefault().
						getLog().log(new Status(IStatus.ERROR, MTJUIPlugin.getPluginId(), "Failed to export Antenna build files",e)); //$NON-NLS-1$
					}
					
				}
			});
		} catch (Exception e){
			MTJUIPlugin.getDefault().
				getLog().log(new Status(IStatus.ERROR, MTJUIPlugin.getPluginId(), "Failed to export Antenna build files",e)); //$NON-NLS-1$
		}
		return true;
	}

}
