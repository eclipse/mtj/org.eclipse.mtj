/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code
 *     David Marques (Motorola) - Using Utils nature management functions.
 *     David Marques (Motorola) - Avoid removing builder twice.
 */

package org.eclipse.mtj.internal.ui.actions.preprocessing;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.core.project.MTJNature;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.actions.AbstractJavaProjectAction;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * An action that enables the natures, etc such that a project can have
 * preprocessor functionality.
 * 
 * @author Craig Setera
 */
public class DisablePreprocessingAction extends AbstractHandler {
	/**
	 * Disable preprocessing on the specified java project.
	 * 
	 * @param javaProject
	 * @throws CoreException
	 */
	private void disablePreprocessing(IJavaProject javaProject)
			throws CoreException {
		IProject project = javaProject.getProject();
		MTJNature.removeNatureFromProject(project,
				IMTJCoreConstants.J2ME_PREPROCESSING_NATURE_ID,
				new NullProgressMonitor());
	}
	
	public Object execute(ExecutionEvent event) throws ExecutionException {
        ISelection selection = HandlerUtil.getCurrentSelection(event);

        if (selection instanceof IStructuredSelection && !selection.isEmpty()) {
            IJavaProject javaProject = AbstractJavaProjectAction.getJavaProject(((IStructuredSelection) selection).getFirstElement());

            if (javaProject != null) {
                try {
                    disablePreprocessing(javaProject);
                } catch (CoreException e) {
                    MTJLogger.log(IStatus.ERROR, e);
                }
            }
        }

        return null;
    }
}
