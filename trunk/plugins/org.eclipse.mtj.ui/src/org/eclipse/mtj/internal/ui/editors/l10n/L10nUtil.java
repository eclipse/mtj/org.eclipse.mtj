package org.eclipse.mtj.internal.ui.editors.l10n;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.internal.core.text.IDocumentElementNode;
import org.eclipse.mtj.internal.core.text.l10n.L10nEntry;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocale;
import org.eclipse.mtj.internal.core.text.l10n.L10nLocales;

/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version 
 */
public class L10nUtil {

    /**
     * This method finds all conflicting locales.
     * 
     * @param locales target locales instance.
     * @return the list of all conflicting locales.
     */
    public static List<L10nLocale> findConflictedLocales(L10nLocales locales) {
        List<L10nLocale> conflicted = new LinkedList<L10nLocale>();
        IDocumentElementNode[] localeNodes = locales.getChildNodes();
        for (IDocumentElementNode localeNode : localeNodes) {
            L10nLocale locale = (L10nLocale) localeNode;
            if (localeHasKeyConflicts(locale)) {
                conflicted.add(locale);
            }
        }
        return conflicted;
    }

    /**
     * Checks whether the specified locale has conflicting keys.
     * 
     * @param locale target locale instance.
     * @return true if there are conslicts false otherwise.
     */
    private static boolean localeHasKeyConflicts(L10nLocale locale) {
        IDocumentElementNode nodes[] = locale.getChildNodes();
        for (IDocumentElementNode node : nodes) {
            L10nEntry entry = (L10nEntry) node;
            if (entry.getStatus().getSeverity() == IStatus.ERROR) {
                return true;
            }
        }
        return false;
    }

}
