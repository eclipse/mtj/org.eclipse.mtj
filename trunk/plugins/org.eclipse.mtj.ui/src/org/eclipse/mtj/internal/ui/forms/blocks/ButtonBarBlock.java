/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola)  - Initial version
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     David Marques (Motorola) - Adding scan button and implementing buttons
 *     							  customization.
 *     David Marques (Motorola) - Adding setEnabled() method.
 */
package org.eclipse.mtj.internal.ui.forms.blocks;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * @author Diego Madruga Sandin
 */
public class ButtonBarBlock {

    /**
     * Add Button
     */
    public static final int BUTTON_ADD = 1 << 1;

    public static final int BUTTON_ADD_INDEX = 0;
    
    /**
     * Button
     */
    public static final int BUTTON_EDIT = 1 << 2;

    public static final int BUTTON_EDIT_INDEX = 1;
    

    /**
     * Button
     */
    public static final int BUTTON_DOWN = 1 << 5;

    public static final int BUTTON_DOWN_INDEX = 4;

    /**
     * Button
     */
    public static final int BUTTON_REMOVE = 1 << 3;

    public static final int BUTTON_REMOVE_INDEX = 2;

    /**
     * Button
     */
    public static final int BUTTON_UP = 1 << 4;

    public static final int BUTTON_UP_INDEX = 3;

    /**
     * Button
     */
    public static final int BUTTON_SCAN = 1 << 6;

    public static final int BUTTON_SCAN_INDEX = 5;
    
    /**
     * All buttons will be disabled
     */
    public static final int BUTTON_ALL = BUTTON_ADD | BUTTON_EDIT | BUTTON_REMOVE | 
    	BUTTON_UP | BUTTON_DOWN | BUTTON_SCAN;
    
    private Map<Integer, Button> buttons;

    /**
     * @param parent
     * @param toolkit
     * @param buttons
     */
    public ButtonBarBlock(Composite parent, FormToolkit toolkit,
            int buttons) {
        initialize(parent, toolkit, buttons);
    }

    /**
     * Creates an instance of the buttons block.
     * 
     * @param parent parent composite.
     * @param buttons enabled buttons.
     */
    public ButtonBarBlock(Composite parent, int buttons) {
        initialize(parent, buttons);
    }
    
    /**
     * @param button
     * @param eventType
     * @param listener
     */
    public void addButtonListener(int button, int eventType, Listener listener) {
        buttons.get(button).addListener(eventType, listener);
    }

    /**
     * @param index
     * @return
     */
    public Button getButton(int index) {
        return buttons.get(index);
    }

    /**
     * @param index
     * @return
     */
    public boolean isButtonEnabled(int index) {
        return buttons.get(index).isEnabled();
    }

    /**
     * @param index
     * @param listener
     */
    public void setButtonMouseListener(int index, MouseListener listener) {
        buttons.get(index).addMouseListener(listener);
    }

    /**
     * @param index
     * @param enabled
     */
    public void setEnabled(int index, boolean enabled) {
        buttons.get(index).setEnabled(enabled);
    }
    
    public void setButtonVisible(int index, boolean visible) {
        Button button = getButton(index);
        Object layoutData = button.getLayoutData();
        Composite buttonsPanel = button.getParent();
        Layout parentLayout = buttonsPanel.getLayout();
        
        if (parentLayout instanceof GridLayout && layoutData instanceof GridData) {
            button.setVisible(visible);
            ((GridData)layoutData).exclude = !visible;
            
            Composite buttonsPanelOwner = buttonsPanel.getParent();
            
            if (buttonsPanelOwner == null) {
                buttonsPanel.layout();
            } else {
                buttonsPanelOwner.layout();
            }
        }
    }

    private void createButton(Composite parent, FormToolkit toolkit, int buttonIndex, String buttonTitle) {
        Button button;
        
        if (toolkit == null) {
            button = new Button(parent, SWT.PUSH);
            button.setText(buttonTitle);
        } else {
            button = toolkit.createButton(parent, buttonTitle, SWT.PUSH);
        }
        
        button.setEnabled(false);
        
        if (parent.getLayout() instanceof GridLayout) {
            GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).applyTo(button);
        }
        
        buttons.put(buttonIndex, button);
    }
    
    /**
     * @param parent
     * @param toolkit
     * @param buttons2 
     */
    private void initialize(Composite parent, FormToolkit toolkit, int _buttons) {
        buttons = new HashMap<Integer, Button>();
        
        int[] buttonMasks = new int[] { BUTTON_ADD, BUTTON_EDIT, BUTTON_REMOVE, BUTTON_UP, BUTTON_DOWN, BUTTON_SCAN };
        int[] buttonIndices = new int[] { BUTTON_ADD_INDEX, BUTTON_EDIT_INDEX, BUTTON_REMOVE_INDEX, BUTTON_UP_INDEX,
                BUTTON_DOWN_INDEX, BUTTON_SCAN_INDEX };
        String[] buttonTitles = new String[] { MTJUIMessages.buttonBarBlock_button_add,
                MTJUIMessages.buttonBarBlock_button_edit, MTJUIMessages.buttonBarBlock_button_remove,
                MTJUIMessages.buttonBarBlock_button_up, MTJUIMessages.buttonBarBlock_button_down,
                MTJUIMessages.ButtonBarBlock_Scan };

        for (int i = 0; i < buttonMasks.length; i++) {
            if ((_buttons & buttonMasks[i]) != 0) {
                createButton(parent, toolkit, buttonIndices[i], buttonTitles[i]);
            }
        }
    }
    
    /**
     * @param parent
     * @param buttons2 
     */
    private void initialize(Composite parent, int _buttons) {
        initialize(parent, null, _buttons);
    }

	/**
	 * Sets all block child widgets enabled/disabled.
	 * 
	 * @param state true if enabled false otherwise.
	 */
	public void setEnabled(boolean state) {
		for (Button button : this.buttons.values()) {
			button.setEnabled(state);
		}
	}
}
