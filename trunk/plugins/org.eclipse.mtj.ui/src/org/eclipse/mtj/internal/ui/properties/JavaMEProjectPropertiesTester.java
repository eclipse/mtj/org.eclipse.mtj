/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.properties;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.build.CodeValidationBuilder;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.sdk.device.midp.Configuration;
import org.eclipse.mtj.internal.core.sdk.device.midp.Profile;
import org.osgi.framework.Version;

public class JavaMEProjectPropertiesTester extends PropertyTester {
    private static final String PROPERTY_CONFIGURE_PREVERIFIER = "configurePreverifier"; //$NON-NLS-1$
    private static final String PROPERTY_CONFIGURE_VALIDATION = "configureCodeValidation"; //$NON-NLS-1$
    private static final String PROPERTY_SUPPORTS_JMUNIT = "supportsJMUnit"; //$NON-NLS-1$
    private static final String PROPERTY_SUPPORTS_DEPENDENCIES = "supportsDependencies"; //$NON-NLS-1$

    public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
        if (receiver instanceof IProject) {
            if (PROPERTY_CONFIGURE_PREVERIFIER.equals(property)) {
                // No preverifier for projects with configuration version >= 1.8
                IProject project = (IProject) receiver;

                if (MidletSuiteFactory.isMidletSuiteProject(project)) {
                    IMidletSuiteProject midletSuiteProject = MidletSuiteFactory.getMidletSuiteProject(JavaCore.create((IProject) receiver));
                    Version configurationVersion;

                    try {
                        configurationVersion = midletSuiteProject.getApplicationDescriptor()
                                .getConfigurationSpecificationVersion();
                    } catch (CoreException ex) {
                        configurationVersion = null;
                    }

                    if (configurationVersion != null) {
                        return configurationVersion.compareTo(Configuration.CLDC_18.getVersion()) < 0;
                    }
                }

                return true;
            } else if (PROPERTY_CONFIGURE_VALIDATION.equals(property)) {
                // No code validation page if there is no code validators that supports the given project
                IProject project = (IProject) receiver;

                if (MidletSuiteFactory.isMidletSuiteProject(project)) {
                    return !CodeValidationBuilder.getCodeValidators(JavaCore.create(project)).isEmpty();
                }
            } else if (PROPERTY_SUPPORTS_JMUNIT.equals(property)) {
                IProject project = (IProject) receiver;

                if (MidletSuiteFactory.isMidletSuiteProject(project)) {
                    IMidletSuiteProject midletSuiteProject = MidletSuiteFactory.getMidletSuiteProject(JavaCore.create((IProject) receiver));
                    String meConfiguration = midletSuiteProject.getApplicationDescriptor().getMicroEditionConfiguration();
                    String meProfile = midletSuiteProject.getApplicationDescriptor().getMicroEditionProfile();

                    if ((Configuration.CLDC_10.toString().equals(meConfiguration) || Configuration.CLDC_11.toString()
                            .equals(meConfiguration))
                            && !Profile.MEEP_80.toString().equals(meProfile)
                            && !Profile.IMP_NG.toString().equals(meProfile)
                            && !Profile.IMP_10.toString().equals(meProfile)) {

                        return true;
                    }
                }
            } else if (PROPERTY_SUPPORTS_DEPENDENCIES.equals(property)) {
                IProject project = (IProject) receiver;

                if (MidletSuiteFactory.isMidletSuiteProject(project)) {
                    IMidletSuiteProject midletSuiteProject = MidletSuiteFactory.getMidletSuiteProject(JavaCore.create((IProject) receiver));
                    String meProfile = midletSuiteProject.getApplicationDescriptor().getMicroEditionProfile();

                    return Profile.MEEP_80.toString().equals(meProfile);
                }
            }
        }

        return false;
    }
}
