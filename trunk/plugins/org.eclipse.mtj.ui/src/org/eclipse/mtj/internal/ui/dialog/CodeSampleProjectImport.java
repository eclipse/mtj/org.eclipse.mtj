package org.eclipse.mtj.internal.ui.dialog;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.ui.wizards.ClassPathDetector;
import org.eclipse.jdt.ui.wizards.JavaCapabilityConfigurationPage;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory.MidletSuiteCreationRunnable;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteProject;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ArchiveUtils;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ProjectImporter;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ProjectImporterMessage;
import org.eclipse.mtj.internal.ui.wizards.importer.common.ProjectRecord;
import org.eclipse.mtj.internal.ui.wizards.importer.netbeans.NetBeansProjectImporter;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.IOverwriteQuery;
import org.eclipse.ui.wizards.datatransfer.FileSystemStructureProvider;
import org.eclipse.ui.wizards.datatransfer.ImportOperation;

/**
 * Handles project netbeans and eclipse project import to workspace.
 * 
 * @author Pranav Gothadiya (Nokia)
 * 
 */
@SuppressWarnings("restriction")
public class CodeSampleProjectImport {

	/**
	 * Imports the given path into the workspace as a project.
	 * 
	 * @param projectPath
	 * @throws CoreException
	 *             if operation fails catastrophically
	 */
	protected static void loadEclipseProjectToWorkSpace(IPath projectPath)
			throws CoreException {

		// Load the project description file
		final IProjectDescription description = ResourcesPlugin.getWorkspace()
				.loadProjectDescription(
						projectPath.append(IPath.SEPARATOR
								+ IProjectDescription.DESCRIPTION_FILE_NAME));

		final IProject project = ResourcesPlugin.getWorkspace().getRoot()
				.getProject(description.getName());

		// Only import the project if it doesn't appear to already exist. If
		// it looks like it exists, tell the user about it.
		if (project.exists()) {
			System.err.println("Build.commandLine.projectExists"
					+ project.getName());
		}

		IWorkspaceRunnable runnable = new IWorkspaceRunnable() {
			public void run(IProgressMonitor monitor) throws CoreException {
				project.create(description, monitor);
				project.open(IResource.NONE, monitor);
				project.build(IncrementalProjectBuilder.FULL_BUILD, monitor);
			}
		};

		ResourcesPlugin.getWorkspace().run(
				runnable,
				ResourcesPlugin.getWorkspace().getRuleFactory()
						.modifyRule(ResourcesPlugin.getWorkspace().getRoot()),
				IResource.NONE, null);
	}

	public static void importNetbeansProject(final IPath projectPath,
			IProgressMonitor monitor) throws CoreException {
		if (projectPath == null)
			return;

		IWorkspaceRunnable runnable = new IWorkspaceRunnable() {
			public void run(IProgressMonitor monitor) throws CoreException {
				ProjectImporter importer = new NetBeansProjectImporter();
				ProjectRecord[] foundProjects = importer
						.searchProjectsFromDirectory(new File(projectPath
								.toFile().getAbsolutePath()), monitor);

				System.out.println("foundProjects!! " + foundProjects.length);

				if (foundProjects.length <= 0) {
					throw new CoreException(new Status(IStatus.INFO,
							"org.eclipse.mtj.ui",
							"Project type not supported by MTJ."));
				}

				for (int i = 0; i < foundProjects.length; i++) {
					try {
						createExistingProject(foundProjects[i], importer,
								monitor);
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			}
		};

		ResourcesPlugin.getWorkspace().run(
				runnable,
				ResourcesPlugin.getWorkspace().getRuleFactory()
						.modifyRule(ResourcesPlugin.getWorkspace().getRoot()),
				IResource.NONE, null);

	}

	@SuppressWarnings({ "unchecked" })
	private static boolean createExistingProject(final ProjectRecord record,
			final ProjectImporter importer, final IProgressMonitor monitor)
			throws InvocationTargetException, InterruptedException {
		Display.getDefault().syncExec(new Runnable() {
			public void run() {
				final Shell shell = new Shell(Display.getDefault());
				IOverwriteQuery overwriteQuery = new IOverwriteQuery() {

					public String queryOverwrite(String pathString) {
						return YES;
					}
				};

				String projectName = record.getProjectName();
				final IWorkspace workspace = MTJCore.getWorkspace();
				final IProject project = workspace.getRoot().getProject(
						projectName);

				IProjectDescription newProjectDescription = workspace
						.newProjectDescription(projectName);

				try {
					monitor.beginTask(
							NLS.bind(
									ProjectImporterMessage.WizardProjectsImportPage_CreateProjectTask,
									record.getProjectName()), 100);
					JavaCapabilityConfigurationPage.createProject(project,
							newProjectDescription.getLocationURI(),
							new SubProgressMonitor(monitor, 20));

					if (record.getProvider() == null) { // import from file
														// system
						IPath locationPath = new Path(new File(record
								.getProjectRoot()).getAbsolutePath());
						File importSource = locationPath.toFile();
						// If it is not under the root, copy files
						if (!Platform.getLocation().isPrefixOf(locationPath)) {
							List<File> filesToImport = FileSystemStructureProvider.INSTANCE
									.getChildren(importSource);
							ImportOperation operation = new ImportOperation(
									project.getFullPath(), importSource,
									FileSystemStructureProvider.INSTANCE,
									overwriteQuery, filesToImport);
							operation.setContext(shell);
							operation.setOverwriteResources(true); // may need
																	// to
							// overwrite
							// .project, .classpath files
							operation.setCreateContainerStructure(false);
							operation.run(new SubProgressMonitor(monitor, 20));

							monitor.worked(50);
							importer.projectCreated(record, project,
									new SubProgressMonitor(monitor, 20));
						}
					} else { // import from archive
						Object root = ArchiveUtils.getChild(record
								.getProvider(), record.getProvider().getRoot(),
								record.getProjectRoot());
						List<File> fileSystemObjects = record.getProvider()
								.getChildren(root);

						// guess strip level
						int stripLevel = ArchiveUtils.guessStripLevel(record
								.getProjectRoot());
						record.getProvider().setStrip(stripLevel);
						ImportOperation operation = new ImportOperation(project
								.getFullPath(), record.getProvider().getRoot(),
								record.getProvider(), overwriteQuery,
								fileSystemObjects);
						operation.setContext(shell);
						operation.run(monitor);
						importer.projectCreated(record, project,
								new SubProgressMonitor(monitor, 20));
					}

					monitor.setTaskName(ProjectImporterMessage.WizardProjectsImportPage_ProcessingMessage);

					// Refreshes the project's folder in order for the
					// ClassPathDetector
					// to work properly
					project.refreshLocal(IResource.DEPTH_INFINITE, null);

					ClassPathDetector detector = new ClassPathDetector(project,
							null);
					JavaCapabilityConfigurationPage javaPropPage = new JavaCapabilityConfigurationPage();
					javaPropPage.init(JavaCore.create(project),
							detector.getOutputLocation(),
							detector.getClasspath(), false);
					javaPropPage.configureJavaProject(null);

					removeJ2SELibraries(javaPropPage.getJavaProject(), monitor);

					IDevice device = importer.getProjectDevice(record);

					MidletSuiteCreationRunnable builder = MidletSuiteFactory
							.getMidletSuiteCreationRunnable(project,
									javaPropPage.getJavaProject(),
									(IMIDPDevice) device, MidletSuiteProject
											.getDefaultJadFileName(project));
					builder.run(monitor);

					project.build(IncrementalProjectBuilder.FULL_BUILD, monitor);
					monitor.done();

				} catch (CoreException e) {
					try {
						project.delete(true, monitor);
					} catch (CoreException ignored) {
					}

				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		});

		return true;

	}

	private static void removeJ2SELibraries(IJavaProject javaProject,
			IProgressMonitor monitor) throws JavaModelException {
		IClasspathEntry[] entries = javaProject.getRawClasspath();
		ArrayList<IClasspathEntry> list = new ArrayList<IClasspathEntry>();

		for (int i = 0; i < entries.length; i++) {
			if (!isJ2SELibraryEntry(entries[i])) {
				list.add(entries[i]);
			}
		}

		entries = list.toArray(new IClasspathEntry[list.size()]);
		javaProject.setRawClasspath(entries, monitor);
	}

	private static boolean isJ2SELibraryEntry(IClasspathEntry entry) {
		boolean isJ2SEEntry = false;

		if (entry.getEntryKind() == IClasspathEntry.CPE_LIBRARY) {
			if (entry.getPath().lastSegment().equals("JRE_LIB")) { //$NON-NLS-1$
				isJ2SEEntry = true;
			}
		} else if (entry.getEntryKind() == IClasspathEntry.CPE_CONTAINER) {
			if (entry.getPath().lastSegment()
					.equals("org.eclipse.jdt.launching.JRE_CONTAINER")) { //$NON-NLS-1$
				isJ2SEEntry = true;
			}
		}
		return isJ2SEEntry;
	}

}
