/**
 * Copyright (c) 2009 Motorola.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Fernando Rocha (Motorola) - Initial implementation
 *     Rafael Amaral (Motorola)- Changing title group name
 */
package org.eclipse.mtj.internal.ui.wizards.projects;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.sdk.device.midp.Configuration;
import org.eclipse.mtj.internal.core.sdk.device.midp.PackagingModel;
import org.eclipse.mtj.internal.core.sdk.device.midp.Profile;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.wizards.dialogfields.*;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;

/**
 * @author Fernando Rocha
 * @since 1.0
 */
public class NewMidletProjectWizardPageProperties extends WizardPage {

    private final class OptionsGroup extends Observable implements
            IDialogFieldListener {

        private SelectionButtonDialogField enableJMUnit;
        private SelectionButtonDialogField enableLocalization;
        private SelectionButtonDialogField enablePreprocessing;
        private StringDialogField packageField;
        private StringDialogField propertiesFolder;

        public OptionsGroup() {
            enablePreprocessing = new SelectionButtonDialogField(SWT.CHECK);
            enablePreprocessing
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_optionsGroup_enablePreprocessing);
            enablePreprocessing.setDialogFieldListener(this);

            enableLocalization = new SelectionButtonDialogField(SWT.CHECK);
            enableLocalization
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_optionsGroup_enableLocalization);
            enableLocalization.setDialogFieldListener(this);

            propertiesFolder = new StringDialogField();
            propertiesFolder
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_optionsGroup_propertiesFolder);
            propertiesFolder.setDialogFieldListener(this);
            propertiesFolder.setTextWithoutUpdate("res");
            propertiesFolder.setEnabled(false);

            packageField = new StringDialogField();
            packageField
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_optionsGroup_package);
            packageField.setDialogFieldListener(this);

            enableJMUnit = new SelectionButtonDialogField(SWT.CHECK);
            enableJMUnit
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_optionsGroup_enableJMUnit);
            enableJMUnit.setDialogFieldListener(this);

            enablePreprocessing.setSelection(false);
            enableLocalization.setSelection(false);
            enableLocalization.attachDialogField(propertiesFolder);
            enableLocalization
                    .attachDialogFields(new DialogField[] { packageField });
        }

        public Control createControl(Composite parent) {
            final int numColumns = 2;

            final Group group = new Group(parent, SWT.NONE);
            group.setLayout(initGridLayout(new GridLayout(numColumns, false),
                    true));
            group
                    .setText(MTJUIMessages.NewMidletProjectWizardPageProperties_midlet_additional_support);

            enablePreprocessing.doFillIntoGrid(group, numColumns);
            enableLocalization.doFillIntoGrid(group, numColumns);
            propertiesFolder.doFillIntoGrid(group, numColumns);
            LayoutUtil.setHorizontalGrabbing(propertiesFolder
                    .getTextControl(null));
            packageField.doFillIntoGrid(group, numColumns);
            LayoutUtil.setHorizontalGrabbing(packageField.getTextControl(null));
            enableJMUnit.doFillIntoGrid(group, numColumns);

            return group;
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.wizards.dialogfields.IDialogFieldListener#dialogFieldChanged(org.eclipse.mtj.internal.ui.wizards.dialogfields.DialogField)
         */
        public void dialogFieldChanged(DialogField field) {
            fireEvent();
        }

        /**
         * @return
         */
        public String getPackage() {
            return packageField.getText();
        }

        /**
         * @return
         */
        public String getPropertiesFolder() {
            return propertiesFolder.getText();
        }

        public boolean isJMUnitEnable() {
            return enableJMUnit.isSelected();
        }

        public boolean isLocalizationEnable() {
            return enableLocalization.isSelected();
        }

        public boolean isPreprocessingEnable() {
            return enablePreprocessing.isSelected();
        }

        protected void fireEvent() {
            setChanged();
            notifyObservers();
        }

    }

    private final class PropertiesGroup extends Observable implements
            IDialogFieldListener, Observer {

        private Map<String, IAPI> configurations;
        private ComboDialogField packagingModel;
        private ComboDialogField meConfiguration;
        private ComboDialogField meProfile;
        private StringDialogField midletName;
        private StringDialogField midletVendor;
        private StringDialogField midletVersion;
        private Map<String, IAPI> profiles;
        
        private String[] configurationNames;
        private String[] meepSupportedConfigurationNames = {Configuration.CLDC_18.getName()};
        
        private String[] profileNames;
        private String[] libletSupportedProfileNames = {Profile.MEEP_80.getName()};

        public PropertiesGroup() {
            configurations = new HashMap<String, IAPI>();
            profiles = new HashMap<String, IAPI>();

            String[] packagingModelNames = new String[] {PackagingModel.MIDLET.getName(),
            		PackagingModel.LIBLET.getName()};
            packagingModel = new ComboDialogField(SWT.READ_ONLY);
            packagingModel
                    .setLabelText(MTJUIMessages.jadPropertiesEditorPage_PackagingModel_text);
            packagingModel.setDialogFieldListener(this);
            packagingModel.setItems(packagingModelNames);
            packagingModel.setTextWithoutUpdate(packagingModelNames[0]);
            
            midletName = new StringDialogField();
            midletName
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_midletName);
            midletName.setDialogFieldListener(this);

            midletVendor = new StringDialogField();
            midletVendor
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_midletVendor);
            midletVendor.setDialogFieldListener(this);

            midletVersion = new StringDialogField();
            midletVersion
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_midletVersion);
            midletVersion.setDialogFieldListener(this);

            IAPI[] meConfigurations = Configuration.values();
            configurationNames = new String[meConfigurations.length];
            String defaultConfiguration = "";
            for (int i = 0; i < meConfigurations.length; i++) {
                configurationNames[i] = meConfigurations[i].getName();
                if (meConfigurations[i].toString().equals(
                        MTJUIMessages.MeConfigurationVersion)) {
                    defaultConfiguration = meConfigurations[i].getName();
                }
                configurations.put(configurationNames[i], meConfigurations[i]);
            }

            meConfiguration = new ComboDialogField(SWT.READ_ONLY);
            meConfiguration
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_meConfiguration);
            meConfiguration.setDialogFieldListener(this);
            meConfiguration.setItems(configurationNames);
            meConfiguration.setTextWithoutUpdate(defaultConfiguration);

            IAPI[] meProfiles = Profile.values();
            profileNames = new String[meProfiles.length];
            String defaultProfile = "";
            for (int i = 0; i < meProfiles.length; i++) {
                profileNames[i] = meProfiles[i].getName();
                if (meProfiles[i].toString().equals(
                        MTJUIMessages.MeProfileVersion)) {
                    defaultProfile = meProfiles[i].getName();
                }

                profiles.put(profileNames[i], meProfiles[i]);
            }

            meProfile = new ComboDialogField(SWT.READ_ONLY);
            meProfile
                    .setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_meProfile);
            meProfile.setDialogFieldListener(this);
            meProfile.setItems(profileNames);
            meProfile.setTextWithoutUpdate(defaultProfile);
        }

        /**
         * @param parent
         * @return
         */
        public Control createControl(Composite parent) {
            final int numColumns = 2;

            final Group group = new Group(parent, SWT.NONE);
            group.setLayout(initGridLayout(new GridLayout(numColumns, false),
                    true));
            group
                    .setText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_contents);

            packagingModel.doFillIntoGrid(group, numColumns);
            LayoutUtil.setHorizontalGrabbing(packagingModel.getComboControl(null));
            midletName.doFillIntoGrid(group, numColumns);
            LayoutUtil.setHorizontalGrabbing(midletName.getTextControl(null));
            midletVendor.doFillIntoGrid(group, numColumns);
            LayoutUtil.setHorizontalGrabbing(midletVendor.getTextControl(null));
            midletVersion.doFillIntoGrid(group, numColumns);
            LayoutUtil
                    .setHorizontalGrabbing(midletVersion.getTextControl(null));
            meConfiguration.doFillIntoGrid(group, numColumns);
            LayoutUtil.setHorizontalGrabbing(meConfiguration
                    .getComboControl(null));
            meProfile.doFillIntoGrid(group, numColumns);
            LayoutUtil.setHorizontalGrabbing(meProfile.getComboControl(null));

            meConfiguration.getComboControl(null).addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    updateComboFields();
                }
            });
            
            meProfile.getComboControl(null).addSelectionListener(new SelectionAdapter() {
            	public void widgetSelected(SelectionEvent e) {
            		updateComboFields();
            	}
            });
            packagingModel.getComboControl(null).addSelectionListener(new SelectionAdapter() {
            	public void widgetSelected(SelectionEvent e) {
                    if (packagingModel.getText().equals(PackagingModel.MIDLET.getName())) {
                    	midletName.setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_midletName);
                    	midletVendor.setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_midletVendor);
                    	midletVersion.setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_midletVersion);
                
                    } else if (packagingModel.getText().equals(PackagingModel.LIBLET.getName())) {
                    	midletName.setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_libletName);
                    	midletVendor.setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_libletVendor);
                    	midletVersion.setLabelText(MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_libletVersion);
                    }
                    updateComboFields();
            	}
            });
            
            initValues();
            updateComboFields();

            return group;
        }

        private void updateComboFields() {
        	Display.getDefault().asyncExec(new Runnable() {
        		public void run() {
        	        if (packagingModel.getText().equals(PackagingModel.MIDLET.getName())) {
        	        	String selectedItem = meProfile.getText();
        		        meProfile.setItems(profileNames);
        		        meProfile.selectItem(selectedItem);
        	        } else if (packagingModel.getText().equals(PackagingModel.LIBLET.getName())) {
        		        meProfile.setItems(libletSupportedProfileNames);
        		        meProfile.selectItem(0);
        	        }
        	        
        	        if (meProfile.getText().equals(Profile.MEEP_80.getName())) {
        		        meConfiguration.setItems(meepSupportedConfigurationNames);
        		        meConfiguration.selectItem(0);
        	        } else {
        	        	String selectedItem = meConfiguration.getText();
        		        meConfiguration.setItems(configurationNames);
        		        meConfiguration.selectItem(selectedItem);
        	        }
        	        
        	        String meProfileSelected = meProfile.getText();
        	        String meConfigurationSelected = meConfiguration.getText();
        	        
        	        // No JMUnit support for headless profiles (IMP, IMP-NG, MEEP) and CLDC with version that not supported by JMUnit library 
                    if (!Configuration.CLDC_10.getName().equals(meConfigurationSelected)
                            && !Configuration.CLDC_11.getName().equals(meConfigurationSelected)
                            || Profile.MEEP_80.getName().equals(meProfileSelected)
                            || Profile.IMP_NG.getName().equals(meProfileSelected)
                            || Profile.IMP_10.getName().equals(meProfileSelected)) {
        	            optionsGroup.enableJMUnit.setSelection(false);
        	            optionsGroup.enableJMUnit.setEnabled(false);
        	        } else {
        	            optionsGroup.enableJMUnit.setEnabled(true);
        	        }
        		}
        	});
        }
        
        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.wizards.dialogfields.IDialogFieldListener#dialogFieldChanged(org.eclipse.mtj.internal.ui.wizards.dialogfields.DialogField)
         */
        public void dialogFieldChanged(DialogField field) {
            fireEvent();
        }

        public PackagingModel getPackagingModel() {
        	if (packagingModel.getText().equals(PackagingModel.LIBLET.getName())) {
        		return PackagingModel.LIBLET;
        	}
        	
        	return PackagingModel.MIDLET;
        }
        
        public String getMeConfiguration() {
            return configurations.get(meConfiguration.getText()).toString();
        }

        public String getMeProfile() {
            return profiles.get(meProfile.getText()).toString();
        }

        public String getMIDletName() {
            return midletName.getText();
        }

        public String getMIDletVendor() {
            return midletVendor.getText();
        }

        public String getMIDletVersion() {
            return midletVersion.getText();
        }

        /* (non-Javadoc)
         * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
         */
        public void update(Observable o, Object arg) {
            String midlet = midletName.getText();
            String projectName = pageOne.getProjectName();
            if ( projectName.length() > 0 && midlet.startsWith(projectName.substring(0,
                    projectName.length() - 1))
                    || midlet.equals(MTJUIMessages.MidletDefaultNameEnd)) {
                midletName.setText(projectName
                        + MTJUIMessages.MidletDefaultNameEnd);
            }
        }

        private <A extends IMIDPAPI> A findActualRequiredAPI(A[] values, IMIDPAPI requiredAPI) {
            if (requiredAPI != null) {
                for (A candidate : values) {
                    if (requiredAPI.getType() == candidate.getType()
                            && requiredAPI.getIdentifier().equals(candidate.getIdentifier())
                            && requiredAPI.getVersion().equals(candidate.getVersion())) {
                        return candidate;
                    }
                }
            }

            return null;
        }

        private void updateProfileAndConfiguration() {
            IDevice activeDevice = pageOne.getSelectedDevice();

            if (activeDevice instanceof IMIDPDevice) {
                IMIDPDevice activeMEDevice = (IMIDPDevice) activeDevice;
                Configuration deviceConfiguration = findActualRequiredAPI(Configuration.values(),
                        activeMEDevice.getCLDCAPI());
                Profile deviceProfile = findActualRequiredAPI(Profile.values(), activeMEDevice.getMIDPAPI());

                if (deviceConfiguration != null) {
                    meConfiguration.setTextWithoutUpdate(deviceConfiguration.getName());
                }

                if (deviceProfile != null) {
                    meProfile.setTextWithoutUpdate(deviceProfile.getName());
                }
            }
        }
        
        
        private void initValues() {
            midletName.setText(pageOne.getProjectName()
                    + MTJUIMessages.MidletDefaultNameEnd);
            midletVendor.setText(MTJUIMessages.MidletDefaultVendor);
            midletVersion.setText(MTJUIMessages.MidletInitialVersion_1_0_0);
            
            pageOne.getConfigurationGroup().addObserver(new Observer() {
                public void update(Observable o, Object arg) {
                    updateProfileAndConfiguration();
                }
            });
            
            updateProfileAndConfiguration();
        }

        protected void fireEvent() {
            setChanged();
            notifyObservers();
        }

    }

    private final class Validator implements Observer {

        private Pattern correctPattern = Pattern
                .compile("([a-z_]\\w*)(\\.[a-z_]\\w*)*");
        private Pattern packagePattern = Pattern
                .compile("([a-zA-Z_]\\w*)(\\.[a-zA-Z_]\\w*)*");

        public void update(Observable o, Object arg) {
            if (getMIDletName().equals("")) {
                setErrorMessage(NLS
                        .bind(
                                MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_fieldEmpty,
                                MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_midletName));
                setMessage(null);
                setPageComplete(false);
                return;
            }
            if (getMIDletVendor().equals("")) {
                setErrorMessage(NLS
                        .bind(
                                MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_fieldEmpty,
                                MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_midletVendor));
                setMessage(null);
                setPageComplete(false);
                return;
            }
            if (getMIDletVersion().equals("")) {
                setErrorMessage(NLS
                        .bind(
                                MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_fieldEmpty,
                                MTJUIMessages.NewMidletProjectWizardPageTwo_propertiesGroup_midletVersion));
                setMessage(null);
                setPageComplete(false);
                return;
            }

            if (isLocalizationEnabled()) {
                String packageName = getPackageName();
                if (packageName.equals("")) {
                    setErrorMessage(null);
                    setMessage(
                            MTJUIMessages.NewMidletProjectWizardPageTwo_optionsGroup_packageEmpty,
                            IMessageProvider.WARNING);
                    setPageComplete(true);
                    return;
                }
                Matcher packageMatcher = packagePattern.matcher(packageName);
                if (packageMatcher.matches()) {
                    Matcher correctMatcher = correctPattern
                            .matcher(packageName);
                    // Correct package
                    if (correctMatcher.matches()) {
                        setErrorMessage(null);
                        setMessage(null);
                        setPageComplete(true);
                        return;
                    }
                    // Upper case package
                    else {
                        setErrorMessage(null);
                        setMessage(
                                MTJUIMessages.NewMidletProjectWizardPageTwo_optionsGroup_packageUpperCase,
                                IMessageProvider.WARNING);
                        setPageComplete(true);
                        return;
                    }
                }
                // Error package
                else {
                    setMessage(null);
                    setErrorMessage(NLS
                            .bind(
                                    MTJUIMessages.NewMidletProjectWizardPageTwo_optionsGroup_packageError,
                                    packageName));
                    setPageComplete(false);
                    return;
                }
            }
            setErrorMessage(null);
            setMessage(null);
            setPageComplete(true);
        }
    }

    private static final String PAGE_NAME = "NewJavaProjectWizardPageTwo"; //$NON-NLS-1$
    private OptionsGroup optionsGroup;
    private NewMidletProjectWizardPageOne pageOne;
    private PropertiesGroup propertiesGroup;
    private Validator validator;

    public NewMidletProjectWizardPageProperties(
            NewMidletProjectWizardPageOne firstPage) {
        super(PAGE_NAME);
        setTitle(MTJUIMessages.NewMidletProjectWizardPageTwo_title);
        setDescription(MTJUIMessages.NewMidletProjectWizardPageTwo_description);

        pageOne = firstPage;
        validator = new Validator();

        propertiesGroup = new PropertiesGroup();
        optionsGroup = new OptionsGroup();

        pageOne.getProjectNameGroup().addObserver(propertiesGroup);
        propertiesGroup.addObserver(validator);
        optionsGroup.addObserver(validator);
    }

    public void createControl(Composite parent) {
        initializeDialogUnits(parent);

        ScrolledComposite scrolledComposite = new ScrolledComposite(parent,
                SWT.V_SCROLL | SWT.H_SCROLL);
        scrolledComposite.setExpandHorizontal(true);
        scrolledComposite.setExpandVertical(true);
        scrolledComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
                true));

        final Composite composite = new Composite(scrolledComposite, SWT.NONE);
        composite.setFont(parent.getFont());
        composite.setLayout(initGridLayout(new GridLayout(), true));

        // create UI elements
        Control nameControl = createPropertiesControl(composite);
        nameControl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        Control optionsControl = createOptionsControl(composite);
        optionsControl.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

        scrolledComposite.setContent(composite);
        scrolledComposite.setMinSize(composite.computeSize(SWT.DEFAULT,
                SWT.DEFAULT));
        setControl(scrolledComposite);
    }

    public PackagingModel getPackagingModel() {
    	return propertiesGroup.getPackagingModel();
    }
    
    public String getMeConfiguration() {
        return propertiesGroup.getMeConfiguration();
    }

    public String getMeProfile() {
        return propertiesGroup.getMeProfile();
    }

    public String getMIDletName() {
        return propertiesGroup.getMIDletName();
    }

    public String getMIDletVendor() {
        return propertiesGroup.getMIDletVendor();
    }

    public String getMIDletVersion() {
        return propertiesGroup.getMIDletVersion();
    }

    public String getPackageName() {
        return optionsGroup.getPackage();
    }

    public String getPropertiesFolderName() {
        return optionsGroup.getPropertiesFolder();
    }

    public boolean isJMUnitEnabled() {
        return optionsGroup.isJMUnitEnable();
    }

    public boolean isLocalizationEnabled() {
        return optionsGroup.isLocalizationEnable();
    }

    public boolean isPreprocessingEnabled() {
        return optionsGroup.isPreprocessingEnable();
    }

    private Control createOptionsControl(Composite parent) {
        return optionsGroup.createControl(parent);
    }

    private GridLayout initGridLayout(GridLayout layout, boolean margins) {
        layout.horizontalSpacing = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
        layout.verticalSpacing = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_SPACING);
        if (margins) {
            layout.marginWidth = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_MARGIN);
            layout.marginHeight = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_MARGIN);
        } else {
            layout.marginWidth = 0;
            layout.marginHeight = 0;
        }
        return layout;
    }

    protected Control createPropertiesControl(Composite parent) {
        return propertiesGroup.createControl(parent);
    }

}
