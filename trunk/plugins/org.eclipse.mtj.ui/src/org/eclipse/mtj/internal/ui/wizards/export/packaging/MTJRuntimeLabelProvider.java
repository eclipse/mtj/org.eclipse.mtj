/*
* Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
* All rights reserved.
* This component and the accompanying materials are made available
* under the terms of "Eclipse Public License v1.0"
* which accompanies this distribution, and is available
* at the URL "http://www.eclipse.org/legal/epl-v10.html".
*
* Initial Contributors:
* 		Nokia Corporation - initial contribution.
*
*/
package org.eclipse.mtj.internal.ui.wizards.export.packaging;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.swt.graphics.Image;

public class MTJRuntimeLabelProvider extends LabelProvider {

	@Override
	public Image getImage(Object element) {
		return null;
	}

	@Override
	public String getText(Object element) {
		String text = null;
		if (element instanceof MTJRuntime) {
			MTJRuntime runtime = (MTJRuntime) element;
			text = runtime.getName();
		}
		return text;
	}
}
