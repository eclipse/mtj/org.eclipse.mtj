/**
 * Copyright (c) 2004,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - refactored class name from 
 *                                ConvertToMidletSuiteAction to 
 *                                ConvertToMidletProjectAction.
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874] 
 *     Gustavo de Paula (Motorola)  - MTJ project converter refactoring
 *     David Marques (Motorola) - Fixing EclipseME project conversion. 
 */
package org.eclipse.mtj.internal.ui.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.midp.J2MENature;
import org.eclipse.mtj.core.project.midp.ProjectConvertionException;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.project.midp.MTJProjectConverter;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.dialog.DeviceSelectDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

/**
 * An action delegate implementation for converting a Java Project to a Java ME
 * MIDlet Project.
 * 
 * @author Craig Setera
 */
public class ConvertToMidletProjectAction extends AbstractHandler {

    /**
     * Construct a new convert to MIDlet project action delegate.
     */
    public ConvertToMidletProjectAction() {
        super();
    }

    public Object execute(ExecutionEvent event) throws ExecutionException {
        ISelection selection = HandlerUtil.getCurrentSelection(event);

        if (selection instanceof IStructuredSelection && !selection.isEmpty()) {
            Shell shell = HandlerUtil.getActiveShell(event);
            
            try {
                IMIDPDevice device = getDevice(shell);
                if (device == null) {
                    MessageDialog.openError(shell, MTJUIMessages.ConvertToMidletProjectAction_error_no_device_title,
                            MTJUIMessages.ConvertToMidletProjectAction_error_no_device_message);
                } else {
                    convertSelectedProjects(device, (IStructuredSelection) selection, shell);
                }
            } catch (PersistenceException e) {
                handleException(e, shell);
            }
        }

        return null;
    }

    /**
     * Convert the selected projects using the specified platform definition.
     * 
     * @param device
     */
    private void convertSelectedProjects(IMIDPDevice device, IStructuredSelection selection, Shell shell) {
        // Setup the progress monitoring
        ProgressMonitorDialog dialog = new ProgressMonitorDialog(shell);

        try {
            // Run as an atomic Workspace operation
            dialog.open();
            IProgressMonitor monitor = dialog.getProgressMonitor();
            MTJCore.getWorkspace().run(getRunnable(device, selection), monitor);
        } catch (CoreException e) {
            handleException(e, shell);
        }
        finally{
        	 dialog.close();
        }
    }

    /**
     * Get the device to be used when converting the projects.
     * 
     * @return
     * @throws PersistenceException
     */
    private IMIDPDevice getDevice(Shell shell) throws PersistenceException {
        IMIDPDevice device = null;

        // Check that there are platform definitions to choose
        // from...
        if (MTJCore.getDeviceRegistry().getDeviceCount() > 0) {
            // Prompt the user
            DeviceSelectDialog dialog = new DeviceSelectDialog(shell);

            if (dialog.open() == Window.OK) {
                device = (IMIDPDevice) dialog.getSelectedDevice();
            }
        }

        return device;
    }

    /**
     * Return the workspace runnable that will make all of the changes to
     * convert the projects to Java ME MIDlet suites.
     * 
     * @param def
     * @return
     */
    private IWorkspaceRunnable getRunnable(final IMIDPDevice def, final IStructuredSelection selection) {
        return new IWorkspaceRunnable() {
            public void run(IProgressMonitor monitor) throws CoreException {
                // Collect the projects to be converted
                monitor
                        .beginTask(
                                MTJUIMessages.ConvertToMidletProjectAction_convert_taskname,
                                selection.size());

                Iterator<?> iter = selection.iterator();
                while (iter.hasNext()) {
                    IJavaProject javaProject = AbstractJavaProjectAction.getJavaProject(iter.next());
                    if (javaProject != null) {
                        monitor
                                .setTaskName(MTJUIMessages
                                        .bind(
                                                MTJUIMessages.ConvertToMidletProjectAction_convert_taskname2,
                                                javaProject.getElementName()));
                        boolean hasMtjNature = false;
                        boolean hasEclipseMeNature = false;
                        IProject project = javaProject.getProject();
                        try {
                            hasMtjNature = J2MENature.hasMtjCoreNature(project);
                        } catch (CoreException e) { /* Munch */
                        }

                        try {
                            hasEclipseMeNature = project
                                    .hasNature(IMTJCoreConstants.ECLIPSE_ME_NATURE);
                        } catch (CoreException e) { /* Munch */
                        }

                        if (!hasMtjNature) {
                            if (hasEclipseMeNature) {
                                try {
                                    // migrate EclipseME project to MTJ project
                                    MTJProjectConverter.getInstance()
                                            .convertEclipseMeProject(
                                                    project, def,
                                                    new SubProgressMonitor(
                                                            monitor, 50));
                                } catch (ProjectConvertionException e) {
                                    MTJStatusHandler.throwCoreException(IStatus.ERROR,
                                            -999, e.getMessage());
                                }
                            } else {
                                try {
                                    MTJProjectConverter.getInstance()
                                            .convertJavaProject(javaProject,
                                                    (IMIDPDevice) def, monitor);
                                } catch (InvocationTargetException e1) {
                                    MTJStatusHandler.throwCoreException(IStatus.ERROR,
                                            -999, e1.getTargetException());
                                } catch (InterruptedException e1) {
                                    MTJStatusHandler.throwCoreException(IStatus.ERROR,
                                            -999, e1);
                                }
                            }
                        }
                    }
                    monitor.worked(1);
                }
                monitor.done();
            }
        };
    }

    /**
     * Handle an exception during the conversion process.
     * 
     * @param e
     */
    private void handleException(Throwable e, Shell shell) {
        MTJLogger.log(IStatus.ERROR, e);
        MessageDialog.openError(shell, MTJUIMessages.ConvertToMidletProjectAction_handleException_title, e.toString());
    }
}
