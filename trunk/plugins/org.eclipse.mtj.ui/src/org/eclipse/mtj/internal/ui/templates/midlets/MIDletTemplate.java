/**
 * Copyright (c) 2009,2012 Motorola and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 */
package org.eclipse.mtj.internal.ui.templates.midlets;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IContributor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.mtj.ui.templates.AbstractTemplateWizardPage;
import org.osgi.framework.Bundle;

/**
 * MIDletTemplateObject Class wraps information about the template.
 * 
 * @author David Marques
 * @since 1.0
 */
public class MIDletTemplate {

    private IConfigurationElement  extension;

    /**
     * Creates a MIDletTemplateObject from an IConfigurationElement.
     * 
     * @param extension - template extension.
     */
    public MIDletTemplate(IConfigurationElement _extension) {
       this.extension = _extension;
    }

    /**
     * Gets the template id.
     * 
     * @return the id
     */
    public String getId() {
        return this.extension.getAttribute("id");
    }
    
    /**
     * Gets the template name.
     * 
     * @return the name
     */
    public String getName() {
        return this.extension.getAttribute("name");
    }

    /**
     * Gets the template overview..
     * 
     * @return the overview
     */
    public String getOverview() {
        return this.extension.getAttribute("overview");
    }
    
    /**
     * Gets the template description.
     * 
     * @return the description
     */
    public String getDescription() {
        return this.extension.getAttribute("description");
    }

    /**
     * Gets the template page instance.
     * 
     * @return the page instance.
     * @throws CoreException  - if an instance of the page 
     * could not be created for any reason.
     */
    public AbstractTemplateWizardPage getPage() throws CoreException {
        return (AbstractTemplateWizardPage)this.extension.createExecutableExtension("page");
    }

    /**
     * @return the permissions
     */
    public String[] getPermissions() {
        String[] permissions = {};
        String perms = extension.getAttribute("permissions");
        if (perms != null && perms.trim().length() > 0x00) {
            permissions = perms.split(",");
        }
        return permissions;
    }
    
    /**
     * Gets the bundle contributing this template.
     * 
     * @return template's bundle.
     */
    public Bundle getBundle() {
        IContributor contributor = this.extension.getContributor();
        return Platform.getBundle(contributor.getName());
    }
    
}
