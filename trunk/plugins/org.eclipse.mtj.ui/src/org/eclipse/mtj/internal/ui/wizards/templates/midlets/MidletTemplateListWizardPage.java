/**
 * Copyright (c) 2009, 2012 Motorola and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version.
 *     Gorkem Ercan (Nokia)     - Merge New MIDlet wizard with New MIDlet from 
 *                                template wizard
 *     Gorkem Ercan (Nokia)     - Change templates details view
 *     David Arago (Motorola)   - Disabling template usage on page creation.
 */
package org.eclipse.mtj.internal.ui.wizards.templates.midlets;

import java.util.Collection;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.ui.wizards.NewTypeWizardPage;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.templates.midlets.MIDletTemplate;
import org.eclipse.mtj.internal.ui.templates.midlets.MIDletTemplateRegistry;
import org.eclipse.mtj.internal.ui.wizards.midlet.NewMidletWizard;
import org.eclipse.mtj.internal.ui.wizards.templates.TemplateWizardPageDelegate;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledFormText;

/**
 * MidletTemplateListWizardPage class is the wizard page for MIDlet templates
 * selection and description.
 * 
 * @author David Marques
 * @since 1.0
 */
public class MidletTemplateListWizardPage extends NewTypeWizardPage {

    public static final String MIDLET_TEMPLATE_WIZARD_PAGE_1 = "MidletTemplateListWizardPage"; //$NON-NLS-1$
    private ScrolledFormText descriptionText;
    private TableViewer viewer;
    protected MIDletTemplate template;
	private Button disable;

    /**
     * Creates the page.
     */
    public MidletTemplateListWizardPage() {
        super(true, MIDLET_TEMPLATE_WIZARD_PAGE_1);

        this.setTitle(MTJUIMessages.MidletTemplateWizardPage1_Page1Title);
        this
                .setDescription(MTJUIMessages.MidletTemplateWizardPage1_Page1Description);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.wizard.WizardPage#canFlipToNextPage()
     */
    @Override
    public boolean canFlipToNextPage() {
        return !this.viewer.getSelection().isEmpty();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        Composite composite = new Composite(parent,SWT.NONE);
        composite.setLayout(new GridLayout());
        composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        this.createTemplatesList(composite);
        this.createDetails(composite);
        this.setControl(composite);
    }

    /**
     * Creates the details for the template page.
     * 
     * @param composite parent composite.
     */
    private void createDetails(final Composite composite) {
        Label label = new Label(composite, SWT.NONE);
        label.setText(MTJUIMessages.MidletTemplateWizardPage1_DescriptionLabel);
        label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final FormToolkit toolkit = new FormToolkit(composite.getDisplay());
		int borderStyle = toolkit.getBorderStyle() == SWT.BORDER ? SWT.NULL : SWT.BORDER;
		Composite container = new Composite(composite, borderStyle);
		FillLayout flayout = new FillLayout();
		flayout.marginWidth = 1;
		flayout.marginHeight = 1;
		container.setLayout(flayout);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		descriptionText = new ScrolledFormText(container, SWT.V_SCROLL | SWT.H_SCROLL, false);
		if (borderStyle == SWT.NULL) {
			descriptionText.setData(FormToolkit.KEY_DRAW_BORDER, FormToolkit.TREE_BORDER);
			toolkit.paintBordersFor(container);
		}
		FormText ftext = toolkit.createFormText(descriptionText, false);
		descriptionText.setFormText(ftext);
		descriptionText.setExpandHorizontal(true);
		descriptionText.setExpandVertical(true);
		descriptionText.setBackground(toolkit.getColors().getBackground());
		descriptionText.setForeground(toolkit.getColors().getForeground());
		ftext.marginWidth = 2;
		ftext.marginHeight = 2;
		ftext.setHyperlinkSettings(toolkit.getHyperlinkGroup());
		descriptionText.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				if (toolkit != null) {
					toolkit.dispose();
				}
			}
		});  	
        descriptionText.setEnabled(false);
        // TODO: Implement permissions after refactoring code for signing.
        // Button addPermisssionsToDescriptor = new Button(composite,
        // SWT.CHECK);
        // addPermisssionsToDescriptor.setText("Add Permissions to Application Descriptor.");
        // data = new GridData(GridData.FILL_HORIZONTAL);
        // permissionsList.setLayoutData(data);
        // addPermisssionsToDescriptor.setLayoutData(data);
    }

    /**
     * Creates the template list for the template page.
     * 
     * @param composite parent composite.
     */
    private void createTemplatesList(final Composite composite) {
        GridData data = null;

        disable = new Button(composite, SWT.CHECK);
        disable.setText("Create MIDlet using a template.");
        disable.setSelection(false);
        
        disable.addSelectionListener(new SelectionListener(){
            public void widgetDefaultSelected(SelectionEvent e) {}

            public void widgetSelected(SelectionEvent e) {
                boolean enabled = disable.getSelection();
                viewer.getControl().setEnabled(enabled);
                viewer.setSelection(null);
                
                descriptionText.setText(Utils.EMPTY_STRING);
                descriptionText.setEnabled(enabled);
                getWizard().getContainer().updateButtons();
            }
        });
        
        Label label = new Label(composite, SWT.NONE);
        label
                .setText(MTJUIMessages.MidletTemplateWizardPage1_AvailableTemplates);
        data = new GridData(GridData.FILL_HORIZONTAL);
        label.setLayoutData(data);

        ScrolledComposite listScroll = new ScrolledComposite(composite,
                SWT.V_SCROLL | SWT.BORDER);
        listScroll.setExpandHorizontal(true);
        listScroll.setExpandVertical(true);
        data = new GridData(GridData.FILL_HORIZONTAL);
        data.heightHint = 100;
        listScroll.setLayoutData(data);
        listScroll.setLayout(new GridLayout(0x01, false));

        Table table = new Table(listScroll, SWT.SINGLE);
        data = new GridData(GridData.FILL_HORIZONTAL);
        table.setLayoutData(data);
        listScroll.setContent(table);
        viewer = new TableViewer(table);
        
        viewer.setLabelProvider(new ITableLabelProvider() {

            public void addListener(ILabelProviderListener listener) {
            }

            public void dispose() {
            }

            public Image getColumnImage(Object element, int columnIndex) {
                return MTJUIPluginImages.DESC_TEMPLATE_OBJ.createImage();
            }

            public String getColumnText(Object element, int columnIndex) {
                if (element instanceof MIDletTemplate) {
                    return ((MIDletTemplate) element).getName();
                }
                return null;
            }

            public boolean isLabelProperty(Object element, String property) {
                return false;
            }

            public void removeListener(ILabelProviderListener listener) {
            }

        });
        viewer.setContentProvider(new IStructuredContentProvider() {

            public void dispose() {
            }

            public Object[] getElements(Object inputElement) {
                Collection<MIDletTemplate> templates = null;
                MIDletTemplateRegistry registry = null;
                if (!(inputElement instanceof MIDletTemplateRegistry)) {
                    return new Object[0x00];
                }
                registry = (MIDletTemplateRegistry) inputElement;
                templates = registry.getTemplates();
                return templates.toArray(new MIDletTemplate[templates
                        .size()]);
            }

            public void inputChanged(Viewer viewer, Object oldInput,
                    Object newInput) {
            }
        });
        viewer.setInput(MIDletTemplateRegistry.getInstance());
        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                template = getCurrentTemplate(viewer);
                if (template == null) {
                    return;
                }
                updateDetails(template);

                final IWizardPage page = getWizard().getPage(NewMidletWizard.TemplateCustomPage);
                Display.getCurrent().asyncExec(new Runnable() {
                    public void run() {
                        try {
                            TemplateWizardPageDelegate page2 = (TemplateWizardPageDelegate) page;
                            page2.setTitle(template.getName());
                            page2.setDescription(template.getOverview());
                            page2.setPage(template.getPage());
                            page2.getWizard().getContainer().updateButtons();
                        } catch (CoreException e) {
                            MessageDialog
                                    .openError(
                                            getShell(),
                                            MTJUIMessages.MidletTemplateWizardPage1_UnableToBuildUi,
                                            e.getMessage());
                        }
                    }
                });
            }
        });
        viewer.getControl().setEnabled(false);
    }

    /**
     * Gets the current selected template.
     * 
     * @return template object.
     */
    public MIDletTemplate getSelectedTemplate() {
        return this.getCurrentTemplate(this.viewer);
    }
    
    /**
     * Gets the current selected template.
     * 
     * @param viewer table viewer.
     * @return the template's MIDletTemplateObject.
     */
    private MIDletTemplate getCurrentTemplate(TableViewer viewer) {
        IStructuredSelection structuredSelection = null;

        ISelection selection = viewer.getSelection();
        if (selection.isEmpty() || !(selection instanceof IStructuredSelection)) {
            return null;
        }
        structuredSelection = (IStructuredSelection) selection;

        return (MIDletTemplate) structuredSelection.getFirstElement();
    }

    /**
     * Updates all details based on the new current template.
     * 
     * @param template current template.
     */
	private void updateDetails(MIDletTemplate template) {
		if( template==null )return;
		StringBuffer buffer = new StringBuffer();
		buffer.append("<p>"); //$NON-NLS-1$
		buffer.append(template.getDescription());
		buffer.append("</p>"); //$NON-NLS-1$
		String[] permisions = template.getPermissions();
		buffer.append("<p indent=\'10\'><b>");//$NON-NLS-1$
		buffer.append(MTJUIMessages.MidletTemplateWizardPage1_PermissionsLabel);
		buffer.append("</b></p>"); //$NON-NLS-1$
		if(permisions == null || permisions.length<1){
			buffer.append("<li bindent=\'10\'>"); //$NON-NLS-1$
			buffer.append(MTJUIMessages.MidletTemplateWizardPage1_PermissionsNone);
			buffer.append("</li>"); //$NON-NLS-1$
		}else{
			for (String permission : permisions) {
				buffer.append("<li bindent=\'10\'>"); //$NON-NLS-1$
				buffer.append(permission);
				buffer.append("</li>"); //$NON-NLS-1$
			}
		}
		descriptionText.setText(buffer.toString());
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage#isPageComplete()
	 */
	public boolean isPageComplete() {
		boolean result = false;
		if (this.disable.getSelection()) {
			result = !this.viewer.getSelection().isEmpty();
		} else {
			result = true;
		}
		return result;
	}
}
