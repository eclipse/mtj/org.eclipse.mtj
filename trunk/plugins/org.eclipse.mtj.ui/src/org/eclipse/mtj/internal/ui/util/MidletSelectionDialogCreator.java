/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Hugo Raniere (Motorola)  - Fixed issue of not showing MIDlets in 
 *                                Eclipse 3.3+
 *     Hugo Raniere (Motorola)  - Allowing the creation of MidletSelection
 *                                Dialog with a custom message
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]                      
 */
package org.eclipse.mtj.internal.ui.util;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.ui.IJavaElementSearchConstants;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.mtj.internal.core.util.MidletSearchScope;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.SelectionDialog;

/**
 * A simple helper class for creating a Type selection dialog that allows for
 * selection of MIDlet subclasses.
 * 
 * @author Craig Setera
 */
public class MidletSelectionDialogCreator {

    /**
     * Create a new MIDlet selection dialog.
     * 
     * @param shell parent the parent shell of the dialog to be created
     * @param context the runnable context used to show progress when the dialog
     *            is being populated
     * @param javaProject the project that contains the MIDlets to be included
     * @param multipleSelect true if multiple selection is allowed
     * @return The dialog created
     * @throws JavaModelException if the selection dialog could not be opened
     */
    public static SelectionDialog createMidletSelectionDialog(Shell shell,
            IRunnableContext context, IJavaProject javaProject,
            boolean multipleSelect) throws JavaModelException {
        return createMidletSelectionDialog(
                shell,
                context,
                javaProject,
                multipleSelect,
                MTJUIMessages.MidletSelectionDialogCreator_createMidletSelectionDialog_message);
    }

    /**
     * Create a new MIDlet selection dialog.
     * 
     * @param shell parent the parent shell of the dialog to be created
     * @param context the runnable context used to show progress when the dialog
     *            is being populated
     * @param javaProject the project that contains the MIDlets to be included
     * @param multipleSelect true if multiple selection is allowed
     * @param message a custom message to show in the dialog
     * @return The dialog created
     * @throws JavaModelException if the selection dialog could not be opened
     */
    public static SelectionDialog createMidletSelectionDialog(Shell shell,
            IRunnableContext context, IJavaProject javaProject,
            boolean multipleSelect, String message) throws JavaModelException {
        IJavaSearchScope searchScope = new MidletSearchScope(javaProject);

        SelectionDialog dialog = JavaUI.createTypeDialog(shell, context,
                searchScope,
                IJavaElementSearchConstants.CONSIDER_CLASSES_AND_INTERFACES,
                multipleSelect, "**"); //$NON-NLS-1$

        dialog
                .setTitle(MTJUIMessages.MidletSelectionDialogCreator_createMidletSelectionDialog_title);
        dialog
                .setMessage(MTJUIMessages.MidletSelectionDialogCreator_createMidletSelectionDialog_message);

        return dialog;
    }

    // Private constructor for static access
    private MidletSelectionDialogCreator() {
    }
}
