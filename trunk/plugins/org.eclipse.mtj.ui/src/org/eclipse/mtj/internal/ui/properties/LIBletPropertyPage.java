/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/

package org.eclipse.mtj.internal.ui.properties;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.*;
import org.eclipse.mtj.core.project.midp.IApplicationDescriptor;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.midp.J2MENature;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.project.midp.LibletProperties;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.sdk.device.midp.PackagingModel;
import org.eclipse.mtj.internal.core.util.ClasspathChangeMonitor;
import org.eclipse.mtj.internal.core.util.ColonDelimitedProperties;
import org.eclipse.mtj.internal.core.util.IClasspathChangeListener;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.IWorkbenchPropertyPage;
import org.eclipse.ui.dialogs.PropertyPage;

public class LIBletPropertyPage extends PropertyPage implements
		IWorkbenchPropertyPage {

	private List<LibletProperties> liblets = new ArrayList<LibletProperties>();
	private ComboViewer comboViewer;
	private boolean dirty = false;
	
	private Text jadUrlText;
	private Text libletNameText;
	private Text libletVendorText;
	private Text libletVersionText;
	private Button libletRadioButton;
	private Button proprietaryRadioButton;
	private Button serviceRadioButton;
	private Button standardRadioButton;
	private Button optionalRadioButton;
	private Button requiredRadioButton;
	private Button relativeUrlButton;
	private Label jadUrlLabel;
	private Button removeButton;
	private Label notificationLabel;
	private Label libletVendorLabel;
	private Label libletVersionLabel;
	
	private Composite contentComposite;
	private Composite noLibletComposite;
	private Composite mainComposite;
	private Composite jadURLComposite;

	@Override
	protected Control createContents(Composite parent) {
		noDefaultAndApplyButton();
		initLIBletList();
		
		contentComposite = new Composite(parent, SWT.NONE);
		contentComposite.setLayout(new StackLayout());
		
		noLibletComposite  = new Composite(contentComposite, SWT.NONE);
		noLibletComposite.setLayout(new GridLayout());
		
		mainComposite = new Composite(contentComposite, SWT.NONE);
		
		Label noLibletLabel = new Label(noLibletComposite, SWT.NONE);
		noLibletLabel.setText(MTJUIMessages.LIBletPropertyPage_no_liblet_message);
		
		Button noLibletButton = new Button(noLibletComposite, SWT.NONE);
		noLibletButton.setText(MTJUIMessages.LIBletPropertyPage_no_liblet_add_button);
		Listener addButtonListener = new Listener(){
			public void handleEvent(Event event) {
				LibletProperties newItem = new LibletProperties(
						MTJUIMessages.LIBletPropertyPage_default_liblet_name, 
						LibletProperties.Level.OPTIONAL,
						LibletProperties.Type.STANDARD,
						"",  //$NON-NLS-1$
						MTJUIMessages.LIBletPropertyPage_default_liblet_vendor,
						MTJUIMessages.LIBletPropertyPage_default_liblet_version,
						""); //$NON-NLS-1$
				liblets.add(newItem);
				dirty = true;
				refreshComboViewer();
				comboViewer.setSelection(new StructuredSelection(newItem));
			}			
		};
		noLibletButton.addListener(SWT.Selection, addButtonListener);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 6;
		mainComposite.setLayout(gridLayout);
		
		Label label = new Label(mainComposite, SWT.NONE);
		label.setText(MTJUIMessages.LIBletPropertyPage_liblet_label);
				
		comboViewer = new ComboViewer(mainComposite, SWT.READ_ONLY);
		GridData gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = SWT.FILL;
		comboViewer.getCombo().setLayoutData(gridData);
		comboViewer.setContentProvider(new ArrayContentProvider());
		comboViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof LibletProperties) {
					LibletProperties item = (LibletProperties) element;
					return item.getName() + " (" + getTypeName(item.getType()) + ")"; //$NON-NLS-1$ //$NON-NLS-2$
				}
				return super.getText(element);
			}
		});
		comboViewer.setInput(liblets);
		
		Button addButton = new Button(mainComposite, SWT.NONE);
		addButton.setText(MTJUIMessages.LIBletPropertyPage_add_button);
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		addButton.setLayoutData(gridData);
		addButton.addListener(SWT.Selection, addButtonListener);
		
		removeButton = new Button(mainComposite, SWT.NONE);
		removeButton.setText(MTJUIMessages.LIBletPropertyPage_remove_button);
		removeButton.addListener(SWT.Selection, new Listener(){
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) comboViewer.getSelection();
				LibletProperties listItem = (LibletProperties) selection.getFirstElement();
				int selectionIndex = comboViewer.getCombo().getSelectionIndex();
				
				liblets.remove(listItem);
				dirty = true;
				refreshComboViewer();
				
				if (liblets.size() != 0) {
				    selectionIndex = selectionIndex < liblets.size() ? selectionIndex : liblets.size() - 1;
				    StructuredSelection newSelection = new StructuredSelection(liblets.get(selectionIndex));
				    comboViewer.setSelection(newSelection);
				}
			}			
		});
		
		int maxButtonWidth = Math.max(addButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x,
				removeButton.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
		((GridData) addButton.getLayoutData()).widthHint = maxButtonWidth;
		gridData = new GridData();
		gridData.widthHint = maxButtonWidth;
		removeButton.setLayoutData(gridData);
		
		Label levelLabel = new Label(mainComposite, SWT.NONE);
		levelLabel.setText(MTJUIMessages.LIBletPropertyPage_level_label);
		
		Composite levelGroup = new Composite(mainComposite, SWT.NONE);
		levelGroup.setLayout(new RowLayout(SWT.HORIZONTAL));
		gridData = new GridData();
		gridData.horizontalSpan = 5;
		levelGroup.setLayoutData(gridData);
		
		optionalRadioButton = new Button(levelGroup, SWT.RADIO);
		optionalRadioButton.setText(MTJUIMessages.LIBletPropertyPage_optional_button);
		optionalRadioButton.setSelection(true);
		requiredRadioButton = new Button(levelGroup, SWT.RADIO);
		requiredRadioButton.setText(MTJUIMessages.LIBletPropertyPage_required_button);
		
		Label typeLabel = new Label(mainComposite, SWT.NONE);
		typeLabel.setText(MTJUIMessages.LIBletPropertyPage_type_label);
		
		Composite typeGroup = new Composite(mainComposite, SWT.NONE);
		typeGroup.setLayout(new RowLayout(SWT.HORIZONTAL));
		gridData = new GridData();
		gridData.horizontalSpan = 5;
		typeGroup.setLayoutData(gridData);
		
		libletRadioButton = new Button(typeGroup, SWT.RADIO);
		libletRadioButton.setText(MTJUIMessages.LIBletPropertyPage_liblet_button);
		libletRadioButton.setSelection(true);
		standardRadioButton = new Button(typeGroup, SWT.RADIO);
		standardRadioButton.setText(MTJUIMessages.LIBletPropertyPage_standard_button);
		serviceRadioButton = new Button(typeGroup, SWT.RADIO);
		serviceRadioButton.setText(MTJUIMessages.LIBletPropertyPage_service_button);
		proprietaryRadioButton = new Button(typeGroup, SWT.RADIO);
		proprietaryRadioButton.setText(MTJUIMessages.LIBletPropertyPage_proprietary_button);
		
		jadURLComposite = new Composite(mainComposite, SWT.NONE);
		gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		jadURLComposite.setLayout(gridLayout);
		gridData = new GridData();
		gridData.horizontalSpan = 6;
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalIndent = 15;
		jadURLComposite.setLayoutData(gridData);
		
		jadUrlLabel = new Label(jadURLComposite, SWT.NONE);
		jadUrlLabel.setText(MTJUIMessages.LIBletPropertyPage_jad_url_label);
		
		jadUrlText = new Text(jadURLComposite, SWT.BORDER);
		gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = SWT.FILL;
		jadUrlText.setLayoutData(gridData);
		
		relativeUrlButton = new Button(jadURLComposite, SWT.NONE);
		relativeUrlButton.setText(MTJUIMessages.LIBletPropertyPage_relative_url_label);
		relativeUrlButton.addListener(SWT.Selection, new Listener(){
			public void handleEvent(Event event) {
				IStructuredSelection selection = (IStructuredSelection) comboViewer.getSelection();
				LibletProperties listItem = (LibletProperties) selection.getFirstElement();
				String jadURL = IMTJCoreConstants.EMULATION_LIBS_FOLDER_NAME
						+ "/" + listItem.getJadFileName(); //$NON-NLS-1$
				jadUrlText.setText(jadURL);			
				updateLIBletListItem(listItem);
			}			
		});
		
		new Label(jadURLComposite, SWT.NONE);
		notificationLabel = new Label(jadURLComposite, SWT.NONE);
		notificationLabel.setText(MTJUIMessages.LIBletPropertyPage_liblet_notification_label);
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		notificationLabel.setLayoutData(gridData);
		
		Label libletInformationLabel = new Label(mainComposite, SWT.NONE);
		libletInformationLabel.setText(MTJUIMessages.LIBletPropertyPage_liblet_information_label);
		gridData = new GridData();
		gridData.horizontalSpan = 6;
		gridData.verticalIndent = 15;
		libletInformationLabel.setLayoutData(gridData);
		
		Label libletNameLabel = new Label(mainComposite, SWT.NONE);
		libletNameLabel.setText(MTJUIMessages.LIBletPropertyPage_liblet_name_label);
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalIndent = 10;
		libletNameLabel.setLayoutData(gridData);
		
		libletNameText = new Text(mainComposite, SWT.BORDER);
		gridData = new GridData();
		gridData.horizontalSpan = 4;
		gridData.widthHint = 200;
		libletNameText.setLayoutData(gridData);
		
		libletVendorLabel = new Label(mainComposite, SWT.NONE);
		libletVendorLabel.setText(MTJUIMessages.LIBletPropertyPage_liblet_vendor_label);
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalIndent = 10;
		libletVendorLabel.setLayoutData(gridData);
		
		libletVendorText = new Text(mainComposite, SWT.BORDER);
		gridData = new GridData();
		gridData.horizontalSpan = 4;
		gridData.widthHint = 200;
		libletVendorText.setLayoutData(gridData);
		
		libletVersionLabel = new Label(mainComposite, SWT.NONE);
		libletVersionLabel.setText(MTJUIMessages.LIBletPropertyPage_liblet_version_label);
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalIndent = 10;
		libletVersionLabel.setLayoutData(gridData);
		
		libletVersionText = new Text(mainComposite, SWT.BORDER);
		gridData = new GridData();
		gridData.horizontalSpan = 4;
		gridData.widthHint = 200;
		libletVersionText.setLayoutData(gridData);
			
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			private LibletProperties previousSelection;
			private Button[] radioButtons = {libletRadioButton, proprietaryRadioButton,
					serviceRadioButton, standardRadioButton, optionalRadioButton,
					requiredRadioButton};
			
			public void selectionChanged(SelectionChangedEvent event) {
				
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				if (selection.size() > 0) {
					LibletProperties item = (LibletProperties)selection.getFirstElement();
					
					if (previousSelection != null && previousSelection != item) {
						updateLIBletListItem(previousSelection);
					}
					
					jadUrlText.setText(item.getJadURL());
					libletNameText.setText(item.getName());
					libletVendorText.setText(item.getVendor());
					libletVersionText.setText(item.getVersion());
					
					for (Button b : radioButtons) {
						b.setSelection(false);
					}
					
					switch (item.getType()) {
					case LIBLET:
						libletRadioButton.setSelection(true);
						break;
					case PROPRIETARY:
						proprietaryRadioButton.setSelection(true);
						break;
					case SERVICE:
						serviceRadioButton.setSelection(true);
						break;
					case STANDARD:
						standardRadioButton.setSelection(true);
						break;
					}
					
					switch (item.getLevel()) {
					case OPTIONAL:
						optionalRadioButton.setSelection(true);
						break;
					case REQUIRED:
						requiredRadioButton.setSelection(true);
						break;
					}
					
					previousSelection = item;
					setComponentsAvailability();
				}
			}
		});
	    
		SelectionAdapter typeSelectionAdapter = new SelectionAdapter() {
			@Override
	        public void widgetSelected(SelectionEvent e) {
				if (((Button) e.widget).getSelection()) {
					setComponentsAvailability();
				}
			}
		};
		
		libletRadioButton.addSelectionListener(typeSelectionAdapter);
		proprietaryRadioButton.addSelectionListener(typeSelectionAdapter);
		serviceRadioButton.addSelectionListener(typeSelectionAdapter);
		standardRadioButton.addSelectionListener(typeSelectionAdapter);
        
		ClasspathChangeMonitor.getInstance().addClasspathChangeListener(
				getMidletSuiteProject(), new IClasspathChangeListener() {
			public void classpathChanged() {
				updateLIBletProjectItems();

				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						refreshComboViewer();
						performOk();
					}
				});
			}
		});
		
		updateLIBletProjectItems();
		refreshComboViewer();
		setComponentsAvailability();
		
		return contentComposite;
	}
	
	private void refreshComboViewer() {
		comboViewer.refresh();
		if (liblets.size() == 0) {
			((StackLayout) contentComposite.getLayout()).topControl = noLibletComposite;
		} else {
			((StackLayout) contentComposite.getLayout()).topControl = mainComposite;
			ISelection selection = new StructuredSelection(liblets.get(0));
			comboViewer.setSelection(selection );
		}
		contentComposite.layout();		
	}
	
	private void updateLIBletProjectItems() {
		IMidletSuiteProject midletProject = getMidletSuiteProject();
		String[] requiredProjectNames = null;
		try {
			requiredProjectNames = midletProject.getJavaProject().getRequiredProjectNames();
		} catch (JavaModelException e) {
			e.printStackTrace();
		}				
		
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
        
        List<IMidletSuiteProject> libletProjects = new ArrayList<IMidletSuiteProject>();
        for (String projectName : requiredProjectNames) {
        	IProject project = workspace.getRoot().getProject(projectName);
        	
        	IMidletSuiteProject midletSuiteProject = MidletSuiteFactory
        			.getMidletSuiteProject(JavaCore.create(project));
        	
        	if (midletSuiteProject.getProjectPackagingModel() == PackagingModel.LIBLET) {
        		libletProjects.add(midletSuiteProject);
        	}
        }
        
        Map<String, LibletProperties.Level> libletProjectsLevel = new HashMap<String, LibletProperties.Level>();
        
        Iterator<LibletProperties> iterator = liblets.iterator();
        while (iterator.hasNext()) {
            LibletProperties libletProp = (LibletProperties) iterator.next();
        	if (libletProp.getType() == LibletProperties.Type.LIBLET) {
        	    libletProjectsLevel.put(libletProp.getProjectName(), libletProp.getLevel());
        		iterator.remove();
        	}
        }
        
        for (IMidletSuiteProject project : libletProjects) {
        	IApplicationDescriptor descriptor = project.getApplicationDescriptor();
        	ColonDelimitedProperties properties = (ColonDelimitedProperties) descriptor.getManifestProperties();
        	
            String libletName = properties.getProperty(IJADConstants.JAD_LIBLET_NAME);
    		String libletVendor = properties.getProperty(IJADConstants.JAD_LIBLET_VENDOR);
    		String libletVersion = properties.getProperty(IJADConstants.JAD_LIBLET_VERSION);
    		
    		String jadURL = IMTJCoreConstants.EMULATION_LIBS_FOLDER_NAME + "/" + project.getJadFileName(); //$NON-NLS-1$
    		
    		LibletProperties.Level level = libletProjectsLevel.get(project.getProject().getName());
    		
    		LibletProperties newItem = new LibletProperties(libletName, level != null ? level : LibletProperties.Level.OPTIONAL, 
    				LibletProperties.Type.LIBLET, jadURL, libletVendor, libletVersion,
    				project.getProject().getName());
 
    		newItem.setJadFileName(project.getJadFileName());
    		
    		liblets.add(newItem);        		
        }
	}
	
	private void updateLIBletListItem(LibletProperties item) {
        if (!item.getJadURL().equals(jadUrlText.getText())) {
		    item.setJadURL(jadUrlText.getText());
		    dirty = true;
        }
        
        if (!item.getName().equals(libletNameText.getText())) {
		    item.setName(libletNameText.getText());
		    dirty = true;
        }
        
        if (!item.getVendor().equals(libletVendorText.getText())) {
		    item.setVendor(libletVendorText.getText());
		    dirty = true;
        }
        
        if (!item.getVersion().equals(libletVersionText.getText())) {
		    item.setVersion(libletVersionText.getText());
		    dirty = true;
        }
		
        LibletProperties.Type type = null;
		if (libletRadioButton.getSelection()) {
			type = LibletProperties.Type.LIBLET;
		} else if (proprietaryRadioButton.getSelection()) {
			type = LibletProperties.Type.PROPRIETARY;
		} else if (serviceRadioButton.getSelection()) {
			type = LibletProperties.Type.SERVICE;
		} else if (standardRadioButton.getSelection()) {
			type = LibletProperties.Type.STANDARD;
		}
		
		if (type != item.getType()) {
			item.setType(type);
			dirty = true;
		}

		LibletProperties.Level level = null;
		if (optionalRadioButton.getSelection()) {
			level = LibletProperties.Level.OPTIONAL;
		} else if (requiredRadioButton.getSelection()) {
			level = LibletProperties.Level.REQUIRED;
		}
		
		if (level != item.getLevel()) {
			item.setLevel(level);
			dirty = true;
		}
		
		comboViewer.refresh();	
	}
	
	private void setComponentsAvailability() {
		boolean islibletRadioButtonSelected = libletRadioButton.getSelection();
		boolean isServiceRadioButtonSelected = serviceRadioButton.getSelection();

		proprietaryRadioButton.setEnabled(!islibletRadioButtonSelected);
		serviceRadioButton.setEnabled(!islibletRadioButtonSelected);
		standardRadioButton.setEnabled(!islibletRadioButtonSelected);
		libletNameText.setEnabled(!islibletRadioButtonSelected);
		libletRadioButton.setEnabled(islibletRadioButtonSelected);
		removeButton.setEnabled(!islibletRadioButtonSelected);
		libletVendorText.setEnabled(!islibletRadioButtonSelected);
		
        setComponentExclude(jadURLComposite, !islibletRadioButtonSelected);
        setComponentExclude(libletVendorLabel, isServiceRadioButtonSelected);
		setComponentExclude(libletVendorText, isServiceRadioButtonSelected);
		setComponentExclude(libletVersionLabel, isServiceRadioButtonSelected);
		setComponentExclude(libletVersionText, isServiceRadioButtonSelected);
		
		mainComposite.layout();
	}
	
	private void setComponentExclude(Control component, boolean exclude) {
	    component.setVisible(!exclude);
        Object layoutData = component.getLayoutData();
        if (layoutData instanceof GridData) {
             ((GridData) layoutData).exclude = exclude;
        }
	}
	
	@Override
    public void setVisible(boolean visible) {
        if (!visible) {
            	
          	IStructuredSelection selection = (IStructuredSelection) comboViewer.getSelection();
          	LibletProperties item = (LibletProperties) selection.getFirstElement();
       		if (item != null) {
       		    updateLIBletListItem(item);
       		}
        		
            if (dirty) {
                String title = MTJUIMessages.LIBletPropertyPage_unsavedchanges_title;
                String message = MTJUIMessages.LIBletPropertyPage_unsavedchanges_message;
                String[] buttonLabels = new String[] {
                        MTJUIMessages.LIBletPropertyPage_unsavedchanges_button_save,
                        MTJUIMessages.LIBletPropertyPage_unsavedchanges_button_discard,
                        MTJUIMessages.LIBletPropertyPage_unsavedchanges_button_ignore };
                MessageDialog dialog = new MessageDialog(getShell(), title,
                        null, message, MessageDialog.QUESTION,
                        buttonLabels, 0);
                int res = dialog.open();
                if (res == 0) {
                    /* Apply changes */
                    performOk();
                } else if (res == 1) {
                    /* Discard changes */
                 	initLIBletList();
                } else {
                    /* keep unsaved */
                }
            }
        }
        super.setVisible(visible);
    }
	
	private void initLIBletList() {
		liblets.clear();
		dirty = false;
		
		IProject project = getCurrentProject();

		boolean isJ2meProject = false;
		try {
			isJ2meProject = J2MENature.hasMtjCoreNature(project);
		} catch (CoreException e) {
		}

		if (!isJ2meProject) {
			return;
		}
		
		List<LibletProperties> properties = null;		
		try {
			LibletProperties[] libletProperties = getMidletSuiteProject().getLibletProperties();
			
			if (libletProperties != null) {
			    properties = Arrays.asList(libletProperties);
			}
		} catch (CoreException e) {
			// ignore
		}
		
		if (properties != null) {
		    liblets.addAll(properties);
		}
	}
	
	private IProject getCurrentProject() {
        IProject project = null;
        IAdaptable adaptable = getElement();

        if (adaptable instanceof IProject) {
            project = (IProject) adaptable;
        } else if (adaptable instanceof IJavaProject) {
            project = ((IJavaProject) adaptable).getProject();
        }

        return project;
    }
	
	private IMidletSuiteProject getMidletSuiteProject() {
		IJavaProject javaProject = JavaCore.create(getCurrentProject());
		IMidletSuiteProject midletProject = MidletSuiteFactory
				.getMidletSuiteProject(javaProject);

		return midletProject;
	}
	
	@Override
	public boolean performOk() {
	    IStructuredSelection selection = (IStructuredSelection) comboViewer.getSelection();
        LibletProperties item = (LibletProperties) selection.getFirstElement();
        if (item != null) {
            updateLIBletListItem(item);
        }	    
	    
		IProject project = getCurrentProject();

		boolean isJ2meProject = false;
		try {
			isJ2meProject = J2MENature.hasMtjCoreNature(project);
		} catch (CoreException e) {
		}

		if (!isJ2meProject) {
			return true;
		}
		
		IMidletSuiteProject midletProject = getMidletSuiteProject();
		
		try {
			midletProject.setLibletProperties(liblets.toArray(new LibletProperties[0]));
			midletProject.saveMetaData();
		} catch (CoreException e) {
			e.printStackTrace();
		}
		
		dirty = false;
		return true;
	}
	
    private static String getTypeName(LibletProperties.Type type) {
        switch (type) {
        case LIBLET:
            return MTJUIMessages.LIBletPropertyPage_liblet_button;
        case PROPRIETARY:
            return MTJUIMessages.LIBletPropertyPage_proprietary_button;
        case SERVICE:
            return MTJUIMessages.LIBletPropertyPage_service_button;
        case STANDARD:
            return MTJUIMessages.LIBletPropertyPage_standard_button;
        }

        return null;
    }
}
