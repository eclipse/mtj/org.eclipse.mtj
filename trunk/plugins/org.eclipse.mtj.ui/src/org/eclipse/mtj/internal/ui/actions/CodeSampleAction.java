package org.eclipse.mtj.internal.ui.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.internal.ui.dialog.CodeSamplesDialog;

/**
 * Code sample action handler
 * 
 * @author Pranav Gothadiya (Nokia)
 * 
 */
public class CodeSampleAction extends AbstractHandler {

	private static final String PARM_MSG = "org.eclipse.mtj.ui.samples";

	public Object execute(ExecutionEvent event) throws ExecutionException {

		CodeSamplesDialog dialog = new CodeSamplesDialog(null,
				event.getParameter(PARM_MSG));
		dialog.create();

		if (dialog.open() == Window.OK) {
		}
		return null;
	}

}