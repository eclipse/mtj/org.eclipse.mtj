/*
* Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
* All rights reserved.
* This component and the accompanying materials are made available
* under the terms of "Eclipse Public License v1.0"
* which accompanies this distribution, and is available
* at the URL "http://www.eclipse.org/legal/epl-v10.html".
*
* Initial Contributors:
* 		Nokia Corporation - initial contribution.
*
*/
package org.eclipse.mtj.internal.ui.wizards.export.packaging;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.preferences.J2MEPreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;

public class MidletPackageExportPage extends WizardPage {
	private List<IJavaProject> fSelectedJavaProjects = new ArrayList<IJavaProject>();
	private Table configurationTable;
	private Table projectsTable;
	private CheckboxTableViewer configurationTableViewer;
	private CheckboxTableViewer projectsTableViewer;
	private Button btnObfuscate;
	private HashMap<IJavaProject, List<MTJRuntime>> selections = new HashMap<IJavaProject, List<MTJRuntime>>();
	private Combo destinationFolder;
	
	private static final int DESTINARTION_HISTORY_LENGTH = 5;
	private static final String SETTINGS_KEY_DESTINATION_NAMES = "MidletPackageExportPage.KEY_DESTINATION_NAMES"; //$NON-NLS-1$
	private static final String SETTINGS_KEY_OVERWRITE_EXISTING_FILES = "MidletPackageExportPage.KEY_OVERWRITE_EXISTING_FILES"; //$NON-NLS-1$
	private static final String SETTINGS_KEY_OBFUSCATE = "MidletPackageExportPage.KEY_OBFUSCATE"; //$NON-NLS-1$
	private Button btnUseDeployedDirectory;
	private Button btnBrowse;
	
	
	
	protected MidletPackageExportPage() {
		super("MidletPackageExportPage"); //$NON-NLS-1$
		setTitle(MTJUIMessages.MidletExportPageTitle);
		setDescription(MTJUIMessages.MidletExportPageDescription);
	}

	public void createControl(Composite parent) {
      initializeDialogUnits(parent);
      Composite workArea = new Composite(parent, SWT.NONE);
      setControl(workArea);
      workArea.setLayout(new GridLayout(1, false));
      
      Composite projectConfigurationGroup = new Composite(workArea, SWT.NONE);
      projectConfigurationGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
      projectConfigurationGroup.setLayout(new GridLayout(2, true));
      
      Label lblConfigurations = new Label(projectConfigurationGroup, SWT.NONE);
      lblConfigurations.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 1));
      lblConfigurations.setSize(217, 13);
      lblConfigurations.setText(MTJUIMessages.MidletExportPageConfigurationDescription);
      
      projectsTableViewer = CheckboxTableViewer.newCheckList(projectConfigurationGroup, SWT.BORDER | SWT.FULL_SELECTION);
      projectsTableViewer.addCheckStateListener(new ICheckStateListener() {
      	public void checkStateChanged(CheckStateChangedEvent event) {
      		IJavaProject project = (IJavaProject)event.getElement();
      		if(selections.containsKey(project)){
      			removeSelection(project);
      		}else{
      			addToSelection(project);
      		}
      		updateSelectedState();
      		updatePageStatus();
      	}
      });
      projectsTable = projectsTableViewer.getTable();
      projectsTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
      projectsTableViewer.setContentProvider(new WorkbenchContentProvider() {
          public Object[] getElements(Object element) {
              if (element instanceof IJavaProject[]) {
                  return (IJavaProject[]) element;
              }
              return null;
          }
      });
      projectsTableViewer.setLabelProvider(new WorkbenchLabelProvider());
      projectsTableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
		public void selectionChanged(SelectionChangedEvent event) {
				updateSelectedState();
		}
	});
     
      
      configurationTableViewer = CheckboxTableViewer.newCheckList(projectConfigurationGroup, SWT.BORDER | SWT.FULL_SELECTION);
      configurationTableViewer.addCheckStateListener(new ICheckStateListener() {
      	public void checkStateChanged(CheckStateChangedEvent event) {
      		MTJRuntime runtime = (MTJRuntime)event.getElement();
      		IJavaProject project = getSelectedProject();
      		if (event.getChecked()){
      			addToSelection(project,runtime);
      		}else{
      			removeSelection(project, runtime);
      		}
      		updateSelectedState();
      	}
      });
      configurationTable = configurationTableViewer.getTable();
      configurationTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
      configurationTableViewer.setContentProvider(new MTJRuntimeListContentProvider());
      configurationTableViewer.setLabelProvider(new MTJRuntimeLabelProvider());
      
      Group destinationGroup = new Group(workArea, SWT.NONE);
      destinationGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
      destinationGroup.setText(MTJUIMessages.MidletExportPageDestinationDirectoryGroupLabel);
      GridLayout gl_destinationGroup = new GridLayout(3, false);
      destinationGroup.setLayout(gl_destinationGroup);
      
      
      Label lblDestinationDirectory = new Label(destinationGroup, SWT.NONE);
      lblDestinationDirectory.setText(MTJUIMessages.MidletExportPageDestinationDirectoryLabel);
      
      destinationFolder = new Combo(destinationGroup, SWT.NONE);
      destinationFolder.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
      destinationFolder.addModifyListener(new ModifyListener() {
      	public void modifyText(ModifyEvent e) {
      		updatePageStatus();
      	}
      });
      
      btnBrowse = new Button(destinationGroup, SWT.NONE);
      btnBrowse.addSelectionListener(new SelectionAdapter() {
      	@Override
      	public void widgetSelected(SelectionEvent e) {
      		handleDestinationButtonBrowseSelected();
      	}
      });
      btnBrowse.setText(MTJUIMessages.MidletExportPageDestinationDirectoryBrowseButton);
      
      Link deploymentOption = new Link(destinationGroup, SWT.NONE);
      deploymentOption.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1));
      deploymentOption.setText(MTJUIMessages.MidletPackageExportPage_deploymentOption_text);
      deploymentOption.addSelectionListener(new SelectionAdapter() {
       	@Override
     	public void widgetSelected(SelectionEvent e) {
     		PreferencesUtil.createPreferenceDialogOn(getShell(), J2MEPreferencePage.ID, null, null).open();
     	}
     });
      btnUseDeployedDirectory = new Button(destinationGroup, SWT.CHECK);
      btnUseDeployedDirectory.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
      btnUseDeployedDirectory.addSelectionListener(new SelectionAdapter() {
      	@Override
      	public void widgetSelected(SelectionEvent e) {
      		if (btnUseDeployedDirectory.getSelection()){
      			destinationFolder.setEnabled(false);
      			btnBrowse.setEnabled(false);
      		}else{
      			destinationFolder.setEnabled(true);
      			btnBrowse.setEnabled(true);      			
      		}
      		updatePageStatus();
       	}
      });
      btnUseDeployedDirectory.setText(MTJUIMessages.MidletExportPageDestinationDirectoryDeploymentOption);

      Group grpOptions = new Group(workArea, SWT.NONE);
      grpOptions.setText(MTJUIMessages.MidletExportPageOptionsGroupLabel);
      grpOptions.setLayout(new GridLayout(1, false));
      grpOptions.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
      
      Link proguardPreferencesLink = new Link(grpOptions, SWT.NONE);
      proguardPreferencesLink.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
      proguardPreferencesLink.addSelectionListener(new SelectionAdapter() {
         	@Override
         	public void widgetSelected(SelectionEvent e) {
         		PreferencesUtil.createPreferenceDialogOn(getShell(), J2MEPreferencePage.ID, null, null).open();
         		updatePageStatus();
         	}
         });

      proguardPreferencesLink.setText(MTJUIMessages.MidletExportPageObfuscatorOptionPreferencesLink);
      
      btnObfuscate = new Button(grpOptions, SWT.CHECK);
      btnObfuscate.addSelectionListener(new SelectionAdapter() {
      	@Override
      	public void widgetSelected(SelectionEvent e) {
      		updatePageStatus();
       	}
      });
      btnObfuscate.setText(MTJUIMessages.MidletExportPageObfuscatorOptionLabel);

      Dialog.applyDialogFont(parent);
      initializeProjects();
      restoreWizardValues();
      updatePageStatus();
	}
	
	private void handleDestinationButtonBrowseSelected() {
       DirectoryDialog dialog = new DirectoryDialog(getContainer().getShell(),
                SWT.SAVE | SWT.SHEET);
        dialog.setMessage(MTJUIMessages.MidletExportPageDestinationDirectoryDialogMessage);
        dialog.setText(MTJUIMessages.MidletExportPageDestinationDirectoryDialogTitle);
        dialog.setFilterPath(getDestinationDirectory());
        String selectedDirectoryName = dialog.open();
        if (selectedDirectoryName != null) {
            setErrorMessage(null);
            destinationFolder.setText(selectedDirectoryName);
        }
		
	}
	/*package*/ String getDestinationDirectory(){
		return destinationFolder.getText().trim();
	}
	
	private void updatePageStatus() {
		if(btnObfuscate.getSelection() && !MTJCore.getProguardJarFile().exists()){
			setErrorMessage(MTJUIMessages.AbstractCreatePackageAction_warnAboutProguardConfiguration_message);
			setPageComplete(false);
			return;
		}
		if (projectsTableViewer.getCheckedElements().length <1 ){
			setPageComplete(false);
			return;
		}
		if (!getUseDeployed()) {
			if (getUseDeployed() && getDestinationDirectory().isEmpty()) {
				setErrorMessage(MTJUIMessages.MidletExportPageDestinationNoDirectoryErrorMessage);
				setPageComplete(false);
				return;
			}
			File f = new File(getDestinationDirectory());
			if (!f.exists()) {
				setErrorMessage(MTJUIMessages.MidletExportPageDestinationDirectoryNotExsitsDialogErrorMessage);
				setPageComplete(false);
				return;
			}
			if (!f.isDirectory()) {
				setErrorMessage(MTJUIMessages.MidletExportPageDestinationDirectoryNotAdirectoryErrormMessage);
				setPageComplete(false);
				return;
			}
		}
		
		setErrorMessage(null);
		setPageComplete(true);
	}
	
    private void initializeProjects() {
        IWorkspaceRoot rootWorkspace = ResourcesPlugin.getWorkspace().getRoot();
        
        IJavaModel javaModel = JavaCore.create(rootWorkspace);
        IJavaProject[] javaProjects;
        try {
            javaProjects = javaModel.getJavaProjects();
        }
        catch (JavaModelException e) {
            javaProjects= new IJavaProject[0];
        }
        ArrayList<IJavaProject> midlets = new ArrayList<IJavaProject>(javaProjects.length);
        for (int i = 0; i < javaProjects.length; i++) {
			if(MidletSuiteFactory.isMidletSuiteProject(javaProjects[i].getProject())){
				midlets.add(javaProjects[i]);
			}
		}
        if(midlets.size()>0){
        	projectsTableViewer.setInput(midlets.toArray(new IJavaProject[midlets.size()]));
        	projectsTableViewer.setSelection(new StructuredSelection(midlets.get(0)));
        }
        
        if (fSelectedJavaProjects != null && !fSelectedJavaProjects.isEmpty()) {
          for (IJavaProject iJavaProject : fSelectedJavaProjects) {
        	addToSelection(iJavaProject);  
          }
          updateSelectedState();
		}
        projectsTableViewer.getTable().setFocus();
        
    }
	public void setSelectedProjects(List<IJavaProject> projects) {
		fSelectedJavaProjects.addAll(projects);
	}
	
	private void updateSelectedState(){
		IJavaProject project = getSelectedProject();
		if (configurationTableViewer.getInput() != project ){
			configurationTableViewer.setInput(project);
		}
		
		List<MTJRuntime> runtimes = selections.get(project);
		if(runtimes == null ){
			runtimes= Collections.emptyList();
		}
		Set<IJavaProject> keySet = selections.keySet();
		projectsTableViewer.setCheckedElements(keySet.toArray(new IJavaProject[keySet.size()]));
		configurationTableViewer.setCheckedElements(runtimes.toArray(new MTJRuntime[runtimes.size()]));
	}

	private IJavaProject getSelectedProject() {
		return (IJavaProject)((IStructuredSelection)projectsTableViewer.getSelection()).getFirstElement();
	}
	
	/**
	 * Add the project and its active runtime to the list of selected projects to export
	 * @param project
	 */
	private void addToSelection(IJavaProject project){
		IMidletSuiteProject msp = MidletSuiteFactory.getMidletSuiteProject(project);
		MTJRuntimeList list = msp.getRuntimeList();
		addToSelection(project, list.getActiveMTJRuntime());
	}
	private void addToSelection(IJavaProject project, MTJRuntime runtime){
		List<MTJRuntime> list;
		if(selections.containsKey(project)){
			list = selections.get(project);
		}else{
			list= new ArrayList<MTJRuntime>();
		}
		if(!list.contains(runtime))
			list.add(runtime);
		selections.put(project, list);		
	}
	/**
	 * Remove the project from the list of selected projects to export.
	 * 
	 * @param project
	 */
	private void removeSelection(IJavaProject project){
		selections.remove(project);
	}
	
	private void removeSelection(IJavaProject project, MTJRuntime runtime){
		List<MTJRuntime> list= selections.get(project);
		if (list != null ){
			list.remove(runtime);
		}
		if (list == null || list.isEmpty()){
			removeSelection(project);
		}
	}
	
	private void saveWizardValues(){
		IDialogSettings settings =  getDialogSettings();
        if (settings != null) {
            String[] directoryNames = settings
                    .getArray(SETTINGS_KEY_DESTINATION_NAMES);
            if (directoryNames == null) {
				directoryNames = new String[0];
			}
            directoryNames = addToHistory(directoryNames, getDestinationDirectory());
            settings.put(SETTINGS_KEY_DESTINATION_NAMES, directoryNames);

            settings.put(SETTINGS_KEY_OVERWRITE_EXISTING_FILES,
                    btnUseDeployedDirectory.getSelection());

            settings.put(SETTINGS_KEY_OBFUSCATE,
                    btnObfuscate.getSelection());

        }
	}
	
   private String[] addToHistory(String[] history, String newEntry) {
        java.util.List<String> l = new ArrayList<String>();
        l.addAll(Arrays.asList(history));
        l.remove(newEntry);
        l.add(0, newEntry);

        // since only one new item was added, we can be over the limit
        // by at most one item
        if (l.size() > DESTINARTION_HISTORY_LENGTH) {
			l.remove(DESTINARTION_HISTORY_LENGTH);
		}

        String[] r = new String[l.size()];
        l.toArray(r);
        return r;
    }
	
	private void restoreWizardValues() {
		IDialogSettings settings = getDialogSettings();
		if (settings != null) {
			String[] directoryNames = settings
					.getArray(SETTINGS_KEY_DESTINATION_NAMES);
			//directories
			if (directoryNames != null) {
				for (int i = 0; i < directoryNames.length; i++) {
					destinationFolder.add(directoryNames[i]);
				}
			}
			//options
			btnObfuscate.setSelection(settings.getBoolean(SETTINGS_KEY_OBFUSCATE));
			boolean useDeployed = settings.getBoolean(SETTINGS_KEY_OVERWRITE_EXISTING_FILES);
			btnUseDeployedDirectory.setSelection(useDeployed);
			destinationFolder.setEnabled(!useDeployed);
			btnBrowse.setEnabled(!useDeployed);
		}
	}
	
	/**
	 * Finish the page, save values for restoring next time. 
	 */
	public void finish(){
		saveWizardValues();
	}
	
	/**
	 * Returns the projects and runtimes selected by user
	 * @return 
	 */
	/*package*/ Map<IJavaProject, List<MTJRuntime>> getProjectSelections(){
		return selections;
	}
	/**
	 * Returns if the generated jar be obfuscated
	 * @return
	 */
	/*package*/ boolean getObfuscate(){
		return btnObfuscate.getSelection();
	}
	/**
	 * Returns if overwrite existing files is selected
	 * @return
	 */
	/*package*/ boolean getUseDeployed(){
		return btnUseDeployedDirectory.getSelection();
	}
	
}
