/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma (Sybase)        - Initial implementation
 *     Diego Sandin (Motorola) - Use Eclipse Message Bundles [Bug 255874] 
 */
package org.eclipse.mtj.internal.ui.editors.jad.form.pages;

import java.util.Map;

import org.eclipse.mtj.core.project.midp.DescriptorPropertyDescription;
import org.eclipse.mtj.core.project.midp.IJADDescriptorsProvider;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.sdk.device.midp.Configuration;
import org.eclipse.mtj.internal.core.sdk.device.midp.Profile;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.ui.editors.jad.ListDescriptorPropertyDescription;
import org.eclipse.mtj.ui.editors.jad.ListDescriptorPropertyDescription.IListDescriptorPropertyFilter;

/**
 * provide required JAD attributes descriptors
 * 
 * @author Gang Ma
 */
public class RequiredJADDesciptorsProvider implements IJADDescriptorsProvider {

    /**
     * Required property descriptors.
     */
    private static final DescriptorPropertyDescription[] REQUIRED_DESCRIPTORS = new DescriptorPropertyDescription[] {
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_JAR_URL,
                    MTJUIMessages.RequiredJADDesciptorsProvider_midlet_jar_url,
                    DescriptorPropertyDescription.DATATYPE_URL),
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_NAME,
                    MTJUIMessages.RequiredJADDesciptorsProvider_midlet_name,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_VENDOR,
                    MTJUIMessages.RequiredJADDesciptorsProvider_midlet_vendor,
                    DescriptorPropertyDescription.DATATYPE_STRING),
            new DescriptorPropertyDescription(IJADConstants.JAD_MIDLET_VERSION,
                    MTJUIMessages.RequiredJADDesciptorsProvider_midlet_version,
                    DescriptorPropertyDescription.DATATYPE_STRING),
                    new DescriptorPropertyDescription(IJADConstants.JAD_LIBLET_JAR_URL,
                    MTJUIMessages.RequiredJADDesciptorsProvider_liblet_jar_url,
					DescriptorPropertyDescription.DATATYPE_STRING, true),
			new DescriptorPropertyDescription(IJADConstants.JAD_LIBLET_NAME,
					MTJUIMessages.RequiredJADDesciptorsProvider_liblet_name,
					DescriptorPropertyDescription.DATATYPE_STRING, true),
			new DescriptorPropertyDescription(IJADConstants.JAD_LIBLET_VENDOR,
					MTJUIMessages.RequiredJADDesciptorsProvider_liblet_vendor,
					DescriptorPropertyDescription.DATATYPE_STRING, true),
			new DescriptorPropertyDescription(IJADConstants.JAD_LIBLET_VERSION,
					MTJUIMessages.RequiredJADDesciptorsProvider_liblet_version,
					DescriptorPropertyDescription.DATATYPE_STRING, true),
            createConfigurationPropertyDescription(),
            new ListDescriptorPropertyDescription(
                    IJADConstants.JAD_MICROEDITION_PROFILE,
                    MTJUIMessages.RequiredJADDesciptorsProvider_microedition_profile,
                    getProfileNamesAndValues()) };

	private static ListDescriptorPropertyDescription createConfigurationPropertyDescription() {
		ListDescriptorPropertyDescription result = new ListDescriptorPropertyDescription(
		        IJADConstants.JAD_MICROEDITION_CONFIG,
		        MTJUIMessages.RequiredJADDesciptorsProvider_microedition_configuration,
		        getConfigurationNamesAndValues());
		
		result.setFilter(new IListDescriptorPropertyFilter() {
			private String[] filterPropertyNames = new String[] { IJADConstants.JAD_MICROEDITION_PROFILE };
			private String[][] meepSupportedConfigurations = new String[][] { 
					new String[] { Configuration.CLDC_18.getName(), Configuration.CLDC_18.toString() } 
			};

			public String[] getFilterPropertyNames() {
				return filterPropertyNames;
			}

			public String[][] filterNamesAndValues(Map<String, Object> filterProperties, String[][] namesAndValues) {
			    // MEEP profile supports only CLDC 1.8 configuration
				if (Profile.MEEP_80.toString().equals(filterProperties.get(IJADConstants.JAD_MICROEDITION_PROFILE))) {
					return meepSupportedConfigurations;
				}

				return namesAndValues;
			}
		});
		
		return result;
	}

    /**
     * Return the J2ME configuration names and values.
     * 
     * @return
     */
    private static String[][] getConfigurationNamesAndValues() {
        String[][] namesAndValues = null;

        IAPI[] specifications = Configuration.values();

        namesAndValues = new String[specifications.length][];
        for (int i = 0; i < specifications.length; i++) {
            IAPI spec = specifications[i];
            namesAndValues[i] = new String[2];
            namesAndValues[i][0] = spec.getName();
            namesAndValues[i][1] = spec.toString();
        }

        return namesAndValues;
    }

    /**
     * Return the J2ME profile names and values.
     * 
     * @return
     */
    private static String[][] getProfileNamesAndValues() {
        String[][] namesAndValues = null;

        IAPI[] specifications = Profile.values();

        namesAndValues = new String[specifications.length][];
        for (int i = 0; i < specifications.length; i++) {
            IAPI spec = specifications[i];
            namesAndValues[i] = new String[2];
            namesAndValues[i][0] = spec.getName();
            namesAndValues[i][1] = spec.toString();
        }

        return namesAndValues;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.IJADDescriptorsProvider#getDescriptorPropertyDescriptions()
     */
    public DescriptorPropertyDescription[] getDescriptorPropertyDescriptions() {
        return REQUIRED_DESCRIPTORS;
    }

}
