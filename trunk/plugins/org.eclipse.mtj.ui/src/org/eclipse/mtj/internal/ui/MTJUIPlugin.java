/**
 * Copyright (c) 2004,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Diego Sandin (Motorola)  - Adapting code to use Jetty version 5.1.11        
 *     David Marques(Motorola)  - Adding buildGridData functions.     
 *     Rafael Amaral (Motorola) - Adding SDK references verification [Bug 282738].
 *     Jon Dearden (Research In Motion) - Replaced deprecated use of Preferences 
 *                                        [Bug 285699]              
 */
package org.eclipse.mtj.internal.ui;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.xml.transform.TransformerException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPersistentPreferenceStore;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDeviceRegistry;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.launching.midp.ota.OTAServer;
import org.eclipse.mtj.internal.core.sdk.device.DeviceRegistry;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.util.PluginPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Resource;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;
import org.eclipse.ui.forms.FormColors;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.osgi.framework.BundleContext;

/**
 * The plug-in implementation class.
 * 
 * @author Craig Setera
 */
public class MTJUIPlugin extends AbstractUIPlugin {

    /**
     * A class wrapper to act as a key into the color cache
     */
    private static class ColorCacheKey {

        private int blue;
        private Display display;
        private int green;
        private int red;

        /**
         * Constructor.
         * 
         * @param display
         * @param red
         * @param green
         * @param blue
         */
        private ColorCacheKey(Display display, int red, int green, int blue) {
            this.display = display;
            this.red = red;
            this.green = green;
            this.blue = blue;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            boolean equals = false;

            if (obj instanceof ColorCacheKey) {
                ColorCacheKey other = (ColorCacheKey) obj;
                equals = display.equals(other.display) && (red == other.red)
                        && (green == other.green) && (blue == other.blue);
            }

            return equals;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            return display.hashCode() ^ (red << 24) ^ (green << 16) ^ blue;
        }
    }

    /**
     * A class wrapper to act as a key into the color cache
     */
    private static class FontCacheKey {

        private Display display;
        private int height;
        private String name;
        private int style;

        /**
         * Constructor
         * 
         * @param display
         * @param name
         * @param height
         * @param style
         */
        private FontCacheKey(Display display, String name, int height, int style) {
            this.display = display;
            this.name = name;
            this.height = height;
            this.style = style;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            boolean equals = false;

            if (obj instanceof FontCacheKey) {
                FontCacheKey other = (FontCacheKey) obj;
                equals = display.equals(other.display)
                        && name.equals(other.name) && (height == other.height)
                        && (style == other.style);
            }

            return equals;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            return display.hashCode() ^ name.hashCode() ^ (height << 16)
                    ^ style;
        }
    }

    // The shared instance.
    private static MTJUIPlugin plugin;

    /**
     * Display an error to the user given the specified information.
     * 
     * @param shell the parent shell of the dialog, or null if none
     * @param severity the severity; one of <code>OK</code>, <code>ERROR</code>,
     *            <code>INFO</code>, <code>WARNING</code>, or
     *            <code>CANCEL</code>
     * @param code the plug-in-specific status code, or <code>OK</code>
     * @param title the title to use for this dialog, or <code>null</code> to
     *            indicate that the default title should be used
     * @param message the message to show in the error dialog
     * @param exception a low-level exception, or <code>null</code> if not
     *            applicable
     */
    public static void displayError(Shell shell, int severity, int code,
            String title, String message, Throwable exception) {

        String id = getDefault().getBundle().getSymbolicName();

        Status status = new Status(severity, id, code, message, exception);

        /* Opens the error dialog to display the given error */
        ErrorDialog.openError(shell, title, message, status);
    }

    public static IWorkbenchPage getActivePage() {
        return getDefault().internalGetActivePage();
    }

    /**
     * Return the active window's shell.
     * 
     * @return the active window shell
     */
    public static Shell getActiveWindowShell() {
        MTJUIPlugin plugin = getDefault();
        IWorkbenchWindow activeWindow = plugin.getWorkbench()
                .getActiveWorkbenchWindow();

        return activeWindow.getShell();
    }

    public static Shell getActiveWorkbenchShell() {
        IWorkbenchWindow window = getActiveWorkbenchWindow();
        if (window != null) {
            return window.getShell();
        }
        return null;
    }

    public static IWorkbenchWindow getActiveWorkbenchWindow() {
        return getDefault().getWorkbench().getActiveWorkbenchWindow();
    }

    /**
     * Get a color from the cache.
     * 
     * @param display
     * @param red
     * @param green
     * @param blue
     * @return
     */
    public static Color getColor(Display display, int red, int green, int blue) {
        Map<ColorCacheKey, Resource> cache = getDefault().colorCache;
        ColorCacheKey key = new ColorCacheKey(display, red, green, blue);

        Color color = (Color) cache.get(key);
        if (color == null) {
            color = new Color(display, red, green, blue);
            cache.put(key, color);
        }

        return color;
    }

    /**
     * Return a preference store that sits on top of the MTJ core preferences
     * for the specified project.
     * 
     * @param context
     * @return
     */
    public static IPersistentPreferenceStore getCoreProjectPreferenceStore(
            IProject context) {
        ProjectScope projectScope = new ProjectScope(context);
        return new ScopedPreferenceStore(projectScope,
                IMTJCoreConstants.PLUGIN_ID);
    }

    /**
     * Returns the shared instance.
     */
    public static MTJUIPlugin getDefault() {
        return plugin;
    }

    /**
     * Return the dialog settings section within the specified parent settings
     * object. This method will create the new section if necessary.
     * 
     * @param dialogSettings
     * @param sectionName
     * @return
     */
    public static IDialogSettings getDialogSettings(
            IDialogSettings dialogSettings, String sectionName) {
        IDialogSettings settings = dialogSettings.getSection(sectionName);
        if (settings == null) {
            settings = dialogSettings.addNewSection(sectionName);
        }

        return settings;
    }

    /**
     * Return the dialog settings section within the plugin's root settings
     * object. This method will create the new section if necessary.
     * 
     * @param dialogSettings
     * @param sectionName
     * @return
     */
    public static IDialogSettings getDialogSettings(String sectionName) {
        return getDialogSettings(getDefault().getDialogSettings(), sectionName);
    }

    /**
     * Get the specified font from the cache.
     * 
     * @param display
     * @param name
     * @param height
     * @param style
     * @return
     */
    public static Font getFont(Display display, String name, int height,
            int style) {
        Map<FontCacheKey, Resource> cache = getDefault().fontCache;
        FontCacheKey key = new FontCacheKey(display, name, height, style);

        Font font = (Font) cache.get(key);
        if (font == null) {
            font = new Font(display, name, height, style);
            cache.put(key, font);
        }

        return font;
    }

    /**
     * Returns the symbolic name of MTJUIPlugin as specified by its
     * Bundle-SymbolicName manifest header.
     * 
     * @return the symbolic name of MTJUIPlugin.
     */
    public static String getPluginId() {
        return getDefault().getBundle().getSymbolicName();
    }

    public static IWorkspace getWorkspace() {
        return MTJCore.getWorkspace();
    }

    public static boolean isFullNameModeEnabled() {
        return false;
    }

    // Some caches
    private Map<ColorCacheKey, Resource> colorCache;

    // A preference store wrapper around the core plug-in preferences
    private IPreferenceStore corePreferenceStore;

    private FormToolkit dialogsFormToolkit;

    private FormColors fFormColors;

    private MTJLabelProvider fLabelProvider;

    private Map<FontCacheKey, Resource> fontCache;

    /**
     * The shared text file document provider.
     */
    private IDocumentProvider fTextFileDocumentProvider;

    /**
     * The constructor.
     */
    public MTJUIPlugin() {
        super();
        if (plugin == null) {
            plugin = this;
        }

        colorCache = new HashMap<ColorCacheKey, Resource>();
        fontCache = new HashMap<FontCacheKey, Resource>();
    }

    /**
     * Return an IPreferenceStore wrapper around the MTJ core plug-in
     * preferences.
     * 
     * @return
     */
    public IPreferenceStore getCorePreferenceStore() {
        if (corePreferenceStore == null) {
            corePreferenceStore = new PluginPreferenceStore(MTJCore.getPluginId());
        }

        return corePreferenceStore;
    }

    /**
     * Builds a GriData instance from the specified parameters.
     * 
     * @param horizontalAlignment Horizontal Alignment.
     * @param verticalAlignment Vertical Alignment.
     * @param grabExcessHorizontalSpace Grab excess horizontal space.
     * @param grabExcessVerticalSpace Grab excess vertical space.
     * @return a new GridData instance.
     */
    public static GridData buildGridData(int horizontalAlignment, int verticalAlignment
    		, boolean grabExcessHorizontalSpace, boolean grabExcessVerticalSpace) {
    	GridData gridData = new GridData();
    	gridData.horizontalAlignment = horizontalAlignment;
    	gridData.verticalAlignment 	 = verticalAlignment;
    	gridData.grabExcessHorizontalSpace = grabExcessHorizontalSpace;
    	gridData.grabExcessVerticalSpace = grabExcessVerticalSpace;
		return gridData;
	}
    
    /**
     * Builds a GriData instance from the specified parameters.
     * 
     * @param grabExcessHorizontalSpace Grab excess horizontal space.
     * @param grabExcessVerticalSpace Grab excess vertical space.
     * @return a new GridData instance.
     */
    public static GridData buildGridData(boolean grabExcessHorizontalSpace, boolean grabExcessVerticalSpace) {
		return buildGridData(SWT.LEFT, SWT.CENTER, grabExcessHorizontalSpace, grabExcessVerticalSpace);
	}
    
    public FormToolkit getDialogsFormToolkit() {
        if (dialogsFormToolkit == null) {
            FormColors colors = new FormColors(Display.getCurrent());
            colors.setBackground(null);
            colors.setForeground(null);
            dialogsFormToolkit = new FormToolkit(colors);
        }
        return dialogsFormToolkit;
    }

    public FormColors getFormColors(Display display) {
        if (fFormColors == null) {
            fFormColors = new FormColors(display);
            fFormColors.markShared();
        }
        return fFormColors;
    }

    public MTJLabelProvider getLabelProvider() {
        if (fLabelProvider == null) {
            fLabelProvider = new MTJLabelProvider();
        }
        return fLabelProvider;
    }

    /**
     * Returns the shared text file document provider for this plug-in.
     * 
     * @return the shared text file document provider
     * @since 0.9.1
     */
    public synchronized IDocumentProvider getTextFileDocumentProvider() {
        if (fTextFileDocumentProvider == null) {
            fTextFileDocumentProvider = new TextFileDocumentProvider();
        }
        return fTextFileDocumentProvider;
    }

    /**
     * This method is called upon plug-in activation
     * 
     * @param context the bundle context for this plug-in
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        
        // Dispose of the cached values
        Iterator<Resource> iter = colorCache.values().iterator();
        while (iter.hasNext()) {
            Color color = (Color) iter.next();
            color.dispose();
        }

        iter = fontCache.values().iterator();
        while (iter.hasNext()) {
            Font font = (Font) iter.next();
            font.dispose();
        }

        // Startup the OTA server if set to startup immediately
        IPreferenceStore store = getPreferenceStore();
        boolean startAtStart = store
                .getBoolean(IMTJCoreConstants.PREF_OTA_SERVER_START_AT_START);
        if (startAtStart) {
            startupOTAServer();
        }
        
        verifyDevices();
    }

    /**
     * This method is called when the plug-in is stopped
     * 
     * @param context the bundle context for this plug-in
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        try {
            if (dialogsFormToolkit != null) {
                dialogsFormToolkit.dispose();
                dialogsFormToolkit = null;
            }
        } finally {
            super.stop(context);
        }
    }

    private IWorkbenchPage internalGetActivePage() {
        IWorkbenchWindow window = getWorkbench().getActiveWorkbenchWindow();
        if (window == null) {
            return null;
        }
        return window.getActivePage();
    }

    /**
     * Start up the OTA server if the user requested it be started up
     * immediately.
     */
    private void startupOTAServer() {
        try {
            OTAServer.getInstance().start();
        } catch (Exception e) {
            MTJLogger.log(IStatus.ERROR, "startupOTAServer", e); //$NON-NLS-1$
        }
    }
    

    /**
     * Verifies if there are references to invalid devices, if it is true
     * prompts to the user to choose between remove that references or not
     */
    private void verifyDevices() {

        IDeviceRegistry deviceRegistry = MTJCore.getDeviceRegistry();

        if (deviceRegistry instanceof DeviceRegistry) {

        	List<String> sdkWithInvalidReferences = 
        		((DeviceRegistry) deviceRegistry).getInvalidSDKNames();

            if (!sdkWithInvalidReferences.isEmpty()) {

                boolean toRemove = showWarningInvalidReferences(sdkWithInvalidReferences);

                if (toRemove) {
                    try {
                        ((DeviceRegistry) deviceRegistry).updateReferences();
                    } catch (PersistenceException e) {
                        MTJLogger.log(IStatus.ERROR, e);
                    } catch (TransformerException e) {
                        MTJLogger.log(IStatus.ERROR, e);
                    } catch (IOException e) {
                        MTJLogger.log(IStatus.ERROR, e);
                    }
                }
            }
        }
    }

    /**
     * Creates the dialog that prompts to the user to choose between remove the
     * invalid references or not
     * 
     * @param sdkWithInvalidReferences - list of SDK that have invalid
     *            references
     * @return true if user chooses yes, false otherwise
     */
    private boolean showWarningInvalidReferences(final List<String> sdkWithInvalidReferences) {
        final Display display = Display.getDefault();
        final AtomicBoolean result = new AtomicBoolean(false);

        display.syncExec(new Runnable() {
            public void run() {
                String message = MTJUIMessages.MTJUIPlugin_missing_devices_text + "\n";

                for (String removedSDK : sdkWithInvalidReferences) {
                    message += "\n" + removedSDK; //$NON-NLS-1$
                }

                message += "\n\n" + MTJUIMessages.MTJUIPlugin_remove_invalid_references_question;

                result.set(MessageDialog.openQuestion(display.getActiveShell(),
                        MTJUIMessages.MTJUIPlugin_missing_devices_dialog_title, message));
            }
        });
        
        return result.get();
    }
}
