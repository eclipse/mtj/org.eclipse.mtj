/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Verison.
 *     Rafael Amaral (Motorola) - Changing to make MTJ extension point handle 
 *                                dynamic add/remove of plugins
 */
package org.eclipse.mtj.internal.ui.templates.midlets;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionDelta;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IRegistryChangeEvent;
import org.eclipse.core.runtime.IRegistryChangeListener;
import org.eclipse.core.runtime.Platform;

/**
 * MIDletTemplateRegistry class is the registry for the extensions
 * of the org.eclipse.mtj.ui.midletTemplate extension point.
 * 
 * @author David Marques
 * @since 1.0
 */
public class MIDletTemplateRegistry implements IRegistryChangeListener{

    // Constants -----------------------------------------------------

    private static final String TEMPLATE_EXT_ID = "org.eclipse.mtj.ui.midlettemplate";
    private static final String PLUGIN_ID = "org.eclipse.mtj.ui";
    private static final String EXT_TEMPLATE = "midlettemplate";
    
    // Attributes ----------------------------------------------------

    private static MIDletTemplateRegistry instance;
    private Map<String, MIDletTemplate> extensions;
    
    // Static --------------------------------------------------------

    /**
     * Gets the single instance of this class.
     * 
     * @return the singleton.
     */
    public static synchronized MIDletTemplateRegistry getInstance() {
        if (MIDletTemplateRegistry.instance == null) {
            MIDletTemplateRegistry.instance = new MIDletTemplateRegistry();
            Platform.getExtensionRegistry().addRegistryChangeListener(
                    MIDletTemplateRegistry.instance);
        }
        return MIDletTemplateRegistry.instance;
    }
    
    // Constructors --------------------------------------------------

    /**
     * Create a MIDletTemplateRegistry.
     */
    private MIDletTemplateRegistry() {
        IExtensionRegistry      registry   = null;
        IConfigurationElement[] extensions = null;
        MIDletTemplate    object     = null;
        
        this.extensions = new Hashtable<String, MIDletTemplate>();
        registry   = Platform.getExtensionRegistry();
        extensions = registry.getConfigurationElementsFor(TEMPLATE_EXT_ID);
        for (IConfigurationElement extension : extensions) {
            object = new MIDletTemplate(extension);
            this.extensions.put(object.getName(), object);
        }
    }
    
    // Public --------------------------------------------------------

    /**
     * Gets all registered templates.
     * 
     * @return template collection.
     */
    public Collection<MIDletTemplate> getTemplates() {
        return this.extensions.values();
    }
    
    /**
     * Gets the template with the specified name.
     * 
     * @param name - template name.
     * @return the template.
     */
    public MIDletTemplate getTemplate(String name) {
        return this.extensions.get(name);
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.core.runtime.IRegistryChangeListener#registryChanged(org.
     * eclipse.core.runtime.IRegistryChangeEvent)
     */
    public void registryChanged(IRegistryChangeEvent event) {

        IExtensionDelta[] deltas = event.getExtensionDeltas(PLUGIN_ID,
                EXT_TEMPLATE);
        
        for (int i = 0; i < deltas.length; i++) {
            
            if (deltas[i].getKind() == IExtensionDelta.ADDED) {
                
                IConfigurationElement[] elements = deltas[i].getExtension()
                        .getConfigurationElements();
                
                for (IConfigurationElement element : elements) {
                    MIDletTemplate object = new MIDletTemplate(
                            element);
                    this.extensions.put(object.getName(), object);
                }
            } else {
                
                IConfigurationElement[] elements = deltas[i].getExtension()
                        .getConfigurationElements();
                
                for (IConfigurationElement element : elements) {
                    MIDletTemplate object = new MIDletTemplate(
                            element);
                    this.extensions.remove(object.getName());
                }
            }
        }
    }
    
    // X implementation ----------------------------------------------

    // Y overrides ---------------------------------------------------

    // Package protected ---------------------------------------------

    // Protected -----------------------------------------------------

    // Private -------------------------------------------------------
}
