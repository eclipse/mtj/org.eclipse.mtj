/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Fixing page scrolling.
 *     David Marques (Motorola) - Updating to support signing enhancements.
 *     David Marques (Motorola) - Updating to reload aliases after external changes.
 *     David Marques (Motorola) - Avoiding disabling the permissions list upon disabling
 *                                signing.
 *     David Marques (Motorola) - Adding support for certificates.
 */
package org.eclipse.mtj.internal.ui.editors.jad.form.pages;

import java.io.File;
import java.util.*;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProjectListener;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.core.build.sign.SignatureProperties;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.sdk.device.midp.PackagingModel;
import org.eclipse.mtj.internal.core.sdk.device.midp.Profile;
import org.eclipse.mtj.internal.core.security.meep.MEEPPermissionsProvider;
import org.eclipse.mtj.internal.core.security.meep.MEEPPermissionsProvider.PermissionDefinition;
import org.eclipse.mtj.internal.core.sign.*;
import org.eclipse.mtj.internal.core.sign.KeyStoreEntry.Type;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.dialog.MEEPSecurityPermissionsDialog;
import org.eclipse.mtj.internal.ui.dialog.SecurityPermissionsDialog;
import org.eclipse.mtj.internal.ui.dialog.SecurityPermissionsDialog.PermissionNode;
import org.eclipse.mtj.internal.ui.dialog.SigningPasswordDialog;
import org.eclipse.mtj.internal.ui.editors.jad.form.JADFormEditor;
import org.eclipse.mtj.internal.ui.editors.jad.form.pages.OverviewEditorPage.ActiveDeviceChangeListener;
import org.eclipse.mtj.internal.ui.editors.jad.form.pages.OverviewEditorPage.DeviceProfileChangeListener;
import org.eclipse.mtj.internal.ui.editors.jad.form.pages.OverviewEditorPage.PackagingModelChangeListener;
import org.eclipse.mtj.internal.ui.forms.blocks.ButtonBarBlock;
import org.eclipse.mtj.internal.ui.forms.blocks.GenericListBlock;
import org.eclipse.mtj.internal.ui.forms.blocks.GenericListBlockItem;
import org.eclipse.mtj.internal.ui.forms.blocks.GenericListBlockListener;
import org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.dialogs.CheckedTreeSelectionDialog;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

/**
 * SigningJADEditorPage class adds a signing page to the JAD editor.
 * 
 * @author David Marques
 * @since 1.0
 */
public class SigningJADEditorPage extends AbstractJADEditorPage implements ActiveDeviceChangeListener, DeviceProfileChangeListener, PackagingModelChangeListener {

    private static enum ListId {
        Optional, Required
    }

    private class SecurityPermissionListItem implements GenericListBlockItem {
        private String permission;
        
        private boolean permissionClass;

        /**
         * Creates a new instance of SecurityPermissionListItem.
         * 
         * @param _permission
         */
        public SecurityPermissionListItem(String permission, boolean permissionClass) {
            this.permission = permission;
            this.permissionClass = permissionClass;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        public boolean equals(Object obj) {
            if (obj instanceof SecurityPermissionListItem) {
                SecurityPermissionListItem other = (SecurityPermissionListItem) obj;
                return this.getText().equals(other.getText());
            } else {
                return super.equals(obj);
            }
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.forms.blocks.GenericListBlockItem#getImage()
         */
        public Image getImage() {
            return MTJUIPluginImages.DESC_PERMISSION_OBJ.createImage();
        }

        /* (non-Javadoc)
         * @see org.eclipse.mtj.internal.ui.forms.blocks.GenericListBlockItem#getText()
         */
        public String getText() {
            return this.permission;
        }
        
        public void setPermission(String permission) {
            this.permission = permission;
        }
        
        public void setPermissionClass(boolean permissionClass) {
            this.permissionClass = permissionClass;
        }

        public boolean isPermissionClass() {
            return permissionClass;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        public int hashCode() {
            String text = this.getText();
            int hash = 0x00;
            for (int i = 0; i < text.length(); i++) {
                hash += text.charAt(i);
            }
            return hash;
        }
    }

    public static final String ID = "signing"; //$NON-NLS-1$
    private static final String COMMA = ","; //$NON-NLS-1$

    private static final String TITLE = MTJUIMessages.PermissionsPage_title;
    private Combo combo;
    private Button enableButton;
    private GenericListBlock<SecurityPermissionListItem> optPermissionBlock;
    private List<SecurityPermissionListItem> optMIDPPermissions;
    private List<SecurityPermissionListItem> optMEEPPermissions;
    private IMidletSuiteProjectListener projectListener;

    private GenericListBlock<SecurityPermissionListItem> reqPermissionBlock;

    private List<SecurityPermissionListItem> reqMIDPPermissions;
    private List<SecurityPermissionListItem> reqMEEPPermissions;

    private MEEPPermissionsProvider meepPermissionsProvider;
    private PackagingModel packagingModel; 
    private boolean useMEEPSecurity;
    

    /**
     * Creates a SigningJADEditorPage instance.
     * 
     * @param editor the parent editor
     */
    public SigningJADEditorPage(JADFormEditor editor, OverviewEditorPage overviewEditorPage) {
        super(editor, ID, TITLE);

        IMidletSuiteProject midletSuiteProject = MidletSuiteFactory.getMidletSuiteProject(getJavaProject());

        ISDK sdk = null;
        MTJRuntimeList runtimeList = midletSuiteProject.getRuntimeList();
        
        if (runtimeList != null) {
            MTJRuntime activeMTJRuntime = runtimeList.getActiveMTJRuntime();
            
            if (activeMTJRuntime != null) {
                IDevice device = activeMTJRuntime.getDevice();
                
                if (device != null) {
                    sdk = device.getSDK();
                }
            }
        }
        
        meepPermissionsProvider = new MEEPPermissionsProvider(sdk);
        packagingModel = midletSuiteProject.getProjectPackagingModel();
        useMEEPSecurity = Profile.MEEP_80.toString().equals(
                midletSuiteProject.getApplicationDescriptor().getMicroEditionProfile());
        
        overviewEditorPage.addActiveDeviceChangeListener(this);
        overviewEditorPage.addDeviceProfileChangeListener(this);
        overviewEditorPage.addPackagingModelChangeListener(this);
        
        reqMIDPPermissions = new LinkedList<SecurityPermissionListItem>();
        reqMEEPPermissions = new LinkedList<SecurityPermissionListItem>();
        optMIDPPermissions = new LinkedList<SecurityPermissionListItem>();
        optMEEPPermissions = new LinkedList<SecurityPermissionListItem>();
        listenForProjectSignatureUpdates();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#dispose()
     */
    public void dispose() {
        super.dispose();
        IMidletSuiteProject midletSuiteProject = MidletSuiteFactory
                .getMidletSuiteProject(getJavaProject(), false);
        if (midletSuiteProject != null) {
            midletSuiteProject.removeMTJProjectListener(this.projectListener);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    public void doSave(IProgressMonitor monitor) {
        IPreferenceStore store = getPreferenceStore();
        
        clearClassPermissionsFromStore(store,
                collectAndNormalizePropertiesWithPrefix(IJADConstants.JAD_MIDLET_PERMISSION).keySet());
        clearClassPermissionsFromStore(store,
                collectAndNormalizePropertiesWithPrefix(IJADConstants.JAD_MIDLET_PERMISSION_OPTIONAL).keySet());
        clearClassPermissionsFromStore(store,
                collectAndNormalizePropertiesWithPrefix(IJADConstants.JAD_LIBLET_PERMISSION).keySet());
        clearClassPermissionsFromStore(store,
                collectAndNormalizePropertiesWithPrefix(IJADConstants.JAD_LIBLET_PERMISSION_OPTIONAL).keySet());
        
        List<String> reqClassPermissionsSpecs = new ArrayList<String>();
        List<String> optClassPermissionsSpecs = new ArrayList<String>();
        List<String> reqNonClassPermissionsSpecs = new ArrayList<String>();
        List<String> optNonClassPermissionsSpecs = new ArrayList<String>();

        List<SecurityPermissionListItem> reqPermissions = useMEEPSecurity ? reqMEEPPermissions : reqMIDPPermissions;
        List<SecurityPermissionListItem> optPermissions = useMEEPSecurity ? optMEEPPermissions : optMIDPPermissions;
        
        for (SecurityPermissionListItem permission : reqPermissions) {
            String permissionSpec = permission.getText().trim();
            
            if (permission.isPermissionClass()) {
                reqClassPermissionsSpecs.add(permissionSpec);
            } else {
                reqNonClassPermissionsSpecs.add(permissionSpec);
            }
        }
        
        for (SecurityPermissionListItem permission : optPermissions) {
            String permissionSpec = permission.getText().trim();
            
            if (permission.isPermissionClass()) {
                optClassPermissionsSpecs.add(permissionSpec);
            } else {
                optNonClassPermissionsSpecs.add(permissionSpec);
            }
        }
        
        if (useMEEPSecurity) {
            saveClassPermissionsToStore(store,
                    packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_MIDLET_PERMISSION
                            : IJADConstants.JAD_LIBLET_PERMISSION, reqClassPermissionsSpecs);
            saveClassPermissionsToStore(store,
                    packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_MIDLET_PERMISSION_OPTIONAL
                            : IJADConstants.JAD_LIBLET_PERMISSION_OPTIONAL, optClassPermissionsSpecs);
        }

        saveNonClassPermissionsToStore(store,
                packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_MIDLET_PERMISSIONS
                        : IJADConstants.JAD_LIBLET_PERMISSIONS, reqNonClassPermissionsSpecs);
        
        saveNonClassPermissionsToStore(store,
                packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_MIDLET_PERMISSIONS_OPTIONAL
                        : IJADConstants.JAD_LIBLET_PERMISSIONS_OPTIONAL, optNonClassPermissionsSpecs);

        this.updateProjectSigningState();
        
        setDirty(false);
    }
    
    private void clearClassPermissionsFromStore(IPreferenceStore store, Set<String> permissionJADKeys) {
        for (String permissionJADKey : permissionJADKeys) {
            store.setToDefault(permissionJADKey);;
        }
    }
    
    private void saveClassPermissionsToStore(IPreferenceStore store, String permissionsJADKeyPrefix,
            Collection<String> permissionSpecs) {
        int idx = 1;

        for (String permissionSpec : permissionSpecs) {
            store.setValue(permissionsJADKeyPrefix + idx, permissionSpec);

            idx = idx + 1;
        }
    }
    
    private List<String> collectNonClassPermissions(IPreferenceStore store, String permissionsJADKey) {
        List<String> result = new ArrayList<String>();

        if (store.contains(permissionsJADKey)) {
            String permissions = store.getString(permissionsJADKey);

            if (permissions.length() > 0) {
                String[] permissionsSplit = permissions.split(COMMA);

                for (String permission : permissionsSplit) {
                    result.add(permission.trim());
                }
            }
        }

        return result;
    }
    
    private void saveNonClassPermissionsToStore(IPreferenceStore store, String permissionsJADKey,
            Collection<String> permissionSpecs) {
        StringBuilder toSave = new StringBuilder();

        for (String permissionSpec : permissionSpecs) {
            if (toSave.length() > 0) {
                toSave.append(COMMA);
            }
            
            toSave.append(permissionSpec);
        }
        
        store.setValue(permissionsJADKey, toSave.toString());
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#editorInputChanged()
     */
    public void editorInputChanged() {
        IPreferenceStore store = getPreferenceStore();
        
        reqMEEPPermissions.clear();
        reqMIDPPermissions.clear();
        optMEEPPermissions.clear();
        optMIDPPermissions.clear();
        
        String reqPermissionJADKeyPrefix = packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_MIDLET_PERMISSION
                : IJADConstants.JAD_LIBLET_PERMISSION;
        String notRelevantReqPermissionJADKeyPrefix = packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_LIBLET_PERMISSION
                : IJADConstants.JAD_MIDLET_PERMISSION;

        String optPermissionJADKeyPrefix = packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_MIDLET_PERMISSION_OPTIONAL
                : IJADConstants.JAD_LIBLET_PERMISSION_OPTIONAL;
        String notRelevantOptPermissionJADKeyPrefix = packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_LIBLET_PERMISSION_OPTIONAL
                : IJADConstants.JAD_MIDLET_PERMISSION_OPTIONAL;
        
        // Load permissions for all packaging models, but clear permissions for 
        // not relevant packaging model or duplicate permissions in preference store
        
        LinkedHashMap<String, String> reqClassPermission = collectAndNormalizePropertiesWithPrefix(reqPermissionJADKeyPrefix);
        LinkedHashMap<String, String> notRelavantReqClassPermission = collectAndNormalizePropertiesWithPrefix(notRelevantReqPermissionJADKeyPrefix);
                
        LinkedHashMap<String, String> optClassPermission = collectAndNormalizePropertiesWithPrefix(optPermissionJADKeyPrefix);
        LinkedHashMap<String, String> notRelavantOptClassPermission = collectAndNormalizePropertiesWithPrefix(notRelevantOptPermissionJADKeyPrefix);
        
        LinkedHashSet<String> reqClassPermissionsSpecs = new LinkedHashSet<String>(reqClassPermission.values());
        
        if (!useMEEPSecurity || reqClassPermissionsSpecs.size() < reqClassPermission.size()) {
            clearClassPermissionsFromStore(store, reqClassPermission.keySet());
        }
        
        LinkedHashSet<String> optClassPermissionsSpecs = new LinkedHashSet<String>();
        
        for (String permissionSpec : optClassPermission.values()) {
            if (!reqClassPermissionsSpecs.contains(permissionSpec)) {
                optClassPermissionsSpecs.add(permissionSpec);
            }
        }
        
        if (!useMEEPSecurity || optClassPermissionsSpecs.size() < optClassPermission.size()) {
            clearClassPermissionsFromStore(store, optClassPermission.keySet());
        }
        
        if (notRelavantReqClassPermission.size() > 0) {
            for (String permissionSpec : notRelavantReqClassPermission.values()) {
                if (!optClassPermissionsSpecs.contains(permissionSpec)) {
                    reqClassPermissionsSpecs.add(permissionSpec);
                }
            }

            clearClassPermissionsFromStore(store, notRelavantReqClassPermission.keySet());
        }
        
        if (notRelavantOptClassPermission.size() > 0) {
            for (String permissionSpec : notRelavantOptClassPermission.values()) {
                if (!reqClassPermissionsSpecs.contains(permissionSpec)) {
                    optClassPermissionsSpecs.add(permissionSpec);
                }
            }

            clearClassPermissionsFromStore(store, notRelavantOptClassPermission.keySet());
        }
        
        String reqPermissionJADKey = packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_MIDLET_PERMISSIONS
                : IJADConstants.JAD_LIBLET_PERMISSIONS;
        String notRelevantReqPermissionJADKey = packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_LIBLET_PERMISSIONS
                : IJADConstants.JAD_MIDLET_PERMISSIONS;

        String optPermissionJADKey = packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_MIDLET_PERMISSIONS_OPTIONAL
                : IJADConstants.JAD_LIBLET_PERMISSIONS_OPTIONAL;
        String notRelevantOptPermissionJADKey = packagingModel == PackagingModel.MIDLET ? IJADConstants.JAD_LIBLET_PERMISSIONS_OPTIONAL
                : IJADConstants.JAD_MIDLET_PERMISSIONS_OPTIONAL;
        
        // Load permissions for all packaging models, but clear permissions for 
        // not relevant packaging model or duplicate permissions in preference store
        
        List<String> reqNonClassPermission = collectNonClassPermissions(store, reqPermissionJADKey);
        List<String> notRelavantReqNonClassPermission = collectNonClassPermissions(store, notRelevantReqPermissionJADKey);
                
        List<String> optNonClassPermission = collectNonClassPermissions(store, optPermissionJADKey);
        List<String> notRelavantOptNonClassPermission = collectNonClassPermissions(store, notRelevantOptPermissionJADKey);
        
        LinkedHashSet<String> reqNonClassPermissionsSpecs = new LinkedHashSet<String>(reqNonClassPermission);
        
        if (reqNonClassPermissionsSpecs.size() < reqNonClassPermission.size()) {
            store.setToDefault(reqPermissionJADKey);
        }
        
        LinkedHashSet<String> optNonClassPermissionsSpecs = new LinkedHashSet<String>();
        
        for (String permissionSpec : optNonClassPermission) {
            if (!reqNonClassPermissionsSpecs.contains(permissionSpec)) {
                optNonClassPermissionsSpecs.add(permissionSpec);
            }
        }
        
        if (optNonClassPermissionsSpecs.size() < optNonClassPermission.size()) {
            store.setToDefault(optPermissionJADKey);
        }
        
        if (notRelavantReqNonClassPermission.size() > 0) {
            for (String permissionSpec : notRelavantReqNonClassPermission) {
                if (!optNonClassPermissionsSpecs.contains(permissionSpec)) {
                    reqNonClassPermissionsSpecs.add(permissionSpec);
                }
            }

            store.setToDefault(notRelevantReqPermissionJADKey);
        }
        
        if (notRelavantOptNonClassPermission.size() > 0) {
            for (String permissionSpec : notRelavantOptNonClassPermission) {
                if (!reqNonClassPermissionsSpecs.contains(permissionSpec)) {
                    optNonClassPermissionsSpecs.add(permissionSpec);
                }
            }

            store.setToDefault(notRelevantOptPermissionJADKey);
        }
        
        for (String permissionSpec : reqClassPermissionsSpecs) {
            reqMEEPPermissions.add(new SecurityPermissionListItem(permissionSpec, true));
        }
        
        for (String permissionSpec : optClassPermissionsSpecs) {
            optMEEPPermissions.add(new SecurityPermissionListItem(permissionSpec, true));
        }
        
        if (store.needsSaving()) {
            if (useMEEPSecurity) {
                saveClassPermissionsToStore(store, reqPermissionJADKeyPrefix, reqClassPermissionsSpecs);
                saveClassPermissionsToStore(store, optPermissionJADKeyPrefix, optClassPermissionsSpecs);
            }
            
            saveNonClassPermissionsToStore(store, reqPermissionJADKey, reqNonClassPermissionsSpecs);
            saveNonClassPermissionsToStore(store, optPermissionJADKey, optNonClassPermissionsSpecs);
        }
        
        if (useMEEPSecurity) {
            for (String permissionSpec : reqNonClassPermissionsSpecs) {
                reqMEEPPermissions.add(new SecurityPermissionListItem(permissionSpec, false));
            }
            
            for (String permissionSpec : optNonClassPermissionsSpecs) {
                optMEEPPermissions.add(new SecurityPermissionListItem(permissionSpec, false));
            }
        } else {
            for (String permissionSpec : reqNonClassPermissionsSpecs) {
                reqMIDPPermissions.add(new SecurityPermissionListItem(permissionSpec, false));
            }
            
            for (String permissionSpec : optNonClassPermissionsSpecs) {
                optMIDPPermissions.add(new SecurityPermissionListItem(permissionSpec, false));
            }
        }
        
        if (this.reqPermissionBlock != null) {
            this.reqPermissionBlock.update(null);
        }
        if (this.optPermissionBlock != null) {
            this.optPermissionBlock.update(null);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getTitle()
     */
    public String getTitle() {
        return TITLE;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#isManagingProperty(java.lang.String)
     */
    public boolean isManagingProperty(String property) {
        if (property == null) {
            return false;
        }
        
        String[] managedProperties = new String[] { IJADConstants.JAD_MIDLET_PERMISSIONS,
                IJADConstants.JAD_MIDLET_PERMISSIONS_OPTIONAL, IJADConstants.JAD_LIBLET_PERMISSIONS,
                IJADConstants.JAD_LIBLET_PERMISSIONS_OPTIONAL };
        
        String[] managedPropertyPrefixes = new String[] { IJADConstants.JAD_MIDLET_PERMISSION,
                IJADConstants.JAD_MIDLET_PERMISSION_OPTIONAL, IJADConstants.JAD_LIBLET_PERMISSION,
                IJADConstants.JAD_LIBLET_PERMISSION_OPTIONAL };
    
        for (String managedProperty : managedProperties) {
            if (managedProperty.equals(property)) {
                return true;
            }
        }
    
        for (String managedPropertyPrefix : managedPropertyPrefixes) {
            if (property.startsWith(managedPropertyPrefix)) {
                String value = property.substring(managedPropertyPrefix.length());
                
                try {
                    if (Integer.parseInt(value) > 0) {
                        return true;
                    }
                } catch (NumberFormatException e) {}
            }
        }
        
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.forms.editor.FormPage#setActive(boolean)
     */
    public void setActive(boolean active) {
        super.setActive(active);
        if (active) {
            this.getErrorMessageManager().removeAllMessages();
        }
    }
    
    private void editMEEPPermission(ListId listId, SecurityPermissionListItem itemToEdit) {
        GenericListBlock<SecurityPermissionListItem> block = null;

        switch (listId) {
            case Required:
                block = reqPermissionBlock;
                break;
            case Optional:
                block = optPermissionBlock;
                break;
            default:
                return;
        }

        List<String> existingPermissions = new ArrayList<String>();

        for (SecurityPermissionListItem item : reqMEEPPermissions) {
            existingPermissions.add(item.getText());
        }
        
        for (SecurityPermissionListItem item : optMEEPPermissions) {
            existingPermissions.add(item.getText());
        }

        PermissionDefinition permission = MEEPSecurityPermissionsDialog.getPermission(getPartControl().getShell(),
                meepPermissionsProvider, existingPermissions, itemToEdit.getText());

        if (permission == null) {
            return;
        }

        itemToEdit.setPermission(permission.toString());
        itemToEdit.setPermissionClass(permission.isPermissionClass());

        setDirty(true);

        block.update(null);
    }

    private void addPermissions(ListId listId) {
        Object[] result = null;
        Shell parentShell = getPartControl().getShell();
        GenericListBlock<SecurityPermissionListItem> block = null;
        List<SecurityPermissionListItem> list = null;

        switch (listId) {
            case Required:
                block = reqPermissionBlock;
                list = useMEEPSecurity ? reqMEEPPermissions : reqMIDPPermissions;
                break;
            case Optional:
                block = optPermissionBlock;
                list = useMEEPSecurity ? optMEEPPermissions : optMIDPPermissions;
                break;
            default:
                return;
        }

        if (useMEEPSecurity) {
            List<String> existingPermissions = new ArrayList<String>();

            for (SecurityPermissionListItem item : reqMEEPPermissions) {
                existingPermissions.add(item.getText());
            }
            
            for (SecurityPermissionListItem item : optMEEPPermissions) {
                existingPermissions.add(item.getText());
            }

            PermissionDefinition permission = MEEPSecurityPermissionsDialog.getPermission(parentShell,
                    meepPermissionsProvider, existingPermissions, null);

            if (permission != null) {
                result = new Object[] { permission };
            }
        } else {

            CheckedTreeSelectionDialog dialog = SecurityPermissionsDialog.createDialog(parentShell);

            if (dialog.open() == Dialog.OK) {
                result = dialog.getResult();
            }
        }

        if (result != null) {
            addPermissionsToList(list, result, block);
        }
    }

    /**
     * Adds the permission to the specified list.
     * 
     * @param permissionsList target list.
     * @param result selected items.
     */
    private void addPermissionsToList(List<SecurityPermissionListItem> permissionsList, Object[] result,
            GenericListBlock<SecurityPermissionListItem> block) {
        List<SecurityPermissionListItem> newPermissions = new ArrayList<SigningJADEditorPage.SecurityPermissionListItem>();

        for (Object object : result) {
            PermissionNode midpPermission = object instanceof PermissionNode ? (PermissionNode) object : null;
            PermissionDefinition meepPermission = object instanceof PermissionDefinition ? (PermissionDefinition) object
                    : null;

            if (midpPermission != null || meepPermission != null) {
                boolean isNew = true;
                String permission = midpPermission == null ? meepPermission.toString() : midpPermission.getPermission();

                for (SecurityPermissionListItem item : (useMEEPSecurity ? reqMEEPPermissions : reqMIDPPermissions)) {
                    if (item.getText().equals(permission)) {
                        isNew = false;
                    }
                }
                
                if (isNew) {
                    for (SecurityPermissionListItem item : (useMEEPSecurity ? optMEEPPermissions : optMIDPPermissions)) {
                        if (item.getText().equals(permission)) {
                            isNew = false;
                        }
                    }
                }
                
                if (isNew) {
                    SecurityPermissionListItem newPermission = new SecurityPermissionListItem(permission,
                            meepPermission == null ? false : meepPermission.isPermissionClass());

                    permissionsList.add(newPermission);
                    newPermissions.add(newPermission);
                }
            }
        }

        if (!newPermissions.isEmpty()) {
            setDirty(true);

            block.update(new StructuredSelection(newPermissions));
        }
    }

    /**
     * Creates the section for the alias selection.
     * 
     * @param managedForm parent form.
     * @param body parent composite.
     */
    private void createAliasSection(IManagedForm managedForm, Composite body) {
        FormToolkit toolkit = managedForm.getToolkit();

        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gridData.horizontalSpan = 2;

        Composite sectionBody = createSection(managedForm, body,
                MTJUIMessages.PermissionsPage_signProperties, null, gridData);
        sectionBody.setLayout(new GridLayout(2, false));

        toolkit
                .createLabel(sectionBody,
                        MTJUIMessages.PermissionsPage_keyAlias);
        combo = new Combo(sectionBody, SWT.DROP_DOWN | SWT.READ_ONLY);
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        combo.setLayoutData(gridData);
        combo.setFocus();
        combo.addSelectionListener(new SelectionListener() {

            public void widgetDefaultSelected(SelectionEvent e) {
            }

            public void widgetSelected(SelectionEvent e) {
                setDirty(true);
            }
        });
    }

    /**
     * Creates the section for the permissions lists.
     * 
     * @param managedForm parent form.
     * @param body parent composite.
     */
    private void createListsSection(IManagedForm managedForm, Composite body) {
        Composite reqSectionBody = createSection(managedForm, body,
                MTJUIMessages.PermissionsPage_requiredPermissionsTitle,
                MTJUIMessages.PermissionsPage_requiredPermissionsMessage,
                new GridData(SWT.FILL, SWT.FILL, true, true));
        reqSectionBody.setLayout(new GridLayout(0x01, false));

        reqPermissionBlock = new GenericListBlock<SecurityPermissionListItem>(reqSectionBody, SWT.BORDER | SWT.MULTI,
                ButtonBarBlock.BUTTON_ALL, useMEEPSecurity ? reqMEEPPermissions : reqMIDPPermissions);
        
        reqPermissionBlock.setButtonVisible(ButtonBarBlock.BUTTON_EDIT_INDEX, useMEEPSecurity);
        reqPermissionBlock.setButtonVisible(ButtonBarBlock.BUTTON_SCAN_INDEX, !useMEEPSecurity);
        
        reqPermissionBlock
                .addGenericListBlockListener(new GenericListBlockListener<SecurityPermissionListItem>() {

                    public void addButtonPressed() {
                        addPermissions(ListId.Required);
                    }
                    
                    public void editButtonPressed(SecurityPermissionListItem item) {
                        editMEEPPermission(ListId.Required, item);
                    }

                    public void downButtonPressed() {
                        setDirty(true);
                    }

                    public void itemsRemoved(
                            List<SecurityPermissionListItem> _items) {
                        setDirty(true);
                    }

                    public void scan() {
                        scanProjectClasses(ListId.Required);
                    }

                    public void upButtonPressed() {
                        setDirty(true);
                    }
                });

        Composite optSectionBody = createSection(managedForm, body,
                MTJUIMessages.PermissionsPage_optionalPermissionsTitle,
                MTJUIMessages.PermissionsPage_optionalPermissionsMessage,
                new GridData(SWT.FILL, SWT.FILL, true, true));
        optSectionBody.setLayout(new GridLayout(0x01, false));

        optPermissionBlock = new GenericListBlock<SecurityPermissionListItem>(optSectionBody, SWT.BORDER | SWT.MULTI,
                ButtonBarBlock.BUTTON_ALL, useMEEPSecurity ? optMEEPPermissions : optMIDPPermissions);
        
        optPermissionBlock.setButtonVisible(ButtonBarBlock.BUTTON_EDIT_INDEX, useMEEPSecurity);
        optPermissionBlock.setButtonVisible(ButtonBarBlock.BUTTON_SCAN_INDEX, !useMEEPSecurity);
        
        optPermissionBlock
                .addGenericListBlockListener(new GenericListBlockListener<SecurityPermissionListItem>() {

                    public void addButtonPressed() {
                        addPermissions(ListId.Optional);
                    }
                    
                    public void editButtonPressed(SecurityPermissionListItem item) {
                        editMEEPPermission(ListId.Optional, item);
                    }

                    public void downButtonPressed() {
                        setDirty(true);
                    }

                    public void itemsRemoved(
                            List<SecurityPermissionListItem> _items) {
                        setDirty(true);
                    }

                    public void scan() {
                        scanProjectClasses(ListId.Optional);
                    }

                    public void upButtonPressed() {
                        setDirty(true);
                    }
                });

    }

    /**
     * Creates a generic section.
     * 
     * @param managedForm parent form.
     * @param body parent composite.
     */
    private Composite createSection(IManagedForm managedForm,
            Composite listsParent, String text, String description,
            GridData gridData) {
        FormToolkit toolkit = managedForm.getToolkit();

        Section section = toolkit.createSection(listsParent,
                Section.DESCRIPTION | ExpandableComposite.TITLE_BAR);
        if (text != null) {
            section.setText(text);
        }
        if (description != null) {
            section.setDescription(description);
        }
        section.setLayoutData(gridData);
        section.setLayout(new GridLayout(0x01, false));

        Composite client = toolkit.createComposite(section, SWT.NONE);
        client.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        section.setClient(client);
        return client;
    }

    /**
     * Creates the section for the top section.
     * 
     * @param managedForm parent form.
     * @param body parent composite.
     */
    private void createTopSection(IManagedForm managedForm, Composite body) {
        FormToolkit toolkit = managedForm.getToolkit();
        enableButton = toolkit.createButton(body,
                MTJUIMessages.PermissionsPage_signPackage, SWT.CHECK);
        enableButton.addSelectionListener(new SelectionListener() {

            public void widgetDefaultSelected(SelectionEvent e) {
            }

            public void widgetSelected(SelectionEvent e) {
                updateWidgetsStates(enableButton.getSelection());
                setDirty(true);
            }
        });

        ISignatureProperties signProperties = getProjectSignatureProperties();
        if (signProperties != null) {
            enableButton.setSelection(signProperties.isSignProject());
        }

        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        gridData.horizontalSpan = 2;
        enableButton.setLayoutData(gridData);
    }

    /**
     * Gets the signature properties for the project.
     * 
     * @return ISignatureProperties instance.
     */
    private ISignatureProperties getProjectSignatureProperties() {
        ISignatureProperties signProperties = null;
        IMidletSuiteProject midletProject = null;

        try {
            midletProject = MidletSuiteFactory
                    .getMidletSuiteProject(getJavaProject());
            signProperties = midletProject.getSignatureProperties();
        } catch (CoreException e) {
            String message = NLS.bind(
                    "Unable to get project signing properties: {0}", e
                            .getMessage());
            MTJLogger.log(IStatus.ERROR, message);
            getErrorMessageManager().addMessage(message, message, null,
                    IMessageProvider.ERROR);
        }
        return signProperties;
    }

    /**
     * List all aliases into the combo box.
     * 
     * @param combo target combo.
     * @throws KeyStoreManagerException
     */
    private void listAliases(final Combo combo) {
        Display.getDefault().asyncExec(new Runnable() {
            public void run() {
                ISignatureProperties properties = null;
                String password = null;
                String path = null;
                File file = null;

                combo.removeAll();
                properties = getProjectSignatureProperties();
                if (properties == null) {
                    return;
                }

                try {
                    path = properties.getAbsoluteKeyStorePath(getJavaProject()
                            .getProject());
                    if (path == null) {
                        return;
                    }
                } catch (CoreException e) {
                    MTJLogger
                            .log(IStatus.ERROR, NLS.bind(
                                    "Unable to get keystore path: {0}", e
                                            .getMessage()));
                }

                file = new File(path);
                if (!file.exists() || !file.isFile()) {
                    return;
                }

                if (properties.getPasswordStorageMethod() == SignatureProperties.PASSMETHOD_PROMPT) {
                    SigningPasswordDialog dialog = new SigningPasswordDialog(
                            getPartControl().getShell(), false);
                    dialog.setTitle("KeyStore Password");
                    if (dialog.open() == Dialog.OK) {
                        password = dialog.getPassword();
                    } else {
                        return;
                    }
                } else {
                    password = properties.getKeyStorePassword();
                }

                try {
                    IKeyStoreManager manager = new DefaultKeyStoreManager(file,
                            password);
                    manager.setProvider(properties.getKeyStoreProvider());
                    manager.setKeystoreType(properties.getKeyStoreType());

                    List<KeyStoreEntry> entries = manager.getEntries();
                    for (KeyStoreEntry entry : entries) {
                    	if (entry.getType() == Type.KEY_PAIR) {							
                    		combo.add(entry.getAlias());
						}
                    }
                } catch (KeyStoreManagerException e) {
                    String message = NLS.bind(
                            "Unable to get keystore aliases: {0}", e
                                    .getMessage());
                    MTJLogger.log(IStatus.ERROR, message);
                    getErrorMessageManager().addMessage(message, message, null,
                            IMessageProvider.ERROR);
                } catch (Exception e) {
                    String message = e.getMessage();
                    MTJLogger.log(IStatus.ERROR, message);
                    getErrorMessageManager().addMessage(message, message, null,
                            IMessageProvider.ERROR);
                }
                String alias = properties.getKeyAlias();
                // In case no alias is selected yet return;
                if (alias == null) {
                    return;
                }
                for (int i = 0; i < combo.getItemCount(); i++) {
                    if (alias.equals(combo.getItem(i))) {
                        combo.select(i);
                    }
                }
            }
        });
    }

    /**
     * Adds a listener for the suite to update aliases upon external changes.
     */
    private void listenForProjectSignatureUpdates() {
        this.projectListener = new IMidletSuiteProjectListener() {

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.IMTJProjectListener#classpathRefreshed()
             */
            public void classpathRefreshed() {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProjectListener#jadFileNameChanged()
             */
            public void jadFileNameChanged() {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.IMTJProjectListener#metaDataSaved()
             */
            public void metaDataSaved() {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.IMTJProjectListener#packageCreated()
             */
            public void packageCreated() {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.IMTJProjectListener#signaturePropertiesChanged()
             */
            public void signaturePropertiesChanged() {
                if (enableButton != null && !enableButton.isDisposed()
                        && enableButton.getSelection()) {
                    listAliases(combo);
                }
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProjectListener#tempKeyPasswordChanged()
             */
            public void tempKeyPasswordChanged() {
            }

            /* (non-Javadoc)
             * @see org.eclipse.mtj.core.project.midp.IMidletSuiteProjectListener#tempKeystorePasswordChanged()
             */
            public void tempKeystorePasswordChanged() {
            }
        };
        IMidletSuiteProject midletSuiteProject = MidletSuiteFactory
                .getMidletSuiteProject(getJavaProject());
        if (midletSuiteProject != null) {
            midletSuiteProject.addMTJProjectListener(this.projectListener);
        }
    }

    /**
     * Scans the project classes for occurrences of the classes requiring
     * permissions and opens a permissions dialog with all permissions found
     * selected.
     * 
     * @param listId id of the target list.
     */
    private void scanProjectClasses(ListId listId) {
        IMidletSuiteProject midletSuiteProject = MidletSuiteFactory
                .getMidletSuiteProject(getJavaProject());
        if (midletSuiteProject == null) {
            return;
        }

        CheckedTreeSelectionDialog dialog = SecurityPermissionsDialog
                .createDialog(getPartControl().getShell());
        SecurityPermissionsScanner scanner = new SecurityPermissionsScanner(
                midletSuiteProject);
        List<PermissionsGroup> permissionsGroups = scanner
                .getRequiredPermissions();
        dialog.setInitialSelections(permissionsGroups.toArray());
        if (dialog.open() == Dialog.OK) {
            Object[] result = dialog.getResult();

            GenericListBlock<SecurityPermissionListItem> block = null;
            List<SecurityPermissionListItem> list = null;
            switch (listId) {
                case Required:
                    block = reqPermissionBlock;
                    list = reqMIDPPermissions;
                    break;
                case Optional:
                    block = optPermissionBlock;
                    list = optMIDPPermissions;
                    break;
            }

            if (list != null) {
                addPermissionsToList(list, result, block);
            }
        }
    }

    /**
     * Updates the project's signature properties.
     */
    private void updateProjectSigningState() {
        ISignatureProperties signProperties = null;
        IMidletSuiteProject midletProject = null;

        midletProject = MidletSuiteFactory
                .getMidletSuiteProject(getJavaProject());
        signProperties = getProjectSignatureProperties();
        if (signProperties == null) {
            return;
        }

        try {
            signProperties.setSignProject(this.enableButton.getSelection());
            int selectedIndex = this.combo.getSelectionIndex();
            if (selectedIndex >= 0x00) {
                signProperties.setKeyAlias(this.combo.getItem(selectedIndex));
            }
            midletProject.setSignatureProperties(signProperties);
            midletProject.saveMetaData();
        } catch (CoreException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    protected void createFormContent(IManagedForm managedForm) {
        FormToolkit toolkit = managedForm.getToolkit();
        ScrolledForm form = managedForm.getForm();
        form.setExpandHorizontal(true);
        form.setExpandVertical(true);

        toolkit.decorateFormHeading(form.getForm());
        createErrorMessageHandler(managedForm);
        form.setText(getTitle());

        Composite body = form.getBody();
        body.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        GridLayout layout = new GridLayout(2, true);
        layout.horizontalSpacing = 10;
        layout.verticalSpacing = 20;
        body.setLayout(layout);
        this.createTopSection(managedForm, body);
        this.createAliasSection(managedForm, body);
        this.createListsSection(managedForm, body);
        updateWidgetsStates(enableButton.getSelection());
        form.reflow(true);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
     */
    protected String getHelpResource() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.part.EditorPart#setInput(org.eclipse.ui.IEditorInput)
     */
    protected void setInput(IEditorInput input) {
        super.setInput(input);
        editorInputChanged();
    }

    /**
     * Updates the widgets enabled state.
     * 
     * @param state true to enable false to disable.
     */
    protected void updateWidgetsStates(boolean state) {
        this.combo.setEnabled(state);
        if (state) {
            this.listAliases(this.combo);
        }
    }

    public void activeDeviceChanged(IDevice newActiveDevice) {
        ISDK sdk = newActiveDevice == null? null : newActiveDevice.getSDK();
        
        if (meepPermissionsProvider == null || meepPermissionsProvider.getSDK() != sdk) {
            meepPermissionsProvider = new MEEPPermissionsProvider(sdk);
        }
    }

    public void deviceProfileChanged(String newProfile) {
        boolean useMEEPSecurityOld = useMEEPSecurity;
        
        useMEEPSecurity = Profile.MEEP_80.toString().equals(newProfile);
        
        if (useMEEPSecurityOld != useMEEPSecurity && getPartControl() != null) {
            reqPermissionBlock.setButtonVisible(ButtonBarBlock.BUTTON_EDIT_INDEX, useMEEPSecurity);
            optPermissionBlock.setButtonVisible(ButtonBarBlock.BUTTON_EDIT_INDEX, useMEEPSecurity);
            
            reqPermissionBlock.setButtonVisible(ButtonBarBlock.BUTTON_SCAN_INDEX, !useMEEPSecurity);
            optPermissionBlock.setButtonVisible(ButtonBarBlock.BUTTON_SCAN_INDEX, !useMEEPSecurity);
            
            reqPermissionBlock.setInput(useMEEPSecurity ? reqMEEPPermissions : reqMIDPPermissions);
            optPermissionBlock.setInput(useMEEPSecurity ? optMEEPPermissions : optMIDPPermissions);
        }
    }

    public void packagingModelChanged(PackagingModel newPackagingModel) {
        packagingModel = newPackagingModel;
    }
}
