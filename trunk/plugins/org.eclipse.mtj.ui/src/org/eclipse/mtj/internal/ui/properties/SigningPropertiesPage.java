/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)  - Initial implementation
 *     Kevin Hunter (Individual) - Added signature support
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse
 *                                 standards
 *     Gang  Ma	(Sybase)         - Change page validation to follow eclipse 
 *     				   UI guide
 *     Feng Wang (Sybase)        - Fix bug 246769: disable "Project..." button 
 *                                 when necessary. Also clear error message when 
 *                                 "Sign project" check box unchecked.
 *     Diego Sandin (Motorola)   - Use Eclipse Message Bundles [Bug 255874]
 *     David Marques (Motorola)  - Updating page to support signing enhancements.
 *     David Marques (Motorola)  - Externalizing strings and some fixes.
 *     David Marques (Motorola)  - Implementing password changing and fixing
 *                                 some bugs.
 *     David Marques (Motorola)  - Fixing performDefaults and password button. 
 */
package org.eclipse.mtj.internal.ui.properties;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.midp.J2MENature;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.sign.PreferencesSignatureProperties;
import org.eclipse.mtj.internal.core.build.sign.SignatureProperties;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.sign.DefaultKeyStoreManager;
import org.eclipse.mtj.internal.core.sign.KeyStoreManagerException;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.dialog.SigningPasswordDialog;
import org.eclipse.mtj.internal.ui.forms.blocks.SigningBlock;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbenchPropertyPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;
import org.eclipse.ui.dialogs.PropertyPage;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;

/**
 * Property page implementation for Java ME properties associated with signing
 * the project.
 * 
 * @author Craig Setera
 * @author Kevin Hunter
 */
public class SigningPropertiesPage extends PropertyPage implements
		IWorkbenchPropertyPage {

	private ISignatureProperties sigProps;

	private SigningBlock signingBlock;
	
	private Text keystoreTxt;

	private Text keystorePasswordTxt;

	private Button ksPasswordBtn3;

	private Button ksPasswordBtn2;

	private Button ksPasswordBtn1;

	private Button passwordBtn;

	private Button externalBtn;

	private Button projectBtn;

	private Text keyPasswordTxt;

	private Button projectSpecificBtn;

	private DefaultKeyStoreManager keyStoreManager;
	
	/**
	 * Default constructor.
	 */
	public SigningPropertiesPage() {
	}

	/**
	 * Utility routine to convert an empty string to a null value when unloading
	 * Text widgets.
	 * 
	 * @param s
	 * @return
	 */
	public String convertEmptyToNull(String s) {
		if (s == null) {
			return (null);
		}

		if (s.length() == 0) {
			return (null);
		}

		return (s);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.preference.PreferencePage#isValid()
	 */
	@Override
	public boolean isValid() {
		boolean result = true;
		if (this.projectSpecificBtn.getSelection()) {
			try {
				String ksPath = this.sigProps.getAbsoluteKeyStorePath(getProject());
				result &=  ksPath != null && ksPath.length() > 0x00;
				if (this.sigProps.getPasswordStorageMethod() != SignatureProperties.PASSMETHOD_PROMPT) {
					String ksPass = this.keystorePasswordTxt.getText();
					result &=  ksPass != null && ksPass.length() > 0x00;
				}
			} catch (CoreException e) {
				result = false;
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.preference.PreferencePage#performOk()
	 */
	@Override
	public boolean performOk() {
		IProject project = getProject();

		boolean isJ2meProject = false;
		try {
			isJ2meProject = J2MENature.hasMtjCoreNature(project);
		} catch (CoreException e) {
		}

		if (!isJ2meProject) {
			return (true);
		}

		boolean succeeded = false;

		if (sigProps.getPasswordStorageMethod() != SignatureProperties.PASSMETHOD_PROMPT) {
			sigProps.setKeyStorePassword(this.keystorePasswordTxt.getText());
			sigProps.setKeyPassword(this.keyPasswordTxt.getText());
		}
		sigProps.setKeyStoreProvider(this.signingBlock.getProvider());
		sigProps.setKeyStoreType(this.signingBlock.getKeystoreType());
		
		try {
			IMTJProject midletProject = getMidletSuiteProject();
			midletProject.setSignatureProperties(sigProps);
			midletProject.saveMetaData();

			succeeded = true;
		} catch (CoreException ex) {
			String message = NLS.bind(MTJUIMessages.SigningPropertiesPage_unableToSaveSuiteMetadata, ex.getMessage());
			MTJLogger.log(IStatus.ERROR, message);
		}
		return succeeded;
	}

	/**
	 * This routine is called in response to the "Browse" button to allow the
	 * user to choose a keystore file using a File Open dialog.
	 */
	private void browseExternalForKeystore() {
		FileDialog dlg = new FileDialog(getShell(), SWT.OPEN);

		dlg
				.setText(MTJUIMessages.J2MESigningPropertiesPage_browse_dialog_title);
		dlg.setFilterNames(IMTJUIConstants.BROWSE_FILTER_NAMES);
		dlg.setFilterExtensions(IMTJUIConstants.BROWSE_FILTER_EXTENSIONS);

		String path = dlg.open();
		if (path == null) {
			return;
		}

		this.resetPasswords();
		this.sigProps.setKeyStoreDisplayPath(path);
		keystoreTxt.setText(path);
		this.loadAliases();
	}

	/**
	 * Resets all Ui for passwords and the passwords properties
	 */
	private void resetPasswords() {
		keystorePasswordTxt.setEnabled(false);
		keystorePasswordTxt.setText(Utils.EMPTY_STRING);
		keyPasswordTxt.setEnabled(false);
		keyPasswordTxt.setText(Utils.EMPTY_STRING);
		ksPasswordBtn1.setSelection(true);
		ksPasswordBtn2.setSelection(false);
		ksPasswordBtn3.setSelection(false);
		sigProps.setPasswordStorageMethod(SignatureProperties.PASSMETHOD_PROMPT);
		sigProps.setKeyStorePassword(Utils.EMPTY_STRING);
		sigProps.setKeyPassword(Utils.EMPTY_STRING);
	}

	/**
	 * This routine is called in response to the "Browse" button to allow the
	 * user to choose a keystore file using a File Open dialog.
	 */
	private void browseProjectForKeystore() {
		ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
				getShell(), new WorkbenchLabelProvider(),
				new WorkbenchContentProvider());
		dialog
				.setTitle(MTJUIMessages.J2MESigningPropertiesPage_browse_dialog_title);
		dialog
				.setMessage(MTJUIMessages.J2MESigningPropertiesPage_browse_dialog_message);
		dialog.setInput(getProject());
		dialog.setAllowMultiple(false);
		dialog
				.setEmptyListMessage(MTJUIMessages.J2MESigningPropertiesPage_browse_need_file);
		dialog.setStatusLineAboveButtons(true);
		dialog.setValidator(new ISelectionStatusValidator() {
			public IStatus validate(Object[] selection) {
				if (selection.length == 0) {
					return new Status(
							IStatus.ERROR,
							IMTJUIConstants.PLUGIN_ID,
							1,
							MTJUIMessages.J2MESigningPropertiesPage_browse_need_file,
							null);
				}

				if (!(selection[0] instanceof IFile)) {
					return new Status(
							IStatus.ERROR,
							IMTJUIConstants.PLUGIN_ID,
							1,
							MTJUIMessages.J2MESigningPropertiesPage_browse_need_file,
							null);
				}

				return new Status(IStatus.OK, IMTJUIConstants.PLUGIN_ID,
						IStatus.OK, ((IFile) selection[0]).getName(), null);
			}
		});

		if (dialog.open() != Window.OK) {
			return;
		}

		Object[] result = dialog.getResult();
		if ((result.length == 0) || !(result[0] instanceof IFile)) {
			return;
		}

		IFile theFile = (IFile) result[0];
		IPath fullPath = theFile.getFullPath();
		/*
		 * The full path comes back with the name of the project in the first
		 * segment. Removing the first segment gives us a path that's really
		 * (from a file system point of view) relative to the project home
		 * directory.
		 */
		IPath relativePath = fullPath.removeFirstSegments(1);
		String filePath = relativePath.toString();

		/*
		 * "toRelative" in this sense means prepending the prefix to put the
		 * string into its "relative display form".
		 */
		this.resetPasswords();
		String displayPath = SignatureProperties.toRelative(filePath);
		this.sigProps.setKeyStoreDisplayPath(displayPath);
		keystoreTxt.setText(displayPath);
		this.loadAliases();
	}

	/**
	 * Loads all keystore's private keys aliases.
	 */
	private void loadAliases() {
		File keyStoreFile = null;
		try {
			String path = this.sigProps.getAbsoluteKeyStorePath(getProject());
			if (path == null) {
				return;
			}
			
			keyStoreFile = new File(path);
			if (!keyStoreFile.exists() || !keyStoreFile.isFile()) {
				return;
			}
			
			if (this.sigProps.getPasswordStorageMethod() != SignatureProperties.PASSMETHOD_PROMPT) {
				String kstPassword = this.keystorePasswordTxt.getText();
				keyStoreManager = new DefaultKeyStoreManager(keyStoreFile, kstPassword);
				keyStoreManager.setProvider(this.signingBlock.getProvider());
				keyStoreManager.setKeystoreType(this.signingBlock.getKeystoreType());
				this.signingBlock.setInput(keyStoreManager);
				this.passwordBtn.setEnabled(true);
				String alias = sigProps.getKeyAlias();
				if (alias != null) {				
					this.signingBlock.setCurrentAlias(alias);
				}
			} else {
				openKeyStorePasswordDialog(keyStoreFile);
			}
		} catch (CoreException e) {
			String message = NLS.bind(MTJUIMessages.SigningPropertiesPage_unableToGetKeystoreAliases, e.getMessage());
			setErrorMessage(message);
			MTJLogger.log(IStatus.ERROR, message);
		}
	}

	/**
	 * Opens a keystore password dialog.
	 * 
	 * @param keyStoreFile target keystore file.
	 */
	private void openKeyStorePasswordDialog(final File keyStoreFile) {
		Display.getDefault().asyncExec(new Runnable(){
			public void run() {				
				SigningPasswordDialog dialog = new SigningPasswordDialog(getShell(), true);
				dialog.setTitle(MTJUIMessages.SigningPropertiesPage_enterPassword);
				dialog.setDescription(MTJUIMessages.SigningPropertiesPage_enterKeystorePassword);
				
				if (dialog.open() != Dialog.OK) {
					return;
				}
				
				String kstPassword = dialog.getPassword();
				sigProps.setPasswordStorageMethod(SignatureProperties.PASSMETHOD_PROMPT);
				if (dialog.isSavingInWorkspace()) {
					sigProps.setPasswordStorageMethod(SignatureProperties.PASSMETHOD_IN_KEYRING);
					keystorePasswordTxt.setText(kstPassword);
					keystorePasswordTxt.setEnabled(true);
					keyPasswordTxt.setEnabled(true);
					ksPasswordBtn2.setSelection(true);
					ksPasswordBtn1.setSelection(false);
					ksPasswordBtn3.setSelection(false);
				}
				
				keyStoreManager = new DefaultKeyStoreManager(keyStoreFile, kstPassword);
				keyStoreManager.setProvider(signingBlock.getProvider());
				keyStoreManager.setKeystoreType(signingBlock.getKeystoreType());
				signingBlock.setInput(keyStoreManager);
				passwordBtn.setEnabled(true);
			}
		});
	}

	/**
	 * Utility routine to convert a null value to an empty string when loading
	 * Text widgets.
	 * 
	 * @param s
	 * @return
	 */
	private String convertNullToEmpty(String s) {
		if (s == null) {
			return (""); //$NON-NLS-1$
		}

		return (s);
	}

	/**
	 * Get the MIDlet suite project for this project.
	 * 
	 * @return
	 */
	private IMidletSuiteProject getMidletSuiteProject() {
		IJavaProject javaProject = JavaCore.create(getProject());
		IMidletSuiteProject midletProject = MidletSuiteFactory
				.getMidletSuiteProject(javaProject);

		return midletProject;
	}

	/**
	 * Get the selected project or <code>null</code> if a project is not
	 * selected.
	 * 
	 * @return
	 */
	private IProject getProject() {
		IProject project = null;
		IAdaptable adaptable = getElement();

		if (adaptable instanceof IProject) {
			project = (IProject) adaptable;
		} else if (adaptable instanceof IJavaProject) {
			project = ((IJavaProject) adaptable).getProject();
		}

		return project;
	}

	/**
	 * Return a boolean indicating whether the selected element is a J2ME
	 * project.
	 * 
	 * @param project
	 * @return
	 */
	private boolean isJ2MEProject(IProject project) {
		boolean j2meProject = false;

		if (project != null) {
			try {
				IProjectNature nature = project
						.getNature(IMTJCoreConstants.MTJ_NATURE_ID);
				j2meProject = (nature != null);
			} catch (CoreException e) {
			}
		}

		return j2meProject;
	}

	/**
	 * This routine loads the controls that are part of the Signing Properties
	 * Group. It is separated from loadPageData() because we also need to do
	 * this in response to the "Defaults" button being pressed.
	 */
	private void loadSigningData() {
		boolean isProjectSpecific = sigProps.isProjectSpecific();
		
		projectSpecificBtn.setSelection(isProjectSpecific);
		this.setEnabled(isProjectSpecific);
		
		this.keystoreTxt.setText(convertNullToEmpty(sigProps
				.getKeyStoreDisplayPath()));

		this.keystorePasswordTxt.setText(convertNullToEmpty(sigProps
				.getKeyStorePassword()));
		
		this.keyPasswordTxt.setText(convertNullToEmpty(sigProps
				.getKeyPassword()));
		
		int passwdType = this.sigProps.getPasswordStorageMethod();
		switch (passwdType) {
			case SignatureProperties.PASSMETHOD_IN_PROJECT:
				this.ksPasswordBtn1.setSelection(false);
				this.ksPasswordBtn2.setSelection(false);
				this.ksPasswordBtn3.setSelection(true);
			break;
			case SignatureProperties.PASSMETHOD_IN_KEYRING:
				this.ksPasswordBtn1.setSelection(false);
				this.ksPasswordBtn3.setSelection(false);
				this.ksPasswordBtn2.setSelection(true);
			break;
			case SignatureProperties.PASSMETHOD_PROMPT:
				this.ksPasswordBtn2.setSelection(false);
				this.ksPasswordBtn3.setSelection(false);
				this.ksPasswordBtn1.setSelection(true);
			break;
		}
		
		this.signingBlock.setProvider(sigProps.getKeyStoreProvider());
		this.signingBlock.setKeystoreType(sigProps.getKeyStoreType());
		if (isProjectSpecific) {			
			loadAliases();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse
	 * .swt.widgets.Composite)
	 */
	protected Control createContents(Composite parent) {
		this.loadSigningProperties();
		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
				"org.eclipse.mtj.ui.help_J2MEProjectPropertiesPage"); //$NON-NLS-1$

		IProject project = getProject();
		if (!isJ2MEProject(project)) {
			Label lbl = new Label(parent, SWT.NONE);
			lbl
					.setText(MTJUIMessages.J2MESigningPropertiesPage_error_not_midlet_project);
			return (lbl);
		}

		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(MTJUIPlugin.buildGridData(SWT.LEFT, SWT.CENTER,
				true, true));
		GridLayout layout = new GridLayout(0x01, false);
		layout.marginWidth  = 0x00;
		layout.marginHeight = 0x00;
		composite.setLayout(layout);

		projectSpecificBtn = new Button(composite, SWT.CHECK);
		projectSpecificBtn.setText(MTJUIMessages.SigningPropertiesPage_enableProjectSpecific);
		projectSpecificBtn.setLayoutData(MTJUIPlugin.buildGridData(SWT.LEFT, SWT.CENTER,
				true, false));
		projectSpecificBtn.addSelectionListener(new SelectionListener(){
			public void widgetDefaultSelected(SelectionEvent e) {
				
			}
			public void widgetSelected(SelectionEvent e) {
				loadSigningProperties();
				sigProps.setProjectSpecific(projectSpecificBtn.getSelection());
				setEnabled(sigProps.isProjectSpecific());
				if (sigProps.isSignProject() && sigProps.isProjectSpecific()) {					
					loadAliases();
				}
			}
		});
		createKeyStoreSection(composite);
		signingBlock = new SigningBlock();
		signingBlock.createBlock(composite);
		this.loadSigningData();
		return composite;
	}

	/**
	 * Loads the signing properties.
	 */
	private void loadSigningProperties() {
		// Get the associated MIDlet suite project
		IMTJProject midletProject = getMidletSuiteProject();

		ISignatureProperties props = null;
		try {
			props = midletProject.getSignatureProperties();
		} catch (CoreException e) {
		}

		sigProps = new SignatureProperties();
		if (props != null) {
			sigProps.copy(props);
		} else {
			sigProps.clear();
		}
	}

	/**
	 * Enables/Disables all child widgets.
	 * 
	 * @param enabled true if enabled false otherwise.
	 */
	private void setEnabled(boolean enabled) {
		this.keystoreTxt.setEnabled(enabled);
		this.ksPasswordBtn3.setEnabled(enabled);
		this.ksPasswordBtn2.setEnabled(enabled);
		this.ksPasswordBtn1.setEnabled(enabled);
		this.passwordBtn.setEnabled(enabled);
		this.externalBtn.setEnabled(enabled);
		this.projectBtn.setEnabled(enabled);
		this.signingBlock.setEnabled(enabled);
		
		boolean enablePasswords = this.sigProps
			.getPasswordStorageMethod() != SignatureProperties.PASSMETHOD_PROMPT; 
		this.keystorePasswordTxt.setEnabled(enablePasswords && enabled);
		this.keyPasswordTxt.setEnabled(enablePasswords && enabled);
	}

	/**
	 * Creates the keystore section.
	 * 
	 * @param parent parent composite.
	 */
	private void createKeyStoreSection(Composite parent) {
		Group ksGroup = new Group(parent, SWT.NONE);
		ksGroup.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL, SWT.FILL,
				true, false));
		GridLayout layout = new GridLayout(0x04, false);
		layout.verticalSpacing = 0x00;
		ksGroup.setLayout(layout);
		ksGroup.setText(MTJUIMessages.SigningPropertiesPage_keystore);

		Label location = new Label(ksGroup, SWT.NONE);
		location.setText(MTJUIMessages.SigningPropertiesPage_location);
		GridData gridData = MTJUIPlugin.buildGridData(SWT.FILL, SWT.CENTER,
				true, false);
		gridData.horizontalSpan = 4;
		location.setLayoutData(gridData);

		keystoreTxt = new Text(ksGroup, SWT.BORDER);
		keystoreTxt.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL,
				SWT.CENTER, true, false));

		projectBtn = new Button(ksGroup, SWT.NONE);
		projectBtn.setText(MTJUIMessages.SigningPropertiesPage_project);
		projectBtn.setLayoutData(MTJUIPlugin.buildGridData(false, false));
		projectBtn.addMouseListener(new MouseListener() {

			public void mouseDoubleClick(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
			}

			public void mouseUp(MouseEvent e) {
				browseProjectForKeystore();
			}

		});

		externalBtn = new Button(ksGroup, SWT.NONE);
		externalBtn.setText(MTJUIMessages.SigningPropertiesPage_external);
		externalBtn.setLayoutData(MTJUIPlugin.buildGridData(false, false));
		externalBtn.addMouseListener(new MouseListener() {

			public void mouseDoubleClick(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
			}

			public void mouseUp(MouseEvent e) {
				browseExternalForKeystore();
			}

		});

		passwordBtn = new Button(ksGroup, SWT.NONE);
		passwordBtn.setText(MTJUIMessages.SigningPropertiesPage_changePassword);
		passwordBtn.setLayoutData(MTJUIPlugin.buildGridData(false, false));
		passwordBtn.addMouseListener(new MouseListener(){

			public void mouseUp(MouseEvent e) {
				if (keyStoreManager == null) {
					return;
				}
				
				SigningPasswordDialog dialog = new SigningPasswordDialog(getShell(), false);
				dialog.setTitle(MTJUIMessages.SigningSettings_ChangePasswordTitle);
				dialog.setDescription(MTJUIMessages.SigningSettings_ChangePasswordMessage);
				if (dialog.open() != Dialog.OK) {
					return;
				}
				
				String kstPassword = dialog.getPassword();
				try {
					keyStoreManager.changeKeystorePassword(kstPassword);
					
					IMTJProject midletProject = getMidletSuiteProject();
					sigProps.setKeyStorePassword(kstPassword);
					midletProject.setSignatureProperties(sigProps);
					midletProject.saveMetaData();
					
					if (sigProps.getPasswordStorageMethod() != SignatureProperties.PASSMETHOD_PROMPT) {
						keystorePasswordTxt.setText(sigProps.getKeyStorePassword());
					}
				} catch (KeyStoreManagerException exc) {
					MessageDialog.openError(getShell(), MTJUIMessages.SigningSettings_KeyStoreManagerErrorTitle, exc.getMessage());
					MTJLogger.log(IStatus.ERROR, exc.getMessage());
				} catch (CoreException exc) {
					String message = NLS.bind(MTJUIMessages.SigningPropertiesPage_unableToSaveSuiteMetadata, exc.getMessage());
					MTJLogger.log(IStatus.ERROR, message);
				}
			}
			
			public void mouseDoubleClick(MouseEvent e) {
			}

			public void mouseDown(MouseEvent e) {
			}
		});
		
		Composite passwordGroup = new Composite(ksGroup, SWT.NONE);
		gridData = MTJUIPlugin.buildGridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 4;
		passwordGroup.setLayoutData(gridData);
		passwordGroup.setLayout(new GridLayout(3, true));

		ksPasswordBtn1 = new Button(passwordGroup, SWT.RADIO);
		ksPasswordBtn1.setText(MTJUIMessages.SigningPropertiesPage_prompForPassword);
		ksPasswordBtn1.setLayoutData(MTJUIPlugin.buildGridData(false, false));
		ksPasswordBtn1.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent e) {
				
			}

			public void widgetSelected(SelectionEvent e) {
				keystorePasswordTxt.setEnabled(!ksPasswordBtn1.getSelection());
				keystorePasswordTxt.setText(Utils.EMPTY_STRING);
				keyPasswordTxt.setEnabled(!ksPasswordBtn1.getSelection());
				keyPasswordTxt.setText(Utils.EMPTY_STRING);
				sigProps.setPasswordStorageMethod(SignatureProperties.PASSMETHOD_PROMPT);
				sigProps.setKeyStorePassword(Utils.EMPTY_STRING);
				sigProps.setKeyPassword(Utils.EMPTY_STRING);
			}
		});
		
		ksPasswordBtn2 = new Button(passwordGroup, SWT.RADIO);
		ksPasswordBtn2.setText(MTJUIMessages.SigningPropertiesPage_savePasswordInWorkspace);
		ksPasswordBtn2.setLayoutData(MTJUIPlugin.buildGridData(false, false));
		ksPasswordBtn2.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent e) {
				
			}

			public void widgetSelected(SelectionEvent e) {
				keystorePasswordTxt.setEnabled(ksPasswordBtn2.getSelection());
				keyPasswordTxt.setEnabled(ksPasswordBtn2.getSelection());
				sigProps.setPasswordStorageMethod(SignatureProperties.PASSMETHOD_IN_KEYRING);
			}
		});
		ksPasswordBtn3 = new Button(passwordGroup, SWT.RADIO);
		ksPasswordBtn3.setText(MTJUIMessages.SigningPropertiesPage_savePasswordInProject);
		ksPasswordBtn3.setLayoutData(MTJUIPlugin.buildGridData(true, false));
		ksPasswordBtn3.addSelectionListener(new SelectionListener(){

			public void widgetDefaultSelected(SelectionEvent e) {
				
			}

			public void widgetSelected(SelectionEvent e) {
				keystorePasswordTxt.setEnabled(ksPasswordBtn3.getSelection());
				keyPasswordTxt.setEnabled(ksPasswordBtn3.getSelection());
				sigProps.setPasswordStorageMethod(SignatureProperties.PASSMETHOD_IN_PROJECT);
			}
		});
		
		Composite ksPasswordGroup = new Composite(ksGroup, SWT.NONE);
		gridData = MTJUIPlugin.buildGridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 4;
		ksPasswordGroup.setLayoutData(gridData);
		ksPasswordGroup.setLayout(new GridLayout(2, false));

		Label kstPasswordLabel = new Label(ksPasswordGroup, SWT.NONE);
		kstPasswordLabel.setText(MTJUIMessages.SigningPropertiesPage_keyStorePassword);
		kstPasswordLabel.setLayoutData(MTJUIPlugin.buildGridData(false, false));

		keystorePasswordTxt = new Text(ksPasswordGroup, SWT.BORDER | SWT.PASSWORD);
		keystorePasswordTxt.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL,
				SWT.CENTER, true, false));
		keystorePasswordTxt.setEnabled(false);
		
		Label keyPasswordLabel = new Label(ksPasswordGroup, SWT.NONE);
		keyPasswordLabel.setText(MTJUIMessages.SigningPropertiesPage_keyPassword);
		keyPasswordLabel.setLayoutData(MTJUIPlugin.buildGridData(false, false));

		keyPasswordTxt = new Text(ksPasswordGroup, SWT.BORDER | SWT.PASSWORD);
		keyPasswordTxt.setLayoutData(MTJUIPlugin.buildGridData(SWT.FILL,
				SWT.CENTER, true, false));
		keyPasswordTxt.setEnabled(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.preference.PreferencePage#performDefaults()
	 */
	protected void performDefaults() {
		super.performDefaults();
		this.setErrorMessage(null);
		//Saves whether the project is being signed.
		boolean signProject = this.sigProps.isSignProject();
		this.sigProps.clear();
		this.sigProps.setSignProject(signProject);
		
		this.signingBlock.setInput(null);
		this.setEnabled(false);
		try {
			this.sigProps = new PreferencesSignatureProperties();
			this.loadSigningData();
		} catch (CoreException e) {
			MTJLogger.log(IStatus.ERROR, e);
		}
	}
}
