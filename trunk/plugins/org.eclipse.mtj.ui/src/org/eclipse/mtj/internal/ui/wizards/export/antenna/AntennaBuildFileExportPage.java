/*
* Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
* All rights reserved.
* This component and the accompanying materials are made available
* under the terms of "Eclipse Public License v1.0"
* which accompanies this distribution, and is available
* at the URL "http://www.eclipse.org/legal/epl-v10.html".
*
* Initial Contributors:
* 		Nokia Corporation - initial contribution.
*
*
*/
package org.eclipse.mtj.internal.ui.wizards.export.antenna;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.export.AntennaBuildExport;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.preferences.J2MEPreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.eclipse.ui.model.WorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
/**
 * Export wizard page for {@link AntennaBuildFileExportWizard}
 * 
 * @author Gorkem Ercan
 *
 */
public class AntennaBuildFileExportPage extends WizardPage {

	private List<IJavaProject> fSelectedJavaProjects = new ArrayList<IJavaProject>();
	private Table table;
	private TableViewer tableViewer;
	private Text txtMtjbuild;
	private Text txtMtjbuildxml;
	
	protected AntennaBuildFileExportPage() {
		super("AntennaBuildFileWizardPage"); //$NON-NLS-1$
		setPageComplete(false);
		setTitle(MTJUIMessages.AntennaBuildFileExportPage_title);
		setDescription(MTJUIMessages.AntennaBuildFileExportPage_description);
	}

	public void createControl(Composite parent) {
       initializeDialogUnits(parent);
       Composite workArea = new Composite(parent, SWT.NONE);
       setControl(workArea);
       workArea.setLayout(new GridLayout(1, false));
       
       Link antennaPrefLink = new Link(workArea, SWT.NONE);
       antennaPrefLink.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
       antennaPrefLink.addSelectionListener(new SelectionAdapter() {
       	@Override
       	public void widgetSelected(SelectionEvent e) {
       		PreferencesUtil.createPreferenceDialogOn(getShell(), J2MEPreferencePage.ID, null, null).open();
       		updatePageStatus();
       	}
       });
       antennaPrefLink.setText(MTJUIMessages.AntennaBuildFileExportPage_preferencesLink);
        
       Label lblSelectTheProject = new Label(workArea, SWT.NONE);
       lblSelectTheProject.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
       lblSelectTheProject.setText(MTJUIMessages.AntennaBuildFileExportPage_selectProject);
       
       tableViewer = new TableViewer(workArea, SWT.BORDER | SWT.FULL_SELECTION);
       tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
       	public void selectionChanged(SelectionChangedEvent event) {
       		updatePageStatus();
       	}
       });
       table = tableViewer.getTable();
       table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
       
       Composite composite = new Composite(workArea, SWT.NONE);
       composite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
       composite.setLayout(new GridLayout(2, false));
       
       Label lblBuildDirectory = new Label(composite, SWT.NONE);
       lblBuildDirectory.setText(MTJUIMessages.AntennaBuildFileExportPage_buildDirectory);
       
       txtMtjbuild = new Text(composite, SWT.BORDER);
       txtMtjbuild.addModifyListener(new ModifyListener() {
       	public void modifyText(ModifyEvent e) {
       		updatePageStatus();
       	}
       });
       txtMtjbuild.setText(AntennaBuildExport.DEFAULT_BUILD_FOLDER);
       txtMtjbuild.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
       
       Label lblNameForBuild = new Label(composite, SWT.NONE);
       lblNameForBuild.setBounds(0, 0, 49, 13);
       lblNameForBuild.setText(MTJUIMessages.AntennaBuildFileExportPage_buildFileName);
       
       txtMtjbuildxml = new Text(composite, SWT.BORDER);
       txtMtjbuildxml.setText(AntennaBuildExport.DEFAULT_BUILD_XML); 
       txtMtjbuildxml.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
       txtMtjbuildxml.addModifyListener(new ModifyListener() {
          	public void modifyText(ModifyEvent e) {
          		updatePageStatus();
          	}
          });
       
       tableViewer.setContentProvider(new WorkbenchContentProvider() {
    	   @Override
			public Object[] getElements(Object element) {
				if (element instanceof IJavaProject[]) {
					return (Object[]) element;
				}
				return null;
			}
       });
       tableViewer.setLabelProvider(new WorkbenchLabelProvider());
       
       initializeProjects();
       updatePageStatus();
       Dialog.applyDialogFont(parent);
	}

	private void updatePageStatus() {
		if (!isAntennaConfigurationValid()) {
			setErrorMessage(MTJUIMessages.AntennaBuildFileExportPage_antennaPreferencesMessage);
			setPageComplete(false);
			return;
		}
		if (tableViewer.getSelection().isEmpty()){
			setPageComplete(false);
			return;
		}
		if(txtMtjbuild.getText() == null || txtMtjbuild.getText().length()<1){
			setPageComplete(false);
			setErrorMessage(MTJUIMessages.AntennaBuildFileExportPage_buildDirectoryMissingMessage);
			return;
		}
		if(txtMtjbuildxml.getText() == null || txtMtjbuildxml.getText().length()<1){
			setPageComplete(false);
			setErrorMessage(MTJUIMessages.AntennaBuildFileExportPage_buildFileMissingMessage);
			return;
		}
		
		setErrorMessage(null);
		setPageComplete(true);
	}

    private void initializeProjects() {
        IWorkspaceRoot rootWorkspace = ResourcesPlugin.getWorkspace().getRoot();
        
        IJavaModel javaModel = JavaCore.create(rootWorkspace);
        IJavaProject[] javaProjects;
        try {
            javaProjects = javaModel.getJavaProjects();
        }
        catch (JavaModelException e) {
            javaProjects= new IJavaProject[0];
        }
        ArrayList<IJavaProject> midlets = new ArrayList<IJavaProject>(javaProjects.length);
        for (int i = 0; i < javaProjects.length; i++) {
			if(MidletSuiteFactory.isMidletSuiteProject(javaProjects[i].getProject())){
				midlets.add(javaProjects[i]);
			}
		}
        tableViewer.setInput(midlets.toArray(new IJavaProject[midlets.size()]));
       
        if (fSelectedJavaProjects != null && !fSelectedJavaProjects.isEmpty()) {
           tableViewer.setSelection(new StructuredSelection(fSelectedJavaProjects.get(0))); 
        }
    }
	
	/*package*/ void setSelectedProjects(List<IJavaProject> projects) {
		fSelectedJavaProjects.addAll(projects);
	}
	
    /**
     * Checks if the antenna preference actually points to a valid jar file.
     * 
     * @return true if configuration points t
     */
    private boolean isAntennaConfigurationValid() {
        // A simplistic view of whether or not this is a valid configuration
        IPreferenceStore prefs = MTJUIPlugin.getDefault().getCorePreferenceStore();
        File antennaJar = new File(prefs
                .getString(IMTJCoreConstants.PREF_ANTENNA_JAR));
        File wtkRoot = new File(prefs
                .getString(IMTJCoreConstants.PREF_WTK_ROOT));
        return antennaJar.exists() && antennaJar.isFile() && wtkRoot.exists()
                && wtkRoot.isDirectory();
    }

	public IMidletSuiteProject getMidletProject() {
		if (tableViewer.getSelection().isEmpty())
			return null;
		IStructuredSelection selection = (IStructuredSelection)tableViewer.getSelection();
		IJavaProject project = (IJavaProject)selection.getFirstElement();
		return MidletSuiteFactory.getMidletSuiteProject(project);
	}

	public String getBuildFileName() {
		return txtMtjbuildxml.getText();
	}

	public String getBuildDirectory() {
		return txtMtjbuild.getText();
	}
}
