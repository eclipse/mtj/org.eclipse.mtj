/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.internal.ui.viewers;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;

/**
 * A viewer sorter implementation that does a string sort based on the value of
 * the label provided during instantiation.
 * 
 * @author Craig Setera
 */
public class LabelProviderViewerSorter extends ViewerSorter {

    /**
     * Construct a new viewer sorter that sorts based on the label provider
     * provided in the compare method.
     */
    public LabelProviderViewerSorter() {
        super();
    }

    /**
     * @see org.eclipse.jface.viewers.ViewerSorter#compare(org.eclipse.jface.viewers.Viewer,
     *      java.lang.Object, java.lang.Object)
     */
    public int compare(Viewer viewer, Object e1, Object e2) {
        ILabelProvider labelProvider = (ILabelProvider) ((StructuredViewer) viewer)
                .getLabelProvider();

        String label1 = labelProvider.getText(e1);
        String label2 = labelProvider.getText(e2);

        return label1.compareTo(label2);
    }
}
