/**
 * Copyright (c) 2003,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.ui
 */
package org.eclipse.mtj.internal.ui.editor.context;

import org.eclipse.mtj.internal.ui.editor.MTJFormEditor;
import org.eclipse.ui.IEditorInput;

/**
 * @since 0.9.1
 */
public abstract class UTF8InputContext extends InputContext {

    /**
     * @param editor
     * @param input
     * @param primary
     */
    public UTF8InputContext(MTJFormEditor editor, IEditorInput input,
            boolean primary) {
        super(editor, input, primary);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.context.InputContext#getDefaultCharset()
     */
    protected String getDefaultCharset() {
        return "UTF-8"; //$NON-NLS-1$
    }

}
