/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editor;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.ViewerComparator;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class GenericSourcePage extends MTJSourcePage {

    /**
     * @param editor
     * @param id
     * @param title
     */
    public GenericSourcePage(MTJFormEditor editor, String id, String title) {
        super(editor, id, title);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#createOutlineComparator()
     */
    @Override
    public ViewerComparator createOutlineComparator() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#createOutlineContentProvider()
     */
    @Override
    public ITreeContentProvider createOutlineContentProvider() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#createOutlineLabelProvider()
     */
    @Override
    public ILabelProvider createOutlineLabelProvider() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#updateSelection(java.lang.Object)
     */
    @Override
    public void updateSelection(Object object) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#createOutlinePage()
     */
    @Override
    protected ISortableContentOutlinePage createOutlinePage() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#updateOutlinePageSelection(java.lang.Object)
     */
    @Override
    protected void updateOutlinePageSelection(Object rangeElement) {
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJSourcePage#refreshMarkStatus()
     */
    @Override
    public void refreshMarkStatus() {
        return;
        
    }

}
