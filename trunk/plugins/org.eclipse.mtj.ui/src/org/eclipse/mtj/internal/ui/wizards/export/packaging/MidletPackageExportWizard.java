/*
* Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
* All rights reserved.
* This component and the accompanying materials are made available
* under the terms of "Eclipse Public License v1.0"
* which accompanies this distribution, and is available
* at the URL "http://www.eclipse.org/legal/epl-v10.html".
*
* Initial Contributors:
* 		Nokia Corporation - initial contribution.
*
*/
package org.eclipse.mtj.internal.ui.wizards.export.packaging;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteProject;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;

public class MidletPackageExportWizard extends Wizard implements IExportWizard {

	private IStructuredSelection fSelection;
	private MidletPackageExportPage page;
	
	
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		setWindowTitle(MTJUIMessages.MidletPackageExportWizardTitle);
		setDefaultPageImageDescriptor(MTJUIPluginImages.DESC_MIDLET_JAR_EXPORT_WIZ);
		setNeedsProgressMonitor(true);
		fSelection = selection;
		IDialogSettings section = MTJUIPlugin
				.getDialogSettings("MidletPackageExportWizard");//$NON-NLS-1$
		setDialogSettings(section);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void addPages() {
		page = new MidletPackageExportPage();
		List projects = fSelection.toList();
        page.setSelectedProjects(projects);
		addPage(page);
		
	}

	@Override
	public boolean performFinish() {
		
		try {
			this.getContainer().run(false, true, new IRunnableWithProgress() {
				
				public void run(IProgressMonitor monitor) throws InvocationTargetException,
						InterruptedException {
					Map<IJavaProject, List<MTJRuntime>> selection = page.getProjectSelections();
					SubMonitor localMonitor = SubMonitor.convert(monitor, selection.size());
					for (IJavaProject javaProject : selection.keySet()) {
						if (localMonitor.isCanceled()){
							return;
						}
						MidletSuiteProject midletProject = (MidletSuiteProject) MidletSuiteFactory
								.getMidletSuiteProject(javaProject);
						try {
							String destination = null;
							if(!page.getUseDeployed()){
								destination=page.getDestinationDirectory();
							}
							midletProject.createPackage(
									selection.get(javaProject),
									page.getObfuscate(),destination, localMonitor.newChild(1));
						} catch (CoreException e) {
							MTJUIPlugin
									.getDefault()
									.getLog()
									.log(new Status(IStatus.ERROR, MTJUIPlugin
											.getPluginId(),
											"Failed to export midlet jars", e)); //$NON-NLS-1$

						}
					}
				}
			});
		} catch (Exception e) {
			MTJUIPlugin.getDefault().
			getLog().log(new Status(IStatus.ERROR, MTJUIPlugin.getPluginId(), "Failed to export midlet jar files",e)); //$NON-NLS-1$

		}
		page.finish();
		return true;
	}

}
