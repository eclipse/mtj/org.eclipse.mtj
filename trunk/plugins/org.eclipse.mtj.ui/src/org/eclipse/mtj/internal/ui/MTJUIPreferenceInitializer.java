/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 *     Gang  Ma (Sybase)        - Set default TEMPLATES_USE_CODEFORMATTER to true
 *     Jon Dearden (Research In Motion) - Replaced deprecated use of Preferences 
 *                                        [Bug 285699]
 */
package org.eclipse.mtj.internal.ui;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.mtj.internal.ui.editor.text.ColorManager;

/**
 * Preference initializer for default MTJ UI preferences.
 * 
 * @author Craig Setera
 */
public class MTJUIPreferenceInitializer extends AbstractPreferenceInitializer {

    public static final boolean PREF_DEF_TEMPLATES_USE_CODEFORMATTER = true;

    /**
     * Construct a new Initializer instance.
     */
    public MTJUIPreferenceInitializer() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
     */
    public void initializeDefaultPreferences() {

        IEclipsePreferences defs = new DefaultScope().getNode(IMTJUIConstants.PLUGIN_ID);
        defs.putBoolean(IMTJUIConstants.TEMPLATES_USE_CODEFORMATTER,
                PREF_DEF_TEMPLATES_USE_CODEFORMATTER);

        IPreferenceStore store = MTJUIPlugin.getDefault().getPreferenceStore();

        /* Initialize some editor preferences */
        ColorManager.initializeDefaults(store);
        store.setDefault(IPreferenceConstants.EDITOR_FOLDING_ENABLED, false);
    }

}
