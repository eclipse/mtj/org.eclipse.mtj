/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.dialog;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.*;
import org.eclipse.mtj.internal.core.security.meep.*;
import org.eclipse.mtj.internal.core.security.meep.MEEPPermissionsProvider.PermissionDefinition;
import org.eclipse.mtj.internal.core.security.meep.MEEPPermissionsProvider.PermissionDescriptor;
import org.eclipse.mtj.internal.core.security.meep.MEEPPermissionsProvider.PermissionError;
import org.eclipse.mtj.internal.core.security.meep.MEEPPermissionsProvider.PermissionsFactory;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.accessibility.AccessibleAdapter;
import org.eclipse.swt.accessibility.AccessibleEvent;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

public class MEEPSecurityPermissionsDialog extends Dialog {
    private static final RGB ERROR_FOREGROUND_RGB = new RGB(0xFF, 0, 0);

    private static final CharsetEncoder ASCII_CHARSET_ENCODER = Charset.forName("US-ASCII").newEncoder(); //$NON-NLS-1$

    private Map<RGB, Color> colors = new HashMap<RGB, Color>();

    private Button okButton;
    private ComboViewer permissionsCombo;
    private Text nameField;
    private Button useNullName;
    private Text actionsField;
    private Button useNullActions;
    private Label errorLabel;
    private ParameterDefinition nameDefinition;
    private ParameterDefinition actionsDefinition;

    private PermissionsFactory permissionsFactory;

    private PermissionDefinition permission;

    private PermissionDefinition selectPermission;
    
    private MEEPSecurityPermissionsDialog(Shell parentShell, MEEPPermissionsProvider permissionsProvider,
            Collection<String> existingPermissions, String permissionToEdit) {
        super(parentShell);

        setShellStyle(SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL | getDefaultOrientation());

        if (permissionToEdit != null) {
            existingPermissions = new HashSet<String>(existingPermissions);
            existingPermissions.remove(permissionToEdit);
        }

        this.permissionsFactory = permissionsProvider.getPermissionsFactory(existingPermissions);
        this.selectPermission = permissionToEdit == null ? null : permissionsFactory.getPermission(permissionToEdit);
    }

    public static PermissionDefinition getPermission(Shell parentShell, MEEPPermissionsProvider permissionsProvider,
            Collection<String> existingPermissions, String permissionToEdit) {
        MEEPSecurityPermissionsDialog dialog = new MEEPSecurityPermissionsDialog(parentShell, permissionsProvider,
                existingPermissions, permissionToEdit);

        dialog.open();

        return dialog.permission;
    }

    @Override
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);

        newShell.addDisposeListener(new DisposeListener() {
            public void widgetDisposed(DisposeEvent e) {
                for (Color color : colors.values()) {
                    color.dispose();
                }
            }
        });

        newShell.setText(selectPermission == null ? MTJUIMessages.MEEPSecurityPermissionsDialog_titleAdd
                : MTJUIMessages.MEEPSecurityPermissionsDialog_titleEdit);
    }

    @Override
    protected Control createButtonBar(Composite parent) {
        return createButtonBar(parent, true);
    }

    private Composite createButtonBar(Composite parent, boolean createButtons) {
        Composite composite = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();

        layout.numColumns = 0;
        layout.makeColumnsEqualWidth = true;
        layout.marginWidth = 5;
        layout.marginHeight = 0;
        layout.marginBottom = 5;
        layout.horizontalSpacing = 5;
        layout.verticalSpacing = 5;

        composite.setLayout(layout);

        GridData data = new GridData(GridData.HORIZONTAL_ALIGN_END | GridData.VERTICAL_ALIGN_CENTER);

        composite.setLayoutData(data);
        composite.setFont(parent.getFont());

        if (createButtons) {
            createButtonsForButtonBar(composite);
        }

        return composite;
    }

    private Composite createContainerForControlWithLabel(Composite parent, int numColumns) {
        Composite result = new Composite(parent, SWT.NONE);

        GridLayout layout = new GridLayout();

        layout.marginHeight = 0;
        layout.marginWidth = 0;
        layout.verticalSpacing = 2;
        layout.numColumns = numColumns;
        layout.makeColumnsEqualWidth = false;

        result.setLayout(layout);
        result.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        return result;
    }

    private Color getColor(RGB rgb) {
        synchronized (colors) {
            Color color = colors.get(rgb);

            if (color == null) {
                color = new Color(Display.getCurrent(), rgb);
                colors.put(rgb, color);
            }

            return color;
        }
    }

    @Override
    protected void handleShellCloseEvent() {
        cancelPressed();
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);

        applyDialogFont(container);
        initializeDialogUnits(container);

        GridLayoutFactory.fillDefaults().margins(5, 5).spacing(5, 5).applyTo(container);

        container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        Composite permissionsContainer = createContainerForControlWithLabel(container, 1);

        Label lblMeepPermissionClass = new Label(permissionsContainer, SWT.NONE);
        lblMeepPermissionClass.setText(MTJUIMessages.MEEPSecurityPermissionsDialog_permissionLabel);
        lblMeepPermissionClass.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false));

        permissionsCombo = new ComboViewer(permissionsContainer, SWT.READ_ONLY | SWT.DROP_DOWN | SWT.BORDER);
        permissionsCombo.setContentProvider(new ArrayContentProvider());
        permissionsCombo.setLabelProvider(new LabelProvider());

        PermissionDescriptor[] availablePermissions = permissionsFactory.getAvailablePermissions();

        permissionsCombo.setInput(availablePermissions);
        permissionsCombo.setSelection(new StructuredSelection(availablePermissions[0]));

        GridDataFactory.fillDefaults()
                .align(SWT.FILL, SWT.CENTER)
                .grab(true, false)
                .hint(Math.max(400, permissionsCombo.getCombo().computeSize(SWT.DEFAULT, SWT.DEFAULT).x), SWT.DEFAULT)
                .applyTo(permissionsCombo.getCombo());

        Composite nameContainer = createContainerForControlWithLabel(container, 2);

        nameDefinition = new ParameterDefinition(nameContainer,
                MTJUIMessages.MEEPSecurityPermissionsDialog_targetLabel, new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        updateNameAndActions();
                    }
                });

        GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).grab(true, false).span(2, 1).applyTo(nameDefinition);

        nameField = new Text(nameContainer, SWT.BORDER);

        GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).span(1, 1).applyTo(nameField);

        useNullName = new Button(nameContainer, SWT.CHECK);
        useNullName.setText(MTJUIMessages.MEEPSecurityPermissionsDialog_useNull);

        GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).grab(false, false).span(1, 1).applyTo(useNullName);

        Composite actionsContainer = createContainerForControlWithLabel(container, 2);

        actionsDefinition = new ParameterDefinition(actionsContainer,
                MTJUIMessages.MEEPSecurityPermissionsDialog_actionLabel, new SelectionAdapter() {
                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        if (!actionsDefinition.useParameter()) {
                            PermissionDescriptor permissionDescr = getSelectedDescriptor();

                            if (!permissionDescr.actionsCanBeOptional(true)) {
                                nameDefinition.setUseParameter(false);
                            }
                        }

                        updateNameAndActions();
                    }
                });

        GridDataFactory.fillDefaults()
                .align(SWT.LEFT, SWT.CENTER)
                .grab(true, false)
                .span(2, 1)
                .applyTo(actionsDefinition);

        actionsField = new Text(actionsContainer, SWT.BORDER);

        GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).span(1, 1).applyTo(actionsField);

        useNullActions = new Button(actionsContainer, SWT.CHECK);
        useNullActions.setText(MTJUIMessages.MEEPSecurityPermissionsDialog_useNull);

        GridDataFactory.fillDefaults()
                .align(SWT.LEFT, SWT.CENTER)
                .grab(false, false)
                .span(1, 1)
                .applyTo(useNullActions);

        errorLabel = new Label(container, SWT.NONE);
        errorLabel.setForeground(getColor(ERROR_FOREGROUND_RGB));
        errorLabel.setText(" "); //$NON-NLS-1$
        errorLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

        PermissionDescriptor descriptor = null;

        if (selectPermission != null) {
            descriptor = permissionsFactory.getDescriptor(selectPermission);
            permissionsCombo.setSelection(new StructuredSelection(descriptor));

            if (selectPermission.getNumberOfParameters() > 0) {
                if (descriptor.nameCanBeOptional()) {
                    nameDefinition.setUseParameter(true);
                }

                String nameValue = selectPermission.getName();

                useNullName.setSelection(nameValue == null);

                if (nameValue != null) {
                    nameField.setText(nameValue);
                }

                if (selectPermission.getNumberOfParameters() > 1) {
                    if (descriptor.actionsCanBeOptional(true)) {
                        actionsDefinition.setUseParameter(true);
                    }

                    String actionsValue = selectPermission.getActions();

                    useNullActions.setSelection(actionsValue == null);

                    if (actionsValue != null) {
                        actionsField.setText(actionsValue);
                    }
                }
            }
        }

        nameField.addVerifyListener(new VerifyListener() {
            public void verifyText(VerifyEvent e) {
                if (e.text != null && !ASCII_CHARSET_ENCODER.canEncode(e.text)) {
                    e.doit = false;
                }
            }
        });

        nameField.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                updateOKButton();
            }
        });

        nameField.getAccessible().addAccessibleListener(new AccessibleAdapter() {
            @Override
            public void getName(AccessibleEvent e) {
                e.result = MTJUIMessages.MEEPSecurityPermissionsDialog_targetFieldAccessibleName;
            }
        });

        actionsField.addVerifyListener(new VerifyListener() {
            public void verifyText(VerifyEvent e) {
                if (e.text != null && !ASCII_CHARSET_ENCODER.canEncode(e.text)) {
                    e.doit = false;
                }
            }
        });

        actionsField.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
                updateOKButton();
            }
        });

        actionsField.getAccessible().addAccessibleListener(new AccessibleAdapter() {
            @Override
            public void getName(AccessibleEvent e) {
                e.result = MTJUIMessages.MEEPSecurityPermissionsDialog_actionFieldAccessibleName;
            }
        });

        permissionsCombo.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                updateNameAndActions();
            }
        });

        permissionsCombo.getCombo().getAccessible().addAccessibleListener(new AccessibleAdapter() {
            @Override
            public void getName(AccessibleEvent e) {
                e.result = MTJUIMessages.MEEPSecurityPermissionsDialog_permissionComboAccessibleName;
            }
        });

        useNullName.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                updateNameAndActions();
            }
        });

        useNullActions.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                updateNameAndActions();
            }
        });

        return container;
    }

    @Override
    protected Control createContents(Composite parent) {
        try {
            return super.createContents(parent);
        } finally {
            updateNameAndActions();
        }
    }

    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        okButton = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
        createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
    }

    @Override
    protected void okPressed() {
        permission = permissionsFactory.getPermission(getSelectedDescriptor(), getNameValue(), getActionsValue(),
                getNumberOfParameters());

        super.okPressed();
    }

    @Override
    protected void cancelPressed() {
        permission = null;

        super.cancelPressed();
    }

    private void updateNameAndActions() {
        PermissionDescriptor permissionDescr = getSelectedDescriptor();

        nameDefinition.setEnabled(permissionDescr.supportsName());
        nameDefinition.setOptional(permissionDescr.supportsName() && permissionDescr.nameCanBeOptional());

        actionsDefinition.setEnabled(permissionDescr.supportsAction() && nameDefinition.useParameter());
        actionsDefinition.setOptional(permissionDescr.supportsAction() && nameDefinition.useParameter()
                && permissionDescr.actionsCanBeOptional(nameDefinition.useParameter()));

        useNullName.setEnabled(permissionDescr != null && permissionDescr.supportsName()
                && nameDefinition.useParameter());
        nameField.setEnabled(useNullName.isEnabled() && !useNullName.getSelection());

        useNullActions.setEnabled(permissionDescr != null && permissionDescr.supportsAction()
                && actionsDefinition.useParameter());
        actionsField.setEnabled(useNullActions.isEnabled() && !useNullActions.getSelection());

        updateOKButton();
    }

    private PermissionDescriptor getSelectedDescriptor() {
        return (PermissionDescriptor) ((StructuredSelection) permissionsCombo.getSelection()).getFirstElement();
    }

    private String getNameValue() {
        return getParameterValue(useNullName, nameField, false);
    }

    private String getActionsValue() {
        return getParameterValue(useNullActions, actionsField, true);
    }

    private String getParameterValue(Button useNull, Text parameterField, boolean trim) {
        if (useNull.getSelection()) {
            return null;
        } else {
            String result = parameterField.getText();

            if (trim) {
                result = result.trim();
            }

            return result;
        }
    }

    private int getNumberOfParameters() {
        int result = 0;

        if (nameDefinition.useParameter()) {
            result = result + 1;

            if (actionsDefinition.useParameter()) {
                result = result + 1;
            }
        }

        return result;
    }

    private void updateOKButton() {
        PermissionDescriptor permissionDescr = getSelectedDescriptor();

        boolean enable;

        if (permissionDescr == null) {
            enable = false;
        } else {
            PermissionError valid = permissionsFactory.validatePermission(permissionDescr, getNameValue(),
                    getActionsValue(), getNumberOfParameters());

            String errorMsg;

            switch (valid) {
                case INVALID_NAME:
                    errorMsg = MTJUIMessages.MEEPSecurityPermissionsDialog_Error_InvalidName;
                    break;
                case INVALID_ACTION:
                    errorMsg = MTJUIMessages.MEEPSecurityPermissionsDialog_Error_InvalidAction;
                    break;
                case UNDEFINED_NAME:
                    errorMsg = MTJUIMessages.MEEPSecurityPermissionsDialog_Error_UndefinedName;
                    break;
                case UNDEFINED_ACTION:
                    errorMsg = MTJUIMessages.MEEPSecurityPermissionsDialog_Error_UndefinedAction;
                    break;
                case CONFLICT:
                    errorMsg = MTJUIMessages.MEEPSecurityPermissionsDialog_Error_Conflict;
                    break;
                default:
                    errorMsg = null;
                    break;
            }

            enable = errorMsg == null;

            if (!enable) {
                errorLabel.setText(errorMsg);
            }
        }

        if (enable) {
            errorLabel.setText(" "); //$NON-NLS-1$
        }

        errorLabel.getParent().layout();

        okButton.setEnabled(enable);
    }


    private static class ParameterDefinition extends Composite {
        private Button useParameter;
        private Label parameterLabel;

        public ParameterDefinition(Composite parent, String labelText, SelectionListener actionListener) {
            super(parent, SWT.NONE);

            GridLayoutFactory.fillDefaults().margins(0, 0).spacing(0, 0).numColumns(2).applyTo(this);

            useParameter = new Button(this, SWT.CHECK);
            useParameter.setText(labelText);

            GridDataFactory.fillDefaults()
                    .align(SWT.FILL, SWT.CENTER)
                    .grab(true, false)
                    .exclude(true)
                    .applyTo(useParameter);

            if (actionListener != null) {
                useParameter.addSelectionListener(actionListener);
            }

            parameterLabel = new Label(this, SWT.NONE);
            parameterLabel.setText(labelText);

            GridDataFactory.fillDefaults()
                    .align(SWT.FILL, SWT.CENTER)
                    .grab(true, false)
                    .exclude(false)
                    .applyTo(parameterLabel);
        }

        public boolean useParameter() {
            return isEnabled() && (((GridData) useParameter.getLayoutData()).exclude || useParameter.getSelection());
        }

        public void setUseParameter(boolean value) {
            useParameter.setSelection(value);
        }

        public void setOptional(boolean optional) {
            useParameter.setVisible(optional);
            ((GridData) useParameter.getLayoutData()).exclude = !optional;

            parameterLabel.setVisible(!optional);
            ((GridData) parameterLabel.getLayoutData()).exclude = optional;

            getParent().layout();
        }

        @Override
        public void setEnabled(boolean enabled) {
            super.setEnabled(enabled);

            parameterLabel.setEnabled(enabled);
            useParameter.setEnabled(enabled);
        }
    }
}
