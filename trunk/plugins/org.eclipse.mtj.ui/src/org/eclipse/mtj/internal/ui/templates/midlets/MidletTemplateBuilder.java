/**
 * Copyright (c) 2009, 2012 Motorola and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial Version.
 *     Gorkem Ercan (Nokia)     - Merge New MIDlet wizard with New MIDlet from 
 *                                template wizard
 *     David Marques (Motorola) - Fixing resources creation.
 *     Pranav Gothadiya (Nokia) - Fixed Midlet templates override existing files.
 */
package org.eclipse.mtj.internal.ui.templates.midlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.osgi.util.NLS;

/**
 * MidletTemplateBuilder class builds code from a template object.
 * 
 * @author David Marques
 * @since 1.0
 */
public class MidletTemplateBuilder{

    private MIDletTemplate template;
    private Map<String, String>  dictionary;
    


    /**
     * Creates a MidletTemplateBuilder instance to build code from the specified
     * template using the specified provider to get the template dictionary.
     * 
     * @param template template object.
     * @param provider template provider.
     */
    public MidletTemplateBuilder(MIDletTemplate template,
            Map<String, String> dictionary) {
    	Assert.isNotNull(template);
    	Assert.isNotNull(dictionary);
        this.template   = template;
        this.dictionary = dictionary;
    }
 
    
    /**
     * Builds the code from the template into the specified source folder into
     * the specified package. The MIDlet class name specified will be used in
     * order to create the MIDlet class file.
     * 
     * @param _source target source folder.
     * @param _package target package name.
     * @param midletName MIDlet class name.
     * @param isAddingToJad if true adds a MIDlet entry into the application
     *            descriptor.
     * @throws MIDletTemplateBuilderException - In case any error occurs.
     */
    public IType build(IPackageFragmentRoot root, IPackageFragment pack,
            String midletName, IProgressMonitor monitor) throws MIDletTemplateBuilderException {
       
        IType createdType = null;
        try {
        	
    		monitor.beginTask("Creating MIDlet package", 4);
    		if (pack == null) {
    			pack= root.getPackageFragment(""); //$NON-NLS-1$
    		}
    		if (!pack.exists()) {
    			String packName= pack.getElementName();
    			pack= root.createPackageFragment(packName, true, new SubProgressMonitor(monitor, 1));
    		} else {
    			monitor.worked(1);
    		}
    		MIDletTemplateBuffer buffer = MIDletTemplateBuffer.createTemplateBuffer(template, dictionary, pack.getElementName(), midletName);
    		if (buffer == null ){
    			throw new MIDletTemplateBuilderException(
                        MTJUIMessages.MidletTemplateBuilder_UnableToGenerateClasses
                        );
    		}
			Map<String, String> conflictingJavaClassMap = createConflictingJavaClassMap(
					getConflictingFileNames(root, pack, midletName, monitor),
					pack);
			Map<String, String> conflictingResourcesMap = new HashMap<String, String>();
			createConflictingResourcesMap(root, conflictingResourcesMap);
            for (String name : buffer.getTemplateBuffers().keySet()) {
                boolean isMidlet = false;
                String code = buffer.getTemplateBuffers().get(name);
                String clazzName = NLS.bind("{0}.java", name.replace( //$NON-NLS-1$
                        ".template", Utils.EMPTY_STRING)); //$NON-NLS-1$
                if (clazzName.equalsIgnoreCase("$class_name$.java")) { //$NON-NLS-1$
                    clazzName = NLS.bind("{0}.java", midletName); //$NON-NLS-1$
                    isMidlet = true;
                }
				// Check if the file already exists in the package. If yes,
				// rename the file
				if (conflictingJavaClassMap.containsKey(clazzName)) {
					String newClazzName = conflictingJavaClassMap
							.get(clazzName);
					if (null != newClazzName && !newClazzName.isEmpty()) {
						clazzName = newClazzName;
					}
				}
				// Check if this Class code is referring any replaced class
				// names. If yes, replace all such references with new names
				for (String currentClazzName : conflictingJavaClassMap.keySet()) {
					String existingClazzReference = currentClazzName.replace(
							".java", Utils.EMPTY_STRING);

					if (code.contains(existingClazzReference)) {
						String newClazzReference = conflictingJavaClassMap.get(
								currentClazzName).replace(".java",
								Utils.EMPTY_STRING);
						if (null != newClazzReference
								&& !newClazzReference.isEmpty()) {
							code = code.replaceAll(existingClazzReference,
									newClazzReference);
						}
					}
				}
				// Check if this Class code is referring any replaced resource
				// names. If yes, replace all such references with new names
				for (String currentResourceName : conflictingResourcesMap
						.keySet()) {
					if (code.contains(currentResourceName)) {
						String newResourceName = conflictingResourcesMap
								.get(currentResourceName);
						if (null != newResourceName
								&& !newResourceName.isEmpty()) {
							code = code.replaceAll(currentResourceName,
									newResourceName);
						}
					}
				}

                ICompilationUnit cu = pack.createCompilationUnit(clazzName,
                        code, true, new SubProgressMonitor(monitor, 1));
                if (isMidlet) {
                    createdType = cu.getType(midletName);
                }
            }

            IProject project = root.getJavaProject().getProject();
            File folder = new File(project.getLocation().toOSString());
            File bundleFile = FileLocator.getBundleFile(template.getBundle());
	        if (!bundleFile.isDirectory()) {
	        	copyResources(new ZipFile(bundleFile), folder);
	        } else {
	        	 copyResources(bundleFile, folder);	        
	        }
	        monitor.worked(1);
            project.refreshLocal(IResource.DEPTH_INFINITE, new SubProgressMonitor(monitor, 1));
            return createdType;

        } catch (Exception e) {
            throw new MIDletTemplateBuilderException(
                    MTJUIMessages.MidletTemplateBuilder_UnableToGenerateClasses
                            + e.getMessage());
        }
    }
    
    public String[] getConflictingFileNames(IPackageFragmentRoot root, IPackageFragment pack,
            String midletName, IProgressMonitor monitor){
    	ArrayList<String> conflicts = new ArrayList<String>();
    	if(pack.exists()){ //Check for conflicting java files
    		MIDletTemplateBuffer buffer = MIDletTemplateBuffer.createTemplateBuffer(template, dictionary, pack.getElementName(), midletName);
    		for (String name : buffer.getTemplateBuffers().keySet()) {
                
                String clazzName = NLS.bind("{0}.java", name.replace( //$NON-NLS-1$
                        ".template", Utils.EMPTY_STRING)); //$NON-NLS-1$
                if (clazzName.equalsIgnoreCase("$class_name$.java")) { //$NON-NLS-1$
                    clazzName = NLS.bind("{0}.java", midletName); //$NON-NLS-1$
                }
                ICompilationUnit compilationUnit = pack.getCompilationUnit(clazzName);
				if(compilationUnit.exists()){
                	conflicts.add(compilationUnit.getElementName());
                }
    		}

    	}
    	// check conflicting resource files
    	
    	
    	return conflicts.toArray(new String[conflicts.size()]);
    	
    }

    /**
     * Copies the source file content into the target file.
     * 
     * @param source source file.
     * @param target target file.
     * @throws IOException If any IO error occurs.
     */
    private void copyFile(File source, File target) throws IOException {
        File file = null;
        if (source.isDirectory()) {
            file = new File(target, source.getName());
            if (!file.exists() && !file.mkdir()) {
                return;
            }
            File[] children = source.listFiles();
            for (File child : children) {
                copyFile(child, file);
            }
        } else {
        	file = new File(resolveFileName(target, source.getName()));
            FileOutputStream out = null;
            FileInputStream in = null;
            try {
                out = new FileOutputStream(file);
                in = new FileInputStream(source);
                copyStreams(in, out);
            } finally {
                out.close();
                in.close();
            }
        }
    }

    /**
     * Copies all content inside the plug-in template folder to the specified
     * target folder.
     * 
     * @param bundleFolder source folder.
     * @param projectFolder target folder.
     * @throws IOException If any IO error occurs.
     */
    private void copyResources(File bundleFolder, File projectFolder)
            throws IOException {
        String path = NLS
                .bind("templates/{0}/resources", this.template.getId()); //$NON-NLS-1$
        File resources = new File(bundleFolder, path);
        if (resources.exists() && resources.isDirectory()) {
            File[] children = resources.listFiles();
            for (File child : children) {
                copyFile(child, projectFolder);
            }
        }
    }

    /**
     * Copies all resources within the plug-in template resources folder to the
     * specified target folder.
     * 
     * @param zipFile plug-in JAR file.
     * @param projectFolder Target folder.
     * @throws IOException If any IO error occurs.
     */
    private void copyResources(ZipFile zipFile, File projectFolder)
            throws IOException {
        String expression = NLS.bind(
                "templates/{0}/resources/.+", this.template.getId()); //$NON-NLS-1$
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = entries.nextElement();
            if (Pattern.matches(expression, zipEntry.getName())) {
                IPath path = new Path(zipEntry.getName())
                        .removeFirstSegments(0x03);
                File file = new File(projectFolder, path.toString());
                if (!zipEntry.isDirectory()) {					
                	createFileFromZipFile(file, zipFile, zipEntry);
				} else {
					createFolderFromZipFile(file);
				}
            }
        }
    }

    /**
     * Creates the folder from the zip folder.
     * 
     * @param folder folder instance.
     */
    private void createFolderFromZipFile(File folder) {
    	if (!folder.exists()) {
    		folder.mkdir();
		}
	}

	/**
     * Reads an InputStream and writes the content into the target OutputStream.
     * 
     * @param in InputStream instance.
     * @param out OutputStream instance.
     * @throws IOException If any IO error occurs.
     */
    private void copyStreams(InputStream in, OutputStream out)
            throws IOException {
        int _byte = -1;
        while ((_byte = in.read()) != -1) {
            out.write(_byte);
        }
    }

    /**
     * Copies the ZipEntry file content to a target file.
     * 
     * @param file target file.
     * @param zipFile zip file.
     * @param zipEntry zip entry.
     * @throws IOException If any IO error occurs.
     */
    private void createFileFromZipFile(File file, ZipFile zipFile,
            ZipEntry zipEntry) throws IOException {
        OutputStream out = new FileOutputStream(file);
        InputStream in = zipFile.getInputStream(zipEntry);
        copyStreams(in, out);
        out.close();
        in.close();
    }
    
	/**
	 * Creates a map for the conflicting file names in the package.Map contains
	 * the new names as values.
	 * 
	 * @param conflictingFileNames
	 *            conflicting file names in the package.
	 * @param pack
	 *            target package.
	 */
	private Map<String, String> createConflictingJavaClassMap(
			String[] conflictingFileNames, IPackageFragment pack) {
		Map<String, String> replacingFileNamesMap = new HashMap<String, String>();
		for (int i = 0; i < conflictingFileNames.length; i++) {
			String clazzName = conflictingFileNames[i];
			String originalClazzName = conflictingFileNames[i];
			int index = 1;
			String newClazzName = null;
			while (pack.getCompilationUnit(clazzName).exists()) {
				newClazzName = NLS.bind("{0}.java",
						originalClazzName.replace(".java", Utils.EMPTY_STRING)
								+ index);
				index = index + 1;
				clazzName = newClazzName;
			}
			replacingFileNamesMap.put(originalClazzName, newClazzName);
		}
		return replacingFileNamesMap;
	}

	/**
	 * Creates a map for the conflicting resource names in the project.Map
	 * contains the new names as values.
	 * 
	 * @param root
	 *            Project root folder.
	 * @param conflictingResourcesMap
	 *            Map containing old names as keys and new names as values.
	 */
	private void createConflictingResourcesMap(IPackageFragmentRoot root,
			Map<String, String> conflictingResourcesMap) throws IOException {
		IProject project = root.getJavaProject().getProject();
		File folder = new File(project.getLocation().toOSString());
		File bundleFile = FileLocator.getBundleFile(template.getBundle());

		if (bundleFile.isDirectory()) {
			String path = NLS.bind(
					"templates/{0}/resources", this.template.getId()); //$NON-NLS-1$
			File resources = new File(bundleFile, path);
			if (resources.exists() && resources.isDirectory()) {
				File[] children = resources.listFiles();
				for (File child : children) {
					replaceResourceName(child, folder, conflictingResourcesMap);
				}
			}
		}
	}

	private void replaceResourceName(File source, File target,
			Map<String, String> conflictingResourcesMap) {
		File file = null;
		if (source.isDirectory()) {
			file = new File(target, source.getName());
			if (!file.exists() && !file.mkdir()) {
				return;
			}
			File[] children = source.listFiles();
			for (File child : children) {
				replaceResourceName(child, file, conflictingResourcesMap);
			}
		} else {
			String newFilePath = resolveFileName(target, source.getName());
			if (!newFilePath.equals(target.getAbsolutePath()
					.concat(File.separator).concat(source.getName()))) {
				String newFileName = newFilePath.substring(newFilePath
						.lastIndexOf(File.separator) + 1);
				conflictingResourcesMap.put(source.getName(), newFileName);
			}
		}
	}

	private String resolveFileName(File target, String fileName) {
		File newFile = new File(target, fileName);
		for (File file : target.listFiles()) {
			if (fileName.equals(file.getName())) {
				int index = 1;
				while (newFile.exists()) {
					String fileFirstPart = Utils.EMPTY_STRING;
					String fileExtension = Utils.EMPTY_STRING;
					int extensionIndex = fileName.lastIndexOf(".");
					if (extensionIndex > 0) {
						fileExtension = fileName.substring(extensionIndex);
						fileFirstPart = fileName.substring(0, extensionIndex)
								.concat("_") + index;
					} else {
						fileFirstPart = fileName.concat("_") + index;
					}
					newFile = new File(target,
							fileFirstPart.concat(fileExtension));
					index = index + 1;
				}
				break;
			}
		}
		return newFile.getAbsolutePath();
	}
    
}
