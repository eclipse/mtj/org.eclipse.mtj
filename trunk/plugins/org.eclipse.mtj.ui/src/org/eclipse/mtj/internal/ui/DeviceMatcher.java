/**
 * Copyright (c) 2009 Sony Ericsson.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Daniel Olsson (Sony Ericsson) - Initial contribution
 *     Gustavo de Paula (Individual) - Remove dependency with MTJCore.getDeviceMacher
 */
package org.eclipse.mtj.internal.ui;

import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.core.sdk.device.DeviceMatchCache;
import org.eclipse.mtj.internal.core.sdk.device.IDeviceMatchCache;
import org.eclipse.mtj.internal.core.sdk.device.IDeviceMatcher;
import org.eclipse.mtj.internal.ui.dialog.DeviceMatchDialog;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.Display;

/**
 * <p>
 * This class is a user interactive implementation of {@link IDeviceMatcher}. It
 * will prompt the user with a dialog to choose the best matching SDK, device
 * combination for the SDK, device that is not installed.
 * </p>
 * <p>
 * If told to by the user, it will also put the chosen value in a cache so that
 * subsequent user interaction for the same choice is not needed.
 * </p>
 * <p>
 * This implementation is called from non-UI code.
 * </p>
 * 
 * @author Daniel Olsson
 */
public class DeviceMatcher implements IDeviceMatcher {

    private IDevice device = null;
    private String configName = null;
    protected boolean saveMatch = false;

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.mtj.core.sdk.device.IDeviceMatcher#match(java.lang.String,
     * java.lang.String)
     */
    private IDevice matchDevice(final String configurationName,
            final String deviceGroup, final String deviceName, final Set<String> existingConfigNames) {
        try {
            Display display = Display.getDefault();
            display.syncExec(new Runnable() {
                public void run() {
                    DeviceMatchDialog dialog = new DeviceMatchDialog(Display
                            .getDefault().getActiveShell());
                    dialog.setConfigurationName(configurationName);
                    dialog.setDeviceGroup(deviceGroup);
                    dialog.setDeviceName(deviceName);
                    dialog.setExistingConfigNames(existingConfigNames);
                    if (dialog.open() == IStatus.OK) {
                        device = dialog.getSelectedDevice();
                        saveMatch = dialog.getToggleState();
                        configName = dialog.getConfigurationName();
                    }
                }
            });
        } catch (SWTException e) {
            e.printStackTrace();
        }
        if (device != null && saveMatch) {
            IDeviceMatchCache cache = DeviceMatchCache.getInstance();
            cache.saveInCache(cache.createKey(deviceGroup, deviceName), cache
                    .createValue(device));
        }
        return device;
    }

    public IDevice match(String deviceGroup, String deviceName) {
        return matchDevice(null, deviceGroup, deviceName, null);
    }

    public String[] match(String configurationName, String deviceGroup,
            String deviceName, Set<String> existingConfigNames) {
        device = matchDevice(configurationName, deviceGroup, deviceName, existingConfigNames);
        if (device != null) {
            return new String[] { configName, device.getSDKName(),
                    device.getName() };
        } else {
            return null;
        }
    }

}
