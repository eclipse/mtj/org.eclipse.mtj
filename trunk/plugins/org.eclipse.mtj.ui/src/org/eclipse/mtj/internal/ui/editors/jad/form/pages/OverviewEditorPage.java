/**
 * Copyright (c) 2008,2009 Motorola and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola)  - Initial version
 *     Diego Sandin (Motorola)  - Added runtime section
 *     Feng Wang (Sybase)       - Modify runtime section, replace Device Selector with
 *                                Configuration Manager, for Multi-configs support.
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     Fernando Rocha(Motorola) - Add validation to JAR URL.
 *     Fernando Rocha(Motorola) - Problem marker for the description file.
 *     Jon Dearden (Research In Motion) - Move fields from JADOTAPropertiesEditorPage 
 *                                        and JADPushRegistryEditorPage onto 
 *                                        JADOptionalPropertiesEditorPage [Bug 284452]
 */
package org.eclipse.mtj.internal.ui.editors.jad.form.pages;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.debug.ui.ILaunchShortcut;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.mtj.core.project.midp.DescriptorPropertyDescription;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.event.AddMTJRuntimeEvent;
import org.eclipse.mtj.core.project.runtime.event.IMTJRuntimeChangeListener;
import org.eclipse.mtj.core.project.runtime.event.IMTJRuntimeListChangeListener;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeDeviceChangeEvent;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeNameChangeEvent;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeWorkspaceSymbolSetsChangeEvent;
import org.eclipse.mtj.core.project.runtime.event.RemoveMTJRuntimeEvent;
import org.eclipse.mtj.core.project.runtime.event.SwitchActiveMTJRuntimeEvent;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.project.runtime.MTJRuntimeListUtils;
import org.eclipse.mtj.internal.core.sdk.device.midp.Configuration;
import org.eclipse.mtj.internal.core.sdk.device.midp.PackagingModel;
import org.eclipse.mtj.internal.core.sdk.device.midp.Profile;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.mtj.internal.ui.configurations.ConfigManageComponent;
import org.eclipse.mtj.internal.ui.editors.FormLayoutFactory;
import org.eclipse.mtj.internal.ui.editors.jad.form.JADFormEditor;
import org.eclipse.mtj.internal.ui.preferences.ExtendedStringFieldEditor;
import org.eclipse.mtj.internal.ui.wizards.export.antenna.AntennaExportAction;
import org.eclipse.mtj.internal.ui.wizards.export.packaging.ExportPackageAction;
import org.eclipse.mtj.ui.editors.jad.IFilteredJADPropertyEditor;
import org.eclipse.mtj.ui.editors.jad.IManifestPreferenceStore;
import org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage;
import org.eclipse.mtj.ui.editors.jad.ListDescriptorPropertyDescription;
import org.eclipse.mtj.ui.editors.jad.ListDescriptorPropertyDescription.IListDescriptorPropertyFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;

/**
 * @author Diego Madruga Sandin
 */
public class OverviewEditorPage extends JADPropertiesEditorPage implements
        IPropertyChangeListener, IMTJRuntimeListChangeListener,
        IMTJRuntimeChangeListener {

    public static final String PACKAGING_MODEL_PROPERTY = "PACKAGING_MODEL";
	
	public static final String[][] MIDLET_LIBLET_KEYS = {
		{IJADConstants.JAD_MIDLET_JAR_URL, IJADConstants.JAD_LIBLET_JAR_URL},
	    {IJADConstants.JAD_MIDLET_NAME, IJADConstants.JAD_LIBLET_NAME},
	    {IJADConstants.JAD_MIDLET_VENDOR, IJADConstants.JAD_LIBLET_VENDOR},
	    {IJADConstants.JAD_MIDLET_VERSION, IJADConstants.JAD_LIBLET_VERSION}};

	private Map<String, FieldEditor> fieldEditorsMap = new HashMap<String, FieldEditor>(); 
	private Map<String, FieldEditor> libletFieldEditorsMap = new HashMap<String, FieldEditor>();    
    private Map<String, FieldEditor> sharedFieldEditorsMap = new HashMap<String, FieldEditor>();;
    
    private List<DescriptorPropertyDescription> midletDescriptors = new ArrayList<DescriptorPropertyDescription>();
    private List<DescriptorPropertyDescription> libletDescriptors = new ArrayList<DescriptorPropertyDescription>();
    private List<DescriptorPropertyDescription> sharedDescriptors = new ArrayList<DescriptorPropertyDescription>();
        
    private Combo packagingModelCombo;
    
    private Composite midletComposite;
    
    private Composite libletComposite;
    
    private Composite activePanel;
    
    private List<Label> propertyLabels = new ArrayList<Label>();
    
    private PackagingModel packagingModel = PackagingModel.MIDLET;
    
    private List<ActiveDeviceChangeListener> activeDeviceChangeListeners = new ArrayList<ActiveDeviceChangeListener>();
    private List<DeviceProfileChangeListener> deviceProfileChangeListeners = new ArrayList<DeviceProfileChangeListener>();
    private List<PackagingModelChangeListener> packagingModelChangeListeners = new ArrayList<PackagingModelChangeListener>();
	
    /**
     * The page unique identifier
     */
    private static final String OVERVIEW_PAGEID = "overview"; //$NON-NLS-1$

    private ConfigManageComponent configManager;

    /**
     * The current setting of the jar URL after a setInput call. If changed on
     * save, we will trigger a clean build to cause the jar file to be
     * regenerated.
     */
    private String loadedJarUrl;

    private IMidletSuiteProject midletProject;

    private IJavaProject project;

    /**
     * A constructor that creates the Overview EditorPage and initializes it
     * with the editor.
     * 
     * @param editor the parent editor
     */
    public OverviewEditorPage(JADFormEditor editor) {
        super(editor, OVERVIEW_PAGEID, MTJUIMessages.OverviewEditorPage_title);

        this.project = JavaCore.create(((JADFormEditor) getEditor())
                .getJadFile().getProject());

        this.midletProject = MidletSuiteFactory
                .getMidletSuiteProject(this.project);
    }
    
    public void addActiveDeviceChangeListener(ActiveDeviceChangeListener listener) {
        activeDeviceChangeListeners.add(listener);
    }
    
    public void addDeviceProfileChangeListener(DeviceProfileChangeListener listener) {
        deviceProfileChangeListeners.add(listener);
    }
    
    public void addPackagingModelChangeListener(PackagingModelChangeListener listener) {
        packagingModelChangeListeners.add(listener);
    }

    public void activeMTJRuntimeSwitched(SwitchActiveMTJRuntimeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
        
        MTJRuntime newActiveMTJRuntime = event.getNewActiveMTJRuntime();
        IDevice newActiveDevice = newActiveMTJRuntime == null ? null : newActiveMTJRuntime.getDevice();

        if (newActiveDevice != null) {
            for (ActiveDeviceChangeListener listener : activeDeviceChangeListeners) {
                listener.activeDeviceChanged(newActiveDevice);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.configuration.IConfigurationsChangeListener#configurationAdded(org.eclipse.mtj.core.model.configuration.AddConfigEvent)
     */
    public void mtjRuntimeAdded(AddMTJRuntimeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.configuration.IConfigurationsChangeListener#configurationRemoved(org.eclipse.mtj.core.model.configuration.RemoveConfigEvent)
     */
    public void mtjRuntimeRemoved(RemoveMTJRuntimeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.configuration.IConfigurationChangeListener#deviceChanged(org.eclipse.mtj.core.model.configuration.ConfigDeviceChangeEvent)
     */
    public void deviceChanged(MTJRuntimeDeviceChangeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#dispose()
     */
    @Override
    public void dispose() {
        super.dispose();
        if (isDirty()) {
            configManager.performCancel();
        }
        configManager.dispose();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#doSave(org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void doSave(IProgressMonitor monitor) {
        monitor.setTaskName(getTitle());
        
        if (packagingModelCombo.getText().equals(PackagingModel.MIDLET.getName())) {
        	for (FieldEditor element : libletFieldEditorsMap.values()) {
    			if (element instanceof StringFieldEditor) {
    				((StringFieldEditor) element).setStringValue("");
    			}
    		}
        } else {
        	for (FieldEditor element : fieldEditorsMap.values()) {
    			if (element instanceof StringFieldEditor) {
    				((StringFieldEditor) element).setStringValue("");
    			}
    		}
        }

		for (FieldEditor element : libletFieldEditorsMap.values()) {
			element.store();
		}

		for (FieldEditor element : fieldEditorsMap.values()) {
			element.store();
		}
		
		for (FieldEditor element : sharedFieldEditorsMap.values()) {
			element.store();
		}

        setDirty(false);     
        
        validateFields();

        String jarPropertyName = IJADConstants.JAD_MIDLET_JAR_URL;
        
        if (packagingModelCombo.getText().equals(PackagingModel.LIBLET.getName())) {
        	jarPropertyName = IJADConstants.JAD_LIBLET_JAR_URL;
    	}
        
        String currentJarUrl = getPreferenceStore().getString(
        		jarPropertyName);

        Control jarURLControl = null;
        for (FieldEditor field : fieldEditorsMap.values()) {
            if (field instanceof ExtendedStringFieldEditor) {
                String fieldEditorLabel = field.getLabelText();
                if (fieldEditorLabel
                        .equals(MTJUIMessages.RequiredJADDesciptorsProvider_midlet_jar_url)) {
                    jarURLControl = ((ExtendedStringFieldEditor) field)
                            .getFieldEditorTextControl();
                    break;
                }
            }
        }

        if (!Utils.isValidFileName(currentJarUrl)) {
            getPreferenceStore().putValue(jarPropertyName,
                    loadedJarUrl);
            getErrorMessageManager()
                    .removeMessage(
                            "textLength_" + MTJUIMessages.RequiredJADDesciptorsProvider_midlet_jar_url, jarURLControl); //$NON-NLS-1$
        } else if (!currentJarUrl.substring(currentJarUrl.length() - 4)
                .equalsIgnoreCase(".jar")) { //$NON-NLS-1$
            getPreferenceStore().putValue(jarPropertyName,
                    loadedJarUrl);
            getErrorMessageManager()
                    .removeMessage(
                            "textLength_" + MTJUIMessages.RequiredJADDesciptorsProvider_midlet_jar_url, jarURLControl); //$NON-NLS-1$
        }

        if (!currentJarUrl.equals(loadedJarUrl)) {
            ((JADFormEditor) getEditor()).setCleanRequired(true);
        }
        IDevice device = configManager.getActiveConfiguration().getDevice();
        if ((midletProject != null) && (device != null)) {
            try {
                midletProject.refreshClasspath(monitor,
                        getPreferenceStore().getString(IJADConstants.JAD_MICROEDITION_CONFIG),
                        getPreferenceStore().getString(IJADConstants.JAD_MICROEDITION_PROFILE));
                configManager.performFinish();
                midletProject.saveMetaData();
            } catch (CoreException e) {
                e.printStackTrace();
            }
        }

    }
    
    /**
     * Validate editor fields
     */
    public void validateFields() {
    	Collection<FieldEditor> verifiedFields = null;
    	
    	if (packagingModelCombo.isDisposed()) {
    		return;
    	}
    	
    	if (packagingModelCombo.getText().equals(PackagingModel.MIDLET.getName())) { 
    		verifiedFields = fieldEditorsMap.values();
    	} else if (packagingModelCombo.getText().equals(PackagingModel.LIBLET.getName())) {
    		verifiedFields = libletFieldEditorsMap.values();
    	}
    	
    	for (FieldEditor field : verifiedFields) {
			if (field instanceof ExtendedStringFieldEditor) {
				String fieldEditorLabel = field.getLabelText();
				Text control = ((ExtendedStringFieldEditor) field)
						.getFieldEditorTextControl();
				if (control.getText().equals(Utils.EMPTY_STRING)) {
					getErrorMessageManager()
							.addMessage(
									"textLength_" + fieldEditorLabel, //$NON-NLS-1$
									MTJUIMessages.OverviewEditorPage_empty_field_error_msg,
									null, IMessageProvider.ERROR, control);
				}
			}
		}
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getTitle()
     */
    @Override
    public String getTitle() {
        return MTJUIMessages.OverviewEditorPage_title;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#linkActivated(org.eclipse.ui.forms.events.HyperlinkEvent)
     */
    @Override
    public void linkActivated(HyperlinkEvent e) {
        String href = (String) e.getHref();
        if (href.equals("package")) { //$NON-NLS-1$
            new ExportPackageAction(getJavaProject()).run();
        } else if (href.equals("antenna")) { //$NON-NLS-1$
            new AntennaExportAction(project).run();
        } else if (href.startsWith("launchShortcut.")) { //$NON-NLS-1$
            handleLaunchShortcut(href);
        }
    }

    public void nameChanged(MTJRuntimeNameChangeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
     */
    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getProperty().equals(FieldEditor.VALUE)) {
            if (event.getSource() instanceof FieldEditor) {
                String preferenceName = ((FieldEditor) event.getSource()).getPreferenceName();
                
                if (event.getSource() instanceof ExtendedStringFieldEditor) {
                    boolean isLiblet = packagingModelCombo.getText().equals(PackagingModel.LIBLET.getName());
                    if ((!isLiblet && fieldEditorsMap.containsKey(preferenceName))
                            || (isLiblet && libletFieldEditorsMap.containsKey(preferenceName))) {
    
                        String fieldEditorLabel = ((FieldEditor) event.getSource()).getLabelText();
    
                        Control c = ((ExtendedStringFieldEditor) event.getSource()).getFieldEditorTextControl();
    
                        if (event.getNewValue().equals(Utils.EMPTY_STRING)) {
    
                            getErrorMessageManager().addMessage("textLength_" + fieldEditorLabel, //$NON-NLS-1$
                                    MTJUIMessages.OverviewEditorPage_empty_field_error_msg, null, IMessageProvider.ERROR, c);
                        } else if (((ExtendedStringFieldEditor) event.getSource()).getPreferenceName().equals(
                                IJADConstants.JAD_MIDLET_JAR_URL)) {
                            String jarFileName = (String) event.getNewValue();
                            if (jarFileName.length() < 4 || !jarFileName.substring(jarFileName.length() - 4).equalsIgnoreCase(".jar")) { //$NON-NLS-1$
                                getErrorMessageManager().addMessage(
                                        "textLength_" + fieldEditorLabel, //$NON-NLS-1$
                                        MTJUIMessages.J2MEProjectPropertiesPage_validatePage_jar_error, null,
                                        IMessageProvider.ERROR, c);
                            } else if (!Utils.isValidFileName(jarFileName)) {
                                getErrorMessageManager().addMessage(
                                        "textLength_" + fieldEditorLabel, //$NON-NLS-1$
                                        MTJUIMessages.OverviewEditorPage_invalid_jar_file_name, null,
                                        IMessageProvider.ERROR, c);
                            } else {
                                getErrorMessageManager().removeMessage("textLength_" + fieldEditorLabel, c); //$NON-NLS-1$
                            }
                        } else {
                            getErrorMessageManager().removeMessage("textLength_" + fieldEditorLabel, c); //$NON-NLS-1$
                        }
                    }
                }
                
                if (IJADConstants.JAD_MICROEDITION_PROFILE.equals(preferenceName)) {
                    for (DeviceProfileChangeListener listener : deviceProfileChangeListeners) {
                        listener.deviceProfileChanged((String) event.getNewValue());
                    }
                }
            }
            setDirty(true);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.configuration.IConfigurationChangeListener#symbolSetChanged()
     */
    public void symbolSetChanged() {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.model.configuration.IConfigurationChangeListener#workspaceScopeSymbolSetsChanged(org.eclipse.mtj.core.model.configuration.ConfigWorkspaceSymbolSetsChangeEvent)
     */
    public void workspaceScopeSymbolSetsChanged(
            MTJRuntimeWorkspaceSymbolSetsChangeEvent event) {
        setDirty(MTJRuntimeListUtils.isMTJRuntimeListDirty(midletProject));
    }

    /**
     * Create section for debugging options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createDebuginSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticSection(toolkit, parent,
                MTJUIMessages.OverviewEditorPage_debugging_section_title);
        Composite container = createStaticSectionClient(toolkit, section, null);

        FormText text = createClient(container,
                MTJUIMessages.overviewPage_launchsection_debuglinks, toolkit,
                this);

        text.setImage("debugMidlet", MTJUIPluginImages.DESC_DEBUG_MIDLET //$NON-NLS-1$
                .createImage());
        text.setImage("debugjad", MTJUIPluginImages.DESC_DEBUG_JAD //$NON-NLS-1$
                .createImage());
        text.setImage("debugOta", MTJUIPluginImages.DESC_DEBUG_OTA //$NON-NLS-1$
                .createImage());

        section.setClient(container);
    }

    /**
     * Create section for exporting options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createExportingSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticSection(toolkit, parent,
                MTJUIMessages.OverviewEditorPage_exporting_section_title);
        Composite container = createStaticSectionClient(toolkit, section, null);

        FormText text = createClient(container,
                MTJUIMessages.overviewPage_exporting, toolkit, this);

        text.setImage("antenna", MTJUIPluginImages.DESC_ANT.createImage()); //$NON-NLS-1$

        section.setClient(container);
    }

    /**
     * Create section for required information about the application.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createOverviewSection(final IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticBasicSection(toolkit, parent,
                MTJUIMessages.overviewPage_requiredsection_title,
                MTJUIMessages.overviewPage_requiredsection_description);

        Composite sectionClient = createStaticSectionClient(toolkit, section, null);
        sectionClient.setLayout(FormLayoutFactory
                .createSectionClientTableWrapLayout(false, 1));

        createSectionContent(managedForm, sectionClient, this);
        section.setClient(sectionClient);

    }
    
    @Override
    protected void createSectionContent(IManagedForm managedForm,
            Composite composite, final IPropertyChangeListener propertyChangeListener) {

        FormToolkit toolkit = managedForm.getToolkit();

        composite.setLayout(FormLayoutFactory.createSectionClientGridLayout(
                false, 2));
        
        new Label(composite, SWT.NONE);
        new Label(composite, SWT.NONE);
                
        initPropertyDescriptors();    

		Label packagingModelLabel = new Label(composite, SWT.NONE);
		packagingModelLabel
				.setText(MTJUIMessages.jadPropertiesEditorPage_PackagingModel_text);
		propertyLabels.add(packagingModelLabel);

		packagingModelCombo = new Combo(composite, SWT.READ_ONLY);
		packagingModelCombo.add(PackagingModel.MIDLET.getName());
		packagingModelCombo.add(PackagingModel.LIBLET.getName());
		packagingModelCombo.select(0);
		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		packagingModelCombo.setLayoutData(gridData);

		activePanel = new Composite(composite, SWT.NONE);
		gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		activePanel.setLayoutData(gridData);

		final StackLayout stackLayout = new StackLayout();
		activePanel.setLayout(stackLayout);

		packagingModelCombo.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				PackagingModel newPackagingModel = null;

				if (packagingModelCombo.getText().equals(PackagingModel.MIDLET.getName())) {
					newPackagingModel = PackagingModel.MIDLET;
					if (newPackagingModel == packagingModel) {
						return;
					}

					for (int i = 0; i < MIDLET_LIBLET_KEYS.length; i++) {
						((StringFieldEditor) fieldEditorsMap
								.get(MIDLET_LIBLET_KEYS[i][0])).setStringValue(((StringFieldEditor) libletFieldEditorsMap
										.get(MIDLET_LIBLET_KEYS[i][1])).getStringValue());
						((StringFieldEditor) fieldEditorsMap
								.get(MIDLET_LIBLET_KEYS[i][0])).setEmptyStringAllowed(false);
						((StringFieldEditor) libletFieldEditorsMap
								.get(MIDLET_LIBLET_KEYS[i][1])).setEmptyStringAllowed(true);
					}
				} else {
					newPackagingModel = PackagingModel.LIBLET;
					if (newPackagingModel == packagingModel) {
						return;
					}

					for (int i = 0; i < MIDLET_LIBLET_KEYS.length; i++) {
						((StringFieldEditor) libletFieldEditorsMap
								.get(MIDLET_LIBLET_KEYS[i][1])).setStringValue(((StringFieldEditor) fieldEditorsMap
										.get(MIDLET_LIBLET_KEYS[i][0])).getStringValue());
						((StringFieldEditor) fieldEditorsMap
								.get(MIDLET_LIBLET_KEYS[i][0])).setEmptyStringAllowed(true);
						((StringFieldEditor) libletFieldEditorsMap
								.get(MIDLET_LIBLET_KEYS[i][1])).setEmptyStringAllowed(false);
					}
				}

				setPackagingModel(newPackagingModel);
			}
		});

		libletComposite = new Composite(activePanel, SWT.NONE);
		libletComposite.setLayout(FormLayoutFactory
				.createSectionClientGridLayout(false, 2));

		initPropertyEditors(libletDescriptors, libletFieldEditorsMap,
				propertyChangeListener, libletComposite, toolkit);
        
              
        midletComposite = new Composite(activePanel, SWT.NONE);
        midletComposite.setLayout(FormLayoutFactory.createSectionClientGridLayout(
                            false, 2));
        initPropertyEditors(midletDescriptors, fieldEditorsMap, propertyChangeListener,
        		midletComposite, toolkit);
        
        initPropertyEditors(sharedDescriptors, sharedFieldEditorsMap, propertyChangeListener,
        		composite, toolkit);
        
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				PackagingModel newPackagingModel = PackagingModel.MIDLET;

				if (((StringFieldEditor) fieldEditorsMap
						.get(IJADConstants.JAD_MIDLET_NAME)).getStringValue().equals("")
						&& !((StringFieldEditor) libletFieldEditorsMap
								.get(IJADConstants.JAD_LIBLET_NAME)).getStringValue().equals("")) {
					if (!packagingModelCombo.isDisposed()) {
						packagingModelCombo.select(1);
					}
					newPackagingModel = PackagingModel.LIBLET;
				}

				setPackagingModel(newPackagingModel);
			}
		});
        
        int maxLabelWidth = 0;
        for (Label label : propertyLabels) {
        	int labelWidth = label.computeSize(SWT.DEFAULT, SWT.DEFAULT).x;
            maxLabelWidth = labelWidth > maxLabelWidth ? labelWidth : maxLabelWidth;
        }
        for (Label label : propertyLabels) {
        	gridData = new GridData();
            gridData.widthHint = maxLabelWidth;
            label.setLayoutData(gridData);
        }

        updateEditComponents();
        addContextHelp(midletComposite);
    }
    
    private void setPackagingModel(PackagingModel packagingModel) {
    	this.packagingModel = packagingModel;
      
    	if (activePanel != null && !activePanel.isDisposed()) {
    	    if (packagingModel == PackagingModel.MIDLET) {
        	    ((StackLayout) activePanel.getLayout()).topControl = midletComposite;
            } else if(packagingModel == PackagingModel.LIBLET) {
        	    ((StackLayout) activePanel.getLayout()).topControl = libletComposite;
            }
    	    activePanel.layout();
    	}
    	    
    	FieldEditor profileEditor = sharedFieldEditorsMap.get(IJADConstants.JAD_MICROEDITION_PROFILE);     
    	if (profileEditor != null) {
     	   	((IFilteredJADPropertyEditor) profileEditor).performFiltering(PACKAGING_MODEL_PROPERTY, packagingModel.toString());
     	}
    	
    	for (PackagingModelChangeListener listener : packagingModelChangeListeners) {
            listener.packagingModelChanged(packagingModel);
        }
    }
    
    private void initPropertyDescriptors() {
    	for (DescriptorPropertyDescription d : getDescriptors()) {
        	if (d.isLibletDescriptor()) {
        		libletDescriptors.add(d);
        	} else {
        		if (d.getPropertyName().equals(IJADConstants.JAD_MICROEDITION_CONFIG)
        				|| d.getPropertyName().equals(IJADConstants.JAD_MICROEDITION_PROFILE)) {
        			sharedDescriptors.add(d);
        			if (d.getPropertyName().equals(IJADConstants.JAD_MICROEDITION_PROFILE)) {
        				((ListDescriptorPropertyDescription) d).setFilter(new IListDescriptorPropertyFilter() {
 
        					private String[][] libletSupportedProfiles = new String[][] { 
        							new String[] { Profile.MEEP_80.getName(), Profile.MEEP_80.toString() } 
        					};
        					
							public String[] getFilterPropertyNames() {
								return new String[]{PACKAGING_MODEL_PROPERTY};
							}

							public String[][] filterNamesAndValues(
									Map<String, Object> filterProperties,
									String[][] namesAndValues) {
								if (PackagingModel.LIBLET.toString().equals(filterProperties.get(PACKAGING_MODEL_PROPERTY))) {
									return libletSupportedProfiles;
								}

								return namesAndValues;
							}
        					
        				});
        			}
        		} else {
        			midletDescriptors.add(d);
        		}		
        	}
        }   
    }
    
    private void initPropertyEditors(List<DescriptorPropertyDescription> descriptors,
    		Map<String, FieldEditor> editorsMap,
    		final IPropertyChangeListener propertyChangeListener, Composite parent, 
    		FormToolkit toolkit) {
    	Map<String, List<IFilteredJADPropertyEditor>> dependentEditors = new HashMap<String, List<IFilteredJADPropertyEditor>>();
    	
    	for (DescriptorPropertyDescription descriptor : descriptors) {

        	FieldEditor fieldEditor = null;
        	
            switch (descriptor.getDataType()) {

            case DescriptorPropertyDescription.DATATYPE_INT:
            	fieldEditor = createIntegerFieldEditor(toolkit, parent,
            			descriptor);
                break;

            case DescriptorPropertyDescription.DATATYPE_LIST:
            	fieldEditor = createComboFieldEditor(toolkit, parent,
            			descriptor);
                break;

            case DescriptorPropertyDescription.DATATYPE_URL:
            case DescriptorPropertyDescription.DATATYPE_STRING:
            default:
            	fieldEditor = createStringFieldEditor(toolkit, parent,
            			descriptor);
                break;
            }
            
            editorsMap.put(descriptor.getPropertyName(), fieldEditor);
            
            if (fieldEditor instanceof IFilteredJADPropertyEditor) {
            	IFilteredJADPropertyEditor filteredEditor = (IFilteredJADPropertyEditor) fieldEditor;
            	String[] filterPropertyNames = filteredEditor.getFilterPropertyNames();
            	
            	if (filterPropertyNames != null) {
            		for (String filterPropertyName : filterPropertyNames) {
						List<IFilteredJADPropertyEditor> propertyDependentEditors = dependentEditors.get(filterPropertyName);
						
						if (propertyDependentEditors == null) {
							propertyDependentEditors = new ArrayList<IFilteredJADPropertyEditor>();
							dependentEditors.put(filterPropertyName, propertyDependentEditors);
						}
						
						propertyDependentEditors.add(filteredEditor);
					}
            	}
            }

            Label label = fieldEditor.getLabelControl(parent);
            toolkit.adapt(label, false, false);
            propertyLabels.add(label);
            
            // Listen for property change events on the editor
            fieldEditor.setPropertyChangeListener(propertyChangeListener);
        }
        
        for (Entry<String, List<IFilteredJADPropertyEditor>> dependentEditorsEntry : dependentEditors.entrySet()) {
			final String masterEditorPropertyName = dependentEditorsEntry.getKey();
			FieldEditor masterEditor = editorsMap.get(masterEditorPropertyName);
			
			if (masterEditor != null) {
				final List<IFilteredJADPropertyEditor> propertyDependentEditors = dependentEditorsEntry.getValue();
				masterEditor.setPropertyChangeListener(new IPropertyChangeListener() {
					public void propertyChange(final PropertyChangeEvent event) {
						propertyChangeListener.propertyChange(event);
						
						if (IFilteredJADPropertyEditor.UPDATE_FILTER_NOTIFICATION.equals(event.getProperty())) {
    						for (final IFilteredJADPropertyEditor propertyDependentEditor : propertyDependentEditors) {
								propertyDependentEditor.performFiltering(masterEditorPropertyName, event.getNewValue());
    						}
						}
					}
				});
			}
		}

        // Adapt the Combo instances...
        Control[] children = parent.getChildren();
        for (Control control : children) {
            if (control instanceof Combo) {
                toolkit.adapt(control, false, false);
            }
        }
    }

    @Override
    protected void updateEditComponents() {
		IPreferenceStore store = (IPreferenceStore) getPreferenceStore();
		
		validateStore(store);
		
		for (FieldEditor fieldEditor : fieldEditorsMap.values()) {
			fieldEditor.setPreferenceStore(store);
			fieldEditor.load();
		}

		for (FieldEditor fieldEditor : libletFieldEditorsMap.values()) {
			fieldEditor.setPreferenceStore(store);
			fieldEditor.load();
		}

	    for (FieldEditor fieldEditor : sharedFieldEditorsMap.values()) {
		    fieldEditor.setPreferenceStore(store);
		    fieldEditor.load();
		}

	    for (FieldEditor fieldEditor : fieldEditorsMap.values()) {
	        if (fieldEditor instanceof IFilteredJADPropertyEditor) {
                ((IFilteredJADPropertyEditor) fieldEditor).updateDependencies();
            }
        }

        for (FieldEditor fieldEditor : libletFieldEditorsMap.values()) {
            if (fieldEditor instanceof IFilteredJADPropertyEditor) {
                ((IFilteredJADPropertyEditor) fieldEditor).updateDependencies();
            }
        }

        for (FieldEditor fieldEditor : sharedFieldEditorsMap.values()) {
            if (fieldEditor instanceof IFilteredJADPropertyEditor) {
                ((IFilteredJADPropertyEditor) fieldEditor).updateDependencies();
            }
        }
    }
    
    private PackagingModel extractPackagingModel(IPreferenceStore store) {
        PackagingModel result = PackagingModel.MIDLET;
        
        if (store.getString(IJADConstants.JAD_MIDLET_NAME).equals("")
                && !store.getString(IJADConstants.JAD_LIBLET_NAME).equals("")) {
            result = PackagingModel.LIBLET;
        }
        
        return result;
    }
    
    private void validateStore(IPreferenceStore store) {
        PackagingModel newPackagingModel = extractPackagingModel(store);
        
    	int firstIndex = newPackagingModel == PackagingModel.MIDLET ? 0 : 1;
    	int secondIndex = newPackagingModel == PackagingModel.MIDLET ? 1 : 0;
    	
    	if (packagingModelCombo != null) {
            packagingModelCombo.select(firstIndex);            
    	}
    	
    	if (newPackagingModel != packagingModel) {
    		setPackagingModel(newPackagingModel);
    	}
    	
    	for (int i=0; i<MIDLET_LIBLET_KEYS.length; i++) {
    		if (store.getString(MIDLET_LIBLET_KEYS[i][firstIndex]).equals("")
    				&& !store.getString(MIDLET_LIBLET_KEYS[i][secondIndex]).equals("")) {
    			store.setValue(MIDLET_LIBLET_KEYS[i][firstIndex],
    					store.getString(MIDLET_LIBLET_KEYS[i][secondIndex]));
    		}
    		store.setValue(MIDLET_LIBLET_KEYS[i][secondIndex], "");
    	}
    	
    	if (newPackagingModel == PackagingModel.LIBLET) {
    		if (!store.getString(IJADConstants.JAD_MICROEDITION_PROFILE)
    				.equals(Profile.MEEP_80.toString())) { 
    		    store.setValue(IJADConstants.JAD_MICROEDITION_PROFILE, Profile.MEEP_80.toString());
    		}
    		if (!store.getString(IJADConstants.JAD_MICROEDITION_CONFIG)
    				.equals(Configuration.CLDC_18.toString())) {
    		    store.setValue(IJADConstants.JAD_MICROEDITION_CONFIG, Configuration.CLDC_18.toString());
    		}
    	}
    	
		String tagRegEx = "[1-9]+[0-9]*";
		String[] keys = ((IManifestPreferenceStore) store).preferenceNames();
		for (String key : keys) {
			if (key.matches(IJADConstants.JAD_MIDLET_DEPENDENCY + tagRegEx)
					|| key.matches(IJADConstants.JAD_LIBLET_DEPENDENCY + tagRegEx)
					|| key.matches(IJADConstants.JAD_MIDLET_DEPENDENCY_JAD_URL + tagRegEx)
					|| key.matches(IJADConstants.JAD_LIBLET_DEPENDENCY_JAD_URL + tagRegEx)) {
				store.setValue(key, "");
			}
		}
    }
    
    /**
     * Create section for packaging options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createPackagingSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticSection(toolkit, parent,
                MTJUIMessages.OverviewEditorPage_packaging_section_title);
        Composite container = createStaticSectionClient(toolkit, section, null);

        FormText text = createClient(container,
                MTJUIMessages.overviewPage_deploying, toolkit, this);

        text.setImage("package", MTJUIPluginImages.DESC_PACKAGE.createImage()); //$NON-NLS-1$

        section.setClient(container);
    }

    /**
     * Create section for running options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createRunningSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        // Running links
        Section section = createStaticSection(toolkit, parent,
                MTJUIMessages.OverviewEditorPage_running_section_title);
        Composite container = createStaticSectionClient(toolkit, section, null);

        FormText text = createClient(container,
                MTJUIMessages.overviewPage_launchsection_runlinks, toolkit,
                this);

        text.setImage("runMidlet", MTJUIPluginImages.DESC_RUN_MIDLET //$NON-NLS-1$
                .createImage());
        text.setImage("runjad", MTJUIPluginImages.DESC_RUN_JAD.createImage()); //$NON-NLS-1$
        text.setImage("runOta", MTJUIPluginImages.DESC_RUN_OTA.createImage()); //$NON-NLS-1$

        section.setClient(container);
    }

    /**
     * Create section for debugging options.
     * 
     * @param managedForm a managed form that wraps a form widget.
     * @param parent the section parent.
     * @param toolkit The toolkit responsible for creating SWT controls adapted
     *            to work in Eclipse forms.
     */
    private void createRuntimeSection(IManagedForm managedForm,
            Composite parent, FormToolkit toolkit) {

        Section section = createStaticBasicSection(toolkit, parent,
                MTJUIMessages.overviewPage_runtimesection_title,
                MTJUIMessages.overviewPage_runtimesection_description);

        Composite sectionClient = createStaticSectionClient(toolkit, section, null);
        sectionClient.setLayout(FormLayoutFactory
                .createSectionClientGridLayout(true, 1));

        createRuntimeSectionContent(managedForm, sectionClient, this);
        section.setClient(sectionClient);
    }

    /**
     * @param managedForm
     * @param sectionClient
     * @param overviewEditorPage
     */
    private void createRuntimeSectionContent(IManagedForm managedForm,
            Composite sectionClient, OverviewEditorPage overviewEditorPage) {

        configManager = new ConfigManageComponent(midletProject);
        configManager.setIncludeGroup(false);
        configManager.createContents(sectionClient);
        configManager.setConfigurationsChangeListener(this);
        configManager.setConfigurationChangeListener(this);
    }

    /**
     * Fill the page body with all available sections.
     * 
     * @param managedForm the managed form that wraps the form widget
     */
    private void fillEditorPageBody(IManagedForm managedForm) {

        FormToolkit toolkit = managedForm.getToolkit();

        Composite body = managedForm.getForm().getBody();
        body.setLayout(FormLayoutFactory.createFormTableWrapLayout(true, 2));

        Composite left = toolkit.createComposite(body);
        left.setLayout(FormLayoutFactory
                .createFormPaneTableWrapLayout(false, 1));
        left.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB));

        createOverviewSection(managedForm, left, toolkit);
        createPackagingSection(managedForm, left, toolkit);
        createExportingSection(managedForm, left, toolkit);

        Composite right = toolkit.createComposite(body);
        right.setLayout(FormLayoutFactory.createFormPaneTableWrapLayout(false,
                1));
        right.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB));

        createRunningSection(managedForm, right, toolkit);
        createDebuginSection(managedForm, right, toolkit);
        createRuntimeSection(managedForm, right, toolkit);

    }

    /**
     * Handles a launch shortcut activation.
     * 
     * @param href the launch shortcut. The format of href should be
     *            <code>launchShortcut.&lt;mode&gt;.&lt;launchShortcutId&gt;</code>
     */
    private void handleLaunchShortcut(String href) {

        href = href.substring(15);
        int index = href.indexOf('.');
        if (index < 0) {
            return; // error. Format of href should be
        }
        // launchShortcut.<mode>.<launchShortcutId>
        String mode = href.substring(0, index);
        String id = href.substring(index + 1);

        IExtensionRegistry registry = Platform.getExtensionRegistry();
        IConfigurationElement[] elements = registry
                .getConfigurationElementsFor("org.eclipse.debug.ui.launchShortcuts"); //$NON-NLS-1$
        for (IConfigurationElement element : elements) {
            if (id.equals(element.getAttribute("id"))) { //$NON-NLS-1$
                try {
                    ILaunchShortcut shortcut = (ILaunchShortcut) element
                            .createExecutableExtension("class"); //$NON-NLS-1$
                    shortcut.launch(new StructuredSelection(project), mode);
                } catch (CoreException e1) {
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#addContextHelp(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void addContextHelp(Composite c) {
        PlatformUI.getWorkbench().getHelpSystem().setHelp(c,
                "org.eclipse.mtj.ui.help_JADRequiredPropertiesEditorPage"); //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
     */
    @Override
    protected void createFormContent(IManagedForm managedForm) {

        final ScrolledForm form = managedForm.getForm();
        FormToolkit toolkit = managedForm.getToolkit();

        form.setText(getTitle());
        toolkit.decorateFormHeading(form.getForm());

        createErrorMessageHandler(managedForm);

        /*
         * launch the help system UI, displaying the documentation identified by
         * the href parameter.
         */
        final String href = getHelpResource();
        if (href != null) {
            IToolBarManager manager = form.getToolBarManager();
            Action helpAction = new Action(
                    MTJUIMessages.OverviewEditorPage_help_action) {
                @Override
                public void run() {
                    PlatformUI.getWorkbench().getHelpSystem()
                            .displayHelpResource(href);
                }
            };

            helpAction.setImageDescriptor(MTJUIPluginImages.DESC_LINKTOHELP);
            manager.add(helpAction);
        }
        form.updateToolBar();

        fillEditorPageBody(managedForm);
        Display.getDefault().asyncExec(new Runnable() {
			public void run() {
                validateFields();
			}
        });
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage#getHelpResource()
     */
    @Override
    protected String getHelpResource() {
        return "/org.eclipse.mtj.doc.user/html/reference/editors/jad_editor/overview.html"; //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionDescription()
     */
    @Override
    protected String getSectionDescription() {
        return Utils.EMPTY_STRING;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#getSectionTitle()
     */
    @Override
    protected String getSectionTitle() {
        return Utils.EMPTY_STRING;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.ui.editors.jad.JADPropertiesEditorPage#setInput(org.eclipse.ui.IEditorInput)
     */
    @Override
    protected void setInput(IEditorInput input) {
        super.setInput(input);

        // Store the current JAR URL setting for later save comparison
        loadedJarUrl = getPreferenceStore().getString(
                IJADConstants.JAD_MIDLET_JAR_URL);
    }
    
    
    public static interface ActiveDeviceChangeListener {
        void activeDeviceChanged(IDevice newActiveDevice);
    }
    

    public static interface DeviceProfileChangeListener {
        void deviceProfileChanged(String newProfile);
    }
    
    public static interface PackagingModelChangeListener {
        void packagingModelChanged(PackagingModel newPackagingModel);
    }
}
