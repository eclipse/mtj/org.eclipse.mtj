/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Adding setEnabled() method.
 *     David Marques (Motorola) - Fixing buttons management.
 */
package org.eclipse.mtj.internal.ui.forms.blocks;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;

/**
 * GenericListBlock class provides a generic Ui list block that contains a
 * single column table and some customized buttons.
 * 
 * @author David Marques
 * 
 * @param <T> An implementation of the GenericListBlockItem to wrap the list
 *            items.
 */
public class GenericListBlock<T extends GenericListBlockItem> {

    private List<GenericListBlockListener<T>> listeners;
    private ButtonBarBlock buttonBar;
    private int buttons;
    private TableViewer viewer;
    private List<T> input;

    /**
     * Creates a GenericListBlock using the input as the model.
     * 
     * @param _parent parent composite.
     * @param _tableStyle the style of the block's table.
     * @param _buttons the buttons that will be available on the block. <b>See
     *            ButtonBarBlock class constants.</b>
     * @param _input the input model.
     */
    public GenericListBlock(Composite _parent, int _tableStyle, int _buttons,
            List<T> _input) {
        if (_input == null) {
            throw new IllegalArgumentException("Input can not be null.");
        }

        this.listeners = new ArrayList<GenericListBlockListener<T>>();
        this.input = _input;
        this.createControl(_parent, _tableStyle, _buttons);
    }

    /**
     * Adds the GenericListBlockListener to the block.
     * 
     * @param _listener listener to add.
     */
    public void addGenericListBlockListener(
            GenericListBlockListener<T> _listener) {
        this.listeners.add(_listener);
    }

    /**
     * Removes the GenericListBlockListener from the block.
     * 
     * @param _listener listener to remove.
     */
    public void removeGenericListBlockListener(
            GenericListBlockListener<T> _listener) {
        this.listeners.remove(_listener);
    }

    /**
     * Creates the Ui of the block.
     * 
     * @param parent parent composite.
     * @param _style table style.
     * @param _buttons block buttons.
     */
    private void createControl(Composite parent, int _style, int _buttons) {
        Composite mainComposite = new Composite(parent, SWT.NONE);
        mainComposite.setBackground(new Color(Display.getDefault(), 255, 255,
                255));

        GridLayout layout = new GridLayout(2, false);
        mainComposite.setLayout(layout);
        mainComposite
                .setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        Table table = new Table(mainComposite, _style);
        GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
        table.setLayoutData(data);

        viewer = new TableViewer(table);
        viewer.setContentProvider(new AdvancedListContentProvider());
        viewer.setLabelProvider(new AdvancedListLabelProvider());
        viewer.setInput(this.input);
        viewer.addSelectionChangedListener(new ISelectionChangedListener() {
            public void selectionChanged(SelectionChangedEvent event) {
                updateButtonsStates();
            }
        });
        this.createButtons(mainComposite, _buttons);
    }

    public void setButtonVisible(int index, boolean visible) {
        buttonBar.setButtonVisible(index, visible);
    }
    
    
    /**
     * Creates the buttons of the block.
     * 
     * @param mainComposite parent composite.
     * @param _buttons block buttons.
     */
    private void createButtons(Composite mainComposite, int _buttons) {
        Composite buttonsComposite = new Composite(mainComposite, SWT.NONE);
        buttonsComposite.setBackground(new Color(Display.getDefault(), 255,
                255, 255));

        GridData data = new GridData(SWT.FILL, SWT.CENTER, false, false);
        buttonsComposite.setLayoutData(data);

        GridLayoutFactory.fillDefaults()
                .equalWidth(true)
                .numColumns(1)
                .margins(0, 0)
                .spacing(0, 3)
                .applyTo(buttonsComposite);

        this.buttons = _buttons | ButtonBarBlock.BUTTON_UP | ButtonBarBlock.BUTTON_DOWN;
        buttonBar = new ButtonBarBlock(buttonsComposite, this.buttons);

        if ((_buttons & ButtonBarBlock.BUTTON_ADD) != 0) {
            buttonBar.setEnabled(ButtonBarBlock.BUTTON_ADD_INDEX, true);
            buttonBar.getButton(ButtonBarBlock.BUTTON_ADD_INDEX).addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    for (GenericListBlockListener<T> listener : listeners) {
                        listener.addButtonPressed();
                    }
                }
            });
        }
        
        if ((_buttons & ButtonBarBlock.BUTTON_EDIT) != 0) {
            buttonBar.getButton(ButtonBarBlock.BUTTON_EDIT_INDEX).addSelectionListener(new SelectionAdapter() {
                @SuppressWarnings("unchecked")
                @Override
                public void widgetSelected(SelectionEvent e) {
                    ISelection selection = viewer.getSelection();
                    if (!(selection instanceof IStructuredSelection)) {
                        return;
                    }

                    IStructuredSelection structuredSelection = (IStructuredSelection) selection;
                    
                    if (structuredSelection.size() != 1) {
                        return;
                    }
                    
                    T itemToEdit = (T) structuredSelection.getFirstElement();
                    
                    for (GenericListBlockListener<T> listener : listeners) {
                        listener.editButtonPressed(itemToEdit);
                    }
                }
            });
        }

        if ((_buttons & ButtonBarBlock.BUTTON_SCAN) != 0) {
            buttonBar.setEnabled(ButtonBarBlock.BUTTON_SCAN_INDEX, true);
            buttonBar.getButton(ButtonBarBlock.BUTTON_SCAN_INDEX).addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    for (GenericListBlockListener<T> listener : listeners) {
                        listener.scan();
                    }
                }
            });
        }

        if ((_buttons & ButtonBarBlock.BUTTON_REMOVE) != 0) {
            buttonBar.getButton(ButtonBarBlock.BUTTON_REMOVE_INDEX).addSelectionListener(new SelectionAdapter() {
                @SuppressWarnings("unchecked")
                @Override
                public void widgetSelected(SelectionEvent e) {
                    ISelection selection = viewer.getSelection();
                    if (!(selection instanceof IStructuredSelection)) {
                        return;
                    }

                    IStructuredSelection structuredSelection = (IStructuredSelection) selection;
                    List<T> items = (List<T>) structuredSelection.toList();
                    
                    if (items.size() == 0) {
                        return;
                    }
                    
                    int indexToSelect = -1;
                    
                    for (T item : items) {
                        indexToSelect = Math.max(indexToSelect, input.indexOf(item));
                    }
                    
                    indexToSelect = Math.min(indexToSelect + 1, input.size() - 1) - items.size();

                    input.removeAll(items);

                    update(indexToSelect < 0 ? null : new StructuredSelection(input.get(indexToSelect)));

                    for (GenericListBlockListener<T> listener : listeners) {
                        listener.itemsRemoved(items);
                    }
                }
            });
        }


        buttonBar.getButton(ButtonBarBlock.BUTTON_UP_INDEX).addSelectionListener(new SelectionAdapter() {
            @SuppressWarnings("unchecked")
            @Override
            public void widgetSelected(SelectionEvent e) {
                ISelection selection = viewer.getSelection();
                
                if (!(selection instanceof IStructuredSelection)) {
                    return;
                }

                IStructuredSelection structuredSelection = (IStructuredSelection) selection;
                
                if (structuredSelection.size() != 1) {
                    return;
                }
                
                T item = (T) ((IStructuredSelection) selection).getFirstElement();
                int index = input.indexOf(item);
                
                if (index > 0) {
                    input.remove(index);
                    input.add(--index, item);
                    viewer.refresh();
                }
                
                updateButtonsStates();
                
                for (GenericListBlockListener<T> listener : listeners) {
                    listener.upButtonPressed();
                }
            }
        });

        buttonBar.getButton(ButtonBarBlock.BUTTON_DOWN_INDEX).addSelectionListener(new SelectionAdapter() {
            @SuppressWarnings("unchecked")
            @Override
            public void widgetSelected(SelectionEvent e) {
                ISelection selection = viewer.getSelection();
                
                if (!(selection instanceof IStructuredSelection)) {
                    return;
                }
                
                IStructuredSelection structuredSelection = (IStructuredSelection) selection;
                
                if (structuredSelection.size() != 1) {
                    return;
                }
                
                T item = (T) ((IStructuredSelection) selection).getFirstElement();
                int index = input.indexOf(item);
                
                if (index < input.size() - 1) {
                    input.remove(index);
                    input.add(++index, item);
                    viewer.refresh();
                }
                
                updateButtonsStates();
                
                for (GenericListBlockListener<T> listener : listeners) {
                    listener.downButtonPressed();
                }
            }
        });
        
        updateButtonsStates();
    }

    /**
     * Updates the buttons states.
     */
    private void updateButtonsStates() {
        if (viewer.getControl().isEnabled()) {
            int selectionCount = 0;
            ISelection selection = viewer.getSelection();

            if (!selection.isEmpty() && (selection instanceof IStructuredSelection)) {
                selectionCount = ((IStructuredSelection) selection).size();
            }

            if ((buttons & ButtonBarBlock.BUTTON_REMOVE) != 0) {
                buttonBar.setEnabled(ButtonBarBlock.BUTTON_REMOVE_INDEX, selectionCount > 0);
            }
            
            if ((buttons & ButtonBarBlock.BUTTON_EDIT) != 0) {
                buttonBar.setEnabled(ButtonBarBlock.BUTTON_EDIT_INDEX, selectionCount == 1);
            }
            
            if ((buttons & ButtonBarBlock.BUTTON_UP) != 0) {
                boolean enabled = selectionCount == 1;
                
                if (enabled) {
                    enabled = input.indexOf(((IStructuredSelection) selection).getFirstElement()) > 0;
                }
                
                buttonBar.setEnabled(ButtonBarBlock.BUTTON_UP_INDEX, enabled);
            }
            
            if ((buttons & ButtonBarBlock.BUTTON_DOWN) != 0) {
                boolean enabled = selectionCount == 1;
                
                if (enabled) {
                    enabled = input.indexOf(((IStructuredSelection) selection).getFirstElement()) < input.size() - 1;
                }
                
                buttonBar.setEnabled(ButtonBarBlock.BUTTON_DOWN_INDEX, enabled);
            }
        }
    }

    private class AdvancedListContentProvider implements
            IStructuredContentProvider {

        /*
         * (non-Javadoc)
         * 
         * @see
         * org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse
         * .jface.viewers.Viewer, java.lang.Object, java.lang.Object)
         */
        public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
            viewer.refresh();
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * org.eclipse.jface.viewers.IStructuredContentProvider#getElements(
         * java.lang.Object)
         */
        public Object[] getElements(Object inputElement) {
            if (inputElement instanceof List) {
                List<?> items = (List<?>) inputElement;
                return items.toArray(new GenericListBlockItem[items.size()]);
            }
            return null;
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.eclipse.jface.viewers.IContentProvider#dispose()
         */
        public void dispose() {
        }
    }

    private class AdvancedListLabelProvider extends LabelProvider {

        /*
         * (non-Javadoc)
         * 
         * @see
         * org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
         */
        public Image getImage(Object element) {
            Image image = null;
            if (element instanceof GenericListBlockItem) {
                image = ((GenericListBlockItem) element).getImage();
            }
            return image;
        }

        /*
         * (non-Javadoc)
         * 
         * @see
         * org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
         */
        public String getText(Object element) {
            String text = null;
            if (element instanceof GenericListBlockItem) {
                text = ((GenericListBlockItem) element).getText();
            }
            return text;
        }
    }

    /**
     * Refreshes the block's viewer.
     */
    public void update(IStructuredSelection newSelection) {
        viewer.refresh();
        
        if (newSelection != null) {
            viewer.setSelection(newSelection, true);
        }
        
        updateButtonsStates();
    }

    /**
     * Sets all block child widgets enabled/disabled.
     * 
     * @param state true if enabled false otherwise.
     */
    public void setEnabled(boolean state) {
        this.viewer.getControl().setEnabled(state);
        this.buttonBar.setEnabled(state);
        this.updateButtonsStates();
    }

    public void setInput(List<T> input) {
        if (input == null) {
            throw new IllegalArgumentException("Input can not be null.");
        }
        
        this.input = input;
        
        viewer.setInput(input);
        viewer.refresh();
        
        viewer.setSelection(null);
        
        updateButtonsStates();
    }
}
