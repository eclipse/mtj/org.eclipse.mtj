package org.eclipse.mtj.internal.ui.dialog;

/**
 * Example data
 * 
 * @author Pranav Gothadiya (Nokia)
 * 
 */
public class CodeSampleData {

	/*
	 * Name of the example
	 */
	private String exampleName;

	/*
	 * Path of the example
	 */
	private String examplePath;

	/*
	 * Description of the example
	 */
	private String exampledDscription;

	public String getExampleName() {
		return exampleName;
	}

	public void setExampleName(String exampleName) {
		this.exampleName = exampleName;
	}

	public String getExamplePath() {
		return examplePath;
	}

	public void setExamplePath(String examplePath) {
		this.examplePath = examplePath;
	}

	public String getExampledDscription() {
		return exampledDscription;
	}

	public void setExampledDscription(String exampledDscription) {
		this.exampledDscription = exampledDscription;
	}

}
