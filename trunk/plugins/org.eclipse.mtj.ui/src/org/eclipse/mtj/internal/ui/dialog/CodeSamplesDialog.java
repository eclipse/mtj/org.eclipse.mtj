package org.eclipse.mtj.internal.ui.dialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.core.sdk.device.AbstractMIDPDevice;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.Hyperlink;

/**
 * Provides a dialog to user to select the example project from the default SDK
 * and allows user to load the project directly to the workspace.
 * 
 * @author Pranav Gothadiya (Nokia)
 * 
 */
public class CodeSamplesDialog extends TitleAreaDialog {

	private static final String ERROR_MSG_TYPE = "Error";
	private static final String ERROR_NO_SDK_FOUND = "No SDK found. Please import SDK through Device Management.";
	private static final String ERROR_COULD_NOT_LOAD_SDK = "Could not load default SDK. Please select right SDK.";
	private static final String NO_EXAMPLES_FOUND = "No examples found. Check if examples are available for the selected SDK.";
	private static final String ERROR_WHILE_IMPORTING_PROJECT = "Error while importing selected examples.";
	private TableItem item;
	private Text descriptionWindow;
	private int selectedIndex = -1;
	boolean Selected = false;
	private IPath locPath;
	private TableViewer tableViewer;
	private Color paintColorWhite, paintColorBlue, paintColorBlack;
	private ArrayList<Hyperlink> hyperLinks = new ArrayList<Hyperlink>();
	private ArrayList<CodeSampleData> examples = new ArrayList<CodeSampleData>();
	private String exampleFoldername;
	private String title = "Example Projects";
	private String selectedExampleDir;

	private boolean errUI = false;
	private String errMsg = "";
	private String strSelectedSDK = "";

	public CodeSamplesDialog(Shell parentShell, String message) {
		super(parentShell);
		// TODO: message variable - how to use?

		initializeSamplesDialog();

	}

	private void initializeSamplesDialog() {
		try {
			List<IDevice> deviceList = MTJCore.getDeviceRegistry()
					.getAllDevices();
			if (deviceList.size() > 0) {
				IDevice defaultDevice = MTJCore.getDeviceRegistry()
						.getDefaultDevice();
				if (defaultDevice == null) {
					setErrMsg(ERROR_COULD_NOT_LOAD_SDK);
				} else {

					AbstractMIDPDevice advice = (AbstractMIDPDevice) defaultDevice;
					File[] exampleFolders = getAbsoluteExampleFolder(advice);

					if (exampleFolders == null || exampleFolders.length <= 0) {
						setErrMsg(NO_EXAMPLES_FOUND);
					} else {
						examples = loadExamples(exampleFolders);
						if (examples == null || examples.size() <= 0) {
							setErrMsg(NO_EXAMPLES_FOUND);
						}
					}
				}
			} else {
				setErrMsg(ERROR_NO_SDK_FOUND);
			}
		} catch (PersistenceException e) {
			e.printStackTrace();
			setErrMsg("Error while looking for imported SDKs.");
		}
	}

	@Override
	public void create() {
		super.create();

		// Set the message
		if (isErrUI()) {
			setMessage(" Problem while loading example applications.",
					IMessageProvider.WARNING);
		} else {
			setMessage(
					"Click on the '"
							+ title
							+ "' to see the description of the code example.\n"
							+ " Click on the 'Load Project' button to load example into your workspace.",
					IMessageProvider.INFORMATION);
		}
	}

	/**
	 * 
	 * @param xmlInputStream
	 */
	public ArrayList<CodeSampleData> loadExamples(File[] exampleFolders) {
		try {
			ArrayList<CodeSampleData> examples = new ArrayList<CodeSampleData>();
			for (int i = 0; i < exampleFolders.length; i++) {
				File node = exampleFolders[i];
				if (node != null && node.isDirectory()) {

					// Check if project is valid. It should be either Eclipse or
					// Netbeans project
					boolean validExample = false;
					validExample = new File(node.getAbsoluteFile()
							+ "\\.project").exists()
							|| new File(node.getAbsoluteFile()
									+ "\\nbproject\\genfiles.properties")
									.exists();

					if (validExample) {
						CodeSampleData exampleData = new CodeSampleData();
						exampleData.setExampleName(node.getName());
						exampleData.setExamplePath(node.getAbsolutePath());
						exampleData.setExampledDscription("Resource Location: "
								+ node.getCanonicalPath());
						examples.add(exampleData);
					}
				}
			}

			return examples;

		} catch (Exception e) {
			System.out.println("Examples loading failed!");
			return null;
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {

		getShell().setText(title);

		// If no example present, better not to display UI
		// TODO: move this logic from here to CodeSampleAction class
		if (examples.size() == 0) {

			setErrUI(true);
			Label label3 = new Label(parent, SWT.NONE);
			GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
			gridData.horizontalSpan = 2;
			gridData.heightHint = 60;
			label3.setLayoutData(gridData);
			String msg = "\n\n Reason:\n " + getErrMsg();
			if (!getStrSelectedSDK().equals("")) {
				msg += "\n Selected SDK: " + getStrSelectedSDK();
			}
			label3.setText(msg);
		} else {

			try {
				GridLayout layout = new GridLayout();
				layout.numColumns = 3;
				parent.setLayout(layout);

				Label label1 = new Label(parent, SWT.NONE);
				GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
				gridData.horizontalSpan = 3;
				label1.setLayoutData(gridData);

				Composite composite = new Composite(parent, SWT.NONE);
				gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
				gridData.horizontalSpan = 3;
				gridData.heightHint = 220;
				composite.setLayoutData(gridData);

				TableColumnLayout Tlayout = new TableColumnLayout();
				composite.setLayout(Tlayout);

				tableViewer = new TableViewer(composite, SWT.BORDER
						| SWT.FULL_SELECTION);
				final Table table = tableViewer.getTable();
				table.setHeaderVisible(true);
				table.setLinesVisible(true);

				TableViewerColumn tableViewerColumn = new TableViewerColumn(
						tableViewer, SWT.NONE);
				TableColumn tblclmnFirst = tableViewerColumn.getColumn();
				Tlayout.setColumnData(tblclmnFirst, new ColumnWeightData(1,
						ColumnWeightData.MINIMUM_WIDTH, true));
				tblclmnFirst.setText("Sl. No.");

				TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(
						tableViewer, SWT.WRAP);
				TableColumn tblclmnLast = tableViewerColumn_1.getColumn();
				// Specify width using weights
				Tlayout.setColumnData(tblclmnLast, new ColumnWeightData(6,
						ColumnWeightData.MINIMUM_WIDTH, true));
				tblclmnLast.setText(title);

				for (int i = 1; i <= examples.size(); i++) {
					item = new TableItem(table, SWT.NONE);
					item.setText(0, String.valueOf(i));
				}

				TableItem[] items = table.getItems();

				Label label2 = new Label(parent, SWT.NONE);
				gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
				gridData.horizontalSpan = 3;
				label2.setLayoutData(gridData);

				Label label3 = new Label(parent, SWT.NONE);
				gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
				gridData.horizontalSpan = 3;
				label3.setLayoutData(gridData);
				label3.setText("Description:");

				descriptionWindow = new Text(parent, SWT.WRAP | SWT.BORDER
						| SWT.V_SCROLL | SWT.READ_ONLY);
				gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
				gridData.horizontalSpan = 3;
				gridData.heightHint = 150;
				descriptionWindow.setLayoutData(gridData);

				Label label_allign = new Label(parent, SWT.NONE);
				gridData = new GridData(GridData.BEGINNING, GridData.END,
						false, false);
				gridData.horizontalSpan = 2;
				label_allign.setLayoutData(gridData);

				Display display = composite.getDisplay();
				paintColorWhite = new Color(display, 255, 255, 255);
				paintColorBlue = new Color(display, 0, 0, 255);
				paintColorBlack = new Color(display, 0, 0, 0);
				hyperLinks.clear();

				for (int i = 0; i < examples.size(); i++) {
					final TableEditor editor = new TableEditor(table);

					final Hyperlink link = new Hyperlink(table, SWT.WRAP);
					link.setText(examples.get(i).getExampleName());
					link.setBackground(paintColorWhite);

					editor.grabHorizontal = true;
					editor.setEditor(link, items[i], 1);
					hyperLinks.add(link);

					link.addHyperlinkListener(new HyperlinkAdapter() {
						public void linkActivated(HyperlinkEvent e) {

							Hyperlink clickedLink = (Hyperlink) e.getSource();
							for (Hyperlink tmplink : hyperLinks) {
								if (tmplink != clickedLink) {
									tmplink.setForeground(paintColorBlack);
								}
							}
							clickedLink.setForeground(paintColorBlue);

							for (int i = 0; i < examples.size(); i++) {
								if (examples.get(i).getExampleName()
										.equals(e.getLabel())) {
									selectedIndex = i;
									descriptionWindow.setText(examples.get(i)
											.getExampledDscription());
									break;
								}

							}
						}
					});
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.toString());
			}
		}

		return parent;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		GridData gridData = new GridData(GridData.END, GridData.CENTER, false,
				false);
		parent.setLayoutData(gridData);

		// Create Add button
		createOkButton(parent, OK, "Load Project", true);

		// Create Cancel button
		Button cancelButton = createButton(parent, CANCEL, "Cancel", false);
		// Add a SelectionListener
		cancelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setReturnCode(CANCEL);
				close();
			}
		});
	}

	protected Button createOkButton(Composite parent, int id, String label,
			boolean defaultButton) {
		// increment the number of columns in the button bar
		((GridLayout) parent.getLayout()).numColumns++;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		if (isErrUI()) {
			button.setEnabled(!isErrUI());
		}

		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				loadExample();
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		setButtonLayoutData(button);
		return button;
	}

	private void loadExample() {
		if (selectedIndex == -1) {
			MessageDialog.openError(getShell(), ERROR_MSG_TYPE,
					"Please select example and try again.");
			return;
		} else {
			final String workSpacepath = getWorkspacePath();
			selectedExampleDir = examples.get(selectedIndex).getExamplePath();
			exampleFoldername = examples.get(selectedIndex).getExampleName();

			IProject project = ResourcesPlugin.getWorkspace().getRoot()
					.getProject(exampleFoldername);
			if (project != null && project.exists()) {
				MessageDialog
						.openError(
								getShell(),
								ERROR_MSG_TYPE,
								"Project '"
										+ exampleFoldername
										+ "' cannot be imported because already exist in the workspace");
				okPressed();
				return;
			}

			final File destpath = new File(workSpacepath + "\\"
					+ exampleFoldername);

			if (destpath.exists()) {
				MessageBox dialog = new MessageBox(getShell(),
						SWT.ICON_QUESTION | SWT.OK | SWT.CANCEL);
				dialog.setText("Import info");
				dialog.setMessage("Project folder '"
						+ exampleFoldername
						+ "' already exist in the workspace.\nDo you want to clean and import?");
				int returnCode = dialog.open();
				if (returnCode == SWT.CANCEL) {
					return;
				}// else will be handled in ProgressMonitorDialog thread
			}

			ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
			try {
				dialog.run(true, false, new IRunnableWithProgress() {
					public void run(IProgressMonitor monitor) {
						monitor.beginTask("Importing project "
								+ exampleFoldername + "...",
								IProgressMonitor.UNKNOWN);
						try {
							if (destpath.exists()) {
								recursiveDelete(destpath);
							}

							File srcPath = new File(selectedExampleDir);
							copyFolder(srcPath, destpath);

							locPath = new org.eclipse.core.runtime.Path(
									workSpacepath + "\\" + exampleFoldername);
							loadProjectToWorkspace(locPath, monitor);
						} catch (IOException e) {
							e.printStackTrace();
							MessageDialog.openError(getShell(), ERROR_MSG_TYPE,
									ERROR_WHILE_IMPORTING_PROJECT);
						} catch (CoreException e) {
							e.printStackTrace();

							MessageDialog.openError(getShell(), ERROR_MSG_TYPE,
									ERROR_WHILE_IMPORTING_PROJECT);
						} catch (Exception e) {
							e.printStackTrace();
							MessageDialog.openError(getShell(), ERROR_MSG_TYPE,
									ERROR_WHILE_IMPORTING_PROJECT);
						}
						monitor.done();
					}
				});
			} catch (InvocationTargetException e) {
				e.printStackTrace();
				MessageDialog.openError(getShell(), ERROR_MSG_TYPE,
						ERROR_WHILE_IMPORTING_PROJECT);
			} catch (InterruptedException e) {
				e.printStackTrace();
				MessageDialog.openError(getShell(), ERROR_MSG_TYPE,
						ERROR_WHILE_IMPORTING_PROJECT);
			}
			okPressed();
		}
	}

	private void recursiveDelete(File file) {
		if (!file.exists())
			return;
		if (file.isDirectory()) {
			for (File f : file.listFiles()) {
				recursiveDelete(f);
			}
		}
		file.delete();
	}

	private File[] getAbsoluteExampleFolder(AbstractMIDPDevice advice) {
		String executablePath = advice.getExecutable().getAbsolutePath();

		String path = null;

		if (executablePath != null && executablePath.indexOf("bin") != -1) {
			path = executablePath.substring(0,
					executablePath.lastIndexOf("bin") - 1);
			setStrSelectedSDK(path);
		}

		String examplePath = advice.getExamplePath();
		if (examplePath == null || examplePath.equals("")) {
			return null;
		}
		/*
		 * Latest Nokia SDK keeps examples applications outside of SDK. Path
		 * identification is needed.
		 */
		File exPath = new File(examplePath);
		if (exPath != null && exPath.exists()) {
			File[] exampleFolders = exPath.listFiles();
			if (exampleFolders != null && exampleFolders.length > 0) {
				return exampleFolders;
			}
		}

		// If no examples available
		return null;
	}

	private String getWorkspacePath() {
		String workSpacepath = null;
		URL workspace = Platform.getInstanceLocation().getURL();

		try {
			File file = new File(FileLocator.resolve(workspace).toURI());
			workSpacepath = file.getAbsolutePath();
			workSpacepath = workSpacepath.substring(0, workSpacepath.length());

			if (workSpacepath.indexOf("file:") != -1) {
				workSpacepath = workSpacepath.substring(6,
						workSpacepath.length());
			}

		} catch (URISyntaxException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return workSpacepath;
	}

	protected void loadProjectToWorkspace(IPath projectPath,
			IProgressMonitor monitor) throws CoreException {
		try {
			CodeSampleProjectImport.loadEclipseProjectToWorkSpace(projectPath);
		} catch (Exception e) {
			System.out.println("Try loading netbeans project");
			System.out.println(projectPath.toFile().getName());
			CodeSampleProjectImport.importNetbeansProject(projectPath, monitor);
		}
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void okPressed() {
		super.okPressed();
	}

	public static void copyFolder(File src, File dest) throws IOException {

		if (src.isDirectory()) {

			if (!dest.exists()) {
				dest.mkdir();
			}

			String files[] = src.list();
			for (String file : files) {
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				copyFolder(srcFile, destFile);
			}

		} else {
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dest);

			byte[] buffer = new byte[1024];

			int length;
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}

			in.close();
			out.close();
		}
	}

	public boolean isErrUI() {
		return errUI;
	}

	public void setErrUI(boolean errUI) {
		this.errUI = errUI;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getStrSelectedSDK() {
		return strSelectedSDK;
	}

	public void setStrSelectedSDK(String strSelectedSDK) {
		this.strSelectedSDK = strSelectedSDK;
	}

}
