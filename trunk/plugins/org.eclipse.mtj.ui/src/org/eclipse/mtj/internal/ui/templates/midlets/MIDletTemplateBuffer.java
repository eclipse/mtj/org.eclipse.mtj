/*******************************************************************************
 * Copyright (c) 2012 Nokia Corporation and/or its subsidiary(-ies).
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Nokia Corporation - initial implementation 
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.templates.midlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.osgi.util.NLS;
/**
 * A buffer that generates and holds all the templated files on a template.
 * 
 * @author Gorkem Ercan
 *
 */
public final class MIDletTemplateBuffer {
	private MIDletTemplate template;
	private Map<String, String> templateBuffers;
	private Map<String, String>  dictionary;
	private String packageName;
	private String clazzName;
	
	public static MIDletTemplateBuffer createTemplateBuffer (MIDletTemplate  template, Map<String, String> dictionary,
			String packageName, String clazzname){
        MIDletTemplateBuffer buffer = new MIDletTemplateBuffer(template,dictionary);
        buffer.clazzName = clazzname;
        buffer.packageName = packageName;
        try {
			buffer.initBuffer();
		} catch (IOException e) {
			return null;
		}
        return buffer;
	}
	
	public Map<String, String> getTemplateBuffers() {
		return templateBuffers;
	}

	private MIDletTemplateBuffer(MIDletTemplate template, Map<String, String> dictionary ){
		this.template = template;
		this.dictionary = dictionary;
		templateBuffers= new HashMap<String, String>();

 	}
	
	private void initBuffer() throws IOException{
        Object bundleObj = null;
		File file = FileLocator.getBundleFile(template.getBundle());
	        if (!file.isDirectory()) {
	            bundleObj = new ZipFile(file);
	            processTemplateFiles((ZipFile) bundleObj);
	        } else {
	            bundleObj = file;
	            processTemplateFiles((File) bundleObj);
	        }		
		
	}

	
    /**
     * Gets all template files within the templates folder inside the plug-in
     * folder.
     * 
     * @param folder plug-in folder.
     * @return a MAP with the template names as keys and template files as
     *         values.
     * @throws IOException 
     */
    private void processTemplateFiles(File folder) throws IOException {
        String path = NLS.bind("templates/{0}/java", this.template.getId()); //$NON-NLS-1$
        File javaFolder = new File(folder, path);
        if (javaFolder.exists() && javaFolder.isDirectory()) {
            File[] files = javaFolder.listFiles();
            for (File file : files) {
                if (file.isFile()
                        && Pattern.matches(".+[.template]", file.getName())) { //$NON-NLS-1$
                    templateBuffers.put(file.getName(), processTemplate(new FileInputStream(file)));
                }
            }
        }
    }

    /**
     * Gets all template ZipEntries within the plug-in JAR file.
     * 
     * @param zipFile plug-in JAR file.
     * @return a MAP with the template names as keys and template ZipEntries as
     *         values.
     * @throws IOException 
     */
    private void processTemplateFiles(ZipFile zipFile) throws IOException {
        String expression = NLS.bind(
                "templates/{0}/java/.+[.]template", this.template.getId()); //$NON-NLS-1$
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            if (Pattern.matches(expression, entry.getName())) {
                IPath path = new Path(entry.getName());
                templateBuffers.put(path.lastSegment(), processTemplate(zipFile.getInputStream(entry)));
            }
        }
    }
	
    /**
     * Processes the template code replacing all template tags for the providers
     * dictionary of values.
     * 
     * @param in Template InputStream.
     * @param _package target package.
     * @param _name MIDlet name.
     * @return the processed code as String.
     * @throws IOException If any IO error occurs.
     * @throws CoreException Any core error occurs.
     */
    private String processTemplate(InputStream in)
            throws IOException {
        BufferedReader reader = null;
        StringBuffer buffer = null;

        reader = new BufferedReader(new InputStreamReader(in));
        buffer = new StringBuffer();
        if ((packageName != null) && (packageName.length() > 0x00)) {
            buffer.append(NLS.bind("package {0};\n", packageName)); //$NON-NLS-1$
        }

        String line = null;
        while ((line = reader.readLine()) != null) {
            buffer.append(line).append("\n"); //$NON-NLS-1$
        }
        reader.close();

        String content = buffer.toString();
        content = content.replace("$class_name$", clazzName); //$NON-NLS-1$

        for (String key : dictionary.keySet()) {
            content = content.replace(key, dictionary.get(key));
        }
        return content;
    }
	
	
}
