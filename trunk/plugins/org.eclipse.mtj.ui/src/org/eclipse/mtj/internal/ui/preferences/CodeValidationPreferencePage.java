/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.ui.preferences;

import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.PixelConverter;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.*;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.CodeValidator;
import org.eclipse.mtj.core.build.CodeValidator.Severity;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.CodeValidationBuilder;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.ui.IEmbeddableWorkbenchPreferencePage;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.FormColors;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.SharedScrolledComposite;

/**
 * Preference page with code validation settings.
 */

public class CodeValidationPreferencePage extends PreferencePage implements IEmbeddableWorkbenchPreferencePage {
    private static final String SETTINGS_SECTION_NAME = "CodeValidationSettings"; //$NON-NLS-1$
    private static final String SETTINGS_EXPANDED = "expanded"; //$NON-NLS-1$

    private IJavaProject project;

    private List<CodeValidator> codeValidators;

    private List<ExpandableComposite> expandableComposites;

    private List<Map<String, ComboViewer>> markersCombos;

    public CodeValidationPreferencePage() {
        this(MTJUIPlugin.getDefault().getCorePreferenceStore(), null);
    }

    public CodeValidationPreferencePage(IPreferenceStore preferenceStore, IJavaProject project) {

        this.project = project;
        setPreferenceStore(preferenceStore);
    }

    @Override
    public Point computeSize() {
        Point size = super.computeSize();

        size.y = 10; //see bug 294763

        return size;
    }

    @Override
    protected Control createContents(Composite parent) {
        if (project != null) {
            noDefaultAndApplyButton();
        }

        PixelConverter pixelConverter = new PixelConverter(parent);
        Composite mainPanel = new Composite(parent, SWT.NONE);

        mainPanel.setFont(parent.getFont());

        GridLayoutFactory.fillDefaults().margins(0, 0).applyTo(mainPanel);

        final ScrolledPageContent scroll = new ScrolledPageContent(mainPanel);

        GridDataFactory.fillDefaults()
                .align(SWT.FILL, SWT.FILL)
                .grab(true, true)
                .hint(SWT.DEFAULT, pixelConverter.convertHeightInCharsToPixels(30))
                .applyTo(scroll);

        codeValidators = new ArrayList<CodeValidator>();

        codeValidators.addAll(CodeValidationBuilder.getCodeValidators(project));

        Collections.sort(codeValidators, new Comparator<CodeValidator>() {
            public int compare(CodeValidator cv1, CodeValidator cv2) {
                return cv1.getValidatorName().compareTo(cv2.getValidatorName());
            }
        });

        expandableComposites = new ArrayList<ExpandableComposite>();
        markersCombos = new ArrayList<Map<String, ComboViewer>>();

        for (CodeValidator codeValidator : codeValidators) {
            createCodeValidatorSection(codeValidator, scroll);
        }

        IDialogSettings settings = MTJUIPlugin.getDefault().getDialogSettings().getSection(SETTINGS_SECTION_NAME);

        restoreSectionExpansionStates(settings);

        return mainPanel;
    }

    private Severity readSeverity(IPreferenceStore preferenceStore, CodeValidator codeValidator, String markerID,
            Severity defaultValue) {
        String key = getMarkerStoreKey(codeValidator, markerID);

        if (preferenceStore.contains(key)) {
            try {
                return Severity.valueOf(preferenceStore.getString(key));
            } catch (Throwable t) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    private String getMarkerStoreKey(CodeValidator codeValidator, String markerID) {
        return codeValidator.getClass().getName() + "." + markerID; //$NON-NLS-1$
    }

    private Severity getDefaultSeverity(CodeValidator codeValidator, String markerID) {
        if (project == null) {
            return codeValidator.getDefaultSeverity(markerID);
        } else {
            return readSeverity(MTJUIPlugin.getDefault().getCorePreferenceStore(), codeValidator, markerID,
                    codeValidator.getDefaultSeverity(markerID));
        }
    }

    private Severity getSeverity(CodeValidator codeValidator, String markerID) {
        return readSeverity(getPreferenceStore(), codeValidator, markerID, getDefaultSeverity(codeValidator, markerID));
    }

    @Override
    public void dispose() {
        IDialogSettings settings = MTJUIPlugin.getDefault().getDialogSettings().addNewSection(SETTINGS_SECTION_NAME);

        storeSectionExpansionStates(settings);

        super.dispose();
    }

    private void restoreSectionExpansionStates(IDialogSettings settings) {
        for (int i = 0; i < expandableComposites.size(); i++) {
            ExpandableComposite excomposite = expandableComposites.get(i);

            if (settings == null) {
                excomposite.setExpanded(i == 0); // only expand the first node by default
            } else {
                excomposite.setExpanded(settings.getBoolean(SETTINGS_EXPANDED + String.valueOf(i)));
            }
        }
    }

    private void storeSectionExpansionStates(IDialogSettings settings) {
        for (int i = 0; i < expandableComposites.size(); i++) {
            ExpandableComposite expandableComposite = expandableComposites.get(i);
            settings.put(SETTINGS_EXPANDED + String.valueOf(i), expandableComposite.isExpanded());
        }
    }

    private void createCodeValidatorSection(CodeValidator codeValidator, final ScrolledPageContent scroll) {
        ExpandableComposite result = new ExpandableComposite(scroll.getBody(), SWT.NONE, ExpandableComposite.TWISTIE
                | ExpandableComposite.CLIENT_INDENT);

        result.setText(codeValidator.getValidatorName());

        result.setExpanded(false);
        result.setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DIALOG_FONT));

        GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, false).applyTo(result);

        result.addExpansionListener(new ExpansionAdapter() {
            @Override
            public void expansionStateChanged(ExpansionEvent e) {
                scroll.reflow(true);
            }
        });

        scroll.adaptChild(result);

        Composite inner = new Composite(result, SWT.NONE);

        inner.setFont(scroll.getBody().getFont());

        GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(false).margins(5, 5).applyTo(inner);

        result.setClient(inner);

        Map<String, ComboViewer> markerCombos = new LinkedHashMap<String, ComboViewer>();

        for (Entry<String, Severity[]> supportedMarkerConfigurations : codeValidator.getSupportedMarkersConfigurations()
                .entrySet()) {
            String markerID = supportedMarkerConfigurations.getKey();

            markerCombos.put(markerID,
                    addComboBox(inner, markerID, codeValidator, supportedMarkerConfigurations.getValue(), scroll));
        }

        expandableComposites.add(result);
        markersCombos.add(markerCombos);
    }

    private ComboViewer addComboBox(Composite parent, String markerID, CodeValidator codeValidator,
            Severity[] supportedSeverities, ScrolledPageContent scroll) {

        Label label = new Label(parent, SWT.LEFT);
        label.setFont(JFaceResources.getDialogFont());
        label.setText(MessageFormat.format(MTJUIMessages.CodeValidator_Marker_Label_Pattern,
                codeValidator.getMarkerName(markerID)));

        GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).grab(true, false).applyTo(label);

        ComboViewer comboViewer = new ComboViewer(parent, SWT.READ_ONLY);

        comboViewer.setContentProvider(new ArrayContentProvider());
        comboViewer.setLabelProvider(new LabelProvider() {
            @Override
            public String getText(Object element) {
                if (element instanceof Severity) {
                    switch ((Severity) element) {
                        case ERROR:
                            return MTJUIMessages.CodeValidator_Severity_Error;
                        case WARNING:
                            return MTJUIMessages.CodeValidator_Severity_Warning;
                        case IGNORE:
                            return MTJUIMessages.CodeValidator_Severity_Ignore;
                    }
                }

                return super.getText(element);
            }
        });

        comboViewer.setInput(supportedSeverities);

        Combo comboBox = comboViewer.getCombo();

        comboBox.setFont(JFaceResources.getDialogFont());

        scroll.adaptChild(comboBox);

        comboViewer.setSelection(new StructuredSelection(getSeverity(codeValidator, markerID)));

        GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).grab(false, false).applyTo(comboBox);

        addHighlight(parent, label, comboBox);

        return comboViewer;
    }

    private void addHighlight(final Composite parent, final Label label, final Combo comboBox) {
        comboBox.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                highlight(parent, label, comboBox, SWT.NONE);
            }

            public void focusGained(FocusEvent e) {
                highlight(parent, label, comboBox, SWT.COLOR_WIDGET_DARK_SHADOW);
            }
        });

        MouseTrackAdapter labelComboListener = new MouseTrackAdapter() {
            @Override
            public void mouseEnter(MouseEvent e) {
                highlight(parent, label, comboBox,
                        comboBox.isEnabled() ? comboBox.isFocusControl() ? SWT.COLOR_WIDGET_DARK_SHADOW
                                : SWT.COLOR_WIDGET_NORMAL_SHADOW : SWT.NONE);
            }

            @Override
            public void mouseExit(MouseEvent e) {
                if (!comboBox.isFocusControl())
                    highlight(parent, label, comboBox, SWT.NONE);
            }
        };
        comboBox.addMouseTrackListener(labelComboListener);
        label.addMouseTrackListener(labelComboListener);

        HighligherMouseMoveTrackListener parentListener = new HighligherMouseMoveTrackListener(parent, label, comboBox);

        parent.addMouseMoveListener(parentListener);
        parent.addMouseTrackListener(parentListener);
        parent.addMouseListener(parentListener);

        MouseAdapter labelClickListener = new MouseAdapter() {
            @Override
            public void mouseDown(MouseEvent e) {
                comboBox.setFocus();
            }
        };

        label.addMouseListener(labelClickListener);
    }

    private void highlight(final Composite parent, final Label label, final Combo comboBox, final int color) {
        Object data = label.getData();

        if (data == null) {
            if (color != SWT.NONE) {
                PaintListener painter = new HighlightPainter(parent, label, comboBox, color);

                parent.addPaintListener(painter);
                label.setData(painter);
            } else {
                return;
            }
        } else {
            if (color == SWT.NONE) {
                parent.removePaintListener((PaintListener) data);
                label.setData(null);
            } else if (color != ((HighlightPainter) data).getColor()) {
                ((HighlightPainter) data).setColor(color);
            } else {
                return;
            }
        }

        parent.redraw();
    }

    private boolean usesProjectSpecificSettings(IProject project) {
        return MTJUIPlugin.getCoreProjectPreferenceStore(project).getBoolean(
                IMTJCoreConstants.PREF_CODE_VALIDATION_USE_PROJECT);
    }

    @Override
    public boolean performOk() {
        IPreferenceStore store = getPreferenceStore();

        for (int i = 0; i < codeValidators.size(); i++) {
            CodeValidator codeValidator = codeValidators.get(i);
            Map<String, ComboViewer> markerCombos = markersCombos.get(i);

            for (Entry<String, ComboViewer> markerEntry : markerCombos.entrySet()) {
                String markerID = markerEntry.getKey();
                ComboViewer comboViewer = markerEntry.getValue();

                store.setValue(getMarkerStoreKey(codeValidator, markerID),
                        ((Severity) ((IStructuredSelection) comboViewer.getSelection()).getFirstElement()).name());
            }
        }

        buildSuites();

        return super.performOk();
    }

    @Override
    public void performDefaults() {
        for (int i = 0; i < codeValidators.size(); i++) {
            CodeValidator codeValidator = codeValidators.get(i);
            Map<String, ComboViewer> markerCombos = markersCombos.get(i);

            for (Entry<String, ComboViewer> markerEntry : markerCombos.entrySet()) {
                String markerID = markerEntry.getKey();
                ComboViewer comboViewer = markerEntry.getValue();

                comboViewer.setSelection(new StructuredSelection(getDefaultSeverity(codeValidator, markerID)));
            }
        }

        super.performDefaults();
    }

    @Override
    public void performApply() {
        super.performApply();
    }

    private void buildSuites() {
        List<IProject> toBuild = new ArrayList<IProject>();

        if (project == null) {
            IWorkspaceRoot root = MTJCore.getWorkspace().getRoot();
            IProject[] projects = root.getProjects();

            for (IProject project : projects) {
                try {
                    if (project.isOpen() && project.hasNature(IMTJCoreConstants.MTJ_NATURE_ID)
                            && !usesProjectSpecificSettings(project)
                            && !CodeValidationBuilder.getCodeValidators(JavaCore.create(project)).isEmpty()) {
                        toBuild.add(project);
                    }
                } catch (CoreException e) {
                    MTJLogger.log(IStatus.ERROR, MTJUIMessages.CodeValidationPreferencePage_error_build_midlet_suite, e);
                }
            }
        } else {
            toBuild.add(project.getProject());
        }

        doBuild(toBuild);
    }

    public void doBuild(final List<IProject> toBuild) {
        if (toBuild == null || toBuild.size() == 0) {
            return;
        }

        IRunnableWithProgress runnable = new IRunnableWithProgress() {
            public void run(IProgressMonitor monitor) throws InvocationTargetException {
                for (IProject element : toBuild) {
                    SubProgressMonitor subMonitor = new SubProgressMonitor(monitor, IProgressMonitor.UNKNOWN);
                    
                    try {
                        element.build(IncrementalProjectBuilder.FULL_BUILD, subMonitor);
                    } catch (CoreException e) {
                        MTJLogger.log(IStatus.WARNING, MTJUIMessages.CodeValidationPreferencePage_error_build_suite, e);
                    } finally {
                        subMonitor.done();
                    }
                }
            }
        };
        
        try {
            PlatformUI.getWorkbench().getProgressService().busyCursorWhile(runnable);
        } catch (Throwable t) {
            MTJLogger.log(IStatus.WARNING, MTJUIMessages.CodeValidationPreferencePage_error_build_suite, t);
        }
    }

    public void init(IWorkbench workbench) {}


    private class ScrolledPageContent extends SharedScrolledComposite {
        private FormToolkit formToolkit;

        public ScrolledPageContent(Composite parent) {
            this(parent, SWT.V_SCROLL | SWT.H_SCROLL);
        }

        public ScrolledPageContent(Composite parent, int style) {
            super(parent, style);

            FormColors colors = new FormColors(Display.getCurrent());

            colors.setBackground(null);
            colors.setForeground(null);

            formToolkit = new FormToolkit(colors);

            setFont(parent.getFont());

            setExpandHorizontal(true);
            setExpandVertical(true);

            Composite body = new Composite(this, SWT.NONE);

            body.setFont(parent.getFont());

            GridLayoutFactory.fillDefaults().margins(0, 0).applyTo(body);

            setContent(body);
        }

        public void adaptChild(Control childControl) {
            formToolkit.adapt(childControl, true, true);
        }

        public Composite getBody() {
            return (Composite) getContent();
        }
    }


    private class HighligherMouseMoveTrackListener extends MouseTrackAdapter implements MouseMoveListener,
            MouseListener {
        private Composite parent;
        private Label label;
        private Combo comboBox;

        public HighligherMouseMoveTrackListener(Composite parent, Label label, Combo comboBox) {
            this.parent = parent;
            this.label = label;
            this.comboBox = comboBox;
        }

        @Override
        public void mouseExit(MouseEvent e) {
            if (!comboBox.isFocusControl())
                highlight(parent, label, comboBox, SWT.NONE);
        }

        public void mouseMove(MouseEvent e) {
            int color = comboBox.isEnabled() ? comboBox.isFocusControl() ? SWT.COLOR_WIDGET_DARK_SHADOW
                    : isAroundLabel(e) ? SWT.COLOR_WIDGET_NORMAL_SHADOW : SWT.NONE : SWT.NONE;
            highlight(parent, label, comboBox, color);
        }

        public void mouseDown(MouseEvent e) {
            if (isAroundLabel(e))
                comboBox.setFocus();
        }

        public void mouseDoubleClick(MouseEvent e) {}

        public void mouseUp(MouseEvent e) {}

        private boolean isAroundLabel(MouseEvent e) {
            int lx = label.getLocation().x;
            Rectangle c = comboBox.getBounds();
            int x = e.x;
            int y = e.y;
            boolean isAroundLabel = lx - 5 < x && x < c.x && c.y - 2 < y && y < c.y + c.height + 2;
            return isAroundLabel;
        }
    }


    private static class HighlightPainter implements PaintListener {
        private final Composite parent;
        private final Label label;
        private final Combo comboBox;
        private int color;

        public HighlightPainter(Composite parent, Label label, Combo comboBox, int color) {
            this.parent = parent;
            this.label = label;
            this.comboBox = comboBox;
            this.color = color;
        }

        public void paintControl(PaintEvent e) {
            if (((GridData) label.getLayoutData()).exclude) {
                parent.removePaintListener(this);
                label.setData(null);
                return;
            }

            int gap = 7;
            int arrow = 3;
            Rectangle l = label.getBounds();
            Point c = comboBox.getLocation();

            e.gc.setForeground(e.display.getSystemColor(color));
            int x2 = c.x - gap;
            int y = l.y + l.height / 2 + 1;

            e.gc.drawLine(l.x + l.width + gap, y, x2, y);
            e.gc.drawLine(x2 - arrow, y - arrow, x2, y);
            e.gc.drawLine(x2 - arrow, y + arrow, x2, y);
        }

        public int getColor() {
            return color;
        }

        public void setColor(int color) {
            this.color = color;
        }
    }
}
