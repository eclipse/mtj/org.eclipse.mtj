/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Fixing antenna export bugs.
 *     Diego Madruga (Motorola) - User defined symbol sets were not exported [Bug 289186]
 */
package org.eclipse.mtj.internal.core.build.export.states;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.build.export.AntennaExportException;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.statemachine.StateMachine;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * CreateAntPreprocessTaskState creates the pre-processing target for the
 * build.xml file.
 * 
 * @author David Marques
 * @since 1.0
 */
public class CreateAntPreprocessTaskState extends AbstractCreateAntTaskState {

    /**
     * Creates a {@link CreateAntInitTaskState} instance bound to the specified
     * state machine in order to create pre-process target for the specified
     * project.
     * 
     * @param machine bound {@link StateMachine} instance.
     * @param project target {@link IMidletSuiteProject} instance.
     * @param _document target {@link Document}.
     */
    public CreateAntPreprocessTaskState(StateMachine _stateMachine,
            IMidletSuiteProject _suiteProject, Document _document, String buildFolder, String buildFile) {
        super(_stateMachine, _suiteProject, _document, buildFolder, buildFile );
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.mtj.internal.core.build.export.states.AbstractCreateAntTaskState
     * #onEnter(org.eclipse.mtj.core.project.runtime.MTJRuntime)
     */
    protected void onEnter(MTJRuntime runtime) throws AntennaExportException {
        Document document = getDocument();
        Element root = document.getDocumentElement();
        IProject project = getMidletSuiteProject().getProject();

        String configName = getFormatedName(runtime.getName());
        Element preprocess = XMLUtils.createTargetElement(document, root, NLS
                .bind("preprocess-{0}", configName), getDependencies(runtime)); //$NON-NLS-1$

        try {

            ISymbolSet symbolset = runtime.getSymbolSetForPreprocessing();

            List<ISymbolSet> sets = runtime.getWorkspaceScopeSymbolSets();
            if ((sets != null) && (!sets.isEmpty())) {
                for (ISymbolSet set : sets) {
                    symbolset.add(set.getSymbols());
                }
            }

            IFile symbolSetFile = createSymbolSetFile(configName, symbolset);

            writePreprocessTask(project, document, preprocess, runtime,
                    symbolSetFile);

            Set<IProject> requiredProjects = getRequiredProjects(project);
            if (requiredProjects.size() > 0) {
                for (IProject required : requiredProjects) {
                    writePreprocessTask(required, document, preprocess,
                            runtime, symbolSetFile);
                }

                Element copy = document.createElement("copy"); //$NON-NLS-1$
                preprocess.appendChild(copy);
                copy.setAttribute("overwrite", "true"); //$NON-NLS-1$ //$NON-NLS-2$
                copy
                        .setAttribute(
                                "todir", NLS //$NON-NLS-1$
                                        .bind(
                                                "{0}/{1}/{2}/classes/", new String[] { getBuildFolder(), configName, //$NON-NLS-1$
                                                        getFormatedName(project
                                                                .getName()) }));
                for (IProject required : requiredProjects) {
                    Element fileset = document.createElement("fileset"); //$NON-NLS-1$
                    copy.appendChild(fileset);
                    fileset
                            .setAttribute(
                                    "dir", NLS.bind("{0}/{1}/{2}/classes/", //$NON-NLS-1$ //$NON-NLS-2$
                                                    new String[] {
                                                            getBuildFolder(),
                                                            configName,
                                                            getFormatedName(required
                                                                    .getName()) }));
                }
            }
        } catch (IOException e) {
            throw new AntennaExportException(
                    e,
                    Messages.CreateAntPreprocessTaskState_UnableToCreateSymbolSet);
        }
    }

    /**
     * Writes the preprocess target for the specified project into the specified
     * document root for the specified runtime.
     * 
     * @param _project target project
     * @param _document target document
     * @param _root target root element
     * @param _runtime current runtime
     * @param _symbolSetFile sumbol set file
     * @throws AntennaExportException Any error occurs.
     */
    private void writePreprocessTask(IProject _project, Document _document,
            Element _root, MTJRuntime _runtime, IFile _symbolSetFile)
            throws AntennaExportException {
        IJavaProject javaProject = JavaCore.create(_project);
        if (javaProject == null) {
            return;
        }

        IMTJProject suiteProject = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);
        if (suiteProject == null) {
            return;
        }
        String configName = getFormatedName(_runtime.getName());

        IResource[] sources = Utils.getSourceFolders(suiteProject
                .getJavaProject());
        for (IResource source : sources) {
            Element wtkpreprocess = _document.createElement("wtkpreprocess"); //$NON-NLS-1$
            _root.appendChild(wtkpreprocess);

            wtkpreprocess.setAttribute("debuglevel", "info"); //$NON-NLS-1$ //$NON-NLS-2$
            wtkpreprocess.setAttribute("verbose", "true"); //$NON-NLS-1$ //$NON-NLS-2$
            wtkpreprocess.setAttribute("printsymbols", "true"); //$NON-NLS-1$ //$NON-NLS-2$
            wtkpreprocess.setAttribute("srcdir", NLS.bind("..{0}", source //$NON-NLS-1$ //$NON-NLS-2$
                    .getFullPath()));
            wtkpreprocess.setAttribute("destdir", NLS.bind( //$NON-NLS-1$
                    "{0}/{1}/{2}/classes/", new String[] { //$NON-NLS-1$
                            getBuildFolder(), configName,
                            getFormatedName(_project.getName()) }));

            Element symbolsFile = _document.createElement("symbols_file"); //$NON-NLS-1$
            symbolsFile
                    .setAttribute(
                            "name", NLS.bind("{0}/{1}/" //$NON-NLS-1$ //$NON-NLS-2$
                                            ,
                                            new String[] {
                                                    "${basedir}", _symbolSetFile.getProjectRelativePath().toString() })); //$NON-NLS-1$
            wtkpreprocess.appendChild(symbolsFile);
        }
    }

    /**
     * Creates the symbol set file for the specified {@link ISymbolSet}
     * instance.
     * 
     * @param configName current runtime configuration name.
     * @param symbolset symbol set instance.
     * @return the {@link IFile} handle to the target file.
     * @throws IOException Any IO error occurs.
     */
    private IFile createSymbolSetFile(String configName, ISymbolSet symbolset)
            throws IOException {
        StringBuffer buffer = new StringBuffer();
        for (ISymbol symbol : symbolset.getSymbols()) {
            buffer
                    .append(NLS
                            .bind(
                                    "{0}={1}\n", new String[] { symbol.getName(), symbol.getSafeValue() })); //$NON-NLS-1$
        }

        IProject project = getMidletSuiteProject().getProject();
        IFile file = project.getFile(NLS.bind(
                "mtj-build/{0}.symbols", configName)); //$NON-NLS-1$
        File symbols = file.getLocation().toFile();

        FileOutputStream stream = new FileOutputStream(symbols);
        stream.write(buffer.toString().getBytes());
        stream.flush();
        stream.close();
        return file;
    }
}
