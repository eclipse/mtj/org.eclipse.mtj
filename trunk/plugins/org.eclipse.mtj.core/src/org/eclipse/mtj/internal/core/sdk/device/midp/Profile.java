/**
 * Copyright (c) 2009, 2013 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Gorkem Ercan (Nokia) - enum definition mixed with configuration  
 *     T. Bakardzhiev (Prosyst) - IMP-NG Profile is written incorrectly in jad [Bug 405002]   
 */
package org.eclipse.mtj.internal.core.sdk.device.midp;

import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType;
import org.osgi.framework.Version;

/**
 * @author Diego Madruga Sandin
 */
public enum Profile implements IMIDPAPI {

    /**
     * Mobile Information Device Profile (1.0)
     */
    MIDP_10("MIDP", "Mobile Information Device Profile (1.0)", new Version( //$NON-NLS-1$//$NON-NLS-2$
            "1.0")), //$NON-NLS-1$

    /**
     * Mobile Information Device Profile (2.0)
     */
    MIDP_20("MIDP", "Mobile Information Device Profile (2.0)", new Version( //$NON-NLS-1$//$NON-NLS-2$
            "2.0")), //$NON-NLS-1$

    /**
     * Mobile Information Device Profile (2.1)
     */
    MIDP_21("MIDP", "Mobile Information Device Profile (2.1)", new Version( //$NON-NLS-1$//$NON-NLS-2$
            "2.1")), //$NON-NLS-1$

    /**
     * Information Module Profile (1.0)
     */
    IMP_10("IMP", "Information Module Profile (1.0)", new Version("1.0")), //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$

    /**
     * Information Module Profile (NG)
     */
    IMP_NG("IMP-NG", "Information Module Profile (NG)", Version.emptyVersion), //$NON-NLS-1$//$NON-NLS-2$
    
    /**
     * ME Embedded Profile (8.0)
     */
    MEEP_80("MEEP", "ME Embedded Profile (8.0)", new Version("8.0")); //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$

    private String identifier;
    private String name;
    private Version version;

    /**
     * @param identifier the profile identifier
     * @param name the profile name
     * @param version the profile version
     */
    Profile(String identifier, String name, Version version) {
        this.identifier = identifier;
        this.name = name;
        this.version = version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#getIdentifier()
     */
    public String getIdentifier() {
        return identifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#getVersion()
     */
    public Version getVersion() {
        return version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI#getType()
     */
    public MIDPAPIType getType() {
        return MIDPAPIType.PROFILE;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#setIdentifier(java.lang.String)
     */
    public void setIdentifier(String identifier) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#setName(java.lang.String)
     */
    public void setName(String name) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#setType(org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType)
     */
    public void setType(MIDPAPIType type) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IAPI#setVersion(org.osgi.framework.Version)
     */
    public void setVersion(Version version) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        Version version = getVersion();
        
        return getIdentifier()
                + (Version.emptyVersion.equals(version) ? "" : "-"
                        + version.getMajor() + "." + version.getMinor());
    }
}
