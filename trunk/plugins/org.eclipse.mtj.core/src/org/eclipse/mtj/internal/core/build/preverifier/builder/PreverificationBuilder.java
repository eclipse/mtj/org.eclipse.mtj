/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Fixed sharing violation while packaging.
 *     Gang  Ma (Sybase)        - Fixed runtime jar file locked after building.   
 *     Feng Wang (Sybase)       - Ensure runtime JAD contains correct size of 
 *                                runtime JAR, keeping runtime JAR unlocked 
 *                                after building                  
 *     Hugo Raniere (Motorola)  - Handling the case that there is no valid 
 *                                preverifier
 *     Diego Sandin (Motorola)  - Fix error when running builder in project 
 *                                with name equal to previously deleted project
 *     Feng Wang (Sybase) - Modify getDeploymentFolder(IProject, IProgressMonitor)
 *                          method, make a deployment folder for current active
 *                          configuration, to support build for multi-configs.
 *     David Marques (Motorola) - Extending MTJIncrementalProjectBuilder
 *     David Marques (Motorola) - Implementing obfuscation and signing states.
 *     David Marques(Motorola)  - Removing packaging from this builder.
 *     David Marques(Motorola)  - Fixing project dependencies support.
 */
package org.eclipse.mtj.internal.core.build.preverifier.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.MTJBuildState;
import org.eclipse.mtj.core.build.preverifier.PreverifierNotFoundException;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.*;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.sdk.device.midp.PackagingModel;
import org.eclipse.mtj.internal.core.util.RequiredProjectsCPEntryVisitor;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.osgi.util.NLS;

/**
 * Provides an incremental project builder implementation to do a J2ME
 * preverification of classes. It is imperative that this builder follow the
 * standard Java builder. The standard Java builder will generate the standard
 * compiled class. The preverifier will then preverify that generated class.
 * 
 * @author Craig Setera
 */
public class PreverificationBuilder extends MTJIncrementalProjectBuilder {

    private static BuildLoggingConfiguration buildLoggingConfig = BuildLoggingConfiguration.getInstance();
    private static BuildConsoleProxy consoleProxy = BuildConsoleProxy.getInstance();

    private static Map<IProject, Map<IProject, IClasspathEntry[]>> lastProjectsClassPath = new HashMap<IProject, Map<IProject, IClasspathEntry[]>>();

    /**
     * Clean the output of the specified project.
     * 
     * @param project
     * @param cleanDeployed
     * @param monitor
     * @throws JavaModelException
     * @throws CoreException
     */
    public static void cleanProject(IProject project, boolean cleanDeployed, IProgressMonitor monitor)
            throws JavaModelException, CoreException {

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(BuilderMessages.PreverificationBuilder_0, project));
        }

        IJavaProject javaProject = JavaCore.create(project);

        BuildInfo buildInfo = new BuildInfo(IncrementalProjectBuilder.CLEAN_BUILD, new HashMap<Object, Object>(),
                javaProject);

        // Clear and remove the old verified directory and runtime folders
        IFolder oldVerifiedFolder = project.getFolder(MTJCore.getVerifiedOutputDirectoryName());

        if (oldVerifiedFolder.exists()) {
            Utils.clearContainer(oldVerifiedFolder, monitor);
            oldVerifiedFolder.delete(true, monitor);
        }

        IFolder oldRuntimeFolder = project.getFolder(IMTJCoreConstants.TEMP_FOLDER_NAME).getFolder(
                IMTJCoreConstants.RUNTIME_FOLDER_NAME);
        if (oldRuntimeFolder.exists()) {
            Utils.clearContainer(oldRuntimeFolder, monitor);
            oldRuntimeFolder.delete(true, monitor);
        }

        // Clear the classes and libraries
        IFolder preverified = buildInfo.getMidletSuite().getVerifiedOutputFolder(monitor);
        if (preverified.exists()) {
            IResource[] children = preverified.members();
            for (IResource resource : children) {
                if (resource.getType() == IResource.FOLDER) {
                    Utils.clearContainer((IFolder) resource, monitor);
                } else {
                    resource.delete(true, monitor);
                }
            }
        }
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(NLS.bind(BuilderMessages.PreverificationBuilder_1, project));
        }
    }

    /**
     * Clear previous preverification markers for the given resource.
     * 
     * @throws CoreException
     */
    static void clearPreverifierMarkers(IProject ownerProject, IResource resource) throws CoreException {
        IMarker[] foundMarkers = resource.findMarkers(IMTJCoreConstants.JAVAME_PREVERIFY_PROBLEM_MARKER, false,
                IResource.DEPTH_INFINITE);

        for (IMarker marker : foundMarkers) {
            if (ownerProject.getName().equals(marker.getAttribute(ResourceDeltaBuilder.MARKER_ATTRIBUTE_BELONG_TO))) {
                marker.delete();
            }
        }
    }

    private void clearNoPreverifierFoundMarker() throws CoreException {
        getProject().deleteMarkers(IMTJCoreConstants.JAVAME_PREVERIFY_MISSING_PREVERIFIER_PROBLEM_MARKER, false,
                IResource.DEPTH_ZERO);
    }

    /**
     * Create an error marker on the project about no preverifier found.
     * 
     * @throws CoreException
     */
    private void createNoPreverifierFoundMarker() throws CoreException {
        final IProject project = getProject();
        IWorkspaceRunnable runnable = new IWorkspaceRunnable() {
            public void run(IProgressMonitor monitor) throws CoreException {
                IMarker marker = project.createMarker(IMTJCoreConstants.JAVAME_PREVERIFY_MISSING_PREVERIFIER_PROBLEM_MARKER);
                marker.setAttribute(ResourceDeltaBuilder.MARKER_ATTRIBUTE_BELONG_TO, project.getName());
                marker.setAttribute(IMarker.MESSAGE,
                        BuilderMessages.PreverificationBuilder_PreverifierNotFoundErrorMessage);
                marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
            }
        };
        project.getWorkspace().run(runnable, null);
    }

    /**
     * Get the projects that are required by the project being built.
     * 
     * @param javaProject
     * @param monitor
     * @return
     * @throws CoreException
     */
    private IJavaProject[] getRequiredProjects(IJavaProject javaProject, IProgressMonitor monitor) throws CoreException {
        RequiredProjectsCPEntryVisitor visitor = new RequiredProjectsCPEntryVisitor();
        visitor.getRunner().run(javaProject, visitor, monitor);

        ArrayList<IJavaProject> projects = visitor.getRequiredProjects();
        return projects.toArray(new IJavaProject[projects.size()]);
    }

    /**
     * Preverify the libraries associated with the current java project in the
     * build info.
     * 
     * @param buildInfo
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    private void preverifyLibraries(BuildInfo buildInfo, IProgressMonitor monitor) throws CoreException,
            PreverifierNotFoundException {
        IProject project = buildInfo.getCurrentJavaProject().getProject();
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_47 + project);
        }

        if ((project != null) && project.isAccessible()) {
            monitor.setTaskName(BuilderMessages.PreverificationBuilder_48 + project.getName());

            // Figure the resource delta to be used
            buildInfo.setCurrentResourceDelta(getDelta(project));
            // Hand off to the build helper for the heavy lifting
            ResourceDeltaBuilder deltaBuilder = new ResourceDeltaBuilder(buildInfo);
            deltaBuilder.preverifyLibraries(monitor);
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_49 + project);
        }
    }

    /**
     * Preverify the project based on the specified build information.
     * 
     * @param buildInfo
     * @param monitor
     * @throws CoreException
     * @throws PreverifierNotFoundException
     */
    private void preverifyProject(BuildInfo buildInfo, IProgressMonitor monitor) throws CoreException,
            PreverifierNotFoundException {
        IProject project = buildInfo.getCurrentJavaProject().getProject();

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_50 + project);
        }

        if ((project != null) && project.isAccessible()) {
            monitor.setTaskName(BuilderMessages.PreverificationBuilder_51 + project.getName());

            // Figure the resource delta to be used
            buildInfo.setCurrentResourceDelta(getDelta(project));
            // Hand off to the build helper for the heavy lifting
            ResourceDeltaBuilder deltaBuilder = new ResourceDeltaBuilder(buildInfo);
            deltaBuilder.build(monitor);
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_52 + project);
        }
    }

    /**
     * Set the resources in the output directory to be derived so they are left
     * alone by the team support.
     * 
     * @param verifiedFolder
     * @param monitor
     * @throws CoreException
     */
    private void setResourcesAsDerived(IFolder verifiedFolder, IProgressMonitor monitor) throws CoreException {
        // Refresh from the folder down so that we can
        // set derived on these...
        verifiedFolder.refreshLocal(IResource.DEPTH_INFINITE, monitor);

        // Get the starting folder
        Utils.setResourcesAsDerived(verifiedFolder);
    }

    private boolean compareClasspath(IClasspathEntry[] cp1, IClasspathEntry[] cp2) {
        if (cp1 == null) {
            return cp2 == null;
        } else if (cp2 == null) {
            return false;
        } else if (cp1.length != cp2.length) {
            return false;
        }

        for (int i = 0; i < cp1.length; i++) {
            if (!cp1[i].equals(cp2[i])) {
                return false;
            }
        }

        return true;
    }

    private boolean arePreverifiedClassesConsistent(IProject project) {
        IFolder tempFolder = project.getFolder(IMTJCoreConstants.TEMP_FOLDER_NAME);
        IResourceDelta delta = getDelta(project);
        
        if (delta != null) {
            IResourceDelta tempFolderDelta = delta.findMember(tempFolder.getProjectRelativePath());
    
            if (tempFolderDelta != null) {
                int kind = tempFolderDelta.getKind();
    
                if ((kind & IResourceDelta.REMOVED) != 0) {
                    return false;
                } else if ((kind & IResourceDelta.CHANGED) != 0) {
                    if (delta.findMember(tempFolder.getFolder(IMTJCoreConstants.VERIFIED_FOLDER_NAME)
                            .getProjectRelativePath()) != null) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#doBuild
     * (int, java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
     */
    @SuppressWarnings("rawtypes")
    protected IProject[] doBuild(int kind, Map args, IProgressMonitor monitor) throws CoreException {
        IProject project = getProject();
        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_61 + project);
        }

        IJavaProject rootJavaProject = JavaCore.create(project);

        List<IJavaProject> noLibletProjects = new ArrayList<IJavaProject>();

        for (IJavaProject requiredProject : getRequiredProjects(rootJavaProject, monitor)) {
            IMidletSuiteProject midletSuite = MidletSuiteFactory.getMidletSuiteProject(requiredProject);

            if (midletSuite.getProjectPackagingModel() != PackagingModel.LIBLET) {
                noLibletProjects.add(requiredProject);
            }
        }

        IJavaProject[] requiredProjects = noLibletProjects.toArray(new IJavaProject[0]);

        // TODO Fix monitoring
        monitor.subTask(BuilderMessages.PreverificationBuilder_62);

        // force full build on classpath changes
        boolean forceFullBuild = false;

        Map<IProject, IClasspathEntry[]> classpaths = lastProjectsClassPath.get(project);

        if (classpaths == null) {
            classpaths = new HashMap<IProject, IClasspathEntry[]>();
            lastProjectsClassPath.put(project, classpaths);
        }

        HashSet<IProject> removedRequiredProjects = new HashSet<IProject>(classpaths.keySet());

        removedRequiredProjects.remove(project);

        if (classpaths.size() != requiredProjects.length + 1) {
            forceFullBuild = true;
        }

        IClasspathEntry[] oldRootCP = classpaths.get(project);
        IClasspathEntry[] newRootCP = rootJavaProject.getResolvedClasspath(true);

        if (!forceFullBuild && (!compareClasspath(oldRootCP, newRootCP) || !arePreverifiedClassesConsistent(project))) {
            forceFullBuild = true;
        }

        classpaths.put(project, newRootCP);

        for (IJavaProject requiredProject : requiredProjects) {
            removedRequiredProjects.remove(requiredProject.getProject());
            IClasspathEntry[] oldCP = classpaths.get(requiredProject.getProject());
            IClasspathEntry[] newCP = requiredProject.getResolvedClasspath(true);

            if (!forceFullBuild
                    && (!compareClasspath(oldCP, newCP) || !arePreverifiedClassesConsistent(requiredProject.getProject()))) {
                forceFullBuild = true;
            }

            classpaths.put(requiredProject.getProject(), newCP);
        }

        if (forceFullBuild) {
            doClean(FULL_BUILD, monitor);

            for (IProject removedRequiredProject : removedRequiredProjects) {
                if (removedRequiredProject.exists()) {
                    clearPreverifierMarkers(project, removedRequiredProject);
                }
            }
        }

        clearNoPreverifierFoundMarker();

        BuildInfo buildInfo = new BuildInfo(forceFullBuild ? FULL_BUILD : kind, args, rootJavaProject);

        // Start with our project
        try {
            preverifyProject(buildInfo, monitor);

            // Now prereq projects
            for (IJavaProject requiredProject : requiredProjects) {
                buildInfo.setCurrentJavaProject(requiredProject);
                preverifyProject(buildInfo, monitor);
            }

            // Make sure all of the libraries in the project have been
            // preverified
            if (buildInfo.areLibrariesPreverified()) {
                buildInfo.setCurrentJavaProject(rootJavaProject);
                preverifyLibraries(buildInfo, monitor);

                for (IJavaProject requiredProject : requiredProjects) {
                    buildInfo.setCurrentJavaProject(requiredProject);
                    preverifyLibraries(buildInfo, monitor);
                }
            }

            // Convert to IProject instances
            IProject[] interestingProjects = new IProject[requiredProjects.length];
            for (int i = 0; i < requiredProjects.length; i++) {
                interestingProjects[i] = requiredProjects[i].getProject();
            }
            // Refresh...
            if (buildInfo.isPackageDirty()) {
                // Set all of the resources in the verified path
                // to be derived resources
                setResourcesAsDerived(buildInfo.getVerifiedClassesFolder(monitor), monitor);
                setResourcesAsDerived(buildInfo.getVerifiedLibsFolder(monitor), monitor);
            }
            if (buildLoggingConfig.isPreverifierTraceEnabled()) {
                consoleProxy.traceln(BuilderMessages.PreverificationBuilder_63 + project);
            }
            monitor.done();
            return interestingProjects;
        } catch (PreverifierNotFoundException e) {
            createNoPreverifierFoundMarker();
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#doClean
     * (int, org.eclipse.core.runtime.IProgressMonitor)
     */
    protected void doClean(int kind, IProgressMonitor monitor) throws CoreException {
        IProject project = getProject();

        clearNoPreverifierFoundMarker();
        clearPreverifierMarkers(project, project);

        IJavaProject[] requiredProjects = getRequiredProjects(JavaCore.create(project), monitor);

        for (IJavaProject requiredProject : requiredProjects) {
            if (requiredProject != null) {
                clearPreverifierMarkers(project, requiredProject.getProject());
            }
        }

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_64 + project);
        }
        cleanProject(project, false, monitor);

        if (buildLoggingConfig.isPreverifierTraceEnabled()) {
            consoleProxy.traceln(BuilderMessages.PreverificationBuilder_65 + project);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#getBuilderId
     * ()
     */
    protected String getBuilderId() {
        return IMTJCoreConstants.J2ME_PREVERIFIER_ID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#onStateExit
     * (org.eclipse.mtj.internal.core.build.BuildStateMachine,
     * org.eclipse.core.runtime.IProgressMonitor)
     */
    protected void onStateExit(BuildStateMachine stateMachine, IProgressMonitor monitor) throws CoreException {
        super.onStateExit(stateMachine, monitor);
        BuildSpecManipulator manipulator = new BuildSpecManipulator(getMTJProject().getProject());
        if (!manipulator.hasBuilder(IMTJCoreConstants.PACKAGE_BUILDER_ID)) {
            manipulator.addBuilderAfter(IMTJCoreConstants.J2ME_PREVERIFIER_ID, IMTJCoreConstants.PACKAGE_BUILDER_ID,
                    null);
            manipulator.commitChanges(monitor);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @seeorg.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#
     * getEnterState()
     */
    protected MTJBuildState getEnterState() {
        return MTJBuildState.PRE_PREVERIFICATION;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#getExitState
     * ()
     */
    protected MTJBuildState getExitState() {
        return MTJBuildState.POST_PREVERIFICATION;
    }
}
