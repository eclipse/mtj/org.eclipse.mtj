/**
 * Copyright (c) 2009 Motorola and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Fixing antenna export bugs.
 *     Eric S. Dias  (Motorola) - Adding support to generate proper property files.
 *     Jon Dearden (Research In Motion) - Replaced deprecated use of Preferences
 *                                        [Bug 285699]
 *     Igor Gatis (Google) - Exported Antenna build files ignore project's CLDC
 *     										and MIDP versions [305768]
 *     David Aragao(Motorola) - Add support to create the source folder file filter
 *     							Task based on build.properties file.
 */
package org.eclipse.mtj.internal.core.build.export;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.midp.IApplicationDescriptor;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.PreferenceAccessor;
import org.eclipse.mtj.internal.core.build.export.states.AbstractCreateAntTaskState;
import org.eclipse.mtj.internal.core.build.export.states.CreateAntBuildFilesFilterState;
import org.eclipse.mtj.internal.core.build.export.states.CreateAntBuildTaskState;
import org.eclipse.mtj.internal.core.build.export.states.CreateAntCleanTaskState;
import org.eclipse.mtj.internal.core.build.export.states.CreateAntGeneratePropertiesState;
import org.eclipse.mtj.internal.core.build.export.states.CreateAntInitTaskState;
import org.eclipse.mtj.internal.core.build.export.states.CreateAntPackageTaskState;
import org.eclipse.mtj.internal.core.build.export.states.CreateAntPreprocessTaskState;
import org.eclipse.mtj.internal.core.build.export.states.CreateAntTaskStateTransition;
import org.eclipse.mtj.internal.core.statemachine.AbstractStateMachineEvent;
import org.eclipse.mtj.internal.core.statemachine.StateMachine;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * AntennaBuildExport class creates a build.xml file
 * for the project. The building process is based on a
 * set of states within a state machine where each state
 * builds one step of the building process when entered.
 *
 * @author David Marques
 * @since 1.0
 */
public class AntennaBuildExport {

    public static final String PROGUARD_HOME = "wtk.proguard.home"; //$NON-NLS-1$
    public static final String WTK_CLDC_VERSION = "wtk.cldc.version"; //$NON-NLS-1$
    public static final String WTK_MIDP_VERSION = "wtk.midp.version"; //$NON-NLS-1$
    public static final String ANTENNA_LIB = "antenna.lib"; //$NON-NLS-1$
    public static final String MTJ_BUILD_PROPERTIES = "mtj-build.properties"; //$NON-NLS-1$
    public static final String WTK_HOME = "wtk.home"; //$NON-NLS-1$
    public static final String DO_OBFUSCATE = "do-obfuscate"; //$NON-NLS-1$
    public static final String DO_AUTOVERSION = "do-autoversion"; //$NON-NLS-1$
    public static final String DEFAULT_BUILD_XML = "mtj-build.xml"; //$NON-NLS-1$
	public static final String DEFAULT_BUILD_FOLDER = "mtj-build"; //$NON-NLS-1$

    public static class CreateAntTaskDoneEvent extends AbstractStateMachineEvent {}

    private IMidletSuiteProject suiteProject;
    private StateMachine        stateMachine;
    private String buildFileName;
    private String buildFolderName;
    private boolean isLocalization;

    /**
     * Creates an instance of a {@link AntennaBuildExport} class
     * to create build.xml file to the specified {@link IMidletSuiteProject}
     * project instance.
     *
     * @param _suiteProject target project.
     */
    public AntennaBuildExport(IMidletSuiteProject _suiteProject, String file, String folder) {
        if (_suiteProject == null) {
            throw new IllegalArgumentException(Messages.NewAntennaBuildExport_InvalidSuiteProject);
        }

        this.suiteProject = _suiteProject;
        this.buildFileName = file;
        this.buildFolderName = folder;
    }

    /**
     * Exports the build.xml file for the specified project.
     *
     * @param _monitor activity progress monitor.
     * @throws AntennaExportException Any error occurs during the
     * 		   build.xml creation process.
     */
    public void doExport(IProgressMonitor monitor) throws AntennaExportException {
        this.stateMachine = new StateMachine();
        Document document = this.createNewDocument();
        if (document == null) {
            throw new AntennaExportException(Messages.NewAntennaBuildExport_UnableToCreateXMLDoc);
        }

        isLocalization = false;
        try {
            isLocalization = this.suiteProject.getProject().hasNature(IMTJCoreConstants.L10N_NATURE_ID);
        } catch (CoreException e) {
        }

        this.createAntExportStates(this.stateMachine, document);
        
        if (monitor.isCanceled())
        	return;
        
        try {
            IProject project = this.suiteProject.getProject();
            IFolder build = project.getFolder(buildFolderName);
            if (!build.exists()) {
                build.create(true, true, monitor);
            }

            //Creating the mtj-build.task directory
            IFolder buildTask = project.getFolder(buildFolderName+"/custom-tasks"); //$NON-NLS-1$
            if (!buildTask.exists()) {
                buildTask.create(true, true, monitor);
            }

            if (isLocalization) {
                InputStream stream = MTJCore.getResourceAsStream(new Path(
                "templates/GeneratingProperties.class.template")); //$NON-NLS-1$

                //Creating the GeneratingProperties.class file
                IFile fileTask = buildTask.getFile("GeneratingProperties.class"); //$NON-NLS-1$
                if (!fileTask.exists()) {
                    fileTask.create(stream, true, monitor);
                }
            }
            
            InputStream stream = MTJCore.getResourceAsStream(new Path(
            "templates/BuildFilesFilter.class.template")); //$NON-NLS-1$

            //Creating the buildFilesFilter.class file
            IFile fileTask = buildTask.getFile("BuildFilesFilter.class"); //$NON-NLS-1$
            if (!fileTask.exists()) {
                fileTask.create(stream, true, new NullProgressMonitor());
            }

            this.stateMachine.start();
            IFile file = build.getFile(buildFileName);
            XMLUtils.writeDocument(file.getLocation().toFile(), document);

            this.writeBuildProperties(build.getFile(MTJ_BUILD_PROPERTIES));
            build.refreshLocal(IResource.DEPTH_INFINITE, monitor);
        } catch (Exception e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }

    private static String extractVersion(String str) {
        String[] split = str.split("-"); //$NON-NLS-1$
        if (split.length == 2) {
        	str = split[1];
        }
        return str;
    }

    /**
     * Writes the properties file for the build configuration.
     *
     * @param file target file.
     * @throws IOException Any IO error occurs.
     */
    private void writeBuildProperties(IFile file) throws IOException {
    	IApplicationDescriptor jad = this.suiteProject.getApplicationDescriptor();

        PreferenceAccessor pref = PreferenceAccessor.instance;
        Properties properties = new Properties();
        properties.put(DO_AUTOVERSION, "false"); //$NON-NLS-1$
        properties.put(DO_OBFUSCATE, "false"); //$NON-NLS-1$
        properties.put(ANTENNA_LIB, pref.getString(IMTJCoreConstants.PREF_ANTENNA_JAR));
        properties.put(WTK_HOME, pref.getString(IMTJCoreConstants.PREF_WTK_ROOT));
        properties.put(WTK_CLDC_VERSION, extractVersion(jad.getMicroEditionConfiguration()));
        properties.put(WTK_MIDP_VERSION, extractVersion(jad.getMicroEditionProfile()));

        String proguardDir = pref.getString(IMTJCoreConstants.PREF_PROGUARD_DIR);
        if (proguardDir != null && proguardDir.length() > 0x00) {
            properties.put(PROGUARD_HOME, proguardDir);
        }
        FileOutputStream out=null;
        try{
        	 out= new FileOutputStream(file.getLocation().toFile());
        	properties.store(out, Messages.NewAntennaBuildExport_comment);
        }finally{
        	if (out!=null)
        		out.close();
        }
        
        
    }

    /**
     * Creates the states and add it's transitions.
     *
     * @param _stateMachine target state machine.
     * @param _document target {@link Document} instance.
     */
    private void createAntExportStates(StateMachine _stateMachine,
            Document _document) {
        AbstractCreateAntTaskState state1  = null;
        AbstractCreateAntTaskState state2  = null;
        AbstractCreateAntTaskState state3  = null;

        Element root = createProjectElement(_document, "build-all"); //$NON-NLS-1$
        setupDefaultTarget(_document, root);
        setupAntenna(_document, root);

        if (isLocalization)
            setupGeneratingProperties(_document, root);
        
        setupBuildFilesFilter(_document, root);

        state1 = new CreateAntCleanTaskState(this.stateMachine, this.suiteProject, _document, buildFolderName, buildFileName);
        this.stateMachine.addState(state1);
        this.stateMachine.setInitialState(state1);

        state2 = new CreateAntInitTaskState(this.stateMachine, this.suiteProject, _document, buildFolderName, buildFileName);
        state2.setDependencies("clean"); //$NON-NLS-1$
        this.stateMachine.addState(state2);
        state1.addTransition(new CreateAntTaskStateTransition(state1, state2));

        boolean isPreprocessing = false;
        try {
            isPreprocessing = this.suiteProject.getProject().hasNature(IMTJCoreConstants.J2ME_PREPROCESSING_NATURE_ID);
        } catch (CoreException e) {
        }

        if (isLocalization) {
            state3 = new CreateAntGeneratePropertiesState(this.stateMachine, this.suiteProject, _document, buildFolderName, buildFileName);
            state3.setDependencies("init"); //$NON-NLS-1$
            this.stateMachine.addState(state3);
            state2.addTransition(new CreateAntTaskStateTransition(state2, state3));

            if (isPreprocessing) {
            	state1 = new CreateAntPreprocessTaskState(this.stateMachine, this.suiteProject, _document, buildFolderName, buildFileName);
            } else {
            	state1 = new CreateAntBuildFilesFilterState(this.stateMachine, this.suiteProject, _document, buildFolderName, buildFileName);
            }

            state1.setDependencies("generate-resources"); //$NON-NLS-1$
            state3.addTransition(new CreateAntTaskStateTransition(state3, state1));
        }else{
            if (isPreprocessing) {
            	state1 = new CreateAntPreprocessTaskState(this.stateMachine, this.suiteProject, _document, buildFolderName, buildFileName);
            } else {
            	state1 = new CreateAntBuildFilesFilterState(this.stateMachine, this.suiteProject, _document, buildFolderName, buildFileName);
            }

            state1.setDependencies("init"); //$NON-NLS-1$
            state2.addTransition(new CreateAntTaskStateTransition(state2, state1));
        }
        state2 = new CreateAntBuildTaskState(this.stateMachine, this.suiteProject, _document, buildFolderName, buildFileName);
        if (isPreprocessing) {
        	state2.setDependencies("preprocess"); //$NON-NLS-1$
        } else {
        	state2.setDependencies("buildFilesFilter"); //$NON-NLS-1$
        }

        state1.addTransition(new CreateAntTaskStateTransition(state1, state2));
        
        state1 = new CreateAntPackageTaskState(this.stateMachine, this.suiteProject, _document, buildFolderName, buildFileName);
        state1.setDependencies("build"); //$NON-NLS-1$
        state2.addTransition(new CreateAntTaskStateTransition(state2, state1));
    }

    /**
     * Sets up the default build target.
     *
     * @param _document target {@link Document} instance.
     * @param root root document node.
     */
    private void setupDefaultTarget(Document _document, Element root) {
        Element buildAll = XMLUtils.createTargetElement(_document, root, "build-all", "clean-all"); //$NON-NLS-1$ //$NON-NLS-2$

        MTJRuntimeList runtimeList = this.suiteProject.getRuntimeList();
        for (MTJRuntime runtime : runtimeList) {
            String configName = runtime.getName().replace(" ",  "_"); //$NON-NLS-1$ //$NON-NLS-2$
            Element antCall = _document.createElement("antcall"); //$NON-NLS-1$
            antCall.setAttribute("target", NLS.bind("package-{0}", configName)); //$NON-NLS-1$ //$NON-NLS-2$
            buildAll.appendChild(antCall);
        }
    }

    /**
     * Sets up antenna properties on the xml file.
     *
     * @param _document target {@link Document} instance.
     * @param root document root node.
     */
    private void setupAntenna(Document _document, Element root) {
        Element taskdef = _document.createElement("taskdef"); //$NON-NLS-1$
        taskdef.setAttribute("resource", "antenna.properties"); //$NON-NLS-1$ //$NON-NLS-2$
        taskdef.setAttribute("classpath", NLS.bind("${0}{1}{2}", new String[] {"{", ANTENNA_LIB, "}"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
        root.appendChild(taskdef);

        //<property file="mtj-build.properties" />
        Element property = _document.createElement("property"); //$NON-NLS-1$
        property.setAttribute("file", NLS.bind("{0}/{1}" //$NON-NLS-1$ //$NON-NLS-2$
                , new String[] {buildFolderName, MTJ_BUILD_PROPERTIES}));
        root.appendChild(property);
    }

    /**
     * Sets up task properties on the build antenna xml file.
     *
     * @param _document target {@link Document} instance.
     * @param root document root node.
     */
    private void setupGeneratingProperties(Document _document, Element root) {
        Element taskdef = _document.createElement("taskdef"); //$NON-NLS-1$
        taskdef.setAttribute("classpath", buildFolderName+"/custom-tasks"); //$NON-NLS-1$ //$NON-NLS-2$
        taskdef.setAttribute("classname", "GeneratingProperties"); //$NON-NLS-1$ //$NON-NLS-2$
        taskdef.setAttribute("name", "generatingProperties"); //$NON-NLS-1$ //$NON-NLS-2$
        root.appendChild(taskdef);
    }
    
    /**
     * Sets up task properties on the build antenna xml file.
     * 
     * @param _document target {@link Document} instance.
     * @param root document root node.
     */
    private void setupBuildFilesFilter(Document _document, Element root) {
        Element taskdef = _document.createElement("taskdef"); //$NON-NLS-1$
        taskdef.setAttribute("classpath", buildFolderName+"/custom-tasks"); //$NON-NLS-1$ //$NON-NLS-2$
        taskdef.setAttribute("classname", "BuildFilesFilter"); //$NON-NLS-1$ //$NON-NLS-2$
        taskdef.setAttribute("name", "buildFilesFilter"); //$NON-NLS-1$ //$NON-NLS-2$
        root.appendChild(taskdef);
    }

    /**
     * Creates the project root element node.
     *
     * @param _document target {@link Document} instance.
     * @param _target target project name.
     * @return the root xml node.
     */
    private Element createProjectElement(Document _document, String _target) {
        Element root = _document.createElement("project"); //$NON-NLS-1$
        _document.appendChild(root);

        root.setAttribute("name", this.suiteProject.getProject().getName()); //$NON-NLS-1$
        root.setAttribute("default", _target); //$NON-NLS-1$
        root.setAttribute("basedir", ".."); //$NON-NLS-1$ //$NON-NLS-2$
        return root;
    }

    /**
     * Creates a new {@link Document} instance.
     *
     * @return an empty document instance.
     */
    private Document createNewDocument() {
        Document result = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder        builder = factory.newDocumentBuilder();
            result = builder.newDocument();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return result;
    }

}
