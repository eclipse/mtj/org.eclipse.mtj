/**
 * Copyright (c) 2009 Research In Motion Limited.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial Version
 */
package org.eclipse.mtj.internal.core.sdk;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IManagedDevice;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.persistence.PersistableUtilities;
import org.eclipse.mtj.internal.core.sdk.device.DeviceRegistry;

/**
 * Wrapper class to make an ISDKProvider for legacy imported devices obtained
 * from the DeviceRegistry. Used by the DeviceManagementPreferencePage to
 * display ISDKProviders, ISDKs, and IDevices in a consistent manner.
 * DeviceManagementPreferencePage treats imported devices simply as devices
 * provided by the "Manually Installed SDKs" ISDKProvider. It has no knowledge
 * of the DeviceRegistry.
 * 
 * @Since 1.1
 */
public class ImportedSDKProvider extends AbstractSDKProvider {
    
    private static ImportedSDKProvider instance = null;

    /**
     * @return the instance
     */
    public synchronized static ImportedSDKProvider getInstance() {
        if (instance == null) {
            instance = new ImportedSDKProvider();
        }
        return instance;
    }
    
    private ImportedSDKProvider() {
        name = Messages.SdkProvider_ImportedSdkProviderName;
    }

    // << ISDKProvider >>
    public String getIdentifier() {
        return "MtjImportedSdkProviderId"; //$NON-NLS-1$
    }

    // << ISDKProvider >>
    public int getSDKCount() {
        return getSDKs().size();
    }

    // << ISDKProvider >>
    public List<ISDK> getSDKs() {
        List<ISDK> importedSdks = new ArrayList<ISDK>();
        List<IDevice> allDevices;
        try {
            allDevices = DeviceRegistry.getInstance().getAllDevices();
        } catch (PersistenceException e) {
            e.printStackTrace();
            return importedSdks;
        }
        for (IDevice device : allDevices) {
            if (!(device instanceof IManagedDevice)) {
                ISDK sdk = device.getSDK();
                if (sdk != null && !importedSdks.contains(sdk)) {
                    importedSdks.add(sdk);
                }
            }
        }
        return importedSdks;
    }

    /**
     * Called by DeviceManagementPreferencePage to remove a device. 
     * The device is removed from the legacy DeviceRegistry.
     */
    public boolean removeDevice(IDevice device) {
        try {
            MTJCore.getDeviceRegistry().removeDevice(device);
        } catch (PersistenceException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Called by DeviceManagementPreferencePage to duplicate a device. 
     * The device is duplicated in the legacy DeviceRegistry.
     */
    public IDevice duplicateDevice(IDevice device, String newName) {
        IDevice clonedDevice = null;
        try {
            clonedDevice = (IDevice) PersistableUtilities
            .clonePersistable(device);
        } catch (PersistenceException e) {
            e.printStackTrace();
            return null;
        }
        clonedDevice.setName(newName);
        MTJCore.getDeviceRegistry().enableDeviceAddedEvent(true);
        try {
            MTJCore.getDeviceRegistry().addDevice(clonedDevice);
            return clonedDevice;
        } catch (IllegalArgumentException e) {
            MTJCore.getDeviceRegistry().enableDeviceAddedEvent(false);
            e.printStackTrace();
        } catch (PersistenceException e) {
            MTJCore.getDeviceRegistry().enableDeviceAddedEvent(false);
            e.printStackTrace();
        }
        return null;
    }
    
}
