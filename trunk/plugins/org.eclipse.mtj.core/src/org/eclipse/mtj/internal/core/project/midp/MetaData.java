/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)  - Initial implementation
 *     Kevin Hunter (Individual) - Added signing support
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 *     Diego Sandin (Motorola) &
 *     Hugo Raniere (Motorola)   - Remove Bouncy Castle dependencies
 *     Hugo Raniere (Motorola)   - Save user passwords in project's metadata file (w/o Bouncy Castle)
 *     Feng Wang (Sybase) - 1. Add multi-configs support, keeping compatible with
 *                             old version of meta data file.
 *                          2. Make save Meta data operation do not erase other elements
 *                             (elements other than "device", "signing", "configurations")
 *                             in meta data file. This is for backward compatibility.
 *     David Marques (Motorola) - Updating methods for loading/saving signature properties
 *                                to support signing enhancements.
 *     David Aragao (Motorola) - Problem when try to import a project without 
 *     				 copy the files to the workspace. [Bug - 270157]
 *     Daniel Olsson (Sony Ericsson) - Adding support for matching non-installed devices [Bug - 284262]
 *     Jon Dearden (Research In Motion) - Added new APIs for supporting SDK provider meta data 
 *                                        [Bug 286675].
 */
package org.eclipse.mtj.internal.core.project.midp;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.equinox.security.storage.ISecurePreferences;
import org.eclipse.equinox.security.storage.SecurePreferencesFactory;
import org.eclipse.equinox.security.storage.StorageException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.midp.IMIDPMetaData;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.MTJCoreErrors;
import org.eclipse.mtj.internal.core.build.preverifier.builder.PreverificationBuilder;
import org.eclipse.mtj.internal.core.build.sign.Base64EncDec;
import org.eclipse.mtj.internal.core.build.sign.SignatureProperties;
import org.eclipse.mtj.internal.core.project.runtime.MTJRuntimeListUtils;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.osgi.framework.Version;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This class holds the metadata for the MIDlet suite project. This information
 * is persisted to a file called ".mtj" in the project's root directory.
 */
public class MetaData implements IMIDPMetaData, IMetaDataConstants {

    private MTJRuntimeList configurations;

    private IDevice device;

    /**
     * The name that will be used for the deployed MIDlet Suite jad file
     */
    private String jadFileName;

    private IProject project;

    private Element rootXmlElement;

    private SignatureProperties signatureProps;

    private LibletProperties[] libletProperties; 
    
    private Version version;
    
    /**
     * Optional collection of arbitrary meta data for the use of an SDK provider.
     */
    private Hashtable<String, Properties> sdkProviderProperties; // Uses lazy initialization

    /**
     * Construct a new metadata object for the MIDlet suite project.
     * 
     * @param suite
     */
    public MetaData(IMidletSuiteProject suite) {
        this(suite.getJavaProject().getProject());
    }

    /**
     * Construct a new metadata object for the MIDlet suite project.
     * 
     * @param suite
     */
    public MetaData(IProject project) {
        this.project = project;

        try {
            loadMetaData();
        } catch (CoreException e) {
            // Failure to load the metadata, log and initialize to defaults
            MTJLogger.log(IStatus.ERROR, "loadMetaData() failed", e); //$NON-NLS-1$
            initializeToDefaults();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getDevice()
     */
    public IDevice getDevice() {
        return device;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IMIDPMetaData#getJadFileName()
     */
    public String getJadFileName() {
        return jadFileName;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getConfigurations()
     */
    public MTJRuntimeList getRuntimeList() {
        if (configurations == null) {
            configurations = new MTJRuntimeList();
        }
        return configurations;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getSignatureProperties()
     */
    public ISignatureProperties getSignatureProperties() {
        return signatureProps;
    }
    
    public LibletProperties[] getLibletProperties() {
    	return libletProperties;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getVersion()
     */
    public Version getVersion() {
        return version;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#saveMetaData()
     */
    public void saveMetaData() throws CoreException {

        IFile storeFile = getStoreFile();
        if (storeFile == null) {
            MTJLogger.log(IStatus.WARNING,
                    "saveMetaData failed due to null store file"); //$NON-NLS-1$
        } else {
            if (storeFile.exists() && storeFile.isReadOnly()) {
                // Attempt to clear the read-only flag via the IResource
                // interface. This should invoke the team provider and
                // do necessary checkouts.
                ResourceAttributes attributes = storeFile
                        .getResourceAttributes();
                attributes.setReadOnly(false);
                storeFile.setResourceAttributes(attributes);
            }

            saveMetaDataToFile(storeFile);
        }
    }
    
    /* (non-Javadoc)
     * @see
     * org.eclipse.mtj.core.project.IMTJProject#setSDKProviderMetaData(java.lang.String, java.utils.Properties)
     */
    public void setSDKProviderMetaData(String sdkId, final Properties metaData)
            throws IllegalArgumentException {
        if (sdkId == null || sdkId.length() == 0) {
            throw new IllegalArgumentException("The sdkId must not be null or empty."); //$NON-NLS-1$
        }
        if (sdkProviderProperties == null) {
            // Lazy initialization
            sdkProviderProperties = new Hashtable<String, Properties>();
        } else {
            // Erase any prior properties for this SDK
            sdkProviderProperties.remove(sdkId);
        }
        // Supplying a null metaData argument leaves the meta data erased for this SDK
        if (metaData != null) {
            sdkProviderProperties.put(sdkId, metaData);
        }
        if (sdkProviderProperties.isEmpty()) {
            sdkProviderProperties = null;
        }
    }

    /* (non-Javadoc)
     * @see
     * org.eclipse.mtj.core.project.IMTJProject#getSDKProviderMetaData(java.lang.String)
     */
    public Properties getSDKProviderMetaData(String sdkId)
            throws IllegalArgumentException {
        if (sdkId == null || sdkId.length() == 0) {
            throw new IllegalArgumentException("The sdkId must not be null or empty."); //$NON-NLS-1$
        }
        if (sdkProviderProperties == null) {
            return null;
        }
        return sdkProviderProperties.get(sdkId);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.midp.IMIDPMetaData#setJadFileName(java.lang.String)
     */
    public void setJadFileName(String jadFileName) {
        this.jadFileName = jadFileName.replace(' ', '_');
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#setConfigurations(org.eclipse.mtj.internal.core.project.configuration.Configurations)
     */
    public void setMTJRuntimeList(MTJRuntimeList configurations)
            throws IllegalArgumentException {
        if (configurations != null) {
            this.configurations = configurations;
        } else {
            throw new IllegalArgumentException(
                    "The runtime list must not be null.");
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#setSignatureProperties(org.eclipse.mtj.core.build.sign.ISignatureProperties)
     */
    public void setSignatureProperties(ISignatureProperties p) {
        if (signatureProps == null) {
            signatureProps = new SignatureProperties();
        }
        signatureProps.copy(p);
    }
    
    public void setLibletProperties(LibletProperties[] properties) {
    	libletProperties = properties;
    }

    /**
     * @param opmode
     * @return
     * @throws GeneralSecurityException
     */
    private Cipher createCipher(int opmode) throws GeneralSecurityException {
        PBEKeySpec keySpec = new PBEKeySpec(CRYPTO_PASS.toCharArray());
        SecretKeyFactory keyFactory = SecretKeyFactory
                .getInstance(IMetaDataConstants.CRYPTO_ALGORITHM);
        SecretKey secretKey = keyFactory.generateSecret(keySpec);

        AlgorithmParameterSpec paramSpec = new PBEParameterSpec(CRYPTO_SALT,
                CRYPTO_ITERATION_COUNT);
        Cipher cipher = Cipher.getInstance(secretKey.getAlgorithm());
        cipher.init(opmode, secretKey, paramSpec);
        return cipher;
    }

    /**
     * @param base
     * @param subElementName
     * @return
     */
    private String decodePassword(Element base, String subElementName) {
        Element subElement = XMLUtils.getFirstElementWithTagName(base,
                subElementName);
        if (subElement == null) {
            return (null);
        }

        String encoded = XMLUtils.getElementText(subElement);
        byte[] cryptBytes = Base64EncDec.decode(encoded);
        if (cryptBytes == null) {
            return (null);
        }

        try {
            byte[] passwordBytes = null;
            Cipher cipher = createCipher(Cipher.DECRYPT_MODE);
            passwordBytes = cipher.doFinal(cryptBytes);

            return (new String(passwordBytes, "UTF8")); //$NON-NLS-1$
        } catch (UnsupportedEncodingException e) {
        } catch (GeneralSecurityException e) {
        }

        return (null);
    }

    /**
     * @param password
     * @return
     */
    private String encodePassword(String password) {
        if (password == null) {
            return (""); //$NON-NLS-1$
        }

        try {
            byte[] passwordBytes = password.getBytes("UTF8"); //$NON-NLS-1$
            byte[] cryptBytes = null;
            Cipher cipher = createCipher(Cipher.ENCRYPT_MODE);
            cryptBytes = cipher.doFinal(passwordBytes);

            return (Base64EncDec.encode(cryptBytes));
        } catch (UnsupportedEncodingException e) {
        } catch (GeneralSecurityException e) {
        }
        return (""); //$NON-NLS-1$
    }

    /**
     * Return the IFile instance in which the metadata must be saved.
     * <p>
     * In the case of the file is not yet available, this method will return
     * <code>null</code>.
     * 
     * @return the Metadata IFile instance or <code>null</code> if this file in
     *         not available.
     */
    private IFile getStoreFile() {
        IFile storeFile = null;

        if (project != null) {
            storeFile = project.getFile(METADATA_FILE);
        }

        return storeFile;
    }

    /**
     * Get the IFile instance in which the metadata is to be stored.
     * 
     * @return
     */
    private IFile getStoreIFile() {
        return project.getFile(METADATA_FILE);
    }

    /**
     * Initialize the metadata to default values
     */
    private void initializeToDefaults() {
        signatureProps = new SignatureProperties();
        configurations = new MTJRuntimeList();
        libletProperties = new LibletProperties[0];
    }

    /**
     * Besides loading configurations from meta data file, this method also
     * handle compatible issue. It's means that it can load configurations from
     * old version of meta data file, by creating a configuration from the
     * device information.
     * 
     * @param rootElement
     * @return <b>true</b> if the loaded configuration was altered to match the running environment, <b>false</b> otherwise
     * @throws PersistenceException
     */
    private boolean loadConfigsCompatibly(Element rootElement)
            throws PersistenceException {
        // load device if there any. New version meta data file (0.9.1) have no
        // top level <device> element
        boolean changedDevice = loadDevice(rootElement);
        Element configsElement = XMLUtils.getFirstElementWithTagName(
                rootElement, MTJRuntimeList.ELEM_CONFIGURATIONS);
        if (configsElement != null) {
            configurations = new MTJRuntimeList(configsElement);
        } else {
            // For old version meta data file(before MTJ 0.9.1), there is no
            // <configurations> element, so we create one. After
            // Metadata#savemetada() performed, this configurations will
            // be persisted in meta data file.
            configurations = new MTJRuntimeList();
        }
        // (configurations.isEmpty() && device != null) means that the meta data
        // file is of old version. So we will create a configuration from the
        // device.
        if (configurations.isEmpty() && (device != null)) {
            setDeviceIntoActiveConfig(device);
        }
        boolean configsGotChanged = false;
        for (MTJRuntime config : configurations) {
            configsGotChanged = config.isMatched() ? true : configsGotChanged;
        }
        return changedDevice || configsGotChanged;
    }

    /**
     * Load Configuration from meta data file.
     * 
     * @param rootElement
     * @throws PersistenceException
     */
    private boolean loadConfigurations(Element rootElement)
            throws PersistenceException {
        // can load form both old version and new version meta data file.
        boolean changedConfig = loadConfigsCompatibly(rootElement);
        // set device
        MTJRuntime activeConfig = configurations.getActiveMTJRuntime();
        if (activeConfig != null) {
            device = configurations.getActiveMTJRuntime().getDevice();
        }
        return changedConfig;
    }

    /**
     * @param rootElement
     * @return false if the given device was loaded, true otherwise
     * @throws PersistenceException
     */
    private boolean loadDevice(Element rootElement) throws PersistenceException {
        boolean loadedDifferentDevice = false;
        Element deviceElement = XMLUtils.getFirstElementWithTagName(
                rootElement, ELEM_DEVICE);
        if (deviceElement != null) {
            String deviceGroup = deviceElement.getAttribute(ATTR_DEVICEGROUP);
            String deviceName = deviceElement.getAttribute(ATTR_DEVICENAME);
            device = MTJCore.getDeviceRegistry().getDevice(deviceGroup,
                    deviceName);
            if (device == null) {
                device = MTJRuntimeListUtils.match(deviceGroup, deviceName);
                if (device != null) {
                    MTJLogger.log(IStatus.INFO, deviceGroup + "/" + deviceName
                            + " was converted to " + device.toString());
                    loadedDifferentDevice = true;
                }
            }
        }
        return loadedDifferentDevice;
    }

    /**
     * @throws CoreException
     */
    private void loadMetaData() throws CoreException {
        boolean bRewrite = loadMetaDataFromFile();

        if (signatureProps == null) {
            signatureProps = new SignatureProperties();
            signatureProps.clear();
            bRewrite = true;
        }

        if (bRewrite) {
            WorkspaceJob job = new WorkspaceJob("Rewrite Project Metadata") {
                public IStatus runInWorkspace(IProgressMonitor monitor)
                        throws CoreException {
                    final IJavaProject javaProject = JavaCore.create(project);
                    final IMidletSuiteProject midletProject = MidletSuiteFactory
                            .getMidletSuiteProject(javaProject);
                    midletProject.refreshClasspath(monitor);
                    saveMetaData();
                    PreverificationBuilder.cleanProject(midletProject
                            .getProject(), true, monitor);
                    midletProject.getProject().build(
                            IncrementalProjectBuilder.FULL_BUILD, monitor);
                    return Status.OK_STATUS;
                }
            };
            job.schedule();
        }
    }

    /**
     * @throws CoreException
     */
    private boolean loadMetaDataFromFile() throws CoreException {
        boolean alteredConfig = false;
        IFile storeFile = getStoreFile();
        if (storeFile == null) {
			initializeToDefaults();
		}
        
        IPath location = storeFile.getLocation();
        if (location != null && location.toFile().exists()) {
            try {
                File localFile = location.toFile();
                Document document = XMLUtils.readDocument(localFile);
                if (document == null) {
                    return alteredConfig;
                }

                rootXmlElement = document.getDocumentElement();
                if (!rootXmlElement.getNodeName().equals(ELEM_ROOT_NAME)) {
                    return alteredConfig;
                }

                version = XMLUtils.getVersion(document);

                /*
                 * Get the name that will be used for the deployed MIDlet Suite
                 * jad file
                 */
                jadFileName = rootXmlElement.getAttribute(ATTR_JAD_FILE);
                alteredConfig = loadConfigurations(rootXmlElement);
                loadSignatureProperties(rootXmlElement);
                loadSDKProviderProperties(rootXmlElement);
                loadLibletProperties(rootXmlElement);

            } catch (ParserConfigurationException pce) {
                MTJStatusHandler.throwCoreException(IStatus.WARNING, 99999, pce);
            } catch (SAXException se) {
                MTJStatusHandler.throwCoreException(IStatus.WARNING, 99999, se);
            } catch (IOException ioe) {
                MTJStatusHandler.throwCoreException(IStatus.WARNING, 99999, ioe);
            } catch (PersistenceException e) {
                MTJStatusHandler.throwCoreException(IStatus.WARNING, 99999, e);
            }
        } else {
            initializeToDefaults();
        }
        return alteredConfig;
    }

    /**
     * @param rootElement
     * @throws CoreException
     */
    private void loadSignatureProperties(Element rootElement)
            throws CoreException {
        SignatureProperties sp = new SignatureProperties();

        Element signRoot = XMLUtils.getFirstElementWithTagName(rootElement,
                ELEM_SIGNING);
        if (signRoot == null) {
            return;
        }

        String attr = signRoot.getAttribute(ATTR_PROJECT_SPECIFIC);
        if (attr == null) {
            return;
        }
        sp.setProjectSpecific(Boolean.valueOf(attr).booleanValue());

        attr = signRoot.getAttribute(ATTR_SIGN_PROJECT);
        if (attr == null) {
            return;
        }
        sp.setSignProject(Boolean.valueOf(attr).booleanValue());
        signatureProps = sp;

        Element e;

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_KEYSTORE);
        if (e != null) {
            sp.setKeyStoreDisplayPath(XMLUtils.getElementText(e));
        }

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_ALIAS);
        if (e != null) {
            sp.setKeyAlias(XMLUtils.getElementText(e));
        }

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_PROVIDER);
        if (e != null) {
            sp.setKeyStoreProvider(XMLUtils.getElementText(e));
        }

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_KEYSTORETYPE);
        if (e != null) {
            sp.setKeyStoreType(XMLUtils.getElementText(e));
        }

        e = XMLUtils.getFirstElementWithTagName(signRoot, ELEM_PASSWORDS);
        if (e != null) {
            attr = e.getAttribute(ATTR_STOREPASSWORDS);
            int nMethod = ISignatureProperties.PASSMETHOD_PROMPT;
            if (attr != null) {
                try {
                    nMethod = Integer.valueOf(attr).intValue();
                } catch (Exception ex) {
                }
            }

            switch (nMethod) {
                case ISignatureProperties.PASSMETHOD_IN_KEYRING:
                    sp.setPasswordStorageMethod(nMethod);

                    ISecurePreferences root = SecurePreferencesFactory
                            .getDefault();

                    String url = Utils.getKeyringURL(project);
                    if (root.nodeExists(url)) {
                        ISecurePreferences preferences = root.node(url);
                        try {
                            sp.setKeyStorePassword(preferences.get(
                                    KEYRING_KEYSTOREPASS_KEY, ""));
                            sp.setKeyPassword(preferences.get(
                                    KEYRING_KEYPASS_KEY, ""));
                        } catch (StorageException e1) {
                            signatureProps = sp;
                            MTJCoreErrors.throwCoreExceptionError(7676,
                                    "Could not re password into keyring,", e1);
                        }

                    }

                    break;
                case ISignatureProperties.PASSMETHOD_IN_PROJECT:
                    sp.setPasswordStorageMethod(nMethod);
                    sp
                            .setKeyStorePassword(decodePassword(e,
                                    ELEM_PWD_KEYSTORE));
                    sp.setKeyPassword(decodePassword(e, ELEM_PWD_KEY));
                    break;
                case ISignatureProperties.PASSMETHOD_PROMPT:
                default:
                    sp
                            .setPasswordStorageMethod(ISignatureProperties.PASSMETHOD_PROMPT);
                    sp.setKeyStorePassword(null);
                    sp.setKeyPassword(null);
                    break;
            }
        }
    }
    
    private void loadLibletProperties(Element rootElement)
            throws CoreException {

        Element libletsRoot = XMLUtils.getFirstElementWithTagName(rootElement,
                ELEM_LIBLETS);
        if (libletsRoot == null) {
            return;
        }

        NodeList libletNodes = libletsRoot.getElementsByTagName(ELEM_LIBLET);
        LibletProperties[] properties = new LibletProperties[libletNodes.getLength()];
        for (int i=0; i<libletNodes.getLength(); i++) {
        	Element libletElement = (Element) libletNodes.item(i);
        	String name = libletElement.getAttribute(ATTR_NAME);
        	String level = libletElement.getAttribute(ATTR_LEVEL);
        	String type = libletElement.getAttribute(ATTR_TYPE);
        	String jadURL = libletElement.getAttribute(ATTR_JAD_URL);
        	String vendor = libletElement.getAttribute(ATTR_VENDOR);
        	String version = libletElement.getAttribute(ATTR_VERSION);
        	String projectName = libletElement.getAttribute(ATTR_PROJECT_NAME);
        	
        	LibletProperties lp = new LibletProperties(name, 
        			LibletProperties.Level.getByJadName(level),
        			LibletProperties.Type.getByJadName(type),
        			jadURL, vendor, version, projectName);
        	
        	properties[i] = lp;
        }
        
        libletProperties = properties;
    }
    
    /**
     * @param rootElement
     * @throws CoreException
     */
    private void loadSDKProviderProperties(Element rootElement)
            throws PersistenceException {
        Element sdkProvidersPropertiesElement = XMLUtils
                .getFirstElementWithTagName(rootElement,
                        MTJRuntime.ELEM_SDK_PROVIDER_PROPERTIES);
        if (sdkProvidersPropertiesElement == null) {
            sdkProviderProperties = null;
            return;
        }
        NodeList sdkNodes = sdkProvidersPropertiesElement
                .getElementsByTagName(MTJRuntime.ELEM_SDK);
        // For each SDK ID...
        for (int s = 0; s < sdkNodes.getLength(); s++) {
            Element sdkElement = (Element) sdkNodes.item(s);
            String sdkId = sdkElement.getAttribute(MTJRuntime.ATTR_SDK_ID);
            NodeList propertyNodes = sdkElement
                    .getElementsByTagName(MTJRuntime.ELEM_SDK_PROPERTY);
            Properties properties = new Properties();
            // For each property...
            for (int p = 0; p < propertyNodes.getLength(); p++) {
                Element propertyElement = (Element) propertyNodes.item(p);
                String name = propertyElement
                        .getAttribute(MTJRuntime.ATTR_SDK_PROPERTY_NAME);
                String value = propertyElement
                        .getAttribute(MTJRuntime.ATTR_SDK_PROPERTY_VALUE);
                properties.put(name, value);
            }
            if (sdkProviderProperties == null) {
                // Lazy initialization
                sdkProviderProperties = new Hashtable<String, Properties>();
            } else {
                // Erase any prior properties for this SDK
                sdkProviderProperties.remove(sdkId);
            }
            sdkProviderProperties.put(sdkId, properties);
        }
    }

    /**
     * @param parent
     * @param name
     */
    private void removeChildXmlElement(Element parent, String name) {
        Element child = XMLUtils.getFirstElementWithTagName(parent, name);
        if (child != null) {
            child.getParentNode().removeChild(child);
        }
    }

    /**
     * @param rootElement
     */
    private void saveConfigurations(Element rootElement) {
        if (configurations == null) {
            return;
        }
        // remove old one
        removeChildXmlElement(rootElement, MTJRuntimeList.ELEM_CONFIGURATIONS);
        // create new one
        Element configsElement = XMLUtils.createChild(rootElement,
                MTJRuntimeList.ELEM_CONFIGURATIONS);
        for (MTJRuntime config : configurations) {
            Element configElement = XMLUtils.createChild(configsElement,
                    MTJRuntime.ELEM_CONFIGURATION);
            configElement.setAttribute(MTJRuntime.ATTR_RUNTIMENAME, config
                    .getName());
            configElement.setAttribute(MTJRuntime.ATTR_RUNTIMEACTIVE, String
                    .valueOf(config.isActive()));
            saveDevice(configElement, config.getDevice());
            saveSymbolSet(configElement, config.getSymbolSet());
            saveWorkspaceSymbolSets(configElement, config
                    .getWorkspaceScopeSymbolSets());
        }
    }

    /**
     * @param rootElement
     * @param device
     */
    private void saveDevice(Element rootElement, IDevice device) {
        if (device == null) {
            return;
        }
        // remove old one
        removeChildXmlElement(rootElement, ELEM_DEVICE);
        // create new one
        Element newDeviceElement = XMLUtils.createChild(rootElement,
                ELEM_DEVICE);
        newDeviceElement.setAttribute(ATTR_DEVICEGROUP, device.getSDKName());
        newDeviceElement.setAttribute(ATTR_DEVICENAME, device.getName());
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#saveMetaDataToFile(org.eclipse.core.resources.IFile)
     */
    private void saveMetaDataToFile(IFile storeFile) throws CoreException {
        try {
            String pluginVersion = MTJCore.getMTJCoreVersion();
            Version newVersion = new Version(pluginVersion);
            if (rootXmlElement == null) {
                rootXmlElement = XMLUtils.createRootElement(ELEM_ROOT_NAME,
                        newVersion);
            } else {
                rootXmlElement.setAttribute(XMLUtils.ATTR_VERSION, newVersion
                        .toString());
            }

            if (jadFileName != null) {
                rootXmlElement.setAttribute(ATTR_JAD_FILE, jadFileName);
            }
            // We keep top level <device> element in meta data file, just for
            // compatible with old version MTJ
            saveDevice(rootXmlElement, device);

            saveSignatureProps(rootXmlElement);
            saveConfigurations(rootXmlElement);
            saveSDKProviderProperties(rootXmlElement);
            saveLibletProperties(rootXmlElement);
            File localFile = storeFile.getLocation().toFile();
            XMLUtils
                    .writeDocument(localFile, rootXmlElement.getOwnerDocument());
            version = newVersion;
            getStoreIFile().refreshLocal(1, new NullProgressMonitor());
        } catch (ParserConfigurationException pce) {
            MTJStatusHandler.throwCoreException(IStatus.WARNING, 99999, pce);
        } catch (TransformerException te) {
            MTJStatusHandler.throwCoreException(IStatus.WARNING, 99999, te);
        } catch (IOException ioe) {
            MTJStatusHandler.throwCoreException(IStatus.WARNING, 99999, ioe);
        }
    }

    /**
     * @param rootElement
     * @throws CoreException
     */
    private void saveSignatureProps(Element rootElement) throws CoreException {
        // remove old one
        removeChildXmlElement(rootElement, ELEM_SIGNING);
        // create new one
        Element newSignRoot = XMLUtils.createChild(rootElement, ELEM_SIGNING);
        boolean bSign = signatureProps.isSignProject();
        newSignRoot.setAttribute(ATTR_SIGN_PROJECT, Boolean.toString(bSign));
        boolean bSpec = signatureProps.isProjectSpecific();
        newSignRoot
                .setAttribute(ATTR_PROJECT_SPECIFIC, Boolean.toString(bSpec));

        // The alias is necessary either if the project has specific
        // signing settings or not.
        XMLUtils.createTextElement(newSignRoot, ELEM_ALIAS, signatureProps
                .getKeyAlias());
        if (!bSpec) {
            return;
        }
        XMLUtils.createTextElement(newSignRoot, ELEM_KEYSTORE, signatureProps
                .getKeyStoreDisplayPath());
        XMLUtils.createTextElement(newSignRoot, ELEM_PROVIDER, signatureProps
                .getKeyStoreProvider());
        XMLUtils.createTextElement(newSignRoot, ELEM_KEYSTORETYPE,
                signatureProps.getKeyStoreType());

        Element passRoot = XMLUtils.createChild(newSignRoot, ELEM_PASSWORDS);
        passRoot.setAttribute(ATTR_STOREPASSWORDS, Integer
                .toString(signatureProps.getPasswordStorageMethod()));

        ISecurePreferences root = SecurePreferencesFactory.getDefault();
        String url = Utils.getKeyringURL(project);
        if (root.nodeExists(url)) {
            ISecurePreferences preferences = root.node(url);
            preferences.clear();
        }

        switch (signatureProps.getPasswordStorageMethod()) {
            case ISignatureProperties.PASSMETHOD_IN_PROJECT:
                XMLUtils.createTextElement(passRoot, ELEM_PWD_KEYSTORE,
                        encodePassword(signatureProps.getKeyStorePassword()));
                XMLUtils.createTextElement(passRoot, ELEM_PWD_KEY,
                        encodePassword(signatureProps.getKeyPassword()));
                break;
            case ISignatureProperties.PASSMETHOD_IN_KEYRING:

                ISecurePreferences node = root.node(url);
                try {
                    node.put(KEYRING_KEYSTOREPASS_KEY, signatureProps
                            .getKeyStorePassword(), false);

                    node.put(KEYRING_KEYPASS_KEY, signatureProps
                            .getKeyPassword(), false);
                } catch (StorageException e) {
                    MTJCoreErrors.throwCoreExceptionError(7676,
                            "Could not save password into keyring,", e);
                }

                break;
            case ISignatureProperties.PASSMETHOD_PROMPT:
            default:
                break;
        }
    }
    
    private void saveLibletProperties(Element rootElement) throws CoreException {
    	// remove old one
        removeChildXmlElement(rootElement, ELEM_LIBLETS);
        
        if (libletProperties != null) {
            // create new one
            Element newLibletsRoot = XMLUtils.createChild(rootElement, ELEM_LIBLETS);
            
            for (LibletProperties p : libletProperties) {
            	Element libletElement = XMLUtils.createChild(newLibletsRoot, ELEM_LIBLET);
            	libletElement.setAttribute(ATTR_NAME, p.getName());
            	libletElement.setAttribute(ATTR_LEVEL, p.getLevel().getJadName());
            	libletElement.setAttribute(ATTR_TYPE, p.getType().getJadName());
            	libletElement.setAttribute(ATTR_JAD_URL, p.getJadURL());
            	libletElement.setAttribute(ATTR_VENDOR, p.getVendor());
            	libletElement.setAttribute(ATTR_VERSION, p.getVersion());
            	libletElement.setAttribute(ATTR_PROJECT_NAME, p.getProjectName());
            }
        }
    }
    
    /**
     * @param rootElement
     * @throws CoreException
     */
    private void saveSDKProviderProperties(Element rootElement) throws CoreException {
        if (sdkProviderProperties == null || sdkProviderProperties.isEmpty()) {
            return;
        }
        // Remove old "sdkProviderProperties" element
        removeChildXmlElement(rootElement,
                MTJRuntime.ELEM_SDK_PROVIDER_PROPERTIES);
        // Create new "sdkProviderProperties" element
        Element sdkProvidersPropertiesElement = XMLUtils.createChild(
                rootElement, MTJRuntime.ELEM_SDK_PROVIDER_PROPERTIES);
        Enumeration<String> sdkProviderIds = sdkProviderProperties.keys();
        // For each SDK ID...
        while (sdkProviderIds.hasMoreElements()) {
            String id = sdkProviderIds.nextElement();
            Properties properties = sdkProviderProperties.get(id);
            Element sdkElement = XMLUtils.createChild(
                    sdkProvidersPropertiesElement, MTJRuntime.ELEM_SDK);
            sdkElement.setAttribute(MTJRuntime.ATTR_SDK_ID, id);
            Enumeration<?> propertyNames = properties.propertyNames();
            // For each property...
            while (propertyNames.hasMoreElements()) {
                String name = (String) propertyNames.nextElement();
                String value = properties.getProperty(name);
                Element propertyElement = XMLUtils.createChild(sdkElement,
                        MTJRuntime.ELEM_SDK_PROPERTY);
                propertyElement.setAttribute(MTJRuntime.ATTR_SDK_PROPERTY_NAME,
                        name);
                propertyElement.setAttribute(
                        MTJRuntime.ATTR_SDK_PROPERTY_VALUE, value);
            }
        }       
    }

    /**
     * @param symbolSetElement
     * @param symbol
     */
    private void saveSymbol(Element symbolSetElement, ISymbol symbol) {
        Element symbolElement = XMLUtils.createChild(symbolSetElement,
                MTJRuntime.ELEM_SYMBOL);
        symbolElement
                .setAttribute(MTJRuntime.ATTR_SYMBOLNAME, symbol.getName());
        symbolElement.setAttribute(MTJRuntime.ATTR_SYMBOLVALUE, symbol
                .getValue());

    }

    /**
     * @param configElement
     * @param symbolSet
     */
    private void saveSymbolSet(Element configElement, ISymbolSet symbolSet) {
        if (symbolSet == null) {
            return;
        }
        Element symbolSetElement = XMLUtils.createChild(configElement,
                MTJRuntime.ELEM_SYMBOL_SET);

        symbolSetElement.setAttribute(MTJRuntime.ATTR_SYMBOL_SET_NAME,
                symbolSet.getName());

        for (ISymbol symbol : symbolSet.getSymbols()) {
            saveSymbol(symbolSetElement, symbol);
        }
    }

    /**
     * @param configElement
     * @param workspaceScopeSymbolSets
     */
    private void saveWorkspaceSymbolSets(Element configElement,
            List<ISymbolSet> workspaceScopeSymbolSets) {
        if ((workspaceScopeSymbolSets == null)
                || (workspaceScopeSymbolSets.isEmpty())) {
            return;
        }
        for (ISymbolSet s : workspaceScopeSymbolSets) {
            Element symbolSetElement = XMLUtils.createChild(configElement,
                    MTJRuntime.ELEM_WORKSPACE_SYMBOLSET);
            symbolSetElement.setAttribute(
                    MTJRuntime.ATTR_WORKSPACESYMBOLSETNAME, s.getName());
        }

    }

    /**
     * Set the device into active configuration, if project has no
     * configurations, we create one from the device.
     * 
     * @param device
     */
    private void setDeviceIntoActiveConfig(IDevice device) {
        MTJRuntime activeConfig = configurations.getActiveMTJRuntime();
        if (activeConfig != null) {
            activeConfig.setDevice(device);
            // if activeConfig is not null, must return here, make sure not add
            // the acriveConfig again.
            return;
        }
        if ((activeConfig == null) && !configurations.isEmpty()) {
            activeConfig = configurations.get(0);
        }
        // if configurations is empty, we create a configuration according the
        // device, and add it into the configurations
        if (activeConfig == null) {
            activeConfig = new MTJRuntime(device.getName());
            activeConfig.setSymbolSet(device.getSymbolSet());
        }
        activeConfig.setActive(true);
        activeConfig.setDevice(device);
        configurations.add(activeConfig);
    }
}
