/**
 * Copyright (c) 2003, 2009 Craig Setera and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Craig Setera (EclipseME)     - Initial implementation
 *     Diego Sandin (Motorola)      - Refactoring package name to follow eclipse
 *                                    standards
 *     Gustavo de Paula (Motorola)  - Refactore preverification interfaces
 *     Marc Wilhelm	                - Bug 283113: preverification is slow
 */
package org.eclipse.mtj.internal.core.build.preverifier;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.*;
import org.eclipse.debug.core.IStreamListener;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IStreamMonitor;
import org.eclipse.debug.core.model.IStreamsProxy;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.IVMInstallType;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.mtj.core.build.preverifier.IPreverificationError;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.PreferenceAccessor;
import org.eclipse.mtj.internal.core.build.BuildConsoleProxy;
import org.eclipse.mtj.internal.core.build.BuildLoggingConfiguration;
import org.eclipse.mtj.internal.core.build.IBuildConsoleProxy;
import org.eclipse.mtj.internal.core.util.EnvironmentVariables;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.TemporaryFileManager;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.osgi.framework.Version;


/**
 * A standard preverifier implementation. This preverifier requires the
 * pre-verification binary and the available CLDC parameters to be specified.
 * Once created, the preverifier may be stored and retrieved using the standard
 * persistence mechanism.
 *
 * @author Craig Setera
 */
public class StandardPreverifier implements IPreverifier {

    /** Declare whether classes should be pre-verified by the preverifier */
    public static final String BUILD_ARG_PREVERIFY_CLASSES = "preverifyClasses"; //$NON-NLS-1$

    /** Declare whether libraries should be pre-verified by the preverifier */
    public static final String BUILD_ARG_PREVERIFY_LIBS = "preverifyLibraries"; //$NON-NLS-1$

    /** Declare what project the output of the preverifier should be placed */
    public static final String BUILD_ARG_PREVERIFY_TARGET = "preverifyTargetProject"; //$NON-NLS-1$

    // The regular expression we will use to match the preverify error
    private static final String PREV_ERR_REGEX = "^Error preverifying class (\\S*)$"; //$NON-NLS-1$

    // The compiled pattern for regular expression matching
    private static final Pattern PREV_ERR_PATTERN = Pattern.compile(
            PREV_ERR_REGEX, Pattern.MULTILINE);

    private BuildConsoleProxy consoleProxy = BuildConsoleProxy.getInstance();

    // The parameters to use
    private StandardPreverifierParameters parameters;

    // The executable to use for pre-verification
    private File preverifierExecutable;

    /**
     * @return Returns the parameters.
     */
    public StandardPreverifierParameters getParameters() {
        return parameters;
    }

    /**
     * @return Returns the preverifierExecutable.
     */
    public File getPreverifierExecutable() {
        return preverifierExecutable;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        String preverifierExeString = persistenceProvider
                .loadString("preverifierExecutable");
        preverifierExecutable = new File(preverifierExeString);
        parameters = (StandardPreverifierParameters) persistenceProvider
                .loadPersistable("parameters");
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.build.preverifier.IPreverifier#preverify(org.eclipse.mtj.core.project.IMTJProject, org.eclipse.core.resources.IResource[], org.eclipse.core.resources.IFolder, org.eclipse.core.runtime.IProgressMonitor)
     */
    public IPreverificationError[] preverify(IMTJProject midletProject,
            IResource[] toVerify, IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException {


        ensureOutputFolderExists(outputFolder, monitor);

        IPath outputFolderLocation = outputFolder.getLocation();
        if (outputFolderLocation == null) {
            IStatus status = new Status(IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID,
                    "Failed To Get The Preverification Output Folder Location");
            throw new CoreException(status);
        }

        File outputFolderAsFile = outputFolderLocation.toFile();
        ArrayList<String> baseArguments = constructCommandLine(midletProject,
                outputFolderAsFile, monitor);

        ArrayList<String> arguments = new ArrayList<String>(baseArguments);
        ArrayList<String> classNames = new ArrayList<String>(toVerify.length);


        // Append the resources that need to be preverified
        for (IResource resource : toVerify) {

            IPath resLoc = resource.getLocation();

            switch (resource.getType()) {
                case IResource.FOLDER:
                case IResource.PROJECT:
                    if (resLoc != null) {
                        classNames.add(resLoc.toOSString());
                    }
                    break;

                case IResource.FILE:
                    if (resource.getName().endsWith(".class")) {
                        addClassTarget(classNames, resource);
                    } else if (resource.getName().endsWith(".jar")) {
                        if (resLoc != null) {
                            classNames.add(resLoc.toOSString());
                        }
                    }
                    break;
            }

        }
        // List of errors found during preverification
        ArrayList<IPreverificationError> allErrors = new ArrayList<IPreverificationError>();

        if (classNames.size()>0){
            File preverifierArguments = null;
            try {
                preverifierArguments = File.createTempFile("mtj.preverifier.", null);
                final String argsFname = preverifierArguments.getAbsolutePath();
                FileWriter os = new FileWriter(preverifierArguments);
                String args = classNames.toString();
                args = args.substring(1,args.length()-1);
                args = args.replaceAll(", ", " ");
                os.write(args);
                os.write("\n");
                os.close();

                arguments.add("@"+argsFname);
                // Launch the system process
                String[] commandLine = arguments.toArray(new String[arguments
                                                                    .size()]);
                IPreverificationError[] errors = runPreverifier(commandLine, null,
                        monitor);
                allErrors.addAll(Arrays.asList(errors));

            } catch (IOException e) {
                IStatus status = new Status(IStatus.ERROR,
                        IMTJCoreConstants.PLUGIN_ID, "Failed to create temp file for preverification.",
                        e);
                throw new CoreException(status);
            } finally {
                if (null!=preverifierArguments && preverifierArguments.exists()){
                    preverifierArguments.delete();
                }
            }
        }

        return allErrors.toArray(new IPreverificationError[allErrors.size()]);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.build.preverifier.IPreverifier#preverifyJarFile(org.eclipse.mtj.core.project.IMTJProject, java.io.File, org.eclipse.core.resources.IFolder, org.eclipse.core.runtime.IProgressMonitor)
     */
    public IPreverificationError[] preverifyJarFile(IMTJProject midletProject,
            File jarFile, IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException {

        File srcDirectory = null;
        File tgtDirectory = null;

        try {
            srcDirectory = TemporaryFileManager.instance.createTempDirectory(
                    jarFile.getName().replace('.', '_') + "_", ".tmp");
            srcDirectory.mkdirs();

            // Expand files from .jar
            Utils.extractArchive(jarFile, srcDirectory);

            // Create the target directory for the preverification. We will
            // tell the preverifier to use this when doing the preverification.
            tgtDirectory = TemporaryFileManager.instance.createTempDirectory(
                    jarFile.getName().replace('.', '_') + "_", ".tmp");

            tgtDirectory.mkdirs();
        } catch (SecurityException se) {
            IStatus status = new Status(IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID,
                    "Failed to inflate jar file due to a security violation.",
                    se);
            throw new CoreException(status);
        } catch (IOException ioe) {
            IStatus status = new Status(IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID, "Failed to inflate jar file.",
                    ioe);
            throw new CoreException(status);
        }

        ArrayList<String> arguments = constructCommandLine(midletProject,
                tgtDirectory, monitor);
        arguments.add(srcDirectory.toString());

        // Launch the system process
        String[] environment = getEnvironment(jarFile);
        String[] commandLine = arguments.toArray(new String[arguments.size()]);
        IPreverificationError[] errors = runPreverifier(commandLine,
                environment, monitor);

        // TODO we need to test the outcome of the previous before going much
        // further here...

        try {
            // Copy all of the non-class resources so they end up back in the
            // jar file
            FileFilter classFilter = new FileFilter() {
                public boolean accept(File pathname) {
                    return pathname.isDirectory()
                            || !pathname.getName().endsWith(".class");
                }
            };

            Utils.copy(srcDirectory, tgtDirectory, classFilter);

            // Finally, re-jar the output of the preverification into the
            // requested jar file...
            File outputJarFile = new File(outputFolder.getLocation().toFile(),
                    jarFile.getName());
            Utils.createArchive(outputJarFile, tgtDirectory);
        } catch (IOException ioe) {
            IStatus status = new Status(
                    IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID,
                    "Failed to re-jar the output of the preverification into the requested jar file.",
                    ioe);
            throw new CoreException(status);
        }

        return errors;
    }

    /**
     * @param parameters The parameters to set.
     */
    public void setParameters(StandardPreverifierParameters parameters) {
        this.parameters = parameters;
    }

    /**
     * @param preverifierExecutable The preverifierExecutable to set.
     */
    public void setPreverifierExecutable(File preverifierExecutable) {
        this.preverifierExecutable = preverifierExecutable;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        persistenceProvider.storeString("preverifierExecutable",
                preverifierExecutable.toString());
        persistenceProvider.storePersistable("parameters", parameters);
    }

    /**
     * Add the classpath information to the preverifier arguments based on
     * specified {@link IMTJProject}.
     * <p>
     * This method includes the "<code>-classpath</code>" option plus the
     * project's classpath to the argument list.
     * </p>
     *
     * @param args the list of arguments used to configure the preverification
     *            execution.
     * @param mtjProject the project from which we will retrieve the classpath
     *            information.
     * @throws CoreException if unable to compute the classpath.
     */
    private void addClasspath(ArrayList<String> args, IMTJProject mtjProject)
            throws CoreException {

        String classpath = getFullClasspath(mtjProject);

        args.add("-classpath"); //$NON-NLS-1$
        args.add(classpath);
    }

    /**
     * Add a class target to the resources to be verified.
     *
     * @param args list of arguments that represents the command line for
     *            triggering the preverification process
     * @param resource the class target that will be included in the resources
     *            to be verified.
     */
    private void addClassTarget(List<String> args, IResource resource)
            throws JavaModelException {

        IProject project = resource.getProject();
        IJavaProject javaProject = JavaCore.create(project);
        if (javaProject != null) {
            String className = extractClassName(javaProject, resource);
            if (className != null) {
                args.add(className);
            }
        }
    }

    /**
     * Add the directory in which output is written to the argument list.
     *
     * @param args the list of arguments used to configure the preverification
     *            execution.
     * @param outputDir the directory in which output is written.
     */
    private void addOutputDirArg(ArrayList<String> args, File outputDir) {
        args.add("-d");
        args.add(outputDir.toString());
    }

    /**
     * Construct the command line for triggering the preverification process.
     *
     * @param mtjProject project from we'll retrieve the parameters to be used
     *            for controlling the CLDC preverification and the full
     *            classpath including all Java ME libraries.
     * @param outputFolder the directory in which the preverification output is
     *            written.
     * @param monitor
     * @return the list of arguments that represents the command line for
     *         triggering the preverification process
     * @throws CoreException if this method fails. Reasons include:
     *             <ul>
     *             <li>The CLDC configuration parameters could not be retrieved
     *             from the specified {@link IMTJProject}.</li>
     *             <li>The classpath information could not be retrieved from the
     *             specified {@link IMTJProject}.</li>
     *             </ul>
     */
    private ArrayList<String> constructCommandLine(IMTJProject mtjProject,
            File outputFolder, IProgressMonitor monitor) throws CoreException {

        ArrayList<String> cmdArgs = new ArrayList<String>();

        // The pathname string for the preverifier binary.
        cmdArgs.add(preverifierExecutable.toString());

        // Include CLDC configuration parameters
        String[] configurationParameters = getCLDCConfigurationParameters(mtjProject);
        for (String configurationParameter : configurationParameters) {
            cmdArgs.add(configurationParameter);
        }

        // Configure the classpath
        addClasspath(cmdArgs, mtjProject);

        // Set the output folder for the preverified files
        addOutputDirArg(cmdArgs, outputFolder);

        return cmdArgs;
    }

    /**
     * Ensure that the specified output folder exists in the file system.
     * <p>
     * If the folder doesn't exists, we'll try to create it using the
     * {@link IFolder#create(boolean, boolean, IProgressMonitor)} method.
     * </p>
     *
     * @param folder the folder that must exist in the file system.
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @throws CoreException if this method fails to create the folder for any
     *             reason.
     */
    private void ensureOutputFolderExists(IFolder folder,
            IProgressMonitor monitor) throws CoreException {
        // Make sure the output folder exists before we start
        if (!folder.exists()) {
            folder.create(true, true, monitor);
        }
    }

    /**
     * Extract the class name from the specified IResource within the specified
     * java project.
     *
     * @param javaProject the java project to provide the relative name
     * @param resource the resource to extract a class name
     * @return the class name or <code>null</code> if the resource name cannot
     *         be converted for some reason.
     */
    private String extractClassName(IJavaProject javaProject, IResource resource) {
        IPath classPath = null;
        try {
            classPath = extractResourcePath(javaProject, resource);
        } catch (JavaModelException e) {
            MTJLogger.log(IStatus.WARNING,
                    "Failed to extract the path from the specified IResource",
                    e);
        }

        return (classPath == null) ? null : classPath.removeFileExtension()
                .toString().replace('/', '.');
    }

    /**
     * Extract the path from the specified IResource within the specified java
     * project.
     *
     * @param javaProject the project where the resource is located.
     * @param resource an {@link IResource} instance from which the path will be
     *            retrieved. The resource must not be <code>null</code>.
     * @return the path extracted from the specified {@link IResource}.
     * @throws JavaModelException if this element does not exist or if an
     *             exception occurs while accessing its corresponding resource
     */
    private IPath extractResourcePath(IJavaProject javaProject,
            IResource resource) throws JavaModelException {

        IPath resultPath = null;
        IPath projectOutputPath = javaProject.getOutputLocation()
                .makeAbsolute();

        IPath resourcePath = resource.getFullPath();

        IClasspathEntry[] classpath = javaProject.getRawClasspath();
        for (IClasspathEntry entry : classpath) {
            if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
                IPath entryPath = entry.getOutputLocation();
                entryPath = (entryPath == null) ? projectOutputPath : entryPath
                        .makeAbsolute();

                if (entryPath.isPrefixOf(resourcePath)) {
                    resultPath = resourcePath.removeFirstSegments(entryPath
                            .segmentCount());
                }
            }
        }

        return resultPath;
    }

    /**
     * Get the environment values for the pre-verification processing.
     *
     * @param The resources to verify. If this is a jar file, the jar program
     *            must be available on the path.
     * @return
     * @throws CoreException
     */
    private String[] getEnvironment(File jarFileToVerify) throws CoreException {
        String[] environment = null;

        if (jarFileToVerify != null) {
            // See if the jar executable is available already...
            if (!isJarExecutableOnPath()) {
                // See if we can get it from the VM installation
                IVMInstall fullJDK = searchForVMInstallWithJar();
                if (fullJDK == null) {
                    IStatus status = MTJStatusHandler.newStatus(IStatus.ERROR,
                            IMTJCoreConstants.ERR_COULD_NOT_FIND_JAR_TOOL,
                            "Could not find jar tool executable.");
                    MTJStatusHandler.statusPrompt(status, this);
                } else {
                    // Found a VM installation with the jar tool...
                    // Set the PATH environment value so that the
                    // preverifier can find the jar tool.
                    String pathValue = new File(fullJDK.getInstallLocation(),
                            "bin").toString();
                    environment = getEnvironmentWithAugmentedPath(pathValue);
                }
            }
        }

        return environment;
    }

    /**
     * Augment the PATH environment variables and return them in a form that can
     * be used in an exec() call.
     *
     * @param pathValue
     * @return
     * @throws CoreException
     */
    private String[] getEnvironmentWithAugmentedPath(String pathValue)
            throws CoreException {
        String[] environment = null;

        try {
            EnvironmentVariables envVars = new EnvironmentVariables();
            String path = envVars.getVariable("PATH");
            path = path + File.pathSeparator + pathValue;
            envVars.setVariable("PATH", path);

            environment = envVars.convertToStrings();

        } catch (IOException e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, -999, e);
        }

        return environment;
    }

    /**
     * Get the full classpath including all J2ME libraries from the specified
     * {@link IMTJProject}.
     *
     * @param midletProject the project from the classpath must be extracted.
     * @return the project's full classpath as a string.
     * @throws CoreException if unable to compute the classpath.
     */
    private String getFullClasspath(IMTJProject midletProject)
            throws CoreException {

        IJavaProject javaProject = midletProject.getJavaProject();

        String[] entries = JavaRuntime
                .computeDefaultRuntimeClassPath(javaProject);

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < entries.length; i++) {
            if (i != 0) {
                sb.append(File.pathSeparatorChar);
            }

            sb.append(entries[i]);
        }

        return sb.toString();
    }

    /**
     * Get the executable for creation of jar files.
     *
     * @return
     */
    private String getJarExecutable() {
        String executable = null;

        String os = System.getProperty("os.name").toLowerCase();

        if ((os.indexOf("windows 9") > -1) || (os.indexOf("nt") > -1)
                || (os.indexOf("windows 2000") > -1)
                || (os.indexOf("windows xp") > -1)) {
            executable = "jar.exe";
        } else {
            executable = "jar";
        }

        return executable;
    }

    /**
     * Return a boolean indicating whether the specified virtual machine
     * installation appears to have the jar executable within it.
     *
     * @param install
     * @return
     */
    private boolean installContainsJarExecutable(IVMInstall install) {
        boolean containsJar = false;

        File installLocation = install.getInstallLocation();
        if (installLocation != null) {
            File bin = new File(installLocation, "bin");
            if (bin.exists()) {
                File[] matches = bin.listFiles(new FileFilter() {
                    public boolean accept(File pathname) {
                        return pathname.isFile()
                                && pathname.getName().startsWith("jar");
                    }
                });

                containsJar = ((matches != null) && (matches.length > 0));
            }
        }

        return containsJar;
    }

    /**
     * Return a boolean indicating whether the JAR executable can be found on
     * the system path.
     *
     * @param testJar
     * @return
     * @throws CoreException
     */
    private boolean isJarExecutableOnPath() throws CoreException {
        boolean onPath = false;

        String executable = getJarExecutable();
        try {
            EnvironmentVariables envVars = new EnvironmentVariables();
            String pathString = envVars.getVariable("PATH");
            StringTokenizer st = new StringTokenizer(pathString,
                    File.pathSeparator);
            while (st.hasMoreTokens()) {
                File path = new File(st.nextToken());
                File jar = new File(path, executable);
                if (jar.exists()) {
                    onPath = true;
                    break;
                }
            }
        } catch (IOException e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, -999, e);
        }

        return onPath;
    }

    /**
     * Search for and return a virtual machine installation that appears to have
     * the jar tool executable contained within.
     *
     * @return
     */
    private IVMInstall searchForVMInstallWithJar() {
        IVMInstall fullJDK = null;

        IVMInstall install = JavaRuntime.getDefaultVMInstall();
        if (installContainsJarExecutable(install)) {
            fullJDK = install;
        } else {
            IVMInstallType installType = install.getVMInstallType();
            IVMInstall[] installs = installType.getVMInstalls();
            for (IVMInstall install2 : installs) {
                install = install2;
                if (installContainsJarExecutable(install)) {
                    fullJDK = install;
                    break;
                }
            }
        }
        return fullJDK;
    }

    /**
     * Return the parameters to be used for controlling the CLDC
     * preverification.
     *
     * @param midletProject the project from which we will retrieve the
     *            configuration parameters
     * @return the CLDC configuration parameters that will be used for
     *         preverification such as <code>-nofinalize</code>,
     *         <code>-nonative</code> and <code>-nofp</code>
     * @throws CoreException if an error occurs while getting the CLDC
     *             configuration parameters from the {@link IMTJProject}.
     */
    protected String[] getCLDCConfigurationParameters(IMTJProject midletProject)
            throws CoreException {

        IProject project = midletProject.getProject();
        Version configVersion = PreferenceAccessor.instance
                .getPreverificationConfigurationVersion(project);

        return isCLDC1_0(configVersion) ? parameters.cldc10 : parameters.cldc11;
    }

    /**
     * Handle the arrival of text on the error stream.
     *
     * @param text
     * @param errorList
     */
    protected void handleErrorReceived(String text,
            List<IPreverificationError> errorList) {
        text = text.trim();
        Matcher matcher = PREV_ERR_PATTERN.matcher(text);
        if (matcher.find()) {
            // Found a match for the error...
            if (matcher.groupCount() > 0) {
                final String classname = matcher.group(1);

                String errorText = "Error preverifying class";
                if (matcher.end() < text.length()) {
                    StringBuffer sb = new StringBuffer(errorText);
                    sb.append(": ");

                    String detail = text.substring(matcher.end());
                    detail = detail.trim();
                    sb.append(detail);
                    errorText = sb.toString();
                }

                IClassErrorInformation classInfo = new IClassErrorInformation() {
                    public String getName() {
                        return classname;
                    }

                    public String getSourceFile() {
                        return null;
                    }
                };

                IPreverificationErrorLocation location = new PreverificationErrorLocation(
                        PreverificationErrorLocationType.UNKNOWN_LOCATION,
                        classInfo);
                IPreverificationError error = new PreverificationError(
                        PreverificationErrorType.UNKNOWN_ERROR, location, text);
                errorList.add(error);
            }
        } else {
            MTJLogger.log(IStatus.WARNING, text);
        }
    }

    /**
     * Return a boolean indicating whether the specified configuration is a 1.0
     * CLDC config.
     *
     * @param configSpec
     * @return
     */
    protected boolean isCLDC1_0(Version configVersion) {
        return ((configVersion.getMajor() == 1) && (configVersion.getMinor() == 0));
    }

    /**
     * Run the preverifier program and capture the errors that occurred during
     * preverification.
     *
     * @param commandLine the preverifier command line to be executed.
     * @param environment the working directory, or null.
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @throws CoreException if the creation of the the preverification process
     *             fails.
     */
    protected IPreverificationError[] runPreverifier(String[] commandLine,
            String[] environment, IProgressMonitor monitor)
            throws CoreException {

        // The list of errors found during the preverification process.
        final ArrayList<IPreverificationError> errorList = new ArrayList<IPreverificationError>();

        IProcess process = Utils.launchApplication(commandLine, null,
                environment, "Preverifier", "CLDC Preverifier");

        // Listen on the process output streams
        IStreamsProxy proxy = process.getStreamsProxy();
        IStreamMonitor errorStreamMonitor = proxy.getErrorStreamMonitor();
        if (BuildLoggingConfiguration.getInstance()
                .isPreverifierOutputEnabled()) {
            consoleProxy
                    .traceln("======================== Launching Preverification =========================");
            consoleProxy.addConsoleStreamListener(
                    IBuildConsoleProxy.Stream.ERROR, errorStreamMonitor);
            consoleProxy.addConsoleStreamListener(
                    IBuildConsoleProxy.Stream.OUTPUT, proxy
                            .getOutputStreamMonitor());
        }

        IStreamListener errorListener = new IStreamListener() {
            public void streamAppended(String text, IStreamMonitor monitor) {
                handleErrorReceived(text, errorList);
            }
        };
        
        synchronized (errorStreamMonitor) {
            String collected = errorStreamMonitor.getContents();
            
            if (collected != null) {
                errorListener.streamAppended(collected, errorStreamMonitor);
            }
            
            errorStreamMonitor.addListener(errorListener);
        }

        // Wait until completion
        while ((!monitor.isCanceled()) && (!process.isTerminated())) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
            ;
        }

        if (BuildLoggingConfiguration.getInstance()
                .isPreverifierOutputEnabled()) {
            consoleProxy
                    .traceln("======================== Preverification exited with code: "
                            + process.getExitValue());
        }

        return errorList.toArray(new IPreverificationError[errorList.size()]);
    }
}
