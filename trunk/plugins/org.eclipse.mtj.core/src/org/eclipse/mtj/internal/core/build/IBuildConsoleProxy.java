/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 */
package org.eclipse.mtj.internal.core.build;

import java.io.PrintWriter;

/**
 * An interface from which callers can access the BuildConsole.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @author Craig Setera
 */
public interface IBuildConsoleProxy {

    /**
     * Identifier for the streams supported by the BuildConsole.
     * 
     * @author Diego Madruga Sandin
     * @since 1.0
     */
    public enum Stream {

        /**
         * Identifier for the standard error stream.
         */
        ERROR,

        /**
         * Identifier for the standard out stream.
         */
        OUTPUT,

        /**
         * Identifier for the standard trace stream.
         */
        TRACE
    };

    /**
     * Return the PrintWriter for the console with the specified identifier.
     * 
     * @param stream Identifier for the stream to be used.
     * @return PrintWriter instance with the given {@link Stream} id.
     */
    public PrintWriter getConsoleWriter(Stream stream);

}
