/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.core.security.meep;

import java.io.*;
import java.net.*;
import java.security.Permission;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Helper class for accessing JAR file contents that does not keep JAR file locked on the file system.
 */
class JarFileDataAccessor {
    private File filePointer;
    private Set<String> entryNames = new HashSet<String>();

    public JarFileDataAccessor(File filePointer) throws IOException {
        this.filePointer = filePointer;

        JarFile jarFile = new JarFile(filePointer);

        try {
            Enumeration<JarEntry> entries = jarFile.entries();

            while (entries.hasMoreElements()) {
                entryNames.add(entries.nextElement().getName());
            }
        } finally {
            jarFile.close();
        }
    }

    private byte[] getData(InputStream in) throws IOException {
        try {
            BufferedInputStream bin = new BufferedInputStream(in);
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            int b;

            while ((b = bin.read()) != -1) {
                result.write(b);
            }

            return result.toByteArray();
        } finally {
            in.close();
        }
    }

    public boolean hasEntry(String name) {
        return entryNames.contains(name);
    }

    public synchronized byte[] getEntryData(String entryName) throws IOException {
        if (entryName == null) {
            throw new IOException("no entry name specified");
        } else {
            if (!hasEntry(entryName)) {
                throw new FileNotFoundException("JAR entry " + entryName + " not found in " + filePointer.getName());
            }

            JarFile jarFile = new JarFile(filePointer);

            try {
                return getData(jarFile.getInputStream(jarFile.getEntry(entryName)));
            } finally {
                jarFile.close();
            }
        }
    }

    private synchronized long getEntryLength(String entryName) throws IOException {
        if (entryName == null) {
            return filePointer.length();
        } else {
            if (!hasEntry(entryName)) {
                return -1;
            }

            JarFile jarFile = new JarFile(filePointer);

            try {
                return jarFile.getEntry(entryName).getSize();
            } finally {
                jarFile.close();
            }
        }
    }

    public URL getEntryURL(String entryName) {
        if (entryName == null || !hasEntry(entryName)) {
            return null;
        }

        try {
            return new URL("jar", "", -1, filePointer.toURI().toURL() + "!/" + entryName, new URLStreamHandler() {
                @Override
                protected URLConnection openConnection(URL u) throws IOException {
                    return new EntryURLConnection(u);
                }

                @Override
                protected URLConnection openConnection(URL u, Proxy p) throws IOException {
                    return openConnection(u);
                }
            });
        } catch (MalformedURLException ex) {
            return null;
        }
    }


    private class EntryURLConnection extends URLConnection {
        private Permission permission;
        private String contentType;
        private String entryName;

        EntryURLConnection(URL url) throws IOException {
            super(url);

            entryName = null;

            String spec = url.getFile();
            int idx = spec.indexOf("!/");

            if (idx >= 0 && (idx + 2 != spec.length())) {
                entryName = spec.substring(idx + 2);
            }
        }

        public Permission getPermission() throws IOException {
            if (permission == null) {
                permission = new FilePermission(filePointer.getAbsolutePath(), "read");
            }

            return permission;
        }

        @Override
        public void connect() throws IOException {
            if (!connected) {
                if (entryName != null && !hasEntry(entryName)) {
                    throw new FileNotFoundException("JAR entry " + entryName + " not found in " + filePointer.getPath());
                }

                connected = true;
            }
        }

        public InputStream getInputStream() throws IOException {
            if (entryName == null || !hasEntry(entryName)) {
                return null;
            }

            byte[] data = getEntryData(entryName);

            return data == null ? null : new ByteArrayInputStream(data);
        }

        public int getContentLength() {
            try {
                return (int) getEntryLength(entryName);
            } catch (IOException ex) {
                return -1;
            }
        }

        public Object getContent() throws IOException {
            connect();

            return super.getContent();
        }

        public String getContentType() {
            if (contentType == null) {
                if (entryName == null) {
                    contentType = "x-java/jar";
                } else {
                    BufferedInputStream in = null;
                    try {
                        connect();
                        in = new BufferedInputStream(getInputStream());
                        contentType = guessContentTypeFromStream(in);
                    } catch (IOException e) {} finally {
                        if (in != null) {
                            try {
                                in.close();
                            } catch (IOException ex) {}
                        }
                    }
                }
                if (contentType == null) {
                    contentType = guessContentTypeFromName(entryName);
                }

                if (contentType == null) {
                    contentType = "content/unknown";
                }
            }

            return contentType;
        }
    }
}
