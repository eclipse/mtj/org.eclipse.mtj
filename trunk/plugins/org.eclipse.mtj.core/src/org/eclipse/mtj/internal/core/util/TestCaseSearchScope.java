/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Aragao (Motorola) - Initial version
 */

package org.eclipse.mtj.internal.core.util;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;

/**
 * TestCaseSearchScope class creates a search scope based on
 * the jmunit.framework.cldc11.TestCase class.
 * 
 * @author David Aragao
 */
public class TestCaseSearchScope extends AbstractSuperClassSearchScope {

private static final String searchClassName = "jmunit/framework/cldc11/TestCase.class";
	
	public TestCaseSearchScope(IJavaProject javaProject)
			throws JavaModelException {
		super();
		this.setScopes(this.createSearchScopes(javaProject, searchClassName,IMTJCoreConstants.TEST_CASE_SUPERCLASS));
	}

}
