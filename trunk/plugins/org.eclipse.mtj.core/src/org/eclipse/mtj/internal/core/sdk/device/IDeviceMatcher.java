/**
 * Copyright (c) 2009 Sony Ericsson.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Daniel Olsson (Sony Ericsson) - Initial contribution
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.util.Set;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.sdk.device.IDevice;

/**
 * <p>
 * This is the interface for implementing matchers for SDKs and devices not
 * installed. The matchers are contributed via the extension point
 * <b>org.eclipse.mtj.core.devicematcher</b>.
 * </p>
 * <p>
 * Implementers need to implement the method {@link #match(String, String)}.
 * </p>
 * 
 * @author Daniel Olsson
 * @see {@link #match(String, String)}
 */
public interface IDeviceMatcher {

    /**
     * Match an existing device against the given device group and device name
     * 
     * @param deviceGroup
     * @param deviceName
     * @return matching device, <b>null</b> if none found
     * @see {@link IDevice}
     */
    IDevice match(String deviceGroup, String deviceName);

    /**
     * Match an existing device against the given device group and device name.
     * This method is intended to change the given MTJRuntime instance.
     * 
     * @param configurationName name of the MTJRuntime
     * @param deviceGroup name of the SDK to match
     * @param deviceName name of the device to match
     * @param existingConfigNames existing configuration names
     * @return String[] consisting of [&ltnew configuration name&gt, &ltSDK&gt,
     *         &ltdevice&gt], <b>null</b> if no match
     * @see {@link MTJRuntime}
     */
    String[] match(String configurationName, String deviceGroup,
            String deviceName, Set<String> existingConfigNames);

}
