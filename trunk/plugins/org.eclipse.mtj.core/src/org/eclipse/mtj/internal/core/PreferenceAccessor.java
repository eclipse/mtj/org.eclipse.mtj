/**
 * Copyright (c) 2004,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Fixed nullPointExcepiton on 
 *                                getPreverificationConfigurationVersion
 *     Hugo Raniere (Motorola)  - Including preference to store the default preverifier
 *     Gang Ma      (Sybase)    - Add getting debug level from preference
 *     David Marques (Motorola) - Updating getSpecifiedProguardOptions in order to support
 *                                project specific proguard config file path.
 *     Gustavo de Paula (Motorola) - Add proguard preverifier 
 *     Jon Dearden (Research In Motion) - [Bug 285699] Replaced deprecated use of Preferences 
 *                                                                 
 */
package org.eclipse.mtj.internal.core;

import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.project.midp.IApplicationDescriptor;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.osgi.util.NLS;
import org.osgi.framework.Version;
import org.osgi.service.prefs.BackingStoreException;

/**
 * A helper wrapper around the more complex preferences supported by MTJ.
 * 
 * @author Craig Setera    
 */
public class PreferenceAccessor {

    /**
     * The singleton instance of the Obfuscation Preferences
     */
    public static final PreferenceAccessor instance = new PreferenceAccessor();

    /**
     * Separator for multi-valued preferences
     */
    public static final String MULTI_VALUE_SEPARATOR = "|"; //$NON-NLS-1$

    // Scope search contexts
    private IEclipsePreferences defaultPreferences;
    private IEclipsePreferences instancePreferences;

    /**
     * Construct a new preference wrapper.
     * 
     * @param preferenceStore
     */
    private PreferenceAccessor() {
        super();
        defaultPreferences = new DefaultScope()
                .getNode(IMTJCoreConstants.PLUGIN_ID);
        instancePreferences = new InstanceScope()
                .getNode(IMTJCoreConstants.PLUGIN_ID);
    }
    
    // -------------------
    // Default preferences
    // -------------------

    /**
     * @return Returns the defaultProguardOptions.
     */
    public String getDefaultProguardOptions() {
        return defaultPreferences.get(
                IMTJCoreConstants.PREF_PROGUARD_OPTIONS,
                Utils.EMPTY_STRING);
    }
    
    // -------------------
    // Project preferences
    // -------------------
    
    /**
     * Return a boolean indicating whether or not automatic versioning should be
     * done while packaging.
     * 
     * @param project
     * @return
     */
    public boolean getAutoversionPackage(IProject project) {
        IEclipsePreferences preferences = getProjectPreferences(project,
                IMTJCoreConstants.PREF_PKG_USE_PROJECT,
                IMTJCoreConstants.PREF_PKG_AUTOVERSION);
        
        return preferences.getBoolean(
                IMTJCoreConstants.PREF_PKG_AUTOVERSION,
                MTJCorePreferenceInitializer.PREF_DEF_PKG_AUTOVERSION);
    }

    /**
     * Return the excluded manifest properties based on the specified project.
     * 
     * @param project
     * @return
     */
    public String[] getExcludedManifestProperties(IProject project) {
        IEclipsePreferences preferences = getProjectPreferences(project,
                IMTJCoreConstants.PREF_PKG_USE_PROJECT,
                IMTJCoreConstants.PREF_PKG_EXCLUDED_PROPS);
        String value = preferences.get(
                IMTJCoreConstants.PREF_PKG_EXCLUDED_PROPS,
                MTJCorePreferenceInitializer.PREF_DEF_PKG_EXCLUDED_PROPS);

        return parseMultiValuedPreferenceValue(value);
    }

    /**
     * Return the Preprocess debug level from the preferences.
     * 
     * @param project
     * @return
     */
    public String getPreprecessorDebuglevel(IProject project) {
        IEclipsePreferences preferences = getProjectPreferences(project,
                IMTJCoreConstants.PREF_PREPROCESS_USE_PROJECT,
                IMTJCoreConstants.PREF_PREPROCESS_DEBUG_LEVEL);
        String value = preferences.get(
                IMTJCoreConstants.PREF_PREPROCESS_DEBUG_LEVEL,
                MTJCorePreferenceInitializer.PREF_DEF_PREPROCESS_DEBUGLEVEL);

        return value;
    }

    /**
     * Return the configuration to be used for preverification.
     * 
     * @param project
     * @return
     * @throws CoreException
     */
    public Version getPreverificationConfigurationVersion(IProject project)
            throws CoreException {
        Version version = null;

        IJavaProject javaProject = JavaCore.create(project);
        IMidletSuiteProject suite = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);

        // Figure out which preference store to use
        IEclipsePreferences preferences = getProjectPreferences(project,
                IMTJCoreConstants.PREF_PREVERIFY_USE_PROJECT,
                IMTJCoreConstants.PREF_PREVERIFY_CONFIG_LOCATION);

        // Figure out where to extract the configuration from
        String location = preferences.get(
                IMTJCoreConstants.PREF_PREVERIFY_CONFIG_LOCATION,
                MTJCorePreferenceInitializer.PREF_DEF_PREVERIFY_CONFIG_LOCATION);

        // Pull the configuration from the specified place
        if (IMTJCoreConstants.PREF_PREVERIFY_CONFIG_LOCATION_JAD
                .equals(location)) {
            IApplicationDescriptor jad = suite.getApplicationDescriptor();
            version = jad.getConfigurationSpecificationVersion();
        } else if (IMTJCoreConstants.PREF_PREVERIFY_CONFIG_LOCATION_PLATFORM
                .equals(location)) {
            version = new Version("1.0"); //$NON-NLS-1$

            IMIDPDevice device = (IMIDPDevice) suite.getRuntimeList()
                    .getActiveMTJRuntime().getDevice();
            if (device != null) {

                IMIDPAPI api = device.getCLDCAPI();
                if (api != null) {
                    version = api.getVersion();
                }
            }
        } else {
            String identifierOrVersion = preferences.get(
                    IMTJCoreConstants.PREF_PREVERIFY_CONFIG_VALUE,
                    MTJCorePreferenceInitializer.PREF_DEF_PREVERIFY_CONFIG_VALUE);

            // This handles the transition to the case where configurations
            // have been dropped from the plug-in extension points.
            if (identifierOrVersion.startsWith("CLDC")) { //$NON-NLS-1$
                String[] split = identifierOrVersion.split("-"); //$NON-NLS-1$
                if (split.length == 2) {
                    identifierOrVersion = split[1];
                    version = new Version(identifierOrVersion);
                }
            }

        }

        // Fall back if we don't have a specification yet
        if (version == null) {
            version = new Version("1.0"); //$NON-NLS-1$
        }

        return version;
    }

    /**
     * Return the Proguard keep expressions from the preferences.
     * 
     * @param project
     * @return
     */
    public String[] getProguardKeepExpressions(IProject project) {
        IEclipsePreferences preferences = getProjectPreferences(project,
                IMTJCoreConstants.PREF_OBFUSCATION_USE_PROJECT,
                IMTJCoreConstants.PREF_PROGUARD_KEEP);
        String value = preferences.get(
                IMTJCoreConstants.PREF_PROGUARD_KEEP,
                MTJCorePreferenceInitializer.PREF_DEF_PROGUARD_KEEP);

        return parseMultiValuedPreferenceValue(value);
    }

    /**
     * Return the specified proguard options.
     * 
     * @param project
     * @return Returns the specifiedOptions.
     */
    public String getSpecifiedProguardOptions(IProject project) {
        String options = null;

        IEclipsePreferences preferences = getProjectPreferences(project,
                IMTJCoreConstants.PREF_OBFUSCATION_USE_PROJECT,
                IMTJCoreConstants.PREF_PROGUARD_OPTIONS);

        options = preferences.get(IMTJCoreConstants.PREF_PROGUARD_OPTIONS,
                MTJCorePreferenceInitializer.PREF_DEF_PROGUARD_OPTIONS);

        // In case the options are project specific, make paths project
        // relative.
        if (preferences.getBoolean(
                IMTJCoreConstants.PREF_OBFUSCATION_USE_PROJECT, false)) {
            StringBuffer buffer = new StringBuffer();
            String array[] = options.split(" "); //$NON-NLS-1$
            for (String option : array) {
                if (Pattern.matches("@.+", option)) { //$NON-NLS-1$
                    String config = option.substring(0x01); // Remove @
                    option = NLS.bind("@{0}", project //$NON-NLS-1$
                            .getLocation().append(config).toOSString());
                }
                buffer.append(" ").append(option); //$NON-NLS-1$
            }
            options = buffer.toString().trim();
        }
        return options;
    }

    /**
     * Return whether to use the specified proguard options.
     * 
     * @param project
     * @return Returns the useSpecifiedOptions.
     */
    public boolean isUseSpecifiedProguardOptions(IProject project) {
        IEclipsePreferences preferences = getProjectPreferences(project,
                IMTJCoreConstants.PREF_OBFUSCATION_USE_PROJECT,
                IMTJCoreConstants.PREF_PROGUARD_USE_SPECIFIED);

        return preferences.getBoolean(
                IMTJCoreConstants.PREF_PROGUARD_USE_SPECIFIED,
                MTJCorePreferenceInitializer.PREF_DEF_PROGUARD_USE_SPECIFIED);
    }
    
    /**
     * 
     * @param project
     * @return
     */
    public String getPreverifierType(IProject project) {
        IEclipsePreferences preferences = getProjectPreferences(project,
                IMTJCoreConstants.PREF_PREVERIFY_USE_PROJECT,
                IMTJCoreConstants.PREF_PREVERIFY_TYPE);
        return preferences.get(
                IMTJCoreConstants.PREF_PREVERIFY_TYPE,
                MTJCorePreferenceInitializer.PREF_DEF_PREVERIFY_TYPE);
    }

    /**
     * Return the correct preferences based on the specified project and any
     * groups of keys that tell whether the preference should be pulled from the
     * project or instance preferences.
     * 
     * @param project
     * @param selectionKeys
     * @return
     */
    private IEclipsePreferences getProjectPreferences(IProject project,
            String projectSpecificKey, String preferenceKey) {
        ProjectScope projectScope = new ProjectScope(project);
        IEclipsePreferences prefNode = projectScope
                .getNode(IMTJCoreConstants.PLUGIN_ID);

        boolean useProjectSpecific = prefNode.getBoolean(projectSpecificKey,
                false);
        prefNode = useProjectSpecific ? prefNode : instancePreferences;

        if (prefNode.get(preferenceKey, null) == null) {
            prefNode = defaultPreferences;
        }

        return prefNode;
    }

    /**
     * Return the parsed multi-value preference.
     * 
     * @param value
     * @return
     */
    private String[] parseMultiValuedPreferenceValue(String value) {
        StringTokenizer st = new StringTokenizer(value, MULTI_VALUE_SEPARATOR);

        int count = st.countTokens();
        String[] names = new String[count];
        for (int i = 0; i < count; i++) {
            names[i] = st.nextToken();
        }

        return names;
    }
    
    // ----------------
    // Core preferences
    // ----------------
    
    /**
     * Get a String preference from the core.
     */
    public String getString(String key) {
        String str = instancePreferences.get(key, null);
        return (str != null) ? str : defaultPreferences.get(key, Utils.EMPTY_STRING);
    }
    
    /**
     * Get a boolean preference from the core.
     */
    public boolean getBoolean(String key) {
        if (containsKey(instancePreferences, key))
            return instancePreferences.getBoolean(key, false);
        return defaultPreferences.getBoolean(key, false); 
    }
    
    /**
     * Get an int preference from the core.
     */
    public int getInt(String key) {
        if (containsKey(instancePreferences, key))
            return instancePreferences.getInt(key, 0);
        return defaultPreferences.getInt(key, 0); 
    }
    
    private boolean containsKey(IEclipsePreferences prefs, String key) {
        if (key == null)
            return false;
        String[] keys;
        try {
            keys = prefs.keys();
        } catch (BackingStoreException e) {
            e.printStackTrace();
            return false;
        }
        for (String str : keys) {
            if (key.equals(str))
                return true;
        }
        return false;
    }
    
}
