/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Gustavo de Paula (Motorola)  - Refactor preverification interfaces                                
 */
package org.eclipse.mtj.internal.core.build.preverifier;

import org.eclipse.mtj.internal.core.Messages;

/**
 * Enumeration type to represent the location of a preverication error.
 * 
 * @author Craig Setera
 */
public class PreverificationErrorLocationType {
    private static final String[] STRINGS = new String[] { Messages.PreverificationErrorLocationType_Class_declaration,
    	Messages.PreverificationErrorLocationType_Unknown_location };

    /** The code representing a class declaration */
    public static final int CLASS_DEFINITION_CODE = 0;

    /** A class declaration */
    public static final PreverificationErrorLocationType CLASS_DEFINITION = new PreverificationErrorLocationType(
            CLASS_DEFINITION_CODE);

    /** The code when the location of the error is unknown */
    public static final int UNKNOWN_LOCATION_CODE = 1;

    /** The location of the error is unknown */
    public static final PreverificationErrorLocationType UNKNOWN_LOCATION = new PreverificationErrorLocationType(
            UNKNOWN_LOCATION_CODE);

    // The code representing the location
    private int typeCode;

    /**
     * private constructor to limit options.
     * 
     * @param typeCode
     */
    private PreverificationErrorLocationType(int typeCode) {
        super();
        this.typeCode = typeCode;
    }

    /**
     * Return the location type code.
     * 
     * @return
     */
    public int getTypeCode() {
        return typeCode;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return STRINGS[typeCode];
    }
}
