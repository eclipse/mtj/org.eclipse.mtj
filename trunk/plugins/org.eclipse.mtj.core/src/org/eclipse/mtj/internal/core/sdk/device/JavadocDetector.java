/**
 * Copyright (c) 2003,2010 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma (Sybase) - Initial implementation
 *     
 */
package org.eclipse.mtj.internal.core.sdk.device;

import static org.eclipse.mtj.internal.core.Trace.trace;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.core.sdk.device.ILibrary;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary;
import org.eclipse.mtj.internal.core.Trace;
import org.osgi.framework.Version;


/**
 * Helper class to detect the javadoc location
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @since 1.0
 */
public class JavadocDetector {
	private static final String ZIP_EXT = ".zip";
    private static final String PACKAGE_LIST = "package-list"; //$NON-NLS-1$


	public URL detectJavadoc(ILibrary library) {
		boolean trace = Trace.isOptionEnabled(Trace.JAVADOC_DETECT_PERF);
		long enterTime=0;
		if (trace){
				 enterTime = System.currentTimeMillis();
		}
		
		URL retURL  = searchForJavaDoc(library);

		if (trace){
			long elapsed = System.currentTimeMillis() - enterTime;
			trace(Trace.JAVADOC_DETECT_PERF, "Detected javadoc for ", //$NON-NLS-1$
				library.toString(), " in ", elapsed, "ms"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return retURL;
	}


    
    private URL searchForJavaDoc(ILibrary library) {
        URL foundURL = null;

        File[] rootDirs = getDefaultDocRootDirectories(library);
        // a javadoc root directory exist
        if (rootDirs != null) {
            for (File docDir : rootDirs) {
                foundURL = searchForLibraryDoc(docDir, library);
                if (foundURL != null)
                    break;
            }
        }
        return foundURL;
    }
    
    


    // get the default javadoc directory for the library
    private File[] getDefaultDocRootDirectories(ILibrary library) {
        if (library == null)
            return null;

        ArrayList<File> docfiles = new ArrayList<File>();
        File parentDir = library.toFile().getParentFile();
        for (int i = 0; i < 5 && parentDir != null; i++) {
            File[] tmpDocfiles = parentDir.listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    // find the directory whose name contains "doc" string
                    return pathname.isDirectory()
                            && pathname.getName().indexOf("doc") != -1; //$NON-NLS-1$
                }
            });
            for (File file : tmpDocfiles) {
                docfiles.add(file);
            }
            parentDir = parentDir.getParentFile();

        }

        if (docfiles.size() > 0)
            return docfiles.toArray(new File[0]);
        else
            return null;

    }
    
    
    private URL searchForLibraryDoc(File parentDir, final ILibrary library) {
        if (library instanceof IMIDPLibrary) {
            // get the directories with javadoc
            File[] javadocDirsOrZips = parentDir.listFiles(new FileFilter() {
                public boolean accept(File folder) {
                    if (folder.isDirectory()) {
                        File indexFile = new File(folder, "index.html"); //$NON-NLS-1$
                        if (indexFile.isFile()) {
                            File packageList = new File(folder, PACKAGE_LIST);
                            if (packageList.isFile()) {
                                return true;
                            }
                        }
                    }
                    return false;
                }
            });

            if (javadocDirsOrZips.length == 0) {
                // consder the case when javadocs are zipped
                javadocDirsOrZips = parentDir.listFiles(new FileFilter() {
                    public boolean accept(File file) {
                        if (file.isFile() && file.getName().toLowerCase().endsWith(ZIP_EXT)) {
                            try {
                                ZipFile zip = new ZipFile(file);
                                try {
                                    ZipEntry indexEntry = zip.getEntry("index.html");
                                    if (indexEntry != null && !indexEntry.isDirectory()) {
                                        ZipEntry packageListEntry = zip.getEntry(PACKAGE_LIST);

                                        if (packageListEntry != null && !packageListEntry.isDirectory()) {
                                            return true;
                                        }
                                    }

                                } finally {
                                    zip.close();
                                }
                            } catch (Throwable t) {
                                return false;
                            }
                        }
                        return false;
                    }
                });
            }

            if (javadocDirsOrZips.length > 0) {
                try {
                    JarFile jar = new JarFile(library.toFile());
                    try {
                        IMIDPLibrary midpLib = (IMIDPLibrary) library;
                        boolean mustHaveJavaLang = midpLib.hasProfile() || midpLib.hasConfiguration();
                        Set<String> expandedJarEntries = new HashSet<String>();
                        Enumeration<JarEntry> jarEntries = jar.entries();

                        while (jarEntries.hasMoreElements()) {
                            File entryFile = new File(jarEntries.nextElement().getName());

                            while (entryFile != null) {
                                if (expandedJarEntries.add(entryFile.getPath().replace(File.separator, "/"))) {
                                    entryFile = entryFile.getParentFile();
                                } else {
                                    entryFile = null;
                                }
                            }
                        }

                        trace(Trace.JAVADOC_DETECT_DEBUG, "Searching javadocs for ", midpLib); //$NON-NLS-1$
                        
                        File detected = null;
                        
                        for (int i = 0; i < javadocDirsOrZips.length; i++) {
                            BufferedReader reader;
                            String packagesListName;
                            ZipFile javadocsZip = null;
                            File javadocDirOrZip = javadocDirsOrZips[i];

                            if (javadocDirOrZip.isDirectory()) {
                                File pList = new File(javadocDirOrZip, PACKAGE_LIST);
                                reader = new BufferedReader(new FileReader(pList));
                                packagesListName = pList.toString();
                            } else {
                                javadocsZip = new ZipFile(javadocDirOrZip);
                                reader = new BufferedReader(new InputStreamReader(
                                        javadocsZip.getInputStream(javadocsZip.getEntry(PACKAGE_LIST))));
                                packagesListName = javadocDirOrZip.toString() + File.separator + PACKAGE_LIST;
                            }

                            try {
                                if (detectByPackagesList(mustHaveJavaLang, expandedJarEntries, reader, packagesListName)
                                        || detectByLibraryFileName(library, javadocsZip, javadocDirOrZip)) {
                                    if (checkJavadocsVersion(midpLib, javadocsZip, javadocDirOrZip)) {
                                        trace(Trace.JAVADOC_DETECT_DEBUG,
                                                "Found javadocs for ", midpLib, " in ", javadocDirOrZip); //$NON-NLS-1$//$NON-NLS-2$
                                        return getJavadocsURL(javadocDirOrZip);
                                    } else {
                                        detected = javadocDirOrZip;
                                    }
                                }
                            } finally {
                                try {
                                    reader.close();
                                } finally {
                                    if (javadocsZip != null) {
                                        javadocsZip.close();
                                    }
                                }
                            }
                        }
                        
                        if (detected != null) {
                            trace(Trace.JAVADOC_DETECT_DEBUG, "Found javadocs for ", midpLib, " in ", detected); //$NON-NLS-1$//$NON-NLS-2$
                            return getJavadocsURL(detected);
                        }
                    } finally {
                        jar.close();
                    }
                } catch (IOException e) {
                    MTJCore.getMTJCore()
                            .getLog()
                            .log(new Status(IStatus.WARNING, MTJCore.getPluginId(), "Error while matchin javadocs", e)); //$NON-NLS-1$
                }

            }

            // Could not detect
            // Now recurse to sub directories
            File[] subdirectories = parentDir.listFiles(new FileFilter() {
                public boolean accept(File pathname) {
                    return pathname.isDirectory();
                }
            });
            URL url = null;
            for (int i = 0; i < subdirectories.length && url == null; i++) {
                url = searchForLibraryDoc(subdirectories[i], library);
            }
            return url;
        }
        return null;
    }

    private boolean detectByLibraryFileName(final ILibrary library, ZipFile javadocsZip, File javadocDirOrZip)
            throws MalformedURLException {
        String libFileName = library.toFile().getName();
        int dotIdx = libFileName.lastIndexOf('.');
        final String libName;
        
        if (dotIdx > -1) {
        	libName = libFileName.substring(0, dotIdx);
        } else {
        	libName = libFileName;
        }
      
        String pathName = getNormalizedJavadocsPathName(javadocsZip, javadocDirOrZip);
        String libraryName = libName.toLowerCase().replace('_', '-');
        
        return pathName.startsWith(libraryName) || libraryName.startsWith(pathName);
    }

    private String getNormalizedJavadocsPathName(ZipFile javadocsZip, File javadocDirOrZip) {
        // treat '-' and '_' characters as same for better detection
        String result = javadocDirOrZip.getName().toLowerCase().replace('_', '-');
        
        if (javadocsZip != null && result.endsWith(ZIP_EXT)) {
            result = result.substring(0, result.length() - ZIP_EXT.length());
        }
        
        return result;
    }
    
    private boolean checkJavadocsVersion(IMIDPLibrary library, ZipFile javadocsZip, File javadocDirOrZip) {
        List< ? extends IAPI> apis = library.getAPIs();
        
        if (apis == null) {
            return true;
        }
        
        String pathName = getNormalizedJavadocsPathName(javadocsZip, javadocDirOrZip);
        int versionIdx = pathName.lastIndexOf('-') + 1;
        
        if (versionIdx <= 0 || versionIdx >= pathName.length()) {
            return true;
        }
        
        Version javadocsVersion;
        
        try {
            javadocsVersion = new Version(pathName.substring(versionIdx).trim());
        } catch (IllegalArgumentException ex) {
            // No version specified in the javadocs path name
            return true;
        }
        
        for (IAPI api : apis) {
            if (javadocsVersion.compareTo(api.getVersion()) == 0) {
                return true;
            }
        }
        
        return false;
    }

    private boolean detectByPackagesList(boolean mustHaveJavaLang, Set<String> expandedJarEntries,
            BufferedReader packagesListReader, String packagesListName) throws IOException {
        boolean foundJavaLang = false;
        boolean found = false;
        String s = packagesListReader.readLine();

        while (s != null) {
            if (s.startsWith("#")) { //$NON-NLS-1$
                s = packagesListReader.readLine();
                continue;
            }
            // try to workaround the jsr classes packaged into CLDC & MIDP libraries.
            // very often SDKs pack the jsr classes to these libraries and without 
            // this guard the jsr javadocs are mapped to these libraries
            if (mustHaveJavaLang && !foundJavaLang && "java.lang".equals(s.trim())) { //$NON-NLS-1$
                trace(Trace.JAVADOC_DETECT_DEBUG,
                        "java.lang entry found in line ", s, " package-list is ", packagesListName); //$NON-NLS-1$//$NON-NLS-2$
                foundJavaLang = true;
            }
            if (!expandedJarEntries.contains(s.replace('.', '/'))) {
                found = false;
                break;
            }
            s = packagesListReader.readLine();
            found = true;

            if (mustHaveJavaLang && !foundJavaLang) {
                found = false;
            }
        }

        return found;
    }

    private URL getJavadocsURL(File javadocDirOrZip) throws MalformedURLException {
        if (javadocDirOrZip.isDirectory()) {
            return javadocDirOrZip.toURI().toURL();
        } else {
            try {
                return new URI("jar:" + javadocDirOrZip.toURI().toString() + "!/").toURL(); //$NON-NLS-1$ //$NON-NLS-2$
            } catch (URISyntaxException ex) {
                return null;
            } 
        }
    }
}
