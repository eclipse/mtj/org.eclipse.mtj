/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/

package org.eclipse.mtj.internal.core.sdk.device.midp;

import org.eclipse.mtj.internal.core.Messages;

public enum PackagingModel {
	
	MIDLET("MIDLET", Messages.packagingModel_MIDletSuite_text),
	LIBLET("LIBLET", Messages.packagingModel_Liblet_text);
	
	private String identifier;
    private String name;

    PackagingModel(String identifier, String name) {
    	this.identifier = identifier;
    	this.name = name;
    }
    
    public String getIdentifier() {
    	return identifier;
    }
    
    public String getName() {
    	return name;
    }
}
