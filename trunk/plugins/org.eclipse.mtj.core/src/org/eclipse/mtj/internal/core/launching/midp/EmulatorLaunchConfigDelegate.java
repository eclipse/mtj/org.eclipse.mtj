/**
 * Copyright (c) 2003,2013 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Gang Ma (Sybase) 	- Add check debugger setting support
 *     Gang Ma 	(Sybase) 	- Add check whether the MIDlet Suite has been 
 *     				  installed while launching in OTA mode
 *     David Marques (Motorola) - Avoiding null pointers.
 *     T. Bakardzhiev (Prosyst)  - Eclipse 4.2 jetty adjustments. 
 */
package org.eclipse.mtj.internal.core.launching.midp;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.eclipse.core.runtime.*;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.launching.AbstractJavaLaunchConfigurationDelegate;
import org.eclipse.jdt.launching.IVMRunner;
import org.eclipse.jdt.launching.VMRunnerConfiguration;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.midp.IApplicationDescriptor;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.launching.ILaunchConstants;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.sdk.device.AbstractMIDPDevice;
import org.eclipse.mtj.internal.core.sdk.device.JavaEmulatorDevice;
import org.eclipse.mtj.internal.core.util.ColonDelimitedProperties;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.osgi.util.NLS;

/**
 * Launch configuration for launching a Wireless Toolkit emulator that launches
 * as an executable rather than a Java class.
 * 
 * @author Craig Setera
 */
public class EmulatorLaunchConfigDelegate extends
        AbstractJavaLaunchConfigurationDelegate {
    private static final String EXE_EXT = ".exe"; //$NON-NLS-1$

    /**
     * Command of device
     */
    private static final String DEVICE_COMMAND = "-Xdevice:";

    /**
     * Command of listing all installed MIDlet suite in the emulator
     */
    private static final String LIST_INSTALLED_MIDLET_COMMAND = "-Xjam:list";

    /**
     * Status code for which the no MIDlet status is registered.
     */
    private static final IStatus NO_MIDLET_STATUS = new Status(IStatus.ERROR,
            IMTJCoreConstants.PLUGIN_ID, IMTJCoreConstants.ERR_OTA_NO_MIDLETS,
            "", null);

    /**
     * Command of removing specific installed MIDlet suite in the emulator
     */
    private static final String REMOVE_INSTALLED_MIDLET_COMMAND = "-Xjam:remove=";

    /**
     * Construct a new Executable emulator launch config delegate.
     */
    public EmulatorLaunchConfigDelegate() {
        super();
    }

    /* (non-Javadoc)
     * @see org.eclipse.debug.core.model.ILaunchConfigurationDelegate#launch(org.eclipse.debug.core.ILaunchConfiguration, java.lang.String, org.eclipse.debug.core.ILaunch, org.eclipse.core.runtime.IProgressMonitor)
     */
    public void launch(ILaunchConfiguration launchConfig, String mode,
            ILaunch launch, IProgressMonitor monitor) throws CoreException {
        // Make sure we have a progress monitor
        if (monitor == null) {
            monitor = new NullProgressMonitor();
        }

        // Get the platform definition and test that it isn't
        // the unspecified platform definition
        IDevice device = getDevice(launchConfig);
        if (device == null) {
            // The platform definition isn't available, so we can't
            // launch
            MTJStatusHandler.throwCoreException(IStatus.ERROR, -999,
                    "Device is unspecified or unavailable");
        }

        monitor.beginTask(NLS.bind(Messages.launchdelegate_launching,
                new Object[] { launchConfig.getName() }), 3);

        // check for cancellation
        if (monitor.isCanceled()) {
            return;
        }

        // Set up the VM runner
        monitor.subTask(Messages.launchdelegate_verifying_attrs);
        IVMRunner runner = getVMRunner(launchConfig, device, mode);

        // Create VM config
        VMRunnerConfiguration runConfig = getVMRunnerConfiguration(
                launchConfig, device, mode);

        // check for cancellation
        if (monitor.isCanceled()) {
            return;
        }

        // done the verification phase
        monitor.worked(1);

        // Handle the source locator support
        monitor.subTask(Messages.launchdelegate_source_locator);

        setDefaultSourceLocator(launch, launchConfig);
        monitor.worked(1);

        // Launch the configuration - 1 unit of work
        ((EmulatorRunner) runner).run(runConfig, launchConfig, launch, monitor);

        // check for cancellation
        if (!monitor.isCanceled()) {
            monitor.done();
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.launching.AbstractJavaLaunchConfigurationDelegate#preLaunchCheck(org.eclipse.debug.core.ILaunchConfiguration, java.lang.String, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public boolean preLaunchCheck(ILaunchConfiguration configuration,
            String mode, IProgressMonitor monitor) throws CoreException {
        boolean continueLaunch = super.preLaunchCheck(configuration, mode,
                monitor);
        if (continueLaunch) {
            continueLaunch = verifyEmulationSettings(configuration, mode,
                    monitor);
        }

        return continueLaunch;
    }

    /**
     * check if the suite has already installed in the emulator, if so, remove
     * it
     * 
     * @param desc
     * @param device
     * @throws CoreException
     */
    private void checkAndRemoveInstalledMidlet(IApplicationDescriptor desc,
            IDevice device) throws CoreException {
        if (!(device instanceof JavaEmulatorDevice)) {
            String midlet_name = desc.getManifestProperties().getProperty(
                    IJADConstants.JAD_MIDLET_NAME);
            String midlet_vendor_name = desc.getManifestProperties()
                    .getProperty(IJADConstants.JAD_MIDLET_VENDOR);
            AbstractMIDPDevice aDevice = (AbstractMIDPDevice) device;
            File executable = aDevice.getExecutable();
            String[] arguments = new String[] {
                    DEVICE_COMMAND + aDevice.getName(),
                    LIST_INSTALLED_MIDLET_COMMAND };

            String[] commandLine = populateCommandLine(executable, arguments);
            File workingDirectory = executable.getParentFile();
            String output = Utils
                    .getStandardOutput("get installed MIDlet Suite",
                            commandLine, workingDirectory);
            String regex = "\\[\\d+\\]";

            String[] suites = output.split(regex);
            // the MIDlet suite number
            int suitNum = 0;
            boolean installed = false;
            for (int i = 1; i < suites.length; i++) {
                String suiteInfo = suites[i];
                if (suiteInfo.trim().length() > 0) {
                    suitNum++;
                    ColonDelimitedProperties props = parseMidetSuiteInfo(suiteInfo);
                    String suiteName = props.getProperty("Name");
                    String vendorName = props.getProperty("Vendor");
                    if (midlet_name.equals(suiteName)
                            && midlet_vendor_name.equals(vendorName)) {
                        installed = true;
                        break;
                    }
                }

            }
            // if MIDlet suite has been installed , remove it
            if (installed) {
                arguments = new String[] { DEVICE_COMMAND + aDevice.getName(),
                        REMOVE_INSTALLED_MIDLET_COMMAND + suitNum };

                commandLine = populateCommandLine(executable, arguments);
                Utils.getStandardOutput("remove MIDlet Suite", commandLine,
                        workingDirectory);
            }

        }
    }

    /**
     * Return the device selected by the launch configuration.
     * 
     * @param launchConfig
     * @return
     * @throws CoreException
     */
    private IDevice getDevice(ILaunchConfiguration launchConfig)
            throws CoreException {
        IDevice device = null;

        if (launchConfig
                .getAttribute(ILaunchConstants.USE_PROJECT_DEVICE, true)) {
            device = getMidletSuite(launchConfig).getRuntimeList()
                    .getActiveMTJRuntime().getDevice();
        } else {
            String toolkitName = launchConfig.getAttribute(
                    ILaunchConstants.EMULATED_DEVICE_GROUP, "");
            String deviceName = launchConfig.getAttribute(
                    ILaunchConstants.EMULATED_DEVICE, "");

            try {
                device = MTJCore.getDeviceRegistry().getDevice(toolkitName,
                        deviceName);
            } catch (PersistenceException e) {
                MTJStatusHandler.throwCoreException(IStatus.ERROR, -999, e);
            }
        }

        return device;
    }

    /**
     * Return the MIDlet suite that is selected for this launch.
     * 
     * @param launchConfig
     * @return
     * @throws CoreException
     */
    private IMidletSuiteProject getMidletSuite(ILaunchConfiguration launchConfig)
            throws CoreException {
    	IMidletSuiteProject result = null;
        IJavaProject javaProject = getJavaProject(launchConfig);
        if (javaProject != null && javaProject.getProject().hasNature(IMTJCoreConstants.MTJ_NATURE_ID)) {
			result = MidletSuiteFactory.getMidletSuiteProject(javaProject);
		}
        return result;
    }

    /**
     * parse the installed suite message
     * 
     * @param suiteInfo
     * @return ColonDelimitedProperties
     */
    private ColonDelimitedProperties parseMidetSuiteInfo(String suiteInfo) {
        ColonDelimitedProperties props = new ColonDelimitedProperties();

        StringReader reader = new StringReader(suiteInfo);
        try {
            props.load(reader);
        } catch (Exception e) {
            MTJLogger.log(IStatus.WARNING, "Error parsing installed apps", e);
            MTJLogger.log(IStatus.WARNING, suiteInfo);
        }
        return props;
    }

    private String[] populateCommandLine(File executable, String[] arguments) {
        boolean isWin32 = Platform.getOS().equals(Platform.OS_WIN32);

        String command = executable.getAbsolutePath();
        boolean useCmdShell = isWin32 && !command.toLowerCase().endsWith(EXE_EXT);
        ArrayList<String> list = new ArrayList<String>();
        
        if (useCmdShell) {
        	list.add("cmd");
        	list.add("/c");
        	
            command = "\"" + executable.getName() + "\""; //$NON-NLS-1$ //$NON-NLS-2$
        }
        
        list.add(command);
        list.addAll(Arrays.asList(arguments));
        
        return list.toArray(new String[list.size()]);
    }

    /**
     * Return a boolean concerning whether the user wants to continue the
     * launch.
     * 
     * @return
     * @throws CoreException
     */
    private boolean promptWhetherToContinue(IStatus status)
            throws CoreException {
        Boolean shouldContinue = (Boolean) MTJStatusHandler.statusPrompt(status, this);
        return (shouldContinue != null) ? shouldContinue.booleanValue() : false;
    }

    /**
     * Verify the current emulation settings before launching. Return a boolean
     * indicating whether the specified settings are valid.
     * 
     * @param configuration
     * @param monitor
     * @return
     * @throws CoreException
     */
    private boolean verifyEmulationSettings(ILaunchConfiguration configuration,
            String mode, IProgressMonitor monitor) throws CoreException {
        boolean valid = true;
        // if in debug mode,check the debugger setting
        if (ILaunchManager.DEBUG_MODE.equals(mode)) {
            IStatus status = new Status(IStatus.INFO,
                    IMTJCoreConstants.PLUGIN_ID,
                    IMTJCoreConstants.INFO_DEBUGGER_SETTINGS_CHECK, "", null);
            valid = promptWhetherToContinue(status);
            if (!valid) {
                return valid;
            }
        }
        // If this is OTA, there should be at least one MIDlet
        // defined in the JAD file.
        boolean doOTA = configuration.getAttribute(IMIDPLaunchConstants.DO_OTA,
                false);
        if (doOTA) {
            IJavaProject javaProject = getJavaProject(configuration);
            if (javaProject != null) {
                IMidletSuiteProject suite = MidletSuiteFactory
                        .getMidletSuiteProject(javaProject);
                IApplicationDescriptor desc = suite.getApplicationDescriptor();
                if (desc != null) {
                    valid = (desc.getMidletCount() > 0);
                    if (!valid) {
                        valid = promptWhetherToContinue(NO_MIDLET_STATUS);
                        if (!valid) {
                            return valid;
                        }
                    }
                    // check if the suite has already installed in the emulator,
                    // if so, remove it
                    checkAndRemoveInstalledMidlet(desc, getDevice(configuration));
                }
            }

        }

        return valid;
    }

    /**
     * @param launchConfig
     * @param device
     * @param mode
     * @return
     * @throws CoreException
     * @see org.eclipse.mtj.core.launching.AbstractEmulatorLaunchConfigDelegate#getVMRunner(org.eclipse.debug.core.ILaunchConfiguration,
     *      java.lang.String)
     */
    protected IVMRunner getVMRunner(ILaunchConfiguration launchConfig,
            IDevice device, String mode) throws CoreException {
        // Set up the VM runner
        IMidletSuiteProject suite = getMidletSuite(launchConfig);
        return new EmulatorRunner(suite, device, mode);
    }

    /**
     * @param launchConfig
     * @param device
     * @param mode
     * @return
     * @throws CoreException
     * @see org.eclipse.mtj.core.launching.AbstractEmulatorLaunchConfigDelegate#getVMRunnerConfiguration(org.eclipse.debug.core.ILaunchConfiguration,
     *      java.lang.String)
     */
    protected VMRunnerConfiguration getVMRunnerConfiguration(
            ILaunchConfiguration launchConfig, IDevice device, String mode)
            throws CoreException {
        File workingDir = verifyWorkingDirectory(launchConfig);
        String workingDirName = null;
        if (workingDir != null) {
            workingDirName = workingDir.getAbsolutePath();
        }

        // Create VM config
        VMRunnerConfiguration runConfig = new VMRunnerConfiguration(device
                .getName(), new String[0]);
        runConfig.setVMArguments(new String[0]);
        runConfig.setWorkingDirectory(workingDirName);
        runConfig.setVMSpecificAttributesMap(new HashMap<String, Object>());

        return runConfig;
    }
}
