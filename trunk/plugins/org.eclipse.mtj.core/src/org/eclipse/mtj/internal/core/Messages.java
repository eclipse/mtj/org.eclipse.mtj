/**
 * Copyright (c) 2004,2009 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 *     Diego Sandin (Motorola)   - Adopt ICU4J into MTJ
 */
package org.eclipse.mtj.internal.core;

import org.eclipse.osgi.util.NLS;

public final class Messages extends NLS {

    public static String OffsetedStackTraceParser_getClassNode_unableToFindClassFile;
    public static String OffsetedStackTraceParser_getClassNode_unableToRealizeMethodNode;
    public static String OffsetedStackTraceParser_getClassNode_unableToRealizeMidletSuiteProject;
    public static String OffsetedStackTraceParser_getClassNode_unableToRealizeTargetMethod;
    public static String OffsetedStackTraceParser_getClassNode_verifiedFolderDoesNotExist;
    public static String SignatureUtils_noAliasSelected;
	public static String SignatureUtils_noKeystoreConfigured;
	public static String SignatureUtils_passwordDialogTitle;
	public static String StackTraceParser_getStackBaseMethod_method_not_found;
    
    public static String StateMachine_invalidInitialState;
	public static String StateMachine_machineAlreadyStarted;
	public static String StateMachine_machineNotYetStarted;
	public static String StateMachine_noInitialState;
	public static String StateMachine_nullInitialState;
	public static String AbstractCreateAntTaskState_documentNotNull;
	public static String AbstractCreateAntTaskState_projectCanNotBeNull;
	public static String AbstractCreateAntTaskState_stateMachineNotNull;
	public static String AbstractJavaTool_delegateNotAvailable;
    public static String AbstractState_conflictingTransitions;
	public static String AbstractState_invalidInitialState;
	public static String AbstractState_invalidSourceState;
	public static String AbstractState_noSourceState;
	public static String AbstractState_noTargetState;
	public static String AbstractState_stateNotNull;
	public static String AbstractState_transitionNotNull;
	public static String AbstractStateTransition_sourceStateIsNull;
	public static String AbstractStateTransition_targetStateIsNull;
	public static String AntennaBuildExporter_Antennalibrary_not_found;
    public static String AntennaBuildExporter_buildProperties_comment;
    public static String AntennaBuildExporter_file_is_invalid;
    public static String AntennaBuildExporter_template_notfound;
    public static String AntennaBuildExporter_unresolved_classpath;
    public static String AntennaBuildExporter_warning_comment;
    public static String AntennaBuildExporter_WTK_not_found;
    public static String BuildLoggingConfiguration_unknown_configuration_error;
	public static String BuildPropertiesChange_changeMessage;
	public static String BuildPropertiesMoveParticipant_buildPropertiesChangeMessage;
	public static String BuildPropertiesMoveParticipant_buildPropertiesMoveParticipantName;
	public static String BuildPropertiesPackageRenameParticipant_buildPropertiesChangeMessage;
	public static String BuildPropertiesPackageRenameParticipant_buildPropertiesPackageRenamePaticipant;
	public static String BuildPropertiesRenameParticipant_buildPropertiesChangeMessage;
	public static String BuildPropertiesRenameParticipant_buildPropertiesParticipantName;
	public static String CopyPreverifier_UnsupportedClassFormat;
    public static String CopyPreverifier_UnsupportedClassFormatJAR;
    public static String CreateAntPreprocessTaskState_UnableToCreateSymbolSet;
    
    public static String debugvmrunner_constructing_cmd_line;
    public static String debugvmrunner_couldnt_connect_to_vm;
    public static String debugvmrunner_debug_target_string;
    public static String debugvmrunner_establishing_debug_conn;
    public static String debugvmrunner_exe_doesnt_exist;
    public static String debugvmrunner_finding_free_socket;
    public static String debugvmrunner_launching_vm;
    public static String debugvmrunner_no_connector;
    public static String debugvmrunner_no_free_socket;
    public static String debugvmrunner_process_label_string;
    public static String debugvmrunner_starting_VM;
    public static String debugvmrunner_unable_to_locate_exe;
    public static String debugvmrunner_workingdir_not_dir;

    public static String DefaultKeyStoreManager_22;
	public static String DefaultKeyStoreManager_certificateInfoFormat;
	public static String DefaultKeyStoreManager_InvalidKeyPassword;
	public static String DefaultKeyStoreManager_invalidKeystoreFile;
	public static String DefaultKeyStoreManager_invalidKsPassword;
	public static String DefaultKeyStoreManager_keyStoreNotNull;
	public static String DefaultKeyStoreManager_keystorePasswordEmpty;
	public static String DefaultKeyStoreManager_keystorePasswordNotNull;
	public static String DefaultKeyStoreManager_unableToAccessKeystoreContent;
	public static String DefaultKeyStoreManager_unableToCreateMD5Fingerprint;
	public static String DefaultKeyStoreManager_unableToGetEncodedCertificate;
	public static String DefaultKeyStoreManager_unableToOpenKeystore;
	public static String DeployedJADWriter_unableToResolveJadName;
    public static String DeviceFinder_0;
    public static String DeviceFinder_1;
    public static String DeviceFinder_2;
    public static String DeviceFinder_3;
    public static String EmulatorRunner_4;
    public static String EmulatorRunner_6;
	public static String error_midp_prefs_not_set;

    public static String JavaEmulatorDeviceImporter_0;

    public static String JavaMEClasspathContainerInitializer_Could_not_load_JavaMEClasspath;
	public static String KeyChainSet_noAliasSelected;

    public static String launchdelegate_launching;
    public static String launchdelegate_no_debug;
    public static String launchdelegate_no_run;
    public static String launchdelegate_source_locator;
    public static String launchdelegate_verifying_attrs;
    public static String L10nLocales_duplicatedLocale;
    public static String LibraryImporterRegistry_0;
    public static String LibraryImporterRegistry_1;

    public static String MidletSuiteProject_device_not_available;
    public static String MidletSuiteProject_preverifier_missing_default;
    public static String MidletSuiteProject_preverifier_missing_device;

    public static String MTJBuildProperties_invalidMethodAccess;
	public static String MTJBuildProperties_invalidMethodCall;
	public static String MTJBuildProperties_invalidMTJProject;
	public static String MTJBuildProperties_mtjBuildPropertiesComment;
	public static String MTJCore_MigrationJob_title;
    public static String MTJCore_no_message;
    public static String MTJCore_no_message2;
    public static String MTJCore_OK_STATUS_msg;

    public static String MTJCoreError_10001;
    public static String MTJCoreError_10002;
    public static String MTJCoreError_10003;
    public static String MTJCoreError_10004;
    public static String MTJCoreError_10005;
    public static String MTJCoreError_10006;
    public static String MTJCoreError_10007;
    public static String MTJCoreError_10008;
    public static String MTJCoreError_10009;
    public static String MTJCoreError_10010;
    public static String MTJCoreError_10011;
    public static String MTJCoreError_10012;
    public static String MTJCoreError_10013;
    public static String MTJCoreError_10014;
    public static String MTJCoreError_Default;
    public static String MTJCoreError_InternalTemplate;

    public static String MTJProjectConverter_convert_taskname;
    public static String MTJProjectConverter_convertProject_convertion_error;
    public static String MTJProjectConverter_convertProject_device_unavailable;
    public static String MTJProjectConverter_modifyMetadata_exception;
    public static String MTJProjectConverter_modifyMetadata_taskname;
    public static String MTJProjectConverter_removeBuilderAndNature_taskname;
    public static String MTJProjectConverter_renameEclipseMETmpFolder_taskname;
    public static String MTJProjectConverter_renamePreferenceStoreFile_taskname;
    public static String NewAntennaBuildExport_comment;
	public static String NewAntennaBuildExport_InvalidSuiteProject;
	public static String NewAntennaBuildExport_UnableToCreateXMLDoc;
	public static String PreferencesSignatureProperties_unableToLoadPropertiesFromPreferences;
	public static String PreprocessorBuilder_cleaningProcessedFolder;
	public static String PreprocessorBuilder_collectingSources;
	public static String PreprocessorBuilder_preprocessingSourceFiles;
	public static String PreprocessorBuilder_preprocessingSources;
	public static String PreverificationErrorLocationType_Class_declaration;
    public static String PreverificationErrorLocationType_Class_field;
    public static String PreverificationErrorLocationType_Method_definition;
    public static String PreverificationErrorLocationType_Method_field;
    public static String PreverificationErrorLocationType_Method_instruction;
    public static String PreverificationErrorLocationType_Unknown_location;

    public static String PreverificationErrorType_finalizers;
    public static String PreverificationErrorType_float;
    public static String PreverificationErrorType_miss_class_def;
    public static String PreverificationErrorType_native;
    public static String PreverificationErrorType_no_error;
    public static String PreverificationErrorType_unknown;
    public static String SymbolDefinitionSetRegistry_addDefinitionSet_no_name_exception;

    public static String Utils_commandLine;
    public static String Utils_commandLine_not_found;
    public static String Utils_xml_parse_error;
    public static String XMLPersistenceProvider_0;
    public static String XMLPersistenceProvider_1;
    public static String XMLPersistenceProvider_8;
    
    public static String L10nLocale_0;
    public static String L10nLocale_2;
    public static String L10nLocale_4;
    public static String L10nLocale_5;
    public static String L10nLocale_7;

    public static String SdkProvider_ImportedSdkProviderName;
    
    public static String packagingModel_MIDletSuite_text;
    public static String packagingModel_Liblet_text;
    
    private static final String BUNDLE_NAME = "org.eclipse.mtj.internal.core.messages";//$NON-NLS-1$

    static {
        NLS.initializeMessages(BUNDLE_NAME, Messages.class);
    }

    private Messages() {
        // Do not instantiate
    }
}