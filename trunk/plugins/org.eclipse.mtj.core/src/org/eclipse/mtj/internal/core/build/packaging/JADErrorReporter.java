package org.eclipse.mtj.internal.core.build.packaging;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.filebuffers.ITextFileBufferManager;
import org.eclipse.core.filebuffers.LocationKind;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.preverifier.builder.BuilderMessages;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.osgi.util.NLS;



public class JADErrorReporter {
	
	private IFile descriptorFile;
	protected IDocument textDocument;
	
	public JADErrorReporter( IFile file){
		descriptorFile = file;
		textDocument = createDocument(file);
	}
	
	
	public void validate(IProgressMonitor monitor){
		try {
			descriptorFile.deleteMarkers(IMTJCoreConstants.JAD_PROBLEM_MARKER, true, IResource.DEPTH_ZERO );
		} catch (CoreException e) {
			MTJCore.getMTJCore()
			.getLog()
			.log(new Status(IStatus.WARNING, MTJCore.getPluginId(),
					"Error validating JAD", e)); //$NON-NLS-1$		
			}
		parseJAD(textDocument, monitor);
		
	}
	
	private void parseJAD(IDocument document, IProgressMonitor monitor){
		try {
			String header=null;
			HashMap<String, String> headers = new HashMap<String, String>();
			int l = 0;
			for (; l < document.getNumberOfLines(); l++) {
				IRegion lineInfo = document.getLineInformation(l);
				String line = document.get(lineInfo.getOffset(),
						lineInfo.getLength());
				// test lines' length
				Charset charset = Charset.forName("UTF-8"); //$NON-NLS-1$
				String lineDelimiter = document.getLineDelimiter(l);
				if (lineDelimiter == null) {
					lineDelimiter = ""; //$NON-NLS-1$
				}
				ByteBuffer byteBuf = charset.encode(line);
				if (byteBuf.limit() + lineDelimiter.length() > 512) {
					addMarker(BuilderMessages.JADError_too_long, l+1,  IMarker.SEVERITY_ERROR);
				}
				
				if (line.length() == 0) {
					// Empty Line
					if (l == 0) {
						addMarker(BuilderMessages.JADError_empty_line, 1, IMarker.SEVERITY_ERROR);
						return;
					}
					if(header != null ){
						headers.put(header.toLowerCase(), header);
						header = null;
					}
					break;
				}
				// Expecting New Header save old one
				if (header != null) {
					headers.put(header.toLowerCase(), header);
					header = null;
				}
				
				int colon = line.indexOf(':');
				if (colon == -1) { /* no colon */
					addMarker(BuilderMessages.JADError_no_colon, l+1, IMarker.SEVERITY_ERROR);
					return;
				}
				
				String headerName = getHeaderName(line);
				if (headerName == null) {
					addMarker(BuilderMessages.JADError_invalid_attrib, l+1, IMarker.SEVERITY_ERROR);
					return;
				}
				header = headerName;
				if (headers.containsKey(header.toLowerCase())) {
					addMarker(BuilderMessages.JADError_duplicate_attib, l+1, IMarker.SEVERITY_WARNING);
				}
				
			}
		    
		    //Check mandatory JAD attributes
			// MIDlet-Jar-Size is not checked as it is filled in by the package builder
			if(!attributeExists(headers, IJADConstants.JAD_MIDLET_NAME) && !attributeExists(headers, IJADConstants.JAD_LIBLET_NAME)){
				addMarker( NLS.bind(BuilderMessages.JADError_missing_mandatory, IJADConstants.JAD_MIDLET_NAME), 1, IMarker.SEVERITY_ERROR);
				return;
			} else if (attributeExists(headers, IJADConstants.JAD_MIDLET_NAME) && attributeExists(headers, IJADConstants.JAD_LIBLET_NAME)) {
				addMarker( NLS.bind(BuilderMessages.JADError_not_conform_to_packaging_model, IJADConstants.JAD_LIBLET_NAME), 1, IMarker.SEVERITY_ERROR);
				return;
			} else if (!attributeExists(headers, IJADConstants.JAD_MIDLET_NAME) && attributeExists(headers, IJADConstants.JAD_LIBLET_NAME)) {
				if(!attributeExists(headers, IJADConstants.JAD_LIBLET_VENDOR)){
					addMarker( NLS.bind(BuilderMessages.JADError_missing_mandatory, IJADConstants.JAD_LIBLET_VENDOR), 1, IMarker.SEVERITY_ERROR);
					return;
				}
				if(!attributeExists(headers, IJADConstants.JAD_LIBLET_VERSION)){
					addMarker( NLS.bind(BuilderMessages.JADError_missing_mandatory, IJADConstants.JAD_LIBLET_VERSION), 1, IMarker.SEVERITY_ERROR);
					return;
				}
				if(!attributeExists(headers, IJADConstants.JAD_LIBLET_JAR_URL)){
					addMarker( NLS.bind(BuilderMessages.JADError_missing_mandatory, IJADConstants.JAD_LIBLET_JAR_URL), 1, IMarker.SEVERITY_ERROR);
					return;
				}
				if(attributeExists(headers, IJADConstants.JAD_MIDLET_VENDOR)){
				    addMarker( NLS.bind(BuilderMessages.JADError_not_conform_to_packaging_model, IJADConstants.JAD_MIDLET_VENDOR), 1, IMarker.SEVERITY_ERROR);
				    return;
			    }
			    if(attributeExists(headers, IJADConstants.JAD_MIDLET_VERSION)){
				    addMarker( NLS.bind(BuilderMessages.JADError_not_conform_to_packaging_model, IJADConstants.JAD_MIDLET_VERSION), 1, IMarker.SEVERITY_ERROR);
				    return;
			    }
			    if(attributeExists(headers, IJADConstants.JAD_MIDLET_JAR_URL)){
				    addMarker( NLS.bind(BuilderMessages.JADError_not_conform_to_packaging_model, IJADConstants.JAD_MIDLET_JAR_URL), 1, IMarker.SEVERITY_ERROR);
				    return;
			    }
			} else {			
			    if(!attributeExists(headers, IJADConstants.JAD_MIDLET_VENDOR)){
				    addMarker( NLS.bind(BuilderMessages.JADError_missing_mandatory, IJADConstants.JAD_MIDLET_VENDOR), 1, IMarker.SEVERITY_ERROR);
				    return;
			    }
			    if(!attributeExists(headers, IJADConstants.JAD_MIDLET_VERSION)){
				    addMarker( NLS.bind(BuilderMessages.JADError_missing_mandatory, IJADConstants.JAD_MIDLET_VERSION), 1, IMarker.SEVERITY_ERROR);
				    return;
			    }
			    if(!attributeExists(headers, IJADConstants.JAD_MIDLET_JAR_URL)){
				    addMarker( NLS.bind(BuilderMessages.JADError_missing_mandatory, IJADConstants.JAD_MIDLET_JAR_URL), 1, IMarker.SEVERITY_ERROR);
				    return;
			    }
			    if(attributeExists(headers, IJADConstants.JAD_LIBLET_VENDOR)){
					addMarker( NLS.bind(BuilderMessages.JADError_not_conform_to_packaging_model, IJADConstants.JAD_LIBLET_VENDOR), 1, IMarker.SEVERITY_ERROR);
					return;
				}
				if(attributeExists(headers, IJADConstants.JAD_LIBLET_VERSION)){
					addMarker( NLS.bind(BuilderMessages.JADError_not_conform_to_packaging_model, IJADConstants.JAD_LIBLET_VERSION), 1, IMarker.SEVERITY_ERROR);
					return;
				}
				if(attributeExists(headers, IJADConstants.JAD_LIBLET_JAR_URL)){
					addMarker( NLS.bind(BuilderMessages.JADError_not_conform_to_packaging_model, IJADConstants.JAD_LIBLET_JAR_URL), 1, IMarker.SEVERITY_ERROR);
					return;
				}
			}
			
			if(!attributeExists(headers, IJADConstants.JAD_MICROEDITION_CONFIG)){
				addMarker( NLS.bind(BuilderMessages.JADError_missing_mandatory, IJADConstants.JAD_MICROEDITION_CONFIG), 1, IMarker.SEVERITY_ERROR);
				return;
			}
			if(!attributeExists(headers, IJADConstants.JAD_MICROEDITION_PROFILE)){
				addMarker( NLS.bind(BuilderMessages.JADError_missing_mandatory, IJADConstants.JAD_MICROEDITION_PROFILE), 1, IMarker.SEVERITY_ERROR);
				return;
			}			
						
		}catch (BadLocationException e) {
			MTJCore.getMTJCore().getLog().log( new Status(IStatus.WARNING, MTJCore.getPluginId(),"Error parsing JAD file",e)); //$NON-NLS-1$
		}
		
	}
	
	
	private boolean attributeExists(Map<String,String> headers, String attribute){
		if(!headers.containsKey(attribute.toLowerCase())){
			return false;
		}
		return true;
	}
	
	private String getHeaderName(String line) {
		for (int i = 0; i < line.length(); i++) {
			char c = line.charAt(i);
			if (c == ':') {
				return line.substring(0, i);
			}
			if ((c < 'A' || 'Z' < c) && (c < 'a' || 'z' < c) && (c < '0' || '9' < c)) {
				if (i == 0) {
					return null;
				}
				if (c != '-' && c != '_') {
					return null;
				}
			}
		}
		return null;
	}
	
	
	private void addMarker(String message, int lineNumber, int severity) {
		try {

			IMarker marker = descriptorFile
					.createMarker(IMTJCoreConstants.JAD_PROBLEM_MARKER);

			marker.setAttribute(IMarker.MESSAGE, message);
			marker.setAttribute(IMarker.SEVERITY, severity);
			if (lineNumber == -1)
				lineNumber = 1;
			marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
			
		} catch (CoreException e) {
			MTJCore.getMTJCore()
					.getLog()
					.log(new Status(IStatus.WARNING, MTJCore.getPluginId(),
							"Error creating marker for JAD file", e)); //$NON-NLS-1$
		}

	}
	
	private IDocument createDocument(IFile file) {
		if (!file.exists()) {
			return null;
		}
		ITextFileBufferManager manager = FileBuffers.getTextFileBufferManager();
		if (manager == null) {
			return null;
		}
		try {
			manager.connect(file.getFullPath(), LocationKind.NORMALIZE, null);
			ITextFileBuffer textBuf = manager.getTextFileBuffer(file.getFullPath(), LocationKind.NORMALIZE);
			IDocument document = textBuf.getDocument();
			manager.disconnect(file.getFullPath(), LocationKind.NORMALIZE, null);
			return document;
		} catch (CoreException e) {
			MTJCore.getMTJCore().getLog().log( new Status(IStatus.WARNING, MTJCore.getPluginId(),"Error creating document for JAD file",e)); //$NON-NLS-1$
		}
		return null;
	}

}
