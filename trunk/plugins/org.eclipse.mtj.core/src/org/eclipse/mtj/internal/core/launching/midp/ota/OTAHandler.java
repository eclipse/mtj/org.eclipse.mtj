/**
 * Copyright (c) 2003, 2013 Craig Setera and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards. Added serialVersionUID.
 *     Diego Sandin (Motorola)  - Adapting code to use Jetty version 5.1.11
 *     Diego Sandin (Motorola)  - Fix problem with OTA deployment after multiple
 *                                MTJRuntime feature inclusion.
 *     Diego Sandin (Motorola)  - Fix problem with OTA deployment.
 *     Gorkem Ercan (Nokia)     - [281658]Move Jetty dependency to the latest
 *     T. Bakardzhiev (Prosyst)  - Eclipse 4.2 jetty adjustments. 
 */
package org.eclipse.mtj.internal.core.launching.midp.ota;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.jetty.http.MimeTypes;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;

/**
 * Jetty HttpHandler implementation for the OTA implementation.
 *
 * @author Craig Setera
 */
public class OTAHandler extends AbstractHandler {

    private static final String OTA_CONTEXT_PATH = "/ota"; //$NON-NLS-1$

	// Internal map of the known MIME types
    private static final String[] KNOWN_MIME_TYPES = new String[] { "jad", //$NON-NLS-1$
            "text/vnd.sun.j2me.app-descriptor", "jar", //$NON-NLS-1$ //$NON-NLS-2$
            "application/java-archive" }; //$NON-NLS-1$

    /**
     * Internal map of file extensions to MIME types.
     */
    private static Map<String, String> mimeTypeMap;

    /**
     * Default serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * @return
     */
    private static Map<String, String> getMimeTypeMap() {
        if (mimeTypeMap == null) {
            mimeTypeMap = new HashMap<String, String>();

            for (int i = 0; i < KNOWN_MIME_TYPES.length;) {
                mimeTypeMap.put(KNOWN_MIME_TYPES[i++], KNOWN_MIME_TYPES[i++]);
            }
        }

        return mimeTypeMap;
    }

    /**
     * Default constructor
     */
    public OTAHandler() {
    	super();
    }

    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        String project = null;
        String file = null;

        StringTokenizer st = new StringTokenizer(target, "/"); //$NON-NLS-1$
        if (st.hasMoreTokens()) {
            project = st.nextToken();
        }
        if (st.hasMoreTokens()) {
            file = st.nextToken();
        }

        if ((project != null) && (file != null)) {
            try {
                handle(project, file, target, request, response);
            } catch (CoreException e) {
                throw new IOException(e.getMessage());
            }
        }
    }

    /**
     * Get the appropriate content encoding for the specified file.
     *
     * @param requestedFile
     * @return
     */
    private String getContentEncoding(IFile requestedFile) {
        return (requestedFile.getFileExtension().equals("jad")) ? "UTF-8" //$NON-NLS-1$ //$NON-NLS-2$
                : null;
    }

    /**
     * @param requestedFile
     * @return
     */
    private String getContentType(IFile requestedFile) {
        String contentType = getMimeTypeMap().get(
                requestedFile.getFileExtension());

        return (contentType != null) ? contentType : "application/octet-stream"; //$NON-NLS-1$
    }

    /**
     * Return the IJavaProject for the specified name or <code>null</code> if no
     * such project is found.
     *
     * @param projectName
     * @return
     */
    private IJavaProject getJavaProject(String projectName) {
        IJavaProject javaProject = null;

        IWorkspaceRoot root = MTJCore.getWorkspace().getRoot();
        IProject project = root.getProject(projectName);

        if (project != null) {
            javaProject = JavaCore.create(project);
        }

        return javaProject;
    }

    /**
     * Handle sending the specified file in the specified Eclipse Java Project.
     *
     * This handle tries to find the jad file in the current active MTJRuntime
     * deploy folder.
     *
     * @param javaProject the project from where the jad file must be searched
     * @param file the jad file name
     * @param pathParams
     * @param request
     * @param response
     * @throws CoreException
     * @throws IOException
     */
    private void handle(IJavaProject javaProject, String file,
            String pathParams, HttpServletRequest request, HttpServletResponse response)
            throws CoreException, IOException {
        String deployableJadFolderName = IMTJCoreConstants.TEMP_FOLDER_NAME
                + File.separator + IMTJCoreConstants.EMULATION_FOLDER_NAME;
        IFolder deployedFolder = javaProject.getProject().getFolder(
                deployableJadFolderName);

        if (deployedFolder.exists()) {
            IFile requestedFile = deployedFolder.getFile(file);

            if (requestedFile.exists()) {
                sendFile(requestedFile, response);
            }
        }
    }

    /**
     * Handle sending the specified file in the specified Eclipse project.
     *
     * @param project
     * @param file
     * @param pathParams
     * @param request
     * @param response
     * @throws CoreException
     * @throws IOException
     */
    private void handle(String project, String file, String pathParams,
            HttpServletRequest request, HttpServletResponse response) throws CoreException,
            IOException {
        IJavaProject javaProject = getJavaProject(project);
        if (javaProject != null) {
            handle(javaProject, file, pathParams, request, response);
        }
    }

    /**
     * Send the specified file back to the caller.
     *
     * @param requestedFile
     * @param response
     * @throws CoreException
     * @throws IOException
     */
    private void sendFile(IFile requestedFile, HttpServletResponse response)
            throws CoreException, IOException {
        String contentType = getContentType(requestedFile);
        String contentEncoding = getContentEncoding(requestedFile);

        File javaFile = requestedFile.getLocation().toFile();

        response.setCharacterEncoding(contentEncoding);
        response.setContentType(contentType);
        response.setContentLength((int) javaFile.length());

        InputStream is = requestedFile.getContents();
        OutputStream os = response.getOutputStream();
        Utils.copyInputToOutput(is, os);
        os.close();
        is.close();

        response.flushBuffer();
    }

    static ContextHandler getContextHandler(){
        ContextHandler ctxHandler = new ContextHandler(OTAHandler.OTA_CONTEXT_PATH);
        ctxHandler.setHandler(new OTAHandler());
        MimeTypes types =new MimeTypes();
        types.setMimeMap(getMimeTypeMap());
        ctxHandler.setMimeTypes(types);
        return ctxHandler;

    }

}
