/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 */
package org.eclipse.mtj.internal.core.build;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.eclipse.debug.core.IStreamListener;
import org.eclipse.debug.core.model.IStreamMonitor;

/**
 * The proxied implementation of {@link IBuildConsoleProxy}.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @author Craig Setera
 */
public class BuildConsoleProxy implements IBuildConsoleProxy {

    /**
     * A dummy output stream that accepts output bytes and sends them to
     * nowhere.
     */
    private static class NullOutputStream extends OutputStream {

        /* (non-Javadoc)
         * @see java.io.OutputStream#write(int)
         */
        @Override
        public void write(int arg0) throws IOException {
        }
    }

    private static final BuildConsoleProxy instance = new BuildConsoleProxy();

    private static NullOutputStream nullStream = new NullOutputStream();

    /**
     * Access method to the unique instance of BuildConsoleProxy.
     * 
     * @return the BuildConsoleProxy unique instance.
     */
    public static synchronized BuildConsoleProxy getInstance() {
        return instance;
    }

    private IBuildConsoleProxy proxy;
    private PrintWriter traceWriter;

    private BuildConsoleProxy() {
    }

    /**
     * Add a new listener to the specified console stream.
     * 
     * @param id
     * @param monitor
     */
    public void addConsoleStreamListener(Stream id, IStreamMonitor monitor) {
        final PrintWriter writer = getConsoleWriter(id);
        IStreamListener listener = new IStreamListener() {
            public void streamAppended(String text, IStreamMonitor monitor) {
                writer.print(text);
                writer.flush();
            }
        };
        
        synchronized (monitor) {
            String collected = monitor.getContents();
            
            if (collected != null) {
                listener.streamAppended(collected, monitor);
            }
            
            monitor.addListener(listener);
        }

        writer.print(monitor.getContents());
        writer.flush();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.build.IBuildConsoleProxy#getConsoleWriter(org.eclipse.mtj.core.build.IBuildConsoleProxy.Stream)
     */
    public PrintWriter getConsoleWriter(Stream id) {
        return (proxy == null) ? new PrintWriter(nullStream) : proxy
                .getConsoleWriter(id);
    }

    /**
     * Set the proxy being wrapped by this proxy.
     * 
     * @param proxy
     */
    public void setProxy(IBuildConsoleProxy proxy) {
        this.proxy = proxy;
        traceWriter = null;
    }

    /**
     * Print the specified information to the log output stream.
     * 
     * @param text
     */
    public void trace(String text) {
        if (traceWriter == null) {
            traceWriter = getConsoleWriter(Stream.TRACE);
        }

        traceWriter.print(text);
        traceWriter.flush();
    }

    /**
     * Print the specified information to the log output stream.
     * 
     * @param text
     */
    public void traceln(String text) {
        trace(text + "\n"); //$NON-NLS-1$
    }
}
