/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Fixing path problems.
 *     David Marques (Motorola) - Handling non public outter classes.
 *     David Marques (Motorola) - Refactoring to follow PDE build.properties.
 *     David Marques (Motorola) - Fixing some bugs.
 */
package org.eclipse.mtj.internal.core.build.packaging;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.internal.core.build.MTJBuildProperties;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.osgi.util.NLS;

import de.schlichtherle.io.File;

/**
 * PackageBuilderVisitor class implements both {@link IResourceDeltaVisitor} and
 * {@link IResourceVisitor} in order to allow collecting resources from either
 * an {@link IResourceDelta} or an {@link IResource}. The resources collected
 * must be in the build properties file if any in order to be packaged. It
 * collects both classes and non class resources within the project's source
 * folders.
 * 
 * @author David Marques
 */
public class PackageBuilderVisitor implements IResourceVisitor,
		IResourceDeltaVisitor {
	private IMidletSuiteProject suiteProject;
	private MTJBuildProperties properties;
	private List<IFile> resources;
	private LinkedHashSet<IFile> classes;
	private File jar;

	/**
	 * Creates a PackageBuilderVisitor to collect resources from the specified
	 * {@link IMidletSuiteProject} instance in order to package resources into
	 * the specified JAR.
	 * 
	 * @param suiteProject
	 *            source project.
	 * @param jarFile
	 *            target JAR file.
	 */
	public PackageBuilderVisitor(IMidletSuiteProject suiteProject, File jarFile) {
		this.resources = new ArrayList<IFile>();
		this.classes = new LinkedHashSet<IFile>();
		this.properties = MTJBuildProperties.getBuildProperties(suiteProject);
		this.suiteProject = suiteProject;
		this.jar = jarFile;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.resources.IResourceVisitor#visit(org.eclipse.core.resources
	 * .IResource)
	 */
	public boolean visit(IResource resource) throws CoreException {
		if (resource instanceof IFile) {
			IFile file = getOutputFileCandidate((IFile) resource);
			if (isOnBuildFolder(file)) {
				if (file.getName().endsWith(".class")) { //$NON-NLS-1$
					IFile preverified = getPreVerifiedClassFile(file);
					if (preverified != null) {
						if (isOnBuildProperties(file, true)) {
							this.classes.add(preverified);
						} else {
							this.removeFromJAR(preverified);
						}
					}
				} else {
					if (isOnBuildProperties(resource, false)) {
						this.resources.add(file);
					} else {
						this.removeFromJAR(file);
					}
				}
			}
		}
		return true;
	}
	
	private IFile getOutputFileCandidate(IFile file) {
	    IPath verifiedPath = suiteProject.getVerifiedClassesOutputFolder(new NullProgressMonitor()).getFullPath();
	    IPath filePath = file.getFullPath();
	    int commonSegnmentCount = filePath.matchingFirstSegments(verifiedPath);

        if (commonSegnmentCount == verifiedPath.segmentCount()) {
            IPath relativeFilePath = filePath.removeFirstSegments(commonSegnmentCount);
            List<IPath> outputLocations = Utils.getOutputLocations(suiteProject, false);
            
            for (IPath outputLocation : outputLocations) {
                IFile candidate = suiteProject.getProject().getWorkspace().getRoot().getFile(outputLocation.append(relativeFilePath));
                
                if (candidate != null && candidate.exists()) {
                    return candidate;
                }
            }
        }

        return file;
	}

	/**
	 * Checks whether the {@link IFile} instance is on the build folder.
	 * 
	 * @param file
	 *            target file.
	 * @return true if on build folder, false otherwise.
	 * @throws JavaModelException
	 *             if element does not exist.
	 */
	private boolean isOnBuildFolder(IFile file) throws JavaModelException {
	    List<IPath> outputLocations = Utils.getOutputLocations(suiteProject, false);
	    
        return Utils.makePathRelativeToOutputLocation(file.getFullPath(), outputLocations, null) != null;
	}

	/**
	 * Checks whether a resource is listed on the build.properties file in order
	 * to be packaged.
	 * 
	 * @param resource
	 *            target resource.
	 * @param isClassFile
	 *            true if class file.
	 * @return true if listed false otherwise.
	 * @throws CoreException
	 *             Any core error occurred.
	 */
	private boolean isOnBuildProperties(IResource resource, boolean isClassFile)
			throws CoreException {
		MTJRuntimeList runtimes = this.suiteProject.getRuntimeList();
		MTJRuntime active = runtimes.getActiveMTJRuntime();
		boolean result = false;
		if (active != null) {
			IPath resourcePath = getOutputFolderRelativePath(resource);
			if (!resourcePath.isEmpty()) {
				resourcePath = findResourceOriginalPath(resourcePath, isClassFile);
				if (!resourcePath.isEmpty()) {					
					result = isResourceFileOnBuildProperties(active, resourcePath);
				}
			}
		}
		return result;
	}

    private IJavaElement findSourceElement(IPath classFilePath) throws CoreException {
        // Try to find after indexing is completed.
        // E.g. after import a project some classes that are not indexed yet cannot be found via findElement method,
        // and as the result the corrupted JAR file can be produced
        String extension = classFilePath.getFileExtension();
        IPath packagePath = classFilePath.removeLastSegments(1);
        String packageName = packagePath.toString().replace(IPath.SEPARATOR, '.'); //$NON-NLS-1$
        String typeName = classFilePath.lastSegment();
        String qualifiedName = null;

        typeName = typeName.substring(0, typeName.length() - extension.length() - 1);

        if (packageName.length() > 0) {
            qualifiedName = packageName + "." + typeName; //$NON-NLS-1$
        } else {
            qualifiedName = typeName;
        }

        IType type = suiteProject.getJavaProject().findType(qualifiedName, (IProgressMonitor) null);

        return type == null ? null : type.getParent();
    }
	
	/**
	 * Gets the source path of the resource within the build folder.
	 * 
	 * @param resourcePath build folder relative resource's path.
	 * @param isClassFile true if it is a class file.
	 * @return the source path of the resource.
	 * @throws CoreException Any core error occurs.
	 */
	private IPath findResourceOriginalPath(IPath resourcePath, boolean isClassFile) throws CoreException {
		IPath result = Path.EMPTY;
		IResource[] sources = Utils.getSourceFolders(this.suiteProject.getJavaProject());
		
		if (isClassFile) {
			IJavaElement jElement = findSourceElement(getOutterClassName(resourcePath));
			
			if (jElement != null) {					
				String packageName = getPackageName(jElement);
				String sourceName  = jElement.getElementName();
				resourcePath = new Path(packageName).append(sourceName);
			}
		}
		
		for (IResource source : sources) {
			IContainer container = (IContainer)source.getAdapter(IContainer.class);
			if (container != null) {
				IResource resource =  container.findMember(resourcePath);
				if (resource != null) {
					result = resource.getFullPath();
				}
			}
		}
		return result;
	}

	/**
	 * Checks whether the resource is on the build.properties file.
	 * 
	 * @param active active runtime configuration.
	 * @param resourcePath target resource.
	 * @return true if included false if not.
	 * @throws CoreException Any core error occurs.
	 */
	private boolean isResourceFileOnBuildProperties(MTJRuntime active, IPath resourcePath) throws CoreException {
		boolean result = false;
		
		IResource[] includes = properties.getIncludes(active);
		for (IResource include : includes) {
			IPath includePath = include.getFullPath();
			
			if (!includePath.equals(resourcePath)) {
                if (includePath.isPrefixOf(resourcePath)) {
                    result = true;
                    IResource[] excludes = properties.getExcludes(active);
                    for (IResource exclude : excludes) {
                        IPath excludePath = exclude.getFullPath();
                        if ((includePath.isPrefixOf(excludePath) && excludePath
                                .isPrefixOf(resourcePath))
                                || resourcePath.equals(excludePath)) {
                            result = false;
                            break;
                        }
                    }
                }
            } else {
                result = true;
            }
			
			if (result) {
				break;
			}
		}
		return result;
	}

	/**
	 * Gets the class file name for the top parent
	 * class.
	 * 
	 * @param classPath class file name.
	 * @return top class file name.
	 */
	private IPath getOutterClassName(IPath classPath) {
		IPath result = classPath;
		String className = classPath.lastSegment();
		if (className != null && className.contains("$")) { //$NON-NLS-1$
			className = className.substring(0x00, className.indexOf("$")); //$NON-NLS-1$
			result = classPath.removeLastSegments(0x01)
				.append(NLS.bind("{0}.class", className)); //$NON-NLS-1$
		}
		return result;
	}

	/**
	 * Gets the package name in the a/b/c format.
	 * 
	 * @param element target {@link IJavaElement} instance.
	 * @return the package name or null if the element
	 * is not within a package.
	 */
	private String getPackageName(IJavaElement element) {
		String result = Utils.EMPTY_STRING;
		IJavaElement parent = element.getParent();
		if (parent instanceof IPackageFragment) {
			IPackageFragment package_ = (IPackageFragment) parent;
			result = package_.getElementName().replace(".", "/"); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return result;
	}

	/**
	 * Gets the output folder relative path for a resource.
	 * 
	 * @param resource target resource.
	 * @return relative path.
	 * @throws CoreException Any core error occurred.
	 */
    private IPath getOutputFolderRelativePath(IResource resource) throws CoreException {

        List<IPath> outputLocations = Utils.getOutputLocations(suiteProject, false);

        return Utils.makePathRelativeToOutputLocation(resource.getFullPath(), outputLocations, new Path(
                Utils.EMPTY_STRING));
    }

	/**
	 * Gets the preverified class file from a class file
	 * within the binary folder.
	 * 
	 * @param clazz target class file.
	 * @return preverified class file.
	 * @throws CoreException Any core error occurs.
	 */
	private IFile getPreVerifiedClassFile(IFile clazz) throws CoreException {
		IFolder folder = this.suiteProject
				.getVerifiedClassesOutputFolder(new NullProgressMonitor());
		IFile preverfied = folder.getFile(getOutputFolderRelativePath(clazz));
		if (preverfied.exists()) {
			return preverfied;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.core.resources.IResourceDelta)
	 */
	public boolean visit(IResourceDelta delta) throws CoreException {
		IResource resource = delta.getResource();
		if (resource instanceof IFile) {
			IFile file = (IFile) resource;
			switch (delta.getKind()) {
			case IResourceDelta.CHANGED:
			case IResourceDelta.ADDED:
				this.visit(file);
				break;
			case IResourceDelta.REMOVED:
				this.removeFromJAR(file);
				break;
			}
		}
		return true;
	}

	/**
	 * Removes the file from the jar file.
	 * 
	 * @param file target file.
	 * @throws CoreException Any core error occurred.
	 */
	private void removeFromJAR(IFile file) throws CoreException {
		IPath zipPath = null;
		if (file.getName().endsWith(".class")) { //$NON-NLS-1$
			zipPath = PackageBuilder.getClassZipPath(this.suiteProject, file);
		} else {
			zipPath = PackageBuilder
					.getResourceZipPath(this.suiteProject, file);
		}

		if (zipPath != null) {
			File zipFile = new File(jar, zipPath.toString());
			if (zipFile.exists()) {
				zipFile.delete();
			}
		}
	}

	/**
	 * Gets all resource files collected.
	 * 
	 * @return resources list.
	 */
	public List<IFile> getResourcesToPackage() {
		List<IFile> copy = new ArrayList<IFile>();
		copy.addAll(this.resources);
		return copy;
	}

	/**
	 * Gets all class files collected.
	 * 
	 * @return class list.
	 */
	public List<IFile> getClassesToPackage() {
		List<IFile> copy = new ArrayList<IFile>();
		copy.addAll(this.classes);
		return copy;
	}
}
