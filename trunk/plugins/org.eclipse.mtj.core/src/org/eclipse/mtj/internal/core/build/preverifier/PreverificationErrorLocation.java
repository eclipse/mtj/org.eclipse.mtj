/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Gustavo de Paula (Motorola)  - Refactor preverification interfaces                                
 */
package org.eclipse.mtj.internal.core.build.preverifier;


/**
 * The location of the error.
 * 
 * @author Craig Setera
 */
public class PreverificationErrorLocation implements
        IPreverificationErrorLocation {
    private PreverificationErrorLocationType locationType;
    private IClassErrorInformation classInformation;

    /**
     * Construct a new location object.
     * 
     * @param locationType
     * @param classInformation
     * @param methodInformation
     * @param fieldInformation
     * @param lineNumber
     */
    public PreverificationErrorLocation(
            PreverificationErrorLocationType locationType,
            IClassErrorInformation classInformation) {
        super();

        this.classInformation = classInformation;
        this.locationType = locationType;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.preverifier.results.IPreverificationErrorLocation#getClassInformation()
     */
    public IClassErrorInformation getClassInformation() {
        return classInformation;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.preverifier.results.IPreverificationErrorLocation#getLineNumber()
     */
    public int getLineNumber() {
        return -1;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.preverifier.results.IPreverificationErrorLocation#getLocationType()
     */
    public PreverificationErrorLocationType getLocationType() {
        return locationType;
    }

}
