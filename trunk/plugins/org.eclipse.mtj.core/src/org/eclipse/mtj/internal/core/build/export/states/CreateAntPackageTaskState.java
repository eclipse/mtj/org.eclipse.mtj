/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Adding third party library support.
 */
package org.eclipse.mtj.internal.core.build.export.states;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.internal.core.build.export.AntennaBuildExport;
import org.eclipse.mtj.internal.core.build.export.AntennaExportException;
import org.eclipse.mtj.internal.core.statemachine.StateMachine;
import org.eclipse.mtj.internal.core.util.LibraryCollector;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * CreateAntPackageTaskState class creates the package target
 * for the build export.
 * 
 * @author David Marques
 * @since 1.0
 */
public class CreateAntPackageTaskState extends AbstractCreateAntTaskState {

	/**
	 * Creates a {@link CreateAntPackageTaskState} instance bound to the
	 * specified state machine in order to create package target for the
	 * specified project.
	 * 
	 * @param machine bound {@link StateMachine} instance.
	 * @param project target {@link IMidletSuiteProject} instance.
	 * @param _document target {@link Document}.
	 */
	public CreateAntPackageTaskState(StateMachine machine, IMidletSuiteProject project,
			Document _document, String buildFolder, String buildFile) {
		super(machine, project, _document, buildFolder, buildFile);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.build.export.states.AbstractCreateAntTaskState#onEnter(org.eclipse.mtj.core.project.runtime.MTJRuntime)
	 */
	protected void onEnter(MTJRuntime runtime) throws AntennaExportException {
		Document document = getDocument();
		Element  root     = document.getDocumentElement();

		String configName = getFormatedName(runtime.getName());
		Element target = XMLUtils.createTargetElement(document, root, NLS.bind(
				"package-{0}", configName), getDependencies(runtime)); //$NON-NLS-1$

		writePackageTask(target, runtime);
	}

	/**
	 * Writes the task.
	 * 
	 * @param target root element.
	 * @param runtime current runtime.
	 * @throws AntennaExportException 
	 */
	private void writePackageTask(Element target, MTJRuntime runtime) throws AntennaExportException {
		IProject project  = getMidletSuiteProject().getProject();
		Document document = getDocument();
		String configName = runtime.getName().replace(" ", "_"); //$NON-NLS-1$ //$NON-NLS-2$
		
		Element wtkPackage = document.createElement("wtkpackage"); //$NON-NLS-1$
		String projectName = getFormatedName(project.getName());
		
		String values[] = new String[] {configName, getMidletSuiteProject().getJadFileName()};
		wtkPackage.setAttribute("jadfile", NLS.bind("deployed/{0}/{1}", values)); //$NON-NLS-1$ //$NON-NLS-2$
		
		values = new String[] {configName, getMidletSuiteProject().getJarFilename()};
		wtkPackage.setAttribute("jarfile", NLS.bind("deployed/{0}/{1}", values)); //$NON-NLS-1$ //$NON-NLS-2$
		wtkPackage.setAttribute("autoversion", NLS.bind("${0}{1}{2}" //$NON-NLS-1$ //$NON-NLS-2$
				, new String[] {"{", AntennaBuildExport.DO_AUTOVERSION, "}"})); //$NON-NLS-1$ //$NON-NLS-2$
		wtkPackage.setAttribute("obfuscate", NLS.bind("${0}{1}{2}" //$NON-NLS-1$ //$NON-NLS-2$
				, new String[] {"{", AntennaBuildExport.DO_OBFUSCATE, "}"})); //$NON-NLS-1$ //$NON-NLS-2$
		wtkPackage.setAttribute("preverify", "true"); //$NON-NLS-1$ //$NON-NLS-2$
		
		StringBuffer bootClassPathString = new StringBuffer();
		IPath libPaths[] = resolveLibraryPaths(getMidletSuiteProject(), false);
        for (IPath path : libPaths) {
            bootClassPathString.append(path.toOSString()).append(":"); //$NON-NLS-1$
        }
		wtkPackage.setAttribute("bootclasspath", bootClassPathString.toString()); //$NON-NLS-1$
		
		StringBuffer libClassPathString = new StringBuffer();
        libPaths = resolveLibraryPaths(getMidletSuiteProject(), true);
        for (IPath path : libPaths) {
            libClassPathString.append(path.toOSString()).append(":"); //$NON-NLS-1$
        }
        wtkPackage.setAttribute("libclasspath", libClassPathString.toString()); //$NON-NLS-1$
		
		target.appendChild(wtkPackage);
		
		String dirs[] = new String[] {"{0}/{1}/{2}/bin/", "{0}/{1}/{2}/resources/"}; //$NON-NLS-1$ //$NON-NLS-2$
		for (String dir : dirs) {			
			Element fileset = document.createElement("fileset"); //$NON-NLS-1$
			wtkPackage.appendChild(fileset);
			fileset.setAttribute("dir", NLS.bind(dir, //$NON-NLS-1$
					new String[] { getBuildFolder(), configName,
					getFormatedName(projectName)}));
		}
	}

	/**
     * Gets all library paths on this project class path.
     * 
     * @param midletSuiteProject target suite project.
     * @param excludeNonExported if true excludes the
     * non exported libraries from the result array.
     * 
     * @return array of paths.
     * @throws AntennaExportException Any error occurs.
     */
	private IPath[] resolveLibraryPaths(IMidletSuiteProject midletSuiteProject, boolean excludeNonExported) throws AntennaExportException {
        IJavaProject javaProject = getMidletSuiteProject().getJavaProject();
        
        LibraryCollector collector = new LibraryCollector();
        try {
            collector.getRunner().run(javaProject, collector, new NullProgressMonitor());
        } catch (CoreException exc) {
            throw new AntennaExportException(exc, "Unable to resolve library dependecies."); //$NON-NLS-1$
        }
        return collector.getLibraryPaths(excludeNonExported);
    }
	
}
