/**
 * Copyright (c) 2000,2008 IBM Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation         - initial API and implementation
 *     Diego Sandin (Motorola) - Adapted code from org.eclipse.pde.core
 */
package org.eclipse.mtj.internal.core.text;

import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.internal.core.IEditable;
import org.eclipse.mtj.internal.core.IModel;
import org.eclipse.mtj.internal.core.IModelChangeProvider;

/**
 * @since 0.9.1
 */
public interface IEditingModel extends IModel, IModelChangeProvider,
        IReconcilingParticipant, IEditable {

    /**
     * @return
     */
    public String getCharset();

    /**
     * @return
     */
    public IDocument getDocument();

    /**
     * @return
     */
    public boolean isStale();

    /**
     * @param charset
     */
    public void setCharset(String charset);

    /**
     * @param stale
     */
    public void setStale(boolean stale);

}
