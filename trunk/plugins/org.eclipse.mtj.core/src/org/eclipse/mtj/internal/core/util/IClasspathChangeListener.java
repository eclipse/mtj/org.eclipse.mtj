/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Rafael Amaral (Motorola) - Initial version
 */

package org.eclipse.mtj.internal.core.util;

import org.eclipse.mtj.core.project.IMTJProject;

/**
 * Classes that implement this interface should work with ClasspathChangeMonitor.
 * 
 * When a class that implements this interface is associated to a 
 * {@link ClasspathChangeMonitor} instance, it will call the 
 * {@link #classpathChanged()} method when a change on the classpath 
 * of the associated {@link IMTJProject} instance occurs.
 */
public interface IClasspathChangeListener {
    /**
     * This method is called by ClasspathChangeMonitor when
     * a change on a classpath associated to this listener occurs
     */
    public void classpathChanged();
}
