/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.sign;

/**
 *	KeyStoreEntry class wraps information about
 *  a keystore entries. 
 *
 *  @author David Marques
 */
public class KeyStoreEntry {

	public static enum Type {
		KEY_PAIR, CERIFICATE
	}

	private String alias;
	private Type type;

	/**
	 * Creates an instance of a keystore entry.
	 * 
	 * @param _alias entry name.
	 * @param _type entry type.
	 */
	public KeyStoreEntry(String _alias, Type _type) {
		if (_alias == null) {
			throw new IllegalArgumentException("Invalid alias name");
		}

		if (_type == null) {
			throw new IllegalArgumentException("Invalid alias type");
		}

		this.alias = _alias;
		this.type  = _type;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof KeyStoreEntry) {
			KeyStoreEntry other = (KeyStoreEntry) obj;
			result = this.alias.equals(other.alias) && this.type == other.type;
		}
		return result;
	}

	/**
	 * Gets the entry alias name.
	 * 
	 * @return alias name.
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Gets the entry type.
	 * 
	 * @return type.
	 */
	public Type getType() {
		return type;
	}
}
