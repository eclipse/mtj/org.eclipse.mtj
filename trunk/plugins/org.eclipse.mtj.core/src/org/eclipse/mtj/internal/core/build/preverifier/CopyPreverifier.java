/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.core.build.preverifier;

import java.io.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.*;
import org.eclipse.mtj.core.build.preverifier.IPreverificationError;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.util.Utils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.ClassNode;
import org.osgi.framework.Version;

/**
 * This preverifier does not do actual class files verification, but only copies original compiled classes 
 * to verification results folder. It is used for class files verification of applications with CLDC 1.8 
 * or later configuration when verification is performed on-device by type checking. The only verification is 
 * performed by this type of preverifier is the checking that compliance level of the project compiled 
 * classes is 1.7 or higher (this requirement of CLDC 1.8 spec for this type of verification).   
 */
public class CopyPreverifier implements IPreverifier {
    private static final String CLASS_EXT = ".class"; //$NON-NLS-1$

    private static final String MIN_SUPPORTED_VERSION_TEXT = "51.0"; // Java 7 class file format version //$NON-NLS-1$
    private static final Version MIN_SUPPORTED_VERSION = new Version(MIN_SUPPORTED_VERSION_TEXT);

    public void loadUsing(IPersistenceProvider persistenceProvider) throws PersistenceException {}

    public void storeUsing(IPersistenceProvider persistenceProvider) throws PersistenceException {}

    public IPreverificationError[] preverify(IMTJProject mtjProject, IResource[] toVerify, IFolder outputFolder,
            IProgressMonitor monitor) throws CoreException {
        List<IPath> outputLocations = Utils.getOutputLocations(mtjProject, true);
        List<IPreverificationError> errors = new ArrayList<IPreverificationError>();

        for (IResource resource : toVerify) {
            try {
                if (verifyResource(resource, errors)) {
                    IPath outputRelativePath = Utils.makePathRelativeToOutputLocation(resource.getFullPath(),
                            outputLocations, resource.getFullPath().removeFirstSegments(1));
                    IPath targetPath = outputFolder.getLocation().append(outputRelativePath);
                    targetPath.toFile().getParentFile().mkdirs();
                    File sourceFile = resource.getLocation().toFile();
                    Utils.copy(sourceFile, targetPath.toFile());
                }
            } catch (Throwable t) {
                IStatus status = new Status(IStatus.ERROR, IMTJCoreConstants.PLUGIN_ID, "Failed to copy file " //$NON-NLS-1$
                        + resource.getFullPath().toOSString() + " to the preverification output folder.", t); //$NON-NLS-1$
                throw new CoreException(status);
            }
        }

        return errors.toArray(new IPreverificationError[errors.size()]);
    }

    public IPreverificationError[] preverifyJarFile(IMTJProject mtjProject, File jarFile, IFolder outputFolder,
            IProgressMonitor monitor) throws CoreException {
        List<IPreverificationError> errors = new ArrayList<IPreverificationError>();

        try {
            if (verifyJarFile(jarFile, errors)) {
                // create plain File object here to avoid possible involving of TrueZIP File object
                File sourceJarFile = new File(jarFile.getPath());

                Utils.copy(sourceJarFile, outputFolder.getLocation().append(jarFile.getName()).toFile());
            }
        } catch (Throwable t) {
            IStatus status = new Status(IStatus.ERROR, IMTJCoreConstants.PLUGIN_ID,
                    "Failed to copy jar to the preverification output folder.", t); //$NON-NLS-1$
            throw new CoreException(status);
        }

        return errors.toArray(new IPreverificationError[errors.size()]);
    }

    public File getPreverifierExecutable() {
        return null;
    }

    private boolean verifyJarFile(File jarFile, List<IPreverificationError> errors) throws IOException {
        JarFile jar = new JarFile(jarFile.getAbsolutePath());
        boolean result = true;

        try {
            Enumeration<JarEntry> entries = jar.entries();

            while (entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();

                if (entry.getName().toLowerCase().endsWith(CLASS_EXT)) {
                    if (!verifyClass(jar.getInputStream(entry), errors, true)) {
                        result = false;
                    }
                }
            }

        } finally {
            jar.close();
        }

        return result;
    }

    private boolean verifyResource(IResource resource, List<IPreverificationError> errors) throws IOException {
        if (resource.getName().toLowerCase().endsWith(CLASS_EXT)) {
            return verifyClass(new FileInputStream(resource.getLocation().toFile()), errors, false);
        }

        return true;
    }

    private boolean verifyClass(InputStream is, List<IPreverificationError> errors, boolean includeClassInMessage)
            throws IOException {
        if (is != null) {
            try {
                ClassReader reader = new ClassReader(new BufferedInputStream(is));
                final ClassNode node = new ClassNode();

                reader.accept(node, 0);

                String version = (node.version & 0xFFFF) + "." + ((node.version >> 16) & 0xFFFF); //$NON-NLS-1$

                if (new Version(version).compareTo(MIN_SUPPORTED_VERSION) < 0) {

                    String errorMessageTemplate;

                    if (includeClassInMessage) {
                        errorMessageTemplate = Messages.CopyPreverifier_UnsupportedClassFormatJAR;
                    } else {
                        errorMessageTemplate = Messages.CopyPreverifier_UnsupportedClassFormat;
                    }

                    final String className = node.name.replace('/', '.');
                    String errorMessage = MessageFormat.format(errorMessageTemplate, version, className,
                            MIN_SUPPORTED_VERSION_TEXT);

                    PreverificationErrorLocation errorLocation = new PreverificationErrorLocation(
                            PreverificationErrorLocationType.CLASS_DEFINITION, new IClassErrorInformation() {
                                public String getName() {

                                    return className;
                                }

                                public String getSourceFile() {
                                    return null;
                                }
                            });
                    errors.add(new PreverificationError(PreverificationErrorType.UNSUPPORTED_CLASS_FILE_FORMAT,
                            errorLocation, errorMessage));

                    return false;
                }
            } finally {
                is.close();
            }
        }

        return true;
    }
}
