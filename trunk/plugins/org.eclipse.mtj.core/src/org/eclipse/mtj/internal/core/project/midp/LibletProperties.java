/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/

package org.eclipse.mtj.internal.core.project.midp;


public class LibletProperties {
	public enum Level {
		OPTIONAL, 
		REQUIRED;
	
		public String getJadName() {
			switch (this) {
			case OPTIONAL: return "optional"; //$NON-NLS-1$
			case REQUIRED: return "required"; //$NON-NLS-1$
			}
			
			return null;
		}
		
		public static Level getByJadName(String name) {
			if (name.equals("optional")) { //$NON-NLS-1$
				return OPTIONAL;
			} else if (name.equals("required")) { //$NON-NLS-1$
			    return REQUIRED;
			}
			
			return null;
		}
	}
	
	public enum Type {
		LIBLET,
		STANDARD,
		SERVICE,
		PROPRIETARY;
	
		public String getJadName() {
			switch (this) {
			case LIBLET: return "liblet"; //$NON-NLS-1$
			case PROPRIETARY: return "proprietary"; //$NON-NLS-1$
			case SERVICE: return "service"; //$NON-NLS-1$
			case STANDARD: return "standard"; //$NON-NLS-1$
			}
			
			return null;
		}
		
		public static Type getByJadName(String name) {
			if (name.equals("liblet")) { //$NON-NLS-1$
			    return LIBLET;
			} else if (name.equals("proprietary")) { //$NON-NLS-1$
				return PROPRIETARY;
			} else if (name.equals("service")) { //$NON-NLS-1$
				return SERVICE;
			} else if (name.equals("standard")) { //$NON-NLS-1$
			    return STANDARD;
			}
			
			return null;
		}
	}
	
	private String name;
	private Level level;
	private Type type;
	private String jadURL;
	private String vendor;
	private String version;
	private String jadFileName;
	private String projectName;
	
	public LibletProperties(String name, Level level, Type type,
			String jadURL, String vendor, String version, String projectName) {
		this.name = name;
		this.level = level;
		this.type = type;
		this.jadURL = jadURL;
		this.vendor = vendor;
		this.version = version;
		this.setProjectName(projectName);
	}
	
	public String getJadPropertyString() {
		return type.getJadName() + ";" + level.getJadName() //$NON-NLS-1$
				+ ";" + name + ";" //$NON-NLS-1$ //$NON-NLS-2$
		        + (type != Type.SERVICE ? vendor + ";" + version : ";"); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Level getLevel() {
		return level;
	}
	public void setLevel(Level level) {
		this.level = level;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public String getJadURL() {
		return jadURL;
	}
	public void setJadURL(String jadURL) {
		this.jadURL = jadURL;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}

	public String getJadFileName() {
		return jadFileName;
	}

	public void setJadFileName(String jadFileName) {
		this.jadFileName = jadFileName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
}
