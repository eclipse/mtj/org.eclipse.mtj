/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Refactoring to allow listeners.
 *     David Marques (Motorola) - Refactoring to follow PDE build.properties.
 *     David Marques (Motorola) - Fixing some access methods.
 *     David Marques (Motorola) - Synchronizing access to properties.
 */
package org.eclipse.mtj.internal.core.build;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.osgi.util.NLS;

/**
 * MTJBuildProperties class extends the {@link Properties} class
 * in order to implement build properties for the MTJ projects.
 * 
 * @author David Marques
 */
public class MTJBuildProperties extends Properties {

	private class RuntimeBuildProperties {
		
		List<IResource> includes;
		List<IResource> excludes;
		
		public RuntimeBuildProperties() {
			this.includes = new ArrayList<IResource>();
			this.excludes = new ArrayList<IResource>();
		}
		
		public void clear() {
			this.includes.clear();
			this.excludes.clear();
		}
	}
	/**
	 * Refreshes the given file. Used for refreshing the build.properteis 
	 * after its content has been modified. 
	 */
	private class RefreshJob extends WorkspaceJob{
		private IResource file;
		public RefreshJob(String name, IResource file) {
			super(name);
			this.file = file;
			this.setSystem(true);
			this.setUser(false);
		}

		@Override
		public IStatus runInWorkspace(IProgressMonitor monitor)
				throws CoreException {
			if (file != null ){
				file.refreshLocal(IResource.DEPTH_ZERO, monitor);
			}
			return Status.OK_STATUS;
		}
		
	}
	
	public static final String FILE_NAME = "build.properties"; //$NON-NLS-1$
	private static final long serialVersionUID = -3576616481147533654L;
	private static Map<IMTJProject, MTJBuildProperties> map; 
	
	private Map<MTJRuntime, RuntimeBuildProperties> runtimeProperties;
	private List<MTJBuildPropertiesChangeListener>  listeners;
	private IMTJProject                             mtjProject;
	private Object                                  mutex;
	
	/**
	 * Gets the MTJBuildProperties instance for the specified {@link IMTJProject}
	 * instance.
	 * 
	 * @param mtjProject target {@link IMTJProject} instance.
	 * @return the build properties.
	 */
	public static synchronized MTJBuildProperties getBuildProperties(IMTJProject mtjProject) {
		if (MTJBuildProperties.map == null) {
			MTJBuildProperties.map = new HashMap<IMTJProject, MTJBuildProperties>();
		}
		
		MTJBuildProperties buildProperties = MTJBuildProperties.map.get(mtjProject);
		if (buildProperties == null) {
			buildProperties = new MTJBuildProperties(mtjProject);
			MTJBuildProperties.map.put(mtjProject, buildProperties);
		}
		return buildProperties;
	}
	
	/**
	 * Creates an instance of a MTJBuildProperties class
	 * to setup build properties for the specified MYJ
	 * project.
	 * 
	 * @param _mtjProject target project.
	 */
	private MTJBuildProperties(IMTJProject _mtjProject) {
		if (_mtjProject == null) {
			throw new IllegalArgumentException(Messages.MTJBuildProperties_invalidMTJProject);
		}
		this.listeners  = new ArrayList<MTJBuildPropertiesChangeListener>();
		this.mtjProject = _mtjProject;
		this.mutex      = new Object();
		
		this.runtimeProperties = new HashMap<MTJRuntime, RuntimeBuildProperties>();
		for (MTJRuntime runtime : this.mtjProject.getRuntimeList()) {
			RuntimeBuildProperties runtimeProperties = new RuntimeBuildProperties();
			this.runtimeProperties.put(runtime, runtimeProperties);
		}
		this.loadFromFile();
	}
	
	/**
	 * Reloads the content from the file and updates 
	 * all {@link MTJBuildPropertiesChangeListener}
	 * instances.
	 */
	public void load() {
		this.loadFromFile();
		this.notifyListeners();
	}
	
	/**
	 * Loads the content from the file.
	 */
	private synchronized void loadFromFile() {
		IResource file = getBuildPropertyFile();
		if (file.exists()) {
			try {
				FileInputStream in = new FileInputStream(file.getLocation().toOSString());
				
				try {
				    this.doLoad(in);
				} finally {
				    in.close();
				}
			} catch (IOException e) {
				MTJLogger.log(IStatus.ERROR, e);
			}
		}
	}

	/**
	 * Loads the content from the build.properties file.
	 * 
	 * @param inputStream source {@link InputStream} instance.
	 * @throws IOException Any IO error occurs.
	 */
	private void doLoad(InputStream inputStream) throws IOException {
		super.load(inputStream);
		
		MTJRuntimeList runtimeList = this.mtjProject.getRuntimeList();
		for (MTJRuntime runtime : runtimeList) {
			String includes = getProperty(NLS.bind("{0}.includes", runtime.getName())); //$NON-NLS-1$
			String excludes = getProperty(NLS.bind("{0}.excludes", runtime.getName())); //$NON-NLS-1$
			
			RuntimeBuildProperties runtimeProperties = this.runtimeProperties.get(runtime);
			if (runtimeProperties != null) {
				runtimeProperties.clear();
				if (includes == null && excludes == null) {
					includeDefaultResources(runtimeProperties.includes);
				} else {
					parseResourcesList(runtimeProperties.includes, includes);
					parseResourcesList(runtimeProperties.excludes, excludes);
				}
			}
		}
	}

	/**
	 * Adds the change listener to the {@link MTJBuildProperties}.
	 * 
	 * @param listener target listener.
	 */
	public void addPropertiesChangeListener(
			MTJBuildPropertiesChangeListener listener) {
		synchronized (this.listeners) {			
			if (!this.listeners.contains(listener)) {
				this.listeners.add(listener);
			}
		}
	}
	
	/**
	 * Removes the change listener from the {@link MTJBuildProperties}.
	 * 
	 * @param listener target listener.
	 */
	public void removePropertiesChangeListener(
			MTJBuildPropertiesChangeListener listener) {
		synchronized (this.listeners) {			
			if (this.listeners.contains(listener)) {
				this.listeners.remove(listener);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.Properties#load(java.io.InputStream)
	 */
	public synchronized void load(InputStream inStream) throws IOException {
		throw new IllegalAccessError(Messages.MTJBuildProperties_invalidMethodAccess);
	}
	
	/* (non-Javadoc)
	 * @see java.util.Properties#load(java.io.Reader)
	 */
	public synchronized void load(Reader reader) throws IOException {
		throw new IllegalAccessError(Messages.MTJBuildProperties_invalidMethodAccess);
	}
	
	/* (non-Javadoc)
	 * @see java.util.Properties#loadFromXML(java.io.InputStream)
	 */
	public synchronized void loadFromXML(InputStream in) throws IOException,
			InvalidPropertiesFormatException {
		throw new IllegalAccessError(Messages.MTJBuildProperties_invalidMethodAccess);
	}
	
	/**
	 * Includes the default resources. The default resources are
	 * the project source folders.
	 * 
	 * @param list default resources.
	 */
	private void includeDefaultResources(List<IResource> list) {
	    synchronized (this.mutex) {            
	        list.clear();
	        IResource[] sources = Utils.getSourceFolders(this.mtjProject.getJavaProject());
	        try {			
	            list.addAll(Arrays.asList(sources));
	        } catch (Exception e) {
	            MTJLogger.log(IStatus.ERROR, e);
	        }
        }
	}

	/**
	 * Parses the string list into the {@link List}.
	 * 
	 * @param list target list.
	 * @param stringList source string.
	 */
	private void parseResourcesList(List<IResource> list, String stringList) {
		if (stringList != null) {
			String separator = System.getProperty("line.separator"); //$NON-NLS-1$
			stringList = stringList.replaceAll(separator, Utils.EMPTY_STRING);
			
			synchronized (this.mutex) {                
			    IProject project = this.mtjProject.getProject();
			    String[] paths   = stringList.split(","); //$NON-NLS-1$
			    for (String path : paths) {
			        IResource resource = project.findMember(path);
			        if (resource != null) {
			            list.add(resource);
			        }
			    }
            }
		}
	}

	/* (non-Javadoc)
	 * @see java.util.Properties#store(java.io.Writer, java.lang.String)
	 */
	public void store(Writer writer, String comments) throws IOException {
		PrintWriter printWriter = null;
		if (!(writer instanceof PrintWriter)) {
			printWriter = new PrintWriter(writer);
		} else {
			printWriter = (PrintWriter) writer; 
		}
		
		try {			
			writeProperties(printWriter, comments);
			printWriter.flush();
			printWriter.close();
		} finally {
			notifyListeners();
		}
	}
	
	/* (non-Javadoc)
	 * @see java.util.Properties#store(java.io.OutputStream, java.lang.String)
	 */
	public void store(OutputStream out, String comments) throws IOException {
		PrintWriter writer = new PrintWriter(new OutputStreamWriter(out));
		this.store(writer, comments);
	}

	/**
	 * Stores the properties content into the build.properties file.
	 * 
	 * @throws IOException Any IO error occured.
	 */
	public void store() throws IOException {
		IResource file  = getBuildPropertyFile();
		PrintWriter printWriter = new PrintWriter(new FileWriter(file.getLocation().toFile()));
		
		try {
		    this.store(printWriter, Messages.MTJBuildProperties_mtjBuildPropertiesComment);
		} finally { 
		    printWriter.close();
		}
		
		// Use a job to avoid workspace locks.
		RefreshJob job = new RefreshJob("refresh build.properties", file); //$NON-NLS-1$
		job.schedule();
	}
	
	/**
	 * Gets the build.properties file in the specified project.
	 * 
	 * @param project target project.
	 * @return a reosurce handler.
	 */
	public IFile getBuildPropertyFile() {
		return this.mtjProject.getProject().getFile(MTJBuildProperties.FILE_NAME);
	}
	
	/**
	 * Gets the {@link IMTJProject} instance containing
	 * this buid.properties.
	 * 
	 * @return mtj project.
	 */
	public IMTJProject getMTJProject() {
		return this.mtjProject;
	}
	
	/**
	 * Sets the {@link MTJBuildProperties} content. This
	 * call does not update the build.properties file 
	 * until saved with {@link #store()}.
	 * 
	 * @param stream {@link InputStream} instance.
	 * @throws IOException Any IO error occurs.
	 */
	public void setContent(InputStream stream) throws IOException {
		this.doLoad(stream);
	}
	
	/**
	 * Gets the properties content as an {@link StringBuffer}.
	 * 
	 * @return stirng buffer.
	 */
	public StringBuffer getContent() {
		StringWriter writer = new StringWriter();
		writeProperties(new PrintWriter(writer), null);
		return writer.getBuffer();
	}
	
	/**
	 * Removes the resource from the build.properties. 
	 * 
	 * @param _resource target reosurce.
	 */
	public void deleteResource(IResource _resource) {
		for (MTJRuntime runtime : this.mtjProject.getRuntimeList()) {
			RuntimeBuildProperties runtimeProperties = this.runtimeProperties.get(runtime);
			if (runtimeProperties != null) {
				removeResourceAndChildren(runtimeProperties.includes, _resource);
				removeResourceAndChildren(runtimeProperties.excludes, _resource);
			}
		}
	}
	
	/**
	 * Removes the resource and it's children.
	 * 
	 * @param list target list.
	 * @param _resource target reosurce.
	 */
	private void removeResourceAndChildren(List<IResource> list, IResource _resource) {
		List<IResource> removals = new ArrayList<IResource>();
		
		for (IResource resource : list) {
			IPath candidatePath = resource.getProjectRelativePath();
			IPath myPath        = _resource.getProjectRelativePath();
			if (myPath.isPrefixOf(candidatePath) || myPath.equals(candidatePath)) {
				removals.add(resource);
			}
		}
		
		synchronized (this.mutex) {
		    list.removeAll(removals);
        }
	}

	/**
	 * Includes the resource into the {@link MTJRuntime} includes within
	 * the build.properties file.
	 * 
	 * @param _resource target resource.
	 * @param _runtime target runtime.
	 */
	public synchronized void includeResource(IResource _resource, MTJRuntime _runtime) {
		RuntimeBuildProperties runtimeProperties = this.runtimeProperties.get(_runtime);
		if (runtimeProperties != null) {
		    synchronized (this.mutex) {                
		        //Removes resource and children from excludes
		        removeResourceAndChildren(runtimeProperties.excludes, _resource);
		        //Removes resource and children from includes			
		        removeResourceAndChildren(runtimeProperties.includes, _resource);
		        //Includes parent resource if needed.
		        if (!isResourceIncludedByParent(_resource, _runtime)) {				
		            runtimeProperties.includes.add(_resource);
		        }
            }
		}
	}
	
	/**
	 * Checks whether the resource inclusion is already
	 * covered by another include.
	 * 
	 * @param _resource target resource.
	 * @param _runtime current runtime.
	 * @return true if covered false otherwise.
	 */
	private boolean isResourceIncludedByParent(IResource _resource, MTJRuntime _runtime) {
		RuntimeBuildProperties runtimeProperties = this.runtimeProperties.get(_runtime);
		if (runtimeProperties == null) {
			return false;
		}
		boolean result = false;
		IPath   myPath = _resource.getFullPath();

		List<IResource> includes = runtimeProperties.includes;
		for (IResource include : includes) {
			IPath includePath = include.getFullPath();
			// If there is a parent include check if there
			// is any exclude between it and my path.
			if (!includePath.isPrefixOf(myPath)) {
				continue;
			}
			result = true;
			
			List<IResource> excludes = runtimeProperties.excludes;
			for (IResource exclude : excludes) {
				IPath excludePath = exclude.getFullPath();
				if (includePath.isPrefixOf(excludePath) 
						&& excludePath.isPrefixOf(myPath)) {
					result = false;
					break;
				}
			}
			
			// If there is any include with no excludes
			// between it and the resource it is already
			// been included by parent.
			if (result) {
				break;
			}
		}
		return result;
	}

	/**
	 * Excludes the resource from the {@link MTJRuntime} excludes within
	 * the build.properties file.
	 * 
	 * @param _resource target resource.
	 * @param _runtime target runtime.
	 */
	public synchronized void excludeResource(IResource _resource, MTJRuntime _runtime) {
		RuntimeBuildProperties runtimeProperties = this.runtimeProperties.get(_runtime);
		if (runtimeProperties != null) {
		    synchronized (this.mutex) {                
		        //Removes resource and children from includes						
		        removeResourceAndChildren(runtimeProperties.includes, _resource);
		        //Removes resource and children from excludes
		        removeResourceAndChildren(runtimeProperties.excludes, _resource);
		        //Excludes parent resource if needed.
		        if (!isResourceExcludedByParent(_resource, _runtime)) {				
		            runtimeProperties.excludes.add(_resource);
		        }
            }
		}
	}
	
	/**
	 * Checks whether the resource exclusion is already
	 * covered by another exclude.
	 * 
	 * @param _resource target resource.
	 * @param _runtime current runtime.
	 * @return true if covered false otherwise.
	 */
	private boolean isResourceExcludedByParent(IResource _resource, MTJRuntime _runtime) {
		RuntimeBuildProperties runtimeProperties = this.runtimeProperties.get(_runtime);
		if (runtimeProperties == null) {
			return false;
		}
		boolean result = false;
		IPath   myPath = _resource.getFullPath();

		List<IResource> excludes = runtimeProperties.excludes;
		for (IResource exclude : excludes) {
			IPath excludePath = exclude.getFullPath();
			// If there is a parent exclude check if there
			// is any include between it and my path.
			if (!excludePath.isPrefixOf(myPath)) {
				continue;
			}
			result = true;
			
			List<IResource> includes = runtimeProperties.includes;
			for (IResource include : includes) {
				IPath includePath = include.getFullPath();
				if (excludePath.isPrefixOf(includePath) 
						&& includePath.isPrefixOf(myPath)) {
					result = false;
					break;
				}
			}
			
			// If there is any exclude with no includes
			// between it and the resource it is already
			// been excluded by parent.
			if (result) {
				break;
			}
		}
		return result;
	}
	
	/**
	 * Gets all includes for the specified {@link MTJRuntime}.
	 * 
	 * @param _runtime target runtime.
	 * @return included resources.
	 */
	public IResource[] getIncludes(MTJRuntime _runtime) {
		IResource[] result = new IResource[0];
		
		RuntimeBuildProperties runtimeProperties = this.runtimeProperties.get(_runtime);
		if (runtimeProperties == null) {						
			addRuntime(_runtime);
			runtimeProperties = this.runtimeProperties.get(_runtime);
		}
		
		synchronized (this.mutex) {
		    result = runtimeProperties.includes.toArray(new IResource[0x00]);
        }
		
		return result;
	}
	
	/**
	 * Gets all excludes for the specified {@link MTJRuntime}.
	 * 
	 * @param _runtime target runtime.
	 * @return excluded resources.
	 */
	public IResource[] getExcludes(MTJRuntime _runtime) {
		IResource[] result = new IResource[0];
		
		RuntimeBuildProperties runtimeProperties = this.runtimeProperties.get(_runtime);
		if (runtimeProperties == null) {						
			addRuntime(_runtime);
			runtimeProperties = this.runtimeProperties.get(_runtime);
		}
		
		synchronized (this.mutex) {		    
		    result = runtimeProperties.excludes.toArray(new IResource[0x00]);
		}
		
		return result;
	}
	
	private void addRuntime(MTJRuntime _runtime) {
		RuntimeBuildProperties runtimeProperties = this.runtimeProperties.get(_runtime);
		if (runtimeProperties == null) {			
			runtimeProperties = new RuntimeBuildProperties();
			includeDefaultResources(runtimeProperties.includes);
			this.runtimeProperties.put(_runtime, runtimeProperties);
		}
	}

	/**
	 * Writes the properties into the {@link PrintWriter} instance.
	 * 
	 * @param printWriter target writer.
	 * @param comments source file comments.
	 */
	private void writeProperties(PrintWriter printWriter, String comments) {
		printWriter.println((comments == null ? Messages
				.MTJBuildProperties_mtjBuildPropertiesComment : comments));
		
		synchronized (this.mutex) {            
		    for (MTJRuntime runtime : this.mtjProject.getRuntimeList()) {
		        RuntimeBuildProperties runtimeProperties = this.runtimeProperties.get(runtime);
		        if (runtimeProperties != null) {			
		            String key   = null;
		            String value = null;
		            
		            key   = NLS.bind("{0}.includes", runtime.getName()); //$NON-NLS-1$
		            value = this.format(this.getResourcesString(runtimeProperties.includes));
		            printWriter.println(NLS.bind("{0}={1}", new String[] {key, value})); //$NON-NLS-1$
		            
		            key   = NLS.bind("{0}.excludes", runtime.getName()); //$NON-NLS-1$
		            value = this.format(this.getResourcesString(runtimeProperties.excludes));
		            printWriter.println(NLS.bind("{0}={1}", new String[] {key, value})); //$NON-NLS-1$
		        }
		    }
        }
	}
	
	/**
	 * Formats the property value in order to split comma
	 * separated values each one in a line.
	 * 
	 * @param value comma separated values.
	 * @return the formatted string.
	 */
	private String format(String value) {
		String separator = System.getProperty("line.separator"); //$NON-NLS-1$
		StringBuffer buffer = new StringBuffer();
		if (value != null) {
			String[] values = value.split(","); //$NON-NLS-1$
			for (String path : values) {
				if (buffer.length() > 0) {
					buffer.append(","); //$NON-NLS-1$
				}
				buffer.append(NLS.bind("{0}\\{1}" //$NON-NLS-1$
						, new String[] {path, separator}));
			}
		}
		return buffer.toString();
	}
	
	/**
	 * Formats as string containing a comma separated list of
	 * resources.
	 * 
	 * @param list array of resources.
	 * @return formatted string.
	 */
	private String getResourcesString(List<IResource> list) {
		StringBuffer buffer = new StringBuffer();
		for (IResource resource : list) {
			String path = resource.getProjectRelativePath().toString();
			if (buffer.length() > 0) {
				buffer.append(","); //$NON-NLS-1$
			}
			buffer.append(path);
		}
		return buffer.toString();
	}
	
	/**
	 * Notifies all listeners.
	 */
	private void notifyListeners() {
		for (MTJBuildPropertiesChangeListener listener : this.listeners) {
			listener.propertiesChanged(this);
		}
	}
}
