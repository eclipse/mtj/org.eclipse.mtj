/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.util;

import java.util.ArrayList;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;

/**
 * RequiredProjectsCPEntryVisitor visitor extends the
 * {@link AbstractClasspathEntryVisitor} in order to
 * collect all exported required projects from an 
 * {@link IJavaProject} instance.
 * 
 * @author David Marques
 */
public class RequiredProjectsCPEntryVisitor extends
		AbstractClasspathEntryVisitor {

	private ArrayList<IJavaProject> requiredProjects;

	/** Construct a new instance. */
	public RequiredProjectsCPEntryVisitor() {
		requiredProjects = new ArrayList<IJavaProject>();
	}

	/**
	 * @return Returns the requiredProjects.
	 */
	public ArrayList<IJavaProject> getRequiredProjects() {
		return requiredProjects;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mtj.internal.core.util.AbstractClasspathEntryVisitor#visitProject
	 * (org.eclipse.jdt.core.IClasspathEntry, org.eclipse.jdt.core.IJavaProject,
	 * org.eclipse.jdt.core.IJavaProject,
	 * org.eclipse.core.runtime.IProgressMonitor)
	 */
	public boolean visitProject(IClasspathEntry entry,
			IJavaProject javaProject, IJavaProject classpathProject,
			IProgressMonitor monitor) throws CoreException {
		boolean continueVisitation = entry.getEntryKind()==IClasspathEntry.CPE_PROJECT;

		if (continueVisitation) {
			requiredProjects.add(classpathProject);
		}

		return continueVisitation;
	}

}
