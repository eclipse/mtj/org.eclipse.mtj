/**
 * Copyright (c) 2003,2009 Motorola Inc. and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Gustavo de Paula (Motorola)  - Initial Creation
 *     David Aragao(Motorola)       - Adding Singleton implementation
 */
package org.eclipse.mtj.internal.core.build.preverifier;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.IStreamListener;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IStreamMonitor;
import org.eclipse.debug.core.model.IStreamsProxy;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.BuildConsoleProxy;
import org.eclipse.mtj.internal.core.build.BuildLoggingConfiguration;
import org.eclipse.mtj.internal.core.build.IBuildConsoleProxy;
import org.eclipse.mtj.internal.core.util.TemporaryFileManager;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * Use proguard to preverify the MIDlet suite classes. Design and code is mostly
 * based on org.eclipse.mtj.core.model.impl.StandardPreverifier.
 *
 * TODO Refactor code to have a common class to be used both by
 * StandPreverifier and ProguardPreverifier
 *
 * @author wgp010
 */
public class ProguardPreverifier implements IPreverifier {

    /**
     * The list of locations in which to look for the java executable in
     * candidate VM install locations, relative to the VM install location. Code
     * from org.eclipse.mtj.core.model.implJavaEmulatorDevice. TODO Refactor to
     * have a common place for it
     */
    private static final String[] CANDIDATE_JAVA_LOCATIONS = {
            "bin" + File.separatorChar + "java", //$NON-NLS-2$ //$NON-NLS-1$
            "bin" + File.separatorChar + "java.exe", //$NON-NLS-2$ //$NON-NLS-1$
            "jre" + File.separatorChar + "bin" + File.separatorChar + "java", //$NON-NLS-3$ //$NON-NLS-2$ //$NON-NLS-1$
            "jre"   + File.separatorChar + "bin" + File.separatorChar + "java.exe" }; //$NON-NLS-3$ //$NON-NLS-2$ //$NON-NLS-1$

    /**
     * Max number of characters in the command
     */
    private static final int MAX_COMMAND_LENGTH = 2000;

    // The regular expression we will use to match the preverify
    // error
    private static final String PREV_ERR_REGEX = "^Unable to access jarfile (\\S*)$";

    //
    private static ProguardPreverifier proguardPreverifier;

    // The compiled pattern for regular expression matching
    private static final Pattern PREV_ERR_PATTERN = Pattern.compile(
            PREV_ERR_REGEX, Pattern.MULTILINE);

    /**
     * singleton implementation
     *
     * @return
     */
    public static ProguardPreverifier getInstance() {
        if (proguardPreverifier == null) {
            proguardPreverifier = new ProguardPreverifier();
        }
        return proguardPreverifier;
    }

    /**
     * Path to proguard lib
     */
    private String proguardJarFilePath = null;

    /**
     * Class constructor
     */
    private ProguardPreverifier() {
        this.proguardJarFilePath = MTJCore.getProguardJarFile()
                .getAbsolutePath();
    }

    public File getPreverifierExecutable() {
        // TODO Auto-generated method stub
        return null;
    }

    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // Not necessary to load any data
    }

    public PreverificationError[] preverify(IMTJProject mtjProject,
            IResource[] toVerify, IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException {
        ArrayList<PreverificationError> allErrors = new ArrayList<PreverificationError>();

        // Create the temporary file of commands for
        // the verifier
        ensureFolderExists(outputFolder, monitor);

        // construct the command line to call proguard
        ArrayList<String> baseArguments = constructCommandLine();

        // read the final arguments that will be added to proguard command line.
        // those parameters should be added
        // after the -injar parameter
        File outputFile = outputFolder.getLocation().toFile();
        String[] configurationParameters = getProguardFinalConfigurationParameters(
                mtjProject, outputFile.getAbsolutePath());

        ArrayList<String> arguments = new ArrayList<String>(baseArguments);

        // add the output folder
        addOutputToInJar(arguments, mtjProject);

        for (int i = 0; i < toVerify.length; i++) {
            IResource resource = toVerify[i];

            // add projects and jar files
            switch (resource.getType()) {
                case IResource.FOLDER:
                case IResource.PROJECT:
                    addFileToInJar(arguments, resource.getLocation()
                            .toOSString());
                    break;

                case IResource.FILE:
                    if (resource.getName().endsWith(".jar")) {
                        addFileToInJar(arguments, resource.getLocation()
                                .toOSString());
                    }
                    break;
            }

            if (commandLength(arguments) > MAX_COMMAND_LENGTH) {
                // Configuration parameters
                for (String configuration : configurationParameters) {
                    arguments.add(configuration);
                }

                // Launch the system process
                String[] commandLine = (String[]) arguments
                        .toArray(new String[arguments.size()]);
                PreverificationError[] errors = runPreverifier(commandLine,
                        null, monitor);
                allErrors.addAll(Arrays.asList(errors));

                arguments = new ArrayList<String>(baseArguments);
                // add the output folder
                addOutputToInJar(arguments, mtjProject);
            }
        }

        if (arguments.size() != baseArguments.size()) {
            for (String configuration : configurationParameters) {
                arguments.add(configuration);
            }

            // Launch the system process
            String[] commandLine = (String[]) arguments
                    .toArray(new String[arguments.size()]);
            PreverificationError[] errors = runPreverifier(commandLine, null,
                    monitor);
            allErrors.addAll(Arrays.asList(errors));
        }

        return (PreverificationError[]) allErrors
                .toArray(new PreverificationError[allErrors.size()]);
    }

    public PreverificationError[] preverifyJarFile(IMTJProject mtjProject,
            File jarFile, IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException {
        // Rather than trying to preverify a jar file, we will expand it
        // first and then preverify against the expanded classes.
        File srcDirectory = new File("");
        try {
            srcDirectory = TemporaryFileManager.instance.createTempDirectory(
                    jarFile.getName().replace('.', '_') + "_", ".tmp");
        } catch (IOException ioe) {
            IStatus status = new Status(IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID, "Failed to create directory.",
                    ioe);
            throw new CoreException(status);
        }
        srcDirectory.mkdirs();

        try {
            Utils.extractArchive(jarFile, srcDirectory);
        } catch (SecurityException se) {
            IStatus status = new Status(IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID,
                    "Failed to inflate jar file due to a security violation.",
                    se);
            throw new CoreException(status);
        } catch (IOException ioe) {
            IStatus status = new Status(IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID, "Failed to inflate jar file.",
                    ioe);
            throw new CoreException(status);
        }

        // Create the target directory for the preverification. We will
        // tell the preverifier to use this when doing the preverification.
        File tgtDirectory = new File("");
        try {
            tgtDirectory = TemporaryFileManager.instance.createTempDirectory(
                    jarFile.getName().replace('.', '_') + "_", ".tmp");
        } catch (IOException ioe) {
            IStatus status = new Status(IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID, "Failed to create directory.",
                    ioe);
            throw new CoreException(status);
        }
        tgtDirectory.mkdirs();

        ArrayList<String> arguments = constructCommandLine();
        arguments.add(srcDirectory.toString());

        // Launch the system process
        String[] commandLine = (String[]) arguments
                .toArray(new String[arguments.size()]);
        PreverificationError[] errors = runPreverifier(commandLine, null,
                monitor);

        // TODO we need to test the outcome of the previous before going much
        // further
        // here...
        // Copy all of the non-class resources so they end up back in the
        // jar file
        FileFilter classFilter = new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.isDirectory()
                        || !pathname.getName().endsWith(".class");
            }
        };
        try {
            Utils.copy(srcDirectory, tgtDirectory, classFilter);
        } catch (SecurityException se) {
            IStatus status = new Status(
                    IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID,
                    "Failed copy specified source due to a security violation.",
                    se);
            throw new CoreException(status);
        } catch (IOException ioe) {
            IStatus status = new Status(IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID,
                    "Failed to copy specified source.", ioe);
            throw new CoreException(status);
        }

        // Finally, re-jar the output of the pre-verification into the requested
        // jar file...
        File outputJarFile = new File(outputFolder.getLocation().toFile(),
                jarFile.getName());
        try {
            Utils.createArchive(outputJarFile, tgtDirectory);
        } catch (IOException ioe) {
            IStatus status = new Status(IStatus.ERROR,
                    IMTJCoreConstants.PLUGIN_ID,
                    "Failed to create zip source folder.", ioe);
            throw new CoreException(status);
        }

        return errors;
    }

    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        // Not necessary to store any data
    }

    /**
     * Add any file to be preverified
     *
     * @param args
     * @param resource
     * @throws JavaModelException
     */
    private void addFileToInJar(List<String> args, String filePath)
            throws JavaModelException {
        if (filePath != null) {
            args.add("-injars");
            args.add("'" + filePath + "'");
        }
    }

    /**
     * Add the output folder target to be verified.
     *
     * @param args
     * @param resource
     * @throws JavaModelException
     */
    private void addOutputToInJar(List<String> args, IMTJProject mtjProject)
            throws JavaModelException {

        // Find the source directory this class resides in
        String outputPath = null;

        String s1 = mtjProject.getProject().getLocation().toOSString();
        String s2 = mtjProject.getJavaProject().getOutputLocation()
                .removeFirstSegments(1).toOSString();
        // IPath.SEPARATOR
        outputPath = s1 + File.separatorChar + s2;

        if (outputPath != null) {
            args.add("-injars");
            args.add("'" + outputPath + "'");
        }
    }

    /**
     * Return the length of the command-line length given the specified argument
     * list.
     *
     * @param arguments
     * @return
     */
    private int commandLength(ArrayList<String> arguments) {
        int length = 0;

        Iterator<String> iter = arguments.iterator();
        while (iter.hasNext()) {
            Object arg = (Object) iter.next();
            length += arg.toString().length();
            if (iter.hasNext())
                length++;
        }

        return length;
    }

    /**
     * Construct the command line for the specified pre-verification.
     *
     * @param midletProject
     * @param target
     * @return
     * @throws CoreException
     */
    private ArrayList<String> constructCommandLine() throws CoreException {
        ArrayList<String> arguments = new ArrayList<String>();

        // The program we are running...
        arguments.add(this.getJavaExecutable().getAbsolutePath());
        arguments.add("-jar");
        arguments.add(this.proguardJarFilePath);

        return arguments;
    }

    /**
     * Ensure the specified output folder exists or create if it does not
     * already exist.
     *
     * @param folder
     * @param monitor
     * @throws CoreException
     */
    private void ensureFolderExists(IFolder folder, IProgressMonitor monitor)
            throws CoreException {
        // Make sure the output folder exists before we start
        if (!folder.exists()) {
            folder.create(true, true, monitor);
        }
    }

    /**
     * Get the full classpath including all J2ME libraries.
     *
     * @param midletProject
     * @return
     * @throws CoreException
     */
    private String getFullClasspath(IMTJProject mtjProject)
            throws CoreException {
        IJavaProject javaProject = mtjProject.getJavaProject();

        String[] entries = JavaRuntime
                .computeDefaultRuntimeClassPath(javaProject);

        // start in 1 to remove the output folder from the runtime. the output
        // folder should be included in the
        // -injar proguard options
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i < entries.length; i++) {
            if (i != 1) {
                sb.append(File.pathSeparatorChar);
            }
            sb.append("'" + entries[i] + "'");
        }

        return sb.toString();
    }

    /**
     * Return the Java executable to be used for launching this device. Code
     * from org.eclipse.mtj.core.model.implJavaEmulatorDevice. TODO Refactore to
     * have a common place for it
     *
     * @return
     */
    private File getJavaExecutable() {
        File executable = null;

        IVMInstall vmInstall = JavaRuntime.getDefaultVMInstall();
        File installLocation = vmInstall.getInstallLocation();

        for (int i = 0; i < CANDIDATE_JAVA_LOCATIONS.length; i++) {
            String javaLocation = CANDIDATE_JAVA_LOCATIONS[i];
            File javaExecutable = new File(installLocation, javaLocation);
            if (javaExecutable.exists()) {
                executable = javaExecutable;
                break;
            }
        }
        return executable;
    }

    /**
     * Return the parameters to be used for controlling the proguard preverifier
     *
     * @param midletProject
     * @return
     * @throws CoreException if an error occurs working with the MIDlet project.
     */
    private String[] getProguardFinalConfigurationParameters(
            IMTJProject mtjProject, String output) throws CoreException {
        return new String[] { "-outjars", "'" + output + "'", "-libraryjars",
                this.getFullClasspath(mtjProject), "'-ignorewarnings'",
                "-dontusemixedcaseclassnames", "-dontshrink", "-dontoptimize",
                "-dontobfuscate", "-microedition" };
    }

    /**
     * Handle the arrival of text on the error stream.
     *
     * TODO Change to support proguard error messages
     *
     * @param text
     * @param errorList
     */
    private void handleErrorReceived(String text,
            List<PreverificationError> errorList) {
        text = text.trim();
        Matcher matcher = PREV_ERR_PATTERN.matcher(text);
        if (matcher.find()) {
            // Found a match for the error...
            if (matcher.groupCount() > 0) {
                final String classname = matcher.group(1);

                String errorText = "Error preverifying class";
                if (matcher.end() < text.length()) {
                    StringBuffer sb = new StringBuffer(errorText);
                    sb.append(": ");

                    String detail = text.substring(matcher.end());
                    detail = detail.trim();
                    sb.append(detail);
                    errorText = sb.toString();
                }

                IClassErrorInformation classInfo = new IClassErrorInformation() {
                    public String getName() {
                        return classname;
                    }

                    public String getSourceFile() {
                        return null;
                    }
                };

                PreverificationErrorLocation location = new PreverificationErrorLocation(
                        PreverificationErrorLocationType.UNKNOWN_LOCATION,
                        classInfo);
                PreverificationError error = new PreverificationError(
                        PreverificationErrorType.UNKNOWN_ERROR, location, text);
                errorList.add(error);
            }
        } else {
            MTJLogger.log(IStatus.WARNING, text);
        }
    }

    /**
     * Run the preverifier program and capture the errors that occurred during
     * pre-verification.
     *
     * @param commandLine
     * @param environment
     * @throws CoreException
     */
    private PreverificationError[] runPreverifier(String[] commandLine,
            String[] environment, IProgressMonitor monitor)
            throws CoreException {
        final ArrayList<PreverificationError> errorList = new ArrayList<PreverificationError>();

        IProcess process = Utils.launchApplication(commandLine, null,
                environment, "Preverifier", "CLDC Preverifier");

        // Listen on the process output streams
        IStreamsProxy proxy = process.getStreamsProxy();
        if (BuildLoggingConfiguration.getInstance()
                .isPreverifierOutputEnabled()) {
            BuildConsoleProxy
                    .getInstance()
                    .traceln(
                            "======================== Launching Preverification =========================");
            BuildConsoleProxy.getInstance().addConsoleStreamListener(
                    IBuildConsoleProxy.Stream.ERROR,
                    proxy.getErrorStreamMonitor());
            BuildConsoleProxy.getInstance().addConsoleStreamListener(
                    IBuildConsoleProxy.Stream.OUTPUT,
                    proxy.getOutputStreamMonitor());
        }

        proxy.getErrorStreamMonitor().addListener(new IStreamListener() {
            public void streamAppended(String text, IStreamMonitor monitor) {
                handleErrorReceived(text, errorList);
            }
        });

        // Wait until completion
        while ((!monitor.isCanceled()) && (!process.isTerminated())) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
            ;
        }

        if (BuildLoggingConfiguration.getInstance()
                .isPreverifierOutputEnabled()) {
            BuildConsoleProxy.getInstance().traceln(
                    "======================== Preverification exited with code: "
                            + process.getExitValue());
        }

        return (PreverificationError[]) errorList
                .toArray(new PreverificationError[errorList.size()]);
    }
}