/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Implementing IStatusHandler instances
 *                                notifications.
 *     David Marques (Motorola) - Synchronizing state variable to avoid
 *                                errors caused by auto build calls.
 *     David Marques (Motorola) - Implementing getCurrentState method.
 */
package org.eclipse.mtj.internal.core.build;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.mtj.core.build.MTJBuildState;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;

/**
 *	BuildStateMachine class implements a state machine for
 *  the MTJ project's build process. All states are defined
 *  within the {@link MTJBuildState} enumeration.
 *  
 *  @author David Marques
 */
public class BuildStateMachine {

	private static Map<IMTJProject, BuildStateMachine> map;

	/**
	 * Gets the BuildStateMachine instance for the specified 
	 * {@link IMTJProject} instance.
	 * 
	 * @param mtjProject target project.
	 * @return the BuildStateMachine instance.
	 */
	public static synchronized BuildStateMachine getInstance(IMTJProject mtjProject) {
		BuildStateMachine stateMachine = null;
		
		if (BuildStateMachine.map == null) {
			BuildStateMachine.map = new HashMap<IMTJProject, BuildStateMachine>();
		}
		
		stateMachine = BuildStateMachine.map.get(mtjProject);
		if (stateMachine == null) {
			stateMachine = new BuildStateMachine(mtjProject);
			BuildStateMachine.map.put(mtjProject, stateMachine);
		}
		return stateMachine;
	}
	
	private List<IStatus> buildStatuses;
	private MTJBuildState currentState;
	private IMTJProject   mtjProject;
	private Object        lock;
	
	/**
	 * Creates an instance of a BuildStateMachine for the
	 * specified project.
	 * 
	 * @param mtjProject target project.
	 */
	private BuildStateMachine(IMTJProject mtjProject) {
		this.buildStatuses = new ArrayList<IStatus>();
		this.mtjProject = mtjProject;
		this.lock       = new Object();
	}
	
	/**
	 * Starts the build state machine into the {@link MTJBuildState}
	 * PRE_BUILD state.
	 * 
	 * @param monitor progress monitor.
	 * @throws CoreException Any core error occurred.
	 */
	public synchronized void start(IProgressMonitor monitor) throws CoreException {
		
		synchronized (lock) {			
			this.currentState = null;
		}
		this.buildStatuses.clear();
		this.changeState(MTJBuildState.PRE_BUILD, monitor);
	}
	
	/**
	 * Changes the current build state and notifyes all hooks.
	 * 
	 * @param state new state.
	 * @param monitor progress monitor.
	 * @throws CoreException Any core error occurred.
	 */
	public synchronized void changeState(MTJBuildState state
			, IProgressMonitor monitor) throws CoreException {

		synchronized (lock) {			
			if (this.currentState == null && state != MTJBuildState.PRE_BUILD) {
				throw new CoreException(MTJStatusHandler.newStatus(IStatus.ERROR
						, 999, "Build state machine has not been initialized."));
			}
			if (this.currentState == state) {
				return;
			}
			this.currentState = state;
		}
		
		List<BuildHookInfo> hooks = BuildHooksRegistry.getInstance().getBuildHooks();
		for (BuildHookInfo hook : hooks) {
			try {				
				hook.getHook().buildStateChanged(this.mtjProject, this.currentState, monitor);
			} catch (CoreException e) {
				handleException(e);
			}
		}
		
		synchronized (lock) {			
			if (this.currentState == MTJBuildState.POST_BUILD 
					&& this.buildStatuses.size() > 0x00) {
				// Call status handlers to do any post build actions
				for (IStatus buildStatus : this.buildStatuses) {
					IStatusHandler handler = DebugPlugin.getDefault().getStatusHandler(buildStatus);
					if (handler != null) {
						handler.handleStatus(buildStatus, this);
					}
				}
			}
		}
	}

	/**
	 * Gets the current build state or null if the build state machine is not started
	 * 
	 * @return current state.
	 */
	public MTJBuildState getCurrentState() {
		return this.currentState;
	}
	
	/**
	 * Handles {@link CoreException} instances storing any non
	 * {@link IStatus} that is not IStatus.ERROR statuses in order to
	 * call {@link IStatusHandler} instances.
	 * <br>s
	 * In case an IStatus.ERROR status {@link CoreException} occurs it
	 * just throws the same exception. 
	 * 
	 * @param e source exception.
	 * @throws CoreException the specified exception in case it
	 * has an error status.
	 */
	private void handleException(CoreException e) throws CoreException {
		IStatus status = e.getStatus();
		if (status.getSeverity() == IStatus.ERROR) {
			throw e;
		}
		this.buildStatuses.add(status);
	}
}
