/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.build.preverifier;

import org.eclipse.mtj.core.build.preverifier.IPreverificationError;

/**
 * The description of an error that occurred during preverification.
 * 
 * @author Craig Setera
 */
public class PreverificationError implements IPreverificationError {

    private String detailMessage;
    private IPreverificationErrorLocation errorLocation;
    private PreverificationErrorType errorType;

    /**
     * Constructs a new PreverificationError with the specified type, location
     * and detail message.
     * 
     * @param type enumeration for errors that were found during preverification
     *            (which is saved for later retrieval by the {@link #getType()}
     *            ).
     * @param location the location where the error occurred (which is saved for
     *            later retrieval by the {@link #getLocation()} method).
     * @param detailMessage the detail message (which is saved for later
     *            retrieval by the {@link #getDetail()} method).
     */
    public PreverificationError(PreverificationErrorType type,
            IPreverificationErrorLocation location, String detailMessage) {
        super();

        this.detailMessage = detailMessage;
        this.errorLocation = location;
        this.errorType = type;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.build.preverifier.IPreverificationError#getDetail()
     */
    public String getDetail() {
        return detailMessage;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.build.preverifier.IPreverificationError#getLocation()
     */
    public IPreverificationErrorLocation getLocation() {
        return errorLocation;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.build.preverifier.IPreverificationError#getType()
     */
    public PreverificationErrorType getType() {
        return errorType;
    }
}
