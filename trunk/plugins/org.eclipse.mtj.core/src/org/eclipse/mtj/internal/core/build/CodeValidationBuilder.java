/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.core.build;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.jdt.core.*;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.CodeValidator;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * <code>CodeValidationBuilder</code> starts before JavaBuilder and allows to run Java code validation 
 * from 3rd party plugins based on top of MTJ plugin.
 * Custom Java code validation is performed by subclasses of {@link CodeValidator} class that 
 * can be registered via <code>org.eclipse.mtj.core.codevalidator</code> extension point.
 */
public class CodeValidationBuilder extends IncrementalProjectBuilder {
    private static final String EXT_ID = MTJCore.getMTJCore().getBundle().getSymbolicName() + ".codevalidator"; //$NON-NLS-1$
    private static final String EXT_CLASS_ATTRIBUTE = "class"; //$NON-NLS-1$

    private static Map<IProject, List<IClasspathEntry>> lastSourceClassPath = new HashMap<IProject, List<IClasspathEntry>>();

    public static List<CodeValidator> getCodeValidators(IJavaProject javaProject) {
        List<CodeValidator> result = new ArrayList<CodeValidator>();
        IConfigurationElement[] configurationElements = Platform.getExtensionRegistry().getConfigurationElementsFor(
                EXT_ID);

        if (configurationElements != null) {
            IMidletSuiteProject midletSuiteProject = javaProject == null ? null : MidletSuiteFactory.getMidletSuiteProject(javaProject);

            for (IConfigurationElement element : configurationElements) {
                CodeValidator codeValidator;

                try {
                    codeValidator = (CodeValidator) element.createExecutableExtension(EXT_CLASS_ATTRIBUTE);
                } catch (Throwable t) {
                    MTJLogger.log(IStatus.ERROR, t);

                    codeValidator = null;
                }

                if (codeValidator != null && (midletSuiteProject == null || codeValidator.supportsValidationFor(midletSuiteProject))) {
                    result.add(codeValidator);
                }
            }
        }

        return result;
    }

    @Override
    protected IProject[] build(int kind, Map<String, String> args, IProgressMonitor monitor) throws CoreException {
        IProject project = getProject();
        List<IClasspathEntry> oldSourceCP = lastSourceClassPath.get(project);
        List<IClasspathEntry> newSourceCP = new ArrayList<IClasspathEntry>();
        IJavaProject javaProject = JavaCore.create(project);
        IClasspathEntry[] projectCP = javaProject.getResolvedClasspath(true);
        List<CodeValidator> codeValidators = getCodeValidators(javaProject);

        if (projectCP != null) {
            for (IClasspathEntry cpEntry : projectCP) {
                if (cpEntry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
                    newSourceCP.add(cpEntry);
                }
            }
        }

        lastSourceClassPath.put(project, newSourceCP);

        if (kind == FULL_BUILD) {
            fullBuild(codeValidators);
        } else {

            IResourceDelta delta = getDelta(project);

            if (delta == null) {
                fullBuild(codeValidators);
            } else {
                if (!compareClasspath(oldSourceCP, newSourceCP)) {
                    fullBuild(codeValidators);
                } else {
                    incrementalBuild(delta, codeValidators);
                }
            }
        }

        return null;
    }

    @Override
    protected void clean(IProgressMonitor monitor) throws CoreException {
        cleanValidationMarkers(getProject());
    }

    private boolean compareClasspath(List<IClasspathEntry> cp1, List<IClasspathEntry> cp2) {
        if (cp1 == null) {
            cp1 = new ArrayList<IClasspathEntry>();
        }

        if (cp2 == null) {
            cp2 = new ArrayList<IClasspathEntry>();
        }

        if (cp1.size() != cp2.size()) {
            return false;
        }

        for (int i = 0; i < cp1.size(); i++) {
            if (!cp1.get(i).equals(cp2.get(i))) {
                return false;
            }
        }

        return true;
    }

    private void cleanValidationMarkers(IResource resource) throws CoreException {
        if (resource.exists()) {
            resource.deleteMarkers(IMTJCoreConstants.JAVAME_VALIDATION_PROBLEM_MARKER, false, IResource.DEPTH_INFINITE);
        }
    }

    private void fullBuild(List<CodeValidator> codeValidators) throws CoreException {
        IProject project = getProject();

        cleanValidationMarkers(project);
        project.accept(new ResourceVisitor(codeValidators));
    }

    private void incrementalBuild(IResourceDelta delta, List<CodeValidator> codeValidators) throws CoreException {
        delta.accept(new ResourceDeltaVisitor(codeValidators));
    }


    private class ResourceVisitor implements IResourceVisitor {
        private List<CodeValidator> codeValidators;

        public ResourceVisitor(List<CodeValidator> codeValidators) {
            this.codeValidators = codeValidators;
        }

        public boolean visit(IResource resource) throws CoreException {
            final ICompilationUnit unit = toCompilationUnit(resource);

            if (unit != null) {
                cleanValidationMarkers(resource);

                for (final CodeValidator codeValidator : codeValidators) {
                    SafeRunner.run(new ISafeRunnable() {
                        public void run() throws Exception {
                            codeValidator.validate(unit);
                        }

                        public void handleException(Throwable t) {
                            MTJLogger.log(IStatus.ERROR, t);
                        }
                    });
                }
            }

            return true;
        }

        private ICompilationUnit toCompilationUnit(IResource resource) {
            if (resource == null || resource.getType() != IResource.FILE) {
                return null;
            }

            IJavaElement javaElement = JavaCore.create((IFile) resource);

            if (javaElement != null && javaElement.getElementType() == IJavaElement.COMPILATION_UNIT) {
                IPackageFragmentRoot root = (IPackageFragmentRoot) javaElement.getAncestor(IJavaElement.PACKAGE_FRAGMENT_ROOT);

                if (root != null && root.exists()) {
                    return (ICompilationUnit) javaElement;
                }
            }

            return null;
        }
    }


    private class ResourceDeltaVisitor extends ResourceVisitor implements IResourceDeltaVisitor {
        public ResourceDeltaVisitor(List<CodeValidator> codeValidators) {
            super(codeValidators);
        }

        public boolean visit(IResourceDelta delta) throws CoreException {
            IResource resource = delta.getResource();
            boolean result = false;

            switch (delta.getKind()) {
                case IResourceDelta.ADDED:
                case IResourceDelta.CHANGED:
                    result = visit(resource);
                    break;
            }

            return result;
        }
    }
}
