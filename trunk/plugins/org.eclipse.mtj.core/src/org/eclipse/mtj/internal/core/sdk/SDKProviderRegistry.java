/**
 * Copyright (c) 2009 Research In Motion Limited.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial version
 */
package org.eclipse.mtj.internal.core.sdk;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.IRegistryChangeEvent;
import org.eclipse.core.runtime.IRegistryChangeListener;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.RegistryFactory;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.ISDKProvider;

/**
 * SDKProviderRegistry is a registry for the 
 * org.eclipse.mtj.core.sdkprovider extension point.
 */
public class SDKProviderRegistry implements IRegistryChangeListener {

    private static SDKProviderRegistry instance;
    protected Set<ISDKProvider> sdkProviders; // Uses lazy initialization
    protected ArrayList<ISDKProviderRegistryListener> listenerList;
    
    /**
     * Gets the single class instance.
     * 
     * @return The singleton instance.
     */
    public synchronized static SDKProviderRegistry getInstance() {
        if (instance == null) {
            instance = new SDKProviderRegistry();
        }
        return instance;
    }

    /**
     * Enforce singleton.
     */
    private SDKProviderRegistry() {
        listenerList = new ArrayList<ISDKProviderRegistryListener>();
        RegistryFactory.getRegistry().addRegistryChangeListener(this);
    }
    
    public synchronized void addListener(ISDKProviderRegistryListener listener) {
        listenerList.add(listener);
    }
    
    public synchronized void removeListener(ISDKProviderRegistryListener listener) {
        listenerList.remove(listener);
    }

    /**
     * Gets a snapshot Collection of the registered ISDKProviders. There is no
     * guarantee on sort order.
     * 
     * @return ISDKProvider Collection. The Collection may be empty.
     */
    public synchronized List<ISDKProvider> getSDKProviders() {
        initialize();
        return new ArrayList<ISDKProvider>(sdkProviders);
    }
    
    /**
     * Obtain the number of ISDKProviders registered.
     */
    public synchronized int getSDKProviderCount() {
        initialize();
        return sdkProviders.size();
    }
    
    /**
     * For first time initialization only. Not for refresh.
     */
    protected void initialize() {
        if (sdkProviders == null) {
            sdkProviders = new TreeSet<ISDKProvider>();
            readSdkProviders(false);
        }
    }
    
    /**
     * Refresh the list of SDK providers.
     */
    protected synchronized void refresh() {
        if (sdkProviders == null) {
            sdkProviders = new TreeSet<ISDKProvider>();
            readSdkProviders(true);
        }
    }

    /**
     * Read the SDK providers from the registered extensions to the
     * "sdkProvider" extension point. This only occurs when the instance of this
     * registry is first created, or when a sdkProvider extension is added to
     * or removed from the extension registry.
     * 
     * @param fireNotification Flag to indicate if ISDKProviderRegistryListeners
     *            should be notified about any SDK providers added or removed.
     */
    protected void readSdkProviders(boolean fireNotification) {
        
        String pluginId = MTJCore.getMTJCore().getBundle().getSymbolicName();
        
        if (pluginId == null)
            return;
        
        IExtensionRegistry registry = Platform.getExtensionRegistry();
        Set<ISDKProvider> foundProviders = new TreeSet<ISDKProvider>();
        IConfigurationElement[] providerExtensions = registry
                .getConfigurationElementsFor(pluginId, "sdkprovider"); //$NON-NLS-1$
        
        for (IConfigurationElement providerExtension : providerExtensions) {
            try {
                ISDKProvider sdkProvider = (ISDKProvider) providerExtension
                        .createExecutableExtension("class"); //$NON-NLS-1$
                foundProviders.add(sdkProvider);
            } catch (CoreException e) {
                e.printStackTrace();
                continue;
            }
        }

        Set<ISDKProvider> providersToAdd = new TreeSet<ISDKProvider>(foundProviders);
        providersToAdd.removeAll(sdkProviders);
        Set<ISDKProvider> providersToRemove = new TreeSet<ISDKProvider>(foundProviders);
        providersToRemove.removeAll(providersToAdd);
        addSDKProviders(providersToAdd, fireNotification);
        removeSDKProviders(providersToRemove, fireNotification);
    }
    
    protected synchronized void addSDKProviders(Set<ISDKProvider> providersToAdd,
            boolean fireNotification) throws IllegalArgumentException {
        
        if (providersToAdd == null)
            throw new IllegalArgumentException();
        
        if (!providersToAdd.isEmpty()) {
            sdkProviders.addAll(providersToAdd);
            if (fireNotification) {
                fireSDKProvidersAddedEvent(providersToAdd);
            }
        }
    }
    
    protected synchronized void removeSDKProviders(
            Set<ISDKProvider> providersToRemove, boolean fireNotification)
            throws IllegalArgumentException {
        
        if (providersToRemove == null)
            throw new IllegalArgumentException();
        
        if (!providersToRemove.isEmpty()) {
            sdkProviders.removeAll(providersToRemove);
            if (fireNotification) {
                fireSDKProvidersRemovedEvent(providersToRemove);
            }
        }
    }    
    
    /**
     * Fire an event that indicates a SDK provider has been added.
     * 
     * @param device
     */
    protected synchronized void fireSDKProvidersAddedEvent(final Set<ISDKProvider> providers) {
        final Iterator<ISDKProviderRegistryListener> listeners = listenerList.iterator();
        while (listeners.hasNext()) {
            final ISDKProviderRegistryListener listener = listeners.next();
            Thread t = new Thread(new Runnable() {
                public void run() {
                    listener.sdkProvidersAdded(providers);
                }});
            t.start();
        }
    }

    /**
     * Fire an event that indicates a SDK provider has been removed.
     * 
     * @param device
     */
    protected synchronized void fireSDKProvidersRemovedEvent(final Set<ISDKProvider> providers) {
        Iterator<ISDKProviderRegistryListener> listeners = listenerList.iterator();
        while (listeners.hasNext()) {
            final ISDKProviderRegistryListener listener = listeners.next();
            Thread t = new Thread(new Runnable() {
                public void run() {
                    listener.sdkProvidersRemoved(providers);
                }});
            t.start();
        }
    }

    // << IRegistryChangeListener >>
    public void registryChanged(IRegistryChangeEvent event) {
        refresh();
    }

}
