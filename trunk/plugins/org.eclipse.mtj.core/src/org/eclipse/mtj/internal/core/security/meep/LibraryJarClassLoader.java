/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.core.security.meep;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

/**
 * The class loader that perform class/resource loading from underlining ME library JAR files
 * and does not keep the affected JAR files locked on the file system (in comparison to URLClassLoader).   
 */
class LibraryJarClassLoader extends ClassLoader {
    private Map<File, JarFileDataAccessor> accessors = new HashMap<File, JarFileDataAccessor>();

    public LibraryJarClassLoader() {}

    public void addJarFile(File jarFile) {
        if (jarFile.isFile() && jarFile.exists() && !accessors.containsKey(jarFile)
                && jarFile.getName().endsWith(".jar")) {
            try {
                JarFileDataAccessor accessor = new JarFileDataAccessor(jarFile);

                accessors.put(jarFile, accessor);

            } catch (Throwable t) {}
        }
    }

    @Override
    protected Class< ? > findClass(String name) throws ClassNotFoundException {
        byte[] data;

        try {
            data = getClassData(name);
        } catch (IOException ex) {
            throw new ClassNotFoundException("Could not find class " + name, ex);
        }

        if (data == null) {
            throw new ClassNotFoundException("Could not find class " + name);
        }

        Class< ? > clazz = defineClass(name, data, 0, data.length);
        String pkgName = extractPackageName(name);

        if (pkgName != null) {
            Package pkg = getPackage(pkgName);

            if (pkg == null) {
                definePackage(pkgName, null, null, null, null, null, null, null);
            }
        }

        return clazz;
    }

    private String extractPackageName(String fqnName) {
        int idx = fqnName.lastIndexOf('.');

        return idx >= 0 ? fqnName.substring(0, idx) : null;
    }

    private byte[] getClassData(String name) throws IOException {
        String classEntryName = name.replace('.', '/') + ".class";

        for (JarFileDataAccessor accessor : accessors.values()) {
            if (accessor.hasEntry(classEntryName)) {
                return accessor.getEntryData(classEntryName);
            }
        }

        return null;
    }

    @Override
    public URL findResource(String name) {
        try {
            Enumeration<URL> resources = findResources(name);

            return resources.hasMoreElements() ? resources.nextElement() : null;
        } catch (IOException ex) {
            return null;
        }
    }

    @Override
    public Enumeration<URL> findResources(String name) throws IOException {
        List<URL> result = new ArrayList<URL>();

        for (JarFileDataAccessor accessor : accessors.values()) {
            if (accessor.hasEntry(name)) {
                URL u = accessor.getEntryURL(name);

                if (u != null) {
                    result.add(u);
                }
            }
        }

        return Collections.enumeration(result);
    }
}
