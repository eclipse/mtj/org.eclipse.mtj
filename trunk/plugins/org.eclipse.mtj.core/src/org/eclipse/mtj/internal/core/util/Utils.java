/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards and added serialVersionUID.
 *     David Marques (Motorola) - Adding getJavaProjectSourceDirectories method.
 *     David Marques (Motorola) - Adding project nature management methods
 *     David Marques (Motorola) - Fixing clearContainer method to support removing
 *                                read only files.     
 *     David Marques (Motorola) - Adding codeFormat method.
 *     David Marques (Motorola) - Adding getRequiredProjects method.
 *     David Marques (Motorola) - Moving getKeyringURL() from Metadata.
 *     David Marques (Motorola) - Improving extractsSourceFolderRelativePath
 *                                method to extract relative path from both
 *                                source and build folders.
 *     Diego Sandin (Motorola)  - Moving project nature management methods to 
 *                                MTJNature class.
 *     David Marques (Motorola) - Adding getSourceFolders method.
 *     David Marques (Motorola) - Adding isAutobuilding
 *     David Arag�o (Motorola)  - Add isValidFolderName
 *     Fernando Rocha(Motorola) - Refactor in isValidFolderName and add methods
 *                                for marker management
 *     David Marques (Motorola) - Fixing OS specific behavior.
 */
package org.eclipse.mtj.internal.core.util;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.*;
import org.eclipse.debug.core.*;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IStreamMonitor;
import org.eclipse.debug.core.model.IStreamsProxy;
import org.eclipse.jdt.core.*;
import org.eclipse.jdt.core.formatter.CodeFormatter;
import org.eclipse.jdt.internal.core.BinaryType;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.project.midp.IMetaDataConstants;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;
import org.w3c.dom.Document;

/**
 * Various utility functions.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @since 1.0
 */
@SuppressWarnings("restriction")
public class Utils {

    /**
     * This class is used as part of monitoring process output in the
     * #getProcessOutput method. It monitors output from the process (either
     * stdout or stderr) and appends the results to the specified
     * <code>StringBuffer</code>. The listener approach prevents a process from
     * stalling because output blocks.
     */
    private static class StreamListener implements IStreamListener {
        private StringBuffer buf;

        public StreamListener(StringBuffer buf) {
            this.buf = buf;
        }

        public void streamAppended(String text, IStreamMonitor monitor) {
            if (buf != null) {
                buf.append(text);
            }
        }
    }
    
    /**
     * An empty String : "".
     */
    public static final String EMPTY_STRING = ""; //$NON-NLS-1$

    private static boolean caseSensitiveFileSystem;

    // Track whether the file system is case sensitive
    private static boolean caseSensitivityChecked;

    // Private default file filter
    private static final FileFilter DEFAULT_FILTER = new FileFilter() {
        public boolean accept(File pathname) {
            // Default is to copy all files
            return true;
        }
    };
    // TODO This should probably be handled better...
    private static final String PRIVATE_CONFIGURATION = "org.eclipse.debug.ui.private"; //$NON-NLS-1$

    /**
     * Return a boolean indicating whether the specified paths point to the same
     * file.
     * 
     * @param path1
     * @param path2
     * @return
     * @throws CoreException
     */
    public static boolean arePathTargetsEqual(IPath path1, IPath path2)
            throws CoreException {
        boolean equal = false;

        File file1 = getPathTargetFile(path1);
        File file2 = getPathTargetFile(path2);

        if ((file1 != null) && (file2 != null)) {
            equal = file1.equals(file2);
        }

        return equal;
    }

    /**
     * Clear the specified container of all resources recursively.
     * 
     * @param container the container from which we'll retrieve a list of
     *            existing member resources (projects, folders and files).
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired
     * @throws CoreException if this method fails. Reasons include:
     *             <ul>
     *             <li>the list of existing member resources from the container
     *             could not be retrieved.</li>
     *             <li>This resources could not be deleted for some reason.</li>
     *             <li>This resources or one of its descendants is out of sync
     *             with the local file system.</li>
     *             </ul>
     */
    public static void clearContainer(IContainer container,
            IProgressMonitor monitor) throws CoreException {

        if (container.exists()) {
            IResource[] resources = container.members();

            for (IResource resource : resources) {

                /*Remove read only property in case it is set.*/
                ResourceAttributes attributes = resource
                        .getResourceAttributes();

                if (attributes != null) {
                    if (attributes.isReadOnly()) {
                        attributes.setReadOnly(false);
                        resource.setResourceAttributes(attributes);
                    }

                    if (resource instanceof IContainer) {
                        clearContainer((IContainer) resource, monitor);
                    }

                    if (resource.exists()) {
                        resource.delete(true, monitor);
                    }
                }
            }
        }
    }

    /**
     * Formats the code.
     * 
     * @param project java project.
     * @param sourceString string.
     * @param kind kid of format.
     * @param initialIndentationLevel
     * @param lineDelim
     * @return
     */
    public static String codeFormat(IJavaProject project, String sourceString,
            int kind, int initialIndentationLevel, String lineDelim) {
        CodeFormatter formatter = ToolFactory.createCodeFormatter(project
                .getOptions(true));
        TextEdit edit = formatter.format(kind, sourceString, 0, sourceString
                .length(), initialIndentationLevel, lineDelim);
        if (edit != null) {
            org.eclipse.jface.text.Document document = new org.eclipse.jface.text.Document(
                    sourceString);
            try {
                edit.apply(document);
                return document.get();
            } catch (MalformedTreeException e) {
            } catch (BadLocationException e) {
            }
        }
        return sourceString;
    }

    /**
     * Copy from the specified source to the specified destination, recursively
     * as necessary.
     * 
     * @param source
     * @param destination
     * @throws IOException
     */
    public static void copy(File source, File destination) throws IOException {
        copy(source, destination, null);
    }

    /**
     * Copy from the specified source to the specified destination, recursively
     * as necessary. The file filter will be used to determine what files will
     * be copied along the way.
     * 
     * @param source the element to be copied.
     * @param destination the destination of the copied elements.
     * @param filter filter for abstract pathnames.
     * @throws IOException if fails to copy the contents from a source directory
     *             to the specified destination.
     * @throws SecurityException If a security manager exists and:
     *             <ul>
     *             <li>Denies read access to the directory or does not permit
     *             the named directory and all necessary parent directories to
     *             be created.</li>
     *             <li>Denies read and write access to the file.</li>
     *             </ul>
     */
    public static void copy(File source, File destination, FileFilter filter)
            throws IOException, SecurityException {
        if (source.exists()) {
            if (source.isDirectory()) {
                copyDirectory(source, destination, filter);
            } else {
                copyFile(source, destination, filter);
            }
        }
    }

    /**
     * Copy the specified directory and all of its contents recursively to the
     * specified destination.
     * 
     * @param source the directory from which the resources will be copied.
     * @param destination the directory where the resources will be copied to.
     * @param filter filter for abstract pathnames.
     * @throws IOException if fails to copy the contents of the source directory
     *             to the specified destination.
     * @throws SecurityException If a security manager exists and denies read
     *             access to the directory or does not permit the named
     *             directory and all necessary parent directories to be created
     */
    public static void copyDirectory(File source, File destination,
            FileFilter filter) throws IOException, SecurityException {
        if (filter == null) {
            filter = DEFAULT_FILTER;
        }

        if (source.exists() && source.isDirectory() && filter.accept(source)) {
            destination.mkdirs();

            File[] sources = source.listFiles();
            for (File newSource : sources) {
                File newDestination = new File(destination, newSource.getName());

                if (newSource.isDirectory()) {
                    copyDirectory(newSource, newDestination, filter);
                } else {
                    copyFile(newSource, newDestination, filter);
                }
            }
        }
    }

    /**
     * Copy the specified source file into the specified target file.
     * 
     * @param sourceFile the file to be copied.
     * @param targetFile the file that will be the copy of the passed
     *            sourceFile.
     * @param filter filter for abstract pathnames.
     * @throws IOException if fails reading the sourceFile or writing to the
     *             targetFile.
     * @throws SecurityException If a security manager exists and denies read
     *             and write access to the file.
     */
    public static void copyFile(File sourceFile, File targetFile,
            FileFilter filter) throws IOException, SecurityException {
        if (filter == null) {
            filter = DEFAULT_FILTER;
        }
        if (filter.accept(sourceFile)) {
            FileInputStream fis = new FileInputStream(sourceFile);
            
            try {
                FileOutputStream fos = new FileOutputStream(targetFile);
                
                try {
                    copyInputToOutput(fis, fos);
                } finally {
                    fos.close();
                }
            } finally {
                fis.close();
            }
        }
    }

    /**
     * Copy the specified source file into the specified target file.
     * 
     * @param sourceFile the file to be copied.
     * @param targetFile the file that will be the copy of the passed
     *            sourceFile.
     * @throws IOException if fails reading the sourceFile or writing to the
     *             targetFile.
     * @throws SecurityException If a security manager exists and denies read
     *             and write access to the file.
     */
    public static void copyFile(IFile sourceFile, IFile targetFile)
            throws IOException, SecurityException {
        copyFile(sourceFile.getLocation().toFile(), targetFile.getLocation()
                .toFile(), null);
    }

    /**
     * Copy the contents of the input to the output stream.
     * 
     * @param input an input stream of bytes.
     * @param output an output stream of bytes.
     * @throws IOException if fails reading the input or writing to the output
     */
    public static void copyInputToOutput(InputStream input, OutputStream output)
            throws IOException {
        input = new BufferedInputStream(input);
        output = new BufferedOutputStream(output);
        
        int b;
        
        while ((b = input.read()) != -1) {
            output.write(b);
        }

        output.flush();
    }

    /**
     * Create a new zip archive with the contents specified by the source
     * folder.
     * 
     * @param tgtArchiveFile the new zip archive that will contain the elements
     *            available in the srcFolder.
     * @param srcFolder the folder from which the elements will be compressed in
     *            the new zip archive
     * @throws IOException if the tgtArchiveFile cannot be opened for any
     *             reason.
     * @throws SecurityException if a security manager exists and denies write
     *             or read access to the tgtArchiveFile or srcFolder.
     */
    public static void createArchive(File tgtArchiveFile, File srcFolder)
            throws IOException {

        if (srcFolder.exists() && srcFolder.isDirectory()) {
            ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(
                    tgtArchiveFile));
            try {
                addFolderToArchive(zos,
                        srcFolder.getAbsolutePath().length() + 1, srcFolder);
            } finally {
                if (zos != null) {
                    try {
                        zos.close();
                    } catch (IOException e) {
                    }
                }
            }
        }
    }

    /**
     * Delete the specified file, recursively as necessary.
     * 
     * @param file
     */
    public static void delete(File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File file2 : files) {
                    delete(file2);
                }
            }

            file.delete();
        }
    }

    /**
     * If the system property "mtj.dump.launch" is set to "true", dump the
     * command line that was used to launch the emulator.
     */
    public static void dumpCommandLine(ILaunch launch) {
        // Search for the process command-line
        IProcess[] processes = launch.getProcesses();
        if ((processes != null) && (processes.length > 0)) {
            IProcess process = processes[0];
            if (process != null) {
                String commandLine = process
                        .getAttribute(IProcess.ATTR_CMDLINE);
                dumpCommandLine(commandLine);
            }
        }
    }

    /**
     * If the system property "mtj.dump.launch" is set to "true", dump the
     * command line that was used to launch the emulator.
     * 
     * @param commandLine
     */
    public static void dumpCommandLine(String commandLine) {
        // Pull the dump choice from system properties
        String propValue = System.getProperty(
                IMTJCoreConstants.PROP_DUMP_LAUNCH, Boolean.FALSE.toString());
        boolean doDump = propValue.equalsIgnoreCase(Boolean.TRUE.toString());

        // Only do this if requested
        if (doDump) {
            // Let the user know what happened via the log file
            String text = (commandLine == null) ? org.eclipse.mtj.internal.core.Messages.Utils_commandLine_not_found
                    : org.eclipse.mtj.internal.core.Messages.Utils_commandLine
                            + commandLine;
            MTJLogger.log(IStatus.INFO, text);
        }
    }

    /**
     * If the system property "mtj.dump.launch" is set to "true", dump the
     * command line that was used to launch the emulator.
     * 
     * @param commandLine
     */
    public static void dumpCommandLine(String[] commandLine) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < commandLine.length; i++) {
            String string = commandLine[i];
            if (i != 0) {
                sb.append(' ');
            }
            sb.append(string);
        }

        dumpCommandLine(sb.toString());
    }

    /**
     * Performs a runtime exec on the given command line via the Debug plug-in.
     * 
     * @param commandLine the command line.
     * @param workingDirectory the working directory, or <code>null</code>.
     * @param env The environment to be used or <code>null</code> to take the
     *            default.
     * @return the resulting process or null if the exec is canceled.
     * @throws CoreException if the exec fails.
     */
    public static Process exec(String[] commandLine, File workingDirectory,
            String[] env) throws CoreException {
        return DebugPlugin.exec(commandLine, workingDirectory, env);
    }

    /**
     * Test if the specified executable file exist. Add a ".exe" if necessary to
     * handle Windows systems.
     * 
     * @param executableFile
     * @return
     */
    public static boolean executableExists(File executableFile) {
        File winExecutable = new File(executableFile.getParentFile(),
                executableFile.getName() + ".exe"); //$NON-NLS-1$

        return executableFile.exists() || winExecutable.exists();
    }

    /**
     * Extract the archive specified by the file into the provided directory.
     * 
     * @param jarFile
     * @param tgtDirectory
     * @throws IOException if failed to read a ZIP file entry
     * @throws SecurityException If a security manager exists and does not
     *             permit the named directory and all necessary parent
     *             directories to be created
     */
    public static void extractArchive(File jarFile, File tgtDirectory)
            throws IOException, SecurityException {

        ZipInputStream zis = new ZipInputStream(new FileInputStream(jarFile));

        try {
            ZipEntry zipEntry = null;

            while ((zipEntry = zis.getNextEntry()) != null) {

                if (zipEntry.isDirectory()) {
                    File directory = new File(tgtDirectory, zipEntry.getName());
                    directory.mkdirs();
                } else {
                    File zipEntryFile = new File(tgtDirectory, zipEntry
                            .getName());
                    File zipEntryParentFile = zipEntryFile.getParentFile();

                    if (zipEntryParentFile != null) {

                        zipEntryParentFile.mkdirs();
                        FileOutputStream fos = new FileOutputStream(
                                zipEntryFile);
                        try {
                            Utils.copyInputToOutput(zis, fos);
                        } finally {
                            if (fos != null) {
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                }
                            }
                        }
                    }
                }
            }
        } finally {
            try {
                zis.close();
            } catch (IOException e) {
            }
        }
    }

    /**
     * Extract the class name from the specified IResource within the specified
     * java project.
     * 
     * @param javaProject the java project to provide the relative name
     * @param resource the resource to extract a class name
     * @return the class name or <code>null</code> if the resource name cannot
     *         be converted for some reason.
     * @throws JavaModelException
     */
    public static String extractClassName(IJavaProject javaProject,
            IResource resource) throws JavaModelException {
        IPath classPath = extractsSourceFolderRelativePath(javaProject,
                resource);
        return (classPath == null) ? null : classPath.removeFileExtension()
                .toString().replace('/', '.');
    }

    /**
     * Extract the class path from the specified IResource within the specified
     * java project.
     * 
     * @param javaProject target project.
     * @param resource target resource.
     * @return the extracted resource path
     * @throws JavaModelException
     */
    public static IPath extractsSourceFolderRelativePath(
            IJavaProject javaProject, IResource resource)
            throws JavaModelException {
        return extractsSourceFolderRelativePath(javaProject, resource, false);
    }

    /**
     * Extract the class path from the specified IResource within the specified
     * java project.
     * 
     * @param javaProject target project.
     * @param resource target resource.
     * @param isSource true if resource is inside source folder, false if it is
     *            inside build folder.
     * @return the extracted resource path
     * @throws JavaModelException
     */
    public static IPath extractsSourceFolderRelativePath(
            IJavaProject javaProject, IResource resource, boolean isSource)
            throws JavaModelException {
        IPath resultPath = null;
        IPath projectOutputPath = javaProject.getOutputLocation()
                .makeAbsolute();

        IPath resourcePath = resource.getFullPath();

        IClasspathEntry[] classpath = javaProject.getRawClasspath();
        for (IClasspathEntry entry : classpath) {
            if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
                IPath entryPath = null;
                if (isSource) {
                    entryPath = entry.getPath().makeAbsolute();
                } else {
                    entryPath = entry.getOutputLocation();
                    entryPath = (entryPath == null) ? projectOutputPath
                            : entryPath.makeAbsolute();
                }

                if (entryPath.isPrefixOf(resourcePath)) {
                    resultPath = resourcePath.removeFirstSegments(entryPath
                            .segmentCount());
                    break;
                }
            }
        }

        return resultPath;
    }

    /**
     * Return the Javadoc classpath attribute or <code>null</code> if none is
     * attached.
     * 
     * @return
     */
    public static IClasspathAttribute getJavadocAttribute(IClasspathEntry entry) {
        int index = getJavadocAttributeIndex(entry);
        return (index == -1) ? null : entry.getExtraAttributes()[index];
    }

    /**
     * Return the Javadoc classpath attribute index or <code>-1</code> if none
     * is attached.
     * 
     * @return
     */
    public static int getJavadocAttributeIndex(IClasspathEntry entry) {
        int index = -1;

        IClasspathAttribute[] attributes = entry.getExtraAttributes();
        for (int i = 0; i < attributes.length; i++) {
            IClasspathAttribute attribute = attributes[i];
            if (attribute.getName().equals(
                    IClasspathAttribute.JAVADOC_LOCATION_ATTRIBUTE_NAME)) {
                index = i;
                break;
            }
        }

        return index;
    }

    /**
     * Gets the source directories in a java project.
     * 
     * @param javaProject project instance.
     * @return an array containing the source directories.
     * @throws CoreException If an error occurs.
     */
    public static String[] getJavaProjectSourceDirectories(
            IJavaProject javaProject) throws CoreException {
        List<String> paths = new LinkedList<String>();

        IClasspathEntry[] classpathEntries = null;

        classpathEntries = javaProject.getResolvedClasspath(true);
        for (IClasspathEntry entry : classpathEntries) {
            if ((entry.getEntryKind() == IClasspathEntry.CPE_SOURCE)
                    && (entry.getContentKind() == IPackageFragmentRoot.K_SOURCE)) {
                IPath path = entry.getPath();
                paths.add(path.lastSegment());
            }
        }
        return paths.toArray(new String[paths.size()]);
    }

    /**
     * Gets the keyring url for the specified project.
     * 
     * @return keyring url.
     */
    public static String getKeyringURL(IProject project) {
        StringBuffer buf = new StringBuffer();
        buf.append(IMetaDataConstants.KEYRING_URL_BASE);

        String projectName = project.getName();
        int nLength = projectName.length();
        for (int i = 0; i < nLength; i++) {
            char c = projectName.charAt(i);
            if (Character.isLetterOrDigit(c) || (c == '.')) {
                buf.append(c);
            } else {
                buf.append('%');
                String hexString = Integer.toHexString(c);
                if ((hexString.length() & 0x01) != 0) {
                    buf.append('0');
                }
                buf.append(hexString);
            }
        }
        return buf.toString();
    }

    /**
     * Retrieve all MIDlets in the given java project
     * 
     * @param monitor
     * @param project
     * @return a list of MIDlet type
     */
    public static List<IType> getMidletTypesInProject(IProgressMonitor monitor,
            IJavaProject project) {
        IType[] types;
        HashSet<IType> result = new HashSet<IType>(5);
        try {
            IType midlet = project
                    .findType(IMTJCoreConstants.MIDLET_SUPERCLASS);
            ITypeHierarchy hierarchy = midlet.newTypeHierarchy(project,
                    new SubProgressMonitor(monitor, 1));
            types = hierarchy.getAllSubtypes(midlet);
            int length = types.length;
            if (length != 0) {
                for (int i = 0; i < length; i++) {
                    if (!types[i].isBinary()) {
                        result.add(types[i]);
                    }
                }
            }
        } catch (JavaModelException jme) {
        }
        monitor.done();
        return new ArrayList<IType>(result);
    }

    /**
     * Get a new launch instance with the specified configuration name.
     * 
     * @param configName
     * @return
     * @throws CoreException
     */
    public static ILaunch getNewLaunch(String configName) throws CoreException {
        return new Launch(getNewLaunchConfiguration(configName),
                ILaunchManager.RUN_MODE, null);
    }

    /**
     * Get the launch configuration to be used in building the ILaunch instance.
     * 
     * @param name name for the launch configuration
     * @return Returns a new launch configuration working copy of this type,
     *         that resides locally with the metadata.
     * @throws CoreException if an instance of this type of launch configuration
     *             could not be created for any reason.
     */
    public static ILaunchConfiguration getNewLaunchConfiguration(String name)
            throws CoreException {
        ILaunchManager launchManager = DebugPlugin.getDefault()
                .getLaunchManager();
        ILaunchConfigurationType configType = launchManager
                .getLaunchConfigurationType(IJavaLaunchConfigurationConstants.ID_JAVA_APPLICATION);

        ILaunchConfigurationWorkingCopy config = configType.newInstance(null,
                name);
        config.setAttribute(PRIVATE_CONFIGURATION, true);

        return config;
    }

    /**
     * Return the resource that is resolved for the specified path. This will
     * return one of three things:
     * <ul>
     * <li>An instance of org.eclipse.core.resources.IResource if the entry
     * refers to a workspace resource.</li>
     * <li>An instance of java.io.File if the entry refers to a non-workspace
     * resource.</li>
     * <li><code>null</code> if the entry points to a non-existent resource.</li>
     * </ul>
     * 
     * @param path the path to look for an object
     * @return the resolved object
     */
    public static Object getPathTarget(IPath path) {
        Object target = null;

        IWorkspaceRoot root = MTJCore.getWorkspace().getRoot();
        target = root.findMember(path);
        if (target == null) {
            File externalFile = new File(path.toOSString());
            if ((externalFile != null) && (externalFile.isFile())) {
                target = externalFile;
            } else {
                target = null;
            }
        }

        return target;
    }

    /**
     * Return the java.io.File instance referenced by the specified path or
     * <code>null</code> if no such file exists.
     * 
     * @param entry
     * @return
     * @throws CoreException
     */
    public static File getPathTargetFile(IPath path) throws CoreException {
        File entryFile = null;

        Object pathTarget = getPathTarget(path);

        if (pathTarget instanceof IResource) {
            entryFile = ((IResource) pathTarget).getLocation().toFile();
        } else if (pathTarget instanceof File) {
            entryFile = (File) pathTarget;
        }

        return entryFile;
    }

    /**
     * Execute a process and collect its stdout and stderr.
     * 
     * @param name Name of the process for display purposes
     * @param commandLine Array of <code>String</code>s that provides the
     *            command line. The first entry in the array is the executable.
     * @param stdout <code>StringBuffer</code> to which the process' stdout will
     *            be appended. May be <code>null</code>.
     * @param stderr <code>StringBuffer</code> to which the process' stderr will
     *            be appended. May be <code>null</code>.
     * @return The exit value of the process.
     * @throws CoreException if the execution of the command line fails.
     */
    public static int getProcessOutput(String name, String[] commandLine,
            StringBuffer stdout, StringBuffer stderr) throws CoreException {
        int result = 0x00;
        
        String os = Platform.getOS();
        if (os.toLowerCase().contains("win32")) {
            IProcess process = Utils.launchApplication(commandLine, null, null,
                    name, name);

            // Listen on the process output streams
            IStreamsProxy proxy = process.getStreamsProxy();
            
            collectStreamContents(proxy.getOutputStreamMonitor(), stdout);
            collectStreamContents(proxy.getErrorStreamMonitor(), stderr);

            // Wait until completion
            while (!process.isTerminated()) {
                try {
                    Thread.sleep(1000); // sleep for one second
                } catch (InterruptedException e) {

                }
            }
            
            result = process.getExitValue();
        } else {
            Process execProcess = exec(commandLine, null, null);
            try {
                    result = execProcess.waitFor();
            } catch (InterruptedException e) {
                    // Continue...
            }
            
            readOutput(execProcess.getInputStream(), stdout);
            readOutput(execProcess.getErrorStream(), stderr);
        }
		
    	return result;
    }
    
    private static void collectStreamContents(IStreamMonitor monitor, StringBuffer target) {
        synchronized (monitor) {
            String collected = monitor.getContents();
            
            if (collected != null) {
                target.append(collected);
            }
            
            monitor.addListener(new StreamListener(target));
        }
    }

    /**
     * Reads the {@link InputStream} content into the {@link StringBuffer} instance.
     * 
     * @param inputStream source InputStream.
     * @param buffer destination buffer.
     * @throws CoreException Any Core error occurs.
     */
    private static void readOutput(InputStream inputStream, StringBuffer buffer) throws CoreException {
    	try {
        	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));  
        	String line = null;
        	while ((line = reader.readLine()) != null) {
        		buffer.append(line).append("\n");
			}
		} catch (IOException e) {
			throw new CoreException(MTJStatusHandler
					.newStatus(IStatus.ERROR, -999, e.getMessage(), e));
		}
	}

	/**
     * Attempt to retrieve the fully-qualified class name.
     * 
     * @param type
     * @return
     */
    public static String getQualifiedClassName(IType type) {
        String classname = null;

        if (type instanceof BinaryType) {
            classname = getQualifiedClassName((BinaryType) type);
        } else {
            classname = type.getFullyQualifiedName();
        }

        return classname;
    }

    /**
     * Gets the required projects from a project.
     * 
     * @param javaProject target project
     * @return an array with the required projects.
     * @throws JavaModelException any error occurs.
     */
    public static IJavaProject[] getRequiredProjects(IJavaProject javaProject)
            throws JavaModelException {
        List<IJavaProject> required = new ArrayList<IJavaProject>();
        String[] names = javaProject.getRequiredProjectNames();
        for (String name : names) {
            IProject project = MTJCore.getWorkspace().getRoot()
                    .getProject(name);
            if (project.exists() && project.isOpen()) {
                required.add(JavaCore.create(project));
            }
        }
        return required.toArray(new IJavaProject[required.size()]);
    }

    /**
     * Return the resource that is resolved for the specified classpath entry.
     * This will return one of three things:
     * <ul>
     * <li>An instance of org.eclipse.core.resources.IResource if the entry
     * refers to a workspace resource.</li>
     * <li>An instance of java.io.File if the entry refers to a non-workspace
     * resource.</li>
     * <li><code>null</code> if the entry points to a non-existent resource.</li>
     * </ul>
     * 
     * @param entry the entry to be resolved
     * @return the resolved object
     */
    public static Object getResolvedClasspathEntry(IClasspathEntry entry) {
        IClasspathEntry resolved = JavaCore.getResolvedClasspathEntry(entry);
        return getPathTarget(resolved.getPath());
    }

    /**
     * Return the java.io.File instance referenced by the specified classpath
     * entry or <code>null</code> if no such file exists.
     * 
     * @param entry
     * @return
     * @throws CoreException
     */
    public static final File getResolvedClasspathEntryFile(IClasspathEntry entry)
            throws CoreException {
        IClasspathEntry resolved = JavaCore.getResolvedClasspathEntry(entry);
        return getPathTargetFile(resolved.getPath());
    }

    /**
     * Launch the specified command line and return the output from the standard
     * output.
     * 
     * @param name
     * @param commandLine
     * @return
     * @throws CoreException
     */
    public static String getStandardOutput(String name, String[] commandLine)
            throws CoreException {
        return getStandardOutput(name, commandLine, null);
    }

    /**
     * Launch the specified command line and return the output from the standard
     * output.
     * 
     * @param name name to be used for the launch configuration and label
     *            assigned to the created process
     * @param commandLine the command line to be executed.
     * @return the output value from the standard output after executing the
     *         command line.
     * @throws CoreException if the specified command line fails fails to be
     *             launched.
     */
    public static String getStandardOutput(String name, String[] commandLine,
            File workingDirectory) throws CoreException {

        IProcess process = Utils.launchApplication(commandLine,
                workingDirectory, null, name, name);

        // Listen on the process output streams
        IStreamsProxy proxy = process.getStreamsProxy();

        // Wait until completion
        while (!process.isTerminated()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            ;
        }

        return proxy.getOutputStreamMonitor().getContents();
    }

    /**
     * Reads the content from a stream into a string.
     * 
     * @param stream input stream.
     * @return the stream content.
     */
    public static String getStreamContent(InputStream stream) {
        String result = null;

        StringBuffer buffer = new StringBuffer();
        int c = -1;
        try {
            while ((c = stream.read()) != -1) {
                buffer.append((char) c);
            }
            result = buffer.toString();
        } catch (IOException e) {
            /* NOOP */
        }
        return result;
    }

    /**
     * @param in
     * @return
     */
    public static org.eclipse.jface.text.Document getTextDocument(InputStream in) {
        ByteArrayOutputStream output = null;
        String result = null;
        try {
            output = new ByteArrayOutputStream();

            byte buffer[] = new byte[1024];
            int count;
            while ((count = in.read(buffer, 0, buffer.length)) > 0) {
                output.write(buffer, 0, count);
            }

            result = output.toString("UTF-8"); //$NON-NLS-1$
            output.close();
            output = null;
            in.close();
            in = null;
        } catch (IOException e) {
            // close open streams
            if (output != null) {
                try {
                    output.close();
                } catch (IOException ee) {
                }
            }

            if (in != null) {
                try {
                    in.close();
                } catch (IOException ee) {
                }
            }
        }
        return result == null ? null : new org.eclipse.jface.text.Document(
                result);
    }

    /**
     * Gets a Document instance of the XML file on the specified path.
     * 
     * @param path XML file path.
     * @return a Document instance representing an in memory xml structure.
     * @throws IOException - If any loading / parsing error occurs.
     */
    public static Document getXmlDocument(IPath path) throws IOException {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            return builder.parse(path.toFile());
        } catch (Exception e) {
            throw new IOException(Messages.Utils_xml_parse_error);
        }
    }

    /**
     * Return a boolean indicating whether the host file system appears to be
     * case sensitive.
     * 
     * @return case sensitivity of the host file system
     */
    public static boolean isFileSystemCaseSensitive() {
        if (!caseSensitivityChecked) {
            caseSensitivityChecked = true;

            Location location = Platform.getInstallLocation();
            if (location != null) {
                URL url = location.getURL();
                if (url != null) {
                    String urlString = url.toString();
                    if (urlString.startsWith("file:/")) { //$NON-NLS-1$
                        urlString = urlString.substring("file:/".length()) //$NON-NLS-1$
                                .toUpperCase();
                        caseSensitiveFileSystem = !(new File(urlString))
                                .exists();
                    }
                }
            }
        }

        return caseSensitiveFileSystem;
    }

    /**
     * Return a boolean indicating whether the specified type is a MIDlet
     * subclass.
     * 
     * @param type
     * @return
     */
    public static boolean isMidlet(IType type, IProgressMonitor monitor)
            throws JavaModelException {
        boolean isMidlet = false;

        if (type != null) {
            IJavaProject javaProject = type.getJavaProject();

            if (!type.exists() && type.isBinary()) {
                // A binary type, which won't help much... Attempt to convert
                // to a source type and use that to do the lookup
                String classname = getQualifiedClassName(type);
                if (classname != null) {
                    IType sourceType = javaProject.findType(classname);
                    isMidlet = isMidlet(sourceType, monitor);
                }
            } else {
                ITypeHierarchy typeHierarchy = type
                        .newSupertypeHierarchy(monitor);
                IType midletType = javaProject
                        .findType(IMTJCoreConstants.MIDLET_SUPERCLASS);

                isMidlet = (midletType != null)
                        && typeHierarchy.contains(midletType);
            }
        }

        return isMidlet;
    }

    /**
     * Launch a new application and return the IProcess representing the
     * application.
     * 
     * @param commandLine the command line to be executed.
     * @param workingDirectory the working directory, or <code>null</code>.
     * @param environment the environment to be used or null to take the
     *            default.
     * @param configName name for the launch configuration
     * @param label the label assigned to the process
     * @return the process representing the application
     * @throws CoreException if the creation of the the system process fails.
     */
    public static IProcess launchApplication(String[] commandLine,
            File workingDirectory, String[] environment, String configName,
            String label) throws CoreException {

        // Execute the process.
        Process execProcess = exec(commandLine, workingDirectory, environment);

        // Wrap it up in a JDT IProcess instance
        Launch launch = new Launch(getNewLaunchConfiguration(configName),
                ILaunchManager.RUN_MODE, null);

        IProcess process = DebugPlugin.newProcess(launch, execProcess, label);
        Utils.dumpCommandLine(commandLine);

        return process;
    }

    /**
     * Switches workspace auto build on/off.
     * 
     * @param state if true turns on auto build, if false
     * turns it off. 
     * @throws CoreException Any error occurred while setting
     * the workspace auto build settings.
     */
    public static void switchAutoBuild(boolean state) throws CoreException {
    	IWorkspace workspace = MTJCore.getWorkspace();
		IWorkspaceDescription description = workspace.getDescription();
		description.setAutoBuilding(state);
		workspace.setDescription(description);
    }
    
    /**
     * Gets the workspace's auto build state.
     * 
     * @return true if auto build is on false otherwise.
     */
    public static boolean isAutoBuilding() {
    	IWorkspace workspace = MTJCore.getWorkspace();
		IWorkspaceDescription description = workspace.getDescription();
		return description.isAutoBuilding();
    }
    
    /**
     * Add the specified file as an entry to the archive output stream.
     * 
     * @param zos
     * @param prefixLength
     * @param file
     * @throws IOException
     */
    private static void addFileEntryToArchive(ZipOutputStream zos,
            int prefixLength, File file) throws IOException {
        // Create a new zip entry
        ZipEntry entry = getEntryForFile(file, prefixLength);
        zos.putNextEntry(entry);

        // Write the contents for the new zip entry
        FileInputStream fis = new FileInputStream(file);
        try {
            copyInputToOutput(fis, zos);
        } finally {
            zos.closeEntry();

            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                }
            }
        }
    }

    /**
     * Add the contents of the specified folder to the archive being created.
     * 
     * @param zos
     * @param prefixLength
     * @param srcFolder
     * @throws IOException
     */
    private static void addFolderToArchive(ZipOutputStream zos,
            int prefixLength, File srcFolder) throws IOException {
        File[] files = srcFolder.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                addFolderToArchive(zos, prefixLength, file);
            } else {
                addFileEntryToArchive(zos, prefixLength, file);
            }
        }
    }

    /**
     * @param file
     * @param prefixLength
     * @return
     */
    private static ZipEntry getEntryForFile(File file, int prefixLength) {
        String fileString = file.getAbsolutePath();

        String entryName = fileString.substring(prefixLength)
                .replace('\\', '/');
        ZipEntry entry = new ZipEntry(entryName);
        entry.setSize(file.length());
        entry.setTime(file.lastModified());
        return entry;
    }

    /**
     * Attempt to retrieve the fully-qualified class name.
     * 
     * @param type
     * @return
     */
    private static String getQualifiedClassName(BinaryType type) {
        IJavaElement javaElement = type.getPackageFragment();
        StringBuffer name = new StringBuffer(type.getElementName());

        while (javaElement.getElementType() != IJavaElement.JAVA_PROJECT) {
            String elementName = javaElement.getElementName();
            if ((elementName != null) && (elementName.length() > 0)) {
                name.insert(0, '.').insert(0, elementName);
            }

            javaElement = javaElement.getParent();
        }

        return name.toString();
    }

    /**
     * Gets all {@link IJavaProject} instance's source folders.
     * 
     * @param javaProject target {@link IJavaProject} instance.
     * @return array of folders.
     */
    public static IResource[] getSourceFolders(IJavaProject javaProject) {
        List<IResource> sources = new ArrayList<IResource>();
        try {
            IClasspathEntry[] classpath = javaProject.getRawClasspath();
            for (IClasspathEntry entry : classpath) {
                if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
                    IPath entryPath = entry.getPath();
                    IProject project = javaProject.getProject();
                    // Resource is the project itself if there is no 
                    // separate source folder
                    IResource resource = project;
                    	
                    if( entryPath.segmentCount() > 1 ){// A real folder find it in the project 
                    	 entryPath = entryPath.removeFirstSegments(1);
                    	 resource = project.getFolder(entryPath);
                    }
                     
                    if (resource.exists()) {
                        sources.add(resource);
                    }
                }
            }
        } catch (JavaModelException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
        return sources.toArray(new IResource[sources.size()]);
    }

    /**
     * Private constructor
     */
    private Utils() {
        super();
    }

    /**
     * Recursively set the resources in the specified container and all
     * resources within that container as derived.
     * 
     * @param container
     * @throws CoreException
     */
    public static void setResourcesAsDerived(IContainer container)
            throws CoreException {
        if (container.exists()) {
            // Mark this folder first...
            container.setDerived(true, null);
    
            // Recursively handle the members of the directory
            IResource[] resources = container.members();
            for (IResource resource : resources) {
                if (resource instanceof IContainer) {
                    setResourcesAsDerived((IContainer) resource);
                } else {
                    resource.setDerived(true, null);
                }
            }
        }
    }
    
    /**
     * Verifies if a folder name is valid.
     * @param folderName
     * @return
     */
    public static boolean isValidFolderName(String folderName) {
                IWorkspace workspace = MTJCore.getWorkspace();
                IStatus result = workspace.validateName(folderName, IResource.FOLDER);
		return result.isOK();
	}
    
    /**
     * Verifies if a file name is valid.
     * @param fileName
     * @return
     */
    public static boolean isValidFileName(String fileName) {
        IWorkspace workspace = MTJCore.getWorkspace();
        IStatus result = workspace.validateName(fileName, IResource.FILE);
        return result.isOK();
    }

    /**
     * Create an error marker in a resource 
     * @param resource
     * @param message
     * @throws CoreException
     */
    public static void createErrorMarker(IResource resource, String message) throws CoreException {
        IMarker marker = resource.createMarker(IMTJCoreConstants.JAVAME_PROBLEM_MARKER);
        marker.setAttribute(IMarker.MESSAGE, message);
        marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
    }
    
    /**
     * Remove all the error markers
     * @param resource
     * @throws CoreException
     */
    public static void removeAllErrorMarkers(IResource resource) throws CoreException {
        resource.deleteMarkers(IMTJCoreConstants.JAVAME_PROBLEM_MARKER, true, IResource.DEPTH_ZERO);
    }

    /**
     * Returns a path equivalent to this path, but relative to the output location it belongs to or
     * default value if it does not belong to any of provided output locations
     * 
     * @param path
     * @param outputLocations output locations defined in a project
     * @param defaultValue default value
     * @return relative path
     */
    public static IPath makePathRelativeToOutputLocation(IPath path, List<IPath> outputLocations, IPath defaultValue) {
        for (IPath outputLocation : outputLocations) {
            int commonSegnmentCount = path.matchingFirstSegments(outputLocation);

            if (commonSegnmentCount == outputLocation.segmentCount()) {
                return path.removeFirstSegments(commonSegnmentCount);
            }
        }

        return defaultValue;
    }

    /**
     * Returns the list of all output locations defined in the given project and optionally in
     * projects it depends on.
     * 
     * @param javaProject MTJ project
     * @param considerDependentProjects whether to answer output locations of projects the given depends on
     * @return list of output locations
     */
    public static List<IPath> getOutputLocations(IMTJProject mtjProject, boolean considerDependentProjects) {
        return collectOutputLocations(mtjProject.getJavaProject(), new HashSet<IJavaProject>(),
                considerDependentProjects);
    }

    private static List<IPath> collectOutputLocations(IJavaProject javaProject, HashSet<IJavaProject> processed,
            boolean considerDependentProjects) {
        List<IPath> result = new ArrayList<IPath>();

        if (processed.add(javaProject)) {
            IPath projectOutputLocation;
            try {
                projectOutputLocation = javaProject.getOutputLocation();
            } catch (JavaModelException ex1) {
                projectOutputLocation = null;
            }

            if (projectOutputLocation != null) {
                result.add(projectOutputLocation);
            }

            try {
                IClasspathEntry[] rawClasspath = javaProject.getRawClasspath();
                if (rawClasspath != null) {
                    for (IClasspathEntry entry : rawClasspath) {
                        if (entry.getEntryKind() == IClasspathEntry.CPE_PROJECT && considerDependentProjects) {
                            IPath projectPath = entry.getPath();
                            IProject project = (IProject) MTJCore.getWorkspace().getRoot().findMember(projectPath);
                            if (project != null) {
                                result.addAll(collectOutputLocations(JavaCore.create(project), processed,
                                        considerDependentProjects));
                            }
                        } else if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
                            IPath outputLocation = entry.getOutputLocation();

                            if (outputLocation != null) {
                                result.add(outputLocation);
                            }
                        }
                    }
                }
            } catch (JavaModelException ex) {}
        }

        return result;
    }
}
