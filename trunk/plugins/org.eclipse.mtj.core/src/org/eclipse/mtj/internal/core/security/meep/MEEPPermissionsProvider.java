/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.core.security.meep;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.text.Collator;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.ILibrary;

/**
 * <code>MEEPPermissionsProvider</code> class is helper class to extract available permissions from an SDK and to work with them.
 * 
 * Available permissions and their possible parameters are extracted by parsing classes and their constructors from the SDK libraries 
 * for new MEEP permissions (see JSR 361 for details) and "impng.permissions" files in the folders of SDK libraries for legacy 
 * IMPNG permissions.
 */
public class MEEPPermissionsProvider {
    private static final String BASE_CLASS_NAME = "java.security.Permission";

    private static final String IMPNG_PERMISSIONS_FILE_NAME = "impng.permissions";

    private static final String NULL_PARAMETER = "null";

    // Use double-quote to mark absent parameter since these parameters cannot contain double-quote
    private static final String NO_PARAMETER = "\"";

    private Map<String, PermissionDescriptor> permissions;
    
    private ISDK sdk;

    public MEEPPermissionsProvider(ISDK sdk) {
        this.sdk = sdk;
        
        fill(sdk);
    }
    
    public ISDK getSDK() {
        return sdk;
    }

    private void listClasses(File jarFilePointer, Set<String> allClassesNames) {
        try {
            JarFile jarFile = null;

            try {
                jarFile = new JarFile(jarFilePointer);

                for (Enumeration< ? extends JarEntry> jarEntryEnum = jarFile.entries(); jarEntryEnum.hasMoreElements();) {
                    JarEntry jarEntry = jarEntryEnum.nextElement();
                    String entryName = jarEntry.getName();
                    if (!jarEntry.isDirectory() && entryName != null && entryName.endsWith(".class")) {
                        allClassesNames.add(entryName.substring(0, entryName.lastIndexOf('.'))
                                .replace('/', '.')
                                .replace('$', '.'));
                    }
                }
            } finally {
                if (jarFile != null) {
                    jarFile.close();
                }
            }
        } catch (Throwable t) {}
    }

    private void loadIMPNGPermissions(Set<File> libDirs) {
        for (File libDir : libDirs) {
            File permissionsFile = new File(libDir, IMPNG_PERMISSIONS_FILE_NAME);

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(permissionsFile),
                        "UTF-8"));

                try {
                    String line;

                    while ((line = reader.readLine()) != null) {
                        line = line.trim();

                        if (line.length() > 0 && !line.startsWith("#")) {
                            permissions.put(line, new PermissionDescriptor(line, false));
                        }
                    }
                } finally {
                    reader.close();
                }
            } catch (Throwable t) {}
        }
    }

    private void fill(ISDK sdk) {
        permissions = new HashMap<String, PermissionDescriptor>();

        Set<File> libs = new HashSet<File>();
        Set<File> libDirs = new HashSet<File>();

        if (sdk != null) {
            List<IDevice> devices;

            try {
                devices = sdk.getDeviceList();
            } catch (Throwable t) {
                devices = null;
            }

            if (devices != null) {
                for (IDevice device : devices) {
                    IDeviceClasspath deviceCP = device.getClasspath();

                    if (deviceCP != null) {
                        List<ILibrary> libraries = deviceCP.getEntries();

                        if (libraries != null) {
                            for (ILibrary library : libraries) {
                                if (library != null) {
                                    File libFile = library.toFile();

                                    if (libFile != null) {
                                        File libDir = libFile.getParentFile();

                                        if (libDir != null) {
                                            libDirs.add(libDir);
                                        }

                                        if (libFile.isFile() && libFile.getName().toLowerCase().endsWith(".jar")) {
                                            libs.add(libFile);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        loadIMPNGPermissions(libDirs);
        loadMEEPPermissions(libs);
    }

    private void loadMEEPPermissions(Set<File> libs) {
        Set<String> allClassNames = new HashSet<String>();
        LibraryJarClassLoader classLoader = new LibraryJarClassLoader();

        for (File libFile : libs) {
            try {
                classLoader.addJarFile(libFile);
                listClasses(libFile, allClassNames);
            } catch (Throwable t) {}
        }

        Class< ? > baseClass;

        try {
            baseClass = classLoader.loadClass(BASE_CLASS_NAME);
        } catch (ClassNotFoundException ex) {
            baseClass = null;
        }

        if (baseClass != null) {
            for (String className : allClassNames) {
                try {
                    Class< ? > candidate = classLoader.loadClass(className);

                    if (!Modifier.isAbstract(candidate.getModifiers()) && baseClass.isAssignableFrom(candidate)) {
                        PermissionDescriptor descriptor = new PermissionDescriptor(className, true);

                        Constructor< ? >[] constructors = candidate.getConstructors();

                        if (constructors != null) {
                            for (Constructor< ? > constructor : constructors) {
                                Class< ? >[] parameterTypes = constructor.getParameterTypes();
                                if (parameterTypes != null && parameterTypes.length <= 2) {
                                    boolean accept = true;

                                    for (int i = 0; i < parameterTypes.length && accept; i++) {
                                        accept &= parameterTypes[i] == String.class;
                                    }

                                    if (accept) {
                                        descriptor.numberOfSupportedArgumentes.add(parameterTypes.length);
                                    }
                                }
                            }
                        }

                        if (!descriptor.numberOfSupportedArgumentes.isEmpty()) {
                            permissions.put(className, descriptor);
                        }
                    }
                } catch (Throwable t) {}
            }
        }
    }

    public PermissionsFactory getPermissionsFactory(Collection<String> excludePermissions) {
        return new PermissionsFactory(excludePermissions);
    }


    public static enum PermissionError {
        OK, INVALID_NAME, INVALID_ACTION, UNDEFINED_ACTION, UNDEFINED_NAME, CONFLICT;
    }


    public class PermissionsFactory {
        private Map<String, Map<String, Set<String>>> existingMEEPPermissions = new HashMap<String, Map<String, Set<String>>>();
        private PermissionDescriptor[] availablePermissions;

        private PermissionsFactory(Collection<String> excludePermissions) {
            if (excludePermissions == null) {
                excludePermissions = Collections.emptySet();
            }

            Set<String> excludePermissionNames = new HashSet<String>();

            for (String excludePermission : excludePermissions) {
                String[] permissionComponents = splitPermissionComponents(excludePermission);

                if (permissionComponents != null) {
                    PermissionDescriptor descriptor = permissions.get(permissionComponents[0]);

                    if (descriptor != null) {
                        String permission = descriptor.getPermission();

                        if (descriptor.isPermissionClass()) {
                            if (!descriptor.supportsName()) {
                                excludePermissionNames.add(permission);
                            } else {
                                Map<String, Set<String>> existingNames = existingMEEPPermissions.get(permission);

                                if (existingNames == null) {
                                    existingNames = new HashMap<String, Set<String>>();
                                    existingMEEPPermissions.put(permission, existingNames);
                                }

                                Set<String> existingActions = existingNames.get(permissionComponents[1]);

                                if (existingActions == null) {
                                    existingActions = new HashSet<String>();
                                    existingNames.put(permissionComponents[1], existingActions);
                                }

                                if (permissionComponents[2] == null) {
                                    existingActions.add(null);
                                } else {
                                    for (String action : permissionComponents[2].split(",")) {
                                        existingActions.add(action.trim());
                                    }
                                }
                            }
                        } else {
                            excludePermissionNames.add(permission);
                        }
                    }
                }
            }

            List<PermissionDescriptor> avilablePermissionsList = new ArrayList<PermissionDescriptor>();

            for (PermissionDescriptor permissionDescriptor : permissions.values()) {
                if (!excludePermissionNames.contains(permissionDescriptor.getPermission())) {
                    avilablePermissionsList.add(permissionDescriptor);
                }
            }

            Collections.sort(avilablePermissionsList);

            availablePermissions = avilablePermissionsList.toArray(new PermissionDescriptor[avilablePermissionsList.size()]);
        }

        public PermissionDescriptor getDescriptor(PermissionDefinition permission) {
            return permissions.get(permission.getPermission());
        }

        public PermissionDefinition getPermission(String permission) {
            String[] permissionComponents = splitPermissionComponents(permission);

            if (permissionComponents != null) {
                PermissionDescriptor descriptor = permissions.get(permissionComponents[0]);

                if (descriptor != null) {
                    int numberOfParameters = 0;

                    if (descriptor.supportsName() && !NO_PARAMETER.equals(permissionComponents[1])) {
                        numberOfParameters = numberOfParameters + 1;

                        if (descriptor.supportsAction() && !NO_PARAMETER.equals(permissionComponents[2])) {
                            numberOfParameters = numberOfParameters + 1;
                        }
                    }

                    if (numberOfParameters == 0 && !descriptor.nameCanBeOptional() && descriptor.isPermissionClass()) {
                        return null;
                    } else if (numberOfParameters == 1 && !descriptor.actionsCanBeOptional(true)) {
                        return null;
                    }

                    return getPermissionWithoutValidation(
                            descriptor,
                            permissionComponents[1],
                            permissionComponents[2],
                            NO_PARAMETER.equals(permissionComponents[2]) ? (NO_PARAMETER.equals(permissionComponents[1]) ? 0
                                    : 1)
                                    : 2);
                }
            }

            return null;
        }

        private String[] splitPermissionComponents(String permission) {
            String[] result = new String[] { null, NO_PARAMETER, NO_PARAMETER };
            int idx = permission.indexOf(' ');

            if (idx < 0) {
                result[0] = permission.trim();
                permission = "";
            } else {
                result[0] = permission.substring(0, idx).trim();
                permission = permission.substring(idx).trim();
            }

            for (int i = 1; i < 3 && permission.length() > 0; i++) {
                if (permission.startsWith("\"")) {
                    idx = permission.indexOf('"', 1);

                    if (idx < 0) {
                        return null;
                    } else {
                        result[i] = permission.substring(1, idx);
                        permission = permission.substring(idx + 1).trim();
                    }
                } else {
                    if (!permission.startsWith(NULL_PARAMETER)) {
                        return null;
                    }

                    idx = permission.indexOf(' ');
                    result[i] = null;

                    if (idx < 0) {
                        if (NULL_PARAMETER.length() < permission.length()) {
                            return null;
                        }

                        permission = "";
                    } else {
                        if (idx != NULL_PARAMETER.length()) {
                            return null;
                        }

                        permission = permission.substring(idx).trim();
                    }
                }
            }

            return result;
        }

        public PermissionDescriptor[] getAvailablePermissions() {
            return availablePermissions;
        }

        public PermissionError validatePermission(PermissionDescriptor descriptor, String name, String actions,
                int numberOfParameters) {
            if (descriptor.isPermissionClass()) {
                if (numberOfParameters > 0) {
                    if (descriptor.supportsName()) {
                        if (name != null && name.indexOf('"') >= 0) {
                            return PermissionError.INVALID_NAME;
                        }

                        if (numberOfParameters > 1) {
                            if (descriptor.supportsAction()) {
                                if (actions != null && actions.indexOf('"') >= 0) {
                                    return PermissionError.INVALID_ACTION;
                                }
                            }
                        } else {
                            if (!descriptor.numberOfSupportedArgumentes.contains(1)) {
                                return PermissionError.UNDEFINED_ACTION;
                            }
                        }
                    }
                } else {
                    if (!descriptor.numberOfSupportedArgumentes.contains(0)) {
                        return PermissionError.UNDEFINED_NAME;
                    }
                }

                PermissionDefinition permission = getPermissionWithoutValidation(descriptor, name, actions,
                        numberOfParameters);
                Map<String, Set<String>> existingNames = existingMEEPPermissions.get(permission.getPermission());

                if (existingNames != null) {
                    Set<String> existingActions = existingNames.get(permission.getNumberOfParameters() > 0 ? permission.getName()
                            : NO_PARAMETER);

                    if (existingActions != null) {
                        Set<String> definedActions = permission.getActionsSet();

                        if (definedActions.isEmpty()) {
                            if (existingActions.contains(null) && permission.getNumberOfParameters() > 1
                                    || existingActions.contains(NO_PARAMETER) && permission.getNumberOfParameters() < 2) {
                                return PermissionError.CONFLICT;
                            }
                        } else {
                            for (String action : definedActions) {
                                if (existingActions.contains(action)) {
                                    return PermissionError.CONFLICT;
                                }
                            }
                        }
                    }
                }
            }

            return PermissionError.OK;
        }

        private PermissionDefinition getPermissionWithoutValidation(PermissionDescriptor descriptor, String name,
                String actions, int numberOfParameters) {
            if (descriptor.isPermissionClass()) {
                boolean hasName = descriptor.supportsName() && numberOfParameters > 0;
                boolean hasAction = descriptor.supportsAction() && numberOfParameters > 1;

                return new PermissionDefinition(descriptor.getPermission(), hasName ? name : null, hasName && hasAction
                        && actions != null ? actions.trim() : null, true, numberOfParameters);
            } else {
                return new PermissionDefinition(descriptor.getPermission(), null, null, false, 0);
            }
        }

        public PermissionDefinition getPermission(PermissionDescriptor descriptor, String name, String actions,
                int numberOfParameters) {
            if (validatePermission(descriptor, name, actions, numberOfParameters) != PermissionError.OK) {
                return null;
            }

            return getPermissionWithoutValidation(descriptor, name, actions, numberOfParameters);
        }
    }


    public static class PermissionDefinition implements Comparable<PermissionDefinition> {
        private String permission;
        private String name;
        private LinkedHashSet<String> actions = new LinkedHashSet<String>();
        private boolean permissionClass;
        private int numberOfParameters;

        private PermissionDefinition(String permission, String name, String actions, boolean permissionClass,
                int numberOfParameters) {
            this.permission = permission;
            this.name = name;
            this.permissionClass = permissionClass;
            this.numberOfParameters = numberOfParameters;

            if (numberOfParameters > 1) {
                parseActions(actions);
            }
        }

        private void parseActions(String actionsString) {
            if (actionsString != null) {
                for (String action : actionsString.split(",")) {
                    actions.add(action.trim());
                }
            }
        }

        public String getPermission() {
            return permission;
        }

        private Set<String> getActionsSet() {
            return actions;
        }

        public String getName() {
            return name;
        }

        public int getNumberOfParameters() {
            return numberOfParameters;
        }

        public String getActions() {
            if (numberOfParameters < 2 || actions.isEmpty()) {
                return null;
            }

            StringBuilder result = new StringBuilder();

            for (String action : actions) {
                if (result.length() > 0) {
                    result.append(",");
                }

                result.append(action);
            }

            return result.toString();
        }

        public boolean isPermissionClass() {
            return permissionClass;
        }

        @Override
        public String toString() {
            StringBuilder result = new StringBuilder(permission);

            if (isPermissionClass()) {
                if (numberOfParameters > 0) {
                    result.append(" ");

                    if (name == null) {
                        result.append(NULL_PARAMETER);
                    } else {
                        result.append("\"");
                        result.append(name);
                        result.append("\"");
                    }

                    String actionsString = getActions();

                    if (numberOfParameters > 1) {
                        result.append(" ");

                        if (actionsString == null) {
                            result.append(NULL_PARAMETER);
                        } else {
                            result.append("\"");
                            result.append(actionsString);
                            result.append("\"");
                        }
                    }
                }
            }

            return result.toString();
        }

        public int compareTo(PermissionDefinition o) {
            return Collator.getInstance().compare(toString(), o.toString());
        }
    }


    public static class PermissionDescriptor implements Comparable<PermissionDescriptor> {
        private String permission;
        private boolean permissionClass;
        private Set<Integer> numberOfSupportedArgumentes = new HashSet<Integer>();

        private PermissionDescriptor(String permission, boolean permissionClass) {
            this.permission = permission;
            this.permissionClass = permissionClass;
        }

        public boolean isPermissionClass() {
            return permissionClass;
        }

        public String getPermission() {
            return permission;
        }

        @Override
        public String toString() {
            return getPermission();
        }

        public int compareTo(PermissionDescriptor o) {
            return Collator.getInstance().compare(getPermission(), o.getPermission());
        }

        @Override
        public int hashCode() {
            return getPermission().hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof PermissionDescriptor) {
                return getPermission().equals(((PermissionDescriptor) obj).getPermission());
            }

            return false;
        }

        public boolean supportsName() {
            return numberOfSupportedArgumentes.contains(1) || numberOfSupportedArgumentes.contains(2);
        }

        public boolean supportsAction() {
            return numberOfSupportedArgumentes.contains(2);
        }

        public boolean nameCanBeOptional() {
            return numberOfSupportedArgumentes.contains(0);
        }

        public boolean actionsCanBeOptional(boolean nameRequired) {
            return numberOfSupportedArgumentes.contains(nameRequired ? 1 : 0);
        }
    }
}
