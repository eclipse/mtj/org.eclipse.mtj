/*******************************************************************************
 * Copyright (c) 2008 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase) - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.core.refactoring;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.launching.midp.IMIDPLaunchConstants;
import org.eclipse.osgi.util.NLS;

/**
 * This class update the JAD file URL attribute in the JAD launch configurations
 * according the project name change.
 * 
 * @author wangf
 */
public class JadLaunchConfigProjectRenameChange extends Change {
    /**
     * The Jad launch configuration to be refactored
     */
    private ILaunchConfiguration launchConfiguration;
    /**
     * The new project name
     */
    private String newProjectName;

    public JadLaunchConfigProjectRenameChange(
            ILaunchConfiguration launchConfiguration, String newProjectName) {
        this.launchConfiguration = launchConfiguration;
        this.newProjectName = newProjectName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ltk.core.refactoring.Change#getModifiedElement()
     */
    @Override
    public Object getModifiedElement() {
        return launchConfiguration;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.ltk.core.refactoring.Change#getName()
     */
    @Override
    public String getName() {
        return NLS.bind(
                RefactoringMessages.JadLaunchConfigProjectRenameChange_name,
                launchConfiguration.getName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.ltk.core.refactoring.Change#initializeValidationData(org.
     * eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    public void initializeValidationData(IProgressMonitor pm) {
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.ltk.core.refactoring.Change#isValid(org.eclipse.core.runtime
     * .IProgressMonitor)
     */
    @Override
    public RefactoringStatus isValid(IProgressMonitor pm) throws CoreException,
            OperationCanceledException {
        if (!launchConfiguration.exists()) {
            return RefactoringStatus.createFatalErrorStatus(NLS.bind(
                    RefactoringMessages.JadLaunchConfig_doNotExist,
                    launchConfiguration.getName()));
        }
        return new RefactoringStatus();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.eclipse.ltk.core.refactoring.Change#perform(org.eclipse.core.runtime
     * .IProgressMonitor)
     */
    @Override
    public Change perform(IProgressMonitor pm) throws CoreException {
        ILaunchConfigurationWorkingCopy wc = launchConfiguration
                .getWorkingCopy();
        String oldJadFileLocation = wc.getAttribute(
                IMIDPLaunchConstants.SPECIFIED_JAD_URL, "");

        String newJadFileLocation = getNewJadFileLocation(oldJadFileLocation,
                newProjectName);
        wc.setAttribute(IMIDPLaunchConstants.SPECIFIED_JAD_URL,
                newJadFileLocation);

        if (wc.isDirty()) {
            wc.doSave();
        }
        // return null makes the rename operation cannot undo, what is we expect
        return null;
    }

    /**
     * Return the new launching Jad file location according the new project
     * name.
     * 
     * @param oldJadFileLocation
     * @return
     */
    private String getNewJadFileLocation(String oldJadFileLocation,
            String newProjectName) {
        IPath newProjectPath = MTJCore.getWorkspace().getRoot()
                .getProject(newProjectName).getLocation();
        IPath oldJadPath = new Path(oldJadFileLocation);
        // The relative Jad file path relative against the project path
        IPath relativeJadPath = oldJadPath.removeFirstSegments(newProjectPath
                .segmentCount());
        IPath newJadPath = newProjectPath.append(relativeJadPath);
        return newJadPath.toPortableString();
    }
}
