/*******************************************************************************
 * Copyright (c) 2010 Nokia Corporation and/or its subsidiary(-ies).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Nokia Corporation - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.internal.core;

import org.eclipse.core.runtime.Platform;
import org.eclipse.mtj.core.MTJCore;
/**
 * Utility for providing tracing to MTJCore
 * 
 */
public class Trace {
	
	public static final String JAVADOC_DETECT_PERF = MTJCore.getPluginId() + "/perf/javadoc";  //$NON-NLS-1$
	public static final String JAVADOC_DETECT_DEBUG = MTJCore.getPluginId() + "/debug/javadoc";  //$NON-NLS-1$
	
	/**
	 * Checks if the given trace option is enabled
	 * @param traceOption
	 * @return
	 */
	public static boolean isOptionEnabled(String traceOption){
		String option = Platform.getDebugOption(traceOption);
		return option != null && "true".equals(option); //$NON-NLS-1$
	}
	/**
	  * Print trace messages to stdout if the given trace option is enabled.
	  *
	  * This uses variable messageParts to avoid the cost of contructing 
	  * messages
	  * 
	  * @param traceOption the option string
	  * @param messageParts a number of objects that constitute a message
	  */
    public static void  trace( String traceOption, Object... messageParts ){
		if (!MTJCore.getMTJCore().isDebugging()) {
			return;
		}
		if (isOptionEnabled(traceOption)){
			for (int i = 0; i < messageParts.length; i++) {
				System.out.print(messageParts[i]);
			}
			System.out.println();
		}
	}
}
