/**
 * Copyright (c) 2003,2013 Craig Setera and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Diego Sandin (Motorola)  - Adapting code to use Jetty version 5.1.11
 *     Diego Sandin (Motorola)  - Fixed NPE [Bug 267668]
 *     Gorkem Ercan (Nokia)     - [281658] Move Jetty dependency to the latest
 *    T. Bakardzhiev (Prosyst)  - Eclipse 4.2 jetty adjustments. 
 */
package org.eclipse.mtj.internal.core.launching.midp.ota;

import java.io.File;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jetty.server.*;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;


/**
 * Singleton management of the Over the Air HTTP Server.
 *
 * @author Craig Setera
 */
public class OTAServer {

    /**
     * Singleton instance of the OTAServer
     */
    private static OTAServer instance;

    /**
     * Return the unique instance of the OTAServer
     *
     * @return a unique OTAServer instance.
     */
    public static synchronized OTAServer getInstance() {
        if (instance == null) {
            instance = new OTAServer();
        }
        return instance;
    }

    /**
     * Return the port to be listened on for OTA connections.
     *
     * @return
     */
    public static synchronized int getPort() {
        Connector connector = getInstance().getHttpServer().getConnectors()[0];
        
        return connector instanceof NetworkConnector ? ((NetworkConnector) connector).getLocalPort() : 0;
    }

    // The HttpServer instance in use for serving the content
    private Server httpServer;

    /**
     * The HTTP Server instance
     */
    private Server server;

    /**
     * Creates a new instance of OTAServer.
     */
    private OTAServer() {
        super();
        server = getHttpServer();
    }

    /**
     * @throws Exception
     */
    public synchronized void start() throws Exception {
        if (server != null && !server.isStarted()) {
            server.start();
        }
    }

    /**
     * Stop the HttpServer if it is currently running.
     * @throws Exception
     */
    public synchronized void stop() throws Exception {
        if (server!= null && server.isStarted()) {
            server.stop();
        }
    }

    /**
     * Calculate a filename to direct Jetty logging
     */
    private String getLogFileName() {
        // Calculate the name of the file to be used for logging
        IPath stateLocation = MTJCore.getMTJCore().getStateLocation();
        IPath logDirectory = stateLocation.append("jetty"); //$NON-NLS-1$

        File logDirectoryFile = logDirectory.toFile();
        if (!logDirectoryFile.exists()) {
            logDirectoryFile.mkdir();
        }

        File logFile = new File(logDirectoryFile, "logfile"); //$NON-NLS-1$
        String filename = logFile + "yyyy_mm_dd.txt"; //$NON-NLS-1$
        return filename;
    }
    /**
     * Create and configure the HTTP server instance.
     *
     * @return
     */
    private Server createHttpServer() {
        int portNumber = 0;

        if (Platform.getPreferencesService().getBoolean(MTJCore.getPluginId(), IMTJCoreConstants.PREF_OTA_PORT_DEFINED,
                false, null)) {
            portNumber = Platform.getPreferencesService().getInt(MTJCore.getPluginId(), IMTJCoreConstants.PREF_OTA_PORT,
                    0, null);
        }

        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        
        if (portNumber >= 0) {
            connector.setPort(portNumber);
        }

        server.setConnectors(new Connector[] { connector });
        server.setHandler(OTAHandler.getContextHandler());
        
        NCSARequestLog log = new NCSARequestLog(getLogFileName());
        
        server.addManaged(log);
        
        return server;
    }

    /**
     * Get the HttpServer instance.
     *
     * @return
     */
    private Server getHttpServer() {
        if (httpServer == null) {
            httpServer = createHttpServer();
        }

        return httpServer;
    }
}
