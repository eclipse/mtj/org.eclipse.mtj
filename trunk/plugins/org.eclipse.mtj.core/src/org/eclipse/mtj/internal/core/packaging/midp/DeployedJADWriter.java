/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     David Marques (Motorola) - Adding signing state for hooks
 *     David Aragao (Motorola)  - Problem when try to import a project without 
 *     							  copy the files to the workspace. [Bug - 270157]
 */
package org.eclipse.mtj.internal.core.packaging.midp;

import java.io.File;
import java.io.IOException;


import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.build.MTJBuildState;
import org.eclipse.mtj.core.project.midp.IApplicationDescriptor;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.build.BuildStateMachine;
import org.eclipse.mtj.internal.core.build.sign.SignatureUtils;
import org.eclipse.mtj.internal.core.project.midp.IJADConstants;
import org.eclipse.mtj.internal.core.project.midp.IJadSignature;
import org.eclipse.mtj.internal.core.project.midp.LibletProperties;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.sdk.device.midp.PackagingModel;
import org.eclipse.mtj.internal.core.sdk.device.midp.Profile;
import org.eclipse.mtj.internal.core.util.ColonDelimitedProperties;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * A helper class that is used to create a JAD file representing the deployed
 * JAR file.
 * 
 * @author Craig Setera
 */
public class DeployedJADWriter {
    
    private IMidletSuiteProject midletSuite;
    private File deploymentFolder;
    private File deployedJarFile;

    /**
     * Construct a new JAD Writer to handle the specified deployment.
     */
    public DeployedJADWriter(IMidletSuiteProject midletSuite,
            IFolder deploymentFolder, File deployedJar) {
    	this(midletSuite,deploymentFolder.getLocation().toFile(),deployedJar);
    }
    
    /**
     * Construct a new JAD Writer to handle the specified deployment.
     */
    public DeployedJADWriter(IMidletSuiteProject midletSuite,
            File deploymentFolder, File deployedJar) {
        super();
        this.midletSuite = midletSuite;
        this.deploymentFolder = deploymentFolder;
        this.deployedJarFile = deployedJar;
    }

    /**
     * Write out a deployed JAD file for the previously created JAR file.
     * 
     * @param incrementalBuild
     * @param monitor
     * @throws IOException
     * @throws CoreException
     */
    public void writeDeployedJAD(boolean incrementalBuild,
            IProgressMonitor monitor) throws IOException, CoreException {
        // Read the source jad file and update the jar file
        // length property.
        IApplicationDescriptor appDescriptor = midletSuite
                .getApplicationDescriptor();
        ColonDelimitedProperties jadProperties = (ColonDelimitedProperties) appDescriptor
                .getManifestProperties();

        String jarSizePropertyName = IJADConstants.JAD_MIDLET_JAR_SIZE;
        String jarURLPropertyName = IJADConstants.JAD_MIDLET_JAR_URL;
        String jarDependencyName = IJADConstants.JAD_MIDLET_DEPENDENCY;
        String jarDependencyJadUrlName = IJADConstants.JAD_MIDLET_DEPENDENCY_JAD_URL;
        if (jadProperties.getProperty(IJADConstants.JAD_LIBLET_JAR_URL) != null
        		&& !jadProperties.getProperty(IJADConstants.JAD_LIBLET_JAR_URL).equals("")) {
        	jarSizePropertyName = IJADConstants.JAD_LIBLET_JAR_SIZE;
        	jarURLPropertyName = IJADConstants.JAD_LIBLET_JAR_URL;
        	jarDependencyName = IJADConstants.JAD_LIBLET_DEPENDENCY;
            jarDependencyJadUrlName = IJADConstants.JAD_LIBLET_DEPENDENCY_JAD_URL;
        }
        
        // Update the size of the jar file
        jadProperties.setProperty(jarSizePropertyName, (Long
                .valueOf(deployedJarFile.length())).toString());

        // If requested, we overwrite the JAD URL such that it is just the
        // name of the jar file. This is for runtime handling.
        if (incrementalBuild) {
            jadProperties.setProperty(jarURLPropertyName,
                    deployedJarFile.getName());
        }
            
        LibletProperties[] libletProperties = midletSuite.getLibletProperties();
        if (libletProperties != null) {
        	int index = 1;
        	IWorkspace workspace = ResourcesPlugin.getWorkspace();      	      	
            for (int i=0; i<libletProperties.length; i++) {
            	String requiredProjectName = libletProperties[i].getProjectName();

            	if (libletProperties[i].getType() == LibletProperties.Type.LIBLET
            			&& requiredProjectName != null && !requiredProjectName.trim().equals("")) {
            		IProject project = workspace.getRoot().getProject(requiredProjectName);
                	
                	IMidletSuiteProject midletSuiteRequiredProject = MidletSuiteFactory
                			.getMidletSuiteProject(JavaCore.create(project));
                	
                	if (midletSuiteRequiredProject.getProjectPackagingModel() != PackagingModel.LIBLET) {
                		continue;
                	}
        	    }
            	
            	jadProperties.setProperty(jarDependencyName + index,
            			libletProperties[i].getJadPropertyString());
            	if (libletProperties[i].getJadURL() != null
            			&& !libletProperties[i].getJadURL().trim().equals("")) {
            		jadProperties.setProperty(jarDependencyJadUrlName + index,
            				libletProperties[i].getJadURL());
            	}
            	index++;
            }
        }
        
        // write the JAD file in case signing blows up - at least we'll have
        // something non-bogus.
        writeJad(appDescriptor);


        // Retrieve the signature object for this project. This will be null
        // if we're not supposed to sign this project.
        IJadSignature signatureObject = SignatureUtils
                .getSignatureObject(midletSuite);
        if (signatureObject != null) {
         	BuildStateMachine stateMachine = BuildStateMachine.getInstance(midletSuite);
          	stateMachine.changeState(MTJBuildState.PRE_SIGNING, monitor);
            signJad(signatureObject, deployedJarFile, jadProperties);
            writeJad(appDescriptor);
            stateMachine.changeState(MTJBuildState.POST_SIGNING, monitor);
        }
    }

    /**
     * Sign the JAR file associated with the project and add the signature
     * properties to the JAD file.
     * 
     * @param signatureObject IJadSignature object that will do the actual
     *            signing
     * @param deployedJarFile File referencing the JAR file to be signed
     * @param jadProperties Contents of the JAD file to be added to
     * @throws CoreException
     */
    private void signJad(IJadSignature signatureObject, File deployedJarFile,
            ColonDelimitedProperties jadProperties) throws CoreException {
        signatureObject.computeSignature(deployedJarFile);

        boolean isLiblet = midletSuite.getProjectPackagingModel() == PackagingModel.LIBLET;
        boolean isMEEP = midletSuite.getApplicationDescriptor().getMicroEditionProfile()
        		.equals(Profile.MEEP_80.toString());
        String jarRsaShaPropertyName = isLiblet ? IJADConstants.JAD_LIBLET_JAR_RSA_SHA1
        		: isMEEP ? IJADConstants.JAD_MIDLET_JAR_RSA_SHA1_1 : IJADConstants.JAD_MIDLET_JAR_RSA_SHA1;
        String certificatePropertyName = isLiblet ? IJADConstants.JAD_LIBLET_CERTIFICATE
        		: IJADConstants.JAD_MIDLET_CERTIFICATE;
        
        jadProperties.setProperty(jarRsaShaPropertyName, signatureObject.getJarSignatureString());

        String[] certificates = signatureObject.getCertificateStrings();
        for (int i = 1; i <= certificates.length; i++) {
            jadProperties.setProperty(certificatePropertyName + i,
                    certificates[i - 1]);
        }
    }

    /**
     * Write the application descriptor out to the deployed location.
     * 
     * @param appDescriptor
     * @throws IOException
     */
    private void writeJad(IApplicationDescriptor appDescriptor)
            throws IOException {
    	String jadName = midletSuite.getJadFileName();
		if (jadName != null) {
			File deployedJadFile = new File(deploymentFolder, jadName);
			if(!deployedJadFile.exists()){
				deployedJadFile.createNewFile();
			}
			appDescriptor.store(deployedJadFile);
		} else {
			MTJLogger.log(IStatus.ERROR, Messages.DeployedJADWriter_unableToResolveJadName);
		}
    }
}
