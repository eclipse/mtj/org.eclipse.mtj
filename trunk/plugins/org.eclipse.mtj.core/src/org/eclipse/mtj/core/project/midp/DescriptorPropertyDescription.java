/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)         - Initial implementation
 *     Diego Sandin (Motorola)          - Refactoring package name to follow eclipse 
 *                                        standards
 *     Jon Dearden (Research In Motion) - Added support for applying 
 *                                        DescriptorPropertyDescription(s) to specific
 *                                        page sections [Bug 284452]
 */
package org.eclipse.mtj.core.project.midp;

/**
 * This class represents an application descriptor property description.
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * 
 * @since 1.0
 */
public class DescriptorPropertyDescription {

    /** Integer property type */
    public static final int DATATYPE_INT = 3;

    /** List property type */
    public static final int DATATYPE_LIST = 4;

    /** String property type */
    public static final int DATATYPE_STRING = 1;

    /** URL property type */
    public static final int DATATYPE_URL = 2;

    /** The data type for this property */
    private int dataType;

    /** The displayable name for this property */
    private String displayName;

    /** The name of the underlying property */
    private String propertyName;
    
    /** The JAD editor page identifier for which this property is associated */
    private String pageId;
    
    /** The JAD editor page section identifier for which this property is associated */
    private String sectionId;
    
    private boolean isLibletDescriptor = false;

    /**
     * Creates a new instance of DescriptorPropertyDescription.
     * 
     * @param propertyName the name of the underlying property. This is
     *            case-sensitive and must not be <code>null</code> or an empty
     *            String <code>""</code>.
     * @param displayName the displayable name for this property. This is
     *            case-sensitive and must not be <code>null</code> or an empty
     *            String <code>""</code>.
     * @param dataType the data type for this property. Possible data types are:
     *            <ul>
     *            <li>{@link #DATATYPE_STRING}</li>
     *            <li>{@link #DATATYPE_URL}</li>
     *            <li>{@link #DATATYPE_INT}</li>
     *            <li>{@link #DATATYPE_LIST}</li>
     *            </ul>
     */
    public DescriptorPropertyDescription(String propertyName,
            String displayName, int dataType, boolean isLibletDescriptor) {
        super();
        this.propertyName = propertyName;
        this.displayName = displayName;
        this.dataType = dataType;
        this.isLibletDescriptor = isLibletDescriptor;
    }
    
    public DescriptorPropertyDescription(String propertyName,
            String displayName, int dataType) {
    	this(propertyName, displayName, dataType, false);
    }

    /**
     * Returns the data type for this property.
     * 
     * @return the data type. Possible data types are:
     *         <ul>
     *         <li>{@link #DATATYPE_STRING}</li>
     *         <li>{@link #DATATYPE_URL}</li>
     *         <li>{@link #DATATYPE_INT}</li>
     *         <li>{@link #DATATYPE_LIST}</li>
     *         </ul>
     */
    public int getDataType() {
        return dataType;
    }

    /**
     * Returns the displayable name for this property.
     * 
     * @return the display name.
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Returns the name of the underlying property
     * 
     * @return the property name.
     */
    public String getPropertyName() {
        return propertyName;
    }
    
    /**
     * Returns the JAD editor page identifier for which this property is associated
     * 
     * @return the page identifier.
     */
    public String getPageId() {
        return pageId;
    }
    
    /**
     * Returns the JAD editor page section identifier for which this property is associated
     * 
     * @return the page section identifier.
     */
    public String getSectionId() {
        return sectionId;
    }
    
    public boolean isLibletDescriptor() {
    	return isLibletDescriptor;
    }

    /**
     * Sets the data type for this property.
     * 
     * @param i the data type. Possible data types are:
     *            <ul>
     *            <li>{@link #DATATYPE_STRING}</li>
     *            <li>{@link #DATATYPE_URL}</li>
     *            <li>{@link #DATATYPE_INT}</li>
     *            <li>{@link #DATATYPE_LIST}</li>
     *            </ul>
     */
    public void setDataType(int i) {
        dataType = i;
    }

    /**
     * Sets the displayable name for this property.
     * 
     * @param string the display name. This is case-sensitive and must not be
     *            <code>null</code> or an empty String <code>""</code>.
     */
    public void setDisplayName(String string) {
        displayName = string;
    }

    /**
     * Sets the page ID for this property.
     * 
     * @param string the page ID.
     */
    public void setPageId(String string) {
        pageId = string;
    }
    
    /**
     * Sets the page section ID for this property.
     * 
     * @param string the page section ID.
     */
    public void setSectionId(String string) {
        sectionId = string;
    }
}
