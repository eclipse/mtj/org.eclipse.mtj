/*******************************************************************************
 * Copyright (c) 2014 Oracle and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Oracle - initial implementation
 *******************************************************************************/
package org.eclipse.mtj.core.build;

import java.util.LinkedHashMap;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;

/**
 * <code>CodeValidator</code> class is the super class for a class that performs custom 
 * Java code validation. To perform custom Java code validation subclasses of this class 
 * can be registered via <code>org.eclipse.mtj.core.codevalidator</code> extension point.<br/>
 * Validation itself is possible using visitor pattern applied to the abstract syntax tree of 
 * the validated compilation unit. 
 * 
 * @see ASTVisitor
 */
public abstract class CodeValidator extends ASTVisitor {
    private IResource validatedResource;
    private IEclipsePreferences preferences;

    public final void validate(ICompilationUnit compilationUnit) {
        if (compilationUnit != null) {
            this.validatedResource = compilationUnit.getResource();

            ProjectScope projectScope = new ProjectScope(validatedResource.getProject());
            IEclipsePreferences projectPrefs = projectScope.getNode(IMTJCoreConstants.PLUGIN_ID);

            if (projectPrefs.getBoolean(IMTJCoreConstants.PREF_CODE_VALIDATION_USE_PROJECT, false)) {
                preferences = projectPrefs;
            } else {
                preferences = InstanceScope.INSTANCE.getNode(MTJCore.getPluginId());
            }

            ASTParser parser = ASTParser.newParser(AST.JLS8);

            parser.setKind(ASTParser.K_COMPILATION_UNIT);
            parser.setSource(compilationUnit);
            parser.setResolveBindings(true);

            ASTNode root = parser.createAST(null);

            root.accept(this);
        }
    }

    /**
     * Answers whether this code validator supports validation for the given project.
     * 
     * @param project {@link IMidletSuiteProject} instance
     * @return whether the validation is supported
     */
    public abstract boolean supportsValidationFor(IMidletSuiteProject project);

    /**
     * Creates a {@link IMarker marker} for a problem found during validation.
     * 
     * @param message marker message
     * @param node AST node
     * @param lineNumber line number
     * @param markerID marker ID
     * @throws CoreException
     */
    protected final void createMarker(String message, ASTNode node, int lineNumber, String markerID)
            throws CoreException {
        Severity severity = getSeverity(markerID);

        if (severity != Severity.IGNORE) {
            IMarker marker = validatedResource.createMarker(IMTJCoreConstants.JAVAME_VALIDATION_PROBLEM_MARKER);
            int startPosition = node.getStartPosition();

            marker.setAttribute(IMarker.MESSAGE, message);

            marker.setAttribute(IMarker.SEVERITY, severity.getValue());
            marker.setAttribute(IMarker.CHAR_START, startPosition);
            marker.setAttribute(IMarker.CHAR_END, startPosition + node.getLength());
            marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
        }
    }

    /**
     * Answers the default severity value for the specified marker ID.
     * Null value is considered as {@link Severity#IGNORE}.<br/><br/>
     * 
     * <b>Note:</b> the implementation of this method must consider group markers.
     * The return value for a marker that belongs to some group is ignored.
     * 
     * @param markerID marker ID
     * @return severity
     */
    public abstract Severity getDefaultSeverity(String markerID);

    /**
     * Answers severities that can be assigned to the given marker or null 
     * if the marker does not support configuration of its severity.<br/><br/>
     * 
     * <b>Note:</b> the implementation of this method must consider group markers.
     * The return value for a marker that belongs to some group is ignored.
     * 
     * @param markerID marker ID
     * @return supported severities
     */
    public abstract Severity[] getSupportedSeverities(String markerID);

    /**
     * Answers code validator name.
     * 
     * @return validator name
     */
    public abstract String getValidatorName();

    /**
     * Answers IDs of markers that can be created by this validator.
     * 
     * @return array of marker IDs
     */
    public abstract String[] getMarkerIDs();

    /**
     * Answers map with markers that support configuration.
     * Keys in this map are marker IDs, values are severities that can be assigned to the corresponding marker.
     * The result can contain either markers created by this validator or group markers if same 
     * severity configuration is applied to several markers created by this validator.
     * 
     * @return map with supported markers configurations
     */
    public final LinkedHashMap<String, Severity[]> getSupportedMarkersConfigurations() {
        LinkedHashMap<String, Severity[]> result = new LinkedHashMap<String, Severity[]>();

        for (String markerID : getMarkerIDs()) {
            String groupMarkerID = getGroupMarkerID(markerID);
            String configurableMarkerID = groupMarkerID == null ? markerID : groupMarkerID;
            Severity[] supportedSeverities = getSupportedSeverities(configurableMarkerID);

            if (supportedSeverities != null && supportedSeverities.length > 0) {
                result.put(configurableMarkerID, supportedSeverities);
            }
        }

        return result;
    }

    /**
     * Answers group marker ID for the given ID of marker create by this validator or 
     * null if the given marker does not belong to any group of markers.<br/><br/>
     * 
     * Group markers can be used for cases when the same severity configuration should be applied
     * to several markers created by this validator.
     * 
     * @param markerID marker ID
     * @return group marker ID or null
     */
    public abstract String getGroupMarkerID(String markerID);

    /**
     * Answers the name of configurable marker. 
     * 
     * @param markerID marker ID
     * @return marker name
     * @see {@link #getConfigurableMarkerIDs()}
     */
    public abstract String getMarkerName(String markerID);

    private Severity getSeverity(String markerID) {
        String groupMarkerID = getGroupMarkerID(markerID);

        if (groupMarkerID != null) {
            markerID = groupMarkerID;
        }

        Severity defaultSeverity = getDefaultSeverity(markerID);

        if (defaultSeverity == null) {
            defaultSeverity = Severity.IGNORE;
        }

        try {
            return Severity.valueOf(preferences.get(getClass().getName() + "." + markerID, defaultSeverity.name())); //$NON-NLS-1$
        } catch (Throwable t) {
            return defaultSeverity;
        }
    }


    /**
     * <code>Severity</code> enum represents the severity for code validation markers.
     *
     */
    public enum Severity {
        /**
         * Error marker severity
         */
        ERROR(IMarker.SEVERITY_ERROR),
        /**
         * Warning marker severity
         */
        WARNING(IMarker.SEVERITY_WARNING),
        /**
         * Ignored marker severity
         */
        IGNORE(-1);

        private int value;

        private Severity(int severity) {
            this.value = severity;
        }

        int getValue() {
            return value;
        }
    }
}
