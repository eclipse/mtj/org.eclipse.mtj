/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.project.midp;

/**
 * The definition of a MIDlet within the application descriptor.
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IMidletDefinition {

    /**
     * Get the name of the class extending the
     * <code>javax.microedition.midlet.MIDlet</code> class for the MIDlet.
     * 
     * @return Returns the className.
     */
    public abstract String getClassName();

    /**
     * A case-sensitive absolute path name of an image (PNG) within the JAR for
     * the icon of the MIDlet. If the icon was be omitted, will return the empty
     * string <code>""</code>.
     * 
     * @return Returns the icon.
     */
    public abstract String getIcon();

    /**
     * Get the name to be used to identify this MIDlet to the user.
     * 
     * @return Returns the name.
     */
    public abstract String getName();

    /**
     * Return the MIDlet number.
     * 
     * @return the number used in the MIDlet-<n> attribute.
     */
    public abstract int getNumber();

    /**
     * Set the name of the class extending the
     * <code>javax.microedition.midlet.MIDlet</code> class for the MIDlet. The
     * classname <strong>MUST</strong> be non-<code>null</code> and contain only
     * characters for Java class names. The class <strong>MUST</strong> have a
     * public no-args constructor. The class name <strong>IS</strong> case
     * sensitive.
     * 
     * @param className The className to set.
     */
    public abstract void setClassName(String className);

    /**
     * A case-sensitive absolute path name of an image (PNG) within the JAR for
     * the icon of the MIDlet. The icon may be omitted.
     * 
     * @param icon The icon to set.
     */
    public abstract void setIcon(String icon);

    /**
     * Name to be used to identify this MIDlet to the user. The name must be
     * present and be non-null.
     * 
     * @param name The name to set.
     */
    public abstract void setName(String name);

    /**
     * Returns a string representation of the MIDlet definition in the following
     * pattern: <br>
     * <code>&lt;MIDlet name&gt;,&lt;icon&gt;,&lt;MIDlet class name&gt;</code>.
     * 
     * @return the textual representation of the MIDlet definition.
     */
    public abstract String toString();

}