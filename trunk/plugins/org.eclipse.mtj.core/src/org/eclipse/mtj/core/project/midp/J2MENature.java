/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     David Marques (Motorola) - Renaming EclipseME to MTJ                               
 */
package org.eclipse.mtj.core.project.midp;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.project.MTJNature;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.BuildSpecManipulator;

/**
 * The nature for Java ME projects.
 * 
 * @see org.eclipse.core.resources.IProjectNature
 * @see org.eclipse.mtj.core.project.MTJNature
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * 
 * @since 1.0
 */
public class J2MENature extends MTJNature {

    /**
     * Return a boolean indicating whether the specified project has the MTJ
     * nature associated with it.
     * 
     * @param project the project to be tested for the MTJ nature
     * @return a boolean indicating whether the specified project has the MTJ
     *         nature
     * @throws CoreException if this method fails.
     */
    public static boolean hasMtjCoreNature(IProject project)
            throws CoreException {
        return project.hasNature(IMTJCoreConstants.MTJ_NATURE_ID);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.MTJNature#configure()
     */
    public void configure() throws CoreException {
        BuildSpecManipulator manipulator = new BuildSpecManipulator(getProject());
        
        manipulator.addBuilderBefore(JavaCore.BUILDER_ID, IMTJCoreConstants.J2ME_VALIDATOR_ID, null);
        manipulator.addBuilderAfter(JavaCore.BUILDER_ID, IMTJCoreConstants.J2ME_PREVERIFIER_ID, null);
        
        manipulator.commitChanges(new NullProgressMonitor());
    }
}
