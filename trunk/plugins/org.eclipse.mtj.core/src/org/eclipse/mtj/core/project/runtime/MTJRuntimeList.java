/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)      - Initial implementation
 *     Diego Sandin (Motorola) - Enhanced javadoc [bug 270532]
 */

package org.eclipse.mtj.core.project.runtime;

import java.util.*;

import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.runtime.event.AddMTJRuntimeEvent;
import org.eclipse.mtj.core.project.runtime.event.IMTJRuntimeListChangeListener;
import org.eclipse.mtj.core.project.runtime.event.RemoveMTJRuntimeEvent;
import org.eclipse.mtj.core.project.runtime.event.SwitchActiveMTJRuntimeEvent;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class is used to maintains the list of runtimes for MTJ project. Each
 * MTJ project have one instance of this class. We retrieve the instance by
 * {@link IMTJProject#getRuntimeList()}.
 * <p>
 * Each MTJRuntimeList is stored in the metadata file available in the project
 * this list belongs to.<br>
 * Bellow is a sample on the structure of a MTJRuntimeList stored in a metadata
 * file.
 * 
 * <pre>
 * &lt;configurations&gt;
 *   &lt;configuration active="true" name="Config1"&gt;
 *      ...
 *   &lt;/configuration&gt;
 *   &lt;configuration active="false" name="Config2"&gt;
 *      ...
 *   &lt;/configuration&gt;
 *   &lt;configuration active="false" name="Config3"&gt;
 *      ...
 *   &lt;/configuration&gt;
 * &lt;/configurations&gt;
 * </pre>
 * </p>
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 * 
 * @see MTJRuntime
 * 
 * @since 1.0
 */
public class MTJRuntimeList extends ArrayList<MTJRuntime> {

    /**
     * The metadata element for storing the list of runtimes.
     */
    public static final String ELEM_CONFIGURATIONS = "configurations"; //$NON-NLS-1$

    /**
     * The serial version UID for this class.
     */
    private static final long serialVersionUID = -8304504334314517586L;

    /**
     * The listeners listen to runtime list change event.
     */
    private List<IMTJRuntimeListChangeListener> listeners = new ArrayList<IMTJRuntimeListChangeListener>();

    /**
     * Creates a new instance of MTJRuntimeList. This is an empty runtime list
     * with no runtime configured yet.
     */
    public MTJRuntimeList() {
    }

    /**
     * Creates a new MTJRuntimeList from the metadata available in the project
     * holding this list of runtimes.
     * <p>
     * We try to read the runtimes from the DOM element retrieved from the
     * project metadata and put them into the runtime list.
     * </p>
     * 
     * @param runtimeListElement The DOM element containing the runtime data
     *            (the {@link #ELEM_CONFIGURATIONS} XML element).
     * @throws PersistenceException if fails to create a runtime persisted in
     *             the metadata.
     */
    public MTJRuntimeList(Element runtimeListElement)
            throws PersistenceException {
        NodeList configs = runtimeListElement
                .getElementsByTagName(MTJRuntime.ELEM_CONFIGURATION);
        Set<String> existingConfigNames = new HashSet<String>();
        
        for (int i = 0; i < configs.getLength(); i++) {
            MTJRuntime config = new MTJRuntime((Element) configs.item(i), existingConfigNames);
            add(config);
            existingConfigNames.add(config.getName());
        }
    }

    /**
     * Appends the specified MTJRuntime to the end of this list.
     * 
     * <p>
     * This method notifies all listeners after adding the MTJRuntime, through
     * the invocation of
     * {@link IMTJRuntimeListChangeListener#mtjRuntimeAdded(AddMTJRuntimeEvent)}
     * .
     * </p>
     * 
     * @param runtime MTJRuntime to be appended to this list.
     * @return <tt>true</tt> (as per the general contract of Collection.add).
     * 
     * @see java.util.ArrayList#add(java.lang.Object)
     */
    @Override
    public boolean add(MTJRuntime runtime) {

        boolean result = super.add(runtime);

        if (result == false) {
            return result;
        }

        // notify event listener
        AddMTJRuntimeEvent event = new AddMTJRuntimeEvent(this, runtime);
        for (IMTJRuntimeListChangeListener listener : listeners) {
            listener.mtjRuntimeAdded(event);
        }

        return result;
    }

    /**
     * Appends all of the MTJRuntimes in the specified Collection to the end of
     * this list, in the order that they are returned by the specified
     * Collection's Iterator.
     * <p>
     * This method notifies all listeners after adding each MTJRuntime, through
     * the invocation of
     * {@link IMTJRuntimeListChangeListener#mtjRuntimeAdded(AddMTJRuntimeEvent)}
     * .
     * </p>
     * 
     * @param runtimes the MTJRuntimes to be inserted into this list.
     * 
     * @return <tt>true</tt> if this list changed as a result of the call.
     * @throws NullPointerException if the specified collection is
     *             <code>null</code>.
     * 
     * @see java.util.ArrayList#addAll(java.util.Collection)
     * 
     */
    @Override
    public boolean addAll(Collection<? extends MTJRuntime> runtimes)
            throws NullPointerException {
        boolean modified = false;
        for (MTJRuntime runtime : runtimes) {
            modified = this.add(runtime);
        }
        return modified;
    }

    /**
     * Adds the listener to the collection of listeners who will be notified
     * when the runtime list state changes. The listener is notified by invoking
     * one of methods defined in the <code>IMTJRuntimeListChangeListener</code>
     * interface.
     * 
     * @param listener the listener that should be notified when the state of
     *            the runtime list changes.
     */
    public void addMTJRuntimeListChangeListener(
            IMTJRuntimeListChangeListener listener) {
        listeners.add(listener);
    }

    /**
     * Get the MTJRuntime that is currently active.
     * <p>
     * If no runtime is active this method will return <code>null</code>.
     * </p>
     * 
     * @return the MTJRuntime that is currently active or <code>null</code> in
     *         no active runtime could be found.
     */
    public MTJRuntime getActiveMTJRuntime() {
        for (MTJRuntime c : this) {
            if (c.isActive()) {
                return c;
            }
        }
        return null;
    }

    /**
     * Removes a single instance of the specified MTJRuntime from this list, if
     * it is present.
     * <p>
     * This method notifies all listeners after removing the MTJRuntime, through
     * the invocation of
     * {@link IMTJRuntimeListChangeListener#mtjRuntimeRemoved(RemoveMTJRuntimeEvent)}
     * .
     * </p>
     * 
     * @param o MTJRuntime to be removed from this list, if present.
     * @return <tt>true</tt> if the list contained the specified MTJRuntime.
     * 
     * @see java.util.ArrayList#remove(java.lang.Object)
     */
    @Override
    public boolean remove(Object o) {
        boolean result = super.remove(o);
        if (result == false) {
            return result;
        }
        if (o instanceof MTJRuntime) {
            MTJRuntime removedConfig = (MTJRuntime) o;
            RemoveMTJRuntimeEvent event = new RemoveMTJRuntimeEvent(this,
                    removedConfig);
            for (IMTJRuntimeListChangeListener listener : listeners) {
                listener.mtjRuntimeRemoved(event);
            }
        }
        return result;
    }

    /**
     * Removes from this collection all of its MTJRuntimes that are contained in
     * the specified collection.
     * 
     * <p>
     * This method notifies all listeners after removing each MTJRuntime,
     * through the invocation of
     * {@link IMTJRuntimeListChangeListener#mtjRuntimeRemoved(RemoveMTJRuntimeEvent)}
     * .
     * </p>
     * 
     * @param runtimes MTJRuntimes to be removed from this collection.
     * @return <tt>true</tt> if this collection changed as a result of the call.
     * @throws NullPointerException if the specified collection is
     *             <code>null</code>.
     * @see java.util.AbstractCollection#removeAll(java.util.Collection)
     */
    @Override
    public boolean removeAll(Collection<?> runtimes) {
        boolean modified = false;
        for (Object o : runtimes) {
            modified = this.remove(o);
        }
        return modified;
    }

    /**
     * Removes the listener from the collection of listeners who will be
     * notified when the runtime state changes.
     * <p>
     * <b>Note:</b>Since instance of MTJRuntime have a long life cycle (as long
     * as the MTJ project), clients should remove listener manually when it no
     * longer used.
     * </p>
     * 
     * @param listener the listener that should no longer be notified when the
     *            state of the runtime list changes.
     */
    public void removeMTJRuntimeListChangeListener(
            IMTJRuntimeListChangeListener listener) {
        listeners.remove(listener);
    }

    /**
     * Switch the current active MTJRuntime.
     * 
     * @param runtime MTJRuntime to be set as the active one.
     */
    public void switchActiveMTJRuntime(MTJRuntime runtime) {
        MTJRuntime oldActiveConfig = getActiveMTJRuntime();
        if (oldActiveConfig != null) {
            getActiveMTJRuntime().setActive(false);
        }
        runtime.setActive(true);
        // notify event listener
        SwitchActiveMTJRuntimeEvent event = new SwitchActiveMTJRuntimeEvent(
                this, oldActiveConfig, runtime);
        for (IMTJRuntimeListChangeListener listener : listeners) {
            listener.activeMTJRuntimeSwitched(event);
        }
    }

}
