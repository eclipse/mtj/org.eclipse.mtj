/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)          - Initial implementation
 *     Gustavo de Paula (Motorola) - Refactored class name to MTJRuntime
 *     Diego Sandin (Motorola)     - Enhanced javadoc [bug 270532]
 *     David Marques (Motorola)    - Fixing SymbolSet name loading.
 *     David Marques (Motorola)    - Cloning SymbolSet references for runtimes.
 *     Daniel Olsson (Sony Ericsson)- Matching of non-found devices  [Bug - 284262]
 *     Jon Dearden (Research In Motion) - Added new constants for supporting SDK provider 
 *                                        meta data [Bug 286675].
 **/
package org.eclipse.mtj.core.project.runtime;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.runtime.event.IMTJRuntimeChangeListener;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeDeviceChangeEvent;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeNameChangeEvent;
import org.eclipse.mtj.core.project.runtime.event.MTJRuntimeWorkspaceSymbolSetsChangeEvent;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.eclipse.mtj.core.sdk.device.IDeviceRegistry;
import org.eclipse.mtj.core.symbol.ISymbol;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.core.symbol.ISymbolSetRegistry;
import org.eclipse.mtj.internal.core.project.runtime.MTJRuntimeListUtils;
import org.eclipse.mtj.internal.core.sdk.device.IDeviceMatcher;
import org.eclipse.mtj.internal.core.symbol.SymbolSet;
import org.eclipse.mtj.internal.core.symbol.SymbolUtils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This class contains runtime information for multiples runtimes support. Now
 * it contains "device" and "symbol set" information, and a boolean "active" to
 * indicate if this runtime is the current active one.
 * <p>
 * Each MTJRuntime is stored in the metadata file available in the project this
 * runtime belongs to.<br>
 * Bellow is a sample on the structure of a MTJRuntime stored in a metadata
 * file.
 * 
 * <pre>
 *      &lt;configuration active="true" name="Default device"&gt;
 *         &lt;device group="Microemulator" name="Default device"/&gt;
 *         &lt;symbolSet>
 *            &lt;symbol name="CLDC" value="1.1"/&gt;
 *            &lt;symbol name="MIDP" value="2.0"/&gt;
 *            &lt;symbol name="JSR135" value="1.0"/&gt;
 *            &lt;symbol name="JSR75" value="1.0"/&gt;
 *            &lt;symbol name="JSR120" value="1.0"/&gt;
 *           &lt;symbol name="JSR82" value="1.1"/&gt;
 *         &lt;/symbolSet&gt;
 *      &lt;/configuration&gt;
 * </pre>
 * </p>
 * 
 * @noinstantiate This class is not intended to be instantiated by clients.
 * @noextend This class is not intended to be subclassed by clients.
 * @see ISymbolSet
 * 
 * @since 1.0
 */
public class MTJRuntime {

    /**
     * The metadata attribute for storing the group from which the device
     * configured in a runtime may be retrieved.
     */
    public static final String ATTR_DEVICEGROUP = "group"; //$NON-NLS-1$

    /**
     * The metadata attribute for storing the name of the device associated to a
     * runtime.
     */
    public static final String ATTR_DEVICENAME = "name"; //$NON-NLS-1$

    /**
     * The metadata attribute for storing if one runtime is active.
     */
    public static final String ATTR_RUNTIMEACTIVE = "active"; //$NON-NLS-1$

    /**
     * The metadata attribute for storing the name of a runtime.
     */
    public static final String ATTR_RUNTIMENAME = "name"; //$NON-NLS-1$

    /**
     * The metadata attribute for storing the name of the SymbolSet.
     */
    public static final String ATTR_SYMBOL_SET_NAME = "name"; //$NON-NLS-1$

    /**
     * The metadata attribute for storing the name of a symbol.
     */
    public static final String ATTR_SYMBOLNAME = "name"; //$NON-NLS-1$
    
    /**
     * The metadata attribute for storing a Workspace SymbolSet exported by the
     * runtime.
     */
    public static final String ATTR_WORKSPACESYMBOLSETNAME = "name"; //$NON-NLS-1$
    
    /**
     * The metadata attribute for storing the value of a symbol.
     */
    public static final String ATTR_SYMBOLVALUE = "value"; //$NON-NLS-1$

    /**
     * The metadata attribute for storing the ID of a SDK.
     */
    public static final String ATTR_SDK_ID = "id"; //$NON-NLS-1$
    
    /**
     * The metadata attribute for storing the name of a SDK property.
     */
    public static final String ATTR_SDK_PROPERTY_NAME = "name"; //$NON-NLS-1$
    
    /**
     * The metadata attribute for storing the value of a SDK property.
     */
    public static final String ATTR_SDK_PROPERTY_VALUE = "value"; //$NON-NLS-1$
    
    /**
     * The metadata element for storing the runtime.
     */
    public static final String ELEM_CONFIGURATION = "configuration"; //$NON-NLS-1$

    /**
     * The metadata element for storing the device configured in a runtime.
     */
    public static final String ELEM_DEVICE = "device"; //$NON-NLS-1$

    /**
     * The metadata element for storing a symbol.
     */
    public static final String ELEM_SYMBOL = "symbol"; //$NON-NLS-1$

    /**
     * The metadata element for storing the set of symbols associated to a
     * runtime.
     */
    public static final String ELEM_SYMBOL_SET = "symbolSet"; //$NON-NLS-1$

    /**
     * The metadata element for storing the symbol sets that are publicly
     * available in the workspace.
     */
    public static final String ELEM_WORKSPACE_SYMBOLSET = "workspaceSymbolSet"; //$NON-NLS-1$
    
    /**
     * The metadata element for storing an optional collection of arbitrary meta
     * data for the use of an SDK provider.
     */
    public static final String ELEM_SDK_PROVIDER_PROPERTIES = "sdkProviderProperties"; //$NON-NLS-1$
    
    /**
     * The metadata element for storing the meta data for a specific vendor-provided SDK.
     */
    public static final String ELEM_SDK = "sdk"; //$NON-NLS-1$
    
    /**
     * The metadata element for storing specific property for an SDK.
     */
    public static final String ELEM_SDK_PROPERTY = "property"; //$NON-NLS-1$

    /**
     * The boolean value to indicate if this runtime is the current active one.
     */
    private boolean active;

    /**
     * The device of the runtime.
     */
    private IDevice device;

    /**
     * The listeners listen to this runtime properties change event.
     */
    private List<IMTJRuntimeChangeListener> listeners = new ArrayList<IMTJRuntimeChangeListener>();

    /**
     * The runtime name.
     */
    private String name;

    /**
     * The symbols of the runtime. For preprocessing support.
     */
    private ISymbolSet symbolSet;

    /**
     * SymbolSets from workspace scope.
     */
    private List<ISymbolSet> workspaceScopeSymbolSets;

    /**
     * Specifies weather this config resembles what is in the config file or if
     * it has been adapted to fit the local environment.
     */
    private boolean isMatched = false;

    /**
     * Construct a runtime from the metadata available in the project holding
     * this runtime.
     * <p>
     * This constructor follows the following steps to load the runtime
     * information from the metadata:
     * <ol>
     * <li>Retrieve the instance of the IDevice associated to this runtime from
     * the {@link IDeviceRegistry}.</li>
     * <li>Load the SymbolSets.</li>
     * <li>Load the SymbolSets from workspace scope using the
     * {@link ISymbolSetRegistry}.</li>
     * </ol>
     * </p>
     * 
     * @param configElement The DOM element containing the runtime data (the
     *            {@link #ELEM_CONFIGURATION} XML element).
     * @throws PersistenceException if the runtime could not be created. Reasons
     *             include:
     *             <ul>
     *             <li>Could not load the device from the
     *             {@link IDeviceRegistry}.</li>
     *             <li>Could not load the WorkspaceSymbolSets from the
     *             {@link ISymbolSetRegistry}.</li>
     *             </ul>
     */
    public MTJRuntime(Element configElement, Set<String> existingConfigNames) throws PersistenceException {
        name = configElement.getAttribute(ATTR_RUNTIMENAME);
        active = Boolean
                .valueOf(configElement.getAttribute(ATTR_RUNTIMEACTIVE));
        loadDevice(configElement, existingConfigNames);
        loadSymbolSet(configElement);
        loadWorkspaceSymbolSets(configElement);
    }

    /**
     * Creates a new instance of MTJRuntime with the given name.
     * <p>
     * The runtime name <strong>must</strong> be unique in the project scope.
     * </p>
     * 
     * @param name the unique name for the new runtime. This is case-sensitive
     *            and must not be <code>null</code> or an empty String
     *            <code>""</code>.
     */
    public MTJRuntime(String name) {
        this.name = name;
    }

    /**
     * Adds the listener to the collection of listeners who will be notified
     * when the runtime state changes. The listener is notified by invoking one
     * of methods defined in the <code>IMTJRuntimeChangeListener</code>
     * interface.
     * 
     * @param listener the listener that should be notified when the runtime
     *            state changes.
     */
    public void addMTJRuntimeChangeListener(IMTJRuntimeChangeListener listener) {
        listeners.add(listener);
    }

    /**
     * Indicates whether some other MTJRuntine is "equal to" this one. If the
     * names are equal, then the runtimes are considered equals.
     * 
     * @param obj the reference MTJRuntine with which to compare.
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MTJRuntime other = (MTJRuntime) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    /**
     * Notify listeners that the symbolSet has changed.
     * 
     * <p>
     * This method notifies all listeners that the symbolSet has changed through
     * the invocation of {@link IMTJRuntimeChangeListener#symbolSetChanged()}.
     * </p>
     */
    public void fireSymbolSetChanged() {
        for (IMTJRuntimeChangeListener listener : listeners) {
            listener.symbolSetChanged();
        }
    }

    /**
     * Returns the IDevice instance associated to this runtime.
     * 
     * @return the IDevice instance associated to this runtime.
     */
    public IDevice getDevice() {
        return device;
    }

    /**
     * Returns the unique name of this runtime.
     * 
     * @return the name of the runtime.
     */
    public String getName() {
        return name;
    }

    /**
     * Return the Symbolset associated with this runtime.
     * <p>
     * If no symbolset was previously specified, this method will return a newly
     * created SysbolSet using "Default" as name.
     * </p>
     * <p>
     * <strong>NOTE:</strong> All preprocessing related code should NOT use this
     * method to get ISymbolSet, instead,
     * {@link #getSymbolSetForPreprocessing()} should be used for preprocessing
     * purpose.
     * </p>
     * 
     * @return Symbol set associated with this runtime.
     * @see #getSymbolSetForPreprocessing()
     */
    public ISymbolSet getSymbolSet() {
        if (symbolSet == null) {
        	SymbolSet deviceSymbolSet = (SymbolSet) device.getSymbolSet();
        	try {				
        		symbolSet = (ISymbolSet) deviceSymbolSet.clone();
			} catch (CloneNotSupportedException e) {
				symbolSet = new SymbolSet();
				symbolSet.setName(this.getName());
			}
        }
        return symbolSet;
    }

    /**
     * Return the Symbolset associated with this runtime refactored to be used
     * in preprocessing.
     * <p>
     * <strong>NOTE:</strong> All preprocessing related function
     * <strong>must</strong> use this method to get the SymbolSet.
     * </p>
     * <p>
     * The returned SymbolSet contains one more Symbol then SymbolSet returned
     * by {@link #getSymbolSet()}. (key = the runtime name(With spaces replaced
     * by <code>"_"</code>), value=<code>true</code>)
     * </p>
     * <p>
     * The name of the generated Symbol set will be the same from the one
     * returned by {@link #getSymbolSet()} plus the
     * <code>" For Preprocessing"</code> suffix.
     * </p>
     * 
     * @return a version of the runtime symbolset to be used in preprocessing.
     */
    public ISymbolSet getSymbolSetForPreprocessing() {

        ISymbolSet symbolSetForPreprocessing = MTJCore.getSymbolSetFactory()
                .createSymbolSet(
                        getSymbolSet().getName() + " For Preprocessing"); //$NON-NLS-1$

        symbolSetForPreprocessing.add(symbolSet.getSymbols());

        String configName = getName().replace(' ', '_');
        configName = SymbolUtils.replaceFirstNonLetterChar(configName);
        symbolSetForPreprocessing.add(configName, Boolean.TRUE.toString(),
                ISymbol.TYPE_RUNTIME);

        return symbolSetForPreprocessing;
    }

    /**
     * Returns a list of referenced SymbolSets that are on the workspace level.
     * Those SymbolSets are used during the code preprocessing.
     * <p>
     * If no workspace SymbolSets were specified, an empty list will be
     * returned.
     * </p>
     * 
     * @return the list of symbol sets on the workspace level or an an empty
     *         list in case no workspace SymbolSets were specified.
     */
    public List<ISymbolSet> getWorkspaceScopeSymbolSets() {
        if (workspaceScopeSymbolSets == null) {
            workspaceScopeSymbolSets = new ArrayList<ISymbolSet>();
        }
        return workspaceScopeSymbolSets;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /**
     * Indicates whenever this runtime is active.
     * 
     * @return <code>true</code> if this runtime is active, <code>false</code>
     *         otherwise.
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Removes the listener from the collection of listeners who will be
     * notified when the runtime state changes.
     * <p>
     * <b>Note:</b>Since instance of MTJRuntime have a long life cycle (as long
     * as the MTJ project), clients should remove listener manually when it no
     * longer used.
     * </p>
     * 
     * @param listener the listener that should no longer be notified when the
     *            runtime state changes.
     */
    public void removeMTJRuntimeChangeListener(
            IMTJRuntimeChangeListener listener) {
        listeners.remove(listener);
    }

    /**
     * Set if the runtime is active or not.
     * 
     * @param active the new activation state. Use <code>true</code> if this
     *            runtime must be activated, <code>false</code> otherwise.
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Sets the IDevice instance associated to the runtime.
     * <p>
     * This method notifies all listeners after setting the new device, through
     * the invocation of
     * {@link IMTJRuntimeChangeListener#deviceChanged(MTJRuntimeDeviceChangeEvent)}
     * .
     * </p>
     * <p>
     * If the given device is the same as the older one nothing is updated and
     * no event will be fired.
     * <p>
     * 
     * @param device the new device to be associated to this runtime.
     */
    public void setDevice(IDevice device) {

        // If device is the same object, just return
        if (this.device == device) {
            return;
        }

        IDevice oldDevice = this.device;
        this.device = device;

        symbolSet = device.getSymbolSet();

        // notify listeners
        MTJRuntimeDeviceChangeEvent event = new MTJRuntimeDeviceChangeEvent(
                this, oldDevice, device);
        for (IMTJRuntimeChangeListener listener : listeners) {
            listener.deviceChanged(event);
        }

    }

    /**
     * Sets a new name for the runtime. The name <strong>must</strong> be unique
     * in the project scope.
     * <p>
     * The name is case-sensitive and must not be <code>null</code> or an empty
     * String <code>""</code>.
     * </p>
     * <p>
     * If the given name is the same as the older one nothing will be updated
     * and no event will be fired.
     * <p>
     * <p>
     * This method notifies all listeners after changing the name of the runtime
     * through the invocation of
     * {@link IMTJRuntimeChangeListener#nameChanged(MTJRuntimeNameChangeEvent)}.
     * </p>
     * 
     * @param name the new name for the runtime. This is case-sensitive and must
     *            not be <code>null</code> or an empty String <code>""</code>.
     */
    public void setName(String name) {
        if (this.name.equals(name)) {
            return;
        }
        String oldName = this.name;
        this.name = name;
        // notify listeners
        MTJRuntimeNameChangeEvent event = new MTJRuntimeNameChangeEvent(this,
                oldName, name);
        for (IMTJRuntimeChangeListener listener : listeners) {
            listener.nameChanged(event);
        }

    }

    /**
     * Set the Symbolset associated with this runtime.
     * 
     * @param symbolSet the new Symbolset to be associated with this runtime.
     */
    public void setSymbolSet(ISymbolSet symbolSet) {
        this.symbolSet = symbolSet;
    }

    /**
     * Set the list of referenced SymbolSets that are on the workspace level.
     * Those SymbolSets are used during the code preprocessing.
     * <p>
     * This method notifies all listeners after setting the Workspace Scope
     * SymbolSets through the invocation of
     * {@link IMTJRuntimeChangeListener#workspaceScopeSymbolSetsChanged(MTJRuntimeWorkspaceSymbolSetsChangeEvent)}
     * .
     * </p>
     * <p>
     * If the given list of SymbolSets is the same as the older one nothing is
     * updated and no event will be fired.
     * <p>
     * 
     * @param symbolSets list of referenced SymbolSets that are on the workspace
     *            level.
     */
    public void setWorkspaceScopeSymbolSets(List<ISymbolSet> symbolSets) {

        if (MTJRuntimeListUtils.workspaceSymbolsetsEquals(
                workspaceScopeSymbolSets, symbolSets)) {
            return;
        }

        List<ISymbolSet> oldSets = workspaceScopeSymbolSets;
        workspaceScopeSymbolSets = symbolSets;

        // notify listeners
        MTJRuntimeWorkspaceSymbolSetsChangeEvent event = new MTJRuntimeWorkspaceSymbolSetsChangeEvent(
                this, oldSets, symbolSets);
        for (IMTJRuntimeChangeListener listener : listeners) {
            listener.workspaceScopeSymbolSetsChanged(event);
        }

    }

    /**
     * This method returns a string equal to the value of: <blockquote>
     * 
     * <pre>
     * "name=" + {@link #getName()} + "|active=" + {@link #isActive()} + "|device=" + {@link #getDevice()} + "|symbolSet=" + {@link #getSymbolSet()}
     * </pre>
     * 
     * </blockquote>
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer().append("name=").append(name) //$NON-NLS-1$
                .append("|active=").append(active).append("|device=").append( //$NON-NLS-1$ //$NON-NLS-2$
                        device.getName()).append("|symbolSet=").append( //$NON-NLS-1$
                        symbolSet);
        return sb.toString();
    }

	/**
	 * Load the IDevice instance persisted in the {@link #ELEM_DEVICE} element
	 * from the XML file. If not found try to match it against installed
	 * {@link IDeviceMatcher}
	 * 
	 * @param configElement
	 *            The DOM element containing the device reference.
	 * @throws PersistenceException
	 *             if fails to load the device from the {@link IDeviceRegistry}.
	 */
	private void loadDevice(Element configElement, Set<String> existingConfigNames) throws PersistenceException {
		Element deviceElement = XMLUtils.getFirstElementWithTagName(
				configElement, ELEM_DEVICE);
		if (deviceElement != null) {
			String deviceGroup = deviceElement.getAttribute(ATTR_DEVICEGROUP);
			String deviceName = deviceElement.getAttribute(ATTR_DEVICENAME);
			if (!loadDevice(deviceGroup, deviceName)) {
				String[] matched = MTJRuntimeListUtils.match(name, deviceGroup,
						deviceName, existingConfigNames);
				if (matched != null && loadDevice(matched[1], matched[2])) {
					name = matched[0];
					MTJLogger.log(IStatus.INFO, deviceGroup + "/" + deviceName //$NON-NLS-1$
							+ " was converted to " + device.toString()); //$NON-NLS-1$
					isMatched = true;
				}
			}
		}
	}

	/**
	 * @param deviceGroup
	 * @param deviceName
	 * @throws PersistenceException
	 */
	private boolean loadDevice(String deviceGroup, String deviceName)
			throws PersistenceException {
		device = MTJCore.getDeviceRegistry().getDevice(deviceGroup, deviceName);
		if (device == null) {
			return false;
		} else {
			return true;
		}
	}

    /**
     * Load symbols from the stored SymbolSet.
     * 
     * @param configElement The DOM element containing the symbolSet.
     */
    private void loadSymbolSet(Element configElement) {
        symbolSet = new SymbolSet();

        Element symbolSetElement = XMLUtils.getFirstElementWithTagName(
        		configElement, ELEM_SYMBOL_SET);
        
        if (!symbolSetElement.getAttribute(ATTR_SYMBOL_SET_NAME).equals("")) {
            symbolSet.setName(symbolSetElement.getAttribute(ATTR_SYMBOL_SET_NAME));
        } else {
            if (device != null) {
                symbolSet.setName(device.getName());
            }
        }

        NodeList symbols = symbolSetElement.getElementsByTagName(ELEM_SYMBOL);
        for (int i = 0; i < symbols.getLength(); i++) {
            Element symbolElement = (Element) symbols.item(i);
            String symbolName = symbolElement.getAttribute(ATTR_SYMBOLNAME);
            String symbolValue = symbolElement.getAttribute(ATTR_SYMBOLVALUE);
            symbolSet.add(symbolName, symbolValue);
        }
    }

    /**
     * Load the SymbolSets from workspace scope.
     * 
     * @param configElement The DOM element containing the workspaceSymbolSet
     *            references.
     * @throws PersistenceException if fails to retrieve the symbols available
     *             in the workspaceSymbolSet.
     */
    private void loadWorkspaceSymbolSets(Element configElement)
            throws PersistenceException {
        NodeList symbolSets = configElement
                .getElementsByTagName(ELEM_WORKSPACE_SYMBOLSET);
        if ((symbolSets == null) || (symbolSets.getLength() == 0)) {
            return;
        }
        workspaceScopeSymbolSets = new ArrayList<ISymbolSet>();
        for (int i = 0; i < symbolSets.getLength(); i++) {
            Element symbolSet = (Element) symbolSets.item(i);
            String symbolSetName = symbolSet.getAttribute(ATTR_SYMBOLNAME);
            ISymbolSet definitionSet = MTJCore.getSymbolSetRegistry()
                    .getSymbolSet(symbolSetName);
            if (definitionSet != null) {
                workspaceScopeSymbolSets.add(definitionSet);
            }
        }
    }

    public boolean isMatched() {
        return isMatched;
    }
}
