/**
 * 
 */
package org.eclipse.mtj.internal.core.sdk;

import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.core.sdk.ISDKProvider;

/**
 * Basic implementation of the ISDK interface that deals with
 * the devices that have been "imported" rather than those that
 * have been provided by {@link ISDKProvider} instances.
 * 
 * @author Craig Setera
 */
public abstract class PersistentSDK implements ISDK {

	public boolean isPersistent() {
		return true;
	}

	public void loadUsing(IPersistenceProvider persistenceProvider)
		throws PersistenceException 
	{
		// TODO Persist the devices that are part of this SDK
	}

	public void storeUsing(IPersistenceProvider persistenceProvider)
		throws PersistenceException 
	{
		// TODO Persist the devices that are part of this SDK
	}
}
