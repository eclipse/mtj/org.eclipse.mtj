/**
 * 
 */
package org.eclipse.mtj.internal.core.sdk;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.mtj.core.sdk.ISDKProvider;

/**
 * Wraps the {@link IConfigurationElement} that defines an {@link ISDKProvider}.
 * 
 * @author Craig Setera
 */
public class SDKProviderExtension {

    private static final String ATTR_ID = "id";
    private static final String ATTR_NAME = "name";
    private static final String ATTR_CLASS = "class";

    private IConfigurationElement configurationElement;

    /**
     * Construct a new extension wrapper.
     * 
     * @param configurationElement
     */
    public SDKProviderExtension(IConfigurationElement configurationElement) {
    	this.configurationElement = configurationElement;
    }

    /**
     * Return the id of this extension element.
     * 
     * @return
     */
    public String getId() {
        return configurationElement.getAttribute(ATTR_ID);
    }

    /**
     * Return the name of this extension element.
     * 
     * @return
     */
    public String getName() {
        return configurationElement.getAttribute(ATTR_NAME);
    }

    /**
     * Get a new instance of the {@link ISDKProvider} for the configuration element.
     * 
     * @return
     * @throws CoreException
     */
    public ISDKProvider getSDKProvider() throws CoreException {
        return (ISDKProvider) configurationElement.createExecutableExtension(ATTR_CLASS);
    }
}
