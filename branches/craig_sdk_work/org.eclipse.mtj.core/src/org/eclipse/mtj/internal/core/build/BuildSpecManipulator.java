/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     David Marques (Motorola) - Adding isFirstBuilder and isLastBuilder.
 */
package org.eclipse.mtj.internal.core.build;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;

/**
 * Handles changes to the project builder command list.
 * 
 * @author Craig Setera
 */
public class BuildSpecManipulator {

    private IProject project;
    private IProjectDescription projectDescription;
    private boolean updated;
    private ArrayList<ICommand> commands;

    /**
     * Construct a new command handler
     * 
     * @throws CoreException
     */
    public BuildSpecManipulator(IProject project) throws CoreException {
        this.project = project;
        this.projectDescription = project.getDescription();
    }

    /**
     * Add a new builder after the specified build.
     * 
     * @param relativeBuilder
     * @param builder
     * @param arguments
     * @throws CoreException
     */
    public void addBuilderAfter(String relativeBuilder, String builder,
            Map<?, ?> arguments) throws CoreException {
        ArrayList<ICommand> commands = getCommands();
        ICommand command = newCommand(builder, arguments);

        int relativeIndex = indexOf(relativeBuilder);
        if ((relativeIndex == -1) || (relativeIndex == (commands.size() - 1))) {
            commands.add(command);
        } else {
            commands.add(relativeIndex + 1, command);
        }

        updated = true;
    }

    /**
     * Add a new builder before the specified build.
     * 
     * @param relativeBuilder
     * @param builder
     * @param arguments
     * @throws CoreException
     */
    public void addBuilderBefore(String relativeBuilder, String builder,
            Map<?, ?> arguments) throws CoreException {
        ArrayList<ICommand> commands = getCommands();
        ICommand command = newCommand(builder, arguments);

        int relativeIndex = indexOf(relativeBuilder);
        if ((relativeIndex == -1) || (commands.size() == 0)) {
            commands.add(command);
        } else {
            commands.add(relativeIndex, command);
        }

        updated = true;
    }

    /**
     * Commit any changes made to the list of commands in the build
     * specification.
     * 
     * @param monitor
     * @throws CoreException
     */
    public void commitChanges(IProgressMonitor monitor) throws CoreException {
        if (updated) {
            IProjectDescription description = project.getDescription();
            ICommand[] commandArray = (ICommand[]) commands
                    .toArray(new ICommand[commands.size()]);
            description.setBuildSpec(commandArray);
            project.setDescription(description, monitor);

            updated = false;
        }
    }

    /**
     * Return a boolean indicating whether the specified builder has arguments.
     * 
     * @param builder
     * @return
     * @throws CoreException
     */
    public boolean hasArguments(String builder) throws CoreException {
        boolean hasArgs = false;

        int index = indexOf(builder);
        if (index != -1) {
            ICommand command = (ICommand) getCommands().get(index);
            Map<?, ?> args = command.getArguments();
            hasArgs = (args != null) && (args.size() > 0);
        }

        return hasArgs;
    }

    /**
     * Return a boolean indicating whether the command list contains the
     * specified builder.
     * 
     * @param builder
     * @return
     * @throws CoreException
     */
    public boolean hasBuilder(String builder) throws CoreException {
        return (indexOf(builder) != -1);
    }

    /**
     * Find the specified builder in the list of builders and return the index.
     * Return <code>-1</code> if not found.
     * 
     * @param builder
     * @return
     * @throws CoreException
     */
    public int indexOf(String builder) throws CoreException {
        int index = -1;

        ArrayList<ICommand> list = getCommands();
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ICommand command = (ICommand) list.get(i);
                if (command.getBuilderName().equals(builder)) {
                    index = i;
                    break;
                }
            }
        }

        return index;
    }

    /**
     * Replaces the specified builder with a new command and the specified
     * arguments. Does nothing if the specified builder could not be found.
     * 
     * @param builder
     * @param arguments
     * @throws CoreException
     */
    public void replaceBuilder(String builder, Map<?, ?> arguments)
            throws CoreException {
        ArrayList<ICommand> commands = getCommands();
        ICommand command = newCommand(builder, arguments);

        int relativeIndex = indexOf(builder);
        if (relativeIndex != -1) {
            commands.set(relativeIndex, command);
            updated = true;
        }
    }

    /**
     * Remove the builder with the specified identifier. Does nothing if the
     * builder is not in the list.
     * 
     * @param builder
     * @throws CoreException
     */
    public void removeBuilder(String builder) throws CoreException {
        int index = indexOf(builder);
        if (index != -1) {
            getCommands().remove(index);
            updated = true;
        }
    }

    /**
     * Create a new command instance for the specified builder.
     * 
     * @param description
     * @param id
     * @return
     */
    private ICommand newCommand(String builder, Map<?, ?> arguments) {
        ICommand command = projectDescription.newCommand();
        command.setBuilderName(builder);
        if (arguments != null) {
            command.setArguments(arguments);
        }

        return command;
    }

    /**
     * Return the commands as a list that can be manipulated.
     * 
     * @return
     * @throws CoreException
     */
    private ArrayList<ICommand> getCommands() throws CoreException {
        if (commands == null) {
            ICommand[] commandArray = projectDescription.getBuildSpec();
            commands = new ArrayList<ICommand>(Arrays.asList(commandArray));
        }

        return commands;
    }

	/**
	 * Checks whether the builder is the first one.
	 * 
	 * @param builderId target builder id.
	 * @return true if first false if not.
	 * @throws CoreException Any core error occurred.
	 */
	public boolean isFirstBuilder(String builderId) throws CoreException {
		boolean result = false;
		List<ICommand> commands = getCommands();
		String id = commands.get(0).getBuilderName();
		if (id != null && id.equals(builderId)) {
			result = true;
		}
		return result;
	}
	
	/**
	 * Checks whether the builder is the last one.
	 * 
	 * @param builderId target builder id.
	 * @return true if last false if not.
	 * @throws CoreException Any core error occurred.
	 */
	public boolean isLastBuilder(String builderId) throws CoreException {
		boolean result = false;
		List<ICommand> commands = getCommands();
		String id = commands.get(commands.size() - 1).getBuilderName();
		if (id != null && id.equals(builderId)) {
			result = true;
		}
		return result;
	}
}
