/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.ILibrary;

/**
 * A representation of a deviceClasspath that can be converted to various other
 * representations.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noextend This class is not intended to be subclassed by clients.
 * @author Craig Setera
 */
public class DeviceClasspath implements IDeviceClasspath {

    private ArrayList<ILibrary> entries;

    /**
     * Construct a new deviceClasspath instance
     */
    public DeviceClasspath() {
        super();
        entries = new ArrayList<ILibrary>();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceClasspath#addEntry(org.eclipse.mtj.core.sdk.device.midp.library.ILibrary)
     */
    public void addEntry(ILibrary pathEntry) {
        entries.add(pathEntry);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceClasspath#asClasspathEntries()
     */
    public List<IClasspathEntry> asClasspathEntries() {
        List<ILibrary> allEntries = getEntries();
        List<IClasspathEntry> cpEntries = new ArrayList<IClasspathEntry>(
                allEntries.size());

        for (int i = 0; i < allEntries.size(); i++) {
            cpEntries.add(allEntries.get(i).toClasspathEntry());

        }

        return cpEntries;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceClasspath#equals(org.eclipse.mtj.core.sdk.device.IDeviceClasspath)
     */
    public boolean equals(IDeviceClasspath deviceClasspath) {
        List<ILibrary> myEntries = getEntries();
        List<ILibrary> otherEntries = deviceClasspath.getEntries();

        boolean equals = (myEntries.size() == otherEntries.size());
        for (int i = 0; equals && (i < myEntries.size()); i++) {
            equals = myEntries.get(i).equals(otherEntries.get(i));
        }

        return equals;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceClasspath#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        boolean equals = false;

        if (obj instanceof DeviceClasspath) {
            equals = equals((IDeviceClasspath) obj);
        }

        return equals;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceClasspath#getEntries()
     */
    public List<ILibrary> getEntries() {
        return entries;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceClasspath#hashCode()
     */
    @Override
    public int hashCode() {
        List<ILibrary> allEntries = getEntries();
        int hashCode = allEntries.size() << 24;

        for (ILibrary allEntrie : allEntries) {
            hashCode = hashCode ^ allEntrie.hashCode();
        }

        return hashCode;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        entries.clear();
        int entryCount = persistenceProvider.loadInteger("entryCount"); //$NON-NLS-1$
        for (int i = 0; i < entryCount; i++) {
            entries.add((ILibrary) persistenceProvider
                    .loadPersistable("entry" + i)); //$NON-NLS-1$
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceClasspath#removeEntry(org.eclipse.mtj.core.sdk.device.midp.library.ILibrary)
     */
    public void removeEntry(ILibrary library) {
        entries.remove(library);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        persistenceProvider.storeInteger("entryCount", entries.size()); //$NON-NLS-1$

        int i = 0;
        for (ILibrary lib : entries) {
            persistenceProvider.storePersistable("entry" + i++, lib); //$NON-NLS-1$
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDeviceClasspath#toString()
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();

        List<ILibrary> allEntries = getEntries();

        for (int i = 0; i < allEntries.size(); i++) {
            if (i != 0) {
                sb.append(File.pathSeparatorChar);
            }

            sb.append(allEntries.get(i).toFile());
        }

        return sb.toString();
    }
}
