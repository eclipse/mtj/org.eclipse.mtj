/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.util.tools;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchDelegate;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.IStatusHandler;
import org.eclipse.debug.core.Launch;
import org.eclipse.debug.core.model.ILaunchConfigurationDelegate;
import org.eclipse.jdt.launching.IJavaLaunchConfigurationConstants;
import org.eclipse.jdt.launching.IRuntimeClasspathEntry;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.launching.LibraryLocation;
import org.eclipse.mtj.internal.core.Messages;
import org.eclipse.mtj.internal.core.util.Utils;

/**
 * This class provides abstract functionality for the easy creation of
 * Java-based tools.
 * 
 * @author Craig Setera
 */
public abstract class AbstractJavaTool {

    public static final String JAVA_APP_LAUNCH_ID = "org.eclipse.jdt.launching.localJavaApplication"; //$NON-NLS-1$

    /**
     * Status handler to prompt in the UI thread
     */
    private static final IStatus promptStatus = new Status(IStatus.INFO,
            "org.eclipse.debug.ui", 200, "", null); //$NON-NLS-1$//$NON-NLS-2$

    /**
     * Status handler to prompt the user to resolve the missing launch delegate
     * issue
     */
    private static final IStatus delegateNotAvailable = new Status(
            IStatus.INFO, "org.eclipse.debug.core", 226, "", null); //$NON-NLS-1$ //$NON-NLS-2$

    /**
     * Get the application arguments.
     * 
     * @return
     */
    protected abstract String[] getArguments();

    /**
     * Get the name of the class to be run.
     * 
     * @return
     * @uml.property name="className"
     */
    protected abstract String getClassName();

    /**
     * Get the classpath for this run.
     * 
     * @return
     */
    protected abstract String[] getClasspath();

    /**
     * Get the name of this tool.
     * 
     * @return
     * @uml.property name="name"
     */
    protected abstract String getName();

    /**
     * Get the virtual machine arguments.
     * 
     * @return
     */
    protected abstract String[] getVMArguments();

    /**
     * Launch the java class with all of the specified arguments.
     * 
     * @param monitor
     * @return
     */
    public ILaunch launch(IProgressMonitor monitor) throws CoreException {
        ILaunch launch = null;

        // Get the launch config type
        DebugPlugin debugPlugin = DebugPlugin.getDefault();
        ILaunchManager launchManager = debugPlugin.getLaunchManager();
        ILaunchConfigurationType launchType = launchManager
                .getLaunchConfigurationType(JAVA_APP_LAUNCH_ID);

        // Create and fill in the launch configuration
        ILaunchConfigurationWorkingCopy launchConfig = launchType.newInstance(
                null, getName());
        fillLaunchConfig(launchConfig);

        // Launch
        launch = new Launch(launchConfig, ILaunchManager.RUN_MODE, null);

        Set<String> modes = new HashSet<String>();
        modes.add(ILaunchManager.RUN_MODE);

        ILaunchDelegate[] delegates = launchType.getDelegates(modes);
        ILaunchConfigurationDelegate delegate = null;

        if (delegates.length == 1) {
            delegate = delegates[0].getDelegate();
        } else if (delegates.length == 0) {
            IStatusHandler handler = DebugPlugin.getDefault().getStatusHandler(
                    promptStatus);
            if (handler != null) {
                handler.handleStatus(delegateNotAvailable, new Object[] { this,
                        ILaunchManager.RUN_MODE });
            }
            IStatus status = new Status(IStatus.CANCEL, DebugPlugin
                    .getUniqueIdentifier(), IStatus.ERROR,
                    Messages.AbstractJavaTool_delegateNotAvailable, null);
            throw new CoreException(status);
        }

        delegate.launch(launchConfig, ILaunchManager.RUN_MODE, launch, monitor);

        Utils.dumpCommandLine(launch);

        return launch;
    }

    /**
     * Fill in the settings in the launch configuration.
     * 
     * @param launchConfig
     * @throws CoreException
     */
    private void fillLaunchConfig(ILaunchConfigurationWorkingCopy launchConfig)
            throws CoreException {
        launchConfig.setAttribute(
                IJavaLaunchConfigurationConstants.ATTR_MAIN_TYPE_NAME,
                getClassName());
        launchConfig
                .setAttribute(
                        IJavaLaunchConfigurationConstants.ATTR_DEFAULT_CLASSPATH,
                        false);
        launchConfig.setAttribute(
                IJavaLaunchConfigurationConstants.ATTR_CLASSPATH,
                getClasspathMementos());
        launchConfig.setAttribute(
                IJavaLaunchConfigurationConstants.ATTR_PROGRAM_ARGUMENTS,
                getProgramArgumentsString());
        launchConfig.setAttribute(
                IJavaLaunchConfigurationConstants.ATTR_VM_ARGUMENTS,
                getVMArgumentsString());
    }

    /**
     * Get a string containing the specified arguments correctly layed out.
     * 
     * @param arguments
     * @return
     */
    private String getArgumentsString(String[] arguments) {
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arguments.length; i++) {
            if (i != 0)
                sb.append(' ');
            sb.append(arguments[i]);
        }

        return sb.toString();
    }

    /**
     * Add the standard (VM) libraries to the mementos.
     * 
     * @param mementos
     */
    private void addStandardLibraries(List<String> mementos)
            throws CoreException {
        IVMInstall vm = JavaRuntime.getDefaultVMInstall();
        if (vm != null) {
            LibraryLocation[] libs = JavaRuntime.getLibraryLocations(vm);
            if (libs != null) {
                for (int i = 0; i < libs.length; i++) {
                    IPath location = libs[i].getSystemLibraryPath();
                    IRuntimeClasspathEntry r = JavaRuntime
                            .newArchiveRuntimeClasspathEntry(location);
                    r
                            .setClasspathProperty(IRuntimeClasspathEntry.STANDARD_CLASSES);
                    mementos.add(r.getMemento());
                }
            }
        }
    }

    /**
     * Get a list of mementos for the classpath entries.
     * 
     * @return
     * @throws CoreException
     */
    private List<String> getClasspathMementos() throws CoreException {

        String[] cpEntries = getClasspath();
        List<String> mementos = new ArrayList<String>(cpEntries.length);

        for (int i = 0; i < cpEntries.length; i++) {
            // Convert to a runtime classpath entry
            String cpEntry = cpEntries[i];
            IRuntimeClasspathEntry rcpEntry = JavaRuntime
                    .newArchiveRuntimeClasspathEntry(new Path(cpEntry));

            // Save the memento
            mementos.add(rcpEntry.getMemento());
        }

        // Add the standard libraries to the classpath
        addStandardLibraries(mementos);

        return mementos;
    }

    /**
     * Get the program arguments as a string.
     * 
     * @return
     */
    private String getProgramArgumentsString() {
        return getArgumentsString(getArguments());
    }

    /**
     * Get the VM arguments as a string.
     * 
     * @return
     */
    private String getVMArgumentsString() {
        return getArgumentsString(getVMArguments());
    }
}
