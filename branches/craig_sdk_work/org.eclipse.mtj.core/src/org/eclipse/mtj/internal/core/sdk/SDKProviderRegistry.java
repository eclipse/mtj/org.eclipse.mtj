/**
 * 
 */
package org.eclipse.mtj.internal.core.sdk;

import java.util.ArrayList;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.sdk.ISDKProvider;

/**
 * The registry that provides access to the available {@link ISDKProvider} instances
 * that have been registered by extension point.
 */
public class SDKProviderRegistry {
	private static SDKProviderRegistry instance;
	
	/**
	 * Return the singleton instance of the SDK provider registry.
	 * 
	 * @return
	 */
	public static final SDKProviderRegistry getInstance() {
		if (instance == null) {
			instance = new SDKProviderRegistry();
		}
		
		return instance;
	}
	
	private ISDKProvider[] providers;
	
	/**
	 * Return the current {@link ISDKProvider} instances registered to
	 * the extension point.
	 * 
	 * @return
	 * @throws CoreException 
	 */
	public ISDKProvider[] getSDKProviders() 
		throws CoreException 
	{
		if (providers == null)  {
			providers = readProviders();
		}
		
		return providers;
	}

	/**
	 * Read in the {@link ISDKProvider} instances from the Eclipse registry.
	 * 
	 * @return
	 * @throws CoreException 
	 */
	private ISDKProvider[] readProviders() 
		throws CoreException 
	{
		ArrayList<ISDKProvider> providers = new ArrayList<ISDKProvider>();
		
        String pluginId = MTJCore.getMTJCore().getBundle().getSymbolicName();

        // check if this bundle does not have a specified symbolic name
        if (pluginId != null) {
            IExtensionRegistry registry = Platform.getExtensionRegistry();
            IConfigurationElement[] elements = 
            	registry.getConfigurationElementsFor(pluginId, ISDKProvider.EXT_SDK_PROVIDER);

            for (int i = 0; i < elements.length; i++) {
            	SDKProviderExtension extension = 
            		new SDKProviderExtension(elements[i]);
            	
            	ISDKProvider provider = extension.getSDKProvider();
            	provider.setIdentifier(extension.getId());
            	providers.add(provider);
            }
        }
        
		return (ISDKProvider[]) providers.toArray(new ISDKProvider[providers.size()]);
	}
}
