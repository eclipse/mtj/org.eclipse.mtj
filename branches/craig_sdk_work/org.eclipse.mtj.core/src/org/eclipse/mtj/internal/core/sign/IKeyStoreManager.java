/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Adding keystore management functions.
 */
package org.eclipse.mtj.internal.core.sign;

import java.util.List;

/**
 * IKeyStoreManager interface defines a set of methods
 * for classes providing keystore management.
 * 
 * @author David Marques
 * @since 1.0
 */
public interface IKeyStoreManager {

	/**
	 * Gets the certificate's information to show to the
	 * user. The information format depends on the implementation.
	 * 
	 * @param alias certificate alias.
	 * @return an information String.
	 * @throws KeyStoreManagerException Any keystore management error occurs.
	 */
	public String getCertificateInformation(String alias) throws KeyStoreManagerException;
	
	/**
	 * Gets all the aliases from all private keys in the keystore.
	 * 
	 * @return a list of aliases.
	 * @throws KeyStoreManagerException Any keystore management error occurs.
	 */
	public List<String> getPrivateKeysAliases() throws KeyStoreManagerException;
	
	/**
	 * Changes the keystore password to the specified value.
	 * 
	 * @param password new password.
	 * @throws KeyStoreManagerException Any keystore management error occurs.
	 */
	public void changeKeystorePassword(String password) throws KeyStoreManagerException;
	
	
	/**
	 * Generates a keypair with an auto signed certificate on the keystore.
	 * 
	 * @param keyPairInfo keypair and certificate information.
	 * @throws KeyStoreManagerException Any keystore management error occurs.
	 */
	public void generateKeyPair(KeyPairInfo keyPairInfo) throws KeyStoreManagerException; 
	
	/**
	 * Removes the keystore entry with the specified alias from the keystore.
	 * 
	 * @param alias entry alias.
	 * @throws KeyStoreManagerException Any keystore management error occurs.
	 */
	public void deleteKeyPair(String alias) throws KeyStoreManagerException; 
	
	/**
	 * Generates a CSR for the entry with the specified alias into the specified
	 * destination folder.
	 * 
	 * @param alias target entry alias.
	 * @param password entry's password.
	 * @param destination destination folder to generate CSR file.
	 * @throws KeyStoreManagerException Any keystore management error occurs.
	 */
	public void generateCSR(String alias, String password, String destination) throws KeyStoreManagerException;
	
	/**
	 * Imports a certificate into the keystore.
	 * 
	 * @param alias target entry alias.
	 * @param password target entry password.
	 * @param certPath path to the certificate file.
	 * @throws KeyStoreManagerException Any keystore management error occurs.
	 */
	public void importCertificate(String alias, String password, String certPath) throws KeyStoreManagerException;
	
	/**
	 * Sets the keystore provider.
	 * 
	 * @param provider keystore provider.
	 */
	public void setProvider(String provider);

	
	/**
	 * Sets the keytore type.
	 * 
	 * @param type keystore type. 
	 */
	public void setKeystoreType(String type);
	
}
