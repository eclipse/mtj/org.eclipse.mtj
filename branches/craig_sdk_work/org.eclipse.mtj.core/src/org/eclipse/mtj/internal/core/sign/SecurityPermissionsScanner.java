/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.sign;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.Label;
import org.objectweb.asm.commons.EmptyVisitor;
import org.objectweb.asm.tree.ClassNode;

/**
 * SecurityPermissionsScanner class scans class files for usage of
 * classes that require security permissions.
 * <br>
 * The scanner calculates all required permissions during the scan 
 * process.
 * 
 * @author David Marques
 * @since 1.0
 *
 */
public class SecurityPermissionsScanner extends EmptyVisitor implements IResourceVisitor {

    // Constants -----------------------------------------------------

	private static final String DOT = "."; //$NON-NLS-1$

	private static final String SLASH = "/"; //$NON-NLS-1$

	private static final String CLASS_DESCRIPTOR_REGEXP = "L([a-zA-Z]\\w+)(/\\w+)*;"; //$NON-NLS-1$
	
    // Attributes ----------------------------------------------------

    private List<PermissionsGroup> permissions;
    private IFolder classFolder;
    
    // Static --------------------------------------------------------

    // Constructors --------------------------------------------------

    /**
     * Creates a SecurityScanner to scan the specified
     * class folder.
     * 
     * @param _classFolder target folder.
     */
    public SecurityPermissionsScanner(IFolder _classFolder) {
        this.permissions = new ArrayList<PermissionsGroup>();
        this.classFolder = _classFolder;
    }
    
    // Public --------------------------------------------------------

    /**
     * Gets the required permissions found on the
     * folder's classes.
     * 
     * @return permissions list.
     */
    public synchronized List<PermissionsGroup> getRequiredPermissions() {
        List<PermissionsGroup> permissions = new ArrayList<PermissionsGroup>();
        try {
        	this.permissions.clear();
        	this.classFolder.accept(this);
        } catch (CoreException e) {
        	MTJLogger.log(IStatus.ERROR, e);
        } finally {
        	permissions.addAll(this.permissions);
        }
        return permissions;
    }
    
    // X implementation ----------------------------------------------

    /* (non-Javadoc)
     * @see org.objectweb.asm.commons.EmptyVisitor#visitMethodInsn(int, java.lang.String, java.lang.String, java.lang.String)
     */
    public void visitMethodInsn(int opcode, String owner, String name,
            String desc) {
       String className = owner.replace(SLASH, DOT);
       this.inspectClass(className);
    }
    
    
    /* (non-Javadoc)
     * @see org.objectweb.asm.commons.EmptyVisitor#visitField(int, java.lang.String, java.lang.String, java.lang.String, java.lang.Object)
     */
    public FieldVisitor visitField(int access, String name, String desc,
    		String signature, Object value) {
    	if (desc.matches(CLASS_DESCRIPTOR_REGEXP)) {
    		StringBuffer buffer = new StringBuffer(desc);
    		buffer.deleteCharAt(0x00); // Remove L
    		buffer.deleteCharAt(buffer.length() - 1); // Remove ;
    		this.inspectClass(buffer.toString().replace(SLASH, DOT));
		}
    	return super.visitField(access, name, desc, signature, value);
    }

    /* (non-Javadoc)
     * @see org.objectweb.asm.commons.EmptyVisitor#visitLocalVariable(java.lang.String, java.lang.String, java.lang.String, org.objectweb.asm.Label, org.objectweb.asm.Label, int)
     */
    public void visitLocalVariable(String name, String desc, String signature,
    		Label start, Label end, int index) {
    	if (desc.matches(CLASS_DESCRIPTOR_REGEXP)) {
    		StringBuffer buffer = new StringBuffer(desc);
    		buffer.deleteCharAt(0x00); // Remove L
    		buffer.deleteCharAt(buffer.length() - 1); // Remove ;
    		this.inspectClass(buffer.toString().replace(SLASH, DOT));
		}
    	super.visitLocalVariable(name, desc, signature, start, end, index);
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IResourceVisitor#visit(org.eclipse.core.resources.IResource)
     */
    public boolean visit(IResource resource) throws CoreException {
        
        if (resource instanceof IFolder) {
            return true;
        }
        
        if (!(resource instanceof IFile && 
                Pattern.matches(".+(.class)", resource.getName()))) { //$NON-NLS-1$
            return false;
        }
        
        try {            
            ClassReader classReader = new ClassReader(((IFile)resource).getContents());
            ClassNode   classNode   = new ClassNode();
            classReader.accept(classNode, 0x00);
            classNode.accept(this);
        } catch (IOException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
        return false;
    }
    
    // Y overrides ---------------------------------------------------

    // Package protected ---------------------------------------------

    // Protected -----------------------------------------------------

    // Private -------------------------------------------------------
    
    /**
     * Inspects the specified class name in order
     * to realize if it is listed on any security
     * permission registered.
     * 
     * @param className name of class.
     */
    private void inspectClass(String className) {
    	 List<PermissionsGroup> permissions = PermissionsGroupsRegistry
				.getInstance().getPermissions();
		for (PermissionsGroup permission : permissions) {
			if (!(permission.getClassName().equals(className))) {
				continue;
			}
			
			if (!this.permissions.contains(permission)) {				
				this.permissions.add(permission);
			}
		}
    }
    
}
