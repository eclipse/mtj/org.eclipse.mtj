/**
 * Copyright (c) 2004,2008 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 */
package org.eclipse.mtj.internal.core.build.sign;

/**
 * This class carries a pair of passwords back from the status handler
 * (SigningPasswordsHandler) in the ui area.
 * <p>
 * <b>Note:</b> This class/interface is part of an interim API that is still
 * under development and expected to change before reaching stability. It is
 * being made available at this early stage to solicit feedback from pioneering
 * adopters on the understanding that any code that uses this API will almost
 * certainly be broken as the API evolves.
 * </p>
 * 
 * @author Kevin Hunter
 */
public class SignaturePasswords {

    private String keystorePassword;
    private String keyPassword;

    /**
     * Constructor
     */
    public SignaturePasswords() {
    }

    /**
     * @param keystorePass
     * @param keyPass
     */
    public SignaturePasswords(String keystorePass, String keyPass) {
        keystorePassword = keystorePass;
        keyPassword = keyPass;
    }

    /**
     * @return
     */
    public String getKeystorePassword() {
        return (keystorePassword);
    }

    /**
     * @param value
     */
    public void setKeystorePassword(String value) {
        keystorePassword = value;
    }

    /**
     * @return
     */
    public String getKeyPassword() {
        return (keyPassword);
    }

    /**
     * @param value
     */
    public void setKeyPassword(String value) {
        keyPassword = value;
    }
}
