/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Adapting code to use Jetty version 5.1.11
 *                                
 */
package org.eclipse.mtj.internal.core.sdk.device;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.preverifier.IPreverifier;
import org.eclipse.mtj.core.persistence.IBundleReferencePersistable;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.sdk.device.IDeviceClasspath;
import org.eclipse.mtj.core.sdk.device.ILibrary;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary;
import org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.build.preverifier.builder.PreverificationBuilder;
import org.eclipse.mtj.internal.core.launching.midp.IMIDPLaunchConstants;
import org.eclipse.mtj.internal.core.launching.midp.LaunchingUtils;
import org.eclipse.mtj.internal.core.launching.midp.ota.OTAServer;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.TemporaryFileManager;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * Abstract superclass of the various device implementations.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public abstract class AbstractMIDPDevice implements IMIDPDevice,
        IBundleReferencePersistable {

    // Variables that define this device
    protected String bundle;
    protected boolean debugServer;
    protected String description;
    protected IDeviceClasspath deviceClasspath;
    protected Properties deviceProperties;
    protected ISymbolSet deviceSymbolSet;
    protected File executable;

    protected String groupName;
    protected String launchCommandTemplate;
    protected String name;
    protected IPreverifier preverifier;
    protected String[] protectionDomains;

    /**
     * Test equality on a AbstractMIDPDevice and return a boolean indicating
     * equality.
     * 
     * @param device
     * @return
     */
    public boolean equals(AbstractMIDPDevice device) {
        return deviceClasspath.equals(device.deviceClasspath)
                && executable.equals(device.executable)
                && name.equals(device.name)
                && groupName.equals(device.groupName);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IBundleReferencePersistable#getBundle()
     */
    public String getBundle() {
        return bundle;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDevice#getClasspath()
     */
    public IDeviceClasspath getClasspath() {
        return deviceClasspath;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice#getConfigurationLibrary()
     */
    public IMIDPAPI getCLDCAPI() {
        return getLibraryWithType(MIDPAPIType.CONFIGURATION);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDevice#getDescription()
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return Returns the executable.
     */
    public File getExecutable() {
        return executable;
    }

    /**
     * @return Returns the launchCommandTemplate.
     */
    public String getLaunchCommandTemplate() {
        return launchCommandTemplate;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice#getProfileLibrary()
     */
    public IMIDPAPI getMIDPAPI() {
        return getLibraryWithType(MIDPAPIType.PROFILE);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDevice#getName()
     */
    public String getName() {
        return name;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice#getPreverifier()
     */
    public IPreverifier getPreverifier() {
        return preverifier;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPDevice#getProtectionDomains()
     */
    public String[] getProtectionDomains() {
        return protectionDomains;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDevice#getSDKName()
     */
    public String getSDKName() {
        return groupName;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDevice#getSymbolSet()
     */
    public ISymbolSet getSymbolSet() {
        return deviceSymbolSet;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.IDevice#isDebugServer()
     */
    public boolean isDebugServer() {
        return debugServer;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        deviceClasspath = (DeviceClasspath) persistenceProvider
                .loadPersistable("classpath"); //$NON-NLS-1$
        debugServer = persistenceProvider.loadBoolean("debugServer"); //$NON-NLS-1$
        description = persistenceProvider.loadString("description"); //$NON-NLS-1$
        deviceProperties = persistenceProvider
                .loadProperties("deviceProperties"); //$NON-NLS-1$
        name = persistenceProvider.loadString("name"); //$NON-NLS-1$

        protectionDomains = new String[persistenceProvider
                .loadInteger("protectionDomainsCount")]; //$NON-NLS-1$
        for (int i = 0; i < protectionDomains.length; i++) {
            protectionDomains[i] = persistenceProvider.loadString(
                    "protectionDomain" + i).trim(); //$NON-NLS-1$
        }

        groupName = persistenceProvider.loadString("groupName"); //$NON-NLS-1$
        preverifier = (IPreverifier) persistenceProvider
                .loadPersistable("preverifier"); //$NON-NLS-1$
        launchCommandTemplate = persistenceProvider
                .loadString("rawLaunchCommand"); //$NON-NLS-1$

        String executableString = persistenceProvider.loadString("executable"); //$NON-NLS-1$
        if ((executableString != null)
                && (executableString.trim().length() > 0)) {
            executable = new File(executableString);
        }
        deviceSymbolSet = MTJCore.getSymbolSetFactory()
                .createSymbolSetFromDevice(this);
        ISymbolSet pss = MTJCore.getSymbolSetFactory()
                .createSymbolSetFromProperties(deviceProperties);

        deviceSymbolSet.add(pss.getSymbols());
        deviceSymbolSet.setName(name);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IBundleReferencePersistable#setBundle(java.lang.String)
     */
    public void setBundle(String bundle) {
        this.bundle = bundle;
    }

    /**
     * @param deviceClasspath The deviceClasspath to set.
     */
    public void setClasspath(IDeviceClasspath deviceClasspath) {
        this.deviceClasspath = deviceClasspath;
    }

    /**
     * @param debugServer The debugServer to set.
     */
    public void setDebugServer(boolean debugServer) {
        this.debugServer = debugServer;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param deviceProperties The deviceProperties to set.
     */
    public void setDeviceProperties(Properties deviceProperties) {
        this.deviceProperties = deviceProperties;
    }

    /**
     * @param executable The executable to set.
     */
    public void setExecutable(File executable) {
        this.executable = executable;
    }

    /**
     * @param groupName The groupName to set.
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @param launchCommandTemplate The launchCommandTemplate to set.
     */
    public void setLaunchCommandTemplate(String launchCommandTemplate) {
        this.launchCommandTemplate = launchCommandTemplate;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param preverifier The preverifier to set.
     */
    public void setPreverifier(IPreverifier preverifier) {
        this.preverifier = preverifier;
    }

    /**
     * @param protectionDomains The protectionDomains to set.
     */
    public void setProtectionDomains(String[] protectionDomains) {
        this.protectionDomains = protectionDomains;

        // Trim any spaces that could cause problems
        for (int i = 0; i < protectionDomains.length; i++) {
            protectionDomains[i] = protectionDomains[i].trim();
        }
    }

    /**
     * @param symbolSet the deviceSymbolSet to set
     */
    public synchronized void setSymbolSet(ISymbolSet symbolSet) {
        this.deviceSymbolSet = symbolSet;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        persistenceProvider.storePersistable("classpath", deviceClasspath); //$NON-NLS-1$
        persistenceProvider.storeBoolean("debugServer", debugServer); //$NON-NLS-1$
        persistenceProvider.storeString("description", description); //$NON-NLS-1$
        persistenceProvider.storeProperties("deviceProperties", //$NON-NLS-1$
                deviceProperties);
        persistenceProvider.storeString("name", name); //$NON-NLS-1$
        persistenceProvider.storeInteger("protectionDomainsCount", //$NON-NLS-1$
                protectionDomains.length);
        for (int i = 0; i < protectionDomains.length; i++) {
            persistenceProvider.storeString("protectionDomain" + i, //$NON-NLS-1$
                    protectionDomains[i]);
        }
        persistenceProvider.storeString("groupName", groupName); //$NON-NLS-1$
        persistenceProvider.storePersistable(
                "preverifier", (IPersistable) preverifier); //$NON-NLS-1$
        persistenceProvider.storeString("rawLaunchCommand", //$NON-NLS-1$
                launchCommandTemplate);

        if (executable != null) {
            persistenceProvider
                    .storeString("executable", executable.toString()); //$NON-NLS-1$
        }
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer(getSDKName());
        sb.append("/").append(getName()); //$NON-NLS-1$

        return sb.toString();
    }

    /**
     * Look for the library with the specified type and return it. If it cannot
     * be found, return <code>null</code>
     * 
     * @param type
     * @return
     */
    private IMIDPAPI getLibraryWithType(MIDPAPIType type) {
        IMIDPAPI result = null;

        IDeviceClasspath deviceClasspath = getClasspath();
        List<ILibrary> libraries = (List<ILibrary>) deviceClasspath
                .getEntries();

        for (ILibrary library : libraries) {
            if ((library instanceof IMIDPLibrary)
                    && (((IMIDPLibrary) library).getAPI(type) != null)) {
                result = ((IMIDPLibrary) library).getAPI(type);
                break;
            }
        }

        return result;
    }

    /**
     * Add a value from the launch configuration to the execution properties
     * map.
     * 
     * @param executionProperties
     * @param propertyName
     * @param launchConfiguration
     * @param launchConstant
     * @throws CoreException
     */
    protected void addLaunchConfigurationValue(
            Map<String, String> executionProperties, String propertyName,
            ILaunchConfiguration launchConfiguration, String launchConstant)
            throws CoreException {
        String configValue = launchConfiguration.getAttribute(launchConstant,
                (String) null);
        if (configValue != null) {
            executionProperties.put(propertyName, configValue);
        }
    }

    /**
     * Copy the deployed jar and jad for use during launching. Return the folder
     * into which the copy has been made.
     * 
     * @param suite
     * @param monitor
     * @param launchFromJad - If launch from a JAD file.
     * @return
     * @throws IOException
     */
    protected File copyForLaunch(IMidletSuiteProject suite,
            IProgressMonitor monitor, boolean launchFromJad)
            throws CoreException {
        try {
            // If launch from JAD, will copy the runtime JAD and JAR to JAD
            // Launching Base Folder. This folder is located in .mtj.tmp folder.
            if (launchFromJad) {
                IFolder emulationFolder = LaunchingUtils
                        .getEmulationFolder(suite);
                // jadLaunchDir is the directory containing JAD and JAR for
                // launching
                File jadLaunchDir = LaunchingUtils.makeJadLaunchBaseDir(suite);
                // Get JAD and JAR for copy.
                File runtimeJad = emulationFolder.getFile(
                        suite.getJadFileName()).getLocation().toFile();
                File runtimeJar = emulationFolder.getFile(
                        suite.getJarFilename()).getLocation().toFile();
                File emulateJad = new File(jadLaunchDir, suite.getJadFileName());
                File emulateJar = new File(jadLaunchDir, suite.getJarFilename());
                // Do file copy.
                Utils.copyFile(runtimeJad, emulateJad, null);
                Utils.copyFile(runtimeJar, emulateJar, null);
                // Refresh local.
                LaunchingUtils.getProjectTempFolder(suite).refreshLocal(
                        IResource.DEPTH_INFINITE, monitor);
                return jadLaunchDir;
            }
            // If launch from Midlet, will copy the runtime JAD and JAR to temp
            // folder. The folder is located in system temp directory.
            else {
                File tempFolder = null;
                IFolder emulationFolder = LaunchingUtils
                        .getEmulationFolder(suite);
                File emulationDirectory = emulationFolder.getLocation()
                        .toFile();
                String folderName = suite.getProject().getName().replace(' ',
                        '_');
                tempFolder = TemporaryFileManager.instance.createTempDirectory(
                        folderName, ".launch"); //$NON-NLS-1$
                Utils.copyDirectory(emulationDirectory, tempFolder, null);
                return tempFolder;
            }
        } catch (IOException e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, -999, e);
            return null;
        }

    }

    /**
     * Return the JAD file to use for launching from the specified temporary
     * directory.
     * 
     * @param midletSuite
     * @param temporaryDirectory
     * @param monitor
     * @return
     */
    protected File getJadForLaunch(IMidletSuiteProject midletSuite,
            File temporaryDirectory, IProgressMonitor monitor) {
        return new File(temporaryDirectory, midletSuite.getJadFileName());
    }

    /**
     * Return the Over the Air URL for accessing the JAD file via the built-in
     * OTA HTTP server.
     * 
     * @param launchConfig
     * @return
     * @throws CoreException
     */
    protected String getOTAURL(ILaunchConfiguration launchConfig,
            IMidletSuiteProject midletSuite) throws CoreException {
        // If we are doing OTA launching, make sure that the
        // OTA server has been started
        try {
            OTAServer.getInstance().start();
        } catch (Exception e) {
            MTJLogger.log(IStatus.WARNING, "Failed to launch OTA Server", e);
        }

        String projectName = midletSuite.getProject().getName();
        String jadName = midletSuite.getJadFileName();

        StringBuffer sb = new StringBuffer();
        sb.append("http://localhost:").append(OTAServer.getPort()).append( //$NON-NLS-1$
                "/ota/").append(urlEncoded(projectName)).append('/').append( //$NON-NLS-1$
                urlEncoded(jadName));

        return sb.toString();
    }

    /**
     * Return the deviceClasspath string to be used when launching the specified
     * project.
     * 
     * @param midletSuite
     * @return
     * @throws CoreException
     */
    protected String getProjectClasspathString(IMTJProject midletSuite,
            File temporaryDirectory, IProgressMonitor monitor)
            throws CoreException {
        de.schlichtherle.io.File deployedJar = PreverificationBuilder
                .getRuntimeJar(midletSuite.getProject(), monitor);
        return new File(temporaryDirectory, deployedJar.getName())
                .getAbsolutePath();
    }

    /**
     * Return the url specified by the user for direct launching or
     * <code>null</code> if none was specified.
     * 
     * @param launchConfiguration
     * @return
     * @throws CoreException
     */
    protected String getSpecifiedJadURL(ILaunchConfiguration launchConfiguration)
            throws CoreException {
        return launchConfiguration.getAttribute(
                IMIDPLaunchConstants.SPECIFIED_JAD_URL, (String) null);
    }

    /**
     * Return a boolean indicating whether or not the emulation should just
     * directly launch a specified JAD URL without the other things that
     * normally happen during launching.
     * 
     * @param configuration
     * @return
     * @throws CoreException
     */
    protected boolean shouldDirectLaunchJAD(ILaunchConfiguration configuration)
            throws CoreException {
        return configuration.getAttribute(IMIDPLaunchConstants.DO_JAD_LAUNCH,
                false);
    }

    /**
     * Return a boolean indicating whether or not the emulation should be
     * launched as OTA.
     * 
     * @param configuration
     * @return
     * @throws CoreException
     */
    protected boolean shouldDoOTA(ILaunchConfiguration configuration)
            throws CoreException {
        return configuration.getAttribute(IMIDPLaunchConstants.DO_OTA, true);
    }

    /**
     * Encode the specified information for a URL.
     * 
     * @param value
     * @return
     */
    protected String urlEncoded(String value) {
        String encoded = null;

        try {
            encoded = URLEncoder.encode(value, "UTF-8"); //$NON-NLS-1$
        } catch (UnsupportedEncodingException e) {
            // Should never happen
        }

        return encoded;
    }
}
