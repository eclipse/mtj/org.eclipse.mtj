/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code
 *     Feng Wang (Sybase) - Add Symbols from active configuration to
 *                          preprocessor, for multi-configs support.
 *     Gang Ma      (Sybase)    - Add debug level setting to preprocessor
 *     Gang Ma      (Sybase)    - Fixed the encoding problem of preprocessed
 *                                file, see bug: 259341 
 *     David Marques (Motorola) - Extending MTJIncrementalProjectBuilder
 */
package org.eclipse.mtj.internal.core.build.preprocessor;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.ResourceAttributes;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.build.MTJBuildState;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.PreferenceAccessor;
import org.eclipse.mtj.internal.core.build.BuildConsoleProxy;
import org.eclipse.mtj.internal.core.build.BuildLoggingConfiguration;
import org.eclipse.mtj.internal.core.build.IBuildConsoleProxy;
import org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder;
import org.eclipse.mtj.internal.core.hook.sourceMapper.SourceMapperAccess;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

import antenna.preprocessor.v3.ILineFilter;
import antenna.preprocessor.v3.ILogger;
import antenna.preprocessor.v3.IPreprocessorListener;
import antenna.preprocessor.v3.PPException;
import antenna.preprocessor.v3.Preprocessor;

/**
 * An incremental builder implementation that provides preprocess support as
 * defined for the Antenna Ant tools.<br/>
 * <p>
 * The preprocessor supports the following directives inside a Java source file.
 * All directives must immediately follow a "//" comment that starts at the
 * beginning of a line (whitespace is allowed left of them, but no Java code).
 * That way, they don't interfere with normal Java compilation. Directives must
 * not span multiple lines of code.
 * <p />
 * <table class="color">
 * <tr>
 * <th width="30%"> Directive </th>
 * <th>Description</th>
 * </tr>
 * <tr>
 * <td>#define &lt;identifier&gt;</td>
 * <td>Defines an identifier, thus making its value "true" when it is referenced
 * in further expressions.</td>
 * </tr>
 * <tr>
 * <td>#undefine &lt;identifier&gt;</td>
 * <td>Undefines an identifier, thus making its value "false" when it is
 * referenced in further expressions.</td>
 * </tr>
 * <tr>
 * <td>#ifdef &lt;identifier&gt;</td>
 * <td rowspan="8">The following lines are compiled only if the given identifier
 * is defined (or undefined, in the case of an "#ifndef" directive). "#else"
 * does exactly what your think it does. Each directive must be ultimately
 * closed by an "#endif" directive. The "#elifdef" and "#elifndef" directives
 * help to specify longer conditional cascades without having to nest each
 * level.
 * <p />
 * The "#if" and "#elif" directives even allow to use complex expressions. These
 * expressions are very much like Java boolean expressions. They may consist of
 * identifiers, parentheses and the usual "&amp;&amp;", "||", "^", and "!"
 * operators.
 * <p />
 * Please note that "#ifdef" and "#ifndef" don't support complex expressions.
 * They only expect a single argument - the symbol to be checked.
 * </td>
 * </tr>
 * <tr>
 * <td>#ifndef &lt;identifier&gt;</td>
 * </tr>
 * <tr>
 * <td>#else</td>
 * </tr>
 * <tr>
 * <td>#endif</td>
 * </tr>
 * <tr>
 * <td>#elifdef &lt;identifier&gt;</td>
 * </tr>
 * <tr>
 * <td>#elifndef &lt;identifier&gt;</td>
 * </tr>
 * <tr>
 * <td>#if &lt;expression&gt;</td>
 * </tr>
 * <tr>
 * <td>#elif &lt;expression&gt;</td>
 * </tr>
 * <tr>
 * <td>#include &lt;filename&gt;</td>
 * <td rowspan="2">Includes the given source file at the current position. Must
 * be terminated by "#endinclude" for technical reasons. The filename may also
 * be given as an Ant-style property (${name}). The property needs to be defined
 * in your build.xml file then. Note that relative file names are interpreted as
 * relative to the build.xml file.</td>
 * </tr>
 * <tr>
 * <td>#endinclude</td>
 * </tr>
 * </table>
 * 
 * @author Craig Setera
 */
public class PreprocessorBuilder extends MTJIncrementalProjectBuilder {

    private BuildConsoleProxy consoleProxy = BuildConsoleProxy.getInstance();

    /**
     * Resource delta visitor for visiting changed java files for preprocessing.
     */
    private class ResourceDeltaVisitor extends ResourceVisitor implements
            IResourceDeltaVisitor {

        /**
         * @param symbols
         * @param monitor
         */
        protected ResourceDeltaVisitor(IProgressMonitor monitor) {
            super(monitor);
        }

        /*
         * (non-Javadoc)
         * @see
         * org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse
         * .core.resources.IResourceDelta)
         */
        public boolean visit(IResourceDelta delta) throws CoreException {
            boolean value = false;

            switch (delta.getKind()) {
                case IResourceDelta.ADDED:
                case IResourceDelta.CHANGED:
                    value = visit(delta.getResource());
                    break;

                case IResourceDelta.REMOVED: {
                    IFile outputFile = getOutputFile(delta.getResource());
                    if (outputFile.exists()) {
                        outputFile.delete(true, monitor);
                    }
                }
                    break;

                default:
                    // Do nothing
            }

            return value;
        }
    }

    /**
     * Resource visitor for visiting java files for preprocessing.
     */
    protected class ResourceVisitor implements IResourceVisitor {
        protected IProgressMonitor monitor;

        /**
         * @param symbols
         * @param monitor
         */
        protected ResourceVisitor(IProgressMonitor monitor) {
            this.monitor = monitor;
        }

        /*
         * (non-Javadoc)
         * @see
         * org.eclipse.core.resources.IResourceVisitor#visit(org.eclipse.core
         * .resources.IResource)
         */
        public boolean visit(IResource resource) throws CoreException {
            if (shouldBeProcessed(resource, monitor)) {
                preprocess(resource, monitor);
            }

            return true;
        }
    }

    /** A preprocessor problem marker */
    public static final String PROBLEM_MARKER = "org.eclipse.mtj.core.preprocessor.problemmarker";

    // The sub-directory into which the processed output will go
    public static final String PROCESSED_DIRECTORY = ".processed";

    private static final String PROCESSED_PATH = "/" + PROCESSED_DIRECTORY;

    /**
     * Return an appropriate output file for the specified resource. This file
     * is not guaranteed to exist.
     * 
     * @param resource
     * @return
     */
    public static IFile getOutputFile(IResource resource) {
        IPath projectRelativePath = new Path(PROCESSED_PATH).append(resource
                .getProjectRelativePath());
        return resource.getProject().getFile(projectRelativePath);
    }

    /**
     * Return the project that is to hold the preprocess output results for the
     * specified project.
     * 
     * @param project
     * @return
     */
    public static IProject getOutputProject(IProject project) {
        String ppProjectName = "." + project.getName() + "_PP";
        return MTJCore.getWorkspace().getRoot().getProject(
                ppProjectName);
    }

    /**
     * Return a boolean indicating whether or not the specified resource has
     * already been preprocessed and should be ignored for further processing.
     * 
     * @param resource
     * @return
     */
    public static boolean isPreprocessed(IResource resource) {
        IPath projectPath = resource.getProjectRelativePath();
        return projectPath.segment(0).equals(PROCESSED_DIRECTORY);
    }

    private BuildLoggingConfiguration buildLoggingConfig;

    // Tracker for a warning check
    private boolean checkedForHook;

    /**
     * Construct a new builder instance.
     */
    public PreprocessorBuilder() {
        super();
        buildLoggingConfig = BuildLoggingConfiguration.getInstance();

        // During the first actual build, we will check to see
        // if the hook has been installed. If not, the build
        // will continue, but the user will be warned via the error
        // log that it won't actually work.
        checkedForHook = false;
    }

    /**
     * add the debug level defines to preprocessor
     * 
     * @param preprocessor
     * @throws PPException
     */
    private void addDebuglevelDefines(Preprocessor preprocessor)
            throws PPException {
        String debuglevel = PreferenceAccessor.instance
                .getPreprecessorDebuglevel(getProject());
        if (PreprocessorHelper.isLegalDebuglevel(debuglevel)) {
            StringBuffer sb = new StringBuffer();
            sb.append(PreprocessorHelper.J2ME_PREPROCESS_DEBUG_LEVEL_KEY)
                    .append("=").append(debuglevel);
            preprocessor.addDefines(sb.toString());
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#doBuild(int, java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
     */
    @SuppressWarnings("unchecked")
    protected IProject[] doBuild(int kind, Map args, IProgressMonitor monitor)
            throws CoreException {
        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy.traceln("> PreprocessorBuilder.build");
        }

        if (!checkedForHook) {
            if (!SourceMapperAccess.isHookCodeInstalled()) {
                MTJLogger
                        .log(
                                IStatus.WARNING,
                                "Preprocessor invoked, but hook is not installed.  "
                                        + "Consult the installation instructions for MTJ.");
            }

            checkedForHook = true;
        }

        doPreprocessing(kind, monitor);

        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy.traceln("< PreprocessorBuilder.build");
        }

        return null;
    }

    /*
     * (non-Javadoc)
     * @see
     * org.eclipse.core.resources.IncrementalProjectBuilder#clean(org.eclipse
     * .core.runtime.IProgressMonitor)
     */
    @Override
    protected void clean(IProgressMonitor monitor) throws CoreException {
        IProject srcProject = getProject();
        IFolder folder = srcProject.getFolder(PROCESSED_PATH);
        Utils.clearContainer(folder, monitor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#getBuilderId()
     */
    protected String getBuilderId() {
    	return IMTJCoreConstants.J2ME_PREPROCESSOR_ID;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#getEnterState()
     */
    protected MTJBuildState getEnterState() {
    	return MTJBuildState.PRE_PREPROCESS;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.core.build.MTJIncrementalProjectBuilder#getExitState()
     */
    protected MTJBuildState getExitState() {
    	return MTJBuildState.POST_PREPROCESS;
    }
    
    /**
     * Clear previous preprocessor markers from the specified resource.
     * 
     * @param resource
     * @throws CoreException
     */
    private void clearPreprocessorMarkers(IResource resource)
            throws CoreException {
        IMarker[] markers = resource.findMarkers(PROBLEM_MARKER, true,
                IResource.DEPTH_ONE);
        for (IMarker element : markers) {
            element.delete();
        }
    }

    /**
     * Create the parent folders of this file.
     * 
     * @param outputFile
     * @param monitor
     * @throws CoreException
     */
    private void createParentFolders(IResource resource,
            IProgressMonitor monitor) throws CoreException {
        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy
                    .traceln("> PreprocessorBuilder.createParentFolders; resource = "
                            + resource);
        }

        IContainer parent = resource.getParent();
        if (parent.getType() == IResource.FOLDER) {
            createParentFolders(parent, monitor);

            IFolder folder = (IFolder) parent;
            if (!folder.exists()) {
                folder.create(true, true, monitor);
                folder.setDerived(true);
            }
        }

        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy.traceln("< PreprocessorBuilder.createParentFolders");
        }
    }

    /**
     * Create a new marker in the specified resource.
     * 
     * @param resource
     * @param document
     * @param message
     * @param lineNumber
     * @param offset
     * @param length
     * @param error
     * @throws CoreException
     * @throws BadLocationException
     */
    protected void createResourceMarker(IResource resource, IDocument document,
            String message, int lineNumber, int offset, int length,
            boolean error) {
        int severity = error ? IMarker.SEVERITY_ERROR
                : IMarker.SEVERITY_WARNING;

        try {
            IMarker marker = resource.createMarker(PROBLEM_MARKER);
            marker.setAttribute(IMarker.MESSAGE, message);
            marker.setAttribute(IMarker.SEVERITY, severity);

            int start = document.getLineOffset(lineNumber) + offset;
            int end = start + length;
            marker.setAttribute(IMarker.CHAR_START, start);
            marker.setAttribute(IMarker.CHAR_END, end);
        } catch (Exception e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }

    /**
     * Do the preprocessing build.
     * 
     * @param kind
     * @param args
     * @param monitor
     * @throws CoreException
     */
    private void doPreprocessing(int kind, IProgressMonitor monitor)
            throws CoreException {
        // Collect the java files that were updated
        IProject project = getProject();

        // if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
        // consoleProxy
        // .traceln("> PreprocessorBuilder.doPreprocessing; project = "
        // + project
        // + "; symbols = "
        // + ((symbols == null) ? "none" : symbols.getName()));
        // }

        // Wrap up the symbol definition set in another wrapper set. Changes
        // made to the set while processing the source will not propagate
        // to the base definition set
        // symbols = new SymbolDefinitionSet(symbols);

        // Traverse the changes in the resource delta, processing the
        // resources along the way.
        IResourceDelta delta = getDelta(project);
        if (delta == null) {
            ResourceVisitor visitor = new ResourceVisitor(monitor);
            project.accept(visitor);
        } else {
            ResourceDeltaVisitor visitor = new ResourceDeltaVisitor(monitor);
            delta.accept(visitor);
        }

        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy.traceln("< PreprocessorBuilder.doPreprocessing");
        }
    }

    /**
     * Get Symbols string from active configuration.
     * 
     * @return
     */

    private String getActiveSymbolSetString() {
        IMTJProject midletSuite = getMTJProject();
        ISymbolSet symbolSet = midletSuite.getRuntimeList()
                .getActiveMTJRuntime().getSymbolSetForPreprocessing();
        return symbolSet.getSymbolSetString();
    }

    /**
     * Get the content encoding for the specified file. If none is set, attempt
     * to pick up the appropriate encoding for the underlying platform.
     * 
     * @param file
     * @return
     * @throws CoreException
     */
    private String getContentEncoding(IFile file) throws CoreException {

        String encoding = file.getCharset();
        if (encoding == null) {
            encoding = System.getProperty("file.encoding", "UTF-8");
        }

        return encoding;
    }

    /**
     * Return a document instance based on the contents of the specified file.
     * 
     * @param file
     * @return
     * @throws CoreException
     * @throws IOException
     */
    private IDocument getDocumentForFile(IFile file) throws CoreException {
        String charset = file.getCharset(true);
        InputStream contentStream = file.getContents();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(contentStream,
                    charset));
        } catch (UnsupportedEncodingException e) {
            // Should not happen
        }

        String line = null;
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);

        try {
            while ((line = reader.readLine()) != null) {
                writer.println(line);
            }
        } catch (IOException e) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, -999, e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) { /* Munch */
            }
        }

        return new Document(stringWriter.toString());
    }

    /**
     * Return a boolean indicating whether or not the specified resource is on
     * the build path.
     * 
     * @param resource
     * @param monitor
     * @return
     */
    private boolean isOnBuildPath(IResource resource, IProgressMonitor monitor) {
        boolean onBuildPath = false;

        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy
                    .traceln("> PreprocessorBuilder.isOnBuildPath; resource = "
                            + resource);
        }

        IJavaElement javaElement = JavaCore.create((IFile) resource);
        if (javaElement != null) {
            IPackageFragmentRoot root = (IPackageFragmentRoot) javaElement
                    .getAncestor(IJavaElement.PACKAGE_FRAGMENT_ROOT);
            onBuildPath = root.exists();
        }

        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy
                    .traceln("< PreprocessorBuilder.isOnBuildPath; returning = "
                            + onBuildPath);
        }

        return onBuildPath;
    }

    /**
     * Preprocess the specified java resource.
     * 
     * @param resource
     * @param symbols
     * @param monitor
     * @throws CoreException
     */
    protected void preprocess(final IResource resource, IProgressMonitor monitor)
            throws CoreException {
        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy
                    .traceln("> PreprocessorBuilder.preprocess; resource = "
                            + resource);
            // consoleProxy
            // .traceln("- PreprocessorBuilder.preprocess symbol set = "
            // + symbols.getName());
        }

        // The document being processed... Used for offset conversions
        final IDocument document = getDocumentForFile((IFile) resource);

        // Clear the previous preprocessor errors
        clearPreprocessorMarkers(resource);

        ILogger logger = new ILogger() {
            public void log(String message) {
                if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
                    consoleProxy.traceln(message);
                }
            }
        };

        ILineFilter filter = new ILineFilter() {
            public String filter(String line) {
                return line;
            }
        };

        IPreprocessorListener listener = new IPreprocessorListener() {
            public void error(Exception e, int lineNumber, int offset,
                    int length) {
                if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
                    consoleProxy.getConsoleWriter(
                            IBuildConsoleProxy.Stream.ERROR).println(
                            "Preprocessor error: " + e);
                }

                createResourceMarker(resource, document, e.getMessage(),
                        lineNumber, offset, length, true);
            }

            public void warning(String message, int lineNumber, int offset,
                    int length) {
                if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
                    consoleProxy.getConsoleWriter(
                            IBuildConsoleProxy.Stream.OUTPUT).println(
                            "Preprocessor warning: " + message);
                }

                createResourceMarker(resource, document, message, lineNumber,
                        offset, length, false);
            }
        };

        IFile file = (IFile) resource;
        File localFile = file.getLocation().toFile();
        String charset = getContentEncoding(file);

        InputStream is = file.getContents();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        Preprocessor preprocessor = new Preprocessor(logger, filter);
        preprocessor.setFile(localFile);
        preprocessor.setListener(listener);

        try {
            List<ISymbolSet> sets = getMTJProject()
                    .getRuntimeList().getActiveMTJRuntime()
                    .getWorkspaceScopeSymbolSets();
            for (ISymbolSet symbols : sets) {
                preprocessor.addDefines(symbols.toString());
            }
            preprocessor.addDefines(getActiveSymbolSetString());
            addDebuglevelDefines(preprocessor);
            preprocessor.preprocess(is, bos, charset);
            writeProcessedResults(resource, bos.toByteArray(), charset, monitor);
        } catch (Exception e1) {
            MTJStatusHandler.throwCoreException(IStatus.ERROR, -999, e1);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }

        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy.traceln("< PreprocessorBuilder.preprocess");
        }
    }

    /**
     * Set the specified file to be read-only or not based on the specified
     * flag.
     * 
     * @param outputFile
     * @param readOnly
     * @throws CoreException
     */
    private void setReadOnly(IFile outputFile, boolean readOnly)
            throws CoreException {
        ResourceAttributes attributes = new ResourceAttributes();
        attributes.setReadOnly(readOnly);
        outputFile.setResourceAttributes(attributes);
    }

    /**
     * Return a boolean indicating whether the specified resource should be
     * processed.
     * 
     * @param resource
     * @param monitor
     * @return
     * @throws CoreException
     */
    private boolean shouldBeProcessed(IResource resource,
            IProgressMonitor monitor) throws CoreException {
        return (resource.getType() == IResource.FILE)
                && (resource.getName().endsWith(".java"))
                && !isPreprocessed(resource)
                && isOnBuildPath(resource, monitor);
    }

    /**
     * Write out the results of the processing.
     * 
     * @param resource
     * @param charset
     * @param symbols
     * @param sourceDocument
     * @param monitor
     * @throws CoreException
     */
    private void writeProcessedResults(final IResource resource,
            byte[] contents, String charset, IProgressMonitor monitor)
            throws CoreException {
        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy.traceln("> PreprocessorBuilder.writeProcessedResults");
        }

        ByteArrayInputStream is = new ByteArrayInputStream(contents);

        final IFile outputFile = getOutputFile(resource);
        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy
                    .traceln("- PreprocessorBuilder.writeProcessedResults; outputFile = "
                            + outputFile);
        }

        if (outputFile.exists()) {
            if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
                consoleProxy
                        .traceln("- PreprocessorBuilder.writeProcessedResults; outputFile exists, setting new contents");
            }
            setReadOnly(outputFile, false);
            outputFile.setContents(is, true, false, monitor);
        } else {
            IContainer container = outputFile.getParent();
            if (!container.exists()) {
                createParentFolders(outputFile, monitor);
            }

            outputFile.create(is, false, monitor);
        }

        outputFile.setCharset(charset, monitor);
        outputFile.setDerived(true);
        setReadOnly(outputFile, true);

        // It seems this should not be necessary, but let's see if it
        // helps on some problems we are seeing...
        // outputFile.getProject().build(AUTO_BUILD, monitor);

        if (buildLoggingConfig.isPreprocessorTraceEnabled()) {
            consoleProxy.traceln("< PreprocessorBuilder.writeProcessedResults");
        }
    }
}
