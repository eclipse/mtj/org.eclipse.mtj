/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow
 *                                eclipse standards
 */
package org.eclipse.mtj.internal.core.build.preverifier.builder;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.build.preverifier.StandardPreverifier;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;
import org.eclipse.mtj.internal.core.text.IResourceFilter;

import de.schlichtherle.io.File;

/**
 * Holds information necessary while executing the build processing.
 * 
 * @author Craig Setera
 */
class BuildInfo {
    // Build specified information
    private int buildKind;
    private Map<?, ?> buildArgs;

    // Project references
    private IMidletSuiteProject midletSuite;
    private IMidletSuiteProject targetMidletSuite;
    private IJavaProject baseJavaProject;
    private IJavaProject currentJavaProject;

    // The deployed jar file into which our output is being written
    private File runtimeJarFile;

    // Source folder paths used to filter class files
    // that aren't actually in output folders
    private IPath[] outputFolders;

    // The ResourceDelta being built
    private IResourceDelta currentResourceDelta;

    // Filtering in effect
    private IResourceFilter resourceFilter;

    // The folders
    private IFolder runtimeFolder;

    private IFolder verifiedClassesFolder;
    private IFolder verifiedLibsFolder;

    // Whether or not the packaged output has been rendered "dirty"
    private boolean packageDirty;

    // Whether or not the classpath was changed
    private boolean classpathChanged;

    // The libraries preverified or attempted to be preverified with failure
    private Set<File> preverifiedLibraries;

    /**
     * Construct a new BuildInfo instance to be used in preverification
     * building.
     * 
     * @param kind
     * @param args
     * @param baseJavaProject
     */
    BuildInfo(int kind, Map<?, ?> args, IJavaProject baseJavaProject)
            throws JavaModelException {
        this.buildKind = kind;
        this.buildArgs = args;
        this.baseJavaProject = baseJavaProject;
        this.midletSuite = MidletSuiteFactory
                .getMidletSuiteProject(baseJavaProject);

        classpathChanged = false;
        preverifiedLibraries = new HashSet<File>();

        setCurrentJavaProject(baseJavaProject);
    }

    /**
     * Add a preverified library to those that have been visited.
     * 
     * @param libraryFile
     */
    public void addPreverifiedLibrary(File libraryFile) {
        preverifiedLibraries.add(libraryFile);
    }

    /**
     * Return a boolean indicating whether the build arguments specify
     * preverification of classes. This implementation is careful to lean toward
     * enabling preverification to make sure that older project versions work
     * without change.
     * 
     * @return
     */
    public boolean areClassesPreverified() {
        // This implementation leans toward enabling preverification
        boolean preverify = true;

        String stringValue = (String) buildArgs
                .get(StandardPreverifier.BUILD_ARG_PREVERIFY_CLASSES);
        if (stringValue != null) {
            if (stringValue.equals("false")) {
                preverify = false;
            }
        }

        return preverify;
    }

    /**
     * Return a boolean indicating whether the build arguments specify
     * preverification of libraries. This implementation is careful to lean
     * toward enabling preverification to make sure that older project versions
     * work without change.
     * 
     * @return
     */
    public boolean areLibrariesPreverified() {
        // This implementation leans toward enabling preverification
        boolean preverify = true;

        String stringValue = (String) buildArgs
                .get(StandardPreverifier.BUILD_ARG_PREVERIFY_LIBS);
        if (stringValue != null) {
            if (stringValue.equals("false")) {
                preverify = false;
            }
        }

        return preverify;
    }

    /**
     * @return Returns the baseJavaProject.
     */
    public IJavaProject getBaseJavaProject() {
        return baseJavaProject;
    }

    /**
     * @return Returns the buildArgs.
     */
    public Map<?, ?> getBuildArgs() {
        return buildArgs;
    }

    /**
     * @return Returns the buildKind.
     */
    public int getBuildKind() {
        return buildKind;
    }

    /**
     * @return Returns the currentJavaProject.
     */
    public IJavaProject getCurrentJavaProject() {
        return currentJavaProject;
    }

    /**
     * @return Returns the currentResourceDelta.
     */
    public IResourceDelta getCurrentResourceDelta() {
        return currentResourceDelta;
    }

    /**
     * Return the jar file that is being used for runtime.
     * 
     * @param monitor
     * @return
     * @throws CoreException
     */
    public File getRuntimeJarFile(IProgressMonitor monitor)
            throws CoreException {
        if (runtimeJarFile == null) {
            runtimeJarFile = PreverificationBuilder.getRuntimeJar(
                    baseJavaProject.getProject(), monitor);
        }

        return runtimeJarFile;
    }

    /**
     * @return Returns the midletSuite.
     */
    public IMidletSuiteProject getMidletSuite() {
        return midletSuite;
    }

    /**
     * @return Returns the resourceFilter.
     */
    public IResourceFilter getResourceFilter() {
        return resourceFilter;
    }

    /**
     * Return the runtime folder.
     * 
     * @param monitor
     * @return
     */
    public IFolder getRuntimeFolder(IProgressMonitor monitor) {
        if (runtimeFolder == null) {
            IFolder tempFolder = getTargetMidletSuite().getProject().getFolder(
                    IMTJCoreConstants.TEMP_FOLDER_NAME);
            runtimeFolder = tempFolder
                    .getFolder(IMTJCoreConstants.EMULATION_FOLDER_NAME);
        }

        return runtimeFolder;
    }

    /**
     * Return the midlet suite into which the preverified output should be
     * placed.
     * 
     * @return
     */
    public IMidletSuiteProject getTargetMidletSuite() {
        if (targetMidletSuite == null) {
            String targetProjectName = (String) buildArgs
                    .get(StandardPreverifier.BUILD_ARG_PREVERIFY_TARGET);

            if (targetProjectName == null) {
                targetMidletSuite = midletSuite;
            } else {
                IProject project = MTJCore.getWorkspace().getRoot()
                        .getProject(targetProjectName);
                IJavaProject javaProject = JavaCore.create(project);
                targetMidletSuite = MidletSuiteFactory
                        .getMidletSuiteProject(javaProject);
            }
        }

        return targetMidletSuite;
    }

    /**
     * Return a boolean indicating whether the current java project is the
     * midlet suite project.
     * 
     * @return
     */
    public boolean isCurrentProjectMidletSuite() {
        return currentJavaProject.equals(baseJavaProject);
    }

    /**
     * Return the verified classes folder to be used.
     * 
     * @param monitor
     * @return
     * @throws CoreException
     */
    IFolder getVerifiedClassesFolder(IProgressMonitor monitor)
            throws CoreException {
        if (verifiedClassesFolder == null) {
            verifiedClassesFolder = getTargetMidletSuite()
                    .getVerifiedClassesOutputFolder(monitor);
            PreverificationBuilder
                    .createFolders(verifiedClassesFolder, monitor);
        }

        return verifiedClassesFolder;
    }

    /**
     * Return the verified libraries folder to be used.
     * 
     * @param monitor
     * @return
     * @throws CoreException
     */
    IFolder getVerifiedLibsFolder(IProgressMonitor monitor)
            throws CoreException {
        if (verifiedLibsFolder == null) {
            verifiedLibsFolder = getTargetMidletSuite()
                    .getVerifiedLibrariesOutputFolder(monitor);
            PreverificationBuilder.createFolders(verifiedLibsFolder, monitor);
        }

        return verifiedLibsFolder;
    }

    /**
     * Return a boolean indicating whether the current java project is also the
     * base java project.
     * 
     * @return
     */
    boolean isCurrentProjectTheBaseProject() {
        return currentJavaProject.equals(baseJavaProject);
    }

    /**
     * Return a boolean indicating whether the specified resource is in an
     * output folder.
     * 
     * @param resource
     * @return
     */
    boolean isOutputResource(IResource resource) {
        boolean isOutput = false;

        IPath resourcePath = resource.getFullPath();
        for (int i = 0; i < outputFolders.length; i++) {
            IPath outputPath = outputFolders[i];
            if (outputPath.isPrefixOf(resourcePath)) {
                isOutput = true;
                break;
            }
        }

        return isOutput;
    }

    /**
     * @param currentJavaProject The currentJavaProject to set.
     */
    public void setCurrentJavaProject(IJavaProject currentJavaProject)
            throws JavaModelException {
        this.currentJavaProject = currentJavaProject;
        resourceFilter = new SourceResourcesFilter(currentJavaProject);
        initializeOutputFolders(currentJavaProject);
    }

    /**
     * @param currentResourceDelta The currentResourceDelta to set.
     */
    public void setCurrentResourceDelta(IResourceDelta currentResourceDelta) {
        this.currentResourceDelta = currentResourceDelta;
    }

    /**
     * Return whether there has previously been an attempt to preverify the
     * specified library.
     * 
     * @param libraryFile
     * @return
     */
    public boolean hasLibraryBeenPreverified(File libraryFile) {
        return preverifiedLibraries.contains(libraryFile);
    }

    /**
     * Return whether the specified argument name was set to true in the build
     * arguments.
     * 
     * @param buildInfo
     * @param argName
     * @return
     */
    public boolean isBuildArgumentTrue(String argName) {
        Boolean value = (Boolean) getBuildArgs().get(argName);
        return ((value != null) && value.booleanValue());
    }

    /**
     * @return Returns the classpathChanged.
     */
    public boolean isClasspathChanged() {
        return classpathChanged;
    }

    /**
     * @return Returns the dirtyPackage.
     */
    protected boolean isPackageDirty() {
        return packageDirty;
    }

    /**
     * @param classpathChanged The classpathChanged to set.
     */
    public void setClasspathChanged(boolean classpathChanged) {
        this.classpathChanged = classpathChanged;
    }

    /**
     * @param dirtyPackage The dirtyPackage to set.
     */
    protected void setPackageDirty(boolean dirtyPackage) {
        this.packageDirty = dirtyPackage;
    }

    /**
     * Initialize the output folder used for filtering based on the specified
     * java project.
     * 
     * @param javaProject
     * @throws JavaModelException
     */
    private void initializeOutputFolders(IJavaProject javaProject)
            throws JavaModelException {
        // Initialize the output folders so we can filter the
        // the class files outside the output folders
        Set<IPath> outputFolderSet = new HashSet<IPath>();
        outputFolderSet.add(javaProject.getOutputLocation());

        IClasspathEntry[] entries = javaProject.getRawClasspath();
        for (int i = 0; i < entries.length; i++) {
            IClasspathEntry entry = entries[i];
            if (entry.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
                IPath outputPath = entry.getOutputLocation();
                if (outputPath != null) {
                    outputFolderSet.add(outputPath);
                }
            }
        }

        outputFolders = (IPath[]) outputFolderSet
                .toArray(new IPath[outputFolderSet.size()]);
    }
}
