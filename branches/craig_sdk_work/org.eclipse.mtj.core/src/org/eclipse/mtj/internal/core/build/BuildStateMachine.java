/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.build;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.build.MTJBuildState;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.internal.core.util.MTJStatusHandler;

/**
 *	BuildStateMachine class implements a state machine for
 *  the MTJ project's build process. All states are defined
 *  within the {@link MTJBuildState} enumeration.
 *  
 *  @author David Marques
 */
public class BuildStateMachine {

	private static Map<IMTJProject, BuildStateMachine> map;

	/**
	 * Gets the BuildStateMachine instance for the specified 
	 * {@link IMTJProject} instance.
	 * 
	 * @param mtjProject target project.
	 * @return the BuildStateMachine instance.
	 */
	public static synchronized BuildStateMachine getInstance(IMTJProject mtjProject) {
		BuildStateMachine stateMachine = null;
		
		if (BuildStateMachine.map == null) {
			BuildStateMachine.map = new HashMap<IMTJProject, BuildStateMachine>();
		}
		
		stateMachine = BuildStateMachine.map.get(mtjProject);
		if (stateMachine == null) {
			stateMachine = new BuildStateMachine(mtjProject);
			BuildStateMachine.map.put(mtjProject, stateMachine);
		}
		return stateMachine;
	}
	
	private MTJBuildState currentState;
	private IMTJProject   mtjProject;
	
	/**
	 * Creates an instance of a BuildStateMachine for the
	 * specified project.
	 * 
	 * @param mtjProject target project.
	 */
	private BuildStateMachine(IMTJProject mtjProject) {
		this.mtjProject = mtjProject;
	}
	
	/**
	 * Starts the build state machine into the {@link MTJBuildState}
	 * PRE_BUILD state.
	 * 
	 * @param monitor progress monitor.
	 * @throws CoreException Any core error occurred.
	 */
	public void start(IProgressMonitor monitor) throws CoreException {
		this.currentState = null;
		this.changeState(MTJBuildState.PRE_BUILD, monitor);
	}
	
	/**
	 * Changes the current build state and notifyes all hooks.
	 * 
	 * @param state new state.
	 * @param monitor progress monitor.
	 * @throws CoreException Any core error occurred.
	 */
	public void changeState(MTJBuildState state, IProgressMonitor monitor) throws CoreException {
		if (this.currentState == null && state != MTJBuildState.PRE_BUILD) {
			throw new CoreException(MTJStatusHandler.newStatus(IStatus.ERROR
					, 999, "Build state machine has not been initialized."));
		}
		if (this.currentState == state) {
			return;
		}
		this.currentState = state;
		
		List<BuildHookInfo> hooks = BuildHooksRegistry.getInstance().getBuildHooks();
		for (BuildHookInfo hook : hooks) {
			hook.getHook().buildStateChanged(this.mtjProject, this.currentState, monitor);
		}
	}
}
