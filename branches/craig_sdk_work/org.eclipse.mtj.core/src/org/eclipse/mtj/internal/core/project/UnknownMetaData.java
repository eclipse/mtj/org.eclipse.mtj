/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.project;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.build.sign.ISignatureProperties;
import org.eclipse.mtj.core.project.IMetaData;
import org.eclipse.mtj.core.project.runtime.MTJRuntimeList;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.osgi.framework.Version;

/**
 * @author Diego Madruga Sandin
 * @since 1.0
 */
public class UnknownMetaData implements IMetaData {

    /**
     * 
     */
    public UnknownMetaData() {

    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getConfigurations()
     */
    public MTJRuntimeList getRuntimeList() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getDevice()
     */
    public IDevice getDevice() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getSignatureProperties()
     */
    public ISignatureProperties getSignatureProperties() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#getVersion()
     */
    public Version getVersion() {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#saveMetaData()
     */
    public void saveMetaData() throws CoreException {
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#setConfigurations(org.eclipse.mtj.internal.core.project.configuration.Configurations)
     */
    public void setMTJRuntimeList(MTJRuntimeList configurations)
            throws IllegalArgumentException {
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#setDevice(org.eclipse.mtj.core.sdk.device.IDevice)
     */
    public void setDevice(IDevice device) {
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.project.IMetaData#setSignatureProperties(org.eclipse.mtj.internal.core.build.sign.ISignatureProperties)
     */
    public void setSignatureProperties(ISignatureProperties p) {
    }

}
