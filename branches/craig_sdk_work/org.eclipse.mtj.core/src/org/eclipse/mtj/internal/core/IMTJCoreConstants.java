/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All Rights Reserved.
 * Licensed under the Eclipse Public License - v 1.0
 * For more information see http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package and class name 
 *                                to follow eclipse standards
 *     Hugo Raniere (Motorola)  - Removing Preprocessor code
 *     Feng Wang (Sybase)       - Add LAUNCH_FROM_JAD_FOLDER constant for
 *                                launching from JAD.
 *     Hugo Raniere (Motorola)  - Adding key to represent default preverifier
 *     Diego Sandin (Motorola)  - Re-enabling Preprocessor code
 *     Hugo Raniere (Motorola)  - Adding key to represent a java me problem marker
 *     Diego Sandin (Motorola)  - Adding key to represent a missing device definition
 *                                problem marker
 *     Gang Ma      (Sybase)    - Adding key to represent preprocess debug level
 *     David Marques(Motorola)  - Adding key to represent process type.
 *     David Marques(Motorola)  - Adding L10N Nature Constant.
 *     
 */
package org.eclipse.mtj.internal.core;

/**
 * Constant definitions for use throughout the plug-in.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IMTJCoreConstants {

    // The plug-in ID
    public static final String PLUGIN_ID = "org.eclipse.mtj.core"; //$NON-NLS-1$

    // Directory preference keys and defaults
    public static final String PREF_DEPLOYMENT_DIR = "deployment_dir"; //$NON-NLS-1$
    public static final String PREF_VERIFIED_DIR = "verified_dir"; //$NON-NLS-1$

    // New project creation preferences
    public static final String PREF_USE_RESOURCES_DIR = "use_resources_dir"; //$NON-NLS-1$
    public static final String PREF_RESOURCES_DIR = "resources_dir"; //$NON-NLS-1$
    public static final String PREF_FORCE_JAVA11 = "force_java11"; //$NON-NLS-1$

    // Over the Air preferences
    public static final String PREF_OTA_SERVER_START_AT_START = "ota_start_at_start"; //$NON-NLS-1$
    public static final String PREF_OTA_PORT_DEFINED = "ota_port_defined"; //$NON-NLS-1$
    public static final String PREF_OTA_PORT = "ota_port"; //$NON-NLS-1$
    public static final String PREF_OTA_AUTODEPLOY = "ota_autodeploy"; //$NON-NLS-1$

    // Proguard obfuscation preferences
    public static final String PREF_OBFUSCATION_USE_PROJECT = "obfuscate_use_project"; //$NON-NLS-1$
    public static final String PREF_PROGUARD_DIR = "proguard_dir"; //$NON-NLS-1$
    public static final String PREF_PROGUARD_USE_SPECIFIED = "proguard_use_specified"; //$NON-NLS-1$
    public static final String PREF_PROGUARD_OPTIONS = "proguard_options"; //$NON-NLS-1$
    public static final String PREF_PROGUARD_KEEP = "proguard_keep"; //$NON-NLS-1$

    // Packaging related preferences
    public static final String PREF_PKG_USE_PROJECT = "pkg_use_project"; //$NON-NLS-1$
    public static final String PREF_PKG_AUTOVERSION = "pkg_autoversion"; //$NON-NLS-1$
    public static final String PREF_PKG_EXCLUDED_PROPS = "pkg_excluded_props"; //$NON-NLS-1$
    public static final String PREF_PKG_BUILD_XML = "pkg_build_xml"; //$NON-NLS-1$

    // Preverifier related preferences
    public static final String PREF_PREVERIFY_USE_PROJECT = "pkg_use_project"; //$NON-NLS-1$

    public static final String PREF_PREVERIFY_CONFIG_LOCATION = "preverify_config_location"; //$NON-NLS-1$
    public static final String PREF_PREVERIFY_CONFIG_VALUE = "preverify_config_value"; //$NON-NLS-1$
    public static final String PREF_DEFAULT_PREVERIFIER = "default_preverifier"; //$NON-NLS-1$

    public static final String PREF_PREVERIFY_CONFIG_LOCATION_JAD = "jad"; //$NON-NLS-1$
    public static final String PREF_PREVERIFY_CONFIG_LOCATION_PLATFORM = "platform"; //$NON-NLS-1$
    public static final String PREF_PREVERIFY_CONFIG_LOCATION_SPECIFIED = "specified"; //$NON-NLS-1$

    public static final String MANIFEST_FILE_NAME = "META-INF/MANIFEST.MF";

    // Antenna settings
    public static final String PREF_WTK_ROOT = "wtk_root"; //$NON-NLS-1$
    public static final String PREF_ANTENNA_JAR = "antenna_jar"; //$NON-NLS-1$

    // Miscellaneous preferences
    public static final String PREF_RMTDBG_TIMEOUT = "rmt_debug_delay"; //$NON-NLS-1$
    public static final String PREF_RMTDBG_INTERVAL = "rmt_debug_poll_interval"; //$NON-NLS-1$
    public static final String PREF_AUTO_LAUNCH_MIGRATION = "auto_launch_migration"; //$NON-NLS-1$

    // Preprocessor preferences
    public static final String PREF_PREPROCESS_USE_PROJECT = "preprocess_use_project"; //$NON-NLS-1$
    public static final String PREF_PREPROCESS_DEBUG_LEVEL = "preprocess_debuglevel"; //$NON-NLS-1$

    /**
     * Debugging property keys Key that uses "true" or "false" to control
     * whether or not to dump the launch command line for the emulator
     */
    public static final String PROP_DUMP_LAUNCH = "mtj.dump.launch"; //$NON-NLS-1$

    /** The superclass of all MIDlets */
    public static final String MIDLET_SUPERCLASS = "javax.microedition.midlet.MIDlet"; //$NON-NLS-1$

    // Project folder names...

    /**
     * The directory used to hold temporary files such as preverified and
     * incrementally built jar files
     */
    public static final String TEMP_FOLDER_NAME = ".mtj.tmp"; //$NON-NLS-1$

    /**
     * The sub-folder of the TEMP folder that holds runtime classes
     */
    public static final String RUNTIME_FOLDER_NAME = "runtime";

    // The sub-folder of the TEMP folder that holds verified classes and
    // libraries
    public static final String VERIFIED_FOLDER_NAME = "verified"; //$NON-NLS-1$

    // The sub-folder of the TEMP folder that holds the JAR and JAD files for
    // execution by the emulator
    public static final String EMULATION_FOLDER_NAME = "emulation"; //$NON-NLS-1$

    // The sub-folder of the ".mtj.tmp" folder that holds JAR and JAD for
    // launching from JAD emulation
    public static final String LAUNCH_FROM_JAD_FOLDER = "launchFromJAD"; //$NON-NLS-1$

    // Miscellaneous
    public static final String PROGUARD_JAR = "proguard.jar"; //$NON-NLS-1$

    public static final String MTJ_NATURE_ID = PLUGIN_ID + ".nature"; //$NON-NLS-1$

    public static final String J2ME_PREPROCESSING_NATURE_ID = PLUGIN_ID
            + ".preprocessingNature"; //$NON-NLS-1$
    public static final String J2ME_PREVERIFIER_ID = PLUGIN_ID + ".preverifier"; //$NON-NLS-1$
    public static final String J2ME_PREPROCESSOR_ID = PLUGIN_ID
            + ".preprocessor"; //$NON-NLS-1$
    public static final String JAVAME_PROBLEM_MARKER = PLUGIN_ID + ".problem"; //$NON-NLS-1$

    /**
     * Missing device definition resource marker ID
     */
    public static final String JAVAME_MISSING_DEVICE_MARKER = PLUGIN_ID
            + ".device.missing"; //$NON-NLS-1$

    public static final String MTJ_PROCESS_TYPE = "MTJProcess"; //$NON-NLS-1$

    // ////////////////////////////////////////////////////////////////////
    // Error messages
    // ////////////////////////////////////////////////////////////////////
    /** No MIDlets defined during OTA launch */
    public static final int ERR_OTA_NO_MIDLETS = 100;

    /** Errors occurred during obfuscation */
    public static final int ERR_OBFUSCATION_ERRORS = 101;

    /** Error searching for jar executable */
    public static final int ERR_COULD_NOT_FIND_JAR_TOOL = 102;

    /** Need signature passwords (not really an error, but triggers a dialog) */
    public static final int INFO_NEED_SIGNATURE_PASSWORDS = 103;

    /**
     * Need check for debug setting(not really an error, but triggers a dialog
     * if the current debugger setting is not satisfied to debug a MIDlet)
     */
    public static final int INFO_DEBUGGER_SETTINGS_CHECK = 104;

    /**
     * This constant is the old eclipse me nature id.
     */
    public static final String ECLIPSE_ME_NATURE = "eclipseme.core.nature"; //$NON-NLS-1$

    /**
     * This constant holds the L10N Nature ID
     */
    public static final String L10N_NATURE_ID = PLUGIN_ID + ".l10nNature"; //$NON-NLS-1$

    /**
     * This constant holds the L10N Builder ID
     */
    public static final String L10N_BUILDER_ID = PLUGIN_ID + ".l10nBuilder"; //$NON-NLS-1$

    /**
     * This constant holds the JMUnit Nature ID
     */
    public static final String JMUNIT_NATURE_ID = "org.eclipse.mtj.jmunit.jmunitNature"; //$NON-NLS-1$

    public static final String VERSION_NLS_BIND_TEMPLATE = "{0}.{1}";
}
