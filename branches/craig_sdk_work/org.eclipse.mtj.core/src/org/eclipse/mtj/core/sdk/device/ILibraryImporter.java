/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 *     Diego Sandin (Motorola) - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.sdk.device;

import java.io.File;
import java.util.List;

import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.mtj.core.MTJCore;

/**
 * The ILibraryImporter provides a way for clients to create an ILibrary
 * instance based on a jar file.
 * <p>
 * MTJ provides 1 implementation for this interface, the UEI library importer(
 * {@link #LIBRARY_IMPORTER_UEI}) and it is accessible through the
 * {@link MTJCore#getLibraryImporter(String)} method.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface ILibraryImporter {

    /**
     * Constant that represents the UEI library importer
     */
    public static final String LIBRARY_IMPORTER_UEI = "org.eclipse.mtj.libraryimporter.uei";

    /**
     * Construct and return a new ILibrary instance for the specified library
     * file.
     * 
     * @param libraryJarFile the jar file to be wrapped up as a library.
     * @return the newly created ILibrary instance.
     */
    public abstract ILibrary createLibraryFor(File libraryJarFile);

    /**
     * Construct and return a new ILibrary instance for the specified library
     * file with the provided access rules.
     * 
     * @param libraryJarFile the jar file to be wrapped up as a library.
     * @param accessRules the list with all access rules for this library.
     * @return the newly created ILibrary instance.
     */
    public abstract ILibrary createLibraryFor(File libraryJarFile,
            List<IAccessRule> accessRules);
}