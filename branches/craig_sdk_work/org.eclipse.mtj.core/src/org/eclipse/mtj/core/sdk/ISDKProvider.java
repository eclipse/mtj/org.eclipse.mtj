/**
 * 
 */
package org.eclipse.mtj.core.sdk;

/**
 * Implementations of the ISDKProvider interface provide access
 * to underlying {@link ISDK} instances.  A particular vendor
 * may create a single {@link ISDKProvider} that returns multiple
 * {@link ISDK} instance for the various SDK's that make up their
 * product line.
 * 
 * @author Craig Setera
 */
public interface ISDKProvider {
	/** The extension point for this interface */
	public static final String EXT_SDK_PROVIDER = "sdkprovider";
	
	/**
	 * Return the {@link ISDK} instances for this particular provider.
	 * 
	 * @return
	 */
	ISDK[] getSDKs();
	
	/**
	 * Set the identifier of this {@link ISDKProvider} as specified
	 * in the extension point.
	 * 
	 * @param identifier
	 */
	void setIdentifier(String identifier);
}
