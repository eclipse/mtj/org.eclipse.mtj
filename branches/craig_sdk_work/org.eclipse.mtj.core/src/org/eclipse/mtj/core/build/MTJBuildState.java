/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Adding obfuscation and signing.
 */
package org.eclipse.mtj.core.build;

/**
 * MTJBuildState enumeration defines all states of the MTJ build process.
 * <p>
 * The ordered list of possible states is provided below:
 * <ol>
 * <li>{@link #PRE_BUILD} [Before build process starts]</li>
 * <li>{@link #PRE_PREPROCESS} [Before preprocessing]</li>
 * <li>{@link #POST_PREPROCESS} [After preprocessing]</li>
 * <li>{@link #PRE_COMPILE} [Before JDT builder starts]</li>
 * <li>{@link #POST_COMPILE} [After JDT builder ends]</li>
 * <li>{@link #PRE_LOCALIZATION} [Before localization]</li>
 * <li>{@link #POST_LOCALIZATION} [After localization]</li>
 * <li>{@link #PRE_PREVERIFICATION} [Before preverifying]</li>
 * <li>{@link #POST_PREVERIFICATION} [After preverifying]</li>
 * <li>{@link #PRE_OBFUSCATION} [Before obfuscation]</li>
 * <li>{@link #POST_OBFUSCATION} [After obfuscation]</li>
 * <li>{@link #PRE_SIGNING} [Before signing]</li>
 * <li>{@link #POST_SIGNING} [After signing]</li>
 * <li>{@link #POST_BUILD} [After build process ends]</li>
 * </ol>
 * </p>
 * 
 * @author David Marques
 * @since 1.0
 */
public enum MTJBuildState {

    /**
     * This state represents the state before the build starts.
     */
    PRE_BUILD,

    /**
     * This state represents the state after the build finishes.
     */
    POST_BUILD,

    /**
     * This state represents the state before preprocessing.
     */
    PRE_PREPROCESS,

    /**
     * This state represents the state after preprocessing.
     */
    POST_PREPROCESS,

    /**
     * This state represents the state before preverification.
     */
    PRE_PREVERIFICATION,

    /**
     * This state represents the state after preverification.
     */
    POST_PREVERIFICATION,

    /**
     * This state represents the state before building localization.
     */
    PRE_LOCALIZATION,

    /**
     * This state represents the state after building localization.
     */
    POST_LOCALIZATION,

    /**
     * This state represents the state before compilation.
     */
    PRE_COMPILATION,

    /**
     * This state represents the state after compilation.
     */
    POST_COMPILATION, 
    
    /**
     * This state represents the state before obfuscation.
     */
    PRE_OBFUSCATION,
    
    /**
     * This state represents the state after obfuscation.
     */
    POST_OBFUSCATION,
    
    /**
     * This state represents the state before signing.
     */
    PRE_SIGNING,
    
    /**
     * This state represents the state after signing.
     */
    POST_SIGNING

}
