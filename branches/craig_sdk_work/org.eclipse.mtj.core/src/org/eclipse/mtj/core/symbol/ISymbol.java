/**
 * Copyright (c) 2008, 2009 Motorola Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gustavo de Paula (Motorola) - Initial implementation
 *     Diego Sandin (Motorola)     - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.symbol;

import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.persistence.IPersistable;

/**
 * A Symbol is a pair &lt;name; value&gt; and may be used for preprocessing
 * purposes.
 * <p>
 * Clients must use {@link MTJCore#getSymbolSetFactory()} to retrieve an
 * {@link ISymbolSetFactory} instance and use the
 * {@link ISymbolSetFactory#createSymbol(String, String)} to create an ISymbol
 * instance.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * 
 * @since 1.0
 */
public interface ISymbol extends IPersistable {

    /**
     * When a symbol represents an ability. For example, if a device is touch
     * screen or not.
     */
    public static final int TYPE_ABILITY = 0;

    /**
     * When a symbol represents an MTJRuntime ID.
     */
    public static final int TYPE_RUNTIME = 1;

    /**
     * If name equals, then symbol equals.
     * 
     * @param obj the reference object with which to compare.
     * 
     * @return <code>true</code> if this object is the same as the obj argument;
     *         <code>false</code> otherwise.
     */
    public abstract boolean equals(Object obj);

    /**
     * Return the name of the symbol.
     * 
     * @return the symbol name.
     */
    public abstract String getName();

    /**
     * Get value in safe format for preprocessor.
     * 
     * @return symbol value in safe format for preprocessor.
     */
    public abstract String getSafeValue();

    /**
     * Get the symbol type.
     * 
     * @return the symbol type. Possible types are {@link #TYPE_ABILITY} and
     *         {@link #TYPE_RUNTIME}.
     */
    public abstract int getType();

    /**
     * Return the symbol value.
     * 
     * @return the value of the symbol.
     */
    public abstract String getValue();

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    public abstract int hashCode();

    /**
     * Sets the the name of the symbol.
     * 
     * @param identifier the name of the symbol. This is case-sensitive and must
     *            not be <code>null</code> or an empty String <code>""</code>.
     */
    public abstract void setName(String identifier);

    /**
     * Set the symbol type.
     * 
     * @param type the symbol type. Possible types are {@link #TYPE_ABILITY} and
     *            {@link #TYPE_RUNTIME}.
     */
    public abstract void setType(int type);

    /**
     * Set the value of the symbol.
     * 
     * @param value the value of the symbol.
     */
    public abstract void setValue(String value);

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public abstract String toString();

}