/**
 * Copyright (c) 2003, 2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     Diego Sandin (Motorola)  - Added application descriptor constant value                           
 *     Hugo Raniere (Motorola)  - Handling the case that there is no valid 
 *                                preverifier
 *     Feng Wang (Sybase)       -  1, Add getConfigurations() method for 
 *                                    multi-configurations support
 *                                 2, Remove getEnabledSymbolDefinitionSet() 
 *                                    method, since SymbolDefinitionSets are 
 *                                    contained by configuration.
 *     Diego Sandin (Motorola)  - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.project.midp;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.build.preverifier.IPreverificationError;
import org.eclipse.mtj.core.build.preverifier.PreverifierNotFoundException;
import org.eclipse.mtj.core.project.IMTJProject;

/**
 * This interface represents an specialization of an {@link IMTJProject} to
 * represent a MIDlet suite project.
 * <p>
 * Features of MTJ projects include:
 * <ul>
 * <li>Has an associated "Application Descriptor" file.</li>
 * <li>Controls the process of preverifying its resources.</li>
 * <li>Carry properties related to the signing process.</li>
 * <li>Notify listeners when it's state changes.</li>
 * </ul>
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 * @noimplement This interface is not intended to be implemented by clients.
 */
public interface IMidletSuiteProject extends IMTJProject {

    /**
     * The project's application descriptor file name.
     */
    public static final String APPLICATION_DESCRIPTOR_NAME = "Application Descriptor"; //$NON-NLS-1$

    /**
     * Return an ApplicationDescriptor instance wrapped around the Application
     * Descriptor (JAD) file for this MIDlet suite.
     * 
     * @return the suite's application descriptor.
     */
    public IApplicationDescriptor getApplicationDescriptor();

    /**
     * Return the {@link IFile} instance of the Application Descriptor (JAD)
     * file for this MIDlet suite.
     * 
     * @return the suite's application descriptor IFile.
     */
    public IFile getApplicationDescriptorFile();

    /**
     * Return the name that must be used on the project's jad file after
     * deployment as specified in the project's metadata file.
     * 
     * @return the deployable jad file name.
     */
    public String getJadFileName();

    /**
     * Return the name to use for the deployable JAR file.
     * 
     * @return the jar file name.
     */
    public String getJarFilename();

    /**
     * Get the temporary password for a key in the keystore.
     * 
     * @return temporary password for a key in the keystore.
     */
    public abstract String getTempKeyPassword();

    /**
     * Get the temporary password for the keystore.
     * 
     * @return temporary password for the keystore.
     */
    public abstract String getTempKeystorePassword();

    /**
     * Get the IFolder into which verified classes should be written.
     * 
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @return a handle to the folder where verified classes should be
     *         outputted.
     */
    public abstract IFolder getVerifiedClassesOutputFolder(
            IProgressMonitor monitor);

    /**
     * Get the IFolder into which verified libraries should be written.
     * 
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @return a handle to the folder where libraries should be outputted.
     */
    public abstract IFolder getVerifiedLibrariesOutputFolder(
            IProgressMonitor monitor);

    /**
     * Get the IFolder into which verified classes should be written.
     * 
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @return the verified root output folder.
     */
    public abstract IFolder getVerifiedOutputFolder(IProgressMonitor monitor);

    /**
     * Preverify the specified resources. Return the map of class names with
     * preverification errors mapped to the error that was caused.
     * 
     * @param toVerify the resources to be preverified
     * @param outputFolder the folder into which the output will be written
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @return An array of errors found during preverification.
     * @throws CoreException if this method fails. Reasons include:
     *             <ul>
     *             <li>The existence of outputFolder could not be guaranteed.</li>
     *             <li>The command line for triggering the preverification
     *             process could not be created.</li>
     *             <li>The preverification process could not be created
     *             correctly.</li>
     *             </ul>
     * @throws PreverifierNotFoundException a default preverifier was not
     *             specified.
     */
    public IPreverificationError[] preverify(IResource[] toVerify,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, PreverifierNotFoundException;

    /**
     * Launch the preverification process on the specified jar file.
     * 
     * @param jarFile The jar file to be preverified.
     * @param outputFolder The folder into which the output is to be placed.
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @return An array of errors found during preverification.
     * @throws CoreException if this method fails. Reasons include:
     *             <ul>
     *             <li>The <code>.jar</code> file could not be expanded.</li>
     *             <li>The existence of outputFolder could not be guaranteed.</li>
     *             <li>The command line for triggering the preverification
     *             process could not be created.</li>
     *             <li>The preverification process could not be created
     *             correctly.</li>
     *             </ul>
     * @throws PreverifierNotFoundException a default preverifier was not
     *             specified.
     */
    public IPreverificationError[] preverifyJarFile(File jarFile,
            IFolder outputFolder, IProgressMonitor monitor)
            throws CoreException, PreverifierNotFoundException;

    /**
     * Set the name that must be used on the project's jad file after deployment
     * as specified in the project's metadata file.
     * 
     * @param jadFileName the deployable jad file name.
     */
    public void setJadFileName(String jadFileName);

    /**
     * Get the temporary password for a key in the keystore.
     * 
     * @param pass temporary password for a key in the keystore.
     */
    public abstract void setTempKeyPassword(String pass);

    /**
     * Get the temporary password for the keystore.
     * 
     * @param pass temporary password for the keystore.
     */
    public abstract void setTempKeystorePassword(String pass);
}
