/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.sdk;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IDevice;
import org.osgi.framework.Version;

/**
 * This interface represents an SDK. It can be used to implement a automatic SDK
 * install on MTJ.
 * 
 * @since 1.0
 */
public interface ISDK {

	/**
	 * The specified device has been added by the user.
	 * 
	 * @param device
	 */
	public abstract void deviceAdded(IDevice device);
	
	/**
	 * The specified device has been edited by the user.
	 * 
	 * @param device
	 */
	public abstract void deviceEdited(IDevice device);
	
	/**
	 * The specified device has been removed by the user.
	 * 
	 * @param device
	 */
	public abstract void deviceRemoved(IDevice device);
	
    /**
     * Return the displayable description of this SDK. This description will be
     * displayed within the user interface. If this method returns a
     * <code>null</code> value, the SDK's name will be used as the description
     * instead.
     * 
     * @return the description of this SDK or <code>null</code> if the SDK's
     *         name should be used instead.
     */
    public abstract String getDescription();

    /**
     * Return the fully configured device instances available in this SDK.
     * 
     * @return the list of devices in this SDK
     * @throws CoreException thrown if an error occur while retrieving the
     *             device list.
     */
    public abstract List<IDevice> getDeviceList() throws CoreException;

    /**
     * Return the unique identifier for this {@link ISDK} instance.  This value
     * will not be displayed to the user, but may be used by the underlying implementation.
     * 
     * @return
     */
    public abstract String getIdentifier();
    
    /**
     * Return the name of this SDK. This name will be displayed within the user
     * interface and must never be <code>null</code>.
     * 
     * @return the name of this SDK.
     */
    public abstract String getName();

    /**
     * Return the version of this SDK. This version will be displayed within the
     * user interface and must never be <code>null</code>.
     * 
     * @return the version of this SDK.
     */
    public abstract Version getVersion();

    /**
     * Return a boolean indicating whether or not this {@link ISDK} instances
     * is persistable.  If it is persistable, it should write out its information
     * into the provided {@link IPersistenceProvider}.
     * @return
     */
    public abstract boolean isPersistent();

    /**
     * Load the state of this object using the specified persistence state
     * information.
     * 
     * @param persistenceProvider the {@link IPersistenceProvider}
     *            implementation that provides the facilities for storing and
     *            retrieving persistable objects.
     * @throws PersistenceException if any error occur while loading the
     *             persisted information.
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException;

    /**
     * Save the state of this object using the specified persistence state
     * information.
     * 
     * @param persistenceProvider the {@link IPersistenceProvider}
     *            implementation that provides the facilities for storing and
     *            retrieving persistable objects.
     * @throws PersistenceException if any error occur while saving the
     *             persistable information.
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException;
}