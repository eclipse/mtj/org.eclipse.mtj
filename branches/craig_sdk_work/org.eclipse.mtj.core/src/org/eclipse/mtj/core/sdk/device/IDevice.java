/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.core.sdk.device;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.launching.LaunchEnvironment;
import org.eclipse.mtj.core.persistence.IPersistable;
import org.eclipse.mtj.core.sdk.ISDK;
import org.eclipse.mtj.core.symbol.ISymbolSet;
import org.eclipse.mtj.core.symbol.ISymbolSetFactory;

/**
 * The device interface specifies the representation of an emulated device. Each
 * instance of a SDK must provide a list of emulated devices that implement this
 * interface.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface IDevice extends IPersistable {

    /**
     * Return the deviceClasspath provided by the device.
     * <p>
     * This deviceClasspath includes all libraries for the device including
     * configuration and profile libraries.
     * </p>
     * 
     * @return the IDeviceClasspath instance that represents the classpath
     *         provided by the device.
     */
    public IDeviceClasspath getClasspath();

    /**
     * Return the displayable description of this device.
     * <p>
     * This description will be displayed within the user interface. If this
     * method returns a <code>null</code> value, the device's name will be used
     * as the description instead.
     * </p>
     * 
     * @return the string containing the description of this device or
     *         <code>null</code> if the device's name should be used instead.
     */
    public String getDescription();

    /**
     * Return the unique identifier for this particular device instance.  This identifier
     * will not be displayed to the user, but will be used to manage the device.
     * 
     * @return
     */
    public String getIdentifier();
    
    /**
     * Return the SymbolSet associated with this device. The SymbolSet should be
     * based in the devices properties.
     * <p>
     * Clients may use {@link MTJCore#getSymbolSetFactory()} to get a
     * {@link ISymbolSetFactory} instance which provides a mechanism to create
     * symbols sets.
     * </p>
     * 
     * @return the SymbolSet associated with this device.
     */
    public ISymbolSet getSymbolSet();

    /**
     * Return the name of the SDK that contains this device belongs. This method
     * must never return a <code>null</code> value as the SDK name.
     * <p>
     * This value will be used to group devices belong to the same SDK.
     * </p>
     * 
     * @return the non-<code>null</code> SDK name associated to this device.
     */
    public String getSDKName();

    /**
     * Return the command-line arguments for launching this device given the
     * specified launch environment.
     * 
     * @param launchEnvironment the launch environment instance that provides
     *            the necessary information to a IDevice implementation for
     *            determining the correct command-line for execution of an
     *            emulator.
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting is not desired.
     * @return a String containing the command-line arguments for launching the
     *         {@link IDevice} implementation.
     * @throws CoreException If fails to determine the launch command line for
     *             the {@link IDevice} implementation.
     */
    public String getLaunchCommand(LaunchEnvironment launchEnvironment,
            IProgressMonitor monitor) throws CoreException;

    /**
     * Return the name of this device. This name will be used when interacting
     * with the emulator. This name may or may not be displayed within the user
     * interface, dependent on whether a display name is provided by this
     * device. This method must never return <code>null</code>.
     * 
     * @return the non-null name of this device.
     */
    public String getName();

    /**
     * Return the SDK that manages this device.  The SDK will be responsible
     * for persisting the instance or choosing not to persist.
     *  
     * @return
     */
    public ISDK getSDK();
    
    /**
     * Return a boolean describing whether this device wants to act as a debug
     * server rather than attaching to the debugger as a client.
     * 
     * @return if the device acts as a debug server
     */
    public boolean isDebugServer();
    
    /**
     * Return a boolean indicating whether or not the definition of this device
     * is editable in some way.  Certain SDK's may not allow for devices to be edited.
     * 
     * @return
     */
    public boolean isEditable();
    
    /**
     * Set the name of this device. This name will be used when interacting with
     * the emulator. This name may or may not be displayed within the user
     * interface, dependent on whether a display name is provided by this
     * device. This method must never return <code>null</code>.
     * 
     * @param name the non-null name of this device.
     */
    public void setName(String name);
}
