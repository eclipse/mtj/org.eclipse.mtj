/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 *     David Marques (Motorola) - Removing noimplement tag.
 */
package org.eclipse.mtj.internal.core.hook.sourceMapper;

import org.eclipse.core.resources.IFile;

/**
 * Services implementing this interface are capable of mapping a source file
 * resource to another source file resource.
 * 
 * @author Craig Setera
 */
public interface SourceMapper {

    /**
     * Return a mapped resource or <code>null</code> if the original resource
     * should be used.
     * 
     * @param sourceResource
     * @return
     */
    IFile getMappedResource(IFile sourceFile);
}
