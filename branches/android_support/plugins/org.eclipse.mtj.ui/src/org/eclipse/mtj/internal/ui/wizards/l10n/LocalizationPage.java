/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Adding Wizard reference.                     
 */
package org.eclipse.mtj.internal.ui.wizards.l10n;

import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.wizard.IWizardContainer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPluginImages;
import org.eclipse.swt.widgets.Composite;

/**
 * LocalizationPage represents the page for localization configuration for a
 * specified project.
 */
public class LocalizationPage extends WizardPage {

    private IWizardContainer container;
    private LocalizationBlock block;
    private IJavaProject project;

    /**
     * Creates a LocalizationPage for the specified project.
     * 
     * @param javaProject target project.
     * @param wizardContainer container.
     */
    protected LocalizationPage(IJavaProject javaProject,
            IWizardContainer wizardContainer) {
        super(MTJUIMessages.LocalizationPage_LocalizationPage);

        this.container = wizardContainer;
        this.project = javaProject;
        this.setTitle(MTJUIMessages.LocalizationPage_PageTitle);
        this.setDescription(MTJUIMessages.LocalizationPage_PageDescription);
        this.setImageDescriptor(MTJUIPluginImages.DESC_ADD_L10N);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        this.block = new LocalizationBlock(project, this.container);
        this.block.createMainComposite(parent);
        this.setControl(this.block.getControl());
    }

    /**
     * Gets the selected source folder.
     * 
     * @return source folder.
     */
    public IResource getDestination() {
        return this.block.getDestination();
    }

    /**
     * Gets the selected package.
     * 
     * @return selected package.
     */
    public IJavaElement getPackage() {
        return this.block.getPackage();
    }

}
