/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.ui.console;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleFactory;
import org.eclipse.ui.console.IConsoleManager;

/**
 * A factory for the creation of a console instance that can be used to hold
 * builder output.
 * 
 * @author Craig Setera
 */
public class BuilderConsoleFactory implements IConsoleFactory {
    
    private IConsoleManager consoleManager = null;
    private BuilderConsole console = null;

    public BuilderConsoleFactory() {
        super();
        consoleManager = ConsolePlugin.getDefault().getConsoleManager();
    }

    /**
     * @see org.eclipse.ui.console.IConsoleFactory#openConsole()
     */
    public void openConsole() {
        if (console == null) {
            console = new BuilderConsole();
            consoleManager.addConsoles(new IConsole[] { console });
        }

        consoleManager.showConsoleView(console);
    }
}
