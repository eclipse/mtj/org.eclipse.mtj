/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME)   - Initial implementation
 *     Diego Sandin (Motorola)    - Refactoring package name to follow eclipse
 *                                  standards
 *     Feng(Marvin) Wang (Sybase) - Add validations. Ensure that user cannot
 *                                  create a MIDlet in a non-MIDlet project.
 *     Gang Ma (Sybase)           - Add validations to ensure that user cannot
 *     				    create a Non-MIDlet class.
 */
package org.eclipse.mtj.internal.ui.wizards.midlet.page;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.ui.wizards.NewTypeWizardPage;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.mtj.internal.ui.IMTJUIConstants;
import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.mtj.internal.ui.MTJUIPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * Wizard page for the creation of a new MIDlet subclass.
 * 
 * @author Craig Setera
 */
public class NewMidletWizardPage extends NewTypeWizardPage {

    /** The name this page is registered as within the wizard */
    public static final String PAGE_NAME = "NewMidletClass"; //$NON-NLS-1$

    // Dialog settings constants
    private static final String SETTINGS_ADD_TO_JAD = "addToJad"; //$NON-NLS-1$
    private static final String SETTINGS_CONSTRUCTORS = "constructors"; //$NON-NLS-1$
    private static final String SETTINGS_UNIMPLEMENTED = "unimplemented"; //$NON-NLS-1$

    // Controls
    private Button addToJadButton;
    private Button constructorsButton;
    private Button unimplementedButton;

    /**
     * Creates a new NewMidletWizardPage
     */
    public NewMidletWizardPage() {
        super(true, PAGE_NAME);

        setTitle(MTJUIMessages.NewMidletWizardPage_title);
        setDescription(MTJUIMessages.NewMidletWizardPage_description);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    public void createControl(Composite parent) {
        initializeDialogUnits(parent);

        Composite composite = new Composite(parent, SWT.NONE);

        int nColumns = 4;

        GridLayout layout = new GridLayout();
        layout.numColumns = nColumns;
        composite.setLayout(layout);

        // pick & choose the wanted UI components

        createContainerControls(composite, nColumns);
        createPackageControls(composite, nColumns);
        createEnclosingTypeControls(composite, nColumns);

        createSeparator(composite, nColumns);

        createTypeNameControls(composite, nColumns);
        createModifierControls(composite, nColumns);

        createSuperClassControls(composite, nColumns);
        createSuperInterfacesControls(composite, nColumns);
        createMethodStubSelectionControls(composite, nColumns);
        createAddToJADSelectionControl(composite, nColumns);

        boolean addToJad = true;
        boolean constructors = true;
        boolean unimplemented = true;

        IDialogSettings section = getDialogSettings().getSection(PAGE_NAME);
        if (section != null) {
            addToJad = section.getBoolean(SETTINGS_ADD_TO_JAD);
            constructors = section.getBoolean(SETTINGS_CONSTRUCTORS);
            unimplemented = section.getBoolean(SETTINGS_UNIMPLEMENTED);
        }

        addToJadButton.setSelection(addToJad);
        constructorsButton.setSelection(constructors);
        unimplementedButton.setSelection(unimplemented);

        setControl(composite);

        Dialog.applyDialogFont(composite);
    }

    /**
     * The wizard owning this page is responsible for calling this method with
     * the current selection. The selection is used to initialize the fields of
     * the wizard page.
     * 
     * @param selection used to initialize the fields
     */
    public void init(IStructuredSelection selection) {
        IJavaElement jelem = getInitialJavaElement(selection);
        initContainerPage(jelem);
        initTypePage(jelem);
        setSuperClass(IMTJCoreConstants.MIDLET_SUPERCLASS, true);
    }

    /**
     * Return a boolean indicating whether the "add to jad" button was selected.
     * 
     * @return
     */
    public boolean isAddToJadSelected() {
        return addToJadButton.getSelection();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.ui.wizards.NewElementWizardPage#setVisible(boolean)
     */
    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (visible) {
            setFocus();
        }
    }

    /**
     * Create the "Add to JAD" selection control.
     * 
     * @param parent
     * @param numColumns
     */
    private void createAddToJADSelectionControl(Composite parent, int numColumns) {
        new Label(parent, SWT.NONE);
        Composite composite = new Composite(parent, SWT.NONE);

        GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        composite.setLayout(layout);

        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.horizontalSpan = numColumns - 1;
        composite.setLayoutData(gridData);

        addToJadButton = new Button(composite, SWT.CHECK);
        addToJadButton
                .setText(MTJUIMessages.NewMidletWizardPage_add_to_jad_btn_text);
    }

    /**
     * Create the controls for method stub selection.
     * 
     * @param composite
     */
    private void createMethodStubSelectionControls(Composite parent,
            int numColumns) {
        new Label(parent, SWT.NONE);
        Composite composite = new Composite(parent, SWT.NONE);

        GridLayout layout = new GridLayout();
        layout.numColumns = 1;
        composite.setLayout(layout);

        GridData gridData = new GridData();
        gridData.grabExcessHorizontalSpace = true;
        gridData.horizontalSpan = numColumns - 1;
        composite.setLayoutData(gridData);

        Label l = new Label(composite, SWT.NONE);
        l.setText(MTJUIMessages.NewMidletWizardPage_which_methods);

        constructorsButton = new Button(composite, SWT.CHECK);
        constructorsButton
                .setText(MTJUIMessages.NewMidletWizardPage_super_const);
        constructorsButton.setSelection(true);

        unimplementedButton = new Button(composite, SWT.CHECK);
        unimplementedButton
                .setText(MTJUIMessages.NewMidletWizardPage_unimplemented);
        unimplementedButton.setSelection(true);
    }

    private void doStatusUpdate() {
        // status of all used components
        IStatus[] status = new IStatus[] {
                fContainerStatus,
                isEnclosingTypeSelected() ? fEnclosingTypeStatus
                        : fPackageStatus, fTypeNameStatus, fModifierStatus,
                fSuperClassStatus, fSuperInterfacesStatus };
        // the mode severe status will be displayed and the OK button
        // enabled/disabled.
        updateStatus(status);
    }

    private IProject getContainedProject() {
        IProject prj = null;
        String str = getPackageFragmentRootText();
        IPath path = new Path(str);
        IResource res = getWorkspaceRoot().findMember(path);
        if (res != null) {
            prj = res.getProject();
        }
        return prj;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.ui.wizards.NewTypeWizardPage#containerChanged()
     */
    @Override
    protected IStatus containerChanged() {
        IStatus containerStatus = super.containerChanged();
        String str = getPackageFragmentRootText();
        IPath path = new Path(str);
        IResource res = getWorkspaceRoot().findMember(path);
        if (res != null) {
            int resType = res.getType();
            if ((resType == IResource.PROJECT) || (resType == IResource.FOLDER)) {
                IProject proj = res.getProject();
                try {
                    if (!proj.hasNature(IMTJCoreConstants.MTJ_NATURE_ID)) {
                        if (res.exists()) {
                            if (resType == IResource.PROJECT) {
                                return new Status(
                                        IStatus.ERROR,
                                        IMTJUIConstants.PLUGIN_ID,
                                        MTJUIMessages.NewMidletWizardPage_warning_NotAMidletProject);
                            } else {
                                return new Status(
                                        IStatus.ERROR,
                                        IMTJUIConstants.PLUGIN_ID,
                                        MTJUIMessages.NewMidletWizardPage_warning_NotInAMidletProject);
                            }
                        }
                    }
                } catch (CoreException e) {
                    return new Status(
                            IStatus.ERROR,
                            IMTJUIConstants.PLUGIN_ID,
                            MTJUIMessages.NewMidletWizardPage_warning_NotAMidletProject);
                }
            }
        }
        return containerStatus;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.ui.wizards.NewTypeWizardPage#createTypeMembers(org.eclipse.jdt.core.IType, org.eclipse.jdt.ui.wizards.NewTypeWizardPage.ImportsManager, org.eclipse.core.runtime.IProgressMonitor)
     */
    @Override
    protected void createTypeMembers(IType newType, ImportsManager imports,
            IProgressMonitor monitor) throws CoreException {
        createInheritedMethods(newType, constructorsButton.getSelection(),
                unimplementedButton.getSelection(), imports,
                new SubProgressMonitor(monitor, 1));

        IDialogSettings section = MTJUIPlugin.getDialogSettings(
                getDialogSettings(), PAGE_NAME);
        section.put(SETTINGS_ADD_TO_JAD, addToJadButton.getSelection());
        section.put(SETTINGS_CONSTRUCTORS, constructorsButton.getSelection());
        section.put(SETTINGS_UNIMPLEMENTED, unimplementedButton.getSelection());
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.ui.wizards.NewTypeWizardPage#handleFieldChanged(java.lang.String)
     */
    @Override
    protected void handleFieldChanged(String fieldName) {
        super.handleFieldChanged(fieldName);
        doStatusUpdate();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jdt.ui.wizards.NewTypeWizardPage#superClassChanged()
     */
    @Override
    protected IStatus superClassChanged() {
        IStatus superClassStatus = super.superClassChanged();
        boolean isMidlet = false;

        String sclassName = getSuperClass();
        IProject project = getContainedProject();
        IJavaProject javaProject = JavaCore.create(project);
        try {
            if (javaProject != null) {
                isMidlet = Utils.isMidlet(javaProject.findType(sclassName),
                        new NullProgressMonitor());
            }
        } catch (JavaModelException e) {
        }

        if (!isMidlet) {
            return new Status(
                    IStatus.ERROR,
                    IMTJUIConstants.PLUGIN_ID,
                    MTJUIMessages.NewMidletWizardPage_warning_super_must_be_midlet);
        }
        return superClassStatus;
    }
}
