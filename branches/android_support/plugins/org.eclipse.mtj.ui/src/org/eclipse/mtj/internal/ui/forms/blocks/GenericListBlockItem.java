/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.ui.forms.blocks;

import org.eclipse.swt.graphics.Image;

/**
 * GenericListBlockItem class provides an interface to
 * be implemented for classes that wish to be displayed 
 * on a GenericListBlock. 
 * <br>
 * By implementing this interface the block will know
 * what text and image to display for the items.
 * 
 * @author David Marques
 * @since 1.0
 */
public interface GenericListBlockItem {

	/**
	 * Gets the item's image.
	 * 
	 * @return image.
	 */
	public Image getImage();
	
	/**
	 * Gets the item's text.
	 * 
	 * @return text.
	 */
	public String getText();
	
}
