/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola)  - Initial version
 *     Diego Sandin (Motorola)  - Use Eclipse Message Bundles [Bug 255874]
 *     David Marques (Motorola) - Adding scan button and implementing buttons
 *     							  customization.
 *     David Marques (Motorola) - Adding setEnabled() method.
 */
package org.eclipse.mtj.internal.ui.forms.blocks;

import java.util.ArrayList;

import org.eclipse.mtj.internal.ui.MTJUIMessages;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * @author Diego Madruga Sandin
 */
public class ButtonBarBlock {

    /**
     * Add Button
     */
    public static final int BUTTON_ADD = 1 << 1;

    public static final int BUTTON_ADD_INDEX = 0;;

    /**
     * Button
     */
    public static final int BUTTON_DOWN = 1 << 4;

    public static final int BUTTON_DOWN_INDEX = 3;

    /**
     * Button
     */
    public static final int BUTTON_REMOVE = 1 << 2;

    public static final int BUTTON_REMOVE_INDEX = 1;

    /**
     * Button
     */
    public static final int BUTTON_UP = 1 << 3;

    public static final int BUTTON_UP_INDEX = 2;

    /**
     * Button
     */
    public static final int BUTTON_SCAN = 1 << 5;

    public static final int BUTTON_SCAN_INDEX = 4;
    
    /**
     * All buttons will be disabled
     */
    public static final int BUTTON_ALL = BUTTON_ADD | BUTTON_REMOVE | 
    	BUTTON_UP | BUTTON_DOWN | BUTTON_SCAN;
    
    private ArrayList<Button> buttons;

    /**
     * @param parent
     * @param toolkit
     * @param buttons
     */
    public ButtonBarBlock(Composite parent, FormToolkit toolkit,
            int buttons) {
        initialize(parent, toolkit, buttons);
    }

    /**
     * Creates an instance of the buttons block.
     * 
     * @param parent parent composite.
     * @param buttons enabled buttons.
     */
    public ButtonBarBlock(Composite parent, int buttons) {
        initialize(parent, buttons);
    }
    
    /**
     * @param button
     * @param eventType
     * @param listener
     */
    public void addButtonListener(int button, int eventType, Listener listener) {
        buttons.get(button).addListener(eventType, listener);
    }

    /**
     * @param index
     * @return
     */
    public Button getButton(int index) {
        return buttons.get(index);
    }

    /**
     * @param index
     * @return
     */
    public boolean isButtonEnabled(int index) {
        return buttons.get(index).isEnabled();
    }

    /**
     * @param index
     * @param listener
     */
    public void setButtonMouseListener(int index, MouseListener listener) {
        buttons.get(index).addMouseListener(listener);
    }

    /**
     * @param index
     * @param enabled
     */
    public void setEnabled(int index, boolean enabled) {
        buttons.get(index).setEnabled(enabled);
    }

    /**
     * @param parent
     * @param toolkit
     * @param buttons2 
     */
    private void initialize(Composite parent, FormToolkit toolkit, int _buttons) {

        buttons = new ArrayList<Button>(4);

        if ((_buttons & BUTTON_ADD) != 0) {
        	Button add = toolkit.createButton(parent,
        			MTJUIMessages.buttonBarBlock_button_add, SWT.PUSH);
        	add.setEnabled(false);
        	buttons.add(add);
        }
        if ((_buttons & BUTTON_REMOVE) != 0) {
        	Button remove = toolkit.createButton(parent,
        			MTJUIMessages.buttonBarBlock_button_remove, SWT.PUSH);
        	remove.setEnabled(false);
        	buttons.add(remove);
        }
        if ((_buttons & BUTTON_UP) != 0) {
            Button up = toolkit.createButton(parent,
            		MTJUIMessages.buttonBarBlock_button_up, SWT.PUSH);
            up.setEnabled(false);
            buttons.add(up);
        }
        if ((_buttons & BUTTON_DOWN) != 0) {
            Button down = toolkit.createButton(parent,
            		MTJUIMessages.buttonBarBlock_button_down, SWT.PUSH);
            down.setEnabled(false);
            buttons.add(down);
        }
        if ((_buttons & BUTTON_SCAN) != 0) {
            Button scan = toolkit.createButton(parent,
            		MTJUIMessages.ButtonBarBlock_Scan, SWT.PUSH);
            scan.setEnabled(false);
            buttons.add(scan);
        }
    }
    
    /**
     * @param parent
     * @param buttons2 
     * @param toolkit
     */
    private void initialize(Composite parent, int _buttons) {

        buttons = new ArrayList<Button>(4);

        if ((_buttons & BUTTON_ADD) != 0) {        	
        	Button add = new Button(parent, SWT.PUSH);
        	add.setText(MTJUIMessages.buttonBarBlock_button_add);
        	add.setEnabled(false);
        	buttons.add(add);
        }
        if ((_buttons & BUTTON_REMOVE) != 0) {        	
        	Button remove = new Button(parent, SWT.PUSH);
        	remove.setText(MTJUIMessages.buttonBarBlock_button_remove);
        	remove.setEnabled(false);
        	buttons.add(remove);
        }
        if ((_buttons & BUTTON_UP) != 0) {            
        	Button up = new Button(parent, SWT.PUSH);
        	up.setText(MTJUIMessages.buttonBarBlock_button_up);
        	up.setEnabled(false);
        	buttons.add(up);
        }
        if ((_buttons & BUTTON_DOWN) != 0) {            
        	Button down = new Button(parent, SWT.PUSH);
        	down.setText(MTJUIMessages.buttonBarBlock_button_down);
        	down.setEnabled(false);
        	buttons.add(down);
        }
        if ((_buttons & BUTTON_SCAN) != 0) {
            Button scan = new Button(parent, SWT.PUSH);
            scan.setText(MTJUIMessages.ButtonBarBlock_Scan);
            scan.setEnabled(false);
            buttons.add(scan);
        }
    }

	/**
	 * Sets all block child widgets enabled/disabled.
	 * 
	 * @param state true if enabled false otherwise.
	 */
	public void setEnabled(boolean state) {
		for (Button button : this.buttons) {
			button.setEnabled(state);
		}
	}
}
