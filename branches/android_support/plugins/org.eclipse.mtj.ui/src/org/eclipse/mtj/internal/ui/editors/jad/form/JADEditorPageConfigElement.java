/**
 * Copyright (c) 2003,2008 Sybase Corporation and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Gang Ma 		(Sybase) - Initial implementation 
 */
package org.eclipse.mtj.internal.ui.editors.jad.form;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.mtj.ui.editors.jad.AbstractJADEditorPage;

/**
 * class wrapped IConfigurationElement instance for jadEditorPages
 * extension-point extensions
 * 
 * @author Gang Ma
 */
public class JADEditorPageConfigElement {

    /**
     * 
     */
    private static final String JAD_EDITOR_PAGE_CLASS = "class";

    /**
     * 
     */
    private static final String ATTR_PRIORITY = "priority";

    /**
     * 
     */
    private IConfigurationElement element;

    /**
     * @param jadAttrElement
     */
    public JADEditorPageConfigElement(IConfigurationElement jadAttrElement) {
        this.element = jadAttrElement;
    }

    /**
     * @return
     * @throws CoreException
     */
    public AbstractJADEditorPage getJADEditorPage() throws CoreException {

        return (AbstractJADEditorPage) element
                .createExecutableExtension(JAD_EDITOR_PAGE_CLASS);

    }

    /**
     * Return the priority of this element.
     * 
     * @return
     */
    public int getPriority() {
        int priority = 60;
        try {
            priority = Integer.parseInt(element.getAttribute(ATTR_PRIORITY));
        } catch (NumberFormatException e) {
        }

        return priority;
    }
}
