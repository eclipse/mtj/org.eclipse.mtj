/**
 * Copyright (c) 2003,2009 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse
 *                                standards
 */
package org.eclipse.mtj.internal.ui;

import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * An extended preference page implementation for MTJ embedding.
 * 
 * @author Craig Setera
 */
public interface IEmbeddableWorkbenchPreferencePage extends
        IWorkbenchPreferencePage {
    /**
     * Performs special processing when this page's Defaults button has been
     * pressed.
     * <p>
     * This is a framework hook method for subclasses to do special things when
     * the Defaults button has been pressed. Subclasses may override, but should
     * call <code>super.performDefaults</code>.
     * </p>
     */
    public void performDefaults();

    /**
     * Performs special processing when this page's Apply button has been
     * pressed.
     * <p>
     * This is a framework hook method for sublcasses to do special things when
     * the Apply button has been pressed. The default implementation of this
     * framework method simply calls <code>performOk</code> to simulate the
     * pressing of the page's OK button.
     * </p>
     * 
     * @see #performOk
     */
    public void performApply();
}
