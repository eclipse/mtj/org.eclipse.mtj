/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.ui.editors.l10n;

import org.eclipse.mtj.internal.ui.editor.MTJFormTextEditorContributor;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public class L10nEditorContributor extends MTJFormTextEditorContributor {

    /**
     * @param menuName
     */
    public L10nEditorContributor() {
        super("Localization Data Editor"); //$NON-NLS-1$
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.internal.ui.editor.MTJFormTextEditorContributor#supportsHyperlinking()
     */
    @Override
    public boolean supportsHyperlinking() {
        return true;
    }
}
