/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.internal.core.externallibrary.model;

/**
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public enum Visibility {
    
    /**
     * Will be available to all users.
     */
    PUBLIC, 
    
    /**
     * Will be available only for internal use
     */
    INTERNAL, 
    
    /**
     * An invalid visibility
     */
    INVALID;

    /**
     * Gets a Visibility element that represents the visibility argument.
     * 
     * @param visibility a string that represents the visibility.
     * @return the Visibility element.
     */
    public static Visibility getFromString(final String visibility) {

        if (visibility == null) {
            return INVALID;
        }

        if (visibility.equals(PUBLIC.toString())) {
            return PUBLIC;
        } else if (visibility.equals(INTERNAL.toString())) {
            return INTERNAL;
        } else {
            return INVALID;
        }
    }
}
