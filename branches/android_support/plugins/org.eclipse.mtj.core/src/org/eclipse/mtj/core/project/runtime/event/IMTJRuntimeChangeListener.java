/**
 * Copyright (c) 2008, 2009 Sybase Inc. and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Feng Wang (Sybase)          - Initial implementation
 *     Gustavo de Paula (Motorola) - Configuration refactory [Bug 265522]
 *     Diego Sandin (Motorola)     - Enhanced javadoc [bug 270532]
 */
package org.eclipse.mtj.core.project.runtime.event;

import org.eclipse.mtj.core.project.runtime.MTJRuntime;

/**
 * Classes that implement this interface provide methods that deal with the
 * events that are generated when the properties of a {@link MTJRuntime} change.
 * <p>
 * All Events are constructed with a reference to the object, the "source", that
 * is logically deemed to be the object upon which the Event in question
 * initially occurred upon.
 * </p>
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface IMTJRuntimeChangeListener {

    /**
     * Sent when the
     * {@link MTJRuntime#setDevice(org.eclipse.mtj.core.sdk.device.IDevice)}
     * method is invoked.
     * 
     * @param event an MTJRuntimeDeviceChangeEvent object containing the device
     *            information.
     */
    void deviceChanged(MTJRuntimeDeviceChangeEvent event);

    /**
     * Sent when the {@link MTJRuntime#setName(String)} method is invoked.
     * 
     * @param event an MTJRuntimeNameChangeEvent object containing the name
     *            information.
     */
    void nameChanged(MTJRuntimeNameChangeEvent event);

    /**
     * Sent when the {@link MTJRuntime#fireSymbolSetChanged()} method is
     * invoked.
     */
    void symbolSetChanged();

    /**
     * Sent when the
     * {@link MTJRuntime#setWorkspaceScopeSymbolSets(java.util.List)} method is
     * invoked.
     * 
     * @param event an MTJRuntimeWorkspaceSymbolSetsChangeEvent object
     *            containing the workspace SymbolSet information.
     */
    void workspaceScopeSymbolSetsChanged(
            MTJRuntimeWorkspaceSymbolSetsChangeEvent event);
}
