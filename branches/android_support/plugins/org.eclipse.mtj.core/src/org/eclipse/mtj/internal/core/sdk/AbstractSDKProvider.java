/**
 * Copyright (c) 2009 Research In Motion Limited.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Jon Dearden (Research In Motion) - Initial Version
 */
package org.eclipse.mtj.internal.core.sdk;

import java.text.Collator;

import org.eclipse.mtj.core.sdk.ISDKProvider;
import org.eclipse.mtj.internal.core.util.Utils;
import org.eclipse.swt.graphics.Image;

/**
 * Abstract implementation of an ISDKProvider. This class provides a default
 * compareTo() method used for sorting in the Device Management dialog and an
 * empty name and description.
 * 
 * @Since 1.1
 */
public abstract class AbstractSDKProvider implements ISDKProvider {
    
    protected String name = Utils.EMPTY_STRING;
    
    public String getName() {
        return name;
    }

    public String getDescription() {
        return name;
    }

    public int compareTo(ISDKProvider other) {
        if (other == null) 
            throw new NullPointerException();
        if (other == this)
            return 0;
        return Collator.getInstance().compare(this.getName(), other.getName());
    }

    /**
     * Returning null will cause the DeviceManagementPreferencePage to present a
     * default file folder image for the SDK provider.
     */
    public Image getLogo() {
        return null;
    }

}
