/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.build.export.states;

import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.mtj.core.project.midp.IMidletSuiteProject;
import org.eclipse.mtj.core.project.runtime.MTJRuntime;
import org.eclipse.mtj.internal.core.build.export.AntennaBuildExport;
import org.eclipse.mtj.internal.core.build.export.AntennaExportException;
import org.eclipse.mtj.internal.core.statemachine.StateMachine;
import org.eclipse.mtj.internal.core.util.xml.XMLUtils;
import org.eclipse.osgi.util.NLS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * CreateAntInitTaskState initializes the directories used
 * dusring the build process.
 * 
 * @author David Marques
 * @since 1.0
 */
public class CreateAntInitTaskState extends AbstractCreateAntTaskState {

	/**
	 * Creates a {@link CreateAntInitTaskState} instance bound to the
	 * specified state machine in order to create init target for the
	 * specified project.
	 * 
	 * @param machine bound {@link StateMachine} instance.
	 * @param project target {@link IMidletSuiteProject} instance.
	 * @param _document target {@link Document}.
	 */
	public CreateAntInitTaskState(StateMachine _stateMachine,
			IMidletSuiteProject _suiteProject, Document _document) {
		super(_stateMachine, _suiteProject, _document);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.mtj.internal.core.build.export.states.AbstractCreateAntTaskState#onEnter(org.eclipse.mtj.core.project.runtime.MTJRuntime)
	 */
	protected void onEnter(MTJRuntime runtime) throws AntennaExportException {
		Document document = getDocument();
		Element root = document.getDocumentElement();

		IProject project = getMidletSuiteProject().getProject();
		Set<IProject> requiredProjects = getRequiredProjects(project);
		
		String configName = getFormatedName(runtime.getName());
		Element init = XMLUtils.createTargetElement(document, root, NLS
				.bind("init-{0}", configName), getDependencies(runtime)); //$NON-NLS-1$
		for (String dir : getTargetProjectDirectories(
				getMidletSuiteProject().getProject(), configName)) {
			Element mkdir = document.createElement("mkdir"); //$NON-NLS-1$
			init.appendChild(mkdir);
			mkdir.setAttribute("dir", dir); //$NON-NLS-1$
		}

		for (IProject required : requiredProjects) {
			for (String dir : getRequiredProjectDirectories(required,
					configName)) {
				Element mkdir = document.createElement("mkdir"); //$NON-NLS-1$
				init.appendChild(mkdir);
				mkdir.setAttribute("dir", dir); //$NON-NLS-1$
			}
		}
	}

	/**
	 * Gets the directories to be created for the required project.  
	 * 
	 * @param _project required project.
	 * @param configName runtime configuration.
	 * @return an array with the directories paths.
	 */
	private String[] getRequiredProjectDirectories(IProject _project,
			String configName) {
		String projectName = getFormatedName(_project.getName());
		String values[] = new String[] { AntennaBuildExport.BUILD_FOLDER, configName, projectName };
		return new String[] { NLS.bind("{0}/{1}/{2}/", values), //$NON-NLS-1$
				NLS.bind("deployed/{0}/", configName), //$NON-NLS-1$
				NLS.bind("{0}/{1}/{2}/classes/", values), //$NON-NLS-1$
				NLS.bind("{0}/{1}/{2}/resources/", values) }; //$NON-NLS-1$
	}

	/**
	 * Gets the directories to be created for the target project.  
	 * 
	 * @param _project target project.
	 * @param configName runtime configuration.
	 * @return an array with the directories paths.
	 */
	private String[] getTargetProjectDirectories(IProject _project,
			String configName) {
		String projectName = getFormatedName(_project.getName());
		String values[] = new String[] { AntennaBuildExport.BUILD_FOLDER, configName, projectName };
		return new String[] { NLS.bind("{0}/{1}/{2}/", values), //$NON-NLS-1$
				NLS.bind("deployed/{0}/", configName), //$NON-NLS-1$
				NLS.bind("{0}/{1}/{2}/classes/", values), //$NON-NLS-1$
				NLS.bind("{0}/{1}/{2}/resources/", values), //$NON-NLS-1$
				NLS.bind("{0}/{1}/{2}/bin/", values) }; //$NON-NLS-1$
	}
}
