/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     Rafael Amaral (Motorola) - Adding hookId
 */
package org.eclipse.mtj.internal.core.build;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.mtj.core.build.IMTJBuildHook;

/**
 * BuildHookInfo class wraps an extension of the
 * mtjbuildhook extension point.
 * 
 * @author David Marques
 */
public class BuildHookInfo {

    private String hookId;
    private IConfigurationElement element;
    private IMTJBuildHook hook;

    /**
	 * Creates an instance of a BuildHookInfo associated
	 * to the specified extension.
     * 
     * @param _element extension element.
     */
    public BuildHookInfo(IConfigurationElement _element) {
        this.element = _element;
        this.hookId = this.element.getAttribute("hook");
    }

    /**
     * Gets the hook instance.
     * 
     * @return the hook.
     * @throws CoreException If any core error occurred.
     */
    public IMTJBuildHook getHook() throws CoreException {
        if (this.hook == null) {
            this.hook = (IMTJBuildHook) this.element
                    .createExecutableExtension("hook");
        }
        return this.hook;
    }
    
    /**
     * Gets the hook info id
     * 
     * @return the hookId
     */
    public String getHookId() {
        return hookId;
    }
}
