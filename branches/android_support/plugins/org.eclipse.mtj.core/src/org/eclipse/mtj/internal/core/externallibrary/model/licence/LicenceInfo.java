/**
 * Copyright (c) 2008 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.externallibrary.model.licence;

import java.net.URI;

/**
 * This Class represents the licensing information available in a library.
 * 
 * @author Diego Madruga Sandin
 * @since 0.9.1
 */
public final class LicenceInfo {

    /**
     * The license name
     */
    private String name;

    /**
     * Additional license information can be found in this URI
     */
    private URI uri;

    /**
     * Create a new LicenceInfo
     * 
     * @param name the license name
     * @param uri an URI to a remote location were additional license information
     *            can be found
     */
    public LicenceInfo(String name, URI uri) {
        super();
        this.name = name;
        this.uri = uri;
    }

    /**
     * Return the license name.
     * 
     * @return the license name.
     */
    public String getName() {
        return name;
    }

    /**
     * Return the URI were additional license information can be found.
     * 
     * @return the additional license information URI.
     */
    public URI getUri() {
        return uri;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LicenceInfo other = (LicenceInfo) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (uri == null) {
            if (other.uri != null) {
                return false;
            }
        } else if (!uri.equals(other.uri)) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((uri == null) ? 0 : uri.hashCode());
        return result;
    }

}
