/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Diego Sandin (Motorola) - Initial Version
 */
package org.eclipse.mtj.core.project;

/**
 * Classes that implement this interface provide methods that deal with the
 * events that are generated when an IMTJProject change state or a deployable
 * package is created.
 * <p>
 * <strong>EXPERIMENTAL</strong>. This class or interface has been added as part
 * of a work in progress. There is no guarantee that this API will work or that
 * it will remain the same. Please do not use this API without consulting with
 * the MTJ team.
 * </p>
 * 
 * @since 1.0
 */
public interface IMTJProjectListener {

    /**
     * Sent when the {@link IMTJProject#saveMetaData()} is invoked.
     */
    public abstract void metaDataSaved();

    /**
     * Sent when the
     * {@link IMTJProject#createPackage(boolean, boolean, org.eclipse.core.runtime.IProgressMonitor)}
     * is invoked.
     */
    public abstract void packageCreated();

    /**
     * Sent when the
     * {@link IMTJProject#setSignatureProperties(org.eclipse.mtj.core.build.sign.ISignatureProperties)}
     * is invoked.
     */
    public abstract void signaturePropertiesChanged();

    /**
     * Sent when the
     * {@link IMTJProject#refreshClasspath(org.eclipse.core.runtime.IProgressMonitor)}
     * is invoked.
     */
    public abstract void classpathRefreshed();

}