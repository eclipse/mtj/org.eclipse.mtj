package org.eclipse.mtj.internal.core.util.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This adapter provides a way for a visitor to be accepted by a document.
 * 
 * @author David Marques
 */
public class DocumentAdapter {

    private Document document;

    /**
     * Creates an adapter for the specified document.
     * 
     * @param document target document.
     */
    public DocumentAdapter(Document document) {
        if (document == null) {
            throw new IllegalArgumentException("Document can not be null.");
        }
        this.document = document;
    }

    /**
     * Accepts the visitor do visit the document.
     * 
     * @param visitor implementation.
     */
    public void accept(DocumentVisitor visitor) {
        Element root = this.document.getDocumentElement();
        visitor.visitElement(root);

        NodeList nodes = root.getChildNodes();
        if (nodes.getLength() > 0x00) {
            accept(nodes, visitor);
        }
    }

    /**
     * Visits the nodes inside the node list.
     * 
     * @param nodes node list.
     * @param visitor implementation.
     */
    private void accept(NodeList nodes, DocumentVisitor visitor) {
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            switch (node.getNodeType()) {
                case Node.ELEMENT_NODE:
                    visitor.visitElement((Element) node);
                    break;
            }

            NodeList children = node.getChildNodes();
            if (children.getLength() > 0x00) {
                accept(children, visitor);
            }
        }
    }

}
