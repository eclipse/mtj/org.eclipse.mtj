/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 */
package org.eclipse.mtj.internal.core.build;

/**
 *  MTJBuildPropertiesChangeListener interface defines a set
 *  of callback methods for classes that wish to be notified
 *  upon changes on a {@link MTJBuildProperties} instance.
 *  
 *  @author David Marques
 */
public interface MTJBuildPropertiesChangeListener {

	public void propertiesChanged(MTJBuildProperties buildProperties);
	
}
