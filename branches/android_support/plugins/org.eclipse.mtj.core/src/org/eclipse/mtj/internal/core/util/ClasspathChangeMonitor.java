/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Rafael Amaral (Motorola) - Initial version
 *     David Marques (Motorola) - Fixing classpath file verification.
 */

package org.eclipse.mtj.internal.core.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mtj.core.MTJCore;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.internal.core.IMTJCoreConstants;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * ClasspathChangeMonitor class provides a monitor 
 * to classpath changes of MTJ Projects
 */
public class ClasspathChangeMonitor implements IResourceChangeListener {

    private static ClasspathChangeMonitor instance;

    private Map<IMTJProject, List<IClasspathChangeListener>> map;
    
    /**
     * Returns the single instance of this class
     * 
     * @return instance
     */    
    public static ClasspathChangeMonitor getInstance() {
        if (instance == null) {
            instance = new ClasspathChangeMonitor();
        }
        return instance;
    }

    private ClasspathChangeMonitor() {
	this.map = new HashMap<IMTJProject, List<IClasspathChangeListener>>();
    }

    /**
     * Adds a listener to be notified when classpath changes
     * 
     * @param project - project associated with the listener
     * @param classpathChangeListener - listener to be notified
     */
    public void addClasspathChangeListener(IMTJProject project, IClasspathChangeListener classpathChangeListener){
	List<IClasspathChangeListener> listeners = this.map.get(project);
	if(listeners == null){
	    listeners = new ArrayList<IClasspathChangeListener>();    
	    this.map.put(project, listeners);
	}
	listeners.add(classpathChangeListener);
    }
    
    /**
     * Removes a listener from this monitor
     * 
     * @param project - project associated with the listener
     * @param classpathChangeListener - listener to be removed
     */
    public void removeClasspathChangeListener(IMTJProject project, IClasspathChangeListener classpathChangeListener){
	List<IClasspathChangeListener> listeners = this.map.get(project);
	if(listeners != null){
	    listeners.remove(classpathChangeListener);
	}
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IResourceChangeListener#resourceChanged(org.eclipse.core.resources.IResourceChangeEvent)
     */
    public void resourceChanged(IResourceChangeEvent event) {
        
        IResourceDelta delta = event.getDelta();
        if(delta == null)
            return;  
        
        ClassPathVisitor visitor = new ClassPathVisitor();
        try {
            delta.accept(visitor);
        } catch (CoreException e) {
            MTJLogger.log(IStatus.ERROR, e);
        }
    }
    
    /**
     * Visits resource deltas looking for the changes on classpath
     * of MTJ projects
     */
    class ClassPathVisitor implements IResourceDeltaVisitor {

        /* (non-Javadoc)
         * @see org.eclipse.core.resources.IResourceDeltaVisitor#visit(org.eclipse.core.resources.IResourceDelta)
         */
        public boolean visit(IResourceDelta delta) throws CoreException {
            boolean result = true;

			IResource resource = delta.getResource();
			try {
				if (resource.getProjectRelativePath().toString().equals(
						".classpath")
						&& delta.getKind() == IResourceDelta.CHANGED) {
					IProject project = resource.getProject();
					if (project != null
							&& project
									.hasNature(IMTJCoreConstants.MTJ_NATURE_ID)) {

						IMTJProject mtjProject = MTJCore.geMTJProject(project);
						List<IClasspathChangeListener> list = map
								.get(mtjProject);
						if (list != null) {
							for (IClasspathChangeListener iClasspathChangeListener : list) {
								iClasspathChangeListener.classpathChanged();
							}
						}
						result = false;
					}
				}
			} catch (CoreException e) {
				MTJLogger.log(IStatus.ERROR, e);
			}

			return result;
        }
    }

}