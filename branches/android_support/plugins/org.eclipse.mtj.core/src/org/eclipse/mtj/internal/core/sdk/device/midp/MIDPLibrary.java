/**
 * Copyright (c) 2003,2008 Craig Setera and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Craig Setera (EclipseME) - Initial implementation
 *     Diego Sandin (Motorola)  - Refactoring package name to follow eclipse 
 *                                standards
 */
package org.eclipse.mtj.internal.core.sdk.device.midp;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.jdt.core.IClasspathAttribute;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.persistence.IPersistenceProvider;
import org.eclipse.mtj.core.persistence.PersistenceException;
import org.eclipse.mtj.core.sdk.device.IAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPAPI;
import org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary;
import org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType;
import org.eclipse.mtj.internal.core.util.log.MTJLogger;

/**
 * A library is a wrapper around a classpath item that attempts to provide
 * further metadata about that library.
 * 
 * @author Craig Setera
 */
public class MIDPLibrary implements IMIDPLibrary {

    private static List<IAccessRule> NO_ACCESS_RULES = new ArrayList<IAccessRule>(
            0);

    private List<IMIDPAPI> apis;
    private URL javadocURL;
    private File libraryFile;
    private IPath sourceAttachmentPath;
    private IPath sourceAttachmentRootPath;
    private List<IAccessRule> accessRules;

    /**
     * Construct a new library.
     */
    public MIDPLibrary() {
        super();
        accessRules = NO_ACCESS_RULES;
    }

    /**
     * Creates a new instance of MIDPLibrary.
     * 
     * @param apis
     * @param javadocURL
     * @param libraryFile
     * @param sourceAttachmentPath
     * @param sourceAttachmentRootPath
     * @param accessRules
     */
    public MIDPLibrary(List<IMIDPAPI> apis, URL javadocURL, File libraryFile,
            IPath sourceAttachmentPath, IPath sourceAttachmentRootPath,
            List<IAccessRule> accessRules) {
        super();
        this.apis = apis;
        this.javadocURL = javadocURL;
        this.libraryFile = libraryFile;
        this.sourceAttachmentPath = sourceAttachmentPath;
        this.sourceAttachmentRootPath = sourceAttachmentRootPath;
        this.accessRules = accessRules;
    }

    /**
     * Test the equality of this library as compared to the specified library.
     * 
     * @param library
     * @return
     */
    public boolean equals(MIDPLibrary library) {
        return libraryFile.equals(library.libraryFile);
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        boolean equals = false;

        if (obj instanceof MIDPLibrary) {
            equals = equals((MIDPLibrary) obj);
        }

        return equals;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary#getAPI(org.eclipse.mtj.core.sdk.device.midp.MIDPAPIType)
     */
    public IMIDPAPI getAPI(MIDPAPIType apiType) {
        IMIDPAPI result = null;

        for (IAPI api : apis) {
            IMIDPAPI temp = (IMIDPAPI) api;
            if (temp.getType() == apiType) {
                result = temp;
                break;
            }
        }

        return result;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibrary#getAPIs()
     */
    public List<? extends IAPI> getAPIs() {
        return apis;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary#getConfiguration()
     */
    public IMIDPAPI getConfiguration() {
        return getAPI(MIDPAPIType.CONFIGURATION);
    }

    /**
     * @return Returns the javadocURL.
     */
    public URL getJavadocURL() {
        return javadocURL;
    }

    /**
     * Return the file that makes up this library.
     * 
     * @return Returns the libraryFile.
     */
    public File getLibraryFile() {
        return libraryFile;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary#getProfile()
     */
    public IMIDPAPI getProfile() {
        return getAPI(MIDPAPIType.PROFILE);
    }

    /**
     * @return Returns the sourceAttachmentPath.
     */
    public IPath getSourceAttachmentPath() {
        return sourceAttachmentPath;
    }

    /**
     * @return Returns the sourceAttachmentRootPath.
     */
    public IPath getSourceAttachmentRootPath() {
        return sourceAttachmentRootPath;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary#hasConfiguration()
     */
    public boolean hasConfiguration() {
        return (getConfiguration() != null);
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return libraryFile.hashCode();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.midp.IMIDPLibrary#hasProfile()
     */
    public boolean hasProfile() {
        return (getProfile() != null);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#loadUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void loadUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        libraryFile = new File(persistenceProvider.loadString("file"));

        int apiLength = persistenceProvider.loadInteger("apiCount");
        apis = new ArrayList<IMIDPAPI>();
        for (int j = 0; j < apiLength; j++) {
            IMIDPAPI midpapi = (IMIDPAPI) persistenceProvider
                    .loadPersistable("entry" + j);
            apis.add(midpapi);
        }

        String javadocURLString = persistenceProvider.loadString("javadocURL");
        if (javadocURLString != null) {
            try {
                javadocURL = new URL(javadocURLString);
            } catch (MalformedURLException e) {
                MTJLogger.log(IStatus.WARNING, "Error loading javadoc url "
                        + javadocURLString, e);
            }
        }

        String sourceAttachString = persistenceProvider
                .loadString("sourceAttachmentPath");
        if (sourceAttachString != null) {
            sourceAttachmentPath = new Path(sourceAttachString);
        }

        String rootPathString = persistenceProvider
                .loadString("sourceAttachmentRootPath");
        if (rootPathString != null) {
            sourceAttachmentRootPath = new Path(rootPathString);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibrary#setAccessRules(java.util.List)
     */
    public void setAccessRules(List<IAccessRule> accessRules) {
        this.accessRules = accessRules;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibrary#setApis(java.util.List)
     */
    @SuppressWarnings("unchecked")
    public void setApis(List<? extends IAPI> apis) {
        this.apis = (List<IMIDPAPI>) apis;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibrary#setJavadocURL(java.net.URL)
     */
    public void setJavadocURL(URL javadocURL) {
        this.javadocURL = javadocURL;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibrary#setLibraryFile(java.io.File)
     */
    public void setLibraryFile(File libraryFile) {
        this.libraryFile = libraryFile;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibrary#setSourceAttachmentPath(org.eclipse.core.runtime.IPath)
     */
    public void setSourceAttachmentPath(IPath sourceAttachmentPath) {
        this.sourceAttachmentPath = sourceAttachmentPath;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibrary#setSourceAttachmentRootPath(org.eclipse.core.runtime.IPath)
     */
    public void setSourceAttachmentRootPath(IPath sourceAttachmentRootPath) {
        this.sourceAttachmentRootPath = sourceAttachmentRootPath;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.persistence.IPersistable#storeUsing(org.eclipse.mtj.core.persistence.IPersistenceProvider)
     */
    public void storeUsing(IPersistenceProvider persistenceProvider)
            throws PersistenceException {
        persistenceProvider.storeString("file", libraryFile.toString());
        persistenceProvider.storeInteger("apiCount", apis.size());
        int i = 0;
        for (IAPI api : apis) {
            persistenceProvider.storePersistable("entry" + i++, api);
        }

        if (javadocURL != null) {
            persistenceProvider
                    .storeString("javadocURL", javadocURL.toString());
        }

        if (sourceAttachmentPath != null) {
            persistenceProvider.storeString("sourceAttachmentPath",
                    sourceAttachmentPath.toString());
        }

        if (sourceAttachmentRootPath != null) {
            persistenceProvider.storeString("sourceAttachmentRootPath",
                    sourceAttachmentRootPath.toString());
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibrary#toClasspathEntry()
     */
    public IClasspathEntry toClasspathEntry() {

        IPath entryPath = new Path(libraryFile.toString());
        IClasspathAttribute[] attributes = getClasspathAttributes();

        return JavaCore.newLibraryEntry(entryPath, sourceAttachmentPath,
                sourceAttachmentRootPath,
                (accessRules == null) ? NO_ACCESS_RULES
                        .toArray(new IAccessRule[0]) : accessRules
                        .toArray(new IAccessRule[accessRules.size()]),
                attributes, false);
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibrary#toFile()
     */
    public File toFile() {
        return libraryFile;
    }

    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return libraryFile.toString();
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibrary#toURL()
     */
    public URL toURL() {
        URL url = null;

        try {
            url = libraryFile.toURI().toURL();
        } catch (MalformedURLException e) {
            // This should not happen
            MTJLogger.log(IStatus.ERROR, e);
        }

        return url;
    }

    /**
     * Return the classpath attributes to be used in constructing the
     * IClasspathEntry.
     * 
     * @return
     */
    private IClasspathAttribute[] getClasspathAttributes() {
        ArrayList<IClasspathAttribute> attributes = new ArrayList<IClasspathAttribute>();

        IClasspathAttribute javadocAttribute = getJavadocAttribute();
        if (javadocAttribute != null)
            attributes.add(javadocAttribute);

        return (IClasspathAttribute[]) attributes
                .toArray(new IClasspathAttribute[attributes.size()]);
    }

    /**
     * Return a classpath attribute specifying the javadoc attachment.
     * 
     * @return
     */
    private IClasspathAttribute getJavadocAttribute() {
        IClasspathAttribute attribute = null;

        if (javadocURL != null) {
            attribute = JavaCore.newClasspathAttribute(
                    IClasspathAttribute.JAVADOC_LOCATION_ATTRIBUTE_NAME,
                    javadocURL.toString());
        }

        return attribute;
    }

    /* (non-Javadoc)
     * @see org.eclipse.mtj.core.sdk.device.ILibrary#getAccessRules()
     */
    public List<IAccessRule> getAccessRules() {
        return accessRules;
    }
}
