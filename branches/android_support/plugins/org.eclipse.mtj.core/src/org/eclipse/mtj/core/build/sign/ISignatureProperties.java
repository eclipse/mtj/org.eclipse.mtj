/**
 * Copyright (c) 2004, 2009 Kevin Hunter and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Kevin Hunter (Individual) - Initial implementation
 *     Craig Setera (EclipseME)  - Incorporated code in EclipseME
 *     Diego Sandin (Motorola)   - Refactoring package name to follow eclipse 
 *                                 standards
 *     David Marques (Motorola)  - Adding projectSpecific getter/setter.
 */
package org.eclipse.mtj.core.build.sign;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;

/**
 * Classes implementing this interface are designed to hold the various
 * project-specific properties relating to potential signing operations.
 * <p>
 * It is mostly used by the UI to allow the user to manipulate the settings, as
 * well as the project properties persistence.
 * 
 * @noimplement This interface is not intended to be implemented by clients.
 * @since 1.0
 */
public interface ISignatureProperties {

    /**
     * Password storage method indicating passwords should be stored in the
     * user's Eclipse keyring
     * 
     * @see #getPasswordStorageMethod()
     * @see #setPasswordStorageMethod(int)
     */
    public static final int PASSMETHOD_IN_KEYRING = 1;
    /**
     * Password storage method indicating passwords should be stored in the .mtj
     * metadata file in the project.
     * 
     * @see #getPasswordStorageMethod()
     * @see #setPasswordStorageMethod(int)
     */
    public static final int PASSMETHOD_IN_PROJECT = 2;
    /**
     * Password storage method indicating passwords should be requested as
     * required and not stored.
     * 
     * @see #getPasswordStorageMethod()
     * @see #setPasswordStorageMethod(int)
     */
    public static final int PASSMETHOD_PROMPT = 0;

    /**
     * String used to prefix project-relative paths in display strings. A
     * display string that starts with this prefix will be considered
     * project-relative. Display strings that do not start with this prefix will
     * be considered absolute.
     * 
     * @see #setKeyStoreDisplayPath(String)
     */
    public static final String PROJECT_RELATIVE_PREFIX = "$/"; //$NON-NLS-1$

    /**
     * Resets the class to its default values
     */
    public void clear();

    /**
     * Copy the values from another instance.
     * 
     * @param other <code>ISignatureProperties</code> object to be copied.
     */
    public void copy(ISignatureProperties other);

    /**
     * Returns the absolute file system path to the keystore file. The specified
     * <code>project</code> instance is used to convert a project-relative path
     * to an absolute path.
     * 
     * @param project <code>IProject</code> to which this
     *            <code>ISignatureProperties</code> belongs.
     * @return <code>String</code> containing absolute path to keystore file.
     * @see #setKeyStoreDisplayPath(String)
     */
    public String getAbsoluteKeyStorePath(IProject project)
            throws CoreException;

    /**
     * Returns the "alias" string identifying the key and certificate that will
     * be used to sign the project.
     * 
     * @return <code>String</code> containing the alias identifying the key and
     *         certificate.
     * @see #setKeyAlias(String)
     */
    public String getKeyAlias();

    /**
     * Returns the key password, if passwords are being saved. Will return
     * <code>null</code> if passwords are not being saved.
     * 
     * @return <code>String</code> containing the key password.
     * @see #setKeyPassword(String)
     */
    public String getKeyPassword();

    /**
     * Returns the display string representing the keystore path. This may be an
     * absolute path, or it may be a project-relative path. Relative paths are
     * of the form "<code>$/[Folder[/Folder...]]filename</code>". Absolute paths
     * have OS-dependent form.
     * 
     * @return <code>String</code> containing displayed path.
     * @see #setKeyStoreDisplayPath(String)
     * @see #isKeyStorePathExternal()
     */
    public String getKeyStoreDisplayPath();

    /**
     * Returns the password for the keystore file, if passwords are being saved.
     * 
     * @return <code>String</code> containing keystore password.
     * @see #setKeyStorePassword(String)
     */
    public String getKeyStorePassword();

    /**
     * Returns the crypto provider string. <code>null</code> indicates the
     * system standard type. This string is passed to
     * <code>KeyStore.getInstance</code> as part of loading the keystore.
     * 
     * @return KeyStore provider string, or <code>null</code> to use the system
     *         default provider.
     * @see #setKeyStoreProvider(String)
     * @see java.security.KeyStore#getInstance(java.lang.String,
     *      java.lang.String)
     */
    public String getKeyStoreProvider();

    /**
     * Type of the keystore file. <code>null</code> indicates the system
     * standard type. This string is passed to <code>KeyStore.getInstance</code>
     * as part of loading the keystore.
     * 
     * @return <code>String</code> indicating keystore file type.
     * @see #setKeyStoreType(String)
     * @see java.security.KeyStore#getInstance(java.lang.String)
     * @see java.security.KeyStore#getInstance(java.lang.String,
     *      java.lang.String)
     */
    public String getKeyStoreType();

    /**
     * Returns the password storage method.
     * 
     * @return One of <code>PASSMETHOD_PROMPT</code>,
     *         <code>PASSMETHOD_IN_KEYRING</code> or
     *         <code>PASSMETHOD_IN_PROJECT</code>.
     * @see #setPasswordStorageMethod(int)
     * @see #PASSMETHOD_IN_KEYRING
     * @see #PASSMETHOD_IN_PROJECT
     * @see #PASSMETHOD_PROMPT
     */
    public int getPasswordStorageMethod();

    /**
     * Indicates whether or not the project is to be signed.
     * 
     * @return <code>true</code> if the project is to be signed,
     *         <code>false</code> otherwise
     * @see #setSignProject(boolean)
     */
    public boolean isSignProject();

    /**
     * Indicates whether or not the project has signing specific settings.
     * 
     * @return <code>true</code> if the project has specific signing settings
     *         <code>false</code> in case it used preference settings.
     * @see #setProjectSpecific(boolean)
     */
    public boolean isProjectSpecific();

    /**
     * Indicates whether the keystore path is external to the project or
     * project-relative.
     * 
     * @return <code>true</code> if the keystore path is external to the
     *         project, <code>false</code> if it's relative to the current
     *         project.
     * @see #setKeyStoreDisplayPath(String)
     */
    public boolean isKeyStorePathExternal();

    /**
     * Sets the "alias" string identifying the key and certificate that will be
     * used to sign the project.
     * 
     * @param strValue <code>String</code> containing the alias identifying the
     *            key and certificate.
     * @see #getKeyAlias()
     */
    public void setKeyAlias(String strValue);

    /**
     * Sets the key password, if passwords are being saved. Ignored if passwords
     * are not being saved.
     * 
     * @param strValue <code>String</code> containing the key password.
     * @see #getKeyPassword()
     */
    public void setKeyPassword(String strValue);

    /**
     * Sets the display string representing the keystore path. This may be an
     * absolute path, or it may be a project-relative path.
     * 
     * @param path <code>String</code> containing displayed path.
     * @see #getKeyStoreDisplayPath()
     */
    public void setKeyStoreDisplayPath(String path);

    /**
     * Sets the password for the keystore file.
     * 
     * @param strValue <code>String</code> containing keystore password.
     * @see #getKeyStorePassword()
     */
    public void setKeyStorePassword(String strValue);

    /**
     * Sets the crypto provider string. <code>null</code> indicates the system
     * standard type.
     * 
     * @param strValue KeyStore provider string, or <code>null</code> to use the
     *            system default provider.
     * @see #getKeyStoreProvider()
     * @see java.security.KeyStore#getInstance(java.lang.String,
     *      java.lang.String)
     */
    public void setKeyStoreProvider(String strValue);

    /**
     * Type of the keystore file. <code>null</code> indicates the system
     * standard type. This string is passed to <code>KeyStore.getInstance</code>
     * as part of loading the keystore.
     * 
     * @param strValue KeyStore type string, or <code>null</code> to use the
     *            system default type.
     * @see #getKeyStoreType()
     * @see java.security.KeyStore#getInstance(java.lang.String)
     * @see java.security.KeyStore#getInstance(java.lang.String,
     *      java.lang.String)
     */
    public void setKeyStoreType(String strValue);

    /**
     * Sets the password storage method.
     * 
     * @param nMethod One of <code>PASSMETHOD_PROMPT</code>,
     *            <code>PASSMETHOD_IN_KEYRING</code> or
     *            <code>PASSMETHOD_IN_PROJECT</code>.
     * @see #getPasswordStorageMethod()
     * @see #PASSMETHOD_IN_KEYRING
     * @see #PASSMETHOD_IN_PROJECT
     * @see #PASSMETHOD_PROMPT
     */
    public void setPasswordStorageMethod(int nMethod);

    /**
     * Indicates whether or not the project is to be signed.
     * 
     * @param bValue <code>true</code> if the project is to be signed,
     *            <code>false</code> otherwise.
     * @see #isSignProject()
     */
    public void setSignProject(boolean bValue);

    /**
     * Indicates whether or not the project has specific signing settings.
     * 
     * @param bValue <code>true</code> if the project has specific signing
     *            settings <code>false
     * 				 </code> otherwise.
     * @see #isProjectSpecific()
     */
    public void setProjectSpecific(boolean bValue);
}
