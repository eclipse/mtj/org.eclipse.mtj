/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     David Marques (Motorola) - Adding onEnter and onExit methods.
 *     David Marques (Motorola) - Refactoring builders.
 */
package org.eclipse.mtj.internal.core.build;

import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceStatus;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.build.MTJBuildState;
import org.eclipse.mtj.core.project.IMTJProject;
import org.eclipse.mtj.internal.core.project.midp.MTJProjectConverter;
import org.eclipse.mtj.internal.core.project.midp.MidletSuiteFactory;

/**
 * MTJIncrementalProjectBuilder class extends {@link IncrementalProjectBuilder}
 * in order to enable the usage of a {@link BuildStateMachine} within the build
 * process.
 * 
 * @author David Marques
 */
public abstract class MTJIncrementalProjectBuilder extends
        IncrementalProjectBuilder {

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IncrementalProjectBuilder#build(int, java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
     */
    @SuppressWarnings("unchecked")
    protected final IProject[] build(int kind, Map args, IProgressMonitor monitor)
            throws CoreException {
        BuildStateMachine stateMachine = BuildStateMachine
                .getInstance(getMTJProject());
        IProject[] result = null;

        BuildSpecManipulator manipulator = new BuildSpecManipulator(
                getProject());
        // Checks whether the builder is the first builder
        // in order to start the build state machine.
        if (manipulator.isFirstBuilder(getBuilderId())) {
            stateMachine.start(monitor);
        }
        
        // Sends an state entry event to the hooks
        onStateEnter(stateMachine, monitor);

        // Clean on full build.
        if (kind == FULL_BUILD) {
			this.doClean(FULL_BUILD, monitor);
		}
        
        // Call the build implementation
        result = doBuild(kind, args, monitor);

        // Sends an state exit event to the hooks
        onStateExit(stateMachine, monitor);
        
        // Checks whether the builder is the last builder
        // in order to send a POST_BUILD event.
        if (manipulator.isLastBuilder(getBuilderId())) {
            stateMachine.changeState(MTJBuildState.POST_BUILD, monitor);
        }
        return result;
    }

    /* (non-Javadoc)
     * @see org.eclipse.core.resources.IncrementalProjectBuilder#clean(org.eclipse.core.runtime.IProgressMonitor)
     */
    protected final void clean(IProgressMonitor monitor) throws CoreException {
    	this.doClean(CLEAN_BUILD, monitor);
    }
    
	/**
	 * This method is called when the builder enters it's entry state,
	 * just before executing the doBuild method.
	 * 
	 * @param stateMachine current state machine.
	 * @param monitor progress monitor.
	 * @throws CoreException Any core error occurred.
	 */
	protected void onStateEnter(BuildStateMachine stateMachine, IProgressMonitor monitor) throws CoreException {
    	// Call the pre hooks
        stateMachine.changeState(getEnterState(), monitor);
	}

	/**
	 * This method is called when the builder enters it's exit state,
	 * just after executing the doBuild method.
	 * 
	 * @param stateMachine current state machine.
	 * @param monitor progress monitor.
	 * @throws CoreException Any core error occurred.
	 */
	protected void onStateExit(BuildStateMachine stateMachine, IProgressMonitor monitor) throws CoreException {
        // Call the post hooks
        stateMachine.changeState(getExitState(), monitor);
	}
	
	/**
     * Gets the builder {@link MTJProjectConverter} instance.
     * 
     * @return mtj project instance.
     */
    protected IMTJProject getMTJProject() {
        IJavaProject javaProject = JavaCore.create(getProject());
        IMTJProject midletSuite = MidletSuiteFactory
                .getMidletSuiteProject(javaProject);
        return midletSuite;
    }

    /**
     * Checks whether the user has canceled the operation.
     * 
     * @param monitor progress monitor.
     * @param forgetBuildState true if the build state must
     * be forgotten.
     */
    protected void assertCancelation(IProgressMonitor monitor, boolean forgetBuildState) {
        if (monitor.isCanceled()) {
        	if (forgetBuildState) {
        		this.forgetLastBuiltState();
			}
			throw new OperationCanceledException();
		}
     }
    
    /**
     * Runs this builder in the specified manner. Subclasses should implement
     * this method to do the processing they require.
     * <p>
     * If the build kind is {@link #INCREMENTAL_BUILD} or {@link #AUTO_BUILD},
     * the <code>getDelta</code> method can be used during the invocation of
     * this method to obtain information about what changes have occurred since
     * the last invocation of this method. Any resource delta acquired is valid
     * only for the duration of the invocation of this method.
     * </p>
     * <p>
     * After completing a build, this builder may return a list of projects for
     * which it requires a resource delta the next time it is run. This
     * builder's project is implicitly included and need not be specified. The
     * build mechanism will attempt to maintain and compute deltas relative to
     * the identified projects when asked the next time this builder is run.
     * Builders must re-specify the list of interesting projects every time they
     * are run as this is not carried forward beyond the next build. Projects
     * mentioned in return value but which do not exist will be ignored and no
     * delta will be made available for them.
     * </p>
     * <p>
     * This method is long-running; progress and cancellation are provided by
     * the given progress monitor. All builders should report their progress and
     * honor cancel requests in a timely manner. Cancelation requests should be
     * propagated to the caller by throwing
     * <code>OperationCanceledException</code>.
     * </p>
     * <p>
     * All builders should try to be robust in the face of trouble. In
     * situations where failing the build by throwing <code>CoreException</code>
     * is the only option, a builder has a choice of how best to communicate the
     * problem back to the caller. One option is to use the
     * {@link IResourceStatus#BUILD_FAILED} status code along with a suitable
     * message; another is to use a {@link MultiStatus} containing finer-grained
     * problem diagnoses.
     * </p>
     * 
     * @param kind the kind of build being requested. Valid values are
     *            <ul>
     *            <li>{@link #FULL_BUILD} - indicates a full build.</li>
     *            <li>{@link #INCREMENTAL_BUILD}- indicates an incremental
     *            build.</li>
     *            <li>{@link #AUTO_BUILD} - indicates an automatically triggered
     *            incremental build (autobuilding on).</li>
     *            </ul>
     * @param args a table of builder-specific arguments keyed by argument name
     *            (key type: <code>String</code>, value type:
     *            <code>String</code>); <code>null</code> is equivalent to an
     *            empty map
     * @param monitor a progress monitor, or <code>null</code> if progress
     *            reporting and cancellation are not desired
     * @return the list of projects for which this builder would like deltas the
     *         next time it is run or <code>null</code> if none
     * @exception CoreException if this build fails.
     * @see IProject#build(int, String, Map, IProgressMonitor)
     */
    protected abstract IProject[] doBuild(int kind, Map<?, ?> args,
            IProgressMonitor monitor) throws CoreException;

    
    /**
     * Called in order to clean the resources from the last
     * build.
     * 
     * @param kind {@link IncrementalProjectBuilder#CLEAN_BUILD} or
     * {@link IncrementalProjectBuilder#FULL_BUILD}.
     * @param monitor progress monitor.
     * @exception CoreException if this clean fails.
     */
    protected abstract void doClean(int kind, IProgressMonitor monitor)
			throws CoreException;
    
    /**
     * Gets the builder id.
     * 
     * @return id.
     */
    protected abstract String getBuilderId();

    /**
     * Gets the state before executing the build.
     * 
     * @return enter state.
     */
    protected abstract MTJBuildState getEnterState();

    /**
     * Gets the state after executing the build.
     * 
     * @return exit state.
     */
    protected abstract MTJBuildState getExitState();

}
