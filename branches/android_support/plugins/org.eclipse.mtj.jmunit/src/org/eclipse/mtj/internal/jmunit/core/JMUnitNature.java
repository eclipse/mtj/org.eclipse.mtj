/**
 * Copyright (c) 2009 Motorola.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     David Marques (Motorola) - Initial version
 *     Eric S. Dias  (Motorola) - Fixing disable JMUnit causes confusion
 *     David Marques (Motorola) - Keeping user defined exclusion filters.
 *     
 * @since 0.9.1
 */
package org.eclipse.mtj.internal.jmunit.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.mtj.core.project.MTJNature;
import org.eclipse.mtj.internal.core.util.JavaProjectAdapter;
import org.eclipse.mtj.internal.jmunit.util.JMUnitTestsVisitor;

/**
 * JMUnitNature class defines a nature behavior of having a JMUnit library on
 * the class path and the ability of exporting the library dependencies into the
 * application.
 * 
 * @author David Marques
 * @since 0.9.1
 */
public class JMUnitNature extends MTJNature {

    private static final String JMUNIT = "JMUnit";
    
    // This static variable holds the tests excluded
    // when disabling jmunit since it can not resolve
    // these classes upon enablement since they are not
    // on the project build path.
    private static Map<IClasspathEntry, IPath[]>  testExclusionPaths;
    
    public JMUnitNature() {
        if (JMUnitNature.testExclusionPaths == null) {
            JMUnitNature.testExclusionPaths = new HashMap<IClasspathEntry, IPath[]>();
        }
    }
    
    /**
     * @see org.eclipse.core.resources.IProjectNature#configure()
     */
    public void configure() throws CoreException {
        IJavaProject javaProject = JavaCore.create(getProject());
        List<IClasspathEntry> oldEntries = new LinkedList<IClasspathEntry>(
                Arrays.asList(javaProject.getRawClasspath()));
        List<IClasspathEntry> newEntries = new LinkedList<IClasspathEntry>();

        for (IClasspathEntry entry : oldEntries) {
            boolean changed = false;
            switch (entry.getEntryKind()) {
                case IClasspathEntry.CPE_CONTAINER:
                    if (entry.getPath().toString().contains(JMUNIT)) {
                        newEntries.add(JavaCore.newContainerEntry(entry
                                .getPath(), true));
                        changed = true;
                    }
                    break;
                case IClasspathEntry.CPE_SOURCE:
                    List<IPath> allPaths = new ArrayList<IPath>(Arrays.asList(entry.getExclusionPatterns()));
                    IPath testExclusions[] = JMUnitNature.testExclusionPaths.get(entry);
                    if (testExclusions != null) {
                        allPaths.removeAll(Arrays.asList(testExclusions));
                    }
                    newEntries.add(JavaCore.newSourceEntry(entry
                            .getPath(), allPaths.toArray(new IPath[0x00])));
                    changed = true;
                    break;
            }
            if (!changed) {
                newEntries.add(entry);
            }
        }

        IClasspathEntry[] array = newEntries
                .toArray(new IClasspathEntry[oldEntries.size()]);
        if (!javaProject.hasClasspathCycle(array)) {
            javaProject.setRawClasspath(null, new NullProgressMonitor());
            javaProject.setRawClasspath(array, new NullProgressMonitor());
        }
    }

    /**
     * @see org.eclipse.core.resources.IProjectNature#deconfigure()
     */
    public void deconfigure() throws CoreException {
        IJavaProject javaProject = JavaCore.create(getProject());
        List<IClasspathEntry> oldEntries = new LinkedList<IClasspathEntry>(
                Arrays.asList(javaProject.getRawClasspath()));
        List<IClasspathEntry> newEntries = new LinkedList<IClasspathEntry>();

        JavaProjectAdapter adapter = new JavaProjectAdapter(javaProject);
        JMUnitTestsVisitor visitor = new JMUnitTestsVisitor(javaProject);
        adapter.accept(visitor);
        IPath[] testPaths = visitor.getJMunitTests();
        
        for (IClasspathEntry entry : oldEntries) {
            boolean changed = false;
            switch (entry.getEntryKind()) {
                case IClasspathEntry.CPE_CONTAINER:
                    if (entry.getPath().toString().contains(JMUNIT)) {
                        newEntries.add(JavaCore.newContainerEntry(entry
                                .getPath(), false));
                        changed = true;
                    }
                    break;
                case IClasspathEntry.CPE_SOURCE:
                    IPath[] exclusionPatterns = entry.getExclusionPatterns();

                    List<IPath> allPaths = new ArrayList<IPath>();
                    allPaths.addAll(Arrays.asList(exclusionPatterns));
                    allPaths.addAll(Arrays.asList(testPaths));
                    
                    IClasspathEntry newEntry = JavaCore.newSourceEntry(entry
                            .getPath(), allPaths.toArray(new IPath[0x00]));
                    newEntries.add(newEntry);
                    
                    if (testPaths.length > 0x00) {                        
                        JMUnitNature.testExclusionPaths.put(newEntry, testPaths);
                    }
                    changed = true;
                    break;
            }
            if (!changed) {
                newEntries.add(entry);
            }
        }

        IClasspathEntry[] array = newEntries
                .toArray(new IClasspathEntry[oldEntries.size()]);
        if (!javaProject.hasClasspathCycle(array)) {
            javaProject.setRawClasspath(null, new NullProgressMonitor());
            javaProject.setRawClasspath(array, new NullProgressMonitor());
        }
    }

}
